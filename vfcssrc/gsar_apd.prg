* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_apd                                                        *
*              Dispositivi hardware                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_172]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-04                                                      *
* Last revis.: 2014-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_apd"))

* --- Class definition
define class tgsar_apd as StdForm
  Top    = 26
  Left   = 102

  * --- Standard Properties
  Width  = 539
  Height = 392+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-08-26"
  HelpContextID=160003945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Constant Properties
  DIS_HARD_IDX = 0
  cFile = "DIS_HARD"
  cKeySelect = "DHCODICE"
  cKeyWhere  = "DHCODICE=this.w_DHCODICE"
  cKeyWhereODBC = '"DHCODICE="+cp_ToStrODBC(this.w_DHCODICE)';

  cKeyWhereODBCqualified = '"DIS_HARD.DHCODICE="+cp_ToStrODBC(this.w_DHCODICE)';

  cPrg = "gsar_apd"
  cComment = "Dispositivi hardware"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DHCODICE = space(5)
  w_DHDESCRI = space(40)
  w_DHTIPDIS = space(1)
  o_DHTIPDIS = space(1)
  w_DHTIPDIS = space(1)
  w_DHNUMPOR = 0
  w_TIPOPENNA = space(2)
  o_TIPOPENNA = space(2)
  w_TIPOTEL = space(2)
  o_TIPOTEL = space(2)
  w_DHPROGRA = space(18)
  o_DHPROGRA = space(18)
  w_DHPROPAR = space(254)
  w_DHTRAPER = space(0)
  w_DHELIFIL = space(1)
  w_DH__BAUD = 0
  w_DHPARITA = space(1)
  w_DHBITSTP = 0
  w_DHBITDAT = 0
  w_DHBUFRIC = 0
  w_DHRITARD = 0
  w_DHBUFTRA = 0
  w_DHNAKDEL = 0
  w_DHESCDEL = 0
  w_DHNUMTER = space(1)
  w_DHFAMCAS = space(1)
  o_DHFAMCAS = space(1)
  w_DHPAGEST = space(1)
  w_DHPOSPAT = space(250)
  w_DHPATFIL = space(250)
  w_DHMATCAS = space(20)
  w_DHPSWPOS = space(10)
  w_DHSTADES = space(1)
  w_DH__BAUD = 0
  w_DHNUMCAR = 0
  w_DHNUMFIS = 0
  w_DHPASQTA = space(1)
  w_DHCARDAT = space(1)
  w_DHFLMBRG = space(1)
  w_SERIAL = space(10)
  w_DHLOTMAT = space(1)
  w_NOTETS = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DIS_HARD','gsar_apd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_apdPag1","gsar_apd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Penne ottiche/Centralini")
      .Pages(1).HelpContextID = 215806120
      .Pages(2).addobject("oPag","tgsar_apdPag2","gsar_apd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Registratori di cassa")
      .Pages(2).HelpContextID = 252809479
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDHCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DIS_HARD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIS_HARD_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIS_HARD_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DHCODICE = NVL(DHCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DIS_HARD where DHCODICE=KeySet.DHCODICE
    *
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIS_HARD')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIS_HARD.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIS_HARD '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DHCODICE',this.w_DHCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SERIAL = 'TEST'
        .w_NOTETS = CalNoteTs()
        .w_DHCODICE = NVL(DHCODICE,space(5))
        .w_DHDESCRI = NVL(DHDESCRI,space(40))
        .w_DHTIPDIS = NVL(DHTIPDIS,space(1))
        .w_DHTIPDIS = NVL(DHTIPDIS,space(1))
        .w_DHNUMPOR = NVL(DHNUMPOR,0)
        .w_TIPOPENNA = ICASE(.w_DHPROGRA='GSUT_BFP','F1',.w_DHPROGRA='GSUT1BFP','F2',.w_DHPROGRA='GSUT_BDG','GC',.w_DHPROGRA='GSUT_BHT','BH',.w_DHPROGRA='GSUT1BHT','B1',.w_DHPROGRA='GSUT_BDT','FT',.w_DHPROGRA='GSUT_BDX','EX',EMPTY(.w_DHPROGRA),'BH', .w_DHPROGRA='GSUT_BDL', 'DL', .w_DHPROGRA='GSUT1BDL', 'D1' , 'AL')
        .w_TIPOTEL = ICASE(.w_DHPROGRA='GSFA_BTP', 'TC', EMPTY(.w_DHPROGRA), 'TC', 'AL')
        .w_DHPROGRA = NVL(DHPROGRA,space(18))
        .w_DHPROPAR = NVL(DHPROPAR,space(254))
        .w_DHTRAPER = NVL(DHTRAPER,space(0))
        .w_DHELIFIL = NVL(DHELIFIL,space(1))
        .w_DH__BAUD = NVL(DH__BAUD,0)
        .w_DHPARITA = NVL(DHPARITA,space(1))
        .w_DHBITSTP = NVL(DHBITSTP,0)
        .w_DHBITDAT = NVL(DHBITDAT,0)
        .w_DHBUFRIC = NVL(DHBUFRIC,0)
        .w_DHRITARD = NVL(DHRITARD,0)
        .w_DHBUFTRA = NVL(DHBUFTRA,0)
        .w_DHNAKDEL = NVL(DHNAKDEL,0)
        .w_DHESCDEL = NVL(DHESCDEL,0)
        .w_DHNUMTER = NVL(DHNUMTER,space(1))
        .w_DHFAMCAS = NVL(DHFAMCAS,space(1))
        .w_DHPAGEST = NVL(DHPAGEST,space(1))
        .w_DHPOSPAT = NVL(DHPOSPAT,space(250))
        .w_DHPATFIL = NVL(DHPATFIL,space(250))
        .w_DHMATCAS = NVL(DHMATCAS,space(20))
        .w_DHPSWPOS = NVL(DHPSWPOS,space(10))
        .w_DHSTADES = NVL(DHSTADES,space(1))
        .w_DH__BAUD = NVL(DH__BAUD,0)
        .w_DHNUMCAR = NVL(DHNUMCAR,0)
        .w_DHNUMFIS = NVL(DHNUMFIS,0)
        .w_DHPASQTA = NVL(DHPASQTA,space(1))
        .w_DHCARDAT = NVL(DHCARDAT,space(1))
        .w_DHFLMBRG = NVL(DHFLMBRG,space(1))
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .w_DHLOTMAT = NVL(DHLOTMAT,space(1))
        cp_LoadRecExtFlds(this,'DIS_HARD')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page2.oPag.oBtn_2_4.enabled = this.oPgFrm.Page2.oPag.oBtn_2_4.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_apd
    * Valorizzo Combo
    this.w_TIPOPENNA=ICASE(This.w_DHPROGRA='GSUT_BFP','F1',;
    This.w_DHPROGRA='GSUT1BFP','F2',;
    This.w_DHPROGRA='GSUT_BDG','GC',;
    This.w_DHPROGRA='GSUT_BHT','BH',;
    This.w_DHPROGRA='GSUT1BHT','B1',;
    This.w_DHPROGRA='GSUT_BDT','FT',;
    This.w_DHPROGRA='GSUT_BDX','EX',;
    this.w_DHPROGRA='GSUT_BDL', 'DL', ;
    this.w_DHPROGRA='GSUT1BDL', 'D1', ;
    EMPTY(This.w_DHPROGRA),'BH','AL')
    
    * Valorizzo Combo
    this.w_TIPOTEL = ICASE(This.w_DHPROGRA='GSFA_BTP', 'TC', EMPTY(This.w_DHPROGRA),'TC', 'AL')
    
    This.SetControlsValue()
    This.mHideControls()
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DHCODICE = space(5)
      .w_DHDESCRI = space(40)
      .w_DHTIPDIS = space(1)
      .w_DHTIPDIS = space(1)
      .w_DHNUMPOR = 0
      .w_TIPOPENNA = space(2)
      .w_TIPOTEL = space(2)
      .w_DHPROGRA = space(18)
      .w_DHPROPAR = space(254)
      .w_DHTRAPER = space(0)
      .w_DHELIFIL = space(1)
      .w_DH__BAUD = 0
      .w_DHPARITA = space(1)
      .w_DHBITSTP = 0
      .w_DHBITDAT = 0
      .w_DHBUFRIC = 0
      .w_DHRITARD = 0
      .w_DHBUFTRA = 0
      .w_DHNAKDEL = 0
      .w_DHESCDEL = 0
      .w_DHNUMTER = space(1)
      .w_DHFAMCAS = space(1)
      .w_DHPAGEST = space(1)
      .w_DHPOSPAT = space(250)
      .w_DHPATFIL = space(250)
      .w_DHMATCAS = space(20)
      .w_DHPSWPOS = space(10)
      .w_DHSTADES = space(1)
      .w_DH__BAUD = 0
      .w_DHNUMCAR = 0
      .w_DHNUMFIS = 0
      .w_DHPASQTA = space(1)
      .w_DHCARDAT = space(1)
      .w_DHFLMBRG = space(1)
      .w_SERIAL = space(10)
      .w_DHLOTMAT = space(1)
      .w_NOTETS = space(0)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_DHTIPDIS = IIF(IsAlt(),'P','R')
        .w_DHTIPDIS = IIF(IsAlt(),'P','R')
        .w_DHNUMPOR = 1
        .w_TIPOPENNA = ICASE(.w_DHPROGRA='GSUT_BFP','F1',.w_DHPROGRA='GSUT1BFP','F2',.w_DHPROGRA='GSUT_BDG','GC',.w_DHPROGRA='GSUT_BHT','BH',.w_DHPROGRA='GSUT1BHT','B1',.w_DHPROGRA='GSUT_BDT','FT',.w_DHPROGRA='GSUT_BDX','EX',EMPTY(.w_DHPROGRA),'BH', .w_DHPROGRA='GSUT_BDL', 'DL', .w_DHPROGRA='GSUT1BDL', 'D1' , 'AL')
        .w_TIPOTEL = ICASE(.w_DHPROGRA='GSFA_BTP', 'TC', EMPTY(.w_DHPROGRA), 'TC', 'AL')
        .w_DHPROGRA = IIF(.w_DHTIPDIS<>'C', ICASE(.w_TIPOPENNA='GC', 'GSUT_BDG',.w_TIPOPENNA='F1','GSUT_BFP',.w_TIPOPENNA='F2','GSUT1BFP',.w_TIPOPENNA='BH','GSUT_BHT',.w_TIPOPENNA='B1','GSUT1BHT',.w_TIPOPENNA='FT','GSUT_BDT',.w_TIPOPENNA='EX','GSUT_BDX',.w_TIPOPENNA='AL','',.w_TIPOPENNA='DL', 'GSUT_BDL', .w_TIPOPENNA='D1', 'GSUT1BDL', .w_DHPROGRA), ICASE(.w_TIPOTEL='TC','GSFA_BTP',.w_TIPOTEL='AL','',.w_DHPROGRA))
        .w_DHPROPAR = IIF(.w_DHTIPDIS='P', ICASE(.w_TIPOPENNA='GC','PDT600Z.TXT',.w_TIPOPENNA='F0','adhoc.hex',.w_TIPOPENNA='BH','DATI.DAT',.w_TIPOPENNA='B1','DATI.DAT', .w_TIPOPENNA='D1', 'Documenti.csv', .w_TIPOPENNA='DL','Rettifiche.csv' , ''), "")
        .w_DHTRAPER = ''
        .w_DHELIFIL = 'N'
        .w_DH__BAUD = IIF(.w_DHTIPDIS='R',.w_DH__BAUD,iif(.w_TIPOPENNA$'F1-F2',9600,19200))
        .w_DHPARITA = iif(.w_TIPOPENNA='GC','0',iif(.w_TIPOPENNA='F1','2',iif(.w_TIPOPENNA='F2','3','0')))
        .w_DHBITSTP = 1
        .w_DHBITDAT = iif(.w_TIPOPENNA$'F1-F0-F2',7,8)
        .w_DHBUFRIC = iif(.w_TIPOPENNA$'F1-F2',4096,0)
        .w_DHRITARD = iif(.w_TIPOPENNA$'F1-F2',99,500)
        .w_DHBUFTRA = iif(.w_TIPOPENNA$'F1-F2',2048,0)
        .w_DHNAKDEL = 100
        .w_DHESCDEL = 30
        .w_DHNUMTER = 'A'
        .w_DHFAMCAS = 'S'
        .w_DHPAGEST = IIF(.w_DHFAMCAS = 'H', .w_DHPAGEST, 'N')
        .w_DHPOSPAT = IIF(.w_DHTIPDIS='R',.w_DHPOSPAT,' ')
        .w_DHPATFIL = IIF(.w_DHTIPDIS='R',.w_DHPATFIL,' ')
        .w_DHMATCAS = IIF(.w_DHTIPDIS='R',.w_DHMATCAS,' ')
        .w_DHPSWPOS = IIF(.w_DHTIPDIS='R',.w_DHPSWPOS,' ')
        .w_DHSTADES = 'D'
        .w_DH__BAUD = IIF(.w_DHTIPDIS='P',.w_DH__BAUD,9600)
          .DoRTCalc(30,31,.f.)
        .w_DHPASQTA = IIF(.w_DHFAMCAS = 'W', .w_DHPASQTA, ' ')
        .w_DHCARDAT = ' '
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
          .DoRTCalc(34,34,.f.)
        .w_SERIAL = 'TEST'
        .w_DHLOTMAT = 'N'
        .w_NOTETS = CalNoteTs()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIS_HARD')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page2.oPag.oBtn_2_4.enabled = this.oPgFrm.Page2.oPag.oBtn_2_4.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDHCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oDHDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oDHTIPDIS_1_3.enabled = i_bVal
      .Page1.oPag.oDHTIPDIS_1_4.enabled = i_bVal
      .Page1.oPag.oDHNUMPOR_1_5.enabled = i_bVal
      .Page1.oPag.oTIPOPENNA_1_6.enabled = i_bVal
      .Page1.oPag.oTIPOTEL_1_7.enabled = i_bVal
      .Page1.oPag.oDHPROGRA_1_8.enabled = i_bVal
      .Page1.oPag.oDHPROPAR_1_9.enabled = i_bVal
      .Page1.oPag.oDHTRAPER_1_10.enabled = i_bVal
      .Page1.oPag.oDHELIFIL_1_11.enabled = i_bVal
      .Page1.oPag.oDH__BAUD_1_12.enabled_(i_bVal)
      .Page1.oPag.oDHPARITA_1_13.enabled_(i_bVal)
      .Page1.oPag.oDHBITSTP_1_14.enabled_(i_bVal)
      .Page1.oPag.oDHBITDAT_1_15.enabled_(i_bVal)
      .Page1.oPag.oDHBUFRIC_1_16.enabled = i_bVal
      .Page1.oPag.oDHRITARD_1_17.enabled = i_bVal
      .Page1.oPag.oDHBUFTRA_1_18.enabled = i_bVal
      .Page1.oPag.oDHNAKDEL_1_19.enabled = i_bVal
      .Page1.oPag.oDHESCDEL_1_20.enabled = i_bVal
      .Page1.oPag.oDHNUMTER_1_36.enabled = i_bVal
      .Page2.oPag.oDHFAMCAS_2_1.enabled = i_bVal
      .Page2.oPag.oDHPAGEST_2_2.enabled = i_bVal
      .Page2.oPag.oDHPOSPAT_2_3.enabled = i_bVal
      .Page2.oPag.oDHPATFIL_2_5.enabled = i_bVal
      .Page2.oPag.oDHMATCAS_2_7.enabled = i_bVal
      .Page2.oPag.oDHPSWPOS_2_8.enabled = i_bVal
      .Page2.oPag.oDHSTADES_2_9.enabled = i_bVal
      .Page2.oPag.oDH__BAUD_2_10.enabled_(i_bVal)
      .Page2.oPag.oDHNUMCAR_2_11.enabled = i_bVal
      .Page2.oPag.oDHNUMFIS_2_12.enabled = i_bVal
      .Page2.oPag.oDHPASQTA_2_13.enabled = i_bVal
      .Page2.oPag.oDHCARDAT_2_14.enabled = i_bVal
      .Page2.oPag.oDHFLMBRG_2_15.enabled = i_bVal
      .Page1.oPag.oDHLOTMAT_1_43.enabled = i_bVal
      .Page2.oPag.oNOTETS_2_28.enabled = i_bVal
      .Page2.oPag.oBtn_2_4.enabled = .Page2.oPag.oBtn_2_4.mCond()
      .Page2.oPag.oBtn_2_6.enabled = .Page2.oPag.oBtn_2_6.mCond()
      .Page2.oPag.oBtn_2_22.enabled = i_bVal
      .Page1.oPag.oObj_1_40.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDHCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDHCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DIS_HARD',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsar_apd
    * disabilito il tab elenco
    if g_Application<>"ADHOC REVOLUTION"
       this.oPgFrm.Pages(this.oPgFrm.PageCount-1).enabled=.f.
       this.oPgFrm.Pages(this.oPgFrm.PageCount-1).Caption=""
    endif
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHCODICE,"DHCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHDESCRI,"DHDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHTIPDIS,"DHTIPDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHTIPDIS,"DHTIPDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHNUMPOR,"DHNUMPOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHPROGRA,"DHPROGRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHPROPAR,"DHPROPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHTRAPER,"DHTRAPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHELIFIL,"DHELIFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DH__BAUD,"DH__BAUD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHPARITA,"DHPARITA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHBITSTP,"DHBITSTP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHBITDAT,"DHBITDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHBUFRIC,"DHBUFRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHRITARD,"DHRITARD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHBUFTRA,"DHBUFTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHNAKDEL,"DHNAKDEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHESCDEL,"DHESCDEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHNUMTER,"DHNUMTER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHFAMCAS,"DHFAMCAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHPAGEST,"DHPAGEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHPOSPAT,"DHPOSPAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHPATFIL,"DHPATFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHMATCAS,"DHMATCAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHPSWPOS,"DHPSWPOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHSTADES,"DHSTADES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DH__BAUD,"DH__BAUD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHNUMCAR,"DHNUMCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHNUMFIS,"DHNUMFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHPASQTA,"DHPASQTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHCARDAT,"DHCARDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHFLMBRG,"DHFLMBRG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DHLOTMAT,"DHLOTMAT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
    i_lTable = "DIS_HARD"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DIS_HARD_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DIS_HARD_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DIS_HARD
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIS_HARD')
        i_extval=cp_InsertValODBCExtFlds(this,'DIS_HARD')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DHCODICE,DHDESCRI,DHTIPDIS,DHNUMPOR,DHPROGRA"+;
                  ",DHPROPAR,DHTRAPER,DHELIFIL,DH__BAUD,DHPARITA"+;
                  ",DHBITSTP,DHBITDAT,DHBUFRIC,DHRITARD,DHBUFTRA"+;
                  ",DHNAKDEL,DHESCDEL,DHNUMTER,DHFAMCAS,DHPAGEST"+;
                  ",DHPOSPAT,DHPATFIL,DHMATCAS,DHPSWPOS,DHSTADES"+;
                  ",DHNUMCAR,DHNUMFIS,DHPASQTA,DHCARDAT,DHFLMBRG"+;
                  ",DHLOTMAT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DHCODICE)+;
                  ","+cp_ToStrODBC(this.w_DHDESCRI)+;
                  ","+cp_ToStrODBC(this.w_DHTIPDIS)+;
                  ","+cp_ToStrODBC(this.w_DHNUMPOR)+;
                  ","+cp_ToStrODBC(this.w_DHPROGRA)+;
                  ","+cp_ToStrODBC(this.w_DHPROPAR)+;
                  ","+cp_ToStrODBC(this.w_DHTRAPER)+;
                  ","+cp_ToStrODBC(this.w_DHELIFIL)+;
                  ","+cp_ToStrODBC(this.w_DH__BAUD)+;
                  ","+cp_ToStrODBC(this.w_DHPARITA)+;
                  ","+cp_ToStrODBC(this.w_DHBITSTP)+;
                  ","+cp_ToStrODBC(this.w_DHBITDAT)+;
                  ","+cp_ToStrODBC(this.w_DHBUFRIC)+;
                  ","+cp_ToStrODBC(this.w_DHRITARD)+;
                  ","+cp_ToStrODBC(this.w_DHBUFTRA)+;
                  ","+cp_ToStrODBC(this.w_DHNAKDEL)+;
                  ","+cp_ToStrODBC(this.w_DHESCDEL)+;
                  ","+cp_ToStrODBC(this.w_DHNUMTER)+;
                  ","+cp_ToStrODBC(this.w_DHFAMCAS)+;
                  ","+cp_ToStrODBC(this.w_DHPAGEST)+;
                  ","+cp_ToStrODBC(this.w_DHPOSPAT)+;
                  ","+cp_ToStrODBC(this.w_DHPATFIL)+;
                  ","+cp_ToStrODBC(this.w_DHMATCAS)+;
                  ","+cp_ToStrODBC(this.w_DHPSWPOS)+;
                  ","+cp_ToStrODBC(this.w_DHSTADES)+;
                  ","+cp_ToStrODBC(this.w_DHNUMCAR)+;
                  ","+cp_ToStrODBC(this.w_DHNUMFIS)+;
                  ","+cp_ToStrODBC(this.w_DHPASQTA)+;
                  ","+cp_ToStrODBC(this.w_DHCARDAT)+;
                  ","+cp_ToStrODBC(this.w_DHFLMBRG)+;
                  ","+cp_ToStrODBC(this.w_DHLOTMAT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIS_HARD')
        i_extval=cp_InsertValVFPExtFlds(this,'DIS_HARD')
        cp_CheckDeletedKey(i_cTable,0,'DHCODICE',this.w_DHCODICE)
        INSERT INTO (i_cTable);
              (DHCODICE,DHDESCRI,DHTIPDIS,DHNUMPOR,DHPROGRA,DHPROPAR,DHTRAPER,DHELIFIL,DH__BAUD,DHPARITA,DHBITSTP,DHBITDAT,DHBUFRIC,DHRITARD,DHBUFTRA,DHNAKDEL,DHESCDEL,DHNUMTER,DHFAMCAS,DHPAGEST,DHPOSPAT,DHPATFIL,DHMATCAS,DHPSWPOS,DHSTADES,DHNUMCAR,DHNUMFIS,DHPASQTA,DHCARDAT,DHFLMBRG,DHLOTMAT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DHCODICE;
                  ,this.w_DHDESCRI;
                  ,this.w_DHTIPDIS;
                  ,this.w_DHNUMPOR;
                  ,this.w_DHPROGRA;
                  ,this.w_DHPROPAR;
                  ,this.w_DHTRAPER;
                  ,this.w_DHELIFIL;
                  ,this.w_DH__BAUD;
                  ,this.w_DHPARITA;
                  ,this.w_DHBITSTP;
                  ,this.w_DHBITDAT;
                  ,this.w_DHBUFRIC;
                  ,this.w_DHRITARD;
                  ,this.w_DHBUFTRA;
                  ,this.w_DHNAKDEL;
                  ,this.w_DHESCDEL;
                  ,this.w_DHNUMTER;
                  ,this.w_DHFAMCAS;
                  ,this.w_DHPAGEST;
                  ,this.w_DHPOSPAT;
                  ,this.w_DHPATFIL;
                  ,this.w_DHMATCAS;
                  ,this.w_DHPSWPOS;
                  ,this.w_DHSTADES;
                  ,this.w_DHNUMCAR;
                  ,this.w_DHNUMFIS;
                  ,this.w_DHPASQTA;
                  ,this.w_DHCARDAT;
                  ,this.w_DHFLMBRG;
                  ,this.w_DHLOTMAT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DIS_HARD_IDX,i_nConn)
      *
      * update DIS_HARD
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DIS_HARD')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DHDESCRI="+cp_ToStrODBC(this.w_DHDESCRI)+;
             ",DHTIPDIS="+cp_ToStrODBC(this.w_DHTIPDIS)+;
             ",DHNUMPOR="+cp_ToStrODBC(this.w_DHNUMPOR)+;
             ",DHPROGRA="+cp_ToStrODBC(this.w_DHPROGRA)+;
             ",DHPROPAR="+cp_ToStrODBC(this.w_DHPROPAR)+;
             ",DHTRAPER="+cp_ToStrODBC(this.w_DHTRAPER)+;
             ",DHELIFIL="+cp_ToStrODBC(this.w_DHELIFIL)+;
             ",DH__BAUD="+cp_ToStrODBC(this.w_DH__BAUD)+;
             ",DHPARITA="+cp_ToStrODBC(this.w_DHPARITA)+;
             ",DHBITSTP="+cp_ToStrODBC(this.w_DHBITSTP)+;
             ",DHBITDAT="+cp_ToStrODBC(this.w_DHBITDAT)+;
             ",DHBUFRIC="+cp_ToStrODBC(this.w_DHBUFRIC)+;
             ",DHRITARD="+cp_ToStrODBC(this.w_DHRITARD)+;
             ",DHBUFTRA="+cp_ToStrODBC(this.w_DHBUFTRA)+;
             ",DHNAKDEL="+cp_ToStrODBC(this.w_DHNAKDEL)+;
             ",DHESCDEL="+cp_ToStrODBC(this.w_DHESCDEL)+;
             ",DHNUMTER="+cp_ToStrODBC(this.w_DHNUMTER)+;
             ",DHFAMCAS="+cp_ToStrODBC(this.w_DHFAMCAS)+;
             ",DHPAGEST="+cp_ToStrODBC(this.w_DHPAGEST)+;
             ",DHPOSPAT="+cp_ToStrODBC(this.w_DHPOSPAT)+;
             ",DHPATFIL="+cp_ToStrODBC(this.w_DHPATFIL)+;
             ",DHMATCAS="+cp_ToStrODBC(this.w_DHMATCAS)+;
             ",DHPSWPOS="+cp_ToStrODBC(this.w_DHPSWPOS)+;
             ",DHSTADES="+cp_ToStrODBC(this.w_DHSTADES)+;
             ",DHNUMCAR="+cp_ToStrODBC(this.w_DHNUMCAR)+;
             ",DHNUMFIS="+cp_ToStrODBC(this.w_DHNUMFIS)+;
             ",DHPASQTA="+cp_ToStrODBC(this.w_DHPASQTA)+;
             ",DHCARDAT="+cp_ToStrODBC(this.w_DHCARDAT)+;
             ",DHFLMBRG="+cp_ToStrODBC(this.w_DHFLMBRG)+;
             ",DHLOTMAT="+cp_ToStrODBC(this.w_DHLOTMAT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DIS_HARD')
        i_cWhere = cp_PKFox(i_cTable  ,'DHCODICE',this.w_DHCODICE  )
        UPDATE (i_cTable) SET;
              DHDESCRI=this.w_DHDESCRI;
             ,DHTIPDIS=this.w_DHTIPDIS;
             ,DHNUMPOR=this.w_DHNUMPOR;
             ,DHPROGRA=this.w_DHPROGRA;
             ,DHPROPAR=this.w_DHPROPAR;
             ,DHTRAPER=this.w_DHTRAPER;
             ,DHELIFIL=this.w_DHELIFIL;
             ,DH__BAUD=this.w_DH__BAUD;
             ,DHPARITA=this.w_DHPARITA;
             ,DHBITSTP=this.w_DHBITSTP;
             ,DHBITDAT=this.w_DHBITDAT;
             ,DHBUFRIC=this.w_DHBUFRIC;
             ,DHRITARD=this.w_DHRITARD;
             ,DHBUFTRA=this.w_DHBUFTRA;
             ,DHNAKDEL=this.w_DHNAKDEL;
             ,DHESCDEL=this.w_DHESCDEL;
             ,DHNUMTER=this.w_DHNUMTER;
             ,DHFAMCAS=this.w_DHFAMCAS;
             ,DHPAGEST=this.w_DHPAGEST;
             ,DHPOSPAT=this.w_DHPOSPAT;
             ,DHPATFIL=this.w_DHPATFIL;
             ,DHMATCAS=this.w_DHMATCAS;
             ,DHPSWPOS=this.w_DHPSWPOS;
             ,DHSTADES=this.w_DHSTADES;
             ,DHNUMCAR=this.w_DHNUMCAR;
             ,DHNUMFIS=this.w_DHNUMFIS;
             ,DHPASQTA=this.w_DHPASQTA;
             ,DHCARDAT=this.w_DHCARDAT;
             ,DHFLMBRG=this.w_DHFLMBRG;
             ,DHLOTMAT=this.w_DHLOTMAT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DIS_HARD_IDX,i_nConn)
      *
      * delete DIS_HARD
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DHCODICE',this.w_DHCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_DHPROGRA<>.w_DHPROGRA
            .w_TIPOPENNA = ICASE(.w_DHPROGRA='GSUT_BFP','F1',.w_DHPROGRA='GSUT1BFP','F2',.w_DHPROGRA='GSUT_BDG','GC',.w_DHPROGRA='GSUT_BHT','BH',.w_DHPROGRA='GSUT1BHT','B1',.w_DHPROGRA='GSUT_BDT','FT',.w_DHPROGRA='GSUT_BDX','EX',EMPTY(.w_DHPROGRA),'BH', .w_DHPROGRA='GSUT_BDL', 'DL', .w_DHPROGRA='GSUT1BDL', 'D1' , 'AL')
        endif
        if .o_DHPROGRA<>.w_DHPROGRA
            .w_TIPOTEL = ICASE(.w_DHPROGRA='GSFA_BTP', 'TC', EMPTY(.w_DHPROGRA), 'TC', 'AL')
        endif
        if .o_TIPOPENNA<>.w_TIPOPENNA.or. .o_TIPOTEL<>.w_TIPOTEL.or. .o_DHTIPDIS<>.w_DHTIPDIS
            .w_DHPROGRA = IIF(.w_DHTIPDIS<>'C', ICASE(.w_TIPOPENNA='GC', 'GSUT_BDG',.w_TIPOPENNA='F1','GSUT_BFP',.w_TIPOPENNA='F2','GSUT1BFP',.w_TIPOPENNA='BH','GSUT_BHT',.w_TIPOPENNA='B1','GSUT1BHT',.w_TIPOPENNA='FT','GSUT_BDT',.w_TIPOPENNA='EX','GSUT_BDX',.w_TIPOPENNA='AL','',.w_TIPOPENNA='DL', 'GSUT_BDL', .w_TIPOPENNA='D1', 'GSUT1BDL', .w_DHPROGRA), ICASE(.w_TIPOTEL='TC','GSFA_BTP',.w_TIPOTEL='AL','',.w_DHPROGRA))
        endif
        if .o_TIPOPENNA<>.w_TIPOPENNA.or. .o_TIPOTEL<>.w_TIPOTEL.or. .o_DHTIPDIS<>.w_DHTIPDIS
            .w_DHPROPAR = IIF(.w_DHTIPDIS='P', ICASE(.w_TIPOPENNA='GC','PDT600Z.TXT',.w_TIPOPENNA='F0','adhoc.hex',.w_TIPOPENNA='BH','DATI.DAT',.w_TIPOPENNA='B1','DATI.DAT', .w_TIPOPENNA='D1', 'Documenti.csv', .w_TIPOPENNA='DL','Rettifiche.csv' , ''), "")
        endif
        if .o_DHTIPDIS<>.w_DHTIPDIS.or. .o_TIPOPENNA<>.w_TIPOPENNA
            .w_DHTRAPER = ''
        endif
        .DoRTCalc(11,11,.t.)
        if .o_TIPOPENNA<>.w_TIPOPENNA.or. .o_DHTIPDIS<>.w_DHTIPDIS
            .w_DH__BAUD = IIF(.w_DHTIPDIS='R',.w_DH__BAUD,iif(.w_TIPOPENNA$'F1-F2',9600,19200))
        endif
        if .o_TIPOPENNA<>.w_TIPOPENNA
            .w_DHPARITA = iif(.w_TIPOPENNA='GC','0',iif(.w_TIPOPENNA='F1','2',iif(.w_TIPOPENNA='F2','3','0')))
        endif
        if .o_TIPOPENNA<>.w_TIPOPENNA
            .w_DHBITSTP = 1
        endif
        if .o_TIPOPENNA<>.w_TIPOPENNA
            .w_DHBITDAT = iif(.w_TIPOPENNA$'F1-F0-F2',7,8)
        endif
        if .o_TIPOPENNA<>.w_TIPOPENNA
            .w_DHBUFRIC = iif(.w_TIPOPENNA$'F1-F2',4096,0)
        endif
        if .o_TIPOPENNA<>.w_TIPOPENNA
            .w_DHRITARD = iif(.w_TIPOPENNA$'F1-F2',99,500)
        endif
        if .o_TIPOPENNA<>.w_TIPOPENNA
            .w_DHBUFTRA = iif(.w_TIPOPENNA$'F1-F2',2048,0)
        endif
        .DoRTCalc(19,22,.t.)
        if .o_DHFAMCAS<>.w_DHFAMCAS
            .w_DHPAGEST = IIF(.w_DHFAMCAS = 'H', .w_DHPAGEST, 'N')
        endif
        if .o_DHTIPDIS<>.w_DHTIPDIS
            .w_DHPOSPAT = IIF(.w_DHTIPDIS='R',.w_DHPOSPAT,' ')
        endif
        if .o_DHTIPDIS<>.w_DHTIPDIS
            .w_DHPATFIL = IIF(.w_DHTIPDIS='R',.w_DHPATFIL,' ')
        endif
        if .o_DHTIPDIS<>.w_DHTIPDIS
            .w_DHMATCAS = IIF(.w_DHTIPDIS='R',.w_DHMATCAS,' ')
        endif
        if .o_DHTIPDIS<>.w_DHTIPDIS
            .w_DHPSWPOS = IIF(.w_DHTIPDIS='R',.w_DHPSWPOS,' ')
        endif
        .DoRTCalc(28,28,.t.)
        if .o_DHTIPDIS<>.w_DHTIPDIS
            .w_DH__BAUD = IIF(.w_DHTIPDIS='P',.w_DH__BAUD,9600)
        endif
        .DoRTCalc(30,31,.t.)
        if .o_DHFAMCAS<>.w_DHFAMCAS
            .w_DHPASQTA = IIF(.w_DHFAMCAS = 'W', .w_DHPASQTA, ' ')
        endif
        if .o_DHFAMCAS<>.w_DHFAMCAS
            .w_DHCARDAT = ' '
        endif
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(34,37,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTIPOPENNA_1_6.enabled = this.oPgFrm.Page1.oPag.oTIPOPENNA_1_6.mCond()
    this.oPgFrm.Page1.oPag.oTIPOTEL_1_7.enabled = this.oPgFrm.Page1.oPag.oTIPOTEL_1_7.mCond()
    this.oPgFrm.Page1.oPag.oDHPROGRA_1_8.enabled = this.oPgFrm.Page1.oPag.oDHPROGRA_1_8.mCond()
    this.oPgFrm.Page1.oPag.oDHPROPAR_1_9.enabled = this.oPgFrm.Page1.oPag.oDHPROPAR_1_9.mCond()
    this.oPgFrm.Page1.oPag.oDH__BAUD_1_12.enabled_(this.oPgFrm.Page1.oPag.oDH__BAUD_1_12.mCond())
    this.oPgFrm.Page1.oPag.oDHPARITA_1_13.enabled_(this.oPgFrm.Page1.oPag.oDHPARITA_1_13.mCond())
    this.oPgFrm.Page1.oPag.oDHBITSTP_1_14.enabled_(this.oPgFrm.Page1.oPag.oDHBITSTP_1_14.mCond())
    this.oPgFrm.Page1.oPag.oDHBITDAT_1_15.enabled_(this.oPgFrm.Page1.oPag.oDHBITDAT_1_15.mCond())
    this.oPgFrm.Page1.oPag.oDHBUFRIC_1_16.enabled = this.oPgFrm.Page1.oPag.oDHBUFRIC_1_16.mCond()
    this.oPgFrm.Page1.oPag.oDHRITARD_1_17.enabled = this.oPgFrm.Page1.oPag.oDHRITARD_1_17.mCond()
    this.oPgFrm.Page1.oPag.oDHBUFTRA_1_18.enabled = this.oPgFrm.Page1.oPag.oDHBUFTRA_1_18.mCond()
    this.oPgFrm.Page1.oPag.oDHNAKDEL_1_19.enabled = this.oPgFrm.Page1.oPag.oDHNAKDEL_1_19.mCond()
    this.oPgFrm.Page1.oPag.oDHESCDEL_1_20.enabled = this.oPgFrm.Page1.oPag.oDHESCDEL_1_20.mCond()
    this.oPgFrm.Page1.oPag.oDHNUMTER_1_36.enabled = this.oPgFrm.Page1.oPag.oDHNUMTER_1_36.mCond()
    this.oPgFrm.Page2.oPag.oDHPAGEST_2_2.enabled = this.oPgFrm.Page2.oPag.oDHPAGEST_2_2.mCond()
    this.oPgFrm.Page2.oPag.oDHPOSPAT_2_3.enabled = this.oPgFrm.Page2.oPag.oDHPOSPAT_2_3.mCond()
    this.oPgFrm.Page2.oPag.oDHPATFIL_2_5.enabled = this.oPgFrm.Page2.oPag.oDHPATFIL_2_5.mCond()
    this.oPgFrm.Page2.oPag.oDHMATCAS_2_7.enabled = this.oPgFrm.Page2.oPag.oDHMATCAS_2_7.mCond()
    this.oPgFrm.Page2.oPag.oDHPSWPOS_2_8.enabled = this.oPgFrm.Page2.oPag.oDHPSWPOS_2_8.mCond()
    this.oPgFrm.Page2.oPag.oDHSTADES_2_9.enabled = this.oPgFrm.Page2.oPag.oDHSTADES_2_9.mCond()
    this.oPgFrm.Page2.oPag.oDH__BAUD_2_10.enabled_(this.oPgFrm.Page2.oPag.oDH__BAUD_2_10.mCond())
    this.oPgFrm.Page2.oPag.oDHNUMCAR_2_11.enabled = this.oPgFrm.Page2.oPag.oDHNUMCAR_2_11.mCond()
    this.oPgFrm.Page2.oPag.oDHNUMFIS_2_12.enabled = this.oPgFrm.Page2.oPag.oDHNUMFIS_2_12.mCond()
    this.oPgFrm.Page2.oPag.oDHPASQTA_2_13.enabled = this.oPgFrm.Page2.oPag.oDHPASQTA_2_13.mCond()
    this.oPgFrm.Page2.oPag.oDHCARDAT_2_14.enabled = this.oPgFrm.Page2.oPag.oDHCARDAT_2_14.mCond()
    this.oPgFrm.Page2.oPag.oDHFLMBRG_2_15.enabled = this.oPgFrm.Page2.oPag.oDHFLMBRG_2_15.mCond()
    this.oPgFrm.Page1.oPag.oDHLOTMAT_1_43.enabled = this.oPgFrm.Page1.oPag.oDHLOTMAT_1_43.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_4.enabled = this.oPgFrm.Page2.oPag.oBtn_2_4.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_22.enabled = this.oPgFrm.Page2.oPag.oBtn_2_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(2).enabled=not(this.w_DHTIPDIS<>'R')
    local i_show1
    i_show1=not(IsAlt())
    this.oPgFrm.Pages(2).enabled=i_show1 and not(this.w_DHTIPDIS<>'R')
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Registratori di cassa"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    this.oPgFrm.Page1.oPag.oDHTIPDIS_1_3.visible=!this.oPgFrm.Page1.oPag.oDHTIPDIS_1_3.mHide()
    this.oPgFrm.Page1.oPag.oDHTIPDIS_1_4.visible=!this.oPgFrm.Page1.oPag.oDHTIPDIS_1_4.mHide()
    this.oPgFrm.Page1.oPag.oDHNUMPOR_1_5.visible=!this.oPgFrm.Page1.oPag.oDHNUMPOR_1_5.mHide()
    this.oPgFrm.Page1.oPag.oTIPOPENNA_1_6.visible=!this.oPgFrm.Page1.oPag.oTIPOPENNA_1_6.mHide()
    this.oPgFrm.Page1.oPag.oTIPOTEL_1_7.visible=!this.oPgFrm.Page1.oPag.oTIPOTEL_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDHTRAPER_1_10.visible=!this.oPgFrm.Page1.oPag.oDHTRAPER_1_10.mHide()
    this.oPgFrm.Page1.oPag.oDHELIFIL_1_11.visible=!this.oPgFrm.Page1.oPag.oDHELIFIL_1_11.mHide()
    this.oPgFrm.Page1.oPag.oDH__BAUD_1_12.visible=!this.oPgFrm.Page1.oPag.oDH__BAUD_1_12.mHide()
    this.oPgFrm.Page1.oPag.oDHPARITA_1_13.visible=!this.oPgFrm.Page1.oPag.oDHPARITA_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDHBITSTP_1_14.visible=!this.oPgFrm.Page1.oPag.oDHBITSTP_1_14.mHide()
    this.oPgFrm.Page1.oPag.oDHBITDAT_1_15.visible=!this.oPgFrm.Page1.oPag.oDHBITDAT_1_15.mHide()
    this.oPgFrm.Page1.oPag.oDHBUFRIC_1_16.visible=!this.oPgFrm.Page1.oPag.oDHBUFRIC_1_16.mHide()
    this.oPgFrm.Page1.oPag.oDHRITARD_1_17.visible=!this.oPgFrm.Page1.oPag.oDHRITARD_1_17.mHide()
    this.oPgFrm.Page1.oPag.oDHBUFTRA_1_18.visible=!this.oPgFrm.Page1.oPag.oDHBUFTRA_1_18.mHide()
    this.oPgFrm.Page1.oPag.oDHNAKDEL_1_19.visible=!this.oPgFrm.Page1.oPag.oDHNAKDEL_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDHESCDEL_1_20.visible=!this.oPgFrm.Page1.oPag.oDHESCDEL_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oDHNUMTER_1_36.visible=!this.oPgFrm.Page1.oPag.oDHNUMTER_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page2.oPag.oDHPAGEST_2_2.visible=!this.oPgFrm.Page2.oPag.oDHPAGEST_2_2.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_4.visible=!this.oPgFrm.Page2.oPag.oBtn_2_4.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_6.visible=!this.oPgFrm.Page2.oPag.oBtn_2_6.mHide()
    this.oPgFrm.Page2.oPag.oDHPASQTA_2_13.visible=!this.oPgFrm.Page2.oPag.oDHPASQTA_2_13.mHide()
    this.oPgFrm.Page2.oPag.oDHFLMBRG_2_15.visible=!this.oPgFrm.Page2.oPag.oDHFLMBRG_2_15.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_22.visible=!this.oPgFrm.Page2.oPag.oBtn_2_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_26.visible=!this.oPgFrm.Page2.oPag.oStr_2_26.mHide()
    this.oPgFrm.Page1.oPag.oDHLOTMAT_1_43.visible=!this.oPgFrm.Page1.oPag.oDHLOTMAT_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page2.oPag.oNOTETS_2_28.visible=!this.oPgFrm.Page2.oPag.oNOTETS_2_28.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_29.visible=!this.oPgFrm.Page2.oPag.oStr_2_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsar_apd
    * Messaggio di avvertimento nel caso attivo check di passaggio quantità al registratore
    If cEvent = "w_DHPASQTA Changed" And this.w_DHPASQTA = 'S'
       Ah_ErrorMsg("Assicurarsi di gestire solo due decimali per quantità e prezzo%0In caso contrario esisteranno differenze tra scontrino e vendita negozio","i")
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDHCODICE_1_1.value==this.w_DHCODICE)
      this.oPgFrm.Page1.oPag.oDHCODICE_1_1.value=this.w_DHCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDHDESCRI_1_2.value==this.w_DHDESCRI)
      this.oPgFrm.Page1.oPag.oDHDESCRI_1_2.value=this.w_DHDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDHTIPDIS_1_3.RadioValue()==this.w_DHTIPDIS)
      this.oPgFrm.Page1.oPag.oDHTIPDIS_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDHTIPDIS_1_4.RadioValue()==this.w_DHTIPDIS)
      this.oPgFrm.Page1.oPag.oDHTIPDIS_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDHNUMPOR_1_5.RadioValue()==this.w_DHNUMPOR)
      this.oPgFrm.Page1.oPag.oDHNUMPOR_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOPENNA_1_6.RadioValue()==this.w_TIPOPENNA)
      this.oPgFrm.Page1.oPag.oTIPOPENNA_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOTEL_1_7.RadioValue()==this.w_TIPOTEL)
      this.oPgFrm.Page1.oPag.oTIPOTEL_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDHPROGRA_1_8.value==this.w_DHPROGRA)
      this.oPgFrm.Page1.oPag.oDHPROGRA_1_8.value=this.w_DHPROGRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDHPROPAR_1_9.value==this.w_DHPROPAR)
      this.oPgFrm.Page1.oPag.oDHPROPAR_1_9.value=this.w_DHPROPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDHTRAPER_1_10.value==this.w_DHTRAPER)
      this.oPgFrm.Page1.oPag.oDHTRAPER_1_10.value=this.w_DHTRAPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDHELIFIL_1_11.RadioValue()==this.w_DHELIFIL)
      this.oPgFrm.Page1.oPag.oDHELIFIL_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDH__BAUD_1_12.RadioValue()==this.w_DH__BAUD)
      this.oPgFrm.Page1.oPag.oDH__BAUD_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDHPARITA_1_13.RadioValue()==this.w_DHPARITA)
      this.oPgFrm.Page1.oPag.oDHPARITA_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDHBITSTP_1_14.RadioValue()==this.w_DHBITSTP)
      this.oPgFrm.Page1.oPag.oDHBITSTP_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDHBITDAT_1_15.RadioValue()==this.w_DHBITDAT)
      this.oPgFrm.Page1.oPag.oDHBITDAT_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDHBUFRIC_1_16.value==this.w_DHBUFRIC)
      this.oPgFrm.Page1.oPag.oDHBUFRIC_1_16.value=this.w_DHBUFRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDHRITARD_1_17.value==this.w_DHRITARD)
      this.oPgFrm.Page1.oPag.oDHRITARD_1_17.value=this.w_DHRITARD
    endif
    if not(this.oPgFrm.Page1.oPag.oDHBUFTRA_1_18.value==this.w_DHBUFTRA)
      this.oPgFrm.Page1.oPag.oDHBUFTRA_1_18.value=this.w_DHBUFTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oDHNAKDEL_1_19.value==this.w_DHNAKDEL)
      this.oPgFrm.Page1.oPag.oDHNAKDEL_1_19.value=this.w_DHNAKDEL
    endif
    if not(this.oPgFrm.Page1.oPag.oDHESCDEL_1_20.value==this.w_DHESCDEL)
      this.oPgFrm.Page1.oPag.oDHESCDEL_1_20.value=this.w_DHESCDEL
    endif
    if not(this.oPgFrm.Page1.oPag.oDHNUMTER_1_36.RadioValue()==this.w_DHNUMTER)
      this.oPgFrm.Page1.oPag.oDHNUMTER_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDHFAMCAS_2_1.RadioValue()==this.w_DHFAMCAS)
      this.oPgFrm.Page2.oPag.oDHFAMCAS_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDHPAGEST_2_2.RadioValue()==this.w_DHPAGEST)
      this.oPgFrm.Page2.oPag.oDHPAGEST_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDHPOSPAT_2_3.value==this.w_DHPOSPAT)
      this.oPgFrm.Page2.oPag.oDHPOSPAT_2_3.value=this.w_DHPOSPAT
    endif
    if not(this.oPgFrm.Page2.oPag.oDHPATFIL_2_5.value==this.w_DHPATFIL)
      this.oPgFrm.Page2.oPag.oDHPATFIL_2_5.value=this.w_DHPATFIL
    endif
    if not(this.oPgFrm.Page2.oPag.oDHMATCAS_2_7.value==this.w_DHMATCAS)
      this.oPgFrm.Page2.oPag.oDHMATCAS_2_7.value=this.w_DHMATCAS
    endif
    if not(this.oPgFrm.Page2.oPag.oDHPSWPOS_2_8.value==this.w_DHPSWPOS)
      this.oPgFrm.Page2.oPag.oDHPSWPOS_2_8.value=this.w_DHPSWPOS
    endif
    if not(this.oPgFrm.Page2.oPag.oDHSTADES_2_9.RadioValue()==this.w_DHSTADES)
      this.oPgFrm.Page2.oPag.oDHSTADES_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDH__BAUD_2_10.RadioValue()==this.w_DH__BAUD)
      this.oPgFrm.Page2.oPag.oDH__BAUD_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDHNUMCAR_2_11.value==this.w_DHNUMCAR)
      this.oPgFrm.Page2.oPag.oDHNUMCAR_2_11.value=this.w_DHNUMCAR
    endif
    if not(this.oPgFrm.Page2.oPag.oDHNUMFIS_2_12.value==this.w_DHNUMFIS)
      this.oPgFrm.Page2.oPag.oDHNUMFIS_2_12.value=this.w_DHNUMFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oDHPASQTA_2_13.RadioValue()==this.w_DHPASQTA)
      this.oPgFrm.Page2.oPag.oDHPASQTA_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDHCARDAT_2_14.RadioValue()==this.w_DHCARDAT)
      this.oPgFrm.Page2.oPag.oDHCARDAT_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDHFLMBRG_2_15.RadioValue()==this.w_DHFLMBRG)
      this.oPgFrm.Page2.oPag.oDHFLMBRG_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDHLOTMAT_1_43.RadioValue()==this.w_DHLOTMAT)
      this.oPgFrm.Page1.oPag.oDHLOTMAT_1_43.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTETS_2_28.value==this.w_NOTETS)
      this.oPgFrm.Page2.oPag.oNOTETS_2_28.value=this.w_NOTETS
    endif
    cp_SetControlsValueExtFlds(this,'DIS_HARD')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DHNUMPOR))  and not(.w_DHTIPDIS='C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDHNUMPOR_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DHNUMPOR)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DHTIPDIS = this.w_DHTIPDIS
    this.o_TIPOPENNA = this.w_TIPOPENNA
    this.o_TIPOTEL = this.w_TIPOTEL
    this.o_DHPROGRA = this.w_DHPROGRA
    this.o_DHFAMCAS = this.w_DHFAMCAS
    return

enddefine

* --- Define pages as container
define class tgsar_apdPag1 as StdContainer
  Width  = 535
  height = 392
  stdWidth  = 535
  stdheight = 392
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDHCODICE_1_1 as StdField with uid="DMDPBTYLYO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DHCODICE", cQueryName = "DHCODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 67765627,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=131, Top=12, InputMask=replicate('X',5)

  add object oDHDESCRI_1_2 as StdField with uid="XGGPBLXWJI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DHDESCRI", cQueryName = "DHDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del dispositivo",;
    HelpContextID = 250615167,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=196, Top=12, InputMask=replicate('X',40)


  add object oDHTIPDIS_1_3 as StdCombo with uid="AWSMWWHZKE",rtseq=3,rtrep=.f.,left=131,top=41,width=139,height=21;
    , ToolTipText = "Tipo dispositivo: penna ottica, centralino";
    , HelpContextID = 3861111;
    , cFormVar="w_DHTIPDIS",RowSource=""+"Penna ottica / file,"+"Centralino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDHTIPDIS_1_3.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oDHTIPDIS_1_3.GetRadio()
    this.Parent.oContained.w_DHTIPDIS = this.RadioValue()
    return .t.
  endfunc

  func oDHTIPDIS_1_3.SetRadio()
    this.Parent.oContained.w_DHTIPDIS=trim(this.Parent.oContained.w_DHTIPDIS)
    this.value = ;
      iif(this.Parent.oContained.w_DHTIPDIS=='P',1,;
      iif(this.Parent.oContained.w_DHTIPDIS=='C',2,;
      0))
  endfunc

  func oDHTIPDIS_1_3.mHide()
    with this.Parent.oContained
      return (IsAhr())
    endwith
  endfunc


  add object oDHTIPDIS_1_4 as StdCombo with uid="KQAVNQGKWQ",rtseq=4,rtrep=.f.,left=131,top=41,width=139,height=21;
    , ToolTipText = "Tipo dispositivo: registratore di cassa, penna ottica, centralino";
    , HelpContextID = 3861111;
    , cFormVar="w_DHTIPDIS",RowSource=""+"Registratore di cassa,"+"Penna ottica / file,"+"Centralino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDHTIPDIS_1_4.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'P',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oDHTIPDIS_1_4.GetRadio()
    this.Parent.oContained.w_DHTIPDIS = this.RadioValue()
    return .t.
  endfunc

  func oDHTIPDIS_1_4.SetRadio()
    this.Parent.oContained.w_DHTIPDIS=trim(this.Parent.oContained.w_DHTIPDIS)
    this.value = ;
      iif(this.Parent.oContained.w_DHTIPDIS=='R',1,;
      iif(this.Parent.oContained.w_DHTIPDIS=='P',2,;
      iif(this.Parent.oContained.w_DHTIPDIS=='C',3,;
      0)))
  endfunc

  func oDHTIPDIS_1_4.mHide()
    with this.Parent.oContained
      return (!IsAhr())
    endwith
  endfunc


  add object oDHNUMPOR_1_5 as StdCombo with uid="GBOWRSHWTK",rtseq=5,rtrep=.f.,left=435,top=41,width=61,height=21;
    , ToolTipText = "Porta di connessione dispositivo";
    , HelpContextID = 73353848;
    , cFormVar="w_DHNUMPOR",RowSource=""+"COM 1,"+"COM 2,"+"COM 3,"+"COM 4,"+"COM 5,"+"COM 6,"+"COM 7,"+"COM 8", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oDHNUMPOR_1_5.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    iif(this.value =6,6,;
    iif(this.value =7,7,;
    iif(this.value =8,8,;
    0)))))))))
  endfunc
  func oDHNUMPOR_1_5.GetRadio()
    this.Parent.oContained.w_DHNUMPOR = this.RadioValue()
    return .t.
  endfunc

  func oDHNUMPOR_1_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DHNUMPOR==1,1,;
      iif(this.Parent.oContained.w_DHNUMPOR==2,2,;
      iif(this.Parent.oContained.w_DHNUMPOR==3,3,;
      iif(this.Parent.oContained.w_DHNUMPOR==4,4,;
      iif(this.Parent.oContained.w_DHNUMPOR==5,5,;
      iif(this.Parent.oContained.w_DHNUMPOR==6,6,;
      iif(this.Parent.oContained.w_DHNUMPOR==7,7,;
      iif(this.Parent.oContained.w_DHNUMPOR==8,8,;
      0))))))))
  endfunc

  func oDHNUMPOR_1_5.mHide()
    with this.Parent.oContained
      return (.w_DHTIPDIS='C')
    endwith
  endfunc


  add object oTIPOPENNA_1_6 as StdCombo with uid="IDWJDIQAXY",rtseq=6,rtrep=.f.,left=141,top=109,width=208,height=21;
    , ToolTipText = "Modello di penna ottica interfacciata";
    , HelpContextID = 255140972;
    , cFormVar="w_TIPOPENNA",RowSource=""+"General Code PDT 600 Z,"+"Formula 734 (1° modalità),"+"Formula 734 (2° modalità),"+"BHT-8000 DENSO  (1°modalità),"+"BHT-8000 DENSO (2°modalità),"+"File di testo,"+"Foglio Excel,"+"Datalogic (Rettifiche),"+"Datalogic (Documenti),"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPENNA_1_6.RadioValue()
    return(iif(this.value =1,'GC',;
    iif(this.value =2,'F1',;
    iif(this.value =3,'F2',;
    iif(this.value =4,'BH',;
    iif(this.value =5,'B1',;
    iif(this.value =6,'FT',;
    iif(this.value =7,'EX',;
    iif(this.value =8,'DL',;
    iif(this.value =9,'D1',;
    iif(this.value =10,'AL',;
    space(2))))))))))))
  endfunc
  func oTIPOPENNA_1_6.GetRadio()
    this.Parent.oContained.w_TIPOPENNA = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPENNA_1_6.SetRadio()
    this.Parent.oContained.w_TIPOPENNA=trim(this.Parent.oContained.w_TIPOPENNA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPENNA=='GC',1,;
      iif(this.Parent.oContained.w_TIPOPENNA=='F1',2,;
      iif(this.Parent.oContained.w_TIPOPENNA=='F2',3,;
      iif(this.Parent.oContained.w_TIPOPENNA=='BH',4,;
      iif(this.Parent.oContained.w_TIPOPENNA=='B1',5,;
      iif(this.Parent.oContained.w_TIPOPENNA=='FT',6,;
      iif(this.Parent.oContained.w_TIPOPENNA=='EX',7,;
      iif(this.Parent.oContained.w_TIPOPENNA=='DL',8,;
      iif(this.Parent.oContained.w_TIPOPENNA=='D1',9,;
      iif(this.Parent.oContained.w_TIPOPENNA=='AL',10,;
      0))))))))))
  endfunc

  func oTIPOPENNA_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS='P')
    endwith
   endif
  endfunc

  func oTIPOPENNA_1_6.mHide()
    with this.Parent.oContained
      return (.w_DHTIPDIS='C')
    endwith
  endfunc


  add object oTIPOTEL_1_7 as StdCombo with uid="TQRSWEGRQT",rtseq=7,rtrep=.f.,left=141,top=109,width=208,height=21;
    , ToolTipText = "Modello centralino";
    , HelpContextID = 250947786;
    , cFormVar="w_TIPOTEL",RowSource=""+"File di testo,"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOTEL_1_7.RadioValue()
    return(iif(this.value =1,'TC',;
    iif(this.value =2,'AL',;
    space(2))))
  endfunc
  func oTIPOTEL_1_7.GetRadio()
    this.Parent.oContained.w_TIPOTEL = this.RadioValue()
    return .t.
  endfunc

  func oTIPOTEL_1_7.SetRadio()
    this.Parent.oContained.w_TIPOTEL=trim(this.Parent.oContained.w_TIPOTEL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOTEL=='TC',1,;
      iif(this.Parent.oContained.w_TIPOTEL=='AL',2,;
      0))
  endfunc

  func oTIPOTEL_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS='C')
    endwith
   endif
  endfunc

  func oTIPOTEL_1_7.mHide()
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'C')
    endwith
  endfunc

  add object oDHPROGRA_1_8 as StdField with uid="FCBSRBFYAD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DHPROGRA", cQueryName = "DHPROGRA",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Programma di gestione",;
    HelpContextID = 45995383,;
   bGlobalFont=.t.,;
    Height=21, Width=110, Left=399, Top=109, InputMask=replicate('X',18)

  func oDHPROGRA_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_DHTIPDIS='P' AND .w_TIPOPENNA='AL') Or (.w_DHTIPDIS='C' AND .w_TIPOTEL='AL'))
    endwith
   endif
  endfunc

  add object oDHPROPAR_1_9 as StdField with uid="VNPCPSTENG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DHPROPAR", cQueryName = "DHPROPAR",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale parametro da passare al driver (es. file di testo da importare)",;
    HelpContextID = 196990344,;
   bGlobalFont=.t.,;
    Height=21, Width=368, Left=141, Top=137, InputMask=replicate('X',254)

  func oDHPROPAR_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'R')
    endwith
   endif
  endfunc

  add object oDHTRAPER_1_10 as StdMemo with uid="RAURLXBJSF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DHTRAPER", cQueryName = "DHTRAPER",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Tracciato personalizzato da utilizzare durante l'importazione mediante driver file di testo",;
    HelpContextID = 182326664,;
   bGlobalFont=.t.,;
    Height=162, Width=387, Left=141, Top=165

  func oDHTRAPER_1_10.mHide()
    with this.Parent.oContained
      return (.w_TIPOPENNA<>'FT' or .w_DHTIPDIS<>'P')
    endwith
  endfunc

  add object oDHELIFIL_1_11 as StdCheck with uid="SNSMTIGJIG",rtseq=11,rtrep=.f.,left=141, top=332, caption="Elimina file dopo importazione",;
    ToolTipText = "Se attivo cancella il file dei dati dopo l'importazione",;
    HelpContextID = 245947006,;
    cFormVar="w_DHELIFIL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDHELIFIL_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDHELIFIL_1_11.GetRadio()
    this.Parent.oContained.w_DHELIFIL = this.RadioValue()
    return .t.
  endfunc

  func oDHELIFIL_1_11.SetRadio()
    this.Parent.oContained.w_DHELIFIL=trim(this.Parent.oContained.w_DHELIFIL)
    this.value = ;
      iif(this.Parent.oContained.w_DHELIFIL=='S',1,;
      0)
  endfunc

  func oDHELIFIL_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPOPENNA<>'FT' or .w_DHTIPDIS<>'P')
    endwith
  endfunc

  add object oDH__BAUD_1_12 as StdRadio with uid="GRTKWBNLVO",rtseq=12,rtrep=.f.,left=141, top=167, width=370,height=23;
    , ToolTipText = "Baud";
    , cFormVar="w_DH__BAUD", ButtonCount=6, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDH__BAUD_1_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="1200"
      this.Buttons(1).HelpContextID = 201049466
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("1200","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="2400"
      this.Buttons(2).HelpContextID = 201049466
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("2400","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="4800"
      this.Buttons(3).HelpContextID = 201049466
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("4800","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.Buttons(4).Caption="9600"
      this.Buttons(4).HelpContextID = 201049466
      this.Buttons(4).Left=i_coord
      this.Buttons(4).Width=(TxtWidth("9600","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(4).Width
      this.Buttons(5).Caption="19200"
      this.Buttons(5).HelpContextID = 201049466
      this.Buttons(5).Left=i_coord
      this.Buttons(5).Width=(TxtWidth("19200","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(5).Width
      this.Buttons(6).Caption="38400"
      this.Buttons(6).HelpContextID = 201049466
      this.Buttons(6).Left=i_coord
      this.Buttons(6).Width=(TxtWidth("38400","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(6).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Baud")
      StdRadio::init()
    endproc

  func oDH__BAUD_1_12.RadioValue()
    return(iif(this.value =1,1200,;
    iif(this.value =2,2400,;
    iif(this.value =3,4800,;
    iif(this.value =4,9600,;
    iif(this.value =5,19200,;
    iif(this.value =6,38400,;
    0)))))))
  endfunc
  func oDH__BAUD_1_12.GetRadio()
    this.Parent.oContained.w_DH__BAUD = this.RadioValue()
    return .t.
  endfunc

  func oDH__BAUD_1_12.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DH__BAUD==1200,1,;
      iif(this.Parent.oContained.w_DH__BAUD==2400,2,;
      iif(this.Parent.oContained.w_DH__BAUD==4800,3,;
      iif(this.Parent.oContained.w_DH__BAUD==9600,4,;
      iif(this.Parent.oContained.w_DH__BAUD==19200,5,;
      iif(this.Parent.oContained.w_DH__BAUD==38400,6,;
      0))))))
  endfunc

  func oDH__BAUD_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'R')
    endwith
   endif
  endfunc

  func oDH__BAUD_1_12.mHide()
    with this.Parent.oContained
      return (.w_TIPOPENNA='FT' OR .w_TIPOPENNA='EX' Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oDHPARITA_1_13 as StdRadio with uid="PBRYONJIBE",rtseq=13,rtrep=.f.,left=141, top=196, width=295,height=23;
    , ToolTipText = "Parità";
    , cFormVar="w_DHPARITA", ButtonCount=5, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDHPARITA_1_13.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="No"
      this.Buttons(1).HelpContextID = 81581431
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("No","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Odd"
      this.Buttons(2).HelpContextID = 81581431
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Odd","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Even"
      this.Buttons(3).HelpContextID = 81581431
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Even","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.Buttons(4).Caption="Mark"
      this.Buttons(4).HelpContextID = 81581431
      this.Buttons(4).Left=i_coord
      this.Buttons(4).Width=(TxtWidth("Mark","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(4).Width
      this.Buttons(5).Caption="Space"
      this.Buttons(5).HelpContextID = 81581431
      this.Buttons(5).Left=i_coord
      this.Buttons(5).Width=(TxtWidth("Space","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(5).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Parità")
      StdRadio::init()
    endproc

  func oDHPARITA_1_13.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'1',;
    iif(this.value =3,'2',;
    iif(this.value =4,'3',;
    iif(this.value =5,'4',;
    space(1)))))))
  endfunc
  func oDHPARITA_1_13.GetRadio()
    this.Parent.oContained.w_DHPARITA = this.RadioValue()
    return .t.
  endfunc

  func oDHPARITA_1_13.SetRadio()
    this.Parent.oContained.w_DHPARITA=trim(this.Parent.oContained.w_DHPARITA)
    this.value = ;
      iif(this.Parent.oContained.w_DHPARITA=='0',1,;
      iif(this.Parent.oContained.w_DHPARITA=='1',2,;
      iif(this.Parent.oContained.w_DHPARITA=='2',3,;
      iif(this.Parent.oContained.w_DHPARITA=='3',4,;
      iif(this.Parent.oContained.w_DHPARITA=='4',5,;
      0)))))
  endfunc

  func oDHPARITA_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'R')
    endwith
   endif
  endfunc

  func oDHPARITA_1_13.mHide()
    with this.Parent.oContained
      return (.w_TIPOPENNA='FT' OR .w_TIPOPENNA='EX' Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oDHBITSTP_1_14 as StdRadio with uid="BEZIYAJGMB",rtseq=14,rtrep=.f.,left=141, top=225, width=78,height=23;
    , ToolTipText = "Bit stop";
    , cFormVar="w_DHBITSTP", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDHBITSTP_1_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="1"
      this.Buttons(1).HelpContextID = 251917702
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("1","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="2"
      this.Buttons(2).HelpContextID = 251917702
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("2","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Bit stop")
      StdRadio::init()
    endproc

  func oDHBITSTP_1_14.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    0)))
  endfunc
  func oDHBITSTP_1_14.GetRadio()
    this.Parent.oContained.w_DHBITSTP = this.RadioValue()
    return .t.
  endfunc

  func oDHBITSTP_1_14.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DHBITSTP==1,1,;
      iif(this.Parent.oContained.w_DHBITSTP==2,2,;
      0))
  endfunc

  func oDHBITSTP_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'R')
    endwith
   endif
  endfunc

  func oDHBITSTP_1_14.mHide()
    with this.Parent.oContained
      return (.w_TIPOPENNA='FT' OR .w_TIPOPENNA='EX' Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oDHBITDAT_1_15 as StdRadio with uid="LUYWUSWWVQ",rtseq=15,rtrep=.f.,left=141, top=254, width=78,height=23;
    , ToolTipText = "Bit dati";
    , cFormVar="w_DHBITDAT", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oDHBITDAT_1_15.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="7"
      this.Buttons(1).HelpContextID = 259466
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("7","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="8"
      this.Buttons(2).HelpContextID = 259466
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("8","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Bit dati")
      StdRadio::init()
    endproc

  func oDHBITDAT_1_15.RadioValue()
    return(iif(this.value =1,7,;
    iif(this.value =2,8,;
    0)))
  endfunc
  func oDHBITDAT_1_15.GetRadio()
    this.Parent.oContained.w_DHBITDAT = this.RadioValue()
    return .t.
  endfunc

  func oDHBITDAT_1_15.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DHBITDAT==7,1,;
      iif(this.Parent.oContained.w_DHBITDAT==8,2,;
      0))
  endfunc

  func oDHBITDAT_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'R')
    endwith
   endif
  endfunc

  func oDHBITDAT_1_15.mHide()
    with this.Parent.oContained
      return (.w_TIPOPENNA='FT' OR .w_TIPOPENNA='EX' Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oDHBUFRIC_1_16 as StdField with uid="QHGCISQKIU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DHBUFRIC", cQueryName = "DHBUFRIC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione Buffer di ricezione (1030..65535 bytes)",;
    HelpContextID = 47188615,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=141, Top=281, cSayPict='"999999"', cGetPict='"999999"'

  func oDHBUFRIC_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'R')
    endwith
   endif
  endfunc

  func oDHBUFRIC_1_16.mHide()
    with this.Parent.oContained
      return (not(.w_TIPOPENNA='F1' or  .w_TIPOPENNA='F2' or  .w_TIPOPENNA='AL') Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oDHRITARD_1_17 as StdField with uid="UBHBBYCQSJ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DHRITARD", cQueryName = "DHRITARD",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ritardo in (1/100 secondi)",;
    HelpContextID = 218428794,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=454, Top=281, cSayPict='"999"', cGetPict='"999"'

  func oDHRITARD_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'R')
    endwith
   endif
  endfunc

  func oDHRITARD_1_17.mHide()
    with this.Parent.oContained
      return (not(.w_TIPOPENNA='F1' or  .w_TIPOPENNA='F2' or  .w_TIPOPENNA='AL') Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oDHBUFTRA_1_18 as StdField with uid="TNZRJGADFX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DHBUFTRA", cQueryName = "DHBUFTRA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione Buffer di trasmissione (1030..65535 bytes)",;
    HelpContextID = 254801271,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=141, Top=307

  func oDHBUFTRA_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'R')
    endwith
   endif
  endfunc

  func oDHBUFTRA_1_18.mHide()
    with this.Parent.oContained
      return (not(.w_TIPOPENNA='F1' or  .w_TIPOPENNA='F2' or  .w_TIPOPENNA='AL') Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oDHNAKDEL_1_19 as StdField with uid="WAIRFAABMH",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DHNAKDEL", cQueryName = "DHNAKDEL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Nak delay",;
    HelpContextID = 258782594,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=141, Top=332, cSayPict='"9999999999"', cGetPict='"9999999999"'

  func oDHNAKDEL_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'R')
    endwith
   endif
  endfunc

  func oDHNAKDEL_1_19.mHide()
    with this.Parent.oContained
      return (not(.w_TIPOPENNA='GC' or .w_TIPOPENNA='AL') Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oDHESCDEL_1_20 as StdField with uid="QAXDTHZQWB",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DHESCDEL", cQueryName = "DHESCDEL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Esc delay",;
    HelpContextID = 251536770,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=274, Top=332, cSayPict='"9999999999"', cGetPict='"9999999999"'

  func oDHESCDEL_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'R')
    endwith
   endif
  endfunc

  func oDHESCDEL_1_20.mHide()
    with this.Parent.oContained
      return (not(.w_TIPOPENNA='GC' or .w_TIPOPENNA='AL') Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc


  add object oDHNUMTER_1_36 as StdCombo with uid="JTAIZSHPOC",rtseq=21,rtrep=.f.,left=454,top=332,width=69,height=21;
    , ToolTipText = "Terminale collegato";
    , HelpContextID = 262190472;
    , cFormVar="w_DHNUMTER",RowSource=""+"A,"+"B,"+"C,"+"D", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDHNUMTER_1_36.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    space(1))))))
  endfunc
  func oDHNUMTER_1_36.GetRadio()
    this.Parent.oContained.w_DHNUMTER = this.RadioValue()
    return .t.
  endfunc

  func oDHNUMTER_1_36.SetRadio()
    this.Parent.oContained.w_DHNUMTER=trim(this.Parent.oContained.w_DHNUMTER)
    this.value = ;
      iif(this.Parent.oContained.w_DHNUMTER=='A',1,;
      iif(this.Parent.oContained.w_DHNUMTER=='B',2,;
      iif(this.Parent.oContained.w_DHNUMTER=='C',3,;
      iif(this.Parent.oContained.w_DHNUMTER=='D',4,;
      0))))
  endfunc

  func oDHNUMTER_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'R')
    endwith
   endif
  endfunc

  func oDHNUMTER_1_36.mHide()
    with this.Parent.oContained
      return (not(.w_TIPOPENNA='GC' or .w_TIPOPENNA='AL') Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc


  add object oObj_1_40 as cp_runprogram with uid="BLXTLIRTNC",left=3, top=437, width=178,height=19,;
    caption='GSUT_BLD(E)',;
   bGlobalFont=.t.,;
    prg="GSUT_BLD('E',w_DHCODICE,'     ')",;
    cEvent = "Delete start",;
    nPag=1;
    , ToolTipText = "Chiude la maschera e carica il transitorio della gestione sottostante";
    , HelpContextID = 20772822


  add object oDHLOTMAT_1_43 as StdCombo with uid="QFTBIORRIA",rtseq=36,rtrep=.f.,left=412,top=362,width=111,height=21;
    , ToolTipText = "Crea lotto/matricola durante caricamento rapido";
    , HelpContextID = 151688586;
    , cFormVar="w_DHLOTMAT",RowSource=""+"Nessuno,"+"Lotto,"+"Matricola,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDHLOTMAT_1_43.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'L',;
    iif(this.value =3,'M',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oDHLOTMAT_1_43.GetRadio()
    this.Parent.oContained.w_DHLOTMAT = this.RadioValue()
    return .t.
  endfunc

  func oDHLOTMAT_1_43.SetRadio()
    this.Parent.oContained.w_DHLOTMAT=trim(this.Parent.oContained.w_DHLOTMAT)
    this.value = ;
      iif(this.Parent.oContained.w_DHLOTMAT=='N',1,;
      iif(this.Parent.oContained.w_DHLOTMAT=='L',2,;
      iif(this.Parent.oContained.w_DHLOTMAT=='M',3,;
      iif(this.Parent.oContained.w_DHLOTMAT=='E',4,;
      0))))
  endfunc

  func oDHLOTMAT_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'R')
    endwith
   endif
  endfunc

  func oDHLOTMAT_1_43.mHide()
    with this.Parent.oContained
      return (.w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="MSSZTUZZZB",Visible=.t., Left=5, Top=12,;
    Alignment=1, Width=123, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="HUXKZAVHEI",Visible=.t., Left=15, Top=111,;
    Alignment=1, Width=123, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="ULHIMPQXZZ",Visible=.t., Left=15, Top=167,;
    Alignment=1, Width=123, Height=18,;
    Caption="Baud:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_TIPOPENNA='FT' OR .w_TIPOPENNA='EX' Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="JQLNIGLMCV",Visible=.t., Left=15, Top=196,;
    Alignment=1, Width=123, Height=18,;
    Caption="Parità:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_TIPOPENNA='FT' OR .w_TIPOPENNA='EX' Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="HEANDCPVKK",Visible=.t., Left=15, Top=225,;
    Alignment=1, Width=123, Height=18,;
    Caption="Bit stop:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (.w_TIPOPENNA='FT' OR .w_TIPOPENNA='EX' Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="BYSBISWUOO",Visible=.t., Left=15, Top=254,;
    Alignment=1, Width=123, Height=18,;
    Caption="Bit dati:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (.w_TIPOPENNA='FT' OR .w_TIPOPENNA='EX' Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="MDLAPBSYIS",Visible=.t., Left=5, Top=41,;
    Alignment=1, Width=123, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="ZZBAFVJBJQ",Visible=.t., Left=378, Top=281,;
    Alignment=1, Width=74, Height=18,;
    Caption="Ritardo:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (not(.w_TIPOPENNA='F1' or  .w_TIPOPENNA='F2' or  .w_TIPOPENNA='AL') Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="HDHNDAYITF",Visible=.t., Left=15, Top=281,;
    Alignment=1, Width=123, Height=18,;
    Caption="Buffer ricezione:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (not(.w_TIPOPENNA='F1' or  .w_TIPOPENNA='F2' or  .w_TIPOPENNA='AL') Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="MQYKYNIHNZ",Visible=.t., Left=15, Top=307,;
    Alignment=1, Width=123, Height=18,;
    Caption="Buffer trasmissione:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (not(.w_TIPOPENNA='F1' or  .w_TIPOPENNA='F2' or  .w_TIPOPENNA='AL') Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="JNGSRUFHNX",Visible=.t., Left=15, Top=137,;
    Alignment=1, Width=123, Height=18,;
    Caption="Parametro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="BAQBXQTGLQ",Visible=.t., Left=338, Top=41,;
    Alignment=1, Width=94, Height=18,;
    Caption="Porta COM:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="ROTCDTJNHD",Visible=.t., Left=15, Top=332,;
    Alignment=1, Width=123, Height=18,;
    Caption="Nak delay:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (not(.w_TIPOPENNA='GC' or .w_TIPOPENNA='AL') Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="DDAGLYFNDM",Visible=.t., Left=210, Top=332,;
    Alignment=1, Width=62, Height=18,;
    Caption="Esc delay:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (not(.w_TIPOPENNA='GC' or .w_TIPOPENNA='AL') Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="IRYPPEYUPE",Visible=.t., Left=341, Top=332,;
    Alignment=1, Width=111, Height=18,;
    Caption="Terminale:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (not(.w_TIPOPENNA='GC' or .w_TIPOPENNA='AL') Or .w_TIPOPENNA='DL' Or .w_TIPOPENNA='D1' Or .w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="GPLNLDLSYI",Visible=.t., Left=22, Top=78,;
    Alignment=0, Width=128, Height=18,;
    Caption="Penne ottiche"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="ANHZGOOSVB",Visible=.t., Left=356, Top=111,;
    Alignment=1, Width=42, Height=18,;
    Caption="Driver:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="DEIFEKGZMF",Visible=.t., Left=19, Top=467,;
    Alignment=0, Width=702, Height=18,;
    Caption="Se viene variata la calculate di tipopenna occorre modificare anche l'area manuale load record end che contiene"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="ZMRZBYBSSA",Visible=.t., Left=13, Top=488,;
    Alignment=0, Width=634, Height=18,;
    Caption="la stessa condizione di calcolo altrimenti nel caricamento del record la combo si setta su altro)"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="SKEDZYZIUU",Visible=.t., Left=260, Top=361,;
    Alignment=1, Width=149, Height=17,;
    Caption="Crea lotto/matricola:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (.w_DHTIPDIS='C')
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="LPLDOQVBPI",Visible=.t., Left=3, Top=167,;
    Alignment=1, Width=135, Height=18,;
    Caption="Tracciato personalizzato:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (.w_TIPOPENNA<>'FT' or .w_DHTIPDIS<>'P')
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="KKMWLUSBKC",Visible=.t., Left=22, Top=78,;
    Alignment=0, Width=128, Height=18,;
    Caption="Centralino"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_DHTIPDIS<>'C')
    endwith
  endfunc

  add object oBox_1_37 as StdBox with uid="MEKFCXCMED",left=16, top=98, width=507,height=3
enddefine
define class tgsar_apdPag2 as StdContainer
  Width  = 535
  height = 392
  stdWidth  = 535
  stdheight = 392
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oDHFAMCAS_2_1 as StdCombo with uid="DRQDZAHEGH",rtseq=22,rtrep=.f.,left=128,top=16,width=177,height=21;
    , ToolTipText = "Famiglia registratore di cassa";
    , HelpContextID = 244069769;
    , cFormVar="w_DHFAMCAS",RowSource=""+"Compatibile Sarema,"+"Compatibile Sweda/Micropos,"+"Nucleo RCH,"+"Sarema protocollo controllato,"+"Sweda con Sarema language,"+"RCH G2000,"+"Olivetti,"+"Olivetti mod. gestionale,"+"Vandoni,"+"Sweda master,"+"RCH mizar,"+"RCH G3000,"+"Sweda sw204,"+"Sweda S5000,"+"Ditron,"+"NCR 2215,"+"Epson,"+"Custom Engineering,"+"Fasy2WincorN2,"+"FasyMistral WincorButterfly,"+"Coris", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDHFAMCAS_2_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    iif(this.value =3,'N',;
    iif(this.value =4,'P',;
    iif(this.value =5,'Z',;
    iif(this.value =6,'R',;
    iif(this.value =7,'O',;
    iif(this.value =8,'Q',;
    iif(this.value =9,'V',;
    iif(this.value =10,'W',;
    iif(this.value =11,'A',;
    iif(this.value =12,'B',;
    iif(this.value =13,'C',;
    iif(this.value =14,'E',;
    iif(this.value =15,'D',;
    iif(this.value =16,'F',;
    iif(this.value =17,'G',;
    iif(this.value =18,'H',;
    iif(this.value =19,'I',;
    iif(this.value =20,'L',;
    iif(this.value =21,'T',;
    space(1)))))))))))))))))))))))
  endfunc
  func oDHFAMCAS_2_1.GetRadio()
    this.Parent.oContained.w_DHFAMCAS = this.RadioValue()
    return .t.
  endfunc

  func oDHFAMCAS_2_1.SetRadio()
    this.Parent.oContained.w_DHFAMCAS=trim(this.Parent.oContained.w_DHFAMCAS)
    this.value = ;
      iif(this.Parent.oContained.w_DHFAMCAS=='S',1,;
      iif(this.Parent.oContained.w_DHFAMCAS=='M',2,;
      iif(this.Parent.oContained.w_DHFAMCAS=='N',3,;
      iif(this.Parent.oContained.w_DHFAMCAS=='P',4,;
      iif(this.Parent.oContained.w_DHFAMCAS=='Z',5,;
      iif(this.Parent.oContained.w_DHFAMCAS=='R',6,;
      iif(this.Parent.oContained.w_DHFAMCAS=='O',7,;
      iif(this.Parent.oContained.w_DHFAMCAS=='Q',8,;
      iif(this.Parent.oContained.w_DHFAMCAS=='V',9,;
      iif(this.Parent.oContained.w_DHFAMCAS=='W',10,;
      iif(this.Parent.oContained.w_DHFAMCAS=='A',11,;
      iif(this.Parent.oContained.w_DHFAMCAS=='B',12,;
      iif(this.Parent.oContained.w_DHFAMCAS=='C',13,;
      iif(this.Parent.oContained.w_DHFAMCAS=='E',14,;
      iif(this.Parent.oContained.w_DHFAMCAS=='D',15,;
      iif(this.Parent.oContained.w_DHFAMCAS=='F',16,;
      iif(this.Parent.oContained.w_DHFAMCAS=='G',17,;
      iif(this.Parent.oContained.w_DHFAMCAS=='H',18,;
      iif(this.Parent.oContained.w_DHFAMCAS=='I',19,;
      iif(this.Parent.oContained.w_DHFAMCAS=='L',20,;
      iif(this.Parent.oContained.w_DHFAMCAS=='T',21,;
      0)))))))))))))))))))))
  endfunc

  add object oDHPAGEST_2_2 as StdCheck with uid="FPQSKZSTHC",rtseq=23,rtrep=.f.,left=373, top=17, caption="Pagamenti estesi",;
    ToolTipText = "Se attivo il driver permette il passaggio alla cassa del dettaglio pagamenti",;
    HelpContextID = 2938250,;
    cFormVar="w_DHPAGEST", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDHPAGEST_2_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDHPAGEST_2_2.GetRadio()
    this.Parent.oContained.w_DHPAGEST = this.RadioValue()
    return .t.
  endfunc

  func oDHPAGEST_2_2.SetRadio()
    this.Parent.oContained.w_DHPAGEST=trim(this.Parent.oContained.w_DHPAGEST)
    this.value = ;
      iif(this.Parent.oContained.w_DHPAGEST=='S',1,;
      0)
  endfunc

  func oDHPAGEST_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHFAMCAS = 'H')
    endwith
   endif
  endfunc

  func oDHPAGEST_2_2.mHide()
    with this.Parent.oContained
      return (.w_DHFAMCAS <> 'H')
    endwith
  endfunc

  add object oDHPOSPAT_2_3 as StdField with uid="ALTKSECMRG",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DHPOSPAT", cQueryName = "DHPOSPAT",;
    bObbl = .f. , nPag = 2, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Path di installazione emettitore scontrino. Non specificare cartella con il nome della famiglia",;
    HelpContextID = 200988042,;
   bGlobalFont=.t.,;
    Height=21, Width=371, Left=128, Top=45, InputMask=replicate('X',250)

  func oDHPOSPAT_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS='R')
    endwith
   endif
  endfunc


  add object oBtn_2_4 as StdButton with uid="HWOSOTZHXT",left=508, top=46, width=23,height=21,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per selezionare la path di installazione di adhoc emettitore scontrino";
    , HelpContextID = 159802922;
  , bGlobalFont=.t.

    proc oBtn_2_4.Click()
      with this.Parent.oContained
        .w_DHPOSPAT=left(cp_getdir(IIF(g_TERMINALSS,'\\TSCLIENT\C\',sys(5)+sys(2003)),"Cartella di installazione Emettitore Scontrino",'Seleziona',64),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_DHTIPDIS='R')
      endwith
    endif
  endfunc

  func oBtn_2_4.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_Application="ad hoc ENTERPRISE" or g_GPOS<>'S')
     endwith
    endif
  endfunc

  add object oDHPATFIL_2_5 as StdField with uid="OYNERDCGVE",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DHPATFIL", cQueryName = "DHPATFIL",;
    bObbl = .f. , nPag = 2, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Path di creazione files scontrino. In terminal server vengono creati sul client",;
    HelpContextID = 235088510,;
   bGlobalFont=.t.,;
    Height=21, Width=371, Left=128, Top=75, InputMask=replicate('X',250)

  func oDHPATFIL_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS='R')
    endwith
   endif
  endfunc


  add object oBtn_2_6 as StdButton with uid="MVGMSQEQBN",left=508, top=76, width=23,height=21,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per selezionare il path di creazione file di ad hoc emettitore scontrino";
    , HelpContextID = 159802922;
  , bGlobalFont=.t.

    proc oBtn_2_6.Click()
      with this.Parent.oContained
        .w_DHPATFIL=left(cp_getdir(IIF(g_TERMINALSS,'\\TSCLIENT\C\',sys(5)+sys(2003)),"Cartella di creazione Files",'Seleziona',64),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_DHTIPDIS='R')
      endwith
    endif
  endfunc

  func oBtn_2_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_Application="ad hoc ENTERPRISE" or g_GPOS<>'S')
     endwith
    endif
  endfunc

  add object oDHMATCAS_2_7 as StdField with uid="FZAZWUOKLP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DHMATCAS", cQueryName = "DHMATCAS",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Matricola cassa",;
    HelpContextID = 251438473,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=128, Top=109, InputMask=replicate('X',20)

  func oDHMATCAS_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS='R')
    endwith
   endif
  endfunc

  add object oDHPSWPOS_2_8 as StdField with uid="BUYEPNUJXW",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DHPSWPOS", cQueryName = "DHPSWPOS",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Password registratore di cassa.",;
    HelpContextID = 62990967,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=128, Top=138, InputMask=replicate('X',10)

  func oDHPSWPOS_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS='R')
    endwith
   endif
  endfunc


  add object oDHSTADES_2_9 as StdCombo with uid="BIRDBOYMOP",rtseq=28,rtrep=.f.,left=128,top=170,width=177,height=21;
    , ToolTipText = "Contenuto riga fiscale scontrino";
    , HelpContextID = 249562505;
    , cFormVar="w_DHSTADES",RowSource=""+"Descrizione,"+"Codice,"+"Codice+descrizione", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDHSTADES_2_9.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'C',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oDHSTADES_2_9.GetRadio()
    this.Parent.oContained.w_DHSTADES = this.RadioValue()
    return .t.
  endfunc

  func oDHSTADES_2_9.SetRadio()
    this.Parent.oContained.w_DHSTADES=trim(this.Parent.oContained.w_DHSTADES)
    this.value = ;
      iif(this.Parent.oContained.w_DHSTADES=='D',1,;
      iif(this.Parent.oContained.w_DHSTADES=='C',2,;
      iif(this.Parent.oContained.w_DHSTADES=='T',3,;
      0)))
  endfunc

  func oDHSTADES_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS='R')
    endwith
   endif
  endfunc

  add object oDH__BAUD_2_10 as StdRadio with uid="RODATSNWON",rtseq=29,rtrep=.f.,left=128, top=199, width=370,height=23;
    , ToolTipText = "Baud";
    , cFormVar="w_DH__BAUD", ButtonCount=6, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oDH__BAUD_2_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="1200"
      this.Buttons(1).HelpContextID = 201049466
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("1200","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="2400"
      this.Buttons(2).HelpContextID = 201049466
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("2400","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="4800"
      this.Buttons(3).HelpContextID = 201049466
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("4800","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.Buttons(4).Caption="9600"
      this.Buttons(4).HelpContextID = 201049466
      this.Buttons(4).Left=i_coord
      this.Buttons(4).Width=(TxtWidth("9600","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(4).Width
      this.Buttons(5).Caption="19200"
      this.Buttons(5).HelpContextID = 201049466
      this.Buttons(5).Left=i_coord
      this.Buttons(5).Width=(TxtWidth("19200","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(5).Width
      this.Buttons(6).Caption="38400"
      this.Buttons(6).HelpContextID = 201049466
      this.Buttons(6).Left=i_coord
      this.Buttons(6).Width=(TxtWidth("38400","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(6).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Baud")
      StdRadio::init()
    endproc

  func oDH__BAUD_2_10.RadioValue()
    return(iif(this.value =1,1200,;
    iif(this.value =2,2400,;
    iif(this.value =3,4800,;
    iif(this.value =4,9600,;
    iif(this.value =5,19200,;
    iif(this.value =6,38400,;
    0)))))))
  endfunc
  func oDH__BAUD_2_10.GetRadio()
    this.Parent.oContained.w_DH__BAUD = this.RadioValue()
    return .t.
  endfunc

  func oDH__BAUD_2_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DH__BAUD==1200,1,;
      iif(this.Parent.oContained.w_DH__BAUD==2400,2,;
      iif(this.Parent.oContained.w_DH__BAUD==4800,3,;
      iif(this.Parent.oContained.w_DH__BAUD==9600,4,;
      iif(this.Parent.oContained.w_DH__BAUD==19200,5,;
      iif(this.Parent.oContained.w_DH__BAUD==38400,6,;
      0))))))
  endfunc

  func oDH__BAUD_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS='R')
    endwith
   endif
  endfunc

  add object oDHNUMCAR_2_11 as StdField with uid="IFNXZKWVKN",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DHNUMCAR", cQueryName = "DHNUMCAR",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero caratteri su righe descrittive",;
    HelpContextID = 245413256,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=504, Top=109

  func oDHNUMCAR_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS='R')
    endwith
   endif
  endfunc

  add object oDHNUMFIS_2_12 as StdField with uid="DQNJIICEHG",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DHNUMFIS", cQueryName = "DHNUMFIS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero caratteri codice/descrizione nelle righe fiscali",;
    HelpContextID = 241126007,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=504, Top=138

  func oDHNUMFIS_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS='R')
    endwith
   endif
  endfunc

  add object oDHPASQTA_2_13 as StdCheck with uid="TGCELMPZWI",rtseq=32,rtrep=.f.,left=373, top=170, caption="Invio quantità vendute",;
    ToolTipText = "Se attivo viene passata la quantità alla riga fiscale dello scontrino",;
    HelpContextID = 216847735,;
    cFormVar="w_DHPASQTA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDHPASQTA_2_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oDHPASQTA_2_13.GetRadio()
    this.Parent.oContained.w_DHPASQTA = this.RadioValue()
    return .t.
  endfunc

  func oDHPASQTA_2_13.SetRadio()
    this.Parent.oContained.w_DHPASQTA=trim(this.Parent.oContained.w_DHPASQTA)
    this.value = ;
      iif(this.Parent.oContained.w_DHPASQTA=='S',1,;
      0)
  endfunc

  func oDHPASQTA_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHFAMCAS = 'W' And .w_DHTIPDIS='R')
    endwith
   endif
  endfunc

  func oDHPASQTA_2_13.mHide()
    with this.Parent.oContained
      return (.w_DHFAMCAS <> 'W')
    endwith
  endfunc


  add object oDHCARDAT_2_14 as StdCombo with uid="BHPVNRALEL",value=1,rtseq=33,rtrep=.f.,left=394,top=228,width=137,height=21;
    , ToolTipText = "Modalità di ritorno dati dal registratore";
    , HelpContextID = 266077578;
    , cFormVar="w_DHCARDAT",RowSource=""+"Ritorna dati,"+"Non ritorna dati,"+"Ritorno con conferma", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDHCARDAT_2_14.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'N',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oDHCARDAT_2_14.GetRadio()
    this.Parent.oContained.w_DHCARDAT = this.RadioValue()
    return .t.
  endfunc

  func oDHCARDAT_2_14.SetRadio()
    this.Parent.oContained.w_DHCARDAT=trim(this.Parent.oContained.w_DHCARDAT)
    this.value = ;
      iif(this.Parent.oContained.w_DHCARDAT=='',1,;
      iif(this.Parent.oContained.w_DHCARDAT=='N',2,;
      iif(this.Parent.oContained.w_DHCARDAT=='C',3,;
      0)))
  endfunc

  func oDHCARDAT_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS='R')
    endwith
   endif
  endfunc


  add object oDHFLMBRG_2_15 as StdCombo with uid="XGOMKTZYFM",rtseq=34,rtrep=.f.,left=394,top=263,width=137,height=21;
    , ToolTipText = "Tcp/ip: il comando viene inviato su una porta del client. file: Zucchetti bridge attende la creazione del file di comando nella cartella indicata";
    , HelpContextID = 228013437;
    , cFormVar="w_DHFLMBRG",RowSource=""+"TCP/IP,"+"File", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDHFLMBRG_2_15.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oDHFLMBRG_2_15.GetRadio()
    this.Parent.oContained.w_DHFLMBRG = this.RadioValue()
    return .t.
  endfunc

  func oDHFLMBRG_2_15.SetRadio()
    this.Parent.oContained.w_DHFLMBRG=trim(this.Parent.oContained.w_DHFLMBRG)
    this.value = ;
      iif(this.Parent.oContained.w_DHFLMBRG=='P',1,;
      iif(this.Parent.oContained.w_DHFLMBRG=='T',2,;
      0))
  endfunc

  func oDHFLMBRG_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DHTIPDIS='R')
    endwith
   endif
  endfunc

  func oDHFLMBRG_2_15.mHide()
    with this.Parent.oContained
      return (Not g_TERMINALSS)
    endwith
  endfunc


  add object oBtn_2_22 as StdButton with uid="AISKWWPJMW",left=112, top=221, width=48,height=45,;
    CpPicture="BMP\PREVIEW.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per stampare uno scontrino di prova";
    , HelpContextID = 151903434;
    , tabstop=.f.,caption='\<Test';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_22.Click()
      do GSPS_KES with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_DHTIPDIS='R' And g_GPOS='S')
      endwith
    endif
  endfunc

  func oBtn_2_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_TERMINALSS Or g_GPOS<>'S')
     endwith
    endif
  endfunc

  add object oNOTETS_2_28 as StdMemo with uid="LXBUNSCZUR",rtseq=37,rtrep=.f.,;
    cFormVar = "w_NOTETS", cQueryName = "NOTETS",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note tecniche per l'utilizzo di adhoc emettitore scontrino in terminal server",;
    HelpContextID = 251731158,;
   bGlobalFont=.t.,;
    Height=96, Width=502, Left=18, Top=291, readonly =.t.

  func oNOTETS_2_28.mHide()
    with this.Parent.oContained
      return (Not g_TERMINALSS)
    endwith
  endfunc

  add object oStr_2_16 as StdString with uid="ZDHSBHJSOH",Visible=.t., Left=1, Top=46,;
    Alignment=1, Width=124, Height=18,;
    Caption="Path em. scont.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="DVAMVFDTQQ",Visible=.t., Left=1, Top=110,;
    Alignment=1, Width=124, Height=18,;
    Caption="Matricola cassa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="DLLAFHDVLO",Visible=.t., Left=1, Top=139,;
    Alignment=1, Width=124, Height=18,;
    Caption="Password cassa:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="UMDMWDUMCX",Visible=.t., Left=1, Top=17,;
    Alignment=1, Width=124, Height=18,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="OEYOVNMBPG",Visible=.t., Left=1, Top=170,;
    Alignment=1, Width=124, Height=18,;
    Caption="Riga scontrino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="DUMPWNKWIS",Visible=.t., Left=302, Top=109,;
    Alignment=1, Width=200, Height=18,;
    Caption="Caratteri righe descrittive:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="NGOMXUSREO",Visible=.t., Left=279, Top=138,;
    Alignment=1, Width=223, Height=18,;
    Caption="Caratteri codice/descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="WVEGNZZFUU",Visible=.t., Left=1, Top=76,;
    Alignment=1, Width=124, Height=18,;
    Caption="Creazione files:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="CXCACVFUAG",Visible=.t., Left=256, Top=228,;
    Alignment=1, Width=134, Height=18,;
    Caption="Ritorno dati:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="ABRAMCYKVT",Visible=.t., Left=1, Top=199,;
    Alignment=1, Width=124, Height=18,;
    Caption="Baud:"  ;
  , bGlobalFont=.t.

  func oStr_2_26.mHide()
    with this.Parent.oContained
      return (.w_TIPOPENNA='FT' OR .w_TIPOPENNA='EX')
    endwith
  endfunc

  add object oStr_2_29 as StdString with uid="WGIXCZGCLT",Visible=.t., Left=37, Top=263,;
    Alignment=1, Width=351, Height=18,;
    Caption="Modalità di utilizzo di Zucchetti bridge:"  ;
  , bGlobalFont=.t.

  func oStr_2_29.mHide()
    with this.Parent.oContained
      return (Not g_TERMINALSS)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_apd','DIS_HARD','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DHCODICE=DIS_HARD.DHCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_apd
Function CalNoteTs()
   Local cRet, oMessLog
   cRet=""
   oMessLog = createobject("Ah_Message")
   oMessLog.AddMsgPartNL("In terminal server la definizione delle cartelle 'path. em. scont.' e 'creazione files' devono indicare una cartella sul client connesso.")
   oMessLog.AddMsgPartNL("Nel caso di condivisione delle cartelle su Windows 2003 server, il client connesso viene visto come '\\tsclient\' e le unità di rete come cartella con stessa lettera della unità.")
   oMessLog.AddMsgPartNL("Es.: la cartella 'C: \programmi\ad hoc emettitore scontrino' verrà vista nella sessione terminal come '\\TSCLIENT\C\programmi\ad hoc emettitore scontrino'.")
   cRet=oMessLog.ComposeMessage()
   Return(cRet)
Endfunc
* --- Fine Area Manuale
