* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1kam                                                        *
*              Account email                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-03-16                                                      *
* Last revis.: 2016-05-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut1kam",oParentObject))

* --- Class definition
define class tgsut1kam as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 617
  Height = 258
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-05"
  HelpContextID=132693143
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut1kam"
  cComment = "Account email"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AMSERIAL = space(5)
  w_AM_EMAIL = space(254)
  w_AM_LOGIN = space(254)
  w_AMPASSWD = space(254)
  w_AM_INVIO = space(254)
  w_UTCODICE = 0
  w_UMFLGPEC = space(1)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut1kamPag1","gsut1kam",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AMSERIAL=space(5)
      .w_AM_EMAIL=space(254)
      .w_AM_LOGIN=space(254)
      .w_AMPASSWD=space(254)
      .w_AM_INVIO=space(254)
      .w_UTCODICE=0
      .w_UMFLGPEC=space(1)
      .w_AMSERIAL=oParentObject.w_AMSERIAL
      .w_AM_EMAIL=oParentObject.w_AM_EMAIL
      .w_AM_LOGIN=oParentObject.w_AM_LOGIN
      .w_AMPASSWD=oParentObject.w_AMPASSWD
      .w_AM_INVIO=oParentObject.w_AM_INVIO
      .w_UTCODICE=oParentObject.w_UTCODICE
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .w_AMSERIAL = .w_ZOOM.GetVar('AMSERIAL')
        .w_AM_EMAIL = .w_ZOOM.GetVar('AM_EMAIL')
        .w_AM_LOGIN = .w_ZOOM.GetVar('AM_LOGIN')
        .w_AMPASSWD = .w_ZOOM.GetVar('AMPASSWD')
        .w_AM_INVIO = .w_ZOOM.GetVar('AM_INVIO')
          .DoRTCalc(6,6,.f.)
        .w_UMFLGPEC = oParentObject.oParentObject.w_UMFLGPEC
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_AMSERIAL=.w_AMSERIAL
      .oParentObject.w_AM_EMAIL=.w_AM_EMAIL
      .oParentObject.w_AM_LOGIN=.w_AM_LOGIN
      .oParentObject.w_AMPASSWD=.w_AMPASSWD
      .oParentObject.w_AM_INVIO=.w_AM_INVIO
      .oParentObject.w_UTCODICE=.w_UTCODICE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
            .w_AMSERIAL = .w_ZOOM.GetVar('AMSERIAL')
            .w_AM_EMAIL = .w_ZOOM.GetVar('AM_EMAIL')
            .w_AM_LOGIN = .w_ZOOM.GetVar('AM_LOGIN')
            .w_AMPASSWD = .w_ZOOM.GetVar('AMPASSWD')
            .w_AM_INVIO = .w_ZOOM.GetVar('AM_INVIO')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsut1kam
    If Lower(m.cEvent) = 'w_zoom selected'
        This.NotifyEvent('Apri_Account')
    EndIf
    
    If m.cEvent = 'Apri_Account'
        OpenGest('A', 'GSUT_AAM', 'AMSERIAL', this.w_AMSERIAL)
    EndIf
    
    If m.cEvent = 'Nuovo_Account'
        do GSUT_BMU with oParentObject.oParentObject, "N"
        This.ecpQuit()
    EndIf
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut1kamPag1 as StdContainer
  Width  = 613
  height = 258
  stdWidth  = 613
  stdheight = 258
  resizeXpos=311
  resizeYpos=175
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOM as cp_zoombox with uid="WNNGBEZXZG",left=-2, top=5, width=617,height=202,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomOnZoom="GSZM_BCC",bRetriveAllRows=.f.,bQueryOnLoad=.t.,bReadOnly=.t.,cMenuFile="",cZoomFile="GSUT1KAM",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.t.,cTable="ACC_MAIL",bNoZoomGridShape=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 226180122


  add object oBtn_1_2 as StdButton with uid="ONORYGBAFF",left=559, top=210, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 140010566;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_3 as StdButton with uid="GRULAJDXPG",left=507, top=210, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare l'account mail";
    , HelpContextID = 118166108;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_4 as StdButton with uid="OJXTCBNFQR",left=55, top=210, width=48,height=45,;
    CpPicture="exe\bmp\apertura.ico", caption="", nPag=1;
    , ToolTipText = "Premere per aprire l'account selezionato";
    , HelpContextID = 140071174;
    , caption='\<Apri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      this.parent.oContained.NotifyEvent("Apri_Account")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="WXNUFUSVAW",left=4, top=210, width=48,height=45,;
    CpPicture="BMP\LOG_PWD.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inserire un nuovo account";
    , HelpContextID = 257304278;
    , caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      this.parent.oContained.NotifyEvent("Nuovo_Account")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut1kam','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
