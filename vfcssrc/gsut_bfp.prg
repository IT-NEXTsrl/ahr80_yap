* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bfp                                                        *
*              Penna ottica formula 734                                        *
*                                                                              *
*      Author: Emiliano Orrico                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_224]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-06-05                                                      *
* Last revis.: 2003-06-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bfp",oParentObject,m.pCursore,m.pTXTFile,m.pObjMsg,m.p_ArrayFieldName,m.p_ArrayFieldPosition)
return(i_retval)

define class tgsut_bfp as StdBatch
  * --- Local variables
  w_BITDAT = 0
  w_BUFRIC = 0
  w_TL = 0
  w_TIME = 0
  w_FILENAME = space(254)
  w_FILENAME1 = space(254)
  w_TIMEIN = 0
  w_TIMEOUT = .f.
  w_PRGGES = space(8)
  w_RITARD = 0
  w_PARITA = 0
  w_BUFTRA = 0
  w_BAURAT = 0
  w_BISTOP = 0
  w_PORTA = 0
  w_ETB = 0
  w_PARITY = space(1)
  w_LETTO = .f.
  w_OLEOBJECT = .NULL.
  w_CR = 0
  w_BAUD = space(5)
  w_TROV = .f.
  w_STRINGA = space(50)
  w_COUNT = 0
  w_STR_X = space(50)
  w_RECINIT = .f.
  w_NLET = 0
  w_INSTRING = space(50)
  w_CONT = 0
  w_STOP = space(1)
  w_FOUTFILE = 0
  w_FINFILE = 0
  w_T1 = 0
  w_FINE = 0
  w_POSQTA = 0
  w_POSPRZ = 0
  w_PARAM = space(254)
  w_T2 = 0
  w_FINFILE = 0
  w_GNEND = 0
  w_GNTOP = 0
  w_RITARDO = 0
  w_CARAT = space(50)
  pCursore = space(10)
  pTXTFile = space(254)
  pObjMsg = .NULL.
  p_ArrayFieldName = space(40)
  p_ArrayFieldPosition = space(40)
  * --- WorkFile variables
  DIS_HARD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Programma di gestione per la penna ottica Formula 734.
    *     ==================================================================================
    *     Per programmare la penna � necessario caricare tramite il software SYSTOOL il file ZUCCHETT.HEX.
    *     In questa modalit� per ogni articolo carico il CODICE, la QUANTITA' ed il PREZZO.
    *     Per poter interagire con la penna carico l'OCX MSCOMM32.OCX e setto le impostazioni della porta a cui
    *     � collegata la penna leggendo i dati da DIS_HARD.
    *     Le impostazioni per la porta sono le seguenti:
    *     
    *     RITARDO: 99
    *     BUFFER RICEZIONE: 4098
    *     BUFFER TRASMISSIONE: 2048
    *     BAUD: 9600
    *     PARITA': even
    *     BIT STOP: 1
    *     BIT DATI: 7
    *     
    *     Il batch legge dalla penna i dati relativi agli articoli e li scrive su di un file di testo creato nella directory temporanea
    *     dell'utente.
    *     Viene quindi lanciato il batch GSUT_BDT che, dato il file di testo appena creato, riempie il cursore passatogli come parametro.
    *     
    * --- Leggo dalla tabella DIS_HARD le impostazioni relative alla penne ottica
    * --- Read from DIS_HARD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIS_HARD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_HARD_idx,2],.t.,this.DIS_HARD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DHPROGRA,DHRITARD,DHBUFRIC,DHBUFTRA,DH__BAUD,DHPARITA,DHBITSTP,DHBITDAT,DHNUMPOR,DHPROPAR"+;
        " from "+i_cTable+" DIS_HARD where ";
            +"DHCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODDISP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DHPROGRA,DHRITARD,DHBUFRIC,DHBUFTRA,DH__BAUD,DHPARITA,DHBITSTP,DHBITDAT,DHNUMPOR,DHPROPAR;
        from (i_cTable) where;
            DHCODICE = this.oParentObject.w_CODDISP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PRGGES = NVL(cp_ToDate(_read_.DHPROGRA),cp_NullValue(_read_.DHPROGRA))
      this.w_RITARD = NVL(cp_ToDate(_read_.DHRITARD),cp_NullValue(_read_.DHRITARD))
      this.w_BUFRIC = NVL(cp_ToDate(_read_.DHBUFRIC),cp_NullValue(_read_.DHBUFRIC))
      this.w_BUFTRA = NVL(cp_ToDate(_read_.DHBUFTRA),cp_NullValue(_read_.DHBUFTRA))
      this.w_BAURAT = NVL(cp_ToDate(_read_.DH__BAUD),cp_NullValue(_read_.DH__BAUD))
      this.w_PARITA = NVL(cp_ToDate(_read_.DHPARITA),cp_NullValue(_read_.DHPARITA))
      this.w_BISTOP = NVL(cp_ToDate(_read_.DHBITSTP),cp_NullValue(_read_.DHBITSTP))
      this.w_BITDAT = NVL(cp_ToDate(_read_.DHBITDAT),cp_NullValue(_read_.DHBITDAT))
      this.w_PORTA = NVL(cp_ToDate(_read_.DHNUMPOR),cp_NullValue(_read_.DHNUMPOR))
      this.w_PARAM = NVL(cp_ToDate(_read_.DHPROPAR),cp_NullValue(_read_.DHPROPAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Trascodifica parit�
    do case
      case this.w_PARITA="0"
        this.w_PARITY = "N"
      case this.w_PARITA="1"
        this.w_PARITY = "O"
      case this.w_PARITA="2"
        this.w_PARITY = "E"
      case this.w_PARITA="3"
        this.w_PARITY = "M"
      case this.w_PARITA="4"
        this.w_PARITY = "S"
    endcase
    if not ah_YesNo("Inserire il lettore nel calamaio e premere s�. No per uscire")
      * --- L'utente ha premuto NO
      i_retcode = 'stop'
      return
    endif
    * --- Aggiungo l'OCX  (MSCOMM32.OCX) al Batch
    * --- Per farlo occorre creare una form (un batch non � un container valido per gli OLE)
    fOleForm = create ("FORM")
    fOleForm.visible= .f.
    fOleForm.Addobject("OleObject","OleControl","MSCOMMLib.MSComm")
    this.w_OLEOBJECT = fOleForm.OleObject
    * --- Definisco la porta da aprire
    this.w_OLEOBJECT.CommPort = this.w_PORTA
    * --- Setto la porta
    this.w_OLEOBJECT.Settings = ALLTRIM(STR(this.w_BAURAT,5,0))+","+this.w_PARITY+","+ALLTRIM(STR( this.w_BITDAT ,2,0))+","+ALLTRIM(STR(this.w_BISTOP,1,0))
    * --- Setto Dimensione Buffer di Ricezione
    this.w_OLEOBJECT.InBufferSize = this.w_BUFRIC
    * --- Apro la Porta ???
    this.w_OLEOBJECT.PortOpen = .T.
    * --- Definisco il numero di caratteri letti per volta
    this.w_OLEOBJECT.InputLen = 1
    * --- Fase ricezione dati
    this.w_OLEOBJECT.Output = "S"+chr(13)
    AddMsgNL("Inizio sincronizzazione...",this.oparentobject)
    * --- Ciclo di Sincronizzazione
    this.w_STRINGA = ""
    this.w_INSTRING = ""
    this.w_FILENAME = tempadhoc()+"\"+SYS(2015)
    this.w_FOUTFILE = fcreate(rtrim(this.w_FILENAME))
    this.w_INSTRING = this.w_OLEOBJECT.Input
    this.w_TIMEIN = seconds()
    this.w_TIMEOUT = .F.
    do while asc(this.w_INSTRING)=0
      if this.w_TIMEIN+5<seconds()
        this.w_TIMEOUT = .T.
        exit
      endif
      this.w_INSTRING = this.w_OLEOBJECT.Input
    enddo
    * --- Ritardo prima della lettura dati
    AddMsgNL("Sincronizzazione terminata",this.oparentobject)
    if type("g_PENSYNC")="N"
      this.w_TIME = g_PENSYNC
    else
      this.w_TIME = 4
    endif
    AddMsgNL("Standby di %1 secondi", this.oparentobject, alltrim(str(this.w_TIME)) )
    this.w_TL = 1
    do while this.w_TL <= this.w_TIME
      wait "" timeout 1
      AddMsgNL("%1%2...",this.oparentobject,space(2), str(this.w_TL))
      this.w_TL = this.w_TL + 1
    enddo
    * --- Leggo i dati sotto forma di stringa (CODICE + QUANTITA' + PREZZO UNITARIO)
    *     Per il firmware zucchett.hex la lunghezza � fissa ed � 43...
    this.w_OLEOBJECT.InputLen = 256
    this.w_NLET = 1
    do while this.w_OLEOBJECT.InBufferCount>0
      this.w_INSTRING = this.w_INSTRING + this.w_OLEOBJECT.Input
      this.w_T1 = Seconds()
      this.w_T2 = Seconds()+this.w_RITARDO/1000
      do while this.w_T1<=this.w_T2
        this.w_T1 = Seconds()
      enddo
      this.w_NLET = this.w_NLET+1
      Write=fWrite(this.w_FOUTFILE,this.w_INSTRING)
      this.w_INSTRING = ""
    enddo
    Chiudo=Fclose(this.w_FOUTFILE)
    * --- Chiudo la connessione con la penna
    this.w_OLEOBJECT.PortOpen = .F.
    * --- Distruggo la Form
    fOleForm.release
    AddMsgNL("%0Lettura dati terminata. Attendere completamento elaborazione",this.oparentobject)
    this.w_FOUTFILE = fopen(rtrim(this.w_FILENAME))
    this.w_FILENAME1 = tempadhoc()+"\"+SYS(2015)
    this.w_FINFILE = fcreate(rtrim(this.w_FILENAME1))
    this.w_STRINGA = ""
    this.w_STR_X = ""
    this.w_STRINGA = fget(this.w_FOUTFILE,1)
    this.w_FINE = 0
    * --- Per Quantit� e Prezzo ho due firmware, Zucchett.hex o AdHoc.hex
    *     Tra i due firmware cambia la posizione di QTA e PRZ
    if UPPER(this.w_PARAM)="ADHOC.HEX"
      this.w_POSQTA = 19
      this.w_POSPRZ = 29
    else
      this.w_POSQTA = 27
      this.w_POSPRZ = 42
    endif
    do while this.w_FINE <= 1 And Not fEof(this.w_FOUTFILE)
      if ASC(this.w_STRINGA) > 0
        this.w_STR_X = this.w_STR_X + this.w_STRINGA
        this.w_FINE = 0
      else
        if not ("ACK"$this.w_STR_X)
          if EMPTY(SUBSTR(this.w_STR_X,this.w_POSQTA,1)) and !EMPTY(this.w_STR_X)
            this.w_STR_X = STUFF(this.w_STR_X,this.w_POSQTA,1,"1")
          endif
          if EMPTY(SUBSTR(this.w_STR_X,this.w_POSPRZ,1)) and !EMPTY(this.w_STR_X)
            this.w_STR_X = STUFF(this.w_STR_X,this.w_POSPRZ,1,"0")
          endif
          Write=fWrite(this.w_FINFILE,this.w_STR_X + CHR(13) + CHR(10))
        endif
        this.w_STR_X = ""
        this.w_FINE = this.w_FINE + 1
      endif
      this.w_STRINGA = fget(this.w_FOUTFILE,1)
    enddo
    Chiudo=Fclose(this.w_FINFILE)
    Chiudo=Fclose(this.w_FOUTFILE)
    * --- Definisco la struttura del file di testo generato dalla penna, 
    *     queste informazioni sono utilizzate dal driver per file di testo
    *     per decifrare il file di testo...
    if UPPER(this.w_PARAM)="ADHOC.HEX"
       
 Dimension L_FieldName[4], L_FieldPosition [4] 
 L_FieldPosition =0 
 L_FieldName="" 
 L_FieldPosition [1]=1 
 L_FieldPosition [2]=16 
 L_FieldPosition [3]=20 
 * 
 L_FieldName[1]="CODICE" 
 L_FieldName[2]="QTAMOV" 
 L_FieldName[3]="PREZZO" 
 
    else
       
 Dimension L_FieldName[4], L_FieldPosition [4] 
 L_FieldPosition =0 
 L_FieldName="" 
 L_FieldPosition [1]=1 
 L_FieldPosition [2]=16 
 L_FieldPosition [3]=28 
 * 
 L_FieldName[1]="CODICE" 
 L_FieldName[2]="QTAMOV" 
 L_FieldName[3]="PREZZO" 
 
    endif
    * --- Riempio il cursore con il file TXT creato, utilzzo il DRIVER file di testo
    *     
    GSUT_BDT (This, this.pCursore , this.w_FILENAME1 , this.pObjMsg, @L_FieldName, @L_FieldPosition)
    Erase(this.w_FILENAME)
    Erase(this.w_FILENAME1)
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dichiarazione Variabili
    * --- Nome del  cursore creato
    this.w_LETTO = .F.
    this.w_ETB = 23
    this.w_TROV = .F.
    this.w_CR = 13
    this.w_STRINGA = ""
    this.w_INSTRING = ""
    this.w_RECINIT = .T.
    this.w_BAUD = ALLTRIM(STR(this.w_BAURAT,5,0))
    this.w_RITARDO = this.w_RITARD*10
    this.w_STOP = ALLTRIM(STR(this.w_BISTOP,1,0))
  endproc


  proc Init(oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition)
    this.pCursore=pCursore
    this.pTXTFile=pTXTFile
    this.pObjMsg=pObjMsg
    this.p_ArrayFieldName=p_ArrayFieldName
    this.p_ArrayFieldPosition=p_ArrayFieldPosition
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DIS_HARD'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition"
endproc
