* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1bcr                                                        *
*              Controllo relazioni tabella                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-26                                                      *
* Last revis.: 2016-05-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut1bcr",oParentObject,m.pOper)
return(i_retval)

define class tgsut1bcr as StdBatch
  * --- Local variables
  pOper = space(6)
  w_TABCHECK = space(15)
  w_PROG = .NULL.
  w_Btn_Ctrl = .NULL.
  w_OBJCTRL = .NULL.
  w_OSOURCE = .NULL.
  w_OLDPOSTIN = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cerca le relazioni di una tabella
    do case
      case Alltrim(this.pOper)="Open"
        * --- Se I tabelle che dipendono da w_TABELLA 
        *      F quelle da cui w_TABELLA dipende
        this.oParentObject.w_Cursor = this.oParentObject.w_TREEVIEW.cCursor
        if Not Empty(this.oParentObject.w_Cursor) And Used(this.oParentObject.w_Cursor)
           
 Select(this.oParentObject.w_Cursor) 
 Zap
        endif
        * --- Mi ritorna il cursore con tutte le tabelle e relazioni
        *     In questo caso passo direttamente quello dello zoom
        this.oParentObject.w_Cursor = CHKRELTAB(this.oParentObject.w_TABELLA, this.oParentObject.w_Cursor, this.oParentObject.w_DIREZ)
        * --- Riassegno il cursore alla treeview
        this.oParentObject.w_TREEVIEW.cCursor = this.oParentObject.w_Cursor
        This.oParentObject.NotifyEvent("Esegui")
      case Alltrim(this.pOper)="Close"
        if used( ( this.oParentObject.w_CURSOR ) )
          select ( this.oParentObject.w_CURSOR )
          use
        endif
      case Alltrim(this.pOper)="Entita" Or Alltrim(this.pOper)="Sedi" Or Alltrim(this.pOper)="Export1" Or Alltrim(this.pOper)="Export2"
        this.w_TABCHECK = Alltrim(this.oParentObject.w_TABPRINC)
        * --- Per gestire la maschera di export tabelle azienda dove sono presenti due zoom
        if Alltrim(this.pOper)="Export2"
          this.w_TABCHECK = Alltrim(this.oParentObject.w_TABPRINC2)
        endif
        if Empty(this.w_TABCHECK)
          if Alltrim(this.pOper)="Export"
            Ah_ErrorMsg("Selezionare una tabella",48,"")
          else
            Ah_ErrorMsg("Selezionare una entit� che abbia presente una tabella principale",48,"")
          endif
          i_retcode = 'stop'
          return
        endif
        this.w_PROG = GSUT_KCR()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          Ah_ErrorMsg("Impossibile aprire treeview controllo relazioni tabella!",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_TABELLA = this.w_TABCHECK
        if Alltrim(this.pOper)="Entita"
          * --- Cerco le tabelle che dipendono da quella principale
          this.w_PROG.w_DIREZ = "I"
        else
          * --- Cerco le tabelle da cui dipende la tabella principale dell'entit�
          this.w_PROG.w_DIREZ = "F"
        endif
        * --- Per fare eseguire il link sul campo TABELLA
        this.w_OBJCTRL = this.w_PROG.GetCtrl("w_TABELLA")
        this.w_OSOURCE.xKey( 1 ) = this.w_TABCHECK
        * --- Disabilitazione i post-In (sono riabilitati al termine dell'esecuzione di ecpDrop
        this.w_OLDPOSTIN = i_ServerConn[1,7]
        i_ServerConn[1,7]=.F.
        this.w_OBJCTRL.ecpDrop(this.w_OSOURCE)     
        i_ServerConn[1,7]= this.w_OLDPOSTIN 
        this.w_Btn_Ctrl = this.w_PROG.GetCtrl("\<Ricerca")
        if Type("this.w_Btn_Ctrl")="O"
          * --- Eseguo il click su ricerca
          this.w_Btn_Ctrl.Click()     
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
