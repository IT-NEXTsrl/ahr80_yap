* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bsd                                                        *
*              Report documenti da causale                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-05                                                      *
* Last revis.: 2016-04-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_MULTIREPORT,w_PARENT,w_TIPDOC,w_CODLIN,w_GRPDEF,w_STMPOPT,w_NoSearchRep,w_CONTREP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bsd",oParentObject,m.w_MULTIREPORT,m.w_PARENT,m.w_TIPDOC,m.w_CODLIN,m.w_GRPDEF,m.w_STMPOPT,m.w_NoSearchRep,m.w_CONTREP)
return(i_retval)

define class tgsar_bsd as StdBatch
  * --- Local variables
  w_MULTIREPORT = .NULL.
  w_PARENT = .NULL.
  w_TIPDOC = space(5)
  w_CODLIN = space(3)
  w_GRPDEF = space(5)
  w_STMPOPT = space(1)
  w_NoSearchRep = .f.
  w_CONTREP = .f.
  w_CPROWORD = 0
  w_LGCODUTE = 0
  w_LGCODGRP = 0
  w_LGPRGSTA = 0
  w_STAPRINC = 0
  w_LGFLGOPT = space(1)
  w_LG_SPLIT = space(254)
  w_LG__LINK = space(254)
  w_LGFLGCHG = .f.
  w_LGGRPDEF = space(5)
  w_LGTIPREP = space(1)
  w_MAINFOUND = .f.
  w_PRGSTAPRI = space(30)
  w_PRGSTAPRO = space(30)
  w_PRGSTASEC = space(8)
  w_LGNO_RST = .f.
  w_LGSTMPOR = space(2)
  w_FLVEAC = space(1)
  w_FLARCO = space(1)
  w_OREP = space(254)
  w_OQRY = space(254)
  w_OBAT = space(254)
  w_OTES = .f.
  w_FLSTLM = space(1)
  w_ODESCRI = space(50)
  w_REPOPT = space(0)
  w_STMPOR = space(7)
  w_RISTAMPA = .f.
  w_CARMEMO = 0
  w_PRNSYS = .f.
  w_OUROWNUM = 0
  w_OUTPUTMP_RECCOUNT = 0
  w_TIPOIN = space(5)
  w_RIFPRA = space(1)
  w_RIFOGG = space(1)
  w_RIFVAL = space(1)
  w_RIFDAT = space(1)
  w_RIFSUP = space(1)
  w_RIFRIGHE = space(1)
  w_RIFGU = space(1)
  w_RIFMINMAX = space(1)
  w_RIFSCOFIN = space(1)
  w_RIFDATDIR = space(1)
  w_RIFDATONO = space(1)
  w_RIFDATTEM = space(1)
  w_RIFDATSPA = space(1)
  w_RIFQTA = space(1)
  w_RIFRESTIT = space(1)
  w_RIFPARTI = space(1)
  w_RIFNOIMPO = space(1)
  w_RIFCOEDIF = space(1)
  w_RIFIMP = space(1)
  w_RIFDATGEN = space(1)
  w_PRTIPCAU = space(1)
  w_RIFRAGFAS = space(1)
  w_CntRepPrin = 0
  w_ReportPrincipale = space(1)
  w_ReportProdPrincipale = space(1)
  w_ReportSelezionato = 0
  w_SelezionaPrincipale = .f.
  w_OLDAREA = 0
  w_QRYDINAMICA = space(254)
  i_qry = .NULL.
  i_qryTmp = .NULL.
  i_qryLoader = .NULL.
  L_i = 0
  w_FSERIAL = .f.
  w_Ordine = space(0)
  w_Filtro = space(50)
  w_Ret = space(1)
  * --- WorkFile variables
  REPO_DOC_idx=0
  TIP_DOCU_idx=0
  OUT_PUTS_idx=0
  OUTPUTMP_idx=0
  TMP_MAST_idx=0
  PAR_PARC_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Recupera sulla base dei parametri i report da concatenare all'oggetto MultiReport
    * --- w_STMPOPT
    *     'S' = Stampa tutti i report secondari,
    *     'N' = Non stampa i report secondari,
    *     'C' = Esclude i report opzionali,
    *     'D' = Richiede se stampare i report secondari
    *     '2' = Stampa solo i report secondari,
    * --- Controllo che il report usato esista nell'output utente.
    if NOT EMPTY(this.w_TIPDOC)
      this.w_RISTAMPA = .F.
      if !IsAlt() AND this.w_CONTREP
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      USE IN SELECT("CURREP2")
    else
      * --- Ciclo sulle lingue, raggruppo per tipo doc, lingua, gruppo di default
      * --- Se ci sono pi� record, il gruppo di default deve essere passato come filtro anche se vuoto
      this.w_OUTPUTMP_RECCOUNT = 0
      if !IsAlt() AND this.w_CONTREP
        * --- Select from OUTPUTMP
        i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2],.t.,this.OUTPUTMP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select MVTIPDOC, ANCODLIN, ANGRPDEF  from "+i_cTable+" OUTPUTMP ";
              +" group by MVTIPDOC, ANCODLIN, ANGRPDEF";
               ,"_Curs_OUTPUTMP")
        else
          select MVTIPDOC, ANCODLIN, ANGRPDEF from (i_cTable);
           group by MVTIPDOC, ANCODLIN, ANGRPDEF;
            into cursor _Curs_OUTPUTMP
        endif
        if used('_Curs_OUTPUTMP')
          select _Curs_OUTPUTMP
          locate for 1=1
          do while not(eof())
          this.w_TIPDOC = NVL(MVTIPDOC,"")
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
            select _Curs_OUTPUTMP
            continue
          enddo
          use
        endif
      endif
      * --- Select from OUTPUTMP
      i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2],.t.,this.OUTPUTMP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MVTIPDOC, ANCODLIN, ANGRPDEF  from "+i_cTable+" OUTPUTMP ";
            +" group by MVTIPDOC, ANCODLIN, ANGRPDEF";
            +" order by MVTIPDOC, ANCODLIN, ANGRPDEF";
             ,"_Curs_OUTPUTMP")
      else
        select MVTIPDOC, ANCODLIN, ANGRPDEF from (i_cTable);
         group by MVTIPDOC, ANCODLIN, ANGRPDEF;
         order by MVTIPDOC, ANCODLIN, ANGRPDEF;
          into cursor _Curs_OUTPUTMP
      endif
      if used('_Curs_OUTPUTMP')
        select _Curs_OUTPUTMP
        locate for 1=1
        do while not(eof())
        if this.w_OUTPUTMP_RECCOUNT = 0
          this.w_OUTPUTMP_RECCOUNT = RECCOUNT()
        endif
        this.w_MULTIREPORT.NewMain()     
        this.w_RISTAMPA = .T.
        this.w_TIPDOC = NVL(MVTIPDOC,"")
        this.w_CODLIN = NVL(ANCODLIN,"")
        this.w_GRPDEF = NVL(ANGRPDEF,"")
        this.w_TIPOIN = NVL(MVTIPDOC," ")
        if Isalt()
          * --- Legge dai parametri di parcellazione i check per la stampa del documento
          * --- Read from PAR_PARC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_PARC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_PARC_idx,2],.t.,this.PAR_PARC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PRRIFPRA,PRRIFOGG,PRRIFVAL,PRRIFDAT,PRRIFSUP,PRRIFRIG,PRRIFGUF,PRRIFMIA,PRRIFSCF,PRDATDIR,PRDATONO,PRDATTEM,PRDATSPA,PRRIFQTA,PRNOIMPO,PRCOEDIF,PRRIFIMP,PRRESTIT,PR_PARTI,PRDATGEN,PRTIPCAU,PRRIFFAS"+;
              " from "+i_cTable+" PAR_PARC where ";
                  +"PRCODAZI = "+cp_ToStrODBC(i_codazi);
                  +" and PRCAUPRE = "+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PRRIFPRA,PRRIFOGG,PRRIFVAL,PRRIFDAT,PRRIFSUP,PRRIFRIG,PRRIFGUF,PRRIFMIA,PRRIFSCF,PRDATDIR,PRDATONO,PRDATTEM,PRDATSPA,PRRIFQTA,PRNOIMPO,PRCOEDIF,PRRIFIMP,PRRESTIT,PR_PARTI,PRDATGEN,PRTIPCAU,PRRIFFAS;
              from (i_cTable) where;
                  PRCODAZI = i_codazi;
                  and PRCAUPRE = this.w_TIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_RIFPRA = NVL(cp_ToDate(_read_.PRRIFPRA),cp_NullValue(_read_.PRRIFPRA))
            this.w_RIFOGG = NVL(cp_ToDate(_read_.PRRIFOGG),cp_NullValue(_read_.PRRIFOGG))
            this.w_RIFVAL = NVL(cp_ToDate(_read_.PRRIFVAL),cp_NullValue(_read_.PRRIFVAL))
            this.w_RIFDAT = NVL(cp_ToDate(_read_.PRRIFDAT),cp_NullValue(_read_.PRRIFDAT))
            this.w_RIFSUP = NVL(cp_ToDate(_read_.PRRIFSUP),cp_NullValue(_read_.PRRIFSUP))
            this.w_RIFRIGHE = NVL(cp_ToDate(_read_.PRRIFRIG),cp_NullValue(_read_.PRRIFRIG))
            this.w_RIFGU = NVL(cp_ToDate(_read_.PRRIFGUF),cp_NullValue(_read_.PRRIFGUF))
            this.w_RIFMINMAX = NVL(cp_ToDate(_read_.PRRIFMIA),cp_NullValue(_read_.PRRIFMIA))
            this.w_RIFSCOFIN = NVL(cp_ToDate(_read_.PRRIFSCF),cp_NullValue(_read_.PRRIFSCF))
            this.w_RIFDATDIR = NVL(cp_ToDate(_read_.PRDATDIR),cp_NullValue(_read_.PRDATDIR))
            this.w_RIFDATONO = NVL(cp_ToDate(_read_.PRDATONO),cp_NullValue(_read_.PRDATONO))
            this.w_RIFDATTEM = NVL(cp_ToDate(_read_.PRDATTEM),cp_NullValue(_read_.PRDATTEM))
            this.w_RIFDATSPA = NVL(cp_ToDate(_read_.PRDATSPA),cp_NullValue(_read_.PRDATSPA))
            this.w_RIFQTA = NVL(cp_ToDate(_read_.PRRIFQTA),cp_NullValue(_read_.PRRIFQTA))
            this.w_RIFNOIMPO = NVL(cp_ToDate(_read_.PRNOIMPO),cp_NullValue(_read_.PRNOIMPO))
            this.w_RIFCOEDIF = NVL(cp_ToDate(_read_.PRCOEDIF),cp_NullValue(_read_.PRCOEDIF))
            this.w_RIFIMP = NVL(cp_ToDate(_read_.PRRIFIMP),cp_NullValue(_read_.PRRIFIMP))
            this.w_RIFRESTIT = NVL(cp_ToDate(_read_.PRRESTIT),cp_NullValue(_read_.PRRESTIT))
            this.w_RIFPARTI = NVL(cp_ToDate(_read_.PR_PARTI),cp_NullValue(_read_.PR_PARTI))
            this.w_RIFDATGEN = NVL(cp_ToDate(_read_.PRDATGEN),cp_NullValue(_read_.PRDATGEN))
            this.w_PRTIPCAU = NVL(cp_ToDate(_read_.PRTIPCAU),cp_NullValue(_read_.PRTIPCAU))
            this.w_RIFRAGFAS = NVL(cp_ToDate(_read_.PRRIFFAS),cp_NullValue(_read_.PRRIFFAS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_PARENT.w_RIFPRA = this.w_RIFPRA
          this.w_PARENT.w_RIFOGG = this.w_RIFOGG
          this.w_PARENT.w_RIFVAL = this.w_RIFVAL
          this.w_PARENT.w_RIFDAT = this.w_RIFDAT
          this.w_PARENT.w_RIFSUP = this.w_RIFSUP
          this.w_PARENT.w_RIFRIGHE = this.w_RIFRIGHE
          this.w_PARENT.w_RIFGU = this.w_RIFGU
          this.w_PARENT.w_RIFMINMAX = this.w_RIFMINMAX
          this.w_PARENT.w_RIFSCOFIN = this.w_RIFSCOFIN
          this.w_PARENT.w_RIFDATDIR = this.w_RIFDATDIR
          this.w_PARENT.w_RIFDATONO = this.w_RIFDATONO
          this.w_PARENT.w_RIFDATTEM = this.w_RIFDATTEM
          this.w_PARENT.w_RIFDATSPA = this.w_RIFDATSPA
          this.w_PARENT.w_RIFQTA = this.w_RIFQTA
          this.w_PARENT.w_RIFNOIMPO = this.w_RIFNOIMPO
          * --- L'inizializzazione � ad 'A' per ogni tipologia
          this.w_PARENT.w_RIFRESTIT = this.w_RIFRESTIT
          this.w_PARENT.w_RIFPARTI = this.w_RIFPARTI
          this.w_PARENT.w_RIFCOEDIF = this.w_RIFCOEDIF
          this.w_PARENT.w_RIFIMP = this.w_RIFIMP
          this.w_PARENT.w_RIFDATGEN = this.w_RIFDATGEN
          this.w_PARENT.w_PRTIPCAU = this.w_PRTIPCAU
          this.w_PARENT.w_RIFRAGFAS = this.w_RIFRAGFAS
        endif
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_MULTIREPORT.CheckPrintSystemChanged()     
          select _Curs_OUTPUTMP
          continue
        enddo
        use
      endif
      USE IN SELECT("CURREP2")
    endif
    i_retval=.T.
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Determino il programma di stampa
    if IsAhe()
      this.w_PRGSTAPRI = LEFT("GSVE_MDV"+SPACE(30),30)
      this.w_PRGSTAPRO = SPACE(30)
      this.w_PRGSTASEC = LEFT("GSVE_MDV"+SPACE(30),30)
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDFLVEAC,TDFLSTLM,TDCATDOC"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDFLVEAC,TDFLSTLM,TDCATDOC;
          from (i_cTable) where;
              TDTIPDOC = this.w_TIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
        this.w_FLSTLM = NVL(cp_ToDate(_read_.TDFLSTLM),cp_NullValue(_read_.TDFLSTLM))
        w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDFLVEAC,TDFLSTLM,TDFLARCO,TDCATDOC"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDFLVEAC,TDFLSTLM,TDFLARCO,TDCATDOC;
          from (i_cTable) where;
              TDTIPDOC = this.w_TIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
        this.w_FLSTLM = NVL(cp_ToDate(_read_.TDFLSTLM),cp_NullValue(_read_.TDFLSTLM))
        this.w_FLARCO = NVL(cp_ToDate(_read_.TDFLARCO),cp_NullValue(_read_.TDFLARCO))
        w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PRGSTAPRI = LEFT(IIF(this.w_FLVEAC="V" OR w_CATDOC="OR", "GSVE_MDV", "GSAC_MDV")+SPACE(30),30)
      this.w_PRGSTAPRO = LEFT("GSDS_SDP"+SPACE(30),30)
      this.w_PRGSTASEC = LEFT(IIF(this.w_FLVEAC="V" OR (w_CATDOC="OR" AND g_ACQU="S") OR (this.w_FLVEAC<>"V" AND g_ACQU="S"), "GSVEAMDV", "GSACAMDV")+SPACE(30),30)
    endif
    * --- Controlliamo se ci sono pi� report principali, nel qual caso permettiamo all'utente di sceglierne uno.
    if !this.w_NoSearchRep and this.w_STMPOPT<>"2"
      * --- Nella query GSAR_BSD sono estratti i report principali 
      *     e quelli produzione principale. Le due uguaglianze sono verificate in OR, 
      *     quindi i due valori devono essere gestiti contemporaneamente
      this.w_ReportPrincipale = "M"
      this.w_ReportProdPrincipale = "R"
      VQ_EXEC( "QUERY\GSAR_BSD" , THIS , "CURREPO" )
      if RECCOUNT("CURREPO")>1
        if USED("CURREP2")
          select CURREPO.* from CURREPO inner join CURREP2 on CURREPO.LGCODICE =CURREP2.LGCODICE ; 
 and CURREPO.CPROWNUM =CURREP2.CPROWNUM into cursor CURREP3
        endif
        * --- Attiviamo la maschera per permettere all'utente di selezionare il report principale.
        *     Per essere visibili in GASR_BKD alcune variabili, che qui sono caller, devono essere dichiarate locali.
        if !USED("CURREP3") OR RECCOUNT("CURREPO")<>RECCOUNT("CURREP3") OR RECCOUNT("CURREP2")<>RECCOUNT("CURREP3")
          this.w_SelezionaPrincipale = .F.
          this.w_ReportSelezionato = 0
          do GSAR_KSD with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_ReportSelezionato>0
            this.w_SelezionaPrincipale = .T.
          endif
          select * from CURREPO into cursor CURREP2
        endif
      else
        this.w_SelezionaPrincipale = .F.
        this.w_ReportSelezionato = 0
      endif
      USE IN SELECT("CURREP3" )
      USE IN SELECT("CURREPO" )
    endif
    this.w_ReportPrincipale = " "
    this.w_ReportProdPrincipale = " "
    if this.w_STMPOPT="D"
      this.w_STMPOPT = this.w_FLSTLM
      if this.w_FLSTLM="B"
        this.w_REPOPT = ""
        * --- Estraggo i report secondari che dovranno essere stampati
        * --- Select from QUERY\GSAR1BSD
        do vq_exec with 'QUERY\GSAR1BSD',this,'_Curs_QUERY_GSAR1BSD','',.f.,.t.
        if used('_Curs_QUERY_GSAR1BSD')
          select _Curs_QUERY_GSAR1BSD
          locate for 1=1
          do while not(eof())
          this.w_REPOPT = this.w_REPOPT + CHR(13) + CHR(10) + OUDESCRI
            select _Curs_QUERY_GSAR1BSD
            continue
          enddo
          use
        endif
        if NOT EMPTY(this.w_REPOPT) AND ah_YesNo("Si desidera stampare anche i report opzionali?%0%1","", ALLTRIM(this.w_REPOPT))
          this.w_STMPOPT = "S"
        else
          this.w_STMPOPT = "C"
        endif
      endif
    endif
    * --- Leggo dettaglio report della causale
    this.w_MAINFOUND = .F.
    this.w_OLDAREA = SELECT()
    vq_exec( "QUERY\GSAR_BSD", this, "GSAR_BSD_CURSOR" )
    * --- Calcola il CPROWORD del primo report associato alla causale
    L_MIN_CPROWORD = 0
    CALCULATE MIN ( CPROWORD ) TO L_MIN_CPROWORD
    * --- Se non � richiesto il cambio di flusso di stampa si adotta l'ordinamento per
    *     LGCODLIN, LGGRPDEF, LGCODUTE, LGCODGRP, CPROWORD
    * --- Il flag di cambia flusso di stampa della prima riga non deve essere considerato
    COUNT FOR LGFLPRSY = "S" AND CPROWORD <> L_MIN_CPROWORD TO L_LGFLPRSY_COUNT
    if L_LGFLPRSY_COUNT = 0
      SELECT * FROM GSAR_BSD_CURSOR ORDER BY LGTIPREP,LGCODLIN, LGGRPDEF, LGCODUTE, LGCODGRP, CPROWORD INTO CURSOR GSAR_BSD_CURSOR
    else
      * --- Se � stato richiesto il cambio di flusso di stampa si adotta l'ordinamento per CPROWORD, LGCODLIN, LGGRPDEF, LGCODUTE, LGCODGRP
      SELECT * FROM GSAR_BSD_CURSOR ORDER BY LGTIPREP,CPROWORD, LGCODLIN, LGGRPDEF, LGCODUTE, LGCODGRP INTO CURSOR GSAR_BSD_CURSOR
    endif
    SELECT GSAR_BSD_CURSOR
    GO TOP
    do while NOT EOF()
      this.w_CPROWORD = CPROWORD
      this.w_LGPRGSTA = LGPRGSTA
      this.w_LGFLGOPT = LGFLGOPT
      this.w_LG_SPLIT = ALLTRIM(NVL(LG_SPLIT, ""))
      this.w_LG__LINK = ALLTRIM(NVL(LG__LINK, ""))
      this.w_LGFLGCHG = .F.
      this.w_LGTIPREP = LGTIPREP
      this.w_OREP = OUNOMREP
      this.w_OQRY = OUNOMQUE
      this.w_OBAT = OUNOMBAT
      this.w_ODESCRI = OUDESCRI
      this.w_OTES = IIF(NVL(OUSTESTO, "N")="S", .T., .F.)
      this.w_LGNO_RST = IIF(NVL(LGNO_RST, "N")="S", .T., .F.)
      this.w_LGSTMPOR = NVL(LGSTMPOR, "  ")
      this.w_STMPOR = this.w_LGSTMPOR+ALLTRIM(this.w_TIPDOC)
      this.w_CARMEMO = OUCARMEM
      this.w_PRNSYS = NVL(LGFLPRSY, " ") = "S"
      * --- Numero report, necessario per i report di produzione
      this.w_OUROWNUM = NVL(OUROWNUM, 0)
      if this.w_STMPOPT<>"2" OR this.w_LGTIPREP<>"M" AND this.w_LGTIPREP<>"R"
        * --- Aggiungo i report
        * --- Modifico la query se ristampa documenti
        if this.w_RISTAMPA
          this.w_QRYDINAMICA = this.w_OQRY
          * --- Eseguo la join tra la query dinamica (output utente) e la tabella temporanea 
          *     OUTPUTMP
           
 Local i_cTempTable, l_Field 
 i_nIdx=cp_AddTableDef("TMP_MAST") && aggiunge la definizione nella lista delle tabelle 
 i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          this.i_qry=createobject("cpquery")
          this.i_qryTmp=createobject("cpquery")
          this.i_qryLoader=createobject("cpqueryloader")
          this.i_qryLoader.LoadQuery(ADDBS(JUSTPATH(trim(this.w_QRYDINAMICA)))+JUSTSTEM(trim(this.w_QRYDINAMICA)), this.i_qryTmp, this.w_PARENT)     
          this.L_i = 1
          * --- Cerco l'esistenza dell'alias MVSERIAL necessario per effettuare la Join
          this.w_FSERIAL = .F.
          do while this.L_i <= alen(this.i_qryTmp.i_Fields,1)
            if Type("This.i_qryTmp.i_Fields[this.l_i,2]")="C" AND this.i_qryTmp.i_Fields[this.l_i,2]="MVSERIAL"
              this.w_FSERIAL = .T.
            endif
            this.L_i = this.L_i + 1
          enddo
          * --- Se trovo MVSERIAL eseguo la query
          if this.w_FSERIAL
            this.i_qry.oParentObject = this.w_PARENT
            this.i_qry.mLoadValuedFilterParam("w_QRYDINAMICA","Nome query","Char",254,0, this.w_QRYDINAMICA,"")     
            this.i_qry.mLoadFile("GSAR1BSD.VQR","GSAR1BSD","GSAR1BSD+")     
            this.i_qry.mLoadFile("Tabella temporanea output","OUTPUTMP","OUTPUTMP")     
            this.i_qry.mLoadField("GSAR1BSD.*","","C",10,0)     
            this.i_qry.mLoadJoin("","GSAR1BSD","OUTPUTMP","OUTPUTMP.MVSERIAL=GSAR1BSD.MVSERIAL","Inner")     
            if Not Empty(this.w_TIPDOC)
              this.i_qry.mLoadWhere("OUTPUTMP.MVTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC)+" AND ",".f.",.f.)     
            endif
            if Not Empty(this.w_CODLIN)
              this.i_qry.mLoadWhere("OUTPUTMP.ANCODLIN = "+cp_ToStrODBC(this.w_CODLIN)+" AND ",".f.",.f.)     
            endif
            if Not Empty(this.w_GRPDEF)
              this.i_qry.mLoadWhere("OUTPUTMP.ANGRPDEF = "+cp_ToStrODBC(this.w_GRPDEF)+ " AND ",".f.",.f.)     
            endif
            if Empty(this.w_GRPDEF) AND this.w_OUTPUTMP_RECCOUNT > 1
              * --- Il filtro deve essere eliminato solo se non ci sono altre righe dove � specificato il gruppo di stampa
              this.i_qry.mLoadWhere("OUTPUTMP.ANGRPDEF is null AND ",".f.",.f.)     
            endif
            this.L_i = 1
            this.w_Ordine = ""
            do while this.L_i <= alen(this.i_qryTmp.i_OrderBy,1)
              if Type("This.i_qryTmp.i_OrderBy[this.l_i,1]")="C"
                if NOT EMPTY(JUSTEXT(this.i_qryTmp.i_OrderBy[this.l_i,1]))
                  l_Field="GSAR1BSD."+JUSTEXT(this.i_qryTmp.i_OrderBy[this.l_i,1])
                  this.w_Ordine = this.w_Ordine+IIF(Empty(this.w_Ordine),"",",")+JUSTEXT(this.i_qryTmp.i_OrderBy[this.l_i,1])
                else
                  l_Field= "GSAR1BSD."+ this.i_qryTmp.i_OrderBy[this.l_i,1]
                  this.w_Ordine = this.w_Ordine+IIF(Empty(this.w_Ordine),"",",")+ this.i_qryTmp.i_OrderBy[this.l_i,1]
                endif
                this.i_qry.mLoadOrder(IIF(Empty(l_Field), this.i_qryTmp.i_OrderBy[this.l_i,1], l_Field), this.i_qryTmp.i_OrderBy[this.l_i,2])     
              endif
              this.L_i = this.L_i + 1
            enddo
            this.i_qry.mDoQuery("","Exec",.F.,.F.,i_cTempTable,.F.,.F.)     
            * --- La query GSAR2BSD legge i dati da TMP_MAST
            this.w_OQRY = ".\QUERY\GSAR2BSD.VQR"
            * --- La query GSAR2BSD esegue una select * quindi si perde gli ordinamenti che 
            *     sono stati usati per creare la tabella temporanea
            *     La query viene eseguita nel AddReport lanciato in seguito.
            *     Per riuscire ad indicare parametricamente l0Ordinamento ci appoggiamo alla variabile
            *     w_OrderBy che per� deve essere definita sulla gestione che viene passata come oggetto
            *     In questo caso w_Parent. 
            *     Aggiungiamo quindi la propriet� w_OrderBy manualmente riportando l'ordinamento calcolato sopra
            if Type("this.w_Parent.w_OrderBy")="U"
              ADDPROPERTY(this.w_Parent, "w_OrderBy", this.w_Ordine)
            endif
          endif
           
 this.TMP_MAST_idx=i_nIdx 
 i_TableProp[i_nIdx,4]=1 && segna come usato 1 volta
          this.i_qryTmp = .NULL.
          this.i_qryLoader = .NULL.
          this.i_qry = .NULL.
        endif
        * --- Potevamo inserire il controllo sul cprownum selezionato direttamente nella query, ma data l'esiguit� dei dati elaborati manteniamo il vecchio controllo.
        do case
          case (this.w_LGTIPREP="M" OR this.w_LGTIPREP="R" OR this.w_STMPOPT="2") AND !this.w_MAINFOUND AND (!this.w_SelezionaPrincipale OR (this.w_ReportSelezionato>0 AND this.w_ReportSelezionato=this.w_OUROWNUM))
            * --- Inserisco solo un report principale
            this.w_MAINFOUND = .T.
            * --- Aggiungo report principale
            if !this.w_RISTAMPA OR this.w_LGTIPREP="M" OR this.w_STMPOPT="2" OR (this.w_RISTAMPA And INLIST(this.w_OUROWNUM, 1, 3)) OR this.w_ReportSelezionato>0
              if this.w_STMPOPT="2"
                * --- Report principale escluso: promuovo il primo report secondario come principale
                * --- inverto i valori LG_SPLIT e LG__LINK
                this.w_LG_SPLIT = ALLTRIM(NVL(GSAR_BSD_CURSOR.LG__LINK, ""))
                this.w_LG__LINK = ALLTRIM(NVL(GSAR_BSD_CURSOR.LG_SPLIT, ""))
              endif
              * --- Dobbiamo gestire con il multireport anche i report di produzione principale nel caso in cui siano stati selezionati
              this.w_MULTIREPORT.AddReport(this.w_PARENT, this.w_OREP, this.w_ODESCRI, this.w_OQRY, this.w_OBAT, this.w_LGNO_RST, .T., this.w_OTES, this.w_LG_SPLIT, "", this.w_STMPOR, this.w_CPROWORD, this.w_LGFLGCHG, this.w_CARMEMO, , "LUCODISO", this.w_PRNSYS)     
            else
              if Type("This.oParentObject.w_NO_RISTAMPA_PROD")<>"U"
                this.oParentObject.w_NO_RISTAMPA_PROD = .T.
              endif
            endif
          case this.w_LGTIPREP<>"M" AND this.w_LGTIPREP<>"R" AND (this.w_STMPOPT$"S-2" OR (this.w_STMPOPT="C" AND this.w_LGFLGOPT<>"S"))
            * --- Aggiungo report secondario/produzione
            if this.w_LGTIPREP="P" AND g_DISB="S" AND this.w_FLARCO="S"
              * --- In caso di ristampa non stampo la produzione
              if !this.w_RISTAMPA Or INLIST(this.w_OUROWNUM, 1, 3)
                this.w_MULTIREPORT.AddReport(this.w_PARENT, this.w_OREP, this.w_ODESCRI, this.w_OQRY, this.w_OBAT, this.w_LGNO_RST, .F., this.w_OTES, this.w_LG_SPLIT, this.w_LG__LINK, this.w_STMPOR, this.w_CPROWORD, this.w_LGFLGCHG, this.w_CARMEMO, , , this.w_PRNSYS)     
              else
                if Type("This.oParentObject.w_NO_RISTAMPA_PROD")<>"U"
                  this.oParentObject.w_NO_RISTAMPA_PROD = .T.
                endif
              endif
            endif
            if this.w_LGTIPREP="S"
              this.w_MULTIREPORT.AddReport(this.w_PARENT, this.w_OREP, this.w_ODESCRI, this.w_OQRY, this.w_OBAT, this.w_LGNO_RST, .F., this.w_OTES, this.w_LG_SPLIT, this.w_LG__LINK, this.w_STMPOR, this.w_CPROWORD, this.w_LGFLGCHG, this.w_CARMEMO, , IIF(EMPTY(g_DEMANDEDLANGUAGE),"LUCODISO",""), this.w_PRNSYS)     
            endif
        endcase
        * --- Rimuovo la propriet� aggiunta manualmente per evitare problemi se si esegue
        *     la stampa due volte di seguito
        if Type("this.w_Parent.w_OrderBy")="C" AND lower(this.w_Parent.class)<>"tgsva_bps"
          REMOVEPROPERTY(this.w_Parent, "w_OrderBy")
        endif
      endif
      SELECT GSAR_BSD_CURSOR
      SKIP
    enddo
    SELECT GSAR_BSD_CURSOR
    USE
    SELECT( this.w_OLDAREA )
    if !this.w_MAINFOUND And Type("g_NoMsgStampDoc")="C" And g_NoMsgStampDoc = "S"
      Ah_ErrorMsg("Non � stato trovato nessun report associato alla causale %1%0Lingua: %2%0Gruppo output: %3","","", this.w_TIPDOC, this.w_CODLIN, this.w_GRPDEF)
    endif
    if this.w_RISTAMPA And VarType(i_cTempTable) = "C"
      if this.w_FSERIAL
        * --- Dopo aver creato la tabella temporanea elimino da Outputmp i record che vanno in stampa
        *     inseriti per i filtri relativi a Lingua e Gruppo report, altrimenti alcuni record potrebbero essere
        *     stampati 2 volte
        this.w_Filtro = "MVSERIAL in (Select distinct MVSERIAL from "+i_cTempTable+" )"
        this.w_Ret = DeleteTable("OUTPUTMP","",.F.,.F.,this.w_Filtro)
      endif
      * --- Drop temporary table TMP_MAST
      i_nIdx=cp_GetTableDefIdx('TMP_MAST')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_MAST')
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if IsAhe()
      this.w_PRGSTAPRI = LEFT("GSVE_MDV"+SPACE(30),30)
      this.w_PRGSTAPRO = SPACE(30)
      this.w_PRGSTASEC = LEFT("GSVE_MDV"+SPACE(30),30)
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDFLVEAC,TDFLSTLM,TDCATDOC"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDFLVEAC,TDFLSTLM,TDCATDOC;
          from (i_cTable) where;
              TDTIPDOC = this.w_TIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
        this.w_FLSTLM = NVL(cp_ToDate(_read_.TDFLSTLM),cp_NullValue(_read_.TDFLSTLM))
        w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDFLVEAC,TDFLSTLM,TDFLARCO,TDCATDOC"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDFLVEAC,TDFLSTLM,TDFLARCO,TDCATDOC;
          from (i_cTable) where;
              TDTIPDOC = this.w_TIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
        this.w_FLSTLM = NVL(cp_ToDate(_read_.TDFLSTLM),cp_NullValue(_read_.TDFLSTLM))
        this.w_FLARCO = NVL(cp_ToDate(_read_.TDFLARCO),cp_NullValue(_read_.TDFLARCO))
        w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PRGSTAPRI = LEFT(IIF(this.w_FLVEAC="V" OR w_CATDOC="OR", "GSVE_MDV", "GSAC_MDV")+SPACE(30),30)
      this.w_PRGSTAPRO = LEFT("GSDS_SDP"+SPACE(30),30)
      this.w_PRGSTASEC = LEFT(IIF(this.w_FLVEAC="V" OR (w_CATDOC="OR" AND g_ACQU="S") OR (this.w_FLVEAC<>"V" AND g_ACQU="S"), "GSVEAMDV", "GSACAMDV")+SPACE(30),30)
    endif
    * --- Select from gsar4bsd
    do vq_exec with 'gsar4bsd',this,'_Curs_gsar4bsd','',.f.,.t.
    if used('_Curs_gsar4bsd')
      select _Curs_gsar4bsd
      locate for 1=1
      do while not(eof())
      ah_errormsg("Associazione report %1 causale %2 non valida",16,,_Curs_gsar4bsd.LGPRGSTA,this.w_TIPDOC)
      i_retval=.F.
      this.bUpdateParentObject=.F.
      i_retcode = 'stop'
      return
        select _Curs_gsar4bsd
        continue
      enddo
      use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_MULTIREPORT,w_PARENT,w_TIPDOC,w_CODLIN,w_GRPDEF,w_STMPOPT,w_NoSearchRep,w_CONTREP)
    this.w_MULTIREPORT=w_MULTIREPORT
    this.w_PARENT=w_PARENT
    this.w_TIPDOC=w_TIPDOC
    this.w_CODLIN=w_CODLIN
    this.w_GRPDEF=w_GRPDEF
    this.w_STMPOPT=w_STMPOPT
    this.w_NoSearchRep=w_NoSearchRep
    this.w_CONTREP=w_CONTREP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='REPO_DOC'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='OUT_PUTS'
    this.cWorkTables[4]='OUTPUTMP'
    this.cWorkTables[5]='*TMP_MAST'
    this.cWorkTables[6]='PAR_PARC'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_OUTPUTMP')
      use in _Curs_OUTPUTMP
    endif
    if used('_Curs_OUTPUTMP')
      use in _Curs_OUTPUTMP
    endif
    if used('_Curs_QUERY_GSAR1BSD')
      use in _Curs_QUERY_GSAR1BSD
    endif
    if used('_Curs_gsar4bsd')
      use in _Curs_gsar4bsd
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_MULTIREPORT,w_PARENT,w_TIPDOC,w_CODLIN,w_GRPDEF,w_STMPOPT,w_NoSearchRep,w_CONTREP"
endproc
