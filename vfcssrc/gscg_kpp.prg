* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kpp                                                        *
*              Numero protocollo Primanota                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-14                                                      *
* Last revis.: 2010-02-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kpp",oParentObject))

* --- Class definition
define class tgscg_kpp as StdForm
  Top    = 112
  Left   = 87

  * --- Standard Properties
  Width  = 388
  Height = 129
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-02-19"
  HelpContextID=118872727
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gscg_kpp"
  cComment = "Numero protocollo Primanota"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PNNUMPRO = 0
  w_PNALFPRO = space(10)
  w_PNANNPRO = space(4)
  w_SERIALE = space(10)
  w_PNPRP = space(2)
  w_NUMPRO = 0
  w_ALFPRO = space(10)
  w_CODCAU = space(5)
  w_PNTIPREG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kppPag1","gscg_kpp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPNNUMPRO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ESERCIZI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PNNUMPRO=0
      .w_PNALFPRO=space(10)
      .w_PNANNPRO=space(4)
      .w_SERIALE=space(10)
      .w_PNPRP=space(2)
      .w_NUMPRO=0
      .w_ALFPRO=space(10)
      .w_CODCAU=space(5)
      .w_PNTIPREG=space(1)
      .w_PNALFPRO=oParentObject.w_PNALFPRO
      .w_PNANNPRO=oParentObject.w_PNANNPRO
      .w_SERIALE=oParentObject.w_SERIALE
      .w_PNPRP=oParentObject.w_PNPRP
      .w_NUMPRO=oParentObject.w_NUMPRO
      .w_CODCAU=oParentObject.w_CODCAU
      .w_PNTIPREG=oParentObject.w_PNTIPREG
        .w_PNNUMPRO = .w_NUMPRO
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
          .DoRTCalc(2,6,.f.)
        .w_ALFPRO = .w_PNALFPRO
    endwith
    this.DoRTCalc(8,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PNALFPRO=.w_PNALFPRO
      .oParentObject.w_PNANNPRO=.w_PNANNPRO
      .oParentObject.w_SERIALE=.w_SERIALE
      .oParentObject.w_PNPRP=.w_PNPRP
      .oParentObject.w_NUMPRO=.w_NUMPRO
      .oParentObject.w_CODCAU=.w_CODCAU
      .oParentObject.w_PNTIPREG=.w_PNTIPREG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPNNUMPRO_1_1.value==this.w_PNNUMPRO)
      this.oPgFrm.Page1.oPag.oPNNUMPRO_1_1.value=this.w_PNNUMPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPNALFPRO_1_2.value==this.w_PNALFPRO)
      this.oPgFrm.Page1.oPag.oPNALFPRO_1_2.value=this.w_PNALFPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPRO_1_13.value==this.w_NUMPRO)
      this.oPgFrm.Page1.oPag.oNUMPRO_1_13.value=this.w_NUMPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oALFPRO_1_16.value==this.w_ALFPRO)
      this.oPgFrm.Page1.oPag.oALFPRO_1_16.value=this.w_ALFPRO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kppPag1 as StdContainer
  Width  = 384
  height = 129
  stdWidth  = 384
  stdheight = 129
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPNNUMPRO_1_1 as StdField with uid="CVZYUGGARN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PNNUMPRO", cQueryName = "PNNUMPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero protocollo da confermare",;
    HelpContextID = 62910907,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=164, Top=50, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oPNALFPRO_1_2 as StdField with uid="QPBKWRMFJI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PNALFPRO", cQueryName = "PNALFPRO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie protocollo da confermare",;
    HelpContextID = 70894011,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=294, Top=50, InputMask=replicate('X',10)


  add object oObj_1_7 as cp_runprogram with uid="CTSTFEMAJP",left=2, top=143, width=239,height=19,;
    caption='GSCG_BIP(I)',;
   bGlobalFont=.t.,;
    prg="GSCG_BIP('I',w_SERIALE)",;
    cEvent = "Update end",;
    nPag=1;
    , ToolTipText = "Inserimento numero protocollo";
    , HelpContextID = 257179190


  add object oObj_1_9 as cp_runprogram with uid="FDRBHBLJYW",left=2, top=168, width=241,height=19,;
    caption='GSCG_BIP(P)',;
   bGlobalFont=.t.,;
    prg="GSCG_BIP('P',w_SERIALE)",;
    cEvent = "w_PNALFPRO Changed",;
    nPag=1;
    , ToolTipText = "aggiorna progressivo protocollo";
    , HelpContextID = 257180982


  add object oBtn_1_11 as StdButton with uid="XNWYCGISAT",left=280, top=77, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 118901478;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_12 as StdButton with uid="JCFFQZETBZ",left=329, top=77, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 126190150;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNUMPRO_1_13 as StdField with uid="IAFNYOSSMR",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NUMPRO", cQueryName = "NUMPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero protocollo attuale",;
    HelpContextID = 74775338,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=164, Top=9, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oALFPRO_1_16 as StdField with uid="DCSEBKOHAW",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ALFPRO", cQueryName = "ALFPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie protocollo attuale",;
    HelpContextID = 74806522,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=294, Top=9, InputMask=replicate('X',10)

  add object oStr_1_3 as StdString with uid="OFJCSBIUDO",Visible=.t., Left=281, Top=53,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="MWNUUBEACK",Visible=.t., Left=15, Top=51,;
    Alignment=1, Width=147, Height=18,;
    Caption="Primo numero protocollo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="HULBKOMSUI",Visible=.t., Left=15, Top=11,;
    Alignment=1, Width=147, Height=18,;
    Caption="Numero protocollo attuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="MPEGKDLAKD",Visible=.t., Left=281, Top=12,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oBox_1_10 as StdBox with uid="HODWAEZIOL",left=2, top=36, width=379,height=4
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kpp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
