* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bel                                                        *
*              Controlli in eliminazione distinta                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_7]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2008-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bel",oParentObject,m.pOper)
return(i_retval)

define class tgsar_bel as StdBatch
  * --- Local variables
  pOper = space(1)
  w_CODART = space(20)
  w_MESS = space(20)
  w_QTATEST = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla se esistono dei Riferimenti alla Distinta o Kit da Eliminare in Anagrafica Articoli 
    *     (da GSDS_MDB e GSAR_MAK evento Delete Start)
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARCODART"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODDIS = "+cp_ToStrODBC(this.oParentObject.w_DBCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARCODART;
        from (i_cTable) where;
            ARCODDIS = this.oParentObject.w_DBCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODART = NVL(cp_ToDate(_read_.ARCODART),cp_NullValue(_read_.ARCODART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pOper="D"
        do case
          case this.oParentObject.w_DBDISKIT="D"
            if i_Rows>0
              this.w_MESS = ah_Msgformat("Codice distinta presente in %1 articoli; impossibile eliminare", ALLTRIM(STR(i_Rows)) )
              if i_Rows=1
                this.w_MESS = ah_Msgformat("Codice distinta presente nell' articolo: %1; impossibile eliminare", ALLTRIM(this.w_CODART) )
              endif
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MESS
            endif
          case this.oParentObject.w_DBDISKIT="K"
            if i_Rows>0
              this.w_MESS = ah_Msgformat("Kit presente in %1 articoli; impossibile eliminare", ALLTRIM(STR(i_Rows)) )
              if i_Rows=1
                this.w_MESS = ah_Msgformat("Kit presente nell' articolo: %1; impossibile eliminare", ALLTRIM(this.w_CODART) )
              endif
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MESS
            endif
          case this.oParentObject.w_DBDISKIT="I"
            if i_Rows>0
              this.w_MESS = ah_Msgformat("Kit Imballo presente in %1 articoli; impossibile eliminare", ALLTRIM(STR(i_Rows)) )
              if i_Rows=1
                this.w_MESS = ah_Msgformat("Kit imballo presente nell' articolo: %1; impossibile eliminare", ALLTRIM(this.w_CODART) )
              endif
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MESS
            endif
        endcase
      case this.pOper="Q"
        this.w_QTATEST = CALQTA(this.oParentObject.w_DBQTADIS,this.oParentObject.w_DBUNIMIS, this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, "", this.oParentObject.w_FLFRAZ1, this.oParentObject.w_MODUM2, this.oParentObject.w_FLUSEP, this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3, g_PERPQD)
        if this.oParentObject.w_FLFRAZ1="S" And this.oParentObject.w_MODUM2="S" And this.w_QTATEST<>INT(this.w_QTATEST)
          this.oParentObject.w_DBCOEUM1 = INT(this.w_QTATEST)+1
          * --- Ricalcolo la quantit� su riga in base alla nuova Qt� nella prima U.M.
          this.oParentObject.w_DBQTADIS = cp_Round((this.oParentObject.w_DBQTADIS*this.oParentObject.w_DBCOEUM1)/this.w_QTATEST,g_PERPQT)
          * --- Per evitare di nuovo il ricalcolo della Qt� nella prima U.M. dovuta al calculate sul campo nei documenti
          this.oParentObject.o_DBQTADIS = this.oParentObject.w_DBQTADIS
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ART_ICOL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
