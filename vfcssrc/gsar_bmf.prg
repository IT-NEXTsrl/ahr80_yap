* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bmf                                                        *
*              Updated modifiche famiglie                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-18                                                      *
* Last revis.: 2005-12-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bmf",oParentObject,m.pAR)
return(i_retval)

define class tgsar_bmf as StdBatch
  * --- Local variables
  pAR = space(3)
  Padre = .NULL.
  w_RIGAPIENA = 0
  w_MESS = space(30)
  * --- WorkFile variables
  GRUDATTR_idx=0
  FAM_ATTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Aggiornamento valori famiglie gruppi
    this.Padre = this.oParentObject
    do case
      case this.pAR="UPD"
        * --- Write into GRUDATTR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.GRUDATTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GRUDATTR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.GRUDATTR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GRMULTIP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FRMULTIP),'GRUDATTR','GRMULTIP');
          +",GROBBLIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FROBBLIG),'GRUDATTR','GROBBLIG');
              +i_ccchkf ;
          +" where ";
              +"GRFAMATT = "+cp_ToStrODBC(this.oParentObject.w_FRCODICE);
                 )
        else
          update (i_cTable) set;
              GRMULTIP = this.oParentObject.w_FRMULTIP;
              ,GROBBLIG = this.oParentObject.w_FROBBLIG;
              &i_ccchkf. ;
           where;
              GRFAMATT = this.oParentObject.w_FRCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.pAR="REF"
        * --- --Refresh per la picture
        this.Padre.mCalc(.T.)     
        this.Padre.oPgFrm.Page1.oPag.oBody.Refresh()     
        this.Padre.WorkFromTrs()     
        this.Padre.mCalcRowObjs()     
        this.Padre.SaveDependsOn()     
        this.Padre.SetControlsValue()     
        this.Padre.mHideControls()     
        this.Padre.ChildrenChangeRow()     
        this.Padre.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
        this.Padre.oPgFrm.Page1.oPag.oBody.nRelRow = 1
        this.Padre.NotifyEvent("Load")     
      case this.pAR="TIP"
        if this.oParentObject.w_FRINPUTA="C"
          this.oParentObject.w_FRDIMENS = 20
        else
          if this.oParentObject.w_FRINPUTA="N"
            this.oParentObject.w_FRDIMENS = 12
            this.oParentObject.w_FRNUMDEC = 3
          endif
        endif
      case this.pAR="CON"
        this.w_RIGAPIENA = 0
        Select(this.oParentObject.ctrsName) 
 go top
        SCAN FOR t_CPROWORD<>0
        COUNT FOR (!EMPTY(t_FRTIPCAR) OR t_FRTIPNUM<>0 OR !EMPTY(t_FRTIPDAT)) TO this.w_RIGAPIENA
        ENDSCAN
        * --- --controllo sulla riga piena
        if this.w_RIGAPIENA<1 AND this.oParentObject.w_FRTIPOLO<>"L"
          this.w_MESS = "Occore inserire almeno una riga nel dettaglio%0"
          ah_errormsg(this.w_MESS)
          this.oParentObject.w_RESCHK = -1
          i_retcode = 'stop'
          return
        else
          this.w_RIGAPIENA = 0
        endif
    endcase
  endproc


  proc Init(oParentObject,pAR)
    this.pAR=pAR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='GRUDATTR'
    this.cWorkTables[2]='FAM_ATTR'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAR"
endproc
