* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg1bpi                                                        *
*              Batch query calcolo plafond                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39][VRS_4]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-08                                                      *
* Last revis.: 2006-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ANNOEL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg1bpi",oParentObject,m.w_ANNOEL)
return(i_retval)

define class tgscg1bpi as StdBatch
  * --- Local variables
  w_ANNOEL = space(4)
  w_CODATT = space(4)
  w_SIMBOLO = space(5)
  w_DESATT = space(50)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch eseguito nella query GSCG4BST utilizzata nel calcolo totalizzatori IVa
    *     le variabili definite onn son altro che i parametri passati attraverso la query
    this.w_CODATT = g_CATAZI
    this.w_ANNOEL = IIF(Not Empty(this.w_ANNOEL),this.w_ANNOEL,alltrim(str(YEAR(i_datsys))))
    GSCG_BPI(this,"F")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,w_ANNOEL)
    this.w_ANNOEL=w_ANNOEL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ANNOEL"
endproc
