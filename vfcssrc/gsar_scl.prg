* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_scl                                                        *
*              Stampa clienti                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_35]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2014-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_scl",oParentObject))

* --- Class definition
define class tgsar_scl as StdForm
  Top    = 15
  Left   = 87

  * --- Standard Properties
  Width  = 574
  Height = 430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-18"
  HelpContextID=177637527
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  MASTRI_IDX = 0
  AGENTI_IDX = 0
  CACOCLFO_IDX = 0
  OUT_PUTS_IDX = 0
  ZONE_IDX = 0
  NAZIONI_IDX = 0
  ESERCIZI_IDX = 0
  CATECOMM_IDX = 0
  PAG_AMEN_IDX = 0
  cPrg = "gsar_scl"
  cComment = "Stampa clienti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_C = space(1)
  w_PARAM = space(1)
  w_NOMPRG = space(10)
  w_AZIENDA = space(10)
  w_PBANCODI = space(15)
  w_PBANCODI = space(15)
  w_PEANCODI = space(15)
  w_RAGSOCINI = space(40)
  w_RAGSOCFIN = space(40)
  w_PANCONSU = space(15)
  w_PEANCODI = space(15)
  w_RAGSOCINI = space(40)
  w_RAGSOCFIN = space(40)
  w_PANCONSU = space(15)
  w_PACODAG1 = space(5)
  w_CATCON = space(5)
  w_CATCON = space(5)
  w_PANCODZON = space(3)
  w_CATECOMM = space(3)
  w_PANNAZION = space(3)
  w_DESCR1 = space(40)
  w_DESCR2 = space(40)
  w_FLCODFIS = space(1)
  w_FLCODFIS = space(1)
  w_ANFLPRIV = space(1)
  w_FLPARIVA = space(10)
  w_FLPARIVA = space(10)
  w_FLNAZIO = space(1)
  w_PDATINIZ = ctod('  /  /  ')
  o_PDATINIZ = ctod('  /  /  ')
  w_FLMOVIM = space(1)
  w_obsodat1 = space(1)
  w_Associa = space(1)
  w_FLAPE = space(1)
  w_FLARCH = space(1)
  w_ORDERBY = space(50)
  w_DESAPP = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ONUME = 0
  o_ONUME = 0
  w_PACODAG2 = space(5)
  w_CODPAG = space(5)
  * --- Area Manuale = Declare Variables
  * --- gsar_scl
      * --- Per gestione sicurezza (a seconda del parametro)
      * --- gestisco e mostro all'utente un nome di maschera diverso..
      func getSecuritycode()
        return(lower(IIF( this.oParentObject='C', 'GSAR_SCL', 'GSAR_SFR' )))
      EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_sclPag1","gsar_scl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPBANCODI_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MASTRI'
    this.cWorkTables[3]='AGENTI'
    this.cWorkTables[4]='CACOCLFO'
    this.cWorkTables[5]='OUT_PUTS'
    this.cWorkTables[6]='ZONE'
    this.cWorkTables[7]='NAZIONI'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='CATECOMM'
    this.cWorkTables[10]='PAG_AMEN'
    return(this.OpenAllTables(10))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsar_scl
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_C=space(1)
      .w_PARAM=space(1)
      .w_NOMPRG=space(10)
      .w_AZIENDA=space(10)
      .w_PBANCODI=space(15)
      .w_PBANCODI=space(15)
      .w_PEANCODI=space(15)
      .w_RAGSOCINI=space(40)
      .w_RAGSOCFIN=space(40)
      .w_PANCONSU=space(15)
      .w_PEANCODI=space(15)
      .w_RAGSOCINI=space(40)
      .w_RAGSOCFIN=space(40)
      .w_PANCONSU=space(15)
      .w_PACODAG1=space(5)
      .w_CATCON=space(5)
      .w_CATCON=space(5)
      .w_PANCODZON=space(3)
      .w_CATECOMM=space(3)
      .w_PANNAZION=space(3)
      .w_DESCR1=space(40)
      .w_DESCR2=space(40)
      .w_FLCODFIS=space(1)
      .w_FLCODFIS=space(1)
      .w_ANFLPRIV=space(1)
      .w_FLPARIVA=space(10)
      .w_FLPARIVA=space(10)
      .w_FLNAZIO=space(1)
      .w_PDATINIZ=ctod("  /  /  ")
      .w_FLMOVIM=space(1)
      .w_obsodat1=space(1)
      .w_Associa=space(1)
      .w_FLAPE=space(1)
      .w_FLARCH=space(1)
      .w_ORDERBY=space(50)
      .w_DESAPP=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_ONUME=0
      .w_PACODAG2=space(5)
      .w_CODPAG=space(5)
        .w_C = this.oParentObject
        .w_PARAM = this.oParentObject
        .w_NOMPRG = IIF( .w_PARAM='C', 'GSAR_SCL', 'GSAR_SFR' )
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_PBANCODI))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_PBANCODI))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_PEANCODI))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_RAGSOCINI))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_RAGSOCFIN))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_PANCONSU))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_PEANCODI))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_RAGSOCINI))
          .link_1_12('Full')
        endif
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_RAGSOCFIN))
          .link_1_13('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_PANCONSU))
          .link_1_14('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_PACODAG1))
          .link_1_15('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CATCON))
          .link_1_16('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CATCON))
          .link_1_17('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_PANCODZON))
          .link_1_18('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CATECOMM))
          .link_1_19('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_PANNAZION))
          .link_1_20('Full')
        endif
          .DoRTCalc(21,23,.f.)
        .w_FLCODFIS = IIF(.w_ONUME=9,'A','T')
        .w_ANFLPRIV = ' '
        .w_FLPARIVA = IIF(.w_ONUME=9,'A','T')
          .DoRTCalc(27,27,.f.)
        .w_FLNAZIO = IIF( .w_ONUME=7 OR .w_ONUME=8,'I','T')
        .w_PDATINIZ = i_datsys
        .w_FLMOVIM = 'N'
        .w_obsodat1 = 'N'
        .w_Associa = 'T'
        .w_FLAPE = 'A'
        .w_FLARCH = 'T'
        .w_ORDERBY = ''
      .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
          .DoRTCalc(36,36,.f.)
        .w_OBTEST = .w_PDATINIZ
    endwith
    this.DoRTCalc(38,41,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_49.enabled = this.oPgFrm.Page1.oPag.oBtn_1_49.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,23,.t.)
        if .o_ONUME<>.w_ONUME
            .w_FLCODFIS = IIF(.w_ONUME=9,'A','T')
        endif
        .DoRTCalc(25,25,.t.)
        if .o_ONUME<>.w_ONUME
            .w_FLPARIVA = IIF(.w_ONUME=9,'A','T')
        endif
        .DoRTCalc(27,27,.t.)
        if .o_ONUME<>.w_ONUME
            .w_FLNAZIO = IIF( .w_ONUME=7 OR .w_ONUME=8,'I','T')
        endif
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .DoRTCalc(29,36,.t.)
        if .o_PDATINIZ<>.w_PDATINIZ
            .w_OBTEST = .w_PDATINIZ
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(38,41,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
    endwith
  return

  proc Calculate_FNISTDYISJ()
    with this
          * --- Valorizza orderby
          .w_ORDERBY = '2'
    endwith
  endproc
  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .cComment = AH_Msgformat(IIF( .w_PARAM = 'C' , "Stampa clienti" , "Stampa fornitori" ))
          .Caption = .cComment
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLCODFIS_1_28.enabled = this.oPgFrm.Page1.oPag.oFLCODFIS_1_28.mCond()
    this.oPgFrm.Page1.oPag.oFLCODFIS_1_29.enabled = this.oPgFrm.Page1.oPag.oFLCODFIS_1_29.mCond()
    this.oPgFrm.Page1.oPag.oFLPARIVA_1_31.enabled = this.oPgFrm.Page1.oPag.oFLPARIVA_1_31.mCond()
    this.oPgFrm.Page1.oPag.oFLPARIVA_1_32.enabled = this.oPgFrm.Page1.oPag.oFLPARIVA_1_32.mCond()
    this.oPgFrm.Page1.oPag.oFLNAZIO_1_33.enabled = this.oPgFrm.Page1.oPag.oFLNAZIO_1_33.mCond()
    this.oPgFrm.Page1.oPag.oFLAPE_1_39.enabled = this.oPgFrm.Page1.oPag.oFLAPE_1_39.mCond()
    this.oPgFrm.Page1.oPag.oFLARCH_1_40.enabled = this.oPgFrm.Page1.oPag.oFLARCH_1_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPBANCODI_1_5.visible=!this.oPgFrm.Page1.oPag.oPBANCODI_1_5.mHide()
    this.oPgFrm.Page1.oPag.oPBANCODI_1_6.visible=!this.oPgFrm.Page1.oPag.oPBANCODI_1_6.mHide()
    this.oPgFrm.Page1.oPag.oPEANCODI_1_7.visible=!this.oPgFrm.Page1.oPag.oPEANCODI_1_7.mHide()
    this.oPgFrm.Page1.oPag.oRAGSOCINI_1_8.visible=!this.oPgFrm.Page1.oPag.oRAGSOCINI_1_8.mHide()
    this.oPgFrm.Page1.oPag.oRAGSOCFIN_1_9.visible=!this.oPgFrm.Page1.oPag.oRAGSOCFIN_1_9.mHide()
    this.oPgFrm.Page1.oPag.oPANCONSU_1_10.visible=!this.oPgFrm.Page1.oPag.oPANCONSU_1_10.mHide()
    this.oPgFrm.Page1.oPag.oPEANCODI_1_11.visible=!this.oPgFrm.Page1.oPag.oPEANCODI_1_11.mHide()
    this.oPgFrm.Page1.oPag.oRAGSOCINI_1_12.visible=!this.oPgFrm.Page1.oPag.oRAGSOCINI_1_12.mHide()
    this.oPgFrm.Page1.oPag.oRAGSOCFIN_1_13.visible=!this.oPgFrm.Page1.oPag.oRAGSOCFIN_1_13.mHide()
    this.oPgFrm.Page1.oPag.oPANCONSU_1_14.visible=!this.oPgFrm.Page1.oPag.oPANCONSU_1_14.mHide()
    this.oPgFrm.Page1.oPag.oPACODAG1_1_15.visible=!this.oPgFrm.Page1.oPag.oPACODAG1_1_15.mHide()
    this.oPgFrm.Page1.oPag.oCATCON_1_16.visible=!this.oPgFrm.Page1.oPag.oCATCON_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCATCON_1_17.visible=!this.oPgFrm.Page1.oPag.oCATCON_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oFLCODFIS_1_28.visible=!this.oPgFrm.Page1.oPag.oFLCODFIS_1_28.mHide()
    this.oPgFrm.Page1.oPag.oFLCODFIS_1_29.visible=!this.oPgFrm.Page1.oPag.oFLCODFIS_1_29.mHide()
    this.oPgFrm.Page1.oPag.oANFLPRIV_1_30.visible=!this.oPgFrm.Page1.oPag.oANFLPRIV_1_30.mHide()
    this.oPgFrm.Page1.oPag.oFLPARIVA_1_31.visible=!this.oPgFrm.Page1.oPag.oFLPARIVA_1_31.mHide()
    this.oPgFrm.Page1.oPag.oFLPARIVA_1_32.visible=!this.oPgFrm.Page1.oPag.oFLPARIVA_1_32.mHide()
    this.oPgFrm.Page1.oPag.oFLNAZIO_1_33.visible=!this.oPgFrm.Page1.oPag.oFLNAZIO_1_33.mHide()
    this.oPgFrm.Page1.oPag.oAssocia_1_38.visible=!this.oPgFrm.Page1.oPag.oAssocia_1_38.mHide()
    this.oPgFrm.Page1.oPag.oFLAPE_1_39.visible=!this.oPgFrm.Page1.oPag.oFLAPE_1_39.mHide()
    this.oPgFrm.Page1.oPag.oFLARCH_1_40.visible=!this.oPgFrm.Page1.oPag.oFLARCH_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_FNISTDYISJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PBANCODI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANCODICE',trim(this.w_PBANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_C);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PBANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPBANCODI_1_5'),i_cWhere,'GSAR_BZC',"Clienti",'GSAR_SCL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PBANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANCODICE',this.w_PBANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PBANCODI = space(15)
      endif
      this.w_DESCR1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PBANCODI = space(15)
        this.w_DESCR1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PBANCODI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANCODICE',trim(this.w_PBANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_C);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PBANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPBANCODI_1_6'),i_cWhere,'GSAR_AFR',"Fornitori",'GSAR_SFR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PBANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANCODICE',this.w_PBANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PBANCODI = space(15)
      endif
      this.w_DESCR1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PBANCODI = space(15)
        this.w_DESCR1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PEANCODI
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PEANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANCODICE',trim(this.w_PEANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PEANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_C);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PEANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPEANCODI_1_7'),i_cWhere,'GSAR_AFR',"Fornitori",'GSAR_SFR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PEANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PEANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANCODICE',this.w_PEANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PEANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PEANCODI = space(15)
      endif
      this.w_DESCR2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBancodi))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PEANCODI = space(15)
        this.w_DESCR2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PEANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAGSOCINI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAGSOCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_RAGSOCINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANDESCRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANDESCRI',trim(this.w_RAGSOCINI))
          select ANTIPCON,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANDESCRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RAGSOCINI)==trim(_Link_.ANDESCRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RAGSOCINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(oSource.parent,'oRAGSOCINI_1_8'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANDESCRI',oSource.xKey(2))
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAGSOCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(this.w_RAGSOCINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANDESCRI',this.w_RAGSOCINI)
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAGSOCINI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RAGSOCINI = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_RAGSOCFIN)) OR  (.w_RAGSOCINI<=.w_RAGSOCFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta")
        endif
        this.w_RAGSOCINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANDESCRI,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAGSOCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAGSOCFIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAGSOCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_RAGSOCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANDESCRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANDESCRI',trim(this.w_RAGSOCFIN))
          select ANTIPCON,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANDESCRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RAGSOCFIN)==trim(_Link_.ANDESCRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RAGSOCFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(oSource.parent,'oRAGSOCFIN_1_9'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANDESCRI',oSource.xKey(2))
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAGSOCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(this.w_RAGSOCFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANDESCRI',this.w_RAGSOCFIN)
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAGSOCFIN = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RAGSOCFIN = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_RAGSOCFIN>=.w_RAGSOCINI) or (empty(.w_RAGSOCINI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta")
        endif
        this.w_RAGSOCFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANDESCRI,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAGSOCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCONSU
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCONSU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_PANCONSU))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANCONSU)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_PANCONSU)+"%");

            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PANCONSU) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPANCONSU_1_10'),i_cWhere,'GSAR_AMC',"Mastri",'GSAR_SFR.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCONSU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_PANCONSU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_PANCONSU)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCONSU = NVL(_Link_.MCCODICE,space(15))
      this.w_DESAPP = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PANCONSU = space(15)
      endif
      this.w_DESAPP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCONSU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PEANCODI
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PEANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANCODICE',trim(this.w_PEANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PEANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_C);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PEANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPEANCODI_1_11'),i_cWhere,'GSAR_BZC',"Clienti",'GSAR_SCL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PEANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PEANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANCODICE',this.w_PEANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PEANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PEANCODI = space(15)
      endif
      this.w_DESCR2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBancodi))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_PEANCODI = space(15)
        this.w_DESCR2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PEANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAGSOCINI
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAGSOCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_RAGSOCINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANDESCRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANDESCRI',trim(this.w_RAGSOCINI))
          select ANTIPCON,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANDESCRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RAGSOCINI)==trim(_Link_.ANDESCRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RAGSOCINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(oSource.parent,'oRAGSOCINI_1_12'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANDESCRI',oSource.xKey(2))
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAGSOCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(this.w_RAGSOCINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANDESCRI',this.w_RAGSOCINI)
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAGSOCINI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RAGSOCINI = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_RAGSOCFIN)) OR  (.w_RAGSOCINI<=.w_RAGSOCFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta")
        endif
        this.w_RAGSOCINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANDESCRI,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAGSOCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAGSOCFIN
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAGSOCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_RAGSOCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANDESCRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_C;
                     ,'ANDESCRI',trim(this.w_RAGSOCFIN))
          select ANTIPCON,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANDESCRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RAGSOCFIN)==trim(_Link_.ANDESCRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RAGSOCFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(oSource.parent,'oRAGSOCFIN_1_13'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_C<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANDESCRI',oSource.xKey(2))
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAGSOCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(this.w_RAGSOCFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_C);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_C;
                       ,'ANDESCRI',this.w_RAGSOCFIN)
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAGSOCFIN = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RAGSOCFIN = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_RAGSOCFIN>=.w_RAGSOCINI) or (empty(.w_RAGSOCINI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta")
        endif
        this.w_RAGSOCFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANDESCRI,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAGSOCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCONSU
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCONSU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_PANCONSU))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANCONSU)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_PANCONSU)+"%");

            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PANCONSU) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPANCONSU_1_14'),i_cWhere,'GSAR_AMC',"Mastri",'GSAR_SCL.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCONSU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_PANCONSU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_PANCONSU)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCONSU = NVL(_Link_.MCCODICE,space(15))
      this.w_DESAPP = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PANCONSU = space(15)
      endif
      this.w_DESAPP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCONSU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PACODAG1
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PACODAG1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_PACODAG1)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_PACODAG1))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PACODAG1)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_PACODAG1)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_PACODAG1)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PACODAG1) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oPACODAG1_1_15'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PACODAG1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PACODAG1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PACODAG1)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PACODAG1 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAPP = NVL(_Link_.AGDESAGE,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PACODAG1 = space(5)
      endif
      this.w_DESAPP = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente inesistente oppure obsoleto")
        endif
        this.w_PACODAG1 = space(5)
        this.w_DESAPP = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PACODAG1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_1_16'),i_cWhere,'GSAR_AC2',"Categorie contabili",'GSAR_SFR.CACOCLFO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_1_17'),i_cWhere,'GSAR_AC2',"Categorie contabili",'GSAR_SCL.CACOCLFO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCODZON
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_PANCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_PANCODZON))
          select ZOCODZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PANCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oPANCODZON_1_18'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_PANCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_PANCODZON)
            select ZOCODZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PANCODZON = space(3)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endif
        this.w_PANCODZON = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATECOMM
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATECOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATECOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATECOMM))
          select CTCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATECOMM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATECOMM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATECOMM_1_19'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATECOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATECOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATECOMM)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATECOMM = NVL(_Link_.CTCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CATECOMM = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATECOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANNAZION
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_PANNAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_PANNAZION))
          select NACODNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANNAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PANNAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oPANNAZION_1_20'),i_cWhere,'GSAR_ANZ',"Nazioni",'GSAR_SCL.NAZIONI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_PANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_PANNAZION)
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANNAZION = NVL(_Link_.NACODNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PANNAZION = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPBANCODI_1_5.value==this.w_PBANCODI)
      this.oPgFrm.Page1.oPag.oPBANCODI_1_5.value=this.w_PBANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPBANCODI_1_6.value==this.w_PBANCODI)
      this.oPgFrm.Page1.oPag.oPBANCODI_1_6.value=this.w_PBANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPEANCODI_1_7.value==this.w_PEANCODI)
      this.oPgFrm.Page1.oPag.oPEANCODI_1_7.value=this.w_PEANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOCINI_1_8.value==this.w_RAGSOCINI)
      this.oPgFrm.Page1.oPag.oRAGSOCINI_1_8.value=this.w_RAGSOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOCFIN_1_9.value==this.w_RAGSOCFIN)
      this.oPgFrm.Page1.oPag.oRAGSOCFIN_1_9.value=this.w_RAGSOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPANCONSU_1_10.value==this.w_PANCONSU)
      this.oPgFrm.Page1.oPag.oPANCONSU_1_10.value=this.w_PANCONSU
    endif
    if not(this.oPgFrm.Page1.oPag.oPEANCODI_1_11.value==this.w_PEANCODI)
      this.oPgFrm.Page1.oPag.oPEANCODI_1_11.value=this.w_PEANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOCINI_1_12.value==this.w_RAGSOCINI)
      this.oPgFrm.Page1.oPag.oRAGSOCINI_1_12.value=this.w_RAGSOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOCFIN_1_13.value==this.w_RAGSOCFIN)
      this.oPgFrm.Page1.oPag.oRAGSOCFIN_1_13.value=this.w_RAGSOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPANCONSU_1_14.value==this.w_PANCONSU)
      this.oPgFrm.Page1.oPag.oPANCONSU_1_14.value=this.w_PANCONSU
    endif
    if not(this.oPgFrm.Page1.oPag.oPACODAG1_1_15.value==this.w_PACODAG1)
      this.oPgFrm.Page1.oPag.oPACODAG1_1_15.value=this.w_PACODAG1
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCON_1_16.value==this.w_CATCON)
      this.oPgFrm.Page1.oPag.oCATCON_1_16.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCON_1_17.value==this.w_CATCON)
      this.oPgFrm.Page1.oPag.oCATCON_1_17.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oPANCODZON_1_18.value==this.w_PANCODZON)
      this.oPgFrm.Page1.oPag.oPANCODZON_1_18.value=this.w_PANCODZON
    endif
    if not(this.oPgFrm.Page1.oPag.oCATECOMM_1_19.value==this.w_CATECOMM)
      this.oPgFrm.Page1.oPag.oCATECOMM_1_19.value=this.w_CATECOMM
    endif
    if not(this.oPgFrm.Page1.oPag.oPANNAZION_1_20.value==this.w_PANNAZION)
      this.oPgFrm.Page1.oPag.oPANNAZION_1_20.value=this.w_PANNAZION
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR1_1_25.value==this.w_DESCR1)
      this.oPgFrm.Page1.oPag.oDESCR1_1_25.value=this.w_DESCR1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR2_1_26.value==this.w_DESCR2)
      this.oPgFrm.Page1.oPag.oDESCR2_1_26.value=this.w_DESCR2
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCODFIS_1_28.RadioValue()==this.w_FLCODFIS)
      this.oPgFrm.Page1.oPag.oFLCODFIS_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCODFIS_1_29.RadioValue()==this.w_FLCODFIS)
      this.oPgFrm.Page1.oPag.oFLCODFIS_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANFLPRIV_1_30.RadioValue()==this.w_ANFLPRIV)
      this.oPgFrm.Page1.oPag.oANFLPRIV_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPARIVA_1_31.RadioValue()==this.w_FLPARIVA)
      this.oPgFrm.Page1.oPag.oFLPARIVA_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPARIVA_1_32.RadioValue()==this.w_FLPARIVA)
      this.oPgFrm.Page1.oPag.oFLPARIVA_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNAZIO_1_33.RadioValue()==this.w_FLNAZIO)
      this.oPgFrm.Page1.oPag.oFLNAZIO_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDATINIZ_1_34.value==this.w_PDATINIZ)
      this.oPgFrm.Page1.oPag.oPDATINIZ_1_34.value=this.w_PDATINIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMOVIM_1_35.RadioValue()==this.w_FLMOVIM)
      this.oPgFrm.Page1.oPag.oFLMOVIM_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oobsodat1_1_37.RadioValue()==this.w_obsodat1)
      this.oPgFrm.Page1.oPag.oobsodat1_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAssocia_1_38.RadioValue()==this.w_Associa)
      this.oPgFrm.Page1.oPag.oAssocia_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAPE_1_39.RadioValue()==this.w_FLAPE)
      this.oPgFrm.Page1.oPag.oFLAPE_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLARCH_1_40.RadioValue()==this.w_FLARCH)
      this.oPgFrm.Page1.oPag.oFLARCH_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oORDERBY_1_42.RadioValue()==this.w_ORDERBY)
      this.oPgFrm.Page1.oPag.oORDERBY_1_42.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PARAM<> 'C')  and not(empty(.w_PBANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBANCODI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PARAM= 'C')  and not(empty(.w_PBANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBANCODI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBancodi))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PARAM= 'C')  and not(empty(.w_PEANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEANCODI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((empty(.w_RAGSOCFIN)) OR  (.w_RAGSOCINI<=.w_RAGSOCFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PARAM='C')  and not(empty(.w_RAGSOCINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRAGSOCINI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta")
          case   not(((.w_RAGSOCFIN>=.w_RAGSOCINI) or (empty(.w_RAGSOCINI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PARAM='C')  and not(empty(.w_RAGSOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRAGSOCFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta")
          case   not(((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBancodi))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PARAM<> 'C')  and not(empty(.w_PEANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEANCODI_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((empty(.w_RAGSOCFIN)) OR  (.w_RAGSOCINI<=.w_RAGSOCFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PARAM<>'C')  and not(empty(.w_RAGSOCINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRAGSOCINI_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta")
          case   not(((.w_RAGSOCFIN>=.w_RAGSOCINI) or (empty(.w_RAGSOCINI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PARAM<>'C')  and not(empty(.w_RAGSOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRAGSOCFIN_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(.w_PARAM<> 'C' OR IsAlt())  and not(empty(.w_PACODAG1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPACODAG1_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice agente inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PANCODZON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPANCODZON_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleto")
          case   (empty(.w_PDATINIZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDATINIZ_1_34.SetFocus()
            i_bnoObbl = !empty(.w_PDATINIZ)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PDATINIZ = this.w_PDATINIZ
    this.o_ONUME = this.w_ONUME
    return

enddefine

* --- Define pages as container
define class tgsar_sclPag1 as StdContainer
  Width  = 570
  height = 430
  stdWidth  = 570
  stdheight = 430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPBANCODI_1_5 as StdField with uid="DUNCZIVMRQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PBANCODI", cQueryName = "PBANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Cliente di inizio selezione",;
    HelpContextID = 31924161,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=175, Top=8, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANCODICE", oKey_2_2="this.w_PBANCODI"

  func oPBANCODI_1_5.mHide()
    with this.Parent.oContained
      return (.w_PARAM<> 'C')
    endwith
  endfunc

  func oPBANCODI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBANCODI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBANCODI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPBANCODI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'GSAR_SCL.CONTI_VZM',this.parent.oContained
  endproc
  proc oPBANCODI_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANCODICE=this.parent.oContained.w_PBANCODI
     i_obj.ecpSave()
  endproc

  add object oPBANCODI_1_6 as StdField with uid="EREIEYSTVR",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PBANCODI", cQueryName = "PBANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Fornitore di inizio selezione",;
    HelpContextID = 31924161,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=175, Top=8, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANCODICE", oKey_2_2="this.w_PBANCODI"

  func oPBANCODI_1_6.mHide()
    with this.Parent.oContained
      return (.w_PARAM= 'C')
    endwith
  endfunc

  func oPBANCODI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBANCODI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBANCODI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPBANCODI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'GSAR_SFR.CONTI_VZM',this.parent.oContained
  endproc
  proc oPBANCODI_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANCODICE=this.parent.oContained.w_PBANCODI
     i_obj.ecpSave()
  endproc

  add object oPEANCODI_1_7 as StdField with uid="JKYOWQANMA",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PEANCODI", cQueryName = "PEANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Fornitore di fine selezione",;
    HelpContextID = 31923393,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=175, Top=33, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANCODICE", oKey_2_2="this.w_PEANCODI"

  func oPEANCODI_1_7.mHide()
    with this.Parent.oContained
      return (.w_PARAM= 'C')
    endwith
  endfunc

  func oPEANCODI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPEANCODI_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPEANCODI_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPEANCODI_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'GSAR_SFR.CONTI_VZM',this.parent.oContained
  endproc
  proc oPEANCODI_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANCODICE=this.parent.oContained.w_PEANCODI
     i_obj.ecpSave()
  endproc

  add object oRAGSOCINI_1_8 as StdField with uid="OLEWYOTYHB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_RAGSOCINI", cQueryName = "RAGSOCINI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta",;
    ToolTipText = "Ragione sociale di inizio selezione",;
    HelpContextID = 48120820,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=175, Top=58, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANDESCRI", oKey_2_2="this.w_RAGSOCINI"

  func oRAGSOCINI_1_8.mHide()
    with this.Parent.oContained
      return (.w_PARAM='C')
    endwith
  endfunc

  func oRAGSOCINI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oRAGSOCINI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRAGSOCINI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(this.parent,'oRAGSOCINI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oRAGSOCINI_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANDESCRI=this.parent.oContained.w_RAGSOCINI
     i_obj.ecpSave()
  endproc

  add object oRAGSOCFIN_1_9 as StdField with uid="NVPEZTTXPS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_RAGSOCFIN", cQueryName = "RAGSOCFIN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta",;
    ToolTipText = "Ragione sociale di fine selezione",;
    HelpContextID = 220314561,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=175, Top=82, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANDESCRI", oKey_2_2="this.w_RAGSOCFIN"

  func oRAGSOCFIN_1_9.mHide()
    with this.Parent.oContained
      return (.w_PARAM='C')
    endwith
  endfunc

  func oRAGSOCFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oRAGSOCFIN_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRAGSOCFIN_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(this.parent,'oRAGSOCFIN_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oRAGSOCFIN_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANDESCRI=this.parent.oContained.w_RAGSOCFIN
     i_obj.ecpSave()
  endproc

  add object oPANCONSU_1_10 as StdField with uid="EXMJWATBSW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PANCONSU", cQueryName = "PANCONSU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro selezionato",;
    HelpContextID = 36786357,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=176, Top=108, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_PANCONSU"

  func oPANCONSU_1_10.mHide()
    with this.Parent.oContained
      return (.w_PARAM='C')
    endwith
  endfunc

  func oPANCONSU_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANCONSU_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANCONSU_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPANCONSU_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'GSAR_SFR.MASTRI_VZM',this.parent.oContained
  endproc
  proc oPANCONSU_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_PANCONSU
     i_obj.ecpSave()
  endproc

  add object oPEANCODI_1_11 as StdField with uid="STHPWYOZXF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PEANCODI", cQueryName = "PEANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Cliente di fine selezione",;
    HelpContextID = 31923393,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=175, Top=33, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANCODICE", oKey_2_2="this.w_PEANCODI"

  func oPEANCODI_1_11.mHide()
    with this.Parent.oContained
      return (.w_PARAM<> 'C')
    endwith
  endfunc

  func oPEANCODI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPEANCODI_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPEANCODI_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPEANCODI_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'GSAR_SCL.CONTI_VZM',this.parent.oContained
  endproc
  proc oPEANCODI_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANCODICE=this.parent.oContained.w_PEANCODI
     i_obj.ecpSave()
  endproc

  add object oRAGSOCINI_1_12 as StdField with uid="VKPUFKLWYU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_RAGSOCINI", cQueryName = "RAGSOCINI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta",;
    ToolTipText = "Ragione sociale di inizio selezione",;
    HelpContextID = 48120820,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=175, Top=58, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANDESCRI", oKey_2_2="this.w_RAGSOCINI"

  func oRAGSOCINI_1_12.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'C')
    endwith
  endfunc

  func oRAGSOCINI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oRAGSOCINI_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRAGSOCINI_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(this.parent,'oRAGSOCINI_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oRAGSOCINI_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANDESCRI=this.parent.oContained.w_RAGSOCINI
     i_obj.ecpSave()
  endproc

  add object oRAGSOCFIN_1_13 as StdField with uid="OSOHGLOZBC",rtseq=13,rtrep=.f.,;
    cFormVar = "w_RAGSOCFIN", cQueryName = "RAGSOCFIN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "La rag.sociale iniziale � pi� grande di quella finale oppure obsoleta",;
    ToolTipText = "Ragione sociale di fine selezione",;
    HelpContextID = 220314561,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=175, Top=83, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_C", oKey_2_1="ANDESCRI", oKey_2_2="this.w_RAGSOCFIN"

  func oRAGSOCFIN_1_13.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'C')
    endwith
  endfunc

  func oRAGSOCFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oRAGSOCFIN_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRAGSOCFIN_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_C)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_C)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(this.parent,'oRAGSOCFIN_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oRAGSOCFIN_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_C
     i_obj.w_ANDESCRI=this.parent.oContained.w_RAGSOCFIN
     i_obj.ecpSave()
  endproc

  add object oPANCONSU_1_14 as StdField with uid="UCPMPLCJTH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PANCONSU", cQueryName = "PANCONSU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento selezionato",;
    HelpContextID = 36786357,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=175, Top=108, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_PANCONSU"

  func oPANCONSU_1_14.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'C')
    endwith
  endfunc

  func oPANCONSU_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANCONSU_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANCONSU_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPANCONSU_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'GSAR_SCL.MASTRI_VZM',this.parent.oContained
  endproc
  proc oPANCONSU_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_PANCONSU
     i_obj.ecpSave()
  endproc

  add object oPACODAG1_1_15 as StdField with uid="MVICQGKLIN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PACODAG1", cQueryName = "PACODAG1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente inesistente oppure obsoleto",;
    ToolTipText = "Agente selezionato",;
    HelpContextID = 265683161,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=446, Top=108, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_PACODAG1"

  func oPACODAG1_1_15.mHide()
    with this.Parent.oContained
      return (.w_PARAM<> 'C' OR IsAlt())
    endwith
  endfunc

  func oPACODAG1_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oPACODAG1_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPACODAG1_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oPACODAG1_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oPACODAG1_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_PACODAG1
     i_obj.ecpSave()
  endproc

  add object oCATCON_1_16 as StdField with uid="QHNOVRYKXP",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contabile selezionata",;
    HelpContextID = 231673382,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=176, Top=133, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_1_16.mHide()
    with this.Parent.oContained
      return (.w_PARAM='C')
    endwith
  endfunc

  func oCATCON_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili",'GSAR_SFR.CACOCLFO_VZM',this.parent.oContained
  endproc
  proc oCATCON_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_CATCON
     i_obj.ecpSave()
  endproc

  add object oCATCON_1_17 as StdField with uid="YJNVKXSMLY",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contabile selezionata",;
    HelpContextID = 231673382,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=175, Top=134, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_1_17.mHide()
    with this.Parent.oContained
      return (.w_PARAM<>'C')
    endwith
  endfunc

  func oCATCON_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili",'GSAR_SCL.CACOCLFO_VZM',this.parent.oContained
  endproc
  proc oCATCON_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_CATCON
     i_obj.ecpSave()
  endproc

  add object oPANCODZON_1_18 as StdField with uid="INSDLAWTRZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PANCODZON", cQueryName = "PANCODZON",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleto",;
    ToolTipText = "Zona selezionata",;
    HelpContextID = 63878181,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=446, Top=134, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_PANCODZON"

  func oPANCODZON_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANCODZON_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANCODZON_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oPANCODZON_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oPANCODZON_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_PANCODZON
     i_obj.ecpSave()
  endproc

  add object oCATECOMM_1_19 as StdField with uid="FNNUKUNBXH",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CATECOMM", cQueryName = "CATECOMM",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale selezionata",;
    HelpContextID = 235998835,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=175, Top=159, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATECOMM"

  func oCATECOMM_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATECOMM_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATECOMM_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATECOMM_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCATECOMM_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATECOMM
     i_obj.ecpSave()
  endproc

  add object oPANNAZION_1_20 as StdField with uid="AGVBJQHMYP",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PANNAZION", cQueryName = "PANNAZION",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Nazione selezionata",;
    HelpContextID = 150582309,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=446, Top=159, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_PANNAZION"

  func oPANNAZION_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANNAZION_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANNAZION_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oPANNAZION_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'GSAR_SCL.NAZIONI_VZM',this.parent.oContained
  endproc
  proc oPANNAZION_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_PANNAZION
     i_obj.ecpSave()
  endproc

  add object oDESCR1_1_25 as StdField with uid="MELWPUDIJJ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCR1", cQueryName = "DESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 16712246,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=318, Top=8, InputMask=replicate('X',40)

  add object oDESCR2_1_26 as StdField with uid="KYHNAZAQJF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCR2", cQueryName = "DESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 33489462,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=318, Top=33, InputMask=replicate('X',40)


  add object oFLCODFIS_1_28 as StdCombo with uid="MVYDOXULTJ",rtseq=23,rtrep=.f.,left=175,top=186,width=139,height=21;
    , ToolTipText = "Codice fiscale";
    , HelpContextID = 86641065;
    , cFormVar="w_FLCODFIS",RowSource=""+"Assente,"+"Valorizzato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLCODFIS_1_28.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'V',;
    space(1))))
  endfunc
  func oFLCODFIS_1_28.GetRadio()
    this.Parent.oContained.w_FLCODFIS = this.RadioValue()
    return .t.
  endfunc

  func oFLCODFIS_1_28.SetRadio()
    this.Parent.oContained.w_FLCODFIS=trim(this.Parent.oContained.w_FLCODFIS)
    this.value = ;
      iif(this.Parent.oContained.w_FLCODFIS=='A',1,;
      iif(this.Parent.oContained.w_FLCODFIS=='V',2,;
      0))
  endfunc

  func oFLCODFIS_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME=9)
    endwith
   endif
  endfunc

  func oFLCODFIS_1_28.mHide()
    with this.Parent.oContained
      return (.w_ONUME<>9)
    endwith
  endfunc


  add object oFLCODFIS_1_29 as StdCombo with uid="MTPIJNUBGJ",rtseq=24,rtrep=.f.,left=175,top=186,width=139,height=21;
    , ToolTipText = "Codice fiscale";
    , HelpContextID = 86641065;
    , cFormVar="w_FLCODFIS",RowSource=""+"Assente,"+"Valorizzato,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLCODFIS_1_29.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'V',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLCODFIS_1_29.GetRadio()
    this.Parent.oContained.w_FLCODFIS = this.RadioValue()
    return .t.
  endfunc

  func oFLCODFIS_1_29.SetRadio()
    this.Parent.oContained.w_FLCODFIS=trim(this.Parent.oContained.w_FLCODFIS)
    this.value = ;
      iif(this.Parent.oContained.w_FLCODFIS=='A',1,;
      iif(this.Parent.oContained.w_FLCODFIS=='V',2,;
      iif(this.Parent.oContained.w_FLCODFIS=='T',3,;
      0)))
  endfunc

  func oFLCODFIS_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME<>9)
    endwith
   endif
  endfunc

  func oFLCODFIS_1_29.mHide()
    with this.Parent.oContained
      return (.w_ONUME=9)
    endwith
  endfunc


  add object oANFLPRIV_1_30 as StdCombo with uid="EVWGYPKRQR",value=3,rtseq=25,rtrep=.f.,left=446,top=186,width=118,height=21;
    , ToolTipText = "Stampa solo i clienti privati";
    , HelpContextID = 31931228;
    , cFormVar="w_ANFLPRIV",RowSource=""+"Attivo,"+"Disattivo,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANFLPRIV_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oANFLPRIV_1_30.GetRadio()
    this.Parent.oContained.w_ANFLPRIV = this.RadioValue()
    return .t.
  endfunc

  func oANFLPRIV_1_30.SetRadio()
    this.Parent.oContained.w_ANFLPRIV=trim(this.Parent.oContained.w_ANFLPRIV)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLPRIV=='S',1,;
      iif(this.Parent.oContained.w_ANFLPRIV=='N',2,;
      iif(this.Parent.oContained.w_ANFLPRIV=='',3,;
      0)))
  endfunc

  func oANFLPRIV_1_30.mHide()
    with this.Parent.oContained
      return (.w_PARAM<> 'C')
    endwith
  endfunc


  add object oFLPARIVA_1_31 as StdCombo with uid="UNLKVOATIT",rtseq=26,rtrep=.f.,left=175,top=217,width=139,height=21;
    , ToolTipText = "Partita IVA";
    , HelpContextID = 117646953;
    , cFormVar="w_FLPARIVA",RowSource=""+"Assente,"+"Valorizzata,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLPARIVA_1_31.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'V',;
    iif(this.value =3,'T',;
    space(10)))))
  endfunc
  func oFLPARIVA_1_31.GetRadio()
    this.Parent.oContained.w_FLPARIVA = this.RadioValue()
    return .t.
  endfunc

  func oFLPARIVA_1_31.SetRadio()
    this.Parent.oContained.w_FLPARIVA=trim(this.Parent.oContained.w_FLPARIVA)
    this.value = ;
      iif(this.Parent.oContained.w_FLPARIVA=='A',1,;
      iif(this.Parent.oContained.w_FLPARIVA=='V',2,;
      iif(this.Parent.oContained.w_FLPARIVA=='T',3,;
      0)))
  endfunc

  func oFLPARIVA_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME<>9)
    endwith
   endif
  endfunc

  func oFLPARIVA_1_31.mHide()
    with this.Parent.oContained
      return (.w_ONUME=9)
    endwith
  endfunc


  add object oFLPARIVA_1_32 as StdCombo with uid="CKKOUKUPNX",rtseq=27,rtrep=.f.,left=175,top=217,width=139,height=21;
    , ToolTipText = "Partita IVA";
    , HelpContextID = 117646953;
    , cFormVar="w_FLPARIVA",RowSource=""+"Assente,"+"Valorizzata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLPARIVA_1_32.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'V',;
    space(10))))
  endfunc
  func oFLPARIVA_1_32.GetRadio()
    this.Parent.oContained.w_FLPARIVA = this.RadioValue()
    return .t.
  endfunc

  func oFLPARIVA_1_32.SetRadio()
    this.Parent.oContained.w_FLPARIVA=trim(this.Parent.oContained.w_FLPARIVA)
    this.value = ;
      iif(this.Parent.oContained.w_FLPARIVA=='A',1,;
      iif(this.Parent.oContained.w_FLPARIVA=='V',2,;
      0))
  endfunc

  func oFLPARIVA_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME=9)
    endwith
   endif
  endfunc

  func oFLPARIVA_1_32.mHide()
    with this.Parent.oContained
      return (.w_ONUME<>9)
    endwith
  endfunc


  add object oFLNAZIO_1_33 as StdCombo with uid="MCTGONJCOH",rtseq=28,rtrep=.f.,left=446,top=217,width=118,height=21;
    , ToolTipText = "Provenienza";
    , HelpContextID = 159168854;
    , cFormVar="w_FLNAZIO",RowSource=""+"Nazionali,"+"Esteri,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLNAZIO_1_33.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLNAZIO_1_33.GetRadio()
    this.Parent.oContained.w_FLNAZIO = this.RadioValue()
    return .t.
  endfunc

  func oFLNAZIO_1_33.SetRadio()
    this.Parent.oContained.w_FLNAZIO=trim(this.Parent.oContained.w_FLNAZIO)
    this.value = ;
      iif(this.Parent.oContained.w_FLNAZIO=='I',1,;
      iif(this.Parent.oContained.w_FLNAZIO=='E',2,;
      iif(this.Parent.oContained.w_FLNAZIO=='T',3,;
      0)))
  endfunc

  func oFLNAZIO_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME=7  OR .w_ONUME=8)
    endwith
   endif
  endfunc

  func oFLNAZIO_1_33.mHide()
    with this.Parent.oContained
      return (!(.w_ONUME=7 OR .w_ONUME=8))
    endwith
  endfunc

  add object oPDATINIZ_1_34 as StdField with uid="KXZLKOTRZF",rtseq=29,rtrep=.f.,;
    cFormVar = "w_PDATINIZ", cQueryName = "PDATINIZ",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 226419280,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=175, Top=249

  add object oFLMOVIM_1_35 as StdCheck with uid="NKVCMYZLTP",rtseq=30,rtrep=.f.,left=354, top=246, caption="Solo movimentati",;
    ToolTipText = "Solo movimentati nell'esercizio corrente",;
    HelpContextID = 155887958,;
    cFormVar="w_FLMOVIM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLMOVIM_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLMOVIM_1_35.GetRadio()
    this.Parent.oContained.w_FLMOVIM = this.RadioValue()
    return .t.
  endfunc

  func oFLMOVIM_1_35.SetRadio()
    this.Parent.oContained.w_FLMOVIM=trim(this.Parent.oContained.w_FLMOVIM)
    this.value = ;
      iif(this.Parent.oContained.w_FLMOVIM=='S',1,;
      0)
  endfunc

  add object oobsodat1_1_37 as StdCheck with uid="TANZHKUOWP",rtseq=31,rtrep=.f.,left=354, top=267, caption="Solo obsoleti",;
    ToolTipText = "Stampa solo obsoleti",;
    HelpContextID = 229826025,;
    cFormVar="w_obsodat1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oobsodat1_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oobsodat1_1_37.GetRadio()
    this.Parent.oContained.w_obsodat1 = this.RadioValue()
    return .t.
  endfunc

  func oobsodat1_1_37.SetRadio()
    this.Parent.oContained.w_obsodat1=trim(this.Parent.oContained.w_obsodat1)
    this.value = ;
      iif(this.Parent.oContained.w_obsodat1=='S',1,;
      0)
  endfunc

  add object oAssocia_1_38 as StdRadio with uid="NVECKGBUVY",rtseq=32,rtrep=.f.,left=19, top=290, width=346,height=23;
    , ToolTipText = "Selezionare il tipo di associazione";
    , cFormVar="w_Associa", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oAssocia_1_38.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Tutti"
      this.Buttons(1).HelpContextID = 96653306
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Non associati a pratiche"
      this.Buttons(2).HelpContextID = 96653306
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Non associati a pratiche","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Associati a pratiche"
      this.Buttons(3).HelpContextID = 96653306
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Associati a pratiche","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Selezionare il tipo di associazione")
      StdRadio::init()
    endproc

  func oAssocia_1_38.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oAssocia_1_38.GetRadio()
    this.Parent.oContained.w_Associa = this.RadioValue()
    return .t.
  endfunc

  func oAssocia_1_38.SetRadio()
    this.Parent.oContained.w_Associa=trim(this.Parent.oContained.w_Associa)
    this.value = ;
      iif(this.Parent.oContained.w_Associa=='T',1,;
      iif(this.Parent.oContained.w_Associa=='N',2,;
      iif(this.Parent.oContained.w_Associa=='A',3,;
      0)))
  endfunc

  func oAssocia_1_38.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR .w_ONUME<>19)
    endwith
  endfunc


  add object oFLAPE_1_39 as StdCombo with uid="FFVUYEGYZW",rtseq=33,rtrep=.f.,left=351,top=292,width=104,height=21;
    , ToolTipText = "Selezione su apertura/chiusura pratica";
    , HelpContextID = 255519062;
    , cFormVar="w_FLAPE",RowSource=""+"Tutte,"+"Aperte,"+"Chiuse", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLAPE_1_39.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oFLAPE_1_39.GetRadio()
    this.Parent.oContained.w_FLAPE = this.RadioValue()
    return .t.
  endfunc

  func oFLAPE_1_39.SetRadio()
    this.Parent.oContained.w_FLAPE=trim(this.Parent.oContained.w_FLAPE)
    this.value = ;
      iif(this.Parent.oContained.w_FLAPE=='T',1,;
      iif(this.Parent.oContained.w_FLAPE=='A',2,;
      iif(this.Parent.oContained.w_FLAPE=='C',3,;
      0)))
  endfunc

  func oFLAPE_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Associa='A')
    endwith
   endif
  endfunc

  func oFLAPE_1_39.mHide()
    with this.Parent.oContained
      return (.w_Associa<>'A' OR !IsAlt() OR .w_ONUME<>19)
    endwith
  endfunc


  add object oFLARCH_1_40 as StdCombo with uid="ACDUZJFCZT",rtseq=34,rtrep=.f.,left=460,top=292,width=104,height=21;
    , ToolTipText = "Selezione su archiviazione pratica";
    , HelpContextID = 119335254;
    , cFormVar="w_FLARCH",RowSource=""+"Archiviate,"+"Non archiviate,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLARCH_1_40.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLARCH_1_40.GetRadio()
    this.Parent.oContained.w_FLARCH = this.RadioValue()
    return .t.
  endfunc

  func oFLARCH_1_40.SetRadio()
    this.Parent.oContained.w_FLARCH=trim(this.Parent.oContained.w_FLARCH)
    this.value = ;
      iif(this.Parent.oContained.w_FLARCH=='A',1,;
      iif(this.Parent.oContained.w_FLARCH=='N',2,;
      iif(this.Parent.oContained.w_FLARCH=='T',3,;
      0)))
  endfunc

  func oFLARCH_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Associa='A' AND .w_FLAPE='C')
    endwith
   endif
  endfunc

  func oFLARCH_1_40.mHide()
    with this.Parent.oContained
      return (.w_Associa<>'A' OR .w_FLAPE<>'C' OR !IsAlt() OR .w_ONUME<>19)
    endwith
  endfunc


  add object oORDERBY_1_42 as StdCombo with uid="AGMOBTFLHD",rtseq=35,rtrep=.f.,left=175,top=321,width=169,height=21;
    , ToolTipText = "Ordinamento";
    , HelpContextID = 33562598;
    , cFormVar="w_ORDERBY",RowSource=""+"Codice,"+"Ragione sociale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oORDERBY_1_42.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    space(50))))
  endfunc
  func oORDERBY_1_42.GetRadio()
    this.Parent.oContained.w_ORDERBY = this.RadioValue()
    return .t.
  endfunc

  func oORDERBY_1_42.SetRadio()
    this.Parent.oContained.w_ORDERBY=trim(this.Parent.oContained.w_ORDERBY)
    this.value = ;
      iif(this.Parent.oContained.w_ORDERBY=='1',1,;
      iif(this.Parent.oContained.w_ORDERBY=='2',2,;
      0))
  endfunc


  add object oObj_1_46 as cp_outputCombo with uid="ZLERXIVPWT",left=175, top=351, width=375,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cDefSep="",cNoDefSep="",;
    nPag=1;
    , HelpContextID = 181235738


  add object oBtn_1_47 as StdButton with uid="VCZMCHUPGB",left=463, top=379, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 217443802;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY) and not empty(.w_pdatiniz))
      endwith
    endif
  endfunc


  add object oBtn_1_49 as StdButton with uid="EFNLDLJGWV",left=516, top=379, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 184954950;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_49.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_21 as StdString with uid="ELGVYCEQRO",Visible=.t., Left=9, Top=251,;
    Alignment=1, Width=163, Height=18,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="AVLXLURTLG",Visible=.t., Left=9, Top=8,;
    Alignment=1, Width=163, Height=15,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_PARAM<> 'C')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="BHVQXTHZIC",Visible=.t., Left=9, Top=33,;
    Alignment=1, Width=163, Height=15,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_PARAM<> 'C')
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="BCSBYALYOG",Visible=.t., Left=324, Top=108,;
    Alignment=1, Width=114, Height=15,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_PARAM<> 'C' OR IsAlt())
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="EUIDGZENRM",Visible=.t., Left=9, Top=351,;
    Alignment=1, Width=163, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="HGBTOEMDXR",Visible=.t., Left=9, Top=134,;
    Alignment=1, Width=163, Height=15,;
    Caption="Cat.contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="ZDUYAETGGJ",Visible=.t., Left=9, Top=108,;
    Alignment=1, Width=163, Height=15,;
    Caption="Mastro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="RDJAYBHXHJ",Visible=.t., Left=324, Top=134,;
    Alignment=1, Width=114, Height=15,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="BPGJVPQSEI",Visible=.t., Left=324, Top=159,;
    Alignment=1, Width=114, Height=15,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="IBNXRMOAOU",Visible=.t., Left=9, Top=61,;
    Alignment=1, Width=163, Height=18,;
    Caption="Da rag.sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="QAKHWAGHPC",Visible=.t., Left=9, Top=88,;
    Alignment=1, Width=163, Height=18,;
    Caption="A rag.sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="PKWGDEMLRM",Visible=.t., Left=9, Top=159,;
    Alignment=1, Width=163, Height=18,;
    Caption="Cat.commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="UTMMWMNISY",Visible=.t., Left=9, Top=189,;
    Alignment=1, Width=163, Height=19,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="MZCSWAZFDA",Visible=.t., Left=9, Top=220,;
    Alignment=1, Width=163, Height=19,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="ZZDXICYDGH",Visible=.t., Left=67, Top=324,;
    Alignment=1, Width=105, Height=19,;
    Caption="Ordina per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="INAJZDWJCP",Visible=.t., Left=324, Top=189,;
    Alignment=1, Width=114, Height=19,;
    Caption="Cliente privato:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (.w_PARAM<> 'C')
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="UBGSVPMQZO",Visible=.t., Left=9, Top=8,;
    Alignment=1, Width=163, Height=15,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (.w_PARAM= 'C')
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="RPGZMGOKGV",Visible=.t., Left=9, Top=31,;
    Alignment=1, Width=163, Height=15,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_PARAM= 'C')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="OSUOKCFJPP",Visible=.t., Left=324, Top=220,;
    Alignment=1, Width=114, Height=19,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (!( .w_ONUME=7 OR .w_ONUME=8))
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_scl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
