* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kdl                                                        *
*              Dichiarazione di intento collegata                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2017-09-01                                                      *
* Last revis.: 2017-10-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kdl",oParentObject))

* --- Class definition
define class tgscg_kdl as StdForm
  Top    = 4
  Left   = 8

  * --- Standard Properties
  Width  = 796
  Height = 345
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-10-31"
  HelpContextID=185981591
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_kdl"
  cComment = "Dichiarazione di intento collegata"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DICODICE = space(15)
  w_DICODIVA = space(5)
  w_DITIPCON = space(1)
  w_DITIPOPE = space(1)
  w_DISERIAL = space(15)
  w_DIDICCOL = space(15)
  w_DIDATINI = ctod('  /  /  ')
  w_DIDATFIN = ctod('  /  /  ')
  w_DIDATREV = ctod('  /  /  ')
  w_DATA_FINE = ctod('  /  /  ')
  w_ANNDIC_COLL = space(4)
  w_DATDIC_COLL = ctod('  /  /  ')
  w_NUMDIC_COLL = 0
  w_SERDOC_COLL = space(3)
  w_DITIPIVA = space(1)
  w_DIC_COLL = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kdlPag1","gscg_kdl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_DIC_COLL = this.oPgFrm.Pages(1).oPag.DIC_COLL
    DoDefault()
    proc Destroy()
      this.w_DIC_COLL = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DICODICE=space(15)
      .w_DICODIVA=space(5)
      .w_DITIPCON=space(1)
      .w_DITIPOPE=space(1)
      .w_DISERIAL=space(15)
      .w_DIDICCOL=space(15)
      .w_DIDATINI=ctod("  /  /  ")
      .w_DIDATFIN=ctod("  /  /  ")
      .w_DIDATREV=ctod("  /  /  ")
      .w_DATA_FINE=ctod("  /  /  ")
      .w_ANNDIC_COLL=space(4)
      .w_DATDIC_COLL=ctod("  /  /  ")
      .w_NUMDIC_COLL=0
      .w_SERDOC_COLL=space(3)
      .w_DITIPIVA=space(1)
      .w_DICODICE=oParentObject.w_DICODICE
      .w_DICODIVA=oParentObject.w_DICODIVA
      .w_DITIPCON=oParentObject.w_DITIPCON
      .w_DITIPOPE=oParentObject.w_DITIPOPE
      .w_DISERIAL=oParentObject.w_DISERIAL
      .w_DIDICCOL=oParentObject.w_DIDICCOL
      .w_DIDATINI=oParentObject.w_DIDATINI
      .w_DIDATFIN=oParentObject.w_DIDATFIN
      .w_DIDATREV=oParentObject.w_DIDATREV
      .w_ANNDIC_COLL=oParentObject.w_ANNDIC_COLL
      .w_DATDIC_COLL=oParentObject.w_DATDIC_COLL
      .w_NUMDIC_COLL=oParentObject.w_NUMDIC_COLL
      .w_SERDOC_COLL=oParentObject.w_SERDOC_COLL
      .w_DITIPIVA=oParentObject.w_DITIPIVA
      .oPgFrm.Page1.oPag.DIC_COLL.Calculate()
          .DoRTCalc(1,5,.f.)
        .w_DIDICCOL = .w_Dic_Coll.getVar('DISERIAL')
          .DoRTCalc(7,9,.f.)
        .w_DATA_FINE = IIF(NOT EMPTY(.w_DIDATREV),.w_DIDATREV,.w_DIDATFIN)
        .w_ANNDIC_COLL = .w_Dic_Coll.getVar('DI__ANNO')
        .w_DATDIC_COLL = CP_TODATE(.w_Dic_Coll.getVar('DIDATLET'))
        .w_NUMDIC_COLL = .w_Dic_Coll.getVar('DINUMDOC')
        .w_SERDOC_COLL = .w_Dic_Coll.getVar('DISERDOC')
    endwith
    this.DoRTCalc(15,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DICODICE=.w_DICODICE
      .oParentObject.w_DICODIVA=.w_DICODIVA
      .oParentObject.w_DITIPCON=.w_DITIPCON
      .oParentObject.w_DITIPOPE=.w_DITIPOPE
      .oParentObject.w_DISERIAL=.w_DISERIAL
      .oParentObject.w_DIDICCOL=.w_DIDICCOL
      .oParentObject.w_DIDATINI=.w_DIDATINI
      .oParentObject.w_DIDATFIN=.w_DIDATFIN
      .oParentObject.w_DIDATREV=.w_DIDATREV
      .oParentObject.w_ANNDIC_COLL=.w_ANNDIC_COLL
      .oParentObject.w_DATDIC_COLL=.w_DATDIC_COLL
      .oParentObject.w_NUMDIC_COLL=.w_NUMDIC_COLL
      .oParentObject.w_SERDOC_COLL=.w_SERDOC_COLL
      .oParentObject.w_DITIPIVA=.w_DITIPIVA
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.DIC_COLL.Calculate()
        .DoRTCalc(1,5,.t.)
            .w_DIDICCOL = .w_Dic_Coll.getVar('DISERIAL')
        .DoRTCalc(7,10,.t.)
            .w_ANNDIC_COLL = .w_Dic_Coll.getVar('DI__ANNO')
            .w_DATDIC_COLL = CP_TODATE(.w_Dic_Coll.getVar('DIDATLET'))
            .w_NUMDIC_COLL = .w_Dic_Coll.getVar('DINUMDOC')
            .w_SERDOC_COLL = .w_Dic_Coll.getVar('DISERDOC')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.DIC_COLL.Calculate()
    endwith
  return

  proc Calculate_UPQAISNNBC()
    with this
          * --- Seleziona record zoom
          GSCG_BDN(this;
              ,'Sele';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.DIC_COLL.Event(cEvent)
        if lower(cEvent)==lower("w_DIC_COLL selected")
          .Calculate_UPQAISNNBC()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kdlPag1 as StdContainer
  Width  = 792
  height = 345
  stdWidth  = 792
  stdheight = 345
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object DIC_COLL as cp_zoombox with uid="UMERANHMVB",left=3, top=6, width=780,height=284,;
    caption='Object',;
   bGlobalFont=.t.,;
    bQueryOnDblClick=.f.,bQueryOnLoad=.t.,bNoZoomGridShape=.f.,bRetriveAllRows=.f.,cZoomOnZoom="",cZoomFile="GSCG_KDL",bOptions=.f.,bAdvOptions=.t.,cTable="DIC_INTE",bReadOnly=.t.,cMenuFile="",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 172891674


  add object oBtn_1_13 as StdButton with uid="KYQESPJULE",left=735, top=293, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193299014;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kdl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
