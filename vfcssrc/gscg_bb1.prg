* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bb1                                                        *
*              Libro inventari (aggiornamento tabella numeregi)                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_8]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-20                                                      *
* Last revis.: 2002-09-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bb1",oParentObject)
return(i_retval)

define class tgscg_bb1 as StdBatch
  * --- Local variables
  w_AZIENDA = space(5)
  * --- WorkFile variables
  NUMEREGI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Effettuo l'aggiornamento alla tabella NUMEREGI
    this.w_AZIENDA = I_CODAZI
    if ah_YesNo("Aggiorno il progressivo di pagina nei registri contabili?")
      * --- Try
      local bErr_0387AA70
      bErr_0387AA70=bTrsErr
      this.Try_0387AA70()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        AH_ERRORMSG("Impossibile aggiornare ultima pagina di stampa. Verificare tabella registri contabili",48)
      endif
      bTrsErr=bTrsErr or bErr_0387AA70
      * --- End
    endif
  endproc
  proc Try_0387AA70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into NUMEREGI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.NUMEREGI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NUMEREGI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.NUMEREGI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"NRULTPLI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ULTPAG),'NUMEREGI','NRULTPLI');
          +i_ccchkf ;
      +" where ";
          +"NRCODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
             )
    else
      update (i_cTable) set;
          NRULTPLI = this.oParentObject.w_ULTPAG;
          &i_ccchkf. ;
       where;
          NRCODAZI = this.w_AZIENDA;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    This.oparentObject.oparentObject.w_ULTPLI=This.oparentObject.w_ULTPAG
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='NUMEREGI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
