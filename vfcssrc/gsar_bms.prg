* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bms                                                        *
*              Gestione maschera log errori                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_10]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-02                                                      *
* Last revis.: 2012-08-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bms",oParentObject)
return(i_retval)

define class tgsar_bms as StdBatch
  * --- Local variables
  w_OK = .NULL.
  w_ESCI = .NULL.
  w_LABDOC = .NULL.
  w_LABIMB = .NULL.
  w_LABTRA = .NULL.
  w_CAMPIMB = .NULL.
  w_CAMPTRA = .NULL.
  w_DESIMB = .NULL.
  w_DESTRA = .NULL.
  w_GSAR_KMS = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da Gsar_Kms. Se la maschera � lanciata dalle offerte rimpicciolisco la maschera
    *     non visualizzo le variabili dei vettori e sposto i bottoni Ok e Esci pi� in alto
    if Upper(this.oParentObject.oParentObject.Class)="TGSOF_AOF"
      this.w_OK = this.oParentObject.GETCTRL(ah_Msgformat("\<OK"))
      this.w_ESCI = this.oParentObject.GETCTRL(ah_Msgformat("\<Esci"))
      this.w_LABDOC = this.oParentObject.GETCTRL(ah_Msgformat("Documento"))
      this.w_CAMPIMB = this.oParentObject.GETCTRL("w_MCALSI")
      this.w_CAMPTRA = this.oParentObject.GETCTRL("w_MCALST")
      this.w_DESIMB = this.oParentObject.GETCTRL("w_DESMCIMB")
      this.w_DESTRA = this.oParentObject.GETCTRL("w_DESMCTRA")
      this.w_GSAR_KMS = this.oParentObject
      this.w_OK.Top = 105
      this.w_ESCI.Top = 105
      this.w_LABDOC.Top = 10
      this.w_CAMPIMB.Top = 35
      this.w_DESIMB.Top = 35
      this.w_CAMPTRA.Top = 60
      this.w_DESTRA.Top = 60
      this.w_GSAR_KMS.Height = 150
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
