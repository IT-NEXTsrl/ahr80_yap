* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bpp                                                        *
*              Aggiorna campi percentuale, importo, tipo provvigione           *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_232]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-06                                                      *
* Last revis.: 2015-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bpp",oParentObject)
return(i_retval)

define class tgsve_bpp as StdBatch
  * --- Local variables
  w_TEMP = space(10)
  w_TMPC = space(10)
  w_MESS = space(100)
  w_MVSERIAL = space(10)
  w_CPROWNUM = 0
  w_MVPERPRO = 0
  w_MVIMPPRO = 0
  w_MVTIPPRO = space(2)
  w_MVPROCAP = 0
  w_MVTIPPR2 = space(2)
  w_MVIMPCAP = 0
  w_MVSCONT1 = 0
  w_MVSCONT2 = 0
  w_MVSCONT3 = 0
  w_MVSCONT4 = 0
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_MVSCOPAG = 0
  w_MVCODAGE = space(5)
  w_MVCODAG2 = space(5)
  w_MVTIPCON = space(1)
  w_MVCODCON = space(15)
  w_MVCODART = space(20)
  w_MVVALMAG = 0
  w_MVCODVAL = space(3)
  w_DECTOT = 0
  w_MVCLADOC = space(2)
  w_MVGENPRO = space(1)
  w_AGSCOPAG = space(1)
  w_AGEPRO = space(5)
  w_CLIPRO = space(5)
  w_ARTPRO = space(5)
  w_SM = 0
  w_CHIAVE = space(10)
  Primo = .f.
  w_APPO = 0
  w_Messaggio = space(10)
  w_COMPOSTO = space(10)
  w_TPPROVVI = space(1)
  w_TIPPRO = space(2)
  w_CODAGE = space(5)
  w_AGOCAP = 0
  w_GRUPRO = space(5)
  w_CATCLI = space(5)
  w_ARTPRO = space(5)
  w_DECTOT = 0
  w_MVFLOMAG = space(1)
  w_MVTCOLIS = space(5)
  w_MVDATREG = ctod("  /  /  ")
  w_MVQTAMOV = 0
  w_MVCODICE = space(20)
  w_MVUNIMIS = space(3)
  w_MVQTAUM1 = 0
  w_MVFLSCOR = space(1)
  w_MVPREZZO = 0
  w_MVCODIVA = space(5)
  w_PARAM = space(3)
  w_ARRPROV = 0
  w_TIPPRO = space(2)
  w_TIPPR2 = space(2)
  w_MASSGEN = space(1)
  w_OK = .f.
  w_MVFLVEAC = space(1)
  w_MVCONTRA = space(20)
  w_CATCOM = space(5)
  w_MVCAOVAL = 0
  w_MVTIPRIG = space(1)
  w_PROGEN = space(1)
  w_RICAGE = .f.
  w_RICCAP = .f.
  w_MVSPEINC = 0
  w_MVFLRINC = space(1)
  w_MVSPEIMB = 0
  w_MVFLRIMB = space(1)
  w_MVSPETRA = 0
  w_MVFLRTRA = space(1)
  w_SPERIP = .f.
  w_LISRIFSCO = space(5)
  w_MVDATDOC = ctod("  /  /  ")
  w_SERIAL = space(10)
  w_DATCAL = ctod("  /  /  ")
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_PREZUM = space(1)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_CALPRZ = 0
  w_PROG = space(3)
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_LTIPPRO = space(2)
  w_LTIPPR2 = space(2)
  * --- WorkFile variables
  DOC_DETT_idx=0
  VOCIIVA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa procedura Aggiorna il campo tipo Provvigione nei documenti
    * --- FIle di LOG
    this.w_MASSGEN = "N"
    * --- Nel caso in cui il check calcola sconto sia cambiato contestualmente all'aggiornamento provvigioni
    *     la variabile globale avrebbe ancora il vecchio valore pertanto l'aggiorno
    g_CALSCO=this.oParentObject.w_PPCALSCO 
 g_LISRIF=this.oParentObject.w_PPLISRIF
    if Type("This.oParentObject.w_MASSGEN")= "C" And This.oParentObject.w_MASSGEN= "S"
      * --- Lanciato da Generazione Massiva di documenti
      this.w_MASSGEN = "S"
      * --- Serve come filtro nella query GSVE_BPP
      this.w_SERIAL = This.oParentObject.w_SERPRO
      this.w_PROGEN = g_PROGEN
    else
      this.w_PROGEN = this.oParentObject.w_PPPROGEN
    endif
    if this.oParentObject.w_OLDCALPRO<>this.oParentObject.w_PPCALPRO
      if this.w_MASSGEN = "S"
        * --- Ne caso di generazione documenti non faccio domande
        this.w_OK = .T.
      else
        this.w_Messaggio = "ATTENZIONE%0L'aggiornamento dei campi tipo, importo e percentuale provvigione sui documenti pu� richiedere molto tempo%0Si desidera continuare?"
        this.w_OK = ah_YesNo(this.w_Messaggio)
      endif
      if this.w_OK
        * --- Try
        local bErr_047AE270
        bErr_047AE270=bTrsErr
        this.Try_047AE270()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Elaborazione interrotta")
        endif
        bTrsErr=bTrsErr or bErr_047AE270
        * --- End
      else
        this.oParentObject.w_PPCALPRO = this.oParentObject.w_OLDCALPRO
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=MSG_TRANSACTION_ERROR
      endif
    endif
    if used("PROVV")
      select PROVV
      use
    endif
  endproc
  proc Try_047AE270()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- A seconda del cambiamento del parametro riimposta  nei documenti il tipo, la perxcentuale e l'importo provvigioni per agente e capoarea
    do case
      case this.oParentObject.w_OLDCALPRO="DI" AND this.oParentObject.w_PPCALPRO$"GI-GD"
        if this.oParentObject.w_PPCALPRO="GI"
          this.w_MVTIPPRO = "CT"
          this.w_MVTIPPR2 = "CT"
        else
          this.w_MVTIPPRO = "ST"
          this.w_MVTIPPR2 = "ST"
        endif
      case this.oParentObject.w_OLDCALPRO="GI" AND this.oParentObject.w_PPCALPRO$"GD-DI"
        if this.oParentObject.w_PPCALPRO="GD"
          this.w_MVTIPPRO = "ST"
          this.w_MVTIPPR2 = "ST"
        else
          this.w_MVTIPPRO = "DC"
          this.w_MVTIPPR2 = "DC"
        endif
      case this.oParentObject.w_OLDCALPRO="GD" AND this.oParentObject.w_PPCALPRO$"GI-DI"
        if this.oParentObject.w_PPCALPRO="GI"
          this.w_MVTIPPRO = "CT"
          this.w_MVTIPPR2 = "CT"
        else
          this.w_MVTIPPRO = "DC"
          this.w_MVTIPPR2 = "DC"
        endif
    endcase
    if this.w_MASSGEN = "N"
      * --- Solo se da modifica parametri provvigioni
      * --- Aggiorno campo tipo agente e tipo capaoarea per i documenti gi� generati o che non hanno il codice agente
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
        do vq_exec with 'QUERY\GSVE1BPP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
            +i_ccchkf;
            +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
        +"DOC_DETT.MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
            +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
            +"MVTIPPRO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
        +"MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
        do vq_exec with 'QUERY\GSVE3BPP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
            +i_ccchkf;
            +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
        +"DOC_DETT.MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
            +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
            +"MVTIPPR2";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
        +"MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.oParentObject.w_PPCALPRO="DI"
      * --- Aggiorno campo tipo agente e tipo capaoarea per i documenti non ancora generati e che  hanno il codice agente
      ah_Msg("Aggiornamento documenti in corso...",.T.)
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
        do vq_exec with 'QUERY\GSVE2BPP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
            +i_ccchkf;
            +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
        +"DOC_DETT.MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
            +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
            +"MVTIPPRO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
        +"MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
        do vq_exec with 'QUERY\GSVE4BPP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
            +i_ccchkf;
            +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
        +"DOC_DETT.MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
            +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
            +"MVTIPPR2";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
        +"MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      DECLARE ARRCALC (16,1)
      ah_Msg("Aggiornamento documenti in corso...")
      DECLARE pARRPROV (21)
      DIMENSION pArrUm[9]
      vq_exec("query\gsve_bpp.vqr",this,"PROVV")
      if USED("PROVV")
        if Reccount ("PROVV")>0
          SELECT PROVV
          GO TOP
          SCAN 
          this.w_MVGENPRO = NVL(MVGENPRO," ")
          this.w_MVSERIAL = NVL(MVSERIAL, SPACE(10))
          this.w_CPROWNUM = NVL(CPROWNUM,0)
          this.w_MVSCONT1 = NVL(MVSCONT1,0)
          this.w_MVSCONT2 = NVL(MVSCONT2,0)
          this.w_MVSCONT3 = NVL(MVSCONT3,0)
          this.w_MVSCONT4 = NVL(MVSCONT4,0)
          this.w_MVSCOCL1 = NVL(MVSCOCL1,0)
          this.w_MVSCOCL2 = NVL(MVSCOCL2,0)
          this.w_MVSCOPAG = NVL(MVSCOPAG,0)
          this.w_MVFLOMAG = NVL(MVFLOMAG," ")
          this.w_MVCODAGE = NVL(MVCODAGE,SPACE(5))
          this.w_MVCODAG2 = NVL(MVCODAG2,SPACE(5))
          this.w_MVTIPCON = NVL(MVTIPCON," ")
          this.w_MVCODCON = NVL(MVCODCON,SPACE(15))
          this.w_MVCODART = NVL(MVCODART,SPACE(20))
          this.w_MVCLADOC = NVL(MVCLADOC,SPACE(2))
          this.w_MVVALMAG = NVL(MVVALMAG,0) *IIF(this.w_MVCLADOC="NC",-1,1)
          this.w_MVCODVAL = NVL(MVCODVAL,SPACE(3))
          * --- Valori di Default
          this.w_MVTIPPRO = IIF(this.w_PROGEN = "S", IIF(this.oParentObject.w_PPCALPRO="DI","DC",IIF(this.oParentObject.w_PPCALPRO="GD","ST","CT")) ,this.w_MVTIPPRO)
          this.w_MVTIPPR2 = IIF(this.w_PROGEN = "S", IIF(this.oParentObject.w_PPCALPRO="DI","DC",IIF(this.oParentObject.w_PPCALPRO="GD","ST","CT")) ,this.w_MVTIPPR2)
          this.w_TIPPRO = NVL(MVTIPPRO,"  ")
          this.w_TIPPR2 = NVL(MVTIPPR2,"  ")
          * --- Nei casi Da Calcolare, Suggerita da tabella, Calcolata da tabella ed Esclusa da tabella
          *     deve ricalcolare con le nuove impostazione.
          *     Negli altri casi, come ad esempio Esclusa manualmente, deve mantenere l'impostazione
          this.w_TIPPRO = IIF(this.w_TIPPRO$"DC-ST-CT-ET", this.w_MVTIPPRO, this.w_TIPPRO)
          this.w_TIPPR2 = IIF(this.w_TIPPR2$"DC-ST-CT-ET", this.w_MVTIPPR2, this.w_TIPPR2)
          this.w_RICAGE = this.w_TIPPRO$"DC-ST-CT-ET"
          this.w_RICCAP = this.w_TIPPR2$"DC-ST-CT-ET"
          this.w_MVPERPRO = NVL(MVPERPRO,0)
          this.w_MVIMPPRO = NVL(MVIMPPRO,0)
          this.w_MVPROCAP = NVL(MVPROCAP,0)
          this.w_MVIMPCAP = NVL(MVIMPCAP,0)
          this.w_AGEPRO = NVL(AGCATPRO,SPACE(5))
          this.w_MVTCOLIS = IIF(NOT EMPTY(NVL(g_LISRIF,SPACE(5))), g_LISRIF, NVL(MVTCOLIS,SPACE(5)) )
          this.w_MVDATREG = NVL(MVDATREG, cp_CharToDate("  -  -  "))
          this.w_MVQTAMOV = NVL(MVQTAMOV,0)
          this.w_MVCODICE = NVL(MVCODICE,SPACE(20))
          this.w_MVUNIMIS = NVL(MVUNIMIS,SPACE(3))
          this.w_MVQTAUM1 = NVL(MVQTAUM1,0)
          this.w_MVFLSCOR = NVL(MVFLSCOR," ")
          this.w_MVCODIVA = NVL(MVCODIVA,SPACE(5))
          this.w_MVPREZZO = NVL(MVPREZZO,0)
          this.w_GRUPRO = NVL(ANGRUPRO,SPACE(5))
          this.w_CATCLI = NVL(ANCATSCM,SPACE(5))
          this.w_AGSCOPAG = NVL(AGSCOPAG,"")
          this.w_ARTPRO = NVL(ARGRUPRO,SPACE(5))
          this.w_DECTOT = NVL(VADECTOT,0)
          this.w_MVFLVEAC = NVL(MVFLVEAC," ")
          this.w_MVCONTRA = NVL(MVCONTRA,SPACE(20))
          this.w_CATCOM = NVL(ANCATCOM,SPACE(5))
          this.w_MVCAOVAL = NVL(MVCAOVAL,0)
          this.w_MVTIPRIG = NVL(MVTIPRIG," ")
          this.w_MVSPEINC = NVL(MVSPEINC,0)
          this.w_MVFLRINC = NVL(MVFLRINC," ")
          this.w_MVSPEIMB = NVL(MVSPEIMB,0)
          this.w_MVFLRIMB = NVL(MVFLRIMB," ")
          this.w_MVSPETRA = NVL(MVSPETRA,0)
          this.w_MVFLRTRA = NVL(MVFLRTRA," ")
          this.w_MVDATDOC = NVL(MVDATDOC, cp_CharToDate("  -  -  "))
          this.w_DATCAL = IIF(EMPTY(this.w_MVDATDOC), this.w_MVDATREG, this.w_MVDATDOC)
          * --- Verifico se ci sono spese ripartite
          this.w_SPERIP = IIF((this.w_MVSPEINC<>0 AND this.w_MVFLRINC="S") OR (this.w_MVSPEIMB<>0 AND this.w_MVFLRIMB="S") OR (this.w_MVSPETRA<>0 AND this.w_MVFLRTRA="S"), .T.,.F.)
          this.w_UNMIS1 = NVL(UNMIS1,SPACE(3))
          this.w_UNMIS2 = NVL(UNMIS2,SPACE(3))
          this.w_UNMIS3 = NVL(UNMIS3,SPACE(3))
          this.w_PREZUM = NVL(ARPREZUM, " ")
          this.w_OPERAT = NVL(OPERAT,"")
          this.w_MOLTIP = NVL(MOLTIP,0)
          this.w_OPERA3 = NVL(OPERA3,"")
          this.w_MOLTI3 = NVL(MOLTI3,0)
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA;
              from (i_cTable) where;
                  IVCODIVA = this.w_MVCODIVA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Calcola l'importo e la percentuale sulla base della Tabella Provvigioni  e assegna a MVTIPPRO il nuovo valore congruente al cambio del parametro
          if this.w_MVSCOCL1=0
            this.w_MVSCOCL2 = 0
          endif
          if this.w_MASSGEN = "S" And this.w_MVTIPRIG <> "D" And (this.w_RICAGE Or this.w_RICCAP)
            * --- Calcolo prima le provvigioni da contratto
            *     Solo nel caso di generazione massiva poich� nel caso di cambio del tipo generazione sui parametri
            *     i documenti, che sono gi� stati caricati a mano, hanno gi� i valori giusti nel caso ci fosse stato un contratto valido
            this.w_PROG = "V"
            * --- Azzero l'Array che verr� riempito dalla Funzione
             
 ARRCALC(1)=0 
 ARRCALC(6)=0 
 ARRCALC(15)=0
            * --- Lancio la funzione di calcolo prezzi che uso solo per prelevare le informazioni da contratto 
            *     Non passo il contratto perch� deve ricalcolare quello valido al momento della generazione
            if this.w_PREZUM<>"C"
              this.w_QTAUM2 = 0
              this.w_QTAUM3 = 0
            else
              this.w_QTAUM2 = CALQTA(this.w_MVQTAUM1,this.w_UNMIS2, this.w_UNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
              this.w_QTAUM3 = CALQTA(this.w_MVQTAUM1,this.w_UNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
            endif
            pArrUm [1] = this.w_PREZUM 
 pArrUm [2] = this.w_MVUNIMIS 
 pArrUm [3] = this.w_MVQTAMOV 
 pArrUm [4] = this.w_UNMIS1 
 pArrUm [5] = this.w_MVQTAUM1 
 pArrUm [6] = this.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
            this.w_CALPRZ = CalPrzli(Space(20), this.w_MVTIPCON , Space(5) , this.w_MVCODART , "XXXXXX" , this.w_MVQTAUM1 , this.w_MVCODVAL , this.w_MVCAOVAL , this.w_DATCAL , this.w_CATCLI , "XXXXXX", "     ", this.w_MVCODCON, this.w_CATCOM, this.w_MVFLSCOR, " ", "     ",this.w_PROG, @ARRCALC, " ", this.w_MVFLVEAC,"N", @pArrUm )
            if this.w_MVFLVEAC<>"A" AND g_PERAGE="S"
              * --- Percentuale provvigione agente
              if ARRCALC(6)<>0 And this.w_RICAGE
                * --- Percentuale Provvigione <>0.
                *     Imposto TIPPRO ='CC' : Calcolata da contratto
                this.w_MVPERPRO = ARRCALC(6)
                this.w_MVIMPPRO = 0
                this.w_MVTIPPRO = "CC"
                * --- Se trovo qualcosa su contratto devo passare l'informazione alla funzione CAL_PROV
                this.w_TIPPRO = this.w_MVTIPPRO
                * --- Write into DOC_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_LTIPPRO),'DOC_DETT','MVTIPPRO');
                  +",MVIMPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPPRO),'DOC_DETT','MVIMPPRO');
                  +",MVPERPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPERPRO),'DOC_DETT','MVPERPRO');
                      +i_ccchkf ;
                  +" where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                         )
                else
                  update (i_cTable) set;
                      MVTIPPRO = this.w_LTIPPRO;
                      ,MVIMPPRO = this.w_MVIMPPRO;
                      ,MVPERPRO = this.w_MVPERPRO;
                      &i_ccchkf. ;
                   where;
                      MVSERIAL = this.w_MVSERIAL;
                      and CPROWNUM = this.w_CPROWNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              * --- Percentuale provvigione capoarea
              if ARRCALC(15)<>0 And this.w_RICCAP
                this.w_MVPROCAP = ARRCALC(15)
                this.w_MVIMPCAP = 0
                this.w_MVTIPPR2 = "CC"
                this.w_TIPPR2 = this.w_MVTIPPR2
                * --- Se trovo qualcosa su contratto devo passare l'informazione alla funzione CAL_PROV
                * --- Write into DOC_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
                  +",MVIMPCAP ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCAP),'DOC_DETT','MVIMPCAP');
                  +",MVPROCAP ="+cp_NullLink(cp_ToStrODBC(this.w_MVPROCAP),'DOC_DETT','MVPROCAP');
                      +i_ccchkf ;
                  +" where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                         )
                else
                  update (i_cTable) set;
                      MVTIPPR2 = this.w_MVTIPPR2;
                      ,MVIMPCAP = this.w_MVIMPCAP;
                      ,MVPROCAP = this.w_MVPROCAP;
                      &i_ccchkf. ;
                   where;
                      MVSERIAL = this.w_MVSERIAL;
                      and CPROWNUM = this.w_CPROWNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
          endif
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_LTIPPRO = pArrprov[ 1 ]
          this.w_LTIPPR2 = pArrprov[ 3 ]
          * --- Se esclusa da tabella marco il tipo provvigione agente
          if this.w_LTIPPRO<>"ET"
            this.w_LTIPPRO = this.w_TIPPRO
          endif
          if this.w_TIPPRO<>"FO"
            this.w_MVPERPRO = pArrprov(2)
            this.w_MVIMPPRO = pArrprov(20)
          endif
          * --- Se esclusa da tabella marco il tipo provvigione capoarea
          if this.w_LTIPPR2<>"ET"
            this.w_LTIPPR2 = this.w_TIPPR2
          endif
          if NOT EMPTY(this.w_MVCODAG2) and this.w_TIPPR2<>"FO"
            this.w_MVPROCAP = IIF(this.w_MVPROCAP=999.9, this.w_MVPROCAP,pArrprov(4))
            this.w_MVIMPCAP = IIF(this.w_MVPROCAP=999.9, 0,pArrprov(21))
          endif
          ah_Msg("Aggiornamento documenti in corso...%1",.T.,.F.,.F., this.w_MVSERIAL+STR(this.w_CPROWNUM) )
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_LTIPPRO),'DOC_DETT','MVTIPPRO');
            +",MVIMPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPPRO),'DOC_DETT','MVIMPPRO');
            +",MVPERPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPERPRO),'DOC_DETT','MVPERPRO');
            +",MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_LTIPPR2),'DOC_DETT','MVTIPPR2');
            +",MVIMPCAP ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCAP),'DOC_DETT','MVIMPCAP');
            +",MVPROCAP ="+cp_NullLink(cp_ToStrODBC(this.w_MVPROCAP),'DOC_DETT','MVPROCAP');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                MVTIPPRO = this.w_LTIPPRO;
                ,MVIMPPRO = this.w_MVIMPPRO;
                ,MVPERPRO = this.w_MVPERPRO;
                ,MVTIPPR2 = this.w_LTIPPR2;
                ,MVIMPCAP = this.w_MVIMPCAP;
                ,MVPROCAP = this.w_MVPROCAP;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_MVSERIAL;
                and CPROWNUM = this.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          SELECT PROVV
          ENDSCAN
          * --- Esecuzione ok
        endif
      endif
    endif
    if bTrsErr
      * --- Raise
      i_Error=MESSAGE()
      return
    else
      if this.w_MASSGEN= "N"
        ah_ErrorMsg("Aggiornamento terminato con successo")
      endif
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PARAM = "CCD"
    pArrprov [1] = this.w_tippro 
 pArrprov [2] = this.w_mvperpro 
 pArrprov [3] = this.w_tippr2 
 pArrprov [4] = this.w_mvprocap 
 pArrprov [5] = this.w_grupro 
 pArrprov [6] = this.w_catcli 
 pArrprov [7] = this.w_agepro 
 pArrprov [8] = IIF(this.w_SPERIP, "S", this.w_agscopag ) 
 pArrprov [9] = this.w_artpro 
 pArrprov [10] = this.w_mvflomag 
 pArrprov [11] = this.w_dectot 
 pArrprov [12] = this.w_mvscont1 
 pArrprov [13] = this.w_mvscont2 
 pArrprov [14] = this.w_mvscont3 
 pArrprov [15] = this.w_mvscont4 
 pArrprov [16] = this.w_mvscocl1 
 pArrprov [17] = this.w_mvscocl2 
 pArrprov [18] = this.w_mvscopag 
 pArrprov [19] = this.w_MVVALMAG *IIF(this.w_MVCLADOC="NC",-1,1) 
 pArrprov [20] = 0 
 pArrprov [21] = 0
    DIMENSION ARRPROV2[11]
    Arrprov2[ 1 ] = this.w_MVCODVAL 
 Arrprov2[ 2 ] = this.w_MVCODART 
 Arrprov2[ 3 ] = this.w_MVTCOLIS 
 Arrprov2[ 4 ] = this.w_MVDATREG 
 Arrprov2[ 5 ] = this.w_MVQTAMOV 
 Arrprov2[ 6 ] = this.w_MVCODICE 
 Arrprov2[ 7 ] = this.w_MVUNIMIS 
 Arrprov2[ 8 ] = this.w_MVQTAUM1 
 Arrprov2[ 9 ] = this.w_MVFLSCOR 
 Arrprov2[ 10 ] = this.w_MVPREZZO 
 Arrprov2[ 11 ] = w_PERIVA
    this.w_ARRPROV = CAL_PROV(this.w_PARAM,@pArrprov,@Arrprov2)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='VOCIIVA'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
