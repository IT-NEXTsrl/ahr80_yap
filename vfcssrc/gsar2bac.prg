* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar2bac                                                        *
*              GESTIONE FILTRO SU ATTRIBUTI                                    *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-11                                                      *
* Last revis.: 2008-01-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pPUNPAD,pPUNTUALE,pTIPSELE,pCICLO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar2bac",oParentObject,m.pOPER,m.pPUNPAD,m.pPUNTUALE,m.pTIPSELE,m.pCICLO)
return(i_retval)

define class tgsar2bac as StdBatch
  * --- Local variables
  pOPER = space(4)
  pPUNPAD = .NULL.
  pPUNTUALE = space(1)
  pTIPSELE = space(1)
  pCICLO = space(1)
  w_PUNPAD = .NULL.
  w_PUNTUALE = space(1)
  w_TIPSELE = space(1)
  w_CICLO = space(1)
  w_APART = space(20)
  w_CONTANUM = 0
  w_NELAB = 0
  w_NFLTATTR = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GESTIONE FILTRO SU ATTRIBUTI
    this.w_PUNPAD = this.pPUNPAD
    this.w_PUNTUALE = this.pPUNTUALE
    this.w_TIPSELE = this.pTIPSELE
    this.w_CICLO = this.pCICLO
    if VARTYPE( this.w_PUNPAD.cTRSNAME ) = "U"
      AH_ERRORMSG("Nessun attributo selezionato",48)
      i_retcode = 'stop'
      return
    endif
    do case
      case this.pOPER = "VIAR"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_NELAB > 0
          do GSVE1KML With "A"
        else
          Select Elab 
 Use
          AH_ERRORMSG("Nessun attributo con modello ' ART_ICOL' selezionato",48)
          i_retcode = 'stop'
          return
        endif
      case this.pOPER = "STAR"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_NELAB > 0
          cp_chprn("QUERY\GSVE_KML.FRX","",this)
        else
          Select Elab 
 Use
          AH_ERRORMSG("Nessun attributo con modello ' ART_ICOL' selezionato",48)
          i_retcode = 'stop'
          return
        endif
      case this.pOPER = "CUAR"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPER = "VICL"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_NELAB > 0
          do GSVE1KML With "C"
        else
          Select Elab 
 Use
          AH_ERRORMSG("Nessun attributo con modello 'CONTI' selezionato",48)
          i_retcode = 'stop'
          return
        endif
      case this.pOPER = "STCL"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_NELAB > 0
          cp_chprn("QUERY\GSVE1KML.FRX","",this)
        else
          Select Elab 
 Use
          AH_ERRORMSG("Nessun attributo con modello 'CONTI' selezionato",48)
          i_retcode = 'stop'
          return
        endif
      case this.pOPER = "CUCL"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Attributi di selezione
    select t_CACODMOD,t_CACODFAM,t_CACODGRU,t_CAVALATT from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "ART_ICOL" And Not Empty (t_CAVALATT) into Cursor Elab 
    this.w_NELAB = RECCOUNT("Elab")
    if this.w_NELAB >0
      * --- Bottoni visualizza e stampa articoli
      *     Elenco articoli con attributi associati
      vq_exec("query\GSVE_BGL",this,"Artico")
      * --- Conto i record (attributi) per ogni articolo
      *     (serivir� pi� avanti per scartare gli articoli non validi se attivo in maschera 
      *     il flag Selezione Puntuale)
      if this.w_TIPSELE = "T"
        Select ARCODART, count(*) as NUM from Artico group by ARCODART into cursor CONTA
      endif
      * --- La join consente gi� di scartare tutti gli articoli che non hanno 
      *     neppure un attributo in comune.
      Select Artico.*,"S" as ACCETTA from Artico inner join Elab on; 
 ASMODFAM=t_CACODMOD and ASCODATT=t_CACODGRU and ASCODFAM=t_CACODFAM and ASVALATT=t_CAVALATT; 
 Into Cursor Artico 
      =wrcursor("Artico")
      if this.w_TIPSELE $ "AT"
        if this.w_TIPSELE ="T"
          * --- Dal risultato della join conto per ogni articolo i record (attributri) rimasti.
          *     Se NON coincidono col totale di attributi di selezione l'articolo NON � valido,
          *     � presente cio� almeno un attributo di selezione non soddisfatto.
          *     Se coincidono occorre controllare il flag PUNTUALE nella maschera:
          *     se ATTIVO confronto il totale dei record (attributi) di CONTAJ (risulatto della join)
          *     con il totale dei record di CONTA (totale di attributi per articolo), se coincide l'articolo viene ACCETTATO
          *     se NON coincide l'articolo viene SCARTATO.
          *     Se il flag puntuale � disattivo l'articolo � ACCETTATO
          Select ARCODART, count(*) as NUM from Artico group by ARCODART into cursor CONTAJ
          Select CONTAJ 
 Go top
          Scan
          this.w_APART = CONTAJ.ARCODART
          if this.w_NELAB > CONTAJ.NUM
            * --- Se sono presenti piu attributi di selezione rspetto a quelli associati
            *     all'articolo scarto l'articolo in questione
            Update Artico set ACCETTA="N" where ARCODART= this.w_APART
            Select Artico 
 Go Top
          else
            if this.w_PUNTUALE = "S"
              Select Conta 
 Locate for ARCODART=this.w_APART
              this.w_CONTANUM =  CONTA.NUM 
              Select CONTAJ
              if this.w_CONTANUM > CONTAJ.NUM
                Update Artico set ACCETTA="N" where ARCODART= this.w_APART
                Select Artico 
 Go Top
              endif
            endif
          endif
          Select CONTAJ
          EndScan
          Select Artico
        endif
      else
        * --- Almeno uno per gruppo o per famiglia
        if this.w_TIPSELE="G"
          * --- Count per GRUPPO
          Select distinct ARCODART, GUID, ASMODFAM, ASCODATT From "Artico" into cursor "_Gruppo_"
          Select Count(*) as CONTA, ARCODART,GUID from "_Gruppo_" Group By GUID Into Cursor "_Gruppo_"
          select t_CACODMOD,t_CACODGRU from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "ART_ICOL" And Not Empty (t_CAVALATT) Group by t_CACODMOD,t_CACODGRU into Cursor "Flt_Attr"
        else
          * --- Count per GRUPPO e FAMIGLIA
          Select distinct ARCODART, GUID, ASMODFAM, ASCODATT, ASCODFAM From "Artico" into cursor "_Gruppo_"
          Select Count(*) as CONTA, ARCODART,GUID from "_Gruppo_" Group By GUID Into Cursor "_Gruppo_"
          select t_CACODMOD,t_CACODGRU, t_CACODFAM from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "ART_ICOL" And Not Empty (t_CAVALATT) Group by t_CACODMOD,t_CACODGRU,t_CACODFAM into Cursor "Flt_Attr"
        endif
        this.w_NFLTATTR = RECCOUNT("Flt_Attr")
        if this.w_NFLTATTR > 0
          * --- Sono eliminati tutti i record che non soddisfano
          Delete from Artico where ARCODART+GUID not in; 
 (Select ARCODART+ GUID from _Gruppo_ where CONTA= this.w_NFLTATTR)
        endif
      endif
      * --- Trasferisco gli articoli nel cursore di stampa __tmp__
      Select * from Artico where ACCETTA="S" into cursor __tmp__ order by ARCODART group by ARCODART
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Attributi di selezione
    select t_CACODMOD,t_CACODFAM,t_CACODGRU,t_CAVALATT from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "CONTI" And Not Empty (t_CAVALATT) into Cursor Elab 
    this.w_NELAB = RECCOUNT("Elab")
    if this.w_NELAB >0
      * --- Bottoni visualizza e stampa CliFor
      *     Elenco CliFor con attributi associati
      vq_exec("query\GSVE1BGL",this,"CliFor")
      if this.w_TIPSELE="T"
        * --- Conto i record (attributi) per ogni CliFor
        *     (serivir� pi� avanti per scartare i CliFor non validi se attivo in maschera 
        *     il flag Selezione Puntuale)
        Select ANCODICE, count(*) as NUM from CliFor group by ANCODICE into cursor CONTA
      endif
      * --- La join consente gi� di scartare tutti i CliFor che non hanno 
      *     neppure un attributo in comune.
      Select CliFor.*,"S" as ACCETTA from CliFor inner join Elab on; 
 ASMODFAM=t_CACODMOD and ASCODATT=t_CACODGRU and ASCODFAM=t_CACODFAM and ASVALATT=t_CAVALATT; 
 Into Cursor CliFor 
      =wrcursor("CliFor")
      * --- 'AT' = Ameno uno /tutti
      if this.w_TIPSELE $ "AT"
        if this.w_TIPSELE="T"
          * --- Dal risultato della join conto per ogni CliFor i record (attributri) rimasti.
          *     Se NON coincidono col totale di attributi di selezione CliFor NON � valido,
          *     � presente cio� almeno un attributo di selezione non soddisfatto.
          *     Se coincidono occorre controllare il flag PUNTUALE nella maschera:
          *     se ATTIVO confronto il totale dei record (attributi) di CONTAJ (risulatto della join)
          *     con il totale dei record di CONTA (totale di attributi per CliFor), se coincide CliFor viene ACCETTATO
          *     se NON coincide CliFor viene SCARTATO.
          *     Se il flag puntuale � disattivo CliFor � ACCETTATO
          Select ANCODICE, count(*) as NUM from CliFor group by ANCODICE into cursor CONTAJ
          Select CONTAJ 
 Go top
          Scan
          this.w_APART = CONTAJ.ANCODICE
          if this.w_NELAB > CONTAJ.NUM
            * --- Se sono presenti piu attributi di selezione rspetto a quelli associati
            *     al CliFor scarto il CliFor in questione
            Update CliFor set ACCETTA="N" where ANCODICE= this.w_APART
            Select CliFor 
 Go Top
          else
            if this.w_PUNTUALE = "S"
              Select Conta 
 Locate for ANCODICE=this.w_APART
              this.w_CONTANUM =  CONTA.NUM 
              Select CONTAJ
              if this.w_CONTANUM > CONTAJ.NUM
                Update CliFor set ACCETTA="N" where ANCODICE= this.w_APART
                Select CliFor 
 Go Top
              endif
            endif
          endif
          Select CONTAJ
          EndScan
          Select CliFor
        endif
      else
        * --- Almeno uno per gruppo o per famiglia
        if this.w_TIPSELE="G"
          * --- Count per GRUPPO
          Select distinct ANTIPCON, ANCODICE, GUID, ASMODFAM, ASCODATT From "Clifor" into cursor "_Gruppo_"
          Select Count(*) as CONTA, ANTIPCON, ANCODICE, GUID from "_Gruppo_" Group By GUID Into Cursor "_Gruppo_"
          select t_CACODMOD,t_CACODGRU from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "CONTI" And Not Empty (t_CAVALATT) Group by t_CACODMOD,t_CACODGRU into Cursor "Flt_Attr"
        else
          * --- Count per GRUPPO e FAMIGLIA
          Select distinct ANTIPCON, ANCODICE, GUID, ASMODFAM, ASCODATT, ASCODFAM From "Clifor" into cursor "_Gruppo_"
          Select Count(*) as CONTA, ANTIPCON, ANCODICE, GUID from "_Gruppo_" Group By GUID Into Cursor "_Gruppo_"
          select t_CACODMOD,t_CACODGRU, t_CACODFAM from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "CONTI" And Not Empty (t_CAVALATT) Group by t_CACODMOD,t_CACODGRU,t_CACODFAM into Cursor "Flt_Attr"
        endif
        this.w_NFLTATTR = RECCOUNT("Flt_Attr")
        if this.w_NFLTATTR > 0
          * --- Sono eliminati tutti i record che non soddisfano
          Delete from Clifor where ANTIPCON+ANCODICE+GUID not in; 
 (Select ANTIPCON+ANCODICE+GUID from _Gruppo_ where CONTA= this.w_NFLTATTR)
        endif
      endif
      * --- Trasferisco i CliFor nel cursore di stampa __tmp__
      Select * from CliFor where ACCETTA="S" into cursor __tmp__ order by ANCODICE group by ANCODICE
    else
    endif
  endproc


  proc Init(oParentObject,pOPER,pPUNPAD,pPUNTUALE,pTIPSELE,pCICLO)
    this.pOPER=pOPER
    this.pPUNPAD=pPUNPAD
    this.pPUNTUALE=pPUNTUALE
    this.pTIPSELE=pTIPSELE
    this.pCICLO=pCICLO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pPUNPAD,pPUNTUALE,pTIPSELE,pCICLO"
endproc
