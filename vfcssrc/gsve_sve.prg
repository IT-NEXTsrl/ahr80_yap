* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_sve                                                        *
*              Stampa versamenti                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_33]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-16                                                      *
* Last revis.: 2012-10-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_sve",oParentObject))

* --- Class definition
define class tgsve_sve as StdForm
  Top    = 5
  Left   = 18

  * --- Standard Properties
  Width  = 437
  Height = 162
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-01"
  HelpContextID=40514153
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  VFI_DETT_IDX = 0
  VFI_MAST_IDX = 0
  cPrg = "gsve_sve"
  cComment = "Stampa versamenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SCELSTAM = space(1)
  w_PREGNO = 0
  w_CODSER = space(10)
  w_ANNO = space(4)
  w_DATREG = ctod('  /  /  ')
  w_DATREG1 = ctod('  /  /  ')
  w_DATINI1 = ctod('  /  /  ')
  w_DATFIN1 = ctod('  /  /  ')
  w_CODVAL = space(3)
  w_SCELMASK = space(8)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_SIMVAL = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_svePag1","gsve_sve",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCELSTAM_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='VFI_DETT'
    this.cWorkTables[2]='VFI_MAST'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSVE_BSV with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SCELSTAM=space(1)
      .w_PREGNO=0
      .w_CODSER=space(10)
      .w_ANNO=space(4)
      .w_DATREG=ctod("  /  /  ")
      .w_DATREG1=ctod("  /  /  ")
      .w_DATINI1=ctod("  /  /  ")
      .w_DATFIN1=ctod("  /  /  ")
      .w_CODVAL=space(3)
      .w_SCELMASK=space(8)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_SIMVAL=space(5)
        .w_SCELSTAM = 'P'
          .DoRTCalc(2,5,.f.)
        .w_DATREG1 = CP_TODATE(.w_DATREG)
        .w_DATINI1 = CP_TODATE(.w_DATINI)
        .w_DATFIN1 = CP_TODATE(.w_DATFIN)
          .DoRTCalc(9,9,.f.)
        .w_SCELMASK = iif(.w_SCELSTAM='F','GSVE_KZF', iif(.w_SCELSTAM='P','GSVE_KZP','GSVE_KZA'))
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
          .DoRTCalc(11,12,.f.)
        .w_SIMVAL = IIF(EMPTY(.w_CODVAL),' ',.w_SIMVAL)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
            .w_DATREG1 = CP_TODATE(.w_DATREG)
            .w_DATINI1 = CP_TODATE(.w_DATINI)
            .w_DATFIN1 = CP_TODATE(.w_DATFIN)
        .DoRTCalc(9,9,.t.)
            .w_SCELMASK = iif(.w_SCELSTAM='F','GSVE_KZF', iif(.w_SCELSTAM='P','GSVE_KZP','GSVE_KZA'))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .DoRTCalc(11,12,.t.)
            .w_SIMVAL = IIF(EMPTY(.w_CODVAL),' ',.w_SIMVAL)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCELSTAM_1_1.RadioValue()==this.w_SCELSTAM)
      this.oPgFrm.Page1.oPag.oSCELSTAM_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPREGNO_1_3.value==this.w_PREGNO)
      this.oPgFrm.Page1.oPag.oPREGNO_1_3.value=this.w_PREGNO
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_11.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_11.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG1_1_13.value==this.w_DATREG1)
      this.oPgFrm.Page1.oPag.oDATREG1_1_13.value=this.w_DATREG1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI1_1_14.value==this.w_DATINI1)
      this.oPgFrm.Page1.oPag.oDATINI1_1_14.value=this.w_DATINI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN1_1_15.value==this.w_DATFIN1)
      this.oPgFrm.Page1.oPag.oDATFIN1_1_15.value=this.w_DATFIN1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_16.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_16.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_24.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_24.value=this.w_SIMVAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsve_svePag1 as StdContainer
  Width  = 433
  height = 162
  stdWidth  = 433
  stdheight = 162
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSCELSTAM_1_1 as StdCombo with uid="ASFBUJQIAA",rtseq=1,rtrep=.f.,left=148,top=7,width=134,height=21;
    , ToolTipText = "Tipo stampa selezionata";
    , HelpContextID = 118908531;
    , cFormVar="w_SCELSTAM",RowSource=""+"Fondo previdenza,"+"Fondo assistenza,"+"F.I.R.R.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSCELSTAM_1_1.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'A',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oSCELSTAM_1_1.GetRadio()
    this.Parent.oContained.w_SCELSTAM = this.RadioValue()
    return .t.
  endfunc

  func oSCELSTAM_1_1.SetRadio()
    this.Parent.oContained.w_SCELSTAM=trim(this.Parent.oContained.w_SCELSTAM)
    this.value = ;
      iif(this.Parent.oContained.w_SCELSTAM=='P',1,;
      iif(this.Parent.oContained.w_SCELSTAM=='A',2,;
      iif(this.Parent.oContained.w_SCELSTAM=='F',3,;
      0)))
  endfunc


  add object oBtn_1_2 as StdButton with uid="SWIOFZSYZC",left=286, top=43, width=18,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Zoom di selezione";
    , HelpContextID = 40313130;
  , bGlobalFont=.t.

    proc oBtn_1_2.Click()
      with this.Parent.oContained
        do (this.Parent.oContained.W_SCELMASK) with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPREGNO_1_3 as StdField with uid="JGRJBWJYIM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PREGNO", cQueryName = "PREGNO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 29455606,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=148, Top=41, cSayPict='"999999"', cGetPict='"999999"'

  add object oANNO_1_11 as StdField with uid="NXDICIFJHQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 34996218,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=231, Top=41, InputMask=replicate('X',4)

  add object oDATREG1_1_13 as StdField with uid="VRGVCJGGOW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATREG1", cQueryName = "DATREG1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 155013942,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=347, Top=41

  add object oDATINI1_1_14 as StdField with uid="XFJOMXHMOY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATINI1", cQueryName = "DATINI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 197415734,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=148, Top=72

  add object oDATFIN1_1_15 as StdField with uid="QCKHMTYEMP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATFIN1", cQueryName = "DATFIN1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 7426870,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=253, Top=72

  add object oCODVAL_1_16 as StdField with uid="RZNSHSJOTL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 234905894,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=148, Top=103, InputMask=replicate('X',3)


  add object oBtn_1_17 as StdButton with uid="RCKKUXUUCR",left=378, top=113, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 33196730;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="LBJUMGNFPN",left=327, top=113, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 40485402;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        do GSVE_BSV with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CODSER))
      endwith
    endif
  endfunc


  add object oObj_1_23 as cp_runprogram with uid="FSAHLBIPGX",left=81, top=179, width=253,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="gsve_bvs",;
    cEvent = "w_SCELSTAM Changed",;
    nPag=1;
    , HelpContextID = 137483494

  add object oSIMVAL_1_24 as StdField with uid="RXTKRSGZFI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 234941478,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=186, Top=103, InputMask=replicate('X',5)

  add object oStr_1_5 as StdString with uid="KRZENEPNWR",Visible=.t., Left=6, Top=72,;
    Alignment=1, Width=139, Height=15,;
    Caption="Provvigioni pagate dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="QHORXGNLDV",Visible=.t., Left=19, Top=38,;
    Alignment=1, Width=126, Height=18,;
    Caption="Registrazione n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="MJDZURGDRB",Visible=.t., Left=19, Top=103,;
    Alignment=1, Width=126, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="TJRHMXJIJZ",Visible=.t., Left=228, Top=72,;
    Alignment=1, Width=22, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="RYRRJUMYEB",Visible=.t., Left=222, Top=41,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="CXELTIOWBR",Visible=.t., Left=309, Top=41,;
    Alignment=1, Width=36, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="UTYPCIOPIC",Visible=.t., Left=19, Top=7,;
    Alignment=1, Width=126, Height=15,;
    Caption="Scelta stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_sve','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
