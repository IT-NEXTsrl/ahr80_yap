* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kex                                                        *
*              Export verso nuova sede                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [101] [VRS_52]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-19                                                      *
* Last revis.: 2017-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_kex
* --- Funzionalit� disponibile sono
* --- se blocco sistema attivo..
if not cp_IsAdministrator(.F.)
   Ah_ErrorMsg("Procedura utilizzabile solo da utenti con privilegi di amministratore%0Rivolgersi al proprio amministratore di sistema",'!')
   return
endif
if g_LOCKALL=.F.
   Ah_ErrorMsg("L'importazione pu� avvenire solo con il sistema in manutenzione%0Portare il sistema in manutenzione per svolgere l'operazione")
   return
endif
* --- Fine Area Manuale
return(createobject("tgsut_kex",oParentObject))

* --- Class definition
define class tgsut_kex as StdForm
  Top    = 27
  Left   = 53

  * --- Standard Properties
  Width  = 499
  Height = 381+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-07-27"
  HelpContextID=65618793
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kex"
  cComment = "Export verso nuova sede"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PATH = space(254)
  w_PWD = space(254)
  w_VERBOSE = .F.
  w_Msg = space(0)
  w_SELEZI = space(1)
  w_SELEZ1 = space(1)
  o_SELEZ1 = space(1)
  w_TABPRINC2 = space(50)
  w_TABPRINC = space(50)
  w_CPWARN = space(1)
  w_ZOOM = .NULL.
  w_ZOOMA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kexPag1","gsut_kex",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Log elaborazione")
      .Pages(2).addobject("oPag","tgsut_kexPag2","gsut_kex",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Archivi multi aziendali")
      .Pages(3).addobject("oPag","tgsut_kexPag3","gsut_kex",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Archivi aziendali")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPATH_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(2).oPag.ZOOM
    this.w_ZOOMA = this.oPgFrm.Pages(3).oPag.ZOOMA
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      this.w_ZOOMA = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSUT_BEX(this,"E")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PATH=space(254)
      .w_PWD=space(254)
      .w_VERBOSE=.f.
      .w_Msg=space(0)
      .w_SELEZI=space(1)
      .w_SELEZ1=space(1)
      .w_TABPRINC2=space(50)
      .w_TABPRINC=space(50)
      .w_CPWARN=space(1)
      .oPgFrm.Page2.oPag.ZOOM.Calculate()
          .DoRTCalc(1,4,.f.)
        .w_SELEZI = 'F'
      .oPgFrm.Page3.oPag.ZOOMA.Calculate()
        .w_SELEZ1 = 'S'
        .w_TABPRINC2 = Alltrim(Nvl(.w_ZOOMA.GetVar('FILENAME'),''))
        .w_TABPRINC = Alltrim(Nvl(.w_ZOOM.GetVar('FILENAME'),''))
        .w_CPWARN = "S"
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page2.oPag.ZOOM.Calculate()
        .oPgFrm.Page3.oPag.ZOOMA.Calculate()
        if .o_SELEZ1<>.w_SELEZ1
          .Calculate_HYMNBRGWUI()
        endif
        .DoRTCalc(1,6,.t.)
            .w_TABPRINC2 = Alltrim(Nvl(.w_ZOOMA.GetVar('FILENAME'),''))
            .w_TABPRINC = Alltrim(Nvl(.w_ZOOM.GetVar('FILENAME'),''))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZOOM.Calculate()
        .oPgFrm.Page3.oPag.ZOOMA.Calculate()
    endwith
  return

  proc Calculate_KLUATRIGSL()
    with this
          * --- Determina zoom pag2
          gsut_bex(this;
              ,'Z';
             )
    endwith
  endproc
  proc Calculate_PXJWQLCAUQ()
    with this
          * --- Applica selezione init
          GSUT_BEX(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_CEZFNMATPR()
    with this
          * --- Dsitrugge temporaneo all'uscita
          gsut_bex(this;
              ,'D';
             )
    endwith
  endproc
  proc Calculate_YLTXLWIVMH()
    with this
          * --- Applica selezione
          GSUT_BEX(this;
              ,'S';
             )
    endwith
  endproc
  proc Calculate_HYMNBRGWUI()
    with this
          * --- Applica selezione selez1
          GSUT_BEX(this;
              ,'1';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_4.enabled = this.oPgFrm.Page3.oPag.oBtn_3_4.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_6.enabled = this.oPgFrm.Page2.oPag.oBtn_2_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("FormLoad")
          .Calculate_KLUATRIGSL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_PXJWQLCAUQ()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.ZOOM.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_CEZFNMATPR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_SELEZI Changed")
          .Calculate_YLTXLWIVMH()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.ZOOMA.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPATH_1_1.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_1.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oPWD_1_2.value==this.w_PWD)
      this.oPgFrm.Page1.oPag.oPWD_1_2.value=this.w_PWD
    endif
    if not(this.oPgFrm.Page1.oPag.oVERBOSE_1_3.RadioValue()==this.w_VERBOSE)
      this.oPgFrm.Page1.oPag.oVERBOSE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMsg_1_8.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_8.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_3.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oSELEZ1_3_2.RadioValue()==this.w_SELEZ1)
      this.oPgFrm.Page3.oPag.oSELEZ1_3_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCPWARN_2_8.RadioValue()==this.w_CPWARN)
      this.oPgFrm.Page2.oPag.oCPWARN_2_8.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SELEZI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oSELEZI_2_3.SetFocus()
            i_bnoObbl = !empty(.w_SELEZI)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SELEZ1 = this.w_SELEZ1
    return

enddefine

* --- Define pages as container
define class tgsut_kexPag1 as StdContainer
  Width  = 495
  height = 381
  stdWidth  = 495
  stdheight = 381
  resizeXpos=315
  resizeYpos=170
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPATH_1_1 as StdField with uid="IIKXZJUYWL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Path di creazione/posizione del file compresso contenente i DBF con i dati delle tabelle da esportare/importare",;
    HelpContextID = 60538122,;
   bGlobalFont=.t.,;
    Height=21, Width=378, Left=102, Top=7, InputMask=replicate('X',254), bHasZoom = .t. 

  func oPATH_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(Upper(Right(Alltrim(.w_PATH),4))='.AHZ')
         bRes=(cp_WarningMsg(thisform.msgFmt("Indicare estensione '.Ahz'")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  proc oPATH_1_1.mZoom
    this.parent.oContained.w_PATH=GetFile( "AHZ" )
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPWD_1_2 as StdField with uid="FPCABYYWEI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PWD", cQueryName = "PWD",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale password per criptare/descriptare il file zip",;
    HelpContextID = 65316618,;
   bGlobalFont=.t.,;
    Height=21, Width=378, Left=102, Top=39, InputMask=replicate('X',254), bHasZoom = .t. , PasswordChar="*"

  proc oPWD_1_2.mZoom
    MostraPwd(this.parent.oContained, "w_PWD")
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oVERBOSE_1_3 as StdCheck with uid="FVKZVFABRD",rtseq=3,rtrep=.f.,left=418, top=63, caption="Verbose",;
    ToolTipText = "Se attivo il log mostra tutte le frasi svolte sul database",;
    HelpContextID = 196204714,;
    cFormVar="w_VERBOSE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVERBOSE_1_3.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oVERBOSE_1_3.GetRadio()
    this.Parent.oContained.w_VERBOSE = this.RadioValue()
    return .t.
  endfunc

  func oVERBOSE_1_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_VERBOSE==.T.,1,;
      0)
  endfunc


  add object oBtn_1_5 as StdButton with uid="CZRHGAVNYF",left=381, top=335, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 65590042;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSUT_BEX(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_PATH))
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="NYQDDFCUPJ",left=435, top=335, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 58301370;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMsg_1_8 as StdMemo with uid="QYBPPIIQMH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 65166138,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=243, Width=468, Left=16, Top=86, tabstop = .f., readonly = .t.


  add object oBtn_1_9 as StdButton with uid="QKNERQNDZN",left=15, top=335, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare il log di elaborazione";
    , HelpContextID = 224140506;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        STRTOFILE(.w_MSG, PUTFILE('',cp_MsgFormat("Log_Elaborazione"),"TXT"))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_Msg ))
      endwith
    endif
  endfunc

  add object oStr_1_4 as StdString with uid="ANZCMLXCSI",Visible=.t., Left=12, Top=7,;
    Alignment=1, Width=88, Height=18,;
    Caption="File:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="DQFWWAPUQY",Visible=.t., Left=12, Top=39,;
    Alignment=1, Width=88, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsut_kexPag2 as StdContainer
  Width  = 495
  height = 381
  stdWidth  = 495
  stdheight = 381
  resizeXpos=378
  resizeYpos=307
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOM as cp_szoombox with uid="TBADBGGCGT",left=2, top=10, width=496,height=314,;
    caption='ZOOM',;
   bGlobalFont=.t.,;
    cTable="TMPVEND1",bOptions=.t.,cZoomFile="GSUT_KEX",bReadOnly=.t.,bQueryOnLoad=.f.,bAdvOptions=.f.,cZoomOnZoom="",cMenuFile="",;
    cEvent = "Init",;
    nPag=2;
    , HelpContextID = 60227178


  add object oSELEZI_2_3 as StdCombo with uid="DAAGJUSWAC",rtseq=5,rtrep=.f.,left=199,top=330,width=175,height=21;
    , ToolTipText = "Selezione archivi multiaziendali (gli archivi multiaziendali selezionati saranno esportati, quelli filtrabili escludendo le informazioni delle altre aziende presenti)";
    , HelpContextID = 184600358;
    , cFormVar="w_SELEZI",RowSource=""+"Nessuno,"+"Filtrabili per azienda,"+"Non filtrabili per azienda,"+"Tutti", bObbl = .t. , nPag = 2;
  , bGlobalFont=.t.


  func oSELEZI_2_3.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oSELEZI_2_3.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_3.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='N',1,;
      iif(this.Parent.oContained.w_SELEZI=='F',2,;
      iif(this.Parent.oContained.w_SELEZI=='G',3,;
      iif(this.Parent.oContained.w_SELEZI=='T',4,;
      0))))
  endfunc


  add object oBtn_2_6 as StdButton with uid="MERWEIWGGT",left=440, top=333, width=48,height=45,;
    CpPicture="BMP\treeview.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per visualizzare la treeview delle relazioni della tabella selezionata";
    , HelpContextID = 220130836;
    , TabStop=.f.,Caption='\<Relazioni';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_6.Click()
      with this.Parent.oContained
        GSUT1BCR(this.Parent.oContained,"Export1")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_TABPRINC))
      endwith
    endif
  endfunc

  add object oCPWARN_2_8 as StdCheck with uid="FALRNQLIWY",rtseq=9,rtrep=.f.,left=199, top=352, caption="Tabella progressivi",;
    ToolTipText = "Attivo: esporta anche la tabella progressivi",;
    HelpContextID = 259883302,;
    cFormVar="w_CPWARN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCPWARN_2_8.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oCPWARN_2_8.GetRadio()
    this.Parent.oContained.w_CPWARN = this.RadioValue()
    return .t.
  endfunc

  func oCPWARN_2_8.SetRadio()
    this.Parent.oContained.w_CPWARN=trim(this.Parent.oContained.w_CPWARN)
    this.value = ;
      iif(this.Parent.oContained.w_CPWARN=="S",1,;
      0)
  endfunc

  add object oStr_2_4 as StdString with uid="QHIQRXWGFP",Visible=.t., Left=3, Top=331,;
    Alignment=1, Width=191, Height=18,;
    Caption="Selezione archivi multi aziendali:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsut_kexPag3 as StdContainer
  Width  = 495
  height = 381
  stdWidth  = 495
  stdheight = 381
  resizeXpos=385
  resizeYpos=292
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMA as cp_szoombox with uid="GEDXXDWEYR",left=1, top=5, width=492,height=326,;
    caption='ZOOMA',;
   bGlobalFont=.t.,;
    cTable="TMPVEND2",bOptions=.t.,cZoomFile="GSUT_KEX",bReadOnly=.t.,bQueryOnLoad=.f.,bAdvOptions=.f.,cZoomOnZoom="",cMenuFile="",;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 260505194

  add object oSELEZ1_3_2 as StdRadio with uid="RMUJOFEZEL",rtseq=6,rtrep=.f.,left=6, top=334, width=136,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le tabelle";
    , cFormVar="w_SELEZ1", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oSELEZ1_3_2.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 50382630
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 50382630
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le tabelle")
      StdRadio::init()
    endproc

  func oSELEZ1_3_2.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZ1_3_2.GetRadio()
    this.Parent.oContained.w_SELEZ1 = this.RadioValue()
    return .t.
  endfunc

  func oSELEZ1_3_2.SetRadio()
    this.Parent.oContained.w_SELEZ1=trim(this.Parent.oContained.w_SELEZ1)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZ1=='S',1,;
      iif(this.Parent.oContained.w_SELEZ1=='D',2,;
      0))
  endfunc


  add object oBtn_3_4 as StdButton with uid="ORSWOUHUED",left=440, top=335, width=48,height=45,;
    CpPicture="BMP\treeview.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per visualizzare la treeview delle relazioni della tabella selezionata";
    , HelpContextID = 220130836;
    , TabStop=.f.,Caption='\<Relazioni';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_4.Click()
      with this.Parent.oContained
        GSUT1BCR(this.Parent.oContained,"Export2")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_TABPRINC2))
      endwith
    endif
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kex','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kex
Proc MostraPwd(pParent, cVar)
   local oVar
   oVar = pParent.GetCtrl(cVar)
   AH_ERRORMSG("Password: %1","i",,Alltrim(oVar.value))
   pParent.&cVar=oVar.value
  oVar=.null.
EndProc
* --- Fine Area Manuale
