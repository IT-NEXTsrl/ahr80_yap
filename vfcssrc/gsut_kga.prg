* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kga                                                        *
*              Gestione conversioni                                            *
*                                                                              *
*      Author: Zucchetti TAM SpA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_75]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-05-13                                                      *
* Last revis.: 2013-09-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_kga
if not cp_IsAdministrator(.F.)
   Ah_ErrorMsg("Procedura utilizzabile solo da utenti con privilegi di amministratore%0Rivolgersi al proprio amministratore di sistema",'!')
   return
endif
if g_LOCKALL=.F.
   Ah_ErrorMsg("La procedura di conversione pu� avvenire solo con il sistema in manutenzione%0Portare il sistema in manutenzione e ripetere l'operazione")
   return
endif
* --- Fine Area Manuale
return(createobject("tgsut_kga",oParentObject))

* --- Class definition
define class tgsut_kga as StdForm
  Top    = 1
  Left   = 3

  * --- Standard Properties
  Width  = 772
  Height = 470
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-09-06"
  HelpContextID=236371095
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  AHEPATCH_IDX = 0
  CONVERSI_IDX = 0
  cPrg = "gsut_kga"
  cComment = "Gestione conversioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_FCODREL = space(15)
  w_FRELE = space(10)
  w_FPATC = 0
  w_FRIPET = space(1)
  w_FOBBLI = space(1)
  w_SELEZI = space(1)
  w_CODREL = space(15)
  w_ORDINE = space(5)
  w_LMESSAGGIO = space(0)
  w_DESCON = space(100)
  w_CalcZoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kgaPag1","gsut_kga",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFCODREL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CalcZoom = this.oPgFrm.Pages(1).oPag.CalcZoom
    DoDefault()
    proc Destroy()
      this.w_CalcZoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AHEPATCH'
    this.cWorkTables[2]='CONVERSI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FCODREL=space(15)
      .w_FRELE=space(10)
      .w_FPATC=0
      .w_FRIPET=space(1)
      .w_FOBBLI=space(1)
      .w_SELEZI=space(1)
      .w_CODREL=space(15)
      .w_ORDINE=space(5)
      .w_LMESSAGGIO=space(0)
      .w_DESCON=space(100)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_FCODREL))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,3,.f.)
        .w_FRIPET = 'X'
        .w_FOBBLI = 'S'
      .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .w_CODREL = .w_CalcZoom.GetVar('COCODREL')
        .w_ORDINE = .w_CalcZoom.GetVar('COORDINE')
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_ORDINE))
          .link_1_18('Full')
        endif
    endwith
    this.DoRTCalc(9,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .DoRTCalc(1,6,.t.)
            .w_CODREL = .w_CalcZoom.GetVar('COCODREL')
            .w_ORDINE = .w_CalcZoom.GetVar('COORDINE')
          .link_1_18('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.CalcZoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FCODREL
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AHEPATCH_IDX,3]
    i_lTable = "AHEPATCH"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AHEPATCH_IDX,2], .t., this.AHEPATCH_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AHEPATCH_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FCODREL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AHEPATCH')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PTCODREL like "+cp_ToStrODBC(trim(this.w_FCODREL)+"%");

          i_ret=cp_SQL(i_nConn,"select PTCODREL,PTRELEAS,PTNUMPAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PTCODREL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PTCODREL',trim(this.w_FCODREL))
          select PTCODREL,PTRELEAS,PTNUMPAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PTCODREL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FCODREL)==trim(_Link_.PTCODREL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FCODREL) and !this.bDontReportError
            deferred_cp_zoom('AHEPATCH','*','PTCODREL',cp_AbsName(oSource.parent,'oFCODREL_1_1'),i_cWhere,'',"Patch",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTCODREL,PTRELEAS,PTNUMPAT";
                     +" from "+i_cTable+" "+i_lTable+" where PTCODREL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTCODREL',oSource.xKey(1))
            select PTCODREL,PTRELEAS,PTNUMPAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FCODREL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTCODREL,PTRELEAS,PTNUMPAT";
                   +" from "+i_cTable+" "+i_lTable+" where PTCODREL="+cp_ToStrODBC(this.w_FCODREL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTCODREL',this.w_FCODREL)
            select PTCODREL,PTRELEAS,PTNUMPAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FCODREL = NVL(_Link_.PTCODREL,space(15))
      this.w_FRELE = NVL(_Link_.PTRELEAS,space(10))
      this.w_FPATC = NVL(_Link_.PTNUMPAT,0)
    else
      if i_cCtrl<>'Load'
        this.w_FCODREL = space(15)
      endif
      this.w_FRELE = space(10)
      this.w_FPATC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AHEPATCH_IDX,2])+'\'+cp_ToStr(_Link_.PTCODREL,1)
      cp_ShowWarn(i_cKey,this.AHEPATCH_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FCODREL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ORDINE
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONVERSI_IDX,3]
    i_lTable = "CONVERSI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2], .t., this.CONVERSI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORDINE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORDINE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODREL,COORDINE,COERRORE";
                   +" from "+i_cTable+" "+i_lTable+" where COORDINE="+cp_ToStrODBC(this.w_ORDINE);
                   +" and COCODREL="+cp_ToStrODBC(this.w_CODREL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODREL',this.w_CODREL;
                       ,'COORDINE',this.w_ORDINE)
            select COCODREL,COORDINE,COERRORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORDINE = NVL(_Link_.COORDINE,space(5))
      this.w_LMESSAGGIO = NVL(_Link_.COERRORE,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_ORDINE = space(5)
      endif
      this.w_LMESSAGGIO = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2])+'\'+cp_ToStr(_Link_.COCODREL,1)+'\'+cp_ToStr(_Link_.COORDINE,1)
      cp_ShowWarn(i_cKey,this.CONVERSI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORDINE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFCODREL_1_1.value==this.w_FCODREL)
      this.oPgFrm.Page1.oPag.oFCODREL_1_1.value=this.w_FCODREL
    endif
    if not(this.oPgFrm.Page1.oPag.oFRELE_1_2.value==this.w_FRELE)
      this.oPgFrm.Page1.oPag.oFRELE_1_2.value=this.w_FRELE
    endif
    if not(this.oPgFrm.Page1.oPag.oFPATC_1_3.value==this.w_FPATC)
      this.oPgFrm.Page1.oPag.oFPATC_1_3.value=this.w_FPATC
    endif
    if not(this.oPgFrm.Page1.oPag.oFRIPET_1_4.RadioValue()==this.w_FRIPET)
      this.oPgFrm.Page1.oPag.oFRIPET_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFOBBLI_1_5.RadioValue()==this.w_FOBBLI)
      this.oPgFrm.Page1.oPag.oFOBBLI_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_9.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLMESSAGGIO_1_19.value==this.w_LMESSAGGIO)
      this.oPgFrm.Page1.oPag.oLMESSAGGIO_1_19.value=this.w_LMESSAGGIO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_20.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_20.value=this.w_DESCON
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kgaPag1 as StdContainer
  Width  = 768
  height = 470
  stdWidth  = 768
  stdheight = 470
  resizeXpos=222
  resizeYpos=322
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFCODREL_1_1 as StdField with uid="RIHIRTLKDP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FCODREL", cQueryName = "FCODREL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice release",;
    HelpContextID = 125832106,;
   bGlobalFont=.t.,;
    Height=21, Width=124, Left=88, Top=9, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="AHEPATCH", oKey_1_1="PTCODREL", oKey_1_2="this.w_FCODREL"

  func oFCODREL_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oFCODREL_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFCODREL_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AHEPATCH','*','PTCODREL',cp_AbsName(this.parent,'oFCODREL_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Patch",'',this.parent.oContained
  endproc

  add object oFRELE_1_2 as StdField with uid="WWBTRMQASL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FRELE", cQueryName = "FRELE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Release",;
    HelpContextID = 222862506,;
   bGlobalFont=.t.,;
    Height=21, Width=95, Left=282, Top=9, InputMask=replicate('X',10)

  add object oFPATC_1_3 as StdField with uid="ONUTNTEVGQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FPATC", cQueryName = "FPATC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero patch",;
    HelpContextID = 224452266,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=456, Top=9, cSayPict='"@Z 999"', cGetPict='"@Z 999"'

  add object oFRIPET_1_4 as StdCheck with uid="WXHLMOEFDF",rtseq=4,rtrep=.f.,left=507, top=9, caption="Ripetibili gi� eseguite",;
    ToolTipText = "Se attivo: ricerca anche le conversioni gi� eseguite ma ripetibili",;
    HelpContextID = 155475114,;
    cFormVar="w_FRIPET", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFRIPET_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'X'))
  endfunc
  func oFRIPET_1_4.GetRadio()
    this.Parent.oContained.w_FRIPET = this.RadioValue()
    return .t.
  endfunc

  func oFRIPET_1_4.SetRadio()
    this.Parent.oContained.w_FRIPET=trim(this.Parent.oContained.w_FRIPET)
    this.value = ;
      iif(this.Parent.oContained.w_FRIPET=='S',1,;
      0)
  endfunc

  add object oFOBBLI_1_5 as StdCheck with uid="TRACEJJRLW",rtseq=5,rtrep=.f.,left=507, top=31, caption="Solo obbligatorie",;
    ToolTipText = "Se attivo: ricerca le sole conversioni obbligatorie",;
    HelpContextID = 65195946,;
    cFormVar="w_FOBBLI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFOBBLI_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFOBBLI_1_5.GetRadio()
    this.Parent.oContained.w_FOBBLI = this.RadioValue()
    return .t.
  endfunc

  func oFOBBLI_1_5.SetRadio()
    this.Parent.oContained.w_FOBBLI=trim(this.Parent.oContained.w_FOBBLI)
    this.value = ;
      iif(this.Parent.oContained.w_FOBBLI=='S',1,;
      0)
  endfunc


  add object oBtn_1_6 as StdButton with uid="TJGKSVEUGA",left=662, top=7, width=48,height=45,;
    CpPicture="BMP\ORIGINE.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire manutenzione tabella conversioni";
    , HelpContextID = 51692518;
    , Caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        GSUT_BGA(this.Parent.oContained,"ORIG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="FPAUDSVDKI",left=714, top=7, width=48,height=45,;
    CpPicture="BMP\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Riesegue l'interrogazione";
    , HelpContextID = 140282371;
    , Caption='\<Interroga', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSUT_BGA(this.Parent.oContained,"CALC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object CalcZoom as cp_szoombox with uid="ZRNYOQOCKV",left=1, top=60, width=765,height=344,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='CONVERSI',cZoomFile='GSUT_KGA',bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,;
    cEvent = "Legge",;
    nPag=1;
    , HelpContextID = 122502170

  add object oSELEZI_1_9 as StdRadio with uid="NIGCHCKNSU",rtseq=6,rtrep=.f.,left=11, top=408, width=140,height=32;
    , ToolTipText = "Seleziona/deseleziona tutti i record alla cancellazione";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_9.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 50280666
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 50280666
      this.Buttons(2).Top=15
      this.SetAll("Width",138)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutti i record alla cancellazione")
      StdRadio::init()
    endproc

  func oSELEZI_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_9.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_9.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_10 as StdButton with uid="JDOTXLNMID",left=714, top=413, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 243688518;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_11 as cp_runprogram with uid="QNTSKLACGW",left=13, top=474, width=214,height=18,;
    caption='GSUT_BGA',;
   bGlobalFont=.t.,;
    prg='GSUT_BGA("SELE")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 106980519


  add object oObj_1_12 as cp_runprogram with uid="CUIKYTQALT",left=231, top=474, width=183,height=18,;
    caption='GSUT_BGA',;
   bGlobalFont=.t.,;
    prg='GSUT_BGA("CALC")',;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 106980519


  add object oBtn_1_13 as StdButton with uid="HTWZHBFLOY",left=662, top=413, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire le conversioni selezionate";
    , HelpContextID = 236399846;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSUT_BGA(this.Parent.oContained,"AGGIO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oLMESSAGGIO_1_19 as StdMemo with uid="EMRMYUICEY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_LMESSAGGIO", cQueryName = "LMESSAGGIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 77509261,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=54, Width=395, Left=166, Top=408

  add object oDESCON_1_20 as StdField with uid="JESLLPCBMU",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione conversione",;
    HelpContextID = 246467018,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=88, Top=35, InputMask=replicate('X',100)

  add object oStr_1_14 as StdString with uid="DNBHGZIKFW",Visible=.t., Left=380, Top=9,;
    Alignment=1, Width=73, Height=15,;
    Caption="Num.patch:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="BKMYPNGZXB",Visible=.t., Left=219, Top=9,;
    Alignment=1, Width=60, Height=15,;
    Caption="Release:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="TXWXNHVNKT",Visible=.t., Left=6, Top=9,;
    Alignment=1, Width=80, Height=15,;
    Caption="Cod.release:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="JLRFZYWJJA",Visible=.t., Left=18, Top=37,;
    Alignment=0, Width=68, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kga','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
