* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste1bmb                                                        *
*              Salvataggio Mandati SDD                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-08-02                                                      *
* Last revis.: 2013-09-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSerialeMandato,w_Bottone,w_SaveSerial,w_PositionPage
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste1bmb",oParentObject,m.pSerialeMandato,m.w_Bottone,m.w_SaveSerial,m.w_PositionPage)
return(i_retval)

define class tgste1bmb as StdBatch
  * --- Local variables
  pSerialeMandato = space(10)
  w_Bottone = .NULL.
  w_SaveSerial = .f.
  w_PositionPage = .f.
  w_Seriale = space(10)
  * --- WorkFile variables
  MANDDATI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato per gestire il salvataggio dell'anagrafica dei "Mandati SDD"
    if this.oParentObject.w_MATIPMAN<>"D"
      i_retcode = 'stop'
      return
    endif
    if this.w_PositionPage
      btrserr=.F. 
 this.oParentobject.EcpSave()
      * --- Se si � verificato un errore al salvataggio allora il record non � stato salvato e non bisogna mettersi in modifica ma rimanere in caricamento
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.pSerialeMandato)
      i_retcode = 'stop'
      return
    endif
    if this.w_SaveSerial
      this.oParentObject.w_NEW_SERIAL = this.oParentObject.w_Macodice
      i_retcode = 'stop'
      return
    endif
    * --- Nome del bottone che ha lanciato questo batch
    *     Variabili
    this.w_Seriale = this.pSerialeMandato
    * --- Creazione cursore delle chiavi
    this.oParentobject.QueryKeySet( "MACODICE="+cp_ToStrODBC(this.w_SERIALE) , "" )
    * --- Caricamento record ed accesso in modifica
    this.oParentobject.LoadRecWarn()
    * --- Posizionamento sulla seconda pagina
    this.oParentObject.w_NEW_SERIAL = space(10)
  endproc


  proc Init(oParentObject,pSerialeMandato,w_Bottone,w_SaveSerial,w_PositionPage)
    this.pSerialeMandato=pSerialeMandato
    this.w_Bottone=w_Bottone
    this.w_SaveSerial=w_SaveSerial
    this.w_PositionPage=w_PositionPage
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MANDDATI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSerialeMandato,w_Bottone,w_SaveSerial,w_PositionPage"
endproc
