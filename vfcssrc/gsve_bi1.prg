* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bi1                                                        *
*              Eventi da import documenti                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_230]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2017-10-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEvent
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bi1",oParentObject,m.pEvent)
return(i_retval)

define class tgsve_bi1 as StdBatch
  * --- Local variables
  pEvent = space(3)
  w_ZOOM = .NULL.
  w_CHKMAST = 0
  w_SERDETT = space(10)
  w_CHKEVAS = space(1)
  w_MESS = space(10)
  w_CHKACCO = space(1)
  w_APPO = space(10)
  w_APPO2 = space(10)
  w_APPO1 = space(10)
  w_MESS1 = space(10)
  w_GSVE_KIM = .NULL.
  w_GEST = .NULL.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_IDRECATT = 0
  w_VALDAATT = space(30)
  w_IDFLDAGG = 0
  w_SERIALMAST = space(10)
  w_BMESSAGG01 = .f.
  w_MESSAGG01 = space(254)
  w_BMESSAGG02 = .f.
  w_MESSAGG02 = space(254)
  w_BMESSAGG03 = .f.
  w_MESSAGG03 = space(254)
  w_BMESSAGG04 = .f.
  w_MESSAGG04 = space(254)
  w_BMESSAGG05 = .f.
  w_MESSAGG05 = space(254)
  w_BMESSAGG06 = .f.
  w_MESSAGG06 = space(254)
  w_QTAEVA = 0
  w_EVAPRZ = 0
  w_NumRiga = 0
  w_RIFCAC = 0
  w_TMPQTA = 0
  w_CAMINVEN = 0
  w_ARMINVEN = 0
  w_RMINVEN = 0
  w_OPERA3 = space(1)
  w_UNMIS3 = space(3)
  w_MOLTI3 = 0
  w_MVQTAEVA = 0
  w_MVUNIMIS = space(3)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_QTAEV1 = 0
  w_ERRMIN = .f.
  w_CONTARIG = 0
  w_TOTRIGHE = 0
  w_CODICE = space(41)
  w_CODART = space(20)
  w_CATIPCON = space(1)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue eventi da Selezione/Spostamento Riga Master (da GSVE_KIM)
    * --- Eventi: 
    *     MRC - Master Row Checked 
    *     MRU - Master Row UnChecked
    *     DRC - Detail Row Checked
    *     DRU - Detail Row UnChecked
    *     DBQ - Before Query Detail 
    *     DAQ - After Query Detail
    *     SCH - w_SELEZI Changed ( Seleziona/Deseleziona Tutti del Detail)
    *     AMC - FindDocError (da GSVE_BI4)
    *     FNR - Selezionare i primi N record del dettaglio. 
    *                Gli N record sono appunto indicati dalla Caller w_SCOPE
    * --- istanzio oggetto per mess. incrementali
    this.w_oMESS=createobject("ah_message")
    this.w_CHKMAST = -1
    this.w_GSVE_KIM = this.oParentObject
    this.w_GEST = this.w_GSVE_KIM.oParentObject
    L_Cur_Ev_Row = this.w_GEST.w_Cur_Ev_Row
    L_Cur_Rag_Row = this.w_GEST.w_Cur_Rag_Row
    ND = this.w_GSVE_KIM.w_ZoomDett.cCursor
    NM = this.w_GSVE_KIM.w_ZoomMast.cCursor
    do case
      case this.pEvent = "MRC" OR this.pEvent = "AMC"
        * --- Eseguito il Check sul Master o
        *     Eseguito il Check su tutte le rige del Master w_SELEZI1 Changed con SELEZI1 ='S'
        * --- Inserisce il Flag Selezionato sulle righe del Detail
        this.oParentObject.w_ERR = .F.
        do case
          case (&NM..FLCONG <> " " Or ( this.oParentObject.w_BLOCKMAST <>0 And this.oParentObject.w_FLSCAF="S" ) Or this.oParentObject.w_BLOCKMAST =-1) And (this.oParentObject.w_TIPCON = &NM..MVTIPCON Or &NM..FLCONG= "1")
            AA=ALLTRIM(STR(this.w_GSVE_KIM.w_ZoomMast.grd.ColumnCount))
            this.w_GSVE_KIM.w_ZoomMast.grd.Column&AA..chk.Value = 0
            this.w_oMESS.AddMsgPartNL("Documento di origine non selezionabile")     
            do case
              case &NM..FLCONG="1"
                this.w_oMESS.AddMsgPartNL("Valute incongruenti")     
                this.oParentObject.w_ERR = .T.
              case &NM..FLCONG="2" 
                this.w_oMESS.AddMsgPartNL("Codici IVA esenzione incongruenti")     
                this.oParentObject.w_ERR = .T.
              case &NM..FLCONG="3"
                this.w_oMESS.AddMsgPartNL("Dati pagamento incongruenti")     
                this.oParentObject.w_ERR = .T.
              case &NM..FLCONG="4"
                this.w_oMESS.AddMsgPartNL("Sconti di piede incongruenti")     
                this.oParentObject.w_ERR = .T.
              case &NM..FLCONG="5"
                this.w_oMESS.AddMsgPartNL("Banche di riferimento incongruenti")     
                this.oParentObject.w_ERR = .T.
              case &NM..FLCONG="6"
                this.w_oMESS.AddMsgPartNL("Codici agenti incongruenti")     
                this.oParentObject.w_ERR = .T.
              case &NM..FLCONG="7"
                this.w_oMESS.AddMsgPartNL("Dati accompagnatori incongruenti")     
                this.oParentObject.w_ERR = .T.
              case &NM..FLCONG="9"
                this.w_oMESS.AddMsgPartNL("Codici IVA per le spese/imballi incongruenti")     
                this.oParentObject.w_ERR = .T.
              case &NM..FLCONG="0"
                this.w_oMESS.AddMsgPartNL("Codice IVA per il bollo incongruente")     
                this.oParentObject.w_ERR = .T.
              case this.oParentObject.w_BLOCKMAST =-1
                this.w_oMESS.AddMsgPartNL("Selezionando un documento con scadenze fissate non � possibile importare altri documenti")     
                this.oParentObject.w_ERR = .T.
              case ( this.oParentObject.w_BLOCKMAST <>0 And this.oParentObject.w_FLSCAF="S" )
                this.w_oMESS.AddMsgPartNL("Il documento selezionato ha le scadenze fissate%0Non � quindi possibile importarlo con altri documenti")     
                this.oParentObject.w_ERR = .T.
              case &NM..FLCONG="A"
                this.w_oMESS.AddMsgPartNL("Dati aggiuntivi incongruenti")     
                this.oParentObject.w_ERR = .T.
            endcase
            if this.pEvent="AMC"
              * --- Nel caso di chiamata dal batch GSVE_BI4 (evento FindDocError)
              *     devo inserire i messaggi di errore nell'oggetto ah_Errorlog
              if this.oParentObject.w_ERR
                this.oParentObject.w_ERRGEN = .T.
                this.oParentObject.w_oERRORLOG.AddMsgLog("Verificare doc.n. %1 del %2", ALLTRIM(STR(&NM..MVNUMDOC,15))+IIF(EMPTY(&NM..MVALFDOC),"","/"+Alltrim(&NM..MVALFDOC)), DTOC(&NM..MVDATDOC))     
                this.w_MESS = this.w_oMess.ComposeMessage()
                this.oParentObject.w_oERRORLOG.AddMsgLogNoTranslate(this.w_MESS)     
              endif
            else
              if this.oParentObject.w_ERR
                this.w_oMESS.ah_ErrorMsg()     
              endif
            endif
          otherwise
            this.oParentObject.w_ERR = "E" $ this.oParentObject.w_TSTDAAG1+this.oParentObject.w_TSTDAAG2
            if !this.pEvent="AMC" or !this.oParentObject.w_ERR
              if !this.oParentObject.w_ERR
                * --- Verifico che non vi siano altri documenti selezionati con campi aggiuntivi differenti da 
                *     quelli impostati nel documento selezionato
                Local L_MACRO
                this.w_IDFLDAGG = 1
                SELECT (NM)
                this.w_IDRECATT = RECNO()
                do while !this.oParentObject.w_ERR AND this.w_IDFLDAGG<=6 AND XCHK=1 AND this.w_IDRECATT > 1
                  SELECT (NM)
                  GO this.w_IDRECATT
                  L_MACRO = "NVL(MVAGG_" + RIGHT("00"+ALLTRIM(STR(this.w_IDFLDAGG)), 2) + ", " +IIF(this.w_IDFLDAGG<5, " ' ' ", "CTOD('  -  -    ')") + ")"
                  this.w_VALDAATT = &L_MACRO
                  * --- Imposto la macro per verificare se il campo � da considerare o no
                  *     No se non di rottura, Non vuoto se rottura semplice, sempre se rottura tassativa
                  L_MACRO = "this.oParentObject.w_TDFLRA" + RIGHT("00"+ALLTRIM(STR(this.w_IDFLDAGG)), 2) + " = 'A' OR " + "this.oParentObject.w_TDFLRA" + RIGHT("00"+ALLTRIM(STR(this.w_IDFLDAGG)), 2) + " = 'S' AND !EMPTY(this.w_VALDAATT)"
                  if &L_MACRO
                    * --- Cerco negli altri record selezionati se il campo � presente e diverso
                    SELECT (NM)
                    GO TOP
                    L_MACRO = "XCHK=1  AND NVL(MVAGG_" + RIGHT("00"+ALLTRIM(STR(this.w_IDFLDAGG)), 2) + ", " +IIF(this.w_IDFLDAGG<5, " ' ' ", "CTOD('  -  -    ')") + ") <> (this.w_VALDAATT) AND (this.oParentObject.w_TDFLRA" + RIGHT("00"+ALLTRIM(STR(this.w_IDFLDAGG)), 2) + " = 'A' OR (this.oParentObject.w_TDFLRA" + RIGHT("00"+ALLTRIM(STR(this.w_IDFLDAGG)), 2) + " = 'S' AND !EMPTY(NVL(MVAGG_" + RIGHT("00"+ALLTRIM(STR(this.w_IDFLDAGG)), 2) + ", " +IIF(this.w_IDFLDAGG<5, " ' ' ", "CTOD('  -  -    ')") + ") ) ) )"
                    LOCATE FOR &L_MACRO
                    if FOUND()
                      this.oParentObject.w_ERR = .T.
                      this.w_oMESS.AddMsgPartNL("Esiste almeno un altro documento selezionato con uno o pi� dati aggiuntivi incongruenti. Impossibile selezionare il documento")     
                    endif
                  endif
                  this.w_IDFLDAGG = this.w_IDFLDAGG + 1
                enddo
                SELECT (NM)
                GO this.w_IDRECATT
                if this.oParentObject.w_ERR and FLDOCU<>"S"
                  this.w_SERIALMAST = MVSERIAL
                  AA=ALLTRIM(STR(this.w_GSVE_KIM.w_ZoomMast.grd.ColumnCount))
                  this.w_GSVE_KIM.w_ZoomMast.grd.Column&AA..chk.Value = 0
                  UPDATE (ND) SET XCHK=0 , MVQTAEVA=0, MVIMPEVA=0 WHERE &ND..MVSERIAL=this.w_SERIALMAST
                endif
              else
                if !this.w_BMESSAGG01 AND this.oParentObject.w_TDFLIA01="S" AND !EMPTY(NVL(this.oParentObject.w_MVAGG_01, " ")) AND NVL(this.oParentObject.w_MVAGG_01, " ")<>NVL(&NM..MVAGG_01," ")
                  this.w_oPART = this.w_oMESS.addmsgpartNL("Campo aggiuntivo %1 incongruente")
                  this.w_oPART.addParam(ALLTRIM(this.oParentObject.w_DACAM_01))     
                  this.w_BMESSAGG01 = .T.
                  this.oParentObject.w_ERR = .T.
                endif
                if !this.w_BMESSAGG02 AND this.oParentObject.w_TDFLIA02="S" AND !EMPTY(NVL(this.oParentObject.w_MVAGG_02, " ")) AND NVL(this.oParentObject.w_MVAGG_02, " ")<>NVL(MVAGG_02," ")
                  this.w_oPART = this.w_oMESS.addmsgpartNL("Campo aggiuntivo %1 incongruente")
                  this.w_oPART.addParam(ALLTRIM(this.oParentObject.w_DACAM_02))     
                  this.w_BMESSAGG02 = .T.
                  this.oParentObject.w_ERR = .T.
                endif
                if !this.w_BMESSAGG03 AND this.oParentObject.w_TDFLIA03="S" AND !EMPTY(NVL(this.oParentObject.w_MVAGG_03, " ")) AND NVL(this.oParentObject.w_MVAGG_03, " ")<>NVL(MVAGG_03," ")
                  this.w_oPART = this.w_oMESS.addmsgpartNL("Campo aggiuntivo %1 incongruente")
                  this.w_oPART.addParam(ALLTRIM(this.oParentObject.w_DACAM_03))     
                  this.w_BMESSAGG03 = .T.
                  this.oParentObject.w_ERR = .T.
                endif
                if !this.w_BMESSAGG04 AND this.oParentObject.w_TDFLIA04="S" AND !EMPTY(NVL(this.oParentObject.w_MVAGG_04, " ")) AND NVL(this.oParentObject.w_MVAGG_04, " ")<>NVL(MVAGG_04," ")
                  this.w_oPART = this.w_oMESS.addmsgpartNL("Campo aggiuntivo %1 incongruente")
                  this.w_oPART.addParam(ALLTRIM(this.oParentObject.w_DACAM_04))     
                  this.w_BMESSAGG04 = .T.
                  this.oParentObject.w_ERR = .T.
                endif
                if !this.w_BMESSAGG05 AND this.oParentObject.w_TDFLIA05="S" AND !EMPTY(NVL(this.oParentObject.w_MVAGG_05, cp_charToDate("  -  -    "))) AND NVL(this.oParentObject.w_MVAGG_05, cp_charToDate("  -  -    "))<>NVL(MVAGG_05,cp_charToDate("  -  -    "))
                  this.w_oPART = this.w_oMESS.addmsgpartNL("Campo aggiuntivo %1 incongruente")
                  this.w_oPART.addParam(ALLTRIM(this.oParentObject.w_DACAM_05))     
                  this.w_BMESSAGG05 = .T.
                  this.oParentObject.w_ERR = .T.
                endif
                if !this.w_BMESSAGG06 AND this.oParentObject.w_TDFLIA06="S" AND !EMPTY(NVL(this.oParentObject.w_MVAGG_06, cp_charToDate("  -  -    "))) AND NVL(this.oParentObject.w_MVAGG_06, cp_charToDate("  -  -    "))<>NVL(MVAGG_06,cp_charToDate("  -  -    "))
                  this.w_oPART = this.w_oMESS.addmsgpartNL("Campo aggiuntivo %1 incongruente")
                  this.w_oPART.addParam(ALLTRIM(this.oParentObject.w_DACAM_06))     
                  this.w_BMESSAGG06 = .T.
                  this.oParentObject.w_ERR = .T.
                endif
                if this.w_BMESSAGG01 OR this.w_BMESSAGG02 OR this.w_BMESSAGG03 OR this.w_BMESSAGG04 OR this.w_BMESSAGG05 OR this.w_BMESSAGG06
                  SELECT(NM)
                  REPLACE FLCONG WITH "A"
                   a=this.w_GSVE_KIM.w_ZOOMMAST.grd.ColumnCount 
 this.w_GSVE_KIM.w_ZOOMMAST.grd.columns(a).chk.value=0
                  this.w_GSVE_KIM.w_ZOOMMAST.REFRESH()     
                endif
              endif
            endif
            if this.oParentObject.w_TDMINVEN<>"E" AND this.oParentObject.w_TDRIOTOT="R"
              SELECT (NM)
              this.w_IDRECATT = RECNO()
              this.w_SERIALMAST = MVSERIAL
              SELECT (ND)
              GO TOP
              SCAN FOR &ND..MVSERIAL= this.w_SERIALMAST
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              Select(ND)
              ENDSCAN
              SELECT (ND)
              GO TOP
              if this.w_ERRMIN
                if this.w_TOTRIGHE=this.w_CONTARIG
                  AA=ALLTRIM(STR(this.oParentObject.w_ZoomMast.grd.ColumnCount))
                  this.oParentObject.w_ZoomMast.grd.Column&AA..chk.Value = 0
                  SELECT (NM)
                  REPLACE XCHK with 0
                endif
              endif
            endif
            if this.pEvent="AMC"
              * --- Nel caso di chiamata dal batch GSVE_BI4 (evento FindDocError)
              *     devo inserire i messaggi di errore nell'oggetto ah_Errorlog
              if this.oParentObject.w_ERR OR this.w_ERRMIN
                this.oParentObject.w_ERRGEN = .T.
                this.oParentObject.w_oERRORLOG.AddMsgLog("Verificare doc.n. %1 del %2", ALLTRIM(STR(&NM..MVNUMDOC,15))+IIF(EMPTY(&NM..MVALFDOC),"","/"+Alltrim(&NM..MVALFDOC)), DTOC(&NM..MVDATDOC))     
                this.w_MESS = this.w_oMess.ComposeMessage()
                this.oParentObject.w_oERRORLOG.AddMsgLogNoTranslate(this.w_MESS)     
              endif
            else
              if this.oParentObject.w_ERR or this.w_ERRMIN
                this.w_oMESS.ah_ErrorMsg()     
              endif
            endif
            if !this.oParentObject.w_ERR AND !this.pEvent="AMC"
              * --- Aggiorno il cursore del dettaglio e sistemo i valori considerando la parte evasa
              this.w_GSVE_KIM.NotifyEvent("CalcRig")     
              UPDATE (ND) SET XCHK=IIF(NVL(MVTIPRIG," ")="D" or ( NVL(MVTIPRIG," ")="F" and (this.oParentObject.w_FLGZER="S" and VISPRE=0)) ,1, 0), MVQTAEVA=IIF(NVL(MVTIPRIG," ")="D",1, IIF(OLQTAEVA<>0,MVQTAMOV-OLQTAEVA,MVQTAMOV)), ;
              VISPRE=cp_ROUND(VISPRE, IIF(MVCODVAL=g_CODLIR, 0, 5)), MVIMPEVA=IIF(OLIMPEVA<>0 AND MVTIPRIG="F",cp_ROUND(MVPREZZO, IIF(MVCODVAL=g_CODLIR, 0, 5))-OLIMPEVA,cp_ROUND(MVPREZZO, IIF(MVCODVAL=g_CODLIR, 0, 5))) ;
              WHERE(FLDOCU<>"S" OR (FLDOCU="S" AND MVFLERIF<>"S" AND (OLQTAEVA<MVQTAMOV OR ((Abs(OLIMPEVA)<Abs(VISPRE) or (this.oParentObject.w_FLGZER="S" and VISPRE=0)) AND MVTIPRIG="F"))))
              * --- Numero di documenti da controllare per scadenze fissate solo se dello stesso ciclo
              if this.oParentObject.w_TIPCON = &NM..MVTIPCON
                this.oParentObject.w_BLOCKMAST = iif( this.oParentObject.w_FLSCAF="S" , -1 , this.oParentObject.w_BLOCKMAST + 1)
              endif
              this.w_CHKMAST = 1
            endif
        endcase
      case this.pEvent = "MRU" 
        * --- Eseguito l' Uncheck sul Master
        * --- Toglie il Flag Selezionato sulle righe del Detail 
         
 UPDATE (ND) SET XCHK=0, MVQTAEVA=0, MVIMPEVA=0 ; 
 WHERE(FLDOCU<>"S" OR (FLDOCU="S" AND (OLQTAEVA<MVQTAMOV OR ; 
 (Abs(OLIMPEVA)<Abs(VISPRE) AND MVTIPRIG="F"))) )
        this.w_CHKMAST = 0
        if this.oParentObject.w_TIPCON = &NM..MVTIPCON
          * --- Numero di documenti da controllare per scadenze fissate solo se dello stesso ciclo
          this.oParentObject.w_BLOCKMAST = iif( this.oParentObject.w_FLSCAF="S" , 0 , this.oParentObject.w_BLOCKMAST - 1 )
        endif
      case this.pEvent = "DRC"
        * --- Eseguito il Check sul Dettaglio
        * --- Inserisce il Flag Selezionato sulla riga del Detail 
        this.w_QTAEVA = IIF(NVL(&ND..MVTIPRIG," ")="D", 1, IIF(NVL(&ND..OLQTAEVA,0)>0,&ND..MVQTAMOV-NVL(&ND..OLQTAEVA,0),NVL(&ND..MVQTAMOV,0)) )
        this.w_EVAPRZ = IIF(&ND..MVFLERIF="S", 0 , IIF(&ND..OLIMPEVA<>0,cp_ROUND(&ND..MVPREZZO, IIF(&ND..MVCODVAL=g_CODLIR, 0, 5))-&ND..OLIMPEVA,cp_ROUND(&ND..MVPREZZO, IIF(&ND..MVCODVAL=g_CODLIR, 0, 5))))
        do case
          case &ND..FLDOCU = "S" AND ((NVL(&ND..MVQTAMOV,0)<=NVL(&ND..OLQTAEVA,0) AND &ND..MVTIPRIG <>"F") OR (Abs(NVL(&ND..VISPRE,0)); 
 <=Abs(NVL(&ND..OLIMPEVA,0)) AND &ND..MVTIPRIG="F"))
            AA=ALLTRIM(STR(this.w_GSVE_KIM.w_ZoomDett.grd.ColumnCount))
            this.w_GSVE_KIM.w_ZoomDett.grd.Column&AA..chk.Value = 0
            this.w_MESS = "Impossibile selezionare una riga gi� importata totalmente%0Eliminare la riga corrispondente sul documento"
            * --- Controllo Se riga gi� evasa senza Import
            if USED (L_Cur_Ev_Row) And Reccount( L_Cur_Ev_Row )>0
               
 Select( L_Cur_Ev_Row ) 
 Locate for &L_Cur_Ev_Row..RESERRIF = &ND..MVSERIAL And &L_Cur_Ev_Row..REROWRIF = &ND..CPROWNUM
              if Found()
                this.w_MESS = "Impossibile selezionare: riga evasa a valore 0 (senza importazione)%0Se si vuole importare la riga, annullare l'importazione"
              endif
            endif
            if USED (L_Cur_Rag_Row) And Reccount( L_Cur_Rag_Row )>0
               
 Select( L_Cur_Rag_Row ) 
 Locate for &L_Cur_Rag_Row..RESERRIF = &ND..MVSERIAL And &L_Cur_Rag_Row..REROWRIF = &ND..CPROWNUM
              if Found()
                this.w_MESS = "Impossibile selezionare: riga raggruppata nella precedente importazione%0Se si vuole importare la riga, annullare l'importazione"
              endif
            endif
            ah_Msg(this.w_MESS,.T.)
          case &NM..FLCONG <> " "
            AA=ALLTRIM(STR(this.w_GSVE_KIM.w_ZoomDett.grd.ColumnCount))
            this.w_GSVE_KIM.w_ZoomDett.grd.Column&AA..chk.Value = 0
            this.w_oMESS.AddMsgPartNL("Documento di origine non selezionabile")     
            do case
              case &NM..FLCONG="1"
                this.w_oMESS.AddMsgPartNL("Valute incongruenti")     
              case &NM..FLCONG="2"
                this.w_oMESS.AddMsgPartNL("Codici IVA esenzione incongruenti")     
              case &NM..FLCONG="3"
                this.w_oMESS.AddMsgPartNL("Dati pagamento incongruenti")     
              case &NM..FLCONG="4"
                this.w_oMESS.AddMsgPartNL("Sconti di piede incongruenti")     
              case &NM..FLCONG="5"
                this.w_oMESS.AddMsgPartNL("Banche di riferimento incongruenti")     
              case &NM..FLCONG="6"
                this.w_oMESS.AddMsgPartNL("Codici agenti incongruenti")     
              case &NM..FLCONG="7"
                this.w_oMESS.AddMsgPartNL("Dati accompagnatori incongruenti")     
              case &NM..FLCONG="9"
                this.w_oMESS.AddMsgPartNL("Codici IVA per le spese incongruenti")     
              case &NM..FLCONG="0"
                this.w_oMESS.AddMsgPartNL("Codice IVA per il bollo incongruente")     
            endcase
            this.w_oMESS.Ah_Msg()     
          otherwise
            this.w_NumRiga = RECNO((ND))
            this.w_RIFCAC = this.oParentObject.w_ZOOMDETT.GetVar("MVRIFCAC")
            if this.oParentObject.w_FLCOAC="S" And g_COAC="S"
              if this.w_RIFCAC>0
                SELECT (ND)
                GO this.w_NumRiga
                AA=ALLTRIM(STR(this.oParentObject.w_ZoomDett.grd.ColumnCount))
                this.oParentObject.w_ZoomDett.grd.Column&AA..chk.Value = 0
                this.w_MESS = "Impossibile selezionare una riga di contributi, selezionare l'articolo di riferimento."
                ah_ErrorMsg( this.w_MESS,"!")
                i_retcode = 'stop'
                return
              endif
              * --- Marco come selezionati anche i componenti accessori legati alla
              *     riga selezionata..
              SELECT (ND)
              REPLACE &ND..MVQTAEVA WITH this.w_QTAEVA ,&ND..MVIMPEVA WITH this.w_EVAPRZ,;
              &ND..XChk WITH 1 FOR &ND..MVRIFCAC=this.oParentObject.w_ROWNUM
              SELECT (ND)
              GO this.w_NumRiga
              if this.oParentObject.w_TDMINVEN<>"E" AND this.oParentObject.w_TDRIOTOT="R"
                this.Page_2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
            SELECT (ND)
            REPLACE &ND..MVQTAEVA WITH IIF(NVL(&ND..MVTIPRIG," ")="D", 1, IIF(&ND..MVFLERIF="S", 0 , IIF(NVL(&ND..OLQTAEVA,0)>0,&ND..MVQTAMOV-NVL(&ND..OLQTAEVA,0),NVL(&ND..MVQTAMOV,0)) ) )
            REPLACE &ND..MVIMPEVA WITH this.w_EVAPRZ
        endcase
      case this.pEvent = "DRU"
        * --- Eseguito l' Uncheck sul Dettaglio
        * --- Toglie il Flag Selezionato sulle riga del Detail 
        if &ND..FLDOCU = "S" AND ((NVL(&ND..MVQTAMOV,0)<=NVL(&ND..OLQTAEVA,0) AND &ND..MVTIPRIG <>"F") OR (Abs(NVL(&ND..VISPRE,0)); 
 <=Abs(NVL(&ND..OLIMPEVA,0)) AND &ND..MVTIPRIG="F"))
          AA=ALLTRIM(STR(this.w_GSVE_KIM.w_ZoomDett.grd.ColumnCount))
          this.w_GSVE_KIM.w_ZoomDett.grd.Column&AA..chk.Value = 1
          this.w_MESS = "Impossibile deselezionare una riga gi� importata totalmente%0Eliminare la riga corrispondente sul documento"
          * --- Controllo Se riga gi� evasa senza Import
          if USED (L_Cur_Ev_Row) And Reccount( L_Cur_Ev_Row )>0
             
 Select( L_Cur_Ev_Row ) 
 Locate for &L_Cur_Ev_Row..RESERRIF = &ND..MVSERIAL And &L_Cur_Ev_Row..REROWRIF = &ND..CPROWNUM
            if Found()
              this.w_MESS = "Impossibile deselezionare: riga evasa a valore 0 (senza importazione)%0Se non si vuole evadere la riga, annullare l'importazione"
            endif
          endif
          if USED (L_Cur_Rag_Row) And Reccount( L_Cur_Rag_Row )>0
             
 Select( L_Cur_Rag_Row ) 
 Locate for &L_Cur_Rag_Row..RESERRIF = &ND..MVSERIAL And &L_Cur_Rag_Row..REROWRIF = &ND..CPROWNUM
            if Found()
              this.w_MESS = "Impossibile deselezionare: riga raggruppata nella precedente importazione"
            endif
          endif
          ah_Msg(this.w_MESS,.T.)
        else
          this.w_APPO = 0
          if &ND..MVTIPRIG="D"
            this.w_APPO = IIF(ah_YesNo("Importo comunque la riga descrittiva nel documento di destinazione?"),1,0)
          endif
          this.w_NumRiga = RECNO((ND))
          this.w_RIFCAC = this.oParentObject.w_ZOOMDETT.GetVar("MVRIFCAC")
          if this.oParentObject.w_FLCOAC="S" And g_COAC="S"
            if this.w_RIFCAC>0
              SELECT (ND)
              GO this.w_NumRiga
              AA=ALLTRIM(STR(this.oParentObject.w_ZoomDett.grd.ColumnCount))
              this.oParentObject.w_ZoomDett.grd.Column&AA..chk.Value =1
              this.w_MESS = "Impossibile deselezionare una riga di contributi, deselezionare l'articolo di riferimento."
              ah_ErrorMsg( this.w_MESS,"!")
              i_retcode = 'stop'
              return
            endif
            * --- Marco come deselezionati anche i componenti accessori legati alla
            *     riga selezionata..
            SELECT (ND)
            REPLACE &ND..MVQTAEVA WITH 0,&ND..MVIMPEVA WITH 0,;
            &ND..XChk WITH 0 FOR &ND..MVRIFCAC=this.oParentObject.w_ROWNUM
            SELECT (ND)
            GO this.w_NumRiga
          endif
          SELECT (ND)
          REPLACE &ND..MVQTAEVA WITH this.w_APPO
          REPLACE &ND..MVIMPEVA WITH 0
        endif
      case this.pEvent = "DBQ"
        * --- Evento Before Query sul Dettaglio
        * --- Salva in RigheDoc le Variazioni Riportate sul Dettaglio
        if USED(this.w_GSVE_KIM.w_ZoomDett.cCursor) AND USED("RigheDoc")
          Select ( ND ) 
 Go Top
          this.w_SERDETT = this.w_GSVE_KIM.w_ZoomDett.GetVar("MVSERIAL")
          SELECT XCHK, MVQTAMOV,OLQTAEVA, MVQTAEVA, MVIMPEVA, OLIMPEVA,MVSERIAL, CPROWNUM, CPROWORD, FLDOCU,; 
 FLINTE, FLACCO, MVTIPRIG, DOIMPEVA, DOQTAEV1,VISPRE, MVCODATT, MVCODCOM, MVCODCEN, ; 
 MVSCONT1, MVSCONT2, MVSCONT3, MVSCONT4, MVUNIMIS, MVCODICE, FLGROR, MVDATDOC, ; 
 MVNUMDOC, MVALFDOC,MVDATEVA, MVCODART,MVCODLOT,MVCODUBI,MVCODMAG,MVFLRISE,MVQTAUM1, MVCLADOC, MVFLVEAC,MVTIPDOC, MVFLERIF,PRSERIAL,ARCODIVA,MVRIFPRE, ; 
 MVAGG_01, MVAGG_02, MVAGG_03, MVAGG_04, MVAGG_05, MVAGG_06,MVNUMRIF,PRSERAGG FROM RigheDoc WHERE MVSERIAL<>this.w_SERDETT ; 
 UNION ; 
 SELECT XCHK, MVQTAMOV,OLQTAEVA, MVQTAEVA, MVIMPEVA,OLIMPEVA, MVSERIAL, CPROWNUM, CPROWORD, FLDOCU,; 
 FLINTE, FLACCO, MVTIPRIG, DOIMPEVA, DOQTAEV1,VISPRE, MVCODATT, MVCODCOM, MVCODCEN, ; 
 MVSCONT1, MVSCONT2, MVSCONT3, MVSCONT4, MVUNIMIS, MVCODICE, FLGROR, MVDATDOC, MVNUMDOC, MVALFDOC,MVDATEVA, MVCODART,MVCODLOT,MVCODUBI,MVCODMAG,MVFLRISE,MVQTAUM1,MVCLADOC,MVFLVEAC,MVTIPDOC, ; 
 MVFLERIF,PRSERIAL,ARCODIVA,MVRIFPRE, MVAGG_01, MVAGG_02, MVAGG_03, MVAGG_04, MVAGG_05, MVAGG_06,MVNUMRIF,PRSERAGG FROM (ND) ; 
 WHERE XCHK = 1 OR OLQTAEVA<>0 OR MVQTAEVA<>0 OR ((MVIMPEVA<>0 OR OLIMPEVA<>0) AND MVTIPRIG="F") ; 
 INTO CURSOR RigheDoc
          if Isalt() and Used(this.oParentObject.w_CUR_NAME)
            Select (this.oParentObject.w_CUR_NAME)
            Select *; 
 From RigheDoc Inner Join (this.oParentObject.w_CUR_NAME) on SERIALE=RigheDoc.MVSERIAL and PRROWNUM=RigheDoc.CPROWNUM Into Cursor RigheDoc 
 Use in (this.oParentObject.w_CUR_NAME)
          endif
          SELECT (ND)
        endif
      case this.pEvent = "DAQ"
        * --- Evento After Query sul Dettaglio
        * --- Riporta sul Dettaglio le Variazioni precedentemente definite e salvate in RigheDoc
        if USED(this.w_GSVE_KIM.w_ZoomDett.cCursor) AND USED("RigheDoc")
          Select ( NM )
          this.w_CHKMAST = this.w_GSVE_KIM.w_ZoomMast.GetVar("XCHK")
          this.w_CHKEVAS = this.w_GSVE_KIM.w_ZoomMast.GetVar("FLINTE")
          this.w_CHKACCO = this.w_GSVE_KIM.w_ZoomMast.GetVar("FLACCO")
           
 SELECT (ND) 
 SCAN
           
 SELECT * FROM RigheDoc WHERE RigheDoc.MVSERIAL = &ND..MVSERIAL ; 
 AND RigheDoc.CPROWNUM = &ND..CPROWNUM INTO CURSOR APPCUR
          if Isalt() and RECCOUNT("APPCUR")=0
             
 SELECT * FROM RigheDoc WHERE RigheDoc.MVSERIAL = &ND..PRSERIAL ; 
 AND &ND..CPROWNUM=-1 INTO CURSOR APPCUR
          endif
          SELECT (ND)
          if RECCOUNT("APPCUR")<>0
             
 REPLACE XCHK WITH APPCUR.XCHK, ; 
 MVQTAEVA WITH IIF(MVTIPRIG="D", 1, APPCUR.MVQTAEVA), ; 
 VISPRE WITH cp_ROUND(VISPRE, IIF(MVCODVAL=g_CODLIR, 0, 5)), ; 
 MVIMPEVA WITH APPCUR.MVIMPEVA, ; 
 OLQTAEVA WITH APPCUR.OLQTAEVA, ; 
 OLIMPEVA WITH APPCUR.OLIMPEVA, ; 
 FLDOCU WITH APPCUR.FLDOCU, ; 
 FLINTE WITH NVL(this.w_CHKEVAS, " "), ; 
 FLACCO WITH NVL(this.w_CHKACCO, " "), ; 
 MVFLERIF WITH NVL(APPCUR.MVFLERIF, "N")
          else
             
 REPLACE FLINTE WITH NVL(this.w_CHKEVAS, " "), ; 
 VISPRE WITH cp_ROUND(VISPRE, IIF(MVCODVAL=g_CODLIR, 0, 5)), ; 
 FLACCO WITH NVL(this.w_CHKACCO, " ")
          endif
          * --- Controllo Riga evasa a valore 0 (senza Import)
          if USED (L_Cur_Ev_Row) And Reccount( L_Cur_Ev_Row )>0
            * --- Ricerco nel cursore contenente le righe evase senza import se 
            *     la riga in oggetto � gi� stata evasa da una precedente importazione 
            *     (sempre in caricamento dello stesso documento)
             
 Select( L_Cur_Ev_Row ) 
 Locate for &L_Cur_Ev_Row..RESERRIF = &ND..MVSERIAL And &L_Cur_Ev_Row..REROWRIF = &ND..CPROWNUM
            if Found()
              * --- Se si, imposto i valori come se avessi effettivamente importato la riga, 
              *     in modo che i controlli funzionino allo stesso modo
               
 SELECT (ND) 
 REPLACE ; 
 FLDOCU With "S", ; 
 XCHK With 1
              if MVTIPRIG="F"
                REPLACE OLIMPEVA With MVPREZZO
              else
                REPLACE OLQTAEVA With MVQTAMOV
              endif
            endif
          endif
          * --- Controllo Riga Raggruppata in Importazione
          if USED (L_Cur_Rag_Row) And Reccount( L_Cur_Rag_Row )>0
            * --- Ricerco nel cursore contenente le righe Raggruppate 
            *     se la riga in oggetto � gi� stata importata
            *     (sempre in caricamento dello stesso documento)
             
 Select( L_Cur_Rag_Row ) 
 Locate for &L_Cur_Rag_Row..RESERRIF = &ND..MVSERIAL And &L_Cur_Rag_Row..REROWRIF = &ND..CPROWNUM
            if Found()
              * --- Se si, imposto i valori come se avessi effettivamente importato la riga, 
              *     in modo che i controlli funzionino allo stesso modo
               
 SELECT (ND) 
 REPLACE ; 
 FLDOCU With "S", ; 
 XCHK With 1
              if MVTIPRIG="F"
                REPLACE OLIMPEVA With MVPREZZO
              else
                REPLACE OLQTAEVA With MVQTAMOV
              endif
            endif
          endif
          SELECT (ND)
          ENDSCAN
          if this.oParentObject.w_FLTRASF="S"
            * --- Eseguo Filtro solo documenti gi� trasferiti
            DELETE FROM (ND) WHERE NVL(FLDOCU,"N") <> "S"
            this.w_GSVE_KIM.w_ZoomDett.grd.Refresh()     
          endif
          if USED("APPCUR")
            SELECT APPCUR
            USE
            SELECT (ND)
          endif
        endif
      case this.pEvent = "SCH" Or this.pEvent = "FNR"
        * --- Evento w_SELEZI Changed o Cliccato su Bottone per selezionare i Primi N REcord
        * --- Seleziona/Deselezione la Righe Dettaglio (se consentito)
        if USED(this.w_GSVE_KIM.w_ZoomDett.cCursor)
          if this.w_GSVE_KIM.w_ZoomMast.GetVar("XCHK") = 1
            * --- Selezionato il Documento Master
            if this.pEvent = "SCH"
              if this.oParentObject.w_SELEZI = "S"
                * --- Seleziona Tutto
                UPDATE (ND) SET XCHK=1, MVQTAEVA=IIF(NVL(MVTIPRIG," ")="D",1, IIF(MVFLERIF="S", 0 , IIF(OLQTAEVA>0,MVQTAMOV-OLQTAEVA,MVQTAMOV))), ; 
 VISPRE=cp_ROUND(VISPRE, IIF(MVCODVAL=g_CODLIR, 0, 5)), MVIMPEVA=IIF(MVFLERIF="S", 0 , IIF(OLIMPEVA<>0,cp_ROUND(MVPREZZO, IIF(MVCODVAL=g_CODLIR, 0, 5))-OLIMPEVA,cp_ROUND(MVPREZZO, IIF(MVCODVAL=g_CODLIR, 0, 5)))) ; 
 WHERE(FLDOCU<>"S" OR (FLDOCU="S" AND (OLQTAEVA<MVQTAMOV OR ; 
 (Abs(OLIMPEVA)<Abs(VISPRE) AND MVTIPRIG="F"))) )
              else
                * --- deseleziona Tutto
                UPDATE (ND) SET XCHK=0, MVQTAEVA=0, MVIMPEVA=0 ; 
 WHERE(FLDOCU<>"S" OR (FLDOCU="S" AND (OLQTAEVA<MVQTAMOV OR ; 
 (Abs(OLIMPEVA)<Abs(VISPRE) AND MVTIPRIG="F"))) )
              endif
            else
              if this.oParentObject.w_Scope <> 0
                * --- Prima deseleziona Tutto
                UPDATE (ND) SET XCHK=0, MVQTAEVA=0, MVIMPEVA=0 ; 
 WHERE(FLDOCU<>"S" OR (FLDOCU="S" AND (OLQTAEVA<MVQTAMOV OR ; 
 (Abs(OLIMPEVA)<Abs(VISPRE) AND MVTIPRIG="F"))) )
                * --- Seleziona solo N Righe
                *     w_Scope deve essere ricalcolato in modo che al massimo risulti delle stesse righe del Cursore
                *     Se w_Scope � maggiore delle righe del cursore potrebbero avvenire degli errori tipo
                *     raddoppiamento righe sul dettaglio
                this.oParentObject.w_SCOPE = Min(this.oParentObject.w_Scope, Reccount(ND))
                if g_COAC="S" And this.oParentObject.w_FLCOAC="S"
                  * --- Se presenti contributi accessori allora allungo il numero di record
                  *     in modo da comprendere tutti i contributi associati...
                  Select ( ND ) 
 Go ( this.oParentObject.w_SCOPE )
                  * --- Mi sposto al record successivo all'ultimo selezionato se non � un contributo...
                  if &ND..MVRIFCAC=0 
                    Skip
                  endif
                  do while &ND..MVRIFCAC>0 and Not Eof(ND)
                    Skip
                  enddo
                  this.oParentObject.w_SCOPE = Recno( ND ) - 1
                endif
                Select ( ND ) 
 Go Top 
 Replace ; 
 XCHK With 1, ; 
 MVQTAEVA With IIF(NVL(MVTIPRIG," ")="D",1, IIF(OLQTAEVA>0,MVQTAMOV-OLQTAEVA,MVQTAMOV)), ; 
 VISPRE With cp_ROUND(VISPRE, IIF(MVCODVAL=g_CODLIR, 0, 5)), ; 
 MVIMPEVA With IIF(OLIMPEVA<>0,cp_ROUND(MVPREZZO, IIF(MVCODVAL=g_CODLIR,0,5))-OLIMPEVA,cp_ROUND(MVPREZZO, IIF(MVCODVAL=g_CODLIR, 0, 5))) ; 
 Next this.oParentObject.w_Scope ; 
 For (FLDOCU<>"S" OR (FLDOCU="S" AND (OLQTAEVA<MVQTAMOV OR ; 
 (Abs(OLIMPEVA)<Abs(VISPRE) AND MVTIPRIG="F"))) ) And Not Empty(Nvl(MVCODICE," "))
              else
                this.w_MESS = "Indicare il numero di record da selezionare"
                ah_Msg(this.w_MESS,.T.)
              endif
            endif
          else
            this.w_MESS = "Documento da importare non selezionato"
            ah_Msg(this.w_MESS,.T.)
            this.oParentObject.w_SELEZI = IIF(this.oParentObject.w_SELEZI = "S" And this.pEvent <> "FNR", "D", "S")
          endif
        endif
      case this.pEvent = "QTACHG" 
        do case
          case (g_COAC="S" And this.oParentObject.w_FLCOAC="S" )
            * --- Riporto sui contributi collegati la quantit� impostata sul padre se 
            *     MVRIFCAC=0 altrimenti impedisco la modifica..
            if this.oParentObject.w_ZOOMDETT.GetVar("MVRIFCAC")>0
              ah_ErrorMsg( "Impossibile modificare quantit� di un contributo accessorio","!","" )
              * --- Rispristino il vecchio valore
              this.oParentObject.w_ZOOMDETT.GRD.COLUMNS[this.oParentObject.w_ZOOMDETT.GRD.ActiveColumn-2].TEXT1.VALUE=&ND..MVQTAEVA
            else
              if VarType(" this.oParentObject.w_ZoomDett.GRD.COLUMNS[this.oParentObject.w_ZoomDett.GRD.ActiveColumn-2].TEXT1.VALUE ")<>"U"
                this.w_NumRiga = RECNO((ND))
                this.w_TMPQTA = this.oParentObject.w_ZOOMDETT.GRD.COLUMNS[this.oParentObject.w_ZOOMDETT.GRD.ActiveColumn-2].TEXT1.VALUE
                REPLACE &ND..MVQTAEVA WITH this.w_TMPQTA FOR &ND..MVRIFCAC=this.oParentObject.w_ROWNUM
                SELECT (ND)
                GO this.w_NumRiga
              endif
            endif
        endcase
        if this.oParentObject.w_TDMINVEN<>"E" AND this.oParentObject.w_TDRIOTOT="R"
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pEvent = "PRZCHG" AND Isalt() and &ND..MVTIPRIG="F"
        if Type(" this.oParentObject.w_ZoomDett.GRD.COLUMNS[9].TEXT1.VALUE ")<>"U"
          if this.oParentObject.w_ZOOMDETT.GRD.COLUMNS[9].TEXT1.VALUE=0 and &ND..MVIMPEVA>0
            ah_ErrorMsg( "Impossibile azzerare importo riga collegate ad altre righe prestazioni","!","" )
            * --- Rispristino il vecchio valore
            this.oParentObject.w_ZOOMDETT.GRD.COLUMNS[9].TEXT1.VALUE=&ND..MVIMPEVA
          endif
        endif
    endcase
    * --- Setta Proprieta' Campi del Cursore
    if this.w_CHKMAST <> -1 AND this.pEvent<>"AMC"
      this.w_ZOOM = this.w_GSVE_KIM.w_ZoomDett
      this.w_APPO1 = "XCHK-MVQTAEVA-CPROWORD-MVCODICE-MVUNIMIS-MVQTAMOV-OLQTAEVA-MVDATEVA"
      this.w_APPO1 = this.w_APPO1 + "-FLINTE-FLEVAS-MVTIPRIG-MVPREZZO-MVIMPEVA-OLIMPEVA-VISPRE-MVDESART"
      SELECT (ND)
      FOR I=1 TO This.w_Zoom.grd.ColumnCount
      NC = ALLTRIM(STR(I))
      this.w_APPO = UPPER(This.w_Zoom.grd.Column&NC..ControlSource)
      if this.w_APPO $ this.w_APPO1
        if TYPE("THIS.OPARENTOBJECT.w_GESTPARENT.cPRG") = "U"
          * --- Questa parte di codice pu� essere eseguita prima che w_GESTPARENT sia inizializzato in GSVE_KIM
          this.oParentObject.w_GESTPARENT = L_PADRE
        endif
        if this.w_APPO <> "XCHK"
          this.w_APPO2 = "IIF(XCHK=0 AND OLQTAEVA=0 AND MVQTAEVA=0, RGB(0,0,0), "
          this.w_APPO2 = this.w_APPO2 + 'IIF(FLDOCU="S", RGB(0,0,255), RGB(255,0,0)))'
          this.w_ZOOM.grd.Column&NC..DynamicForeColor = this.w_APPO2
          if this.w_APPO = "MVQTAEVA" OR this.w_APPO = "MVIMPEVA"
            this.w_ZOOM.grd.Column&NC..Format = "KZR"
            this.w_ZOOM.grd.Column&NC..Enabled = IIF(this.w_CHKMAST=1, .T., .F.)
            * --- Nel caso la maschera di evasione documenti sia lanciata da GSAR_MPG la quantit� evasa � gia impostata in GSAR_MPG, e quindi non deve essere modificabile
            if UPPER( this.oParentObject.w_GESTPARENT.cPRG ) = "GSAR_MPG"
              this.w_ZOOM.grd.Column&NC..ReadOnly = .T.
            endif
          endif
          if IsAlt()
            do case
              case this.w_APPO = "MVCODICE"
                this.w_ZOOM.grd.Column&NC..HDR.caption = "Prestazione"
                this.w_ZOOM.grd.Column&NC..Width = 75
              case this.w_APPO = "MVQTAMOV"
                this.w_ZOOM.grd.Column&NC..Width = 50
                this.w_ZOOM.grd.Column&NC..HDR.caption = "Quantit�"
              case this.w_APPO = "MVQTAEVA"
                this.w_ZOOM.grd.Column&NC..Width = 14
                this.w_ZOOM.grd.Column&NC..HDR.caption = "Varia qta"
              case this.w_APPO = "OLQTAEVA"
                this.w_ZOOM.grd.Column&NC..Width = 0
              case this.w_APPO = "MVDATEVA"
                this.w_ZOOM.grd.Column&NC..Width = 0
              case this.w_APPO = "MVTIPRIG"
                this.w_ZOOM.grd.Column&NC..Width = 0
              case this.w_APPO = "VISPRE"
                this.w_ZOOM.grd.Column&NC..HDR.caption = "Importo di riga"
              case this.w_APPO = "OLIMPEVA"
                this.w_ZOOM.grd.Column&NC..Width = 0
              case this.w_APPO = "MVDESART"
                this.w_ZOOM.grd.Column&NC..Width = 170
            endcase
          else
            do case
              case this.w_APPO = "MVDESART"
                this.w_ZOOM.grd.Column&NC..Visible = .F.
            endcase
          endif
        else
          this.w_ZOOM.grd.Column&NC..Width = 40
          this.w_ZOOM.grd.Column&NC..Header1.caption = "Evasa"
          this.w_ZOOM.grd.Column&NC..Header1.FontSize = 8
          this.w_ZOOM.grd.Column&NC..chk.caption = ""
          this.w_ZOOM.grd.Column&NC..Enabled = IIF(this.w_CHKMAST=1, .T., .F.)
          * --- Nel caso la maschera di evasione documenti sia lanciata da GSAR_MPG la quantit� evasa � gia impostata in GSAR_MPG, e quindi non deve essere modificabile
          if UPPER( this.oParentObject.w_GESTPARENT.cPRG ) = "GSAR_MPG"
            this.w_ZOOM.grd.Column&NC..Header1.caption = "..."
          endif
        endif
      else
        this.w_ZOOM.grd.Column&NC..Visible = .F.
      endif
      ENDFOR
    endif
    * --- !!!ATTENZIONE: Eliminare l'esecuzione della mCalc dalla Maschera
    * --- perche per alcuni eventi, richiamerebbe questo batch, entrando in Loop...
    this.w_GSVE_KIM.w_ZOOMMAST.REFRESH()     
    this.w_GSVE_KIM.w_ZOOMDETT.REFRESH()     
    this.bUpdateParentObject = .F.
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if !UPPER( this.oParentObject.w_GESTPARENT.cPRG ) = "GSAR_MPG"
      if this.pEvent="MRC" OR this.pEvent="AMC"
        this.w_MVUNIMIS = NVL(MVUNIMIS, SPACE(3))
        this.w_MVQTAEVA = NVL(MVQTAMOV,0)
        this.w_CODICE = NVL(MVCODICE, SPACE(41))
        this.w_CODART = NVL(MVCODART, SPACE(20) )
      else
        this.w_MVUNIMIS = this.oParentObject.w_ZOOMDETT.GetVar("MVUNIMIS")
        if this.pEvent = "DRC"
          this.w_MVQTAEVA = this.oParentObject.w_ZOOMDETT.GetVar("MVQTAMOV")
        else
          this.w_MVQTAEVA = this.oParentObject.w_ZOOMDETT.GRD.COLUMNS[5].TEXT1.VALUE
        endif
        this.w_CODICE = this.oParentObject.w_ZOOMDETT.GetVar("MVCODICE")
        this.w_CODART = this.oParentObject.w_ZOOMDETT.GetVar("MVCODART")
      endif
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CAMINVEN,CAOPERAT,CAUNIMIS,CAMOLTIP,CATIPCON"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CAMINVEN,CAOPERAT,CAUNIMIS,CAMOLTIP,CATIPCON;
          from (i_cTable) where;
              CACODICE = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAMINVEN = NVL(cp_ToDate(_read_.CAMINVEN),cp_NullValue(_read_.CAMINVEN))
        this.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
        this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
        this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
        this.w_CATIPCON = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARMINVEN,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARMINVEN,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ARMINVEN = NVL(cp_ToDate(_read_.ARMINVEN),cp_NullValue(_read_.ARMINVEN))
        this.w_UNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
        this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
        this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
        this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_RMINVEN = iif(this.w_CAMINVEN<>0, this.w_CAMINVEN, this.w_ARMINVEN)
      * --- calcolare la quantit� evasa nella prima
      this.w_QTAEV1 = CALQTA(this.w_MVQTAEVA,this.w_MVUNIMIS,this.w_UNMIS2,this.w_OPERAT, this.w_MOLTIP, " ", "N", " ", , this.w_UNMIS3, this.w_OPERA3, this.w_MOLTI3)
      if this.w_RMINVEN<>0 and this.w_QTAEV1< this.w_RMINVEN AND (this.oParentObject.w_MVFLVEAC="V" OR ( this.oParentObject.w_MVFLVEAC="A" AND this.w_CATIPCON="F"))
        do case
          case this.pEvent = "DRC"
            if this.oParentObject.w_TDMINVEN="B"
              this.oParentObject.w_ERR = .T.
              if this.oParentObject.w_MVCLADOC="OR"
                ah_ErrorMsg( "Attenzione: si sta selezionando una riga con quantit� inferiore al quantitativo minimo ordinabile impostato in anagrafica articoli/servizi/codici di ricerca%0Impossibile confermare","!","" )
              else
                ah_ErrorMsg( "Attenzione: si sta selezionando una riga con quantit� inferiore al quantitativo minimo vendibile impostato in anagrafica articoli/servizi/codici di ricerca%0Impossibile confermare","!","" )
              endif
              this.w_NumRiga = RECNO((ND))
              SELECT (ND)
              GO this.w_NumRiga
              AA=ALLTRIM(STR(this.oParentObject.w_ZoomDett.grd.ColumnCount))
              this.oParentObject.w_ZoomDett.grd.Column&AA..chk.Value = 0
              this.oParentObject.w_ZOOMDETT.GRD.COLUMNS[5].TEXT1.VALUE=0
              this.oParentObject.w_ZOOMDETT.GRD.COLUMNS[13].TEXT1.VALUE=0
              i_retcode = 'stop'
              return
            else
              if this.oParentObject.w_MVCLADOC="OR"
                ah_ErrorMsg( "Attenzione: si sta selezionando una riga con quantit� inferiore al quantitativo minimo ordinabile impostato in anagrafica articoli/servizi/codici di ricerca","!","" )
              else
                ah_ErrorMsg( "Attenzione: si sta selezionando una riga con quantit� inferiore al quantitativo minimo vendibile impostato in anagrafica articoli/servizi/codici di ricerca","!","" )
              endif
            endif
          case this.pEvent = "QTACHG" 
            if this.oParentObject.w_MVCLADOC="OR"
              ah_ErrorMsg( "Attenzione: si sta impostando una quantit� inferiore al quantitativo minimo ordinabile impostato in anagrafica articoli/servizi/codici di ricerca","!","" )
            else
              ah_ErrorMsg( "Attenzione: si sta impostando una quantit� inferiore al quantitativo minimo vendibile impostato in anagrafica articoli/servizi/codici di ricerca","!","" )
            endif
            if this.oParentObject.w_TDMINVEN="B"
              * --- Se il controllo � bloccante ripristino il vecchio valore
              this.oParentObject.w_ZOOMDETT.GRD.COLUMNS[this.oParentObject.w_ZOOMDETT.GRD.ActiveColumn-1].TEXT1.VALUE=this.oParentObject.w_OLDQTAEVA
            endif
          case this.pEvent="MRC" OR this.pEvent="AMC"
            * --- totale per documento
            this.w_TOTRIGHE = IIF( &ND..MVSERIAL= this.w_SERIALMAST, this.w_TOTRIGHE+1,0)
            if this.oParentObject.w_TDRIOTOT="R" AND &ND..MVSERIAL= this.w_SERIALMAST
              if this.oParentObject.w_MVCLADOC="OR"
                this.w_oPART = this.w_oMESS.addmsgpartNL("Articolo: %1%0Quantit� ordinata (%4 %2) inferiore al quantitativo minimo ordinabile impostato in anagrafica articoli/servizi/codici di ricerca (%4 %3)")
              else
                this.w_oPART = this.w_oMESS.addmsgpartNL("Articolo: %1%0Quantit� venduta (%4 %2) inferiore al quantitativo minimo vendibile impostato in anagrafica articoli/servizi/codici di ricerca (%4 %3)")
              endif
              this.w_oPART.AddParam(ALLTR( this.w_CODICE ))     
              this.w_oPART.AddParam(ALLTR( STR(this.w_QTAEV1,12,3)))     
              this.w_oPART.AddParam(ALLTR( STR(this.w_RMINVEN,12,3)))     
              this.w_oPART.AddParam(ALLTR( this.w_UNMIS1))     
              this.w_ERRMIN = .T.
              if this.oParentObject.w_TDMINVEN="B"
                this.oParentObject.w_ERR = .T.
                * --- Se il controllo � bloccante deseleziono la riga
                this.w_CONTARIG = this.w_CONTARIG +1
                if this.w_CONTARIG>1
                  this.w_oMESS.AddMsgPartNL("Tali righe verranno escluse dall'importazione")     
                else
                  this.w_oMESS.AddMsgPartNL("Tale riga verr� esclusa dall'importazione")     
                endif
                replace XCHK with 0, MVQTAEVA WITH 0, MVIMPEVA WITH 0 
              endif
            endif
        endcase
      else
        if (this.pEvent="MRC" OR this.pEvent="AMC" ) AND &ND..MVSERIAL= this.w_SERIALMAST
          this.w_TOTRIGHE = IIF( &ND..MVSERIAL= this.w_SERIALMAST, this.w_TOTRIGHE+1,0)
          replace XCHK with (IIF(NVL(MVTIPRIG," ")="D" or ( NVL(MVTIPRIG," ")="F" and (this.oParentObject.w_FLGZER="S" and VISPRE=0)) or CPROWORD=-1,1, 0)), MVQTAEVA with (IIF(NVL(MVTIPRIG," ")="D",1, IIF(OLQTAEVA<>0,MVQTAMOV-OLQTAEVA,MVQTAMOV))), ;
          VISPRE with cp_ROUND(VISPRE, IIF(MVCODVAL=g_CODLIR, 0, 5)), MVIMPEVA with (IIF(OLIMPEVA<>0 AND MVTIPRIG="F",cp_ROUND(MVPREZZO, IIF(MVCODVAL=g_CODLIR, 0, 5))-OLIMPEVA,cp_ROUND(MVPREZZO, IIF(MVCODVAL=g_CODLIR, 0, 5)))) 
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pEvent)
    this.pEvent=pEvent
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEvent"
endproc
