* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_gvc                                                        *
*              Gadget Graph                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-12-16                                                      *
* Last revis.: 2015-12-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_gvc",oParentObject))

* --- Class definition
define class tgsut_gvc as StdGadgetForm
  Top    = 4
  Left   = 2

  * --- Standard Properties
  Width  = 158
  Height = 118
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-18"
  HelpContextID=53035881
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_gvc"
  cComment = "Gadget Graph"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TITLE = space(20)
  o_TITLE = space(20)
  w_RET = space(1)
  w_CHART = space(50)
  w_DATETIME = space(10)
  w_PROGRAM = space(250)
  w_PRGDESCRI = space(50)
  w_GRAPH = .NULL.
  w_oFooter = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_gvc
  bGadgetResize = .f. && mi serve per sapere se eseguo la LoadChart in seguito ad un resize manuale
  
  Proc LoadChart()
    Local l_oldError,ggname
    With This
      .cAlertMsg = ""
      If !Empty(.w_CHART) And cp_FileExist(ForceExt(.w_CHART,'VFC'))
        *-- Gestione dell'errore
        m.ggname = 'frm'+Alltrim(.Name)
        m.l_oldError = On("Error")
         
        On Error &ggname..cAlertMsg=Message()
        *--- se arrivo da resize devo solo ridisegnare il grafico
        If .bGadgetResize
          .bGadgetResize = .f.
          .w_GRAPH.UpdateFoxCharts()
        Else && altrimenti aggiorno il file di origine e lo eseguo
          .w_GRAPH.cFileCfg = ForceExt(.w_CHART,'')
          .w_GRAPH.InitFoxCharts()
          .w_GRAPH.ReadData()
          .w_GRAPH.Visible=.t.
        Endif
         
        On error &l_oldError
      Else
        .cAlertMsg = ah_MsgFormat('Grafico non definito o inesistente:%0"%1"', Alltrim(.w_CHART))
      Endif
    Endwith
  EndProc
  * --- Fine Area Manuale
  *--- Define Header
  add object oHeader as cp_headergadget with Width=this.Width, Left=0, Top=0, nNumPages=1 
  Height = This.Height + this.oHeader.Height

  * --- Define Page Frame
  add object oPgFrm as cp_oPgFrm with PageCount=1, Top=this.oHeader.Height, Width=this.Width, Height=this.Height-this.oHeader.Height
  proc oPgFrm.Init
    cp_oPgFrm::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_gvcPag1","gsut_gvc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_GRAPH = this.oPgFrm.Pages(1).oPag.GRAPH
    this.w_oFooter = this.oPgFrm.Pages(1).oPag.oFooter
    DoDefault()
    proc Destroy()
      this.w_GRAPH = .NULL.
      this.w_oFooter = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TITLE=space(20)
      .w_RET=space(1)
      .w_CHART=space(50)
      .w_DATETIME=space(10)
      .w_PROGRAM=space(250)
      .w_PRGDESCRI=space(50)
      .oPgFrm.Page1.oPag.GRAPH.Calculate()
      .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME)
    endwith
    this.DoRTCalc(1,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_gvc
    This.w_GRAPH.Visible=.f.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.GRAPH.Calculate()
      	*--- Assegnamento caption ad oggetto header
          If .o_TITLE <> .w_TITLE
			   .oHeader.Calculate(.oPgFrm.Page1.oPag.oContained.w_TITLE)
          EndIf
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.GRAPH.Calculate()
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME)
    endwith
  return

  proc Calculate_YFXRANDGJN()
    with this
          * --- Load chart
          .w_RET = Thisform.LoadChart()
      if !.bGadgetResize
          .tLastUpdate = DateTime()
      endif
      if !.bGadgetResize
          .w_DATETIME = Alltrim(TTOC(DateTime()))+' '
      endif
          .w_RET = This.mCalc(.T.)
    endwith
  endproc
  proc Calculate_ZFXTXNEHHT()
    with this
          * --- Gestione resize per ridisegnare il grafico
          .bGadgetResize = .t.
          .bEventArranged = .t.
    endwith
  endproc
  proc Calculate_YCUUTVASEB()
    with this
          * --- Gadget Option
          .w_RET = This.GadgetOption()
          .w_RET = Thisform.LoadChart()
          .w_DATETIME = Alltrim(TTOC(DateTime()))+' '
          .w_RET = This.mCalc(.T.)
    endwith
  endproc
  proc Calculate_QPKIKLOISB()
    with this
          * --- Ridisegno grafico se massimizzo/minimizzo
          .bGadgetResize = .t.
          .w_RET = Thisform.LoadChart()
          .w_RET = This.mCalc(.T.)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.GRAPH.Event(cEvent)
        if lower(cEvent)==lower("GadgetArranged") or lower(cEvent)==lower("GadgetMaximized") or lower(cEvent)==lower("GadgetMinimized") or lower(cEvent)==lower("GadgetOnDemand") or lower(cEvent)==lower("GadgetOnTimer") or lower(cEvent)==lower("oheader RefreshOnDemand")
          .Calculate_YFXRANDGJN()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oFooter.Event(cEvent)
        if lower(cEvent)==lower("MouseUpResizeGadget")
          .Calculate_ZFXTXNEHHT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("GadgetOption")
          .Calculate_YCUUTVASEB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("GadgetMaximize End") or lower(cEvent)==lower("GadgetMinimize End")
          .Calculate_QPKIKLOISB()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TITLE = this.w_TITLE
    return

enddefine

* --- Define pages as container
define class tgsut_gvcPag1 as StdContainer
  Width  = 158
  height = 118
  stdWidth  = 158
  stdheight = 118
  resizeXpos=35
  resizeYpos=75
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object GRAPH as cp_FoxCharts with uid="QQRSJEHEIJ",left=3, top=0, width=150,height=90,;
    caption='Object',;
   bGlobalFont=.t.,;
    cSource="",cFileCfg="",bDisplayLogo=.f.,cLogoFileName="The file name of the chart Logo",bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",;
    cEvent = "oheader RefreshOnDemand",;
    nPag=1;
    , HelpContextID = 124961766


  add object oFooter as cp_FooterGadget with uid="UMXGAILDJX",left=4, top=90, width=147,height=27,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 124961766
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_gvc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
