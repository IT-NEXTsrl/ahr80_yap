* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bas                                                        *
*              Verifica flag destinatario stampa                               *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-01                                                      *
* Last revis.: 2006-09-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bas",oParentObject)
return(i_retval)

define class tgsar_bas as StdBatch
  * --- Local variables
  w_CODICE = space(5)
  w_AZIENDA = space(5)
  * --- WorkFile variables
  SEDIAZIE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica che il flag 'destinatario stampa' sia impostato su un unica sede
    *     Batch utilizzato nelle 'sedi azienda' GSAR_ASE
    this.w_AZIENDA = i_CODAZI
    if this.oParentObject.w_SEDESSTA="S"
      * --- Select from SEDIAZIE
      i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select SECODDES  from "+i_cTable+" SEDIAZIE ";
            +" where SEDESSTA='S' and SECODAZI="+cp_ToStrODBC(this.w_AZIENDA)+"";
             ,"_Curs_SEDIAZIE")
      else
        select SECODDES from (i_cTable);
         where SEDESSTA="S" and SECODAZI=this.w_AZIENDA;
          into cursor _Curs_SEDIAZIE
      endif
      if used('_Curs_SEDIAZIE')
        select _Curs_SEDIAZIE
        locate for 1=1
        do while not(eof())
        this.w_CODICE = SECODDES
        if NOT EMPTY (this.w_CODICE) AND this.w_CODICE<>this.oParentObject.w_SECODDES
          this.oParentObject.w_SEDESSTA = "N"
          AH_ERRORMSG("Non � possibile impostare il flag 'destinatario di stampa' su pi� sedi",48)
          i_retcode = 'stop'
          i_retval = .F.
          return
        endif
          select _Curs_SEDIAZIE
          continue
        enddo
        use
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SEDIAZIE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_SEDIAZIE')
      use in _Curs_SEDIAZIE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
