* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_baq                                                        *
*              Calcolo altri quadri                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39][VRS_7]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-13                                                      *
* Last revis.: 2015-11-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pKEYATT,pANNO,paResult
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_baq",oParentObject,m.pKEYATT,m.pANNO,m.paResult)
return(i_retval)

define class tgscg_baq as StdBatch
  * --- Local variables
  pKEYATT = space(5)
  pANNO = space(4)
  paResult = space(6)
  w_INIANNO = ctod("  /  /  ")
  w_FINANNO = ctod("  /  /  ")
  w_VE37 = 0
  w_VE38 = 0
  w_VF16 = 0
  w_VF17 = 0
  w_PARVE37 = 0
  w_PARVE38 = 0
  w_PARVF16 = 0
  w_PARVF17 = 0
  w_OPPART = 0
  w_OPESENT = 0
  w_LOOP = 0
  w_PARPRO = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue calcolo Altri Quadri da GSCG_BDA,GSCG1BAQ
    * --- Calcolo Altri Qudri: 
    *     VE37: Fatture Es. Diff. dell'anno non incassate
    *     VE38: Fatture Es. Diff. di anni precedenti incassate
    * --- Operazioni che partecipano al calcolo del prorata...
    * --- ...operazioni esenti
    this.w_LOOP = 1
    this.w_OPPART = 0
    this.w_OPESENT = 0
    * --- Eseguo tre volte le stesse query poich� ho bisogno di distinguere gli imponibili Iva
    *     delle Operazioni che partecipano al calcolo del prorata, quelle esenti e quelle escluse dal calcolo
    *     Tutte e tre partecipano al calcolo degli altri quadri VE37 e VE38.
    *     Per quanto riguarda il prorata:
    *     Operazioni che partecipano al calcolo / (Operazioni che partecipano al calcolo + Operazioni Esenti)
    do while this.w_LOOP <=3
      * --- w_LOOP = 3 -> Operazioni escluse dal calcolo del prorata.
      *     Partecipano comunque per il calcolo degli altri Quadri VE37 e VE38
      this.w_PARVE37 = 0
      this.w_PARVE38 = 0
      this.w_PARVF16 = 0
      this.w_PARVF17 = 0
      * --- Imposto il filtro sulle date (tutto l'anno solare)
      this.w_INIANNO = cp_CharToDate("01-01"+"-"+ALLTRIM(this.pANNO))
      this.w_FINANNO = cp_CharToDate("31-12"+"-"+ALLTRIM(this.pANNO))
      do case
        case this.w_LOOP=1
          * --- Operazioni che partecipano al calcolo del prorata
          this.w_PARPRO = "N"
        case this.w_LOOP=2
          * --- Operazioni esenti 
          this.w_PARPRO = "S"
        case this.w_LOOP=3
          * --- Operazioni che NON partecipano al calcolo del prorata
          this.w_PARPRO = "E"
      endcase
      * --- Recupero le partite di apertura / chiusura. Aperte da Reg. con 
      *     competenza ne periodo e chiuse da registrazioni con data registrazione
      *     nel periodo divise per fattura esigibile (SERIAL)
      * --- Recupero totale registrazioni ad esigibilit� differita di vendita
      vq_exec("QUERY\GSCG4QED.VQR",this,"DATI")
      SELECT DATI
      LOCATE FOR RIGO="VE37"
      if FOUND()
        this.w_PARVE37 = cp_ROUND(NVL(VALORE,0),2)
      endif
      SELECT DATI
      LOCATE FOR RIGO="VE38"
      if FOUND()
        this.w_PARVE38 = cp_ROUND(NVL(VALORE,0),2)
      endif
      LOCATE FOR RIGO="VF16"
      if FOUND()
        this.w_PARVF16 = cp_ROUND(NVL(VALORE,0),2)
      endif
      LOCATE FOR RIGO="VF17"
      if FOUND()
        this.w_PARVF17 = cp_ROUND(NVL(VALORE,0),2)
      endif
      if USED("DATI")
        SELECT DATI
        USE
      endif
      * --- Ricerco eventuali incassi manuali, senza nessuna chiusura partite, e li sommo al valore precedentemente calcolato
      vq_exec("QUERY\GSCG9QED.VQR",this,"DATI")
      if USED("DATI")
         
 SELECT DATI 
 GO TOP 
 SCAN
        if NVL(DATI.TIPREG," ")="V"
          this.w_PARVE38 = this.w_PARVE38+NVL(IVIMPONI,0)
        else
          this.w_PARVF17 = this.w_PARVF17+NVL(IVIMPONI,0)
        endif
        ENDSCAN
      endif
      if USED("DATI")
        SELECT DATI
        USE
      endif
      this.w_VE37 = this.w_VE37+INTROUND(this.w_PARVE37, 1)
      this.w_VE38 = this.w_VE38+INTROUND(this.w_PARVE38, 1)
      this.w_VF16 = this.w_VF16+INTROUND(this.w_PARVF16, 1)
      this.w_VF17 = this.w_VF17+INTROUND(this.w_PARVF17, 1)
      do case
        case this.w_LOOP=1
          * --- Calcolo le operazioni che partecipano al calcolo
          this.w_OPPART = this.w_PARVE37-this.w_PARVE38
        case this.w_LOOP=2
          * --- Calcolo le operazioni esenti
          this.w_OPESENT = this.w_PARVE37-this.w_PARVE38
      endcase
      this.w_LOOP = this.w_LOOP + 1
    enddo
     
 paresult[1]=this.w_VE37 
 paresult[2]=this.w_VE38 
 paresult[3]=this.w_VF16 
 paresult[4]=this.w_VF17 
 paresult[5]=this.w_OPPART 
 paresult[6]=this.w_OPESENT
  endproc


  proc Init(oParentObject,pKEYATT,pANNO,paResult)
    this.pKEYATT=pKEYATT
    this.pANNO=pANNO
    this.paResult=paResult
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pKEYATT,pANNO,paResult"
endproc
