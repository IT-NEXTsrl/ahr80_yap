* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_mvm                                                        *
*              Movimenti di magazzino                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_437]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-18                                                      *
* Last revis.: 2017-06-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_mvm"))

* --- Class definition
define class tgsma_mvm as StdTrsForm
  Top    = 4
  Left   = 10

  * --- Standard Properties
  Width  = 794
  Height = 496+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-06-27"
  HelpContextID=221611159
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=193

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  MVM_MAST_IDX = 0
  MVM_DETT_IDX = 0
  CAM_AGAZ_IDX = 0
  CONTI_IDX = 0
  CAN_TIER_IDX = 0
  VALUTE_IDX = 0
  ART_ICOL_IDX = 0
  MAGAZZIN_IDX = 0
  KEY_ARTI_IDX = 0
  CAM_AGAZ_IDX = 0
  UNIMIS_IDX = 0
  ESERCIZI_IDX = 0
  LISTINI_IDX = 0
  VOCIIVA_IDX = 0
  SALDIART_IDX = 0
  ATTIVITA_IDX = 0
  LOTTIART_IDX = 0
  UBICAZIO_IDX = 0
  SALDILOT_IDX = 0
  cFile = "MVM_MAST"
  cFileDetail = "MVM_DETT"
  cKeySelect = "MMSERIAL"
  cKeyWhere  = "MMSERIAL=this.w_MMSERIAL"
  cKeyDetail  = "MMSERIAL=this.w_MMSERIAL and MMNUMRIF=this.w_MMNUMRIF"
  cKeyWhereODBC = '"MMSERIAL="+cp_ToStrODBC(this.w_MMSERIAL)';

  cKeyDetailWhereODBC = '"MMSERIAL="+cp_ToStrODBC(this.w_MMSERIAL)';
      +'+" and MMNUMRIF="+cp_ToStrODBC(this.w_MMNUMRIF)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"MVM_DETT.MMSERIAL="+cp_ToStrODBC(this.w_MMSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MVM_DETT.CPROWORD '
  cPrg = "gsma_mvm"
  cComment = "Movimenti di magazzino"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  cAutoZoom = 'GSMA0MVM'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MMSERIAL = space(10)
  o_MMSERIAL = space(10)
  w_AZICOD = space(5)
  w_MMNUMREG = 0
  w_MMCODUTE = 0
  w_MMDATREG = ctod('  /  /  ')
  o_MMDATREG = ctod('  /  /  ')
  w_MMFLCLFR = space(1)
  w_MMCODESE = space(4)
  o_MMCODESE = space(4)
  w_MMVALNAZ = space(3)
  w_CAONAZ = 0
  w_CAUCOL = space(5)
  w_DTOBSO = ctod('  /  /  ')
  w_FLNDOC = space(1)
  w_MMTIPCON = space(1)
  o_MMTIPCON = space(1)
  w_MMTCAMAG = space(5)
  o_MMTCAMAG = space(5)
  w_MMNUMDOC = 0
  w_MMALFDOC = space(10)
  w_MMDATDOC = ctod('  /  /  ')
  o_MMDATDOC = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DESCAU = space(35)
  w_MMCODCON = space(15)
  o_MMCODCON = space(15)
  w_DESCLF = space(40)
  w_TIPATT = space(1)
  w_MMDESSUP = space(40)
  w_MMSCOCL1 = 0
  o_MMSCOCL1 = 0
  w_MMSCOCL2 = 0
  o_MMSCOCL2 = 0
  w_MMSCOPAG = 0
  o_MMSCOPAG = 0
  w_VALCLF = space(3)
  w_CATSCC = space(5)
  w_MMCODVAL = space(3)
  o_MMCODVAL = space(3)
  w_CAOVAL = 0
  w_SIMVAL = space(5)
  w_DECTOT = 0
  w_DECUNI = 0
  w_CALCPICT = 0
  w_MMCAOVAL = 0
  o_MMCAOVAL = 0
  w_MMTCOLIS = space(5)
  o_MMTCOLIS = space(5)
  w_INILIS = ctod('  /  /  ')
  w_FINLIS = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_MMFLGIOM = space(1)
  w_DESAPP = space(30)
  w_DATA1 = ctod('  /  /  ')
  w_DTOBSOM = ctod('  /  /  ')
  w_VALLIS = space(3)
  w_IVALIS = space(1)
  w_QUALIS = space(1)
  w_OMAG = space(5)
  w_OMAT = space(5)
  w_OCOM = space(15)
  w_OATT = space(15)
  w_CALCPICU = 0
  w_MMCODICE = space(20)
  o_MMCODICE = space(20)
  w_MMCODART = space(20)
  w_MAGPRE = space(5)
  w_MMCODMAG = space(5)
  o_MMCODMAG = space(5)
  w_FLUBIC = space(1)
  w_MMCODMAT = space(5)
  o_MMCODMAT = space(5)
  w_F2UBIC = space(1)
  w_MMNUMRIF = 0
  w_CPROWORD = 0
  o_CPROWORD = 0
  w_UNMIS3 = space(3)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_TIPRIG = space(1)
  w_FLUSEP = space(1)
  w_UNMIS1 = space(3)
  w_GESMAT = space(1)
  w_MOLTIP = 0
  w_OPERAT = space(1)
  w_FLLOTT = space(1)
  w_CODIVA = space(5)
  w_PERIVA = 0
  w_INDIVA = 0
  w_MMCODLIS = space(5)
  o_MMCODLIS = space(5)
  w_CATSCA = space(5)
  w_ARTDIS = space(1)
  w_FLDISP = space(1)
  w_MMCAUMAG = space(5)
  w_MMCAUCOL = space(5)
  w_MMFLCASC = space(1)
  w_MMFLORDI = space(1)
  w_MMFLIMPE = space(1)
  w_MMFLRISE = space(1)
  w_MMFLELGM = space(1)
  w_MMF2CASC = space(1)
  w_MMF2ORDI = space(1)
  w_MMF2IMPE = space(1)
  w_MMF2RISE = space(1)
  w_MMCODCOM = space(15)
  w_MMFLOMAG = space(1)
  w_MMKEYSAL = space(20)
  o_MMKEYSAL = space(20)
  w_MMFLLOTT = space(1)
  w_MMF2LOTT = space(1)
  w_LIPREZZO = 0
  o_LIPREZZO = 0
  w_DESART = space(40)
  w_ACTSCOR = .F.
  w_MMUNIMIS = space(3)
  o_MMUNIMIS = space(3)
  w_FLFRAZ = space(1)
  w_MMQTAMOV = 0
  o_MMQTAMOV = 0
  w_OQTA = 0
  w_MMCODLOT = space(20)
  o_MMCODLOT = space(20)
  w_CAMBIOLOT = .F.
  o_CAMBIOLOT = .F.
  w_STATEROW = space(1)
  w_MMCODUBI = space(20)
  o_MMCODUBI = space(20)
  w_MMCODUB2 = space(20)
  o_MMCODUB2 = space(20)
  w_MMQTAUM1 = 0
  o_MMQTAUM1 = 0
  w_MMPREZZO = 0
  o_MMPREZZO = 0
  w_PREUM1 = 0
  w_MMSCONT1 = 0
  o_MMSCONT1 = 0
  w_MMSCONT2 = 0
  o_MMSCONT2 = 0
  w_MMSCONT3 = 0
  o_MMSCONT3 = 0
  w_MMSCONT4 = 0
  o_MMSCONT4 = 0
  w_VALUNI = 0
  o_VALUNI = 0
  w_VALUNI2 = 0
  w_FLAVA1 = space(1)
  w_MMVALMAG = 0
  o_MMVALMAG = 0
  w_MMIMPNAZ = 0
  w_TIPART = space(2)
  w_MMTIPATT = space(1)
  w_MMCODATT = space(15)
  w_MMIMPCOM = 0
  w_CODCOS = space(5)
  w_MMFLORCO = space(1)
  w_MMFLCOCO = space(1)
  w_MMCODCOS = space(5)
  w_FLCOMM = space(1)
  w_ULTCAR = ctod('  /  /  ')
  w_ULTSCA = ctod('  /  /  ')
  w_MMFLULCA = space(1)
  w_MMFLULPV = space(1)
  w_MMVALULT = 0
  w_MMCODMAG = space(5)
  w_MMCODMAT = space(5)
  w_QTAPER = 0
  w_QTRPER = 0
  w_QTDISC = 0
  w_Q2APER = 0
  w_Q2RPER = 0
  w_QTDISP = 0
  w_Q2DISP = 0
  w_FM1 = space(1)
  w_FM2 = space(1)
  w_TOTALE = 0
  w_PARIVA = space(12)
  w_DECTOP = 0
  w_VISNAZ = 0
  w_CALCPICP = 0
  w_MMRIFPRO = space(11)
  w_DATOBSO = ctod('  /  /  ')
  w_SCOLIS = space(1)
  w_FLSTAT = space(1)
  w_LOTZOOM = .F.
  w_FLPRG = space(1)
  w_CODCON = space(15)
  w_OLDQTA = 0
  w_UBIZOOM = .F.
  w_MMSERPOS = space(10)
  w_OBSMAG = ctod('  /  /  ')
  w_OBSMAT = ctod('  /  /  ')
  w_VARVAL = space(1)
  w_IVASCOR = 0
  w_DESLIS = space(40)
  w_TOTMAT = 0
  w_MTCARI = space(1)
  w_DATLOT = ctod('  /  /  ')
  w_NOPRSC = space(1)
  w_HASEVENT = .F.
  w_HASEVCOP = space(50)
  w_MMFLRETT = space(1)
  w_NUMGES = 0
  w_RESCHK = 0
  w_CODESC = space(5)
  w_SHOWMMT = space(1)
  w_DISLOT = space(1)
  w_UNMIS2 = space(3)
  w_QTAUM2 = 0
  w_UNIMIS2 = space(10)
  w_NOFRAZ1 = space(1)
  w_MODUM2 = space(1)
  w_RICTOT = .F.
  o_RICTOT = .F.
  w_QTOPER = 0
  w_QTIPER = 0
  w_MMLOTMAG = space(5)
  w_MMLOTMAT = space(5)
  o_MMLOTMAT = space(5)
  w_PREZUM = space(1)
  w_CLUNIMIS = space(3)
  w_OLDESE = space(4)
  w_OLDUTE = 0
  w_FLCOM1 = space(1)
  w_ARCLAMAT = space(5)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_MMSERIAL = this.W_MMSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_MMCODESE = this.W_MMCODESE
  op_MMCODUTE = this.W_MMCODUTE
  op_MMNUMREG = this.W_MMNUMREG

  * --- Children pointers
  GSVE_MMT = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsma_mvm
   * --- Gestiti i soli eventi Modifica, cancellazione, inserimento
    * --- Lanciato solo se in stato di Query per prevenire il controllo quando si
    * --- ri preme il tasto in modifica / cariacamento
    * --- F6 controlla evasione righe associate alla stessa riga di origine
   w_DELCARPRO=.NULL.
  func ah_HasCPEvents(i_cOp)
  	if (Upper(this.cFunction) = "QUERY" And (upper(i_cop)='ECPEDIT' Or upper(i_cop)='ECPDELETE')) Or (Upper(this.cFunction)$"EDIT|LOAD" And Upper(i_cop)='ECPF6')
        if  (Upper(i_cop)='ECPEDIT' And (seconds()>this.nLoadTime+60 or this.bupdated)) Or(Upper(i_cop)='ECPDELETE' And this.nLoadTime=0)
          this.LoadRecWarn()  && rilettura se sono passati piu' di 60 secondi dall' ultima lettura o il record � stato variato in Interroga
        ENDIF
  		* esegue controllo se movimento bloccato
  		* se da problemi si pu� chiamare direttamente il Batch
  		This.w_HASEVCOP=i_cop
  		This.NotifyEvent('HasEvent')
  		return ( This.w_HASEVENT )
  	else
  		return(.t.)
  	endif
    	
  endfunc
  w_oldese=space(4)
  w_oldute=0
  
  
  
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MVM_MAST','gsma_mvm')
    stdPageFrame::Init()
    *set procedure to GSVE_MMT additive
    with this
      .Pages(1).addobject("oPag","tgsma_mvmPag1","gsma_mvm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Movimenti magazzino")
      .Pages(1).HelpContextID = 246041388
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSVE_MMT
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[19]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='ART_ICOL'
    this.cWorkTables[6]='MAGAZZIN'
    this.cWorkTables[7]='KEY_ARTI'
    this.cWorkTables[8]='CAM_AGAZ'
    this.cWorkTables[9]='UNIMIS'
    this.cWorkTables[10]='ESERCIZI'
    this.cWorkTables[11]='LISTINI'
    this.cWorkTables[12]='VOCIIVA'
    this.cWorkTables[13]='SALDIART'
    this.cWorkTables[14]='ATTIVITA'
    this.cWorkTables[15]='LOTTIART'
    this.cWorkTables[16]='UBICAZIO'
    this.cWorkTables[17]='SALDILOT'
    this.cWorkTables[18]='MVM_MAST'
    this.cWorkTables[19]='MVM_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(19))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MVM_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MVM_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSVE_MMT = CREATEOBJECT('stdLazyChild',this,'GSVE_MMT')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSVE_MMT)
      this.GSVE_MMT.DestroyChildrenChain()
      this.GSVE_MMT=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSVE_MMT.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSVE_MMT.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSVE_MMT.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSVE_MMT.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_MMSERIAL,"MTSERIAL";
             ,.w_CPROWNUM,"MTROWNUM";
             ,.w_MMNUMRIF,"MTNUMRIF";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MMSERIAL = NVL(MMSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_30_joined
    link_1_30_joined=.f.
    local link_1_37_joined
    link_1_37_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    local link_2_27_joined
    link_2_27_joined=.f.
    local link_2_46_joined
    link_2_46_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from MVM_MAST where MMSERIAL=KeySet.MMSERIAL
    *
    i_nConn = i_TableProp[this.MVM_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2],this.bLoadRecFilter,this.MVM_MAST_IDX,"gsma_mvm")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MVM_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MVM_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"MVM_DETT.","MVM_MAST.")
      i_cTable = i_cTable+' MVM_MAST '
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_30_joined=this.AddJoinedLink_1_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_37_joined=this.AddJoinedLink_1_37(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MMSERIAL',this.w_MMSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CAONAZ = 0
        .w_CAUCOL = space(5)
        .w_DTOBSO = ctod("  /  /  ")
        .w_FLNDOC = space(1)
        .w_DESCAU = space(35)
        .w_DESCLF = space(40)
        .w_TIPATT = 'A'
        .w_VALCLF = space(3)
        .w_CATSCC = space(5)
        .w_CAOVAL = 0
        .w_SIMVAL = space(5)
        .w_DECTOT = 0
        .w_DECUNI = 0
        .w_INILIS = ctod("  /  /  ")
        .w_FINLIS = ctod("  /  /  ")
        .w_DESAPP = space(30)
        .w_DTOBSOM = ctod("  /  /  ")
        .w_VALLIS = space(3)
        .w_IVALIS = space(1)
        .w_QUALIS = space(1)
        .w_TOTALE = 0
        .w_PARIVA = space(12)
        .w_DECTOP = 0
        .w_DATOBSO = ctod("  /  /  ")
        .w_SCOLIS = space(1)
        .w_VARVAL = space(1)
        .w_DESLIS = space(40)
        .w_NOPRSC = space(1)
        .w_HASEVENT = .f.
        .w_HASEVCOP = space(50)
        .w_NUMGES = 0
        .w_RESCHK = 0
        .w_CODESC = space(5)
        .w_SHOWMMT = 'S'
        .w_OLDESE = space(4)
        .w_OLDUTE = 0
        .w_MMSERIAL = NVL(MMSERIAL,space(10))
        .op_MMSERIAL = .w_MMSERIAL
        .w_AZICOD = i_CODAZI
        .w_MMNUMREG = NVL(MMNUMREG,0)
        .op_MMNUMREG = .w_MMNUMREG
        .w_MMCODUTE = NVL(MMCODUTE,0)
        .op_MMCODUTE = .w_MMCODUTE
        .w_MMDATREG = NVL(cp_ToDate(MMDATREG),ctod("  /  /  "))
        .w_MMFLCLFR = NVL(MMFLCLFR,space(1))
        .w_MMCODESE = NVL(MMCODESE,space(4))
        .op_MMCODESE = .w_MMCODESE
          * evitabile
          *.link_1_7('Load')
        .w_MMVALNAZ = NVL(MMVALNAZ,space(3))
          if link_1_8_joined
            this.w_MMVALNAZ = NVL(VACODVAL108,NVL(this.w_MMVALNAZ,space(3)))
            this.w_CAONAZ = NVL(VACAOVAL108,0)
            this.w_DECTOP = NVL(VADECTOT108,0)
          else
          .link_1_8('Load')
          endif
        .w_MMTIPCON = NVL(MMTIPCON,space(1))
        .w_MMTCAMAG = NVL(MMTCAMAG,space(5))
          if link_1_14_joined
            this.w_MMTCAMAG = NVL(CMCODICE114,NVL(this.w_MMTCAMAG,space(5)))
            this.w_DESCAU = NVL(CMDESCRI114,space(35))
            this.w_MMFLCLFR = NVL(CMFLCLFR114,space(1))
            this.w_FLNDOC = NVL(CMFLNDOC114,space(1))
            this.w_CAUCOL = NVL(CMCAUCOL114,space(5))
            this.w_DTOBSOM = NVL(cp_ToDate(CMDTOBSO114),ctod("  /  /  "))
            this.w_VARVAL = NVL(CMVARVAL114,space(1))
          else
          .link_1_14('Load')
          endif
        .w_MMNUMDOC = NVL(MMNUMDOC,0)
        .w_MMALFDOC = NVL(MMALFDOC,space(10))
        .w_MMDATDOC = NVL(cp_ToDate(MMDATDOC),ctod("  /  /  "))
        .w_OBTEST = IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC)
        .w_MMCODCON = NVL(MMCODCON,space(15))
          if link_1_20_joined
            this.w_MMCODCON = NVL(ANCODICE120,NVL(this.w_MMCODCON,space(15)))
            this.w_DESCLF = NVL(ANDESCRI120,space(40))
            this.w_PARIVA = NVL(ANPARIVA120,space(12))
            this.w_VALCLF = NVL(ANCODVAL120,space(3))
            this.w_MMTCOLIS = NVL(ANNUMLIS120,space(5))
            this.w_MMSCOCL1 = NVL(AN1SCONT120,0)
            this.w_MMSCOCL2 = NVL(AN2SCONT120,0)
            this.w_CATSCC = NVL(ANCATSCM120,space(5))
            this.w_DTOBSO = NVL(cp_ToDate(ANDTOBSO120),ctod("  /  /  "))
            this.w_CODESC = NVL(ANCODESC120,space(5))
          else
          .link_1_20('Load')
          endif
        .w_MMDESSUP = NVL(MMDESSUP,space(40))
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(AH_Msgformat(IIF(.w_MMTIPCON="C", "Cliente:",IIF(.w_MMTIPCON="F","Fornitore:",""))),'','')
        .w_MMSCOCL1 = NVL(MMSCOCL1,0)
        .w_MMSCOCL2 = NVL(MMSCOCL2,0)
        .w_MMSCOPAG = NVL(MMSCOPAG,0)
        .w_MMCODVAL = NVL(MMCODVAL,space(3))
          if link_1_30_joined
            this.w_MMCODVAL = NVL(VACODVAL130,NVL(this.w_MMCODVAL,space(3)))
            this.w_DESAPP = NVL(VADESVAL130,space(30))
            this.w_SIMVAL = NVL(VASIMVAL130,space(5))
            this.w_DECTOT = NVL(VADECTOT130,0)
            this.w_DECUNI = NVL(VADECUNI130,0)
            this.w_CAOVAL = NVL(VACAOVAL130,0)
            this.w_DATOBSO = NVL(cp_ToDate(VADTOBSO130),ctod("  /  /  "))
          else
          .link_1_30('Load')
          endif
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_MMCAOVAL = NVL(MMCAOVAL,0)
        .w_MMTCOLIS = NVL(MMTCOLIS,space(5))
          if link_1_37_joined
            this.w_MMTCOLIS = NVL(LSCODLIS137,NVL(this.w_MMTCOLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS137,space(40))
            this.w_FINLIS = NVL(cp_ToDate(LSDTOBSO137),ctod("  /  /  "))
            this.w_INILIS = NVL(cp_ToDate(LSDTINVA137),ctod("  /  /  "))
            this.w_VALLIS = NVL(LSVALLIS137,space(3))
            this.w_IVALIS = NVL(LSIVALIS137,space(1))
            this.w_QUALIS = NVL(LSQUANTI137,space(1))
            this.w_SCOLIS = NVL(LSFLSCON137,space(1))
          else
          .link_1_37('Load')
          endif
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_MMFLGIOM = NVL(MMFLGIOM,space(1))
        .w_DATA1 = IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC)
        .w_OMAG = IIF(EMPTY(.w_MMCODMAG), g_MAGAZI, .w_MMCODMAG)
        .w_OMAT = .w_MMCODMAT
        .w_OCOM = .w_MMCODCOM
        .w_OATT = .w_MMCODATT
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_CALCPICP = DEFPIP(.w_DECTOP)
        .w_MMRIFPRO = NVL(MMRIFPRO,space(11))
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_90.Calculate()
        .w_FLPRG = 'M'
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .w_MMSERPOS = NVL(MMSERPOS,space(10))
        .oPgFrm.Page1.oPag.oObj_1_100.Calculate()
        .w_MMFLRETT = NVL(MMFLRETT,space(1))
        .oPgFrm.Page1.oPag.oObj_1_111.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_118.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'MVM_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from MVM_DETT where MMSERIAL=KeySet.MMSERIAL
      *                            and MMNUMRIF=KeySet.MMNUMRIF
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.MVM_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MVM_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('MVM_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "MVM_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" MVM_DETT"
        link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_27_joined=this.AddJoinedLink_2_27(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_46_joined=this.AddJoinedLink_2_46(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'MMSERIAL',this.w_MMSERIAL  )
        select * from (i_cTable) MVM_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTALE = 0
      scan
        with this
          .w_MAGPRE = space(5)
          .w_FLUBIC = space(1)
          .w_F2UBIC = space(1)
          .w_UNMIS3 = space(3)
          .w_OPERA3 = space(1)
          .w_MOLTI3 = 0
          .w_FLUSEP = space(1)
          .w_UNMIS1 = space(3)
          .w_GESMAT = space(1)
          .w_MOLTIP = 0
          .w_OPERAT = space(1)
          .w_FLLOTT = space(1)
          .w_CODIVA = space(5)
          .w_PERIVA = 0
          .w_INDIVA = 0
          .w_CATSCA = space(5)
          .w_ARTDIS = space(1)
          .w_LIPREZZO = 0
          .w_DESART = space(40)
        .w_ACTSCOR = .F.
          .w_FLFRAZ = space(1)
          .w_FLAVA1 = space(1)
          .w_TIPART = space(2)
          .w_CODCOS = space(5)
          .w_FLCOMM = space(1)
          .w_ULTCAR = ctod("  /  /  ")
          .w_ULTSCA = ctod("  /  /  ")
          .w_QTAPER = 0
          .w_QTRPER = 0
          .w_Q2APER = 0
          .w_Q2RPER = 0
          .w_FLSTAT = space(1)
        .w_LOTZOOM = .T.
        .w_UBIZOOM = .T.
          .w_OBSMAG = ctod("  /  /  ")
          .w_OBSMAT = ctod("  /  /  ")
          .w_IVASCOR = 0
        .w_TOTMAT = -1
          .w_MTCARI = space(1)
          .w_DATLOT = ctod("  /  /  ")
          .w_DISLOT = space(1)
          .w_UNMIS2 = space(3)
          .w_NOFRAZ1 = space(1)
          .w_MODUM2 = space(1)
        .w_RICTOT = .F.
          .w_QTOPER = 0
          .w_QTIPER = 0
          .w_PREZUM = space(1)
          .w_CLUNIMIS = space(3)
          .w_FLCOM1 = space(1)
          .w_ARCLAMAT = space(5)
          .w_CPROWNUM = CPROWNUM
          .w_MMCODICE = NVL(MMCODICE,space(20))
          if link_2_1_joined
            this.w_MMCODICE = NVL(CACODICE201,NVL(this.w_MMCODICE,space(20)))
            this.w_DESART = NVL(CADESART201,space(40))
            this.w_MMCODART = NVL(CACODART201,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS201,space(3))
            this.w_OPERA3 = NVL(CAOPERAT201,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP201,0)
            this.w_TIPRIG = NVL(CA__TIPO201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_MMCODART = NVL(MMCODART,space(20))
          if link_2_2_joined
            this.w_MMCODART = NVL(ARCODART202,NVL(this.w_MMCODART,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1202,space(3))
            this.w_MOLTIP = NVL(ARMOLTIP202,0)
            this.w_UNMIS2 = NVL(ARUNMIS2202,space(3))
            this.w_OPERAT = NVL(AROPERAT202,space(1))
            this.w_CATSCA = NVL(ARCATSCM202,space(5))
            this.w_CODIVA = NVL(ARCODIVA202,space(5))
            this.w_TIPART = NVL(ARTIPART202,space(2))
            this.w_ARTDIS = NVL(ARFLDISP202,space(1))
            this.w_FLUSEP = NVL(ARFLUSEP202,space(1))
            this.w_FLLOTT = NVL(ARFLLOTT202,space(1))
            this.w_MAGPRE = NVL(ARMAGPRE202,space(5))
            this.w_GESMAT = NVL(ARGESMAT202,space(1))
            this.w_DISLOT = NVL(ARDISLOT202,space(1))
            this.w_PREZUM = NVL(ARPREZUM202,space(1))
            this.w_FLCOM1 = NVL(ARSALCOM202,space(1))
            this.w_ARCLAMAT = NVL(ARCLAMAT202,space(5))
          else
          .link_2_2('Load')
          endif
          .w_MMCODMAG = NVL(MMCODMAG,space(5))
          if link_2_4_joined
            this.w_MMCODMAG = NVL(MGCODMAG204,NVL(this.w_MMCODMAG,space(5)))
            this.w_DESAPP = NVL(MGDESMAG204,space(30))
            this.w_FLUBIC = NVL(MGFLUBIC204,space(1))
            this.w_OBSMAG = NVL(cp_ToDate(MGDTOBSO204),ctod("  /  /  "))
          else
          .link_2_4('Load')
          endif
          .w_MMCODMAT = NVL(MMCODMAT,space(5))
          if link_2_6_joined
            this.w_MMCODMAT = NVL(MGCODMAG206,NVL(this.w_MMCODMAT,space(5)))
            this.w_DESAPP = NVL(MGDESMAG206,space(30))
            this.w_F2UBIC = NVL(MGFLUBIC206,space(1))
            this.w_OBSMAT = NVL(cp_ToDate(MGDTOBSO206),ctod("  /  /  "))
          else
          .link_2_6('Load')
          endif
          .w_MMNUMRIF = NVL(MMNUMRIF,0)
          .w_CPROWORD = NVL(CPROWORD,0)
        .w_TIPRIG = IIF(.w_TIPART $ 'AC-FO-FM-DE', 'X', 'R')
          .link_2_15('Load')
          .link_2_20('Load')
          .w_MMCODLIS = NVL(MMCODLIS,space(5))
        .w_FLDISP = IIF(g_PERDIS='S', .w_ARTDIS, ' ')
          .w_MMCAUMAG = NVL(MMCAUMAG,space(5))
          if link_2_27_joined
            this.w_MMCAUMAG = NVL(CMCODICE227,NVL(this.w_MMCAUMAG,space(5)))
            this.w_MMCAUCOL = NVL(CMCAUCOL227,space(5))
            this.w_MMFLCASC = NVL(CMFLCASC227,space(1))
            this.w_MMFLORDI = NVL(CMFLORDI227,space(1))
            this.w_MMFLIMPE = NVL(CMFLIMPE227,space(1))
            this.w_MMFLRISE = NVL(CMFLRISE227,space(1))
            this.w_FLAVA1 = NVL(CMFLAVAL227,space(1))
            this.w_MMFLELGM = NVL(CMFLELGM227,space(1))
            this.w_FLCOMM = NVL(CMFLCOMM227,space(1))
            this.w_MTCARI = NVL(CMMTCARI227,space(1))
          else
          .link_2_27('Load')
          endif
          .w_MMCAUCOL = NVL(MMCAUCOL,space(5))
          * evitabile
          *.link_2_28('Load')
          .w_MMFLCASC = NVL(MMFLCASC,space(1))
          .w_MMFLORDI = NVL(MMFLORDI,space(1))
          .w_MMFLIMPE = NVL(MMFLIMPE,space(1))
          .w_MMFLRISE = NVL(MMFLRISE,space(1))
          .w_MMFLELGM = NVL(MMFLELGM,space(1))
          .w_MMF2CASC = NVL(MMF2CASC,space(1))
          .w_MMF2ORDI = NVL(MMF2ORDI,space(1))
          .w_MMF2IMPE = NVL(MMF2IMPE,space(1))
          .w_MMF2RISE = NVL(MMF2RISE,space(1))
          .w_MMCODCOM = NVL(MMCODCOM,space(15))
          * evitabile
          *.link_2_38('Load')
          .w_MMFLOMAG = NVL(MMFLOMAG,space(1))
          .w_MMKEYSAL = NVL(MMKEYSAL,space(20))
          .w_MMFLLOTT = NVL(MMFLLOTT,space(1))
          .w_MMF2LOTT = NVL(MMF2LOTT,space(1))
          .w_MMUNIMIS = NVL(MMUNIMIS,space(3))
          if link_2_46_joined
            this.w_MMUNIMIS = NVL(UMCODICE246,NVL(this.w_MMUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ246,space(1))
          else
          .link_2_46('Load')
          endif
          .w_MMQTAMOV = NVL(MMQTAMOV,0)
        .w_OQTA = .w_MMQTAMOV
          .w_MMCODLOT = NVL(MMCODLOT,space(20))
          .link_2_50('Load')
        .w_CAMBIOLOT = .F.
        .w_STATEROW = IIF(this.rowstatus()$'AU' AND (.w_CAMBIOLOT or .w_MMQTAUM1>.w_OLDQTA) ,'S','N')
          .w_MMCODUBI = NVL(MMCODUBI,space(20))
          * evitabile
          *.link_2_55('Load')
          .w_MMCODUB2 = NVL(MMCODUB2,space(20))
          * evitabile
          *.link_2_56('Load')
          .w_MMQTAUM1 = NVL(MMQTAUM1,0)
          .w_MMPREZZO = NVL(MMPREZZO,0)
        .w_PREUM1 = IIF(.w_MMQTAUM1=0, 0, cp_ROUND((.w_MMPREZZO * .w_MMQTAMOV) / .w_MMQTAUM1, .w_DECUNI))
          .w_MMSCONT1 = NVL(MMSCONT1,0)
          .w_MMSCONT2 = NVL(MMSCONT2,0)
          .w_MMSCONT3 = NVL(MMSCONT3,0)
          .w_MMSCONT4 = NVL(MMSCONT4,0)
        .w_VALUNI = cp_ROUND(.w_MMPREZZO * (1+.w_MMSCONT1/100)*(1+.w_MMSCONT2/100)*(1+.w_MMSCONT3/100)*(1+.w_MMSCONT4/100),5)
        .w_VALUNI2 = cp_ROUND (.w_VALUNI * (1+.w_MMSCOCL1/100)*(1+.w_MMSCOCL2/100)*(1+.w_MMSCOPAG/100),5)
          .w_MMVALMAG = NVL(MMVALMAG,0)
          .w_MMIMPNAZ = NVL(MMIMPNAZ,0)
          .w_MMTIPATT = NVL(MMTIPATT,space(1))
          .w_MMCODATT = NVL(MMCODATT,space(15))
          .w_MMIMPCOM = NVL(MMIMPCOM,0)
          .w_MMFLORCO = NVL(MMFLORCO,space(1))
          .w_MMFLCOCO = NVL(MMFLCOCO,space(1))
          .w_MMCODCOS = NVL(MMCODCOS,space(5))
          .w_MMFLULCA = NVL(MMFLULCA,space(1))
          .w_MMFLULPV = NVL(MMFLULPV,space(1))
          .w_MMVALULT = NVL(MMVALULT,0)
          .w_MMCODMAG = NVL(MMCODMAG,space(5))
          .link_2_84('Load')
          .w_MMCODMAT = NVL(MMCODMAT,space(5))
          .link_2_85('Load')
        .w_QTDISC = .w_QTAPER-.w_QTRPER + .w_QTOPER - .w_QTIPER
        .w_QTDISP = .w_QTAPER-.w_QTRPER
        .w_Q2DISP = .w_Q2APER-.w_Q2RPER
        .w_FM1 = IIF(.cFunction='Load', 'S', ' ')
        .w_FM2 = IIF(.cFunction='Load', 'S',' ')
        .w_VISNAZ = cp_ROUND(.w_MMIMPNAZ, .w_DECTOP)
        .w_CODCON = IIF(.w_MMTIPCON='F' And Empty(.w_MMCODMAT), .w_MMCODCON, SPACE(15))
        .w_OLDQTA = .w_MMQTAUM1
        .w_QTAUM2 = Caunmis2('R', .w_MMQTAMOV, .w_UNMIS1, IIF(Not Empty(.w_UNMIS3), .w_UNMIS3, .w_UNMIS2), .w_MMUNIMIS, IIF(Not Empty(.w_UNMIS3), .w_OPERA3, .w_OPERAT), IIF(Not Empty(.w_UNMIS3), .w_MOLTI3, .w_MOLTIP))
        .oPgFrm.Page1.oPag.oObj_2_114.Calculate(IIF(.w_MMUNIMIS=.w_UNMIS1 OR EMPTY(.w_MMCODICE),IIF(Not Empty(.w_UNMIS3), Ah_MsgFormat("Qta nella 3^UM:"),IIF(Not Empty(.w_UNMIS2), Ah_MsgFormat("Qta nella 2^UM:"),'')),IIF(.w_MMUNIMIS<>.w_UNMIS1 And .w_MMUNIMIS<>.w_UNMIS2 AND .w_MMUNIMIS<>.w_UNMIS3,"", Ah_MsgFormat("Qta e prezzo nella 1^UM:"))))
        .w_UNIMIS2 = IIF(Not Empty(.w_UNMIS3), .w_UNMIS3, .w_UNMIS2)
          .w_MMLOTMAG = NVL(MMLOTMAG,space(5))
          * evitabile
          *.link_2_121('Load')
          .w_MMLOTMAT = NVL(MMLOTMAT,space(5))
          * evitabile
          *.link_2_122('Load')
        .oPgFrm.Page1.oPag.oObj_2_127.Calculate(Ah_MsgFormat("Commessa:"))
          select (this.cTrsName)
          append blank
          replace MMDATREG with .w_MMDATREG
          replace MMVALNAZ with .w_MMVALNAZ
          replace MMCODMAG with .w_MMCODMAG
          replace MMCODMAT with .w_MMCODMAT
          replace MMFLCASC with .w_MMFLCASC
          replace MMFLORDI with .w_MMFLORDI
          replace MMFLIMPE with .w_MMFLIMPE
          replace MMFLRISE with .w_MMFLRISE
          replace MMF2CASC with .w_MMF2CASC
          replace MMF2ORDI with .w_MMF2ORDI
          replace MMF2IMPE with .w_MMF2IMPE
          replace MMF2RISE with .w_MMF2RISE
          replace MMKEYSAL with .w_MMKEYSAL
          replace MMCODLOT with .w_MMCODLOT
          replace MMCODUBI with .w_MMCODUBI
          replace MMCODUB2 with .w_MMCODUB2
          replace MMQTAUM1 with .w_MMQTAUM1
          replace MMFLULCA with .w_MMFLULCA
          replace MMFLULPV with .w_MMFLULPV
          replace MMVALULT with .w_MMVALULT
          replace MMCODMAG with .w_MMCODMAG
          replace MMCODMAT with .w_MMCODMAT
          replace MMLOTMAG with .w_MMLOTMAG
          replace MMLOTMAT with .w_MMLOTMAT
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTALE = .w_TOTALE+.w_MMVALMAG
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_AZICOD = i_CODAZI
        .w_OBTEST = IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC)
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(AH_Msgformat(IIF(.w_MMTIPCON="C", "Cliente:",IIF(.w_MMTIPCON="F","Fornitore:",""))),'','')
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_DATA1 = IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC)
        .w_OMAG = IIF(EMPTY(.w_MMCODMAG), g_MAGAZI, .w_MMCODMAG)
        .w_OMAT = .w_MMCODMAT
        .w_OCOM = .w_MMCODCOM
        .w_OATT = .w_MMCODATT
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .w_CALCPICP = DEFPIP(.w_DECTOP)
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_90.Calculate()
        .w_FLPRG = 'M'
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_100.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_111.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_118.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_108.enabled = .oPgFrm.Page1.oPag.oBtn_2_108.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_109.enabled = .oPgFrm.Page1.oPag.oBtn_2_109.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_110.enabled = .oPgFrm.Page1.oPag.oBtn_2_110.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsma_mvm
    this.w_oldese=this.w_MMCODESE
    this.w_oldute=this.w_MMCODUTE
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsma_mvm
    * --- Propone l'ultima Data Reg. Inserita
    p_OLDDAT = i_datsys
    if type('this.w_MMDATREG')='D' and APPMVM='Load'
       p_OLDDAT = IIF(EMPTY(this.w_MMDATREG), i_datsys, this.w_MMDATREG)
    endif
    APPMVM=' '
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MMSERIAL=space(10)
      .w_AZICOD=space(5)
      .w_MMNUMREG=0
      .w_MMCODUTE=0
      .w_MMDATREG=ctod("  /  /  ")
      .w_MMFLCLFR=space(1)
      .w_MMCODESE=space(4)
      .w_MMVALNAZ=space(3)
      .w_CAONAZ=0
      .w_CAUCOL=space(5)
      .w_DTOBSO=ctod("  /  /  ")
      .w_FLNDOC=space(1)
      .w_MMTIPCON=space(1)
      .w_MMTCAMAG=space(5)
      .w_MMNUMDOC=0
      .w_MMALFDOC=space(10)
      .w_MMDATDOC=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_DESCAU=space(35)
      .w_MMCODCON=space(15)
      .w_DESCLF=space(40)
      .w_TIPATT=space(1)
      .w_MMDESSUP=space(40)
      .w_MMSCOCL1=0
      .w_MMSCOCL2=0
      .w_MMSCOPAG=0
      .w_VALCLF=space(3)
      .w_CATSCC=space(5)
      .w_MMCODVAL=space(3)
      .w_CAOVAL=0
      .w_SIMVAL=space(5)
      .w_DECTOT=0
      .w_DECUNI=0
      .w_CALCPICT=0
      .w_MMCAOVAL=0
      .w_MMTCOLIS=space(5)
      .w_INILIS=ctod("  /  /  ")
      .w_FINLIS=ctod("  /  /  ")
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_MMFLGIOM=space(1)
      .w_DESAPP=space(30)
      .w_DATA1=ctod("  /  /  ")
      .w_DTOBSOM=ctod("  /  /  ")
      .w_VALLIS=space(3)
      .w_IVALIS=space(1)
      .w_QUALIS=space(1)
      .w_OMAG=space(5)
      .w_OMAT=space(5)
      .w_OCOM=space(15)
      .w_OATT=space(15)
      .w_CALCPICU=0
      .w_MMCODICE=space(20)
      .w_MMCODART=space(20)
      .w_MAGPRE=space(5)
      .w_MMCODMAG=space(5)
      .w_FLUBIC=space(1)
      .w_MMCODMAT=space(5)
      .w_F2UBIC=space(1)
      .w_MMNUMRIF=0
      .w_CPROWORD=10
      .w_UNMIS3=space(3)
      .w_OPERA3=space(1)
      .w_MOLTI3=0
      .w_TIPRIG=space(1)
      .w_FLUSEP=space(1)
      .w_UNMIS1=space(3)
      .w_GESMAT=space(1)
      .w_MOLTIP=0
      .w_OPERAT=space(1)
      .w_FLLOTT=space(1)
      .w_CODIVA=space(5)
      .w_PERIVA=0
      .w_INDIVA=0
      .w_MMCODLIS=space(5)
      .w_CATSCA=space(5)
      .w_ARTDIS=space(1)
      .w_FLDISP=space(1)
      .w_MMCAUMAG=space(5)
      .w_MMCAUCOL=space(5)
      .w_MMFLCASC=space(1)
      .w_MMFLORDI=space(1)
      .w_MMFLIMPE=space(1)
      .w_MMFLRISE=space(1)
      .w_MMFLELGM=space(1)
      .w_MMF2CASC=space(1)
      .w_MMF2ORDI=space(1)
      .w_MMF2IMPE=space(1)
      .w_MMF2RISE=space(1)
      .w_MMCODCOM=space(15)
      .w_MMFLOMAG=space(1)
      .w_MMKEYSAL=space(20)
      .w_MMFLLOTT=space(1)
      .w_MMF2LOTT=space(1)
      .w_LIPREZZO=0
      .w_DESART=space(40)
      .w_ACTSCOR=.f.
      .w_MMUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_MMQTAMOV=0
      .w_OQTA=0
      .w_MMCODLOT=space(20)
      .w_CAMBIOLOT=.f.
      .w_STATEROW=space(1)
      .w_MMCODUBI=space(20)
      .w_MMCODUB2=space(20)
      .w_MMQTAUM1=0
      .w_MMPREZZO=0
      .w_PREUM1=0
      .w_MMSCONT1=0
      .w_MMSCONT2=0
      .w_MMSCONT3=0
      .w_MMSCONT4=0
      .w_VALUNI=0
      .w_VALUNI2=0
      .w_FLAVA1=space(1)
      .w_MMVALMAG=0
      .w_MMIMPNAZ=0
      .w_TIPART=space(2)
      .w_MMTIPATT=space(1)
      .w_MMCODATT=space(15)
      .w_MMIMPCOM=0
      .w_CODCOS=space(5)
      .w_MMFLORCO=space(1)
      .w_MMFLCOCO=space(1)
      .w_MMCODCOS=space(5)
      .w_FLCOMM=space(1)
      .w_ULTCAR=ctod("  /  /  ")
      .w_ULTSCA=ctod("  /  /  ")
      .w_MMFLULCA=space(1)
      .w_MMFLULPV=space(1)
      .w_MMVALULT=0
      .w_MMCODMAG=space(5)
      .w_MMCODMAT=space(5)
      .w_QTAPER=0
      .w_QTRPER=0
      .w_QTDISC=0
      .w_Q2APER=0
      .w_Q2RPER=0
      .w_QTDISP=0
      .w_Q2DISP=0
      .w_FM1=space(1)
      .w_FM2=space(1)
      .w_TOTALE=0
      .w_PARIVA=space(12)
      .w_DECTOP=0
      .w_VISNAZ=0
      .w_CALCPICP=0
      .w_MMRIFPRO=space(11)
      .w_DATOBSO=ctod("  /  /  ")
      .w_SCOLIS=space(1)
      .w_FLSTAT=space(1)
      .w_LOTZOOM=.f.
      .w_FLPRG=space(1)
      .w_CODCON=space(15)
      .w_OLDQTA=0
      .w_UBIZOOM=.f.
      .w_MMSERPOS=space(10)
      .w_OBSMAG=ctod("  /  /  ")
      .w_OBSMAT=ctod("  /  /  ")
      .w_VARVAL=space(1)
      .w_IVASCOR=0
      .w_DESLIS=space(40)
      .w_TOTMAT=0
      .w_MTCARI=space(1)
      .w_DATLOT=ctod("  /  /  ")
      .w_NOPRSC=space(1)
      .w_HASEVENT=.f.
      .w_HASEVCOP=space(50)
      .w_MMFLRETT=space(1)
      .w_NUMGES=0
      .w_RESCHK=0
      .w_CODESC=space(5)
      .w_SHOWMMT=space(1)
      .w_DISLOT=space(1)
      .w_UNMIS2=space(3)
      .w_QTAUM2=0
      .w_UNIMIS2=space(10)
      .w_NOFRAZ1=space(1)
      .w_MODUM2=space(1)
      .w_RICTOT=.f.
      .w_QTOPER=0
      .w_QTIPER=0
      .w_MMLOTMAG=space(5)
      .w_MMLOTMAT=space(5)
      .w_PREZUM=space(1)
      .w_CLUNIMIS=space(3)
      .w_OLDESE=space(4)
      .w_OLDUTE=0
      .w_FLCOM1=space(1)
      .w_ARCLAMAT=space(5)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_AZICOD = i_CODAZI
        .DoRTCalc(3,3,.f.)
        .w_MMCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
        .w_MMDATREG = IIF(TYPE('p_OLDDAT')='D', p_OLDDAT, i_datsys)
        .DoRTCalc(6,6,.f.)
        .w_MMCODESE = CALCESER(.w_MMDATREG, g_CODESE)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_MMCODESE))
         .link_1_7('Full')
        endif
        .w_MMVALNAZ = IIF(EMPTY(.w_MMVALNAZ), g_PERVAL, .w_MMVALNAZ)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_MMVALNAZ))
         .link_1_8('Full')
        endif
        .DoRTCalc(9,12,.f.)
        .w_MMTIPCON = IIF(.w_MMFLCLFR $ "CF", .w_MMFLCLFR, " ")
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_MMTCAMAG))
         .link_1_14('Full')
        endif
        .DoRTCalc(15,17,.f.)
        .w_OBTEST = IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC)
        .DoRTCalc(19,19,.f.)
        .w_MMCODCON = SPACE(15)
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_MMCODCON))
         .link_1_20('Full')
        endif
        .DoRTCalc(21,21,.f.)
        .w_TIPATT = 'A'
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(AH_Msgformat(IIF(.w_MMTIPCON="C", "Cliente:",IIF(.w_MMTIPCON="F","Fornitore:",""))),'','')
        .DoRTCalc(23,24,.f.)
        .w_MMSCOCL2 = IIF(.w_MMSCOCL1=0, 0, .w_MMSCOCL2)
        .DoRTCalc(26,28,.f.)
        .w_MMCODVAL = IIF(EMPTY(.w_VALCLF), g_PERVAL, .w_VALCLF)
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_MMCODVAL))
         .link_1_30('Full')
        endif
        .DoRTCalc(30,33,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_MMCAOVAL = GETCAM(.w_MMCODVAL, IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC), 7)
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_MMTCOLIS))
         .link_1_37('Full')
        endif
        .DoRTCalc(37,44,.f.)
        .w_DATA1 = IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC)
        .DoRTCalc(46,49,.f.)
        .w_OMAG = IIF(EMPTY(.w_MMCODMAG), g_MAGAZI, .w_MMCODMAG)
        .w_OMAT = .w_MMCODMAT
        .w_OCOM = .w_MMCODCOM
        .w_OATT = .w_MMCODATT
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .w_CALCPICU = DEFPIU(.w_DECUNI)
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_MMCODICE))
         .link_2_1('Full')
        endif
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_MMCODART))
         .link_2_2('Full')
        endif
        .DoRTCalc(57,57,.f.)
        .w_MMCODMAG = IIF(NOT EMPTY(IIF(.w_FLLOTT<>'C',SPACE(5),.w_MMCODMAG)),IIF(.w_FLLOTT<>'C',SPACE(5),.w_MMCODMAG),IIF(NOT EMPTY(.w_MAGPRE) , .w_MAGPRE, .w_OMAG))
        .DoRTCalc(58,58,.f.)
        if not(empty(.w_MMCODMAG))
         .link_2_4('Full')
        endif
        .DoRTCalc(59,59,.f.)
        .w_MMCODMAT = .w_OMAT
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_MMCODMAT))
         .link_2_6('Full')
        endif
        .DoRTCalc(61,61,.f.)
        .w_MMNUMRIF = -10
        .DoRTCalc(63,66,.f.)
        .w_TIPRIG = IIF(.w_TIPART $ 'AC-FO-FM-DE', 'X', 'R')
        .DoRTCalc(68,69,.f.)
        if not(empty(.w_UNMIS1))
         .link_2_15('Full')
        endif
        .DoRTCalc(70,74,.f.)
        if not(empty(.w_CODIVA))
         .link_2_20('Full')
        endif
        .DoRTCalc(75,76,.f.)
        .w_MMCODLIS = .w_MMTCOLIS
        .DoRTCalc(78,79,.f.)
        .w_FLDISP = IIF(g_PERDIS='S', .w_ARTDIS, ' ')
        .w_MMCAUMAG = .w_MMTCAMAG
        .DoRTCalc(81,81,.f.)
        if not(empty(.w_MMCAUMAG))
         .link_2_27('Full')
        endif
        .DoRTCalc(82,82,.f.)
        if not(empty(.w_MMCAUCOL))
         .link_2_28('Full')
        endif
        .DoRTCalc(83,92,.f.)
        if not(empty(.w_MMCODCOM))
         .link_2_38('Full')
        endif
        .w_MMFLOMAG = 'X'
        .w_MMKEYSAL = .w_MMCODART
        .w_MMFLLOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_FLUBIC='S'), LEFT(ALLTRIM(.w_MMFLCASC)+IIF(.w_MMFLRISE='+', '-', IIF(.w_MMFLRISE='-', '+', ' ')), 1), ' ')
        .w_MMF2LOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_F2UBIC='S'), LEFT(ALLTRIM(.w_MMF2CASC)+IIF(.w_MMF2RISE='+', '-', IIF(.w_MMF2RISE='-', '+', ' ')), 1), ' ')
        .DoRTCalc(97,98,.f.)
        .w_ACTSCOR = .F.
        .w_MMUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
        .DoRTCalc(100,100,.f.)
        if not(empty(.w_MMUNIMIS))
         .link_2_46('Full')
        endif
        .DoRTCalc(101,102,.f.)
        .w_OQTA = .w_MMQTAMOV
        .DoRTCalc(104,104,.f.)
        if not(empty(.w_MMCODLOT))
         .link_2_50('Full')
        endif
        .w_CAMBIOLOT = .F.
        .w_STATEROW = IIF(this.rowstatus()$'AU' AND (.w_CAMBIOLOT or .w_MMQTAUM1>.w_OLDQTA) ,'S','N')
        .DoRTCalc(107,107,.f.)
        if not(empty(.w_MMCODUBI))
         .link_2_55('Full')
        endif
        .w_MMCODUB2 = SPACE(20)
        .DoRTCalc(108,108,.f.)
        if not(empty(.w_MMCODUB2))
         .link_2_56('Full')
        endif
        .w_MMQTAUM1 = CALQTA(.w_MMQTAMOV,.w_MMUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_NOFRAZ1, .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,'I')
        .w_MMPREZZO = cp_Round(CALMMLIS(.w_LIPREZZO, .w_UNMIS1+.w_UNMIS2+.w_UNMIS3+.w_MMUNIMIS+.w_OPERAT+.w_OPERA3+.w_IVALIS+'P'+ALLTRIM(STR(.w_DECUNI)), IIF(EMPTY(.w_CLUNIMIS), .w_MOLTIP,1), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTI3,1) ,.w_PERIVA), .w_DECUNI)
        .w_PREUM1 = IIF(.w_MMQTAUM1=0, 0, cp_ROUND((.w_MMPREZZO * .w_MMQTAMOV) / .w_MMQTAUM1, .w_DECUNI))
        .DoRTCalc(112,112,.f.)
        .w_MMSCONT2 = 0
        .w_MMSCONT3 = 0
        .w_MMSCONT4 = 0
        .w_VALUNI = cp_ROUND(.w_MMPREZZO * (1+.w_MMSCONT1/100)*(1+.w_MMSCONT2/100)*(1+.w_MMSCONT3/100)*(1+.w_MMSCONT4/100),5)
        .w_VALUNI2 = cp_ROUND (.w_VALUNI * (1+.w_MMSCOCL1/100)*(1+.w_MMSCOCL2/100)*(1+.w_MMSCOPAG/100),5)
        .DoRTCalc(118,118,.f.)
        .w_MMVALMAG = cp_ROUND(.w_MMQTAMOV*.w_VALUNI2, .w_DECTOT)
        .w_MMIMPNAZ = CALCNAZ(.w_MMVALMAG,.w_MMCAOVAL,.w_CAONAZ,IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC),.w_MMVALNAZ,.w_MMCODVAL,IIF(.w_FLAVA1='A',(.w_PERIVA*.w_INDIVA),0))
        .DoRTCalc(121,121,.f.)
        .w_MMTIPATT = 'A'
        .DoRTCalc(123,131,.f.)
        .w_MMFLULCA = IIF(.w_FLAVA1='A' AND .w_MMFLCASC $ '+-' AND .w_MMDATREG>=.w_ULTCAR AND .w_MMFLOMAG<>'S' And .w_MMPREZZO<>0, '=', ' ')
        .w_MMFLULPV = IIF(.w_FLAVA1='V' AND .w_MMFLCASC $ '+-' AND .w_MMDATREG>=.w_ULTSCA AND .w_MMFLOMAG<>'S' And .w_MMPREZZO<>0, '=', ' ')
        .w_MMVALULT = IIF(.w_MMQTAUM1=0, 0, cp_ROUND(.w_MMIMPNAZ/.w_MMQTAUM1, g_PERPUL))
        .DoRTCalc(135,135,.f.)
        if not(empty(.w_MMCODMAG))
         .link_2_84('Full')
        endif
        .DoRTCalc(136,136,.f.)
        if not(empty(.w_MMCODMAT))
         .link_2_85('Full')
        endif
        .DoRTCalc(137,138,.f.)
        .w_QTDISC = .w_QTAPER-.w_QTRPER + .w_QTOPER - .w_QTIPER
        .DoRTCalc(140,141,.f.)
        .w_QTDISP = .w_QTAPER-.w_QTRPER
        .w_Q2DISP = .w_Q2APER-.w_Q2RPER
        .w_FM1 = IIF(.cFunction='Load', 'S', ' ')
        .w_FM2 = IIF(.cFunction='Load', 'S',' ')
        .DoRTCalc(146,148,.f.)
        .w_VISNAZ = cp_ROUND(.w_MMIMPNAZ, .w_DECTOP)
        .w_CALCPICP = DEFPIP(.w_DECTOP)
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_90.Calculate()
        .DoRTCalc(151,154,.f.)
        .w_LOTZOOM = .T.
        .w_FLPRG = 'M'
        .w_CODCON = IIF(.w_MMTIPCON='F' And Empty(.w_MMCODMAT), .w_MMCODCON, SPACE(15))
        .w_OLDQTA = .w_MMQTAUM1
        .w_UBIZOOM = .T.
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_100.Calculate()
        .DoRTCalc(160,165,.f.)
        .w_TOTMAT = -1
        .DoRTCalc(167,172,.f.)
        .w_NUMGES = 0
        .w_RESCHK = 0
        .DoRTCalc(175,175,.f.)
        .w_SHOWMMT = 'S'
        .oPgFrm.Page1.oPag.oObj_1_111.Calculate()
        .DoRTCalc(177,178,.f.)
        .w_QTAUM2 = Caunmis2('R', .w_MMQTAMOV, .w_UNMIS1, IIF(Not Empty(.w_UNMIS3), .w_UNMIS3, .w_UNMIS2), .w_MMUNIMIS, IIF(Not Empty(.w_UNMIS3), .w_OPERA3, .w_OPERAT), IIF(Not Empty(.w_UNMIS3), .w_MOLTI3, .w_MOLTIP))
        .oPgFrm.Page1.oPag.oObj_2_114.Calculate(IIF(.w_MMUNIMIS=.w_UNMIS1 OR EMPTY(.w_MMCODICE),IIF(Not Empty(.w_UNMIS3), Ah_MsgFormat("Qta nella 3^UM:"),IIF(Not Empty(.w_UNMIS2), Ah_MsgFormat("Qta nella 2^UM:"),'')),IIF(.w_MMUNIMIS<>.w_UNMIS1 And .w_MMUNIMIS<>.w_UNMIS2 AND .w_MMUNIMIS<>.w_UNMIS3,"", Ah_MsgFormat("Qta e prezzo nella 1^UM:"))))
        .w_UNIMIS2 = IIF(Not Empty(.w_UNMIS3), .w_UNMIS3, .w_UNMIS2)
        .DoRTCalc(181,182,.f.)
        .w_RICTOT = .F.
        .DoRTCalc(184,185,.f.)
        .w_MMLOTMAG = iif( Empty( .w_MMCODLOT ) And Empty( .w_MMCODUBI ) , SPACE(5) , .w_MMCODMAG )
        .DoRTCalc(186,186,.f.)
        if not(empty(.w_MMLOTMAG))
         .link_2_121('Full')
        endif
        .w_MMLOTMAT = iif( Empty( .w_MMCODLOT ) And Empty( .w_MMCODUB2 ) , SPACE(5) , .w_MMCODMAT )
        .DoRTCalc(187,187,.f.)
        if not(empty(.w_MMLOTMAT))
         .link_2_122('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_118.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_127.Calculate(Ah_MsgFormat("Commessa:"))
      endif
    endwith
    cp_BlankRecExtFlds(this,'MVM_MAST')
    this.DoRTCalc(188,193,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_108.enabled = this.oPgFrm.Page1.oPag.oBtn_2_108.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_109.enabled = this.oPgFrm.Page1.oPag.oBtn_2_109.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_110.enabled = this.oPgFrm.Page1.oPag.oBtn_2_110.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsma_mvm
    this.w_oldese=this.w_MMCODESE
    this.w_oldute=this.w_MMCODUTE
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMMNUMREG_1_3.enabled = i_bVal
      .Page1.oPag.oMMDATREG_1_5.enabled = i_bVal
      .Page1.oPag.oMMTCAMAG_1_14.enabled = i_bVal
      .Page1.oPag.oMMNUMDOC_1_15.enabled = i_bVal
      .Page1.oPag.oMMALFDOC_1_16.enabled = i_bVal
      .Page1.oPag.oMMDATDOC_1_17.enabled = i_bVal
      .Page1.oPag.oMMCODCON_1_20.enabled = i_bVal
      .Page1.oPag.oMMDESSUP_1_23.enabled = i_bVal
      .Page1.oPag.oMMSCOCL1_1_25.enabled = i_bVal
      .Page1.oPag.oMMSCOCL2_1_26.enabled = i_bVal
      .Page1.oPag.oMMSCOPAG_1_27.enabled = i_bVal
      .Page1.oPag.oMMCODVAL_1_30.enabled = i_bVal
      .Page1.oPag.oMMCAOVAL_1_36.enabled = i_bVal
      .Page1.oPag.oMMTCOLIS_1_37.enabled = i_bVal
      .Page1.oPag.oMMCAUMAG_2_27.enabled = i_bVal
      .Page1.oPag.oMMFLOMAG_2_39.enabled = i_bVal
      .Page1.oPag.oMMCODLOT_2_50.enabled = i_bVal
      .Page1.oPag.oMMCODUBI_2_55.enabled = i_bVal
      .Page1.oPag.oMMCODUB2_2_56.enabled = i_bVal
      .Page1.oPag.oMMQTAUM1_2_57.enabled = i_bVal
      .Page1.oPag.oPREUM1_2_59.enabled = i_bVal
      .Page1.oPag.oMMSCONT1_2_60.enabled = i_bVal
      .Page1.oPag.oMMSCONT2_2_61.enabled = i_bVal
      .Page1.oPag.oMMSCONT3_2_62.enabled = i_bVal
      .Page1.oPag.oMMSCONT4_2_63.enabled = i_bVal
      .Page1.oPag.oBtn_2_54.enabled = i_bVal
      .Page1.oPag.oBtn_1_98.enabled = i_bVal
      .Page1.oPag.oBtn_2_108.enabled = .Page1.oPag.oBtn_2_108.mCond()
      .Page1.oPag.oBtn_2_109.enabled = .Page1.oPag.oBtn_2_109.mCond()
      .Page1.oPag.oBtn_2_110.enabled = .Page1.oPag.oBtn_2_110.mCond()
      .Page1.oPag.oObj_1_76.enabled = i_bVal
      .Page1.oPag.oObj_1_77.enabled = i_bVal
      .Page1.oPag.oObj_1_78.enabled = i_bVal
      .Page1.oPag.oObj_1_85.enabled = i_bVal
      .Page1.oPag.oObj_1_88.enabled = i_bVal
      .Page1.oPag.oObj_1_100.enabled = i_bVal
      .Page1.oPag.oObj_1_111.enabled = i_bVal
      .Page1.oPag.oObj_1_118.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oMMNUMREG_1_3.enabled = .t.
        .Page1.oPag.oMMDATREG_1_5.enabled = .t.
        .Page1.oPag.oMMTCAMAG_1_14.enabled = .t.
        .Page1.oPag.oMMNUMDOC_1_15.enabled = .t.
        .Page1.oPag.oMMALFDOC_1_16.enabled = .t.
        .Page1.oPag.oMMDATDOC_1_17.enabled = .t.
        .Page1.oPag.oMMCODCON_1_20.enabled = .t.
      endif
    endwith
    this.GSVE_MMT.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'MVM_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MVM_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEMVM","i_codazi,w_MMSERIAL")
      cp_AskTableProg(this,i_nConn,"PRMVM","i_codazi,w_MMCODESE,w_MMCODUTE,w_MMNUMREG")
      .op_codazi = .w_codazi
      .op_MMSERIAL = .w_MMSERIAL
      .op_codazi = .w_codazi
      .op_MMCODESE = .w_MMCODESE
      .op_MMCODUTE = .w_MMCODUTE
      .op_MMNUMREG = .w_MMNUMREG
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *  this.GSVE_MMT.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MVM_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMSERIAL,"MMSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMNUMREG,"MMNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMCODUTE,"MMCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMDATREG,"MMDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMFLCLFR,"MMFLCLFR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMCODESE,"MMCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMVALNAZ,"MMVALNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMTIPCON,"MMTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMTCAMAG,"MMTCAMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMNUMDOC,"MMNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMALFDOC,"MMALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMDATDOC,"MMDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMCODCON,"MMCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMDESSUP,"MMDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMSCOCL1,"MMSCOCL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMSCOCL2,"MMSCOCL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMSCOPAG,"MMSCOPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMCODVAL,"MMCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMCAOVAL,"MMCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMTCOLIS,"MMTCOLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMFLGIOM,"MMFLGIOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMRIFPRO,"MMRIFPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMSERPOS,"MMSERPOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MMFLRETT,"MMFLRETT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MVM_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])
    i_lTable = "MVM_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MVM_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSMA_SMO with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MMCODICE C(20);
      ,t_MMCODMAG C(5);
      ,t_MMCODMAT C(5);
      ,t_CPROWORD N(5);
      ,t_UNMIS1 C(3);
      ,t_MMCAUMAG C(5);
      ,t_MMFLELGM N(3);
      ,t_MMCODCOM C(15);
      ,t_MMFLOMAG N(3);
      ,t_DESART C(40);
      ,t_MMUNIMIS C(3);
      ,t_MMQTAMOV N(12,3);
      ,t_MMCODLOT C(20);
      ,t_MMCODUBI C(20);
      ,t_MMCODUB2 C(20);
      ,t_MMQTAUM1 N(12,3);
      ,t_MMPREZZO N(18,5);
      ,t_PREUM1 N(18,5);
      ,t_MMSCONT1 N(6,2);
      ,t_MMSCONT2 N(6,2);
      ,t_MMSCONT3 N(6,2);
      ,t_MMSCONT4 N(6,2);
      ,t_MMVALMAG N(18,4);
      ,t_QTAPER N(12,3);
      ,t_QTDISC N(12,3);
      ,t_QTDISP N(12,3);
      ,t_Q2DISP N(12,3);
      ,t_VISNAZ N(18,4);
      ,t_QTAUM2 N(12,3);
      ,t_UNIMIS2 C(10);
      ,MMDATREG D(8);
      ,MMVALNAZ C(3);
      ,MMCODMAG C(5);
      ,MMCODMAT C(5);
      ,MMFLCASC C(1);
      ,MMFLORDI C(1);
      ,MMFLIMPE C(1);
      ,MMFLRISE C(1);
      ,MMF2CASC C(1);
      ,MMF2ORDI C(1);
      ,MMF2IMPE C(1);
      ,MMF2RISE C(1);
      ,MMKEYSAL C(20);
      ,MMCODLOT C(20);
      ,MMCODUBI C(20);
      ,MMCODUB2 C(20);
      ,MMQTAUM1 N(12,3);
      ,MMFLULCA C(1);
      ,MMFLULPV C(1);
      ,MMVALULT N(18,5);
      ,MMLOTMAG C(5);
      ,MMLOTMAT C(5);
      ,CPROWNUM N(10);
      ,t_MMCODART C(20);
      ,t_MAGPRE C(5);
      ,t_FLUBIC C(1);
      ,t_F2UBIC C(1);
      ,t_MMNUMRIF N(3);
      ,t_UNMIS3 C(3);
      ,t_OPERA3 C(1);
      ,t_MOLTI3 N(10,4);
      ,t_TIPRIG C(1);
      ,t_FLUSEP C(1);
      ,t_GESMAT C(1);
      ,t_MOLTIP N(10,5);
      ,t_OPERAT C(1);
      ,t_FLLOTT C(1);
      ,t_CODIVA C(5);
      ,t_PERIVA N(5,2);
      ,t_INDIVA N(3);
      ,t_MMCODLIS C(5);
      ,t_CATSCA C(5);
      ,t_ARTDIS C(1);
      ,t_FLDISP C(1);
      ,t_MMCAUCOL C(5);
      ,t_MMFLCASC C(1);
      ,t_MMFLORDI C(1);
      ,t_MMFLIMPE C(1);
      ,t_MMFLRISE C(1);
      ,t_MMF2CASC C(1);
      ,t_MMF2ORDI C(1);
      ,t_MMF2IMPE C(1);
      ,t_MMF2RISE C(1);
      ,t_MMKEYSAL C(20);
      ,t_MMFLLOTT C(1);
      ,t_MMF2LOTT C(1);
      ,t_LIPREZZO N(18,5);
      ,t_ACTSCOR L(1);
      ,t_FLFRAZ C(1);
      ,t_OQTA N(12,3);
      ,t_CAMBIOLOT L(1);
      ,t_STATEROW C(1);
      ,t_VALUNI N(18,5);
      ,t_VALUNI2 N(18,5);
      ,t_FLAVA1 C(1);
      ,t_MMIMPNAZ N(18,4);
      ,t_TIPART C(2);
      ,t_MMTIPATT C(1);
      ,t_MMCODATT C(15);
      ,t_MMIMPCOM N(18,4);
      ,t_CODCOS C(5);
      ,t_MMFLORCO C(1);
      ,t_MMFLCOCO C(1);
      ,t_MMCODCOS C(5);
      ,t_FLCOMM C(1);
      ,t_ULTCAR D(8);
      ,t_ULTSCA D(8);
      ,t_MMFLULCA C(1);
      ,t_MMFLULPV C(1);
      ,t_MMVALULT N(18,5);
      ,t_QTRPER N(12,3);
      ,t_Q2APER N(12,3);
      ,t_Q2RPER N(12,3);
      ,t_FM1 C(1);
      ,t_FM2 C(1);
      ,t_FLSTAT C(1);
      ,t_LOTZOOM L(1);
      ,t_CODCON C(15);
      ,t_OLDQTA N(12,3);
      ,t_UBIZOOM L(1);
      ,t_OBSMAG D(8);
      ,t_OBSMAT D(8);
      ,t_IVASCOR N(18,5);
      ,t_TOTMAT N(10);
      ,t_MTCARI C(1);
      ,t_DATLOT D(8);
      ,t_DISLOT C(1);
      ,t_UNMIS2 C(3);
      ,t_NOFRAZ1 C(1);
      ,t_MODUM2 C(1);
      ,t_RICTOT L(1);
      ,t_QTOPER N(12,3);
      ,t_QTIPER N(12,3);
      ,t_MMLOTMAG C(5);
      ,t_MMLOTMAT C(5);
      ,t_PREZUM C(1);
      ,t_CLUNIMIS C(3);
      ,t_FLCOM1 C(1);
      ,t_ARCLAMAT C(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsma_mvmbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODICE_2_1.controlsource=this.cTrsName+'.t_MMCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODMAG_2_4.controlsource=this.cTrsName+'.t_MMCODMAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODMAT_2_6.controlsource=this.cTrsName+'.t_MMCODMAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_9.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oUNMIS1_2_15.controlsource=this.cTrsName+'.t_UNMIS1'
    this.oPgFRm.Page1.oPag.oMMCAUMAG_2_27.controlsource=this.cTrsName+'.t_MMCAUMAG'
    this.oPgFRm.Page1.oPag.oMMFLELGM_2_33.controlsource=this.cTrsName+'.t_MMFLELGM'
    this.oPgFRm.Page1.oPag.oMMCODCOM_2_38.controlsource=this.cTrsName+'.t_MMCODCOM'
    this.oPgFRm.Page1.oPag.oMMFLOMAG_2_39.controlsource=this.cTrsName+'.t_MMFLOMAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_44.controlsource=this.cTrsName+'.t_DESART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMMUNIMIS_2_46.controlsource=this.cTrsName+'.t_MMUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMMQTAMOV_2_48.controlsource=this.cTrsName+'.t_MMQTAMOV'
    this.oPgFRm.Page1.oPag.oMMCODLOT_2_50.controlsource=this.cTrsName+'.t_MMCODLOT'
    this.oPgFRm.Page1.oPag.oMMCODUBI_2_55.controlsource=this.cTrsName+'.t_MMCODUBI'
    this.oPgFRm.Page1.oPag.oMMCODUB2_2_56.controlsource=this.cTrsName+'.t_MMCODUB2'
    this.oPgFRm.Page1.oPag.oMMQTAUM1_2_57.controlsource=this.cTrsName+'.t_MMQTAUM1'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMMPREZZO_2_58.controlsource=this.cTrsName+'.t_MMPREZZO'
    this.oPgFRm.Page1.oPag.oPREUM1_2_59.controlsource=this.cTrsName+'.t_PREUM1'
    this.oPgFRm.Page1.oPag.oMMSCONT1_2_60.controlsource=this.cTrsName+'.t_MMSCONT1'
    this.oPgFRm.Page1.oPag.oMMSCONT2_2_61.controlsource=this.cTrsName+'.t_MMSCONT2'
    this.oPgFRm.Page1.oPag.oMMSCONT3_2_62.controlsource=this.cTrsName+'.t_MMSCONT3'
    this.oPgFRm.Page1.oPag.oMMSCONT4_2_63.controlsource=this.cTrsName+'.t_MMSCONT4'
    this.oPgFRm.Page1.oPag.oMMVALMAG_2_67.controlsource=this.cTrsName+'.t_MMVALMAG'
    this.oPgFRm.Page1.oPag.oQTAPER_2_86.controlsource=this.cTrsName+'.t_QTAPER'
    this.oPgFRm.Page1.oPag.oQTDISC_2_88.controlsource=this.cTrsName+'.t_QTDISC'
    this.oPgFRm.Page1.oPag.oQTDISP_2_91.controlsource=this.cTrsName+'.t_QTDISP'
    this.oPgFRm.Page1.oPag.oQ2DISP_2_92.controlsource=this.cTrsName+'.t_Q2DISP'
    this.oPgFRm.Page1.oPag.oVISNAZ_2_95.controlsource=this.cTrsName+'.t_VISNAZ'
    this.oPgFRm.Page1.oPag.oQTAUM2_2_113.controlsource=this.cTrsName+'.t_QTAUM2'
    this.oPgFRm.Page1.oPag.oUNIMIS2_2_115.controlsource=this.cTrsName+'.t_UNIMIS2'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(49)
    this.AddVLine(201)
    this.AddVLine(409)
    this.AddVLine(463)
    this.AddVLine(517)
    this.AddVLine(561)
    this.AddVLine(643)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODICE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MVM_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEMVM","i_codazi,w_MMSERIAL")
          cp_NextTableProg(this,i_nConn,"PRMVM","i_codazi,w_MMCODESE,w_MMCODUTE,w_MMNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MVM_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MVM_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'MVM_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(MMSERIAL,MMNUMREG,MMCODUTE,MMDATREG,MMFLCLFR"+;
                  ",MMCODESE,MMVALNAZ,MMTIPCON,MMTCAMAG,MMNUMDOC"+;
                  ",MMALFDOC,MMDATDOC,MMCODCON,MMDESSUP,MMSCOCL1"+;
                  ",MMSCOCL2,MMSCOPAG,MMCODVAL,MMCAOVAL,MMTCOLIS"+;
                  ",UTCC,UTCV,UTDC,UTDV,MMFLGIOM"+;
                  ",MMRIFPRO,MMSERPOS,MMFLRETT"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_MMSERIAL)+;
                    ","+cp_ToStrODBC(this.w_MMNUMREG)+;
                    ","+cp_ToStrODBC(this.w_MMCODUTE)+;
                    ","+cp_ToStrODBC(this.w_MMDATREG)+;
                    ","+cp_ToStrODBC(this.w_MMFLCLFR)+;
                    ","+cp_ToStrODBCNull(this.w_MMCODESE)+;
                    ","+cp_ToStrODBCNull(this.w_MMVALNAZ)+;
                    ","+cp_ToStrODBC(this.w_MMTIPCON)+;
                    ","+cp_ToStrODBCNull(this.w_MMTCAMAG)+;
                    ","+cp_ToStrODBC(this.w_MMNUMDOC)+;
                    ","+cp_ToStrODBC(this.w_MMALFDOC)+;
                    ","+cp_ToStrODBC(this.w_MMDATDOC)+;
                    ","+cp_ToStrODBCNull(this.w_MMCODCON)+;
                    ","+cp_ToStrODBC(this.w_MMDESSUP)+;
                    ","+cp_ToStrODBC(this.w_MMSCOCL1)+;
                    ","+cp_ToStrODBC(this.w_MMSCOCL2)+;
                    ","+cp_ToStrODBC(this.w_MMSCOPAG)+;
                    ","+cp_ToStrODBCNull(this.w_MMCODVAL)+;
                    ","+cp_ToStrODBC(this.w_MMCAOVAL)+;
                    ","+cp_ToStrODBCNull(this.w_MMTCOLIS)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_MMFLGIOM)+;
                    ","+cp_ToStrODBC(this.w_MMRIFPRO)+;
                    ","+cp_ToStrODBC(this.w_MMSERPOS)+;
                    ","+cp_ToStrODBC(this.w_MMFLRETT)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MVM_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'MVM_MAST')
        cp_CheckDeletedKey(i_cTable,0,'MMSERIAL',this.w_MMSERIAL)
        INSERT INTO (i_cTable);
              (MMSERIAL,MMNUMREG,MMCODUTE,MMDATREG,MMFLCLFR,MMCODESE,MMVALNAZ,MMTIPCON,MMTCAMAG,MMNUMDOC,MMALFDOC,MMDATDOC,MMCODCON,MMDESSUP,MMSCOCL1,MMSCOCL2,MMSCOPAG,MMCODVAL,MMCAOVAL,MMTCOLIS,UTCC,UTCV,UTDC,UTDV,MMFLGIOM,MMRIFPRO,MMSERPOS,MMFLRETT &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_MMSERIAL;
                  ,this.w_MMNUMREG;
                  ,this.w_MMCODUTE;
                  ,this.w_MMDATREG;
                  ,this.w_MMFLCLFR;
                  ,this.w_MMCODESE;
                  ,this.w_MMVALNAZ;
                  ,this.w_MMTIPCON;
                  ,this.w_MMTCAMAG;
                  ,this.w_MMNUMDOC;
                  ,this.w_MMALFDOC;
                  ,this.w_MMDATDOC;
                  ,this.w_MMCODCON;
                  ,this.w_MMDESSUP;
                  ,this.w_MMSCOCL1;
                  ,this.w_MMSCOCL2;
                  ,this.w_MMSCOPAG;
                  ,this.w_MMCODVAL;
                  ,this.w_MMCAOVAL;
                  ,this.w_MMTCOLIS;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_MMFLGIOM;
                  ,this.w_MMRIFPRO;
                  ,this.w_MMSERPOS;
                  ,this.w_MMFLRETT;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MVM_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MVM_DETT_IDX,2])
      *
      * insert into MVM_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(MMSERIAL,MMCODICE,MMCODART,MMCODMAG,MMCODMAT"+;
                  ",MMNUMRIF,CPROWORD,MMCODLIS,MMCAUMAG,MMCAUCOL"+;
                  ",MMFLCASC,MMFLORDI,MMFLIMPE,MMFLRISE,MMFLELGM"+;
                  ",MMF2CASC,MMF2ORDI,MMF2IMPE,MMF2RISE,MMCODCOM"+;
                  ",MMFLOMAG,MMKEYSAL,MMFLLOTT,MMF2LOTT,MMUNIMIS"+;
                  ",MMQTAMOV,MMCODLOT,MMCODUBI,MMCODUB2,MMQTAUM1"+;
                  ",MMPREZZO,MMSCONT1,MMSCONT2,MMSCONT3,MMSCONT4"+;
                  ",MMVALMAG,MMIMPNAZ,MMTIPATT,MMCODATT,MMIMPCOM"+;
                  ",MMFLORCO,MMFLCOCO,MMCODCOS,MMFLULCA,MMFLULPV"+;
                  ",MMVALULT,MMLOTMAG,MMLOTMAT,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MMSERIAL)+","+cp_ToStrODBCNull(this.w_MMCODICE)+","+cp_ToStrODBCNull(this.w_MMCODART)+","+cp_ToStrODBCNull(this.w_MMCODMAG)+","+cp_ToStrODBCNull(this.w_MMCODMAT)+;
             ","+cp_ToStrODBC(this.w_MMNUMRIF)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_MMCODLIS)+","+cp_ToStrODBCNull(this.w_MMCAUMAG)+","+cp_ToStrODBCNull(this.w_MMCAUCOL)+;
             ","+cp_ToStrODBC(this.w_MMFLCASC)+","+cp_ToStrODBC(this.w_MMFLORDI)+","+cp_ToStrODBC(this.w_MMFLIMPE)+","+cp_ToStrODBC(this.w_MMFLRISE)+","+cp_ToStrODBC(this.w_MMFLELGM)+;
             ","+cp_ToStrODBC(this.w_MMF2CASC)+","+cp_ToStrODBC(this.w_MMF2ORDI)+","+cp_ToStrODBC(this.w_MMF2IMPE)+","+cp_ToStrODBC(this.w_MMF2RISE)+","+cp_ToStrODBCNull(this.w_MMCODCOM)+;
             ","+cp_ToStrODBC(this.w_MMFLOMAG)+","+cp_ToStrODBC(this.w_MMKEYSAL)+","+cp_ToStrODBC(this.w_MMFLLOTT)+","+cp_ToStrODBC(this.w_MMF2LOTT)+","+cp_ToStrODBCNull(this.w_MMUNIMIS)+;
             ","+cp_ToStrODBC(this.w_MMQTAMOV)+","+cp_ToStrODBCNull(this.w_MMCODLOT)+","+cp_ToStrODBCNull(this.w_MMCODUBI)+","+cp_ToStrODBCNull(this.w_MMCODUB2)+","+cp_ToStrODBC(this.w_MMQTAUM1)+;
             ","+cp_ToStrODBC(this.w_MMPREZZO)+","+cp_ToStrODBC(this.w_MMSCONT1)+","+cp_ToStrODBC(this.w_MMSCONT2)+","+cp_ToStrODBC(this.w_MMSCONT3)+","+cp_ToStrODBC(this.w_MMSCONT4)+;
             ","+cp_ToStrODBC(this.w_MMVALMAG)+","+cp_ToStrODBC(this.w_MMIMPNAZ)+","+cp_ToStrODBC(this.w_MMTIPATT)+","+cp_ToStrODBC(this.w_MMCODATT)+","+cp_ToStrODBC(this.w_MMIMPCOM)+;
             ","+cp_ToStrODBC(this.w_MMFLORCO)+","+cp_ToStrODBC(this.w_MMFLCOCO)+","+cp_ToStrODBC(this.w_MMCODCOS)+","+cp_ToStrODBC(this.w_MMFLULCA)+","+cp_ToStrODBC(this.w_MMFLULPV)+;
             ","+cp_ToStrODBC(this.w_MMVALULT)+","+cp_ToStrODBCNull(this.w_MMLOTMAG)+","+cp_ToStrODBCNull(this.w_MMLOTMAT)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MMSERIAL',this.w_MMSERIAL,'MMNUMRIF',this.w_MMNUMRIF)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_MMSERIAL,this.w_MMCODICE,this.w_MMCODART,this.w_MMCODMAG,this.w_MMCODMAT"+;
                ",this.w_MMNUMRIF,this.w_CPROWORD,this.w_MMCODLIS,this.w_MMCAUMAG,this.w_MMCAUCOL"+;
                ",this.w_MMFLCASC,this.w_MMFLORDI,this.w_MMFLIMPE,this.w_MMFLRISE,this.w_MMFLELGM"+;
                ",this.w_MMF2CASC,this.w_MMF2ORDI,this.w_MMF2IMPE,this.w_MMF2RISE,this.w_MMCODCOM"+;
                ",this.w_MMFLOMAG,this.w_MMKEYSAL,this.w_MMFLLOTT,this.w_MMF2LOTT,this.w_MMUNIMIS"+;
                ",this.w_MMQTAMOV,this.w_MMCODLOT,this.w_MMCODUBI,this.w_MMCODUB2,this.w_MMQTAUM1"+;
                ",this.w_MMPREZZO,this.w_MMSCONT1,this.w_MMSCONT2,this.w_MMSCONT3,this.w_MMSCONT4"+;
                ",this.w_MMVALMAG,this.w_MMIMPNAZ,this.w_MMTIPATT,this.w_MMCODATT,this.w_MMIMPCOM"+;
                ",this.w_MMFLORCO,this.w_MMFLCOCO,this.w_MMCODCOS,this.w_MMFLULCA,this.w_MMFLULPV"+;
                ",this.w_MMVALULT,this.w_MMLOTMAG,this.w_MMLOTMAT,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- gsma_mvm
    * --- Alla conferma consideto sempre anche la testata
    * --- modificata (gestione data/utente modifica se cambio solo dati ripetuti)
    this.bHeaderUpdated = .t.
    
    * --- Nel caso di modifica di anno o utente in modifica aggiorno il progressivo
    with this
    if ( .w_oldese<>.w_MMCODESE .or. .w_oldute<>.w_MMCODUTE) and .cfunction="Edit"
        i_nConn = i_TableProp[this.MVM_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])
        cp_NextTableProg(this,i_nConn,"PRMVM","i_codazi,w_MMCODESE,w_MMCODUTE,w_MMNUMREG")
    endif
    endwith
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MVM_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update MVM_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'MVM_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " MMNUMREG="+cp_ToStrODBC(this.w_MMNUMREG)+;
             ",MMCODUTE="+cp_ToStrODBC(this.w_MMCODUTE)+;
             ",MMDATREG="+cp_ToStrODBC(this.w_MMDATREG)+;
             ",MMFLCLFR="+cp_ToStrODBC(this.w_MMFLCLFR)+;
             ",MMCODESE="+cp_ToStrODBCNull(this.w_MMCODESE)+;
             ",MMVALNAZ="+cp_ToStrODBCNull(this.w_MMVALNAZ)+;
             ",MMTIPCON="+cp_ToStrODBC(this.w_MMTIPCON)+;
             ",MMTCAMAG="+cp_ToStrODBCNull(this.w_MMTCAMAG)+;
             ",MMNUMDOC="+cp_ToStrODBC(this.w_MMNUMDOC)+;
             ",MMALFDOC="+cp_ToStrODBC(this.w_MMALFDOC)+;
             ",MMDATDOC="+cp_ToStrODBC(this.w_MMDATDOC)+;
             ",MMCODCON="+cp_ToStrODBCNull(this.w_MMCODCON)+;
             ",MMDESSUP="+cp_ToStrODBC(this.w_MMDESSUP)+;
             ",MMSCOCL1="+cp_ToStrODBC(this.w_MMSCOCL1)+;
             ",MMSCOCL2="+cp_ToStrODBC(this.w_MMSCOCL2)+;
             ",MMSCOPAG="+cp_ToStrODBC(this.w_MMSCOPAG)+;
             ",MMCODVAL="+cp_ToStrODBCNull(this.w_MMCODVAL)+;
             ",MMCAOVAL="+cp_ToStrODBC(this.w_MMCAOVAL)+;
             ",MMTCOLIS="+cp_ToStrODBCNull(this.w_MMTCOLIS)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",MMFLGIOM="+cp_ToStrODBC(this.w_MMFLGIOM)+;
             ",MMRIFPRO="+cp_ToStrODBC(this.w_MMRIFPRO)+;
             ",MMSERPOS="+cp_ToStrODBC(this.w_MMSERPOS)+;
             ",MMFLRETT="+cp_ToStrODBC(this.w_MMFLRETT)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'MVM_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'MMSERIAL',this.w_MMSERIAL  )
          UPDATE (i_cTable) SET;
              MMNUMREG=this.w_MMNUMREG;
             ,MMCODUTE=this.w_MMCODUTE;
             ,MMDATREG=this.w_MMDATREG;
             ,MMFLCLFR=this.w_MMFLCLFR;
             ,MMCODESE=this.w_MMCODESE;
             ,MMVALNAZ=this.w_MMVALNAZ;
             ,MMTIPCON=this.w_MMTIPCON;
             ,MMTCAMAG=this.w_MMTCAMAG;
             ,MMNUMDOC=this.w_MMNUMDOC;
             ,MMALFDOC=this.w_MMALFDOC;
             ,MMDATDOC=this.w_MMDATDOC;
             ,MMCODCON=this.w_MMCODCON;
             ,MMDESSUP=this.w_MMDESSUP;
             ,MMSCOCL1=this.w_MMSCOCL1;
             ,MMSCOCL2=this.w_MMSCOCL2;
             ,MMSCOPAG=this.w_MMSCOPAG;
             ,MMCODVAL=this.w_MMCODVAL;
             ,MMCAOVAL=this.w_MMCAOVAL;
             ,MMTCOLIS=this.w_MMTCOLIS;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,MMFLGIOM=this.w_MMFLGIOM;
             ,MMRIFPRO=this.w_MMRIFPRO;
             ,MMSERPOS=this.w_MMSERPOS;
             ,MMFLRETT=this.w_MMFLRETT;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_MMDATREG<>MMDATREG
            i_bUpdAll = .t.
          endif
          if this.w_MMVALNAZ<>MMVALNAZ
            i_bUpdAll = .t.
          endif
        endif
        scan for (t_CPROWORD<>0 AND NOT EMPTY(t_MMCODICE)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.MVM_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.MVM_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSVE_MMT.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_MMSERIAL,"MTSERIAL";
                     ,this.w_CPROWNUM,"MTROWNUM";
                     ,this.w_MMNUMRIF,"MTNUMRIF";
                     )
              this.GSVE_MMT.mDelete()
              *
              * delete from MVM_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MVM_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MMCODICE="+cp_ToStrODBCNull(this.w_MMCODICE)+;
                     ",MMCODART="+cp_ToStrODBCNull(this.w_MMCODART)+;
                     ",MMCODMAG="+cp_ToStrODBCNull(this.w_MMCODMAG)+;
                     ",MMCODMAT="+cp_ToStrODBCNull(this.w_MMCODMAT)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MMCODLIS="+cp_ToStrODBC(this.w_MMCODLIS)+;
                     ",MMCAUMAG="+cp_ToStrODBCNull(this.w_MMCAUMAG)+;
                     ",MMCAUCOL="+cp_ToStrODBCNull(this.w_MMCAUCOL)+;
                     ",MMFLCASC="+cp_ToStrODBC(this.w_MMFLCASC)+;
                     ",MMFLORDI="+cp_ToStrODBC(this.w_MMFLORDI)+;
                     ",MMFLIMPE="+cp_ToStrODBC(this.w_MMFLIMPE)+;
                     ",MMFLRISE="+cp_ToStrODBC(this.w_MMFLRISE)+;
                     ",MMFLELGM="+cp_ToStrODBC(this.w_MMFLELGM)+;
                     ",MMF2CASC="+cp_ToStrODBC(this.w_MMF2CASC)+;
                     ",MMF2ORDI="+cp_ToStrODBC(this.w_MMF2ORDI)+;
                     ",MMF2IMPE="+cp_ToStrODBC(this.w_MMF2IMPE)+;
                     ",MMF2RISE="+cp_ToStrODBC(this.w_MMF2RISE)+;
                     ",MMCODCOM="+cp_ToStrODBCNull(this.w_MMCODCOM)+;
                     ",MMFLOMAG="+cp_ToStrODBC(this.w_MMFLOMAG)+;
                     ",MMKEYSAL="+cp_ToStrODBC(this.w_MMKEYSAL)+;
                     ",MMFLLOTT="+cp_ToStrODBC(this.w_MMFLLOTT)+;
                     ",MMF2LOTT="+cp_ToStrODBC(this.w_MMF2LOTT)+;
                     ",MMUNIMIS="+cp_ToStrODBCNull(this.w_MMUNIMIS)+;
                     ",MMQTAMOV="+cp_ToStrODBC(this.w_MMQTAMOV)+;
                     ",MMCODLOT="+cp_ToStrODBCNull(this.w_MMCODLOT)+;
                     ",MMCODUBI="+cp_ToStrODBCNull(this.w_MMCODUBI)+;
                     ",MMCODUB2="+cp_ToStrODBCNull(this.w_MMCODUB2)+;
                     ",MMQTAUM1="+cp_ToStrODBC(this.w_MMQTAUM1)+;
                     ",MMPREZZO="+cp_ToStrODBC(this.w_MMPREZZO)+;
                     ",MMSCONT1="+cp_ToStrODBC(this.w_MMSCONT1)+;
                     ",MMSCONT2="+cp_ToStrODBC(this.w_MMSCONT2)+;
                     ",MMSCONT3="+cp_ToStrODBC(this.w_MMSCONT3)+;
                     ",MMSCONT4="+cp_ToStrODBC(this.w_MMSCONT4)+;
                     ",MMVALMAG="+cp_ToStrODBC(this.w_MMVALMAG)+;
                     ",MMIMPNAZ="+cp_ToStrODBC(this.w_MMIMPNAZ)+;
                     ",MMTIPATT="+cp_ToStrODBC(this.w_MMTIPATT)+;
                     ",MMCODATT="+cp_ToStrODBC(this.w_MMCODATT)+;
                     ",MMIMPCOM="+cp_ToStrODBC(this.w_MMIMPCOM)+;
                     ",MMFLORCO="+cp_ToStrODBC(this.w_MMFLORCO)+;
                     ",MMFLCOCO="+cp_ToStrODBC(this.w_MMFLCOCO)+;
                     ",MMCODCOS="+cp_ToStrODBC(this.w_MMCODCOS)+;
                     ",MMFLULCA="+cp_ToStrODBC(this.w_MMFLULCA)+;
                     ",MMFLULPV="+cp_ToStrODBC(this.w_MMFLULPV)+;
                     ",MMVALULT="+cp_ToStrODBC(this.w_MMVALULT)+;
                     ",MMLOTMAG="+cp_ToStrODBCNull(this.w_MMLOTMAG)+;
                     ",MMLOTMAT="+cp_ToStrODBCNull(this.w_MMLOTMAT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MMCODICE=this.w_MMCODICE;
                     ,MMCODART=this.w_MMCODART;
                     ,MMCODMAG=this.w_MMCODMAG;
                     ,MMCODMAT=this.w_MMCODMAT;
                     ,CPROWORD=this.w_CPROWORD;
                     ,MMCODLIS=this.w_MMCODLIS;
                     ,MMCAUMAG=this.w_MMCAUMAG;
                     ,MMCAUCOL=this.w_MMCAUCOL;
                     ,MMFLCASC=this.w_MMFLCASC;
                     ,MMFLORDI=this.w_MMFLORDI;
                     ,MMFLIMPE=this.w_MMFLIMPE;
                     ,MMFLRISE=this.w_MMFLRISE;
                     ,MMFLELGM=this.w_MMFLELGM;
                     ,MMF2CASC=this.w_MMF2CASC;
                     ,MMF2ORDI=this.w_MMF2ORDI;
                     ,MMF2IMPE=this.w_MMF2IMPE;
                     ,MMF2RISE=this.w_MMF2RISE;
                     ,MMCODCOM=this.w_MMCODCOM;
                     ,MMFLOMAG=this.w_MMFLOMAG;
                     ,MMKEYSAL=this.w_MMKEYSAL;
                     ,MMFLLOTT=this.w_MMFLLOTT;
                     ,MMF2LOTT=this.w_MMF2LOTT;
                     ,MMUNIMIS=this.w_MMUNIMIS;
                     ,MMQTAMOV=this.w_MMQTAMOV;
                     ,MMCODLOT=this.w_MMCODLOT;
                     ,MMCODUBI=this.w_MMCODUBI;
                     ,MMCODUB2=this.w_MMCODUB2;
                     ,MMQTAUM1=this.w_MMQTAUM1;
                     ,MMPREZZO=this.w_MMPREZZO;
                     ,MMSCONT1=this.w_MMSCONT1;
                     ,MMSCONT2=this.w_MMSCONT2;
                     ,MMSCONT3=this.w_MMSCONT3;
                     ,MMSCONT4=this.w_MMSCONT4;
                     ,MMVALMAG=this.w_MMVALMAG;
                     ,MMIMPNAZ=this.w_MMIMPNAZ;
                     ,MMTIPATT=this.w_MMTIPATT;
                     ,MMCODATT=this.w_MMCODATT;
                     ,MMIMPCOM=this.w_MMIMPCOM;
                     ,MMFLORCO=this.w_MMFLORCO;
                     ,MMFLCOCO=this.w_MMFLCOCO;
                     ,MMCODCOS=this.w_MMCODCOS;
                     ,MMFLULCA=this.w_MMFLULCA;
                     ,MMFLULPV=this.w_MMFLULPV;
                     ,MMVALULT=this.w_MMVALULT;
                     ,MMLOTMAG=this.w_MMLOTMAG;
                     ,MMLOTMAT=this.w_MMLOTMAT;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (t_CPROWORD<>0 AND NOT EMPTY(t_MMCODICE))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSVE_MMT.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_MMSERIAL,"MTSERIAL";
             ,this.w_CPROWNUM,"MTROWNUM";
             ,this.w_MMNUMRIF,"MTNUMRIF";
             )
        this.GSVE_MMT.mReplace()
        this.GSVE_MMT.bSaveContext=.f.
      endscan
     this.GSVE_MMT.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- gsma_mvm
    if not(bTrsErr)
       select (this.cTrsName)
       go top
       this.WorkFromTrs()
        this.SaveDependsOn()
       * --- Controlli Finali
       this.NotifyEvent('ControlliFinali')
       * --- Riposiziona sul Primo record del Temporaneo di M.M.
       * --- Perche' in caso di errore il Puntatore non si Riposiziona giusto
       select (this.cTrsName)
       go top
       this.WorkFromTrs()
       this.SaveDependsOn()
       * ---Memorizza l'ultima operazione eseguita
       APPMVM=this.cFunction
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4,i_cOp5,i_cOp6,i_cOp7,i_cOp8,i_cOp9,i_cOp10,i_cOp11,i_cOp12

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MMFLCASC,space(1))==this.w_MMFLCASC;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMFLRISE,space(1))==this.w_MMFLRISE;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMFLORDI,space(1))==this.w_MMFLORDI;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMFLIMPE,space(1))==this.w_MMFLIMPE;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMFLULPV,space(1))==this.w_MMFLULPV;
              and NVL(&i_cF..MMDATREG,ctod("  /  /  "))==this.w_MMDATREG;
              and NVL(&i_cF..MMFLULPV,space(1))==this.w_MMFLULPV;
              and NVL(&i_cF..MMVALNAZ,space(3))==this.w_MMVALNAZ;
              and NVL(&i_cF..MMFLULCA,space(1))==this.w_MMFLULCA;
              and NVL(&i_cF..MMVALULT,0)==this.w_MMVALULT;
              and NVL(&i_cF..MMFLULCA,space(1))==this.w_MMFLULCA;
              and NVL(&i_cF..MMDATREG,ctod("  /  /  "))==this.w_MMDATREG;
              and NVL(&i_cF..MMFLULCA,space(1))==this.w_MMFLULCA;
              and NVL(&i_cF..MMVALNAZ,space(3))==this.w_MMVALNAZ;
              and NVL(&i_cF..MMFLULPV,space(1))==this.w_MMFLULPV;
              and NVL(&i_cF..MMVALULT,0)==this.w_MMVALULT;
              and NVL(&i_cF..MMCODMAG,space(5))==this.w_MMCODMAG;
              and NVL(&i_cF..MMKEYSAL,space(20))==this.w_MMKEYSAL;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MMFLCASC,space(1)),'SLQTAPER','',NVL(&i_cF..MMQTAUM1,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..MMFLRISE,space(1)),'SLQTRPER','',NVL(&i_cF..MMQTAUM1,0),'restore',i_nConn)
      i_cOp3=cp_SetTrsOp(NVL(&i_cF..MMFLORDI,space(1)),'SLQTOPER','',NVL(&i_cF..MMQTAUM1,0),'restore',i_nConn)
      i_cOp4=cp_SetTrsOp(NVL(&i_cF..MMFLIMPE,space(1)),'SLQTIPER','',NVL(&i_cF..MMQTAUM1,0),'restore',i_nConn)
      i_cOp5=cp_SetTrsOp(NVL(&i_cF..MMFLULPV,space(1)),'SLDATUPV','',NVL(&i_cF..MMDATREG,ctod("  /  /  ")),'restore',i_nConn)
      i_cOp6=cp_SetTrsOp(NVL(&i_cF..MMFLULPV,space(1)),'SLCODVAV','',NVL(&i_cF..MMVALNAZ,space(3)),'restore',i_nConn)
      i_cOp7=cp_SetTrsOp(NVL(&i_cF..MMFLULCA,space(1)),'SLVALUCA','',NVL(&i_cF..MMVALULT,0),'restore',i_nConn)
      i_cOp8=cp_SetTrsOp(NVL(&i_cF..MMFLULCA,space(1)),'SLDATUCA','',NVL(&i_cF..MMDATREG,ctod("  /  /  ")),'restore',i_nConn)
      i_cOp9=cp_SetTrsOp(NVL(&i_cF..MMFLULCA,space(1)),'SLCODVAA','',NVL(&i_cF..MMVALNAZ,space(3)),'restore',i_nConn)
      i_cOp10=cp_SetTrsOp(NVL(&i_cF..MMFLULPV,space(1)),'SLVALUPV','',NVL(&i_cF..MMVALULT,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MMCODMAG,space(5))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" SLQTAPER="+i_cOp1+","           +" SLQTRPER="+i_cOp2+","           +" SLQTOPER="+i_cOp3+","           +" SLQTIPER="+i_cOp4+","           +" SLDATUPV="+i_cOp5+","           +" SLCODVAV="+i_cOp6+","           +" SLVALUCA="+i_cOp7+","           +" SLDATUCA="+i_cOp8+","           +" SLCODVAA="+i_cOp9+","           +" SLVALUPV="+i_cOp10+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SLCODMAG="+cp_ToStrODBC(NVL(&i_cF..MMCODMAG,space(5)));
             +" AND SLCODICE="+cp_ToStrODBC(NVL(&i_cF..MMKEYSAL,space(20)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MMFLCASC,'SLQTAPER',i_cF+'.MMQTAUM1',&i_cF..MMQTAUM1,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..MMFLRISE,'SLQTRPER',i_cF+'.MMQTAUM1',&i_cF..MMQTAUM1,'restore',0)
      i_cOp3=cp_SetTrsOp(&i_cF..MMFLORDI,'SLQTOPER',i_cF+'.MMQTAUM1',&i_cF..MMQTAUM1,'restore',0)
      i_cOp4=cp_SetTrsOp(&i_cF..MMFLIMPE,'SLQTIPER',i_cF+'.MMQTAUM1',&i_cF..MMQTAUM1,'restore',0)
      i_cOp5=cp_SetTrsOp(&i_cF..MMFLULPV,'SLDATUPV',i_cF+'.MMDATREG',&i_cF..MMDATREG,'restore',0)
      i_cOp6=cp_SetTrsOp(&i_cF..MMFLULPV,'SLCODVAV',i_cF+'.MMVALNAZ',&i_cF..MMVALNAZ,'restore',0)
      i_cOp7=cp_SetTrsOp(&i_cF..MMFLULCA,'SLVALUCA',i_cF+'.MMVALULT',&i_cF..MMVALULT,'restore',0)
      i_cOp8=cp_SetTrsOp(&i_cF..MMFLULCA,'SLDATUCA',i_cF+'.MMDATREG',&i_cF..MMDATREG,'restore',0)
      i_cOp9=cp_SetTrsOp(&i_cF..MMFLULCA,'SLCODVAA',i_cF+'.MMVALNAZ',&i_cF..MMVALNAZ,'restore',0)
      i_cOp10=cp_SetTrsOp(&i_cF..MMFLULPV,'SLVALUPV',i_cF+'.MMVALULT',&i_cF..MMVALULT,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SLCODICE',&i_cF..MMKEYSAL;
                 ,'SLCODMAG',&i_cF..MMCODMAG)
      UPDATE (i_cTable) SET ;
           SLQTAPER=&i_cOp1.  ,;
           SLQTRPER=&i_cOp2.  ,;
           SLQTOPER=&i_cOp3.  ,;
           SLQTIPER=&i_cOp4.  ,;
           SLDATUPV=&i_cOp5.  ,;
           SLCODVAV=&i_cOp6.  ,;
           SLVALUCA=&i_cOp7.  ,;
           SLDATUCA=&i_cOp8.  ,;
           SLCODVAA=&i_cOp9.  ,;
           SLVALUPV=&i_cOp10.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MMF2CASC,space(1))==this.w_MMF2CASC;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMF2RISE,space(1))==this.w_MMF2RISE;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMF2ORDI,space(1))==this.w_MMF2ORDI;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMF2IMPE,space(1))==this.w_MMF2IMPE;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMCODMAT,space(5))==this.w_MMCODMAT;
              and NVL(&i_cF..MMKEYSAL,space(20))==this.w_MMKEYSAL;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MMF2CASC,space(1)),'SLQTAPER','',NVL(&i_cF..MMQTAUM1,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..MMF2RISE,space(1)),'SLQTRPER','',NVL(&i_cF..MMQTAUM1,0),'restore',i_nConn)
      i_cOp3=cp_SetTrsOp(NVL(&i_cF..MMF2ORDI,space(1)),'SLQTOPER','',NVL(&i_cF..MMQTAUM1,0),'restore',i_nConn)
      i_cOp4=cp_SetTrsOp(NVL(&i_cF..MMF2IMPE,space(1)),'SLQTIPER','',NVL(&i_cF..MMQTAUM1,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MMCODMAT,space(5))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" SLQTAPER="+i_cOp1+","           +" SLQTRPER="+i_cOp2+","           +" SLQTOPER="+i_cOp3+","           +" SLQTIPER="+i_cOp4+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SLCODMAG="+cp_ToStrODBC(NVL(&i_cF..MMCODMAT,space(5)));
             +" AND SLCODICE="+cp_ToStrODBC(NVL(&i_cF..MMKEYSAL,space(20)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MMF2CASC,'SLQTAPER',i_cF+'.MMQTAUM1',&i_cF..MMQTAUM1,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..MMF2RISE,'SLQTRPER',i_cF+'.MMQTAUM1',&i_cF..MMQTAUM1,'restore',0)
      i_cOp3=cp_SetTrsOp(&i_cF..MMF2ORDI,'SLQTOPER',i_cF+'.MMQTAUM1',&i_cF..MMQTAUM1,'restore',0)
      i_cOp4=cp_SetTrsOp(&i_cF..MMF2IMPE,'SLQTIPER',i_cF+'.MMQTAUM1',&i_cF..MMQTAUM1,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SLCODICE',&i_cF..MMKEYSAL;
                 ,'SLCODMAG',&i_cF..MMCODMAT)
      UPDATE (i_cTable) SET ;
           SLQTAPER=&i_cOp1.  ,;
           SLQTRPER=&i_cOp2.  ,;
           SLQTOPER=&i_cOp3.  ,;
           SLQTIPER=&i_cOp4.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MMFLCASC,space(1))==this.w_MMFLCASC;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMFLRISE,space(1))==this.w_MMFLRISE;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMLOTMAG,space(5))==this.w_MMLOTMAG;
              and NVL(&i_cF..MMKEYSAL,space(20))==this.w_MMKEYSAL;
              and NVL(&i_cF..MMCODUBI,space(20))==this.w_MMCODUBI;
              and NVL(&i_cF..MMCODLOT,space(20))==this.w_MMCODLOT;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MMFLCASC,space(1)),'SUQTAPER','',NVL(&i_cF..MMQTAUM1,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..MMFLRISE,space(1)),'SUQTRPER','',NVL(&i_cF..MMQTAUM1,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MMLOTMAG,space(5))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" SUQTAPER="+i_cOp1+","           +" SUQTRPER="+i_cOp2+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SUCODMAG="+cp_ToStrODBC(NVL(&i_cF..MMLOTMAG,space(5)));
             +" AND SUCODART="+cp_ToStrODBC(NVL(&i_cF..MMKEYSAL,space(20)));
             +" AND SUCODUBI="+cp_ToStrODBC(NVL(&i_cF..MMCODUBI,space(20)));
             +" AND SUCODLOT="+cp_ToStrODBC(NVL(&i_cF..MMCODLOT,space(20)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MMFLCASC,'SUQTAPER',i_cF+'.MMQTAUM1',&i_cF..MMQTAUM1,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..MMFLRISE,'SUQTRPER',i_cF+'.MMQTAUM1',&i_cF..MMQTAUM1,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SUCODART',&i_cF..MMKEYSAL;
                 ,'SUCODUBI',&i_cF..MMCODUBI;
                 ,'SUCODLOT',&i_cF..MMCODLOT;
                 ,'SUCODMAG',&i_cF..MMLOTMAG)
      UPDATE (i_cTable) SET ;
           SUQTAPER=&i_cOp1.  ,;
           SUQTRPER=&i_cOp2.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MMF2CASC,space(1))==this.w_MMF2CASC;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMF2RISE,space(1))==this.w_MMF2RISE;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMLOTMAT,space(5))==this.w_MMLOTMAT;
              and NVL(&i_cF..MMKEYSAL,space(20))==this.w_MMKEYSAL;
              and NVL(&i_cF..MMCODUB2,space(20))==this.w_MMCODUB2;
              and NVL(&i_cF..MMCODLOT,space(20))==this.w_MMCODLOT;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..MMF2CASC,space(1)),'SUQTAPER','',NVL(&i_cF..MMQTAUM1,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..MMF2RISE,space(1)),'SUQTRPER','',NVL(&i_cF..MMQTAUM1,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..MMLOTMAT,space(5))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" SUQTAPER="+i_cOp1+","           +" SUQTRPER="+i_cOp2+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SUCODMAG="+cp_ToStrODBC(NVL(&i_cF..MMLOTMAT,space(5)));
             +" AND SUCODART="+cp_ToStrODBC(NVL(&i_cF..MMKEYSAL,space(20)));
             +" AND SUCODUBI="+cp_ToStrODBC(NVL(&i_cF..MMCODUB2,space(20)));
             +" AND SUCODLOT="+cp_ToStrODBC(NVL(&i_cF..MMCODLOT,space(20)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..MMF2CASC,'SUQTAPER',i_cF+'.MMQTAUM1',&i_cF..MMQTAUM1,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..MMF2RISE,'SUQTRPER',i_cF+'.MMQTAUM1',&i_cF..MMQTAUM1,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SUCODART',&i_cF..MMKEYSAL;
                 ,'SUCODUBI',&i_cF..MMCODUB2;
                 ,'SUCODLOT',&i_cF..MMCODLOT;
                 ,'SUCODMAG',&i_cF..MMLOTMAT)
      UPDATE (i_cTable) SET ;
           SUQTAPER=&i_cOp1.  ,;
           SUQTRPER=&i_cOp2.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4,i_cOp5,i_cOp6,i_cOp7,i_cOp8,i_cOp9,i_cOp10,i_cOp11,i_cOp12

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MMFLCASC,space(1))==this.w_MMFLCASC;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMFLRISE,space(1))==this.w_MMFLRISE;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMFLORDI,space(1))==this.w_MMFLORDI;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMFLIMPE,space(1))==this.w_MMFLIMPE;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMFLULPV,space(1))==this.w_MMFLULPV;
              and NVL(&i_cF..MMDATREG,ctod("  /  /  "))==this.w_MMDATREG;
              and NVL(&i_cF..MMFLULPV,space(1))==this.w_MMFLULPV;
              and NVL(&i_cF..MMVALNAZ,space(3))==this.w_MMVALNAZ;
              and NVL(&i_cF..MMFLULCA,space(1))==this.w_MMFLULCA;
              and NVL(&i_cF..MMVALULT,0)==this.w_MMVALULT;
              and NVL(&i_cF..MMFLULCA,space(1))==this.w_MMFLULCA;
              and NVL(&i_cF..MMDATREG,ctod("  /  /  "))==this.w_MMDATREG;
              and NVL(&i_cF..MMFLULCA,space(1))==this.w_MMFLULCA;
              and NVL(&i_cF..MMVALNAZ,space(3))==this.w_MMVALNAZ;
              and NVL(&i_cF..MMFLULPV,space(1))==this.w_MMFLULPV;
              and NVL(&i_cF..MMVALULT,0)==this.w_MMVALULT;
              and NVL(&i_cF..MMCODMAG,space(5))==this.w_MMCODMAG;
              and NVL(&i_cF..MMKEYSAL,space(20))==this.w_MMKEYSAL;

      i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_MMFLULPV,'SLDATUPV','this.w_MMDATREG',this.w_MMDATREG,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_MMFLULPV,'SLCODVAV','this.w_MMVALNAZ',this.w_MMVALNAZ,'update',i_nConn)
      i_cOp7=cp_SetTrsOp(this.w_MMFLULCA,'SLVALUCA','this.w_MMVALULT',this.w_MMVALULT,'update',i_nConn)
      i_cOp8=cp_SetTrsOp(this.w_MMFLULCA,'SLDATUCA','this.w_MMDATREG',this.w_MMDATREG,'update',i_nConn)
      i_cOp9=cp_SetTrsOp(this.w_MMFLULCA,'SLCODVAA','this.w_MMVALNAZ',this.w_MMVALNAZ,'update',i_nConn)
      i_cOp10=cp_SetTrsOp(this.w_MMFLULPV,'SLVALUPV','this.w_MMVALULT',this.w_MMVALULT,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MMCODMAG)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" SLQTAPER="+i_cOp1  +",";
         +" SLQTRPER="+i_cOp2  +",";
         +" SLQTOPER="+i_cOp3  +",";
         +" SLQTIPER="+i_cOp4  +",";
         +" SLDATUPV="+i_cOp5  +",";
         +" SLCODVAV="+i_cOp6  +",";
         +" SLVALUCA="+i_cOp7  +",";
         +" SLDATUCA="+i_cOp8  +",";
         +" SLCODVAA="+i_cOp9  +",";
         +" SLVALUPV="+i_cOp10  +",";
         +" UTCV="+cp_ToStrODBC(i_codute)  +",";
         +" UTDV="+cp_ToStrODBC(SetInfoDate(This.cCalUtd))  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SLCODMAG="+cp_ToStrODBC(this.w_MMCODMAG);
           +" AND SLCODICE="+cp_ToStrODBC(this.w_MMKEYSAL);
           )
        if i_nModRow<1 .and. .not. empty(this.w_MMCODMAG)
        i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',i_nConn)
        i_cOp5=cp_SetTrsOp(this.w_MMFLULPV,'SLDATUPV','this.w_MMDATREG',this.w_MMDATREG,'insert',i_nConn)
        i_cOp6=cp_SetTrsOp(this.w_MMFLULPV,'SLCODVAV','this.w_MMVALNAZ',this.w_MMVALNAZ,'insert',i_nConn)
        i_cOp7=cp_SetTrsOp(this.w_MMFLULCA,'SLVALUCA','this.w_MMVALULT',this.w_MMVALULT,'insert',i_nConn)
        i_cOp8=cp_SetTrsOp(this.w_MMFLULCA,'SLDATUCA','this.w_MMDATREG',this.w_MMDATREG,'insert',i_nConn)
        i_cOp9=cp_SetTrsOp(this.w_MMFLULCA,'SLCODVAA','this.w_MMVALNAZ',this.w_MMVALNAZ,'insert',i_nConn)
        i_cOp10=cp_SetTrsOp(this.w_MMFLULPV,'SLVALUPV','this.w_MMVALULT',this.w_MMVALULT,'insert',i_nConn)
          =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" (SLCODMAG,SLCODICE  ;
             ,SLQTAPER"+",SLQTRPER"+",SLQTOPER"+",SLQTIPER"+",SLDATUPV"+",SLCODVAV"+",SLVALUCA"+",SLDATUCA"+",SLCODVAA"+",SLVALUPV"+",UTCV"+",UTDV,CPCCCHK) VALUES ("+cp_ToStrODBC(this.w_MMCODMAG)+","+cp_ToStrODBC(this.w_MMKEYSAL)  ;
             +","+i_cOp1;
             +","+i_cOp2;
             +","+i_cOp3;
             +","+i_cOp4;
             +","+i_cOp5;
             +","+i_cOp6;
             +","+i_cOp7;
             +","+i_cOp8;
             +","+i_cOp9;
             +","+i_cOp10;
             +","+cp_ToStrODBC(i_codute);
             +","+cp_ToStrODBC(SetInfoDate(This.cCalUtd));
             +","+cp_ToStrODBC(cp_NewCCChk())+")")
        endif
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',0)
      i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',0)
      i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',0)
      i_cOp5=cp_SetTrsOp(this.w_MMFLULPV,'SLDATUPV','this.w_MMDATREG',this.w_MMDATREG,'update',0)
      i_cOp6=cp_SetTrsOp(this.w_MMFLULPV,'SLCODVAV','this.w_MMVALNAZ',this.w_MMVALNAZ,'update',0)
      i_cOp7=cp_SetTrsOp(this.w_MMFLULCA,'SLVALUCA','this.w_MMVALULT',this.w_MMVALULT,'update',0)
      i_cOp8=cp_SetTrsOp(this.w_MMFLULCA,'SLDATUCA','this.w_MMDATREG',this.w_MMDATREG,'update',0)
      i_cOp9=cp_SetTrsOp(this.w_MMFLULCA,'SLCODVAA','this.w_MMVALNAZ',this.w_MMVALNAZ,'update',0)
      i_cOp10=cp_SetTrsOp(this.w_MMFLULPV,'SLVALUPV','this.w_MMVALULT',this.w_MMVALULT,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SLCODICE',this.w_MMKEYSAL;
                 ,'SLCODMAG',this.w_MMCODMAG)
      UPDATE (i_cTable) SET;
           SLQTAPER=&i_cOp1.  ,;
           SLQTRPER=&i_cOp2.  ,;
           SLQTOPER=&i_cOp3.  ,;
           SLQTIPER=&i_cOp4.  ,;
           SLDATUPV=&i_cOp5.  ,;
           SLCODVAV=&i_cOp6.  ,;
           SLVALUCA=&i_cOp7.  ,;
           SLDATUCA=&i_cOp8.  ,;
           SLCODVAA=&i_cOp9.  ,;
           SLVALUPV=&i_cOp10.  ,;
           UTCV=i_codute  ,;
           UTDV=SetInfoDate(This.cCalUtd)  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
      i_nModRow = _tally
      if i_nModRow<1 .and. .not. empty(this.w_MMCODMAG)
        i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',0)
        i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',0)
        i_cOp3=cp_SetTrsOp(this.w_MMFLORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',0)
        i_cOp4=cp_SetTrsOp(this.w_MMFLIMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',0)
        i_cOp5=cp_SetTrsOp(this.w_MMFLULPV,'SLDATUPV','this.w_MMDATREG',this.w_MMDATREG,'insert',0)
        i_cOp6=cp_SetTrsOp(this.w_MMFLULPV,'SLCODVAV','this.w_MMVALNAZ',this.w_MMVALNAZ,'insert',0)
        i_cOp7=cp_SetTrsOp(this.w_MMFLULCA,'SLVALUCA','this.w_MMVALULT',this.w_MMVALULT,'insert',0)
        i_cOp8=cp_SetTrsOp(this.w_MMFLULCA,'SLDATUCA','this.w_MMDATREG',this.w_MMDATREG,'insert',0)
        i_cOp9=cp_SetTrsOp(this.w_MMFLULCA,'SLCODVAA','this.w_MMVALNAZ',this.w_MMVALNAZ,'insert',0)
        i_cOp10=cp_SetTrsOp(this.w_MMFLULPV,'SLVALUPV','this.w_MMVALULT',this.w_MMVALULT,'insert',0)
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MMKEYSAL,'SLCODMAG',this.w_MMCODMAG)
        INSERT INTO (i_cTable) (SLCODMAG,SLCODICE  ;
         ,SLQTAPER,SLQTRPER,SLQTOPER,SLQTIPER,SLDATUPV,SLCODVAV,SLVALUCA,SLDATUCA,SLCODVAA,SLVALUPV,UTCV,UTDV,CPCCCHK) VALUES (this.w_MMCODMAG,this.w_MMKEYSAL  ;
           ,&i_cOp1.  ;
           ,&i_cOp2.  ;
           ,&i_cOp3.  ;
           ,&i_cOp4.  ;
           ,&i_cOp5.  ;
           ,&i_cOp6.  ;
           ,&i_cOp7.  ;
           ,&i_cOp8.  ;
           ,&i_cOp9.  ;
           ,&i_cOp10.  ;
           ,i_codute  ;
           ,SetInfoDate(This.cCalUtd)  ,cp_NewCCChk())
      endif
    endif
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MMF2CASC,space(1))==this.w_MMF2CASC;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMF2RISE,space(1))==this.w_MMF2RISE;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMF2ORDI,space(1))==this.w_MMF2ORDI;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMF2IMPE,space(1))==this.w_MMF2IMPE;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMCODMAT,space(5))==this.w_MMCODMAT;
              and NVL(&i_cF..MMKEYSAL,space(20))==this.w_MMKEYSAL;

      i_cOp1=cp_SetTrsOp(this.w_MMF2CASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MMF2RISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_MMF2ORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_MMF2IMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MMCODMAT)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" SLQTAPER="+i_cOp1  +",";
         +" SLQTRPER="+i_cOp2  +",";
         +" SLQTOPER="+i_cOp3  +",";
         +" SLQTIPER="+i_cOp4  +",";
         +" UTCV="+cp_ToStrODBC(i_codute)  +",";
         +" UTDV="+cp_ToStrODBC(SetInfoDate(This.cCalUtd))  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SLCODMAG="+cp_ToStrODBC(this.w_MMCODMAT);
           +" AND SLCODICE="+cp_ToStrODBC(this.w_MMKEYSAL);
           )
        if i_nModRow<1 .and. .not. empty(this.w_MMCODMAT)
        i_cOp1=cp_SetTrsOp(this.w_MMF2CASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MMF2RISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MMF2ORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_MMF2IMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',i_nConn)
          =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" (SLCODMAG,SLCODICE  ;
             ,SLQTAPER"+",SLQTRPER"+",SLQTOPER"+",SLQTIPER"+",UTCV"+",UTDV,CPCCCHK) VALUES ("+cp_ToStrODBC(this.w_MMCODMAT)+","+cp_ToStrODBC(this.w_MMKEYSAL)  ;
             +","+i_cOp1;
             +","+i_cOp2;
             +","+i_cOp3;
             +","+i_cOp4;
             +","+cp_ToStrODBC(i_codute);
             +","+cp_ToStrODBC(SetInfoDate(This.cCalUtd));
             +","+cp_ToStrODBC(cp_NewCCChk())+")")
        endif
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MMF2CASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_MMF2RISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',0)
      i_cOp3=cp_SetTrsOp(this.w_MMF2ORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',0)
      i_cOp4=cp_SetTrsOp(this.w_MMF2IMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SLCODICE',this.w_MMKEYSAL;
                 ,'SLCODMAG',this.w_MMCODMAT)
      UPDATE (i_cTable) SET;
           SLQTAPER=&i_cOp1.  ,;
           SLQTRPER=&i_cOp2.  ,;
           SLQTOPER=&i_cOp3.  ,;
           SLQTIPER=&i_cOp4.  ,;
           UTCV=i_codute  ,;
           UTDV=SetInfoDate(This.cCalUtd)  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
      i_nModRow = _tally
      if i_nModRow<1 .and. .not. empty(this.w_MMCODMAT)
        i_cOp1=cp_SetTrsOp(this.w_MMF2CASC,'SLQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',0)
        i_cOp2=cp_SetTrsOp(this.w_MMF2RISE,'SLQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',0)
        i_cOp3=cp_SetTrsOp(this.w_MMF2ORDI,'SLQTOPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',0)
        i_cOp4=cp_SetTrsOp(this.w_MMF2IMPE,'SLQTIPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',0)
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MMKEYSAL,'SLCODMAG',this.w_MMCODMAT)
        INSERT INTO (i_cTable) (SLCODMAG,SLCODICE  ;
         ,SLQTAPER,SLQTRPER,SLQTOPER,SLQTIPER,UTCV,UTDV,CPCCCHK) VALUES (this.w_MMCODMAT,this.w_MMKEYSAL  ;
           ,&i_cOp1.  ;
           ,&i_cOp2.  ;
           ,&i_cOp3.  ;
           ,&i_cOp4.  ;
           ,i_codute  ;
           ,SetInfoDate(This.cCalUtd)  ,cp_NewCCChk())
      endif
    endif
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MMFLCASC,space(1))==this.w_MMFLCASC;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMFLRISE,space(1))==this.w_MMFLRISE;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMLOTMAG,space(5))==this.w_MMLOTMAG;
              and NVL(&i_cF..MMKEYSAL,space(20))==this.w_MMKEYSAL;
              and NVL(&i_cF..MMCODUBI,space(20))==this.w_MMCODUBI;
              and NVL(&i_cF..MMCODLOT,space(20))==this.w_MMCODLOT;

      i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SUQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SUQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MMLOTMAG)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" SUQTAPER="+i_cOp1  +",";
         +" SUQTRPER="+i_cOp2  +",";
         +" UTCV="+cp_ToStrODBC(i_codute)  +",";
         +" UTDV="+cp_ToStrODBC(SetInfoDate(This.cCalUtd))  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SUCODMAG="+cp_ToStrODBC(this.w_MMLOTMAG);
           +" AND SUCODART="+cp_ToStrODBC(this.w_MMKEYSAL);
           +" AND SUCODUBI="+cp_ToStrODBC(this.w_MMCODUBI);
           +" AND SUCODLOT="+cp_ToStrODBC(this.w_MMCODLOT);
           )
        if i_nModRow<1 .and. .not. empty(this.w_MMLOTMAG)
        i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SUQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SUQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',i_nConn)
          =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" (SUCODMAG,SUCODART,SUCODUBI,SUCODLOT  ;
             ,SUQTAPER"+",SUQTRPER"+",UTCV"+",UTDV,CPCCCHK) VALUES ("+cp_ToStrODBC(this.w_MMLOTMAG)+","+cp_ToStrODBC(this.w_MMKEYSAL)+","+cp_ToStrODBC(this.w_MMCODUBI)+","+cp_ToStrODBC(this.w_MMCODLOT)  ;
             +","+i_cOp1;
             +","+i_cOp2;
             +","+cp_ToStrODBC(i_codute);
             +","+cp_ToStrODBC(SetInfoDate(This.cCalUtd));
             +","+cp_ToStrODBC(cp_NewCCChk())+")")
        endif
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SUQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SUQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SUCODART',this.w_MMKEYSAL;
                 ,'SUCODUBI',this.w_MMCODUBI;
                 ,'SUCODLOT',this.w_MMCODLOT;
                 ,'SUCODMAG',this.w_MMLOTMAG)
      UPDATE (i_cTable) SET;
           SUQTAPER=&i_cOp1.  ,;
           SUQTRPER=&i_cOp2.  ,;
           UTCV=i_codute  ,;
           UTDV=SetInfoDate(This.cCalUtd)  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
      i_nModRow = _tally
      if i_nModRow<1 .and. .not. empty(this.w_MMLOTMAG)
        i_cOp1=cp_SetTrsOp(this.w_MMFLCASC,'SUQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',0)
        i_cOp2=cp_SetTrsOp(this.w_MMFLRISE,'SUQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',0)
      cp_CheckDeletedKey(i_cTable,0,'SUCODART',this.w_MMKEYSAL,'SUCODUBI',this.w_MMCODUBI,'SUCODLOT',this.w_MMCODLOT,'SUCODMAG',this.w_MMLOTMAG)
        INSERT INTO (i_cTable) (SUCODMAG,SUCODART,SUCODUBI,SUCODLOT  ;
         ,SUQTAPER,SUQTRPER,UTCV,UTDV,CPCCCHK) VALUES (this.w_MMLOTMAG,this.w_MMKEYSAL,this.w_MMCODUBI,this.w_MMCODLOT  ;
           ,&i_cOp1.  ;
           ,&i_cOp2.  ;
           ,i_codute  ;
           ,SetInfoDate(This.cCalUtd)  ,cp_NewCCChk())
      endif
    endif
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..MMF2CASC,space(1))==this.w_MMF2CASC;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMF2RISE,space(1))==this.w_MMF2RISE;
              and NVL(&i_cF..MMQTAUM1,0)==this.w_MMQTAUM1;
              and NVL(&i_cF..MMLOTMAT,space(5))==this.w_MMLOTMAT;
              and NVL(&i_cF..MMKEYSAL,space(20))==this.w_MMKEYSAL;
              and NVL(&i_cF..MMCODUB2,space(20))==this.w_MMCODUB2;
              and NVL(&i_cF..MMCODLOT,space(20))==this.w_MMCODLOT;

      i_cOp1=cp_SetTrsOp(this.w_MMF2CASC,'SUQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_MMF2RISE,'SUQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_MMLOTMAT)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" SUQTAPER="+i_cOp1  +",";
         +" SUQTRPER="+i_cOp2  +",";
         +" UTCV="+cp_ToStrODBC(i_codute)  +",";
         +" UTDV="+cp_ToStrODBC(SetInfoDate(This.cCalUtd))  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SUCODMAG="+cp_ToStrODBC(this.w_MMLOTMAT);
           +" AND SUCODART="+cp_ToStrODBC(this.w_MMKEYSAL);
           +" AND SUCODUBI="+cp_ToStrODBC(this.w_MMCODUB2);
           +" AND SUCODLOT="+cp_ToStrODBC(this.w_MMCODLOT);
           )
        if i_nModRow<1 .and. .not. empty(this.w_MMLOTMAT)
        i_cOp1=cp_SetTrsOp(this.w_MMF2CASC,'SUQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MMF2RISE,'SUQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',i_nConn)
          =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" (SUCODMAG,SUCODART,SUCODUBI,SUCODLOT  ;
             ,SUQTAPER"+",SUQTRPER"+",UTCV"+",UTDV,CPCCCHK) VALUES ("+cp_ToStrODBC(this.w_MMLOTMAT)+","+cp_ToStrODBC(this.w_MMKEYSAL)+","+cp_ToStrODBC(this.w_MMCODUB2)+","+cp_ToStrODBC(this.w_MMCODLOT)  ;
             +","+i_cOp1;
             +","+i_cOp2;
             +","+cp_ToStrODBC(i_codute);
             +","+cp_ToStrODBC(SetInfoDate(This.cCalUtd));
             +","+cp_ToStrODBC(cp_NewCCChk())+")")
        endif
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_MMF2CASC,'SUQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_MMF2RISE,'SUQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SUCODART',this.w_MMKEYSAL;
                 ,'SUCODUBI',this.w_MMCODUB2;
                 ,'SUCODLOT',this.w_MMCODLOT;
                 ,'SUCODMAG',this.w_MMLOTMAT)
      UPDATE (i_cTable) SET;
           SUQTAPER=&i_cOp1.  ,;
           SUQTRPER=&i_cOp2.  ,;
           UTCV=i_codute  ,;
           UTDV=SetInfoDate(This.cCalUtd)  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
      i_nModRow = _tally
      if i_nModRow<1 .and. .not. empty(this.w_MMLOTMAT)
        i_cOp1=cp_SetTrsOp(this.w_MMF2CASC,'SUQTAPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',0)
        i_cOp2=cp_SetTrsOp(this.w_MMF2RISE,'SUQTRPER','this.w_MMQTAUM1',this.w_MMQTAUM1,'insert',0)
      cp_CheckDeletedKey(i_cTable,0,'SUCODART',this.w_MMKEYSAL,'SUCODUBI',this.w_MMCODUB2,'SUCODLOT',this.w_MMCODLOT,'SUCODMAG',this.w_MMLOTMAT)
        INSERT INTO (i_cTable) (SUCODMAG,SUCODART,SUCODUBI,SUCODLOT  ;
         ,SUQTAPER,SUQTRPER,UTCV,UTDV,CPCCCHK) VALUES (this.w_MMLOTMAT,this.w_MMKEYSAL,this.w_MMCODUB2,this.w_MMCODLOT  ;
           ,&i_cOp1.  ;
           ,&i_cOp2.  ;
           ,i_codute  ;
           ,SetInfoDate(This.cCalUtd)  ,cp_NewCCChk())
      endif
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 AND NOT EMPTY(t_MMCODICE)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSVE_MMT : Deleting
        this.GSVE_MMT.bSaveContext=.f.
        this.GSVE_MMT.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_MMSERIAL,"MTSERIAL";
               ,this.w_CPROWNUM,"MTROWNUM";
               ,this.w_MMNUMRIF,"MTNUMRIF";
               )
        this.GSVE_MMT.bSaveContext=.t.
        this.GSVE_MMT.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.MVM_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MVM_DETT_IDX,2])
        *
        * delete MVM_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.MVM_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])
        *
        * delete MVM_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 AND NOT EMPTY(t_MMCODICE)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsma_mvm
    if not(bTrsErr)
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
        * ---Memorizza l'ultima operazione eseguita
        APPMDV=this.cFunction
    endif
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MVM_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .w_AZICOD = i_CODAZI
        .DoRTCalc(3,6,.t.)
        if .o_MMDATREG<>.w_MMDATREG
          .w_MMCODESE = CALCESER(.w_MMDATREG, g_CODESE)
          .link_1_7('Full')
        endif
        if .o_MMDATREG<>.w_MMDATREG
          .w_MMVALNAZ = IIF(EMPTY(.w_MMVALNAZ), g_PERVAL, .w_MMVALNAZ)
          .link_1_8('Full')
        endif
        .DoRTCalc(9,12,.t.)
          .w_MMTIPCON = IIF(.w_MMFLCLFR $ "CF", .w_MMFLCLFR, " ")
        .DoRTCalc(14,17,.t.)
        if .o_MMDATREG<>.w_MMDATREG.or. .o_MMDATDOC<>.w_MMDATDOC
          .w_OBTEST = IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC)
        endif
        .DoRTCalc(19,19,.t.)
        if .o_MMTCAMAG<>.w_MMTCAMAG.or. .o_MMTIPCON<>.w_MMTIPCON
          .w_MMCODCON = SPACE(15)
          .link_1_20('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(AH_Msgformat(IIF(.w_MMTIPCON="C", "Cliente:",IIF(.w_MMTIPCON="F","Fornitore:",""))),'','')
        .DoRTCalc(21,24,.t.)
        if .o_MMSCOCL1<>.w_MMSCOCL1
          .w_MMSCOCL2 = IIF(.w_MMSCOCL1=0, 0, .w_MMSCOCL2)
        endif
        .DoRTCalc(26,28,.t.)
        if .o_MMTIPCON<>.w_MMTIPCON.or. .o_MMCODCON<>.w_MMCODCON
          .w_MMCODVAL = IIF(EMPTY(.w_VALCLF), g_PERVAL, .w_VALCLF)
          .link_1_30('Full')
        endif
        .DoRTCalc(30,33,.t.)
        if .o_MMCODVAL<>.w_MMCODVAL
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_MMCODVAL<>.w_MMCODVAL
          .w_MMCAOVAL = GETCAM(.w_MMCODVAL, IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC), 7)
        endif
        if .o_MMTIPCON<>.w_MMTIPCON.or. .o_MMCODCON<>.w_MMCODCON
          .link_1_37('Full')
        endif
        .DoRTCalc(37,44,.t.)
        if .o_MMDATDOC<>.w_MMDATDOC.or. .o_MMDATREG<>.w_MMDATREG
          .w_DATA1 = IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC)
        endif
        .DoRTCalc(46,49,.t.)
          .w_OMAG = IIF(EMPTY(.w_MMCODMAG), g_MAGAZI, .w_MMCODMAG)
          .w_OMAT = .w_MMCODMAT
          .w_OCOM = .w_MMCODCOM
          .w_OATT = .w_MMCODATT
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        if .o_MMCODVAL<>.w_MMCODVAL
          .w_CALCPICU = DEFPIU(.w_DECUNI)
        endif
        .DoRTCalc(55,55,.t.)
        if .o_MMCODICE<>.w_MMCODICE
          .link_2_2('Full')
        endif
        .DoRTCalc(57,57,.t.)
        if .o_MMCODICE<>.w_MMCODICE
          .w_MMCODMAG = IIF(NOT EMPTY(IIF(.w_FLLOTT<>'C',SPACE(5),.w_MMCODMAG)),IIF(.w_FLLOTT<>'C',SPACE(5),.w_MMCODMAG),IIF(NOT EMPTY(.w_MAGPRE) , .w_MAGPRE, .w_OMAG))
          .link_2_4('Full')
        endif
        .DoRTCalc(59,59,.t.)
        if .o_MMCODICE<>.w_MMCODICE
          .w_MMCODMAT = .w_OMAT
          .link_2_6('Full')
        endif
        .DoRTCalc(61,66,.t.)
        if .o_MMCODICE<>.w_MMCODICE
          .w_TIPRIG = IIF(.w_TIPART $ 'AC-FO-FM-DE', 'X', 'R')
        endif
        .DoRTCalc(68,68,.t.)
          .link_2_15('Full')
        .DoRTCalc(70,73,.t.)
        if .o_MMCODICE<>.w_MMCODICE
          .link_2_20('Full')
        endif
        .DoRTCalc(75,76,.t.)
        if .o_MMCODICE<>.w_MMCODICE.or. .o_MMTCOLIS<>.w_MMTCOLIS
          .w_MMCODLIS = .w_MMTCOLIS
        endif
        .DoRTCalc(78,79,.t.)
          .w_FLDISP = IIF(g_PERDIS='S', .w_ARTDIS, ' ')
        if .o_MMCODICE<>.w_MMCODICE.or. .o_MMCODMAG<>.w_MMCODMAG.or. .o_MMTCAMAG<>.w_MMTCAMAG
          .w_MMCAUMAG = .w_MMTCAMAG
          .link_2_27('Full')
        endif
        if .o_MMCODICE<>.w_MMCODICE.or. .o_MMCODMAG<>.w_MMCODMAG.or. .o_MMTCAMAG<>.w_MMTCAMAG
          .link_2_28('Full')
        endif
        .DoRTCalc(83,91,.t.)
        if .o_MMCODICE<>.w_MMCODICE
          .link_2_38('Full')
        endif
        .DoRTCalc(93,93,.t.)
          .w_MMKEYSAL = .w_MMCODART
          .w_MMFLLOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_FLUBIC='S'), LEFT(ALLTRIM(.w_MMFLCASC)+IIF(.w_MMFLRISE='+', '-', IIF(.w_MMFLRISE='-', '+', ' ')), 1), ' ')
          .w_MMF2LOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_F2UBIC='S'), LEFT(ALLTRIM(.w_MMF2CASC)+IIF(.w_MMF2RISE='+', '-', IIF(.w_MMF2RISE='-', '+', ' ')), 1), ' ')
        .DoRTCalc(97,99,.t.)
        if .o_MMCODICE<>.w_MMCODICE
          .w_MMUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
          .link_2_46('Full')
        endif
        .DoRTCalc(101,102,.t.)
        if .o_MMSERIAL<>.w_MMSERIAL.or. .o_CPROWORD<>.w_CPROWORD
          .w_OQTA = .w_MMQTAMOV
        endif
        .DoRTCalc(104,104,.t.)
          .w_CAMBIOLOT = .F.
        if .o_MMCODLOT<>.w_MMCODLOT
          .Calculate_XZDFPDUIPY()
        endif
        if .o_CAMBIOLOT<>.w_CAMBIOLOT.or. .o_MMCODLOT<>.w_MMCODLOT.or. .o_MMQTAMOV<>.w_MMQTAMOV.or. .o_MMQTAUM1<>.w_MMQTAUM1
          .w_STATEROW = IIF(this.rowstatus()$'AU' AND (.w_CAMBIOLOT or .w_MMQTAUM1>.w_OLDQTA) ,'S','N')
        endif
        if .o_MMCODICE<>.w_MMCODICE.or. .o_MMCODMAG<>.w_MMCODMAG
          .link_2_55('Full')
        endif
        if .o_MMCODICE<>.w_MMCODICE.or. .o_MMCODMAT<>.w_MMCODMAT
          .w_MMCODUB2 = SPACE(20)
          .link_2_56('Full')
        endif
        if .o_MMQTAMOV<>.w_MMQTAMOV.or. .o_MMUNIMIS<>.w_MMUNIMIS
          .w_MMQTAUM1 = CALQTA(.w_MMQTAMOV,.w_MMUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_NOFRAZ1, .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,'I')
        endif
        if .o_MMCODLIS<>.w_MMCODLIS.or. .o_MMUNIMIS<>.w_MMUNIMIS.or. .o_LIPREZZO<>.w_LIPREZZO
          .w_MMPREZZO = cp_Round(CALMMLIS(.w_LIPREZZO, .w_UNMIS1+.w_UNMIS2+.w_UNMIS3+.w_MMUNIMIS+.w_OPERAT+.w_OPERA3+.w_IVALIS+'P'+ALLTRIM(STR(.w_DECUNI)), IIF(EMPTY(.w_CLUNIMIS), .w_MOLTIP,1), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTI3,1) ,.w_PERIVA), .w_DECUNI)
        endif
        if .o_MMQTAMOV<>.w_MMQTAMOV.or. .o_MMQTAUM1<>.w_MMQTAUM1.or. .o_MMPREZZO<>.w_MMPREZZO
          .w_PREUM1 = IIF(.w_MMQTAUM1=0, 0, cp_ROUND((.w_MMPREZZO * .w_MMQTAMOV) / .w_MMQTAUM1, .w_DECUNI))
        endif
        .DoRTCalc(112,112,.t.)
        if .o_MMSCONT1<>.w_MMSCONT1
          .w_MMSCONT2 = 0
        endif
        if .o_MMSCONT2<>.w_MMSCONT2
          .w_MMSCONT3 = 0
        endif
        if .o_MMSCONT3<>.w_MMSCONT3
          .w_MMSCONT4 = 0
        endif
        if .o_MMPREZZO<>.w_MMPREZZO.or. .o_MMSCONT1<>.w_MMSCONT1.or. .o_MMSCONT2<>.w_MMSCONT2.or. .o_MMSCONT3<>.w_MMSCONT3.or. .o_MMSCONT4<>.w_MMSCONT4.or. .o_RICTOT<>.w_RICTOT
          .w_VALUNI = cp_ROUND(.w_MMPREZZO * (1+.w_MMSCONT1/100)*(1+.w_MMSCONT2/100)*(1+.w_MMSCONT3/100)*(1+.w_MMSCONT4/100),5)
        endif
        if .o_VALUNI<>.w_VALUNI.or. .o_MMSCOCL1<>.w_MMSCOCL1.or. .o_MMSCOCL2<>.w_MMSCOCL2.or. .o_MMSCOPAG<>.w_MMSCOPAG
          .w_VALUNI2 = cp_ROUND (.w_VALUNI * (1+.w_MMSCOCL1/100)*(1+.w_MMSCOCL2/100)*(1+.w_MMSCOPAG/100),5)
        endif
        .DoRTCalc(118,118,.t.)
          .w_TOTALE = .w_TOTALE-.w_mmvalmag
          .w_MMVALMAG = cp_ROUND(.w_MMQTAMOV*.w_VALUNI2, .w_DECTOT)
          .w_TOTALE = .w_TOTALE+.w_mmvalmag
        if .o_MMCODVAL<>.w_MMCODVAL.or. .o_MMCAOVAL<>.w_MMCAOVAL.or. .o_MMVALMAG<>.w_MMVALMAG
          .w_MMIMPNAZ = CALCNAZ(.w_MMVALMAG,.w_MMCAOVAL,.w_CAONAZ,IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC),.w_MMVALNAZ,.w_MMCODVAL,IIF(.w_FLAVA1='A',(.w_PERIVA*.w_INDIVA),0))
        endif
        .DoRTCalc(121,131,.t.)
          .w_MMFLULCA = IIF(.w_FLAVA1='A' AND .w_MMFLCASC $ '+-' AND .w_MMDATREG>=.w_ULTCAR AND .w_MMFLOMAG<>'S' And .w_MMPREZZO<>0, '=', ' ')
          .w_MMFLULPV = IIF(.w_FLAVA1='V' AND .w_MMFLCASC $ '+-' AND .w_MMDATREG>=.w_ULTSCA AND .w_MMFLOMAG<>'S' And .w_MMPREZZO<>0, '=', ' ')
          .w_MMVALULT = IIF(.w_MMQTAUM1=0, 0, cp_ROUND(.w_MMIMPNAZ/.w_MMQTAUM1, g_PERPUL))
        if .o_MMCODICE<>.w_MMCODICE.or. .o_MMCODMAG<>.w_MMCODMAG
          .link_2_84('Full')
        endif
        if .o_MMCODICE<>.w_MMCODICE.or. .o_MMCODMAT<>.w_MMCODMAT
          .link_2_85('Full')
        endif
        .DoRTCalc(137,138,.t.)
          .w_QTDISC = .w_QTAPER-.w_QTRPER + .w_QTOPER - .w_QTIPER
        .DoRTCalc(140,141,.t.)
          .w_QTDISP = .w_QTAPER-.w_QTRPER
          .w_Q2DISP = .w_Q2APER-.w_Q2RPER
        if .o_MMCODICE<>.w_MMCODICE
          .w_FM1 = IIF(.cFunction='Load', 'S', ' ')
        endif
        if .o_MMCODICE<>.w_MMCODICE
          .w_FM2 = IIF(.cFunction='Load', 'S',' ')
        endif
        .DoRTCalc(146,148,.t.)
          .w_VISNAZ = cp_ROUND(.w_MMIMPNAZ, .w_DECTOP)
        if .o_MMSERIAL<>.w_MMSERIAL.or. .o_MMCODESE<>.w_MMCODESE
          .w_CALCPICP = DEFPIP(.w_DECTOP)
        endif
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_90.Calculate()
        .DoRTCalc(151,155,.t.)
          .w_FLPRG = 'M'
        if .o_MMCODCON<>.w_MMCODCON.or. .o_MMCODMAT<>.w_MMCODMAT
          .w_CODCON = IIF(.w_MMTIPCON='F' And Empty(.w_MMCODMAT), .w_MMCODCON, SPACE(15))
        endif
        if .o_MMSERIAL<>.w_MMSERIAL
          .w_OLDQTA = .w_MMQTAUM1
        endif
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_100.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_111.Calculate()
        .DoRTCalc(159,178,.t.)
        if .o_MMQTAMOV<>.w_MMQTAMOV.or. .o_MMUNIMIS<>.w_MMUNIMIS
          .w_QTAUM2 = Caunmis2('R', .w_MMQTAMOV, .w_UNMIS1, IIF(Not Empty(.w_UNMIS3), .w_UNMIS3, .w_UNMIS2), .w_MMUNIMIS, IIF(Not Empty(.w_UNMIS3), .w_OPERA3, .w_OPERAT), IIF(Not Empty(.w_UNMIS3), .w_MOLTI3, .w_MOLTIP))
        endif
        .oPgFrm.Page1.oPag.oObj_2_114.Calculate(IIF(.w_MMUNIMIS=.w_UNMIS1 OR EMPTY(.w_MMCODICE),IIF(Not Empty(.w_UNMIS3), Ah_MsgFormat("Qta nella 3^UM:"),IIF(Not Empty(.w_UNMIS2), Ah_MsgFormat("Qta nella 2^UM:"),'')),IIF(.w_MMUNIMIS<>.w_UNMIS1 And .w_MMUNIMIS<>.w_UNMIS2 AND .w_MMUNIMIS<>.w_UNMIS3,"", Ah_MsgFormat("Qta e prezzo nella 1^UM:"))))
          .w_UNIMIS2 = IIF(Not Empty(.w_UNMIS3), .w_UNMIS3, .w_UNMIS2)
        .DoRTCalc(181,185,.t.)
        if .o_MMCODMAG<>.w_MMCODMAG.or. .o_MMKEYSAL<>.w_MMKEYSAL.or. .o_MMCODLOT<>.w_MMCODLOT.or. .o_MMCODUBI<>.w_MMCODUBI
          .w_MMLOTMAG = iif( Empty( .w_MMCODLOT ) And Empty( .w_MMCODUBI ) , SPACE(5) , .w_MMCODMAG )
          .link_2_121('Full')
        endif
        if .o_MMCODLOT<>.w_MMCODLOT.or. .o_MMCODMAT<>.w_MMCODMAT.or. .o_MMCODUB2<>.w_MMCODUB2.or. .o_MMKEYSAL<>.w_MMKEYSAL.or. .o_MMLOTMAT<>.w_MMLOTMAT
          .w_MMLOTMAT = iif( Empty( .w_MMCODLOT ) And Empty( .w_MMCODUB2 ) , SPACE(5) , .w_MMCODMAT )
          .link_2_122('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_118.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_127.Calculate(Ah_MsgFormat("Commessa:"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEMVM","i_codazi,w_MMSERIAL")
          .op_MMSERIAL = .w_MMSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_MMCODESE<>.w_MMCODESE .or. .op_MMCODUTE<>.w_MMCODUTE
           cp_AskTableProg(this,i_nConn,"PRMVM","i_codazi,w_MMCODESE,w_MMCODUTE,w_MMNUMREG")
          .op_MMNUMREG = .w_MMNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_MMCODESE = .w_MMCODESE
        .op_MMCODUTE = .w_MMCODUTE
      endwith
      this.DoRTCalc(188,193,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MMCODART with this.w_MMCODART
      replace t_MAGPRE with this.w_MAGPRE
      replace t_FLUBIC with this.w_FLUBIC
      replace t_F2UBIC with this.w_F2UBIC
      replace t_MMNUMRIF with this.w_MMNUMRIF
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_OPERA3 with this.w_OPERA3
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_TIPRIG with this.w_TIPRIG
      replace t_FLUSEP with this.w_FLUSEP
      replace t_GESMAT with this.w_GESMAT
      replace t_MOLTIP with this.w_MOLTIP
      replace t_OPERAT with this.w_OPERAT
      replace t_FLLOTT with this.w_FLLOTT
      replace t_CODIVA with this.w_CODIVA
      replace t_PERIVA with this.w_PERIVA
      replace t_INDIVA with this.w_INDIVA
      replace t_MMCODLIS with this.w_MMCODLIS
      replace t_CATSCA with this.w_CATSCA
      replace t_ARTDIS with this.w_ARTDIS
      replace t_FLDISP with this.w_FLDISP
      replace t_MMCAUCOL with this.w_MMCAUCOL
      replace t_MMFLCASC with this.w_MMFLCASC
      replace t_MMFLORDI with this.w_MMFLORDI
      replace t_MMFLIMPE with this.w_MMFLIMPE
      replace t_MMFLRISE with this.w_MMFLRISE
      replace t_MMF2CASC with this.w_MMF2CASC
      replace t_MMF2ORDI with this.w_MMF2ORDI
      replace t_MMF2IMPE with this.w_MMF2IMPE
      replace t_MMF2RISE with this.w_MMF2RISE
      replace t_MMKEYSAL with this.w_MMKEYSAL
      replace t_MMFLLOTT with this.w_MMFLLOTT
      replace t_MMF2LOTT with this.w_MMF2LOTT
      replace t_LIPREZZO with this.w_LIPREZZO
      replace t_ACTSCOR with this.w_ACTSCOR
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_OQTA with this.w_OQTA
      replace t_CAMBIOLOT with this.w_CAMBIOLOT
      replace t_STATEROW with this.w_STATEROW
      replace t_VALUNI with this.w_VALUNI
      replace t_VALUNI2 with this.w_VALUNI2
      replace t_FLAVA1 with this.w_FLAVA1
      replace t_MMIMPNAZ with this.w_MMIMPNAZ
      replace t_TIPART with this.w_TIPART
      replace t_MMTIPATT with this.w_MMTIPATT
      replace t_MMCODATT with this.w_MMCODATT
      replace t_MMIMPCOM with this.w_MMIMPCOM
      replace t_CODCOS with this.w_CODCOS
      replace t_MMFLORCO with this.w_MMFLORCO
      replace t_MMFLCOCO with this.w_MMFLCOCO
      replace t_MMCODCOS with this.w_MMCODCOS
      replace t_FLCOMM with this.w_FLCOMM
      replace t_ULTCAR with this.w_ULTCAR
      replace t_ULTSCA with this.w_ULTSCA
      replace t_MMFLULCA with this.w_MMFLULCA
      replace t_MMFLULPV with this.w_MMFLULPV
      replace t_MMVALULT with this.w_MMVALULT
      replace t_MMCODMAG with this.w_MMCODMAG
      replace t_MMCODMAT with this.w_MMCODMAT
      replace t_QTRPER with this.w_QTRPER
      replace t_Q2APER with this.w_Q2APER
      replace t_Q2RPER with this.w_Q2RPER
      replace t_FM1 with this.w_FM1
      replace t_FM2 with this.w_FM2
      replace t_FLSTAT with this.w_FLSTAT
      replace t_LOTZOOM with this.w_LOTZOOM
      replace t_CODCON with this.w_CODCON
      replace t_OLDQTA with this.w_OLDQTA
      replace t_UBIZOOM with this.w_UBIZOOM
      replace t_OBSMAG with this.w_OBSMAG
      replace t_OBSMAT with this.w_OBSMAT
      replace t_IVASCOR with this.w_IVASCOR
      replace t_TOTMAT with this.w_TOTMAT
      replace t_MTCARI with this.w_MTCARI
      replace t_DATLOT with this.w_DATLOT
      replace t_DISLOT with this.w_DISLOT
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_NOFRAZ1 with this.w_NOFRAZ1
      replace t_MODUM2 with this.w_MODUM2
      replace t_RICTOT with this.w_RICTOT
      replace t_QTOPER with this.w_QTOPER
      replace t_QTIPER with this.w_QTIPER
      replace t_MMLOTMAG with this.w_MMLOTMAG
      replace t_MMLOTMAT with this.w_MMLOTMAT
      replace t_PREZUM with this.w_PREZUM
      replace t_CLUNIMIS with this.w_CLUNIMIS
      replace t_FLCOM1 with this.w_FLCOM1
      replace t_ARCLAMAT with this.w_ARCLAMAT
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate(AH_Msgformat(IIF(.w_MMTIPCON="C", "Cliente:",IIF(.w_MMTIPCON="F","Fornitore:",""))),'','')
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_89.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_90.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_96.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_100.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_111.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_114.Calculate(IIF(.w_MMUNIMIS=.w_UNMIS1 OR EMPTY(.w_MMCODICE),IIF(Not Empty(.w_UNMIS3), Ah_MsgFormat("Qta nella 3^UM:"),IIF(Not Empty(.w_UNMIS2), Ah_MsgFormat("Qta nella 2^UM:"),'')),IIF(.w_MMUNIMIS<>.w_UNMIS1 And .w_MMUNIMIS<>.w_UNMIS2 AND .w_MMUNIMIS<>.w_UNMIS3,"", Ah_MsgFormat("Qta e prezzo nella 1^UM:"))))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_118.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_127.Calculate(Ah_MsgFormat("Commessa:"))
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_114.Calculate(IIF(.w_MMUNIMIS=.w_UNMIS1 OR EMPTY(.w_MMCODICE),IIF(Not Empty(.w_UNMIS3), Ah_MsgFormat("Qta nella 3^UM:"),IIF(Not Empty(.w_UNMIS2), Ah_MsgFormat("Qta nella 2^UM:"),'')),IIF(.w_MMUNIMIS<>.w_UNMIS1 And .w_MMUNIMIS<>.w_UNMIS2 AND .w_MMUNIMIS<>.w_UNMIS3,"", Ah_MsgFormat("Qta e prezzo nella 1^UM:"))))
        .oPgFrm.Page1.oPag.oObj_2_127.Calculate(Ah_MsgFormat("Commessa:"))
    endwith
  return
  proc Calculate_XZDFPDUIPY()
    with this
          * --- Controllo cambio  lotto
          .w_CAMBIOLOT = .T.
    endwith
  endproc
  proc Calculate_RRLREVPYPJ()
    with this
          * --- Gsma_bmc - check fuori transazione
          GSMA_BMC(this;
             )
    endwith
  endproc
  proc Calculate_NODPCSFTOG()
    with this
          * --- Verifica fattibilit� F3, F6, F5 (gsma_bnr))
          GSMA_BNR(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMMTCAMAG_1_14.enabled = this.oPgFrm.Page1.oPag.oMMTCAMAG_1_14.mCond()
    this.oPgFrm.Page1.oPag.oMMCODCON_1_20.enabled = this.oPgFrm.Page1.oPag.oMMCODCON_1_20.mCond()
    this.oPgFrm.Page1.oPag.oMMSCOCL1_1_25.enabled = this.oPgFrm.Page1.oPag.oMMSCOCL1_1_25.mCond()
    this.oPgFrm.Page1.oPag.oMMSCOCL2_1_26.enabled = this.oPgFrm.Page1.oPag.oMMSCOCL2_1_26.mCond()
    this.oPgFrm.Page1.oPag.oMMCODVAL_1_30.enabled = this.oPgFrm.Page1.oPag.oMMCODVAL_1_30.mCond()
    this.oPgFrm.Page1.oPag.oMMCAOVAL_1_36.enabled = this.oPgFrm.Page1.oPag.oMMCAOVAL_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_98.enabled = this.oPgFrm.Page1.oPag.oBtn_1_98.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMMCODMAG_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMMCODMAG_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMMCODMAT_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMMCODMAT_2_6.mCond()
    this.oPgFrm.Page1.oPag.oMMCAUMAG_2_27.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMMCAUMAG_2_27.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMMUNIMIS_2_46.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMMUNIMIS_2_46.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMMQTAMOV_2_48.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMMQTAMOV_2_48.mCond()
    this.oPgFrm.Page1.oPag.oMMCODLOT_2_50.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMMCODLOT_2_50.mCond()
    this.oPgFrm.Page1.oPag.oMMCODUBI_2_55.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMMCODUBI_2_55.mCond()
    this.oPgFrm.Page1.oPag.oMMCODUB2_2_56.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMMCODUB2_2_56.mCond()
    this.oPgFrm.Page1.oPag.oMMQTAUM1_2_57.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMMQTAUM1_2_57.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMMPREZZO_2_58.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMMPREZZO_2_58.mCond()
    this.oPgFrm.Page1.oPag.oPREUM1_2_59.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPREUM1_2_59.mCond()
    this.oPgFrm.Page1.oPag.oMMSCONT1_2_60.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMMSCONT1_2_60.mCond()
    this.oPgFrm.Page1.oPag.oMMSCONT2_2_61.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMMSCONT2_2_61.mCond()
    this.oPgFrm.Page1.oPag.oMMSCONT3_2_62.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMMSCONT3_2_62.mCond()
    this.oPgFrm.Page1.oPag.oMMSCONT4_2_63.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMMSCONT4_2_63.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_54.enabled =this.oPgFrm.Page1.oPag.oBtn_2_54.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_109.enabled =this.oPgFrm.Page1.oPag.oBtn_2_109.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_110.enabled =this.oPgFrm.Page1.oPag.oBtn_2_110.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_2_71.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_71.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSVE_MMT.visible")=='L' And this.GSVE_MMT.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_71.enabled
      this.GSVE_MMT.HideChildrenChain()
    endif 
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_119.visible=!this.oPgFrm.Page1.oPag.oStr_1_119.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oUNMIS1_2_15.visible=!this.oPgFrm.Page1.oPag.oUNMIS1_2_15.mHide()
    this.oPgFrm.Page1.oPag.oMMCAUMAG_2_27.visible=!this.oPgFrm.Page1.oPag.oMMCAUMAG_2_27.mHide()
    this.oPgFrm.Page1.oPag.oMMCODLOT_2_50.visible=!this.oPgFrm.Page1.oPag.oMMCODLOT_2_50.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_54.visible=!this.oPgFrm.Page1.oPag.oBtn_2_54.mHide()
    this.oPgFrm.Page1.oPag.oMMCODUBI_2_55.visible=!this.oPgFrm.Page1.oPag.oMMCODUBI_2_55.mHide()
    this.oPgFrm.Page1.oPag.oMMCODUB2_2_56.visible=!this.oPgFrm.Page1.oPag.oMMCODUB2_2_56.mHide()
    this.oPgFrm.Page1.oPag.oMMQTAUM1_2_57.visible=!this.oPgFrm.Page1.oPag.oMMQTAUM1_2_57.mHide()
    this.oPgFrm.Page1.oPag.oPREUM1_2_59.visible=!this.oPgFrm.Page1.oPag.oPREUM1_2_59.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_2_71.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_71.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSVE_MMT.visible")=='L' And this.GSVE_MMT.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_71.visible
      this.GSVE_MMT.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oQ2DISP_2_92.visible=!this.oPgFrm.Page1.oPag.oQ2DISP_2_92.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_108.visible=!this.oPgFrm.Page1.oPag.oBtn_2_108.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_109.visible=!this.oPgFrm.Page1.oPag.oBtn_2_109.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_110.visible=!this.oPgFrm.Page1.oPag.oBtn_2_110.mHide()
    this.oPgFrm.Page1.oPag.oQTAUM2_2_113.visible=!this.oPgFrm.Page1.oPag.oQTAUM2_2_113.mHide()
    this.oPgFrm.Page1.oPag.oUNIMIS2_2_115.visible=!this.oPgFrm.Page1.oPag.oUNIMIS2_2_115.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_76.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_77.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_78.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_85.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_88.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_89.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_90.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_95.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_96.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_100.Event(cEvent)
        if lower(cEvent)==lower("CheckNoTransaction")
          .Calculate_RRLREVPYPJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("HasEvent")
          .Calculate_NODPCSFTOG()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_111.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_114.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_118.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_127.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MMCODESE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_MMCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZICOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZICOD;
                       ,'ESCODESE',this.w_MMCODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_MMVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MMCODESE = space(4)
      endif
      this.w_MMVALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMVALNAZ
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MMVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MMVALNAZ)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_CAONAZ = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOP = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_MMVALNAZ = space(3)
      endif
      this.w_CAONAZ = 0
      this.w_DECTOP = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.VACODVAL as VACODVAL108"+ ",link_1_8.VACAOVAL as VACAOVAL108"+ ",link_1_8.VADECTOT as VADECTOT108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on MVM_MAST.MMVALNAZ=link_1_8.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and MVM_MAST.MMVALNAZ=link_1_8.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MMTCAMAG
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMTCAMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_MMTCAMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLNDOC,CMCAUCOL,CMDTOBSO,CMVARVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_MMTCAMAG))
          select CMCODICE,CMDESCRI,CMFLCLFR,CMFLNDOC,CMCAUCOL,CMDTOBSO,CMVARVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMTCAMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_MMTCAMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLNDOC,CMCAUCOL,CMDTOBSO,CMVARVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_MMTCAMAG)+"%");

            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLNDOC,CMCAUCOL,CMDTOBSO,CMVARVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MMTCAMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oMMTCAMAG_1_14'),i_cWhere,'GSMA_ACM',"Causali magazzino",'GSMA_MVM.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLNDOC,CMCAUCOL,CMDTOBSO,CMVARVAL";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLNDOC,CMCAUCOL,CMDTOBSO,CMVARVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMTCAMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCLFR,CMFLNDOC,CMCAUCOL,CMDTOBSO,CMVARVAL";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MMTCAMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MMTCAMAG)
            select CMCODICE,CMDESCRI,CMFLCLFR,CMFLNDOC,CMCAUCOL,CMDTOBSO,CMVARVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMTCAMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CMDESCRI,space(35))
      this.w_MMFLCLFR = NVL(_Link_.CMFLCLFR,space(1))
      this.w_FLNDOC = NVL(_Link_.CMFLNDOC,space(1))
      this.w_CAUCOL = NVL(_Link_.CMCAUCOL,space(5))
      this.w_DTOBSOM = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
      this.w_VARVAL = NVL(_Link_.CMVARVAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MMTCAMAG = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_MMFLCLFR = space(1)
      this.w_FLNDOC = space(1)
      this.w_CAUCOL = space(5)
      this.w_DTOBSOM = ctod("  /  /  ")
      this.w_VARVAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSOM>.w_MMDATREG OR EMPTY(.w_DTOBSOM) And .w_VARVAL='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente, a variazione di valore oppure obsoleta")
        endif
        this.w_MMTCAMAG = space(5)
        this.w_DESCAU = space(35)
        this.w_MMFLCLFR = space(1)
        this.w_FLNDOC = space(1)
        this.w_CAUCOL = space(5)
        this.w_DTOBSOM = ctod("  /  /  ")
        this.w_VARVAL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMTCAMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.CMCODICE as CMCODICE114"+ ",link_1_14.CMDESCRI as CMDESCRI114"+ ",link_1_14.CMFLCLFR as CMFLCLFR114"+ ",link_1_14.CMFLNDOC as CMFLNDOC114"+ ",link_1_14.CMCAUCOL as CMCAUCOL114"+ ",link_1_14.CMDTOBSO as CMDTOBSO114"+ ",link_1_14.CMVARVAL as CMVARVAL114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on MVM_MAST.MMTCAMAG=link_1_14.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and MVM_MAST.MMTCAMAG=link_1_14.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MMCODCON
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MMCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MMTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCATSCM,ANDTOBSO,ANCODESC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MMTIPCON;
                     ,'ANCODICE',trim(this.w_MMCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCATSCM,ANDTOBSO,ANCODESC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_MMCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MMTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCATSCM,ANDTOBSO,ANCODESC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_MMCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MMTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCATSCM,ANDTOBSO,ANCODESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANPARIVA like "+cp_ToStrODBC(trim(this.w_MMCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MMTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCATSCM,ANDTOBSO,ANCODESC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANPARIVA like "+cp_ToStr(trim(this.w_MMCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MMTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCATSCM,ANDTOBSO,ANCODESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MMCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oMMCODCON_1_20'),i_cWhere,'GSAR_BZC',"Anagrafica clienti/fornitori",'GSCG1MPN.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MMTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCATSCM,ANDTOBSO,ANCODESC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCATSCM,ANDTOBSO,ANCODESC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCATSCM,ANDTOBSO,ANCODESC";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MMTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCATSCM,ANDTOBSO,ANCODESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCATSCM,ANDTOBSO,ANCODESC";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MMCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MMTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MMTIPCON;
                       ,'ANCODICE',this.w_MMCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANNUMLIS,AN1SCONT,AN2SCONT,ANCATSCM,ANDTOBSO,ANCODESC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
      this.w_PARIVA = NVL(_Link_.ANPARIVA,space(12))
      this.w_VALCLF = NVL(_Link_.ANCODVAL,space(3))
      this.w_MMTCOLIS = NVL(_Link_.ANNUMLIS,space(5))
      this.w_MMSCOCL1 = NVL(_Link_.AN1SCONT,0)
      this.w_MMSCOCL2 = NVL(_Link_.AN2SCONT,0)
      this.w_CATSCC = NVL(_Link_.ANCATSCM,space(5))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_CODESC = NVL(_Link_.ANCODESC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MMCODCON = space(15)
      endif
      this.w_DESCLF = space(40)
      this.w_PARIVA = space(12)
      this.w_VALCLF = space(3)
      this.w_MMTCOLIS = space(5)
      this.w_MMSCOCL1 = 0
      this.w_MMSCOCL2 = 0
      this.w_CATSCC = space(5)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_CODESC = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSO>.w_MMDATREG OR EMPTY(.w_DTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MMCODCON = space(15)
        this.w_DESCLF = space(40)
        this.w_PARIVA = space(12)
        this.w_VALCLF = space(3)
        this.w_MMTCOLIS = space(5)
        this.w_MMSCOCL1 = 0
        this.w_MMSCOCL2 = 0
        this.w_CATSCC = space(5)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_CODESC = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.ANCODICE as ANCODICE120"+ ",link_1_20.ANDESCRI as ANDESCRI120"+ ",link_1_20.ANPARIVA as ANPARIVA120"+ ",link_1_20.ANCODVAL as ANCODVAL120"+ ",link_1_20.ANNUMLIS as ANNUMLIS120"+ ",link_1_20.AN1SCONT as AN1SCONT120"+ ",link_1_20.AN2SCONT as AN2SCONT120"+ ",link_1_20.ANCATSCM as ANCATSCM120"+ ",link_1_20.ANDTOBSO as ANDTOBSO120"+ ",link_1_20.ANCODESC as ANCODESC120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on MVM_MAST.MMCODCON=link_1_20.ANCODICE"+" and MVM_MAST.MMTIPCON=link_1_20.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and MVM_MAST.MMCODCON=link_1_20.ANCODICE(+)"'+'+" and MVM_MAST.MMTIPCON=link_1_20.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MMCODVAL
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_MMCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VADECUNI,VACAOVAL,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_MMCODVAL))
          select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VADECUNI,VACAOVAL,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_MMCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VADECUNI,VACAOVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_MMCODVAL)+"%");

            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VADECUNI,VACAOVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MMCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oMMCODVAL_1_30'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VADECUNI,VACAOVAL,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VADECUNI,VACAOVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VADECUNI,VACAOVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MMCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MMCODVAL)
            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VADECUNI,VACAOVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(30))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MMCODVAL = space(3)
      endif
      this.w_DESAPP = space(30)
      this.w_SIMVAL = space(5)
      this.w_DECTOT = 0
      this.w_DECUNI = 0
      this.w_CAOVAL = 0
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o obsoleta o valuta esercizio non definita")
        endif
        this.w_MMCODVAL = space(3)
        this.w_DESAPP = space(30)
        this.w_SIMVAL = space(5)
        this.w_DECTOT = 0
        this.w_DECUNI = 0
        this.w_CAOVAL = 0
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_30.VACODVAL as VACODVAL130"+ ",link_1_30.VADESVAL as VADESVAL130"+ ",link_1_30.VASIMVAL as VASIMVAL130"+ ",link_1_30.VADECTOT as VADECTOT130"+ ",link_1_30.VADECUNI as VADECUNI130"+ ",link_1_30.VACAOVAL as VACAOVAL130"+ ",link_1_30.VADTOBSO as VADTOBSO130"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_30 on MVM_MAST.MMCODVAL=link_1_30.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_30"
          i_cKey=i_cKey+'+" and MVM_MAST.MMCODVAL=link_1_30.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MMTCOLIS
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMTCOLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_MMTCOLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSDTOBSO,LSDTINVA,LSVALLIS,LSIVALIS,LSQUANTI,LSFLSCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_MMTCOLIS))
          select LSCODLIS,LSDESLIS,LSDTOBSO,LSDTINVA,LSVALLIS,LSIVALIS,LSQUANTI,LSFLSCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMTCOLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_MMTCOLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSDTOBSO,LSDTINVA,LSVALLIS,LSIVALIS,LSQUANTI,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_MMTCOLIS)+"%");

            select LSCODLIS,LSDESLIS,LSDTOBSO,LSDTINVA,LSVALLIS,LSIVALIS,LSQUANTI,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MMTCOLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oMMTCOLIS_1_37'),i_cWhere,'GSAR_ALI',"Listini",'gsma1mvm.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSDTOBSO,LSDTINVA,LSVALLIS,LSIVALIS,LSQUANTI,LSFLSCON";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSDTOBSO,LSDTINVA,LSVALLIS,LSIVALIS,LSQUANTI,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMTCOLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSDTOBSO,LSDTINVA,LSVALLIS,LSIVALIS,LSQUANTI,LSFLSCON";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_MMTCOLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_MMTCOLIS)
            select LSCODLIS,LSDESLIS,LSDTOBSO,LSDTINVA,LSVALLIS,LSIVALIS,LSQUANTI,LSFLSCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMTCOLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_FINLIS = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
      this.w_INILIS = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_QUALIS = NVL(_Link_.LSQUANTI,space(1))
      this.w_SCOLIS = NVL(_Link_.LSFLSCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MMTCOLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_FINLIS = ctod("  /  /  ")
      this.w_INILIS = ctod("  /  /  ")
      this.w_VALLIS = space(3)
      this.w_IVALIS = space(1)
      this.w_QUALIS = space(1)
      this.w_SCOLIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_VALLIS=.w_MMCODVAL AND .w_INILIS<=.w_MMDATREG AND (.w_FINLIS>=.w_MMDATREG OR EMPTY(.w_FINLIS))) OR EMPTY(.w_MMTCOLIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino inesistente, obsoleto non valido oppure valuta listino incongruente")
        endif
        this.w_MMTCOLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_FINLIS = ctod("  /  /  ")
        this.w_INILIS = ctod("  /  /  ")
        this.w_VALLIS = space(3)
        this.w_IVALIS = space(1)
        this.w_QUALIS = space(1)
        this.w_SCOLIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMTCOLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_37(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_37.LSCODLIS as LSCODLIS137"+ ",link_1_37.LSDESLIS as LSDESLIS137"+ ",link_1_37.LSDTOBSO as LSDTOBSO137"+ ",link_1_37.LSDTINVA as LSDTINVA137"+ ",link_1_37.LSVALLIS as LSVALLIS137"+ ",link_1_37.LSIVALIS as LSIVALIS137"+ ",link_1_37.LSQUANTI as LSQUANTI137"+ ",link_1_37.LSFLSCON as LSFLSCON137"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_37 on MVM_MAST.MMTCOLIS=link_1_37.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_37"
          i_cKey=i_cKey+'+" and MVM_MAST.MMTCOLIS=link_1_37.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MMCODICE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_MMCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_MMCODICE))
          select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMCODICE)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_MMCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_MMCODICE)+"%");

            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MMCODICE) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oMMCODICE_2_1'),i_cWhere,'GSMA_BZA',"Chiavi articoli",'GSMA_MVM.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_MMCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_MMCODICE)
            select CACODICE,CADESART,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODICE = NVL(_Link_.CACODICE,space(20))
      this.w_DESART = NVL(_Link_.CADESART,space(40))
      this.w_MMCODART = NVL(_Link_.CACODART,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_TIPRIG = NVL(_Link_.CA__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MMCODICE = space(20)
      endif
      this.w_DESART = space(40)
      this.w_MMCODART = space(20)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_TIPRIG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIG='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente")
        endif
        this.w_MMCODICE = space(20)
        this.w_DESART = space(40)
        this.w_MMCODART = space(20)
        this.w_UNMIS3 = space(3)
        this.w_OPERA3 = space(1)
        this.w_MOLTI3 = 0
        this.w_TIPRIG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.CACODICE as CACODICE201"+ ",link_2_1.CADESART as CADESART201"+ ",link_2_1.CACODART as CACODART201"+ ",link_2_1.CAUNIMIS as CAUNIMIS201"+ ",link_2_1.CAOPERAT as CAOPERAT201"+ ",link_2_1.CAMOLTIP as CAMOLTIP201"+ ",link_2_1.CA__TIPO as CA__TIPO201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on MVM_DETT.MMCODICE=link_2_1.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and MVM_DETT.MMCODICE=link_2_1.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MMCODART
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARCATSCM,ARCODIVA,ARTIPART,ARFLDISP,ARFLUSEP,ARFLLOTT,ARMAGPRE,ARGESMAT,ARDISLOT,ARPREZUM,ARSALCOM,ARCLAMAT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_MMCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_MMCODART)
            select ARCODART,ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARCATSCM,ARCODIVA,ARTIPART,ARFLDISP,ARFLUSEP,ARFLLOTT,ARMAGPRE,ARGESMAT,ARDISLOT,ARPREZUM,ARSALCOM,ARCLAMAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_CATSCA = NVL(_Link_.ARCATSCM,space(5))
      this.w_CODIVA = NVL(_Link_.ARCODIVA,space(5))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_ARTDIS = NVL(_Link_.ARFLDISP,space(1))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_FLLOTT = NVL(_Link_.ARFLLOTT,space(1))
      this.w_MAGPRE = NVL(_Link_.ARMAGPRE,space(5))
      this.w_GESMAT = NVL(_Link_.ARGESMAT,space(1))
      this.w_DISLOT = NVL(_Link_.ARDISLOT,space(1))
      this.w_PREZUM = NVL(_Link_.ARPREZUM,space(1))
      this.w_FLCOM1 = NVL(_Link_.ARSALCOM,space(1))
      this.w_ARCLAMAT = NVL(_Link_.ARCLAMAT,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MMCODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_MOLTIP = 0
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = space(1)
      this.w_CATSCA = space(5)
      this.w_CODIVA = space(5)
      this.w_TIPART = space(2)
      this.w_ARTDIS = space(1)
      this.w_FLUSEP = space(1)
      this.w_FLLOTT = space(1)
      this.w_MAGPRE = space(5)
      this.w_GESMAT = space(1)
      this.w_DISLOT = space(1)
      this.w_PREZUM = space(1)
      this.w_FLCOM1 = space(1)
      this.w_ARCLAMAT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 17 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+17<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.ARCODART as ARCODART202"+ ",link_2_2.ARUNMIS1 as ARUNMIS1202"+ ",link_2_2.ARMOLTIP as ARMOLTIP202"+ ",link_2_2.ARUNMIS2 as ARUNMIS2202"+ ",link_2_2.AROPERAT as AROPERAT202"+ ",link_2_2.ARCATSCM as ARCATSCM202"+ ",link_2_2.ARCODIVA as ARCODIVA202"+ ",link_2_2.ARTIPART as ARTIPART202"+ ",link_2_2.ARFLDISP as ARFLDISP202"+ ",link_2_2.ARFLUSEP as ARFLUSEP202"+ ",link_2_2.ARFLLOTT as ARFLLOTT202"+ ",link_2_2.ARMAGPRE as ARMAGPRE202"+ ",link_2_2.ARGESMAT as ARGESMAT202"+ ",link_2_2.ARDISLOT as ARDISLOT202"+ ",link_2_2.ARPREZUM as ARPREZUM202"+ ",link_2_2.ARSALCOM as ARSALCOM202"+ ",link_2_2.ARCLAMAT as ARCLAMAT202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on MVM_DETT.MMCODART=link_2_2.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+17
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and MVM_DETT.MMCODART=link_2_2.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+17
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MMCODMAG
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MMCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MMCODMAG))
          select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MMCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMMCODMAG_2_4'),i_cWhere,'GSAR_AMA',"Magazzini",'GSMA2AMA.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MMCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MMCODMAG)
            select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESAPP = NVL(_Link_.MGDESMAG,space(30))
      this.w_FLUBIC = NVL(_Link_.MGFLUBIC,space(1))
      this.w_OBSMAG = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MMCODMAG = space(5)
      endif
      this.w_DESAPP = space(30)
      this.w_FLUBIC = space(1)
      this.w_OBSMAG = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_OBSMAG>.w_MMDATREG OR EMPTY(.w_OBSMAG)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MMCODMAG = space(5)
        this.w_DESAPP = space(30)
        this.w_FLUBIC = space(1)
        this.w_OBSMAG = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.MGCODMAG as MGCODMAG204"+ ",link_2_4.MGDESMAG as MGDESMAG204"+ ",link_2_4.MGFLUBIC as MGFLUBIC204"+ ",link_2_4.MGDTOBSO as MGDTOBSO204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on MVM_DETT.MMCODMAG=link_2_4.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and MVM_DETT.MMCODMAG=link_2_4.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MMCODMAT
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_MMCODMAT)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_MMCODMAT))
          select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMCODMAT)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_MMCODMAT)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_MMCODMAT)+"%");

            select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MMCODMAT) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oMMCODMAT_2_6'),i_cWhere,'GSAR_AMA',"Magazzini",'GSMA2AMA.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_MMCODMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_MMCODMAT)
            select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODMAT = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESAPP = NVL(_Link_.MGDESMAG,space(30))
      this.w_F2UBIC = NVL(_Link_.MGFLUBIC,space(1))
      this.w_OBSMAT = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MMCODMAT = space(5)
      endif
      this.w_DESAPP = space(30)
      this.w_F2UBIC = space(1)
      this.w_OBSMAT = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_OBSMAT>.w_MMDATREG OR EMPTY(.w_OBSMAT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MMCODMAT = space(5)
        this.w_DESAPP = space(30)
        this.w_F2UBIC = space(1)
        this.w_OBSMAT = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.MGCODMAG as MGCODMAG206"+ ",link_2_6.MGDESMAG as MGDESMAG206"+ ",link_2_6.MGFLUBIC as MGFLUBIC206"+ ",link_2_6.MGDTOBSO as MGDTOBSO206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on MVM_DETT.MMCODMAT=link_2_6.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and MVM_DETT.MMCODMAT=link_2_6.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_NOFRAZ1 = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_NOFRAZ1 = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVPERIND";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVPERIVA,IVPERIND;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
      this.w_INDIVA = NVL(_Link_.IVPERIND,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_PERIVA = 0
      this.w_INDIVA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMCAUMAG
  func Link_2_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCAUMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_MMCAUMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM,CMMTCARI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_MMCAUMAG))
          select CMCODICE,CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM,CMMTCARI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMCAUMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MMCAUMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oMMCAUMAG_2_27'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM,CMMTCARI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM,CMMTCARI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCAUMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM,CMMTCARI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MMCAUMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MMCAUMAG)
            select CMCODICE,CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM,CMMTCARI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCAUMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_MMCAUCOL = NVL(_Link_.CMCAUCOL,space(5))
      this.w_MMFLCASC = NVL(_Link_.CMFLCASC,space(1))
      this.w_MMFLORDI = NVL(_Link_.CMFLORDI,space(1))
      this.w_MMFLIMPE = NVL(_Link_.CMFLIMPE,space(1))
      this.w_MMFLRISE = NVL(_Link_.CMFLRISE,space(1))
      this.w_FLAVA1 = NVL(_Link_.CMFLAVAL,space(1))
      this.w_MMFLELGM = NVL(_Link_.CMFLELGM,space(1))
      this.w_FLCOMM = NVL(_Link_.CMFLCOMM,space(1))
      this.w_MTCARI = NVL(_Link_.CMMTCARI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MMCAUMAG = space(5)
      endif
      this.w_MMCAUCOL = space(5)
      this.w_MMFLCASC = space(1)
      this.w_MMFLORDI = space(1)
      this.w_MMFLIMPE = space(1)
      this.w_MMFLRISE = space(1)
      this.w_FLAVA1 = space(1)
      this.w_MMFLELGM = space(1)
      this.w_FLCOMM = space(1)
      this.w_MTCARI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCAUMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_27(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAM_AGAZ_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_27.CMCODICE as CMCODICE227"+ ",link_2_27.CMCAUCOL as CMCAUCOL227"+ ",link_2_27.CMFLCASC as CMFLCASC227"+ ",link_2_27.CMFLORDI as CMFLORDI227"+ ",link_2_27.CMFLIMPE as CMFLIMPE227"+ ",link_2_27.CMFLRISE as CMFLRISE227"+ ",link_2_27.CMFLAVAL as CMFLAVAL227"+ ",link_2_27.CMFLELGM as CMFLELGM227"+ ",link_2_27.CMFLCOMM as CMFLCOMM227"+ ",link_2_27.CMMTCARI as CMMTCARI227"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_27 on MVM_DETT.MMCAUMAG=link_2_27.CMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_27"
          i_cKey=i_cKey+'+" and MVM_DETT.MMCAUMAG=link_2_27.CMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MMCAUCOL
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCAUCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCAUCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_MMCAUCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_MMCAUCOL)
            select CMCODICE,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCAUCOL = NVL(_Link_.CMCODICE,space(5))
      this.w_MMF2CASC = NVL(_Link_.CMFLCASC,space(1))
      this.w_MMF2ORDI = NVL(_Link_.CMFLORDI,space(1))
      this.w_MMF2IMPE = NVL(_Link_.CMFLIMPE,space(1))
      this.w_MMF2RISE = NVL(_Link_.CMFLRISE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MMCAUCOL = space(5)
      endif
      this.w_MMF2CASC = space(1)
      this.w_MMF2ORDI = space(1)
      this.w_MMF2IMPE = space(1)
      this.w_MMF2RISE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCAUCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMCODCOM
  func Link_2_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_MMCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_MMCODCOM)
            select CNCODCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODCOM = NVL(_Link_.CNCODCAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_MMCODCOM = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMUNIMIS
  func Link_2_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_MMUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_MMUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MMUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oMMUNIMIS_2_46'),i_cWhere,'GSAR_AUM',"",'GSMA_MVM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_MMUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_MMUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MMUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(.w_MMUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_MMUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_46(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_46.UMCODICE as UMCODICE246"+ ",link_2_46.UMFLFRAZ as UMFLFRAZ246"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_46 on MVM_DETT.MMUNIMIS=link_2_46.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_46"
          i_cKey=i_cKey+'+" and MVM_DETT.MMUNIMIS=link_2_46.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MMCODLOT
  func Link_2_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODLOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_ALO',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_MMCODLOT)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_MMCODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT,LODATSCA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_MMCODART;
                     ,'LOCODICE',trim(this.w_MMCODLOT))
          select LOCODART,LOCODICE,LOFLSTAT,LODATSCA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMCODLOT)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MMCODLOT) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oMMCODLOT_2_50'),i_cWhere,'GSMD_ALO',"Lotti articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MMCODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT,LODATSCA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LOFLSTAT,LODATSCA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente, sospeso, esaurito o scaduto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT,LODATSCA";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_MMCODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LOFLSTAT,LODATSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODLOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LOFLSTAT,LODATSCA";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_MMCODLOT);
                   +" and LOCODART="+cp_ToStrODBC(this.w_MMCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_MMCODART;
                       ,'LOCODICE',this.w_MMCODLOT)
            select LOCODART,LOCODICE,LOFLSTAT,LODATSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODLOT = NVL(_Link_.LOCODICE,space(20))
      this.w_FLSTAT = NVL(_Link_.LOFLSTAT,space(1))
      this.w_DATLOT = NVL(cp_ToDate(_Link_.LODATSCA),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MMCODLOT = space(20)
      endif
      this.w_FLSTAT = space(1)
      this.w_DATLOT = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MMFLLOTT='+' OR .w_FLSTAT<>'E' or (.w_FLSTAT='E' and .w_STATEROW<>'S') or .w_DISLOT<>'S') AND ((.w_FLSTAT<>'S' AND (Empty(.w_DATLOT) OR .w_DATLOT >.w_MMDATREG)) OR (This.cFunction='Edit' And .w_MMQTAUM1 <= .w_OLDQTA) OR this.cfunction="Query" OR CHKLOTUB(.w_MMCODLOT,'',0,.w_MMCODMAG,.w_MMCODART,.w_CODCON,.w_MMFLLOTT,.w_FLSTAT,.w_LOTZOOM,.w_MMDATREG,' ','L',.w_MMCAUMAG,.w_MMCAUCOL,.F.,.w_DISLOT))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice lotto inesistente, incongruente, sospeso, esaurito o scaduto")
        endif
        this.w_MMCODLOT = space(20)
        this.w_FLSTAT = space(1)
        this.w_DATLOT = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODLOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMCODUBI
  func Link_2_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODUBI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_MMCODUBI)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MMCODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_MMCODMAG;
                     ,'UBCODICE',trim(this.w_MMCODUBI))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMCODUBI)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MMCODUBI) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oMMCODUBI_2_55'),i_cWhere,'GSMD_MUB',"Ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MMCODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_MMCODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODUBI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_MMCODUBI);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MMCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_MMCODMAG;
                       ,'UBCODICE',this.w_MMCODUBI)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODUBI = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MMCODUBI = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MMFLLOTT='+') OR CHKLOTUB(.w_MMCODUBI,'',0,.w_MMCODMAG,.w_MMCODART,.w_CODCON,.w_MMFLLOTT,.w_FLSTAT,.w_LOTZOOM,.w_MMDATREG,' ','U','     ','     ',.T.,.w_DISLOT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        endif
        this.w_MMCODUBI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODUBI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMCODUB2
  func Link_2_56(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODUB2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_MUB',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_MMCODUB2)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MMCODMAT);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_MMCODMAT;
                     ,'UBCODICE',trim(this.w_MMCODUB2))
          select UBCODMAG,UBCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MMCODUB2)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MMCODUB2) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oMMCODUB2_2_56'),i_cWhere,'GSMD_MUB',"Ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MMCODMAT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_MMCODMAT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODUB2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_MMCODUB2);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_MMCODMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_MMCODMAT;
                       ,'UBCODICE',this.w_MMCODUB2)
            select UBCODMAG,UBCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MMCODUB2 = NVL(_Link_.UBCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_MMCODUB2 = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_MMF2LOTT='+') OR CHKLOTUB(.w_MMCODUB2,'',0,.w_MMCODMAT,.w_MMCODART,.w_CODCON,.w_MMF2LOTT,.w_FLSTAT,.w_LOTZOOM,.w_MMDATREG,' ','U','     ','     ',.T.,.w_DISLOT)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        endif
        this.w_MMCODUB2 = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODUB2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMCODMAG
  func Link_2_84(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_lTable = "SALDIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2], .t., this.SALDIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER,SLDATUCA,SLDATUPV,SLQTOPER,SLQTIPER";
                   +" from "+i_cTable+" "+i_lTable+" where SLCODMAG="+cp_ToStrODBC(this.w_MMCODMAG);
                   +" and SLCODICE="+cp_ToStrODBC(this.w_MMKEYSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SLCODICE',this.w_MMKEYSAL;
                       ,'SLCODMAG',this.w_MMCODMAG)
            select SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER,SLDATUCA,SLDATUPV,SLQTOPER,SLQTIPER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_QTAPER = NVL(_Link_.SLQTAPER,0)
      this.w_QTRPER = NVL(_Link_.SLQTRPER,0)
      this.w_ULTCAR = NVL(cp_ToDate(_Link_.SLDATUCA),ctod("  /  /  "))
      this.w_ULTSCA = NVL(cp_ToDate(_Link_.SLDATUPV),ctod("  /  /  "))
      this.w_QTOPER = NVL(_Link_.SLQTOPER,0)
      this.w_QTIPER = NVL(_Link_.SLQTIPER,0)
    else
      this.w_QTAPER = 0
      this.w_QTRPER = 0
      this.w_ULTCAR = ctod("  /  /  ")
      this.w_ULTSCA = ctod("  /  /  ")
      this.w_QTOPER = 0
      this.w_QTIPER = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])+'\'+cp_ToStr(_Link_.SLCODICE,1)+'\'+cp_ToStr(_Link_.SLCODMAG,1)
      cp_ShowWarn(i_cKey,this.SALDIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMCODMAT
  func Link_2_85(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDIART_IDX,3]
    i_lTable = "SALDIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2], .t., this.SALDIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMCODMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMCODMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER";
                   +" from "+i_cTable+" "+i_lTable+" where SLCODMAG="+cp_ToStrODBC(this.w_MMCODMAT);
                   +" and SLCODICE="+cp_ToStrODBC(this.w_MMKEYSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SLCODICE',this.w_MMKEYSAL;
                       ,'SLCODMAG',this.w_MMCODMAT)
            select SLCODICE,SLCODMAG,SLQTAPER,SLQTRPER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_Q2APER = NVL(_Link_.SLQTAPER,0)
      this.w_Q2RPER = NVL(_Link_.SLQTRPER,0)
    else
      this.w_Q2APER = 0
      this.w_Q2RPER = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDIART_IDX,2])+'\'+cp_ToStr(_Link_.SLCODICE,1)+'\'+cp_ToStr(_Link_.SLCODMAG,1)
      cp_ShowWarn(i_cKey,this.SALDIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMCODMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMLOTMAG
  func Link_2_121(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_lTable = "SALDILOT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2], .t., this.SALDILOT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMLOTMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMLOTMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SUCODART,SUCODUBI,SUCODLOT,SUCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where SUCODMAG="+cp_ToStrODBC(this.w_MMLOTMAG);
                   +" and SUCODART="+cp_ToStrODBC(this.w_MMKEYSAL);
                   +" and SUCODUBI="+cp_ToStrODBC(this.w_MMCODUBI);
                   +" and SUCODLOT="+cp_ToStrODBC(this.w_MMCODLOT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SUCODART',this.w_MMKEYSAL;
                       ,'SUCODUBI',this.w_MMCODUBI;
                       ,'SUCODLOT',this.w_MMCODLOT;
                       ,'SUCODMAG',this.w_MMLOTMAG)
            select SUCODART,SUCODUBI,SUCODLOT,SUCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
    else
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])+'\'+cp_ToStr(_Link_.SUCODART,1)+'\'+cp_ToStr(_Link_.SUCODUBI,1)+'\'+cp_ToStr(_Link_.SUCODLOT,1)+'\'+cp_ToStr(_Link_.SUCODMAG,1)
      cp_ShowWarn(i_cKey,this.SALDILOT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMLOTMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MMLOTMAT
  func Link_2_122(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDILOT_IDX,3]
    i_lTable = "SALDILOT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2], .t., this.SALDILOT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MMLOTMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MMLOTMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SUCODART,SUCODUBI,SUCODLOT,SUCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where SUCODMAG="+cp_ToStrODBC(this.w_MMLOTMAT);
                   +" and SUCODART="+cp_ToStrODBC(this.w_MMKEYSAL);
                   +" and SUCODUBI="+cp_ToStrODBC(this.w_MMCODUB2);
                   +" and SUCODLOT="+cp_ToStrODBC(this.w_MMCODLOT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SUCODART',this.w_MMKEYSAL;
                       ,'SUCODUBI',this.w_MMCODUB2;
                       ,'SUCODLOT',this.w_MMCODLOT;
                       ,'SUCODMAG',this.w_MMLOTMAT)
            select SUCODART,SUCODUBI,SUCODLOT,SUCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
    else
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDILOT_IDX,2])+'\'+cp_ToStr(_Link_.SUCODART,1)+'\'+cp_ToStr(_Link_.SUCODUBI,1)+'\'+cp_ToStr(_Link_.SUCODLOT,1)+'\'+cp_ToStr(_Link_.SUCODMAG,1)
      cp_ShowWarn(i_cKey,this.SALDILOT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MMLOTMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMMNUMREG_1_3.value==this.w_MMNUMREG)
      this.oPgFrm.Page1.oPag.oMMNUMREG_1_3.value=this.w_MMNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oMMCODUTE_1_4.value==this.w_MMCODUTE)
      this.oPgFrm.Page1.oPag.oMMCODUTE_1_4.value=this.w_MMCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oMMDATREG_1_5.value==this.w_MMDATREG)
      this.oPgFrm.Page1.oPag.oMMDATREG_1_5.value=this.w_MMDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oMMCODESE_1_7.value==this.w_MMCODESE)
      this.oPgFrm.Page1.oPag.oMMCODESE_1_7.value=this.w_MMCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oMMTCAMAG_1_14.value==this.w_MMTCAMAG)
      this.oPgFrm.Page1.oPag.oMMTCAMAG_1_14.value=this.w_MMTCAMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMMNUMDOC_1_15.value==this.w_MMNUMDOC)
      this.oPgFrm.Page1.oPag.oMMNUMDOC_1_15.value=this.w_MMNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMMALFDOC_1_16.value==this.w_MMALFDOC)
      this.oPgFrm.Page1.oPag.oMMALFDOC_1_16.value=this.w_MMALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMMDATDOC_1_17.value==this.w_MMDATDOC)
      this.oPgFrm.Page1.oPag.oMMDATDOC_1_17.value=this.w_MMDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_19.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_19.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oMMCODCON_1_20.value==this.w_MMCODCON)
      this.oPgFrm.Page1.oPag.oMMCODCON_1_20.value=this.w_MMCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_21.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_21.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oMMDESSUP_1_23.value==this.w_MMDESSUP)
      this.oPgFrm.Page1.oPag.oMMDESSUP_1_23.value=this.w_MMDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oMMSCOCL1_1_25.value==this.w_MMSCOCL1)
      this.oPgFrm.Page1.oPag.oMMSCOCL1_1_25.value=this.w_MMSCOCL1
    endif
    if not(this.oPgFrm.Page1.oPag.oMMSCOCL2_1_26.value==this.w_MMSCOCL2)
      this.oPgFrm.Page1.oPag.oMMSCOCL2_1_26.value=this.w_MMSCOCL2
    endif
    if not(this.oPgFrm.Page1.oPag.oMMSCOPAG_1_27.value==this.w_MMSCOPAG)
      this.oPgFrm.Page1.oPag.oMMSCOPAG_1_27.value=this.w_MMSCOPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oMMCODVAL_1_30.value==this.w_MMCODVAL)
      this.oPgFrm.Page1.oPag.oMMCODVAL_1_30.value=this.w_MMCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_32.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_32.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMMCAOVAL_1_36.value==this.w_MMCAOVAL)
      this.oPgFrm.Page1.oPag.oMMCAOVAL_1_36.value=this.w_MMCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMMTCOLIS_1_37.value==this.w_MMTCOLIS)
      this.oPgFrm.Page1.oPag.oMMTCOLIS_1_37.value=this.w_MMTCOLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oUNMIS1_2_15.value==this.w_UNMIS1)
      this.oPgFrm.Page1.oPag.oUNMIS1_2_15.value=this.w_UNMIS1
      replace t_UNMIS1 with this.oPgFrm.Page1.oPag.oUNMIS1_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMCAUMAG_2_27.value==this.w_MMCAUMAG)
      this.oPgFrm.Page1.oPag.oMMCAUMAG_2_27.value=this.w_MMCAUMAG
      replace t_MMCAUMAG with this.oPgFrm.Page1.oPag.oMMCAUMAG_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMFLELGM_2_33.RadioValue()==this.w_MMFLELGM)
      this.oPgFrm.Page1.oPag.oMMFLELGM_2_33.SetRadio()
      replace t_MMFLELGM with this.oPgFrm.Page1.oPag.oMMFLELGM_2_33.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMCODCOM_2_38.value==this.w_MMCODCOM)
      this.oPgFrm.Page1.oPag.oMMCODCOM_2_38.value=this.w_MMCODCOM
      replace t_MMCODCOM with this.oPgFrm.Page1.oPag.oMMCODCOM_2_38.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMFLOMAG_2_39.RadioValue()==this.w_MMFLOMAG)
      this.oPgFrm.Page1.oPag.oMMFLOMAG_2_39.SetRadio()
      replace t_MMFLOMAG with this.oPgFrm.Page1.oPag.oMMFLOMAG_2_39.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMCODLOT_2_50.value==this.w_MMCODLOT)
      this.oPgFrm.Page1.oPag.oMMCODLOT_2_50.value=this.w_MMCODLOT
      replace t_MMCODLOT with this.oPgFrm.Page1.oPag.oMMCODLOT_2_50.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMCODUBI_2_55.value==this.w_MMCODUBI)
      this.oPgFrm.Page1.oPag.oMMCODUBI_2_55.value=this.w_MMCODUBI
      replace t_MMCODUBI with this.oPgFrm.Page1.oPag.oMMCODUBI_2_55.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMCODUB2_2_56.value==this.w_MMCODUB2)
      this.oPgFrm.Page1.oPag.oMMCODUB2_2_56.value=this.w_MMCODUB2
      replace t_MMCODUB2 with this.oPgFrm.Page1.oPag.oMMCODUB2_2_56.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMQTAUM1_2_57.value==this.w_MMQTAUM1)
      this.oPgFrm.Page1.oPag.oMMQTAUM1_2_57.value=this.w_MMQTAUM1
      replace t_MMQTAUM1 with this.oPgFrm.Page1.oPag.oMMQTAUM1_2_57.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPREUM1_2_59.value==this.w_PREUM1)
      this.oPgFrm.Page1.oPag.oPREUM1_2_59.value=this.w_PREUM1
      replace t_PREUM1 with this.oPgFrm.Page1.oPag.oPREUM1_2_59.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMSCONT1_2_60.value==this.w_MMSCONT1)
      this.oPgFrm.Page1.oPag.oMMSCONT1_2_60.value=this.w_MMSCONT1
      replace t_MMSCONT1 with this.oPgFrm.Page1.oPag.oMMSCONT1_2_60.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMSCONT2_2_61.value==this.w_MMSCONT2)
      this.oPgFrm.Page1.oPag.oMMSCONT2_2_61.value=this.w_MMSCONT2
      replace t_MMSCONT2 with this.oPgFrm.Page1.oPag.oMMSCONT2_2_61.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMSCONT3_2_62.value==this.w_MMSCONT3)
      this.oPgFrm.Page1.oPag.oMMSCONT3_2_62.value=this.w_MMSCONT3
      replace t_MMSCONT3 with this.oPgFrm.Page1.oPag.oMMSCONT3_2_62.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMSCONT4_2_63.value==this.w_MMSCONT4)
      this.oPgFrm.Page1.oPag.oMMSCONT4_2_63.value=this.w_MMSCONT4
      replace t_MMSCONT4 with this.oPgFrm.Page1.oPag.oMMSCONT4_2_63.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMMVALMAG_2_67.value==this.w_MMVALMAG)
      this.oPgFrm.Page1.oPag.oMMVALMAG_2_67.value=this.w_MMVALMAG
      replace t_MMVALMAG with this.oPgFrm.Page1.oPag.oMMVALMAG_2_67.value
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAPER_2_86.value==this.w_QTAPER)
      this.oPgFrm.Page1.oPag.oQTAPER_2_86.value=this.w_QTAPER
      replace t_QTAPER with this.oPgFrm.Page1.oPag.oQTAPER_2_86.value
    endif
    if not(this.oPgFrm.Page1.oPag.oQTDISC_2_88.value==this.w_QTDISC)
      this.oPgFrm.Page1.oPag.oQTDISC_2_88.value=this.w_QTDISC
      replace t_QTDISC with this.oPgFrm.Page1.oPag.oQTDISC_2_88.value
    endif
    if not(this.oPgFrm.Page1.oPag.oQTDISP_2_91.value==this.w_QTDISP)
      this.oPgFrm.Page1.oPag.oQTDISP_2_91.value=this.w_QTDISP
      replace t_QTDISP with this.oPgFrm.Page1.oPag.oQTDISP_2_91.value
    endif
    if not(this.oPgFrm.Page1.oPag.oQ2DISP_2_92.value==this.w_Q2DISP)
      this.oPgFrm.Page1.oPag.oQ2DISP_2_92.value=this.w_Q2DISP
      replace t_Q2DISP with this.oPgFrm.Page1.oPag.oQ2DISP_2_92.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTALE_3_1.value==this.w_TOTALE)
      this.oPgFrm.Page1.oPag.oTOTALE_3_1.value=this.w_TOTALE
    endif
    if not(this.oPgFrm.Page1.oPag.oVISNAZ_2_95.value==this.w_VISNAZ)
      this.oPgFrm.Page1.oPag.oVISNAZ_2_95.value=this.w_VISNAZ
      replace t_VISNAZ with this.oPgFrm.Page1.oPag.oVISNAZ_2_95.value
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAUM2_2_113.value==this.w_QTAUM2)
      this.oPgFrm.Page1.oPag.oQTAUM2_2_113.value=this.w_QTAUM2
      replace t_QTAUM2 with this.oPgFrm.Page1.oPag.oQTAUM2_2_113.value
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS2_2_115.value==this.w_UNIMIS2)
      this.oPgFrm.Page1.oPag.oUNIMIS2_2_115.value=this.w_UNIMIS2
      replace t_UNIMIS2 with this.oPgFrm.Page1.oPag.oUNIMIS2_2_115.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODICE_2_1.value==this.w_MMCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODICE_2_1.value=this.w_MMCODICE
      replace t_MMCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODICE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODMAG_2_4.value==this.w_MMCODMAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODMAG_2_4.value=this.w_MMCODMAG
      replace t_MMCODMAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODMAG_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODMAT_2_6.value==this.w_MMCODMAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODMAT_2_6.value=this.w_MMCODMAT
      replace t_MMCODMAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODMAT_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_9.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_9.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_44.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_44.value=this.w_DESART
      replace t_DESART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_44.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMUNIMIS_2_46.value==this.w_MMUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMUNIMIS_2_46.value=this.w_MMUNIMIS
      replace t_MMUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMUNIMIS_2_46.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMQTAMOV_2_48.value==this.w_MMQTAMOV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMQTAMOV_2_48.value=this.w_MMQTAMOV
      replace t_MMQTAMOV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMQTAMOV_2_48.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMPREZZO_2_58.value==this.w_MMPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMPREZZO_2_58.value=this.w_MMPREZZO
      replace t_MMPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMPREZZO_2_58.value
    endif
    cp_SetControlsValueExtFlds(this,'MVM_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MMDATREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMMDATREG_1_5.SetFocus()
            i_bnoObbl = !empty(.w_MMDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MMCODESE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMMCODESE_1_7.SetFocus()
            i_bnoObbl = !empty(.w_MMCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MMTCAMAG) or not(.w_DTOBSOM>.w_MMDATREG OR EMPTY(.w_DTOBSOM) And .w_VARVAL='N'))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMMTCAMAG_1_14.SetFocus()
            i_bnoObbl = !empty(.w_MMTCAMAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente, a variazione di valore oppure obsoleta")
          case   not(.w_MMNUMDOC<>0 OR .w_FLNDOC<>'O')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMMNUMDOC_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero documento obbligatorio")
          case   not((NOT EMPTY(.w_MMDATDOC) OR .w_FLNDOC<>'O') and .w_MMDATDOC<=.w_MMDATREG)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMMDATDOC_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data documento deve essere minore o uguale alla data registrazione")
          case   (empty(.w_MMCODCON) or not(.w_DTOBSO>.w_MMDATREG OR EMPTY(.w_DTOBSO)))  and (.w_MMTIPCON$"CF" AND .cFunction<>'Edit')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMMCODCON_1_20.SetFocus()
            i_bnoObbl = !empty(.w_MMCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   (empty(.w_MMCODVAL) or not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMMCODVAL_1_30.SetFocus()
            i_bnoObbl = !empty(.w_MMCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o obsoleta o valuta esercizio non definita")
          case   (empty(.w_MMCAOVAL))  and (GETVALUT(g_PERVAL, 'VADATEUR')>=IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC) OR .w_CAOVAL=0)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMMCAOVAL_1_36.SetFocus()
            i_bnoObbl = !empty(.w_MMCAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_VALLIS=.w_MMCODVAL AND .w_INILIS<=.w_MMDATREG AND (.w_FINLIS>=.w_MMDATREG OR EMPTY(.w_FINLIS))) OR EMPTY(.w_MMTCOLIS))  and not(empty(.w_MMTCOLIS))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMMTCOLIS_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino inesistente, obsoleto non valido oppure valuta listino incongruente")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsma_mvm
      * --- Controllo Movimenti Generati da un Piano di Produzione
      if i_bRes And .cFunction='Edit' AND NOT EMPTY(.w_MMRIFPRO)
      msg=''
      if LEFT(.w_MMRIFPRO,1) $ 'IO'
          msg='Movimento generato da un impegno componenti in distinta base%0Confermi ugualmente?'
      else
          msg='Movimento generato da un carico produzione in distinta base%0Confermi ugualmente?'
      endif
      if NOT ah_yesno(msg)
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg = Ah_MsgFormat("Operazione abbandonata")
      endif
      endif	
      
      If i_bRes
      * Se tutto Ok lancio i controlli fuori transazione
       .NotifyEvent('CheckNoTransaction')
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_TIPRIG='R') and not(empty(.w_MMCODICE)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_MMCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODICE_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice incongruente")
        case   (empty(.w_MMCODMAG) or not(.w_OBSMAG>.w_MMDATREG OR EMPTY(.w_OBSMAG))) and (NOT EMPTY(.w_MMCODICE) AND (EMPTY(.w_MMCODMAG) OR .w_FM1<>'S')) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_MMCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODMAG_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        case   (empty(.w_MMCODMAT) or not(.w_OBSMAT>.w_MMDATREG OR EMPTY(.w_OBSMAT))) and (NOT EMPTY(.w_MMCODICE) AND NOT EMPTY(.w_MMCAUCOL) AND (EMPTY(.w_MMCODMAT) OR .w_FM2<>'S')) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_MMCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMCODMAT_2_6
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        case   (empty(.w_MMUNIMIS) or not(CHKUNIMI(.w_MMUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3))) and (NOT EMPTY(.w_MMCODICE)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_MMCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMUNIMIS_2_46
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
        case   not(.w_MMQTAMOV>0 AND (.w_FLFRAZ<>'S' OR .w_MMQTAMOV=INT(.w_MMQTAMOV))) and (NOT EMPTY(.w_MMCODICE)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_MMCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMMQTAMOV_2_48
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� movimentata inesistente o non frazionabile")
        case   not((.w_MMFLLOTT='+' OR .w_FLSTAT<>'E' or (.w_FLSTAT='E' and .w_STATEROW<>'S') or .w_DISLOT<>'S') AND ((.w_FLSTAT<>'S' AND (Empty(.w_DATLOT) OR .w_DATLOT >.w_MMDATREG)) OR (This.cFunction='Edit' And .w_MMQTAUM1 <= .w_OLDQTA) OR this.cfunction="Query" OR CHKLOTUB(.w_MMCODLOT,'',0,.w_MMCODMAG,.w_MMCODART,.w_CODCON,.w_MMFLLOTT,.w_FLSTAT,.w_LOTZOOM,.w_MMDATREG,' ','L',.w_MMCAUMAG,.w_MMCAUCOL,.F.,.w_DISLOT))) and (g_PERLOT='S' AND NOT EMPTY(.w_MMCODART) AND .w_FLLOTT$ 'SC' AND (.w_MMFLLOTT $ '+-' OR .w_MMF2LOTT $ '+-') AND .w_MMQTAMOV<>0) and not(empty(.w_MMCODLOT)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_MMCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oMMCODLOT_2_50
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice lotto inesistente, incongruente, sospeso, esaurito o scaduto")
        case   not((.w_MMFLLOTT='+') OR CHKLOTUB(.w_MMCODUBI,'',0,.w_MMCODMAG,.w_MMCODART,.w_CODCON,.w_MMFLLOTT,.w_FLSTAT,.w_LOTZOOM,.w_MMDATREG,' ','U','     ','     ',.T.,.w_DISLOT)) and (g_PERUBI='S' AND NOT EMPTY(.w_MMCODART) AND NOT EMPTY(.w_MMCODMAG) AND .w_FLUBIC='S'  AND .w_MMQTAMOV<>0) and not(empty(.w_MMCODUBI)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_MMCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oMMCODUBI_2_55
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        case   not((.w_MMF2LOTT='+') OR CHKLOTUB(.w_MMCODUB2,'',0,.w_MMCODMAT,.w_MMCODART,.w_CODCON,.w_MMF2LOTT,.w_FLSTAT,.w_LOTZOOM,.w_MMDATREG,' ','U','     ','     ',.T.,.w_DISLOT)) and (g_PERUBI='S' AND NOT EMPTY(.w_MMCODART) AND NOT EMPTY(.w_MMCODMAT) AND .w_F2UBIC='S' AND  .w_MMQTAMOV<>0) and not(empty(.w_MMCODUB2)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_MMCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oMMCODUB2_2_56
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice ubicazione inesistente o incongruente")
        case   not((.w_MMQTAUM1>0 AND (.w_NOFRAZ1<>'S' OR .w_MMQTAUM1=INT(.w_MMQTAUM1)))) and (.w_FLUSEP$'SQ' AND .w_MMUNIMIS<>.w_UNMIS1 AND NOT EMPTY(.w_MMCODICE)) and (.w_CPROWORD<>0 AND NOT EMPTY(.w_MMCODICE))
          .oNewFocus=.oPgFrm.Page1.oPag.oMMQTAUM1_2_57
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� nella 1a UM inesistente o non frazionabile")
      endcase
      i_bRes = i_bRes .and. .GSVE_MMT.CheckForm()
      if .w_CPROWORD<>0 AND NOT EMPTY(.w_MMCODICE)
        * --- Area Manuale = Check Row
        * --- gsma_mvm
        * --- Aggiorna Automatismi
        this.w_FM1=' '
        this.w_FM2=' '
        select (this.cTrsname)
        replace t_FM1 with ' '
        replace t_FM2 with ' '
        IF i_bRes=.T. AND (((g_PERLOT='S'AND .w_FLLOTT $ 'SC') OR (g_PERUBI='S'AND (.w_FLUBIC='S' OR .w_F2UBIC='S'))) AND((.w_MMFLLOTT $ '+-') OR (.w_MMF2LOTT $ '+-'));
            AND ((.cFunction='Load') OR (.cFunction='Edit'))AND .w_FLDISP$ 'SC' AND .w_DISLOT ='S')
           i_bRes =CHKMOVLO(.w_MMCODLOT,.w_MMCODUBI,.w_MMCODUB2,.w_MMCODMAG,.w_MMCODMAT,.w_MMFLLOTT,.w_MMF2LOTT,;
           .w_MMQTAUM1-.w_OLDQTA,.w_MMCODART,.w_FLLOTT,.w_FLUBIC,.w_F2UBIC,.w_FLSTAT,.w_MMSERIAL,'M',SPACE(10),.T.)
           i_bnoChk = .t.	
           i_cErrorMsg = ""
        ENDIF
        
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MMSERIAL = this.w_MMSERIAL
    this.o_MMDATREG = this.w_MMDATREG
    this.o_MMCODESE = this.w_MMCODESE
    this.o_MMTIPCON = this.w_MMTIPCON
    this.o_MMTCAMAG = this.w_MMTCAMAG
    this.o_MMDATDOC = this.w_MMDATDOC
    this.o_MMCODCON = this.w_MMCODCON
    this.o_MMSCOCL1 = this.w_MMSCOCL1
    this.o_MMSCOCL2 = this.w_MMSCOCL2
    this.o_MMSCOPAG = this.w_MMSCOPAG
    this.o_MMCODVAL = this.w_MMCODVAL
    this.o_MMCAOVAL = this.w_MMCAOVAL
    this.o_MMTCOLIS = this.w_MMTCOLIS
    this.o_MMCODICE = this.w_MMCODICE
    this.o_MMCODMAG = this.w_MMCODMAG
    this.o_MMCODMAT = this.w_MMCODMAT
    this.o_CPROWORD = this.w_CPROWORD
    this.o_MMCODLIS = this.w_MMCODLIS
    this.o_MMKEYSAL = this.w_MMKEYSAL
    this.o_LIPREZZO = this.w_LIPREZZO
    this.o_MMUNIMIS = this.w_MMUNIMIS
    this.o_MMQTAMOV = this.w_MMQTAMOV
    this.o_MMCODLOT = this.w_MMCODLOT
    this.o_CAMBIOLOT = this.w_CAMBIOLOT
    this.o_MMCODUBI = this.w_MMCODUBI
    this.o_MMCODUB2 = this.w_MMCODUB2
    this.o_MMQTAUM1 = this.w_MMQTAUM1
    this.o_MMPREZZO = this.w_MMPREZZO
    this.o_MMSCONT1 = this.w_MMSCONT1
    this.o_MMSCONT2 = this.w_MMSCONT2
    this.o_MMSCONT3 = this.w_MMSCONT3
    this.o_MMSCONT4 = this.w_MMSCONT4
    this.o_VALUNI = this.w_VALUNI
    this.o_MMVALMAG = this.w_MMVALMAG
    this.o_RICTOT = this.w_RICTOT
    this.o_MMLOTMAT = this.w_MMLOTMAT
    * --- GSVE_MMT : Depends On
    this.GSVE_MMT.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 AND NOT EMPTY(t_MMCODICE))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MMCODICE=space(20)
      .w_MMCODART=space(20)
      .w_MAGPRE=space(5)
      .w_MMCODMAG=space(5)
      .w_FLUBIC=space(1)
      .w_MMCODMAT=space(5)
      .w_F2UBIC=space(1)
      .w_MMNUMRIF=0
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_UNMIS3=space(3)
      .w_OPERA3=space(1)
      .w_MOLTI3=0
      .w_TIPRIG=space(1)
      .w_FLUSEP=space(1)
      .w_UNMIS1=space(3)
      .w_GESMAT=space(1)
      .w_MOLTIP=0
      .w_OPERAT=space(1)
      .w_FLLOTT=space(1)
      .w_CODIVA=space(5)
      .w_PERIVA=0
      .w_INDIVA=0
      .w_MMCODLIS=space(5)
      .w_CATSCA=space(5)
      .w_ARTDIS=space(1)
      .w_FLDISP=space(1)
      .w_MMCAUMAG=space(5)
      .w_MMCAUCOL=space(5)
      .w_MMFLCASC=space(1)
      .w_MMFLORDI=space(1)
      .w_MMFLIMPE=space(1)
      .w_MMFLRISE=space(1)
      .w_MMFLELGM=space(1)
      .w_MMF2CASC=space(1)
      .w_MMF2ORDI=space(1)
      .w_MMF2IMPE=space(1)
      .w_MMF2RISE=space(1)
      .w_MMCODCOM=space(15)
      .w_MMFLOMAG=space(1)
      .w_MMKEYSAL=space(20)
      .w_MMFLLOTT=space(1)
      .w_MMF2LOTT=space(1)
      .w_LIPREZZO=0
      .w_DESART=space(40)
      .w_ACTSCOR=.f.
      .w_MMUNIMIS=space(3)
      .w_FLFRAZ=space(1)
      .w_MMQTAMOV=0
      .w_OQTA=0
      .w_MMCODLOT=space(20)
      .w_CAMBIOLOT=.f.
      .w_STATEROW=space(1)
      .w_MMCODUBI=space(20)
      .w_MMCODUB2=space(20)
      .w_MMQTAUM1=0
      .w_MMPREZZO=0
      .w_PREUM1=0
      .w_MMSCONT1=0
      .w_MMSCONT2=0
      .w_MMSCONT3=0
      .w_MMSCONT4=0
      .w_VALUNI=0
      .w_VALUNI2=0
      .w_FLAVA1=space(1)
      .w_MMVALMAG=0
      .w_MMIMPNAZ=0
      .w_TIPART=space(2)
      .w_MMTIPATT=space(1)
      .w_MMCODATT=space(15)
      .w_MMIMPCOM=0
      .w_CODCOS=space(5)
      .w_MMFLORCO=space(1)
      .w_MMFLCOCO=space(1)
      .w_MMCODCOS=space(5)
      .w_FLCOMM=space(1)
      .w_ULTCAR=ctod("  /  /  ")
      .w_ULTSCA=ctod("  /  /  ")
      .w_MMFLULCA=space(1)
      .w_MMFLULPV=space(1)
      .w_MMVALULT=0
      .w_MMCODMAG=space(5)
      .w_MMCODMAT=space(5)
      .w_QTAPER=0
      .w_QTRPER=0
      .w_QTDISC=0
      .w_Q2APER=0
      .w_Q2RPER=0
      .w_QTDISP=0
      .w_Q2DISP=0
      .w_FM1=space(1)
      .w_FM2=space(1)
      .w_VISNAZ=0
      .w_FLSTAT=space(1)
      .w_LOTZOOM=.f.
      .w_CODCON=space(15)
      .w_OLDQTA=0
      .w_UBIZOOM=.f.
      .w_OBSMAG=ctod("  /  /  ")
      .w_OBSMAT=ctod("  /  /  ")
      .w_IVASCOR=0
      .w_TOTMAT=0
      .w_MTCARI=space(1)
      .w_DATLOT=ctod("  /  /  ")
      .w_DISLOT=space(1)
      .w_UNMIS2=space(3)
      .w_QTAUM2=0
      .w_UNIMIS2=space(10)
      .w_NOFRAZ1=space(1)
      .w_MODUM2=space(1)
      .w_RICTOT=.f.
      .w_QTOPER=0
      .w_QTIPER=0
      .w_MMLOTMAG=space(5)
      .w_MMLOTMAT=space(5)
      .w_PREZUM=space(1)
      .w_CLUNIMIS=space(3)
      .w_FLCOM1=space(1)
      .w_ARCLAMAT=space(5)
      .DoRTCalc(1,55,.f.)
      if not(empty(.w_MMCODICE))
        .link_2_1('Full')
      endif
      .DoRTCalc(56,56,.f.)
      if not(empty(.w_MMCODART))
        .link_2_2('Full')
      endif
      .DoRTCalc(57,57,.f.)
        .w_MMCODMAG = IIF(NOT EMPTY(IIF(.w_FLLOTT<>'C',SPACE(5),.w_MMCODMAG)),IIF(.w_FLLOTT<>'C',SPACE(5),.w_MMCODMAG),IIF(NOT EMPTY(.w_MAGPRE) , .w_MAGPRE, .w_OMAG))
      .DoRTCalc(58,58,.f.)
      if not(empty(.w_MMCODMAG))
        .link_2_4('Full')
      endif
      .DoRTCalc(59,59,.f.)
        .w_MMCODMAT = .w_OMAT
      .DoRTCalc(60,60,.f.)
      if not(empty(.w_MMCODMAT))
        .link_2_6('Full')
      endif
      .DoRTCalc(61,61,.f.)
        .w_MMNUMRIF = -10
      .DoRTCalc(63,66,.f.)
        .w_TIPRIG = IIF(.w_TIPART $ 'AC-FO-FM-DE', 'X', 'R')
      .DoRTCalc(68,69,.f.)
      if not(empty(.w_UNMIS1))
        .link_2_15('Full')
      endif
      .DoRTCalc(70,74,.f.)
      if not(empty(.w_CODIVA))
        .link_2_20('Full')
      endif
      .DoRTCalc(75,76,.f.)
        .w_MMCODLIS = .w_MMTCOLIS
      .DoRTCalc(78,79,.f.)
        .w_FLDISP = IIF(g_PERDIS='S', .w_ARTDIS, ' ')
        .w_MMCAUMAG = .w_MMTCAMAG
      .DoRTCalc(81,81,.f.)
      if not(empty(.w_MMCAUMAG))
        .link_2_27('Full')
      endif
      .DoRTCalc(82,82,.f.)
      if not(empty(.w_MMCAUCOL))
        .link_2_28('Full')
      endif
      .DoRTCalc(83,92,.f.)
      if not(empty(.w_MMCODCOM))
        .link_2_38('Full')
      endif
        .w_MMFLOMAG = 'X'
        .w_MMKEYSAL = .w_MMCODART
        .w_MMFLLOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_FLUBIC='S'), LEFT(ALLTRIM(.w_MMFLCASC)+IIF(.w_MMFLRISE='+', '-', IIF(.w_MMFLRISE='-', '+', ' ')), 1), ' ')
        .w_MMF2LOTT = IIF((g_PERLOT='S' AND .w_FLLOTT$ 'SC') OR (g_PERUBI='S' AND .w_F2UBIC='S'), LEFT(ALLTRIM(.w_MMF2CASC)+IIF(.w_MMF2RISE='+', '-', IIF(.w_MMF2RISE='-', '+', ' ')), 1), ' ')
      .DoRTCalc(97,98,.f.)
        .w_ACTSCOR = .F.
        .w_MMUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
      .DoRTCalc(100,100,.f.)
      if not(empty(.w_MMUNIMIS))
        .link_2_46('Full')
      endif
      .DoRTCalc(101,102,.f.)
        .w_OQTA = .w_MMQTAMOV
      .DoRTCalc(104,104,.f.)
      if not(empty(.w_MMCODLOT))
        .link_2_50('Full')
      endif
        .w_CAMBIOLOT = .F.
        .w_STATEROW = IIF(this.rowstatus()$'AU' AND (.w_CAMBIOLOT or .w_MMQTAUM1>.w_OLDQTA) ,'S','N')
      .DoRTCalc(107,107,.f.)
      if not(empty(.w_MMCODUBI))
        .link_2_55('Full')
      endif
        .w_MMCODUB2 = SPACE(20)
      .DoRTCalc(108,108,.f.)
      if not(empty(.w_MMCODUB2))
        .link_2_56('Full')
      endif
        .w_MMQTAUM1 = CALQTA(.w_MMQTAMOV,.w_MMUNIMIS,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_NOFRAZ1, .w_MODUM2, '', .w_UNMIS3, .w_OPERA3, .w_MOLTI3,,'I')
        .w_MMPREZZO = cp_Round(CALMMLIS(.w_LIPREZZO, .w_UNMIS1+.w_UNMIS2+.w_UNMIS3+.w_MMUNIMIS+.w_OPERAT+.w_OPERA3+.w_IVALIS+'P'+ALLTRIM(STR(.w_DECUNI)), IIF(EMPTY(.w_CLUNIMIS), .w_MOLTIP,1), IIF(EMPTY(.w_CLUNIMIS),.w_MOLTI3,1) ,.w_PERIVA), .w_DECUNI)
        .w_PREUM1 = IIF(.w_MMQTAUM1=0, 0, cp_ROUND((.w_MMPREZZO * .w_MMQTAMOV) / .w_MMQTAUM1, .w_DECUNI))
      .DoRTCalc(112,112,.f.)
        .w_MMSCONT2 = 0
        .w_MMSCONT3 = 0
        .w_MMSCONT4 = 0
        .w_VALUNI = cp_ROUND(.w_MMPREZZO * (1+.w_MMSCONT1/100)*(1+.w_MMSCONT2/100)*(1+.w_MMSCONT3/100)*(1+.w_MMSCONT4/100),5)
        .w_VALUNI2 = cp_ROUND (.w_VALUNI * (1+.w_MMSCOCL1/100)*(1+.w_MMSCOCL2/100)*(1+.w_MMSCOPAG/100),5)
      .DoRTCalc(118,118,.f.)
        .w_MMVALMAG = cp_ROUND(.w_MMQTAMOV*.w_VALUNI2, .w_DECTOT)
        .w_MMIMPNAZ = CALCNAZ(.w_MMVALMAG,.w_MMCAOVAL,.w_CAONAZ,IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC),.w_MMVALNAZ,.w_MMCODVAL,IIF(.w_FLAVA1='A',(.w_PERIVA*.w_INDIVA),0))
      .DoRTCalc(121,121,.f.)
        .w_MMTIPATT = 'A'
      .DoRTCalc(123,131,.f.)
        .w_MMFLULCA = IIF(.w_FLAVA1='A' AND .w_MMFLCASC $ '+-' AND .w_MMDATREG>=.w_ULTCAR AND .w_MMFLOMAG<>'S' And .w_MMPREZZO<>0, '=', ' ')
        .w_MMFLULPV = IIF(.w_FLAVA1='V' AND .w_MMFLCASC $ '+-' AND .w_MMDATREG>=.w_ULTSCA AND .w_MMFLOMAG<>'S' And .w_MMPREZZO<>0, '=', ' ')
        .w_MMVALULT = IIF(.w_MMQTAUM1=0, 0, cp_ROUND(.w_MMIMPNAZ/.w_MMQTAUM1, g_PERPUL))
      .DoRTCalc(135,135,.f.)
      if not(empty(.w_MMCODMAG))
        .link_2_84('Full')
      endif
      .DoRTCalc(136,136,.f.)
      if not(empty(.w_MMCODMAT))
        .link_2_85('Full')
      endif
      .DoRTCalc(137,138,.f.)
        .w_QTDISC = .w_QTAPER-.w_QTRPER + .w_QTOPER - .w_QTIPER
      .DoRTCalc(140,141,.f.)
        .w_QTDISP = .w_QTAPER-.w_QTRPER
        .w_Q2DISP = .w_Q2APER-.w_Q2RPER
        .w_FM1 = IIF(.cFunction='Load', 'S', ' ')
        .w_FM2 = IIF(.cFunction='Load', 'S',' ')
      .DoRTCalc(146,148,.f.)
        .w_VISNAZ = cp_ROUND(.w_MMIMPNAZ, .w_DECTOP)
      .DoRTCalc(150,154,.f.)
        .w_LOTZOOM = .T.
      .DoRTCalc(156,156,.f.)
        .w_CODCON = IIF(.w_MMTIPCON='F' And Empty(.w_MMCODMAT), .w_MMCODCON, SPACE(15))
        .w_OLDQTA = .w_MMQTAUM1
        .w_UBIZOOM = .T.
      .DoRTCalc(160,165,.f.)
        .w_TOTMAT = -1
      .DoRTCalc(167,178,.f.)
        .w_QTAUM2 = Caunmis2('R', .w_MMQTAMOV, .w_UNMIS1, IIF(Not Empty(.w_UNMIS3), .w_UNMIS3, .w_UNMIS2), .w_MMUNIMIS, IIF(Not Empty(.w_UNMIS3), .w_OPERA3, .w_OPERAT), IIF(Not Empty(.w_UNMIS3), .w_MOLTI3, .w_MOLTIP))
        .oPgFrm.Page1.oPag.oObj_2_114.Calculate(IIF(.w_MMUNIMIS=.w_UNMIS1 OR EMPTY(.w_MMCODICE),IIF(Not Empty(.w_UNMIS3), Ah_MsgFormat("Qta nella 3^UM:"),IIF(Not Empty(.w_UNMIS2), Ah_MsgFormat("Qta nella 2^UM:"),'')),IIF(.w_MMUNIMIS<>.w_UNMIS1 And .w_MMUNIMIS<>.w_UNMIS2 AND .w_MMUNIMIS<>.w_UNMIS3,"", Ah_MsgFormat("Qta e prezzo nella 1^UM:"))))
        .w_UNIMIS2 = IIF(Not Empty(.w_UNMIS3), .w_UNMIS3, .w_UNMIS2)
      .DoRTCalc(181,182,.f.)
        .w_RICTOT = .F.
      .DoRTCalc(184,185,.f.)
        .w_MMLOTMAG = iif( Empty( .w_MMCODLOT ) And Empty( .w_MMCODUBI ) , SPACE(5) , .w_MMCODMAG )
      .DoRTCalc(186,186,.f.)
      if not(empty(.w_MMLOTMAG))
        .link_2_121('Full')
      endif
        .w_MMLOTMAT = iif( Empty( .w_MMCODLOT ) And Empty( .w_MMCODUB2 ) , SPACE(5) , .w_MMCODMAT )
      .DoRTCalc(187,187,.f.)
      if not(empty(.w_MMLOTMAT))
        .link_2_122('Full')
      endif
        .oPgFrm.Page1.oPag.oObj_2_127.Calculate(Ah_MsgFormat("Commessa:"))
    endwith
    this.DoRTCalc(188,193,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MMCODICE = t_MMCODICE
    this.w_MMCODART = t_MMCODART
    this.w_MAGPRE = t_MAGPRE
    this.w_MMCODMAG = t_MMCODMAG
    this.w_FLUBIC = t_FLUBIC
    this.w_MMCODMAT = t_MMCODMAT
    this.w_F2UBIC = t_F2UBIC
    this.w_MMNUMRIF = t_MMNUMRIF
    this.w_CPROWORD = t_CPROWORD
    this.w_UNMIS3 = t_UNMIS3
    this.w_OPERA3 = t_OPERA3
    this.w_MOLTI3 = t_MOLTI3
    this.w_TIPRIG = t_TIPRIG
    this.w_FLUSEP = t_FLUSEP
    this.w_UNMIS1 = t_UNMIS1
    this.w_GESMAT = t_GESMAT
    this.w_MOLTIP = t_MOLTIP
    this.w_OPERAT = t_OPERAT
    this.w_FLLOTT = t_FLLOTT
    this.w_CODIVA = t_CODIVA
    this.w_PERIVA = t_PERIVA
    this.w_INDIVA = t_INDIVA
    this.w_MMCODLIS = t_MMCODLIS
    this.w_CATSCA = t_CATSCA
    this.w_ARTDIS = t_ARTDIS
    this.w_FLDISP = t_FLDISP
    this.w_MMCAUMAG = t_MMCAUMAG
    this.w_MMCAUCOL = t_MMCAUCOL
    this.w_MMFLCASC = t_MMFLCASC
    this.w_MMFLORDI = t_MMFLORDI
    this.w_MMFLIMPE = t_MMFLIMPE
    this.w_MMFLRISE = t_MMFLRISE
    this.w_MMFLELGM = this.oPgFrm.Page1.oPag.oMMFLELGM_2_33.RadioValue(.t.)
    this.w_MMF2CASC = t_MMF2CASC
    this.w_MMF2ORDI = t_MMF2ORDI
    this.w_MMF2IMPE = t_MMF2IMPE
    this.w_MMF2RISE = t_MMF2RISE
    this.w_MMCODCOM = t_MMCODCOM
    this.w_MMFLOMAG = this.oPgFrm.Page1.oPag.oMMFLOMAG_2_39.RadioValue(.t.)
    this.w_MMKEYSAL = t_MMKEYSAL
    this.w_MMFLLOTT = t_MMFLLOTT
    this.w_MMF2LOTT = t_MMF2LOTT
    this.w_LIPREZZO = t_LIPREZZO
    this.w_DESART = t_DESART
    this.w_ACTSCOR = t_ACTSCOR
    this.w_MMUNIMIS = t_MMUNIMIS
    this.w_FLFRAZ = t_FLFRAZ
    this.w_MMQTAMOV = t_MMQTAMOV
    this.w_OQTA = t_OQTA
    this.w_MMCODLOT = t_MMCODLOT
    this.w_CAMBIOLOT = t_CAMBIOLOT
    this.w_STATEROW = t_STATEROW
    this.w_MMCODUBI = t_MMCODUBI
    this.w_MMCODUB2 = t_MMCODUB2
    this.w_MMQTAUM1 = t_MMQTAUM1
    this.w_MMPREZZO = t_MMPREZZO
    this.w_PREUM1 = t_PREUM1
    this.w_MMSCONT1 = t_MMSCONT1
    this.w_MMSCONT2 = t_MMSCONT2
    this.w_MMSCONT3 = t_MMSCONT3
    this.w_MMSCONT4 = t_MMSCONT4
    this.w_VALUNI = t_VALUNI
    this.w_VALUNI2 = t_VALUNI2
    this.w_FLAVA1 = t_FLAVA1
    this.w_MMVALMAG = t_MMVALMAG
    this.w_MMIMPNAZ = t_MMIMPNAZ
    this.w_TIPART = t_TIPART
    this.w_MMTIPATT = t_MMTIPATT
    this.w_MMCODATT = t_MMCODATT
    this.w_MMIMPCOM = t_MMIMPCOM
    this.w_CODCOS = t_CODCOS
    this.w_MMFLORCO = t_MMFLORCO
    this.w_MMFLCOCO = t_MMFLCOCO
    this.w_MMCODCOS = t_MMCODCOS
    this.w_FLCOMM = t_FLCOMM
    this.w_ULTCAR = t_ULTCAR
    this.w_ULTSCA = t_ULTSCA
    this.w_MMFLULCA = t_MMFLULCA
    this.w_MMFLULPV = t_MMFLULPV
    this.w_MMVALULT = t_MMVALULT
    this.w_MMCODMAG = t_MMCODMAG
    this.w_MMCODMAT = t_MMCODMAT
    this.w_QTAPER = t_QTAPER
    this.w_QTRPER = t_QTRPER
    this.w_QTDISC = t_QTDISC
    this.w_Q2APER = t_Q2APER
    this.w_Q2RPER = t_Q2RPER
    this.w_QTDISP = t_QTDISP
    this.w_Q2DISP = t_Q2DISP
    this.w_FM1 = t_FM1
    this.w_FM2 = t_FM2
    this.w_VISNAZ = t_VISNAZ
    this.w_FLSTAT = t_FLSTAT
    this.w_LOTZOOM = t_LOTZOOM
    this.w_CODCON = t_CODCON
    this.w_OLDQTA = t_OLDQTA
    this.w_UBIZOOM = t_UBIZOOM
    this.w_OBSMAG = t_OBSMAG
    this.w_OBSMAT = t_OBSMAT
    this.w_IVASCOR = t_IVASCOR
    this.w_TOTMAT = t_TOTMAT
    this.w_MTCARI = t_MTCARI
    this.w_DATLOT = t_DATLOT
    this.w_DISLOT = t_DISLOT
    this.w_UNMIS2 = t_UNMIS2
    this.w_QTAUM2 = t_QTAUM2
    this.w_UNIMIS2 = t_UNIMIS2
    this.w_NOFRAZ1 = t_NOFRAZ1
    this.w_MODUM2 = t_MODUM2
    this.w_RICTOT = t_RICTOT
    this.w_QTOPER = t_QTOPER
    this.w_QTIPER = t_QTIPER
    this.w_MMLOTMAG = t_MMLOTMAG
    this.w_MMLOTMAT = t_MMLOTMAT
    this.w_PREZUM = t_PREZUM
    this.w_CLUNIMIS = t_CLUNIMIS
    this.w_FLCOM1 = t_FLCOM1
    this.w_ARCLAMAT = t_ARCLAMAT
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MMCODICE with this.w_MMCODICE
    replace t_MMCODART with this.w_MMCODART
    replace t_MAGPRE with this.w_MAGPRE
    replace t_MMCODMAG with this.w_MMCODMAG
    replace t_FLUBIC with this.w_FLUBIC
    replace t_MMCODMAT with this.w_MMCODMAT
    replace t_F2UBIC with this.w_F2UBIC
    replace t_MMNUMRIF with this.w_MMNUMRIF
    replace t_CPROWORD with this.w_CPROWORD
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_OPERA3 with this.w_OPERA3
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_TIPRIG with this.w_TIPRIG
    replace t_FLUSEP with this.w_FLUSEP
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_GESMAT with this.w_GESMAT
    replace t_MOLTIP with this.w_MOLTIP
    replace t_OPERAT with this.w_OPERAT
    replace t_FLLOTT with this.w_FLLOTT
    replace t_CODIVA with this.w_CODIVA
    replace t_PERIVA with this.w_PERIVA
    replace t_INDIVA with this.w_INDIVA
    replace t_MMCODLIS with this.w_MMCODLIS
    replace t_CATSCA with this.w_CATSCA
    replace t_ARTDIS with this.w_ARTDIS
    replace t_FLDISP with this.w_FLDISP
    replace t_MMCAUMAG with this.w_MMCAUMAG
    replace t_MMCAUCOL with this.w_MMCAUCOL
    replace t_MMFLCASC with this.w_MMFLCASC
    replace t_MMFLORDI with this.w_MMFLORDI
    replace t_MMFLIMPE with this.w_MMFLIMPE
    replace t_MMFLRISE with this.w_MMFLRISE
    replace t_MMFLELGM with this.oPgFrm.Page1.oPag.oMMFLELGM_2_33.ToRadio()
    replace t_MMF2CASC with this.w_MMF2CASC
    replace t_MMF2ORDI with this.w_MMF2ORDI
    replace t_MMF2IMPE with this.w_MMF2IMPE
    replace t_MMF2RISE with this.w_MMF2RISE
    replace t_MMCODCOM with this.w_MMCODCOM
    replace t_MMFLOMAG with this.oPgFrm.Page1.oPag.oMMFLOMAG_2_39.ToRadio()
    replace t_MMKEYSAL with this.w_MMKEYSAL
    replace t_MMFLLOTT with this.w_MMFLLOTT
    replace t_MMF2LOTT with this.w_MMF2LOTT
    replace t_LIPREZZO with this.w_LIPREZZO
    replace t_DESART with this.w_DESART
    replace t_ACTSCOR with this.w_ACTSCOR
    replace t_MMUNIMIS with this.w_MMUNIMIS
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_MMQTAMOV with this.w_MMQTAMOV
    replace t_OQTA with this.w_OQTA
    replace t_MMCODLOT with this.w_MMCODLOT
    replace t_CAMBIOLOT with this.w_CAMBIOLOT
    replace t_STATEROW with this.w_STATEROW
    replace t_MMCODUBI with this.w_MMCODUBI
    replace t_MMCODUB2 with this.w_MMCODUB2
    replace t_MMQTAUM1 with this.w_MMQTAUM1
    replace t_MMPREZZO with this.w_MMPREZZO
    replace t_PREUM1 with this.w_PREUM1
    replace t_MMSCONT1 with this.w_MMSCONT1
    replace t_MMSCONT2 with this.w_MMSCONT2
    replace t_MMSCONT3 with this.w_MMSCONT3
    replace t_MMSCONT4 with this.w_MMSCONT4
    replace t_VALUNI with this.w_VALUNI
    replace t_VALUNI2 with this.w_VALUNI2
    replace t_FLAVA1 with this.w_FLAVA1
    replace t_MMVALMAG with this.w_MMVALMAG
    replace t_MMIMPNAZ with this.w_MMIMPNAZ
    replace t_TIPART with this.w_TIPART
    replace t_MMTIPATT with this.w_MMTIPATT
    replace t_MMCODATT with this.w_MMCODATT
    replace t_MMIMPCOM with this.w_MMIMPCOM
    replace t_CODCOS with this.w_CODCOS
    replace t_MMFLORCO with this.w_MMFLORCO
    replace t_MMFLCOCO with this.w_MMFLCOCO
    replace t_MMCODCOS with this.w_MMCODCOS
    replace t_FLCOMM with this.w_FLCOMM
    replace t_ULTCAR with this.w_ULTCAR
    replace t_ULTSCA with this.w_ULTSCA
    replace t_MMFLULCA with this.w_MMFLULCA
    replace t_MMFLULPV with this.w_MMFLULPV
    replace t_MMVALULT with this.w_MMVALULT
    replace t_MMCODMAG with this.w_MMCODMAG
    replace t_MMCODMAT with this.w_MMCODMAT
    replace t_QTAPER with this.w_QTAPER
    replace t_QTRPER with this.w_QTRPER
    replace t_QTDISC with this.w_QTDISC
    replace t_Q2APER with this.w_Q2APER
    replace t_Q2RPER with this.w_Q2RPER
    replace t_QTDISP with this.w_QTDISP
    replace t_Q2DISP with this.w_Q2DISP
    replace t_FM1 with this.w_FM1
    replace t_FM2 with this.w_FM2
    replace t_VISNAZ with this.w_VISNAZ
    replace t_FLSTAT with this.w_FLSTAT
    replace t_LOTZOOM with this.w_LOTZOOM
    replace t_CODCON with this.w_CODCON
    replace t_OLDQTA with this.w_OLDQTA
    replace t_UBIZOOM with this.w_UBIZOOM
    replace t_OBSMAG with this.w_OBSMAG
    replace t_OBSMAT with this.w_OBSMAT
    replace t_IVASCOR with this.w_IVASCOR
    replace t_TOTMAT with this.w_TOTMAT
    replace t_MTCARI with this.w_MTCARI
    replace t_DATLOT with this.w_DATLOT
    replace t_DISLOT with this.w_DISLOT
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_QTAUM2 with this.w_QTAUM2
    replace t_UNIMIS2 with this.w_UNIMIS2
    replace t_NOFRAZ1 with this.w_NOFRAZ1
    replace t_MODUM2 with this.w_MODUM2
    replace t_RICTOT with this.w_RICTOT
    replace t_QTOPER with this.w_QTOPER
    replace t_QTIPER with this.w_QTIPER
    replace t_MMLOTMAG with this.w_MMLOTMAG
    replace t_MMLOTMAT with this.w_MMLOTMAT
    replace t_PREZUM with this.w_PREZUM
    replace t_CLUNIMIS with this.w_CLUNIMIS
    replace t_FLCOM1 with this.w_FLCOM1
    replace t_ARCLAMAT with this.w_ARCLAMAT
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTALE = .w_TOTALE-.w_mmvalmag
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsma_mvmPag1 as StdContainer
  Width  = 790
  height = 496
  stdWidth  = 790
  stdheight = 496
  resizeXpos=242
  resizeYpos=263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMMNUMREG_1_3 as StdField with uid="GEOTHPTMPI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MMNUMREG", cQueryName = "MMNUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 195053811,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=82, Top=9, cSayPict='"999999"', cGetPict='"999999"'

  proc oMMNUMREG_1_3.mBefore
    with this.Parent.oContained
      this.cQueryName = "MMCODESE,MMNUMREG"
    endwith
  endproc

  add object oMMCODUTE_1_4 as StdField with uid="RSFSIXDJEV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MMCODUTE", cQueryName = "MMCODUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di serie della registrazione (legato all'utente)",;
    HelpContextID = 154597621,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=157, Top=9, cSayPict='"9999"', cGetPict='"9999"'

  add object oMMDATREG_1_5 as StdField with uid="IDPDQCVNSS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MMDATREG", cQueryName = "MMDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione",;
    HelpContextID = 189065459,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=275, Top=9

  add object oMMCODESE_1_7 as StdField with uid="MWDUYYXNTZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MMCODESE", cQueryName = "MMCODESE",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio",;
    HelpContextID = 154597621,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=367, Top=9, InputMask=replicate('X',4), tabstop=.f., cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZICOD", oKey_2_1="ESCODESE", oKey_2_2="this.w_MMCODESE"

  func oMMCODESE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMMTCAMAG_1_14 as StdField with uid="KQLJXQWTZJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MMTCAMAG", cQueryName = "MMTCAMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente, a variazione di valore oppure obsoleta",;
    ToolTipText = "Codice della causale magazzino",;
    HelpContextID = 24242419,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=82, Top=33, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_MMTCAMAG"

  func oMMTCAMAG_1_14.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
  endfunc

  func oMMTCAMAG_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMMTCAMAG_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMMTCAMAG_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oMMTCAMAG_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'GSMA_MVM.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oMMTCAMAG_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_MMTCAMAG
    i_obj.ecpSave()
  endproc

  add object oMMNUMDOC_1_15 as StdField with uid="XGRLTPSFEZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MMNUMDOC", cQueryName = "MMNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero documento obbligatorio",;
    ToolTipText = "Numero del documento",;
    HelpContextID = 161499383,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=560, Top=9, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oMMNUMDOC_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MMNUMDOC<>0 OR .w_FLNDOC<>'O')
    endwith
    return bRes
  endfunc

  add object oMMALFDOC_1_16 as StdField with uid="IXUBMVPGMX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MMALFDOC", cQueryName = "MMNUMDOC,MMALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Parte alfanumerica del documento",;
    HelpContextID = 169482487,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=689, Top=9, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  add object oMMDATDOC_1_17 as StdField with uid="QAATDVMZFI",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MMDATDOC", cQueryName = "MMDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data documento deve essere minore o uguale alla data registrazione",;
    ToolTipText = "Data del documento",;
    HelpContextID = 155511031,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=689, Top=33

  func oMMDATDOC_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((NOT EMPTY(.w_MMDATDOC) OR .w_FLNDOC<>'O') and .w_MMDATDOC<=.w_MMDATREG)
    endwith
    return bRes
  endfunc

  add object oDESCAU_1_19 as StdField with uid="UTOOGLUZKC",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 158466506,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=157, Top=33, InputMask=replicate('X',35)

  add object oMMCODCON_1_20 as StdField with uid="NPTFABTWNL",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MMCODCON", cQueryName = "MMCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Cliente/fornitore intestatario del documento",;
    HelpContextID = 188152044,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=82, Top=59, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MMTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_MMCODCON"

  func oMMCODCON_1_20.mCond()
    with this.Parent.oContained
      return (.w_MMTIPCON$"CF" AND .cFunction<>'Edit')
    endwith
  endfunc

  func oMMCODCON_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oMMCODCON_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMMCODCON_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MMTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MMTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oMMCODCON_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti/fornitori",'GSCG1MPN.CONTI_VZM',this.parent.oContained
  endproc
  proc oMMCODCON_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MMTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_MMCODCON
    i_obj.ecpSave()
  endproc

  add object oDESCLF_1_21 as StdField with uid="EGBZXCFNCC",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 130154954,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=268, Left=218, Top=59, InputMask=replicate('X',40)

  add object oMMDESSUP_1_23 as StdField with uid="KWNGJKKOIQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MMDESSUP", cQueryName = "MMDESSUP",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva dell'operazione",;
    HelpContextID = 173074666,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=282, Left=490, Top=59, InputMask=replicate('X',40), TabStop=.F.


  add object oObj_1_24 as cp_calclbl with uid="PZXIGVNVGJ",left=1, top=59, width=77,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 137262106

  add object oMMSCOCL1_1_25 as StdField with uid="BGTIPXMZWN",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MMSCOCL1", cQueryName = "MMSCOCL1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "% Maggiorazione (se positiva) o sconto (se negativa) applicata al cli/for",;
    HelpContextID = 91096823,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=134, Top=421, cSayPict='"999.99"', cGetPict='"999.99"', TabStop=.f.

  func oMMSCOCL1_1_25.mCond()
    with this.Parent.oContained
      return (.w_MMFLCLFR $ 'CF')
    endwith
  endfunc

  add object oMMSCOCL2_1_26 as StdField with uid="RCUZRILZTE",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MMSCOCL2", cQueryName = "MMSCOCL2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "2^% Maggiorazione (se positiva) o sconto (se negativa) applicata al cli/for",;
    HelpContextID = 91096824,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=198, Top=421, cSayPict='"999.99"', cGetPict='"999.99"', tabstop=.f.

  func oMMSCOCL2_1_26.mCond()
    with this.Parent.oContained
      return (.w_MMSCOCL1<>0 AND .w_MMFLCLFR $ 'CF')
    endwith
  endfunc

  add object oMMSCOPAG_1_27 as StdField with uid="JIFPQGHIGQ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MMSCOPAG", cQueryName = "MMSCOPAG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "% Maggiorazione (se positiva) o sconto (se negativa) applicata al pagamento",;
    HelpContextID = 227670259,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=262, Top=421, cSayPict='"999.99"', cGetPict='"999.99"', TabStop=.f.

  add object oMMCODVAL_1_30 as StdField with uid="YVUSDELBPZ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MMCODVAL", cQueryName = "MMCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o obsoleta o valuta esercizio non definita",;
    ToolTipText = "Codice valuta della registrazione",;
    HelpContextID = 137820398,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=82, Top=86, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_MMCODVAL"

  func oMMCODVAL_1_30.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
  endfunc

  func oMMCODVAL_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oMMCODVAL_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMMCODVAL_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oMMCODVAL_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oMMCODVAL_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_MMCODVAL
    i_obj.ecpSave()
  endproc

  add object oSIMVAL_1_32 as StdField with uid="FOSNDQRTWK",rtseq=31,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 39804122,;
   bGlobalFont=.t.,;
    Height=21, Width=45, Left=140, Top=86, InputMask=replicate('X',5)

  add object oMMCAOVAL_1_36 as StdField with uid="LLRKFUZLKX",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MMCAOVAL", cQueryName = "MMCAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio della registrazione",;
    HelpContextID = 127203566,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=271, Top=86, cSayPict='"99999.999999"', cGetPict='"99999.999999"', tabstop=.f.

  func oMMCAOVAL_1_36.mCond()
    with this.Parent.oContained
      return (GETVALUT(g_PERVAL, 'VADATEUR')>=IIF(EMPTY(.w_MMDATDOC), .w_MMDATREG, .w_MMDATDOC) OR .w_CAOVAL=0)
    endwith
  endfunc

  add object oMMTCOLIS_1_37 as StdField with uid="WZHMCAHRRV",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MMTCOLIS", cQueryName = "MMTCOLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino inesistente, obsoleto non valido oppure valuta listino incongruente",;
    ToolTipText = "Codice listino legato all'articolo",;
    HelpContextID = 242095897,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=494, Top=86, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_MMTCOLIS"

  func oMMTCOLIS_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oMMTCOLIS_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMMTCOLIS_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oMMTCOLIS_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'gsma1mvm.LISTINI_VZM',this.parent.oContained
  endproc
  proc oMMTCOLIS_1_37.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_MMTCOLIS
    i_obj.ecpSave()
  endproc


  add object oObj_1_76 as cp_runprogram with uid="HAJAZRISUP",left=369, top=506, width=400,height=18,;
    caption='GSMA_BES(S)',;
   bGlobalFont=.t.,;
    prg="GSMA_BES('S')",;
    cEvent = "w_MMSCOCL1 Changed,w_MMSCOCL2 Changed,w_MMSCOPAG Changed",;
    nPag=1;
    , HelpContextID = 177302983


  add object oObj_1_77 as cp_runprogram with uid="FJBVKFQBNZ",left=369, top=523, width=322,height=20,;
    caption='GSMA_BM2',;
   bGlobalFont=.t.,;
    prg="GSMA_BM2",;
    cEvent = "w_MMTCOLIS Changed,w_MMCAOVAL Changed",;
    nPag=1;
    , HelpContextID = 90942616


  add object oObj_1_78 as cp_runprogram with uid="OTRODZVVQA",left=3, top=558, width=256,height=19,;
    caption='GSMA_BMK',;
   bGlobalFont=.t.,;
    prg="GSMA_BMK",;
    cEvent = "Delete start,ControlliFinali",;
    nPag=1;
    , HelpContextID = 90942641


  add object oObj_1_85 as cp_runprogram with uid="CFHMRERKJS",left=369, top=542, width=201,height=18,;
    caption='GSMA_BM3',;
   bGlobalFont=.t.,;
    prg="GSMA_BM3",;
    cEvent = "w_MMTCAMAG Changed",;
    nPag=1;
    , HelpContextID = 90942617


  add object oObj_1_88 as cp_runprogram with uid="JVWUYDIAQW",left=3, top=541, width=256,height=19,;
    caption='GSMA_BCD(P)',;
   bGlobalFont=.t.,;
    prg="GSMA_BCD('P')",;
    cEvent = "w_MMQTAUM1 Changed",;
    nPag=1;
    , HelpContextID = 177303766


  add object oObj_1_89 as cp_runprogram with uid="BIQMERNKHV",left=3, top=505, width=361,height=19,;
    caption='GSMA_BCD(A)',;
   bGlobalFont=.t.,;
    prg="GSMA_BCD('A')",;
    cEvent = "w_MMCODICE Changed,w_MMQTAMOV Changed, w_MMUNIMIS Changed",;
    nPag=1;
    , HelpContextID = 177307606


  add object oObj_1_90 as cp_runprogram with uid="RWGOKQOUKI",left=3, top=523, width=256,height=19,;
    caption='GSMA_BCD(R)',;
   bGlobalFont=.t.,;
    prg="GSMA_BCD('R')",;
    cEvent = "Ricalcola",;
    nPag=1;
    , HelpContextID = 177303254


  add object oObj_1_95 as cp_runprogram with uid="RSGQAJFLPW",left=369, top=559, width=224,height=18,;
    caption='GSMA_BIL(R)',;
   bGlobalFont=.t.,;
    prg="GSMA_BIL('R')",;
    cEvent = "w_MMPREZZO Changed",;
    nPag=1;
    , HelpContextID = 91132210


  add object oObj_1_96 as cp_runprogram with uid="MROKWQOXOT",left=369, top=576, width=224,height=18,;
    caption='GSMA_BIL(1)',;
   bGlobalFont=.t.,;
    prg="GSMA_BIL('1')",;
    cEvent = "w_PREUM1 Changed",;
    nPag=1;
    , HelpContextID = 91123762


  add object oBtn_1_98 as StdButton with uid="IYGKPXTGUG",left=723, top=83, width=48,height=45,;
    CpPicture="bmp\SCARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare automaticamente il documento da penna con memoria";
    , HelpContextID = 175407153;
    , TabStop=.f.,Caption='\<Car.Rap.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_98.Click()
      with this.Parent.oContained
        GSAR_MPG(thisform)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_98.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MMTCAMAG))
    endwith
  endfunc


  add object oObj_1_100 as cp_runprogram with uid="QMZPDGPXVN",left=3, top=576, width=256,height=19,;
    caption='GSMA_BES(D)',;
   bGlobalFont=.t.,;
    prg="GSMA_BES('D')",;
    cEvent = "w_MMDATREG Changed",;
    nPag=1;
    , HelpContextID = 177306823


  add object oObj_1_111 as cp_runprogram with uid="ACXDTYYRTP",left=3, top=594, width=256,height=19,;
    caption='GSMA_BES(C)',;
   bGlobalFont=.t.,;
    prg="GSMA_BES('C')",;
    cEvent = "w_MMCODCON Changed",;
    nPag=1;
    , HelpContextID = 177307079


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=129, width=772,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Riga",Field2="MMCODICE",Label2="Articolo",Field3="DESART",Label3="Descrizione",Field4="MMCODMAG",Label4="Magaz.",Field5="MMCODMAT",Label5="Mag. coll.",Field6="MMUNIMIS",Label6="U.M.",Field7="MMQTAMOV",Label7="Quantit�",Field8="MMPREZZO",Label8="Prezzo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 267604358


  add object oObj_1_118 as cp_runprogram with uid="EAYRFMFGVA",left=369, top=593, width=224,height=18,;
    caption='GSMA_BPD',;
   bGlobalFont=.t.,;
    prg="GSMA_BPD('S')",;
    cEvent = "Update row start,Delete row start,Insert row start",;
    nPag=1;
    , ToolTipText = "Aggiorna saldi per commessa";
    , HelpContextID = 177492822

  add object oStr_1_38 as StdString with uid="RIEIVZCDPP",Visible=.t., Left=237, Top=9,;
    Alignment=1, Width=36, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="CARVTZRREU",Visible=.t., Left=1, Top=8,;
    Alignment=1, Width=77, Height=15,;
    Caption="Reg. n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="DZTFDHLFTH",Visible=.t., Left=679, Top=11,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="CUWIKEYMJO",Visible=.t., Left=649, Top=33,;
    Alignment=1, Width=39, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="NUMMQZCZQJ",Visible=.t., Left=509, Top=421,;
    Alignment=1, Width=141, Height=15,;
    Caption="Valore totale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="ZDCHXMBPHY",Visible=.t., Left=421, Top=9,;
    Alignment=1, Width=137, Height=15,;
    Caption="N. doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="WATZXJRSEV",Visible=.t., Left=386, Top=86,;
    Alignment=1, Width=106, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="SFHVTROIQS",Visible=.t., Left=147, Top=11,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="HFZCVVUWQW",Visible=.t., Left=1, Top=86,;
    Alignment=1, Width=77, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="BWWWTKIFJK",Visible=.t., Left=204, Top=86,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="FXXOVDVYLA",Visible=.t., Left=509, Top=369,;
    Alignment=1, Width=141, Height=15,;
    Caption="Valore di riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="PBNSVACZKZ",Visible=.t., Left=187, Top=395,;
    Alignment=0, Width=15, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="KADFYOUMHB",Visible=.t., Left=250, Top=395,;
    Alignment=0, Width=15, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="KIBLCHUXDG",Visible=.t., Left=314, Top=395,;
    Alignment=0, Width=12, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="ZJNKZEPMBE",Visible=.t., Left=187, Top=421,;
    Alignment=0, Width=15, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="DWUKYBEWER",Visible=.t., Left=250, Top=421,;
    Alignment=0, Width=15, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="CTELWDDBVE",Visible=.t., Left=84, Top=421,;
    Alignment=1, Width=50, Height=15,;
    Caption="Globali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="EIYGMUXXSZ",Visible=.t., Left=1, Top=33,;
    Alignment=1, Width=77, Height=15,;
    Caption="Causale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_69 as StdString with uid="HTFGJXBGZO",Visible=.t., Left=436, Top=473,;
    Alignment=1, Width=67, Height=15,;
    Caption="Disp. mag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="SAFQPLXQGQ",Visible=.t., Left=607, Top=473,;
    Alignment=1, Width=78, Height=15,;
    Caption="Mag.coll.:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL))
    endwith
  endfunc

  add object oStr_1_75 as StdString with uid="AZYALRSZFU",Visible=.t., Left=51, Top=394,;
    Alignment=1, Width=83, Height=18,;
    Caption="Sconti/mag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="FGJGOCCNXX",Visible=.t., Left=509, Top=395,;
    Alignment=1, Width=141, Height=15,;
    Caption="Valore fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="BXPVXWDEDF",Visible=.t., Left=37, Top=343,;
    Alignment=1, Width=37, Height=18,;
    Caption="Lotto:"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (g_PERLOT<>'S')
    endwith
  endfunc

  add object oStr_1_92 as StdString with uid="IWQAPHFPTQ",Visible=.t., Left=6, Top=369,;
    Alignment=1, Width=68, Height=18,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="KDXWOHBNJW",Visible=.t., Left=227, Top=369,;
    Alignment=1, Width=105, Height=18,;
    Caption="Ubicazione coll.:"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
  endfunc

  add object oStr_1_112 as StdString with uid="NOGWWSWSUF",Visible=.t., Left=356, Top=10,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_113 as StdString with uid="XGZTJNUSXD",Visible=.t., Left=102, Top=473,;
    Alignment=1, Width=63, Height=15,;
    Caption="Esistenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_114 as StdString with uid="RKICHPAXVH",Visible=.t., Left=259, Top=473,;
    Alignment=1, Width=74, Height=15,;
    Caption="Disp. cont.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_119 as StdString with uid="NXGLKZSJXK",Visible=.t., Left=102, Top=449,;
    Alignment=1, Width=63, Height=15,;
    Caption="Causale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_119.mHide()
    with this.Parent.oContained
      return (.w_MMTCAMAG=.w_MMCAUMAG)
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=148,;
    width=768+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=149,width=767+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|MAGAZZIN|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oUNMIS1_2_15.Refresh()
      this.Parent.oMMCAUMAG_2_27.Refresh()
      this.Parent.oMMFLELGM_2_33.Refresh()
      this.Parent.oMMCODCOM_2_38.Refresh()
      this.Parent.oMMFLOMAG_2_39.Refresh()
      this.Parent.oMMCODLOT_2_50.Refresh()
      this.Parent.oMMCODUBI_2_55.Refresh()
      this.Parent.oMMCODUB2_2_56.Refresh()
      this.Parent.oMMQTAUM1_2_57.Refresh()
      this.Parent.oPREUM1_2_59.Refresh()
      this.Parent.oMMSCONT1_2_60.Refresh()
      this.Parent.oMMSCONT2_2_61.Refresh()
      this.Parent.oMMSCONT3_2_62.Refresh()
      this.Parent.oMMSCONT4_2_63.Refresh()
      this.Parent.oMMVALMAG_2_67.Refresh()
      this.Parent.oQTAPER_2_86.Refresh()
      this.Parent.oQTDISC_2_88.Refresh()
      this.Parent.oQTDISP_2_91.Refresh()
      this.Parent.oQ2DISP_2_92.Refresh()
      this.Parent.oVISNAZ_2_95.Refresh()
      this.Parent.oQTAUM2_2_113.Refresh()
      this.Parent.oUNIMIS2_2_115.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oMMCODICE_2_1
      case cFile='MAGAZZIN'
        oDropInto=this.oBodyCol.oRow.oMMCODMAG_2_4
      case cFile='MAGAZZIN'
        oDropInto=this.oBodyCol.oRow.oMMCODMAT_2_6
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oMMUNIMIS_2_46
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oUNMIS1_2_15 as StdTrsField with uid="ZWVSPVGLST",rtseq=69,rtrep=.t.,;
    cFormVar="w_UNMIS1",value=space(3),enabled=.f.,;
    HelpContextID = 206329786,;
    cTotal="", bFixedPos=.t., cQueryName = "UNMIS1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=32, Left=410, Top=343, InputMask=replicate('X',3), cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_UNMIS1"

  func oUNMIS1_2_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MMUNIMIS=.w_UNMIS1 OR EMPTY(.w_MMCODICE))
    endwith
    endif
  endfunc

  func oUNMIS1_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMMCAUMAG_2_27 as StdTrsField with uid="GZBIEJHCTW",rtseq=81,rtrep=.t.,;
    cFormVar="w_MMCAUMAG",value=space(5),;
    ToolTipText = "Codice della causale magazzino",;
    HelpContextID = 3471603,;
    cTotal="", bFixedPos=.t., cQueryName = "MMCAUMAG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=87, Left=167, Top=445, InputMask=replicate('X',5), cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_MMCAUMAG"

  func oMMCAUMAG_2_27.mCond()
    with this.Parent.oContained
      return (1=2)
    endwith
  endfunc

  func oMMCAUMAG_2_27.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MMTCAMAG=.w_MMCAUMAG)
    endwith
    endif
  endfunc

  func oMMCAUMAG_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oMMCAUMAG_2_27.ecpDrop(oSource)
    this.Parent.oContained.link_2_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  add object oMMFLELGM_2_33 as StdTrsCheck with uid="IBRLSUOBKG",rtrep=.t.,;
    cFormVar="w_MMFLELGM", enabled=.f., caption="Movimento fiscale",;
    ToolTipText = "Se attivo: riga valida per la contabilit� fiscale di magazzino",;
    HelpContextID = 232142611,;
    Left=384, Top=395,;
    cTotal="", cQueryName = "MMFLELGM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oMMFLELGM_2_33.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MMFLELGM,&i_cF..t_MMFLELGM),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oMMFLELGM_2_33.GetRadio()
    this.Parent.oContained.w_MMFLELGM = this.RadioValue()
    return .t.
  endfunc

  func oMMFLELGM_2_33.ToRadio()
    this.Parent.oContained.w_MMFLELGM=trim(this.Parent.oContained.w_MMFLELGM)
    return(;
      iif(this.Parent.oContained.w_MMFLELGM=='S',1,;
      0))
  endfunc

  func oMMFLELGM_2_33.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMMCODCOM_2_38 as StdTrsField with uid="LFBQOGBTKT",rtseq=92,rtrep=.t.,;
    cFormVar="w_MMCODCOM",value=space(15),enabled=.f.,;
    ToolTipText = "Codice della commessa associata",;
    HelpContextID = 188152045,;
    cTotal="", bFixedPos=.t., cQueryName = "MMCODCOM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=407, Top=445, InputMask=replicate('X',15), cLinkFile="CAN_TIER", cZoomOnZoom="GSCA_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_MMCODCOM"

  func oMMCODCOM_2_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMMFLOMAG_2_39 as StdTrsCombo with uid="SOUZGJCFHE",rtrep=.t.,;
    cFormVar="w_MMFLOMAG", RowSource=""+"Normale,"+"Sconto merce,"+"Omaggio imponibile,"+"Omaggio imp. + IVA" , ;
    ToolTipText = "Test riga omaggio/sconto merce o normale",;
    HelpContextID = 9029875,;
    Height=25, Width=102, Left=658, Top=343,;
    cTotal="", cQueryName = "MMFLOMAG",;
    bObbl = .f. , nPag=2  , tabstop=.f.;
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oMMFLOMAG_2_39.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MMFLOMAG,&i_cF..t_MMFLOMAG),this.value)
    return(iif(xVal =1,'X',;
    iif(xVal =2,'S',;
    iif(xVal =3,'I',;
    iif(xVal =4,'E',;
    space(1))))))
  endfunc
  func oMMFLOMAG_2_39.GetRadio()
    this.Parent.oContained.w_MMFLOMAG = this.RadioValue()
    return .t.
  endfunc

  func oMMFLOMAG_2_39.ToRadio()
    this.Parent.oContained.w_MMFLOMAG=trim(this.Parent.oContained.w_MMFLOMAG)
    return(;
      iif(this.Parent.oContained.w_MMFLOMAG=='X',1,;
      iif(this.Parent.oContained.w_MMFLOMAG=='S',2,;
      iif(this.Parent.oContained.w_MMFLOMAG=='I',3,;
      iif(this.Parent.oContained.w_MMFLOMAG=='E',4,;
      0)))))
  endfunc

  func oMMFLOMAG_2_39.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMMCODLOT_2_50 as StdTrsField with uid="TFKWSSZOUA",rtseq=104,rtrep=.t.,;
    cFormVar="w_MMCODLOT",value=space(20),;
    ToolTipText = "Codice lotto",;
    HelpContextID = 37157094,;
    cTotal="", bFixedPos=.t., cQueryName = "MMCODLOT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice lotto inesistente, incongruente, sospeso, esaurito o scaduto",;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=75, Top=343, cSayPict=[p_LOT], cGetPict=[p_LOT], InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", cZoomOnZoom="GSMD_ALO", oKey_1_1="LOCODART", oKey_1_2="this.w_MMCODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_MMCODLOT"

  func oMMCODLOT_2_50.mCond()
    with this.Parent.oContained
      return (g_PERLOT='S' AND NOT EMPTY(.w_MMCODART) AND .w_FLLOTT$ 'SC' AND (.w_MMFLLOTT $ '+-' OR .w_MMF2LOTT $ '+-') AND .w_MMQTAMOV<>0)
    endwith
  endfunc

  func oMMCODLOT_2_50.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERLOT<>'S')
    endwith
    endif
  endfunc

  func oMMCODLOT_2_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_50('Part',this)
      if .not. empty(.w_MMLOTMAG)
        bRes2=.link_2_121('Full')
      endif
      if .not. empty(.w_MMLOTMAT)
        bRes2=.link_2_122('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMMCODLOT_2_50.ecpDrop(oSource)
    this.Parent.oContained.link_2_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oMMCODLOT_2_50.mZoom
      with this.Parent.oContained
        do GSMD_BZL with this.Parent.oContained
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oMMCODLOT_2_50.mZoomOnZoom
    local i_obj
    i_obj=GSMD_ALO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.LOCODART=w_MMCODART
     i_obj.w_LOCODICE=this.parent.oContained.w_MMCODLOT
    i_obj.ecpSave()
  endproc

  add object oBtn_2_54 as StdButton with uid="GKUCVKDSGM",width=22,height=19,;
   left=225, top=345,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per creare nuovo lotto";
    , HelpContextID = 221812182;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_54.Click()
      with this.Parent.oContained
        do GSMA_BKL with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_54.mCond()
    with this.Parent.oContained
      return ( NOT EMPTY(.w_MMCODART) AND NOT EMPTY(.w_MMCODMAG) AND g_PERLOT='S' AND .w_MMFLCASC ='+')
    endwith
  endfunc

  func oBtn_2_54.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERLOT<>'S' OR .w_MMFLCASC <>'+' OR EMPTY(.w_MMCODART) OR .w_FLLOTT = 'N' OR EMPTY(.w_FLLOTT) OR .cFunction = 'Query')
    endwith
   endif
  endfunc

  add object oMMCODUBI_2_55 as StdTrsField with uid="RMSPWQREBY",rtseq=107,rtrep=.t.,;
    cFormVar="w_MMCODUBI",value=space(20),;
    ToolTipText = "Codice ubicazione associata al magazzino principale",;
    HelpContextID = 154597617,;
    cTotal="", bFixedPos=.t., cQueryName = "MMCODUBI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice ubicazione inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=75, Top=369, cSayPict=[p_UBI], cGetPict=[p_UBI], InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_MMCODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_MMCODUBI"

  func oMMCODUBI_2_55.mCond()
    with this.Parent.oContained
      return (g_PERUBI='S' AND NOT EMPTY(.w_MMCODART) AND NOT EMPTY(.w_MMCODMAG) AND .w_FLUBIC='S'  AND .w_MMQTAMOV<>0)
    endwith
  endfunc

  func oMMCODUBI_2_55.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
    endif
  endfunc

  func oMMCODUBI_2_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_55('Part',this)
      if .not. empty(.w_MMLOTMAG)
        bRes2=.link_2_121('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMMCODUBI_2_55.ecpDrop(oSource)
    this.Parent.oContained.link_2_55('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oMMCODUBI_2_55.mZoom
      with this.Parent.oContained
        GSMD_BZU(this.Parent.oContained,"A")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oMMCODUBI_2_55.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_MMCODMAG
     i_obj.w_UBCODICE=this.parent.oContained.w_MMCODUBI
    i_obj.ecpSave()
  endproc

  add object oMMCODUB2_2_56 as StdTrsField with uid="KLVGMHPPAK",rtseq=108,rtrep=.t.,;
    cFormVar="w_MMCODUB2",value=space(20),;
    ToolTipText = "Codice ubicazione associata al magazzino collegato",;
    HelpContextID = 154597640,;
    cTotal="", bFixedPos=.t., cQueryName = "MMCODUB2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice ubicazione inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=333, Top=369, cSayPict=[p_UBI], cGetPict=[p_UBI], InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", cZoomOnZoom="GSMD_MUB", oKey_1_1="UBCODMAG", oKey_1_2="this.w_MMCODMAT", oKey_2_1="UBCODICE", oKey_2_2="this.w_MMCODUB2"

  func oMMCODUB2_2_56.mCond()
    with this.Parent.oContained
      return (g_PERUBI='S' AND NOT EMPTY(.w_MMCODART) AND NOT EMPTY(.w_MMCODMAT) AND .w_F2UBIC='S' AND  .w_MMQTAMOV<>0)
    endwith
  endfunc

  func oMMCODUB2_2_56.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI<>'S')
    endwith
    endif
  endfunc

  func oMMCODUB2_2_56.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_56('Part',this)
      if .not. empty(.w_MMLOTMAT)
        bRes2=.link_2_122('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMMCODUB2_2_56.ecpDrop(oSource)
    this.Parent.oContained.link_2_56('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oMMCODUB2_2_56.mZoom
      with this.Parent.oContained
        GSMD_BZU(this.Parent.oContained,"B")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oMMCODUB2_2_56.mZoomOnZoom
    local i_obj
    i_obj=GSMD_MUB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.UBCODMAG=w_MMCODMAT
     i_obj.w_UBCODICE=this.parent.oContained.w_MMCODUB2
    i_obj.ecpSave()
  endproc

  add object oMMQTAUM1_2_57 as StdTrsField with uid="YPYHNZEKGO",rtseq=109,rtrep=.t.,;
    cFormVar="w_MMQTAUM1",value=0,;
    ToolTipText = "Quantit� movimentata riferita alla 1^UM",;
    HelpContextID = 111077111,;
    cTotal="", bFixedPos=.t., cQueryName = "MMQTAUM1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� nella 1a UM inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=21, Width=80, Left=446, Top=343, cSayPict=['@Z '+v_PQ(11)], cGetPict=['@Z '+v_GQ(11)]

  func oMMQTAUM1_2_57.mCond()
    with this.Parent.oContained
      return (.w_FLUSEP$'SQ' AND .w_MMUNIMIS<>.w_UNMIS1 AND NOT EMPTY(.w_MMCODICE))
    endwith
  endfunc

  func oMMQTAUM1_2_57.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MMUNIMIS=.w_UNMIS1 OR EMPTY(.w_MMCODICE))
    endwith
    endif
  endfunc

  func oMMQTAUM1_2_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_MMQTAUM1>0 AND (.w_NOFRAZ1<>'S' OR .w_MMQTAUM1=INT(.w_MMQTAUM1))))
    endwith
    return bRes
  endfunc

  add object oPREUM1_2_59 as StdTrsField with uid="FSTCJKGLQW",rtseq=111,rtrep=.t.,;
    cFormVar="w_PREUM1",value=0,;
    ToolTipText = "Prezzo unitario riferito alla prima U.M.",;
    HelpContextID = 211866634,;
    cTotal="", bFixedPos=.t., cQueryName = "PREUM1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=529, Top=343, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)]

  func oPREUM1_2_59.mCond()
    with this.Parent.oContained
      return (.w_FLUSEP='S' AND .w_MMUNIMIS<>.w_UNMIS1 AND NOT EMPTY(.w_MMCODICE))
    endwith
  endfunc

  func oPREUM1_2_59.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MMUNIMIS=.w_UNMIS1 OR EMPTY(.w_MMCODICE) Or .w_FLUSEP='Q')
    endwith
    endif
  endfunc

  add object oMMSCONT1_2_60 as StdTrsField with uid="SJXFHOAFCQ",rtseq=112,rtrep=.t.,;
    cFormVar="w_MMSCONT1",value=0,;
    ToolTipText = "1^% Maggiorazione (se positiva) o sconto (se negativa) di riga",;
    HelpContextID = 261224713,;
    cTotal="", bFixedPos=.t., cQueryName = "MMSCONT1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=134, Top=395, cSayPict=["999.99"], cGetPict=["999.99"], TabStop=.F.

  func oMMSCONT1_2_60.mCond()
    with this.Parent.oContained
      return (g_NUMSCO>0)
    endwith
  endfunc

  add object oMMSCONT2_2_61 as StdTrsField with uid="XDZSVIXRVM",rtseq=113,rtrep=.t.,;
    cFormVar="w_MMSCONT2",value=0,;
    ToolTipText = "2^% Maggiorazione se positiva o sconto se negativa (di riga)",;
    HelpContextID = 261224712,;
    cTotal="", bFixedPos=.t., cQueryName = "MMSCONT2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=198, Top=395, cSayPict=["999.99"], cGetPict=["999.99"], TabStop=.F.

  func oMMSCONT2_2_61.mCond()
    with this.Parent.oContained
      return (.w_MMSCONT1<>0 AND g_NUMSCO>1)
    endwith
  endfunc

  add object oMMSCONT3_2_62 as StdTrsField with uid="YSWGUZIMZQ",rtseq=114,rtrep=.t.,;
    cFormVar="w_MMSCONT3",value=0,;
    ToolTipText = "3^% Maggiorazione se positiva o sconto se negativa (di riga)",;
    HelpContextID = 261224711,;
    cTotal="", bFixedPos=.t., cQueryName = "MMSCONT3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=262, Top=395, cSayPict=["999.99"], cGetPict=["999.99"], TabStop=.F.

  func oMMSCONT3_2_62.mCond()
    with this.Parent.oContained
      return (.w_MMSCONT2<>0 AND g_NUMSCO>2)
    endwith
  endfunc

  add object oMMSCONT4_2_63 as StdTrsField with uid="OWAHDJAVYJ",rtseq=115,rtrep=.t.,;
    cFormVar="w_MMSCONT4",value=0,;
    ToolTipText = "4^% Maggiorazione se positiva o sconto se negativa (di riga)",;
    HelpContextID = 261224710,;
    cTotal="", bFixedPos=.t., cQueryName = "MMSCONT4",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=326, Top=395, cSayPict=["999.99"], cGetPict=["999.99"], TabStop=.F.

  func oMMSCONT4_2_63.mCond()
    with this.Parent.oContained
      return (.w_MMSCONT3<>0 AND g_NUMSCO>3)
    endwith
  endfunc

  add object oMMVALMAG_2_67 as StdTrsField with uid="DCZLATDRCA",rtseq=119,rtrep=.t.,;
    cFormVar="w_MMVALMAG",value=0,enabled=.f.,;
    ToolTipText = "Importo totale riga al netto degli sconti totali e di riga",;
    HelpContextID = 12830963,;
    cTotal="this.Parent.oContained.w_totale", bFixedPos=.t., cQueryName = "MMVALMAG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=653, Top=369, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oLinkPC_2_71 as StdButton with uid="SUOYSFVRLN",width=48,height=45,;
   left=0, top=395,;
    CpPicture="bmp\MATRICOLE.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alla gestione delle matricole";
    , HelpContextID = 146853502;
    , Caption='\<Matricole';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_71.Click()
      this.Parent.oContained.GSVE_MMT.LinkPCClick()
    endproc

  func oLinkPC_2_71.mCond()
    with this.Parent.oContained
      return (g_MATR='S'  And .w_MMDATREG>=nvl(g_DATMAT,cp_CharToDate('  /  /    ')) And .w_MMQTAMOV<>0 And (.w_MMFLCASC $ '+-' Or .w_MMF2CASC $ '+-' Or .w_MMFLRISE $ '+-' Or .w_MMF2RISE $ '+-' ) AND ((.w_FLLOTT $ 'S-C' AND NOT EMPTY(.w_mmcodlot)) OR .w_FLLOTT='N' OR g_PERLOT<>'S') AND ((.w_FLUBIC='S' AND NOT EMPTY(.w_mmcodubi)) OR EMPTY(.w_FLUBIC) OR g_PERUBI<>'S') and ((.w_F2UBIC='S' AND NOT EMPTY(.w_mmcodub2)) OR EMPTY(.w_F2UBIC) OR .w_MMF2LOTT<>'+' OR g_PERUBI<>'S'))
    endwith
  endfunc

  func oLinkPC_2_71.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ( g_MATR<>'S' or .w_MMDATREG<nvl(g_DATMAT,cp_CharToDate('  /  /    ')) or EMPTY(.w_GESMAT))
    endwith
   endif
  endfunc

  add object oQTAPER_2_86 as StdTrsField with uid="YGILSNDYHD",rtseq=137,rtrep=.t.,;
    cFormVar="w_QTAPER",value=0,enabled=.f.,;
    ToolTipText = "Esistenza articolo",;
    HelpContextID = 203821562,;
    cTotal="", bFixedPos=.t., cQueryName = "QTAPER",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=87, Left=167, Top=472, cSayPict=[v_PQ(14)], cGetPict=[v_GQ(14)]

  add object oQTDISC_2_88 as StdTrsField with uid="NOHOPVBRNR",rtseq=139,rtrep=.t.,;
    cFormVar="w_QTDISC",value=0,enabled=.f.,;
    ToolTipText = "Disponibilit� contabile articolo",;
    HelpContextID = 172810746,;
    cTotal="", bFixedPos=.t., cQueryName = "QTDISC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=102, Left=332, Top=472, cSayPict=[v_PQ(14)], cGetPict=[v_GQ(14)]

  add object oQTDISP_2_91 as StdTrsField with uid="JQTHVGAIBX",rtseq=142,rtrep=.t.,;
    cFormVar="w_QTDISP",value=0,enabled=.f.,;
    ToolTipText = "Disponibilit� articolo relativa al magazzino principale",;
    HelpContextID = 223142394,;
    cTotal="", bFixedPos=.t., cQueryName = "QTDISP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=504, Top=472, cSayPict=[v_PQ(14)], cGetPict=[v_GQ(14)]

  add object oQ2DISP_2_92 as StdTrsField with uid="ZBCAJANUZZ",rtseq=143,rtrep=.t.,;
    cFormVar="w_Q2DISP",value=0,enabled=.f.,;
    ToolTipText = "Disponibilit� articolo relativa al magazzino collegato",;
    HelpContextID = 223151098,;
    cTotal="", bFixedPos=.t., cQueryName = "Q2DISP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=686, Top=472, cSayPict=[v_PQ(14)], cGetPict=[v_GQ(12)]

  func oQ2DISP_2_92.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_MMCAUCOL))
    endwith
    endif
  endfunc

  add object oVISNAZ_2_95 as StdTrsField with uid="KDKRAMQSKA",rtseq=149,rtrep=.t.,;
    cFormVar="w_VISNAZ",value=0,enabled=.f.,;
    ToolTipText = "Importo di riga espresso in valuta di conto usato per le valorizzazioni (netto riga-sconti globali)",;
    HelpContextID = 73858218,;
    cTotal="", bFixedPos=.t., cQueryName = "VISNAZ",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=653, Top=395, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oBtn_2_108 as StdButton with uid="NPJMNTWTZR",width=48,height=45,;
   left=-1, top=445,;
    CpPicture="BMP\ULTVEN.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere agli ultimi prezzi al cliente";
    , HelpContextID = 221612614;
    , tabStop=.f., Caption='\<U';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_108.Click()
      with this.Parent.oContained
        GSAR_BLU(this.Parent.oContained,"MU")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_108.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MMTIPCON<>'C')
    endwith
   endif
  endfunc

  add object oBtn_2_109 as StdButton with uid="YOECWZONEB",width=48,height=45,;
   left=49, top=445,;
    CpPicture="BMP\ULTACQ.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere agli ultimi costi da tutti i fornitori";
    , HelpContextID = 221612326;
    , tabStop=.f., Caption='\<C';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_109.Click()
      with this.Parent.oContained
        GSAR_BLU(this.Parent.oContained,"MC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_109.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MMCODART))
    endwith
  endfunc

  func oBtn_2_109.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_MMTIPCON$'CF')
    endwith
   endif
  endfunc

  add object oBtn_2_110 as StdButton with uid="NMJTVMWYGT",width=48,height=45,;
   left=-1, top=445,;
    CpPicture="BMP\Costi.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere agli ultimi costi da fornitore";
    , HelpContextID = 221612614;
    , tabStop=.f., Caption='\<U';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_110.Click()
      with this.Parent.oContained
        GSAR_BLU(this.Parent.oContained,"MU")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_110.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MMCODART))
    endwith
  endfunc

  func oBtn_2_110.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MMTIPCON<>'F')
    endwith
   endif
  endfunc

  add object oQTAUM2_2_113 as StdTrsField with uid="AATFUNGWCS",rtseq=179,rtrep=.t.,;
    cFormVar="w_QTAUM2",value=0,enabled=.f.,;
    ToolTipText = "Quantit� movimentata riferita alla 2^UM",;
    HelpContextID = 195105274,;
    cTotal="", bFixedPos=.t., cQueryName = "QTAUM2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=446, Top=343

  func oQTAUM2_2_113.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MMUNIMIS<>.w_UNMIS1 Or EMPTY(.w_MMCODICE) or (empty(.w_UNMIS2) And empty(.w_UNMIS3)))
    endwith
    endif
  endfunc

  add object oObj_2_114 as cp_calclbl with uid="XVZERTRDQJ",width=137,height=15,;
   left=272, top=345,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,;
    nPag=2;
    , HelpContextID = 137262106

  add object oUNIMIS2_2_115 as StdTrsField with uid="NSXSHMSAHR",rtseq=180,rtrep=.t.,;
    cFormVar="w_UNIMIS2",value=space(10),enabled=.f.,;
    HelpContextID = 183015354,;
    cTotal="", bFixedPos=.t., cQueryName = "UNIMIS2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=411, Top=343, InputMask=replicate('X',10)

  func oUNIMIS2_2_115.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MMUNIMIS<>.w_UNMIS1 Or EMPTY(.w_MMCODICE) or (empty(.w_UNMIS2) And empty(.w_UNMIS3)))
    endwith
    endif
  endfunc

  add object oObj_2_127 as cp_calclbl with uid="BQOFVBRGLZ",width=92,height=17,;
   left=312, top=446,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,fontname="Arial",fontsize=9,fontItalic=.f.,fontUnderline=.f.,fontBold=.f.,bGlobalFont=.t.,;
    nPag=2;
    , HelpContextID = 137262106

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTALE_3_1 as StdField with uid="GAMJSHRKOI",rtseq=146,rtrep=.f.,;
    cFormVar="w_TOTALE",value=0,enabled=.f.,;
    ToolTipText = "Somma valori di riga (compresi omaggi e sconti merci)",;
    HelpContextID = 147056330,;
    cTotal="", bFixedPos=.t., cQueryName = "TOTALE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=653, Top=421, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]
enddefine

* --- Defining Body row
define class tgsma_mvmBodyRow as CPBodyRowCnt
  Width=758
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMMCODICE_2_1 as StdTrsField with uid="JXLDNFGVBT",rtseq=55,rtrep=.t.,;
    cFormVar="w_MMCODICE",value=space(20),;
    ToolTipText = "Codice articolo",;
    HelpContextID = 87488757,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=149, Left=39, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_MMCODICE"

  proc oMMCODICE_2_1.mAfter
      with this.Parent.oContained
        GSAR_BGP(this.Parent.oContained,this.value)
      endwith
  endproc

  func oMMCODICE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oMMCODICE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMMCODICE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oMMCODICE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Chiavi articoli",'GSMA_MVM.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oMMCODICE_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_MMCODICE
    i_obj.ecpSave()
  endproc

  add object oMMCODMAG_2_4 as StdTrsField with uid="PMOMIEKWAY",rtseq=58,rtrep=.t.,;
    cFormVar="w_MMCODMAG",value=space(5),;
    ToolTipText = "Usato per calcolare saldi articolo",;
    HelpContextID = 20379891,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=52, Left=398, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MMCODMAG"

  func oMMCODMAG_2_4.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MMCODICE) AND (EMPTY(.w_MMCODMAG) OR .w_FM1<>'S'))
    endwith
  endfunc

  func oMMCODMAG_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
      if .not. empty(.w_MMCODUBI)
        bRes2=.link_2_55('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMMCODMAG_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMMCODMAG_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMMCODMAG_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'GSMA2AMA.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oMMCODMAG_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MMCODMAG
    i_obj.ecpSave()
  endproc

  add object oMMCODMAT_2_6 as StdTrsField with uid="SXCFBVTRAT",rtseq=60,rtrep=.t.,;
    cFormVar="w_MMCODMAT",value=space(5),;
    ToolTipText = "Usato per calcolare saldi articolo movimento collegato",;
    HelpContextID = 20379878,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=52, Left=452, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_MMCODMAT"

  func oMMCODMAT_2_6.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MMCODICE) AND NOT EMPTY(.w_MMCAUCOL) AND (EMPTY(.w_MMCODMAT) OR .w_FM2<>'S'))
    endwith
  endfunc

  func oMMCODMAT_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
      if .not. empty(.w_MMCODUB2)
        bRes2=.link_2_56('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMMCODMAT_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMMCODMAT_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oMMCODMAT_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'GSMA2AMA.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oMMCODMAT_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_MMCODMAT
    i_obj.ecpSave()
  endproc

  add object oCPROWORD_2_9 as StdTrsField with uid="HJAVBVHXVV",rtseq=63,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 235275926,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], TabStop=.F.

  add object oDESART_2_44 as StdTrsField with uid="KLODXYKYVC",rtseq=98,rtrep=.t.,;
    cFormVar="w_DESART",value=space(40),enabled=.f.,;
    HelpContextID = 157549002,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=15, Width=205, Left=191, Top=0, InputMask=replicate('X',40)

  add object oMMUNIMIS_2_46 as StdTrsField with uid="TXVYGJCBWE",rtseq=100,rtrep=.t.,;
    cFormVar="w_MMUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 253306649,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=42, Left=506, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_MMUNIMIS"

  func oMMUNIMIS_2_46.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MMCODICE))
    endwith
  endfunc

  func oMMUNIMIS_2_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_46('Part',this)
    endwith
    return bRes
  endfunc

  proc oMMUNIMIS_2_46.ecpDrop(oSource)
    this.Parent.oContained.link_2_46('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMMUNIMIS_2_46.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oMMUNIMIS_2_46'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"",'GSMA_MVM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oMMUNIMIS_2_46.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_MMUNIMIS
    i_obj.ecpSave()
  endproc

  add object oMMQTAMOV_2_48 as StdTrsField with uid="SSRZORVMMZ",rtseq=102,rtrep=.t.,;
    cFormVar="w_MMQTAMOV",value=0,;
    ToolTipText = "Quantit� movimentata",;
    HelpContextID = 23140580,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� movimentata inesistente o non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=551, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]

  func oMMQTAMOV_2_48.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MMCODICE))
    endwith
  endfunc

  proc oMMQTAMOV_2_48.mBefore
    with this.Parent.oContained
      .w_FM1=' '
    endwith
  endproc

  proc oMMQTAMOV_2_48.mAfter
    with this.Parent.oContained
      .w_FM2=' '
    endwith
  endproc

  func oMMQTAMOV_2_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MMQTAMOV>0 AND (.w_FLFRAZ<>'S' OR .w_MMQTAMOV=INT(.w_MMQTAMOV)))
    endwith
    return bRes
  endfunc

  add object oMMPREZZO_2_58 as StdTrsField with uid="TTCNPDSOFE",rtseq=110,rtrep=.t.,;
    cFormVar="w_MMPREZZO",value=0,;
    ToolTipText = "Prezzo di listino (F9) =esegue lo scorporo",;
    HelpContextID = 199022357,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=121, Left=632, Top=0, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)], bHasZoom = .t. 

  func oMMPREZZO_2_58.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MMCODICE))
    endwith
  endfunc

  proc oMMPREZZO_2_58.mZoom
      with this.Parent.oContained
        GSMA_BCD(this.Parent.oContained,"S")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oMMCODICE_2_1.When()
    return(.t.)
  proc oMMCODICE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMMCODICE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_mvm','MVM_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MMSERIAL=MVM_MAST.MMSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
