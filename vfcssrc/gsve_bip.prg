* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bip                                                        *
*              Controlli quantita/prezzo                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_30]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2008-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bip",oParentObject,m.pTipOpe)
return(i_retval)

define class tgsve_bip as StdBatch
  * --- Local variables
  pTipOpe = space(1)
  w_ORIF = space(1)
  w_PRZORI = 0
  w_OPREZ = 0
  w_PADRE = .NULL.
  w_O_OLDEVAS = space(1)
  * --- WorkFile variables
  DOC_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Import da Documenti Collegati da (GSVE_KIM)
    * --- Controlli al Cambio Prezzo sulle Righe (da GSVE_MDV Evento: w_MVPREZZO)
    this.w_PADRE = This.oParentObject
    this.w_OPREZ = this.oParentObject.w_MVPREZZO
    if this.pTipOpe="1"
      * --- Variato Prezzo su Prima U.M.
      this.oParentObject.w_MVPREZZO = IIF(this.oParentObject.w_MVQTAMOV=0, 0, cp_ROUND((this.oParentObject.w_PREUM1 * this.oParentObject.w_MVQTAUM1) / this.oParentObject.w_MVQTAMOV, this.oParentObject.w_DECUNI))
    else
      this.oParentObject.w_PREUM1 = IIF(this.oParentObject.w_MVQTAUM1=0, 0, cp_ROUND((this.oParentObject.w_MVPREZZO * this.oParentObject.w_MVQTAMOV) / this.oParentObject.w_MVQTAUM1, this.oParentObject.w_DECUNI))
    endif
    * --- Non esegue la mCalc
    this.bUpdateParentObject = .F.
    if NOT EMPTY(this.oParentObject.w_MVSERRIF) AND this.oParentObject.w_MVROWRIF<>0 AND (this.oParentObject.w_MVPREZZO<>this.w_OPREZ OR this.pTipOpe="R") And this.oParentObject.w_MVTIPRIG="F" AND Not Empty(this.oParentObject.w_MVFLARIF) AND ( this.w_PADRE.cFunction="Load" Or this.w_PADRE.cFunction="Edit" ) 
      * --- Leggo il prezzo sul documento di origine..
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVPREZZO"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MVROWRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVPREZZO;
          from (i_cTable) where;
              MVSERIAL = this.oParentObject.w_MVSERRIF;
              and CPROWNUM = this.oParentObject.w_MVROWRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PRZORI = NVL(cp_ToDate(_read_.MVPREZZO),cp_NullValue(_read_.MVPREZZO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_ORIF = this.oParentObject.w_MVFLERIF
      this.w_O_OLDEVAS = this.oParentObject.w_OLDEVAS
      if Abs(this.oParentObject.w_MVPREZZO)<= Abs(this.w_PRZORI)
        this.oParentObject.w_MVFLERIF = IIF(ah_YesNo("Evado il valore rimanente sul documento di origine?"), "S", " ")
        this.oParentObject.w_OLDEVAS = IIF( this.oParentObject.w_MVFLERIF="S" , "E" , "R" )
        if ( this.oParentObject.w_MVFLERIF <> this.w_ORIF ) Or ( this.w_O_OLDEVAS<>this.oParentObject.w_OLDEVAS )
          * --- Uniformo tutte le righe del documento che evadono la stessa riga con lo stesso valore
          *     di MVFLERIF
          GSAR_BCM(this, this.w_PADRE , this.oParentObject.w_CPROWNUM , this.oParentObject.w_MVSERRIF, this.oParentObject.w_MVROWRIF , this.oParentObject.w_OLDEVAS )
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- In questo caso deve eseguire la mCalc
          this.bUpdateParentObject = .T.
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pTipOpe)
    this.pTipOpe=pTipOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_DETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOpe"
endproc
