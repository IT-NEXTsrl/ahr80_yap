* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_spf                                                        *
*              Stampa plafond                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_122]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-18                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_spf",oParentObject))

* --- Class definition
define class tgscg_spf as StdForm
  Top    = 40
  Left   = 42

  * --- Standard Properties
  Width  = 457
  Height = 263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-08"
  HelpContextID=141174121
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  ATTIMAST_IDX = 0
  DAT_IVAN_IDX = 0
  ATTIDETT_IDX = 0
  cPrg = "gscg_spf"
  cComment = "Stampa plafond"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_ANNO = space(4)
  w_MOBPLA = space(1)
  w_VALPLA = space(3)
  w_DECTOT = 0
  w_DESVAL = space(35)
  w_CALCPICT = space(10)
  w_MESE = 0
  w_DESCRI = space(35)
  w_PLARES = 0
  w_FLBOLL = space(1)
  w_TESTO = space(1)
  w_SIMVAL = space(5)
  w_PLNODO = space(1)
  w_FLREGI = space(1)
  w_CODATT = space(5)
  o_CODATT = space(5)
  w_INTLIG = space(1)
  w_NUMREG = 0
  w_TIPREG = space(1)
  w_PRPARI = 0
  w_PREFIS = space(20)
  w_PRPARI1 = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_spfPag1","gscg_spf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANNO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='ATTIMAST'
    this.cWorkTables[4]='DAT_IVAN'
    this.cWorkTables[5]='ATTIDETT'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_ANNO=space(4)
      .w_MOBPLA=space(1)
      .w_VALPLA=space(3)
      .w_DECTOT=0
      .w_DESVAL=space(35)
      .w_CALCPICT=space(10)
      .w_MESE=0
      .w_DESCRI=space(35)
      .w_PLARES=0
      .w_FLBOLL=space(1)
      .w_TESTO=space(1)
      .w_SIMVAL=space(5)
      .w_PLNODO=space(1)
      .w_FLREGI=space(1)
      .w_CODATT=space(5)
      .w_INTLIG=space(1)
      .w_NUMREG=0
      .w_TIPREG=space(1)
      .w_PRPARI=0
      .w_PREFIS=space(20)
      .w_PRPARI1=0
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_ANNO = STR(YEAR(i_DATSYS),4,0)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ANNO))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_VALPLA))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,6,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
          .DoRTCalc(8,10,.f.)
        .w_FLBOLL = 'S'
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
          .DoRTCalc(12,14,.f.)
        .w_FLREGI = 'S'
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODATT))
          .link_1_27('Full')
        endif
          .DoRTCalc(17,19,.f.)
        .w_PRPARI = .w_PRPARI1
      .oPgFrm.Page1.oPag.oObj_1_36.Calculate(AH_Msgformat("Questa funzione provvede al calcolo dell'importo disponibile per gli acquisti %0in sospensione IVA (plafond) verificando le registrazioni contabili %0e i documenti di trasporto di acquisto"))
    endwith
    this.DoRTCalc(21,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CODAZI = i_CODAZI
          .link_1_1('Full')
        .DoRTCalc(2,3,.t.)
          .link_1_4('Full')
        .DoRTCalc(5,6,.t.)
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(8,15,.t.)
          .link_1_27('Full')
        .DoRTCalc(17,19,.t.)
        if .o_CODATT<>.w_CODATT
            .w_PRPARI = .w_PRPARI1
        endif
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate(AH_Msgformat("Questa funzione provvede al calcolo dell'importo disponibile per gli acquisti %0in sospensione IVA (plafond) verificando le registrazioni contabili %0e i documenti di trasporto di acquisto"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate(AH_Msgformat("Questa funzione provvede al calcolo dell'importo disponibile per gli acquisti %0in sospensione IVA (plafond) verificando le registrazioni contabili %0e i documenti di trasporto di acquisto"))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMESE_1_8.enabled = this.oPgFrm.Page1.oPag.oMESE_1_8.mCond()
    this.oPgFrm.Page1.oPag.oPRPARI_1_31.enabled = this.oPgFrm.Page1.oPag.oPRPARI_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMESE_1_8.visible=!this.oPgFrm.Page1.oPag.oMESE_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oPLARES_1_13.visible=!this.oPgFrm.Page1.oPag.oPLARES_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZPLNODO,AZCATAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZPLNODO,AZCATAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_PLNODO = NVL(_Link_.AZPLNODO,space(1))
      this.w_CODATT = NVL(_Link_.AZCATAZI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_PLNODO = space(1)
      this.w_CODATT = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANNO
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DAT_IVAN_IDX,3]
    i_lTable = "DAT_IVAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2], .t., this.DAT_IVAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DAT_IVAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IA__ANNO like "+cp_ToStrODBC(trim(this.w_ANNO)+"%");
                   +" and IACODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IACODAZI,IA__ANNO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IACODAZI',this.w_CODAZI;
                     ,'IA__ANNO',trim(this.w_ANNO))
          select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IACODAZI,IA__ANNO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANNO)==trim(_Link_.IA__ANNO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANNO) and !this.bDontReportError
            deferred_cp_zoom('DAT_IVAN','*','IACODAZI,IA__ANNO',cp_AbsName(oSource.parent,'oANNO_1_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Anno di selezione non definito nei parametri IVA")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB";
                     +" from "+i_cTable+" "+i_lTable+" where IA__ANNO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and IACODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IACODAZI',oSource.xKey(1);
                       ,'IA__ANNO',oSource.xKey(2))
            select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB";
                   +" from "+i_cTable+" "+i_lTable+" where IA__ANNO="+cp_ToStrODBC(this.w_ANNO);
                   +" and IACODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IACODAZI',this.w_CODAZI;
                       ,'IA__ANNO',this.w_ANNO)
            select IACODAZI,IA__ANNO,IAVALPLA,IAPLAMOB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNO = NVL(_Link_.IA__ANNO,space(4))
      this.w_VALPLA = NVL(_Link_.IAVALPLA,space(3))
      this.w_MOBPLA = NVL(_Link_.IAPLAMOB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANNO = space(4)
      endif
      this.w_VALPLA = space(3)
      this.w_MOBPLA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Val(.w_ANNO)>1900
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Anno di selezione non definito nei parametri IVA")
        endif
        this.w_ANNO = space(4)
        this.w_VALPLA = space(3)
        this.w_MOBPLA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])+'\'+cp_ToStr(_Link_.IACODAZI,1)+'\'+cp_ToStr(_Link_.IA__ANNO,1)
      cp_ShowWarn(i_cKey,this.DAT_IVAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALPLA
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALPLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALPLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADESVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALPLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALPLA)
            select VACODVAL,VADECTOT,VADESVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALPLA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALPLA = space(3)
      endif
      this.w_DECTOT = 0
      this.w_DESVAL = space(35)
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALPLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIDETT_IDX,3]
    i_lTable = "ATTIDETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIDETT_IDX,2], .t., this.ATTIDETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIDETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATFLREGI,ATPREFIS,ATPRPARI,ATSTAINT,ATNUMREG,ATTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATFLREGI="+cp_ToStrODBC(this.w_FLREGI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_CODATT;
                       ,'ATFLREGI',this.w_FLREGI;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODATT,ATFLREGI,ATPREFIS,ATPRPARI,ATSTAINT,ATNUMREG,ATTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(5))
      this.w_PREFIS = NVL(_Link_.ATPREFIS,space(20))
      this.w_PRPARI1 = NVL(_Link_.ATPRPARI,0)
      this.w_INTLIG = NVL(_Link_.ATSTAINT,space(1))
      this.w_NUMREG = NVL(_Link_.ATNUMREG,0)
      this.w_TIPREG = NVL(_Link_.ATTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(5)
      endif
      this.w_PREFIS = space(20)
      this.w_PRPARI1 = 0
      this.w_INTLIG = space(1)
      this.w_NUMREG = 0
      this.w_TIPREG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIDETT_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)+'\'+cp_ToStr(_Link_.ATFLREGI,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIDETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANNO_1_2.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_2.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oVALPLA_1_4.value==this.w_VALPLA)
      this.oPgFrm.Page1.oPag.oVALPLA_1_4.value=this.w_VALPLA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_6.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_6.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE_1_8.value==this.w_MESE)
      this.oPgFrm.Page1.oPag.oMESE_1_8.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_9.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_9.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPLARES_1_13.value==this.w_PLARES)
      this.oPgFrm.Page1.oPag.oPLARES_1_13.value=this.w_PLARES
    endif
    if not(this.oPgFrm.Page1.oPag.oFLBOLL_1_15.RadioValue()==this.w_FLBOLL)
      this.oPgFrm.Page1.oPag.oFLBOLL_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTESTO_1_16.RadioValue()==this.w_TESTO)
      this.oPgFrm.Page1.oPag.oTESTO_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPARI_1_31.value==this.w_PRPARI)
      this.oPgFrm.Page1.oPag.oPRPARI_1_31.value=this.w_PRPARI
    endif
    if not(this.oPgFrm.Page1.oPag.oPREFIS_1_33.value==this.w_PREFIS)
      this.oPgFrm.Page1.oPag.oPREFIS_1_33.value=this.w_PREFIS
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ANNO)) or not(Val(.w_ANNO)>1900))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Anno di selezione non definito nei parametri IVA")
          case   ((empty(.w_MESE)) or not(.w_MESE>0 AND .w_MESE<13))  and not(.w_FLBOLL<>'R')  and (.w_FLBOLL='R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMESE_1_8.SetFocus()
            i_bnoObbl = !empty(.w_MESE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODATT = this.w_CODATT
    return

enddefine

* --- Define pages as container
define class tgscg_spfPag1 as StdContainer
  Width  = 453
  height = 263
  stdWidth  = 453
  stdheight = 263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANNO_1_2 as StdField with uid="BGOLHXOVAM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Anno di selezione non definito nei parametri IVA",;
    ToolTipText = "Anno di selezione dei documenti da elaborare",;
    HelpContextID = 135656186,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=88, Top=94, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4), cLinkFile="DAT_IVAN", oKey_1_1="IACODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="IA__ANNO", oKey_2_2="this.w_ANNO"

  func oANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oANNO_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oVALPLA_1_4 as StdField with uid="MOQGNIOYSF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_VALPLA", cQueryName = "VALPLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta riferimento plafond",;
    HelpContextID = 229302614,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=88, Top=124, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALPLA"

  func oVALPLA_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESVAL_1_6 as StdField with uid="PWJIOVAEJF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 134304822,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=145, Top=124, InputMask=replicate('X',35)

  add object oMESE_1_8 as StdField with uid="EODIQWTSJY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Mese di stampa dei dati del plafond",;
    HelpContextID = 136293178,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=189, Top=94, cSayPict='"99"', cGetPict='"99"'

  func oMESE_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLBOLL='R')
    endwith
   endif
  endfunc

  func oMESE_1_8.mHide()
    with this.Parent.oContained
      return (.w_FLBOLL<>'R')
    endwith
  endfunc

  func oMESE_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MESE>0 AND .w_MESE<13)
    endwith
    return bRes
  endfunc

  add object oDESCRI_1_9 as StdField with uid="PCWHCZLDGS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 100553782,;
   bGlobalFont=.t.,;
    Height=21, Width=221, Left=221, Top=94, InputMask=replicate('X',35)


  add object oObj_1_11 as cp_runprogram with uid="XEIVSFJOPR",left=-8, top=326, width=320,height=23,;
    caption='GSCG_BPF',;
   bGlobalFont=.t.,;
    prg="GSCG_BPF('A')",;
    cEvent = "Init,w_ANNO Changed,w_FLBOLL Changed",;
    nPag=1;
    , ToolTipText = "Inizializza i dati sulla maschera";
    , HelpContextID = 265380524

  add object oPLARES_1_13 as StdField with uid="CUSSHNQVTQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PLARES", cQueryName = "PLARES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Plafond disponibile ad inizio periodo",;
    HelpContextID = 255605750,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=88, Top=154, cSayPict="v_PV[40+VVL]", cGetPict="v_PV[40+VVL]"

  func oPLARES_1_13.mHide()
    with this.Parent.oContained
      return (.w_MOBPLA='S')
    endwith
  endfunc


  add object oFLBOLL_1_15 as StdCombo with uid="FKDHXDBTWI",rtseq=11,rtrep=.f.,left=313,top=154,width=129,height=21;
    , ToolTipText = "Se definitiva i risultati verranno memorizzati";
    , HelpContextID = 145312598;
    , cFormVar="w_FLBOLL",RowSource=""+"Simulata,"+"Definitiva,"+"Ristampa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLBOLL_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'B',;
    iif(this.value =3,'R',;
    ''))))
  endfunc
  func oFLBOLL_1_15.GetRadio()
    this.Parent.oContained.w_FLBOLL = this.RadioValue()
    return .t.
  endfunc

  func oFLBOLL_1_15.SetRadio()
    this.Parent.oContained.w_FLBOLL=trim(this.Parent.oContained.w_FLBOLL)
    this.value = ;
      iif(this.Parent.oContained.w_FLBOLL=='S',1,;
      iif(this.Parent.oContained.w_FLBOLL=='B',2,;
      iif(this.Parent.oContained.w_FLBOLL=='R',3,;
      0)))
  endfunc

  add object oTESTO_1_16 as StdCheck with uid="AQPQDWWHTD",rtseq=12,rtrep=.f.,left=88, top=216, caption="Solo testo",;
    ToolTipText = "Se attivo stampa solo testo",;
    HelpContextID = 52472522,;
    cFormVar="w_TESTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTESTO_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTESTO_1_16.GetRadio()
    this.Parent.oContained.w_TESTO = this.RadioValue()
    return .t.
  endfunc

  func oTESTO_1_16.SetRadio()
    this.Parent.oContained.w_TESTO=trim(this.Parent.oContained.w_TESTO)
    this.value = ;
      iif(this.Parent.oContained.w_TESTO=='S',1,;
      0)
  endfunc


  add object oBtn_1_18 as StdButton with uid="TSCGIYKJDO",left=344, top=211, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare la stampa";
    , HelpContextID = 141145370;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSCG_BPF(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ANNO) AND NOT EMPTY(.w_MESE))
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="DODMUXEIST",left=394, top=211, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 133856698;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_22 as cp_runprogram with uid="DYHAMWVAJC",left=317, top=326, width=224,height=23,;
    caption='GSCG_BPF',;
   bGlobalFont=.t.,;
    prg="GSCG_BPF('M')",;
    cEvent = "Init,w_MESE Changed",;
    nPag=1;
    , ToolTipText = "Inizializza i dati sulla maschera";
    , HelpContextID = 265380524

  add object oPRPARI_1_31 as StdField with uid="GZVSBCMEQD",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PRPARI", cQueryName = "PRPARI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultima pagina stampata nel corrispondente R.I.",;
    HelpContextID = 100413942,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=88, Top=185, cSayPict='"9999999"', cGetPict='"9999999"'

  func oPRPARI_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INTLIG='S')
    endwith
   endif
  endfunc

  add object oPREFIS_1_33 as StdField with uid="SJBUMXHJGU",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PREFIS", cQueryName = "PREFIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso che comparirÓ nella stampa prima del numero di pagina",;
    HelpContextID = 259031542,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=289, Top=185, InputMask=replicate('X',20)


  add object oObj_1_36 as cp_calclbl with uid="VMQEWPJDXF",left=9, top=21, width=439,height=70,;
    caption='Calcolo importo disponibile per gli acquisti in sospensione IVA (plafond)',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 33764346

  add object oStr_1_10 as StdString with uid="TMIDGPSAFY",Visible=.t., Left=9, Top=4,;
    Alignment=0, Width=60, Height=18,;
    Caption="Attenzione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="SOQUMIYJXF",Visible=.t., Left=176, Top=94,;
    Alignment=1, Width=43, Height=18,;
    Caption="Mese:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_FLBOLL='R')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="CBQYDQMHZI",Visible=.t., Left=4, Top=154,;
    Alignment=1, Width=81, Height=18,;
    Caption="Disponibile:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_MOBPLA='S')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="VCGPNMZWZO",Visible=.t., Left=4, Top=124,;
    Alignment=1, Width=81, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="MFQZVSEWGC",Visible=.t., Left=4, Top=94,;
    Alignment=1, Width=81, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="KMVKUOCTEJ",Visible=.t., Left=267, Top=154,;
    Alignment=1, Width=43, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="VIMMVBDKZN",Visible=.t., Left=147, Top=94,;
    Alignment=1, Width=40, Height=18,;
    Caption="Mese:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_FLBOLL<>'R')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="FWNKOATCCI",Visible=.t., Left=3, Top=185,;
    Alignment=1, Width=84, Height=15,;
    Caption="Ultima pagina:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="QBIQLOVEJE",Visible=.t., Left=179, Top=185,;
    Alignment=1, Width=110, Height=15,;
    Caption="Prefisso num. pag.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_spf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
