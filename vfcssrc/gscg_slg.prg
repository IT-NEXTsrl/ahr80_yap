* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_slg                                                        *
*              Stampa libro giornale                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_69]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-04                                                      *
* Last revis.: 2012-05-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_slg",oParentObject))

* --- Class definition
define class tgscg_slg as StdForm
  Top    = 37
  Left   = 59

  * --- Standard Properties
  Width  = 561
  Height = 293
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-08"
  HelpContextID=208282985
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  CAU_CONT_IDX = 0
  cPrg = "gscg_slg"
  cComment = "Stampa libro giornale"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_CODESE = space(4)
  o_CODESE = space(4)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_TIPSTA = space(1)
  o_TIPSTA = space(1)
  w_PROLIG = 0
  w_CALPRO = space(1)
  o_CALPRO = space(1)
  w_DATINR = ctod('  /  /  ')
  o_DATINR = ctod('  /  /  ')
  w_PRPALG = 0
  w_DATFIR = ctod('  /  /  ')
  w_SALPRO = space(1)
  w_FLTEST = space(1)
  w_CAUCONT = space(5)
  w_FLTCRO = space(1)
  w_DARLIG = 0
  w_AVELIG = 0
  w_TOTDAY = space(1)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_VALUTA = space(3)
  w_STALIG = ctod('  /  /  ')
  w_STAREG = space(1)
  w_INTLIG = space(1)
  w_PREFIS = space(20)
  w_PRPALG1 = 0
  w_CONCAS = space(1)
  w_DESCAU = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_CCOBSO = ctod('  /  /  ')
  w_LblAzie = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_slgPag1","gscg_slg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODESE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_LblAzie = this.oPgFrm.Pages(1).oPag.LblAzie
    DoDefault()
    proc Destroy()
      this.w_LblAzie = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='CAU_CONT'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCG_BLG with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_CODESE=space(4)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_TIPSTA=space(1)
      .w_PROLIG=0
      .w_CALPRO=space(1)
      .w_DATINR=ctod("  /  /  ")
      .w_PRPALG=0
      .w_DATFIR=ctod("  /  /  ")
      .w_SALPRO=space(1)
      .w_FLTEST=space(1)
      .w_CAUCONT=space(5)
      .w_FLTCRO=space(1)
      .w_DARLIG=0
      .w_AVELIG=0
      .w_TOTDAY=space(1)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_VALUTA=space(3)
      .w_STALIG=ctod("  /  /  ")
      .w_STAREG=space(1)
      .w_INTLIG=space(1)
      .w_PREFIS=space(20)
      .w_PRPALG1=0
      .w_CONCAS=space(1)
      .w_DESCAU=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_CCOBSO=ctod("  /  /  ")
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_CODESE = g_CODESE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODESE))
          .link_1_2('Full')
        endif
        .w_DATINI = .w_STALIG
        .w_DATFIN = MESESKIP(IIF(EMPTY(.w_DATINI), .w_INIESE, .w_DATINI+1),0,'F')
        .w_TIPSTA = 'P'
          .DoRTCalc(6,8,.f.)
        .w_PRPALG = IIF(YEAR(.w_DATFIN)<>YEAR(.w_DATINI) AND NOT EMPTY(.w_DATINI),0,.w_PRPALG1)
        .w_DATFIR = MESESKIP(IIF(EMPTY(.w_DATINR), .w_INIESE, .w_DATINR),0,'F')
        .w_SALPRO = ' '
        .DoRTCalc(12,13,.f.)
        if not(empty(.w_CAUCONT))
          .link_1_13('Full')
        endif
        .w_FLTCRO = ' '
      .oPgFrm.Page1.oPag.oObj_1_44.Calculate(IIF(.w_CONCAS='S', "Se attivo: ricalcola i progressivi registro cronologico da inizio esercizio (attenzione: aggiorna solo se stampa definitiva!)", "Se attivo: ricalcola i progressivi libro giornale da inizio esercizio (attenzione: aggiorna solo se stampa definitiva!)"))
      .oPgFrm.Page1.oPag.oObj_1_45.Calculate(IIF(.w_CONCAS='S', "Ultima data di stampa dei progressivi R.C.", "Ultima data di stampa dei progressivi L.G."))
      .oPgFrm.Page1.oPag.oObj_1_46.Calculate(IIF(.w_CONCAS='S', "Se stampa definitiva: aggiorna la data e i progressivi ultima stampa registro cronologico.", "Se stampa definitiva: aggiorna la data e i progressivi ultima stampa libro giornale."))
      .oPgFrm.Page1.oPag.oObj_1_47.Calculate(IIF(.w_CONCAS='S', "Ultima data di stampa dei progressivi R.C.", "Ultima data di stampa dei progressivi L.G."))
      .oPgFrm.Page1.oPag.oObj_1_48.Calculate(IIF(.w_CONCAS='S', "Ultima pagina stampata nel precedente registro cronologico", "Ultima pagina stampata nel precedente libro giornale"))
      .oPgFrm.Page1.oPag.oObj_1_49.Calculate(IIF(.w_CONCAS='S', "Ultimo numero di riga stampata sul registro cronologico.", "Ultimo numero di riga stampata sul giornale"))
      .oPgFrm.Page1.oPag.oObj_1_50.Calculate(IIF(.w_CONCAS='S', "Progressivo dare dei movimenti stampati sul registro cronologico.", "Progressivo dare dei movimenti stampati sul giornale"))
      .oPgFrm.Page1.oPag.oObj_1_51.Calculate(IIF(.w_CONCAS='S', "Progressivo avere dei movimenti stampati sul registro cronologico.", "Progressivo avere dei movimenti stampati sul giornale"))
      .oPgFrm.Page1.oPag.LblAzie.Calculate(IIF(.w_CONCAS='S',AH_MSGFORMAT("Progressivi R.C."),AH_MSGFORMAT("Progressivi L.G.")))
          .DoRTCalc(15,27,.f.)
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page1.oPag.oObj_1_58.Calculate(IIF(.w_CONCAS='S' AND .w_TIPSTA='P' AND ISALT(), .T., .F.))
    endwith
    this.DoRTCalc(29,29,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
        if .o_CALPRO<>.w_CALPRO.or. .o_CODESE<>.w_CODESE.or. .o_TIPSTA<>.w_TIPSTA
            .w_DATINI = .w_STALIG
        endif
        if .o_CODESE<>.w_CODESE.or. .o_DATINI<>.w_DATINI.or. .o_TIPSTA<>.w_TIPSTA
            .w_DATFIN = MESESKIP(IIF(EMPTY(.w_DATINI), .w_INIESE, .w_DATINI+1),0,'F')
        endif
        .DoRTCalc(5,8,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_PRPALG = IIF(YEAR(.w_DATFIN)<>YEAR(.w_DATINI) AND NOT EMPTY(.w_DATINI),0,.w_PRPALG1)
        endif
        if .o_CODESE<>.w_CODESE.or. .o_DATINR<>.w_DATINR.or. .o_TIPSTA<>.w_TIPSTA
            .w_DATFIR = MESESKIP(IIF(EMPTY(.w_DATINR), .w_INIESE, .w_DATINR),0,'F')
        endif
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate(IIF(.w_CONCAS='S', "Se attivo: ricalcola i progressivi registro cronologico da inizio esercizio (attenzione: aggiorna solo se stampa definitiva!)", "Se attivo: ricalcola i progressivi libro giornale da inizio esercizio (attenzione: aggiorna solo se stampa definitiva!)"))
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate(IIF(.w_CONCAS='S', "Ultima data di stampa dei progressivi R.C.", "Ultima data di stampa dei progressivi L.G."))
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate(IIF(.w_CONCAS='S', "Se stampa definitiva: aggiorna la data e i progressivi ultima stampa registro cronologico.", "Se stampa definitiva: aggiorna la data e i progressivi ultima stampa libro giornale."))
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate(IIF(.w_CONCAS='S', "Ultima data di stampa dei progressivi R.C.", "Ultima data di stampa dei progressivi L.G."))
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate(IIF(.w_CONCAS='S', "Ultima pagina stampata nel precedente registro cronologico", "Ultima pagina stampata nel precedente libro giornale"))
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate(IIF(.w_CONCAS='S', "Ultimo numero di riga stampata sul registro cronologico.", "Ultimo numero di riga stampata sul giornale"))
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate(IIF(.w_CONCAS='S', "Progressivo dare dei movimenti stampati sul registro cronologico.", "Progressivo dare dei movimenti stampati sul giornale"))
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(IIF(.w_CONCAS='S', "Progressivo avere dei movimenti stampati sul registro cronologico.", "Progressivo avere dei movimenti stampati sul giornale"))
        .oPgFrm.Page1.oPag.LblAzie.Calculate(IIF(.w_CONCAS='S',AH_MSGFORMAT("Progressivi R.C."),AH_MSGFORMAT("Progressivi L.G.")))
        if .o_TIPSTA<>.w_TIPSTA
          .Calculate_KHVNHWNSIC()
        endif
        if .o_TIPSTA<>.w_TIPSTA
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(IIF(.w_CONCAS='S' AND .w_TIPSTA='P' AND ISALT(), .T., .F.))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate(IIF(.w_CONCAS='S', "Se attivo: ricalcola i progressivi registro cronologico da inizio esercizio (attenzione: aggiorna solo se stampa definitiva!)", "Se attivo: ricalcola i progressivi libro giornale da inizio esercizio (attenzione: aggiorna solo se stampa definitiva!)"))
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate(IIF(.w_CONCAS='S', "Ultima data di stampa dei progressivi R.C.", "Ultima data di stampa dei progressivi L.G."))
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate(IIF(.w_CONCAS='S', "Se stampa definitiva: aggiorna la data e i progressivi ultima stampa registro cronologico.", "Se stampa definitiva: aggiorna la data e i progressivi ultima stampa libro giornale."))
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate(IIF(.w_CONCAS='S', "Ultima data di stampa dei progressivi R.C.", "Ultima data di stampa dei progressivi L.G."))
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate(IIF(.w_CONCAS='S', "Ultima pagina stampata nel precedente registro cronologico", "Ultima pagina stampata nel precedente libro giornale"))
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate(IIF(.w_CONCAS='S', "Ultimo numero di riga stampata sul registro cronologico.", "Ultimo numero di riga stampata sul giornale"))
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate(IIF(.w_CONCAS='S', "Progressivo dare dei movimenti stampati sul registro cronologico.", "Progressivo dare dei movimenti stampati sul giornale"))
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate(IIF(.w_CONCAS='S', "Progressivo avere dei movimenti stampati sul registro cronologico.", "Progressivo avere dei movimenti stampati sul giornale"))
        .oPgFrm.Page1.oPag.LblAzie.Calculate(IIF(.w_CONCAS='S',AH_MSGFORMAT("Progressivi R.C."),AH_MSGFORMAT("Progressivi L.G.")))
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate(IIF(.w_CONCAS='S' AND .w_TIPSTA='P' AND ISALT(), .T., .F.))
    endwith
  return

  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .Caption = IIF(.w_CONCAS='S',AH_MSGFORMAT("Stampa registro cronologico"),.Caption)
    endwith
  endproc
  proc Calculate_KHVNHWNSIC()
    with this
          * --- Azzera causale contabile
          .w_CAUCONT = IIF(.w_TIPSTA<>'P', space(5), .w_CAUCONT)
          .w_DESCAU = IIF(.w_TIPSTA<>'P', space(35), .w_DESCAU)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDATINI_1_3.enabled = this.oPgFrm.Page1.oPag.oDATINI_1_3.mCond()
    this.oPgFrm.Page1.oPag.oDATFIN_1_4.enabled = this.oPgFrm.Page1.oPag.oDATFIN_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCALPRO_1_7.enabled = this.oPgFrm.Page1.oPag.oCALPRO_1_7.mCond()
    this.oPgFrm.Page1.oPag.oDATINR_1_8.enabled = this.oPgFrm.Page1.oPag.oDATINR_1_8.mCond()
    this.oPgFrm.Page1.oPag.oPRPALG_1_9.enabled = this.oPgFrm.Page1.oPag.oPRPALG_1_9.mCond()
    this.oPgFrm.Page1.oPag.oDATFIR_1_10.enabled = this.oPgFrm.Page1.oPag.oDATFIR_1_10.mCond()
    this.oPgFrm.Page1.oPag.oSALPRO_1_11.enabled = this.oPgFrm.Page1.oPag.oSALPRO_1_11.mCond()
    this.oPgFrm.Page1.oPag.oFLTEST_1_12.enabled = this.oPgFrm.Page1.oPag.oFLTEST_1_12.mCond()
    this.oPgFrm.Page1.oPag.oFLTCRO_1_14.enabled = this.oPgFrm.Page1.oPag.oFLTCRO_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCALPRO_1_7.visible=!this.oPgFrm.Page1.oPag.oCALPRO_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDATINR_1_8.visible=!this.oPgFrm.Page1.oPag.oDATINR_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDATFIR_1_10.visible=!this.oPgFrm.Page1.oPag.oDATFIR_1_10.mHide()
    this.oPgFrm.Page1.oPag.oSALPRO_1_11.visible=!this.oPgFrm.Page1.oPag.oSALPRO_1_11.mHide()
    this.oPgFrm.Page1.oPag.oFLTEST_1_12.visible=!this.oPgFrm.Page1.oPag.oFLTEST_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCAUCONT_1_13.visible=!this.oPgFrm.Page1.oPag.oCAUCONT_1_13.mHide()
    this.oPgFrm.Page1.oPag.oFLTCRO_1_14.visible=!this.oPgFrm.Page1.oPag.oFLTCRO_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oSTAREG_1_35.visible=!this.oPgFrm.Page1.oPag.oSTAREG_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oDESCAU_1_54.visible=!this.oPgFrm.Page1.oPag.oDESCAU_1_54.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_45.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_48.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
      .oPgFrm.Page1.oPag.LblAzie.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZSTALIG,AZINTLIG,AZPRPALG,AZPREFLG,AZCONCAS";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZSTALIG,AZINTLIG,AZPRPALG,AZPREFLG,AZCONCAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_STALIG = NVL(cp_ToDate(_Link_.AZSTALIG),ctod("  /  /  "))
      this.w_INTLIG = NVL(_Link_.AZINTLIG,space(1))
      this.w_PRPALG1 = NVL(_Link_.AZPRPALG,0)
      this.w_PREFIS = NVL(_Link_.AZPREFLG,space(20))
      this.w_CONCAS = NVL(_Link_.AZCONCAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_STALIG = ctod("  /  /  ")
      this.w_INTLIG = space(1)
      this.w_PRPALG1 = 0
      this.w_PREFIS = space(20)
      this.w_CONCAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESPROLIG,ESDARLIG,ESAVELIG,ESINIESE,ESVALNAZ,ESFLSTAM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESFINESE,ESPROLIG,ESDARLIG,ESAVELIG,ESINIESE,ESVALNAZ,ESFLSTAM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_2'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESPROLIG,ESDARLIG,ESAVELIG,ESINIESE,ESVALNAZ,ESFLSTAM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESFINESE,ESPROLIG,ESDARLIG,ESAVELIG,ESINIESE,ESVALNAZ,ESFLSTAM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESPROLIG,ESDARLIG,ESAVELIG,ESINIESE,ESVALNAZ,ESFLSTAM";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESFINESE,ESPROLIG,ESDARLIG,ESAVELIG,ESINIESE,ESVALNAZ,ESFLSTAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESPROLIG,ESDARLIG,ESAVELIG,ESINIESE,ESVALNAZ,ESFLSTAM";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESFINESE,ESPROLIG,ESDARLIG,ESAVELIG,ESINIESE,ESVALNAZ,ESFLSTAM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_PROLIG = NVL(_Link_.ESPROLIG,0)
      this.w_DARLIG = NVL(_Link_.ESDARLIG,0)
      this.w_AVELIG = NVL(_Link_.ESAVELIG,0)
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_VALUTA = NVL(_Link_.ESVALNAZ,space(3))
      this.w_TOTDAY = NVL(_Link_.ESFLSTAM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_FINESE = ctod("  /  /  ")
      this.w_PROLIG = 0
      this.w_DARLIG = 0
      this.w_AVELIG = 0
      this.w_INIESE = ctod("  /  /  ")
      this.w_VALUTA = space(3)
      this.w_TOTDAY = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUCONT
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUCONT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gscg_acc',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CAUCONT)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CAUCONT))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUCONT)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CAUCONT)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CAUCONT)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAUCONT) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCAUCONT_1_13'),i_cWhere,'gscg_acc',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUCONT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUCONT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUCONT)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUCONT = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_CCOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CAUCONT = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_CCOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CCOBSO>.w_OBTEST OR EMPTY(.w_CCOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CAUCONT = space(5)
        this.w_DESCAU = space(35)
        this.w_CCOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUCONT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_2.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_2.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_3.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_3.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_4.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_4.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPSTA_1_5.RadioValue()==this.w_TIPSTA)
      this.oPgFrm.Page1.oPag.oTIPSTA_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROLIG_1_6.value==this.w_PROLIG)
      this.oPgFrm.Page1.oPag.oPROLIG_1_6.value=this.w_PROLIG
    endif
    if not(this.oPgFrm.Page1.oPag.oCALPRO_1_7.RadioValue()==this.w_CALPRO)
      this.oPgFrm.Page1.oPag.oCALPRO_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINR_1_8.value==this.w_DATINR)
      this.oPgFrm.Page1.oPag.oDATINR_1_8.value=this.w_DATINR
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPALG_1_9.value==this.w_PRPALG)
      this.oPgFrm.Page1.oPag.oPRPALG_1_9.value=this.w_PRPALG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIR_1_10.value==this.w_DATFIR)
      this.oPgFrm.Page1.oPag.oDATFIR_1_10.value=this.w_DATFIR
    endif
    if not(this.oPgFrm.Page1.oPag.oSALPRO_1_11.RadioValue()==this.w_SALPRO)
      this.oPgFrm.Page1.oPag.oSALPRO_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTEST_1_12.RadioValue()==this.w_FLTEST)
      this.oPgFrm.Page1.oPag.oFLTEST_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUCONT_1_13.value==this.w_CAUCONT)
      this.oPgFrm.Page1.oPag.oCAUCONT_1_13.value=this.w_CAUCONT
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTCRO_1_14.RadioValue()==this.w_FLTCRO)
      this.oPgFrm.Page1.oPag.oFLTCRO_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDARLIG_1_15.value==this.w_DARLIG)
      this.oPgFrm.Page1.oPag.oDARLIG_1_15.value=this.w_DARLIG
    endif
    if not(this.oPgFrm.Page1.oPag.oAVELIG_1_16.value==this.w_AVELIG)
      this.oPgFrm.Page1.oPag.oAVELIG_1_16.value=this.w_AVELIG
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAREG_1_35.RadioValue()==this.w_STAREG)
      this.oPgFrm.Page1.oPag.oSTAREG_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPREFIS_1_39.value==this.w_PREFIS)
      this.oPgFrm.Page1.oPag.oPREFIS_1_39.value=this.w_PREFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_54.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_54.value=this.w_DESCAU
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODESE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATFIN)) or not(.w_DATFIN>.w_DATINI))  and (.w_TIPSTA<>'R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di fine selezione deve essere maggiore dell'ultima stampa")
          case   ((empty(.w_DATFIR)) or not(.w_DATFIR>=.w_DATINR))  and not(.w_TIPSTA<>'R')  and (.w_TIPSTA='R')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIR_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DATFIR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di fine selezione deve essere maggiore o uguale alla data di inizio selezione")
          case   not(.w_CCOBSO>.w_OBTEST OR EMPTY(.w_CCOBSO))  and not(NOT (.w_CONCAS='S' AND .w_TIPSTA='P' AND ISALT()))  and not(empty(.w_CAUCONT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUCONT_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODESE = this.w_CODESE
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    this.o_TIPSTA = this.w_TIPSTA
    this.o_CALPRO = this.w_CALPRO
    this.o_DATINR = this.w_DATINR
    return

enddefine

* --- Define pages as container
define class tgscg_slgPag1 as StdContainer
  Width  = 557
  height = 293
  stdWidth  = 557
  stdheight = 293
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODESE_1_2 as StdField with uid="GBCGSHZJTO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio da cui si ricavano i progressivi - non fa filtro sulla stampa",;
    HelpContextID = 235892262,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=85, Top=16, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDATINI_1_3 as StdField with uid="PCZHOYRAWF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di stampa dei progressivi L.G.",;
    HelpContextID = 29646902,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=398, Top=82

  func oDATINI_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_TIPSTA<>'R' AND .w_CALPRO='S') OR (.w_CONCAS='S' AND .w_TIPSTA='P' AND ISALT()))
    endwith
   endif
  endfunc

  add object oDATFIN_1_4 as StdField with uid="UEEIMPUFEH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di fine selezione deve essere maggiore dell'ultima stampa",;
    ToolTipText = "Data registrazione di fine stampa",;
    HelpContextID = 108093494,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=398, Top=16

  func oDATFIN_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA<>'R')
    endwith
   endif
  endfunc

  func oDATFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFIN>.w_DATINI)
    endwith
    return bRes
  endfunc


  add object oTIPSTA_1_5 as StdCombo with uid="BANKRFMPHZ",rtseq=5,rtrep=.f.,left=32,top=82,width=124,height=20;
    , ToolTipText = "Se stampa definitiva: aggiorna la data e i progressivi ultima stampa libro giornale.";
    , HelpContextID = 170797366;
    , cFormVar="w_TIPSTA",RowSource=""+"Prova,"+"Definitiva,"+"Ristampa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPSTA_1_5.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oTIPSTA_1_5.GetRadio()
    this.Parent.oContained.w_TIPSTA = this.RadioValue()
    return .t.
  endfunc

  func oTIPSTA_1_5.SetRadio()
    this.Parent.oContained.w_TIPSTA=trim(this.Parent.oContained.w_TIPSTA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSTA=='P',1,;
      iif(this.Parent.oContained.w_TIPSTA=='D',2,;
      iif(this.Parent.oContained.w_TIPSTA=='R',3,;
      0)))
  endfunc

  add object oPROLIG_1_6 as StdField with uid="ZIULRSKREG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PROLIG", cQueryName = "PROLIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultimo numero di riga stampata sul giornale",;
    HelpContextID = 259465718,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=398, Top=136, cSayPict='"999999999"', cGetPict='"999999999"'

  add object oCALPRO_1_7 as StdCheck with uid="UEGZQBPNBU",rtseq=7,rtrep=.f.,left=32, top=109, caption="Ricalcola progressivi",;
    ToolTipText = "Se attivo: ricalcola i progressivi libro giornale da inizio esercizio (attenzione: aggiorna solo se stampa definitiva!)",;
    HelpContextID = 134930470,;
    cFormVar="w_CALPRO", bObbl = .f. , nPag = 1;
    , TabStop=.F.;
   , bGlobalFont=.t.


  func oCALPRO_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCALPRO_1_7.GetRadio()
    this.Parent.oContained.w_CALPRO = this.RadioValue()
    return .t.
  endfunc

  func oCALPRO_1_7.SetRadio()
    this.Parent.oContained.w_CALPRO=trim(this.Parent.oContained.w_CALPRO)
    this.value = ;
      iif(this.Parent.oContained.w_CALPRO=='S',1,;
      0)
  endfunc

  func oCALPRO_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA<>'R')
    endwith
   endif
  endfunc

  func oCALPRO_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPSTA='R')
    endwith
  endfunc

  add object oDATINR_1_8 as StdField with uid="RXDLEZZTNS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATINR", cQueryName = "DATINR",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di stampa dei progressivi L.G.",;
    HelpContextID = 180641846,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=98, Top=109

  func oDATINR_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA='R')
    endwith
   endif
  endfunc

  func oDATINR_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPSTA<>'R')
    endwith
  endfunc

  add object oPRPALG_1_9 as StdField with uid="GZVSBCMEQD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PRPALG", cQueryName = "PRPALG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultima pagina stampata nel precedente libro giornale",;
    HelpContextID = 261894646,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=398, Top=109, cSayPict='"9999999"', cGetPict='"9999999"'

  func oPRPALG_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INTLIG='S')
    endwith
   endif
  endfunc

  add object oDATFIR_1_10 as StdField with uid="TNXHXEBKDX",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATFIR", cQueryName = "DATFIR",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di fine selezione deve essere maggiore o uguale alla data di inizio selezione",;
    ToolTipText = "Data registrazione di fine stampa",;
    HelpContextID = 175202358,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=98, Top=136

  func oDATFIR_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA='R')
    endwith
   endif
  endfunc

  func oDATFIR_1_10.mHide()
    with this.Parent.oContained
      return (.w_TIPSTA<>'R')
    endwith
  endfunc

  func oDATFIR_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFIR>=.w_DATINR)
    endwith
    return bRes
  endfunc

  add object oSALPRO_1_11 as StdCheck with uid="EVXPZWMAIF",rtseq=11,rtrep=.f.,left=32, top=161, caption="Saldi progressivi",;
    ToolTipText = "Se attivo: vengono stampati i saldi progressivi nella testata di ogni pagina",;
    HelpContextID = 134930726,;
    cFormVar="w_SALPRO", bObbl = .f. , nPag = 1;
    , TabStop=.F.;
   , bGlobalFont=.t.


  func oSALPRO_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSALPRO_1_11.GetRadio()
    this.Parent.oContained.w_SALPRO = this.RadioValue()
    return .t.
  endfunc

  func oSALPRO_1_11.SetRadio()
    this.Parent.oContained.w_SALPRO=trim(this.Parent.oContained.w_SALPRO)
    this.value = ;
      iif(this.Parent.oContained.w_SALPRO=='S',1,;
      0)
  endfunc

  func oSALPRO_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CONCAS='S')
    endwith
   endif
  endfunc

  func oSALPRO_1_11.mHide()
    with this.Parent.oContained
      return (.w_CONCAS='S')
    endwith
  endfunc

  add object oFLTEST_1_12 as StdCheck with uid="ACSRRSYCGV",rtseq=12,rtrep=.f.,left=32, top=189, caption="Stampa solo testo",;
    ToolTipText = "Se attivo: stampa su modulo solo testo",;
    HelpContextID = 219179862,;
    cFormVar="w_FLTEST", bObbl = .f. , nPag = 1;
    , TabStop=.F.;
   , bGlobalFont=.t.


  func oFLTEST_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLTEST_1_12.GetRadio()
    this.Parent.oContained.w_FLTEST = this.RadioValue()
    return .t.
  endfunc

  func oFLTEST_1_12.SetRadio()
    this.Parent.oContained.w_FLTEST=trim(this.Parent.oContained.w_FLTEST)
    this.value = ;
      iif(this.Parent.oContained.w_FLTEST=='S',1,;
      0)
  endfunc

  func oFLTEST_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_CONCAS='S')
    endwith
   endif
  endfunc

  func oFLTEST_1_12.mHide()
    with this.Parent.oContained
      return (.w_CONCAS='S')
    endwith
  endfunc

  proc oFLTEST_1_12.mAfter
    with this.Parent.oContained
      .w_STAREG='V'
    endwith
  endproc

  add object oCAUCONT_1_13 as StdField with uid="LKSYXVAPDM",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CAUCONT", cQueryName = "CAUCONT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    ToolTipText = "Codice causale contabile",;
    HelpContextID = 114192422,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=57, Top=190, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="gscg_acc", oKey_1_1="CCCODICE", oKey_1_2="this.w_CAUCONT"

  func oCAUCONT_1_13.mHide()
    with this.Parent.oContained
      return (NOT (.w_CONCAS='S' AND .w_TIPSTA='P' AND ISALT()))
    endwith
  endfunc

  func oCAUCONT_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUCONT_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUCONT_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCAUCONT_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'gscg_acc',"Causali contabili",'',this.parent.oContained
  endproc
  proc oCAUCONT_1_13.mZoomOnZoom
    local i_obj
    i_obj=gscg_acc()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CAUCONT
     i_obj.ecpSave()
  endproc

  add object oFLTCRO_1_14 as StdCheck with uid="MYYUQPQJSR",rtseq=14,rtrep=.f.,left=32, top=161, caption="Stampa totali cronologico",;
    ToolTipText = "Se attivo: stampa totali cronologico",;
    HelpContextID = 134114134,;
    cFormVar="w_FLTCRO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLTCRO_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLTCRO_1_14.GetRadio()
    this.Parent.oContained.w_FLTCRO = this.RadioValue()
    return .t.
  endfunc

  func oFLTCRO_1_14.SetRadio()
    this.Parent.oContained.w_FLTCRO=trim(this.Parent.oContained.w_FLTCRO)
    this.value = ;
      iif(this.Parent.oContained.w_FLTCRO=='S',1,;
      0)
  endfunc

  func oFLTCRO_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CONCAS='S')
    endwith
   endif
  endfunc

  func oFLTCRO_1_14.mHide()
    with this.Parent.oContained
      return (NOT .w_CONCAS='S')
    endwith
  endfunc

  add object oDARLIG_1_15 as StdField with uid="YKYWHVMPVA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DARLIG", cQueryName = "DARLIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo dare dei movimenti stampati sul giornale",;
    HelpContextID = 259473462,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=398, Top=190, cSayPict="v_PV(19)", cGetPict="v_GV(19)"

  add object oAVELIG_1_16 as StdField with uid="BEJFRMSNVS",rtseq=16,rtrep=.f.,;
    cFormVar = "w_AVELIG", cQueryName = "AVELIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo avere dei movimenti stampati sul giornale",;
    HelpContextID = 259425542,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=398, Top=217, cSayPict="v_PV(19)", cGetPict="v_GV(19)"


  add object oBtn_1_18 as StdButton with uid="ODNOHOGNYR",left=452, top=243, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 208254234;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        do GSCG_BLG with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((not empty(.w_CODESE)) AND (not empty(.w_DATFIN)))
      endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="XWGDVOSIRO",left=502, top=243, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 200965562;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oSTAREG_1_35 as StdCombo with uid="ODCBMECLIH",rtseq=22,rtrep=.f.,left=124,top=216,width=122,height=20;
    , ToolTipText = "Formato dei moduli di stampa";
    , HelpContextID = 255607846;
    , cFormVar="w_STAREG",RowSource=""+"Orizzontale,"+"Verticale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTAREG_1_35.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'V',;
    space(1))))
  endfunc
  func oSTAREG_1_35.GetRadio()
    this.Parent.oContained.w_STAREG = this.RadioValue()
    return .t.
  endfunc

  func oSTAREG_1_35.SetRadio()
    this.Parent.oContained.w_STAREG=trim(this.Parent.oContained.w_STAREG)
    this.value = ;
      iif(this.Parent.oContained.w_STAREG=='O',1,;
      iif(this.Parent.oContained.w_STAREG=='V',2,;
      0))
  endfunc

  func oSTAREG_1_35.mHide()
    with this.Parent.oContained
      return (.w_FLTEST<>'S')
    endwith
  endfunc

  add object oPREFIS_1_39 as StdField with uid="SJBUMXHJGU",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PREFIS", cQueryName = "PREFIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso che comparirÓ nella stampa prima del numero di pagina",;
    HelpContextID = 191922678,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=398, Top=163, InputMask=replicate('X',20)


  add object oObj_1_44 as cp_setobjprop with uid="HXGWXJKQFL",left=3, top=335, width=175,height=21,;
    caption='ToolTip di w_CALPRO',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_CALPRO",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 71830046


  add object oObj_1_45 as cp_setobjprop with uid="RQWJFYSKJF",left=3, top=359, width=175,height=21,;
    caption='ToolTip di w_DATINR',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_DATINR",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 55041670


  add object oObj_1_46 as cp_setobjprop with uid="IBRQSRGXND",left=3, top=383, width=175,height=21,;
    caption='ToolTip di w_TIPSTA',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_TIPSTA",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 213325846


  add object oObj_1_47 as cp_setobjprop with uid="GMBGAPJKGM",left=3, top=407, width=175,height=21,;
    caption='ToolTip di w_DATINI',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_DATINI",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 55078534


  add object oObj_1_48 as cp_setobjprop with uid="WDJDYJOFQO",left=3, top=431, width=175,height=21,;
    caption='ToolTip di w_PRPALG',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_PRPALG",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 122196234


  add object oObj_1_49 as cp_setobjprop with uid="CJUGJVGXQY",left=3, top=455, width=175,height=21,;
    caption='ToolTip di w_PROLIG',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_PROLIG",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 122196827


  add object oObj_1_50 as cp_setobjprop with uid="VHNVJMRKDH",left=3, top=479, width=175,height=21,;
    caption='ToolTip di w_DARLIG',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_DARLIG",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 55087960


  add object oObj_1_51 as cp_setobjprop with uid="LEBLREVUCY",left=3, top=503, width=175,height=21,;
    caption='ToolTip di w_AVELIG',;
   bGlobalFont=.t.,;
    cProp="ToolTipText",cObj="w_AVELIG",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 105419621


  add object LblAzie as cp_calclbl with uid="KZLEWMBSIL",left=305, top=54, width=212,height=25,;
    caption='LblProgressiviLG',;
   bGlobalFont=.t.,;
    caption="label text",fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,Alignment=0,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 86135873

  add object oDESCAU_1_54 as StdField with uid="ERTJOCHVDD",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 216945718,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=107, Top=190, InputMask=replicate('X',35)

  func oDESCAU_1_54.mHide()
    with this.Parent.oContained
      return (NOT (.w_CONCAS='S' AND .w_TIPSTA='P' AND ISALT()))
    endwith
  endfunc


  add object oObj_1_58 as cp_setobjprop with uid="IPXGWBQNVP",left=500, top=345, width=170,height=21,;
    caption='TabStop di w_DATINI',;
   bGlobalFont=.t.,;
    cProp="Tabstop",cObj="w_DATINI",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 190991494

  add object oStr_1_22 as StdString with uid="AHNZOTPILL",Visible=.t., Left=15, Top=16,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="KGJDWEHWKM",Visible=.t., Left=312, Top=136,;
    Alignment=1, Width=84, Height=15,;
    Caption="Ultima riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="DCDPSJGSPZ",Visible=.t., Left=299, Top=82,;
    Alignment=1, Width=97, Height=15,;
    Caption="Ultima stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="BEGYETGCDR",Visible=.t., Left=312, Top=192,;
    Alignment=1, Width=84, Height=18,;
    Caption="DARE:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="UHBUOSZIEW",Visible=.t., Left=346, Top=219,;
    Alignment=1, Width=50, Height=18,;
    Caption="AVERE:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="DIAZCDOEGQ",Visible=.t., Left=211, Top=16,;
    Alignment=1, Width=185, Height=18,;
    Caption="Stampa reg. fino alla data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="BCLQLFAPER",Visible=.t., Left=32, Top=54,;
    Alignment=0, Width=128, Height=15,;
    Caption="Opzioni di stampa"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="EJCQAUPUWZ",Visible=.t., Left=5, Top=110,;
    Alignment=1, Width=88, Height=18,;
    Caption="Dalla data:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_TIPSTA<>'R')
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="WEHTUVMVWS",Visible=.t., Left=23, Top=136,;
    Alignment=1, Width=70, Height=18,;
    Caption="Alla data:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (.w_TIPSTA<>'R')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="IZCTKFJGKB",Visible=.t., Left=32, Top=216,;
    Alignment=1, Width=89, Height=15,;
    Caption="Modulo stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_FLTEST<>'S')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="FWNKOATCCI",Visible=.t., Left=312, Top=109,;
    Alignment=1, Width=84, Height=15,;
    Caption="Ultima pagina:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="QBIQLOVEJE",Visible=.t., Left=286, Top=163,;
    Alignment=1, Width=110, Height=15,;
    Caption="Prefisso num. pag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="TWJAWQDAXS",Visible=.t., Left=4, Top=192,;
    Alignment=1, Width=50, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (NOT (.w_CONCAS='S' AND .w_TIPSTA='P' AND ISALT()))
    endwith
  endfunc

  add object oBox_1_28 as StdBox with uid="PHJOCSMJKJ",left=32, top=71, width=133,height=1

  add object oBox_1_30 as StdBox with uid="UOOTIQSYGI",left=301, top=71, width=245,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_slg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
