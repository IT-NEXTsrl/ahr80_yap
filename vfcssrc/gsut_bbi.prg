* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bbi                                                        *
*              Gestione indice documenti                                       *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-25                                                      *
* Last revis.: 2012-06-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bbi",oParentObject,m.pPARAM)
return(i_retval)

define class tgsut_bbi as StdBatch
  * --- Local variables
  pPARAM = space(10)
  w_GSUT_MAA = .NULL.
  w_TMPN = 0
  w_TMPD = ctod("  /  /  ")
  w_VALPRED = space(100)
  w_TMPG = space(100)
  w_TMPC = space(100)
  w_TmpPat = space(10)
  w_PERCORSOARCH = space(100)
  w_FOLDER = space(50)
  w_posizpunto = 0
  w_estensione = space(10)
  w_destinazione = space(250)
  w_PATHABS = space(254)
  w_PADRE = .NULL.
  w_DESCRI = space(90)
  w_CURROW = 0
  w_VALATT = space(100)
  w_OKFILE = .f.
  w_Ok = space(10)
  w_SHELLEXEC = 0
  w_cLink = space(255)
  w_cParms = space(10)
  w_COPATHDO = space(100)
  w_STATO = space(1)
  w_SHELLEXEC = 0
  w_cLink = space(255)
  w_cParms = space(10)
  w_ABSPATH = space(255)
  w_GENPRE = space(1)
  w_READAZI = space(5)
  w_GSAG_KPR = .NULL.
  w_MODSCHED = space(1)
  w_GSAG_KPM = .NULL.
  * --- WorkFile variables
  PRODCLAS_idx=0
  PROMINDI_idx=0
  CONTROPA_idx=0
  CAN_TIER_idx=0
  PRODINDI_idx=0
  PAR_PRAT_idx=0
  PAR_ALTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione INDICI DOCUMENTI
    * --- Azioni ................
    do case
      case this.pPARAM = "CARICA"
        * --- Punta al padre e pulisce cursore movimentazione
        this.w_GSUT_MAA = this.oParentObject.GSUT_MAA
        select (this.w_GSUT_MAA.cTrsName)
        if ! empty(this.oParentObject.w_IDCLADOC)
          ZAP
          GO TOP
        endif
        * --- ===================================================================
        * --- Select from PRODCLAS
        i_nConn=i_TableProp[this.PRODCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" PRODCLAS ";
              +" where CDCODCLA = "+cp_ToStrODBC(this.oParentObject.w_IDCLADOC)+"";
              +" order by CPROWORD";
               ,"_Curs_PRODCLAS")
        else
          select * from (i_cTable);
           where CDCODCLA = this.oParentObject.w_IDCLADOC;
           order by CPROWORD;
            into cursor _Curs_PRODCLAS
        endif
        if used('_Curs_PRODCLAS')
          select _Curs_PRODCLAS
          locate for 1=1
          do while not(eof())
          * --- Verifica valore predefinito
          this.w_VALPRED = nvl(_Curs_PRODCLAS.CDVLPRED, " ")
          * --- Carica riga
          this.w_GSUT_MAA.InitRow()     
          this.w_GSUT_MAA.w_CPROWNUM = _Curs_PRODCLAS.CPROWNUM
          this.w_GSUT_MAA.w_CPROWORD = _Curs_PRODCLAS.CPROWORD
          this.w_GSUT_MAA.w_IDCODATT = _Curs_PRODCLAS.CDCODATT
          this.w_GSUT_MAA.w_IDDESATT = _Curs_PRODCLAS.CDDESATT
          this.w_GSUT_MAA.w_IDTIPATT = _Curs_PRODCLAS.CDTIPATT
          this.w_GSUT_MAA.w_IDCHKOBB = _Curs_PRODCLAS.CDCHKOBB
          this.w_GSUT_MAA.w_IDTABKEY = _Curs_PRODCLAS.CDTABKEY
          if empty(this.w_VALPRED)
            this.w_GSUT_MAA.w_IDVALATT = space(100)
            this.w_GSUT_MAA.w_IDVALDAT = cp_CharToDate("  -  -  ")
            this.w_GSUT_MAA.w_IDVALNUM = 0
          else
            * --- Esegue controllo
            w_ERRORE = .F.
            w_ErrorHandler = on("ERROR")
            this.w_TMPG = eval(this.w_VALPRED)
            on error &w_ErrorHandler
            do case
              case this.oParentObject.w_IDTIPATT="N"
                this.w_GSUT_MAA.w_IDVALATT = padr(alltrim(str(this.w_TMPG)),100)
                this.w_GSUT_MAA.w_IDVALDAT = cp_CharToDate("  -  -  ")
                this.w_GSUT_MAA.w_IDVALNUM = this.w_TMPG
              case this.oParentObject.w_IDTIPATT="D"
                this.w_GSUT_MAA.w_IDVALATT = padr(DTOC(this.w_TMPG),100)
                this.w_GSUT_MAA.w_IDVALDAT = this.w_TMPG
                this.w_GSUT_MAA.w_IDVALNUM = 0
              otherwise
                this.w_GSUT_MAA.w_IDVALDAT = cp_CharToDate("  -  -  ")
                this.w_GSUT_MAA.w_IDVALNUM = 0
            endcase
          endif
          * --- Carico temporaneo
          this.w_GSUT_MAA.TrsFromWork()     
            select _Curs_PRODCLAS
            continue
          enddo
          use
        endif
        * --- ===================================================================
        SELECT (this.oParentObject.GSUT_MAA.cTrsName)
        GO TOP
        With this.oParentObject.GSUT_MAA
        .WorkFromTrs()
        .SetControlsValue()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        EndWith
      case this.pPARAM="VALORE"
        * --- Esegue controllo
        w_ERRORE = .F.
        w_ErrorHandler = on("ERROR")
        do case
          case this.oParentObject.w_IDTIPATT="N"
            on error w_ERRORE = .T.
            this.w_TMPN = val(this.oParentObject.w_IDVALATT)
            on error &w_ErrorHandler
            if w_ERRORE or (this.w_TMPN=0 and not empty(this.oParentObject.w_IDVALATT))
              ah_ErrorMSG("Il valore inserito � incompatibile con l'attributo di tipo <Numerico>",48)
              this.oParentObject.w_IDVALATT = space(100)
            else
              this.oParentObject.w_IDVALATT = padr(ALLTRIM(str(this.w_TMPN)),100)
            endif
          case this.oParentObject.w_IDTIPATT="D"
            on error w_ERRORE = .T.
            this.w_TMPD = cp_CharToDate(this.oParentObject.w_IDVALATT)
            on error &w_ErrorHandler
            if w_ERRORE or (empty(this.w_TMPD) and not empty(this.oParentObject.w_IDVALATT))
              ah_ErrorMSG("Il valore inserito � incompatibile con l'attributo di tipo <Data>",48)
              this.oParentObject.w_IDVALATT = space(100)
            else
              this.oParentObject.w_IDVALATT = padr(dtoc(this.w_TMPD),100)
            endif
        endcase
        this.w_PADRE = This.oparentobject
        if Isalt() and this.w_PADRE.w_IDTABKEY="CAN_TIER" and this.w_PADRE.w_IDCODATT="CODICE"
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNDESCAN"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(this.w_PADRE.w_IDVALATT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNDESCAN;
              from (i_cTable) where;
                  CNCODCAN = this.w_PADRE.w_IDVALATT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESCRI = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_PADRE = This.oparentobject
          this.w_CURROW = this.w_PADRE.Search("t_IDCODATT='DESCRIZIONE'"+"and not deleted()")
          this.w_PADRE.MarkPos()     
          if this.w_CURROW>0
            * --- Marco riga scelta come riga collegata a spese\anticipazioni
            this.w_PADRE.Setrow(this.w_CURROW)     
            this.w_PADRE.Set("w_IDVALATT" , this.w_DESCRI, .T. ,.T.)     
            this.w_PADRE.SaveRow()     
          endif
          this.w_PADRE.RePos(.T.)     
        endif
      case this.pPARAM = "ARCHIVIA"
        this.w_TMPC = GETFILE()
        if not empty(this.w_TMPC)
          this.oParentObject.w_IDORIFIL = this.w_TMPC
          this.oParentObject.w_IDORIGIN = IIF(empty(this.oParentObject.w_IDORIFIL), space(100), substr(this.oParentObject.w_IDORIFIL ,RAT("\", this.oParentObject.w_IDORIFIL)+1))
          this.oParentObject.w_IDPATALL = substr(this.oParentObject.w_IDORIFIL,1,rat("\",this.oParentObject.w_IDORIFIL))
          this.oParentObject.w_IDNOMFIL = IIF(empty(this.oParentObject.w_IDORIFIL), space(100), this.oParentObject.w_IDSERIAL+"_"+substr(this.oParentObject.w_IDORIFIL ,RAT("\", this.oParentObject.w_IDORIFIL)+1))
          this.oParentObject.w_IDTIPFIL = lower(substr(this.oParentObject.w_IDNOMFIL, rat(".", this.oParentObject.w_IDNOMFIL)))
        endif
      case this.pPARAM = "VISUALIZZA" OR this.pPARAM = "PATHALLEGATO"
        if this.pPARAM = "PATHALLEGATO" OR SUBSTR( CHKPECLA( this.oParentObject.w_IDCLADOC, i_CODUTE), 1, 1 ) = "S"
          do case
            case this.oParentObject.w_IDMODALL="F"
              if empty(this.oParentObject.w_IDPATALL)
                this.w_TMPC = DCMPATAR(this.oParentObject.w_PATSTD, this.oParentObject.w_TIPRAG, this.oParentObject.w_IDDATRAG, this.oParentObject.w_IDCLADOC)
                this.w_TmpPat = this.w_TMPC+this.oParentObject.w_IDNOMFIL
              else
                this.w_TMPC = alltrim(this.oParentObject.w_IDPATALL)
                this.w_TmpPat = this.w_TMPC+this.oParentObject.w_IDNOMFIL
              endif
            case this.oParentObject.w_IDMODALL="E"
              FileEds("V", this.oParentObject.w_IDCLADOC, this.oParentObject.w_IDORIGIN, alltrim(tempadhoc())+"\"+alltrim(this.oParentObject.w_IDNOMFIL))
              this.w_TmpPat = alltrim(tempadhoc())+"\"+alltrim(this.oParentObject.w_IDNOMFIL)
            case this.oParentObject.w_IDMODALL="I"
              this.w_TmpPat = FileInfinity("V", this.oParentObject.w_IDSERIAL)
            otherwise
              this.w_TMPC = LEFT(this.oParentObject.w_IDORIFIL, RAT("\",this.oParentObject.w_IDORIFIL))
              this.w_TmpPat = this.oParentObject.w_IDORIFIL
          endcase
          if this.pPARAM = "PATHALLEGATO"
            i_retcode = 'stop'
            i_retval = this.w_TmpPat
            return
          else
            if file(alltrim(this.w_TMPPAT)) OR this.oParentObject.w_IDMODALL="I"
              this.w_SHELLEXEC = 0
              if Isalt() and Type("This.oparentobject.GSUT_MAA")="O"
                * --- Read from PRODINDI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PRODINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "IDVALATT"+;
                    " from "+i_cTable+" PRODINDI where ";
                        +"IDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IDSERIAL);
                        +" and IDTABKEY = "+cp_ToStrODBC("CAN_TIER");
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    IDVALATT;
                    from (i_cTable) where;
                        IDSERIAL = this.oParentObject.w_IDSERIAL;
                        and IDTABKEY = "CAN_TIER";
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_VALATT = NVL(cp_ToDate(_read_.IDVALATT),cp_NullValue(_read_.IDVALATT))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if Not Empty(this.w_VALATT)
                  this.w_OKFILE = .f.
                  this.w_OKFILE = gsal_bse(.Null.,this.w_VALATT)
                  if !this.w_OKFILE
                    ah_ErrorMsg("Errore nella creazione dbf  __WORD__!",48)
                    i_retcode = 'stop'
                    return
                  endif
                endif
              endif
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              AH_ERRORMSG("Impossibile visualizzare il file %1%3Il file non � presente nella cartella <%2>",48,"",this.oParentObject.w_IDNOMFIL,this.w_TMPC,CHR(13))
            endif
          endif
        else
          if g_DOCM="S"
            ah_ErrorMsg("Non si possiedono le autorizzazioni per visualizzare il file selezionato",48)
          else
            ah_ErrorMsg("Dati non inseriti",48)
          endif
        endif
      case this.pPARAM = "ARFISICA"
        if file(this.oParentObject.w_IDORIFIL)
          if this.oParentObject.w_IDMODALL="F"
            this.w_PERCORSOARCH = DCMPATAR(this.oParentObject.w_PATSTD, this.oParentObject.w_TIPRAG, this.oParentObject.w_IDDATRAG, this.oParentObject.w_IDCLADOC)
            if (.not.directory(alltrim( this.w_PERCORSOARCH )))
              * --- Creazione directory immagini (LA GENERALE)
              this.w_FOLDER = left(this.w_PERCORSOARCH , len(this.w_PERCORSOARCH )-1)
              * --- Crea la cartella specificata nella variabile w_FOLDER
              *     Imposta la variabile w_ERRORE in caso di errore.
              w_ERRORE = .F.
              w_ErrorHandler = on("ERROR")
              on error w_ERRORE = .T.
              md (this.w_FOLDER)
              on error &w_ErrorHandler
              if w_ERRORE
                AH_ERRORMSG("Impossibile creare la cartella %1",48,"",this.w_FOLDER)
                i_retcode = 'stop'
                return
              else
                ah_msg("Creata nuova cartella %1",.t.,.f.,.f., this.w_FOLDER)
              endif
            endif
            * --- Determina la posizione del punto prima dell'estensione e l'estensione stessa
            this.w_posizpunto = rat(".", this.oParentObject.w_IDORIFIL)
            this.w_estensione = lower(substr(this.oParentObject.w_IDORIFIL,this.w_posizpunto))
            * --- Memorizzo il percorso effettivo di archiviazione.
            this.w_PATHABS = alltrim(this.w_PERCORSOARCH)
            * --- Se il seriale non � vuoto aggiorno il path assoluto dell'indice
            if not empty(this.oParentObject.w_IDSERIAL)
              * --- Write into PROMINDI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PROMINDI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"IDPATALL ="+cp_NullLink(cp_ToStrODBC(this.w_PATHABS),'PROMINDI','IDPATALL');
                    +i_ccchkf ;
                +" where ";
                    +"IDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IDSERIAL);
                       )
              else
                update (i_cTable) set;
                    IDPATALL = this.w_PATHABS;
                    &i_ccchkf. ;
                 where;
                    IDSERIAL = this.oParentObject.w_IDSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            * --- Costruzione Path di default che punta alla cartella in cui sono definiti i file associati alle varie chiavi
            this.w_TmpPat = alltrim( this.w_PERCORSOARCH )+this.oParentObject.w_IDNOMFIL
            * --- Esegue la copia fisica del file <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            copy file (this.oParentObject.w_IDORIFIL) to (this.w_TmpPat) 
          endif
        else
          AH_ERRORMSG("Allegato non associato!",48,"")
        endif
      case this.pPARAM = "CAFISICA"
        * --- Nel caso di archiviazione web il file deve essere sempre cancellato
        if not empty(this.oParentObject.w_IDWEBFIL)
          * --- Read from CONTROPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTROPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "COPATHDO"+;
              " from "+i_cTable+" CONTROPA where ";
                  +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              COPATHDO;
              from (i_cTable) where;
                  COCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TmpPat = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_TmpPat = ADDBS(this.w_TmpPat)+alltrim(this.oParentObject.w_IDWEBFIL)
          w_ERRORE = .F.
          w_ErrorHandler = on("ERROR")
          on error w_ERRORE = .T.
          delete file (this.w_TmpPat)
          on error &w_ErrorHandler
        endif
        * --- Esegue cancellazione del file archiviato solo in caso di COPIA FILE e non nel caso di COLLEGAMENTO
        do case
          case this.oParentObject.w_IDMODALL="F" or Isalt()
            this.w_PERCORSOARCH = DCMPATAR(this.oParentObject.w_PATSTD, this.oParentObject.w_TIPRAG, this.oParentObject.w_IDDATRAG, this.oParentObject.w_IDCLADOC)
            this.w_TmpPat = alltrim(this.w_PERCORSOARCH)+alltrim(this.oParentObject.w_IDNOMFIL)
            if ! Cp_fileexist(this.w_TmpPat)
              this.w_PERCORSOARCH = DCMPATAR(this.oParentObject.w_PATSTD, this.oParentObject.w_TIPRAG, this.oParentObject.w_IDDATRAG, this.oParentObject.w_IDCLADOC,"","","","","","",.t.)
              this.w_TmpPat = alltrim(this.w_PERCORSOARCH)+alltrim(this.oParentObject.w_IDNOMFIL)
            endif
            if ! Cp_fileexist(this.w_TmpPat)
              this.w_PERCORSOARCH = DCMPATAR(this.oParentObject.w_IDPATALL, this.oParentObject.w_TIPRAG, this.oParentObject.w_IDDATRAG, this.oParentObject.w_IDCLADOC,"","","","","","",.t.)
              this.w_TmpPat = alltrim(this.w_PERCORSOARCH)+alltrim(this.oParentObject.w_IDNOMFIL)
            endif
            w_ERRORE = .F.
            w_ErrorHandler = on("ERROR")
            on error w_ERRORE = .T.
            delete file (this.w_TmpPat)
            if cp_fileexist(Alltrim(this.oParentObject.w_OLDPAT)+Alltrim(this.oParentObject.w_OLDFIL))
              * --- Ho annullato l'elaborazione devo eliminare file gi� creato
              Delete file (Alltrim(this.oParentObject.w_OLDPAT)+Alltrim(this.oParentObject.w_OLDFIL))
            endif
            on error &w_ErrorHandler
            if w_ERRORE
              ah_msg("Impossibile cancellare il file %1",.f.,.f.,5,this.oParentObject.w_IDNOMFIL)
              i_retcode = 'stop'
              return
            endif
          case this.oParentObject.w_IDMODALL="E"
        endcase
      case this.pPARAM = "MESSAGGIO"
        if this.oParentObject.VisuMess
          this.w_Ok = ah_YesNo("Vuoi aprire il documento generato?")
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_Ok
            * --- Lancia eseguibile associato al file
            this.w_cLink = this.oParentObject.VisPat
            this.w_cParms = space(1)
            declare integer ShellExecute in shell32.dll integer nHwnd, string cOperation, string cFileName, string cParms, string cDir, integer nShowCmd
             declare integer FindWindow in win32api string cNull, string cWinName 
            w_hWnd = FindWindow(0,_screen.caption)
            this.w_SHELLEXEC = ShellExecute(w_hWnd,"open",alltrim(this.w_cLink),alltrim(this.w_cParms),sys(5)+curdir(),1)
          endif
        endif
      case this.pPARAM = "UPLOAD"
        this.w_TmpPat = GetFile()
        if NOT EMPTY(this.w_TmpPat) AND cp_fileexist(this.w_TmpPat)
          * --- Read from CONTROPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTROPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "COPATHDO"+;
              " from "+i_cTable+" CONTROPA where ";
                  +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              COPATHDO;
              from (i_cTable) where;
                  COCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_COPATHDO = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          private w_ERRORE,w_ErrorHandler
          w_ErrorHandler = on("ERROR")
          w_ERRORE = .F.
          on error w_ERRORE = .T.
          if cp_FileExist(alltrim(this.w_COPATHDO)+JUSTFNAME(this.oParentObject.w_IDWEBFIL))
            delete file (alltrim(this.w_COPATHDO)+JUSTFNAME(this.oParentObject.w_IDWEBFIL))
          endif
          copy file (this.w_TMPPAT) to (alltrim(this.w_COPATHDO)+JUSTFNAME(this.oParentObject.w_IDWEBFIL))
          on error &w_ErrorHandler
          if w_ERRORE
            this.w_TMPC = "Impossibile copiare il documento archiviato nella cartella per il trasferimento"
            ah_ErrorMSG(this.w_TMPC,48)
            i_retcode = 'stop'
            return
          endif
          * --- devo aggiornare l'invio
          if SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1)<>NVL(this.oParentObject.w_IDCRCALL,"")
            if UPPER(this.oParentObject.cFunction)="EDIT"
              this.oParentObject.w_IDCRCALL = SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1)
            else
              * --- Write into PROMINDI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PROMINDI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"IDCRCALL ="+cp_NullLink(cp_ToStrODBC(SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1)),'PROMINDI','IDCRCALL');
                    +i_ccchkf ;
                +" where ";
                    +"IDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IDSERIAL);
                       )
              else
                update (i_cTable) set;
                    IDCRCALL = SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1);
                    &i_ccchkf. ;
                 where;
                    IDSERIAL = this.oParentObject.w_IDSERIAL;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          ah_ErrorMSG("Documento pronto per l'invio",48)
        endif
      case this.pPARAM = "BLOCCA" or this.pPARAM = "SBLOCCA"
        if EMPTY(this.oParentObject.w_IDSERIAL)
          ah_errorMsg("Nessun indice selezionata")
        else
          this.w_STATO = iif(this.pPARAM="BLOCCA","S","N")
          this.oParentObject.w_IDFLPROV = this.w_STATO
          * --- Write into PROMINDI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PROMINDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IDFLPROV ="+cp_NullLink(cp_ToStrODBC(this.w_STATO),'PROMINDI','IDFLPROV');
                +i_ccchkf ;
            +" where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IDSERIAL);
                   )
          else
            update (i_cTable) set;
                IDFLPROV = this.w_STATO;
                &i_ccchkf. ;
             where;
                IDSERIAL = this.oParentObject.w_IDSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          SetMainCaption()
          this.oParentObject.Loadrec()
        endif
      case this.pPARAM = "INSPRESTAZ"
        * --- Lanciato da AM Check Form di GSUT_AID
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia eseguibile associato al file
    * --- Converto eventuale percorso relativo in assoluto
    if this.oParentObject.w_IDMODALL # "I"
      this.w_TmpPat = FULLPATH(Alltrim(this.w_TmpPat))
    endif
    this.w_cLink = this.w_TmpPat
    this.w_cParms = space(1)
    declare integer ShellExecute in shell32.dll integer nHwnd, string cOperation, string cFileName, string cParms, string cDir, integer nShowCmd
     declare integer FindWindow in win32api string cNull, string cWinName 
    w_hWnd = FindWindow(0,_screen.caption)
    this.w_SHELLEXEC = ShellExecute(w_hWnd,"open",alltrim(this.w_cLink),alltrim(this.w_cParms),sys(5)+curdir(),1)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Inserimento provvisorio prestazioni
    if ISALT() AND NOT EMPTY(this.oParentObject.w_COD_PRE) OR NOT EMPTY(this.oParentObject.w_COD_RAG)
      this.w_READAZI = i_CODAZI
      * --- Read from PAR_ALTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PAGENPRE"+;
          " from "+i_cTable+" PAR_ALTE where ";
              +"PACODAZI = "+cp_ToStrODBC(this.w_READAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PAGENPRE;
          from (i_cTable) where;
              PACODAZI = this.w_READAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_GENPRE = NVL(cp_ToDate(_read_.PAGENPRE),cp_NullValue(_read_.PAGENPRE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Lancia la maschera di Inserimento provvisorio delle prestazioni
      this.w_GSAG_KPR = GSAG_KPR(this.w_GENPRE)
      * --- Valorizzo la pratica in GSAG_KPR
      SetValueLinked("M", this.w_GSAG_KPR, "w_CodPratica", this.oParentObject.w_COD_PRA)
      * --- Variabile per ripristinare il valore di g_SCHEDULER
      this.w_MODSCHED = "N"
      if VARTYPE(g_SCHEDULER)<>"C" Or g_SCHEDULER<>"S"
        public g_SCHEDULER
        g_SCHEDULER="S"
        this.w_MODSCHED = "S"
      endif
      * --- Lancio (in modalit� nascosta) la Scelta multipla delle prestazioni
      this.w_GSAG_KPM = GSAG_KPM(this.w_GSAG_KPR)
      do case
        case NOT EMPTY(this.oParentObject.w_COD_PRE)
          * --- Seleziona singola prestazione
          UPDATE ( this.w_GSAG_KPM.w_AGKPM_ZOOM.cCursor ) SET XCHK = 1 WHERE CACODICE=this.oParentObject.w_COD_PRE
        case NOT EMPTY(this.oParentObject.w_COD_RAG)
          * --- Valorizzo raggruppamento prestazioni (scatta la ricerca automaticamente delle prestazioni)
          SetValueLinked("M", this.w_GSAG_KPM, "w_SELRAGG", this.oParentObject.w_COD_RAG)
          * --- Seleziona tutte
          SetValueLinked("M", this.w_GSAG_KPM, "w_SELEZI", "S")
      endcase
      * --- Conferma della maschera di Scelta multipla delle prestazioni
      this.w_GSAG_KPM.EcpSave()     
      if this.w_MODSCHED="S"
        g_SCHEDULER=" "
      endif
    endif
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='PRODCLAS'
    this.cWorkTables[2]='PROMINDI'
    this.cWorkTables[3]='CONTROPA'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='PRODINDI'
    this.cWorkTables[6]='PAR_PRAT'
    this.cWorkTables[7]='PAR_ALTE'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_PRODCLAS')
      use in _Curs_PRODCLAS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
