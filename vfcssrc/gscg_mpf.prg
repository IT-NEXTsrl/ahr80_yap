* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mpf                                                        *
*              Plafond annuale                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_121]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-03                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mpf")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mpf")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mpf")
  return

* --- Class definition
define class tgscg_mpf as StdPCForm
  Width  = 522
  Height = 366
  Top    = 6
  Left   = 7
  cComment = "Plafond annuale"
  cPrg = "gscg_mpf"
  HelpContextID=147465577
  add object cnt as tcgscg_mpf
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mpf as PCContext
  w_DICODAZI = space(5)
  w_DI__ANNO = space(4)
  w_PLAMOB = space(1)
  w_SIMVAL = space(5)
  w_ANNRIF = space(4)
  w_PLAINI = 0
  w_DIPERIOD = 0
  w_DITOTESP = 0
  w_DIPLAUTI = 0
  w_DIPLADIS = 0
  w_TOTEXP = 0
  w_TOTUTI = 0
  w_VALPLA = space(3)
  w_DECTOP = 0
  w_CALCPIP = 0
  proc Save(i_oFrom)
    this.w_DICODAZI = i_oFrom.w_DICODAZI
    this.w_DI__ANNO = i_oFrom.w_DI__ANNO
    this.w_PLAMOB = i_oFrom.w_PLAMOB
    this.w_SIMVAL = i_oFrom.w_SIMVAL
    this.w_ANNRIF = i_oFrom.w_ANNRIF
    this.w_PLAINI = i_oFrom.w_PLAINI
    this.w_DIPERIOD = i_oFrom.w_DIPERIOD
    this.w_DITOTESP = i_oFrom.w_DITOTESP
    this.w_DIPLAUTI = i_oFrom.w_DIPLAUTI
    this.w_DIPLADIS = i_oFrom.w_DIPLADIS
    this.w_TOTEXP = i_oFrom.w_TOTEXP
    this.w_TOTUTI = i_oFrom.w_TOTUTI
    this.w_VALPLA = i_oFrom.w_VALPLA
    this.w_DECTOP = i_oFrom.w_DECTOP
    this.w_CALCPIP = i_oFrom.w_CALCPIP
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DICODAZI = this.w_DICODAZI
    i_oTo.w_DI__ANNO = this.w_DI__ANNO
    i_oTo.w_PLAMOB = this.w_PLAMOB
    i_oTo.w_SIMVAL = this.w_SIMVAL
    i_oTo.w_ANNRIF = this.w_ANNRIF
    i_oTo.w_PLAINI = this.w_PLAINI
    i_oTo.w_DIPERIOD = this.w_DIPERIOD
    i_oTo.w_DITOTESP = this.w_DITOTESP
    i_oTo.w_DIPLAUTI = this.w_DIPLAUTI
    i_oTo.w_DIPLADIS = this.w_DIPLADIS
    i_oTo.w_TOTEXP = this.w_TOTEXP
    i_oTo.w_TOTUTI = this.w_TOTUTI
    i_oTo.w_VALPLA = this.w_VALPLA
    i_oTo.w_DECTOP = this.w_DECTOP
    i_oTo.w_CALCPIP = this.w_CALCPIP
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mpf as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 522
  Height = 366
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=147465577
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PLA_IVAN_IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  cFile = "PLA_IVAN"
  cKeySelect = "DICODAZI,DI__ANNO"
  cKeyWhere  = "DICODAZI=this.w_DICODAZI and DI__ANNO=this.w_DI__ANNO"
  cKeyDetail  = "DICODAZI=this.w_DICODAZI and DI__ANNO=this.w_DI__ANNO and DIPERIOD=this.w_DIPERIOD"
  cKeyWhereODBC = '"DICODAZI="+cp_ToStrODBC(this.w_DICODAZI)';
      +'+" and DI__ANNO="+cp_ToStrODBC(this.w_DI__ANNO)';

  cKeyDetailWhereODBC = '"DICODAZI="+cp_ToStrODBC(this.w_DICODAZI)';
      +'+" and DI__ANNO="+cp_ToStrODBC(this.w_DI__ANNO)';
      +'+" and DIPERIOD="+cp_ToStrODBC(this.w_DIPERIOD)';

  cKeyWhereODBCqualified = '"PLA_IVAN.DICODAZI="+cp_ToStrODBC(this.w_DICODAZI)';
      +'+" and PLA_IVAN.DI__ANNO="+cp_ToStrODBC(this.w_DI__ANNO)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gscg_mpf"
  cComment = "Plafond annuale"
  i_nRowNum = 0
  i_nRowPerPage = 13
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DICODAZI = space(5)
  w_DI__ANNO = space(4)
  w_PLAMOB = space(1)
  w_SIMVAL = space(5)
  w_ANNRIF = space(4)
  w_PLAINI = 0
  w_DIPERIOD = 0
  w_DITOTESP = 0
  w_DIPLAUTI = 0
  w_DIPLADIS = 0
  w_TOTEXP = 0
  w_TOTUTI = 0
  w_VALPLA = space(3)
  w_DECTOP = 0
  w_CALCPIP = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mpfPag1","gscg_mpf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='PLA_IVAN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PLA_IVAN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PLA_IVAN_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mpf'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PLA_IVAN where DICODAZI=KeySet.DICODAZI
    *                            and DI__ANNO=KeySet.DI__ANNO
    *                            and DIPERIOD=KeySet.DIPERIOD
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gscg_mpf
      * --- Setta Ordine per  Periodo
      i_cOrder = 'order by DIPERIOD Asc '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PLA_IVAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PLA_IVAN_IDX,2],this.bLoadRecFilter,this.PLA_IVAN_IDX,"gscg_mpf")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PLA_IVAN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PLA_IVAN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PLA_IVAN '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DICODAZI',this.w_DICODAZI  ,'DI__ANNO',this.w_DI__ANNO  )
      select * from (i_cTable) PLA_IVAN where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ANNRIF = .oParentObject.w_IA__ANNO
        .w_TOTEXP = 0
        .w_TOTUTI = 0
        .w_DICODAZI = NVL(DICODAZI,space(5))
        .w_DI__ANNO = NVL(DI__ANNO,space(4))
        .w_PLAMOB = .oParentObject.w_IAPLAMOB
        .w_SIMVAL = .oParentObject.w_SIMVAL
        .w_PLAINI = .oParentObject.w_IAPLAINI
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(AH_Msgformat(iif( .w_PLAMOB='S','Plafond mobile','Plafond fisso')))
        .w_VALPLA = .oParentObject.w_IAVALPLA
        .w_DECTOP = .oParentObject.w_DECTOP
        .w_CALCPIP = DEFPIP(.w_DECTOP)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PLA_IVAN')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTEXP = 0
      this.w_TOTUTI = 0
      scan
        with this
          .w_DIPERIOD = NVL(DIPERIOD,0)
          .w_DITOTESP = NVL(DITOTESP,0)
          .w_DIPLAUTI = NVL(DIPLAUTI,0)
          .w_DIPLADIS = NVL(DIPLADIS,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTEXP = .w_TOTEXP+.w_DITOTESP
          .w_TOTUTI = .w_TOTUTI+.w_DIPLAUTI
          replace DIPERIOD with .w_DIPERIOD
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_PLAMOB = .oParentObject.w_IAPLAMOB
        .w_SIMVAL = .oParentObject.w_SIMVAL
        .w_PLAINI = .oParentObject.w_IAPLAINI
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(AH_Msgformat(iif( .w_PLAMOB='S','Plafond mobile','Plafond fisso')))
        .w_VALPLA = .oParentObject.w_IAVALPLA
        .w_DECTOP = .oParentObject.w_DECTOP
        .w_CALCPIP = DEFPIP(.w_DECTOP)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_DICODAZI=space(5)
      .w_DI__ANNO=space(4)
      .w_PLAMOB=space(1)
      .w_SIMVAL=space(5)
      .w_ANNRIF=space(4)
      .w_PLAINI=0
      .w_DIPERIOD=0
      .w_DITOTESP=0
      .w_DIPLAUTI=0
      .w_DIPLADIS=0
      .w_TOTEXP=0
      .w_TOTUTI=0
      .w_VALPLA=space(3)
      .w_DECTOP=0
      .w_CALCPIP=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_PLAMOB = .oParentObject.w_IAPLAMOB
        .w_SIMVAL = .oParentObject.w_SIMVAL
        .w_ANNRIF = .oParentObject.w_IA__ANNO
        .w_PLAINI = .oParentObject.w_IAPLAINI
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(AH_Msgformat(iif( .w_PLAMOB='S','Plafond mobile','Plafond fisso')))
        .DoRTCalc(7,12,.f.)
        .w_VALPLA = .oParentObject.w_IAVALPLA
        .w_DECTOP = .oParentObject.w_DECTOP
        .w_CALCPIP = DEFPIP(.w_DECTOP)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PLA_IVAN')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PLA_IVAN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gscg_mpf
    this.mCalc(.T.)
    
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PLA_IVAN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODAZI,"DICODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DI__ANNO,"DI__ANNO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DIPERIOD N(2);
      ,t_DITOTESP N(18,4);
      ,t_DIPLAUTI N(18,4);
      ,t_DIPLADIS N(18,4);
      ,DIPERIOD N(2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mpfbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIPERIOD_2_1.controlsource=this.cTrsName+'.t_DIPERIOD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDITOTESP_2_2.controlsource=this.cTrsName+'.t_DITOTESP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLAUTI_2_3.controlsource=this.cTrsName+'.t_DIPLAUTI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLADIS_2_4.controlsource=this.cTrsName+'.t_DIPLADIS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(72)
    this.AddVLine(207)
    this.AddVLine(344)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPERIOD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PLA_IVAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PLA_IVAN_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PLA_IVAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PLA_IVAN_IDX,2])
      *
      * insert into PLA_IVAN
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PLA_IVAN')
        i_extval=cp_InsertValODBCExtFlds(this,'PLA_IVAN')
        i_cFldBody=" "+;
                  "(DICODAZI,DI__ANNO,DIPERIOD,DITOTESP,DIPLAUTI"+;
                  ",DIPLADIS,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DICODAZI)+","+cp_ToStrODBC(this.w_DI__ANNO)+","+cp_ToStrODBC(this.w_DIPERIOD)+","+cp_ToStrODBC(this.w_DITOTESP)+","+cp_ToStrODBC(this.w_DIPLAUTI)+;
             ","+cp_ToStrODBC(this.w_DIPLADIS)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PLA_IVAN')
        i_extval=cp_InsertValVFPExtFlds(this,'PLA_IVAN')
        cp_CheckDeletedKey(i_cTable,0,'DICODAZI',this.w_DICODAZI,'DI__ANNO',this.w_DI__ANNO,'DIPERIOD',this.w_DIPERIOD)
        INSERT INTO (i_cTable) (;
                   DICODAZI;
                  ,DI__ANNO;
                  ,DIPERIOD;
                  ,DITOTESP;
                  ,DIPLAUTI;
                  ,DIPLADIS;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DICODAZI;
                  ,this.w_DI__ANNO;
                  ,this.w_DIPERIOD;
                  ,this.w_DITOTESP;
                  ,this.w_DIPLAUTI;
                  ,this.w_DIPLADIS;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PLA_IVAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PLA_IVAN_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_DIPERIOD<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PLA_IVAN')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and DIPERIOD="+cp_ToStrODBC(&i_TN.->DIPERIOD)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PLA_IVAN')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and DIPERIOD=&i_TN.->DIPERIOD;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_DIPERIOD<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and DIPERIOD="+cp_ToStrODBC(&i_TN.->DIPERIOD)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and DIPERIOD=&i_TN.->DIPERIOD;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace DIPERIOD with this.w_DIPERIOD
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PLA_IVAN
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PLA_IVAN')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DITOTESP="+cp_ToStrODBC(this.w_DITOTESP)+;
                     ",DIPLAUTI="+cp_ToStrODBC(this.w_DIPLAUTI)+;
                     ",DIPLADIS="+cp_ToStrODBC(this.w_DIPLADIS)+;
                     ",DIPERIOD="+cp_ToStrODBC(this.w_DIPERIOD)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and DIPERIOD="+cp_ToStrODBC(DIPERIOD)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PLA_IVAN')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DITOTESP=this.w_DITOTESP;
                     ,DIPLAUTI=this.w_DIPLAUTI;
                     ,DIPLADIS=this.w_DIPLADIS;
                     ,DIPERIOD=this.w_DIPERIOD;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and DIPERIOD=&i_TN.->DIPERIOD;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PLA_IVAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PLA_IVAN_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_DIPERIOD<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PLA_IVAN
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and DIPERIOD="+cp_ToStrODBC(&i_TN.->DIPERIOD)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and DIPERIOD=&i_TN.->DIPERIOD;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_DIPERIOD<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PLA_IVAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PLA_IVAN_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .w_PLAMOB = .oParentObject.w_IAPLAMOB
          .w_SIMVAL = .oParentObject.w_SIMVAL
        .DoRTCalc(5,5,.t.)
          .w_PLAINI = .oParentObject.w_IAPLAINI
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(AH_Msgformat(iif( .w_PLAMOB='S','Plafond mobile','Plafond fisso')))
        .DoRTCalc(7,12,.t.)
          .w_VALPLA = .oParentObject.w_IAVALPLA
          .w_DECTOP = .oParentObject.w_DECTOP
          .w_CALCPIP = DEFPIP(.w_DECTOP)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(AH_Msgformat(iif( .w_PLAMOB='S','Plafond mobile','Plafond fisso')))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPLAINI_1_6.visible=!this.oPgFrm.Page1.oPag.oPLAINI_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_4.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_4.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oANNRIF_1_5.value==this.w_ANNRIF)
      this.oPgFrm.Page1.oPag.oANNRIF_1_5.value=this.w_ANNRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oPLAINI_1_6.value==this.w_PLAINI)
      this.oPgFrm.Page1.oPag.oPLAINI_1_6.value=this.w_PLAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTEXP_3_1.value==this.w_TOTEXP)
      this.oPgFrm.Page1.oPag.oTOTEXP_3_1.value=this.w_TOTEXP
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTUTI_3_2.value==this.w_TOTUTI)
      this.oPgFrm.Page1.oPag.oTOTUTI_3_2.value=this.w_TOTUTI
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPERIOD_2_1.value==this.w_DIPERIOD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPERIOD_2_1.value=this.w_DIPERIOD
      replace t_DIPERIOD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPERIOD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITOTESP_2_2.value==this.w_DITOTESP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITOTESP_2_2.value=this.w_DITOTESP
      replace t_DITOTESP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITOTESP_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLAUTI_2_3.value==this.w_DIPLAUTI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLAUTI_2_3.value=this.w_DIPLAUTI
      replace t_DIPLAUTI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLAUTI_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLADIS_2_4.value==this.w_DIPLADIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLADIS_2_4.value=this.w_DIPLADIS
      replace t_DIPLADIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLADIS_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'PLA_IVAN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_DIPERIOD>0 AND .w_DIPERIOD<13) and (.w_DIPERIOD<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPERIOD_2_1
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if .w_DIPERIOD<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_DIPERIOD<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DIPERIOD=0
      .w_DITOTESP=0
      .w_DIPLAUTI=0
      .w_DIPLADIS=0
    endwith
    this.DoRTCalc(1,15,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DIPERIOD = t_DIPERIOD
    this.w_DITOTESP = t_DITOTESP
    this.w_DIPLAUTI = t_DIPLAUTI
    this.w_DIPLADIS = t_DIPLADIS
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DIPERIOD with this.w_DIPERIOD
    replace t_DITOTESP with this.w_DITOTESP
    replace t_DIPLAUTI with this.w_DIPLAUTI
    replace t_DIPLADIS with this.w_DIPLADIS
    if i_srv='A'
      replace DIPERIOD with this.w_DIPERIOD
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTEXP = .w_TOTEXP-.w_ditotesp
        .w_TOTUTI = .w_TOTUTI-.w_diplauti
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mpfPag1 as StdContainer
  Width  = 518
  height = 366
  stdWidth  = 518
  stdheight = 366
  resizeXpos=483
  resizeYpos=278
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSIMVAL_1_4 as StdField with uid="MRIHSBBVWN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 127990054,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=447, Top=4, InputMask=replicate('X',5)

  add object oANNRIF_1_5 as StdField with uid="KSZRWWPCCQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ANNRIF", cQueryName = "ANNRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento per il calcolo del plafond fisso",;
    HelpContextID = 35458310,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=336, Top=4, InputMask=replicate('X',4)

  add object oPLAINI_1_6 as StdField with uid="NBUSSXSEJM",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PLAINI", cQueryName = "PLAINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo del plafond ad inizio anno",;
    HelpContextID = 90389494,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=336, Top=32, cSayPict="v_PV(40+(20*g_PERPVL))", cGetPict="v_PV(40+(20*g_PERPVL))"

  func oPLAINI_1_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PLAMOB='S')
    endwith
    endif
  endfunc


  add object oObj_1_10 as cp_calclbl with uid="NLASMHFBKG",left=30, top=5, width=210,height=13,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 30532070


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=30, top=63, width=474,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="DIPERIOD",Label1="Mese",Field2="DITOTESP",Label2="Esportazioni",Field3="DIPLAUTI",Label3="Plafond utilizzato",Field4="DIPLADIS",Label4="Plafond inizio periodo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168625274

  add object oStr_1_7 as StdString with uid="GLQVLTKOPB",Visible=.t., Left=290, Top=4,;
    Alignment=1, Width=39, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="CKRWYWRYCH",Visible=.t., Left=183, Top=32,;
    Alignment=1, Width=146, Height=18,;
    Caption="Plafond ad inizio anno:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_PLAMOB='S')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="EXAESWFUOY",Visible=.t., Left=390, Top=4,;
    Alignment=1, Width=55, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=20,top=82,;
    width=470+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*13*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=21,top=83,width=469+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*13*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTEXP_3_1 as StdField with uid="QGWXFABRLQ",rtseq=11,rtrep=.f.,;
    cFormVar="w_TOTEXP",value=0,enabled=.f.,;
    ToolTipText = "Totale esportazioni",;
    HelpContextID = 218132278,;
    cQueryName = "TOTEXP",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=74, Top=336, cSayPict=[v_PV(40+(20*g_PERPVL))], cGetPict=[v_GV(40+(20*g_PERPVL))]

  add object oTOTUTI_3_2 as StdField with uid="IWCSYFMARA",rtseq=12,rtrep=.f.,;
    cFormVar="w_TOTUTI",value=0,enabled=.f.,;
    ToolTipText = "Totale utilizzazioni",;
    HelpContextID = 97546038,;
    cQueryName = "TOTUTI",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=209, Top=336, cSayPict=[v_PV(40+(20*g_PERPVL))], cGetPict=[v_GV(40+(20*g_PERPVL))]

  add object oStr_3_3 as StdString with uid="OYZPSDJRUP",Visible=.t., Left=3, Top=338,;
    Alignment=1, Width=68, Height=18,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgscg_mpfBodyRow as CPBodyRowCnt
  Width=460
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDIPERIOD_2_1 as StdTrsField with uid="WDRZIHPJSQ",rtseq=7,rtrep=.t.,;
    cFormVar="w_DIPERIOD",value=0,isprimarykey=.t.,;
    ToolTipText = "Mese di riferimento per il calcolo del plafond",;
    HelpContextID = 94382202,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["99"], cGetPict=["99"]

  func oDIPERIOD_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DIPERIOD>0 AND .w_DIPERIOD<13)
    endwith
    return bRes
  endfunc

  add object oDITOTESP_2_2 as StdTrsField with uid="MKWTJMMNLR",rtseq=8,rtrep=.t.,;
    cFormVar="w_DITOTESP",value=0,;
    ToolTipText = "Importo plafond esportazioni, gestito in caso di plafond variabile",;
    HelpContextID = 30042246,;
    cTotal = "this.Parent.oContained.w_totexp", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=42, Top=0, cSayPict=[v_PV(40+(20*g_PERPVL))], cGetPict=[v_GV(40+(20*g_PERPVL))]

  add object oDIPLAUTI_2_3 as StdTrsField with uid="ZGQXXDLHRE",rtseq=9,rtrep=.t.,;
    cFormVar="w_DIPLAUTI",value=0,;
    ToolTipText = "Importo plafond utilizzato",;
    HelpContextID = 9906303,;
    cTotal = "this.Parent.oContained.w_totuti", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=177, Top=0, cSayPict=[v_PV(40+(20*g_PERPVL))], cGetPict=[v_GV(40+(20*g_PERPVL))]

  add object oDIPLADIS_2_4 as StdTrsField with uid="VQXBKKKGSH",rtseq=10,rtrep=.t.,;
    cFormVar="w_DIPLADIS",value=0,;
    ToolTipText = "Plafond disponibile inizio periodo",;
    HelpContextID = 6870903,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=316, Top=0, cSayPict=[v_PV(40+(20*g_PERPVL))], cGetPict=[v_GV(40+(20*g_PERPVL))]
  add object oLast as LastKeyMover
  * ---
  func oDIPERIOD_2_1.When()
    return(.t.)
  proc oDIPERIOD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDIPERIOD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=12
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mpf','PLA_IVAN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DICODAZI=PLA_IVAN.DICODAZI";
  +" and "+i_cAliasName2+".DI__ANNO=PLA_IVAN.DI__ANNO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
