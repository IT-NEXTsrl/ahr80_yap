* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bfd                                                        *
*              Drill down cash flow                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-14                                                      *
* Last revis.: 2014-09-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bfd",oParentObject,m.pTIPO)
return(i_retval)

define class tgste_bfd as StdBatch
  * --- Local variables
  pTIPO = space(1)
  w_NUMPER = 0
  CFMSK = .NULL.
  DDMSK = .NULL.
  w_ZOOMDD = space(10)
  w_INTERVALLO = 0
  w_DATAINI = ctod("  /  /  ")
  w_DATAFIN = ctod("  /  /  ")
  w_STRDD = .NULL.
  w_TOTPER = 0
  w_TOTPROG = 0
  w_VALPER = 0
  w_VALPROG = 0
  w_IndicePeriodi = 0
  w_VALUTA = space(3)
  w_NUMCOR = space(15)
  w_DESCON = space(60)
  w_VALCON = space(3)
  w_TIPOCASHFLOW = space(1)
  w_NONDOMIC = .f.
  w_TIPOLOGIACONTO = space(1)
  w_INIZIO = ctod("  /  /  ")
  w_FINE = ctod("  /  /  ")
  w_INIZIO_MSK = ctod("  /  /  ")
  w_FINE_MSK = ctod("  /  /  ")
  w_TIPOPAG = space(2)
  * --- WorkFile variables
  FILDTREG_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato con parametro '1' da GSTE_KFL (Visualizza Cash Flow)
    *     Effettua Drill Down su Riga
    * --- Utilizzate solo per il parametro 2
    * --- Creiamo un oggetto che punta alla maschera del CASH FLOW (GSTE_KFL)
    this.CFMSK = THIS.OPARENTOBJECT.OPARENTOBJECT.OPARENTOBJECT
    * --- Creo un oggetto che punta alla maschera di drill down
    this.DDMSK = THIS.OPARENTOBJECT
    * --- Leggo il nome del cursore di drill down
    this.w_ZOOMDD = this.DDMSK.w_ZOOMTIPO.cCursor
    * --- Determino il periodo di riferimento dato dal numero di colonna - 2
    this.w_NUMPER = this.oParentObject.w_COLONNA - 3
    this.w_INTERVALLO = IIF(this.CFMSK.w_TIPPER="G",1,IIF(this.CFMSK.w_TIPPER="S",7,IIF(this.CFMSK.w_TIPPER="Q",14,30)))
    this.w_DATAINI = this.CFMSK.w_DATINI
    if this.w_NUMPER>0
      if this.CFMSK.w_TIPPER<>"G"
        do case
          case this.CFMSK.w_TIPPER="S" OR this.CFMSK.w_TIPPER="Q"
            this.w_DATAINI = this.w_DATAINI - DOW(this.w_DATAINI,2) + 1
            this.w_DATAINI = this.w_DATAINI + ((this.w_NUMPER-1)*this.w_INTERVALLO) 
          case this.CFMSK.w_TIPPER="M"
            this.w_DATAINI = DATE(YEAR(this.w_DATAINI),MONTH(this.w_DATAINI),1)
            this.w_DATAINI = this.w_DATAINI + ((this.w_NUMPER-1)*31) 
            this.w_DATAINI = DATE(YEAR(this.w_DATAINI),MONTH(this.w_DATAINI),1)
        endcase
      else
        this.w_DATAINI = this.w_DATAINI + ((this.w_NUMPER-1)*this.w_INTERVALLO) 
      endif
      if this.CFMSK.w_TIPPER="M"
        this.w_DATAFIN = DATE(iif(MONTH(this.w_DATAINI)=12,1,0)+YEAR(this.w_DATAINI),iif(MONTH(this.w_DATAINI)=12,1,MONTH(this.w_DATAINI)+1),1)-1
      else
        this.w_DATAFIN = IIF(this.w_DATAINI+ this.w_INTERVALLO-1<= this.CFMSK.w_DATFIN,this.w_DATAINI+ this.w_INTERVALLO-1,this.CFMSK.w_DATFIN)
      endif
      if this.w_NUMPER>1
        this.w_DATAINI = MAX(this.w_DATAINI,this.CFMSK.w_DATINI)
      else
        this.w_DATAINI = this.CFMSK.w_DATINI
      endif
    else
      this.w_DATAFIN = this.w_DATAINI - 1
      this.w_DATAINI = cp_CharToDate("  -  -    ")
    endif
    do case
      case this.pTipo = "1"
        * --- Leggo il nome della stringa che indica il periodo
        this.w_STRDD = this.DDMSK.w_STRPERIODO
        this.oParentObject.w_TIPOCONTO = this.CFMSK.TIPOCONTI(this.oParentObject.w_RIGA)
        this.oParentObject.w_CONTOTESO = IIF(this.oParentObject.w_TIPOCONTO="C",this.CFMSK.CODCONTI(this.oParentObject.w_RIGA,1),SPACE(15))
        this.oParentObject.w_CONTOCASSA = IIF(this.oParentObject.w_TIPOCONTO="G",this.CFMSK.CODCONTI(this.oParentObject.w_RIGA,1),SPACE(15))
        this.oParentObject.w_VALCONTO = g_PERVAL
        * --- Intesto la maschera di zoom
        this.DDMSK.CAPTION = this.DDMSK.CAPTION + AH_MSGFORMAT ( " -- SITUAZIONE AL ") + STR(DAY(this.w_DATAFIN),2) + "/" + STR(MONTH(this.w_DATAFIN),2) + "/" + STR(YEAR(this.w_DATAFIN),4) + " --"
        if this.w_NUMPER = 0
          this.w_STRDD.CAPTION = AH_MSGFORMAT ("Periodo fino al ")
        else
          this.w_STRDD.CAPTION = AH_MSGFORMAT ("Periodo dal %1/%2/%3 al " , STR(DAY(this.w_DATAINI),2) , STR(MONTH(this.w_DATAINI),2) , STR(YEAR(this.w_DATAINI),4) )
        endif
        this.w_STRDD.CAPTION = this.w_STRDD.CAPTION + STR(DAY(this.w_DATAFIN),2) + "/" + STR(MONTH(this.w_DATAFIN),2) + "/" + STR(YEAR(this.w_DATAFIN),4)
        this.w_STRDD.FORECOLOR = RGB(255,0,0)
        * --- Intesto il periodo sulla maschera
        * --- Inizio la costruzione del cursore
        Select(this.w_ZOOMDD)
        Zap
        this.w_TOTPER = 0
        this.w_TOTPROG = 0
        this.w_NUMPER = this.w_NUMPER + 1
        if this.CFMSK.w_TIPCER = "S"
          this.w_VALPER = cp_ROUND(this.CFMSK.CERTI(this.oParentObject.w_RIGA,this.w_NUMPER),g_PERPVL)
          this.w_TOTPER = this.w_VALPER
          this.w_VALPROG = 0
          FOR this.w_IndicePeriodi = 1 TO this.w_NUMPER
          this.w_VALPROG = this.w_VALPROG + cp_ROUND(this.CFMSK.CERTI(this.oParentObject.w_RIGA, this.w_IndicePeriodi),g_PERPVL)
          ENDFOR
          this.w_TOTPROG = this.w_VALPROG
          INSERT INTO (this.w_ZOOMDD) (TIPO, VALORE,TOTALE) VALUES( "CERTO", this.w_VALPER, this.w_VALPROG)
        endif
        if this.CFMSK.w_TIPEFF = "S"
          this.w_VALPER = cp_ROUND(this.CFMSK.EFFETTIVI(this.oParentObject.w_RIGA,this.w_NUMPER),g_PERPVL)
          this.w_TOTPER = this.w_TOTPER + this.w_VALPER
          this.w_VALPROG = 0
          FOR this.w_IndicePeriodi = 1 TO this.w_NUMPER
          this.w_VALPROG = this.w_VALPROG + cp_ROUND(this.CFMSK.EFFETTIVI(this.oParentObject.w_RIGA, this.w_IndicePeriodi),g_PERPVL)
          ENDFOR
          this.w_TOTPROG = this.w_TOTPROG + this.w_VALPROG
          INSERT INTO (this.w_ZOOMDD) (TIPO, VALORE,TOTALE) VALUES( "EFFETTIVO", this.w_VALPER, this.w_VALPROG)
        endif
        if this.CFMSK.w_TIPATT = "S"
          this.w_VALPER = cp_ROUND(this.CFMSK.ATTESI(this.oParentObject.w_RIGA,this.w_NUMPER),g_PERPVL)
          this.w_TOTPER = this.w_TOTPER + this.w_VALPER
          this.w_VALPROG = 0
          FOR this.w_IndicePeriodi = 1 TO this.w_NUMPER
          this.w_VALPROG = this.w_VALPROG + cp_ROUND(this.CFMSK.ATTESI(this.oParentObject.w_RIGA, this.w_IndicePeriodi),g_PERPVL)
          ENDFOR
          this.w_TOTPROG = this.w_TOTPROG + this.w_VALPROG
          INSERT INTO (this.w_ZOOMDD) (TIPO, VALORE,TOTALE) VALUES( "ATTESO", this.w_VALPER, this.w_VALPROG)
        endif
        INSERT INTO (this.w_ZOOMDD) (TIPO, VALORE,TOTALE) VALUES( "TOTALI", this.w_TOTPER, this.w_TOTPROG)
        SELECT (this.w_ZOOMDD) 
 GO TOP
        this.DDMSK.ZOOMTIPO.GRD.READONLY = .T.
      case this.pTipo = "2"
        if this.oParentObject.oParentObject.w_COLSEL1<4
          ah_ErrorMsg("La funzionalit� � disponibile solo per i movimenti del periodo")
          i_retcode = 'stop'
          return
        endif
        do case
          case this.oParentObject.w_TIPOSEL = "CERTO" OR this.oParentObject.w_TIPOSEL = "EFFETTIVO"
            this.w_TIPOPAG = IIF(this.CFMSK.w_TIPOVISUA="C" OR ALLTRIM(this.CFMSK.w_TIPOPAG)="TOTALI","  ", ALLTRIM(this.CFMSK.w_TIPOPAG))
            this.w_NUMCOR = IIF(this.oParentObject.w_TIPOCONTO="C",this.DDMSK.w_CONTOTESO,IIF(this.oParentObject.w_TIPOCONTO="G",this.DDMSK.w_CONTOCASSA," "))
            this.w_DESCON = IIF(this.oParentObject.w_TIPOCONTO="C",this.DDMSK.w_DESCONTO,IIF(this.oParentObject.w_TIPOCONTO="G",this.DDMSK.w_DESCONTOC," "))
            this.w_TIPOLOGIACONTO = this.oParentObject.w_TIPOCONTO
            this.w_TIPOCASHFLOW = IIF(this.oParentObject.w_TIPOSEL="CERTO","C","E")
            * --- Calcolo le date di inizio e fine filtro. Se nella maschera di lancio Drill Down
            *     (DDMSK) ho selezonato la colonna progressivo (totale) visualizzo tutto il periodo
            *     altrimenti i soli movimenti del periodo
            if this.oParentObject.w_COLDD = 3
              this.w_INIZIO = this.CFMSK.w_DATINI
            else
              this.w_INIZIO = this.w_DATAINI
            endif
            this.w_FINE = this.w_DATAFIN
            this.w_NONDOMIC = IIF(EMPTY(this.w_NUMCOR),.T.,.F.)
            this.w_INIZIO_MSK = this.w_INIZIO
            this.w_FINE_MSK = this.w_FINE
            do GSTE_KDE with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_TIPOSEL = "ATTESO"
            this.w_TIPOPAG = IIF(this.CFMSK.w_TIPOVISUA="C" OR ALLTRIM(this.CFMSK.w_TIPOPAG)="TOTALI","  ", ALLTRIM(this.CFMSK.w_TIPOPAG))
            this.w_NUMCOR = IIF(this.oParentObject.w_TIPOCONTO="C",this.DDMSK.w_CONTOTESO,IIF(this.oParentObject.w_TIPOCONTO="G",this.DDMSK.w_CONTOCASSA," "))
            this.w_DESCON = IIF(this.oParentObject.w_TIPOCONTO="C",this.DDMSK.w_DESCONTO,IIF(this.oParentObject.w_TIPOCONTO="G",this.DDMSK.w_DESCONTOC," "))
            this.w_TIPOLOGIACONTO = this.oParentObject.w_TIPOCONTO
            this.w_TIPOCASHFLOW = "T"
            * --- Calcolo le date di inizio e fine filtro. Se nella maschera di lancio Drill Down
            *     (DDMSK) ho selezonato la colonna progressivo (totale) visualizzo tutto il periodo
            *     altrimenti i soli movimenti del periodo
            if this.oParentObject.w_COLDD = 3
              this.w_INIZIO = this.CFMSK.w_DATINI
            else
              this.w_INIZIO = this.w_DATAINI
            endif
            this.w_FINE = this.w_DATAFIN
            this.w_NONDOMIC = IIF(EMPTY(this.w_NUMCOR),.T.,.F.)
            this.w_INIZIO_MSK = this.w_INIZIO
            this.w_FINE_MSK = this.w_FINE
            do GSTE_KAT with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
    endcase
  endproc


  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='FILDTREG'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
