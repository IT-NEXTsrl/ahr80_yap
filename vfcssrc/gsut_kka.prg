* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kka                                                        *
*              Selezione valori attributo                                      *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-03-22                                                      *
* Last revis.: 2012-06-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_kka
IF EMPTY(NVL(oParentObject.w_IDTABKEY,' '))
  *-- Non creo l'oggetto perch� manca l'archivio di riferimento
   return
ENDIF
* --- Fine Area Manuale
return(createobject("tgsut_kka",oParentObject))

* --- Class definition
define class tgsut_kka as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 435
  Height = 284
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-08"
  HelpContextID=233390953
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kka"
  cComment = "Selezione valori attributo"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CHIAVE1 = space(45)
  w_CHIAVE2 = space(45)
  w_CHIAVE3 = space(45)
  w_CHIAVE4 = space(45)
  w_CHIAVE5 = space(45)
  w_ATTRZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_kka
  ckey0=' '
  ckey1=' '
  ckey2=' '
  ckey3=' '
  ckey4=' '
  ckey5=' '
  numchiavi=0
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kkaPag1","gsut_kka",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ATTRZOOM = this.oPgFrm.Pages(1).oPag.ATTRZOOM
    DoDefault()
    proc Destroy()
      this.w_ATTRZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_kka
    IF TYPE("this.oParentObject.w_IDFLDKEY")="C" AND NOT EMPTY(this.oParentObject.w_IDFLDKEY)
      CHIAVE = this.oParentObject.w_IDFLDKEY
    ELSE
      CHIAVE = cp_KeyToSQL(I_DCX.GetIdxDef(this.oParentObject.w_IDTABKEY ,1)) 
    ENDIF
    commaoccur=occurs(',',CHIAVE)
    *Ciclo sui campi chiave
    i=1 
    if commaoccur > 0 
      do while at(',',CHIAVE) > 0
        poscomma=at(',',CHIAVE)
        k=ALLTRIM(STR(i))
        this.ckey&k=left(chiave,poscomma-1)
        CHIAVE=substr(CHIAVE,poscomma+1)
        i=i+1
      enddo
      *ultimo campo della chiave
      k=ALLTRIM(STR(i))
      this.ckey&k=CHIAVE
    else
      this.ckey1=CHIAVE
    endif
    this.numchiavi=i
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CHIAVE1=space(45)
      .w_CHIAVE2=space(45)
      .w_CHIAVE3=space(45)
      .w_CHIAVE4=space(45)
      .w_CHIAVE5=space(45)
      .oPgFrm.Page1.oPag.ATTRZOOM.Calculate()
        .w_CHIAVE1 = .w_ATTRZOOM.getvar(this.ckey1)
        .w_CHIAVE2 = .w_ATTRZOOM.getvar(this.ckey2)
        .w_CHIAVE3 = .w_ATTRZOOM.getvar(this.ckey3)
        .w_CHIAVE4 = .w_ATTRZOOM.getvar(this.ckey4)
        .w_CHIAVE5 = .w_ATTRZOOM.getvar(this.ckey5)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ATTRZOOM.Calculate()
            .w_CHIAVE1 = .w_ATTRZOOM.getvar(this.ckey1)
            .w_CHIAVE2 = .w_ATTRZOOM.getvar(this.ckey2)
            .w_CHIAVE3 = .w_ATTRZOOM.getvar(this.ckey3)
            .w_CHIAVE4 = .w_ATTRZOOM.getvar(this.ckey4)
            .w_CHIAVE5 = .w_ATTRZOOM.getvar(this.ckey5)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ATTRZOOM.Calculate()
    endwith
  return

  proc Calculate_IFCFWAYSOD()
    with this
          * --- Lettura valori campi chiave e valorizzazione dell'attributo
          GSUT_BKA(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsut_kka
    IF cEvent="FormLoad"
      *Modifico le propriet� dello zoom ATTRZOOM inerenti alla tabella e al file
        this.w_ATTRZOOM.cTable = this.oParentObject.w_IDTABKEY
        this.w_ATTRZOOM.cZoomfile = sys(2015)
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ATTRZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_ATTRZOOM selected")
          .Calculate_IFCFWAYSOD()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kkaPag1 as StdContainer
  Width  = 431
  height = 284
  stdWidth  = 431
  stdheight = 284
  resizeXpos=209
  resizeYpos=119
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ATTRZOOM as cp_zoombox with uid="PLCSBRTTQA",left=3, top=1, width=424,height=281,;
    caption='Object',;
   bGlobalFont=.t.,;
    bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",cTable="",bRetriveAllRows=.f.,cZoomFile="",bOptions=.t.,bAdvOptions=.t.,bNoDefault=.t.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 55393306
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kka','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
