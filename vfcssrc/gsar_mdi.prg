* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mdi                                                        *
*              Plafond                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_77]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-29                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- gsar_mdi
VVL=IIF(g_VALPLA=g_CODEUR, 40, 0)
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mdi")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mdi")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mdi")
  return

* --- Class definition
define class tgsar_mdi as StdPCForm
  Width  = 547
  Height = 395
  Top    = 5
  Left   = 13
  cComment = "Plafond"
  cPrg = "gsar_mdi"
  HelpContextID=80312169
  add object cnt as tcgsar_mdi
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mdi as PCContext
  w_DATLIM0 = space(8)
  w_DATLIM1 = space(8)
  w_DATLIM = space(8)
  w_DATLIMES = space(8)
  w_DICODATT = space(5)
  w_CODAZI = space(10)
  w_VALPLA = space(3)
  w_DECTOT = 0
  w_CALCPIC = 0
  w_PLAMOB = space(1)
  w_ANNRIF = space(4)
  w_PLAINI = 0
  w_DI__ANNO = space(4)
  w_DIPERIOD = 0
  w_DITOTESP = 0
  w_DIPLAUTI = 0
  w_DETUTI = 0
  w_TOTUTI = 0
  w_ESPU12 = 0
  w_UTIU12 = 0
  w_DIFLCALC = space(1)
  w_DIPLADIS = 0
  proc Save(i_oFrom)
    this.w_DATLIM0 = i_oFrom.w_DATLIM0
    this.w_DATLIM1 = i_oFrom.w_DATLIM1
    this.w_DATLIM = i_oFrom.w_DATLIM
    this.w_DATLIMES = i_oFrom.w_DATLIMES
    this.w_DICODATT = i_oFrom.w_DICODATT
    this.w_CODAZI = i_oFrom.w_CODAZI
    this.w_VALPLA = i_oFrom.w_VALPLA
    this.w_DECTOT = i_oFrom.w_DECTOT
    this.w_CALCPIC = i_oFrom.w_CALCPIC
    this.w_PLAMOB = i_oFrom.w_PLAMOB
    this.w_ANNRIF = i_oFrom.w_ANNRIF
    this.w_PLAINI = i_oFrom.w_PLAINI
    this.w_DI__ANNO = i_oFrom.w_DI__ANNO
    this.w_DIPERIOD = i_oFrom.w_DIPERIOD
    this.w_DITOTESP = i_oFrom.w_DITOTESP
    this.w_DIPLAUTI = i_oFrom.w_DIPLAUTI
    this.w_DETUTI = i_oFrom.w_DETUTI
    this.w_TOTUTI = i_oFrom.w_TOTUTI
    this.w_ESPU12 = i_oFrom.w_ESPU12
    this.w_UTIU12 = i_oFrom.w_UTIU12
    this.w_DIFLCALC = i_oFrom.w_DIFLCALC
    this.w_DIPLADIS = i_oFrom.w_DIPLADIS
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DATLIM0 = this.w_DATLIM0
    i_oTo.w_DATLIM1 = this.w_DATLIM1
    i_oTo.w_DATLIM = this.w_DATLIM
    i_oTo.w_DATLIMES = this.w_DATLIMES
    i_oTo.w_DICODATT = this.w_DICODATT
    i_oTo.w_CODAZI = this.w_CODAZI
    i_oTo.w_VALPLA = this.w_VALPLA
    i_oTo.w_DECTOT = this.w_DECTOT
    i_oTo.w_CALCPIC = this.w_CALCPIC
    i_oTo.w_PLAMOB = this.w_PLAMOB
    i_oTo.w_ANNRIF = this.w_ANNRIF
    i_oTo.w_PLAINI = this.w_PLAINI
    i_oTo.w_DI__ANNO = this.w_DI__ANNO
    i_oTo.w_DIPERIOD = this.w_DIPERIOD
    i_oTo.w_DITOTESP = this.w_DITOTESP
    i_oTo.w_DIPLAUTI = this.w_DIPLAUTI
    i_oTo.w_DETUTI = this.w_DETUTI
    i_oTo.w_TOTUTI = this.w_TOTUTI
    i_oTo.w_ESPU12 = this.w_ESPU12
    i_oTo.w_UTIU12 = this.w_UTIU12
    i_oTo.w_DIFLCALC = this.w_DIFLCALC
    i_oTo.w_DIPLADIS = this.w_DIPLADIS
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mdi as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 547
  Height = 395
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=80312169
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PLA_FOND_IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  cFile = "PLA_FOND"
  cKeySelect = "DICODATT"
  cKeyWhere  = "DICODATT=this.w_DICODATT"
  cKeyDetail  = "DICODATT=this.w_DICODATT and DI__ANNO=this.w_DI__ANNO and DIPERIOD=this.w_DIPERIOD"
  cKeyWhereODBC = '"DICODATT="+cp_ToStrODBC(this.w_DICODATT)';

  cKeyDetailWhereODBC = '"DICODATT="+cp_ToStrODBC(this.w_DICODATT)';
      +'+" and DI__ANNO="+cp_ToStrODBC(this.w_DI__ANNO)';
      +'+" and DIPERIOD="+cp_ToStrODBC(this.w_DIPERIOD)';

  cKeyWhereODBCqualified = '"PLA_FOND.DICODATT="+cp_ToStrODBC(this.w_DICODATT)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mdi"
  cComment = "Plafond"
  i_nRowNum = 0
  i_nRowPerPage = 13
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DATLIM0 = ctod('  /  /  ')
  w_DATLIM1 = ctod('  /  /  ')
  w_DATLIM = ctod('  /  /  ')
  w_DATLIMES = ctod('  /  /  ')
  w_DICODATT = space(5)
  w_CODAZI = space(10)
  w_VALPLA = space(3)
  w_DECTOT = 0
  w_CALCPIC = 0
  w_PLAMOB = space(1)
  w_ANNRIF = space(4)
  w_PLAINI = 0
  w_DI__ANNO = space(4)
  w_DIPERIOD = 0
  w_DITOTESP = 0
  w_DIPLAUTI = 0
  w_DETUTI = 0
  w_TOTUTI = 0
  w_ESPU12 = 0
  w_UTIU12 = 0
  w_DIFLCALC = space(1)
  w_DIPLADIS = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mdiPag1","gsar_mdi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='PLA_FOND'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PLA_FOND_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PLA_FOND_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mdi'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PLA_FOND where DICODATT=KeySet.DICODATT
    *                            and DI__ANNO=KeySet.DI__ANNO
    *                            and DIPERIOD=KeySet.DIPERIOD
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsar_mdi
      * --- Setta Ordine per Anno e Periodo
      i_cOrder = 'order by DI__ANNO Desc, DIPERIOD Desc '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PLA_FOND_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PLA_FOND_IDX,2],this.bLoadRecFilter,this.PLA_FOND_IDX,"gsar_mdi")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PLA_FOND')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PLA_FOND.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PLA_FOND '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DICODATT',this.w_DICODATT  )
      select * from (i_cTable) PLA_FOND where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_VALPLA = space(3)
        .w_DECTOT = 0
        .w_PLAMOB = space(1)
        .w_ANNRIF = space(4)
        .w_PLAINI = 0
        .w_TOTUTI = 0
        .w_ESPU12 = 0
        .w_UTIU12 = 0
        .w_DATLIM0 = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(i_DATSYS))),2)+'-'+ALLTR(STR(YEAR(i_DATSYS)-1)))
        .w_DATLIM1 = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(i_DATSYS))),2)+'-'+ALLTR(STR(YEAR(i_DATSYS)-1))) -1
        .w_DATLIM = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(.w_DATLIM0))),2)+'-'+ALLTR(STR(YEAR(.w_DATLIM0))))
        .w_DATLIMES = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(.w_DATLIM1))),2)+'-'+ALLTR(STR(YEAR(.w_DATLIM1))))
        .w_DICODATT = NVL(DICODATT,space(5))
          .link_1_6('Load')
          .link_1_7('Load')
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PLA_FOND')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTUTI = 0
      scan
        with this
          .w_DI__ANNO = NVL(DI__ANNO,space(4))
          .w_DIPERIOD = NVL(DIPERIOD,0)
          .w_DITOTESP = NVL(DITOTESP,0)
          .w_DIPLAUTI = NVL(DIPLAUTI,0)
        .w_DETUTI = IIF(.w_ANNRIF=.w_DI__ANNO, .w_DIPLAUTI, 0)
          .w_DIFLCALC = NVL(DIFLCALC,space(1))
          .w_DIPLADIS = NVL(DIPLADIS,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTUTI = .w_TOTUTI+.w_DETUTI
          replace DI__ANNO with .w_DI__ANNO
          replace DIPERIOD with .w_DIPERIOD
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_DATLIM0 = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(i_DATSYS))),2)+'-'+ALLTR(STR(YEAR(i_DATSYS)-1)))
        .w_DATLIM1 = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(i_DATSYS))),2)+'-'+ALLTR(STR(YEAR(i_DATSYS)-1))) -1
        .w_DATLIM = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(.w_DATLIM0))),2)+'-'+ALLTR(STR(YEAR(.w_DATLIM0))))
        .w_DATLIMES = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(.w_DATLIM1))),2)+'-'+ALLTR(STR(YEAR(.w_DATLIM1))))
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_DATLIM0=ctod("  /  /  ")
      .w_DATLIM1=ctod("  /  /  ")
      .w_DATLIM=ctod("  /  /  ")
      .w_DATLIMES=ctod("  /  /  ")
      .w_DICODATT=space(5)
      .w_CODAZI=space(10)
      .w_VALPLA=space(3)
      .w_DECTOT=0
      .w_CALCPIC=0
      .w_PLAMOB=space(1)
      .w_ANNRIF=space(4)
      .w_PLAINI=0
      .w_DI__ANNO=space(4)
      .w_DIPERIOD=0
      .w_DITOTESP=0
      .w_DIPLAUTI=0
      .w_DETUTI=0
      .w_TOTUTI=0
      .w_ESPU12=0
      .w_UTIU12=0
      .w_DIFLCALC=space(1)
      .w_DIPLADIS=0
      if .cFunction<>"Filter"
        .w_DATLIM0 = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(i_DATSYS))),2)+'-'+ALLTR(STR(YEAR(i_DATSYS)-1)))
        .w_DATLIM1 = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(i_DATSYS))),2)+'-'+ALLTR(STR(YEAR(i_DATSYS)-1))) -1
        .w_DATLIM = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(.w_DATLIM0))),2)+'-'+ALLTR(STR(YEAR(.w_DATLIM0))))
        .w_DATLIMES = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(.w_DATLIM1))),2)+'-'+ALLTR(STR(YEAR(.w_DATLIM1))))
        .DoRTCalc(5,5,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODAZI))
         .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_VALPLA))
         .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .DoRTCalc(10,16,.f.)
        .w_DETUTI = IIF(.w_ANNRIF=.w_DI__ANNO, .w_DIPLAUTI, 0)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PLA_FOND')
    this.DoRTCalc(18,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_1_19.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PLA_FOND',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PLA_FOND_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODATT,"DICODATT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DI__ANNO C(4);
      ,t_DIPERIOD N(2);
      ,t_DITOTESP N(18,4);
      ,t_DIPLAUTI N(18,4);
      ,t_DIPLADIS N(18,4);
      ,DI__ANNO C(4);
      ,DIPERIOD N(2);
      ,t_DETUTI N(18,4);
      ,t_DIFLCALC C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mdibodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDI__ANNO_2_1.controlsource=this.cTrsName+'.t_DI__ANNO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIPERIOD_2_2.controlsource=this.cTrsName+'.t_DIPERIOD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDITOTESP_2_3.controlsource=this.cTrsName+'.t_DITOTESP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLAUTI_2_4.controlsource=this.cTrsName+'.t_DIPLAUTI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLADIS_2_7.controlsource=this.cTrsName+'.t_DIPLADIS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(59)
    this.AddVLine(104)
    this.AddVLine(240)
    this.AddVLine(375)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDI__ANNO_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PLA_FOND_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PLA_FOND_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PLA_FOND_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PLA_FOND_IDX,2])
      *
      * insert into PLA_FOND
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PLA_FOND')
        i_extval=cp_InsertValODBCExtFlds(this,'PLA_FOND')
        i_cFldBody=" "+;
                  "(DICODATT,DI__ANNO,DIPERIOD,DITOTESP,DIPLAUTI"+;
                  ",DIFLCALC,DIPLADIS,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DICODATT)+","+cp_ToStrODBC(this.w_DI__ANNO)+","+cp_ToStrODBC(this.w_DIPERIOD)+","+cp_ToStrODBC(this.w_DITOTESP)+","+cp_ToStrODBC(this.w_DIPLAUTI)+;
             ","+cp_ToStrODBC(this.w_DIFLCALC)+","+cp_ToStrODBC(this.w_DIPLADIS)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PLA_FOND')
        i_extval=cp_InsertValVFPExtFlds(this,'PLA_FOND')
        cp_CheckDeletedKey(i_cTable,0,'DICODATT',this.w_DICODATT,'DI__ANNO',this.w_DI__ANNO,'DIPERIOD',this.w_DIPERIOD)
        INSERT INTO (i_cTable) (;
                   DICODATT;
                  ,DI__ANNO;
                  ,DIPERIOD;
                  ,DITOTESP;
                  ,DIPLAUTI;
                  ,DIFLCALC;
                  ,DIPLADIS;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DICODATT;
                  ,this.w_DI__ANNO;
                  ,this.w_DIPERIOD;
                  ,this.w_DITOTESP;
                  ,this.w_DIPLAUTI;
                  ,this.w_DIFLCALC;
                  ,this.w_DIPLADIS;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PLA_FOND_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PLA_FOND_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_DI__ANNO<>space(4) and t_DIPERIOD<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PLA_FOND')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and DI__ANNO="+cp_ToStrODBC(&i_TN.->DI__ANNO)+;
                 " and DIPERIOD="+cp_ToStrODBC(&i_TN.->DIPERIOD)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PLA_FOND')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and DI__ANNO=&i_TN.->DI__ANNO;
                      and DIPERIOD=&i_TN.->DIPERIOD;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_DI__ANNO<>space(4) and t_DIPERIOD<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and DI__ANNO="+cp_ToStrODBC(&i_TN.->DI__ANNO)+;
                            " and DIPERIOD="+cp_ToStrODBC(&i_TN.->DIPERIOD)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and DI__ANNO=&i_TN.->DI__ANNO;
                            and DIPERIOD=&i_TN.->DIPERIOD;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace DI__ANNO with this.w_DI__ANNO
              replace DIPERIOD with this.w_DIPERIOD
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PLA_FOND
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PLA_FOND')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DITOTESP="+cp_ToStrODBC(this.w_DITOTESP)+;
                     ",DIPLAUTI="+cp_ToStrODBC(this.w_DIPLAUTI)+;
                     ",DIFLCALC="+cp_ToStrODBC(this.w_DIFLCALC)+;
                     ",DIPLADIS="+cp_ToStrODBC(this.w_DIPLADIS)+;
                     ",DI__ANNO="+cp_ToStrODBC(this.w_DI__ANNO)+;
                     ",DIPERIOD="+cp_ToStrODBC(this.w_DIPERIOD)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and DI__ANNO="+cp_ToStrODBC(DI__ANNO)+;
                             " and DIPERIOD="+cp_ToStrODBC(DIPERIOD)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PLA_FOND')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DITOTESP=this.w_DITOTESP;
                     ,DIPLAUTI=this.w_DIPLAUTI;
                     ,DIFLCALC=this.w_DIFLCALC;
                     ,DIPLADIS=this.w_DIPLADIS;
                     ,DI__ANNO=this.w_DI__ANNO;
                     ,DIPERIOD=this.w_DIPERIOD;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and DI__ANNO=&i_TN.->DI__ANNO;
                                      and DIPERIOD=&i_TN.->DIPERIOD;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PLA_FOND_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PLA_FOND_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_DI__ANNO<>space(4) and t_DIPERIOD<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PLA_FOND
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and DI__ANNO="+cp_ToStrODBC(&i_TN.->DI__ANNO)+;
                            " and DIPERIOD="+cp_ToStrODBC(&i_TN.->DIPERIOD)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and DI__ANNO=&i_TN.->DI__ANNO;
                              and DIPERIOD=&i_TN.->DIPERIOD;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_DI__ANNO<>space(4) and t_DIPERIOD<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PLA_FOND_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PLA_FOND_IDX,2])
    if i_bUpd
      with this
          .w_DATLIM0 = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(i_DATSYS))),2)+'-'+ALLTR(STR(YEAR(i_DATSYS)-1)))
          .w_DATLIM1 = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(i_DATSYS))),2)+'-'+ALLTR(STR(YEAR(i_DATSYS)-1))) -1
          .w_DATLIM = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(.w_DATLIM0))),2)+'-'+ALLTR(STR(YEAR(.w_DATLIM0))))
          .w_DATLIMES = cp_CharToDate('01-'+RIGHT('00'+ALLTR(STR(MONTH(.w_DATLIM1))),2)+'-'+ALLTR(STR(YEAR(.w_DATLIM1))))
        .DoRTCalc(5,5,.t.)
          .link_1_6('Full')
          .link_1_7('Full')
        .DoRTCalc(8,8,.t.)
          .w_CALCPIC = DEFPIC(.w_DECTOT)
        .DoRTCalc(10,16,.t.)
          .w_TOTUTI = .w_TOTUTI-.w_detuti
          .w_DETUTI = IIF(.w_ANNRIF=.w_DI__ANNO, .w_DIPLAUTI, 0)
          .w_TOTUTI = .w_TOTUTI+.w_detuti
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(18,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DETUTI with this.w_DETUTI
      replace t_DIFLCALC with this.w_DIFLCALC
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDIPERIOD_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDIPERIOD_2_2.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oANNRIF_1_11.visible=!this.oPgFrm.Page1.oPag.oANNRIF_1_11.mHide()
    this.oPgFrm.Page1.oPag.oPLAINI_1_12.visible=!this.oPgFrm.Page1.oPag.oPLAINI_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oTOTUTI_3_1.visible=!this.oPgFrm.Page1.oPag.oTOTUTI_3_1.mHide()
    this.oPgFrm.Page1.oPag.oESPU12_3_2.visible=!this.oPgFrm.Page1.oPag.oESPU12_3_2.mHide()
    this.oPgFrm.Page1.oPag.oUTIU12_3_3.visible=!this.oPgFrm.Page1.oPag.oUTIU12_3_3.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZPLAMOB,AZANNRIF,AZPLAINI,AZVALPLA";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZPLAMOB,AZANNRIF,AZPLAINI,AZVALPLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(10))
      this.w_PLAMOB = NVL(_Link_.AZPLAMOB,space(1))
      this.w_ANNRIF = NVL(_Link_.AZANNRIF,space(4))
      this.w_PLAINI = NVL(_Link_.AZPLAINI,0)
      this.w_VALPLA = NVL(_Link_.AZVALPLA,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(10)
      endif
      this.w_PLAMOB = space(1)
      this.w_ANNRIF = space(4)
      this.w_PLAINI = 0
      this.w_VALPLA = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALPLA
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALPLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALPLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALPLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALPLA)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALPLA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALPLA = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALPLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oVALPLA_1_7.value==this.w_VALPLA)
      this.oPgFrm.Page1.oPag.oVALPLA_1_7.value=this.w_VALPLA
    endif
    if not(this.oPgFrm.Page1.oPag.oPLAMOB_1_10.RadioValue()==this.w_PLAMOB)
      this.oPgFrm.Page1.oPag.oPLAMOB_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANNRIF_1_11.value==this.w_ANNRIF)
      this.oPgFrm.Page1.oPag.oANNRIF_1_11.value=this.w_ANNRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oPLAINI_1_12.value==this.w_PLAINI)
      this.oPgFrm.Page1.oPag.oPLAINI_1_12.value=this.w_PLAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTUTI_3_1.value==this.w_TOTUTI)
      this.oPgFrm.Page1.oPag.oTOTUTI_3_1.value=this.w_TOTUTI
    endif
    if not(this.oPgFrm.Page1.oPag.oESPU12_3_2.value==this.w_ESPU12)
      this.oPgFrm.Page1.oPag.oESPU12_3_2.value=this.w_ESPU12
    endif
    if not(this.oPgFrm.Page1.oPag.oUTIU12_3_3.value==this.w_UTIU12)
      this.oPgFrm.Page1.oPag.oUTIU12_3_3.value=this.w_UTIU12
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDI__ANNO_2_1.value==this.w_DI__ANNO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDI__ANNO_2_1.value=this.w_DI__ANNO
      replace t_DI__ANNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDI__ANNO_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPERIOD_2_2.value==this.w_DIPERIOD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPERIOD_2_2.value=this.w_DIPERIOD
      replace t_DIPERIOD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPERIOD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITOTESP_2_3.value==this.w_DITOTESP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITOTESP_2_3.value=this.w_DITOTESP
      replace t_DITOTESP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITOTESP_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLAUTI_2_4.value==this.w_DIPLAUTI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLAUTI_2_4.value=this.w_DIPLAUTI
      replace t_DIPLAUTI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLAUTI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLADIS_2_7.value==this.w_DIPLADIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLADIS_2_7.value=this.w_DIPLADIS
      replace t_DIPLADIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPLADIS_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'PLA_FOND')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_DIPERIOD>0 AND .w_DIPERIOD<13) and (NOT EMPTY(.w_DI__ANNO)) and (.w_DI__ANNO<>space(4) and .w_DIPERIOD<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIPERIOD_2_2
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if .w_DI__ANNO<>space(4) and .w_DIPERIOD<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_DI__ANNO<>space(4) and t_DIPERIOD<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DI__ANNO=space(4)
      .w_DIPERIOD=0
      .w_DITOTESP=0
      .w_DIPLAUTI=0
      .w_DETUTI=0
      .w_DIFLCALC=space(1)
      .w_DIPLADIS=0
      .DoRTCalc(1,16,.f.)
        .w_DETUTI = IIF(.w_ANNRIF=.w_DI__ANNO, .w_DIPLAUTI, 0)
    endwith
    this.DoRTCalc(18,22,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DI__ANNO = t_DI__ANNO
    this.w_DIPERIOD = t_DIPERIOD
    this.w_DITOTESP = t_DITOTESP
    this.w_DIPLAUTI = t_DIPLAUTI
    this.w_DETUTI = t_DETUTI
    this.w_DIFLCALC = t_DIFLCALC
    this.w_DIPLADIS = t_DIPLADIS
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DI__ANNO with this.w_DI__ANNO
    replace t_DIPERIOD with this.w_DIPERIOD
    replace t_DITOTESP with this.w_DITOTESP
    replace t_DIPLAUTI with this.w_DIPLAUTI
    replace t_DETUTI with this.w_DETUTI
    replace t_DIFLCALC with this.w_DIFLCALC
    replace t_DIPLADIS with this.w_DIPLADIS
    if i_srv='A'
      replace DI__ANNO with this.w_DI__ANNO
      replace DIPERIOD with this.w_DIPERIOD
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTUTI = .w_TOTUTI-.w_detuti
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mdiPag1 as StdContainer
  Width  = 543
  height = 395
  stdWidth  = 543
  stdheight = 395
  resizeXpos=540
  resizeYpos=240
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVALPLA_1_7 as StdField with uid="MRIHSBBVWN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_VALPLA", cQueryName = "VALPLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 21729110,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=347, Top=9, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALPLA"

  func oVALPLA_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oPLAMOB_1_10 as StdCheck with uid="QYUXPTQEFM",rtseq=10,rtrep=.f.,left=14, top=6, caption="Plafond mobile", enabled=.f.,;
    ToolTipText = "Se attivo: gestisce il plafond mobile",;
    HelpContextID = 41413110,;
    cFormVar="w_PLAMOB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPLAMOB_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PLAMOB,&i_cF..t_PLAMOB),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oPLAMOB_1_10.GetRadio()
    this.Parent.oContained.w_PLAMOB = this.RadioValue()
    return .t.
  endfunc

  func oPLAMOB_1_10.ToRadio()
    this.Parent.oContained.w_PLAMOB=trim(this.Parent.oContained.w_PLAMOB)
    return(;
      iif(this.Parent.oContained.w_PLAMOB=='S',1,;
      0))
  endfunc

  func oPLAMOB_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oANNRIF_1_11 as StdField with uid="KSZRWWPCCQ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ANNRIF", cQueryName = "ANNRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento per il calcolo del plafond fisso",;
    HelpContextID = 102611718,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=240, Top=9, InputMask=replicate('X',4)

  func oANNRIF_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PLAMOB='S')
    endwith
    endif
  endfunc

  add object oPLAINI_1_12 as StdField with uid="NBUSSXSEJM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PLAINI", cQueryName = "PLAINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo del plafond ad inizio anno",;
    HelpContextID = 157542902,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=240, Top=37, cSayPict="v_PV(40+VVL)", cGetPict="v_PV(40+VVL)"

  func oPLAINI_1_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PLAMOB='S')
    endwith
    endif
  endfunc


  add object oObj_1_19 as cp_runprogram with uid="VNLMXLLYNQ",left=5, top=403, width=557,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BCF",;
    cEvent = "w_DI__ANNO Changed,w_DIPERIOD Changed,w_DIPLAUTI Changed,w_DITOTESP Changed,Load",;
    nPag=1;
    , HelpContextID = 97685478


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=68, width=530,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="DI__ANNO",Label1="Anno",Field2="DIPERIOD",Label2="Mese",Field3="DITOTESP",Label3="Esportazioni",Field4="DIPLAUTI",Label4="Plafond utilizzato",Field5="DIPLADIS",Label5="Plafond inizio periodo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 32656774

  add object oStr_1_13 as StdString with uid="GLQVLTKOPB",Visible=.t., Left=125, Top=10,;
    Alignment=1, Width=115, Height=18,;
    Caption="Anno di riferimento:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (.w_PLAMOB='S')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="CKRWYWRYCH",Visible=.t., Left=94, Top=37,;
    Alignment=1, Width=146, Height=18,;
    Caption="Plafond ad inizio anno:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_PLAMOB='S')
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="YDIERAWLPU",Visible=.t., Left=244, Top=343,;
    Alignment=0, Width=136, Height=18,;
    Caption="Tot.utilizzi da inizio anno"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_PLAMOB='S')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="GFAAKXOODD",Visible=.t., Left=109, Top=343,;
    Alignment=0, Width=126, Height=18,;
    Caption="Esport.ultimi 12 mesi"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_PLAMOB<>'S')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="MGJBJIFCYV",Visible=.t., Left=247, Top=343,;
    Alignment=0, Width=132, Height=18,;
    Caption="Utilizz. ultimi 12 mesi"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_PLAMOB<>'S')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="EXAESWFUOY",Visible=.t., Left=297, Top=9,;
    Alignment=1, Width=47, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=87,;
    width=526+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*13*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=88,width=525+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*13*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTUTI_3_1 as StdField with uid="JXDMVQTGPQ",rtseq=18,rtrep=.f.,;
    cFormVar="w_TOTUTI",value=0,enabled=.f.,;
    HelpContextID = 164699446,;
    cQueryName = "TOTUTI",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=240, Top=362, cSayPict=[v_PV(40+VVL)], cGetPict=[v_PV(40+VVL)]

  func oTOTUTI_3_1.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PLAMOB='S')
    endwith
    endif
  endfunc

  add object oESPU12_3_2 as StdField with uid="LLLQTCFDBX",rtseq=19,rtrep=.f.,;
    cFormVar="w_ESPU12",value=0,enabled=.f.,;
    HelpContextID = 10543174,;
    cQueryName = "ESPU12",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=105, Top=362, cSayPict=[v_PV(40+VVL)], cGetPict=[v_PV(40+VVL)]

  func oESPU12_3_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PLAMOB<>'S')
    endwith
    endif
  endfunc

  add object oUTIU12_3_3 as StdField with uid="ULVWQNZLQJ",rtseq=20,rtrep=.f.,;
    cFormVar="w_UTIU12",value=0,enabled=.f.,;
    HelpContextID = 10515014,;
    cQueryName = "UTIU12",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=240, Top=362, cSayPict=[v_PV(40+VVL)], cGetPict=[v_PV(40+VVL)]

  func oUTIU12_3_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PLAMOB<>'S')
    endwith
    endif
  endfunc
enddefine

* --- Defining Body row
define class tgsar_mdiBodyRow as CPBodyRowCnt
  Width=516
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDI__ANNO_2_1 as StdTrsField with uid="VHXIRWBWOO",rtseq=13,rtrep=.t.,;
    cFormVar="w_DI__ANNO",value=space(4),nZero=4,isprimarykey=.t.,;
    ToolTipText = "Anno della denuncia, dato aggiornato automaticamente dalla stampa liquidazioni",;
    HelpContextID = 229361285,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=-2, Top=0, cSayPict=['9999'], cGetPict=['9999'], InputMask=replicate('X',4)

  add object oDIPERIOD_2_2 as StdTrsField with uid="WDRZIHPJSQ",rtseq=14,rtrep=.t.,;
    cFormVar="w_DIPERIOD",value=0,isprimarykey=.t.,;
    ToolTipText = "Mese di riferimento per il calcolo del plafond",;
    HelpContextID = 161535610,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=56, Top=0, cSayPict=["99"], cGetPict=["99"]

  func oDIPERIOD_2_2.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DI__ANNO))
    endwith
  endfunc

  func oDIPERIOD_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DIPERIOD>0 AND .w_DIPERIOD<13)
    endwith
    return bRes
  endfunc

  add object oDITOTESP_2_3 as StdTrsField with uid="MKWTJMMNLR",rtseq=15,rtrep=.t.,;
    cFormVar="w_DITOTESP",value=0,;
    ToolTipText = "Importo plafond esportazioni, gestito in caso di plafond variabile",;
    HelpContextID = 97195654,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=101, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oDIPLAUTI_2_4 as StdTrsField with uid="ZGQXXDLHRE",rtseq=16,rtrep=.t.,;
    cFormVar="w_DIPLAUTI",value=0,;
    ToolTipText = "Importo plafond utilizzato",;
    HelpContextID = 191375745,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=237, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oDIPLADIS_2_7 as StdTrsField with uid="VQXBKKKGSH",rtseq=22,rtrep=.t.,;
    cFormVar="w_DIPLADIS",value=0,;
    ToolTipText = "Plafond disponibile inizio periodo",;
    HelpContextID = 208152951,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=372, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]
  add object oLast as LastKeyMover
  * ---
  func oDI__ANNO_2_1.When()
    return(.t.)
  proc oDI__ANNO_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDI__ANNO_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=12
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mdi','PLA_FOND','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DICODATT=PLA_FOND.DICODATT";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
