* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: lookout                                                         *
*              Seleziona vista preferenziale                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_13]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-10                                                      *
* Last revis.: 2010-10-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo,pProg,pNume
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tlookout",oParentObject,m.pTipo,m.pProg,m.pNume)
return(i_retval)

define class tlookout as StdBatch
  * --- Local variables
  pTipo = space(1)
  pProg = space(8)
  pNume = space(1)
  w_TROV = .f.
  w_OUDESCRI = space(100)
  w_NOMPRG = space(8)
  w_OUNOMREP = space(50)
  w_CODUTE = 0
  w_OUNOMQUE = space(50)
  w_CODAZI = space(5)
  w_OUNOMPRG = space(30)
  w_UTE = 0
  w_OUROWNUM = space(3)
  w_AZI = space(5)
  w_NUME = space(1)
  * --- WorkFile variables
  OUT_PUTS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Selezione Vista Preferenziale
    this.w_NOMPRG = left(this.pProg + space(30),30)
    this.w_CODUTE = i_CODUTE
    this.w_CODAZI = i_CODAZI
    this.w_TROV = .F.
    this.w_NUME = IIF(EMPTY(NVL(this.pNume," ")), " ", "X")
    if this.pTipo="1"
      * --- Cerca Vista Predefinita
      * --- Select from QUERY\LOOKOUT
      do vq_exec with 'QUERY\LOOKOUT',this,'_Curs_QUERY_LOOKOUT','',.f.,.t.
      if used('_Curs_QUERY_LOOKOUT')
        select _Curs_QUERY_LOOKOUT
        locate for 1=1
        do while not(eof())
        if !this.w_TROV AND OUPREDEF="S"
          this.oParentObject.w_ODES = NVL(OUDESCRI, SPACE(100))
          this.oParentObject.w_OREP = NVL(OUNOMREP, SPACE(50))
          this.oParentObject.w_OQRY = NVL(OUNOMQUE, SPACE(50))
          if this.w_NUME="X"
            this.oParentObject.w_ONUME = NVL(OUROWNUM, 0)
          endif
          this.w_TROV = .T.
        endif
          select _Curs_QUERY_LOOKOUT
          continue
        enddo
        use
      endif
    else
      * --- Seleziona Viste del Form
      this.w_OUNOMPRG = SPACE(30)
      vx_exec("QUERY\LOOKOUT.VZM",this)
      if NOT EMPTY(NVL(this.w_OUNOMPRG, SPACE(30)))
        this.oParentObject.w_ODES = NVL(this.w_OUDESCRI, SPACE(100))
        this.oParentObject.w_OREP = NVL(this.w_OUNOMREP, SPACE(50))
        this.oParentObject.w_OQRY = NVL(this.w_OUNOMQUE, SPACE(50))
        if this.w_NUME="X"
          this.oParentObject.w_ONUME = NVL(this.w_OUROWNUM, 0)
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pTipo,pProg,pNume)
    this.pTipo=pTipo
    this.pProg=pProg
    this.pNume=pNume
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OUT_PUTS'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_QUERY_LOOKOUT')
      use in _Curs_QUERY_LOOKOUT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo,pProg,pNume"
endproc
