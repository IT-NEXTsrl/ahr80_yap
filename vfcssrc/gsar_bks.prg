* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bks                                                        *
*              Controlli scadenze varie                                        *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_58]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-27                                                      *
* Last revis.: 2012-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bks",oParentObject,m.pOper)
return(i_retval)

define class tgsar_bks as StdBatch
  * --- Local variables
  pOper = space(15)
  w_OREC = 0
  w_MESS1 = space(10)
  w_TOTPAR = 0
  w_NOCLF = .f.
  w_MESS = space(10)
  w_NOPAG = .f.
  w_OK = .f.
  w_OK1 = .f.
  w_TIPO = .f.
  w_MESS1 = space(10)
  w_PADRE = .NULL.
  * --- WorkFile variables
  PAR_TITE_idx=0
  SCA_VARI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali Scadenze Varie (da GSAR_ASC)
    do case
      case this.pOper="Controlli"
        this.w_PADRE = this.oParentObject.GSTE_MPA
        * --- controlli fatti solo in caso di modifica o inserimento
        if this.oParentObject.cFunction = "Query"
          i_retcode = 'stop'
          return
        endif
        this.w_NOPAG = .F.
        this.w_NOCLF = .F.
        this.w_TOTPAR = 0
        this.w_TIPO = .F.
        this.w_PADRE.MarkPos()     
        * --- Con la select controllo di non aver selezionato sue scadenze uguali da chiudere.
        *     Se ho selezionato la scadenza due volte, al salvataggio blocco.
        *     Altrimenti tutto rimane come prima
         
 SELECT t_PTSERRIF, t_PTORDRIF, t_PTNUMRIF, MAX(t_PTNUMPAR) AS NUMPAR, MAX(t_PTDATSCA) AS DATSCA, COUNT(*) AS CONTA FROM(this.oParentObject.GSTE_MPA.cTrsName) where t_PTORDRIF <> 0; 
 group by t_PTSERRIF,t_PTORDRIF,t_PTNUMRIF having CONTA > 1; 
 INTO CURSOR TEMP
        this.w_PADRE.RePos(.T.)     
        if RECCOUNT("TEMP")=0
          SELECT (this.oParentObject.GSTE_MPA.cTrsName)
          this.w_OREC = RECNO()
          GO TOP
          SCAN FOR NOT EMPTY(CP_TODATE(t_PTDATSCA)) AND t_PTTOTIMP<>0 AND NOT EMPTY(NVL(t_PTNUMPAR," "))
          this.w_TOTPAR = this.w_TOTPAR + (t_PTTOTIMP * IIF(t_PT_SEGNO="D", 1, -1))
          if empty(NVL(t_PTMODPAG,""))
            this.w_NOPAG = .T.
          endif
          if this.oParentObject.w_SCTIPCLF<>t_PTTIPCON OR this.oParentObject.w_SCCODCLF<>t_PTCODCON
            this.w_NOCLF = .T.
          endif
          if t_TIPPAG="RB" 
            this.w_TIPO = .T.
          endif
          ENDSCAN
          if this.w_OREC>0 AND this.w_OREC<=RECCOUNT()
            GOTO this.w_OREC
          endif
          this.w_OK = .T.
          if this.w_NOPAG=.T. OR this.w_NOCLF=.T.
            this.w_MESS = "Verificare righe scadenze%0"
            if this.w_NOPAG
              this.w_MESS = "Verificare righe scadenze%0Tipo pagamento non inserito"
            endif
            if this.w_NOCLF
              this.w_MESS = "Ricalcolare righe scadenze%0Mancata corrispondenza tra i conti delle scadenze e il conto di intestazione"
            endif
            this.w_OK = .F.
            ah_ErrorMsg(this.w_MESS,,"")
          endif
          if this.w_TIPO=.T. AND EMPTY(this.oParentObject.w_SCNUMDOC)
            this.w_MESS1 = "Attenzione: scadenza diversa con tipo pagamento RB, inserire gli estremi del documento per generare i file RiBa/bonifici%0Confermi ugualmente?"
            this.w_OK = ah_YesNo(this.w_MESS1)
          endif
          if this.w_OK and this.oParentObject.w_PFLCRSA="C"
            * --- Test se Totale Importo<>Totale Rate
            this.w_OK1 = (this.oParentObject.w_SCIMPSCA*IIF(this.oParentObject.w_SCTIPSCA="C",1,-1))<>this.w_TOTPAR
            if this.oParentObject.w_SCTIPCLF="G"
              this.w_OK1 = ABS(this.oParentObject.w_SCIMPSCA)<>ABS(this.w_TOTPAR)
            endif
            if this.w_OK1
              this.w_MESS = "Mancata corrispondenza tra gli importi delle righe e il totale impostato%0Confermi ugualmente?"
              this.w_OK = ah_YesNo(this.w_MESS)
            endif
          endif
          if this.oParentObject.w_PFLCRSA="S"
            this.oParentObject.w_SCIMPSCA = ABS(this.w_TOTPAR)
          endif
        else
          this.w_OK = .F.
          ah_ErrorMsg("Partita n. %1 del %2 ripetuta",,"", ALLTRIM(NUMPAR), DTOC(DATSCA))
        endif
        if this.w_OK = .F.
          this.oParentObject.w_RESCHK = -1
        endif
        if USED("TEMP")
           
 SELECT TEMP 
 USE
        endif
      case this.pOper="Change"
        this.w_PADRE = this.oParentObject.GSTE_MPA
        this.w_PADRE.Markpos()     
        this.w_PADRE.FirstRow()     
        do while Not this.w_PADRE.Eof_Trs()
          if Not this.w_PADRE.FullRow() 
            this.w_PADRE.Set("w_PTFLCRSA",IIF(this.oParentObject.w_PFLCRSA="C", 1, 2))     
          endif
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.Repos()     
      case this.pOper="Delete"
        if Isalt()
          * --- Aggiorno stato incasso documenti collegati
          gsal_baf(this,this.oParentObject.w_SCCODICE,"S",Cp_chartodate("  -  -    "),Cp_chartodate("  -  -    "),"D")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      otherwise
        if Not btrserr
          * --- necessario poich� gli oggetti calcolate non sono condizionati 
          *     a eventuali annullamenti della transazione
          * --- Write into PAR_TITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTDATREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SCDATREG),'PAR_TITE','PTDATREG');
                +i_ccchkf ;
            +" where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SCCODICE);
                +" and PTROWORD = "+cp_ToStrODBC(-1);
                   )
          else
            update (i_cTable) set;
                PTDATREG = this.oParentObject.w_SCDATREG;
                &i_ccchkf. ;
             where;
                PTSERIAL = this.oParentObject.w_SCCODICE;
                and PTROWORD = -1;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore aggiornamento data registrazione'
            return
          endif
          if this.oParentObject.w_PFLCRSA="S" AND Isalt()
            * --- Aggiorno stato incasso documenti collegati
            gsal_baf(this," ","T",Cp_chartodate("  -  -    "),Cp_chartodate("  -  -    "),"A")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='SCA_VARI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
