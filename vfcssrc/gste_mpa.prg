* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_mpa                                                        *
*              Situazione scadenze varie                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_176]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-27                                                      *
* Last revis.: 2014-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgste_mpa")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgste_mpa")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgste_mpa")
  return

* --- Class definition
define class tgste_mpa as StdPCForm
  Width  = 797
  Height = 194
  Top    = 77
  Left   = 12
  cComment = "Situazione scadenze varie"
  cPrg = "gste_mpa"
  HelpContextID=147469417
  add object cnt as tcgste_mpa
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgste_mpa as PCContext
  w_ORIGINE = space(1)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CODVAL = space(3)
  w_TIPCLF = space(1)
  w_CODCON = space(15)
  w_IMPSCA = 0
  w_DATVAL = space(8)
  w_CODPAG = space(5)
  w_TIPSCA = space(1)
  w_FLIMPG = space(1)
  w_OBTEST = space(8)
  w_ALFDOC = space(10)
  w_NUMDOC = 0
  w_PTIMPDOC = 0
  w_TIPOPE = space(10)
  w_PTCAOAPE = 0
  w_PTDATAPE = space(8)
  w_PTDATSCA = space(8)
  w_PTCODVAL = space(3)
  w_PTCAOVAL = 0
  w_PTNUMPAR = space(31)
  w_PT_SEGNO = space(1)
  w_PTTOTIMP = 0
  w_PTNUMDOC = 0
  w_PTALFDOC = space(10)
  w_PTDATDOC = space(8)
  w_DESVAL = space(35)
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTFLIMPE = space(2)
  w_PTNUMDIS = space(10)
  w_PTMODPAG = space(10)
  w_PTBANAPP = space(10)
  w_TIPPAG = space(2)
  w_PTBANNOS = space(15)
  w_PTFLINDI = space(1)
  w_PTFLCRSA = space(2)
  w_FLCRSA = space(1)
  w_DESBA = space(35)
  w_PTDESRIG = space(50)
  w_PTNUMCOR = space(25)
  w_PTFLSOSP = space(1)
  w_PTNUMPRO = 0
  w_PTCODAGE = space(5)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_PTFLVABD = space(1)
  w_PTFLRAGG = space(1)
  w_PTDATREG = space(8)
  w_DATSCA = space(8)
  w_SIMBVA = space(5)
  w_NUMPAR = space(10)
  w_TOTPAR = 0
  w_IMPPAR = 0
  proc Save(i_oFrom)
    this.w_ORIGINE = i_oFrom.w_ORIGINE
    this.w_PTSERIAL = i_oFrom.w_PTSERIAL
    this.w_PTROWORD = i_oFrom.w_PTROWORD
    this.w_CODVAL = i_oFrom.w_CODVAL
    this.w_TIPCLF = i_oFrom.w_TIPCLF
    this.w_CODCON = i_oFrom.w_CODCON
    this.w_IMPSCA = i_oFrom.w_IMPSCA
    this.w_DATVAL = i_oFrom.w_DATVAL
    this.w_CODPAG = i_oFrom.w_CODPAG
    this.w_TIPSCA = i_oFrom.w_TIPSCA
    this.w_FLIMPG = i_oFrom.w_FLIMPG
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_ALFDOC = i_oFrom.w_ALFDOC
    this.w_NUMDOC = i_oFrom.w_NUMDOC
    this.w_PTIMPDOC = i_oFrom.w_PTIMPDOC
    this.w_TIPOPE = i_oFrom.w_TIPOPE
    this.w_PTCAOAPE = i_oFrom.w_PTCAOAPE
    this.w_PTDATAPE = i_oFrom.w_PTDATAPE
    this.w_PTDATSCA = i_oFrom.w_PTDATSCA
    this.w_PTCODVAL = i_oFrom.w_PTCODVAL
    this.w_PTCAOVAL = i_oFrom.w_PTCAOVAL
    this.w_PTNUMPAR = i_oFrom.w_PTNUMPAR
    this.w_PT_SEGNO = i_oFrom.w_PT_SEGNO
    this.w_PTTOTIMP = i_oFrom.w_PTTOTIMP
    this.w_PTNUMDOC = i_oFrom.w_PTNUMDOC
    this.w_PTALFDOC = i_oFrom.w_PTALFDOC
    this.w_PTDATDOC = i_oFrom.w_PTDATDOC
    this.w_DESVAL = i_oFrom.w_DESVAL
    this.w_PTTIPCON = i_oFrom.w_PTTIPCON
    this.w_PTCODCON = i_oFrom.w_PTCODCON
    this.w_PTFLIMPE = i_oFrom.w_PTFLIMPE
    this.w_PTNUMDIS = i_oFrom.w_PTNUMDIS
    this.w_PTMODPAG = i_oFrom.w_PTMODPAG
    this.w_PTBANAPP = i_oFrom.w_PTBANAPP
    this.w_TIPPAG = i_oFrom.w_TIPPAG
    this.w_PTBANNOS = i_oFrom.w_PTBANNOS
    this.w_PTFLINDI = i_oFrom.w_PTFLINDI
    this.w_PTFLCRSA = i_oFrom.w_PTFLCRSA
    this.w_FLCRSA = i_oFrom.w_FLCRSA
    this.w_DESBA = i_oFrom.w_DESBA
    this.w_PTDESRIG = i_oFrom.w_PTDESRIG
    this.w_PTNUMCOR = i_oFrom.w_PTNUMCOR
    this.w_PTFLSOSP = i_oFrom.w_PTFLSOSP
    this.w_PTNUMPRO = i_oFrom.w_PTNUMPRO
    this.w_PTCODAGE = i_oFrom.w_PTCODAGE
    this.w_PTSERRIF = i_oFrom.w_PTSERRIF
    this.w_PTORDRIF = i_oFrom.w_PTORDRIF
    this.w_PTNUMRIF = i_oFrom.w_PTNUMRIF
    this.w_PTFLVABD = i_oFrom.w_PTFLVABD
    this.w_PTFLRAGG = i_oFrom.w_PTFLRAGG
    this.w_PTDATREG = i_oFrom.w_PTDATREG
    this.w_DATSCA = i_oFrom.w_DATSCA
    this.w_SIMBVA = i_oFrom.w_SIMBVA
    this.w_NUMPAR = i_oFrom.w_NUMPAR
    this.w_TOTPAR = i_oFrom.w_TOTPAR
    this.w_IMPPAR = i_oFrom.w_IMPPAR
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_ORIGINE = this.w_ORIGINE
    i_oTo.w_PTSERIAL = this.w_PTSERIAL
    i_oTo.w_PTROWORD = this.w_PTROWORD
    i_oTo.w_CODVAL = this.w_CODVAL
    i_oTo.w_TIPCLF = this.w_TIPCLF
    i_oTo.w_CODCON = this.w_CODCON
    i_oTo.w_IMPSCA = this.w_IMPSCA
    i_oTo.w_DATVAL = this.w_DATVAL
    i_oTo.w_CODPAG = this.w_CODPAG
    i_oTo.w_TIPSCA = this.w_TIPSCA
    i_oTo.w_FLIMPG = this.w_FLIMPG
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_ALFDOC = this.w_ALFDOC
    i_oTo.w_NUMDOC = this.w_NUMDOC
    i_oTo.w_PTIMPDOC = this.w_PTIMPDOC
    i_oTo.w_TIPOPE = this.w_TIPOPE
    i_oTo.w_PTCAOAPE = this.w_PTCAOAPE
    i_oTo.w_PTDATAPE = this.w_PTDATAPE
    i_oTo.w_PTDATSCA = this.w_PTDATSCA
    i_oTo.w_PTCODVAL = this.w_PTCODVAL
    i_oTo.w_PTCAOVAL = this.w_PTCAOVAL
    i_oTo.w_PTNUMPAR = this.w_PTNUMPAR
    i_oTo.w_PT_SEGNO = this.w_PT_SEGNO
    i_oTo.w_PTTOTIMP = this.w_PTTOTIMP
    i_oTo.w_PTNUMDOC = this.w_PTNUMDOC
    i_oTo.w_PTALFDOC = this.w_PTALFDOC
    i_oTo.w_PTDATDOC = this.w_PTDATDOC
    i_oTo.w_DESVAL = this.w_DESVAL
    i_oTo.w_PTTIPCON = this.w_PTTIPCON
    i_oTo.w_PTCODCON = this.w_PTCODCON
    i_oTo.w_PTFLIMPE = this.w_PTFLIMPE
    i_oTo.w_PTNUMDIS = this.w_PTNUMDIS
    i_oTo.w_PTMODPAG = this.w_PTMODPAG
    i_oTo.w_PTBANAPP = this.w_PTBANAPP
    i_oTo.w_TIPPAG = this.w_TIPPAG
    i_oTo.w_PTBANNOS = this.w_PTBANNOS
    i_oTo.w_PTFLINDI = this.w_PTFLINDI
    i_oTo.w_PTFLCRSA = this.w_PTFLCRSA
    i_oTo.w_FLCRSA = this.w_FLCRSA
    i_oTo.w_DESBA = this.w_DESBA
    i_oTo.w_PTDESRIG = this.w_PTDESRIG
    i_oTo.w_PTNUMCOR = this.w_PTNUMCOR
    i_oTo.w_PTFLSOSP = this.w_PTFLSOSP
    i_oTo.w_PTNUMPRO = this.w_PTNUMPRO
    i_oTo.w_PTCODAGE = this.w_PTCODAGE
    i_oTo.w_PTSERRIF = this.w_PTSERRIF
    i_oTo.w_PTORDRIF = this.w_PTORDRIF
    i_oTo.w_PTNUMRIF = this.w_PTNUMRIF
    i_oTo.w_PTFLVABD = this.w_PTFLVABD
    i_oTo.w_PTFLRAGG = this.w_PTFLRAGG
    i_oTo.w_PTDATREG = this.w_PTDATREG
    i_oTo.w_DATSCA = this.w_DATSCA
    i_oTo.w_SIMBVA = this.w_SIMBVA
    i_oTo.w_NUMPAR = this.w_NUMPAR
    i_oTo.w_TOTPAR = this.w_TOTPAR
    i_oTo.w_IMPPAR = this.w_IMPPAR
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgste_mpa as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 797
  Height = 194
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-03-17"
  HelpContextID=147469417
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=56

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PAR_TITE_IDX = 0
  VALUTE_IDX = 0
  BAN_CHE_IDX = 0
  MOD_PAGA_IDX = 0
  CONTI_IDX = 0
  COC_MAST_IDX = 0
  BAN_CONTI_IDX = 0
  cFile = "PAR_TITE"
  cKeySelect = "PTSERIAL,PTROWORD"
  cKeyWhere  = "PTSERIAL=this.w_PTSERIAL and PTROWORD=this.w_PTROWORD"
  cKeyDetail  = "PTSERIAL=this.w_PTSERIAL and PTROWORD=this.w_PTROWORD"
  cKeyWhereODBC = '"PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL)';
      +'+" and PTROWORD="+cp_ToStrODBC(this.w_PTROWORD)';

  cKeyDetailWhereODBC = '"PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL)';
      +'+" and PTROWORD="+cp_ToStrODBC(this.w_PTROWORD)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PAR_TITE.PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL)';
      +'+" and PAR_TITE.PTROWORD="+cp_ToStrODBC(this.w_PTROWORD)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PAR_TITE.CPROWNUM '
  cPrg = "gste_mpa"
  cComment = "Situazione scadenze varie"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ORIGINE = space(1)
  w_PTSERIAL = space(10)
  o_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CODVAL = space(3)
  w_TIPCLF = space(1)
  w_CODCON = space(15)
  w_IMPSCA = 0
  w_DATVAL = ctod('  /  /  ')
  w_CODPAG = space(5)
  w_TIPSCA = space(1)
  o_TIPSCA = space(1)
  w_FLIMPG = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_ALFDOC = space(10)
  w_NUMDOC = 0
  w_PTIMPDOC = 0
  w_TIPOPE = space(10)
  w_PTCAOAPE = 0
  w_PTDATAPE = ctod('  /  /  ')
  w_PTDATSCA = ctod('  /  /  ')
  o_PTDATSCA = ctod('  /  /  ')
  w_PTCODVAL = space(3)
  w_PTCAOVAL = 0
  w_PTNUMPAR = space(31)
  o_PTNUMPAR = space(31)
  w_PT_SEGNO = space(1)
  o_PT_SEGNO = space(1)
  w_PTTOTIMP = 0
  o_PTTOTIMP = 0
  w_PTNUMDOC = 0
  w_PTALFDOC = space(10)
  w_PTDATDOC = ctod('  /  /  ')
  w_DESVAL = space(35)
  w_PTTIPCON = space(1)
  o_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTFLIMPE = space(2)
  w_PTNUMDIS = space(10)
  w_PTMODPAG = space(10)
  w_PTBANAPP = space(10)
  o_PTBANAPP = space(10)
  w_TIPPAG = space(2)
  w_PTBANNOS = space(15)
  w_PTFLINDI = space(1)
  w_PTFLCRSA = space(2)
  w_FLCRSA = space(1)
  w_DESBA = space(35)
  w_PTDESRIG = space(50)
  w_PTNUMCOR = space(25)
  w_PTFLSOSP = space(1)
  w_PTNUMPRO = 0
  w_PTCODAGE = space(5)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_PTFLVABD = space(1)
  w_PTFLRAGG = space(1)
  w_PTDATREG = ctod('  /  /  ')
  w_DATSCA = ctod('  /  /  ')
  w_SIMBVA = space(5)
  w_NUMPAR = space(10)
  w_TOTPAR = 0
  w_IMPPAR = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_mpaPag1","gste_mpa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='BAN_CHE'
    this.cWorkTables[3]='MOD_PAGA'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='COC_MAST'
    this.cWorkTables[6]='BAN_CONTI'
    this.cWorkTables[7]='PAR_TITE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_TITE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_TITE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgste_mpa'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_18_joined
    link_2_18_joined=.f.
    local link_2_21_joined
    link_2_21_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PAR_TITE where PTSERIAL=KeySet.PTSERIAL
    *                            and PTROWORD=KeySet.PTROWORD
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gste_mpa
      * --- Setta Ordine per Data Scadenza
      i_cOrder = 'order by PTDATSCA '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2],this.bLoadRecFilter,this.PAR_TITE_IDX,"gste_mpa")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_TITE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_TITE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_TITE '
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_18_joined=this.AddJoinedLink_2_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_21_joined=this.AddJoinedLink_2_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PTSERIAL',this.w_PTSERIAL  ,'PTROWORD',this.w_PTROWORD  )
      select * from (i_cTable) PAR_TITE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTPAR = 0
        .w_ORIGINE = this.oParentObject .w_ORIGINE
        .w_PTSERIAL = NVL(PTSERIAL,space(10))
        .w_PTROWORD = NVL(PTROWORD,0)
        .w_CODVAL = THIS.oParentObject .w_SCCODVAL
        .w_TIPCLF = THIS.oParentObject .w_SCTIPCLF
        .w_CODCON = THIS.oParentObject .w_SCCODCLF
        .w_IMPSCA = THIS.oParentObject .w_SCIMPSCA
        .w_DATVAL = THIS.oParentObject .w_SCDATVAL
        .w_CODPAG = THIS.oParentObject .w_SCCODPAG
        .w_TIPSCA = THIS.oParentObject .w_SCTIPSCA
        .w_FLIMPG = THIS.oParentObject .w_SCFLIMPG
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .w_OBTEST = this.oParentobject .w_SCDATDOC
        .w_ALFDOC = this.oParentObject .w_SCALFDOC
        .w_NUMDOC = this.oParentObject .w_SCNUMDOC
        .w_TIPOPE = THIS.oParentObject .w_TIPOPE
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .w_NUMPAR = 'variabile usata per forzare la creazione di o_PTNUMPAR'
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PAR_TITE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTPAR = 0
      scan
        with this
          .w_DESVAL = space(35)
          .w_TIPPAG = space(2)
          .w_DESBA = space(35)
          .w_SIMBVA = space(5)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_PTIMPDOC = NVL(PTIMPDOC,0)
          .w_PTCAOAPE = NVL(PTCAOAPE,0)
          .w_PTDATAPE = NVL(cp_ToDate(PTDATAPE),ctod("  /  /  "))
          .w_PTDATSCA = NVL(cp_ToDate(PTDATSCA),ctod("  /  /  "))
          .w_PTCODVAL = NVL(PTCODVAL,space(3))
          if link_2_4_joined
            this.w_PTCODVAL = NVL(VACODVAL204,NVL(this.w_PTCODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL204,space(35))
            this.w_SIMBVA = NVL(VASIMVAL204,space(5))
          else
          .link_2_4('Load')
          endif
          .w_PTCAOVAL = NVL(PTCAOVAL,0)
          .w_PTNUMPAR = NVL(PTNUMPAR,space(31))
          .w_PT_SEGNO = NVL(PT_SEGNO,space(1))
          .w_PTTOTIMP = NVL(PTTOTIMP,0)
          .w_PTNUMDOC = NVL(PTNUMDOC,0)
          .w_PTALFDOC = NVL(PTALFDOC,space(10))
          .w_PTDATDOC = NVL(cp_ToDate(PTDATDOC),ctod("  /  /  "))
          .w_PTTIPCON = NVL(PTTIPCON,space(1))
          .w_PTCODCON = NVL(PTCODCON,space(15))
          .w_PTFLIMPE = NVL(PTFLIMPE,space(2))
          .w_PTNUMDIS = NVL(PTNUMDIS,space(10))
          .w_PTMODPAG = NVL(PTMODPAG,space(10))
          if link_2_18_joined
            this.w_PTMODPAG = NVL(MPCODICE218,NVL(this.w_PTMODPAG,space(10)))
            this.w_TIPPAG = NVL(MPTIPPAG218,space(2))
          else
          .link_2_18('Load')
          endif
          .w_PTBANAPP = NVL(PTBANAPP,space(10))
          * evitabile
          *.link_2_19('Load')
          .w_PTBANNOS = NVL(PTBANNOS,space(15))
          if link_2_21_joined
            this.w_PTBANNOS = NVL(BACODBAN221,NVL(this.w_PTBANNOS,space(15)))
            this.w_DESBA = NVL(BADESCRI221,space(35))
          else
          .link_2_21('Load')
          endif
          .w_PTFLINDI = NVL(PTFLINDI,space(1))
          .w_PTFLCRSA = NVL(PTFLCRSA,space(2))
        .w_FLCRSA = .w_PTFLCRSA
          .w_PTDESRIG = NVL(PTDESRIG,space(50))
          .w_PTNUMCOR = NVL(PTNUMCOR,space(25))
          * evitabile
          *.link_2_27('Load')
          .w_PTFLSOSP = NVL(PTFLSOSP,space(1))
          .w_PTNUMPRO = NVL(PTNUMPRO,0)
          .w_PTCODAGE = NVL(PTCODAGE,space(5))
          .w_PTSERRIF = NVL(PTSERRIF,space(10))
          .w_PTORDRIF = NVL(PTORDRIF,0)
          .w_PTNUMRIF = NVL(PTNUMRIF,0)
          .w_PTFLVABD = NVL(PTFLVABD,space(1))
          .w_PTFLRAGG = NVL(PTFLRAGG,space(1))
          .w_PTDATREG = NVL(cp_ToDate(PTDATREG),ctod("  /  /  "))
        .w_DATSCA = .w_PTDATSCA
        .w_IMPPAR = .w_PTTOTIMP*IIF(.w_PT_SEGNO='D', 1,-1)*IIF(.w_PTTIPCON='C', 1,-1)*IIF(This.oParentObject .w_PFLCRSA='S',-1,1)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTPAR = .w_TOTPAR+.w_IMPPAR
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_ORIGINE = this.oParentObject .w_ORIGINE
        .w_CODVAL = THIS.oParentObject .w_SCCODVAL
        .w_TIPCLF = THIS.oParentObject .w_SCTIPCLF
        .w_CODCON = THIS.oParentObject .w_SCCODCLF
        .w_IMPSCA = THIS.oParentObject .w_SCIMPSCA
        .w_DATVAL = THIS.oParentObject .w_SCDATVAL
        .w_CODPAG = THIS.oParentObject .w_SCCODPAG
        .w_TIPSCA = THIS.oParentObject .w_SCTIPSCA
        .w_FLIMPG = THIS.oParentObject .w_SCFLIMPG
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .w_OBTEST = this.oParentobject .w_SCDATDOC
        .w_ALFDOC = this.oParentObject .w_SCALFDOC
        .w_NUMDOC = this.oParentObject .w_SCNUMDOC
        .w_TIPOPE = THIS.oParentObject .w_TIPOPE
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .w_NUMPAR = 'variabile usata per forzare la creazione di o_PTNUMPAR'
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_15.enabled = .oPgFrm.Page1.oPag.oBtn_2_15.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_35.enabled = .oPgFrm.Page1.oPag.oBtn_2_35.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_ORIGINE=space(1)
      .w_PTSERIAL=space(10)
      .w_PTROWORD=0
      .w_CODVAL=space(3)
      .w_TIPCLF=space(1)
      .w_CODCON=space(15)
      .w_IMPSCA=0
      .w_DATVAL=ctod("  /  /  ")
      .w_CODPAG=space(5)
      .w_TIPSCA=space(1)
      .w_FLIMPG=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_ALFDOC=space(10)
      .w_NUMDOC=0
      .w_PTIMPDOC=0
      .w_TIPOPE=space(10)
      .w_PTCAOAPE=0
      .w_PTDATAPE=ctod("  /  /  ")
      .w_PTDATSCA=ctod("  /  /  ")
      .w_PTCODVAL=space(3)
      .w_PTCAOVAL=0
      .w_PTNUMPAR=space(31)
      .w_PT_SEGNO=space(1)
      .w_PTTOTIMP=0
      .w_PTNUMDOC=0
      .w_PTALFDOC=space(10)
      .w_PTDATDOC=ctod("  /  /  ")
      .w_DESVAL=space(35)
      .w_PTTIPCON=space(1)
      .w_PTCODCON=space(15)
      .w_PTFLIMPE=space(2)
      .w_PTNUMDIS=space(10)
      .w_PTMODPAG=space(10)
      .w_PTBANAPP=space(10)
      .w_TIPPAG=space(2)
      .w_PTBANNOS=space(15)
      .w_PTFLINDI=space(1)
      .w_PTFLCRSA=space(2)
      .w_FLCRSA=space(1)
      .w_DESBA=space(35)
      .w_PTDESRIG=space(50)
      .w_PTNUMCOR=space(25)
      .w_PTFLSOSP=space(1)
      .w_PTNUMPRO=0
      .w_PTCODAGE=space(5)
      .w_PTSERRIF=space(10)
      .w_PTORDRIF=0
      .w_PTNUMRIF=0
      .w_PTFLVABD=space(1)
      .w_PTFLRAGG=space(1)
      .w_PTDATREG=ctod("  /  /  ")
      .w_DATSCA=ctod("  /  /  ")
      .w_SIMBVA=space(5)
      .w_NUMPAR=space(10)
      .w_TOTPAR=0
      .w_IMPPAR=0
      if .cFunction<>"Filter"
        .w_ORIGINE = this.oParentObject .w_ORIGINE
        .DoRTCalc(2,3,.f.)
        .w_CODVAL = THIS.oParentObject .w_SCCODVAL
        .w_TIPCLF = THIS.oParentObject .w_SCTIPCLF
        .w_CODCON = THIS.oParentObject .w_SCCODCLF
        .w_IMPSCA = THIS.oParentObject .w_SCIMPSCA
        .w_DATVAL = THIS.oParentObject .w_SCDATVAL
        .w_CODPAG = THIS.oParentObject .w_SCCODPAG
        .w_TIPSCA = THIS.oParentObject .w_SCTIPSCA
        .w_FLIMPG = THIS.oParentObject .w_SCFLIMPG
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .w_OBTEST = this.oParentobject .w_SCDATDOC
        .w_ALFDOC = this.oParentObject .w_SCALFDOC
        .w_NUMDOC = this.oParentObject .w_SCNUMDOC
        .w_PTIMPDOC = THIS.oParentObject .w_SCIMPSCA
        .w_TIPOPE = THIS.oParentObject .w_TIPOPE
        .w_PTCAOAPE = THIS.oParentObject .w_SCCAOVAL
        .w_PTDATAPE = IIF(EMPTY(This.oParentObject .w_SCDATDOC), This.oParentObject .w_SCDATREG, This.oParentObject .w_SCDATDOC)
        .DoRTCalc(19,19,.f.)
        .w_PTCODVAL = .w_CODVAL
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_PTCODVAL))
         .link_2_4('Full')
        endif
        .w_PTCAOVAL = THIS.oParentObject .w_SCCAOVAL
        .DoRTCalc(22,24,.f.)
        .w_PTNUMDOC = .w_NUMDOC
        .w_PTALFDOC = .w_ALFDOC
        .w_PTDATDOC = this.oParentObject .w_SCDATDOC
        .DoRTCalc(28,28,.f.)
        .w_PTTIPCON = .w_TIPCLF
        .w_PTCODCON = .w_CODCON
        .DoRTCalc(31,33,.f.)
        if not(empty(.w_PTMODPAG))
         .link_2_18('Full')
        endif
        .w_PTBANAPP = this.oParentObject .w_SCCODBAN
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_PTBANAPP))
         .link_2_19('Full')
        endif
        .DoRTCalc(35,35,.f.)
        .w_PTBANNOS = THIS.oParentObject .w_SCBANNOS
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_PTBANNOS))
         .link_2_21('Full')
        endif
        .w_PTFLINDI = ' '
        .w_PTFLCRSA = IIF(((.w_TIPCLF='F' and .w_TIPSCA='C') or (.w_TIPCLF='C' and .w_TIPSCA='F')) or .w_ORIGINE='A', 'S', 'C')
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .w_FLCRSA = .w_PTFLCRSA
        .DoRTCalc(40,40,.f.)
        .w_PTDESRIG = this.oParentObject .w_SCDESCRI
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_PTNUMCOR))
         .link_2_27('Full')
        endif
        .w_PTFLSOSP = ' '
        .DoRTCalc(44,44,.f.)
        .w_PTCODAGE = THIS.oParentObject .w_SCCODAGE
        .DoRTCalc(46,48,.f.)
        .w_PTFLVABD = this.oParentObject .w_SCFLVABD
        .DoRTCalc(50,51,.f.)
        .w_DATSCA = .w_PTDATSCA
        .DoRTCalc(53,53,.f.)
        .w_NUMPAR = 'variabile usata per forzare la creazione di o_PTNUMPAR'
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(55,55,.f.)
        .w_IMPPAR = .w_PTTOTIMP*IIF(.w_PT_SEGNO='D', 1,-1)*IIF(.w_PTTIPCON='C', 1,-1)*IIF(This.oParentObject .w_PFLCRSA='S',-1,1)
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_TITE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_15.enabled = this.oPgFrm.Page1.oPag.oBtn_2_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_35.enabled = this.oPgFrm.Page1.oPag.oBtn_2_35.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPTDESRIG_2_26.enabled = i_bVal
      .Page1.oPag.oPTNUMCOR_2_27.enabled = i_bVal
      .Page1.oPag.oPTFLSOSP_2_28.enabled = i_bVal
      .Page1.oPag.oBtn_2_15.enabled = .Page1.oPag.oBtn_2_15.mCond()
      .Page1.oPag.oBtn_2_35.enabled = .Page1.oPag.oBtn_2_35.mCond()
      .Page1.oPag.oObj_1_12.enabled = i_bVal
      .Page1.oPag.oObj_1_18.enabled = i_bVal
      .Page1.oPag.oObj_1_19.enabled = i_bVal
      .Page1.oPag.oObj_1_23.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PAR_TITE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PTSERIAL,"PTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PTROWORD,"PTROWORD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PTDATSCA D(8);
      ,t_PTNUMPAR C(31);
      ,t_PT_SEGNO C(1);
      ,t_PTTOTIMP N(18,4);
      ,t_PTMODPAG C(10);
      ,t_PTBANAPP C(10);
      ,t_PTBANNOS C(15);
      ,t_PTFLCRSA N(3);
      ,t_PTDESRIG C(50);
      ,t_PTNUMCOR C(25);
      ,t_PTFLSOSP N(3);
      ,t_SIMBVA C(5);
      ,CPROWNUM N(10);
      ,t_PTIMPDOC N(18,4);
      ,t_PTCAOAPE N(12,7);
      ,t_PTDATAPE D(8);
      ,t_PTCODVAL C(3);
      ,t_PTCAOVAL N(12,7);
      ,t_PTNUMDOC N(15);
      ,t_PTALFDOC C(10);
      ,t_PTDATDOC D(8);
      ,t_DESVAL C(35);
      ,t_PTTIPCON C(1);
      ,t_PTCODCON C(15);
      ,t_PTFLIMPE C(2);
      ,t_PTNUMDIS C(10);
      ,t_TIPPAG C(2);
      ,t_PTFLINDI C(1);
      ,t_FLCRSA C(1);
      ,t_DESBA C(35);
      ,t_PTNUMPRO N(4);
      ,t_PTCODAGE C(5);
      ,t_PTSERRIF C(10);
      ,t_PTORDRIF N(4);
      ,t_PTNUMRIF N(3);
      ,t_PTFLVABD C(1);
      ,t_PTFLRAGG C(1);
      ,t_PTDATREG D(8);
      ,t_DATSCA D(8);
      ,t_IMPPAR N(18,4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgste_mpabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_3.controlsource=this.cTrsName+'.t_PTDATSCA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_6.controlsource=this.cTrsName+'.t_PTNUMPAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_7.controlsource=this.cTrsName+'.t_PT_SEGNO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_8.controlsource=this.cTrsName+'.t_PTTOTIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_18.controlsource=this.cTrsName+'.t_PTMODPAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANAPP_2_19.controlsource=this.cTrsName+'.t_PTBANAPP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANNOS_2_21.controlsource=this.cTrsName+'.t_PTBANNOS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLCRSA_2_23.controlsource=this.cTrsName+'.t_PTFLCRSA'
    this.oPgFRm.Page1.oPag.oPTDESRIG_2_26.controlsource=this.cTrsName+'.t_PTDESRIG'
    this.oPgFRm.Page1.oPag.oPTNUMCOR_2_27.controlsource=this.cTrsName+'.t_PTNUMCOR'
    this.oPgFRm.Page1.oPag.oPTFLSOSP_2_28.controlsource=this.cTrsName+'.t_PTFLSOSP'
    this.oPgFRm.Page1.oPag.oSIMBVA_2_41.controlsource=this.cTrsName+'.t_SIMBVA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(81)
    this.AddVLine(248)
    this.AddVLine(275)
    this.AddVLine(407)
    this.AddVLine(496)
    this.AddVLine(601)
    this.AddVLine(688)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_3
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      *
      * insert into PAR_TITE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_TITE')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_TITE')
        i_cFldBody=" "+;
                  "(PTSERIAL,PTROWORD,PTIMPDOC,PTCAOAPE,PTDATAPE"+;
                  ",PTDATSCA,PTCODVAL,PTCAOVAL,PTNUMPAR,PT_SEGNO"+;
                  ",PTTOTIMP,PTNUMDOC,PTALFDOC,PTDATDOC,PTTIPCON"+;
                  ",PTCODCON,PTFLIMPE,PTNUMDIS,PTMODPAG,PTBANAPP"+;
                  ",PTBANNOS,PTFLINDI,PTFLCRSA,PTDESRIG,PTNUMCOR"+;
                  ",PTFLSOSP,PTNUMPRO,PTCODAGE,PTSERRIF,PTORDRIF"+;
                  ",PTNUMRIF,PTFLVABD,PTFLRAGG,PTDATREG,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PTSERIAL)+","+cp_ToStrODBC(this.w_PTROWORD)+","+cp_ToStrODBC(this.w_PTIMPDOC)+","+cp_ToStrODBC(this.w_PTCAOAPE)+","+cp_ToStrODBC(this.w_PTDATAPE)+;
             ","+cp_ToStrODBC(this.w_PTDATSCA)+","+cp_ToStrODBCNull(this.w_PTCODVAL)+","+cp_ToStrODBC(this.w_PTCAOVAL)+","+cp_ToStrODBC(this.w_PTNUMPAR)+","+cp_ToStrODBC(this.w_PT_SEGNO)+;
             ","+cp_ToStrODBC(this.w_PTTOTIMP)+","+cp_ToStrODBC(this.w_PTNUMDOC)+","+cp_ToStrODBC(this.w_PTALFDOC)+","+cp_ToStrODBC(this.w_PTDATDOC)+","+cp_ToStrODBC(this.w_PTTIPCON)+;
             ","+cp_ToStrODBC(this.w_PTCODCON)+","+cp_ToStrODBC(this.w_PTFLIMPE)+","+cp_ToStrODBC(this.w_PTNUMDIS)+","+cp_ToStrODBCNull(this.w_PTMODPAG)+","+cp_ToStrODBCNull(this.w_PTBANAPP)+;
             ","+cp_ToStrODBCNull(this.w_PTBANNOS)+","+cp_ToStrODBC(this.w_PTFLINDI)+","+cp_ToStrODBC(this.w_PTFLCRSA)+","+cp_ToStrODBC(this.w_PTDESRIG)+","+cp_ToStrODBCNull(this.w_PTNUMCOR)+;
             ","+cp_ToStrODBC(this.w_PTFLSOSP)+","+cp_ToStrODBC(this.w_PTNUMPRO)+","+cp_ToStrODBC(this.w_PTCODAGE)+","+cp_ToStrODBC(this.w_PTSERRIF)+","+cp_ToStrODBC(this.w_PTORDRIF)+;
             ","+cp_ToStrODBC(this.w_PTNUMRIF)+","+cp_ToStrODBC(this.w_PTFLVABD)+","+cp_ToStrODBC(this.w_PTFLRAGG)+","+cp_ToStrODBC(this.w_PTDATREG)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_TITE')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_TITE')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PTSERIAL',this.w_PTSERIAL,'PTROWORD',this.w_PTROWORD)
        INSERT INTO (i_cTable) (;
                   PTSERIAL;
                  ,PTROWORD;
                  ,PTIMPDOC;
                  ,PTCAOAPE;
                  ,PTDATAPE;
                  ,PTDATSCA;
                  ,PTCODVAL;
                  ,PTCAOVAL;
                  ,PTNUMPAR;
                  ,PT_SEGNO;
                  ,PTTOTIMP;
                  ,PTNUMDOC;
                  ,PTALFDOC;
                  ,PTDATDOC;
                  ,PTTIPCON;
                  ,PTCODCON;
                  ,PTFLIMPE;
                  ,PTNUMDIS;
                  ,PTMODPAG;
                  ,PTBANAPP;
                  ,PTBANNOS;
                  ,PTFLINDI;
                  ,PTFLCRSA;
                  ,PTDESRIG;
                  ,PTNUMCOR;
                  ,PTFLSOSP;
                  ,PTNUMPRO;
                  ,PTCODAGE;
                  ,PTSERRIF;
                  ,PTORDRIF;
                  ,PTNUMRIF;
                  ,PTFLVABD;
                  ,PTFLRAGG;
                  ,PTDATREG;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PTSERIAL;
                  ,this.w_PTROWORD;
                  ,this.w_PTIMPDOC;
                  ,this.w_PTCAOAPE;
                  ,this.w_PTDATAPE;
                  ,this.w_PTDATSCA;
                  ,this.w_PTCODVAL;
                  ,this.w_PTCAOVAL;
                  ,this.w_PTNUMPAR;
                  ,this.w_PT_SEGNO;
                  ,this.w_PTTOTIMP;
                  ,this.w_PTNUMDOC;
                  ,this.w_PTALFDOC;
                  ,this.w_PTDATDOC;
                  ,this.w_PTTIPCON;
                  ,this.w_PTCODCON;
                  ,this.w_PTFLIMPE;
                  ,this.w_PTNUMDIS;
                  ,this.w_PTMODPAG;
                  ,this.w_PTBANAPP;
                  ,this.w_PTBANNOS;
                  ,this.w_PTFLINDI;
                  ,this.w_PTFLCRSA;
                  ,this.w_PTDESRIG;
                  ,this.w_PTNUMCOR;
                  ,this.w_PTFLSOSP;
                  ,this.w_PTNUMPRO;
                  ,this.w_PTCODAGE;
                  ,this.w_PTSERRIF;
                  ,this.w_PTORDRIF;
                  ,this.w_PTNUMRIF;
                  ,this.w_PTFLVABD;
                  ,this.w_PTFLRAGG;
                  ,this.w_PTDATREG;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_PTIMPDOC<>t_PTIMPDOC
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_PTDATSCA) AND t_PTTOTIMP<>0 AND NOT EMPTY(t_PTNUMPAR)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_TITE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_TITE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_PTIMPDOC<>t_PTIMPDOC
            i_bUpdAll = .t.
          endif
        endif
        scan for (NOT EMPTY(t_PTDATSCA) AND t_PTTOTIMP<>0 AND NOT EMPTY(t_PTNUMPAR)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PAR_TITE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_TITE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PTIMPDOC="+cp_ToStrODBC(this.w_PTIMPDOC)+;
                     ",PTCAOAPE="+cp_ToStrODBC(this.w_PTCAOAPE)+;
                     ",PTDATAPE="+cp_ToStrODBC(this.w_PTDATAPE)+;
                     ",PTDATSCA="+cp_ToStrODBC(this.w_PTDATSCA)+;
                     ",PTCODVAL="+cp_ToStrODBCNull(this.w_PTCODVAL)+;
                     ",PTCAOVAL="+cp_ToStrODBC(this.w_PTCAOVAL)+;
                     ",PTNUMPAR="+cp_ToStrODBC(this.w_PTNUMPAR)+;
                     ",PT_SEGNO="+cp_ToStrODBC(this.w_PT_SEGNO)+;
                     ",PTTOTIMP="+cp_ToStrODBC(this.w_PTTOTIMP)+;
                     ",PTNUMDOC="+cp_ToStrODBC(this.w_PTNUMDOC)+;
                     ",PTALFDOC="+cp_ToStrODBC(this.w_PTALFDOC)+;
                     ",PTDATDOC="+cp_ToStrODBC(this.w_PTDATDOC)+;
                     ",PTTIPCON="+cp_ToStrODBC(this.w_PTTIPCON)+;
                     ",PTCODCON="+cp_ToStrODBC(this.w_PTCODCON)+;
                     ",PTFLIMPE="+cp_ToStrODBC(this.w_PTFLIMPE)+;
                     ",PTNUMDIS="+cp_ToStrODBC(this.w_PTNUMDIS)+;
                     ",PTMODPAG="+cp_ToStrODBCNull(this.w_PTMODPAG)+;
                     ",PTBANAPP="+cp_ToStrODBCNull(this.w_PTBANAPP)+;
                     ",PTBANNOS="+cp_ToStrODBCNull(this.w_PTBANNOS)+;
                     ",PTFLINDI="+cp_ToStrODBC(this.w_PTFLINDI)+;
                     ",PTFLCRSA="+cp_ToStrODBC(this.w_PTFLCRSA)+;
                     ",PTDESRIG="+cp_ToStrODBC(this.w_PTDESRIG)+;
                     ",PTNUMCOR="+cp_ToStrODBCNull(this.w_PTNUMCOR)+;
                     ",PTFLSOSP="+cp_ToStrODBC(this.w_PTFLSOSP)+;
                     ",PTNUMPRO="+cp_ToStrODBC(this.w_PTNUMPRO)+;
                     ",PTCODAGE="+cp_ToStrODBC(this.w_PTCODAGE)+;
                     ",PTSERRIF="+cp_ToStrODBC(this.w_PTSERRIF)+;
                     ",PTORDRIF="+cp_ToStrODBC(this.w_PTORDRIF)+;
                     ",PTNUMRIF="+cp_ToStrODBC(this.w_PTNUMRIF)+;
                     ",PTFLVABD="+cp_ToStrODBC(this.w_PTFLVABD)+;
                     ",PTFLRAGG="+cp_ToStrODBC(this.w_PTFLRAGG)+;
                     ",PTDATREG="+cp_ToStrODBC(this.w_PTDATREG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_TITE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PTIMPDOC=this.w_PTIMPDOC;
                     ,PTCAOAPE=this.w_PTCAOAPE;
                     ,PTDATAPE=this.w_PTDATAPE;
                     ,PTDATSCA=this.w_PTDATSCA;
                     ,PTCODVAL=this.w_PTCODVAL;
                     ,PTCAOVAL=this.w_PTCAOVAL;
                     ,PTNUMPAR=this.w_PTNUMPAR;
                     ,PT_SEGNO=this.w_PT_SEGNO;
                     ,PTTOTIMP=this.w_PTTOTIMP;
                     ,PTNUMDOC=this.w_PTNUMDOC;
                     ,PTALFDOC=this.w_PTALFDOC;
                     ,PTDATDOC=this.w_PTDATDOC;
                     ,PTTIPCON=this.w_PTTIPCON;
                     ,PTCODCON=this.w_PTCODCON;
                     ,PTFLIMPE=this.w_PTFLIMPE;
                     ,PTNUMDIS=this.w_PTNUMDIS;
                     ,PTMODPAG=this.w_PTMODPAG;
                     ,PTBANAPP=this.w_PTBANAPP;
                     ,PTBANNOS=this.w_PTBANNOS;
                     ,PTFLINDI=this.w_PTFLINDI;
                     ,PTFLCRSA=this.w_PTFLCRSA;
                     ,PTDESRIG=this.w_PTDESRIG;
                     ,PTNUMCOR=this.w_PTNUMCOR;
                     ,PTFLSOSP=this.w_PTFLSOSP;
                     ,PTNUMPRO=this.w_PTNUMPRO;
                     ,PTCODAGE=this.w_PTCODAGE;
                     ,PTSERRIF=this.w_PTSERRIF;
                     ,PTORDRIF=this.w_PTORDRIF;
                     ,PTNUMRIF=this.w_PTNUMRIF;
                     ,PTFLVABD=this.w_PTFLVABD;
                     ,PTFLRAGG=this.w_PTFLRAGG;
                     ,PTDATREG=this.w_PTDATREG;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_PTDATSCA) AND t_PTTOTIMP<>0 AND NOT EMPTY(t_PTNUMPAR)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PAR_TITE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_PTDATSCA) AND t_PTTOTIMP<>0 AND NOT EMPTY(t_PTNUMPAR)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    if i_bUpd
      with this
          .w_ORIGINE = this.oParentObject .w_ORIGINE
        .DoRTCalc(2,3,.t.)
          .w_CODVAL = THIS.oParentObject .w_SCCODVAL
          .w_TIPCLF = THIS.oParentObject .w_SCTIPCLF
          .w_CODCON = THIS.oParentObject .w_SCCODCLF
          .w_IMPSCA = THIS.oParentObject .w_SCIMPSCA
          .w_DATVAL = THIS.oParentObject .w_SCDATVAL
          .w_CODPAG = THIS.oParentObject .w_SCCODPAG
          .w_TIPSCA = THIS.oParentObject .w_SCTIPSCA
          .w_FLIMPG = THIS.oParentObject .w_SCFLIMPG
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
          .w_OBTEST = this.oParentobject .w_SCDATDOC
          .w_ALFDOC = this.oParentObject .w_SCALFDOC
          .w_NUMDOC = this.oParentObject .w_SCNUMDOC
          .w_PTIMPDOC = THIS.oParentObject .w_SCIMPSCA
          .w_TIPOPE = THIS.oParentObject .w_TIPOPE
        if .o_PTDATSCA<>.w_PTDATSCA
          .w_PTCAOAPE = THIS.oParentObject .w_SCCAOVAL
        endif
        if .o_PTDATSCA<>.w_PTDATSCA
          .w_PTDATAPE = IIF(EMPTY(This.oParentObject .w_SCDATDOC), This.oParentObject .w_SCDATREG, This.oParentObject .w_SCDATDOC)
        endif
        .DoRTCalc(19,19,.t.)
          .w_PTCODVAL = .w_CODVAL
          .link_2_4('Full')
          .w_PTCAOVAL = THIS.oParentObject .w_SCCAOVAL
        .DoRTCalc(22,24,.t.)
          .w_PTNUMDOC = .w_NUMDOC
          .w_PTALFDOC = .w_ALFDOC
          .w_PTDATDOC = this.oParentObject .w_SCDATDOC
        .DoRTCalc(28,28,.t.)
          .w_PTTIPCON = .w_TIPCLF
          .w_PTCODCON = .w_CODCON
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .DoRTCalc(31,38,.t.)
          .w_FLCRSA = .w_PTFLCRSA
        .DoRTCalc(40,40,.t.)
        if .o_PTDATSCA<>.w_PTDATSCA
          .w_PTDESRIG = this.oParentObject .w_SCDESCRI
        endif
        if .o_PTBANAPP<>.w_PTBANAPP
          .link_2_27('Full')
        endif
        .DoRTCalc(43,44,.t.)
          .w_PTCODAGE = THIS.oParentObject .w_SCCODAGE
        .DoRTCalc(46,51,.t.)
        if .o_PTSERIAL<>.w_PTSERIAL
          .w_DATSCA = .w_PTDATSCA
        endif
        .DoRTCalc(53,53,.t.)
        if .o_PTNUMPAR<>.w_PTNUMPAR
          .w_NUMPAR = 'variabile usata per forzare la creazione di o_PTNUMPAR'
        endif
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(55,55,.t.)
        if .o_PT_SEGNO<>.w_PT_SEGNO.or. .o_PTTOTIMP<>.w_PTTOTIMP
          .w_TOTPAR = .w_TOTPAR-.w_imppar
          .w_IMPPAR = .w_PTTOTIMP*IIF(.w_PT_SEGNO='D', 1,-1)*IIF(.w_PTTIPCON='C', 1,-1)*IIF(This.oParentObject .w_PFLCRSA='S',-1,1)
          .w_TOTPAR = .w_TOTPAR+.w_imppar
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PTIMPDOC with this.w_PTIMPDOC
      replace t_PTCAOAPE with this.w_PTCAOAPE
      replace t_PTDATAPE with this.w_PTDATAPE
      replace t_PTCODVAL with this.w_PTCODVAL
      replace t_PTCAOVAL with this.w_PTCAOVAL
      replace t_PTNUMDOC with this.w_PTNUMDOC
      replace t_PTALFDOC with this.w_PTALFDOC
      replace t_PTDATDOC with this.w_PTDATDOC
      replace t_DESVAL with this.w_DESVAL
      replace t_PTTIPCON with this.w_PTTIPCON
      replace t_PTCODCON with this.w_PTCODCON
      replace t_PTFLIMPE with this.w_PTFLIMPE
      replace t_PTNUMDIS with this.w_PTNUMDIS
      replace t_TIPPAG with this.w_TIPPAG
      replace t_PTFLINDI with this.w_PTFLINDI
      replace t_FLCRSA with this.w_FLCRSA
      replace t_DESBA with this.w_DESBA
      replace t_PTNUMPRO with this.w_PTNUMPRO
      replace t_PTCODAGE with this.w_PTCODAGE
      replace t_PTSERRIF with this.w_PTSERRIF
      replace t_PTORDRIF with this.w_PTORDRIF
      replace t_PTNUMRIF with this.w_PTNUMRIF
      replace t_PTFLVABD with this.w_PTFLVABD
      replace t_PTFLRAGG with this.w_PTFLRAGG
      replace t_PTDATREG with this.w_PTDATREG
      replace t_DATSCA with this.w_DATSCA
      replace t_IMPPAR with this.w_IMPPAR
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTBANAPP_2_19.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTBANAPP_2_19.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTFLCRSA_2_23.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTFLCRSA_2_23.mCond()
    this.oPgFrm.Page1.oPag.oPTDESRIG_2_26.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPTDESRIG_2_26.mCond()
    this.oPgFrm.Page1.oPag.oPTFLSOSP_2_28.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPTFLSOSP_2_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_15.enabled =this.oPgFrm.Page1.oPag.oBtn_2_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_35.enabled =this.oPgFrm.Page1.oPag.oBtn_2_35.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oTOTPAR_3_1.visible=!this.oPgFrm.Page1.oPag.oTOTPAR_3_1.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBtn_2_35.visible=!this.oPgFrm.Page1.oPag.oBtn_2_35.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gste_mpa
    
    **Per le partite di saldo non si pu� modificare la scadenza e il numero partita altrimenti la procedura non riesce piu a legare la partita di saldo alla partita di crea
    IF cevent ='w_PTDATSCA Changed' AND this.w_PTFLCRSA='S' 
         ah_errormsg ('Non � possibile modificare la data scadenza per le partite di saldo')
         this.w_PTDATSCA=this.o_PTDATSCA
    ENDIF
    
    IF cevent ='w_PTNUMPAR Changed' AND this.w_PTFLCRSA='S'
         ah_errormsg ('Non � possibile modificare il numero partita per le partite di saldo')
         this.w_PTNUMPAR=this.o_PTNUMPAR
    ENDIF
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PTCODVAL
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PTCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PTCODVAL)
            select VACODVAL,VADESVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_SIMBVA = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PTCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_SIMBVA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.VACODVAL as VACODVAL204"+ ",link_2_4.VADESVAL as VADESVAL204"+ ",link_2_4.VASIMVAL as VASIMVAL204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on PAR_TITE.PTCODVAL=link_2_4.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and PAR_TITE.PTCODVAL=link_2_4.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PTMODPAG
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_lTable = "MOD_PAGA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2], .t., this.MOD_PAGA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTMODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMP',True,'MOD_PAGA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MPCODICE like "+cp_ToStrODBC(trim(this.w_PTMODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MPCODICE,MPTIPPAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MPCODICE',trim(this.w_PTMODPAG))
          select MPCODICE,MPTIPPAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTMODPAG)==trim(_Link_.MPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTMODPAG) and !this.bDontReportError
            deferred_cp_zoom('MOD_PAGA','*','MPCODICE',cp_AbsName(oSource.parent,'oPTMODPAG_2_18'),i_cWhere,'GSAR_AMP',"Tipi di pagamento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPTIPPAG";
                     +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',oSource.xKey(1))
            select MPCODICE,MPTIPPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTMODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPTIPPAG";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_PTMODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_PTMODPAG)
            select MPCODICE,MPTIPPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTMODPAG = NVL(_Link_.MPCODICE,space(10))
      this.w_TIPPAG = NVL(_Link_.MPTIPPAG,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PTMODPAG = space(10)
      endif
      this.w_TIPPAG = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_PTMODPAG)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire codice tipo di pagamento")
        endif
        this.w_PTMODPAG = space(10)
        this.w_TIPPAG = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_PAGA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTMODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MOD_PAGA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_18.MPCODICE as MPCODICE218"+ ",link_2_18.MPTIPPAG as MPTIPPAG218"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_18 on PAR_TITE.PTMODPAG=link_2_18.MPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_18"
          i_cKey=i_cKey+'+" and PAR_TITE.PTMODPAG=link_2_18.MPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PTBANAPP
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTBANAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_PTBANAPP)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_PTBANAPP))
          select BACODBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTBANAPP)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTBANAPP) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oPTBANAPP_2_19'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTBANAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_PTBANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_PTBANAPP)
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTBANAPP = NVL(_Link_.BACODBAN,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PTBANAPP = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTBANAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PTBANNOS
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTBANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_PTBANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_PTBANNOS))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTBANNOS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_PTBANNOS)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_PTBANNOS)+"%");

            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PTBANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oPTBANNOS_2_21'),i_cWhere,'GSTE_ACB',"Elenco conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTBANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_PTBANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_PTBANNOS)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTBANNOS = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBA = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PTBANNOS = space(15)
      endif
      this.w_DESBA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTBANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_21.BACODBAN as BACODBAN221"+ ",link_2_21.BADESCRI as BADESCRI221"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_21 on PAR_TITE.PTBANNOS=link_2_21.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_21"
          i_cKey=i_cKey+'+" and PAR_TITE.PTBANNOS=link_2_21.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PTNUMCOR
  func Link_2_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
    i_lTable = "BAN_CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2], .t., this.BAN_CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTNUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BAN_CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCONCOR like "+cp_ToStrODBC(trim(this.w_PTNUMCOR)+"%");
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_PTTIPCON);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_PTCODCON);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_PTBANAPP);

          i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPCON',this.w_PTTIPCON;
                     ,'CCCODCON',this.w_PTCODCON;
                     ,'CCCODBAN',this.w_PTBANAPP;
                     ,'CCCONCOR',trim(this.w_PTNUMCOR))
          select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTNUMCOR)==trim(_Link_.CCCONCOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTNUMCOR) and !this.bDontReportError
            deferred_cp_zoom('BAN_CONTI','*','CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR',cp_AbsName(oSource.parent,'oPTNUMCOR_2_27'),i_cWhere,'',"Elenco conti correnti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PTTIPCON<>oSource.xKey(1);
           .or. this.w_PTCODCON<>oSource.xKey(2);
           .or. this.w_PTBANAPP<>oSource.xKey(3);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto corrente incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                     +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(oSource.xKey(4));
                     +" and CCTIPCON="+cp_ToStrODBC(this.w_PTTIPCON);
                     +" and CCCODCON="+cp_ToStrODBC(this.w_PTCODCON);
                     +" and CCCODBAN="+cp_ToStrODBC(this.w_PTBANAPP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',oSource.xKey(1);
                       ,'CCCODCON',oSource.xKey(2);
                       ,'CCCODBAN',oSource.xKey(3);
                       ,'CCCONCOR',oSource.xKey(4))
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTNUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                   +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(this.w_PTNUMCOR);
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_PTTIPCON);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_PTCODCON);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_PTBANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',this.w_PTTIPCON;
                       ,'CCCODCON',this.w_PTCODCON;
                       ,'CCCODBAN',this.w_PTBANAPP;
                       ,'CCCONCOR',this.w_PTNUMCOR)
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTNUMCOR = NVL(_Link_.CCCONCOR,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_PTNUMCOR = space(25)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPCON,1)+'\'+cp_ToStr(_Link_.CCCODCON,1)+'\'+cp_ToStr(_Link_.CCCODBAN,1)+'\'+cp_ToStr(_Link_.CCCONCOR,1)
      cp_ShowWarn(i_cKey,this.BAN_CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTNUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPTDESRIG_2_26.value==this.w_PTDESRIG)
      this.oPgFrm.Page1.oPag.oPTDESRIG_2_26.value=this.w_PTDESRIG
      replace t_PTDESRIG with this.oPgFrm.Page1.oPag.oPTDESRIG_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPTNUMCOR_2_27.value==this.w_PTNUMCOR)
      this.oPgFrm.Page1.oPag.oPTNUMCOR_2_27.value=this.w_PTNUMCOR
      replace t_PTNUMCOR with this.oPgFrm.Page1.oPag.oPTNUMCOR_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPTFLSOSP_2_28.RadioValue()==this.w_PTFLSOSP)
      this.oPgFrm.Page1.oPag.oPTFLSOSP_2_28.SetRadio()
      replace t_PTFLSOSP with this.oPgFrm.Page1.oPag.oPTFLSOSP_2_28.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMBVA_2_41.value==this.w_SIMBVA)
      this.oPgFrm.Page1.oPag.oSIMBVA_2_41.value=this.w_SIMBVA
      replace t_SIMBVA with this.oPgFrm.Page1.oPag.oSIMBVA_2_41.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTPAR_3_1.value==this.w_TOTPAR)
      this.oPgFrm.Page1.oPag.oTOTPAR_3_1.value=this.w_TOTPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_3.value==this.w_PTDATSCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_3.value=this.w_PTDATSCA
      replace t_PTDATSCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_6.value==this.w_PTNUMPAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_6.value=this.w_PTNUMPAR
      replace t_PTNUMPAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_7.value==this.w_PT_SEGNO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_7.value=this.w_PT_SEGNO
      replace t_PT_SEGNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_8.value==this.w_PTTOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_8.value=this.w_PTTOTIMP
      replace t_PTTOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_18.value==this.w_PTMODPAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_18.value=this.w_PTMODPAG
      replace t_PTMODPAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANAPP_2_19.value==this.w_PTBANAPP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANAPP_2_19.value=this.w_PTBANAPP
      replace t_PTBANAPP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANAPP_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANNOS_2_21.value==this.w_PTBANNOS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANNOS_2_21.value=this.w_PTBANNOS
      replace t_PTBANNOS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANNOS_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLCRSA_2_23.RadioValue()==this.w_PTFLCRSA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLCRSA_2_23.SetRadio()
      replace t_PTFLCRSA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLCRSA_2_23.value
    endif
    cp_SetControlsValueExtFlds(this,'PAR_TITE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not(.w_TIPPAG<>'BO' OR (.w_TIPPAG='BO' AND NOT EMPTY(.w_PTNUMCOR)) OR (.w_TIPPAG='BO' AND .w_PT_SEGNO='D') OR .w_TIPCLF<>'F')
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("Attenzione: Inserire un Conto Corrente!")
        case   not(.w_PT_SEGNO $ "DA") and (NOT EMPTY(.w_PTDATSCA) AND .w_PTTOTIMP<>0 AND NOT EMPTY(.w_PTNUMPAR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_7
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_PTTOTIMP>0) and (NOT EMPTY(.w_PTDATSCA) AND .w_PTTOTIMP<>0 AND NOT EMPTY(.w_PTNUMPAR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_8
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Attenzione, impossibile inserire importi negativi")
        case   (empty(.w_PTMODPAG) or not(NOT EMPTY(.w_PTMODPAG))) and (NOT EMPTY(.w_PTDATSCA) AND .w_PTTOTIMP<>0 AND NOT EMPTY(.w_PTNUMPAR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_18
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire codice tipo di pagamento")
      endcase
      if NOT EMPTY(.w_PTDATSCA) AND .w_PTTOTIMP<>0 AND NOT EMPTY(.w_PTNUMPAR)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PTSERIAL = this.w_PTSERIAL
    this.o_TIPSCA = this.w_TIPSCA
    this.o_PTDATSCA = this.w_PTDATSCA
    this.o_PTNUMPAR = this.w_PTNUMPAR
    this.o_PT_SEGNO = this.w_PT_SEGNO
    this.o_PTTOTIMP = this.w_PTTOTIMP
    this.o_PTTIPCON = this.w_PTTIPCON
    this.o_PTBANAPP = this.w_PTBANAPP
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_PTDATSCA) AND t_PTTOTIMP<>0 AND NOT EMPTY(t_PTNUMPAR))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PTIMPDOC=0
      .w_PTCAOAPE=0
      .w_PTDATAPE=ctod("  /  /  ")
      .w_PTDATSCA=ctod("  /  /  ")
      .w_PTCODVAL=space(3)
      .w_PTCAOVAL=0
      .w_PTNUMPAR=space(31)
      .w_PT_SEGNO=space(1)
      .w_PTTOTIMP=0
      .w_PTNUMDOC=0
      .w_PTALFDOC=space(10)
      .w_PTDATDOC=ctod("  /  /  ")
      .w_DESVAL=space(35)
      .w_PTTIPCON=space(1)
      .w_PTCODCON=space(15)
      .w_PTFLIMPE=space(2)
      .w_PTNUMDIS=space(10)
      .w_PTMODPAG=space(10)
      .w_PTBANAPP=space(10)
      .w_TIPPAG=space(2)
      .w_PTBANNOS=space(15)
      .w_PTFLINDI=space(1)
      .w_PTFLCRSA=space(2)
      .w_FLCRSA=space(1)
      .w_DESBA=space(35)
      .w_PTDESRIG=space(50)
      .w_PTNUMCOR=space(25)
      .w_PTFLSOSP=space(1)
      .w_PTNUMPRO=0
      .w_PTCODAGE=space(5)
      .w_PTSERRIF=space(10)
      .w_PTORDRIF=0
      .w_PTNUMRIF=0
      .w_PTFLVABD=space(1)
      .w_PTFLRAGG=space(1)
      .w_PTDATREG=ctod("  /  /  ")
      .w_DATSCA=ctod("  /  /  ")
      .w_SIMBVA=space(5)
      .w_IMPPAR=0
      .DoRTCalc(1,14,.f.)
        .w_PTIMPDOC = THIS.oParentObject .w_SCIMPSCA
      .DoRTCalc(16,16,.f.)
        .w_PTCAOAPE = THIS.oParentObject .w_SCCAOVAL
        .w_PTDATAPE = IIF(EMPTY(This.oParentObject .w_SCDATDOC), This.oParentObject .w_SCDATREG, This.oParentObject .w_SCDATDOC)
      .DoRTCalc(19,19,.f.)
        .w_PTCODVAL = .w_CODVAL
      .DoRTCalc(20,20,.f.)
      if not(empty(.w_PTCODVAL))
        .link_2_4('Full')
      endif
        .w_PTCAOVAL = THIS.oParentObject .w_SCCAOVAL
      .DoRTCalc(22,24,.f.)
        .w_PTNUMDOC = .w_NUMDOC
        .w_PTALFDOC = .w_ALFDOC
        .w_PTDATDOC = this.oParentObject .w_SCDATDOC
      .DoRTCalc(28,28,.f.)
        .w_PTTIPCON = .w_TIPCLF
        .w_PTCODCON = .w_CODCON
      .DoRTCalc(31,33,.f.)
      if not(empty(.w_PTMODPAG))
        .link_2_18('Full')
      endif
        .w_PTBANAPP = this.oParentObject .w_SCCODBAN
      .DoRTCalc(34,34,.f.)
      if not(empty(.w_PTBANAPP))
        .link_2_19('Full')
      endif
      .DoRTCalc(35,35,.f.)
        .w_PTBANNOS = THIS.oParentObject .w_SCBANNOS
      .DoRTCalc(36,36,.f.)
      if not(empty(.w_PTBANNOS))
        .link_2_21('Full')
      endif
        .w_PTFLINDI = ' '
        .w_PTFLCRSA = IIF(((.w_TIPCLF='F' and .w_TIPSCA='C') or (.w_TIPCLF='C' and .w_TIPSCA='F')) or .w_ORIGINE='A', 'S', 'C')
        .w_FLCRSA = .w_PTFLCRSA
      .DoRTCalc(40,40,.f.)
        .w_PTDESRIG = this.oParentObject .w_SCDESCRI
      .DoRTCalc(42,42,.f.)
      if not(empty(.w_PTNUMCOR))
        .link_2_27('Full')
      endif
        .w_PTFLSOSP = ' '
      .DoRTCalc(44,44,.f.)
        .w_PTCODAGE = THIS.oParentObject .w_SCCODAGE
      .DoRTCalc(46,48,.f.)
        .w_PTFLVABD = this.oParentObject .w_SCFLVABD
      .DoRTCalc(50,51,.f.)
        .w_DATSCA = .w_PTDATSCA
      .DoRTCalc(53,55,.f.)
        .w_IMPPAR = .w_PTTOTIMP*IIF(.w_PT_SEGNO='D', 1,-1)*IIF(.w_PTTIPCON='C', 1,-1)*IIF(This.oParentObject .w_PFLCRSA='S',-1,1)
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PTIMPDOC = t_PTIMPDOC
    this.w_PTCAOAPE = t_PTCAOAPE
    this.w_PTDATAPE = t_PTDATAPE
    this.w_PTDATSCA = t_PTDATSCA
    this.w_PTCODVAL = t_PTCODVAL
    this.w_PTCAOVAL = t_PTCAOVAL
    this.w_PTNUMPAR = t_PTNUMPAR
    this.w_PT_SEGNO = t_PT_SEGNO
    this.w_PTTOTIMP = t_PTTOTIMP
    this.w_PTNUMDOC = t_PTNUMDOC
    this.w_PTALFDOC = t_PTALFDOC
    this.w_PTDATDOC = t_PTDATDOC
    this.w_DESVAL = t_DESVAL
    this.w_PTTIPCON = t_PTTIPCON
    this.w_PTCODCON = t_PTCODCON
    this.w_PTFLIMPE = t_PTFLIMPE
    this.w_PTNUMDIS = t_PTNUMDIS
    this.w_PTMODPAG = t_PTMODPAG
    this.w_PTBANAPP = t_PTBANAPP
    this.w_TIPPAG = t_TIPPAG
    this.w_PTBANNOS = t_PTBANNOS
    this.w_PTFLINDI = t_PTFLINDI
    this.w_PTFLCRSA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLCRSA_2_23.RadioValue(.t.)
    this.w_FLCRSA = t_FLCRSA
    this.w_DESBA = t_DESBA
    this.w_PTDESRIG = t_PTDESRIG
    this.w_PTNUMCOR = t_PTNUMCOR
    this.w_PTFLSOSP = this.oPgFrm.Page1.oPag.oPTFLSOSP_2_28.RadioValue(.t.)
    this.w_PTNUMPRO = t_PTNUMPRO
    this.w_PTCODAGE = t_PTCODAGE
    this.w_PTSERRIF = t_PTSERRIF
    this.w_PTORDRIF = t_PTORDRIF
    this.w_PTNUMRIF = t_PTNUMRIF
    this.w_PTFLVABD = t_PTFLVABD
    this.w_PTFLRAGG = t_PTFLRAGG
    this.w_PTDATREG = t_PTDATREG
    this.w_DATSCA = t_DATSCA
    this.w_SIMBVA = t_SIMBVA
    this.w_IMPPAR = t_IMPPAR
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PTIMPDOC with this.w_PTIMPDOC
    replace t_PTCAOAPE with this.w_PTCAOAPE
    replace t_PTDATAPE with this.w_PTDATAPE
    replace t_PTDATSCA with this.w_PTDATSCA
    replace t_PTCODVAL with this.w_PTCODVAL
    replace t_PTCAOVAL with this.w_PTCAOVAL
    replace t_PTNUMPAR with this.w_PTNUMPAR
    replace t_PT_SEGNO with this.w_PT_SEGNO
    replace t_PTTOTIMP with this.w_PTTOTIMP
    replace t_PTNUMDOC with this.w_PTNUMDOC
    replace t_PTALFDOC with this.w_PTALFDOC
    replace t_PTDATDOC with this.w_PTDATDOC
    replace t_DESVAL with this.w_DESVAL
    replace t_PTTIPCON with this.w_PTTIPCON
    replace t_PTCODCON with this.w_PTCODCON
    replace t_PTFLIMPE with this.w_PTFLIMPE
    replace t_PTNUMDIS with this.w_PTNUMDIS
    replace t_PTMODPAG with this.w_PTMODPAG
    replace t_PTBANAPP with this.w_PTBANAPP
    replace t_TIPPAG with this.w_TIPPAG
    replace t_PTBANNOS with this.w_PTBANNOS
    replace t_PTFLINDI with this.w_PTFLINDI
    replace t_PTFLCRSA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLCRSA_2_23.ToRadio()
    replace t_FLCRSA with this.w_FLCRSA
    replace t_DESBA with this.w_DESBA
    replace t_PTDESRIG with this.w_PTDESRIG
    replace t_PTNUMCOR with this.w_PTNUMCOR
    replace t_PTFLSOSP with this.oPgFrm.Page1.oPag.oPTFLSOSP_2_28.ToRadio()
    replace t_PTNUMPRO with this.w_PTNUMPRO
    replace t_PTCODAGE with this.w_PTCODAGE
    replace t_PTSERRIF with this.w_PTSERRIF
    replace t_PTORDRIF with this.w_PTORDRIF
    replace t_PTNUMRIF with this.w_PTNUMRIF
    replace t_PTFLVABD with this.w_PTFLVABD
    replace t_PTFLRAGG with this.w_PTFLRAGG
    replace t_PTDATREG with this.w_PTDATREG
    replace t_DATSCA with this.w_DATSCA
    replace t_SIMBVA with this.w_SIMBVA
    replace t_IMPPAR with this.w_IMPPAR
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTPAR = .w_TOTPAR-.w_imppar
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgste_mpaPag1 as StdContainer
  Width  = 793
  height = 194
  stdWidth  = 793
  stdheight = 194
  resizeXpos=173
  resizeYpos=95
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_12 as cp_runprogram with uid="EETGCKVCIC",left=257, top=201, width=184,height=26,;
    caption='GSTE_BPP',;
   bGlobalFont=.t.,;
    prg="GSTE_BPP",;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 9411658


  add object oObj_1_18 as cp_runprogram with uid="XSJOQKJDAB",left=9, top=201, width=240,height=26,;
    caption='GSCG_BKP(S,QUERY)',;
   bGlobalFont=.t.,;
    prg="GSCG_BKP('S','Query')",;
    cEvent = "Delete row start",;
    nPag=1;
    , HelpContextID = 99259361


  add object oObj_1_19 as cp_runprogram with uid="UGYRJSJBRF",left=449, top=201, width=244,height=26,;
    caption='GSCG_BKP(S,EDIT)',;
   bGlobalFont=.t.,;
    prg="GSCG_BKP('S','Edit')",;
    cEvent = "Update row start",;
    nPag=1;
    , HelpContextID = 217672031


  add object oObj_1_23 as cp_runprogram with uid="QSFNFJFIBN",left=9, top=226, width=184,height=26,;
    caption='GSTE_BP1(A)',;
   bGlobalFont=.t.,;
    prg="GSTE_BP1('A')",;
    cEvent = "Salda",;
    nPag=1;
    , HelpContextID = 9226473


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=1, top=1, width=786,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="PTDATSCA",Label1="Scadenza",Field2="PTNUMPAR",Label2="Numero partita",Field3="PT_SEGNO",Label3="D/A",Field4="PTTOTIMP",Label4="Importo",Field5="PTMODPAG",Label5="Pagamento",Field6="PTBANAPP",Label6="Banca appoggio",Field7="PTBANNOS",Label7="Nostra banca",Field8="PTFLCRSA",Label8="Partite",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168621434

  add object oStr_1_20 as StdString with uid="GVGIGOEIQF",Visible=.t., Left=6, Top=141,;
    Alignment=1, Width=82, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="HODHTSGGFC",Visible=.t., Left=3, Top=-79,;
    Alignment=0, Width=587, Height=19,;
    Caption="Attenzione, aggiungere i campi anche nella corrispondente gestione dello storico"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="KTPDLTIWXJ",Visible=.t., Left=236, Top=167,;
    Alignment=0, Width=40, Height=18,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-9,top=20,;
    width=782+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-8,top=21,width=781+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='MOD_PAGA|BAN_CHE|COC_MAST|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPTDESRIG_2_26.Refresh()
      this.Parent.oPTNUMCOR_2_27.Refresh()
      this.Parent.oPTFLSOSP_2_28.Refresh()
      this.Parent.oSIMBVA_2_41.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='MOD_PAGA'
        oDropInto=this.oBodyCol.oRow.oPTMODPAG_2_18
      case cFile='BAN_CHE'
        oDropInto=this.oBodyCol.oRow.oPTBANAPP_2_19
      case cFile='COC_MAST'
        oDropInto=this.oBodyCol.oRow.oPTBANNOS_2_21
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_15 as StdButton with uid="GLUZAKFQDS",width=48,height=45,;
   left=737, top=143,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alla situazione della partita selezionata";
    , HelpContextID = 188648033;
    , tabstop=.f., Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_15.Click()
      do GSCG_KPA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_15.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PTNUMPAR) AND .w_TIPOPE<>'Load')
    endwith
  endfunc

  add object oPTDESRIG_2_26 as StdTrsField with uid="RPKIVRTWNK",rtseq=41,rtrep=.t.,;
    cFormVar="w_PTDESRIG",value=space(50),;
    ToolTipText = "Eventuali note aggiuntive di riga",;
    HelpContextID = 246375741,;
    cTotal="", bFixedPos=.t., cQueryName = "PTDESRIG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=89, Top=141, InputMask=replicate('X',50)

  func oPTDESRIG_2_26.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PTDATSCA) and .w_ORIGINE<>'A')
    endwith
  endfunc

  add object oPTNUMCOR_2_27 as StdTrsField with uid="PICHJRJUDC",rtseq=42,rtrep=.t.,;
    cFormVar="w_PTNUMCOR",value=space(25),;
    ToolTipText = "Conto corrente",;
    HelpContextID = 10484408,;
    cTotal="", bFixedPos=.t., cQueryName = "PTNUMCOR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Conto corrente incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=144, Left=540, Top=166, InputMask=replicate('X',25), bHasZoom = .t. , cLinkFile="BAN_CONTI", oKey_1_1="CCTIPCON", oKey_1_2="this.w_PTTIPCON", oKey_2_1="CCCODCON", oKey_2_2="this.w_PTCODCON", oKey_3_1="CCCODBAN", oKey_3_2="this.w_PTBANAPP", oKey_4_1="CCCONCOR", oKey_4_2="this.w_PTNUMCOR"

  func oPTNUMCOR_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oPTNUMCOR_2_27.ecpDrop(oSource)
    this.Parent.oContained.link_2_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPTNUMCOR_2_27.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BAN_CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PTTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStrODBC(this.Parent.oContained.w_PTCODCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStrODBC(this.Parent.oContained.w_PTBANAPP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStr(this.Parent.oContained.w_PTTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStr(this.Parent.oContained.w_PTCODCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStr(this.Parent.oContained.w_PTBANAPP)
    endif
    do cp_zoom with 'BAN_CONTI','*','CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR',cp_AbsName(this.parent,'oPTNUMCOR_2_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco conti correnti",'',this.parent.oContained
  endproc

  add object oPTFLSOSP_2_28 as StdTrsCheck with uid="CRDHIUFFJM",rtrep=.t.,;
    cFormVar="w_PTFLSOSP",  caption="Sospesa",;
    ToolTipText = "Se attivo: partita sospesa",;
    HelpContextID = 196511046,;
    Left=540, Top=145,;
    cTotal="", cQueryName = "PTFLSOSP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f., AutoSize=.F.;
   , bGlobalFont=.t.


  func oPTFLSOSP_2_28.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PTFLSOSP,&i_cF..t_PTFLSOSP),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oPTFLSOSP_2_28.GetRadio()
    this.Parent.oContained.w_PTFLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oPTFLSOSP_2_28.ToRadio()
    this.Parent.oContained.w_PTFLSOSP=trim(this.Parent.oContained.w_PTFLSOSP)
    return(;
      iif(this.Parent.oContained.w_PTFLSOSP=='S',1,;
      0))
  endfunc

  func oPTFLSOSP_2_28.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPTFLSOSP_2_28.mCond()
    with this.Parent.oContained
      return (.w_ORIGINE<>'A')
    endwith
  endfunc

  add object oBtn_2_35 as StdButton with uid="OTJWRNXRGV",width=48,height=45,;
   left=687, top=143,;
    CpPicture="bmp\Scadenze.bmp", caption="", nPag=2;
    , HelpContextID = 38735322;
    , caption='\<Salda';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_35.Click()
      with this.Parent.oContained
        gste_bp1(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_35.mCond()
    with this.Parent.oContained
      return (Empty(.oParentObject .w_SERDOC))
    endwith
  endfunc

  func oBtn_2_35.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.oParentObject .w_PFLCRSA='C')
    endwith
   endif
  endfunc

  add object oSIMBVA_2_41 as StdTrsField with uid="PUIKPKIFNY",rtseq=53,rtrep=.t.,;
    cFormVar="w_SIMBVA",value=space(5),enabled=.f.,;
    HelpContextID = 35853786,;
    cTotal="", bFixedPos=.t., cQueryName = "SIMBVA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=89, Top=166, InputMask=replicate('X',5)

  add object oStr_2_38 as StdString with uid="WRNKJLUSOX",Visible=.t., Left=442, Top=167,;
    Alignment=1, Width=96, Height=18,;
    Caption="Conto corrente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="YEKXMZZGEA",Visible=.t., Left=38, Top=167,;
    Alignment=1, Width=50, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTPAR_3_1 as StdField with uid="FBFGXUPGVN",rtseq=55,rtrep=.f.,;
    cFormVar="w_TOTPAR",value=0,enabled=.f.,;
    HelpContextID = 40148938,;
    cQueryName = "TOTPAR",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=277, Top=166, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oTOTPAR_3_1.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!Isalt())
    endwith
    endif
  endfunc
enddefine

* --- Defining Body row
define class tgste_mpaBodyRow as CPBodyRowCnt
  Width=772
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPTDATSCA_2_3 as StdTrsField with uid="AETXICPCFD",rtseq=19,rtrep=.t.,;
    cFormVar="w_PTDATSCA",value=ctod("  /  /  "),;
    ToolTipText = "Data della scadenza",;
    HelpContextID = 263939383,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=-2, Top=0

  add object oPTNUMPAR_2_6 as StdTrsField with uid="NVDDWSKHON",rtseq=22,rtrep=.t.,;
    cFormVar="w_PTNUMPAR",value=space(31),;
    ToolTipText = "Numero della partita",;
    HelpContextID = 60816056,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=164, Left=80, Top=0, InputMask=replicate('X',31), bHasZoom = .t. 

  proc oPTNUMPAR_2_6.mZoom
      with this.Parent.oContained
        gste_bp1(this.Parent.oContained,"S")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPT_SEGNO_2_7 as StdTrsField with uid="YJDHLPTDJC",rtseq=23,rtrep=.t.,;
    cFormVar="w_PT_SEGNO",value=space(1),;
    ToolTipText = "Sezione importo partita: D= dare; A =avere",;
    HelpContextID = 220261051,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=23, Left=247, Top=0, cSayPict=["!"], cGetPict=["!"], InputMask=replicate('X',1)

  proc oPT_SEGNO_2_7.mDefault
    with this.Parent.oContained
      if empty(.w_PT_SEGNO)
        .w_PT_SEGNO = IIF(.w_TIPSCA='F', 'A', 'D')
      endif
    endwith
  endproc

  func oPT_SEGNO_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PT_SEGNO $ "DA")
    endwith
    return bRes
  endfunc

  add object oPTTOTIMP_2_8 as StdTrsField with uid="QJFIVLRJER",rtseq=24,rtrep=.t.,;
    cFormVar="w_PTTOTIMP",value=0,;
    ToolTipText = "Importo della scadenza",;
    HelpContextID = 171285178,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, impossibile inserire importi negativi",;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=274, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oPTTOTIMP_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PTTOTIMP>0)
    endwith
    return bRes
  endfunc

  add object oPTMODPAG_2_18 as StdTrsField with uid="NMCZVFVGXG",rtseq=33,rtrep=.t.,;
    cFormVar="w_PTMODPAG",value=space(10),;
    ToolTipText = "Codice tipo di pagamento",;
    HelpContextID = 70650563,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire codice tipo di pagamento",;
   bGlobalFont=.t.,;
    Height=17, Width=86, Left=406, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_PAGA", cZoomOnZoom="GSAR_AMP", oKey_1_1="MPCODICE", oKey_1_2="this.w_PTMODPAG"

  func oPTMODPAG_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oPTMODPAG_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPTMODPAG_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_PAGA','*','MPCODICE',cp_AbsName(this.parent,'oPTMODPAG_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMP',"Tipi di pagamento",'',this.parent.oContained
  endproc
  proc oPTMODPAG_2_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MPCODICE=this.parent.oContained.w_PTMODPAG
    i_obj.ecpSave()
  endproc

  add object oPTBANAPP_2_19 as StdTrsField with uid="LMVWSHGTAN",rtseq=34,rtrep=.t.,;
    cFormVar="w_PTBANAPP",value=space(10),;
    ToolTipText = "Codice della banca di appoggio del cli/for per ric.bancarie e tratte",;
    HelpContextID = 44350138,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=102, Left=495, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_PTBANAPP"

  func oPTBANAPP_2_19.mCond()
    with this.Parent.oContained
      return (.oparentobject .w_SCTIPCLF $ 'CF')
    endwith
  endfunc

  func oPTBANAPP_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
      if .not. empty(.w_PTNUMCOR)
        bRes2=.link_2_27('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPTBANAPP_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPTBANAPP_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oPTBANAPP_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oPTBANAPP_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_PTBANAPP
    i_obj.ecpSave()
  endproc

  add object oPTBANNOS_2_21 as StdTrsField with uid="YHCGUWIMRM",rtseq=36,rtrep=.t.,;
    cFormVar="w_PTBANNOS",value=space(15),;
    ToolTipText = "Nostro C/C. comunicato al fornitore per RB o al cliente per bonifico",;
    HelpContextID = 94681783,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=84, Left=600, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , tabstop=.f., cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_PTBANNOS"

  func oPTBANNOS_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oPTBANNOS_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPTBANNOS_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oPTBANNOS_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenco conti banche",'',this.parent.oContained
  endproc
  proc oPTBANNOS_2_21.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_PTBANNOS
    i_obj.ecpSave()
  endproc

  add object oPTFLCRSA_2_23 as StdTrsCombo with uid="PPIGUDUECT",rtrep=.t.,;
    cFormVar="w_PTFLCRSA", RowSource=""+"Crea,"+"Salda,"+"Acconto" , ;
    HelpContextID = 230065463,;
    Height=22, Width=80, Left=687, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPTFLCRSA_2_23.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PTFLCRSA,&i_cF..t_PTFLCRSA),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'S',;
    iif(xVal =3,'A',;
    space(2)))))
  endfunc
  func oPTFLCRSA_2_23.GetRadio()
    this.Parent.oContained.w_PTFLCRSA = this.RadioValue()
    return .t.
  endfunc

  func oPTFLCRSA_2_23.ToRadio()
    this.Parent.oContained.w_PTFLCRSA=trim(this.Parent.oContained.w_PTFLCRSA)
    return(;
      iif(this.Parent.oContained.w_PTFLCRSA=='C',1,;
      iif(this.Parent.oContained.w_PTFLCRSA=='S',2,;
      iif(this.Parent.oContained.w_PTFLCRSA=='A',3,;
      0))))
  endfunc

  func oPTFLCRSA_2_23.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPTFLCRSA_2_23.mCond()
    with this.Parent.oContained
      return (.w_ORIGINE <> 'A' or  Not isalt() or g_COGE='S')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPTDATSCA_2_3.When()
    return(.t.)
  proc oPTDATSCA_2_3.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPTDATSCA_2_3.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_mpa','PAR_TITE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PTSERIAL=PAR_TITE.PTSERIAL";
  +" and "+i_cAliasName2+".PTROWORD=PAR_TITE.PTROWORD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
