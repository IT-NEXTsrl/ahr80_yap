* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bnp                                                        *
*              Crea cursore num. pagine                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_19]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-27                                                      *
* Last revis.: 2000-01-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bnp",oParentObject)
return(i_retval)

define class tgscg_bnp as StdBatch
  * --- Local variables
  w_parto = space(12)
  w_pagina = 0
  w_indi = space(35)
  w_codazi = space(5)
  w_capo = space(5)
  w_ORIENTA = space(1)
  w_loco = space(30)
  w_MESS = space(100)
  w_provo = space(2)
  w_codo = space(16)
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea un Cursore per la Numerazione Pagine (da GSCG_SNP)
    testa=this.oParentObject.w_testa
    anno=this.oParentObject.w_anno
    this.w_codazi = i_codazi
    * --- dati da stampare in testata
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_indi = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
      this.w_loco = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
      this.w_capo = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
      this.w_provo = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
      this.w_parto = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      this.w_codo = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Variabili da passare al report
    L_INDI=this.w_indi
    L_LOCO=this.w_loco
    L_CAPO=this.w_capo
    L_PROVO=this.w_provo
    L_CODO=this.w_codo
    L_PARTO=this.w_parto
    L_LEGENDA=IIF(this.oParentObject.w_GIORNALE="I",this.oParentObject.w_legenda,"")
    * --- CURSOR PER STAMPARE LE PAGINE
    create cursor __TMP__ (campo1 N(7,0))
    * --- COSTRUISCO IL CURSORE
    this.w_PAGINA = this.oParentObject.w_inipag
    do while this.w_PAGINA<=this.oParentObject.w_finpag
      insert into __TMP__ (campo1) values (this.w_PAGINA)
      this.w_PAGINA = this.w_PAGINA+1
    enddo
    * --- LANCIO LA STAMPA
    SELECT __TMP__
    if this.oParentObject.w_GIORNALE="G"
      * --- STAMPA LIBRO GIORNALE SEMPRE ORIENTAMENTO VERTICALE
      * --- STAMPA REGISTRO CESPITI ORIENTAMENTO ORIZZONTALE
      if this.oParentObject.w_ORINUM="V"
        if this.oParentObject.w_TESTO="S"
          CP_CHPRN("NUMGIO_V.FXP","S", this)
        else
          CP_CHPRN("QUERY\GSCG1SNP.FRX", " ", this)
        endif
      else
        if this.oParentObject.w_TESTO="S"
          CP_CHPRN("NUMGIO_O.FXP","S", this)
        else
          CP_CHPRN("QUERY\GSCG5SNP.FRX", " ", this)
        endif
      endif
    else
      if this.oParentObject.w_ORINUM="V"
        * --- STAMPA ORIENTAMENTO VERTICALE
        if this.oParentObject.w_TESTO="S"
          CP_CHPRN("NUMIVA_V.FXP","S", this)
        else
          CP_CHPRN("QUERY\GSCG3SNP.FRX", " ", this)
        endif
      else
        * --- STAMPA ORIENTAMENTO ORIZZONTALE
        if this.oParentObject.w_TESTO="S"
          CP_CHPRN("NUMIVA_O.FXP","S", this)
        else
          CP_CHPRN("QUERY\GSCG6SNP.FRX", " ", this)
        endif
      endif
    endif
    * --- chiudo il cursore
    if USED("__TMP__")
      SELECT __TMP__
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
