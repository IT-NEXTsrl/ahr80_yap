* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bkc                                                        *
*              Controlli cancellazione nominativi                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-11-14                                                      *
* Last revis.: 2009-03-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bkc",oParentObject)
return(i_retval)

define class tgsar_bkc as StdBatch
  * --- Local variables
  w_TEST = 0
  w_MESS = space(100)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSAR_ANO (Anagrafica nominativi)  esegue Controlli in cancellazione
    this.w_TEST = 0
    if this.w_TEST=0
      * --- Select from OFF_ATTI
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" OFF_ATTI ";
            +" where ATCODNOM="+cp_ToStrODBC(this.oParentObject.w_NOCODICE)+" ";
             ,"_Curs_OFF_ATTI")
      else
        select Count(*) As Conta from (i_cTable);
         where ATCODNOM=this.oParentObject.w_NOCODICE ;
          into cursor _Curs_OFF_ATTI
      endif
      if used('_Curs_OFF_ATTI')
        select _Curs_OFF_ATTI
        locate for 1=1
        do while not(eof())
        this.w_TEST = IIF( Nvl( _Curs_OFF_ATTI.CONTA , 0 )>0, 1 , 0 )
        this.w_MESS = ah_Msgformat("Impossibile cancellare, nominativo presente nelle attivit�") 
          select _Curs_OFF_ATTI
          continue
        enddo
        use
      endif
    endif
    if this.w_TEST=0 and (isapa() or isahe()) and this.oParentObject.w_NOFLGBEN="B"
      * --- Select from DOC_MAST
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" DOC_MAST ";
            +" where MVCODNOM="+cp_ToStrODBC(this.oParentObject.w_NOCODICE)+" ";
             ,"_Curs_DOC_MAST")
      else
        select Count(*) As Conta from (i_cTable);
         where MVCODNOM=this.oParentObject.w_NOCODICE ;
          into cursor _Curs_DOC_MAST
      endif
      if used('_Curs_DOC_MAST')
        select _Curs_DOC_MAST
        locate for 1=1
        do while not(eof())
        this.w_TEST = IIF( Nvl( _Curs_DOC_MAST.CONTA , 0 )>0, 1 , 0 )
        this.w_MESS = ah_Msgformat("Impossibile cancellare, debitore/creditore diverso presente nei documenti") 
          select _Curs_DOC_MAST
          continue
        enddo
        use
      endif
    endif
    if this.w_TEST>0
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='DOC_MAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_OFF_ATTI')
      use in _Curs_OFF_ATTI
    endif
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
