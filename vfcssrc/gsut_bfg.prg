* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bfg                                                        *
*              Gestione maschera visualizzazione                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-12                                                      *
* Last revis.: 2017-03-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Operazione,w_Archivio,w_Chiave,w_Periodo,w_GestAtt,w_bPEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bfg",oParentObject,m.w_Operazione,m.w_Archivio,m.w_Chiave,m.w_Periodo,m.w_GestAtt,m.w_bPEC)
return(i_retval)

define class tgsut_bfg as StdBatch
  * --- Local variables
  w_Operazione = space(2)
  w_Archivio = space(2)
  w_Chiave = space(20)
  w_Periodo = ctod("  /  /  ")
  w_GestAtt = .NULL.
  w_bPEC = .f.
  w_OperPassata = space(2)
  w_ISALT = .f.
  w_FILE = space(20)
  w_RET = .f.
  w_DesPat = space(100)
  w_GSUT_AID = .NULL.
  w_Found = .f.
  w_CDCLAAWE = space(10)
  w_CONFMASK = .f.
  w_DESCRI = space(40)
  w_CDMOTARC = space(1)
  w_CODUTE = 0
  w_CLASSE = space(10)
  w_IDSERIAL = space(10)
  w_TMPC = space(100)
  w_TMPT = space(1)
  w_TMPN = 0
  w_IDVALDAT = ctod("  /  /  ")
  w_IDVALNUM = 0
  w_CDVLPRED = space(100)
  w_INDPREDVAL = space(100)
  w_INDVALATTR = space(0)
  w_CDCAMCUR = space(100)
  w_IDVALATT = space(100)
  w_LICODICE = space(10)
  w_LICAMRAG = space(100)
  w_LICLADOC = space(100)
  w_NOMEFILE = space(20)
  w_TIPOFILE = space(50)
  w_posiz = 0
  w_PATH = space(250)
  w_ChiaveRicerca = space(20)
  w_CritPeri = space(2)
  w_LIPATIMG = space(254)
  w_ImgPat = space(254)
  w_TmpPat = space(10)
  w_MemFile = space(10)
  w_ChkBuffer = space(10)
  w_Volume = space(10)
  w_Ok = .f.
  w_FILEDEL = space(20)
  w_curdir = space(50)
  w_CATCH = space(250)
  w_posizpunto = 0
  w_posiz2punto = 0
  w_range = 0
  w_estensione = space(10)
  w_numfile = 0
  w_MAX = 0
  w_cont = 0
  w_indice = space(5)
  w_destinazione = space(250)
  w_sorgente = space(250)
  w_MARK_BMP = space(10)
  w_MARK_DCX = space(10)
  w_MARK_EPS = space(10)
  w_MARK_JPG = space(10)
  w_MARK_TIF = space(10)
  w_MARK_GIF = space(10)
  w_MARK = space(10)
  w_SHELLEXEC = 0
  w_DelIndex = .f.
  w_DelFile = .f.
  w_IDMODALL = space(1)
  w_TIPOARCH = space(1)
  w_MODALLEG = space(1)
  w_SingMult = 0
  w_EMPTYFOLD = space(1)
  w_RICDESCRI = space(1)
  w_DEMPTYFOLD = space(10)
  w_NEMPTYFOLD = space(10)
  w_DESBRE = space(100)
  w_DESLUN = space(0)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_TMPN1 = 0
  w_CONTA = 0
  w_PATHFILE = space(254)
  w_ORIGFILE = space(200)
  w_PADRE = .NULL.
  w_CURSORE = space(10)
  w_CLASDOCU = space(15)
  w_KEYINDIC = space(20)
  w_DESCRFILE = space(254)
  w_MskMESS = .NULL.
  w_ERRLOG = .f.
  w_NEWOBJ = space(100)
  w_CDPUBWEB = space(1)
  w_INARCHIVIO = space(1)
  w_NOMEFILEORIG = space(20)
  w_IDWEBAPP = space(1)
  w_IDWEBFIL = space(254)
  w_ORIGIDWEBFIL = space(254)
  w_IS_GSUT_GFG = .f.
  w_AECODPRA = space(15)
  w_AEORIFIL = space(100)
  w_AEOGGETT = space(220)
  w_AE_TESTO = space(0)
  w_FL_SAVE = .f.
  w_AEPATHFI = space(254)
  w_TIPOALL = space(5)
  w_CLASALL = space(5)
  w_AETIALCP = space(1)
  w_AECLALCP = space(1)
  w_AETYPEAL = space(1)
  w_AENOMEAL = space(1)
  w_AEDESPRA = space(50)
  w_LSELMUL = .f.
  w_LGFNOMEFILE = space(20)
  w_CODPRE = space(20)
  w_RAGPRE = space(10)
  w_COPIADESCR = space(1)
  w_DESART = space(40)
  w_OKINDICE = .f.
  w_CODAZI = space(5)
  w_PATHPR = space(254)
  w_CODUTE = 0
  w_OKFILE = .f.
  w_MESS = space(100)
  w_RETVAL = 0
  w_LMODALL = space(1)
  w_CODMOD = space(10)
  w_CLAMOD = space(15)
  w_PATMOD = space(254)
  w_QUEMOD = space(60)
  w_TESTO = space(0)
  w_TIPOALLE = space(5)
  w_CLASSEALL = space(5)
  w_OGGETTO = space(200)
  w_PATARC = space(250)
  w_FILEORIG = space(80)
  w_CODPRE = space(20)
  w_RAGPRE = space(10)
  w_CN__ENTE = space(10)
  w_CNTIPPRA = space(10)
  w_CNMATPRA = space(10)
  w_CODICEPRAT = space(15)
  w_DESCPRAT = space(100)
  w_OLDCODPRAT = space(15)
  w_ROWSEARCH = 0
  w_FILEDEST = space(254)
  w_PATHATT = space(254)
  w_IDUTEEXP = 0
  w_IDDATZIP = ctod("  /  /  ")
  w_PATHDEST = space(254)
  w_RETMSG = space(254)
  w_CKUTEEXP = 0
  w_PAVERDOC = space(1)
  w_FLTYPIMP = space(1)
  w_FLSUBFLD = .f.
  w_ZIPNAME = space(254)
  w_NUMIDXRES = 0
  w_NUMFIL = 0
  w_DATZIP = ctot("")
  w_PADATZIP = space(1)
  w_FILEVERS = space(265)
  w_NEWSERIDX = space(20)
  w_COPATHDO = space(100)
  w_CDERASEF = space(1)
  w_ROLLBACK = .f.
  w_ERRODIR = .f.
  w_OldConn = 0
  w_NewConn = 0
  w_oObjInfinity = .NULL.
  w_CDCODATT = space(20)
  w_ERRINF = 0
  w_OBJXMLRES = .NULL.
  w_FILETOSTR = space(254)
  w_NUMNODI = 0
  w_oNODE = .NULL.
  w_iChild = 0
  w_NameError = space(254)
  w_ERRORTRASF = .f.
  w_GSUT_MA1 = .NULL.
  w_cLink = space(255)
  w_cParms = space(10)
  w_ABSPATH = space(255)
  w_GFILE = space(254)
  w_SBLOCBLOC = space(1)
  w_NewDescr = space(254)
  w_NumGiri = 0
  w_DELAY = 0
  w_OBJXML = .NULL.
  w_UploadDocumentsXML = space(254)
  w_sNode = .NULL.
  w_CDCODATT = space(15)
  w_IDVALATT = space(100)
  w_sCHILD = .NULL.
  w_sCHILDDETT = .NULL.
  w_sCHILDDETTPROP = .NULL.
  w_COUNTER = 0
  w_INTESTAZIONE = space(254)
  w_CDCLAINF = space(15)
  w_ID_TESTO = space(0)
  w_CDTIPATT = space(1)
  w_OLDSETCENTURY = space(3)
  * --- WorkFile variables
  LIB_IMMA_idx=0
  PROMCLAS_idx=0
  AZIENDA_idx=0
  PROMINDI_idx=0
  PRODINDI_idx=0
  PRODCLAS_idx=0
  PRO_PERM_idx=0
  PROMODEL_idx=0
  TMP_PRAT_idx=0
  LOGSININ_idx=0
  PAR_ALTE_idx=0
  CONTROPA_idx=0
  DIPENDEN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione file associati ad articoli o documenti. 
    *     Questo Batch viene richiamato dalla maschera GSUT_KFG.
    * --- Operazione richiesta 
    *     ==================
    *     'B' = Blocca indice
    *     'F' = Sblocca indice
    *     'C' = Cattura,
    *     'V' = Visualizza, 
    *     'D' = Elimina, 
    *     'N' = Nuovo documento,
    *     'U' = Duplica documento,
    *     'P' = Duplicazione di una pratica
    *     'R' = Importa file
    *     'S' = tipo di archiviazione singola 
    *     'Z' = Export file
    *     'T' = Trasforma in PDF e indicizza
    *     'K' = Sincornizzazioni
    *     'E' = Richiede la cartella dove esportare il file selezionato
    *     'O' = Apre directory contenente le immagini
    *     'M' = Invia e-mail o invio e-mail con allegati PDF
    *     'G' = Genera PDF
    *     'W' = Invio documenti ad applicazione web infinity
    *     'X' = Carica attributi di ricerca
    * --- Archivio su cui eseguire l'archiviazione
    * --- Valore campo chiave dell'archivio su cui eseguire l'archiviazione
    * --- Data periodo archiviazione
    * --- Riferimento alla gestione attiva (se presente)
    this.w_OperPassata = this.w_Operazione
    this.w_ISALT = ISALT()
    this.w_NOMEFILE = space(20)
    this.w_TIPOFILE = space(50)
    this.w_posiz = 1
    this.w_PATH = space(250)
    this.w_ChiaveRicerca = space(20)
    * --- Path della libreria contenente le cartelle con le immagini (In base alle aziende)
    this.w_LIPATIMG = ""
    this.w_ImgPat = ""
    this.w_TmpPat = ""
    this.w_MemFile = ""
    this.w_ChkBuffer = ""
    this.w_Volume = ""
    this.w_Ok = .T.
    * --- File da cancellare
    this.w_FILEDEL = space(20)
    * --- Directory corrente
    this.w_curdir = space(50)
    * --- File da catturare
    this.w_CATCH = space(250)
    * --- Posizione primo punto nel nome del file, partendo da dx
    this.w_posizpunto = 0
    * --- Posizione secondo punto nel nome del file, partendo da dx
    this.w_posiz2punto = 0
    * --- Numero di caratteri tra i due punti
    this.w_range = 0
    * --- Estensione del file
    this.w_estensione = space(10)
    * --- Numero dei file corrispondenti alle caratteristiche indicate
    this.w_numfile = 0
    * --- Massimo progressivo
    this.w_MAX = 0
    * --- Contatori
    this.w_cont = 1
    this.w_indice = space(5)
    * --- Nome del file in cui sar� memorizzato il file catturato o rinominato
    this.w_destinazione = space(250)
    * --- Path del file da rinominare
    this.w_sorgente = space(250)
    * --- Tipologie di file supportate da Scan & View
    * --- --
    * --- Varibili per selezione multipla . Gestite in invio email e copia file
    this.w_CDPUBWEB = "N"
    this.w_INARCHIVIO = "X"
    this.w_NOMEFILEORIG = space(20)
    * --- Codifica ASCII dei primi 4 caratteri presenti nei file
    this.w_MARK_BMP = Chr(0x42)+Chr(0x4d)+Chr(0x46)+Chr(0x16)
    this.w_MARK_DCX = Chr(0xb1)+Chr(0x68)+Chr(0xde)+Chr(0x3a)
    this.w_MARK_EPS = Chr(0x25)+Chr(0x21)+Chr(0x50)+Chr(0x53)
    this.w_MARK_JPG = Chr(0xff)+Chr(0xd8)+Chr(0xff)+Chr(0xe0)
    this.w_MARK_TIF = Chr(0x49)+Chr(0x49)+Chr(0x2a)+Chr(0x00)
    this.w_MARK_GIF = Chr(0x47)+Chr(0x49)+Chr(0x46)+Chr(0x38)
    this.w_MARK = space(10)
    this.w_SHELLEXEC = 0
    this.w_PADRE = this.oParentObject
    this.w_IS_GSUT_GFG = lower(this.w_PADRE.class) = "tgsut_kfg"
    if this.w_Operazione=="A"
      if this.oParentObject.w_SELMULT
        * --- Inizio elaborazione documenti selezionati
        if this.w_IS_GSUT_GFG
          this.w_CURSORE = this.w_PADRE.w_ZoomGF.cCursor
        else
          this.w_CURSORE = this.w_PADRE.w_PRATICHE.cCursor
        endif
        this.w_TMPN = 0
        this.w_KEYINDIC = ""
         
 Select(this.w_CURSORE) 
 Go Top
        LOCATE FOR XCHK=1
        if FOUND()
          this.w_KEYINDIC = ALLTRIM(GFKEYINDIC)
        endif
         
 Select(this.w_CURSORE) 
 Go Top
      else
        if !this.w_ISALT
          * --- Operazione su documento attivo (record attivo)
          this.w_KEYINDIC = ALLTRIM(this.oParentObject.w_GFKEYINDIC)
        endif
      endif
      if !EMPTY(this.w_KEYINDIC) AND this.w_ARCHIVIO=="CAN_TIER"
        * --- Read from PRODINDI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IDVALATT"+;
            " from "+i_cTable+" PRODINDI where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                +" and IDCODATT = "+cp_ToStrODBC("CODICE");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IDVALATT;
            from (i_cTable) where;
                IDSERIAL = this.w_KEYINDIC;
                and IDCODATT = "CODICE";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_AECODPRA = NVL(cp_ToDate(_read_.IDVALATT),cp_NullValue(_read_.IDVALATT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from PRODINDI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IDVALATT"+;
            " from "+i_cTable+" PRODINDI where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                +" and IDCODATT = "+cp_ToStrODBC("DESCRIZIONE");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IDVALATT;
            from (i_cTable) where;
                IDSERIAL = this.w_KEYINDIC;
                and IDCODATT = "DESCRIZIONE";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_AEDESPRA = NVL(cp_ToDate(_read_.IDVALATT),cp_NullValue(_read_.IDVALATT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if this.w_IS_GSUT_GFG AND Empty(this.w_AECODPRA) and this.w_ISALT
        this.w_AECODPRA = ALLTRIM(this.oParentObject.w_CODPRAT)
        this.w_AEDESPRA = ALLTRIM(this.oParentObject.w_DESPRAT)
      endif
      this.w_AECODPRA = ALLTRIM(this.w_AECODPRA)
      this.w_AEORIFIL = ""
      this.w_AEOGGETT = ""
      this.w_AE_TESTO = ""
      this.w_AEPATHFI = ""
      this.w_FL_SAVE = .F.
      this.w_TIPOALL = ""
      this.w_CLASALL = ""
      this.w_AETIALCP = "N"
      this.w_AECLALCP = "N"
      this.w_AETYPEAL = "O"
      this.w_AENOMEAL = "O"
      this.w_LSELMUL = this.oParentObject.w_SELMULT
      this.w_LGFNOMEFILE = iif(this.oParentObject.w_selmult,this.oParentObject.w_GFNOMEFILE,"")
      do gspr_kae with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      DIMENSION L_INFOARCH(10)
      L_INFOARCH(1) = "CAN_TIER"
      L_INFOARCH(2) = this.w_AECODPRA
      L_INFOARCH(3) = this.w_AEPATHFI
      L_INFOARCH(4) = this.w_AEORIFIL
      L_INFOARCH(5) = this.w_AEOGGETT
      L_INFOARCH(6) = this.w_AE_TESTO
      L_INFOARCH(7) = this.w_TIPOALL
      L_INFOARCH(8) = this.w_CLASALL
      L_INFOARCH(9) = this.w_AEDESPRA
      L_INFOARCH(10) = this.w_Operazione
      if this.w_ISALT and g_AGEN="S" and (Not Empty(this.w_CODPRE) or Not Empty(this.w_RAGPRE))
        GSAG_BPI(this,this.w_AECODPRA,this.w_CODPRE,this.w_RAGPRE,this.w_DESART)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_OKINDICE = .t.
      if this.w_FL_SAVE
        if this.w_AETYPEAL="O"
          this.w_Operazione = "M"
        else
          this.w_Operazione = "G"
        endif
      else
        ah_Errormsg("Operazione interrotta come richiesto", 64)
      endif
    else
      if this.w_Operazione="M"
        DIMENSION L_INFOARCH(10)
        L_INFOARCH(1) = "CAN_TIER"
        L_INFOARCH(2) = this.w_CHIAVE
        L_INFOARCH(3) = this.w_AEPATHFI
        L_INFOARCH(4) = this.w_AEORIFIL
        L_INFOARCH(5) = this.w_AEOGGETT
        L_INFOARCH(6) = this.w_AE_TESTO
        L_INFOARCH(7) = this.w_TIPOALL
        L_INFOARCH(8) = this.w_CLASALL
        L_INFOARCH(9) = this.w_AEDESPRA
        L_INFOARCH(10) = this.w_Operazione
      else
        L_INFOARCH=""
      endif
      this.w_FL_SAVE = .T.
    endif
    do case
      case INLIST(this.w_OPERAZIONE, "C", "S")
        * --- Seleziona il tipo di archiviazione (singola o multipla)
        if this.w_OPERAZIONE="S"
           
 Local nCmd 
 nCmd= 0 
 DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT 
 DEFINE BAR 1 OF popCmd PROMPT Ah_Msgformat("Acquisizione singola") 
 ON SELECTION BAR 1 OF popCmd nCmd = 1 
 DEFINE BAR 2 OF popCmd PROMPT Ah_Msgformat("Acquisizione multipla") 
 ON SELECTION BAR 2 OF popCmd nCmd = 2 
 ACTIVATE POPUP popCmd 
 DEACTIVATE POPUP popCmd 
 RELEASE POPUPS popCmd
          * --- Variabile che indica se l'acquisizione da scanner � singola o multipla
          *     1 = singola 
          *     2 = multipla
          this.w_SingMult = nCmd
          if this.w_SingMult = 0
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Per l'operazione di carica, determina la classe alla quale associare il file
        * --- Legge, se c'�, la classe di archiviazione
        * --- Read from PROMCLAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CDCODCLA,CDMODALL"+;
            " from "+i_cTable+" PROMCLAS where ";
                +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                +" and CDRIFDEF = "+cp_ToStrODBC("S");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CDCODCLA,CDMODALL;
            from (i_cTable) where;
                CDRIFTAB = this.w_ARCHIVIO;
                and CDRIFDEF = "S";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
          this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if empty(this.w_LICLADOC)
          * --- Verifica se c'� almeno una classe
          * --- Read from PROMCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" PROMCLAS where ";
                  +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CDRIFTAB = this.w_ARCHIVIO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows=0
            AH_ERRORMSG("Non sono state definite classi documentali per la tabella %1",48,"",alltrim(this.w_ARCHIVIO))
            i_retcode = 'stop'
            return
          else
            * --- Verifica se c'� una classe da proporre
            * --- Read from PROMCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PROMCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDPUBWEB"+;
                " from "+i_cTable+" PROMCLAS where ";
                    +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                    +" and CDRIFDEF = "+cp_ToStrODBC("D");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CDCODCLA,CDDESCLA,CDTIPARC,CDMODALL,CDPUBWEB;
                from (i_cTable) where;
                    CDRIFTAB = this.w_ARCHIVIO;
                    and CDRIFDEF = "D";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CLASSE = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
              this.w_DESCRI = NVL(cp_ToDate(_read_.CDDESCLA),cp_NullValue(_read_.CDDESCLA))
              this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
              this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
              this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Verifica diritti ...
            if not empty(this.w_CLASSE) and SUBSTR( CHKPECLA( this.w_CLASSE, i_CODUTE), 2, 1 ) <> "S"
              this.w_CLASSE = " "
              this.w_DESCRI = " "
            endif
            * --- --
            * --- Lancia Maschera di selezione Classe Documentale
            this.w_CONFMASK = .F.
            do GSUT_KKW with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_LICLADOC = this.w_CLASSE
            if empty(this.w_LICLADOC) or not this.w_CONFMASK
              i_retcode = 'stop'
              return
            endif
          endif
          * --- Verifica se l'utente pu� archiviare ....
          if SUBSTR( CHKPECLA( this.w_LICLADOC, i_CODUTE), 2, 1 ) <> "S"
            ah_ERRORMSG("Non si possiedono i diritti per archiviare documenti con la classe documentale %1",48,"",alltrim(this.w_LICLADOC))
            i_retcode = 'stop'
            return
          endif
          * --- Non pu� archiviare con SCANNER se la modalit� di archiviazione � COLLEGAMENTO.
          *     In tal caso infatti il file viene salvato dallo scanner nella temp dell'utente, quindi � necessario che l'archiviazione avvenga per copia file.
          *     Se la classe � COLLEGAMENTO, non � specificato nessun percorso di archiviazione, ma se 
          *     mantiene quello originale i files scannerizzati sono nella temp !
          *     Quindi tutto ok solo se: l'archiviazione � da file (quindi non da scanner) oppure se la modalit� di 
          *     archiviazione � copia file (in tal caso va bene anche l'archiviazione da scanner).
          if this.w_OPERAZIONE="S" and this.w_MODALLEG="L"
            AH_ERRORMSG("La classe documentale selezionata [%1] ha modalit� di archiviazione di tipo <collegamento file>.%0Archiviazione da scanner non consentita.",48,"",alltrim(this.w_LICLADOC))
            i_retcode = 'stop'
            return
          endif
        else
          * --- Non pu� archiviare con SCANNER se la modalit� di archiviazione � COLLEGAMENTO.
          *     In tal caso infatti il file viene salvato dallo scanner nella temp dell'utente, quindi � necessario che l'archiviazione avvenga per copia file.
          *     Se la classe � COLLEGAMENTO, non � specificato nessun percorso di archiviazione, ma se 
          *     mantiene quello originale i files scannerizzati sono nella temp !
          *     Quindi tutto ok solo se: l'archiviazione � da file (quindi non da scanner) oppure se la modalit� di 
          *     archiviazione � copia file (in tal caso va bene anche l'archiviazione da scanner).
          if this.w_OPERAZIONE="S" and this.w_MODALLEG="L"
            AH_ERRORMSG("Classe documentale standard [%1] con modalit� di archiviazione di tipo <collegamento file>.%0Archiviazione da scanner non consentita.",48,"",alltrim(this.w_LICLADOC))
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Richiede file da archiviare <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        * --- Legge attributi di interesse per classe documentale selezionata
        this.w_CODAZI = i_CODAZI
        this.w_CODUTE = i_CODUTE
        * --- Read from PROMCLAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CDPATSTD,CDTIPRAG,CDCAMRAG,CDCLAAWE,CDTIPARC,CDMODALL,CDFOLDER,CDGETDES"+;
            " from "+i_cTable+" PROMCLAS where ";
                +"CDCODCLA = "+cp_ToStrODBC(this.w_LICLADOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CDPATSTD,CDTIPRAG,CDCAMRAG,CDCLAAWE,CDTIPARC,CDMODALL,CDFOLDER,CDGETDES;
            from (i_cTable) where;
                CDCODCLA = this.w_LICLADOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LIPATIMG = NVL(cp_ToDate(_read_.CDPATSTD),cp_NullValue(_read_.CDPATSTD))
          this.w_CRITPERI = NVL(cp_ToDate(_read_.CDTIPRAG),cp_NullValue(_read_.CDTIPRAG))
          this.w_LICAMRAG = NVL(cp_ToDate(_read_.CDCAMRAG),cp_NullValue(_read_.CDCAMRAG))
          this.w_CDCLAAWE = NVL(cp_ToDate(_read_.CDCLAAWE),cp_NullValue(_read_.CDCLAAWE))
          this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
          this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
          this.w_EMPTYFOLD = NVL(cp_ToDate(_read_.CDFOLDER),cp_NullValue(_read_.CDFOLDER))
          this.w_RICDESCRI = NVL(cp_ToDate(_read_.CDGETDES),cp_NullValue(_read_.CDGETDES))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Richiede file da archiviare 
        this.w_curdir = sys(5) + sys(2003)
        do case
          case this.w_OPERAZIONE="C"
            * --- Se richiede il folder, si sposta su una cartella temporanea di archiviazione nella tempo dell'utente
            if this.w_EMPTYFOLD="S" and this.w_MODALLEG<>"L"
              this.w_NEMPTYFOLD = "_DMTMPFOLD"
              this.w_DEMPTYFOLD = ADDBS(tempadhoc())+this.w_NEMPTYFOLD
              * --- Abilita gestione errori
              if DIRECTORY(this.w_DEMPTYFOLD)
                * --- Pulizia cartella
                w_ErrorHandler = on("ERROR")
                w_ERRORE = .F.
                on error w_ERRORE = .T.
                cd (this.w_DEMPTYFOLD)
                DELETE FILE *.*
                on error &w_ErrorHandler
                if w_ERRORE
                  AH_ERRORMSG("Impossibile cancellare il contenuto della cartella contenitore: %0%1%0%0Utilizza archiviazione rapida standard singola",48,"",rtrim(this.w_DEMPTYFOLD))
                  this.w_EMPTYFOLD = "N"
                  * --- Ripristina cartella di lavoro ,,,
                  cd (this.w_curdir)
                endif
              else
                * --- Crea la cartella
                w_ErrorHandler = on("ERROR")
                w_ERRORE = .F.
                on error w_ERRORE = .T.
                MD (this.w_DEMPTYFOLD)
                on error &w_ErrorHandler
                if w_ERRORE
                  ah_ERRORMSG("Impossibile creare la cartella contenitore: %0%1%0%0Utilizza archiviazione rapida standard singola",48,"",rtrim(this.w_DEMPTYFOLD))
                  this.w_EMPTYFOLD = "N"
                  * --- Ripristina cartella di lavoro ,,,
                  cd (this.w_curdir)
                else
                  cd (this.w_DEMPTYFOLD)
                endif
              endif
            endif
            * --- Read from DIPENDEN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DIPENDEN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DPPATHPR"+;
                " from "+i_cTable+" DIPENDEN where ";
                    +"DPCODUTE = "+cp_ToStrODBC(this.w_CODUTE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DPPATHPR;
                from (i_cTable) where;
                    DPCODUTE = this.w_CODUTE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_PATHPR = NVL(cp_ToDate(_read_.DPPATHPR),cp_NullValue(_read_.DPPATHPR))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Empty(this.w_PATHPR) OR ! DIRECTORY(this.w_PATHPR)
              * --- Read from CONTROPA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTROPA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "COPATHPR"+;
                  " from "+i_cTable+" CONTROPA where ";
                      +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  COPATHPR;
                  from (i_cTable) where;
                      COCODAZI = this.w_CODAZI;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_PATHPR = NVL(cp_ToDate(_read_.COPATHPR),cp_NullValue(_read_.COPATHPR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if Not Empty(this.w_PATHPR) and DIRECTORY(this.w_PATHPR)
              * --- Applico path di prelevamento indicato nei parametri
              cd (this.w_PATHPR)
            endif
            * --- Richiede il file ...
            this.w_CATCH = getfile()
            * --- Si prepara ARRAY con elenco dei files presenti nella cartella ...
            if this.w_EMPTYFOLD="S" and this.w_MODALLEG<>"L" and not empty(this.w_CATCH)
              * --- Se � sulla cartella di archiviazione ...
              Dimension DMARRFIL(1)
              if this.w_NEMPTYFOLD $ this.w_CATCH
                ADIR(DMARRFIL)
              endif
            endif
          case this.w_OPERAZIONE="S"
            * --- Continua
            this.w_CATCH = GSUT_BBS(this.oParentObject, this.w_SingMult)
        endcase
        * --- Ripristina cartella di lavoro ,,,
        cd (this.w_curdir)
        this.w_DESBRE = " "
        this.w_DESLUN = " "
        if this.w_RICDESCRI="S"
          this.w_CONFMASK = .F.
          do GSUT1KSW with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_DESBRE = IIF(this.w_CONFMASK, this.w_DESBRE, " ")
          this.w_DESLUN = IIF(this.w_CONFMASK, this.w_DESLUN, " ")
        endif
        if not empty(this.w_CATCH)
          * --- Se da cartella contenitore archivia tutti i files presenti nella cartella ...
          if this.w_OPERAZIONE="C" and this.w_EMPTYFOLD="S" and this.w_MODALLEG<>"L" and vartype(DMARRFIL(1))="C"
            this.w_CONTA = 1
            do while this.w_CONTA <= ALEN(DMARRFIL,1)
              this.w_CATCH = this.w_DEMPTYFOLD+"\"+DMARRFIL(this.w_CONTA,1)
              this.w_TMPN = -1
              if FILE(this.w_CATCH)
                Private L_ArrParam
                dimension L_ArrParam(30)
                L_ArrParam(1)="AR"
                L_ArrParam(2)=this.w_CATCH
                L_ArrParam(3)=this.w_LICLADOC
                L_ArrParam(4)=i_CODUTE
                L_ArrParam(5)=this.w_GestAtt
                L_ArrParam(6)=this.w_ARCHIVIO
                L_ArrParam(7)=this.w_CHIAVE
                L_ArrParam(8)=this.w_DESBRE
                L_ArrParam(9)=this.w_DESLUN
                L_ArrParam(10)=.T.
                L_ArrParam(11)=.F.
                L_ArrParam(12)=" "
                L_ArrParam(13)=" "
                L_ArrParam(14)=this.w_GestAtt
                L_ArrParam(15)=this.w_TIPOALLE
                L_ArrParam(17)=" "
                L_ArrParam(18)=this.w_QUEMOD
                L_ArrParam(19)=this.w_OPERAZIONE
                L_ArrParam(23)=(NVL(this.w_CDPUBWEB, "N")="S")
                this.w_TMPN = GSUT_BBA(this, @L_ArrParam)
              endif
              * --- Aggiorna array - Sfrutta posizione 2 per risultato
              DMARRFIL(this.w_CONTA,2) = this.w_TMPN
              * --- Conta ...
              this.w_CONTA = this.w_CONTA+1
            enddo
            * --- Lancia l'archiviazione silente
            if this.w_CONTA>1
              this.w_oMESS=createobject("AH_Message")
              this.w_oMESS.icon = 64
              this.w_oPART = this.w_oMESS.AddMsgPartnl("Resoconto archiviazione:")
              this.w_TMPN = 1
              do while this.w_TMPN < this.w_CONTA
                this.w_TMPN1 = DMARRFIL(this.w_TMPN,2)
                if this.w_TMPN1=0
                  this.w_oPART = this.w_oMESS.AddMsgPartnl("[%1] File: %2 (OK)")
                  this.w_oPART.AddParam(PADL(ALLTRIM(STR(this.w_TMPN,3,0)),3,"0"))     
                  this.w_oPART.AddParam(alltrim(DMARRFIL(this.w_TMPN,1)))     
                else
                  if this.w_TMPN1<0
                    this.w_oPART = this.w_oMESS.AddMsgPartnl("[%1] File: %2 (file non trovato)")
                    this.w_oPART.AddParam(PADL(ALLTRIM(STR(this.w_TMPN,3,0)),3,"0"))     
                    this.w_oPART.AddParam(alltrim(DMARRFIL(this.w_TMPN,1)))     
                  else
                    this.w_oPART = this.w_oMESS.AddMsgPartnl("[%1] File: %2 (errore %3)")
                    this.w_oPART.AddParam(PADL(ALLTRIM(STR(this.w_TMPN,3,0)),3,"0"))     
                    this.w_oPART.AddParam(alltrim(DMARRFIL(this.w_TMPN,1)))     
                    this.w_oPART.AddParam(str(this.w_TMPN1,3,0))     
                  endif
                endif
                this.w_TMPN = this.w_TMPN+1
              enddo
              this.w_oMESS.AH_ErrorMsg(64)     
            endif
          else
            * --- Archiviazione standard singola ...
            if FILE(this.w_CATCH)
              Private L_ArrParam
              dimension L_ArrParam(30)
              L_ArrParam(1)="AR"
              L_ArrParam(2)=this.w_CATCH
              L_ArrParam(3)=this.w_LICLADOC
              L_ArrParam(4)=i_CODUTE
              if this.w_ISALT
                L_ArrParam(5)=this.oParentObject
              else
                L_ArrParam(5)=this.w_GestAtt
              endif
              L_ArrParam(6)=this.w_ARCHIVIO
              L_ArrParam(7)=this.w_CHIAVE
              L_ArrParam(8)=this.w_DESBRE
              L_ArrParam(9)=this.w_DESLUN
              L_ArrParam(10)=.F.
              L_ArrParam(11)=.F.
              L_ArrParam(12)=" "
              L_ArrParam(13)=" "
              L_ArrParam(14)=this.w_GestAtt
              L_ArrParam(15)=this.w_TIPOALLE
              L_ArrParam(16)=this.w_CLASSEALL
              L_ArrParam(17)=" "
              L_ArrParam(18)=this.w_QUEMOD
              L_ArrParam(19)=this.w_OPERAZIONE
              L_ArrParam(23)=(NVL(this.w_CDPUBWEB, "N")="S")
              GSUT_BBA(this,@L_ArrParam)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
        * --- Refresh Zoom
        if this.w_IS_GSUT_GFG
          GSUT_BES(this.oParentObject,this.w_Archivio,this.w_Chiave)
        endif
      case this.w_Operazione=="V"
        * --- Visualizza Immagini
        * --- Legge da classe documentale i dati necessari
        if empty(this.oParentObject.w_GFIDMODALL) 
          * --- Read from PROMCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CDMODALL"+;
              " from "+i_cTable+" PROMCLAS where ";
                  +"CDCODCLA = "+cp_ToStrODBC(this.oParentObject.w_GFCLASDOCU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CDMODALL;
              from (i_cTable) where;
                  CDCODCLA = this.oParentObject.w_GFCLASDOCU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_IDMODALL = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_GFIDMODALL = this.w_IDMODALL
        endif
        do case
          case this.oParentObject.w_GFIDMODALL="E"
            FileEds("V", this.oParentObject.w_GFCLASDOCU, this.oParentObject.w_GFORIGFILE, alltrim(tempadhoc())+"\"+alltrim(this.oParentObject.w_GFNOMEFILE))
            this.oParentObject.w_GFPATHFILE = ADDBS(tempadhoc())
          case this.oParentObject.w_GFIDMODALL="I"
            this.oParentObject.w_GFPATHFILE = ""
            this.oParentObject.w_GFNOMEFILE = FileInfinity("V", this.oParentObject.w_GFKEYINDIC)
          otherwise
            this.oParentObject.w_GFPATHFILE = alltrim(Nvl(this.oParentObject.w_GFPATHFILE," "))
        endcase
        this.oParentObject.w_GFNOMEFILE = alltrim(this.oParentObject.w_GFNOMEFILE)
        if not empty(this.oParentObject.w_GFNOMEFILE)
          * --- Per deposito atti:
          *     se AlteregoTop, lanciato da GSAL_KDA, senza classe documentale e senza indice
          *     allora non effettua il controllo dei diritti
          if SUBSTR( CHKPECLA( this.oParentObject.w_GFCLASDOCU, i_CODUTE), 1, 1 ) = "S" OR (this.w_ISALT AND VARTYPE(this.oparentobject)="O" AND UPPER(this.oparentobject.cPrg)="GSAL_KDA" AND EMPTY(this.oParentObject.w_GFCLASDOCU) AND EMPTY(this.oParentObject.w_GFKEYINDIC))
            * --- Acquisizione nome file da ricercare
            this.w_TmpPat = Nvl(this.oParentObject.w_GFPATHFILE," ") + this.oParentObject.w_GFNOMEFILE
            if ! FILE(this.w_TmpPat) AND this.oParentObject.w_GFIDMODALL<>"I"
              ah_ErrorMsg("Allegato inesistente!",48)
              i_retcode = 'stop'
              return
            endif
            this.w_SHELLEXEC = 0
            if this.w_ISALT and this.w_Archivio="CAN_TIER"
              * --- Creo cursori segnalibri risorse interen e esterne
              this.w_OKFILE = .f.
              this.w_OKFILE = gsal_bse(.Null.,this.w_CHIAVE)
              if !this.w_OKFILE
                ah_ErrorMsg("Errore nella creazione dbf  __WORD__!",48)
                i_retcode = 'stop'
                return
              endif
            endif
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Se non � andato a buon fine, prova a verificare se il file, pur avendo l'estensione
            *     sbagliata, era di uno dei tipi conosciuti 
            if this.w_SHELLEXEC <= 32
              * --- Determina estensione file
              this.w_estensione = lower(iif(RAT(".",this.oParentObject.w_GFNOMEFILE) > 0,RIGHT(this.oParentObject.w_GFNOMEFILE,LEN(this.oParentObject.w_GFNOMEFILE) - RAT(".",this.oParentObject.w_GFNOMEFILE) + 1),""))
              if not this.w_estensione $ ".bmp|.dcx|.eps|.jpg|.jpeg|.tif|.gif"
                * --- Apre il file per vedere se riconosce il tipo ...
                this.w_Found = .T.
                * --- Acquisizione della directory corrente
                this.w_curdir = sys(5) + sys(2003)
                * --- Acquisizione path directory in cui sono situati i file da ricercare
                HANDLE=fopen(this.w_TmpPat)
                this.w_MARK=fread(HANDLE,4)
                =fclose(HANDLE)
                cd (this.w_curdir)
                * --- Si individua l'estensione del file
                do case
                  case this.w_MARK=this.w_MARK_BMP
                    this.w_estensione = ".bmp"
                  case this.w_MARK=this.w_MARK_DCX
                    this.w_estensione = ".dcx"
                  case this.w_MARK=this.w_MARK_EPS
                    this.w_estensione = ".eps"
                  case this.w_MARK=this.w_MARK_JPG
                    this.w_estensione = ".jpg"
                  case this.w_MARK=this.w_MARK_TIF
                    this.w_estensione = ".tif"
                  case this.w_MARK=this.w_MARK_GIF
                    this.w_estensione = ".gif"
                  otherwise
                    this.w_Found = .F.
                endcase
                if this.w_Found
                  * --- Richiesta di conferma rinomino
                  this.w_posiz = RAT(".",this.oParentObject.w_GFNOMEFILE)
                  this.w_estensione = upper(this.w_estensione)
                  this.w_TMPC = upper(iif(this.w_Posiz > 0,SUBSTR(this.oParentObject.w_GFNOMEFILE,1,this.w_Posiz-1), this.oParentObject.w_GFNOMEFILE )+this.w_estensione)
                  if AH_YESNO("Non � stato individuato un programma per visualizzare il file %1 %0Il file � un'immagine %2, si desidera rinominare il file in %3?","",Alltrim(this.oParentObject.w_GFNOMEFILE),upper(substr(this.w_estensione,2,3)),Alltrim(this.w_TMPC))
                    this.oParentObject.w_GFNOMEFILE = this.w_TMPC
                    this.w_TMPC = Nvl(this.oParentObject.w_GFPATHFILE," ") + this.oParentObject.w_GFNOMEFILE
                    * --- Il file viene rinominato
                    rename (this.w_TMPPAT) to (this.w_TMPC)
                    * --- Aggiorna Indice
                    * --- Write into PROMINDI
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PROMINDI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"IDNOMFIL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GFNOMEFILE),'PROMINDI','IDNOMFIL');
                      +",IDTIPFIL ="+cp_NullLink(cp_ToStrODBC(this.w_estensione),'PROMINDI','IDTIPFIL');
                          +i_ccchkf ;
                      +" where ";
                          +"IDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_GFKEYINDIC);
                             )
                    else
                      update (i_cTable) set;
                          IDNOMFIL = this.oParentObject.w_GFNOMEFILE;
                          ,IDTIPFIL = this.w_estensione;
                          &i_ccchkf. ;
                       where;
                          IDSERIAL = this.oParentObject.w_GFKEYINDIC;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                    * --- Riprova a visualizzare ...
                    this.w_TMPPAT = this.w_TMPC
                    this.w_SHELLEXEC = 0
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                endif
              endif
              if this.w_SHELLEXEC <= 32
                this.w_oMESS=createobject("AH_Message")
                this.w_oPART = this.w_oMESS.AddMsgPartNl("Non � stato possibile aprire/visualizzare il file %1")
                this.w_oPART.AddParam(alltrim(this.oParentObject.w_GFNOMEFILE))     
                this.w_oPART = this.w_oMESS.AddMsgPartNl("Se non � definito un programma per l'estensione %1  � necessario associarne uno con l'operazione di <Apri con> dalla gestione file di Windows")
                this.w_oPART.AddParam(upper(alltrim(this.w_estensione)))     
                this.w_Ok = this.w_oMESS.AH_ErrorMsg(64)
                * --- Refresh Zoom
                if this.w_IS_GSUT_GFG
                  GSUT_BES(this.oParentObject,this.w_Archivio,this.w_Chiave,,"F")
                endif
              else
                * --- Refresh Zoom
                if this.w_IS_GSUT_GFG
                  GSUT_BES(this.oParentObject,this.w_Archivio,this.w_Chiave)
                endif
              endif
            endif
          else
            ah_ErrorMsg("Non si possiedono le autorizzazioni per visualizzare il file selezionato",48)
          endif
        endif
      case INLIST(this.w_OPERAZIONE, "D", "B","F")
        if SUBSTR( CHKPECLA( this.oParentObject.w_GFCLASDOCU, i_CODUTE), 3, 1 ) = "S"
          * --- Read from CONTROPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTROPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "COPATHDO"+;
              " from "+i_cTable+" CONTROPA where ";
                  +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              COPATHDO;
              from (i_cTable) where;
                  COCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_COPATHDO = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.oParentObject.w_SELMULT
            this.w_MESS = "Confermi " + icase(this.w_Operazione=="D","eliminazione",this.w_Operazione=="F","lo sblocco","il blocco")+ " dei files selezionati?%0"+ iif(this.w_Operazione=="D","Per i collegamenti verr� eliminato solo il collegamento e non il file.","")
            if AH_YESNO(this.w_MESS,"")
              * --- Inizio elaborazione documenti selezionati
              if this.w_IS_GSUT_GFG
                this.w_CURSORE = this.w_PADRE.w_ZoomGF.cCursor
              else
                this.w_CURSORE = this.w_PADRE.w_PRATICHE.cCursor
              endif
               
 Select(this.w_CURSORE) 
 Go Top
              scan for XCHK=1
              this.w_NOMEFILE = Alltrim(GFNOMEFILE)
              this.w_KEYINDIC = alltrim(GFKEYINDIC)
              this.w_PATHFILE = alltrim(GFPATHFILE)
              this.w_CLASDOCU = alltrim(GFCLASDOCU)
              this.w_IDMODALL = IDMODALL
              this.w_IDWEBFIL = IDWEBFIL
              this.w_IDWEBAPP = IDWEBAPP
              this.Page_5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              Endscan
            endif
          else
            this.w_NOMEFILE = Alltrim(this.oParentObject.w_GFNOMEFILE)
            this.w_KEYINDIC = alltrim(this.oParentObject.w_GFKEYINDIC)
            this.w_PATHFILE = alltrim(Nvl(this.oParentObject.w_GFPATHFILE," "))
            this.w_CLASDOCU = ALLTRIM(this.oParentObject.w_GFCLASDOCU)
            this.w_IDMODALL = Alltrim(this.oParentObject.w_GFIDMODALL)
            this.w_IDWEBFIL = Alltrim(this.oParentObject.w_GFIDWEBFIL)
            this.w_IDWEBAPP = Alltrim(this.oParentObject.w_GFIDWEBAPP)
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Refresh Zoom
          if this.w_IS_GSUT_GFG
            GSUT_BES(this.oParentObject,this.w_Archivio,this.w_Chiave)
          else
            this.w_PADRE.NotifyEvent("Aggiorna")     
          endif
           
 this.oParentObject.NotifyEvent("Ricerca")
        else
          ah_ErrorMsg("Non si possiedono le autorizzazioni per eliminare il file/s selezionato/i",48)
        endif
      case this.w_Operazione=="O"
        if CHKPECLA( this.oParentObject.w_GFCLASDOCU, i_CODUTE) = "SSS"
          this.w_TmpPat = Nvl(this.oParentObject.w_GFPATHFILE," ")
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          ah_ErrorMsg("Non si possiedono le autorizzazioni per eseguire l'operazione richiesta",48)
        endif
      case this.w_Operazione == "I"
        if not empty(this.oParentObject.w_GFKEYINDIC)
          this.w_IDSERIAL = this.oParentObject.w_GFKEYINDIC
          this.w_GSUT_AID = GSUT_AID()
          this.w_GSUT_AID.w_IDSERIAL = this.w_IDSERIAL
          this.w_GSUT_AID.QueryKeySet("IDSERIAL="+chr(39)+this.w_IDSERIAL+chr(39) ,"")     
          this.w_GSUT_AID.LoadRecWarn()     
          this.w_GSUT_AID = .null.
        endif
      case this.w_Operazione=="E"
        * --- Richiede la cartella dove esportare il file selezionato
        this.w_DesPat = CP_GETDIR()
        if this.oParentObject.w_SELMULT
          * --- Inizio elaborazione documenti selezionati
          if this.w_IS_GSUT_GFG
            this.w_CURSORE = this.w_PADRE.w_ZoomGF.cCursor
          else
            this.w_CURSORE = this.w_PADRE.w_PRATICHE.cCursor
          endif
           
 Select(this.w_CURSORE) 
 Go Top
          scan for XCHK=1
          this.w_NOMEFILE = Alltrim(GFNOMEFILE)
          this.w_PATHFILE = alltrim(GFPATHFILE)
          this.w_ORIGFILE = alltrim(GFORIGFILE)
          this.w_KEYINDIC = alltrim(GFKEYINDIC)
          this.w_CLASDOCU = alltrim(GFCLASDOCU)
          this.w_IDMODALL = alltrim(IDMODALL)
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          Endscan
          * --- Fine elaborazione documenti  selezionati
           
 Select(this.w_CURSORE) 
 Go Top
          if this.w_IS_GSUT_GFG
            this.oParentObject.w_ZoomGF.Refresh()
          else
            this.oParentObject.w_PRATICHE.Refresh()
          endif
        else
          * --- Esporta file selezionato
          this.w_NOMEFILE = alltrim(this.oParentObject.w_GFNOMEFILE)
          this.w_PATHFILE = alltrim(Nvl(this.oParentObject.w_GFPATHFILE," "))
          this.w_ORIGFILE = alltrim(this.oParentObject.w_GFORIGFILE)
          this.w_KEYINDIC = alltrim(this.oParentObject.w_GFKEYINDIC)
          this.w_CLASDOCU = ALLTRIM(this.oParentObject.w_GFCLASDOCU)
          this.w_IDMODALL = alltrim(this.oParentObject.w_GFIDMODALL)
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.w_Operazione=="M" or this.w_Operazione=="G"
        * --- Invio e-mail
        if this.w_Operazione == "G"
          * --- Generazione PDF ed invio Email
          if this.w_ISALT and this.w_AETYPEAL <>"P"
            DIMENSION ARRVOCI(2)
             
 ARRVOCI[1]="Invia PDF" 
 ARRVOCI[2]="Archivia PDF"
            this.w_RETVAL = Addmenu(@ARRVOCI)
          else
            this.w_RETVAL = 1
          endif
          if this.w_RETVAL<=0
            i_retcode = 'stop'
            return
          endif
        else
          this.w_RETVAL = 3
        endif
        if this.w_Operazione == "M" and this.w_ISALT
           DIMENSION L_INFOARCH(10) 
 L_INFOARCH(1) = iif(this.w_OKINDICE,"CAN_TIER","") 
 L_INFOARCH(2) =this.oparentobject.w_CODPRAT 
 L_INFOARCH(3) = this.w_AEPATHFI 
 L_INFOARCH(4) = this.w_AEORIFIL 
 L_INFOARCH(5) = this.w_AEOGGETT 
 L_INFOARCH(6) = this.w_AE_TESTO 
 L_INFOARCH(7) = this.w_TIPOALL 
 L_INFOARCH(8) = this.w_CLASALL 
 L_INFOARCH(9) = this.w_AEDESPRA
        endif
         
 Dimension ARRAYALLEG[1] 
 NumAlleg=1 
 ARRAYALLEG(NumAlleg) = " "
        if this.w_RETVAL=3
           
 i_EMAILSUBJECT = "Invio allegato "
          if this.w_ISALT
            gspr_boe(this,i_CODUTE,this.oparentobject.w_CODPRAT)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
           
 i_EMAILSUBJECT = "Invio allegati "
          if !this.w_ISALT
            AH_ERRORMSG("La generazione del pdf andr� a buon fine solo per i file con estensione compatibile",48,"")
          endif
        endif
        if this.w_IS_GSUT_GFG
          this.w_CURSORE = this.w_PADRE.w_ZoomGF.cCursor
        else
          this.w_CURSORE = this.w_PADRE.w_PRATICHE.cCursor
        endif
        Select(this.w_CURSORE)
        if this.oParentObject.w_SELMULT
          * --- Inizio elaborazione documenti selezionati
           
 Go Top 
 scan for XCHK=1
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
           
 Endscan 
 Select(this.w_CURSORE) 
 Go Top
        else
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Lancia email
        this.w_RET = .F.
        if this.w_RETVAL=3 OR (NumAlleg>1 )
          if this.w_RETVAL<>2
            * Variabili publiche per mail 
 i_EMAIL = " " 
 i_EMAILTEXT = " "
            if this.w_bPEC
              * --- Memorizzo le var. globali della configurazione mail
              this.w_RET = ("OK" = CFGMAIL(i_CODUTE,"PUSH_CFG"))
              this.w_RET = not Empty(CFGMAIL(i_CODUTE,"P", "", .T.))
              this.w_RET = this.w_RET and i_bPEC
              if this.w_RET
                this.w_RET = EMAIL( @ARRAYALLEG , g_UEDIAL, .F., "", i_EMAILSUBJECT, g_UEFIRM, .F., .F., @L_INFOARCH)
              else
                ah_Errormsg("Configurazione PEC utente non valida")
              endif
              * --- ripristino le var. globali
              CFGMAIL(i_CODUTE,"POP_CFG")
            else
              this.w_RET = EMAIL( @ARRAYALLEG , g_UEDIAL, .F., "", i_EMAILSUBJECT, g_UEFIRM, .F., .F., @L_INFOARCH)
            endif
            * Variabili publiche per mail 
 i_EMAIL = " " 
 i_EMAILSUBJECT = " " 
 i_EMAILTEXT = " "
            release L_INFOARCH
            if not this.w_RET
              if this.w_RETVAL=3
                ah_ErrorMSG("Impossibile inviare via email il documento selezionato")
              else
                ah_ErrorMSG("Impossibile inviare via email i documenti generati (.pdf)")
              endif
            endif
          endif
          if this.w_ISALT
            * --- Read from PAR_ALTE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PATIPARC"+;
                " from "+i_cTable+" PAR_ALTE where ";
                    +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PATIPARC;
                from (i_cTable) where;
                    PACODAZI = i_CODAZI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_LMODALL = NVL(cp_ToDate(_read_.PATIPARC),cp_NullValue(_read_.PATIPARC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.w_LMODALL = this.oParentObject.w_CDMODALL
          endif
          * --- Cancella il file ...
          if this.w_LMODALL="F"
             
 w_ErrorHandler = on("ERROR") 
 on error w_ERRORE = .T. 
 w_ERRORE = .F. 
 For i=1 to alen(ARRAYALLEG) 
 HANDLE=Fopen(ARRAYALLEG(i)) 
 Fclose(HANDLE) 
 ERASE (ARRAYALLEG(i))
             
 Next 
 on error &w_ErrorHandler
          endif
          * --- Fine elaborazione documenti selezionati
        endif
        if this.w_IS_GSUT_GFG
          this.oParentObject.w_ZoomGF.Refresh()
        else
          this.oParentObject.w_PRATICHE.Refresh()
        endif
        this.oParentObject.NotifyEvent("Ricerca")
      case this.w_Operazione=="N"
        * --- Nuovo Documento
        * --- Per l'operazione Nuovo Documento, determina la classe alla quale associare il file
        * --- Read from PROMCLAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CDCODCLA,CDMODALL,CDPUBWEB"+;
            " from "+i_cTable+" PROMCLAS where ";
                +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                +" and CDRIFDEF = "+cp_ToStrODBC("S");
                +" and CDMODALL = "+cp_ToStrODBC("F");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CDCODCLA,CDMODALL,CDPUBWEB;
            from (i_cTable) where;
                CDRIFTAB = this.w_ARCHIVIO;
                and CDRIFDEF = "S";
                and CDMODALL = "F";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
          this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
          this.w_CDPUBWEB = NVL(cp_ToDate(_read_.CDPUBWEB),cp_NullValue(_read_.CDPUBWEB))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if empty(this.w_LICLADOC)
          * --- Verifica se c'� almeno una classe
          * --- Read from PROMCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" PROMCLAS where ";
                  +"CDRIFTAB = "+cp_ToStrODBC(this.w_ARCHIVIO);
                  +" and CDMODALL = "+cp_ToStrODBC("F");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CDRIFTAB = this.w_ARCHIVIO;
                  and CDMODALL = "F";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows=0
            AH_ERRORMSG("Non sono state definite classi documentali per la tabella %1",48,"",alltrim(this.w_ARCHIVIO))
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Prepariamo le variabili per eventuali filtri sui modelli documenti
        if this.w_ISALT
          if VARTYPE(this.oParentObject.w_CN__ENTE)="C"
            this.w_CN__ENTE = this.oParentObject.w_CN__ENTE
          endif
          if VARTYPE(this.oParentObject.w_CNTIPPRA)="C"
            this.w_CNTIPPRA = this.oParentObject.w_CNTIPPRA
          endif
          if VARTYPE(this.oParentObject.w_CNTIPPRA)="C"
            this.w_CNMATPRA = this.oParentObject.w_CNMATPRA
          endif
        endif
        * --- Lancia la maschera Modelli Documenti
        do GSUT_KMD with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_LICLADOC = this.w_CLAMOD
        this.w_PATARC = addbs(alltrim(this.w_PATARC))+alltrim(this.w_FILEORIG)
        this.w_CATCH = this.w_PATMOD
        if not empty(this.w_CATCH)
          * --- Se da cartella contenitore (utilizabile solo se non si archivia da scanner) archivia tutti i files presenti nella cartella ...
          this.w_CATCH = left(alltrim(this.w_CATCH),250)
          * --- Archiviazione standard singola ...
          if FILE(this.w_CATCH)
            Private L_ArrParam
            dimension L_ArrParam(30)
            L_ArrParam(1)="AR"
            L_ArrParam(2)=this.w_CATCH
            L_ArrParam(3)=this.w_LICLADOC
            L_ArrParam(4)=i_CODUTE
            L_ArrParam(5)=this.oParentObject
            L_ArrParam(6)=this.w_ARCHIVIO
            L_ArrParam(7)=this.w_CHIAVE
            L_ArrParam(8)=this.w_OGGETTO
            L_ArrParam(9)=this.w_TESTO
            L_ArrParam(10)=.F.
            L_ArrParam(11)=.F.
            L_ArrParam(12)="D"
            L_ArrParam(13)=" "
            L_ArrParam(14)=this.w_GestAtt
            L_ArrParam(15)=this.w_TIPOALLE
            L_ArrParam(16)=this.w_CLASSEALL
            L_ArrParam(17)=" "
            L_ArrParam(18)=this.w_QUEMOD
            L_ArrParam(19)=this.w_OPERAZIONE
            L_ArrParam(22)=this.w_PATARC
            L_ArrParam(23)=(NVL(this.w_CDPUBWEB, "N")="S")
            GSUT_BBA(this,@L_ArrParam)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        * --- Aggiorno l'icona degli allegati sul form
        if type("this.oParentObject.BaseClass")="C" and lower(this.oParentObject.BaseClass) = "form"
          this.OparentObject.LoadAttach()
        endif
        * --- Refresh Zoom
        if this.w_IS_GSUT_GFG
          this.oParentObject.NotifyEvent("Ricerca") 
 GSUT_BES(this.oParentObject,this.w_Archivio,this.w_Chiave)
        endif
      case this.w_Operazione=="U" OR this.w_Operazione=="P"
        * --- Duplica Documento
        * --- Per l'operazione Duplica Documento, la classe alla quale associare il file � letta dalla riga dello zoom
        this.w_LICLADOC = this.oParentObject.w_GFCLASDOCU
        if empty(this.w_LICLADOC)
          * --- Non esiste una classe
          if this.w_Operazione="U"
            * --- Da duplicazione pratiche NON deve segnalare nessun messaggio dato che siamo sotto transazione
            AH_ERRORMSG("Non sono state definite classi documentali per la tabella %1",48,"",alltrim(this.w_ARCHIVIO))
          endif
          i_retcode = 'stop'
          return
        endif
        if this.w_Operazione="U"
          * --- Duplicazione documento
          if NOT AH_YESNO("Confermi duplicazione del documento selezionato?")
            i_retcode = 'stop'
            return
          endif
        endif
        if this.w_ISALT AND Alltrim(this.w_ARCHIVIO)=="CAN_TIER" and (this.w_IS_GSUT_GFG or lower(this.w_PADRE.class) = "tgsut_kvt")
          if NOT AH_YesNo("Si vuole duplicare il documento sulla stessa pratica?")
            * --- Mschera di selezione pratica di destinazione
            this.w_OLDCODPRAT = this.oParentObject.w_CODPRAT
            do GSPR_KSP with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if Empty(this.w_CODICEPRAT)
              Ah_ErrorMsg("Operazione annullata",48,"")
              i_retcode = 'stop'
              return
            endif
            this.oParentObject.w_CODPRAT = this.w_CODICEPRAT
            this.w_Chiave = this.w_CODICEPRAT
            if this.w_IS_GSUT_GFG 
              this.w_ROWSEARCH = this.w_PADRE.GSUT_MA1.Search("upper(NVL(t_IDCODATT, ' '))= 'CODICE' AND NOT DELETED()")
              if this.w_ROWSEARCH <> -1
                this.w_PADRE.GSUT_MA1.setrow(this.w_ROWSEARCH)     
                this.w_PADRE.GSUT_MA1.set("w_IDVALATT",this.w_CODICEPRAT)     
              endif
              this.w_ROWSEARCH = this.w_PADRE.GSUT_MA1.Search("upper(NVL(t_IDCODATT, ' '))= 'DESCRIZIONE' AND NOT DELETED()")
              if this.w_ROWSEARCH <> -1
                this.w_PADRE.GSUT_MA1.setrow(this.w_ROWSEARCH)     
                this.w_PADRE.GSUT_MA1.set("w_IDVALATT",this.w_DESCPRAT)     
              endif
            else
              this.oParentObject.w_CNCODCAN = this.w_CODICEPRAT
              this.oParentObject.w_CNDESCAN = this.w_DESCPRAT
            endif
          endif
        endif
        * --- Descrizione letta dalla riga dello zoom
        this.w_OGGETTO = "Copia di "+ ALLTRIM(this.oParentObject.w_GFDESCFILE)
        * --- Chiave indice letta dalla riga dello zoom
        this.w_IDSERIAL = this.oParentObject.w_GFKEYINDIC
        * --- Lettura Note aggiuntive Tipologia e Classe allegato
        * --- Read from PROMINDI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ID_TESTO,IDTIPALL,IDCLAALL"+;
            " from "+i_cTable+" PROMINDI where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ID_TESTO,IDTIPALL,IDCLAALL;
            from (i_cTable) where;
                IDSERIAL = this.w_IDSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TESTO = NVL(cp_ToDate(_read_.ID_TESTO),cp_NullValue(_read_.ID_TESTO))
          this.w_TIPOALLE = NVL(cp_ToDate(_read_.IDTIPALL),cp_NullValue(_read_.IDTIPALL))
          this.w_CLASSEALL = NVL(cp_ToDate(_read_.IDCLAALL),cp_NullValue(_read_.IDCLAALL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Per copiare � sufficiente il path relativo
        this.w_CATCH = ALLTRIM(Nvl(this.oParentObject.w_GFPATHFILE," "))+ALLTRIM(this.oParentObject.w_GFNOMEFILE)
        if not empty(this.w_CATCH)
          * --- Se da cartella contenitore (utilizabile solo se non si archivia da scanner) archivia tutti i files presenti nella cartella ...
          * --- Archiviazione standard singola ...
          if FILE(this.w_CATCH)
            Private L_ArrParam
            dimension L_ArrParam(30)
            L_ArrParam(1)="AR"
            L_ArrParam(2)=this.w_CATCH
            L_ArrParam(3)=this.w_LICLADOC
            L_ArrParam(4)=i_CODUTE
            L_ArrParam(5)=this.oParentObject
            L_ArrParam(6)=this.w_ARCHIVIO
            L_ArrParam(7)=this.w_CHIAVE
            L_ArrParam(8)=this.w_OGGETTO
            L_ArrParam(9)=this.w_TESTO
            if this.w_Operazione="P"
              * --- Da duplicazione pratiche - aggiungiamo i parametri SILENTE e pNoVisMID
              this.oParentObject.w_CODPRAT = This.oparentobject.oparentobject.w_CodDest
              L_ArrParam(10)=.T.
              L_ArrParam(11)=.T.
            else
              * --- Duplicazione documento
              L_ArrParam(10)=.F.
              L_ArrParam(11)=.F.
            endif
            L_ArrParam(12)=" "
            L_ArrParam(13)=" "
            L_ArrParam(14)=this.w_GestAtt
            L_ArrParam(15)=this.w_TIPOALLE
            L_ArrParam(16)=this.w_CLASSEALL
            L_ArrParam(17)=" "
            L_ArrParam(18)=this.w_QUEMOD
            L_ArrParam(19)=this.w_OPERAZIONE
            GSUT_BBA(this,@L_ArrParam)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        * --- Aggiorno l'icona degli allegati sul form
        if this.w_Operazione="U" AND type("this.oParentObject.BaseClass")="C" and lower(this.oParentObject.BaseClass) = "form"
          this.OparentObject.LoadAttach()
        endif
        if this.w_Operazione="U"
          * --- Refresh Zoom
          if this.w_ISALT AND this.w_ARCHIVIO=="CAN_TIER" and this.w_IS_GSUT_GFG and Not Empty(this.w_OLDCODPRAT)
            this.oParentObject.w_CODPRAT = this.w_OLDCODPRAT
            this.w_Chiave = this.oParentObject.w_CODPRAT
            if this.w_IS_GSUT_GFG
              this.w_ROWSEARCH = this.w_PADRE.GSUT_MA1.Search("upper(NVL(t_IDCODATT, ' '))= 'CODICE' AND NOT DELETED()")
              if this.w_ROWSEARCH <> -1
                this.w_PADRE.GSUT_MA1.setrow(this.w_ROWSEARCH)     
                this.w_PADRE.GSUT_MA1.set("w_IDVALATT",this.oParentObject.w_CODPRAT)     
              endif
              this.w_ROWSEARCH = this.w_PADRE.GSUT_MA1.Search("upper(NVL(t_IDCODATT, ' '))= 'DESCRIZIONE' AND NOT DELETED()")
              if this.w_ROWSEARCH <> -1
                this.w_PADRE.GSUT_MA1.setrow(this.w_ROWSEARCH)     
                this.w_PADRE.GSUT_MA1.set("w_IDVALATT",this.oParentObject.w_DESPRAT)     
              endif
            else
              this.oParentObject.w_CNCODCAN = this.oParentObject.w_CODPRAT
              this.oParentObject.w_CNDESCAN = this.oParentObject.w_DESPRAT
            endif
          endif
          if this.w_IS_GSUT_GFG
            GSUT_BES(this.oParentObject,this.w_Archivio,this.w_Chiave)
            this.oParentObject.NotifyEvent("Ricerca")
          endif
        endif
      case this.w_Operazione=="Z"
        this.w_FL_SAVE = .F.
        this.w_FILEDEST = FORCEEXT(TTOC(DATETIME(),1)+"_"+alltrim(str(i_Codute)),"7z")
        this.w_IDUTEEXP = i_codute
        this.w_IDDATZIP = Datetime()
        do gsut_kez with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_FL_SAVE
          * --- Path temporaneo per copia file zip
          this.w_PATHDEST = ADDBS(tempadhoc())+SYS(2015)
          this.w_ERRLOG = .F.
          this.w_MskMESS = GSUT_KEL()
          addmsgnl("Inizio elaborazione %1 %2", this.w_MskMESS, DTOC(DATE()), LEFT(TTOC(DATETIME(),2),8))
          * --- Verifico i file da esportare e li copio nella cartella temporanea
          addmsgnl("Preparazione cartelle temporanee e copia file", this.w_MskMESS)
          if this.oParentObject.w_SELMULT
            * --- Inizio elaborazione documenti selezionati
            if this.w_IS_GSUT_GFG
              this.w_CURSORE = this.w_PADRE.w_ZoomGF.cCursor
            else
              this.w_CURSORE = this.w_PADRE.w_PRATICHE.cCursor
            endif
             
 Select(this.w_CURSORE) 
 Go Top
            scan for XCHK=1
            this.w_NOMEFILE = Alltrim(GFNOMEFILE)
            this.w_PATHFILE = FULLPATH(alltrim(GFPATHFILE))
            this.w_ORIGFILE = alltrim(GFORIGFILE)
            this.w_CLASDOCU = alltrim(GFCLASDOCU)
            this.w_DESCRFILE = Alltrim(GFDESCFILE)
            this.w_KEYINDIC = ALLTRIM(GFKEYINDIC)
            this.w_PATHATT = STRTRAN(ALLTRIM(STRTRAN(this.w_PATHFILE, JUSTDRIVE(this.w_PATHFILE),"")),"..\","")
            * --- Se il documento di origine era semplicemente in "Allegati" dobbiamo fare in modo che sia una sottocartella di w_PATHDEST e quindi mettiamo come primo carattere '\'
            if !LEFT(this.w_PATHATT,1)=="\"
              this.w_PATHATT = "\"+this.w_PATHATT
            endif
            addmsgnl("%1Creazione cartella %2", this.w_MskMESS, CHR(9), this.w_PATHDEST+this.w_PATHATT)
            if !makedir(this.w_PATHDEST+this.w_PATHATT)
              addmsgnl("Errore creazione percorso d'esportazione. Impossibile proseguire", this.w_MskMESS )
              this.w_FL_SAVE = .F.
              this.w_ERRLOG = .T.
              exit
            else
              * --- Copia file in dir di destinazione
              if cp_fileexist( ALLTRIM(ADDBS(this.w_PATHFILE))+ALLTRIM(this.w_NOMEFILE) )
                addmsgnl("%1Copia file %2", this.w_MskMESS, CHR(9), ALLTRIM(ADDBS(this.w_PATHFILE))+ALLTRIM(this.w_NOMEFILE))
                COPY FILE (ALLTRIM(ADDBS(this.w_PATHFILE))+ALLTRIM(this.w_NOMEFILE)) TO (ADDBS(this.w_PATHDEST+this.w_PATHATT)+ALLTRIM(this.w_NOMEFILE))
              else
                this.w_ERRLOG = .T.
                addmsgnl("%1File %2 non esistente", this.w_MskMESS, CHR(9), ALLTRIM(ADDBS(this.w_PATHFILE))+ALLTRIM(this.w_NOMEFILE))
              endif
            endif
            Endscan
            * --- Fine elaborazione documenti  selezionati
             
 Select(this.w_CURSORE) 
 Go Top
          else
            * --- Operazione su documento attivo (record attivo)
            this.w_NOMEFILE = alltrim(this.oParentObject.w_GFNOMEFILE)
            this.w_PATHFILE = alltrim(Nvl(this.oParentObject.w_GFPATHFILE," "))
            this.w_ORIGFILE = alltrim(this.oParentObject.w_GFORIGFILE)
            this.w_CLASDOCU = ALLTRIM(this.oParentObject.w_GFCLASDOCU)
            this.w_DESCRFILE = Alltrim(this.oParentObject.w_GFDESCFILE)
            this.w_KEYINDIC = ALLTRIM(this.oParentObject.w_GFKEYINDIC)
            this.w_PATHATT = STRTRAN(this.w_PATHFILE, JUSTDRIVE(this.w_PATHFILE),"")
            * --- Se il documento di origine era semplicemente in "Allegati" dobbiamo fare in modo che sia una sottocartella di w_PATHDEST e quindi mettiamo come primo carattere '\'
            if !LEFT(this.w_PATHATT,1)=="\"
              this.w_PATHATT = "\"+this.w_PATHATT
            endif
            addmsgnl("%1Creazione cartella %2", this.w_MskMESS, CHR(9), this.w_PATHDEST+this.w_PATHATT)
            if !makedir(this.w_PATHDEST+this.w_PATHATT)
              addmsgnl("Errore creazione percorso d'esportazione. Impossibile proseguire", this.w_MskMESS )
              this.w_FL_SAVE = .F.
              this.w_ERRLOG = .T.
            else
              addmsgnl("%1Copia file %2", this.w_MskMESS, CHR(9), ALLTRIM(ADDBS(this.w_PATHFILE))+ALLTRIM(this.w_NOMEFILE))
              COPY FILE (ALLTRIM(ADDBS(this.w_PATHFILE))+ALLTRIM(this.w_NOMEFILE)) TO (ADDBS(this.w_PATHDEST+this.w_PATHATT)+ALLTRIM(this.w_NOMEFILE))
            endif
          endif
          if this.w_FL_SAVE
            * --- Creazione zip per esportazione
            public L_RETMSG
            L_RETMSG = ""
            addmsgnl("Creazione archivio compresso %1", this.w_MskMESS, this.w_FILEDEST )
            if zipunzip("Z", this.w_FILEDEST, ADDBS(this.w_PATHDEST), "", @L_RETMSG)
              * --- File zip creato, imposto negli indici interessati i valori d'esportazione
              addmsgnl("Inizio aggiornamento indici", this.w_MskMESS )
              * --- Try
              local bErr_05175288
              bErr_05175288=bTrsErr
              this.Try_05175288()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
                addmsgnl("Errore aggiornamento indici%0Cancellazione archivio compresso%0Operazione annullata", this.w_MskMESS )
                this.w_ERRLOG = .T.
                * --- Errore aggiornamento, elimino archivio zip
                DELETE FILE (this.w_FILEDEST)
              endif
              bTrsErr=bTrsErr or bErr_05175288
              * --- End
            else
              this.w_ERRLOG = .T.
              addmsgnl("Errore creazione archivio compresso %1", this.w_MskMESS, L_RETMSG )
              * --- Elimino cartella temporanea
              if !deletefolder(this.w_PATHDEST, this.w_RETMSG)
                addmsgnl("Errore eliminazione cartella temporanea %1", this.w_MskMESS, this.w_RETMSG )
                this.w_ERRLOG = .T.
              endif
            endif
          endif
          if this.w_ERRLOG
            addmsgnl("ATTENZIONE! Procedura terminata con errori", this.w_MskMESS )
          else
            this.w_MskMESS.ecpQuit()     
          endif
          this.w_MskMESS = .NULL.
          this.oParentObject.NotifyEvent("Interroga")
        else
          ah_Errormsg("Operazione interrotta come richiesto", 64)
        endif
      case this.w_Operazione=="R"
        * --- Read from PAR_ALTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PAVERDOC,PADATZIP"+;
            " from "+i_cTable+" PAR_ALTE where ";
                +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PAVERDOC,PADATZIP;
            from (i_cTable) where;
                PACODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PAVERDOC = NVL(cp_ToDate(_read_.PAVERDOC),cp_NullValue(_read_.PAVERDOC))
          this.w_PADATZIP = NVL(cp_ToDate(_read_.PADATZIP),cp_NullValue(_read_.PADATZIP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PAVERDOC = IIF(EMPTY(NVL(this.w_PAVERDOC, " ")), "N", this.w_PAVERDOC )
        this.w_FILEDEST = ""
        this.w_IDUTEEXP = IIF(EMPTY(this.oParentObject.w_SELUTEEXP), i_codute, this.oParentObject.w_SELUTEEXP)
        this.w_FLSUBFLD = .F.
        this.w_FLTYPIMP = "C"
        this.w_FL_SAVE = .F.
        do gsut_kiz with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_FL_SAVE
          this.w_ERRLOG = .F.
          this.w_MskMESS = GSUT_KEL()
          addmsgnl("Inizio elaborazione %1 %2", this.w_MskMESS, DTOC(DATE()), LEFT(TTOC(DATETIME(),2),8))
          * --- Verifica presenza cartelle/file da importare
          addmsgnl("Verifica presenza cartella da analizzare o file da importare %1", this.w_MskMESS, this.w_FILEDEST)
          if cp_FileExist( IIF(this.w_FLTYPIMP="C", JUSTPATH(ADDBS(this.w_FILEDEST)), this.w_FILEDEST ) )
            DIMENSION L_ZipToImport(1)
            L_ZipToImport(1)=""
            if this.w_FLTYPIMP="C"
              DIMENSION L_VisitaDir(1)
              if this.w_FLSUBFLD
                addmsgnl("Analisi file archivio compressi in struttura cartella %1", this.w_MskMESS, ADDBS(ALLTRIM(this.w_FILEDEST)))
                visitadir( ADDBS(ALLTRIM(this.w_FILEDEST)), "*.7z", @L_VisitaDir, .T.)
              else
                Private L_OldPath
                L_OldPath = Sys(5)+Sys(2003)
                private L_SearchDefa
                L_SearchDefa = "'"+ADDBS(ALLTRIM( this.w_FILEDEST ))+"'"
                addmsgnl("Analisi file archivio compressi in cartella %1", this.w_MskMESS, L_SearchDefa)
                Set Default To &L_SearchDefa
                ADIR(L_VisitaDir,"*.7z")
                Set Default to &L_OldPath
              endif
              this.w_TMPN = 1
              do while this.w_TMPN<=ALEN(L_VisitaDir,1)
                * --- Verifico se lo zip rilevato � conosciuto altrimenti lo escludo
                if VARTYPE(L_VisitaDir(this.w_TMPN, 1 ))="C" AND SUBSTR(L_VisitaDir(this.w_TMPN, 5 ), 5,1)<>"D"
                  this.w_ZIPNAME = JUSTFNAME( L_VisitaDir(this.w_TMPN, 1 ) )
                  this.w_IDDATZIP = L_VisitaDir(this.w_TMPN, 3 ) 
                  addmsg("Verifica presenza file archivio compresso %1 in indici...", this.w_MskMESS, this.w_ZIPNAME)
                  this.w_posiz = 0
                  * --- Select from gsut_kiz
                  do vq_exec with 'gsut_kiz',this,'_Curs_gsut_kiz','',.f.,.t.
                  if used('_Curs_gsut_kiz')
                    select _Curs_gsut_kiz
                    locate for 1=1
                    do while not(eof())
                    if Nvl(_Curs_gsut_kiz.IDUTEEXP,0) = this.w_IDUTEEXP
                      this.w_posiz = NVL( _Curs_gsut_kiz.FILEOK , 0 )
                    else
                      this.w_posiz = -1
                    endif
                      select _Curs_gsut_kiz
                      continue
                    enddo
                    use
                  endif
                  do case
                    case this.w_posiz=1
                      if !EMPTY( L_ZipToImport( ALEN( L_ZipToImport, 1 ) ) )
                        DIMENSION L_ZipToImport( ALEN( L_ZipToImport, 1 ) + 1 )
                      endif
                      L_ZipToImport( ALEN( L_ZipToImport, 1 ) ) = IIF(this.w_FLSUBFLD, "", ADDBS(ALLTRIM(this.w_FILEDEST)) )+L_VisitaDir(this.w_TMPN, 1)
                      addmsgnl("rilevato, memorizzazione... ", this.w_MskMESS )
                    case this.w_posiz= -1
                      addmsgnl("file rilevato ma esportato da un altro utente", this.w_MskMESS )
                      this.w_posiz = 0
                    otherwise
                      addmsgnl("non rilevato, ignorato", this.w_MskMESS )
                  endcase
                endif
                this.w_TMPN = this.w_TMPN + 1
              enddo
            else
              if this.w_PADATZIP="S"
                private L_SearchDefa
                L_SearchDefa = '"'+ADDBS(ALLTRIM( justpath(this.w_FILEDEST) ))+'"'
                 
 L_OldPath = Sys(5)+Sys(2003) 
 cd (L_SearchDefa )
                this.w_NUMFIL = ADIR(L_VisitaDir,this.w_FILEDEST ) 
                 
 cd (L_OldPath)
                if this.w_NUMFIL>0
                  this.w_IDDATZIP = cp_chartodatetime(dtoc(l_visitadir(3)) +" "+ l_visitadir(4))
                endif
              endif
              L_ZipToImport(1) = this.w_FILEDEST
              addmsgnl("File archivio compresso %1 da importare, memorizzazione... ", this.w_MskMESS, L_ZipToImport( 1 ) )
            endif
            this.w_TMPN = 1
            addmsgnl("Inizio analisi archivi compressi memorizzati...", this.w_MskMESS)
            do while this.w_TMPN <= ALEN( L_ZipToImport, 1 )
              if !EMPTY(L_ZipToImport(this.w_TMPN)) and cp_FileExist(L_ZipToImport(this.w_TMPN))
                * --- Scompatto lo zip nella cartella temporanea per la successiva analisi
                this.w_PATHDEST = ADDBS(tempadhoc())+SYS(2015)
                public L_RETMSG
                L_RETMSG = ""
                addmsgnl("Scompattazione archivio compresso %1 in cartella temporanea", this.w_MskMESS, L_ZipToImport(this.w_TMPN))
                if zipunzip("U", L_ZipToImport(this.w_TMPN), ADDBS(this.w_PATHDEST), "", @L_RETMSG)
                  * --- Prelevo dall'archivio la lista dei file archiviati
                  addmsgnl("Analisi file presenti in archivio compresso %1 per memorizzazione", this.w_MskMESS, L_ZipToImport(this.w_TMPN))
                  if zipunzip("L", L_ZipToImport(this.w_TMPN), "", "", @L_RETMSG)
                    * --- Suddivido il messaggio in un array per poi analizzarlo
                    ALINES(L_FilesList, L_RETMSG,4+8,CHR(10))
                    DIMENSION L_ExportedFiles(1)
                    L_ExportedFiles(1)=""
                    this.w_posiz = 1
                    do while this.w_Posiz <= ALEN(L_FilesList,1)
                      if VARTYPE(L_FilesList(this.w_Posiz))="C"
                        this.w_TMPC = ALLTRIM( L_FilesList(this.w_Posiz) )
                        if VAL( LEFT( this.w_TMPC, AT( " ", this.w_TMPC ) -1 ) )>0
                          * --- Non � una cartella quindi aggiungo il file alla lista
                          if !EMPTY( L_ExportedFiles( ALEN( L_ExportedFiles, 1 ) ) )
                            DIMENSION L_ExportedFiles( ALEN( L_ExportedFiles, 1 ) + 1)
                          endif
                          L_ExportedFiles( ALEN( L_ExportedFiles, 1 ) ) = SUBSTR( this.w_TMPC, AT( " ", this.w_TMPC ) + 1 )
                          addmsgnl("Memorizzazione file esportato %1", this.w_MskMESS, L_ExportedFiles( ALEN( L_ExportedFiles, 1 ) ) )
                        endif
                      endif
                      this.w_posiz = this.w_posiz + 1
                    enddo
                    * --- Ho lo zip scompattato e la lista dei file da cercare, 
                    *     verifico che associato all'utente indicato sia presente l'esportazione
                    *     del file corrispondente, nel caso sovrascrivo il file e aggiorno l'indice
                    this.w_posiz = 1
                    addmsgnl("Inizio verifiche file esportati e relativo proprietario d'esportazione in indici", this.w_MskMESS )
                    do while this.w_Posiz <= ALEN(L_ExportedFiles,1)
                      this.w_PATHFILE = ADDBS( ALLTRIM( JUSTPATH (L_ExportedFiles(this.w_Posiz) ) ) )
                      this.w_ZIPNAME = ALLTRIM( JUSTFNAME( L_ExportedFiles(this.w_Posiz) ) )
                      this.w_IDSERIAL = ""
                      addmsg("%1Analisi file %2%3... ", this.w_MskMESS, CHR(9), this.w_PATHFILE, this.w_ZIPNAME )
                      this.w_NUMIDXRES = 0
                      * --- Select from gsut1kiz
                      do vq_exec with 'gsut1kiz',this,'_Curs_gsut1kiz','',.f.,.t.
                      if used('_Curs_gsut1kiz')
                        select _Curs_gsut1kiz
                        locate for 1=1
                        do while not(eof())
                        this.w_IDSERIAL = _Curs_gsut1kiz.IDSERIAL
                        this.w_TIPOALLE = _Curs_gsut1kiz.IDMODALL
                        this.w_AEPATHFI = _Curs_gsut1kiz.IDPATALL
                        this.w_FILE = _Curs_gsut1kiz.IDNOMFIL
                        this.w_ORIGFILE = _Curs_gsut1kiz.IDORIFIL
                        this.w_NEWOBJ = ALLTRIM( _Curs_gsut1kiz.IDOGGETT )
                        this.w_NUMIDXRES = this.w_NUMIDXRES + 1
                        this.w_DATZIP = _Curs_gsut1kiz.IDDATZIP
                          select _Curs_gsut1kiz
                          continue
                        enddo
                        use
                      endif
                      if EMPTY(this.w_PATHFILE) and this.w_NUMIDXRES>1
                        this.w_IDSERIAL = ""
                      endif
                      do case
                        case !EMPTY(NVL(this.w_IDSERIAL, " "))
                          addmsgnl("Indice rilevato, copia file e aggiornamento indice", this.w_MskMESS )
                          * --- Il file esportato appartiene all'utente indicato, sovrascrivo il file
                          *     e svuoto i marker nell'indice
                          this.w_FILEORIG = ADDBS(this.w_PATHDEST )+this.w_PATHFILE+this.w_ZIPNAME
                          this.w_TIPOALLE = IIF(EMPTY(NVL( this.w_TIPOALLE, " ")), "F", this.w_TIPOALLE)
                          w_ErrorHandler = on("ERROR")
                          w_ERRORE = .F.
                          on error w_ERRORE = .T.
                          if this.w_TIPOALLE="F"
                            this.w_GFILE = ADDBS( ALLTRIM( this.w_AEPATHFI ) ) + ALLTRIM( this.w_FILE )
                          else
                            this.w_GFILE = ALLTRIM( this.w_ORIGFILE )
                          endif
                          * --- Criterio data zip
                          *     1.	Se coincide o � inferiore alla data di esportazione del file il file viene sovrascritto
                          *     2.	Se maggiore opera prima la copia di back up (anche se il parametro relativo � spento) poi aggiorna il file
                          if this.w_PAVERDOC="S" or (this.w_PADATZIP="S" and this.w_DATZIP>this.w_IDDATZIP)
                            this.w_FILEVERS = ALLTRIM( ADDBS( JUSTPATH( this.w_GFILE ) ) ) + ALLTRIM( JUSTSTEM( this.w_GFILE ) ) + "_" + ALLTRIM( TTOC(DATETIME(),1) ) + "_" + ALLTRIM( STR( this.w_IDUTEEXP ) ) + "." + ALLTRIM( JUSTEXT( this.w_GFILE ) )
                            addmsgnl("%1Creazione indice nascosto per versione file", this.w_MskMESS, CHR(9) )
                            * --- Try
                            local bErr_051693C0
                            bErr_051693C0=bTrsErr
                            this.Try_051693C0()
                            * --- Catch
                            if !empty(i_Error)
                              i_ErrMsg=i_Error
                              i_Error=''
                              * --- rollback
                              bTrsErr=.t.
                              cp_EndTrs(.t.)
                              addmsgnl("%1Impossibile creare indice nascosto per versione file", this.w_MskMESS, CHR(9) )
                              this.w_ERRLOG = .T.
                              w_ERRORE = .T.
                            endif
                            bTrsErr=bTrsErr or bErr_051693C0
                            * --- End
                          endif
                          if !w_ERRORE
                            addmsgnl("%3Copia file da %1 a %2", this.w_MskMESS, this.w_FILEORIG, this.w_GFILE, CHR(9) )
                            COPY FILE ( this.w_FILEORIG ) TO ( this.w_GFILE )
                            if w_ERRORE
                              addmsgnl("%3Impossibile sovrascrivere il file %1 con il file %2", this.w_MskMESS, this.w_FILEORIG , this.w_GFILE, CHR(9) )
                              this.w_ERRLOG = .T.
                            endif
                          endif
                          * --- Ripristino il vecchio errorhandler prima dell'eventuale write
                          on error &w_ErrorHandler
                          if !w_ERRORE
                            addmsgnl("%1Copia file terminata correttamente, aggiornamento indice", this.w_MskMESS, CHR(9) )
                            * --- Write into PROMINDI
                            i_commit = .f.
                            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                              cp_BeginTrs()
                              i_commit = .t.
                            endif
                            i_nConn=i_TableProp[this.PROMINDI_idx,3]
                            i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                            i_ccchkf=''
                            this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                            if i_nConn<>0
                              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                              +"IDUTEEXP ="+cp_NullLink(cp_ToStrODBC(0),'PROMINDI','IDUTEEXP');
                              +",IDZIPNAM ="+cp_NullLink(cp_ToStrODBC(""),'PROMINDI','IDZIPNAM');
                                  +i_ccchkf ;
                              +" where ";
                                  +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                                     )
                            else
                              update (i_cTable) set;
                                  IDUTEEXP = 0;
                                  ,IDZIPNAM = "";
                                  &i_ccchkf. ;
                               where;
                                  IDSERIAL = this.w_IDSERIAL;

                              i_Rows = _tally
                            endif
                            if i_commit
                              cp_EndTrs(.t.)
                            endif
                            if bTrsErr
                              i_Error=MSG_WRITE_ERROR
                              return
                            endif
                          endif
                        case EMPTY(NVL(this.w_IDSERIAL, " ")) AND EMPTY(this.w_PATHFILE) and this.w_NUMIDXRES > 1
                          addmsgnl("Rilevati %1 indici con solito nome file (%2) impossibile procedere con importazione in assenza di archivio con percorsi all'interno", this.w_MskMESS, ALLTRIM(STR( this.w_NUMIDXRES )) , this.w_ZIPNAME )
                        case EMPTY(NVL(this.w_IDSERIAL, " "))
                          addmsgnl("Impossibile rilevare indice corrispondente o codice utente esportatore differente", this.w_MskMESS )
                      endcase
                      this.w_posiz = this.w_posiz + 1
                    enddo
                    * --- Elimino cartella temporanea
                    if !deletefolder(this.w_PATHDEST, this.w_RETMSG)
                      addmsgnl("Errore eliminazione cartella temporanea %1", this.w_MskMESS, this.w_RETMSG )
                      this.w_ERRLOG = .T.
                    endif
                  else
                    addmsgnl("Errore analisi lista documenti presenti in file %1 (%2). Impossibile proseguire", this.w_MskMESS , ALLTRIM(L_ZipToImport(this.w_TMPN)), L_RETMSG)
                    this.w_ERRLOG = .T.
                    * --- Elimino cartella temporanea
                    if !deletefolder(this.w_PATHDEST, this.w_RETMSG)
                      addmsgnl("Errore eliminazione cartella temporanea %1", this.w_MskMESS , this.w_RETMSG)
                      this.w_ERRLOG = .T.
                    endif
                    this.w_TMPN = ALEN( L_ZipToImport, 1 )
                  endif
                else
                  addmsgnl("Errore scompattazione file %1 (%2). Impossibile proseguire", this.w_MskMESS , ALLTRIM(L_ZipToImport(this.w_TMPN)), L_RETMSG)
                  this.w_ERRLOG = .T.
                  this.w_TMPN = ALEN( L_ZipToImport, 1 )
                endif
              endif
              this.w_TMPN = this.w_TMPN + 1
            enddo
            addmsgnl("Fine analisi archivi compressi memorizzati", this.w_MskMESS)
          else
            addmsgnl("Cartella o file indicato inesistente. Impossibile proseguire", this.w_MskMESS)
            this.w_ERRLOG = .T.
          endif
          addmsgnl("Fine elaborazione %1 %2", this.w_MskMESS, DTOC(DATE()), LEFT(TTOC(DATETIME(),2),8))
          if this.w_ERRLOG
            addmsgnl("ATTENZIONE! Procedura terminata con errori", this.w_MskMESS )
          else
            this.oParentObject.NotifyEvent("Interroga")
          endif
          this.w_MskMESS = .NULL.
        else
          ah_Errormsg("Operazione interrotta come richiesto", 64)
        endif
      case this.w_Operazione=="W" OR this.w_Operazione=="WA"
        * --- Invio documenti ad applicazione web infinity
        * --- Legge percorso per documenti
        * --- Read from CONTROPA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTROPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "COPATHDO"+;
            " from "+i_cTable+" CONTROPA where ";
                +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            COPATHDO;
            from (i_cTable) where;
                COCODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COPATHDO = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Aggiunge barra finale
        if empty(strtran(this.w_COPATHDO,"\"," "))
          Ah_ErrorMsg("Percorso archiviazione documenti per invio al webfolder non specificato, impossibile proseguire!",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_COPATHDO = ADDBS(this.w_COPATHDO)
        if g_DMIP="S"
          this.w_COPATHDO = ADDBS(this.w_COPATHDO+SYS(2015))
        endif
        if !DIRECTORY(this.w_COPATHDO)
          this.w_ERRODIR = Not(this.AH_CreateFolder(this.w_COPATHDO))
        endif
        * --- Inizio elaborazione documenti selezionati
        if g_DMIP="S"
          * --- begin transaction
          cp_BeginTrs()
          this.w_ROLLBACK = False
        endif
        this.w_PADRE = this.oParentObject
        if lower(this.w_PADRE.class) = "tgsrv_bch"
          this.w_CURSORE = this.w_PADRE.w_CURSINDI
        else
          this.w_CURSORE = this.w_PADRE.w_ZoomGF.cCursor
        endif
         
 Select(this.w_CURSORE) 
 Go Top
        scan for XCHK=1
        this.w_NOMEFILE = Alltrim(GFNOMEFILE)
        this.w_KEYINDIC = alltrim(GFKEYINDIC)
        this.w_PATHFILE = alltrim(GFPATHFILE)
        this.w_CDERASEF = iif(EMPTY(NVL(CDERASEF," ")), "N", CDERASEF)
        this.w_ORIGFILE = alltrim(GFORIGFILE)
        this.w_CLASDOCU = alltrim(GFCLASDOCU)
        this.w_IDWEBFIL = iif(EMPTY(NVL(IDWEBFIL,"")), iif(this.w_KEYINDIC=LEFT(JUSTFNAME(this.w_NOMEFILE),20),"",this.w_KEYINDIC+"_")+JUSTFNAME(this.w_NOMEFILE), IDWEBFIL)
        * --- Il nome del file non deve eccedere i 100 caratteri
        if Len(this.w_IDWEBFIL)>100
          this.w_estensione = JustExt(this.w_IDWEBFIL)
          this.w_IDWEBFIL = Left(ForceExt(this.w_IDWEBFIL,""), 100-Len(this.w_estensione)-1) + "." + this.w_estensione
        endif
        this.w_ORIGIDWEBFIL = NVL(IDWEBFIL,"")
        this.w_DESCRFILE = Left(Alltrim(GFDESCFILE),100)
        * --- Tronco anche qua perch� nel nodo "LogicalName" viene inserito il primo valore non vuoto tra:
        *     w_ORIGIDWEBFIL
        *     w_NOMEFILE
        *     w_IDWEBFIL
        do case
          case Empty(this.w_ORIGIDWEBFIL) And Len(this.w_NOMEFILE)>100
            this.w_estensione = JustExt(this.w_NOMEFILE)
            this.w_ORIGIDWEBFIL = Left(ForceExt(this.w_NOMEFILE,""), 100-Len(this.w_estensione)-1) + "." + this.w_estensione
          case !Empty(this.w_ORIGIDWEBFIL) And Len(this.w_ORIGIDWEBFIL)>100
            this.w_estensione = JustExt(this.w_ORIGIDWEBFIL)
            this.w_ORIGIDWEBFIL = Left(ForceExt(this.w_ORIGIDWEBFIL,""), 100-Len(this.w_estensione)-1) + "." + this.w_estensione
        endcase
        this.w_IDMODALL = IDMODALL
        this.w_IDWEBAPP = IDWEBAPP
        do case
          case this.w_IDMODALL="E"
            FileEds("V", this.w_CLASDOCU, this.w_ORIGFILE, ADDBS(alltrim(tempadhoc()))+alltrim(this.w_NOMEFILE))
            this.w_TmpPat = ADDBS(tempadhoc()) + this.w_NOMEFILE
          case this.w_IDMODALL="I"
            if g_DMIP="S"
              FileInfinity("W", this.w_KEYINDIC, ADDBS(tempadhoc()) + this.w_NOMEFILE)
              this.w_TmpPat = ADDBS(tempadhoc()) + this.w_NOMEFILE
            endif
          otherwise
            * --- Percorso di origine ...
            this.w_TmpPat = this.w_PATHFILE + this.w_NOMEFILE
        endcase
        if NOT EMPTY(NVL(this.w_TmpPat,"")) AND cp_FileExist(this.w_TmpPat) AND (this.w_IDMODALL<>"I" or g_DMIP="S")
          Select(this.w_CURSORE)
          do case
            case g_DMIP="S"
              if NOT INLIST(this.w_IDWEBAPP,"X","D")
                * --- Write into PROMINDI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PROMINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IDWEBAPP ="+cp_NullLink(cp_ToStrODBC("D"),'PROMINDI','IDWEBAPP');
                  +",IDWEBFIL ="+cp_NullLink(cp_ToStrODBC(this.w_IDWEBFIL),'PROMINDI','IDWEBFIL');
                      +i_ccchkf ;
                  +" where ";
                      +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                         )
                else
                  update (i_cTable) set;
                      IDWEBAPP = "D";
                      ,IDWEBFIL = this.w_IDWEBFIL;
                      &i_ccchkf. ;
                   where;
                      IDSERIAL = this.w_KEYINDIC;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                this.Page_6()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                this.Page_8()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            case NOT(this.w_IDWEBAPP$"SA")
              * --- Se non prevedeva l'invio a web lo devo fare per la prima volta
              if this.w_Operazione="WA" AND this.w_IDMODALL<>"I"
                * --- Write into PROMINDI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PROMINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IDWEBAPP ="+cp_NullLink(cp_ToStrODBC("A"),'PROMINDI','IDWEBAPP');
                  +",IDCRCALL ="+cp_NullLink(cp_ToStrODBC(SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1)),'PROMINDI','IDCRCALL');
                  +",IDERASEF ="+cp_NullLink(cp_ToStrODBC("S"),'PROMINDI','IDERASEF');
                  +",IDWEBFIL ="+cp_NullLink(cp_ToStrODBC(this.w_IDWEBFIL),'PROMINDI','IDWEBFIL');
                  +",IDMODALL ="+cp_NullLink(cp_ToStrODBC("I"),'PROMINDI','IDMODALL');
                      +i_ccchkf ;
                  +" where ";
                      +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                         )
                else
                  update (i_cTable) set;
                      IDWEBAPP = "A";
                      ,IDCRCALL = SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1);
                      ,IDERASEF = "S";
                      ,IDWEBFIL = this.w_IDWEBFIL;
                      ,IDMODALL = "I";
                      &i_ccchkf. ;
                   where;
                      IDSERIAL = this.w_KEYINDIC;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              else
                * --- Write into PROMINDI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PROMINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IDWEBAPP ="+cp_NullLink(cp_ToStrODBC("A"),'PROMINDI','IDWEBAPP');
                  +",IDCRCALL ="+cp_NullLink(cp_ToStrODBC(SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1)),'PROMINDI','IDCRCALL');
                  +",IDERASEF ="+cp_NullLink(cp_ToStrODBC(this.w_CDERASEF),'PROMINDI','IDERASEF');
                  +",IDWEBFIL ="+cp_NullLink(cp_ToStrODBC(this.w_IDWEBFIL),'PROMINDI','IDWEBFIL');
                      +i_ccchkf ;
                  +" where ";
                      +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                         )
                else
                  update (i_cTable) set;
                      IDWEBAPP = "A";
                      ,IDCRCALL = SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1);
                      ,IDERASEF = this.w_CDERASEF;
                      ,IDWEBFIL = this.w_IDWEBFIL;
                      &i_ccchkf. ;
                   where;
                      IDSERIAL = this.w_KEYINDIC;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              this.Page_6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            otherwise
              * --- devo aggiornare l'invio
              if alltrim(SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1))<>alltrim(NVL(IDCRCALL,"")) AND IDWEBAPP#"S"
                if this.w_Operazione="WA" AND this.w_IDMODALL<>"I"
                  * --- Write into PROMINDI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PROMINDI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"IDCRCALL ="+cp_NullLink(cp_ToStrODBC(SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1)),'PROMINDI','IDCRCALL');
                    +",IDMODALL ="+cp_NullLink(cp_ToStrODBC("I"),'PROMINDI','IDMODALL');
                    +",IDWEBAPP ="+cp_NullLink(cp_ToStrODBC("A"),'PROMINDI','IDWEBAPP');
                    +",IDERASEF ="+cp_NullLink(cp_ToStrODBC("S"),'PROMINDI','IDERASEF');
                        +i_ccchkf ;
                    +" where ";
                        +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                           )
                  else
                    update (i_cTable) set;
                        IDCRCALL = SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1);
                        ,IDMODALL = "I";
                        ,IDWEBAPP = "A";
                        ,IDERASEF = "S";
                        &i_ccchkf. ;
                     where;
                        IDSERIAL = this.w_KEYINDIC;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                else
                  * --- Write into PROMINDI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PROMINDI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"IDCRCALL ="+cp_NullLink(cp_ToStrODBC(SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1)),'PROMINDI','IDCRCALL');
                        +i_ccchkf ;
                    +" where ";
                        +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                           )
                  else
                    update (i_cTable) set;
                        IDCRCALL = SYS(2007, FILETOSTR(this.w_TmpPat), -1, 1);
                        &i_ccchkf. ;
                     where;
                        IDSERIAL = this.w_KEYINDIC;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
                this.Page_6()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                do case
                  case IDWEBAPP = "S" and this.w_Operazione # "W"
                    * --- Write into PROMINDI
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PROMINDI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"IDMODALL ="+cp_NullLink(cp_ToStrODBC("I"),'PROMINDI','IDMODALL');
                      +",IDWEBAPP ="+cp_NullLink(cp_ToStrODBC("A"),'PROMINDI','IDWEBAPP');
                          +i_ccchkf ;
                      +" where ";
                          +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                             )
                    else
                      update (i_cTable) set;
                          IDMODALL = "I";
                          ,IDWEBAPP = "A";
                          &i_ccchkf. ;
                       where;
                          IDSERIAL = this.w_KEYINDIC;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  case this.w_Operazione="WA" AND this.w_IDMODALL<>"I"
                    * --- Write into PROMINDI
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PROMINDI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"IDMODALL ="+cp_NullLink(cp_ToStrODBC("I"),'PROMINDI','IDMODALL');
                          +i_ccchkf ;
                      +" where ";
                          +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                             )
                    else
                      update (i_cTable) set;
                          IDMODALL = "I";
                          &i_ccchkf. ;
                       where;
                          IDSERIAL = this.w_KEYINDIC;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                endcase
              endif
          endcase
        else
          if g_DMIP<>"S" 
            do case
              case IDWEBAPP = "S" and this.w_Operazione # "W"
                * --- Write into PROMINDI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PROMINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IDMODALL ="+cp_NullLink(cp_ToStrODBC("I"),'PROMINDI','IDMODALL');
                  +",IDWEBAPP ="+cp_NullLink(cp_ToStrODBC("A"),'PROMINDI','IDWEBAPP');
                      +i_ccchkf ;
                  +" where ";
                      +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                         )
                else
                  update (i_cTable) set;
                      IDMODALL = "I";
                      ,IDWEBAPP = "A";
                      &i_ccchkf. ;
                   where;
                      IDSERIAL = this.w_KEYINDIC;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case this.w_Operazione="WA" AND this.w_IDMODALL<>"I"
                * --- Write into PROMINDI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PROMINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IDMODALL ="+cp_NullLink(cp_ToStrODBC("I"),'PROMINDI','IDMODALL');
                  +",IDWEBAPP ="+cp_NullLink(cp_ToStrODBC("A"),'PROMINDI','IDWEBAPP');
                  +",IDERASEF ="+cp_NullLink(cp_ToStrODBC("S"),'PROMINDI','IDERASEF');
                      +i_ccchkf ;
                  +" where ";
                      +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                         )
                else
                  update (i_cTable) set;
                      IDMODALL = "I";
                      ,IDWEBAPP = "A";
                      ,IDERASEF = "S";
                      &i_ccchkf. ;
                   where;
                      IDSERIAL = this.w_KEYINDIC;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case IDWEBAPP = "N"
                * --- Write into PROMINDI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PROMINDI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IDCRCALL ="+cp_NullLink(cp_ToStrODBC(SPACE(20)),'PROMINDI','IDCRCALL');
                      +i_ccchkf ;
                  +" where ";
                      +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                         )
                else
                  update (i_cTable) set;
                      IDCRCALL = SPACE(20);
                      &i_ccchkf. ;
                   where;
                      IDSERIAL = this.w_KEYINDIC;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
            endcase
          else
            if not cp_FileExist(this.w_TmpPat)
              this.w_OldConn = i_TableProp[this.PROMINDI_idx,3]
              if not empty(i_ServerConn[i_TableProp[this.PROMINDI_IDX,5],8])
                this.w_NewConn = sqlstringconnect(i_ServerConn[i_TableProp[this.AZIENDA_IDX,5],8])
              else
                this.w_NewConn = sqlstringconnect(sqlgetprop(this.w_OldConn,"ConnectString"))
              endif
              if this.w_NewConn>0
                cp_PostOpenConn(this.w_NewConn)
                i_TableProp[this.PROMINDI_idx,3]=this.w_NewConn
              endif
              this.w_NameError = "File inesistente"
              * --- Write into PROMINDI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PROMINDI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"IDWEBERR ="+cp_NullLink(cp_ToStrODBC(this.w_NameError),'PROMINDI','IDWEBERR');
                +",IDWEBRET ="+cp_NullLink(cp_ToStrODBC("E"),'PROMINDI','IDWEBRET');
                    +i_ccchkf ;
                +" where ";
                    +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                       )
              else
                update (i_cTable) set;
                    IDWEBERR = this.w_NameError;
                    ,IDWEBRET = "E";
                    &i_ccchkf. ;
                 where;
                    IDSERIAL = this.w_KEYINDIC;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              if this.w_NewConn>0
                sqldisconn(this.w_NewConn)
                i_TableProp[this.PROMINDI_idx,3]=this.w_OldConn
              endif
            endif
          endif
        endif
        Endscan
        * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        *     Chiudo XML descrittore per invio a Infinity
        if g_DMIP="S"
          if not empty(this.w_UploadDocumentsXML)
            * --- Se non esiste ancora xml descrittore lo creo
            this.w_OBJXML.Save(this.w_UploadDocumentsXML)     
            * --- Faccio lo zip della cartella con il descrittore e i file da inviare
            local L_ERROREZIP
            if ZIPUNZIP("Z", FORCEEXT(this.w_UploadDocumentsXML,"ZIP"), ADDBS(JUSTPATH(this.w_UploadDocumentsXML)), "", @L_ERROREZIP, "7zzip")
              * --- Chiamo procedura per invio del file zippato
              * --- variabili Infinity D.M.S.
              * --- Memorizza ... e imposta
               w_CreateObjErr = .F. 
 w_OldErr=on("ERROR")
              on error w_CreateObjErr=.t.
              * --- Mi connetto come utente di sevizio
              this.w_oObjInfinity = InfinityConnect("", g_REVI="S")
              if vartype(this.w_oObjInfinity)="O"
                this.w_ERRINF = this.w_oObjInfinity.UploadDocuments(FORCEEXT(this.w_UploadDocumentsXML,"ZIP"))
                if this.w_ERRINF = 0
                  this.w_ERRINF = this.w_oObjInfinity.GetUploadResponse(FORCEEXT(this.w_UploadDocumentsXML,"")+"_response.xml","generic")
                  if this.w_ERRINF = 0
                    * --- Gestione xml di risposta
                    this.w_FILETOSTR = FILETOSTR(FORCEEXT(this.w_UploadDocumentsXML,"")+"_response.xml")
                    this.w_OBJXMLRES = createobject("Msxml2.DOMDocument.4.0")
                    this.w_OBJXMLRES.LoadXML(this.w_FILETOSTR)     
                    this.w_NUMNODI = this.w_OBJXMLREs.childNodes.item(1).childNodes.length
                    this.w_iChild = 0
                    * --- * recupera tutti i nodi figli
                    do while this.w_iChild < this.w_NUMNODI
                      * --- recupero nodo Process
                      if this.w_OBJXMLREs.childNodes.item(1).childNodes.item(this.w_iChild).nodeName="Add_UploadDocuments"
                        this.w_oNODE = this.w_OBJXMLREs.childNodes.item(1).childNodes.item(this.w_iChild)
                        this.w_nDocumentUploaded = this.w_nDocumentUploaded + 1
                        this.w_KEYINDIC = this.w_oNODE.GetAttribute("ExternalCode")
                        this.w_NameError = this.w_oNODE.GetAttribute("Error")
                        if not empty(nvl(this.w_NameError,""))
                          this.w_ERRORTRASF = .t.
                          * --- devo scrivere l'errore
                          * --- Write into PROMINDI
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_nConn=i_TableProp[this.PROMINDI_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                          i_ccchkf=''
                          this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                            +"IDWEBERR ="+cp_NullLink(cp_ToStrODBC(this.w_NameError),'PROMINDI','IDWEBERR');
                            +",IDWEBRET ="+cp_NullLink(cp_ToStrODBC("E"),'PROMINDI','IDWEBRET');
                            +",IDWEBAPP ="+cp_NullLink(cp_ToStrODBC("N"),'PROMINDI','IDWEBAPP');
                                +i_ccchkf ;
                            +" where ";
                                +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                                   )
                          else
                            update (i_cTable) set;
                                IDWEBERR = this.w_NameError;
                                ,IDWEBRET = "E";
                                ,IDWEBAPP = "N";
                                &i_ccchkf. ;
                             where;
                                IDSERIAL = this.w_KEYINDIC;

                            i_Rows = _tally
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if bTrsErr
                            i_Error=MSG_WRITE_ERROR
                            return
                          endif
                        else
                          * --- Write into PROMINDI
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_nConn=i_TableProp[this.PROMINDI_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                          i_ccchkf=''
                          this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                            +"IDWEBAPP ="+cp_NullLink(cp_ToStrODBC("X"),'PROMINDI','IDWEBAPP');
                                +i_ccchkf ;
                            +" where ";
                                +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                                   )
                          else
                            update (i_cTable) set;
                                IDWEBAPP = "X";
                                &i_ccchkf. ;
                             where;
                                IDSERIAL = this.w_KEYINDIC;

                            i_Rows = _tally
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if bTrsErr
                            i_Error=MSG_WRITE_ERROR
                            return
                          endif
                        endif
                      endif
                      this.w_iChild = this.w_iChild + 1
                    enddo
                    * --- Se ho avuto degli file in transito ma senza errori li ripristino
                    * --- Write into PROMINDI
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PROMINDI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"IDWEBAPP ="+cp_NullLink(cp_ToStrODBC("N"),'PROMINDI','IDWEBAPP');
                          +i_ccchkf ;
                      +" where ";
                          +"IDWEBAPP = "+cp_ToStrODBC("D");
                             )
                    else
                      update (i_cTable) set;
                          IDWEBAPP = "N";
                          &i_ccchkf. ;
                       where;
                          IDWEBAPP = "D";

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                    * --- commit
                    cp_EndTrs(.t.)
                  else
                    * --- rollback
                    bTrsErr=.t.
                    cp_EndTrs(.t.)
                    this.w_ROLLBACK = True
                    Ah_ErrorMsg("Si � verificato un errore nel ricevimento del file di risposta%0%1",,,this.w_oObjInfinity.LastMsgError)
                  endif
                else
                  * --- rollback
                  bTrsErr=.t.
                  cp_EndTrs(.t.)
                  this.w_ROLLBACK = True
                  Ah_ErrorMsg("Si � verificato un errore nel trasferimento del file%0%1",,,this.w_oObjInfinity.LastMsgError)
                endif
                this.w_oObjInfinity.DisConnect()     
              else
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
                this.w_ROLLBACK = True
                Ah_ErrorMsg("Impossibile connettersi al server Infinity")
              endif
              * --- Ripristina OnError
              on error &w_OldErr
            else
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              this.w_ROLLBACK = True
              ah_errormsg("Errore nella creazione dello zip per l'invio a Infinity dei documenti%0%1","!","",L_ERROREZIP)
            endif
          else
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            this.w_ROLLBACK = True
          endif
        endif
        * --- Fine elaborazione documenti  selezionati
        if lower(this.w_PADRE.class) = "tgsrv_bch"
          this.oParentObject.w_UPLOADERR = this.w_ROLLBACK
        else
          if this.w_ERRORTRASF
            ah_errormsg("Elaborazione terminata: si sono verificati alcuni errori nell'invio dei documenti al D.M.S. di Infinity (controllare gli indici per il resoconto).")
          else
            ah_errormsg("Elaborazione terminata")
          endif
          * --- Refresh Zoom
          GSUT_BES(this.oParentObject,this.w_Archivio,this.w_Chiave) 
        endif
      case this.w_Operazione=="X"
        * --- Carica attributi di ricerca
        vq_exec("QUERY\GSUT0KFG", this, "CursAttrib")
        this.w_GSUT_MA1 = this.oParentObject.GSUT_MA1
        SELECT (this.w_GSUT_MA1.cTrsName) 
 ZAP
        if USED("CursAttrib")
          SELECT CursAttrib
          SCAN
          this.w_GSUT_MA1.InitRow()     
          this.w_GSUT_MA1.w_CPROWORD = CursAttrib.CPROWORD
          this.w_GSUT_MA1.w_IDCODATT = CursAttrib.CDCODATT
          this.w_GSUT_MA1.w_IDDESATT = CursAttrib.CDDESATT
          this.w_GSUT_MA1.w_IDTIPATT = CursAttrib.CDTIPATT
          this.w_GSUT_MA1.w_IDTABKEY = CursAttrib.CDTABKEY
          this.w_GSUT_MA1.w_OPERAT = "="
          this.w_GSUT_MA1.w_OPERAT1 = "="
          this.w_GSUT_MA1.w_OPERAT2 = "="
          this.w_GSUT_MA1.w_OPERAT3 = "="
          this.w_GSUT_MA1.SaveRow()     
          SELECT CursAttrib
          ENDSCAN
          this.w_GSUT_MA1.FirstRow()     
          this.w_GSUT_MA1.SetRow()     
          SELECT CursAttrib 
 USE
        endif
    endcase
    do case
      case this.w_Operazione $ "K"
        this.w_RETVAL = 1
        if this.w_RETVAL=1
          this.w_GestAtt = gsut_kis(this.oparentobject)
          if !(this.w_GestAtt.bSec1)
            this.w_GestAtt.Notifyevent("Done")     
            this.w_GestAtt = .null.
            i_retcode = 'stop'
            return
          endif
          this.w_GestAtt.mcalc(.t.)     
          this.w_GestAtt.w_TIPOPATH = "C"
          this.w_GestAtt.w_PRAAPE = this.oParentObject.w_CODPRAT
          this.w_GestAtt.Notifyevent("Ricerca")     
        else
          this.w_GestAtt = gsut_kem()
          if !(this.w_GestAtt.bSec1)
            this.w_GestAtt.Notifyevent("Done")     
            this.w_GestAtt = .null.
            i_retcode = 'stop'
            return
          endif
        endif
        this.w_GestAtt = .null.
        this.oParentObject.NotifyEvent("Ricerca")
    endcase
    release L_RETMSG
    if inlist(this.w_Operazione, "checkall", "uncheckall")
      this.oParentObject.w_SELEZIONE = iif(this.w_Operazione=="uncheckall", "D", "S")
      this.w_PADRE.SetControlsValue()     
      this.w_PADRE.NotifyEvent("w_SELEZIONE Changed")     
    else
      if this.oParentObject.w_SELMULT
        this.oParentObject.w_SELEZIONE = "D"
        this.w_PADRE.SetControlsValue()     
        this.w_PADRE.NotifyEvent("w_SELEZIONE Changed")     
      endif
    endif
  endproc
  proc Try_05175288()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.oParentObject.w_SELMULT
      * --- Inizio elaborazione documenti selezionati
       
 Select(this.w_CURSORE) 
 Go Top
      scan for XCHK=1
      this.w_KEYINDIC = ALLTRIM(GFKEYINDIC)
      * --- Read from PROMINDI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PROMINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IDUTEEXP"+;
          " from "+i_cTable+" PROMINDI where ";
              +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IDUTEEXP;
          from (i_cTable) where;
              IDSERIAL = this.w_KEYINDIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CKUTEEXP = NVL(cp_ToDate(_read_.IDUTEEXP),cp_NullValue(_read_.IDUTEEXP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_CKUTEEXP=0
        * --- Write into PROMINDI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PROMINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IDUTEEXP ="+cp_NullLink(cp_ToStrODBC(this.w_IDUTEEXP),'PROMINDI','IDUTEEXP');
          +",IDZIPNAM ="+cp_NullLink(cp_ToStrODBC(this.w_FILEDEST),'PROMINDI','IDZIPNAM');
          +",IDDATZIP ="+cp_NullLink(cp_ToStrODBC(this.w_IDDATZIP),'PROMINDI','IDDATZIP');
              +i_ccchkf ;
          +" where ";
              +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                 )
        else
          update (i_cTable) set;
              IDUTEEXP = this.w_IDUTEEXP;
              ,IDZIPNAM = this.w_FILEDEST;
              ,IDDATZIP = this.w_IDDATZIP;
              &i_ccchkf. ;
           where;
              IDSERIAL = this.w_KEYINDIC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      Endscan
      * --- Fine elaborazione documenti  selezionati
       
 Select(this.w_CURSORE) 
 Go Top
    else
      * --- Operazione su documento attivo (record attivo)
      this.w_KEYINDIC = ALLTRIM(this.oParentObject.w_GFKEYINDIC)
      * --- Read from PROMINDI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PROMINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IDUTEEXP"+;
          " from "+i_cTable+" PROMINDI where ";
              +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IDUTEEXP;
          from (i_cTable) where;
              IDSERIAL = this.w_KEYINDIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CKUTEEXP = NVL(cp_ToDate(_read_.IDUTEEXP),cp_NullValue(_read_.IDUTEEXP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_CKUTEEXP=0
        * --- Write into PROMINDI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PROMINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IDUTEEXP ="+cp_NullLink(cp_ToStrODBC(this.w_IDUTEEXP),'PROMINDI','IDUTEEXP');
          +",IDZIPNAM ="+cp_NullLink(cp_ToStrODBC(this.w_FILEDEST),'PROMINDI','IDZIPNAM');
          +",IDDATZIP ="+cp_NullLink(cp_ToStrODBC(this.w_IDDATZIP),'PROMINDI','IDDATZIP');
              +i_ccchkf ;
          +" where ";
              +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
                 )
        else
          update (i_cTable) set;
              IDUTEEXP = this.w_IDUTEEXP;
              ,IDZIPNAM = this.w_FILEDEST;
              ,IDDATZIP = this.w_IDDATZIP;
              &i_ccchkf. ;
           where;
              IDSERIAL = this.w_KEYINDIC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    addmsgnl("Fine aggiornamento indici.%0Cancellazione cartella temporanea", this.w_MskMESS )
    * --- Elimino cartella temporanea
    if !deletefolder(this.w_PATHDEST, this.w_RETMSG)
      addmsgnl("Errore eliminazione cartella temporanea %1", this.w_MskMESS, this.w_RETMSG )
      this.w_ERRLOG = .T.
    endif
    return
  proc Try_051693C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Clono il master in tabella temporanea
    * --- Create temporary table TMP_PRAT
    i_nIdx=cp_AddTableDef('TMP_PRAT') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PROMINDI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL)+"";
          )
    this.TMP_PRAT_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_NEWSERIDX = SPACE(20)
    i_Conn=i_TableProp[this.PROMINDI_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEIDO", "i_CODAZI,w_NEWSERIDX")
    * --- Aggiorno dati in tabella temporanea (serial, nome file, flag hide, ecc.)
    this.w_NEWOBJ = this.w_NEWOBJ + "_copia backup"
    * --- Write into TMP_PRAT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_PRAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PRAT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PRAT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IDSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWSERIDX),'TMP_PRAT','IDSERIAL');
      +",IDFLHIDE ="+cp_NullLink(cp_ToStrODBC("S"),'TMP_PRAT','IDFLHIDE');
      +",IDNOMFIL ="+cp_NullLink(cp_ToStrODBC(JUSTFNAME(this.w_FILEVERS)),'TMP_PRAT','IDNOMFIL');
      +",IDORIFIL ="+cp_NullLink(cp_ToStrODBC(this.w_FILEVERS),'TMP_PRAT','IDORIFIL');
      +",IDZIPNAM ="+cp_NullLink(cp_ToStrODBC(""),'TMP_PRAT','IDZIPNAM');
      +",IDUTEEXP ="+cp_NullLink(cp_ToStrODBC(0),'TMP_PRAT','IDUTEEXP');
      +",UTDC ="+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'TMP_PRAT','UTDC');
      +",UTCC ="+cp_NullLink(cp_ToStrODBC(this.w_IDUTEEXP),'TMP_PRAT','UTCC');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(0),'TMP_PRAT','UTCV');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(.NULL.),'TMP_PRAT','UTDV');
      +",IDOGGETT ="+cp_NullLink(cp_ToStrODBC(this.w_NEWOBJ),'TMP_PRAT','IDOGGETT');
      +",IDPUBWEB ="+cp_NullLink(cp_ToStrODBC("N"),'TMP_PRAT','IDPUBWEB');
          +i_ccchkf ;
      +" where ";
          +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
             )
    else
      update (i_cTable) set;
          IDSERIAL = this.w_NEWSERIDX;
          ,IDFLHIDE = "S";
          ,IDNOMFIL = JUSTFNAME(this.w_FILEVERS);
          ,IDORIFIL = this.w_FILEVERS;
          ,IDZIPNAM = "";
          ,IDUTEEXP = 0;
          ,UTDC = SetInfoDate(g_CALUTD);
          ,UTCC = this.w_IDUTEEXP;
          ,UTCV = 0;
          ,UTDV = .NULL.;
          ,IDOGGETT = this.w_NEWOBJ;
          ,IDPUBWEB = "N";
          &i_ccchkf. ;
       where;
          IDSERIAL = this.w_IDSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Inserisco il nuovo indice nella tabella ufficiale
    * --- Insert into PROMINDI
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_PRAT_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where IDSERIAL="+cp_ToStrODBC(this.w_NEWSERIDX)+"",this.PROMINDI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMP_PRAT
    i_nIdx=cp_GetTableDefIdx('TMP_PRAT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PRAT')
    endif
    * --- Clono il detail
    * --- Create temporary table TMP_PRAT
    i_nIdx=cp_AddTableDef('TMP_PRAT') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PRODINDI_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL)+"";
          )
    this.TMP_PRAT_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiorno dati in tabella temporanea (Seriale)
    * --- Write into TMP_PRAT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_PRAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PRAT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PRAT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IDSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWSERIDX),'TMP_PRAT','IDSERIAL');
          +i_ccchkf ;
      +" where ";
          +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
             )
    else
      update (i_cTable) set;
          IDSERIAL = this.w_NEWSERIDX;
          &i_ccchkf. ;
       where;
          IDSERIAL = this.w_IDSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Inserisco dati detail in tabella ufficiale
    * --- Insert into PRODINDI
    i_nConn=i_TableProp[this.PRODINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMP_PRAT_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where IDSERIAL = "+cp_ToStrODBC(this.w_NEWSERIDX)+"",this.PRODINDI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Read from PRODINDI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PRODINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IDVALATT"+;
        " from "+i_cTable+" PRODINDI where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.w_NEWSERIDX);
            +" and IDCODATT = "+cp_ToStrODBC("CODICE");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IDVALATT;
        from (i_cTable) where;
            IDSERIAL = this.w_NEWSERIDX;
            and IDCODATT = "CODICE";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_IDVALATT = NVL(cp_ToDate(_read_.IDVALATT),cp_NullValue(_read_.IDVALATT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Aggiorno dati indice creato e copia file
    * --- Try
    local bErr_051671A0
    bErr_051671A0=bTrsErr
    this.Try_051671A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into LOGSININ
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.LOGSININ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOGSININ_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.LOGSININ_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"LSCODCAN ="+cp_NullLink(cp_ToStrODBC(this.w_IDVALATT),'LOGSININ','LSCODCAN');
        +",LDSER_ID ="+cp_NullLink(cp_ToStrODBC(this.w_NEWSERIDX),'LOGSININ','LDSER_ID');
        +",LSCODUTE ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'LOGSININ','LSCODUTE');
        +",LSDATSIN ="+cp_NullLink(cp_ToStrODBC(DATE()),'LOGSININ','LSDATSIN');
        +",LSSTATUS ="+cp_NullLink(cp_ToStrODBC("Z"),'LOGSININ','LSSTATUS');
            +i_ccchkf ;
        +" where ";
            +"LSFILENM = "+cp_ToStrODBC(this.w_FILEVERS);
               )
      else
        update (i_cTable) set;
            LSCODCAN = this.w_IDVALATT;
            ,LDSER_ID = this.w_NEWSERIDX;
            ,LSCODUTE = i_CODUTE;
            ,LSDATSIN = DATE();
            ,LSSTATUS = "Z";
            &i_ccchkf. ;
         where;
            LSFILENM = this.w_FILEVERS;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_051671A0
    * --- End
    addmsgnl("%3Copia file da %1 a %2", this.w_MskMESS, this.w_GFILE, this.w_FILEVERS, CHR(9) )
    COPY FILE ( this.w_GFILE ) TO ( this.w_FILEVERS )
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_051671A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into LOGSININ
    i_nConn=i_TableProp[this.LOGSININ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGSININ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOGSININ_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LSFILENM"+",LDSER_ID"+",LSCODCAN"+",LSCODUTE"+",LSDATSIN"+",LSSTATUS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_FILEVERS),'LOGSININ','LSFILENM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NEWSERIDX),'LOGSININ','LDSER_ID');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IDVALATT),'LOGSININ','LSCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'LOGSININ','LSCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(DATE()),'LOGSININ','LSDATSIN');
      +","+cp_NullLink(cp_ToStrODBC("Z"),'LOGSININ','LSSTATUS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LSFILENM',this.w_FILEVERS,'LDSER_ID',this.w_NEWSERIDX,'LSCODCAN',this.w_IDVALATT,'LSCODUTE',i_CODUTE,'LSDATSIN',DATE(),'LSSTATUS',"Z")
      insert into (i_cTable) (LSFILENM,LDSER_ID,LSCODCAN,LSCODUTE,LSDATSIN,LSSTATUS &i_ccchkf. );
         values (;
           this.w_FILEVERS;
           ,this.w_NEWSERIDX;
           ,this.w_IDVALATT;
           ,i_CODUTE;
           ,DATE();
           ,"Z";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia eseguibile associato al file
    * --- Converto eventuale percorso relativo in assoluto
    if this.oParentObject.w_GFIDMODALL # "I"
      this.w_TmpPat = FULLPATH(this.w_TmpPat)
    endif
    this.w_cLink = this.w_TmpPat
    this.w_cParms = space(1)
    declare integer ShellExecute in shell32.dll integer nHwnd, string cOperation, string cFileName, string cParms, string cDir, integer nShowCmd
     declare integer FindWindow in win32api string cNull, string cWinName 
    w_hWnd = FindWindow(0,_screen.caption)
    this.w_SHELLEXEC = ShellExecute(w_hWnd,"open",alltrim(this.w_cLink),alltrim(this.w_cParms),sys(5)+curdir(),1)
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione allegato/i
    if SUBSTR( CHKPECLA( this.w_CLASDOCU, i_CODUTE), 1, 1 ) = "S"
      if not empty(this.w_NOMEFILE)
        do case
          case this.w_IDMODALL="E"
            w_ERRORE = .F.
            if cp_fileexist(alltrim(tempadhoc())+"\"+alltrim(this.w_NOMEFILE))
              w_ErrorHandler = on("ERROR")
              on error w_ERRORE = .T.
              delete file (alltrim(tempadhoc())+"\"+alltrim(this.w_NOMEFILE))
              on error &w_ErrorHandler
            endif
            if NOT w_ERRORE
              FileEds("V", this.w_CLASDOCU, this.w_ORIGFILE, alltrim(tempadhoc())+"\"+alltrim(this.w_NOMEFILE))
            endif
            this.w_PATHFILE = tempadhoc() + "\" 
            this.w_TmpPat = this.w_PATHFILE+ this.w_NOMEFILE
          case this.w_IDMODALL="I"
            this.w_PATHFILE = alltrim(tempadhoc()) + "\"
            this.w_TmpPat = this.w_PATHFILE+ alltrim(this.w_NOMEFILE)
            FileInfinity("W", this.w_KEYINDIC, this.w_TmpPat, this.w_COPATHDO)
          otherwise
            * --- Percorso di origine ...
            this.w_TmpPat = alltrim(tempadhoc()) + "\" + IIF(empty(this.w_ORIGFILE) ,this.w_NOMEFILE,this.w_ORIGFILE)
        endcase
        * --- Percorso di origine ...
        if Not Empty(JUSTEXT(this.w_DESCRFILE)) AND !this.w_ISALT
          this.w_DESCRFILE = STRTRAN(this.w_DESCRFILE,"."+JUSTEXT(this.w_DESCRFILE),"")
        endif
        do case
          case this.w_AENOMEAL="D"
            * --- Elimino dalla descrizione i caratteri non validi e imposto l'estensione uguale a quella originale
            this.w_TmpPat = ADDBS(tempadhoc()) + FORCEEXT(ChrTran(this.w_DESCRFILE, '*\/:?"<>.', "_________"), JUSTEXT(this.w_NOMEFILE))
          case this.w_AENOMEAL="A"
            * --- Invio file con nome uguale al nome del file archiviato
            this.w_TmpPat = ADDBS(tempadhoc()) + this.w_NOMEFILE
          otherwise
            * --- Invio standard (invio con nome file uguale al file originale)
            if Alltrim(this.w_NOMEFILE) <> Alltrim(justfname(this.w_ORIGFILE)) AND !this.w_ISALT
              this.w_TmpPat = ADDBS(tempadhoc()) + IIF(empty(this.w_ORIGFILE),this.w_NOMEFILE,Alltrim(justfname(this.w_ORIGFILE)))
            else
              this.w_TmpPat = ADDBS(tempadhoc()) + IIF(empty(this.w_ORIGFILE),this.w_NOMEFILE,FORCEEXT(ChrTran(JUSTSTEM(this.w_ORIGFILE), '*\/:?"<>.', "_________"), JUSTEXT(this.w_NOMEFILE)))
            endif
        endcase
        if file(this.w_PATHFILE + this.w_NOMEFILE ) and (this.oParentObject.w_SELMULT or not this.w_OperPassata=="A")
          * --- Se modalit� diversa da 'E', cio� da Extended document server', procedo alla copia dell'allegato nella cartella temporanea
          *     altrimenti no, poich� � gi� stato inserito dalla funzione 'FileEds()'.'
          * --- Copia il file originale nella temp
          if INLIST(this.w_IDMODALL,"E","I")
            if this.w_PATHFILE + this.w_NOMEFILE <> this.w_TmpPat
              w_ERRORE = .F.
              if cp_fileexist(alltrim(this.w_TmpPat))
                w_ErrorHandler = on("ERROR")
                on error w_ERRORE = .T.
                delete file (alltrim(this.w_TmpPat))
                on error &w_ErrorHandler
              endif
              if NOT w_ERRORE
                RENAME (this.w_PATHFILE + this.w_NOMEFILE ) to (this.w_TmpPat)
              endif
            endif
          else
            COPY FILE (this.w_PATHFILE + this.w_NOMEFILE ) to (this.w_TmpPat)
          endif
          if FILE(this.w_TmpPat)
            Dimension ARRAYALLEG[NumAlleg]
            ARRAYALLEG(NumAlleg) = this.w_TmpPat
            NumAlleg = NumAlleg + 1
            i_EMAILSUBJECT = i_EMAILSUBJECT+" [" +IIF(empty(this.w_ORIGFILE),this.w_NOMEFILE,this.w_ORIGFILE)+"]"
          endif
        endif
      endif
    else
      ah_ErrorMsg("Non si possiedono le autorizzazioni per eseguire l'operazione richiesta %0Diritti necessari: visualizzazione")
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia file/s selezionato/i
    if not empty(this.w_NOMEFILE)
      do case
        case this.w_IDMODALL="E"
          FileEds("V", this.w_CLASDOCU, this.w_ORIGFILE, alltrim(tempadhoc())+"\"+alltrim(this.w_NOMEFILE))
          this.oParentObject.w_GFPATHFILE = ADDBS(tempadhoc())
          this.w_PATHFILE = ADDBS(tempadhoc())
        case this.w_IDMODALL="I"
          FileInfinity("W", this.w_KEYINDIC, alltrim(tempadhoc())+"\"+alltrim(this.w_NOMEFILE), this.w_COPATHDO)
          this.w_PATHFILE = ADDBS(tempadhoc())
      endcase
      if SUBSTR( CHKPECLA( this.w_CLASDOCU, i_CODUTE), 1, 1 ) = "S"
        if not empty(this.w_DesPat)
          * --- Prima verifica se il file di destinazione esiste gi�
          if empty(this.w_ORIGFILE) or this.w_IDMODALL="E"
            this.w_GFILE = this.w_NOMEFILE
          else
            this.w_GFILE = this.w_ORIGFILE
          endif
          if not(File(this.w_DesPat+IIF(empty(this.w_ORIGFILE) or this.w_IDMODALL="E",this.w_NOMEFILE,this.w_ORIGFILE))) or ah_YesNo("Il file [%1] � gi� presente nella cartella [%2] %0Sovrascrivo il file esistente?","",alltrim(this.w_GFILE),alltrim(this.w_DesPat))
            * --- Percorso di origine ...
            this.w_TmpPat = this.w_PATHFILE + this.w_NOMEFILE
            * --- Esegue la copia del file richiesto ...
            if file(this.w_TMPPAT)
              private w_ERRORE,w_ErrorHandler
              w_ErrorHandler = on("ERROR")
              w_ERRORE = .F.
              on error w_ERRORE = .T.
              copy file (this.w_TMPPAT) to (this.w_DesPat+IIF(empty(this.w_ORIGFILE) or this.w_IDMODALL="E",this.w_NOMEFILE,JUSTFNAME(this.w_ORIGFILE)))
              on error &w_ErrorHandler
              if w_ERRORE
                ah_ErrorMSG("Impossibile estrarre e/o copiare il documento archiviato")
              else
                ah_MSG("� stato estratto e copiato il file [%1] nella cartella:%2",.t.,.f.,.f.,rtrim(iif(empty(this.w_ORIGFILE) or this.w_IDMODALL="E",this.w_NOMEFILE,this.w_ORIGFILE)),rtrim(this.w_DesPat))
              endif
            endif
          endif
        endif
      else
        ah_ErrorMsg("Non si possiedono le autorizzazioni per eseguire l'operazione richiesta %0Diritti necessari: visualizzazione")
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione\Aggiornamento file e indici
    if this.w_Operazione=="D"
      if not empty(this.w_NOMEFILE) and not empty(this.w_KEYINDIC)
        this.w_ImgPat = this.w_PATHFILE
        this.w_TmpPat = alltrim(this.w_ImgPat)+alltrim(this.w_NOMEFILE)
        * --- Prima verifica se era solo un collegamento ... lo tratta in modo particolare ...
        if ! this.oParentObject.w_SELMULT
          do case
            case this.w_IDMODALL=="F"
              this.w_Ok = AH_YESNO("Elimino il file %1?","",alltrim(this.w_NOMEFILE))
              this.w_DelFile = .T.
            case this.w_IDMODALL=="L"
              this.w_Ok = AH_YESNO("Elimino il collegamento al file %1?%0Il file %1 non sar� cancellato.","",alltrim(this.w_NOMEFILE))
              this.w_DelFile = .F.
            case this.w_IDMODALL=="E"
              this.w_Ok = AH_YESNO("Elimino il file %1 dall'archivio E.D.S.?","",alltrim(this.w_NOMEFILE))
              this.w_DelFile = .T.
          endcase
        else
          if this.w_IDMODALL=="L"
            * --- Se collegamento non cancello mai il file
            this.w_DelFile = .F.
          else
            this.w_DelFile = .T.
          endif
        endif
        if this.w_Ok
          if this.w_DelFile
            if ! this.oParentObject.w_SELMULT
              this.w_NOMEFILEORIG = NVL(this.oParentObject.w_GFORIGFILE," ")
            else
              Select(this.w_CURSORE)
              this.w_NOMEFILEORIG = NVL(GFORIGFILE," ")
            endif
            do case
              case this.w_IDMODALL=="E"
                * --- Legge da classe documentale i dati necessari
                FileEds("D", this.w_CLASDOCU, left(this.w_NOMEFILEORIG,16))
              case this.w_IDMODALL=="I"
              otherwise
                delete file (this.w_TmpPat)
            endcase
          endif
          if this.w_IDWEBAPP$"AS"
            delete file (alltrim(this.w_COPATHDO)+JUSTFNAME(this.w_IDWEBFIL))
          endif
          * --- Gestione indici documenti (chiave w_GFKEYINDIC)
          * --- begin transaction
          cp_BeginTrs()
          * --- Try
          local bErr_055B1E90
          bErr_055B1E90=bTrsErr
          this.Try_055B1E90()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            AH_ERRORMSG("Impossibile aggiornare indici documenti",48)
          endif
          bTrsErr=bTrsErr or bErr_055B1E90
          * --- End
        endif
        if this.w_Ok and .f.
          * --- Distingue ...
          this.w_DelIndex = .T.
          if this.w_IDMODALL=="L"
            this.w_DelFile = AH_YESNO("Il file selezionato � stato archiviato in modalit� <collegamento>, si desidera cancellare anche il file di origine ?")
          endif
        endif
      endif
    else
      * --- Eseguo Sblocco\Blocco indici
      this.w_SBLOCBLOC = iif(this.w_Operazione=="B","S","N")
      * --- Write into PROMINDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PROMINDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IDFLPROV ="+cp_NullLink(cp_ToStrODBC(this.w_SBLOCBLOC),'PROMINDI','IDFLPROV');
            +i_ccchkf ;
        +" where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
               )
      else
        update (i_cTable) set;
            IDFLPROV = this.w_SBLOCBLOC;
            &i_ccchkf. ;
         where;
            IDSERIAL = this.w_KEYINDIC;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc
  proc Try_055B1E90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from PRODINDI
    i_nConn=i_TableProp[this.PRODINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
             )
    else
      delete from (i_cTable) where;
            IDSERIAL = this.w_KEYINDIC;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PROMINDI
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
             )
    else
      delete from (i_cTable) where;
            IDSERIAL = this.w_KEYINDIC;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia file per invio web
    if LOWER(this.w_TmpPat)<>LOWER(this.w_COPATHDO+JUSTFNAME(this.w_IDWEBFIL))
      w_ERRORE = .F.
      w_ErrorHandler = on("ERROR")
      on error w_ERRORE = .T.
      if cp_fileexist(LOWER((this.w_COPATHDO+JUSTFNAME(this.w_IDWEBFIL))))
        delete file (alltrim(this.w_COPATHDO)+JUSTFNAME(this.w_IDWEBFIL))
      endif
      COPY FILE LOWER((this.w_TmpPat)) to LOWER((this.w_COPATHDO+JUSTFNAME(this.w_IDWEBFIL)))
      on error &w_ErrorHandler
      if w_ERRORE
        AH_ERRORMSG("Impossibile copiare il documento %2 nella cartella di destinazione %1",48,"",alltrim(this.w_COPATHDO),JUSTFNAME(this.w_NOMEFILE))
      endif
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_NOMEFILE = Alltrim(GFNOMEFILE)
    this.w_PATHFILE = alltrim(GFPATHFILE)
    this.w_ORIGFILE = alltrim(GFORIGFILE)
    this.w_KEYINDIC = alltrim(GFKEYINDIC)
    this.w_CLASDOCU = alltrim(GFCLASDOCU)
    this.w_DESCRFILE = Alltrim(GFDESCFILE)
    this.w_IDMODALL = alltrim(IDMODALL)
    if this.w_RETVAL=3
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      if CHECKPRINTTO("." + justext(this.w_NOMEFILE))
        * --- Read from PROMCLAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CDMODALL"+;
            " from "+i_cTable+" PROMCLAS where ";
                +"CDCODCLA = "+cp_ToStrODBC(this.w_CLASDOCU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CDMODALL;
            from (i_cTable) where;
                CDCODCLA = this.w_CLASDOCU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from PROMINDI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IDTIPALL,IDCLAALL"+;
            " from "+i_cTable+" PROMINDI where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_GFKEYINDIC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IDTIPALL,IDCLAALL;
            from (i_cTable) where;
                IDSERIAL = this.oParentObject.w_GFKEYINDIC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPOALLE = NVL(cp_ToDate(_read_.IDTIPALL),cp_NullValue(_read_.IDTIPALL))
          this.w_CLASSEALL = NVL(cp_ToDate(_read_.IDCLAALL),cp_NullValue(_read_.IDCLAALL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_IDMODALL="I"
          this.w_NOMEFILE = FileInfinity("V", this.w_KEYINDIC)
          this.w_PATHFILE = justpath(this.w_NOMEFILE)
          this.w_NOMEFILE = justfname(this.w_NOMEFILE)
        endif
        ah_msg("Generazione file PDF per %1",.t.,.f.,.f.,this.w_NOMEFILE)
        * --- Generazione file PDF ed invio tramite email
        w_pdfname=" "
        this.w_RET = Crea_Invio_Pdf(this.w_PATHFILE, this.w_NOMEFILE, this.w_CLASDOCU, alltrim(this.w_ORIGFILE), this.w_MODALLEG,@w_PDFNAME, iif(this.w_retval=2,this.w_PATHFILE,tempadhoc()))
        if this.w_RETVAL=2 and (cp_FileExist(w_PDFNAME) or this.w_ISALT)
          * --- Creazione indice con PDF
          private L_SerIndex
          L_SerIndex=""
          if this.w_ISALT
            this.w_Chiave = this.oParentObject.w_CODPRAT
          endif
          if this.w_ISALT
            this.w_NewDescr = ALLTRIM(this.w_DESCRFILE)+" (PDF)"
          else
            this.w_NewDescr = this.w_DESCRFILE
          endif
          if this.w_ISALT
            * --- Per PDF molto grandi il documento pu� non essere ancora definitivo
            *     Prima di procedere ci accertiamo che la creazione del PDF sia terminata
            * --- Massimo 30 cicli, se necessario � possibile definire nel CNF (tramite la g_CREA_PDF_DELAY) un numero differente di giri
            this.w_NumGiri = 0
            this.w_DELAY = IIF( vartype(g_CREA_PDF_DELAY)="N" And g_CREA_PDF_DELAY>0 , g_CREA_PDF_DELAY, 30)
            w_OKOPEN = .F. 
 w_ErrorHandler = on("ERROR") 
 on error w_OKOPEN = .F.
            do while !w_OKOPEN AND this.w_NumGiri < this.w_DELAY
              wait wind "" timeout 1
              w_OKOPEN=.T.
              HANDLE=FOPEN(w_pdfname,2) 
 FCLOSE(HANDLE)
              this.w_NumGiri = this.w_NumGiri + 1
              if HANDLE<0
                * --- Non � riuscito ad aprire il PDF - ancora non � conclusa la generazione
                w_OKOPEN=.F.
                if this.w_NumGiri=this.w_DELAY AND AH_YESNO("Attenzione! La procedura di generazione del PDF potrebbe non essere conclusa. Attendere ancora?")
                  this.w_NumGiri = 0
                endif
              endif
            enddo
            on error &w_ErrorHandler
          endif
          Private L_ArrParam
          dimension L_ArrParam(30)
          L_ArrParam(1)="AR"
          L_ArrParam(2)=w_pdfname
          L_ArrParam(3)=this.oParentObject.w_SELCODCLA
          L_ArrParam(4)=i_CODUTE
          L_ArrParam(5)=this.oParentObject
          L_ArrParam(6)=this.w_ARCHIVIO
          L_ArrParam(7)=this.w_CHIAVE
          L_ArrParam(8)=this.w_NewDescr
          L_ArrParam(9)=this.w_TESTO
          L_ArrParam(10)=.T.
          L_ArrParam(11)=.T.
          L_ArrParam(12)=" "
          L_ArrParam(13)=" "
          L_ArrParam(14)=this.w_GestAtt
          L_ArrParam(15)=this.w_TIPOALLE
          L_ArrParam(16)=this.w_CLASSEALL
          L_ArrParam(17)=" "
          L_ArrParam(18)=" "
          L_ArrParam(19)="C"
          L_ArrParam(22)=this.oParentObject.w_GFPATHFILE
          GSUT_BBA(this,@L_ArrParam, @L_SerIndex)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_RET and FILE(w_PDFNAME)
          Dimension ARRAYALLEG[NumAlleg]
          ARRAYALLEG(NumAlleg) = w_PDFNAME
          NumAlleg = NumAlleg + 1
          i_EMAILSUBJECT = i_EMAILSUBJECT+" [" +JUSTFNAME(w_PDFNAME)+"] "
        endif
      endif
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Nel caso di archiviazione su Infinity Easy mi preparo xml descrittore
    if empty(this.w_UploadDocumentsXML)
      * --- Se non esiste ancora xml descrittore lo creo
      this.w_UploadDocumentsXML = this.w_COPATHDO+SYS(2015)+"_UPLOADDOCUMENTS.zar"
      this.w_OBJXML = createobject("Msxml2.DOMDocument.4.0")
      * --- Encoding usato dalla classe Msxml2.DOMDocument. Es. utf-8, iso-8859-1, iso-8859-15
      *     utf-8: tutti i caratteri
      *     iso-8859-15 (manca il carattere TM)
      *     iso-8859-1 (manca il carattere � )
      if TYPE("g_Encoding_Msxml2_DOMDocument")<>"C"
        public g_Encoding_Msxml2_DOMDocument 
 g_Encoding_Msxml2_DOMDocument="utf-8"
      endif
      this.w_INTESTAZIONE = 'version="1.0" encoding="'+g_Encoding_Msxml2_DOMDocument+'"'
      this.w_OBJXML.appendChild(this.w_OBJXML.createProcessingInstruction("xml", this.w_INTESTAZIONE))     
      this.w_sNode = this.w_OBJXML.createNode(1,"UploadDocuments","")
      this.w_sNode.setAttribute("applicationId", InfinityApplicationID())     
      this.w_sNode.setAttribute("ConsolidationDate","01-01-1900")     
      this.w_sNode.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")     
      this.w_OBJXML.appendChild(this.w_sNode)     
    endif
    if g_REVI="S"
      * --- Read from PROMCLAS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PROMCLAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CDCLAINF"+;
          " from "+i_cTable+" PROMCLAS where ";
              +"CDCODCLA = "+cp_ToStrODBC(this.w_CLASDOCU);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CDCLAINF;
          from (i_cTable) where;
              CDCODCLA = this.w_CLASDOCU;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CDCLAINF = NVL(cp_ToDate(_read_.CDCLAINF),cp_NullValue(_read_.CDCLAINF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      this.w_CDCLAINF = this.w_CLASDOCU
    endif
    * --- Read from PROMINDI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ID_TESTO"+;
        " from "+i_cTable+" PROMINDI where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.w_KEYINDIC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ID_TESTO;
        from (i_cTable) where;
            IDSERIAL = this.w_KEYINDIC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ID_TESTO = NVL(cp_ToDate(_read_.ID_TESTO),cp_NullValue(_read_.ID_TESTO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ID_TESTO = nvl(this.w_ID_TESTO,"")
    this.w_sNode = this.w_OBJXML.createNode(1,"Add_UploadDocuments","")
    this.w_sNode.setAttribute("ExternalCode", alltrim(this.w_KEYINDIC))     
    this.w_sNode.setAttribute("ReferenceName", lower(alltrim(this.w_IDWEBFIL)))     
    this.w_sNode.setAttribute("LogicalName", iif(not empty(this.w_ORIGIDWEBFIL), alltrim(this.w_ORIGIDWEBFIL), iif(empty(alltrim(this.w_NOMEFILE)), alltrim(this.w_IDWEBFIL), alltrim(this.w_NOMEFILE))))     
    this.w_sNode.setAttribute("ClassDoc", alltrim(this.w_CDCLAINF))     
    this.w_sNode.setAttribute("PathOnClient", this.w_COPATHDO+alltrim(this.w_IDWEBFIL))     
    this.w_sNode.setAttribute("Description", this.w_DESCRFILE)     
    this.w_sNode.setAttribute("Notes", this.w_ID_TESTO)     
    this.w_sNode.setAttribute("Crc", SYS(2007, FILETOSTR(this.w_COPATHDO+alltrim(this.w_IDWEBFIL)), -1, 1))     
    this.w_OBJXML.childNodes.Item(1).appendChild(this.w_sNode)     
    * --- Inserisce attributi e relativo valore
    this.w_COUNTER = 1
    this.w_OLDSETCENTURY = SET("CENTURY")
    if this.w_OLDSETCENTURY <> "ON"
      SET CENTURY ON
    endif
    * --- Select from GSUT1BFG
    do vq_exec with 'GSUT1BFG',this,'_Curs_GSUT1BFG','',.f.,.t.
    if used('_Curs_GSUT1BFG')
      select _Curs_GSUT1BFG
      locate for 1=1
      do while not(eof())
      this.w_CDCODATT = CDATTINF
      this.w_IDVALATT = IDVALATT
      this.w_CDTIPATT = CDTIPATT
      if g_REVI="S"
        * --- Verifico se l'attributo � applicabile ed eseguo l'eventuale trascodifica
        this.w_IDVALATT = InfinityAttribValue(this.w_CDCODATT, this.w_IDVALATT)
      endif
      if not empty(this.w_IDVALATT)
        if this.w_CDTIPATT="D"
          this.w_IDVALATT = DTOC(CTOD(this.w_IDVALATT))
        endif
        if this.w_COUNTER=1
          this.w_sCHILD = this.w_OBJXML.createNode(1,"BO_Attribute","")
          this.w_sNode.appendChild(this.w_sCHILD)     
        endif
        this.w_sCHILDDETT = this.w_OBJXML.createNode(1,"Attribute","")
        this.w_sCHILD.appendChild(this.w_sCHILDDETT)     
        this.w_sCHILDDETTPROP = this.w_OBJXML.createNode(1,"AttributeName","")
        this.w_sCHILDDETTPROP.text = alltrim(this.w_CDCODATT)
        this.w_sCHILDDETT.appendChild(this.w_sCHILDDETTPROP)     
        this.w_sCHILDDETTPROP = this.w_OBJXML.createNode(1,"AttributeValue","")
        this.w_sCHILDDETTPROP.text = alltrim(this.w_IDVALATT)
        this.w_sCHILDDETT.appendChild(this.w_sCHILDDETTPROP)     
        this.w_sCHILDDETTPROP = this.w_OBJXML.createNode(1,"Crc","")
        this.w_sCHILDDETTPROP.text = SYS(2007, alltrim(this.w_IDVALATT), -1, 1)
        this.w_sCHILDDETT.appendChild(this.w_sCHILDDETTPROP)     
        * --- Incrementa contatore
        this.w_COUNTER = this.w_COUNTER + 1
      endif
        select _Curs_GSUT1BFG
        continue
      enddo
      use
    endif
    if this.w_OLDSETCENTURY <> "ON"
      SET CENTURY OFF
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_Operazione,w_Archivio,w_Chiave,w_Periodo,w_GestAtt,w_bPEC)
    this.w_Operazione=w_Operazione
    this.w_Archivio=w_Archivio
    this.w_Chiave=w_Chiave
    this.w_Periodo=w_Periodo
    this.w_GestAtt=w_GestAtt
    this.w_bPEC=w_bPEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='LIB_IMMA'
    this.cWorkTables[2]='PROMCLAS'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='PROMINDI'
    this.cWorkTables[5]='PRODINDI'
    this.cWorkTables[6]='PRODCLAS'
    this.cWorkTables[7]='PRO_PERM'
    this.cWorkTables[8]='PROMODEL'
    this.cWorkTables[9]='*TMP_PRAT'
    this.cWorkTables[10]='LOGSININ'
    this.cWorkTables[11]='PAR_ALTE'
    this.cWorkTables[12]='CONTROPA'
    this.cWorkTables[13]='DIPENDEN'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_gsut_kiz')
      use in _Curs_gsut_kiz
    endif
    if used('_Curs_gsut1kiz')
      use in _Curs_gsut1kiz
    endif
    if used('_Curs_GSUT1BFG')
      use in _Curs_GSUT1BFG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsut_bfg
  * ===============================================================================
    * AH_CreateFolder() - Crea la cartella passata come parametro
    *
    Function AH_CreateFolder(pDirectory)
    private lOldErr,lRet
    * On Error
    lOldErr=ON("Error")
    * Init Risultato
    lRet = True
    * Crea
    on error lRet=False
    *
    pDirectory=JUSTPATH(ADDBS(pDirectory))
    *
    MD (pDirectory)
    *
    if not(Empty(lOldErr))
      on error &lOldErr
    else
      on error
    endif   
    * 
    if not(lRet)
      = Ah_ErrorMsg("Impossibile creare la cartella %1",16,,pDirectory)
    endif
    * Risultato
    Return (lRet)
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Operazione,w_Archivio,w_Chiave,w_Periodo,w_GestAtt,w_bPEC"
endproc
