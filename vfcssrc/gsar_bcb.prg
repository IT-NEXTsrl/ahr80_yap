* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bcb                                                        *
*              Controllo cancellazione ns. banca                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-26                                                      *
* Last revis.: 2012-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bcb",oParentObject)
return(i_retval)

define class tgsar_bcb as StdBatch
  * --- Local variables
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da GSAR_ACB: inibisce la cancellazione se il la banca  � presente nel campo ns. banca nei documenti.
    * --- Select from DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_MAST ";
          +" where MVCODBA2="+cp_ToStrODBC(this.oParentObject.w_BACODBAN)+"";
           ,"_Curs_DOC_MAST")
    else
      select * from (i_cTable);
       where MVCODBA2=this.oParentObject.w_BACODBAN;
        into cursor _Curs_DOC_MAST
    endif
    if used('_Curs_DOC_MAST')
      select _Curs_DOC_MAST
      locate for 1=1
      do while not(eof())
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_MsgFormat("La banca che si intende cancellare � presente nei documenti%0Impossibile cancellare")
      i_retcode = 'stop'
      return
        select _Curs_DOC_MAST
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
