* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bnr                                                        *
*              Documenti nuovo record                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_296]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2018-06-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bnr",oParentObject,m.pOpz)
return(i_retval)

define class tgsve_bnr as StdBatch
  * --- Local variables
  pOpz = space(1)
  w_PADRE = .NULL.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_QTAUM1 = 0
  w_QTAMOV = 0
  w_QTAORI = 0
  w_QTAEVA = 0
  w_QTARIM = 0
  w_QTAIMP = 0
  w_QTADEL = 0
  w_ROWNUM = 0
  w_RECO = 0
  w_MESS = space(10)
  w_PADRE = .NULL.
  w_RIFESC = space(10)
  w_OSERRIF = space(10)
  w_SRSERRIF = space(10)
  w_CAORIF = 0
  w_ODLRIF = space(10)
  w_ODLRIF = space(10)
  w_CLARIF = space(2)
  w_STSERRIF = space(10)
  w_PADREUPD = .f.
  w_STATO = space(100)
  w_CESP = .f.
  w_TRIG = 0
  w_DTSERIAL = space(10)
  w_EVSERIAL = space(10)
  w_EV__ANNO = space(4)
  w_ROWNUM = 0
  w_ROWORD = 0
  w_SALDO = 0
  w_RESERIAL = space(10)
  w_RETIPDOC = space(5)
  w_RENUMDOC = 0
  w_REALFDOC = space(10)
  w_REDATDOC = ctod("  /  /  ")
  w_REKEYSAL = space(20)
  w_RECODMAG = space(5)
  w_RECODMAT = space(5)
  w_REQTASAL = 0
  w_REFLIMPE = space(1)
  w_REF2IMPE = space(1)
  w_REFLORDI = space(1)
  w_REF2ORDI = space(1)
  w_TUMCAL = space(50)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_TQTAIM1 = 0
  w_TQTAIMP = 0
  w_FLERIF = space(1)
  w_TmpN = 0
  w_RIFDOC = space(10)
  w_ATOGGETT = space(254)
  w_DATATT = space(10)
  w_AppMess = space(1)
  w_RECNUM = 0
  w_NUMMAG = 0
  w_MESS_CONS = space(250)
  w_FLDEL = .f.
  w_SRV = space(1)
  w_NR = space(5)
  w_SERFAT = space(10)
  w_CURNAME = space(10)
  w_SERDOCESP = space(10)
  w_TIPDOC = space(5)
  w_TIPIMB = space(1)
  w_TESTART = space(20)
  w_KITIMB = space(1)
  * --- WorkFile variables
  DOC_DETT_idx=0
  MOVIMATR_idx=0
  ADDE_SPE_idx=0
  RIGHEEVA_idx=0
  DOC_MAST_idx=0
  DET_DIFF_idx=0
  INCDCORR_idx=0
  TIP_DOCU_idx=0
  ART_ICOL_idx=0
  OFF_ATTI_idx=0
  DISDFAEL_idx=0
  DET_PIAS_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Preliminari caricamento di un Nuovo Documento o inizio Variazione (da GSVE_MDV, GSAC_MDV, GSOR_MDV)
    * --- AGGIUNTA: Check w_MVFLSCAF Changed , esegue il refresh delle Rate/Scadenze
    * --- Evento: N = New Record, 
    *                  E = Ecp Edit   
    *                  V-Q = Edit Started  
    *                  F = MVFLSCAF Changed
    *                  P = MVCODPAG Changed
    *                  S = stampa da GSVE_MDV se IsAlt()
    *                  L = tasto destro o stampa da GSAL_KRD
    *                  Q= delete start
    *                
    do case
      case this.pOpz="L"
        * --- tasto destro o stampa da GSAL_KRD
      case this.pOpz="S"
        * --- stampa da GSVE_MDV se IsAlt()
        this.w_PADRE = this.oParentObject.w_MoviCaller
      otherwise
        * --- da GSVE_MDV, GSAC_MDV o GSOR_MDV
        this.w_PADRE = this.oParentObject
    endcase
    * --- Istanzio Oggetto Messaggio Incrementale
    this.w_oMESS=createobject("ah_message")
    if this.pOpz<>"R" AND this.pOpz<>"Q"
      this.oParentObject.w_HASEVENT = .T.
    endif
    do case
      case this.pOpz="F" or this.pOpz="P" 
        * --- Refresh sulle rate/Scadenze al Chech Scadenze Fissate
        this.oParentObject.GSVE_MRT.mCalc(.T.)     
        if this.pOpz="F" 
          this.oParentObject.GSVE_MRT.oPgFrm.Page1.oPag.oBody.SetFullFocus()     
        endif
        i_retcode = 'stop'
        return
      case this.pOpz="N"
        if this.oParentObject.w_MVANNPRO="    "
          this.oParentObject.w_MVNUMEST = 0
        endif
        if NOT ((this.oParentObject.w_MVFLVEAC="V" AND this.oParentObject.w_MVPRD<>"NN") OR (this.oParentObject.w_MVFLVEAC="A" AND this.oParentObject.w_MVCLADOC="OR") OR (this.oParentObject.w_MVFLVEAC="A" AND this.oParentObject.w_MVCLADOC<>"OR" AND this.oParentObject.w_MVPRD $ "DV-IV"))
          this.oParentObject.w_MVNUMDOC = 0
        endif
      case (this.pOpz="E" AND ( UPPER(this.oParentObject.w_HASEVCOP)="ECPEDIT" or Upper( this.oParentObject.w_HASEVCOP ) ="ECPDELETE" ) ) OR this.pOpz="S" OR this.pOpz="L"
        do case
          case this.pOpz="S"
            this.w_STATO = ah_Msgformat("%0Attenzione")
          case Upper( this.oParentObject.w_HASEVCOP ) ="ECPEDIT"
            this.w_STATO = ah_Msgformat("%0Impossibile modificare")
          otherwise
            this.w_STATO = ah_Msgformat("%0Impossibile eliminare")
        endcase
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        do case
          case Upper( this.oParentObject.w_HASEVCOP ) ="ECPEDIT"
            * --- Setta flag per non rendere Editabile il Cliente/Fornitore
            this.oParentObject.w_FLEDIT = " "
            if this.oParentObject.w_HASEVENT And g_CESP="S" AND this.oParentObject.w_ASSCES<>"N"
              this.w_CESP = .F.
              * --- Select from DOC_DETT
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select Min ( MVCODCES ) As CESPITE  from "+i_cTable+" DOC_DETT ";
                    +" where MVSERIAL="+cp_ToStrODBC(this.oParentObject.w_MVSERIAL)+"";
                     ,"_Curs_DOC_DETT")
              else
                select Min ( MVCODCES ) As CESPITE from (i_cTable);
                 where MVSERIAL=this.oParentObject.w_MVSERIAL;
                  into cursor _Curs_DOC_DETT
              endif
              if used('_Curs_DOC_DETT')
                select _Curs_DOC_DETT
                locate for 1=1
                do while not(eof())
                this.w_CESP = NOT EMPTY(NVL(_Curs_DOC_DETT.CESPITE,SPACE(20))) 
                  select _Curs_DOC_DETT
                  continue
                enddo
                use
              endif
              if this.w_CESP
                this.w_MESS = ah_Msgformat("Il documento � associato a cespiti/movimenti cespiti%0Potrebbe non essere possibile confermare le eventuali modifiche")
              endif
            endif
          case Upper( this.oParentObject.w_HASEVCOP ) ="ECPDELETE" AND NOT EMPTY(this.oParentObject.w_MVSERIAL)
            if this.oParentObject.w_HASEVENT And this.oParentObject.w_MVCLADOC $ "FA-NC"
              this.w_TRIG = 0
              * --- Controllo che non vi siano righe di spese gia abbinate 
              * --- Select from DOC_DETT
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" DOC_DETT ";
                    +" where MV_FLAGG='S' And MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL)+"";
                     ,"_Curs_DOC_DETT")
              else
                select Count(*) As Conta from (i_cTable);
                 where MV_FLAGG="S" And MVSERIAL = this.oParentObject.w_MVSERIAL;
                  into cursor _Curs_DOC_DETT
              endif
              if used('_Curs_DOC_DETT')
                select _Curs_DOC_DETT
                locate for 1=1
                do while not(eof())
                this.w_TRIG = Nvl ( _Curs_DOC_DETT.CONTA , 0 )
                  select _Curs_DOC_DETT
                  continue
                enddo
                use
              endif
              if this.w_TRIG>0
                this.w_MESS = ah_Msgformat("Impossibile eliminare%0Il documento contiene righe di spesa gia ripartite su altre registrazioni")
                this.oParentObject.w_HASEVENT = .F.
              endif
            endif
            if this.oParentObject.w_HASEVENT
              * --- Controllo che non vi siano righe di spese gia abbinate 
              * --- Read from ADDE_SPE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "RLNUMDOC"+;
                  " from "+i_cTable+" ADDE_SPE where ";
                      +"RLSERDOC = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  RLNUMDOC;
                  from (i_cTable) where;
                      RLSERDOC = this.oParentObject.w_MVSERIAL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_TRIG = NVL(cp_ToDate(_read_.RLNUMDOC),cp_NullValue(_read_.RLNUMDOC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_TRIG>0
                this.w_MESS = ah_Msgformat("Impossibile eliminare%0Il documento contiene righe sulle quali sono state ripartite delle fatture di spesa")
                this.oParentObject.w_HASEVENT = .F.
              endif
            endif
            if this.oParentObject.w_HASEVENT AND g_VEFA="S"
              * --- Verifico se � un documento generato dal piano di spedizione
              * --- Read from DET_PIAS
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DET_PIAS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DET_PIAS_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CPROWNUM"+;
                  " from "+i_cTable+" DET_PIAS where ";
                      +"DPSERDOC = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CPROWNUM;
                  from (i_cTable) where;
                      DPSERDOC = this.oParentObject.w_MVSERIAL;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_TRIG = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_TRIG>0
                this.w_MESS = "Il documento � stato generato da un piano di spedizione%0Confermi l'eliminazione?"
                this.oParentObject.w_HASEVENT = ah_YesNo(this.w_MESS)
                this.w_MESS = ""
              endif
            endif
            if this.oParentObject.w_HASEVENT
              * --- Lancio GSVE_BCK ( controlli all'F10 ) per verificare quelli validi anche in
              *     cancellazione
              this.oParentObject.w_RESCHK = 0
              this.w_PADRE.NotifyEvent("ChkFinali")     
              * --- Se qualcosa va storto annullo eventuali messaggi di warning determinati 
              *     in precedenza (Pag2)
              if this.oParentObject.w_RESCHK=-1
                this.w_MESS = ""
                this.oParentObject.w_HASEVENT = .F.
              endif
            endif
            * --- Se il documento � stato aperto premendo Dettagli dal Dettaglio Fatture differite in Modifica,
            *     Non � possibile la cancellazione se il documento � quello presente nel Piano di Fatturazione.
            *     w_Det_Diff viene valorizzato nel batch GSVE_BZM con il Seriale del documento presente nel Piano
            *     Nei documenti � invece definito nell'area manuale Declare Variables ( solo GSVE_MDV poich� non ha senso per gli altri)
            if this.w_PADRE.Class="Tgsve_mdv" And this.oParentObject.w_HASEVENT And this.oParentObject.w_Det_Diff = this.oParentObject.w_MVSERIAL
              this.w_MESS = ah_Msgformat("Per eliminare il documento premere F6 sulla riga del dettaglio fatture differite")
              this.oParentObject.w_HASEVENT = .F.
            endif
            if this.oParentObject.w_HASEVENT And g_COMM="S" And NOT EMPTY(this.oParentObject.w_MVMOVCOM)
              this.w_MESS = "Documento generato da tempificazione%0L'attivit� relativa sar� nuovamente tempificabile%0Confermi ugualmente?"
              this.oParentObject.w_HASEVENT = ah_YesNo(this.w_MESS)
              this.w_MESS = ""
            endif
            if this.oParentObject.w_HASEVENT And g_OFFE="S" AND this.oParentObject.w_MVFLOFFE="S"
              * --- Toglie Riferimenti a Offerta
              this.w_MESS = "Documento generato da"+ iif(Isalt()," preventivo confermato"," offerta confermata") +"%0La sua cancellazione riporter�" +iif(Isalt()," il preventivo"," l'offerta") + " in stato 'in corso';%0Confermi ugualmente?"
              this.oParentObject.w_HASEVENT = ah_YesNo(this.w_MESS)
              this.w_MESS = ""
            endif
            if this.oParentObject.w_HASEVENT And g_AGEN="S" AND NOT EMPTY(this.oParentObject.w_MVSERIAL)
              * --- Se il documento � generato da un piano blocco la modifica e la cancellazione
              * --- Select from GSAG_BNR
              do vq_exec with 'GSAG_BNR',this,'_Curs_GSAG_BNR','',.f.,.t.
              if used('_Curs_GSAG_BNR')
                select _Curs_GSAG_BNR
                locate for 1=1
                do while not(eof())
                this.w_DTSERIAL = ALLTRIM(_Curs_GSAG_BNR.DDSERIAL)
                this.w_MESS = ah_Msgformat("Attenzione: documento generato dal piano di generazione documenti (%1)%0La cancellazione manuale del documento non comporta l'aggiornamento automatico delle date prossimo documento sugli elementi contratto.%0Confermi ugualmente?",this.w_DTSERIAL)
                this.oParentObject.w_HASEVENT = ah_YesNo(this.w_MESS)
                this.w_MESS = ""
                  select _Curs_GSAG_BNR
                  continue
                enddo
                use
              endif
            endif
            if this.oParentObject.w_HASEVENT And Vartype(g_AGFA) <> "U" and g_AGFA="S" AND NOT EMPTY(this.oParentObject.w_MVSERIAL)
              * --- Select from GSFA_BNR
              do vq_exec with 'GSFA_BNR',this,'_Curs_GSFA_BNR','',.f.,.t.
              if used('_Curs_GSFA_BNR')
                select _Curs_GSFA_BNR
                locate for 1=1
                do while not(eof())
                this.w_EVSERIAL = ALLTRIM(_Curs_GSFA_BNR.EVSERIAL)
                this.w_EV__ANNO = ALLTRIM(_Curs_GSFA_BNR.EV__ANNO)
                this.w_MESS = ah_Msgformat("Attenzione: documento associato all'evento (%1) del (%2)%0 Impossibile eliminare ",this.w_EVSERIAL,this.w_EV__ANNO)
                this.oParentObject.w_HASEVENT = .f.
                  select _Curs_GSFA_BNR
                  continue
                enddo
                use
              endif
            endif
        endcase
        if Not Empty( this.w_MESS )
          ah_ErrorMsg(this.w_MESS)
          * --- Se w_MESS pieno significa che i controlli sono falliti in modo bloccante o
          *     con un warning, se con un warning w_HASEVENT � .t. quindi non permetto la
          *     modifica dell'intestario.
          if Upper( this.oParentObject.w_HASEVCOP ) ="ECPEDIT"
            this.oParentObject.w_FLEDIT = IIF( this.oParentObject.w_HASEVENT , "N" ," ") 
          endif
        endif
      case this.pOpz="E" AND UPPER(this.oParentObject.w_HASEVCOP)="ECPF6" And this.w_PADRE.cfunction <>"Query"
        if this.oParentObject.w_ARTPADRE And this.oParentObject.w_MVTIPRIG <> "A" And g_COAC="S"
          * --- Chiave primaria riga padre
          this.w_ROWNUM = this.w_PADRE.Get("CPROWNUM")
          this.w_ROWORD = this.w_PADRE.Get("w_CPROWORD")
          * --- Contributo accessori
          this.w_PADRE.Exec_Select("CheckPad", "Count(*) as Conta" , "t_MVRIFCAC="+ cp_ToStrODBC(this.w_ROWNUM) ,"","","")     
          if Nvl ( CheckPad.Conta , 0 ) >0 
            if AH_YESNO("Contributi accessori: i servizi collegati all'articolo sulla riga %1 saranno eliminati.%0Confermi la cancellazione", "", this.w_ROWORD )
              this.w_PADRE.MarkPos()     
              do while Not this.w_PADRE.Eof_Trs()
                this.w_PADRE.SetRow()     
                if this.w_PADRE.FullRow() And this.oParentObject.w_MVRIFCAC=this.w_ROWNUM
                  ah_msg( "Cancellazione riga: %1",.t.,.f.,.f.,ALLTRIM(STR(this.oParentObject.w_CPROWORD)) )
                  this.w_PADRE.SubtractTotals()     
                  this.w_PADRE.DeleteRow()     
                endif
                this.w_PADRE.NextRow()     
              enddo
              this.w_PADRE.RePos()     
            else
              this.oParentObject.w_HASEVENT = .F.
            endif
          endif
          Use in CheckPad
          if this.oParentObject.w_HASEVENT
            * --- Se elimino una riga devo eliminare anche un'eventuale esplosione a peso..
            *     (e devo ricalcolare tutto, quindi � come una se facessi una riga nuova)
            this.oParentObject.w_TESTPESO = "I"
          endif
        endif
        if this.oParentObject.w_HASEVENT And Isalt() and this.oParentObject.w_MVRIGPRE="S"
          this.w_PADRE.NotifyEvent("ElimRow")     
        endif
        if this.oParentObject.w_HASEVENT And g_CESP="S" And NOT EMPTY(NVL(this.oParentObject.w_MVCESSER,SPACE(10)))
          * --- All'F6 verifica se esistono righe associate a movimenti cespiti
          this.w_MESS = "Impossibile eliminare: riga associata a movimento cespite"
          this.oParentObject.w_HASEVENT = .F.
          ah_ErrorMsg(this.w_MESS)
        endif
        if this.oParentObject.w_HASEVENT And g_MATR="S" AND this.oParentObject.w_GESMAT="S"
          this.w_SALDO = 0
          * --- Read from MOVIMATR
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MOVIMATR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MT_SALDO"+;
              " from "+i_cTable+" MOVIMATR where ";
                  +"MTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                  +" and MTROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                  +" and MTNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF);
                  +" and MT_SALDO = "+cp_ToStrODBC(1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MT_SALDO;
              from (i_cTable) where;
                  MTSERIAL = this.oParentObject.w_MVSERIAL;
                  and MTROWNUM = this.oParentObject.w_CPROWNUM;
                  and MTNUMRIF = this.oParentObject.w_MVNUMRIF;
                  and MT_SALDO = 1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALDO = NVL(cp_ToDate(_read_.MT_SALDO),cp_NullValue(_read_.MT_SALDO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALDO<>0
            this.w_MESS = "La riga movimenta matricole successivamente utilizzate. Impossibile eliminare"
            this.oParentObject.w_HASEVENT = .F.
            ah_ErrorMsg(this.w_MESS)
          endif
        endif
        if this.oParentObject.w_HASEVENT And g_EACD="S" AND this.oParentObject.w_TESCOMP
          this.oParentObject.w_HASEVENT = .F.
          ah_ErrorMsg("Attenzione, riga non eliminabile in quanto associata a documenti di esplosione componenti evasi","i","")
        endif
        if this.oParentObject.w_HASEVENT And this.w_PADRE.cfunction="Edit" And this.oParentObject.w_MVFLEVAS="S" And ( ( (Nvl(this.oParentObject.w_MVQTAEVA,0)=0 And this.oParentObject.w_MVTIPRIG<>"F") Or (Nvl(this.oParentObject.w_MVIMPEVA,0)=0 And this.oParentObject.w_MVTIPRIG="F") ) )
          this.w_ROWNUM = this.oParentObject.w_CPROWNUM
          * --- Read from RIGHEEVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.RIGHEEVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIGHEEVA_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "RESERIAL,REKEYSAL,RECODMAG,RECODMAT,REQTASAL,REFLIMPE,REF2IMPE,REFLORDI,REF2ORDI"+;
              " from "+i_cTable+" RIGHEEVA where ";
                  +"RESERRIF = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                  +" and REROWRIF = "+cp_ToStrODBC(this.w_ROWNUM);
                  +" and RENUMRIF = "+cp_ToStrODBC(-20);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              RESERIAL,REKEYSAL,RECODMAG,RECODMAT,REQTASAL,REFLIMPE,REF2IMPE,REFLORDI,REF2ORDI;
              from (i_cTable) where;
                  RESERRIF = this.oParentObject.w_MVSERIAL;
                  and REROWRIF = this.w_ROWNUM;
                  and RENUMRIF = -20;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_RESERIAL = NVL(cp_ToDate(_read_.RESERIAL),cp_NullValue(_read_.RESERIAL))
            this.w_REKEYSAL = NVL(cp_ToDate(_read_.REKEYSAL),cp_NullValue(_read_.REKEYSAL))
            this.w_RECODMAG = NVL(cp_ToDate(_read_.RECODMAG),cp_NullValue(_read_.RECODMAG))
            this.w_RECODMAT = NVL(cp_ToDate(_read_.RECODMAT),cp_NullValue(_read_.RECODMAT))
            this.w_REQTASAL = NVL(cp_ToDate(_read_.REQTASAL),cp_NullValue(_read_.REQTASAL))
            this.w_REFLIMPE = NVL(cp_ToDate(_read_.REFLIMPE),cp_NullValue(_read_.REFLIMPE))
            this.w_REF2IMPE = NVL(cp_ToDate(_read_.REF2IMPE),cp_NullValue(_read_.REF2IMPE))
            this.w_REFLORDI = NVL(cp_ToDate(_read_.REFLORDI),cp_NullValue(_read_.REFLORDI))
            this.w_REF2ORDI = NVL(cp_ToDate(_read_.REF2ORDI),cp_NullValue(_read_.REF2ORDI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows>0
            * --- Read from DOC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC"+;
                " from "+i_cTable+" DOC_MAST where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_RESERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC;
                from (i_cTable) where;
                    MVSERIAL = this.w_RESERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_RETIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
              this.w_RENUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
              this.w_REALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
              this.w_REDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_MESS = "Riga evasa senza importazione nel documento %1 n. %2 del %3%0Si vuole procedere con la cancellazione?"
            if ah_YesNo(this.w_MESS,"", this.w_RETIPDOC, Alltrim(Str(this.w_RENUMDOC,15))+IIF(Not Empty(this.w_REALFDOC),"/"+Alltrim(this.w_REALFDOC),""), DTOC(this.w_REDATDOC) )
              this.oParentObject.w_HASEVENT = .T.
            else
              this.oParentObject.w_HASEVENT = .F.
            endif
          endif
        endif
        if this.oParentObject.w_HASEVENT And this.oParentObject.w_MVFLARIF<>" " AND NOT EMPTY(this.oParentObject.w_MVSERRIF) AND this.oParentObject.w_MVROWRIF<>0
          this.w_SERRIF = this.oParentObject.w_MVSERRIF
          this.w_ROWRIF = this.oParentObject.w_MVROWRIF
          this.w_QTARIM = 0
          * --- Read from DOC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVQTAMOV,MVQTAEVA,MVQTAUM1"+;
              " from "+i_cTable+" DOC_DETT where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERRIF);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_MVROWRIF);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVQTAMOV,MVQTAEVA,MVQTAUM1;
              from (i_cTable) where;
                  MVSERIAL = this.oParentObject.w_MVSERRIF;
                  and CPROWNUM = this.oParentObject.w_MVROWRIF;
                  and MVNUMRIF = this.oParentObject.w_MVNUMRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_QTAORI = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
            this.w_QTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
            this.w_QTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
            this.w_QTAMOV = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Se cancello una riga che evade riapro la riga di origine...
          this.w_FLERIF = this.oParentObject.w_MVFLERIF
          * --- Determino Quantit� totale importata dalle righe collegate alla stessa riga di origine
          this.w_ROWNUM = this.oParentObject.w_CPROWNUM
          * --- Quantit� riga cancellata da spalmare sulle righe rimaste per colmare eventuali differenze
          *     tra quanttit� importata e quantit� movimentata
          this.w_QTADEL = NVL( this.oParentObject.w_MVQTAIMP ,0)
          this.w_PADRE.MarkPos()     
          this.w_RECNUM = 0
          do while this.w_RECNUM<>-1
            this.w_RECNUM = this.w_PADRE.Search( "NVL(t_MVSERRIF, SPACE(10)) = "+cp_ToStrODBC( this.w_SERRIF ) +" AND NVL(t_MVROWRIF, 0) = "+cp_ToStrODBC( this.w_ROWRIF ) +" AND CPROWNUM <> "+cp_ToStrODBC( this.w_ROWNUM ) +" AND ( NVL(t_MVQTAMOV,0) > NVL(t_MVQTAIMP,0) Or t_MVFLERIF= "+cp_ToStrODBC( "S" )+")" , this.w_RECNUM ) 
            * --- La riga in questione evade la solita riga evasa dalla riga cancellata ?
            if this.w_RECNUM<>-1
              this.w_PADRE.SetRow(this.w_RECNUM)     
              if this.oParentObject.w_MVQTAMOV > this.oParentObject.w_MVQTAIMP
                this.w_TUMCAL = this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3+this.oParentObject.w_MVUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3
                * --- Se non supero il tetto della quantit� movimentata del documento di origine confermo la quantit� movimentata
                this.w_QTAIMP = NVL(this.oParentObject.w_MVQTAIMP,0)
                this.w_QTARIM = this.w_QTAIMP+this.w_QTADEL
                this.w_TQTAIMP = MIN(this.oParentObject.w_MVQTAMOV, this.w_QTARIM)
                * --- Storno dalla quantit� cancellata la quantit� utilizzata, per le eventuali righe successive...
                this.w_QTADEL = this.w_QTADEL-(this.w_TQTAIMP-this.w_QTAIMP)
                if this.oParentObject.w_MVUNIMIS<>this.oParentObject.w_UNMIS1
                  * --- Ricalcolo la qt� movimentata del documento di origine nella prima unit� di misura
                  *     e la confronto con la qt� nella prima unit� di misura sempre del doc
                  *     di origine. Se diverse allora l'utente ha modificato la qt� nella prima unit� di misura 
                  *     quindi nel doc. di destinazione calcolo MVQTAUM1 come rapporto dato da:
                  *     
                  *     ( Qt� 1 Mis. Origine / Qt� movimenta Origine ) * Qta movimentata Destinazione
                  *     
                  *     Ricalcolo la qt� nella prima unit� di misura del doc. di origine
                  this.w_TmpN = CALQTA(this.w_QTAMOV,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, "", this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
                  if this.w_TmpN<>this.w_QTAUM1
                    * --- Eseguo la proporzione..
                    this.w_TQTAIM1 = ( this.w_QTAUM1 / this.w_QTAMOV ) * this.w_TQTAIMP
                    if this.oParentObject.w_NOFRAZ1="S" AND this.w_TQTAIM1<>INT(this.w_TQTAIM1)
                      this.w_TQTAIM1 = cp_ROUND(this.w_TQTAIM1,0)
                    endif
                  else
                    this.w_TQTAIM1 = IIF(this.oParentObject.w_MVTIPRIG="F", 1, CALQTA(this.w_TQTAIMP,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.oParentObject.w_NOFRAZ1, this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
                  endif
                else
                  this.w_TQTAIM1 = IIF(this.oParentObject.w_MVTIPRIG="F", 1, CALQTA(this.w_TQTAIMP,this.oParentObject.w_MVUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.oParentObject.w_NOFRAZ1, this.oParentObject.w_MODUM2, "", this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3))
                endif
                * --- Salvo sul transitorio il dato modificato..
                this.w_PADRE.Set("w_MVQTAIM1" , this.w_TQTAIM1)     
                this.w_PADRE.Set("w_MVQTAIMP" , this.w_TQTAIMP)     
              endif
              if this.w_FLERIF="S"
                * --- Se la riga che ho cancellato chiudeva la riga, cambio lo stato
                *     delle altre righe che evadono la stessa riga
                this.w_PADRE.Set("w_MVFLERIF" , " ")     
                this.w_PADRE.Set("w_OLDEVAS" , "R")     
              endif
            endif
          enddo
          this.w_PADRE.RePos()     
        endif
        if this.oParentObject.w_HASEVENT And Used( this.oParentObject.w_Cur_Rag_Row )
          * --- Se cancello una riga che � frutto di un raggruppamento di ordini
          *     allora elimino le righe che l'hanno determinato dal temporaneo di appoggio
           
 Delete From ( this.oParentObject.w_Cur_Rag_Row ) Where ROWDES= this.oParentObject.w_CPROWNUM
        endif
      case this.pOpz="V"
        * --- Tale controllo viene esguito all'edit started piuttosto che all'HASEVENT,
        *     per evitare che nel caso di ESC e nuova modifica lla LOAD riassegni quanto fatto all'HASEVENT
        if g_EACD="S"
          this.w_PADRE.MarkPos()     
          this.w_PADRE.FirstRow()     
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            * --- Se posso modificare e se produzione attiva marco la riga
            *     se esistono documenti di Evasione Componenti gi� evasi legati ad essa
            if Not Empty( this.oParentObject.w_MVRIFESC ) And CHKEVCOM( this.oParentObject.w_MVRIFESC )
              * --- Valorizzo il campo di controllo, senza marcare la riga come modificata..
              this.w_PADRE.Set("w_TESCOMP",.T.,.T.)     
            endif
            this.w_PADRE.NextRow()     
          enddo
          this.w_PADRE.RePos()     
        endif
      case this.pOpz="Q" AND Upper( this.oParentObject.w_HASEVCOP ) ="ECPDELETE" OR this.pOpz="R"
        * --- Setta i riferimenti del doc se � storicizzato
        if NOT EMPTY(this.oParentObject.w_MVSERIAL)
          this.w_PADREUPD = this.w_PADRE.bUpdated
          this.w_PADRE.MarkPos()     
          * --- Select from QUERY\GSVE1BNR
          do vq_exec with 'QUERY\GSVE1BNR',this,'_Curs_QUERY_GSVE1BNR','',.f.,.t.
          if used('_Curs_QUERY_GSVE1BNR')
            select _Curs_QUERY_GSVE1BNR
            locate for 1=1
            do while not(eof())
            this.w_OSERRIF = _Curs_QUERY_GSVE1BNR.MVSERRIF
            this.w_STSERRIF = _Curs_QUERY_GSVE1BNR.STSERRIF
            this.w_CAORIF = _Curs_QUERY_GSVE1BNR.CAORIF
            this.w_ODLRIF = _Curs_QUERY_GSVE1BNR.ODLRIF
            this.w_CLARIF = _Curs_QUERY_GSVE1BNR.CLARIF
            this.w_PADRE.Exec_Update("t_MVSERRIF='"+this.w_OSERRIF+"'","t_STSERRIF","'"+this.w_STSERRIF+"'","t_CAORIF",STR(this.w_CAORIF),"","",.T.)     
            if this.w_PADRE.Class="Tgsac_mdv"
              this.w_PADRE.Exec_Update("t_MVSERRIF='"+this.w_OSERRIF+"'","t_CLARIF","'"+this.w_CLARIF+"'","t_ODLRIF","'"+this.w_ODLRIF+"'","","",.T.)     
            endif
              select _Curs_QUERY_GSVE1BNR
              continue
            enddo
            use
          endif
          this.w_PADRE.RePos(.T.)     
          this.w_PADRE.bUpdated = this.w_PADREUPD
        endif
    endcase
    * --- Setta per non eseguire ricalcoli
    this.bUpdateParentObject=.f.
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_MESS = ""
    * --- Controlli bloccanti...
    do case
      case this.oParentObject.w_MVFLCONT="S"
        this.w_MESS = ah_Msgformat("Documento contabilizzato")
      case Not Empty( this.oParentObject.w_MVRIFACC )
        this.w_MESS = ah_Msgformat("Documento con acconto contabilizzato")
      case g_COGE<>"S" AND NOT EMPTY(this.oParentObject.w_MVRIFDCO)
        this.w_MESS = ah_Msgformat("Documento confermato in gestione effetti")
      case this.oParentObject.w_MVFLINTR="S"
        this.w_MESS = ah_Msgformat("Il documento ha generato gli elenchi INTRA")
      case this.oParentObject.w_MVFLBLOC="S"
        this.w_MESS = ah_Msgformat("Documento generato da DDT di C/Lavoro rivalorizzato")
      case g_PROD="S" AND (UPPER(ALLTRIM(this.oParentObject.w_MVRIFODL))= "CARICO" OR UPPER(ALLTRIM(this.oParentObject.w_MVRIFODL)) = "SCARICO")
        if UPPER(ALLTRIM(this.oParentObject.w_MVRIFODL))="CARICO"
          this.w_MESS = ah_Msgformat("Documento di carico generato da dichiarazione di produzione")
        else
          this.w_MESS = ah_Msgformat("Documento di scarico generato da dichiarazione di produzione")
        endif
      case this.oParentObject.w_MVFLVEAC="V" AND g_PERAGE="S" AND this.oParentObject.w_MVGENPRO="S"
        this.w_MESS = ah_Msgformat("Il documento ha generato dei movimenti di provvigione")
      case g_GPOS="S" AND ((this.oParentObject.w_MVFLOFFE= "I" And Upper( this.oParentObject.w_HASEVCOP ) ="ECPDELETE")Or( this.oParentObject.w_MVFLOFFE$ "IDF" And Upper( this.oParentObject.w_HASEVCOP ) ="ECPEDIT"))
        this.w_MESS = ah_Msgformat("Documento generato da vendita negozio")
      case Not Empty( this.oParentObject.w_MVMOVCOM ) And Upper( this.oParentObject.w_HASEVCOP ) ="ECPEDIT"
        this.w_MESS = ah_Msgformat("Documento generato da tempificazione")
      case NOT EMPTY(this.oParentObject.w_MVSERDDT)
        this.w_MESS = ah_Msgformat("Documento di scarico componenti per C/Lavoro da magazzino terzista")
      case Not Empty(CHKCONS(IIF(g_PERCCR="S" AND this.oParentObject.w_FLANAL="S",this.oParentObject.w_MVFLVEAC+"C",this.oParentObject.w_MVFLVEAC),this.oParentObject.w_MVDATREG,"B","N"))
        * --- Eseguo controllo su data consolidamento impostata nei dati azienda
        this.w_MESS = CHKCONS(IIF(g_PERCCR="S" AND this.oParentObject.w_FLANAL="S",this.oParentObject.w_MVFLVEAC+"C",this.oParentObject.w_MVFLVEAC),this.oParentObject.w_MVDATREG,"B","N")
      case this.oParentObject.w_MVCLADOC = "RF"
        * --- Se ricevuta fiscale controllo la presenza dell'Incasso.
        *     In tal caso blocco la modifica e la cancellazione
        * --- Select from INCDCORR
        i_nConn=i_TableProp[this.INCDCORR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INCDCORR_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select INSERIAL  from "+i_cTable+" INCDCORR ";
              +" where INORIDOC = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL)+"";
               ,"_Curs_INCDCORR")
        else
          select INSERIAL from (i_cTable);
           where INORIDOC = this.oParentObject.w_MVSERIAL;
            into cursor _Curs_INCDCORR
        endif
        if used('_Curs_INCDCORR')
          select _Curs_INCDCORR
          locate for 1=1
          do while not(eof())
          this.w_MESS = ah_Msgformat("Documento associato ad incasso corrispettivo")
          EXIT
            select _Curs_INCDCORR
            continue
          enddo
          use
        endif
      case g_FAEL="S"
        * --- Controllo fatturazione CBI
        * --- Read from DISDFAEL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DISDFAEL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DISDFAEL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "FARIFDOC"+;
            " from "+i_cTable+" DISDFAEL where ";
                +"FARIFDOC = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            FARIFDOC;
            from (i_cTable) where;
                FARIFDOC = this.oParentObject.w_MVSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RIFDOC = NVL(cp_ToDate(_read_.FARIFDOC),cp_NullValue(_read_.FARIFDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Not Empty(this.w_RIFDOC)
          this.w_MESS = ah_Msgformat("Documento associato a fattura CBI")
        endif
    endcase
    if g_AGEN="S" AND NOT EMPTY(this.oParentObject.w_MVSERIAL)
      * --- Se il documento � generato da un'attivit� blocco la modifica e la cancellazione
      * --- Select from gsve_bnr
      do vq_exec with 'gsve_bnr',this,'_Curs_gsve_bnr','',.f.,.t.
      if used('_Curs_gsve_bnr')
        select _Curs_gsve_bnr
        locate for 1=1
        do while not(eof())
        this.w_ATOGGETT = ALLTRIM(_Curs_gsve_bnr.ATOGGETT)
        this.w_DATATT = LEFT(TTOC(_Curs_gsve_bnr.ATDATINI),10)
        if Upper( this.oParentObject.w_HASEVCOP ) ="ECPDELETE"
          this.w_MESS = ah_Msgformat("Documento generato da un'attivit� (%1 del %2)  ",this.w_ATOGGETT,this.w_DATATT)
          this.oParentObject.w_HASEVENT = .f.
        else
          this.w_MESS = ah_Msgformat("Documento generato da un'attivit� (%1 del %2)",this.w_ATOGGETT,this.w_DATATT)
          if not isalt()
            this.oParentObject.w_HASEVENT = ah_ErrorMsg(this.w_MESS)
            this.w_MESS = ""
          endif
        endif
        EXIT
          select _Curs_gsve_bnr
          continue
        enddo
        use
      endif
    endif
    * --- Se un controllo salta deve valorizzare w_MESS
    * --- Questa composemessage con parametro false non sbianca l'oggetto in quanto serve solo per valorizzare w_HASEVENT
    this.oParentObject.w_HASEVENT = Empty( this.w_MESS )
    if this.oParentObject.w_HASEVENT And this.oParentObject.w_MVCLADOC $ "FA-NC"
      * --- Verifico se Fattura o Nota di credito se ha evaso un documento
      *     soggetto a ripartizione spese. Se si impedisco la modifica in quanto
      *     questa provocherebbe il ricalcolo di MVIMPNAZ...
      this.w_MESS = CHKRIPSPE( this.oParentObject.w_MVSERIAL )
      this.oParentObject.w_HASEVENT = Empty( this.w_MESS )
    endif
    if this.oParentObject.w_HASEVENT
      if this.pOpz="L"
        * --- Non c'� nessuna movimentazione, cerchiamo direttamente in DOC_DETT
        * --- Select from DOC_DETT
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_DETT ";
              +" where MVSERIAL="+cp_ToStrODBC(this.oParentObject.w_MVSERIAL)+" ";
               ,"_Curs_DOC_DETT")
        else
          select * from (i_cTable);
           where MVSERIAL=this.oParentObject.w_MVSERIAL ;
            into cursor _Curs_DOC_DETT
        endif
        if used('_Curs_DOC_DETT')
          select _Curs_DOC_DETT
          locate for 1=1
          do while not(eof())
          do case
            case NOT EMPTY(MVSERRIF) AND NOT ISALT()
              this.w_AppMess = "Alcune righe del documento sono state importate da altri documenti"
              this.oParentObject.w_HASEVENT = .T.
            case MVQTAEVA<>0 OR MVIMPEVA<>0
              this.w_AppMess = "Il documento � stato evaso da altri documenti"
              this.oParentObject.w_HASEVENT = .T.
            case this.oParentObject.w_FLGEFA="S" AND NOT EMPTY(CP_TODATE(MVDATGEN))
              this.w_AppMess = "Il documento ha generato una fattura differita"
              this.oParentObject.w_HASEVENT = .T.
          endcase
            select _Curs_DOC_DETT
            continue
          enddo
          use
        endif
        this.w_oMESS.addmsgpartNL(this.w_AppMess)     
        this.w_MESS = this.w_oMESS.ComposeMessage()
        this.w_STATO = ""
      else
        * --- Gestione standard con il padre chiamante
        this.w_PADRE.MarkPos()     
        * --- All'F5 verifica se esistono righe associate a movimenti cespiti
        if this.oParentObject.w_HASEVENT AND g_CESP="S" And Upper( this.oParentObject.w_HASEVCOP ) ="ECPDELETE" AND NOT EMPTY(this.oParentObject.w_MVSERIAL)
          this.w_PADRE.FirstRow()     
          this.w_RECNUM = this.w_PADRE.Search("Not Empty( t_MVCESSER ) And Not Deleted()")
          if this.w_RECNUM<>-1
            this.w_PADRE.SetRow(this.w_RECNUM)     
            this.w_MESS = ah_Msgformat("Riga movimento: %1 associata a movimento cespite", ALLTRIM(STR(this.oParentObject.w_CPROWORD)) )
            this.oParentObject.w_HASEVENT = .F.
          endif
        endif
        if this.oParentObject.w_HASEVENT
          this.w_PADRE.FirstRow()     
          if UPPER( this.oParentObject.w_HASEVCOP )="ECPEDIT"
            if this.oParentObject.w_FLBLEV = "S"
              this.w_RECNUM = this.w_PADRE.Search("t_MVQTAEVA<>0 OR t_MVIMPEVA<>0")
            endif
            if this.oParentObject.w_FLBLEV <> "S" OR this.w_RECNUM<0
              this.w_RECNUM = this.w_PADRE.Search("NOT EMPTY(t_MVSERRIF) OR t_MVQTAEVA<>0 OR t_MVIMPEVA<>0")
            endif
          else
            if this.oParentObject.w_FLGEFA="S"
              this.w_RECNUM = this.w_PADRE.Search(" t_MVQTAEVA<>0 OR t_MVIMPEVA<>0 OR NOT EMPTY(t_MVDATGEN)")
            else
              this.w_RECNUM = this.w_PADRE.Search(" t_MVQTAEVA<>0 OR t_MVIMPEVA<>0")
            endif
          endif
          if this.w_RECNUM<>-1
            this.w_PADRE.SetRow(this.w_RECNUM)     
            do case
              case this.oParentObject.w_MVQTAEVA<>0 OR this.oParentObject.w_MVIMPEVA<>0 OR (NOT EMPTY(this.oParentObject.w_MVSERRIF) AND UPPER(this.oParentObject.w_HASEVCOP)="ECPEDIT")
                if this.oParentObject.w_MVQTAEVA<>0 OR this.oParentObject.w_MVIMPEVA<>0
                  this.w_oMESS.addmsgpartNL("Il documento � stato evaso da altri documenti")     
                  * --- Se � attivo il blocco doc evasi sulla causale impedisco la modifica
                  if this.oParentObject.w_FLBLEV = "S"
                    this.oParentObject.w_HASEVENT = .F.
                  endif
                endif
                if this.oParentObject.w_HASEVENT AND NOT EMPTY(this.oParentObject.w_MVSERRIF) AND UPPER(this.oParentObject.w_HASEVCOP)="ECPEDIT" AND NOT ISALT()
                  this.w_oMESS.addmsgpartNL("Alcune righe del documento sono state importate da altri documenti")     
                endif
              case this.oParentObject.w_FLGEFA="S" AND NOT EMPTY(CP_TODATE(this.oParentObject.w_MVDATGEN))
                this.w_oMESS.addmsgpartNL("Il documento ha generato una fattura differita")     
                * --- Se � attivo il blocco doc evasi sulla causale impedisco la modifica
                if this.oParentObject.w_FLBLEV = "S"
                  this.oParentObject.w_HASEVENT = .F.
                endif
            endcase
            * --- Se � attivo il blocco doc evasi sulla causale vario il messaggio
            if this.oParentObject.w_HASEVENT
              if Upper( this.oParentObject.w_HASEVCOP ) ="ECPEDIT"
                if NOT ISALT()
                  this.w_oMESS.addmsgpartNL("Potrebbe non essere possibile modificare")     
                endif
              else
                if NOT ISALT()
                  this.w_oMESS.addmsgpartNL("Potrebbe non essere possibile eliminare")     
                endif
              endif
            else
              if Upper( this.oParentObject.w_HASEVCOP ) ="ECPEDIT"
                this.w_oMESS.addmsgpartNL("Impossibile modificare")     
              else
                this.w_oMESS.addmsgpartNL("Impossibile eliminare")     
              endif
            endif
            this.w_MESS = this.w_oMESS.ComposeMessage()
            this.w_STATO = ""
          endif
        endif
         
 DECLARE ArrGiom(1) 
 ArrGiom(1) = Space(5)
        this.w_NUMMAG = 0
        if this.oParentObject.w_HASEVENT
          this.w_PADRE.FirstRow()     
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            if NOT EMPTY(NVL( this.oParentObject.w_MVCODMAG," ")) OR NOT EMPTY(NVL(this.oParentObject.w_MVCODMAT," ")) 
              if this.oParentObject.w_MVFLELGM="S" 
                * --- Vengono memorizzati in un Array i Codici magazzino gi� controllati
                *     in modo che non vengano controllati due volte.
                *     Questa operazione viene eseguita per problemi di lentezza 
                *     nel caso di documenti con molte righe
                if ASCAN(ArrGiom, Left(Alltrim(this.oParentObject.w_MVCODMAG)+Space(5),5) )=0 
                  if NOT EMPTY(this.oParentObject.w_MVCODMAG) AND ! CHKGIOM(this.oParentObject.w_MVCODESE,this.oParentObject.w_MVCODMAG,this.oParentObject.w_MVDATREG,this.oParentObject.w_MVFLELGM)
                    this.w_MESS = ah_Msgformat("Errore: documento stampato sul giornale magazzino; impossibile variare")
                    this.oParentObject.w_HASEVENT = .F.
                    this.w_STATO = ""
                  endif
                  if Not Empty(this.oParentObject.w_MVCODMAG) And this.oParentObject.w_HASEVENT
                     
 DECLARE ArrGiom(ALEN(ArrGiom)+IIF(this.w_NUMMAG=0,0,1)) 
 ArrGiom(Alen(ArrGiom)) = Left(Alltrim(this.oParentObject.w_MVCODMAG)+Space(5),5)
                    this.w_NUMMAG = 1
                  endif
                endif
                if ASCAN(ArrGiom, Left(Alltrim(this.oParentObject.w_MVCODMAT)+Space(5),5) )=0 And this.oParentObject.w_HASEVENT
                  if ! EMPTY(this.oParentObject.w_MVCODMAT) AND ! CHKGIOM(this.oParentObject.w_MVCODESE,this.oParentObject.w_MVCODMAT,this.oParentObject.w_MVDATREG,this.oParentObject.w_MVFLELGM)
                    this.w_MESS = ah_Msgformat("Errore: documento stampato sul giornale magazzino; impossibile variare")
                    this.oParentObject.w_HASEVENT = .F.
                    this.w_STATO = ""
                  endif
                  if Not Empty(this.oParentObject.w_MVCODMAT) And this.oParentObject.w_HASEVENT
                     
 DECLARE ArrGiom(ALEN(ArrGiom)+IIF(this.w_NUMMAG=0,0,1)) 
 ArrGiom(Alen(ArrGiom)) = Left(Alltrim(this.oParentObject.w_MVCODMAT)+Space(5),5)
                    this.w_NUMMAG = 1
                  endif
                endif
              endif
              * --- Controllo data consolidamento magazzino, la devo fare sempre...
              if this.oParentObject.w_HASEVENT
                this.w_MESS_CONS = CHKCONS("M",this.oParentObject.w_MVDATREG,"B","N")
                if Not Empty( this.w_MESS_CONS )
                  this.oParentObject.w_HASEVENT = .F.
                  this.w_MESS = this.w_MESS_CONS
                endif
              endif
            endif
            if Not this.oParentObject.w_HASEVENT
              * --- Appena una riga non � ok esco...
              Exit
            endif
            this.w_PADRE.NextRow()     
          enddo
        endif
        if this.oParentObject.w_HASEVENT
          this.w_NR = ALLTRIM(STR(this.oParentObject.w_CPROWORD))
          this.w_FLDEL = .F.
          this.w_FLDEL = this.w_PADRE.cfunction ="Query" 
          this.w_SRV = iif( this.w_FLDEL , " ", this.w_PADRE.RowStatus() )
          if this.oParentObject.w_MVCLADOC $ "FA-NC" And ( this.w_SRV="U" or this.w_FLDEL ) And this.oParentObject.w_MVTIPRIG="R" And this.oParentObject.w_MVFLELGM="S" And this.oParentObject.w_MVFLPROV="N"
            * --- Read from ADDE_SPE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "RLSERFAT"+;
                " from "+i_cTable+" ADDE_SPE where ";
                    +"RLSERDOC = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                    +" and RLNUMDOC = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                    +" and RLROWDOC = "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                RLSERFAT;
                from (i_cTable) where;
                    RLSERDOC = this.oParentObject.w_MVSERIAL;
                    and RLNUMDOC = this.oParentObject.w_CPROWNUM;
                    and RLROWDOC = this.oParentObject.w_MVNUMRIF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SERFAT = NVL(cp_ToDate(_read_.RLSERFAT),cp_NullValue(_read_.RLSERFAT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Not Empty( this.w_SERFAT )
              if this.w_SRV="U"
                this.w_MESS = ah_Msgformat("Impossibile modificare: la riga %1 � oggetto di ripartizione spese%0Annullare la ripartizione spese da servizi / ripartizione fattura di spese", this.w_NR)
              else
                this.w_MESS = ah_Msgformat("Impossibile eliminare: la riga %1 � oggetto di ripartizione spese%0Annullare la ripartizione spese da servizi / ripartizione fattura di spese", this.w_NR)
              endif
              this.oParentObject.w_HASEVENT = .F.
              this.w_STATO = ""
            endif
            this.w_SERFAT = Space(10)
          endif
        endif
        this.w_PADRE.RePos(.F.)     
      endif
    endif
    if this.oParentObject.w_HASEVENT And this.oParentObject.w_FLGEFA="B" And Not Empty( this.oParentObject.w_MVRIFFAD )
      * --- Avviso se la fattura � stata effettivamente creata dalla generazione
      *     automatica fatture...
      if Upper( this.oParentObject.w_HASEVCOP ) ="ECPEDIT"
        this.w_MESS = ah_Msgformat("Fattura differita generata%0Potrebbe non essere possibile modificare")
        this.w_STATO = ""
      else
        * --- Se in cancellazione impedisco l'operazione se la fattura contiene
        *     righe da ordini aperti (ighe con MVFLARIF vuoto e non descrittive)
        *     Una riga da ordine aperto non � descrittiva, ha pieno MVSERRIF e non 
        *     evade
        this.w_CURNAME = Sys(2015)
        this.w_PADRE.Exec_Select(this.w_CURNAME,"Count(*) As Conta "," Not Empty( t_MVSERRIF ) And Empty( t_MVFLERIF ) And t_MVTIPRIG<>'D' ","","","")     
        if Not (Eof( this.w_CURNAME ) And Bof( this.w_CURNAME )) And Nvl( Conta , 0 )>0
          this.w_MESS = ah_Msgformat("Fattura differita generata da ordini aperti%0Eliminare la fattura dal Piano di fatturazione")
          this.oParentObject.w_HASEVENT = .F.
        else
          this.oParentObject.w_HASEVENT = ah_YesNo("Fattura differita generata%0Confermi ugualmente?")
        endif
         
 Select( this.w_CURNAME ) 
 Use
      endif
    endif
    * --- Determino la presenza di righe con documenti di Carico\Scarico Componenti Non Evasi
    *     SERDOCESP: seriale del documento, se esiste, che ha generato il documento di esplosione che si sta cancellando
    * --- Documento generato da esplosione imballi 
    if this.oParentObject.w_HASEVENT And g_VEFA = "S"
      * --- La funzione CHKDOCESP recupera L'MVSERIAL del documento padre
      this.w_SERDOCESP = CHKDOCESP(this.oParentObject.w_MVSERIAL)
      if Not Empty(this.w_SERDOCESP)
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVTIPDOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOCESP);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVTIPDOC;
            from (i_cTable) where;
                MVSERIAL = this.w_SERDOCESP;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDTIPIMB"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDTIPIMB;
            from (i_cTable) where;
                TDTIPDOC = this.w_TIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPIMB = NVL(cp_ToDate(_read_.TDTIPIMB),cp_NullValue(_read_.TDTIPIMB))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Documento che gestisce gli imballi.
        *     Poich� potrebbe essere stato generato solo per distinta e quindi senza nessun imballo a rendere
        *     controllo anche le righe.
        if this.w_TIPIMB<>"N" AND !this.pOpz="L"
          this.w_PADRE.MarkPos()     
          this.w_PADRE.FirstRow()     
          do while Not this.w_PADRE.Eof_Trs()
            this.w_TESTART = Nvl( this.w_PADRE.Get("t_MVCODART"), " ")
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARKITIMB"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_TESTART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARKITIMB;
                from (i_cTable) where;
                    ARCODART = this.w_TESTART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_KITIMB = NVL(cp_ToDate(_read_.ARKITIMB),cp_NullValue(_read_.ARKITIMB))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_KITIMB="R"
              * --- Al primo imbballo a rendere che trovo do il messaggio e esco dal ciclo
              this.w_MESS = ah_Msgformat("Il documento � stato generato da kit imballi. Eseguire l'operazione sul documenti che lo ha generato%1", this.w_STATO)
              this.oParentObject.w_HASEVENT = .F.
              this.w_STATO = ""
              exit
            endif
            this.w_PADRE.NextRow()     
          enddo
          this.w_PADRE.RePos(.F.)     
        endif
      endif
    endif
    if Not Empty(this.w_MESS)
      this.w_MESS = this.w_MESS + this.w_STATO
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOpz)
    this.pOpz=pOpz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,12)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='MOVIMATR'
    this.cWorkTables[3]='ADDE_SPE'
    this.cWorkTables[4]='RIGHEEVA'
    this.cWorkTables[5]='DOC_MAST'
    this.cWorkTables[6]='DET_DIFF'
    this.cWorkTables[7]='INCDCORR'
    this.cWorkTables[8]='TIP_DOCU'
    this.cWorkTables[9]='ART_ICOL'
    this.cWorkTables[10]='OFF_ATTI'
    this.cWorkTables[11]='DISDFAEL'
    this.cWorkTables[12]='DET_PIAS'
    return(this.OpenAllTables(12))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_GSAG_BNR')
      use in _Curs_GSAG_BNR
    endif
    if used('_Curs_GSFA_BNR')
      use in _Curs_GSFA_BNR
    endif
    if used('_Curs_QUERY_GSVE1BNR')
      use in _Curs_QUERY_GSVE1BNR
    endif
    if used('_Curs_INCDCORR')
      use in _Curs_INCDCORR
    endif
    if used('_Curs_gsve_bnr')
      use in _Curs_gsve_bnr
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpz"
endproc
