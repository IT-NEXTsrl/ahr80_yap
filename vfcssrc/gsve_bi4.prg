* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bi4                                                        *
*              Selezione automatica righe doc                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_55]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-07-02                                                      *
* Last revis.: 2014-12-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEvent
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bi4",oParentObject,m.pEvent)
return(i_retval)

define class tgsve_bi4 as StdBatch
  * --- Local variables
  pEvent = space(3)
  w_ZOOM = space(10)
  w_CHKMAST = 0
  w_SERDETT = space(10)
  w_CHKEVAS = space(1)
  w_MESS = space(10)
  w_MESS1 = space(10)
  w_CHKACCO = space(1)
  w_APPO = space(10)
  w_APPO2 = space(10)
  w_APPO1 = space(10)
  w_CHIAVE = space(30)
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Evento Seleziona/Deseleziona Master (da GSVE_KIM)
    this.oParentObject.w_ERRGEN = .F.
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_PADRE = this.oparentobject
    this.w_CHKMAST = -1
    this.oParentObject.w_ERR = .F.
    this.bUpdateParentObject = .F.
    ND = this.oParentObject.w_ZoomDett.cCursor
    NM = this.oParentObject.w_ZoomMast.cCursor
    do case
      case this.pEvent = "SMC"
        SELECT (NM)
        if Not IsAlt()
          if (reccount()>10)
            if this.oParentObject.w_SELEZ1="S"
              if ah_YesNo("La selezione automatica di %1 documenti potrebbe durare molto tempo. Proseguire comunque?","", ALLTRIM(STR(reccount())) )
              else
                this.oParentObject.w_SELEZ1 = "D"
                i_retcode = 'stop'
                return
              endif
              ah_Msg("SELEZIONE DOCUMENTI...")
            else
              if ah_YesNo("La deselezione automatica di %1 documenti potrebbe durare molto tempo. Proseguire comunque?","", ALLTRIM(STR(reccount())))
                ah_msg("Deselezione documenti...")
              else
                this.oParentObject.w_SELEZ1 = "S"
                i_retcode = 'stop'
                return
              endif
            endif
          endif
        endif
        if this.oParentObject.w_SELEZ1="S"
          UPDATE (this.oParentObject.w_ZoomMast.cCursor) SET XCHK=1 WHERE EMPTY(FLCONG) AND EMPTY(Nvl(MVFLSCAF," "))
          * --- Memorizzo il numero di documenti selezionati...
          Select( this.oParentObject.w_ZoomMast.cCursor ) 
 Count For XCHK=1And this.oParentObject.w_TIPCON = MVTIPCON to this.oParentObject.w_BLOCKMAST
          * --- La count sposta la posizione del cursore oltre l'ultimo record mi rimetto sempre nella prima
          Go top
          * --- La selezione di tutti i documenti passa dall'esecuzione di una query che
          *     mette in Join le query dei due zoom filtrando i documenti "importabili"
          *     (da verificare il check scadenza confermate, se attivo questo check sono da escludere).
          *     
          *     Il risultato delle query � per costruzione identico alla query dello zoom di destra, per cui possiamo 
          *     simulare una After query, assegnamo il cursore ritornato dalla query allo zoom, simuliamo
          *     un conferma di tutte le righe (scan sul cursore dello zoom) eseguiamo una before query e 
          *     confermiamo la maschera...
          this.oParentObject.w_SERIAL = Space(10)
          this.oParentObject.w_TIPO = "T"
          this.oParentObject.w_ZoomDett.NewExtQuery("QUERY\GSVE2DKIM")     
          this.oParentObject.w_ZoomDett.Query()     
          if USED("RIGHEDOC")
            * --- Viene azzerato RIGHEDOC
            SELECT * FROM RIGHEDOC WHERE 1=0 INTO CURSOR RIGHEDOC
          endif
          * --- ****ATTENZIONE QUESTO CODICE NON VERIFICA 
          *     SE LE RIGHE SELEZIONATE SONO IMPORTABILI 
          *     OCCORRE ESEGUIRE UNA SCAN SIMULANDO IL CHECK
          *      (CONFRONTO CON IL DETTAGLIO 
          *     NON POSSO ANDARE SUL DATABASE)****
          * --- Riporto alcuni dati dello zoom di sinistra (testate) su quello di destra..
          if NOT (Used ("DocCal") AND IsAlt())
            UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK=1, MVQTAEVA=iif(MVTIPRIG="D",1,MVQTAMOV), MVIMPEVA=MVPREZZO, FLINTE=O_FLINTE, FLACCO=O_FLACCO WHERE FLDOCU<>"S"
          else
            UPDATE ( this.oParentObject.w_ZoomDett.cCursor ) SET XCHK=1, MVQTAEVA=iif(MVTIPRIG="D",1,MVQTAMOV), MVIMPEVA=MVPREZZO, FLINTE=O_FLINTE, FLACCO=O_FLACCO WHERE FLDOCU<>"S" AND EXIST ; 
 (SELECT 1 FROM Doccal WHERE &ND..MVSERIAL = DOCCAL.MVSERIAL and &ND..CPROWNUM = DOCCAL.CPROWNUM AND &ND..MVNUMRIF = DOCCAL.NUMRIF AND &ND..CPROWORD = DOCCAL.CPROWORD AND &ND..PRSERIAL = DOCCAL.PRSERIAL )
            if Used ("DocCal")
              SELECT DocCal
              USE
            endif
          endif
          * --- ****************************************
          * --- rimetto al vecchia query, applico il filtro della riga attuale sulla griglia...
          this.oParentObject.w_ZoomMast.Refresh()     
          this.oParentObject.w_SERIAL = this.oParentObject.w_ZoomMast.GetVar("MVSERIAL")
          this.oParentObject.w_TIPO = this.oParentObject.w_ZoomMast.GetVar("TIPO")
          this.oParentObject.w_TIPO = iif(Empty(Nvl(this.oParentObject.w_TIPO," ")),"T",this.oParentObject.w_TIPO)
          * --- Riassegnata propiet� per consentire riposizionamento dello zoom
          this.oParentObject.w_ZoomDett.xOldValue = "123456788901"
          if !IsAlt()
            this.oParentObject.w_ZoomDett.NewExtQuery("GSVEDKIM")     
          else
            this.oParentObject.w_ZoomDett.NewExtQuery("QUERY\GSVEDKIM")     
          endif
          Select( this.oParentObject.w_ZoomMast.cCursor )
          SCAN FOR NOT ( EMPTY(FLCONG) AND EMPTY(Nvl(MVFLSCAF," ")) ) OR NOT EMPTY(MVAGG_01) OR NOT EMPTY(MVAGG_02) OR NOT EMPTY(MVAGG_03) OR NOT EMPTY(MVAGG_04) OR NOT EMPTY(MVAGG_05) OR NOT EMPTY(MVAGG_06) OR w_TDRIOTOT="R"
          this.oParentObject.w_FLSCAF = Nvl(MVFLSCAF," ")
          this.oparentobject.notifyEvent("FindDocError")
          ENDSCAN
          * --- La scan sposta la posizione del cursore oltre l'ultimo record mi rimetto sempre nella prima
          Go top
          if this.oParentObject.w_ErrGen
            ah_ErrorMsg("Si sono verificati errori durante la selezione automatica")
            this.oParentObject.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
            this.oParentObject.w_oERRORLOG.ClearLog()     
          endif
        else
          UPDATE (NM) SET XCHK=0 Where FLDOCU<>"S"
          Select( this.oParentObject.w_ZoomMast.cCursor ) 
 Count For XCHK=1And this.oParentObject.w_TIPCON = MVTIPCON to this.oParentObject.w_BLOCKMAST
          * --- La count sposta la posizione del cursore oltre l'ultimo record mi rimetto nella prima
          Go Top
          if USED("RIGHEDOC")
            * --- Viene azzerato RIGHEDOC
            SELECT * FROM RIGHEDOC WHERE 1=0 INTO CURSOR RIGHEDOC
          endif
          if UPPER( this.oParentObject.w_GESTPARENT.cPRG ) <> "GSAR_MPG"
             
 UPDATE (ND) SET XCHK=0, MVQTAEVA=0, MVIMPEVA=0 ; 
 WHERE(FLDOCU<>"S" OR (FLDOCU="S" AND (OLQTAEVA<MVQTAMOV OR ; 
 (Abs(OLIMPEVA)<Abs(VISPRE) AND MVTIPRIG="F"))) )
          endif
        endif
    endcase
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
  endproc


  proc Init(oParentObject,pEvent)
    this.pEvent=pEvent
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEvent"
endproc
