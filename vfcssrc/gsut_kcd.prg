* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kcd                                                        *
*              Caricamento dizionario dati                                     *
*                                                                              *
*      Author: Zucchetti TAM Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_26]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-23                                                      *
* Last revis.: 2008-09-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kcd",oParentObject))

* --- Class definition
define class tgsut_kcd as StdForm
  Top    = 16
  Left   = 11

  * --- Standard Properties
  Width  = 382
  Height = 228
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-02"
  HelpContextID=99173225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kcd"
  cComment = "Caricamento dizionario dati"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FlagDelete = space(1)
  w_FlagUpdate = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kcdPag1","gsut_kcd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFlagDelete_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSUT_BKD with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FlagDelete=space(1)
      .w_FlagUpdate=space(1)
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate(Ah_msgformat("La procedura permette di salvare il dizionario dati corrente %0in un database di servizio. Ci� permette di abilitare la %0selezione della struttura delle tabelle nelle gestioni:"))
    endwith
    this.DoRTCalc(1,2,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(Ah_msgformat("La procedura permette di salvare il dizionario dati corrente %0in un database di servizio. Ci� permette di abilitare la %0selezione della struttura delle tabelle nelle gestioni:"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,2,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate(Ah_msgformat("La procedura permette di salvare il dizionario dati corrente %0in un database di servizio. Ci� permette di abilitare la %0selezione della struttura delle tabelle nelle gestioni:"))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFlagDelete_1_2.RadioValue()==this.w_FlagDelete)
      this.oPgFrm.Page1.oPag.oFlagDelete_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFlagUpdate_1_6.RadioValue()==this.w_FlagUpdate)
      this.oPgFrm.Page1.oPag.oFlagUpdate_1_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kcdPag1 as StdContainer
  Width  = 378
  height = 228
  stdWidth  = 378
  stdheight = 228
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="VQERAGEWET",left=272, top=181, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 99173130;
    , caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFlagDelete_1_2 as StdCheck with uid="SPFHSGPTDA",rtseq=1,rtrep=.f.,left=16, top=159, caption="Cancella dizionario corrente",;
    ToolTipText = "Se impostato la procedura provvede ad eliminare il contenuto del dizionario dati prima di procedere con il caricamento",;
    HelpContextID = 205215237,;
    cFormVar="w_FlagDelete", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFlagDelete_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'S',;
    'N')))
  endfunc
  func oFlagDelete_1_2.GetRadio()
    this.Parent.oContained.w_FlagDelete = this.RadioValue()
    return .t.
  endfunc

  func oFlagDelete_1_2.SetRadio()
    this.Parent.oContained.w_FlagDelete=trim(this.Parent.oContained.w_FlagDelete)
    this.value = ;
      iif(this.Parent.oContained.w_FlagDelete=='S',1,;
      iif(this.Parent.oContained.w_FlagDelete=='S',2,;
      0))
  endfunc


  add object oBtn_1_4 as StdButton with uid="MRPTSAHHIT",left=323, top=181, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 35921737;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFlagUpdate_1_6 as StdCheck with uid="KQHFPZHHJT",rtseq=2,rtrep=.f.,left=16, top=184, caption="Forza aggiornamento",;
    ToolTipText = "Se impostato la procedura provvede ad aggiornare il dizionario dati senza considerare il timestamp dell'ultimo aggiornamento",;
    HelpContextID = 265595383,;
    cFormVar="w_FlagUpdate", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFlagUpdate_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'S',;
    'N')))
  endfunc
  func oFlagUpdate_1_6.GetRadio()
    this.Parent.oContained.w_FlagUpdate = this.RadioValue()
    return .t.
  endfunc

  func oFlagUpdate_1_6.SetRadio()
    this.Parent.oContained.w_FlagUpdate=trim(this.Parent.oContained.w_FlagUpdate)
    this.value = ;
      iif(this.Parent.oContained.w_FlagUpdate=='S',1,;
      iif(this.Parent.oContained.w_FlagUpdate=='S',2,;
      0))
  endfunc


  add object oObj_1_9 as cp_calclbl with uid="SJLXCEWQCB",left=18, top=15, width=346,height=65,;
    caption='La procedura permette di salvare il dizionario dati corrente in un database di servizio',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 204376161

  add object oStr_1_5 as StdString with uid="QMXAYNWPBL",Visible=.t., Left=18, Top=125,;
    Alignment=0, Width=304, Height=18,;
    Caption="Premere il bottone ok per procedere con il caricamento"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="CIACNDSWOH",Visible=.t., Left=18, Top=81,;
    Alignment=0, Width=268, Height=18,;
    Caption="- IMPORT DATI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="KVOOQPPVIE",Visible=.t., Left=18, Top=97,;
    Alignment=0, Width=268, Height=18,;
    Caption="- IMPORT LISTINI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_3 as StdBox with uid="LXNECEOARK",left=13, top=8, width=358,height=141
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kcd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
