* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_ggd                                                        *
*              Gadget Grid                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-12-16                                                      *
* Last revis.: 2015-12-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_ggd",oParentObject))

* --- Class definition
define class tgsut_ggd as StdGadgetForm
  Top    = 7
  Left   = 11

  * --- Standard Properties
  Width  = 133
  Height = 90
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-12-03"
  HelpContextID=36258665
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_ggd"
  cComment = "Gadget Grid"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TITLE = space(254)
  o_TITLE = space(254)
  w_QUERY = space(254)
  o_QUERY = space(254)
  w_RET = .F.
  w_DATETIME = space(20)
  w_PROGRAM = space(250)
  w_BTRSGRID = .F.
  w_PRGDESCRI = space(50)
  w_FONTCLR = 0
  w_PRGGRID = space(250)
  w_oFooter = .NULL.
  w_oGrid = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_ggd
  bMouseHover = .F.
  
  *--- ApplyTheme
  Proc ApplyTheme()
     DoDefault()
     Local l_oldErr, l_OnErrorMsg
     With This
       m.l_OnErrorMsg = ""
       m.l_oldErr = On("Error")
       On Error m.l_OnErrorMsg=ah_MsgFormat("%1%0%2",Iif(Empty(m.l_OnErrorMsg), "Aggiornamento gadget:",m.l_OnErrorMsg),Message())
       
       .w_FONTCLR =  Iif(.nFontColor<>-1, .nFontColor, Rgb(243,243,243))
       .w_oGrid.ForeColor = .w_FONTCLR
       
       On Error &l_oldErr
       If !Empty(m.l_OnErrorMsg) && c'� stato un errore di programma
          .cAlertMsg = Iif(Empty(.cAlertMsg), m.l_OnErrorMsg, ah_MsgFormat("%1%0%2",.cAlertMsg,m.l_OnErrorMsg))
       Endif
       
       .mCalc(.t.)
       .SaveDependsOn()
     Endwith
  EndProc
  
  *--- AddMenu
  *-- Voci a men� per la grid
  Proc AddMenu(loPopupMenu)
     DoDefault()
     *-- Oggetto i_PopupCurForm contiene Thisform
     loMenuItem = m.loPopupMenu.AddMenuItem()
     loMenuItem.Caption = cp_Translate(MSG_EXCEL_EXPORT)
     loMenuItem.BeginGroup = .T.
     loMenuItem.Visible = .T.
     loMenuItem.OnClick = "i_PopupCurForm.w_oGrid.grd.CursorExport('E')"
     loMenuItem = m.loPopupMenu.AddMenuItem()
     loMenuItem.Caption = cp_Translate(MSG_DBF_EXPORT)
     loMenuItem.BeginGroup = .F.
     loMenuItem.Visible = .T.
     loMenuItem.OnClick = "i_PopupCurForm.w_oGrid.grd.CursorExport('D')"
     loMenuItem = m.loPopupMenu.AddMenuItem()
     loMenuItem.Caption = cp_Translate(MSG_CSV_EXPORT)
     loMenuItem.BeginGroup = .F.
     loMenuItem.Visible = .T.
     loMenuItem.OnClick = "i_PopupCurForm.w_oGrid.grd.CursorExport('C')"
  EndProc
  * --- Fine Area Manuale
  *--- Define Header
  add object oHeader as cp_headergadget with Width=this.Width, Left=0, Top=0, nNumPages=1 
  Height = This.Height + this.oHeader.Height

  * --- Define Page Frame
  add object oPgFrm as cp_oPgFrm with PageCount=1, Top=this.oHeader.Height, Width=this.Width, Height=this.Height-this.oHeader.Height
  proc oPgFrm.Init
    cp_oPgFrm::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_ggdPag1","gsut_ggd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oFooter = this.oPgFrm.Pages(1).oPag.oFooter
    this.w_oGrid = this.oPgFrm.Pages(1).oPag.oGrid
    DoDefault()
    proc Destroy()
      this.w_oFooter = .NULL.
      this.w_oGrid = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TITLE=space(254)
      .w_QUERY=space(254)
      .w_RET=.f.
      .w_DATETIME=space(20)
      .w_PROGRAM=space(250)
      .w_BTRSGRID=.f.
      .w_PRGDESCRI=space(50)
      .w_FONTCLR=0
      .w_PRGGRID=space(250)
      .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
          .DoRTCalc(1,7,.f.)
        .w_FONTCLR = Rgb(243,243,243)
      .oPgFrm.Page1.oPag.oGrid.Calculate(.w_QUERY)
        .w_PRGGRID = .w_PRGGRID
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
      	*--- Assegnamento caption ad oggetto header
          If .o_TITLE <> .w_TITLE
			   .oHeader.Calculate(.oPgFrm.Page1.oPag.oContained.w_TITLE)
          EndIf
        if .o_QUERY<>.w_QUERY
        .oPgFrm.Page1.oPag.oGrid.Calculate(.w_QUERY)
        endif
        .DoRTCalc(1,8,.t.)
            .w_PRGGRID = .w_PRGGRID
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oFooter.Calculate(.w_DATETIME, .w_PROGRAM, .w_PRGDESCRI)
        .oPgFrm.Page1.oPag.oGrid.Calculate(.w_QUERY)
    endwith
  return

  proc Calculate_OGRWDURRLK()
    with this
          * --- Refresh Grid
          .w_RET = RefreshGrid(This)
    endwith
  endproc
  proc Calculate_YCUUTVASEB()
    with this
          * --- Gadget Option
          .w_RET = This.GadgetOption()
          .w_RET = RefreshGrid(This)
    endwith
  endproc
  proc Calculate_LSNUCPBSNU()
    with this
          * --- Dimensioni gadget massimizzato
          .w_RET = CalcSize(This)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oFooter.Event(cEvent)
        if lower(cEvent)==lower("GadgetArranged") or lower(cEvent)==lower("GadgetOnDemand") or lower(cEvent)==lower("GadgetOnTimer") or lower(cEvent)==lower("oheader RefreshOnDemand")
          .Calculate_OGRWDURRLK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("GadgetOption")
          .Calculate_YCUUTVASEB()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oGrid.Event(cEvent)
        if lower(cEvent)==lower("GadgetMaximize Init") or lower(cEvent)==lower("GadgetMinimize Init")
          .Calculate_LSNUCPBSNU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_ggd
    If Lower(cEvent)='w_ogrid dblclick'
       With This
         If !Empty(.w_PRGGRID)
           Local cCursor,l_oldArea,l_oldError,ggname
           m.ggname = 'frm'+Alltrim(.Name)
           .cAlertMsg = ""
           m.l_oldError = On("Error")
           On Error &ggname..cAlertMsg=Message()
           
           m.l_oldArea = Select()
           Select(.w_oGrid.cCursor)
           Go Top
           Skip (.w_oGrid.CurrentRow-1)
           ExecScript(.w_PRGGRID)
           Select(m.l_oldArea)
           
           On error &l_oldError
         Endif
       Endwith
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TITLE = this.w_TITLE
    this.o_QUERY = this.w_QUERY
    return

enddefine

* --- Define pages as container
define class tgsut_ggdPag1 as StdContainer
  Width  = 133
  height = 90
  stdWidth  = 133
  stdheight = 90
  resizeXpos=93
  resizeYpos=35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFooter as cp_FooterGadget with uid="UMXGAILDJX",left=-1, top=67, width=133,height=26,;
    caption='Object',;
   bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 141738982


  add object oGrid as cp_VirtualZoom with uid="DKKFDHJZBW",left=3, top=3, width=127,height=61,;
    caption='Griglia',;
   bGlobalFont=.t.,;
    HotTrackingColor=16777215,HighLightColor=16777215,ScrollBarsWidth=8,bTrsHeaderTitle=.f.,HeaderFontBold=.t.,HeaderFontItalic=.f.,HeaderFontSize=0,;
    cEvent = "RefreshGrid",;
    nPag=1;
    , HelpContextID = 235193190
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_ggd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_ggd
*--- Refresh
Proc RefreshGrid(pParent)
   Local l_oldError,ggname
  With m.pParent
    .cAlertMsg = ""
    If Not Empty(.w_QUERY) And cp_FileExist(ForceExt(.w_QUERY,'VQR'))
      m.ggname = 'frm'+Alltrim(.Name)
      .cAlertMsg = ""
      m.l_oldError = On("Error")
      On Error &ggname..cAlertMsg=Message()
       
      *--- Trascodifica griglia
      If Vartype(.w_BTRSGRID)=='L' && Gestione trascodifiche
        .w_oGrid.bTrsHeaderTitle = .w_BTRSGRID
      Endif
       
      *--- Aggiorno la data di ultimo aggiornamento
      .tLastUpdate = DateTime()
      .w_DATETIME = Alltrim(TTOC(.tLastUpdate))+' '
       
      On error &l_oldError
       
      .mCalc(.T.)
      .SaveDependsOn()
      .NotifyEvent('RefreshGrid')
    Else
      .cAlertMsg = ah_MsgFormat('Query non definita o inesistente:%0"%1"', Alltrim(.w_QUERY))
    EndIf
  EndWith
EndProc

Proc CalcSize(pParent)
  With m.pParent
     If .MaxWidth=-1 And .MaxHeight=-1
       local nWidth,nHeight
       *--- Header e testata zoom
       m.nHeight = (.Height-.w_oGrid.Height)+.w_oGrid.nHeaderHeight+(.w_oGrid.nPadding*3)
       *--- Righe
       m.nHeight = m.nHeight + (.w_oGrid.Grd.RowHeight+.w_oGrid.Grd.nVOffset)*.w_oGrid.Grd.RowCount
       m.nHeight = m.nHeight - .w_oGrid.Grd.nVOffset + .w_oGrid.ScrollBarsWidth
       
       m.nWidth = (.Width-.w_oGrid.Width)+(.w_oGrid.nPadding*2)
       *--Colonne
       For i=1 To .w_oGrid.ColumnCount
         m.nWidth = m.nWidth + .w_oGrid.Grd.Columns(i).Width + .w_oGrid.Grd.nHOffset
       Endfor
       m.nWidth = m.nWidth - .w_oGrid.Grd.nHOffset + .w_oGrid.ScrollBarsWidth
       
       .MaxWidth = Max(.Width, m.nWidth)
       .MaxHeight = Max(.Height, m.nHeight)
     Else
       .MaxWidth = -1
       .MaxHeight = -1
     Endif
  EndWith
EndProc
* --- Fine Area Manuale
