* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_msp                                                        *
*              Chiusura partite da primanota                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_281]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-24                                                      *
* Last revis.: 2014-09-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gscg_msp
PARAMETERS pTipGes
* --- Fine Area Manuale
return(createobject("tgscg_msp"))

* --- Class definition
define class tgscg_msp as StdTrsForm
  Top    = 0
  Left   = 1

  * --- Standard Properties
  Width  = 925
  Height = 470+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-18"
  HelpContextID=171301527
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=95

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  SAL_DACO_IDX = 0
  CONTI_IDX = 0
  MOD_PAGA_IDX = 0
  VOCIIVA_IDX = 0
  VALUTE_IDX = 0
  DIS_TINT_IDX = 0
  cFile = "SAL_DACO"
  cKeySelect = "SASERIAL"
  cKeyWhere  = "SASERIAL=this.w_SASERIAL"
  cKeyDetail  = "SASERIAL=this.w_SASERIAL"
  cKeyWhereODBC = '"SASERIAL="+cp_ToStrODBC(this.w_SASERIAL)';

  cKeyDetailWhereODBC = '"SASERIAL="+cp_ToStrODBC(this.w_SASERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"SAL_DACO.SASERIAL="+cp_ToStrODBC(this.w_SASERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'SAL_DACO.CPROWNUM '
  cPrg = "gscg_msp"
  cComment = "Chiusura partite da primanota"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 11
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PUNTAT = space(10)
  w_PUNT = space(10)
  w_CLASS = space(10)
  w_SASERIAL = space(10)
  w_TIPCON = space(1)
  w_CAOVAL = 0
  w_DECTOT = 0
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_SEGSAL = space(1)
  w_NUMVAL = 0
  w_CODCON = space(15)
  w_FASE = 0
  w_FLDIST = space(1)
  w_CALFIR = space(3)
  w_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_CODVAL = space(3)
  w_DINUMERO = 0
  w_PAGRD = space(2)
  w_PAGRI = space(2)
  w_PAGBO = space(2)
  w_PAGRB = space(2)
  w_PAGMA = space(2)
  w_PAGCA = space(2)
  w_FLSACC = space(2)
  w_SIMVAL = space(5)
  w_TOTDAV = 0
  w_FLANEV = space(1)
  w_SIMBVA = space(5)
  w_CAONAZ = 0
  w_DATRIF = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CODNAZ = space(3)
  w_CODVAO = space(3)
  w_DECTOP = 0
  w_VALDEF = space(3)
  w_SIMVAL = space(5)
  w_SIMVAL = space(5)
  w_SADATSCA = ctod('  /  /  ')
  w_SANUMPAR = space(31)
  w_SACODCON = space(15)
  w_SAMODPAG = space(10)
  w_SA_SEGNO = space(1)
  w_SATOTIMP = 0
  w_SACODVAL = space(3)
  w_SAIMPSAL = 0
  o_SAIMPSAL = 0
  w_SAFLSALD = space(1)
  o_SAFLSALD = space(1)
  w_SAIMPABB = 0
  o_SAIMPABB = 0
  w_DARSAL = 0
  w_DIFCAM = 0
  w_SACAOAPE = 0
  o_SACAOAPE = 0
  w_FLSALD = space(1)
  w_SANUMDOC = 0
  w_SAALFDOC = space(10)
  w_SADATDOC = ctod('  /  /  ')
  w_SADATAPE = ctod('  /  /  ')
  w_IMPDOC = 0
  w_SACAOVAL = 0
  w_DARSAL1 = 0
  w_EURVAL = 0
  w_DECRIG = 0
  w_TOTSAL = 0
  w_TOTRES = 0
  w_FLACC = space(1)
  w_ABB = 0
  w_SADESRIG = space(50)
  w_SIMVAO = space(5)
  w_TOTDAO = 0
  w_VALSAL = 0
  w_VALRES = 0
  w_SIMVAO = space(5)
  w_SIMVAO = space(5)
  w_SACODAGE = space(5)
  w_SASERRIF = space(10)
  w_SAORDRIF = 0
  w_SATIPCON = space(1)
  w_SANUMRIF = 0
  w_SAFLVABD = space(1)
  w_DESCON1 = space(40)
  w_FLAGAC = space(1)
  w_SEGNO = space(1)
  w_FLACCESS = .F.
  w_RIFDIS = space(10)
  w_CATPAG = space(2)
  w_GPRIFDIS = space(10)
  w_TIPSBF = space(1)
  w_DATDIS = ctod('  /  /  ')
  w_NUMERO = 0
  w_SELEZIONATO = .F.
  w_SELALL = space(1)
  o_SELALL = space(1)
  w_FLRAGG = 0
  w_SABANAPP = space(10)
  w_SABANNOS = space(15)
  w_ABB = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_msp
  pTipGes=' '
  * ---- Intercetta la Save standard e Conferma Chiusura Partite
  proc ecpSave()
         this.w_PUNT.NotifyEvent('ChiudePartite')
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SAL_DACO','gscg_msp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mspPag1","gscg_msp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Chiusura partite")
      .Pages(1).HelpContextID = 140879380
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gscg_msp
      * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Imposta il Titolo della Finestra della Gestione in Base al tipo
    WITH THIS.PARENT
       IF Type('pTipGes')='U' Or EMPTY(pTipGes)
          .pTipges = 'P'
       ELSE
          .pTipges = pTipges
       ENDIF
    
       DO CASE
            CASE .pTipGes = 'P'
                   .cComment = Ah_Msgformat('Chiusura partite da primanota')
             CASE .pTipGes = 'S'
                   .cComment = Ah_Msgformat('Chiusura partite da saldaconto')
             CASE .pTipGes = 'B'
                   .cComment = Ah_Msgformat('Chiusura conti SBF')
             CASE .pTipGes = 'A'
                   .cComment = Ah_Msgformat('Abbina parite di acconto ')
       ENDCASE
    ENDWITH
    
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ABB = this.oPgFrm.Pages(1).oPag.ABB
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_ABB = .NULL.
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='MOD_PAGA'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='DIS_TINT'
    this.cWorkTables[6]='SAL_DACO'
    * --- Area Manuale = Open Work Table
    * --- gscg_msp
       * colora le righe raggruppate in modo differente
              This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor= ;
                "IIF(EMPTY(NVL(t_FLRAGG,0)),RGB(255,255,255),RGB(255,64,64))"
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SAL_DACO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SAL_DACO_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_SASERIAL = NVL(SASERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from SAL_DACO where SASERIAL=KeySet.SASERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.SAL_DACO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_DACO_IDX,2],this.bLoadRecFilter,this.SAL_DACO_IDX,"gscg_msp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SAL_DACO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SAL_DACO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SAL_DACO '
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SASERIAL',this.w_SASERIAL  )
      select * from (i_cTable) SAL_DACO where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PUNTAT = IIF(TYPE('THIS .w_PUNTAT')<>'O', p_PUNTAT, .w_PUNTAT)
        .w_PUNT = IIF(UPPER(.w_PUNTAT.OPARENTOBJECT.CLASS)="TGSCG_MSC",.w_PUNTAT.OPARENTOBJECT,.w_PUNTAT)
        .w_TIPCON = IIF( .pTIPGES<>'B',.w_PUNTAT.oParentObject .w_PNTIPCON,Space(15))
        .w_CAOVAL = IIF(.w_PUNTAT.oParentObject .w_PNCAOVAL=0, g_CAOVAL, .w_PUNTAT.oParentObject .w_PNCAOVAL)
        .w_DECTOT = .w_PUNTAT.oParentObject .w_DECTOT
        .w_IMPDAR = .w_PUNTAT.oParentObject .w_PNIMPDAR
        .w_IMPAVE = .w_PUNTAT.oParentObject .w_PNIMPAVE
        .w_SEGSAL = IIF(.w_IMPDAR<>0, 'A', IIF(.w_IMPAVE<>0, 'D', IIF(.w_TIPCON='C', 'D', 'A')))
        .w_NUMVAL = 0
        .w_FASE = 0
        .w_FLDIST = space(1)
        .w_CALFIR = space(3)
        .w_SCAINI = ctod("  /  /  ")
        .w_SCAFIN = i_datsys + 365
        .w_CODVAL = SPACE(3)
        .w_DINUMERO = 0
        .w_PAGRD = 'RD'
        .w_PAGRI = 'RI'
        .w_PAGBO = 'BO'
        .w_PAGRB = 'RB'
        .w_PAGMA = 'MA'
        .w_PAGCA = 'CA'
        .w_FLSACC = IIF(.w_PUNTAT.oParentObject .w_FLAGAC='S','S',' ')
        .w_SIMVAL = g_VALSIM
        .w_TOTDAV = IIF(.w_SEGSAL='D', .w_IMPAVE-.w_IMPDAR, .w_IMPDAR-.w_IMPAVE)
        .w_FLANEV = space(1)
        .w_SIMBVA = space(5)
        .w_CAONAZ = IIF(.w_PUNTAT.oParentObject .w_CAONAZ=0, g_CAOVAL, .w_PUNTAT.oParentObject .w_CAONAZ)
        .w_DATRIF = IIF(EMPTY(.w_PUNTAT.oParentObject .w_PNDATDOC), .w_PUNTAT.oParentObject .w_PNDATREG, .w_PUNTAT.oParentObject .w_PNDATDOC)
        .w_OBTEST = .w_PUNTAT.oParentObject .w_PNDATREG
        .w_DATOBSO = ctod("  /  /  ")
        .w_CODNAZ = .w_PUNTAT.oParentObject .w_PNVALNAZ
        .w_CODVAO = .w_PUNTAT.oParentObject .w_PNCODVAL
        .w_DECTOP = .w_PUNTAT.oParentObject .w_DECTOP
        .w_VALDEF = .w_PUNTAT.oParentObject .w_PNCODVAL
        .w_SIMVAL = g_VALSIM
        .w_SIMVAL = g_VALSIM
        .w_TOTSAL = 0
        .w_SIMVAO = .w_PUNTAT.oParentObject .w_SIMVAL
        .w_TOTDAO = IIF(.w_CODNAZ=.w_CODVAO, 0, cp_ROUND(MON2VAL(.w_TOTDAV, .w_CAOVAL, .w_CAONAZ, .w_DATRIF, .w_CODNAZ), .w_DECRIG))
        .w_VALSAL = 0
        .w_SIMVAO = .w_PUNTAT.oParentObject .w_SIMVAL
        .w_SIMVAO = .w_PUNTAT.oParentObject .w_SIMVAL
        .w_FLAGAC = .w_PUNTAT.oParentObject .w_FLAGAC
        .w_SEGNO = IIF(.w_FLAGAC='S',IIF(.w_SEGSAL='D','A','D'),.w_SEGSAL)
        .w_FLACCESS = .T.
        .w_RIFDIS = space(10)
        .w_CATPAG = space(2)
        .w_GPRIFDIS = space(10)
        .w_DATDIS = ctod("  /  /  ")
        .w_NUMERO = 0
        .w_SELEZIONATO = .F.
        .w_SELALL = 'N'
        .w_CLASS = .w_PUNTAT.OPARENTOBJECT.CLASS
        .w_SASERIAL = NVL(SASERIAL,space(10))
        .w_CODCON = IIF( .pTIPGES<>'B',.w_PUNTAT.oParentObject .w_PNCODCON,Space(15))
          .link_1_18('Load')
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .w_TOTRES = IIF(.w_TOTDAV=0, 0, IIF(.w_FLAGAC='S',Abs(.w_TOTDAV)-Abs(.w_TOTSAL),.w_TOTDAV-.w_TOTSAL))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .w_VALRES = IIF(.w_TOTDAO=0, 0, IIF(.w_FLAGAC='S',Abs(.w_TOTDAO)-Abs(.w_VALSAL),.w_TOTDAO-.w_VALSAL))
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(AH_Msgformat(IIF(.pTIPGES='B', 'Data valuta da:','Scadenze da:')))
        .w_TIPSBF = iif(.pTIPGES='B' ,'S','N')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'SAL_DACO')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DIFCAM = 0
          .w_IMPDOC = 0
          .w_EURVAL = 0
          .w_DECRIG = 0
        .w_FLACC = ' '
          .w_DESCON1 = space(40)
          .w_FLRAGG = 0
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_SADATSCA = NVL(cp_ToDate(SADATSCA),ctod("  /  /  "))
          .w_SANUMPAR = NVL(SANUMPAR,space(31))
          .w_SACODCON = NVL(SACODCON,space(15))
          if link_2_3_joined
            this.w_SACODCON = NVL(ANCODICE203,NVL(this.w_SACODCON,space(15)))
            this.w_DESCON1 = NVL(ANDESCRI203,space(40))
          else
          .link_2_3('Load')
          endif
          .w_SAMODPAG = NVL(SAMODPAG,space(10))
          .w_SA_SEGNO = NVL(SA_SEGNO,space(1))
          .w_SATOTIMP = NVL(SATOTIMP,0)
          .w_SACODVAL = NVL(SACODVAL,space(3))
          .w_SAIMPSAL = NVL(SAIMPSAL,0)
          .w_SAFLSALD = NVL(SAFLSALD,space(1))
          .w_SAIMPABB = NVL(SAIMPABB,0)
        .w_DARSAL = (.w_SAIMPSAL+.w_SAIMPABB)*IIF(.w_SEGNO=.w_SA_SEGNO, 1, -1)
          .w_SACAOAPE = NVL(SACAOAPE,0)
        .w_FLSALD = .w_SAFLSALD
          .w_SANUMDOC = NVL(SANUMDOC,0)
          .w_SAALFDOC = NVL(SAALFDOC,space(10))
          .w_SADATDOC = NVL(cp_ToDate(SADATDOC),ctod("  /  /  "))
          .w_SADATAPE = NVL(cp_ToDate(SADATAPE),ctod("  /  /  "))
          .w_SACAOVAL = NVL(SACAOVAL,0)
        .w_DARSAL1 = IIF(.w_SACODVAL=.w_CODNAZ, .w_DARSAL, cp_ROUND(VAL2MON(.w_DARSAL, .w_SACAOVAL, .w_CAONAZ, .w_DATRIF, .w_CODNAZ),.w_DECTOP))
        .oPgFrm.Page1.oPag.oObj_2_24.Calculate(AH_Msgformat(IIF(.w_EURVAL=0,'Differenza:','')))
        .oPgFrm.Page1.oPag.ABB.Calculate(AH_Msgformat( iif(.pTIPGES='B' ,'',IIF(.w_FLACC='S','Acconto:',IIF(((.w_SAIMPABB>0 AND .w_SA_SEGNO='D') OR (.w_SAIMPABB<0 AND .w_SA_SEGNO='A')) AND NOT EMPTY(.w_SAIMPABB),'Abb. passivo:',IIF(NOT EMPTY(.w_SAIMPABB),'Abb. attivo:','Abbuono:')))) ))
        .w_ABB = ABS(.w_SAIMPABB)
          .w_SADESRIG = NVL(SADESRIG,space(50))
          .w_SACODAGE = NVL(SACODAGE,space(5))
          .w_SASERRIF = NVL(SASERRIF,space(10))
          .w_SAORDRIF = NVL(SAORDRIF,0)
          .w_SATIPCON = NVL(SATIPCON,space(1))
          .w_SANUMRIF = NVL(SANUMRIF,0)
          .w_SAFLVABD = NVL(SAFLVABD,space(1))
          .w_SABANAPP = NVL(SABANAPP,space(10))
          .w_SABANNOS = NVL(SABANNOS,space(15))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CLASS = .w_PUNTAT.OPARENTOBJECT.CLASS
        .w_CODCON = IIF( .pTIPGES<>'B',.w_PUNTAT.oParentObject .w_PNCODCON,Space(15))
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .w_TOTRES = IIF(.w_TOTDAV=0, 0, IIF(.w_FLAGAC='S',Abs(.w_TOTDAV)-Abs(.w_TOTSAL),.w_TOTDAV-.w_TOTSAL))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .w_VALRES = IIF(.w_TOTDAO=0, 0, IIF(.w_FLAGAC='S',Abs(.w_TOTDAO)-Abs(.w_VALSAL),.w_TOTDAO-.w_VALSAL))
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(AH_Msgformat(IIF(.pTIPGES='B', 'Data valuta da:','Scadenze da:')))
        .w_TIPSBF = iif(.pTIPGES='B' ,'S','N')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_29.enabled = .oPgFrm.Page1.oPag.oBtn_1_29.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_39.enabled = .oPgFrm.Page1.oPag.oBtn_1_39.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_40.enabled = .oPgFrm.Page1.oPag.oBtn_1_40.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_msp
    * --- 'Trucco' per evitare di entrare in 'Query' al termine della Load
    IF TYPE('p_puntat')<>'O'
           IF TYPE('this.w_PUNTAT') <> 'O'
                    * --- Deve fare 2 'giri' nella BlankRec prima di Uscire, (se esce subito da errore)
           	this.ecpQuit()
           ELSE
                    * --- Primo Giro
           	this.w_PUNTAT = SPACE(10)
           ENDIF
           RETURN
    ENDIF
    
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PUNTAT=space(10)
      .w_PUNT=space(10)
      .w_CLASS=space(10)
      .w_SASERIAL=space(10)
      .w_TIPCON=space(1)
      .w_CAOVAL=0
      .w_DECTOT=0
      .w_IMPDAR=0
      .w_IMPAVE=0
      .w_SEGSAL=space(1)
      .w_NUMVAL=0
      .w_CODCON=space(15)
      .w_FASE=0
      .w_FLDIST=space(1)
      .w_CALFIR=space(3)
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_CODVAL=space(3)
      .w_DINUMERO=0
      .w_PAGRD=space(2)
      .w_PAGRI=space(2)
      .w_PAGBO=space(2)
      .w_PAGRB=space(2)
      .w_PAGMA=space(2)
      .w_PAGCA=space(2)
      .w_FLSACC=space(2)
      .w_SIMVAL=space(5)
      .w_TOTDAV=0
      .w_FLANEV=space(1)
      .w_SIMBVA=space(5)
      .w_CAONAZ=0
      .w_DATRIF=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CODNAZ=space(3)
      .w_CODVAO=space(3)
      .w_DECTOP=0
      .w_VALDEF=space(3)
      .w_SIMVAL=space(5)
      .w_SIMVAL=space(5)
      .w_SADATSCA=ctod("  /  /  ")
      .w_SANUMPAR=space(31)
      .w_SACODCON=space(15)
      .w_SAMODPAG=space(10)
      .w_SA_SEGNO=space(1)
      .w_SATOTIMP=0
      .w_SACODVAL=space(3)
      .w_SAIMPSAL=0
      .w_SAFLSALD=space(1)
      .w_SAIMPABB=0
      .w_DARSAL=0
      .w_DIFCAM=0
      .w_SACAOAPE=0
      .w_FLSALD=space(1)
      .w_SANUMDOC=0
      .w_SAALFDOC=space(10)
      .w_SADATDOC=ctod("  /  /  ")
      .w_SADATAPE=ctod("  /  /  ")
      .w_IMPDOC=0
      .w_SACAOVAL=0
      .w_DARSAL1=0
      .w_EURVAL=0
      .w_DECRIG=0
      .w_TOTSAL=0
      .w_TOTRES=0
      .w_FLACC=space(1)
      .w_ABB=0
      .w_SADESRIG=space(50)
      .w_SIMVAO=space(5)
      .w_TOTDAO=0
      .w_VALSAL=0
      .w_VALRES=0
      .w_SIMVAO=space(5)
      .w_SIMVAO=space(5)
      .w_SACODAGE=space(5)
      .w_SASERRIF=space(10)
      .w_SAORDRIF=0
      .w_SATIPCON=space(1)
      .w_SANUMRIF=0
      .w_SAFLVABD=space(1)
      .w_DESCON1=space(40)
      .w_FLAGAC=space(1)
      .w_SEGNO=space(1)
      .w_FLACCESS=.f.
      .w_RIFDIS=space(10)
      .w_CATPAG=space(2)
      .w_GPRIFDIS=space(10)
      .w_TIPSBF=space(1)
      .w_DATDIS=ctod("  /  /  ")
      .w_NUMERO=0
      .w_SELEZIONATO=.f.
      .w_SELALL=space(1)
      .w_FLRAGG=0
      .w_SABANAPP=space(10)
      .w_SABANNOS=space(15)
      if .cFunction<>"Filter"
        .w_PUNTAT = IIF(TYPE('THIS .w_PUNTAT')<>'O', p_PUNTAT, .w_PUNTAT)
        .w_PUNT = IIF(UPPER(.w_PUNTAT.OPARENTOBJECT.CLASS)="TGSCG_MSC",.w_PUNTAT.OPARENTOBJECT,.w_PUNTAT)
        .w_CLASS = .w_PUNTAT.OPARENTOBJECT.CLASS
        .DoRTCalc(4,4,.f.)
        .w_TIPCON = IIF( .pTIPGES<>'B',.w_PUNTAT.oParentObject .w_PNTIPCON,Space(15))
        .w_CAOVAL = IIF(.w_PUNTAT.oParentObject .w_PNCAOVAL=0, g_CAOVAL, .w_PUNTAT.oParentObject .w_PNCAOVAL)
        .w_DECTOT = .w_PUNTAT.oParentObject .w_DECTOT
        .w_IMPDAR = .w_PUNTAT.oParentObject .w_PNIMPDAR
        .w_IMPAVE = .w_PUNTAT.oParentObject .w_PNIMPAVE
        .w_SEGSAL = IIF(.w_IMPDAR<>0, 'A', IIF(.w_IMPAVE<>0, 'D', IIF(.w_TIPCON='C', 'D', 'A')))
        .DoRTCalc(11,11,.f.)
        .w_CODCON = IIF( .pTIPGES<>'B',.w_PUNTAT.oParentObject .w_PNCODCON,Space(15))
        .w_FASE = 0
        .DoRTCalc(14,16,.f.)
        .w_SCAFIN = i_datsys + 365
        .w_CODVAL = SPACE(3)
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CODVAL))
         .link_1_18('Full')
        endif
        .DoRTCalc(19,19,.f.)
        .w_PAGRD = 'RD'
        .w_PAGRI = 'RI'
        .w_PAGBO = 'BO'
        .w_PAGRB = 'RB'
        .w_PAGMA = 'MA'
        .w_PAGCA = 'CA'
        .w_FLSACC = IIF(.w_PUNTAT.oParentObject .w_FLAGAC='S','S',' ')
        .w_SIMVAL = g_VALSIM
        .w_TOTDAV = IIF(.w_SEGSAL='D', .w_IMPAVE-.w_IMPDAR, .w_IMPDAR-.w_IMPAVE)
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .DoRTCalc(29,30,.f.)
        .w_CAONAZ = IIF(.w_PUNTAT.oParentObject .w_CAONAZ=0, g_CAOVAL, .w_PUNTAT.oParentObject .w_CAONAZ)
        .w_DATRIF = IIF(EMPTY(.w_PUNTAT.oParentObject .w_PNDATDOC), .w_PUNTAT.oParentObject .w_PNDATREG, .w_PUNTAT.oParentObject .w_PNDATDOC)
        .w_OBTEST = .w_PUNTAT.oParentObject .w_PNDATREG
        .DoRTCalc(34,34,.f.)
        .w_CODNAZ = .w_PUNTAT.oParentObject .w_PNVALNAZ
        .w_CODVAO = .w_PUNTAT.oParentObject .w_PNCODVAL
        .w_DECTOP = .w_PUNTAT.oParentObject .w_DECTOP
        .w_VALDEF = .w_PUNTAT.oParentObject .w_PNCODVAL
        .w_SIMVAL = g_VALSIM
        .w_SIMVAL = g_VALSIM
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .DoRTCalc(41,43,.f.)
        if not(empty(.w_SACODCON))
         .link_2_3('Full')
        endif
        .DoRTCalc(44,50,.f.)
        .w_DARSAL = (.w_SAIMPSAL+.w_SAIMPABB)*IIF(.w_SEGNO=.w_SA_SEGNO, 1, -1)
        .DoRTCalc(52,53,.f.)
        .w_FLSALD = .w_SAFLSALD
        .DoRTCalc(55,60,.f.)
        .w_DARSAL1 = IIF(.w_SACODVAL=.w_CODNAZ, .w_DARSAL, cp_ROUND(VAL2MON(.w_DARSAL, .w_SACAOVAL, .w_CAONAZ, .w_DATRIF, .w_CODNAZ),.w_DECTOP))
        .DoRTCalc(62,64,.f.)
        .w_TOTRES = IIF(.w_TOTDAV=0, 0, IIF(.w_FLAGAC='S',Abs(.w_TOTDAV)-Abs(.w_TOTSAL),.w_TOTDAV-.w_TOTSAL))
        .oPgFrm.Page1.oPag.oObj_2_24.Calculate(AH_Msgformat(IIF(.w_EURVAL=0,'Differenza:','')))
        .w_FLACC = ' '
        .oPgFrm.Page1.oPag.ABB.Calculate(AH_Msgformat( iif(.pTIPGES='B' ,'',IIF(.w_FLACC='S','Acconto:',IIF(((.w_SAIMPABB>0 AND .w_SA_SEGNO='D') OR (.w_SAIMPABB<0 AND .w_SA_SEGNO='A')) AND NOT EMPTY(.w_SAIMPABB),'Abb. passivo:',IIF(NOT EMPTY(.w_SAIMPABB),'Abb. attivo:','Abbuono:')))) ))
        .w_ABB = ABS(.w_SAIMPABB)
        .DoRTCalc(68,68,.f.)
        .w_SIMVAO = .w_PUNTAT.oParentObject .w_SIMVAL
        .w_TOTDAO = IIF(.w_CODNAZ=.w_CODVAO, 0, cp_ROUND(MON2VAL(.w_TOTDAV, .w_CAOVAL, .w_CAONAZ, .w_DATRIF, .w_CODNAZ), .w_DECRIG))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .DoRTCalc(71,71,.f.)
        .w_VALRES = IIF(.w_TOTDAO=0, 0, IIF(.w_FLAGAC='S',Abs(.w_TOTDAO)-Abs(.w_VALSAL),.w_TOTDAO-.w_VALSAL))
        .w_SIMVAO = .w_PUNTAT.oParentObject .w_SIMVAL
        .w_SIMVAO = .w_PUNTAT.oParentObject .w_SIMVAL
        .DoRTCalc(75,81,.f.)
        .w_FLAGAC = .w_PUNTAT.oParentObject .w_FLAGAC
        .w_SEGNO = IIF(.w_FLAGAC='S',IIF(.w_SEGSAL='D','A','D'),.w_SEGSAL)
        .w_FLACCESS = .T.
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(AH_Msgformat(IIF(.pTIPGES='B', 'Data valuta da:','Scadenze da:')))
        .DoRTCalc(85,87,.f.)
        .w_TIPSBF = iif(.pTIPGES='B' ,'S','N')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(89,90,.f.)
        .w_SELEZIONATO = .F.
        .w_SELALL = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'SAL_DACO')
    this.DoRTCalc(93,95,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_msp
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSCAINI_1_16.enabled = i_bVal
      .Page1.oPag.oSCAFIN_1_17.enabled = i_bVal
      .Page1.oPag.oCODVAL_1_18.enabled = i_bVal
      .Page1.oPag.oPAGRD_1_20.enabled = i_bVal
      .Page1.oPag.oPAGRI_1_21.enabled = i_bVal
      .Page1.oPag.oPAGBO_1_22.enabled = i_bVal
      .Page1.oPag.oPAGRB_1_23.enabled = i_bVal
      .Page1.oPag.oPAGMA_1_24.enabled = i_bVal
      .Page1.oPag.oPAGCA_1_25.enabled = i_bVal
      .Page1.oPag.oFLSACC_1_26.enabled = i_bVal
      .Page1.oPag.oSACAOAPE_2_13.enabled = i_bVal
      .Page1.oPag.oSACAOVAL_2_20.enabled = i_bVal
      .Page1.oPag.oFLACC_2_25.enabled = i_bVal
      .Page1.oPag.oSADESRIG_2_28.enabled = i_bVal
      .Page1.oPag.oTOTDAO_1_60.enabled = i_bVal
      .Page1.oPag.oSELALL_1_82.enabled_(i_bVal)
      .Page1.oPag.oBtn_1_29.enabled = .Page1.oPag.oBtn_1_29.mCond()
      .Page1.oPag.oBtn_1_36.enabled = i_bVal
      .Page1.oPag.oBtn_1_39.enabled = .Page1.oPag.oBtn_1_39.mCond()
      .Page1.oPag.oBtn_1_40.enabled = .Page1.oPag.oBtn_1_40.mCond()
      .Page1.oPag.oObj_1_30.enabled = i_bVal
      .Page1.oPag.oObj_1_31.enabled = i_bVal
      .Page1.oPag.oObj_1_38.enabled = i_bVal
      .Page1.oPag.oObj_1_57.enabled = i_bVal
      .Page1.oPag.ABB.enabled = i_bVal
      .Page1.oPag.oObj_1_62.enabled = i_bVal
      .Page1.oPag.oObj_1_72.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'SAL_DACO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SAL_DACO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SASERIAL,"SASERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SAL_DACO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_DACO_IDX,2])
    i_lTable = "SAL_DACO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SAL_DACO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_SADATSCA D(8);
      ,t_SANUMPAR C(31);
      ,t_SACODCON C(15);
      ,t_SAMODPAG C(10);
      ,t_SA_SEGNO C(1);
      ,t_SATOTIMP N(18,4);
      ,t_SACODVAL C(3);
      ,t_SAIMPSAL N(18,4);
      ,t_SAFLSALD N(3);
      ,t_DIFCAM N(18,4);
      ,t_SACAOAPE N(12,7);
      ,t_SACAOVAL N(12,7);
      ,t_FLACC N(3);
      ,t_ABB N(18,4);
      ,t_SADESRIG C(50);
      ,t_DESCON1 C(40);
      ,CPROWNUM N(10);
      ,t_SAIMPABB N(18,4);
      ,t_DARSAL N(18,4);
      ,t_FLSALD C(1);
      ,t_SANUMDOC N(15);
      ,t_SAALFDOC C(10);
      ,t_SADATDOC D(8);
      ,t_SADATAPE D(8);
      ,t_IMPDOC N(18,4);
      ,t_DARSAL1 N(18,4);
      ,t_EURVAL N(12,7);
      ,t_DECRIG N(1);
      ,t_SACODAGE C(5);
      ,t_SASERRIF C(10);
      ,t_SAORDRIF N(4);
      ,t_SATIPCON C(1);
      ,t_SANUMRIF N(3);
      ,t_SAFLVABD C(1);
      ,t_FLRAGG N(1);
      ,t_SABANAPP C(10);
      ,t_SABANNOS C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mspbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSADATSCA_2_1.controlsource=this.cTrsName+'.t_SADATSCA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSANUMPAR_2_2.controlsource=this.cTrsName+'.t_SANUMPAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSACODCON_2_3.controlsource=this.cTrsName+'.t_SACODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSAMODPAG_2_4.controlsource=this.cTrsName+'.t_SAMODPAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSA_SEGNO_2_5.controlsource=this.cTrsName+'.t_SA_SEGNO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSATOTIMP_2_6.controlsource=this.cTrsName+'.t_SATOTIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSACODVAL_2_7.controlsource=this.cTrsName+'.t_SACODVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSAIMPSAL_2_8.controlsource=this.cTrsName+'.t_SAIMPSAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSAFLSALD_2_9.controlsource=this.cTrsName+'.t_SAFLSALD'
    this.oPgFRm.Page1.oPag.oDIFCAM_2_12.controlsource=this.cTrsName+'.t_DIFCAM'
    this.oPgFRm.Page1.oPag.oSACAOAPE_2_13.controlsource=this.cTrsName+'.t_SACAOAPE'
    this.oPgFRm.Page1.oPag.oSACAOVAL_2_20.controlsource=this.cTrsName+'.t_SACAOVAL'
    this.oPgFRm.Page1.oPag.oFLACC_2_25.controlsource=this.cTrsName+'.t_FLACC'
    this.oPgFRm.Page1.oPag.oABB_2_27.controlsource=this.cTrsName+'.t_ABB'
    this.oPgFRm.Page1.oPag.oSADESRIG_2_28.controlsource=this.cTrsName+'.t_SADESRIG'
    this.oPgFRm.Page1.oPag.oDESCON1_2_35.controlsource=this.cTrsName+'.t_DESCON1'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(83)
    this.AddVLine(316)
    this.AddVLine(454)
    this.AddVLine(530)
    this.AddVLine(550)
    this.AddVLine(685)
    this.AddVLine(731)
    this.AddVLine(869)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSADATSCA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SAL_DACO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_DACO_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SAL_DACO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SAL_DACO_IDX,2])
      *
      * insert into SAL_DACO
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SAL_DACO')
        i_extval=cp_InsertValODBCExtFlds(this,'SAL_DACO')
        i_cFldBody=" "+;
                  "(SASERIAL,SADATSCA,SANUMPAR,SACODCON,SAMODPAG"+;
                  ",SA_SEGNO,SATOTIMP,SACODVAL,SAIMPSAL,SAFLSALD"+;
                  ",SAIMPABB,SACAOAPE,SANUMDOC,SAALFDOC,SADATDOC"+;
                  ",SADATAPE,SACAOVAL,SADESRIG,SACODAGE,SASERRIF"+;
                  ",SAORDRIF,SATIPCON,SANUMRIF,SAFLVABD,SABANAPP"+;
                  ",SABANNOS,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_SASERIAL)+","+cp_ToStrODBC(this.w_SADATSCA)+","+cp_ToStrODBC(this.w_SANUMPAR)+","+cp_ToStrODBCNull(this.w_SACODCON)+","+cp_ToStrODBC(this.w_SAMODPAG)+;
             ","+cp_ToStrODBC(this.w_SA_SEGNO)+","+cp_ToStrODBC(this.w_SATOTIMP)+","+cp_ToStrODBC(this.w_SACODVAL)+","+cp_ToStrODBC(this.w_SAIMPSAL)+","+cp_ToStrODBC(this.w_SAFLSALD)+;
             ","+cp_ToStrODBC(this.w_SAIMPABB)+","+cp_ToStrODBC(this.w_SACAOAPE)+","+cp_ToStrODBC(this.w_SANUMDOC)+","+cp_ToStrODBC(this.w_SAALFDOC)+","+cp_ToStrODBC(this.w_SADATDOC)+;
             ","+cp_ToStrODBC(this.w_SADATAPE)+","+cp_ToStrODBC(this.w_SACAOVAL)+","+cp_ToStrODBC(this.w_SADESRIG)+","+cp_ToStrODBC(this.w_SACODAGE)+","+cp_ToStrODBC(this.w_SASERRIF)+;
             ","+cp_ToStrODBC(this.w_SAORDRIF)+","+cp_ToStrODBC(this.w_SATIPCON)+","+cp_ToStrODBC(this.w_SANUMRIF)+","+cp_ToStrODBC(this.w_SAFLVABD)+","+cp_ToStrODBC(this.w_SABANAPP)+;
             ","+cp_ToStrODBC(this.w_SABANNOS)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SAL_DACO')
        i_extval=cp_InsertValVFPExtFlds(this,'SAL_DACO')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'SASERIAL',this.w_SASERIAL)
        INSERT INTO (i_cTable) (;
                   SASERIAL;
                  ,SADATSCA;
                  ,SANUMPAR;
                  ,SACODCON;
                  ,SAMODPAG;
                  ,SA_SEGNO;
                  ,SATOTIMP;
                  ,SACODVAL;
                  ,SAIMPSAL;
                  ,SAFLSALD;
                  ,SAIMPABB;
                  ,SACAOAPE;
                  ,SANUMDOC;
                  ,SAALFDOC;
                  ,SADATDOC;
                  ,SADATAPE;
                  ,SACAOVAL;
                  ,SADESRIG;
                  ,SACODAGE;
                  ,SASERRIF;
                  ,SAORDRIF;
                  ,SATIPCON;
                  ,SANUMRIF;
                  ,SAFLVABD;
                  ,SABANAPP;
                  ,SABANNOS;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_SASERIAL;
                  ,this.w_SADATSCA;
                  ,this.w_SANUMPAR;
                  ,this.w_SACODCON;
                  ,this.w_SAMODPAG;
                  ,this.w_SA_SEGNO;
                  ,this.w_SATOTIMP;
                  ,this.w_SACODVAL;
                  ,this.w_SAIMPSAL;
                  ,this.w_SAFLSALD;
                  ,this.w_SAIMPABB;
                  ,this.w_SACAOAPE;
                  ,this.w_SANUMDOC;
                  ,this.w_SAALFDOC;
                  ,this.w_SADATDOC;
                  ,this.w_SADATAPE;
                  ,this.w_SACAOVAL;
                  ,this.w_SADESRIG;
                  ,this.w_SACODAGE;
                  ,this.w_SASERRIF;
                  ,this.w_SAORDRIF;
                  ,this.w_SATIPCON;
                  ,this.w_SANUMRIF;
                  ,this.w_SAFLVABD;
                  ,this.w_SABANAPP;
                  ,this.w_SABANNOS;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.SAL_DACO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SAL_DACO_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_SACAOAPE<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'SAL_DACO')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'SAL_DACO')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_SACAOAPE<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update SAL_DACO
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'SAL_DACO')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " SADATSCA="+cp_ToStrODBC(this.w_SADATSCA)+;
                     ",SANUMPAR="+cp_ToStrODBC(this.w_SANUMPAR)+;
                     ",SACODCON="+cp_ToStrODBCNull(this.w_SACODCON)+;
                     ",SAMODPAG="+cp_ToStrODBC(this.w_SAMODPAG)+;
                     ",SA_SEGNO="+cp_ToStrODBC(this.w_SA_SEGNO)+;
                     ",SATOTIMP="+cp_ToStrODBC(this.w_SATOTIMP)+;
                     ",SACODVAL="+cp_ToStrODBC(this.w_SACODVAL)+;
                     ",SAIMPSAL="+cp_ToStrODBC(this.w_SAIMPSAL)+;
                     ",SAFLSALD="+cp_ToStrODBC(this.w_SAFLSALD)+;
                     ",SAIMPABB="+cp_ToStrODBC(this.w_SAIMPABB)+;
                     ",SACAOAPE="+cp_ToStrODBC(this.w_SACAOAPE)+;
                     ",SANUMDOC="+cp_ToStrODBC(this.w_SANUMDOC)+;
                     ",SAALFDOC="+cp_ToStrODBC(this.w_SAALFDOC)+;
                     ",SADATDOC="+cp_ToStrODBC(this.w_SADATDOC)+;
                     ",SADATAPE="+cp_ToStrODBC(this.w_SADATAPE)+;
                     ",SACAOVAL="+cp_ToStrODBC(this.w_SACAOVAL)+;
                     ",SADESRIG="+cp_ToStrODBC(this.w_SADESRIG)+;
                     ",SACODAGE="+cp_ToStrODBC(this.w_SACODAGE)+;
                     ",SASERRIF="+cp_ToStrODBC(this.w_SASERRIF)+;
                     ",SAORDRIF="+cp_ToStrODBC(this.w_SAORDRIF)+;
                     ",SATIPCON="+cp_ToStrODBC(this.w_SATIPCON)+;
                     ",SANUMRIF="+cp_ToStrODBC(this.w_SANUMRIF)+;
                     ",SAFLVABD="+cp_ToStrODBC(this.w_SAFLVABD)+;
                     ",SABANAPP="+cp_ToStrODBC(this.w_SABANAPP)+;
                     ",SABANNOS="+cp_ToStrODBC(this.w_SABANNOS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'SAL_DACO')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      SADATSCA=this.w_SADATSCA;
                     ,SANUMPAR=this.w_SANUMPAR;
                     ,SACODCON=this.w_SACODCON;
                     ,SAMODPAG=this.w_SAMODPAG;
                     ,SA_SEGNO=this.w_SA_SEGNO;
                     ,SATOTIMP=this.w_SATOTIMP;
                     ,SACODVAL=this.w_SACODVAL;
                     ,SAIMPSAL=this.w_SAIMPSAL;
                     ,SAFLSALD=this.w_SAFLSALD;
                     ,SAIMPABB=this.w_SAIMPABB;
                     ,SACAOAPE=this.w_SACAOAPE;
                     ,SANUMDOC=this.w_SANUMDOC;
                     ,SAALFDOC=this.w_SAALFDOC;
                     ,SADATDOC=this.w_SADATDOC;
                     ,SADATAPE=this.w_SADATAPE;
                     ,SACAOVAL=this.w_SACAOVAL;
                     ,SADESRIG=this.w_SADESRIG;
                     ,SACODAGE=this.w_SACODAGE;
                     ,SASERRIF=this.w_SASERRIF;
                     ,SAORDRIF=this.w_SAORDRIF;
                     ,SATIPCON=this.w_SATIPCON;
                     ,SANUMRIF=this.w_SANUMRIF;
                     ,SAFLVABD=this.w_SAFLVABD;
                     ,SABANAPP=this.w_SABANAPP;
                     ,SABANNOS=this.w_SABANNOS;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SAL_DACO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SAL_DACO_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_SACAOAPE<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete SAL_DACO
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_SACAOAPE<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SAL_DACO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_DACO_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .w_CLASS = .w_PUNTAT.OPARENTOBJECT.CLASS
        .DoRTCalc(4,11,.t.)
          .w_CODCON = IIF( .pTIPGES<>'B',.w_PUNTAT.oParentObject .w_PNCODCON,Space(15))
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .DoRTCalc(13,42,.t.)
          .link_2_3('Full')
        .DoRTCalc(44,50,.t.)
        if .o_SAIMPSAL<>.w_SAIMPSAL.or. .o_SAIMPABB<>.w_SAIMPABB
          .w_DARSAL = (.w_SAIMPSAL+.w_SAIMPABB)*IIF(.w_SEGNO=.w_SA_SEGNO, 1, -1)
        endif
        .DoRTCalc(52,53,.t.)
          .w_FLSALD = .w_SAFLSALD
        .DoRTCalc(55,60,.t.)
        if .o_SACAOAPE<>.w_SACAOAPE.or. .o_SAIMPSAL<>.w_SAIMPSAL.or. .o_SAFLSALD<>.w_SAFLSALD
          .w_DARSAL1 = IIF(.w_SACODVAL=.w_CODNAZ, .w_DARSAL, cp_ROUND(VAL2MON(.w_DARSAL, .w_SACAOVAL, .w_CAONAZ, .w_DATRIF, .w_CODNAZ),.w_DECTOP))
        endif
        .DoRTCalc(62,64,.t.)
          .w_TOTRES = IIF(.w_TOTDAV=0, 0, IIF(.w_FLAGAC='S',Abs(.w_TOTDAV)-Abs(.w_TOTSAL),.w_TOTDAV-.w_TOTSAL))
        .oPgFrm.Page1.oPag.oObj_2_24.Calculate(AH_Msgformat(IIF(.w_EURVAL=0,'Differenza:','')))
        .oPgFrm.Page1.oPag.ABB.Calculate(AH_Msgformat( iif(.pTIPGES='B' ,'',IIF(.w_FLACC='S','Acconto:',IIF(((.w_SAIMPABB>0 AND .w_SA_SEGNO='D') OR (.w_SAIMPABB<0 AND .w_SA_SEGNO='A')) AND NOT EMPTY(.w_SAIMPABB),'Abb. passivo:',IIF(NOT EMPTY(.w_SAIMPABB),'Abb. attivo:','Abbuono:')))) ))
        .DoRTCalc(66,66,.t.)
          .w_ABB = ABS(.w_SAIMPABB)
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .DoRTCalc(68,71,.t.)
          .w_VALRES = IIF(.w_TOTDAO=0, 0, IIF(.w_FLAGAC='S',Abs(.w_TOTDAO)-Abs(.w_VALSAL),.w_TOTDAO-.w_VALSAL))
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(AH_Msgformat(IIF(.pTIPGES='B', 'Data valuta da:','Scadenze da:')))
        .DoRTCalc(73,87,.t.)
          .w_TIPSBF = iif(.pTIPGES='B' ,'S','N')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        if .o_SELALL<>.w_SELALL
          .Calculate_RZWUOJRIKO()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(89,95,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_SAIMPABB with this.w_SAIMPABB
      replace t_DARSAL with this.w_DARSAL
      replace t_FLSALD with this.w_FLSALD
      replace t_SANUMDOC with this.w_SANUMDOC
      replace t_SAALFDOC with this.w_SAALFDOC
      replace t_SADATDOC with this.w_SADATDOC
      replace t_SADATAPE with this.w_SADATAPE
      replace t_IMPDOC with this.w_IMPDOC
      replace t_DARSAL1 with this.w_DARSAL1
      replace t_EURVAL with this.w_EURVAL
      replace t_DECRIG with this.w_DECRIG
      replace t_SACODAGE with this.w_SACODAGE
      replace t_SASERRIF with this.w_SASERRIF
      replace t_SAORDRIF with this.w_SAORDRIF
      replace t_SATIPCON with this.w_SATIPCON
      replace t_SANUMRIF with this.w_SANUMRIF
      replace t_SAFLVABD with this.w_SAFLVABD
      replace t_FLRAGG with this.w_FLRAGG
      replace t_SABANAPP with this.w_SABANAPP
      replace t_SABANNOS with this.w_SABANNOS
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_24.Calculate(AH_Msgformat(IIF(.w_EURVAL=0,'Differenza:','')))
        .oPgFrm.Page1.oPag.ABB.Calculate(AH_Msgformat( iif(.pTIPGES='B' ,'',IIF(.w_FLACC='S','Acconto:',IIF(((.w_SAIMPABB>0 AND .w_SA_SEGNO='D') OR (.w_SAIMPABB<0 AND .w_SA_SEGNO='A')) AND NOT EMPTY(.w_SAIMPABB),'Abb. passivo:',IIF(NOT EMPTY(.w_SAIMPABB),'Abb. attivo:','Abbuono:')))) ))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(AH_Msgformat(IIF(.pTIPGES='B', 'Data valuta da:','Scadenze da:')))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_24.Calculate(AH_Msgformat(IIF(.w_EURVAL=0,'Differenza:','')))
        .oPgFrm.Page1.oPag.ABB.Calculate(AH_Msgformat( iif(.pTIPGES='B' ,'',IIF(.w_FLACC='S','Acconto:',IIF(((.w_SAIMPABB>0 AND .w_SA_SEGNO='D') OR (.w_SAIMPABB<0 AND .w_SA_SEGNO='A')) AND NOT EMPTY(.w_SAIMPABB),'Abb. passivo:',IIF(NOT EMPTY(.w_SAIMPABB),'Abb. attivo:','Abbuono:')))) ))
    endwith
  return
  proc Calculate_RZWUOJRIKO()
    with this
          * --- Seleziona/Deseleziona
          GSCG_BS2(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLSACC_1_26.enabled = this.oPgFrm.Page1.oPag.oFLSACC_1_26.mCond()
    this.oPgFrm.Page1.oPag.oTOTDAO_1_60.enabled = this.oPgFrm.Page1.oPag.oTOTDAO_1_60.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSAIMPSAL_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oSAIMPSAL_2_8.mCond()
    this.oPgFrm.Page1.oPag.oSACAOAPE_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oSACAOAPE_2_13.mCond()
    this.oPgFrm.Page1.oPag.oSACAOVAL_2_20.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oSACAOVAL_2_20.mCond()
    this.oPgFrm.Page1.oPag.oSADESRIG_2_28.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oSADESRIG_2_28.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDINUMERO_1_19.visible=!this.oPgFrm.Page1.oPag.oDINUMERO_1_19.mHide()
    this.oPgFrm.Page1.oPag.oFLSACC_1_26.visible=!this.oPgFrm.Page1.oPag.oFLSACC_1_26.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAL_1_27.visible=!this.oPgFrm.Page1.oPag.oSIMVAL_1_27.mHide()
    this.oPgFrm.Page1.oPag.oTOTDAV_1_28.visible=!this.oPgFrm.Page1.oPag.oTOTDAV_1_28.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAL_1_53.visible=!this.oPgFrm.Page1.oPag.oSIMVAL_1_53.mHide()
    this.oPgFrm.Page1.oPag.oTOTRES_3_2.visible=!this.oPgFrm.Page1.oPag.oTOTRES_3_2.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAO_1_59.visible=!this.oPgFrm.Page1.oPag.oSIMVAO_1_59.mHide()
    this.oPgFrm.Page1.oPag.oTOTDAO_1_60.visible=!this.oPgFrm.Page1.oPag.oTOTDAO_1_60.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oVALSAL_3_3.visible=!this.oPgFrm.Page1.oPag.oVALSAL_3_3.mHide()
    this.oPgFrm.Page1.oPag.oVALRES_3_4.visible=!this.oPgFrm.Page1.oPag.oVALRES_3_4.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAO_3_5.visible=!this.oPgFrm.Page1.oPag.oSIMVAO_3_5.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAO_3_6.visible=!this.oPgFrm.Page1.oPag.oSIMVAO_3_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_7.visible=!this.oPgFrm.Page1.oPag.oStr_3_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_8.visible=!this.oPgFrm.Page1.oPag.oStr_3_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_9.visible=!this.oPgFrm.Page1.oPag.oStr_3_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_3_10.visible=!this.oPgFrm.Page1.oPag.oStr_3_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_74.visible=!this.oPgFrm.Page1.oPag.oStr_1_74.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oDIFCAM_2_12.visible=!this.oPgFrm.Page1.oPag.oDIFCAM_2_12.mHide()
    this.oPgFrm.Page1.oPag.oFLACC_2_25.visible=!this.oPgFrm.Page1.oPag.oFLACC_2_25.mHide()
    this.oPgFrm.Page1.oPag.oABB_2_27.visible=!this.oPgFrm.Page1.oPag.oABB_2_27.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_24.Event(cEvent)
      .oPgFrm.Page1.oPag.ABB.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_72.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODVAL
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL))
          select VACODVAL,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL_1_18'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMBVA = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_SIMBVA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SACODCON
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SACODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SACODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SACODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SATIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SATIPCON;
                       ,'ANCODICE',this.w_SACODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SACODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_SACODCON = space(15)
      endif
      this.w_DESCON1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SACODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ANCODICE as ANCODICE203"+ ",link_2_3.ANDESCRI as ANDESCRI203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on SAL_DACO.SACODCON=link_2_3.ANCODICE"+" and SAL_DACO.SATIPCON=link_2_3.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and SAL_DACO.SACODCON=link_2_3.ANCODICE(+)"'+'+" and SAL_DACO.SATIPCON=link_2_3.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSCAINI_1_16.value==this.w_SCAINI)
      this.oPgFrm.Page1.oPag.oSCAINI_1_16.value=this.w_SCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAFIN_1_17.value==this.w_SCAFIN)
      this.oPgFrm.Page1.oPag.oSCAFIN_1_17.value=this.w_SCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_18.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_18.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDINUMERO_1_19.value==this.w_DINUMERO)
      this.oPgFrm.Page1.oPag.oDINUMERO_1_19.value=this.w_DINUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRD_1_20.RadioValue()==this.w_PAGRD)
      this.oPgFrm.Page1.oPag.oPAGRD_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRI_1_21.RadioValue()==this.w_PAGRI)
      this.oPgFrm.Page1.oPag.oPAGRI_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGBO_1_22.RadioValue()==this.w_PAGBO)
      this.oPgFrm.Page1.oPag.oPAGBO_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRB_1_23.RadioValue()==this.w_PAGRB)
      this.oPgFrm.Page1.oPag.oPAGRB_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGMA_1_24.RadioValue()==this.w_PAGMA)
      this.oPgFrm.Page1.oPag.oPAGMA_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGCA_1_25.RadioValue()==this.w_PAGCA)
      this.oPgFrm.Page1.oPag.oPAGCA_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSACC_1_26.RadioValue()==this.w_FLSACC)
      this.oPgFrm.Page1.oPag.oFLSACC_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_27.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_27.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDAV_1_28.value==this.w_TOTDAV)
      this.oPgFrm.Page1.oPag.oTOTDAV_1_28.value=this.w_TOTDAV
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMBVA_1_37.value==this.w_SIMBVA)
      this.oPgFrm.Page1.oPag.oSIMBVA_1_37.value=this.w_SIMBVA
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_52.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_52.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_53.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_53.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDIFCAM_2_12.value==this.w_DIFCAM)
      this.oPgFrm.Page1.oPag.oDIFCAM_2_12.value=this.w_DIFCAM
      replace t_DIFCAM with this.oPgFrm.Page1.oPag.oDIFCAM_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSACAOAPE_2_13.value==this.w_SACAOAPE)
      this.oPgFrm.Page1.oPag.oSACAOAPE_2_13.value=this.w_SACAOAPE
      replace t_SACAOAPE with this.oPgFrm.Page1.oPag.oSACAOAPE_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSACAOVAL_2_20.value==this.w_SACAOVAL)
      this.oPgFrm.Page1.oPag.oSACAOVAL_2_20.value=this.w_SACAOVAL
      replace t_SACAOVAL with this.oPgFrm.Page1.oPag.oSACAOVAL_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTSAL_3_1.value==this.w_TOTSAL)
      this.oPgFrm.Page1.oPag.oTOTSAL_3_1.value=this.w_TOTSAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTRES_3_2.value==this.w_TOTRES)
      this.oPgFrm.Page1.oPag.oTOTRES_3_2.value=this.w_TOTRES
    endif
    if not(this.oPgFrm.Page1.oPag.oFLACC_2_25.RadioValue()==this.w_FLACC)
      this.oPgFrm.Page1.oPag.oFLACC_2_25.SetRadio()
      replace t_FLACC with this.oPgFrm.Page1.oPag.oFLACC_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oABB_2_27.value==this.w_ABB)
      this.oPgFrm.Page1.oPag.oABB_2_27.value=this.w_ABB
      replace t_ABB with this.oPgFrm.Page1.oPag.oABB_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSADESRIG_2_28.value==this.w_SADESRIG)
      this.oPgFrm.Page1.oPag.oSADESRIG_2_28.value=this.w_SADESRIG
      replace t_SADESRIG with this.oPgFrm.Page1.oPag.oSADESRIG_2_28.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAO_1_59.value==this.w_SIMVAO)
      this.oPgFrm.Page1.oPag.oSIMVAO_1_59.value=this.w_SIMVAO
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDAO_1_60.value==this.w_TOTDAO)
      this.oPgFrm.Page1.oPag.oTOTDAO_1_60.value=this.w_TOTDAO
    endif
    if not(this.oPgFrm.Page1.oPag.oVALSAL_3_3.value==this.w_VALSAL)
      this.oPgFrm.Page1.oPag.oVALSAL_3_3.value=this.w_VALSAL
    endif
    if not(this.oPgFrm.Page1.oPag.oVALRES_3_4.value==this.w_VALRES)
      this.oPgFrm.Page1.oPag.oVALRES_3_4.value=this.w_VALRES
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAO_3_5.value==this.w_SIMVAO)
      this.oPgFrm.Page1.oPag.oSIMVAO_3_5.value=this.w_SIMVAO
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAO_3_6.value==this.w_SIMVAO)
      this.oPgFrm.Page1.oPag.oSIMVAO_3_6.value=this.w_SIMVAO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON1_2_35.value==this.w_DESCON1)
      this.oPgFrm.Page1.oPag.oDESCON1_2_35.value=this.w_DESCON1
      replace t_DESCON1 with this.oPgFrm.Page1.oPag.oDESCON1_2_35.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSELALL_1_82.RadioValue()==this.w_SELALL)
      this.oPgFrm.Page1.oPag.oSELALL_1_82.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSADATSCA_2_1.value==this.w_SADATSCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSADATSCA_2_1.value=this.w_SADATSCA
      replace t_SADATSCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSADATSCA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSANUMPAR_2_2.value==this.w_SANUMPAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSANUMPAR_2_2.value=this.w_SANUMPAR
      replace t_SANUMPAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSANUMPAR_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSACODCON_2_3.value==this.w_SACODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSACODCON_2_3.value=this.w_SACODCON
      replace t_SACODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSACODCON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSAMODPAG_2_4.value==this.w_SAMODPAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSAMODPAG_2_4.value=this.w_SAMODPAG
      replace t_SAMODPAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSAMODPAG_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSA_SEGNO_2_5.value==this.w_SA_SEGNO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSA_SEGNO_2_5.value=this.w_SA_SEGNO
      replace t_SA_SEGNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSA_SEGNO_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSATOTIMP_2_6.value==this.w_SATOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSATOTIMP_2_6.value=this.w_SATOTIMP
      replace t_SATOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSATOTIMP_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSACODVAL_2_7.value==this.w_SACODVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSACODVAL_2_7.value=this.w_SACODVAL
      replace t_SACODVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSACODVAL_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSAIMPSAL_2_8.value==this.w_SAIMPSAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSAIMPSAL_2_8.value=this.w_SAIMPSAL
      replace t_SAIMPSAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSAIMPSAL_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSAFLSALD_2_9.RadioValue()==this.w_SAFLSALD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSAFLSALD_2_9.SetRadio()
      replace t_SAFLSALD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSAFLSALD_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'SAL_DACO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (t_SACAOAPE<>0);
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.pTIPGES<>'B' OR (.pTIPGES='B' AND .w_SAIMPSAL<=.w_SATOTIMP)) and (.pTIPGES<>'B' OR (.pTIPGES='B' AND .w_SAFLSALD<>'S' )) and (.w_SACAOAPE<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSAIMPSAL_2_8
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Attenzione, impossibile saldare per un importo superiore")
        case   empty(.w_SACAOAPE) and ((.w_EURVAL=0 OR .w_SACAOAPE=0 )  AND .pTIPGES<>'B') and (.w_SACAOAPE<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oSACAOAPE_2_13
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   empty(.w_SACAOVAL) and ((.w_EURVAL=0 OR .w_SACAOVAL=0) AND .pTIPGES<>'B') and (.w_SACAOAPE<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oSACAOVAL_2_20
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if .w_SACAOAPE<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SAIMPSAL = this.w_SAIMPSAL
    this.o_SAFLSALD = this.w_SAFLSALD
    this.o_SAIMPABB = this.w_SAIMPABB
    this.o_SACAOAPE = this.w_SACAOAPE
    this.o_SELALL = this.w_SELALL
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_SACAOAPE<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_SADATSCA=ctod("  /  /  ")
      .w_SANUMPAR=space(31)
      .w_SACODCON=space(15)
      .w_SAMODPAG=space(10)
      .w_SA_SEGNO=space(1)
      .w_SATOTIMP=0
      .w_SACODVAL=space(3)
      .w_SAIMPSAL=0
      .w_SAFLSALD=space(1)
      .w_SAIMPABB=0
      .w_DARSAL=0
      .w_DIFCAM=0
      .w_SACAOAPE=0
      .w_FLSALD=space(1)
      .w_SANUMDOC=0
      .w_SAALFDOC=space(10)
      .w_SADATDOC=ctod("  /  /  ")
      .w_SADATAPE=ctod("  /  /  ")
      .w_IMPDOC=0
      .w_SACAOVAL=0
      .w_DARSAL1=0
      .w_EURVAL=0
      .w_DECRIG=0
      .w_FLACC=space(1)
      .w_ABB=0
      .w_SADESRIG=space(50)
      .w_SACODAGE=space(5)
      .w_SASERRIF=space(10)
      .w_SAORDRIF=0
      .w_SATIPCON=space(1)
      .w_SANUMRIF=0
      .w_SAFLVABD=space(1)
      .w_DESCON1=space(40)
      .w_FLRAGG=0
      .w_SABANAPP=space(10)
      .w_SABANNOS=space(15)
      .DoRTCalc(1,43,.f.)
      if not(empty(.w_SACODCON))
        .link_2_3('Full')
      endif
      .DoRTCalc(44,50,.f.)
        .w_DARSAL = (.w_SAIMPSAL+.w_SAIMPABB)*IIF(.w_SEGNO=.w_SA_SEGNO, 1, -1)
      .DoRTCalc(52,53,.f.)
        .w_FLSALD = .w_SAFLSALD
      .DoRTCalc(55,60,.f.)
        .w_DARSAL1 = IIF(.w_SACODVAL=.w_CODNAZ, .w_DARSAL, cp_ROUND(VAL2MON(.w_DARSAL, .w_SACAOVAL, .w_CAONAZ, .w_DATRIF, .w_CODNAZ),.w_DECTOP))
        .oPgFrm.Page1.oPag.oObj_2_24.Calculate(AH_Msgformat(IIF(.w_EURVAL=0,'Differenza:','')))
      .DoRTCalc(62,65,.f.)
        .w_FLACC = ' '
        .oPgFrm.Page1.oPag.ABB.Calculate(AH_Msgformat( iif(.pTIPGES='B' ,'',IIF(.w_FLACC='S','Acconto:',IIF(((.w_SAIMPABB>0 AND .w_SA_SEGNO='D') OR (.w_SAIMPABB<0 AND .w_SA_SEGNO='A')) AND NOT EMPTY(.w_SAIMPABB),'Abb. passivo:',IIF(NOT EMPTY(.w_SAIMPABB),'Abb. attivo:','Abbuono:')))) ))
        .w_ABB = ABS(.w_SAIMPABB)
    endwith
    this.DoRTCalc(68,95,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_SADATSCA = t_SADATSCA
    this.w_SANUMPAR = t_SANUMPAR
    this.w_SACODCON = t_SACODCON
    this.w_SAMODPAG = t_SAMODPAG
    this.w_SA_SEGNO = t_SA_SEGNO
    this.w_SATOTIMP = t_SATOTIMP
    this.w_SACODVAL = t_SACODVAL
    this.w_SAIMPSAL = t_SAIMPSAL
    this.w_SAFLSALD = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSAFLSALD_2_9.RadioValue(.t.)
    this.w_SAIMPABB = t_SAIMPABB
    this.w_DARSAL = t_DARSAL
    this.w_DIFCAM = t_DIFCAM
    this.w_SACAOAPE = t_SACAOAPE
    this.w_FLSALD = t_FLSALD
    this.w_SANUMDOC = t_SANUMDOC
    this.w_SAALFDOC = t_SAALFDOC
    this.w_SADATDOC = t_SADATDOC
    this.w_SADATAPE = t_SADATAPE
    this.w_IMPDOC = t_IMPDOC
    this.w_SACAOVAL = t_SACAOVAL
    this.w_DARSAL1 = t_DARSAL1
    this.w_EURVAL = t_EURVAL
    this.w_DECRIG = t_DECRIG
    this.w_FLACC = this.oPgFrm.Page1.oPag.oFLACC_2_25.RadioValue(.t.)
    this.w_ABB = t_ABB
    this.w_SADESRIG = t_SADESRIG
    this.w_SACODAGE = t_SACODAGE
    this.w_SASERRIF = t_SASERRIF
    this.w_SAORDRIF = t_SAORDRIF
    this.w_SATIPCON = t_SATIPCON
    this.w_SANUMRIF = t_SANUMRIF
    this.w_SAFLVABD = t_SAFLVABD
    this.w_DESCON1 = t_DESCON1
    this.w_FLRAGG = t_FLRAGG
    this.w_SABANAPP = t_SABANAPP
    this.w_SABANNOS = t_SABANNOS
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_SADATSCA with this.w_SADATSCA
    replace t_SANUMPAR with this.w_SANUMPAR
    replace t_SACODCON with this.w_SACODCON
    replace t_SAMODPAG with this.w_SAMODPAG
    replace t_SA_SEGNO with this.w_SA_SEGNO
    replace t_SATOTIMP with this.w_SATOTIMP
    replace t_SACODVAL with this.w_SACODVAL
    replace t_SAIMPSAL with this.w_SAIMPSAL
    replace t_SAFLSALD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSAFLSALD_2_9.ToRadio()
    replace t_SAIMPABB with this.w_SAIMPABB
    replace t_DARSAL with this.w_DARSAL
    replace t_DIFCAM with this.w_DIFCAM
    replace t_SACAOAPE with this.w_SACAOAPE
    replace t_FLSALD with this.w_FLSALD
    replace t_SANUMDOC with this.w_SANUMDOC
    replace t_SAALFDOC with this.w_SAALFDOC
    replace t_SADATDOC with this.w_SADATDOC
    replace t_SADATAPE with this.w_SADATAPE
    replace t_IMPDOC with this.w_IMPDOC
    replace t_SACAOVAL with this.w_SACAOVAL
    replace t_DARSAL1 with this.w_DARSAL1
    replace t_EURVAL with this.w_EURVAL
    replace t_DECRIG with this.w_DECRIG
    replace t_FLACC with this.oPgFrm.Page1.oPag.oFLACC_2_25.ToRadio()
    replace t_ABB with this.w_ABB
    replace t_SADESRIG with this.w_SADESRIG
    replace t_SACODAGE with this.w_SACODAGE
    replace t_SASERRIF with this.w_SASERRIF
    replace t_SAORDRIF with this.w_SAORDRIF
    replace t_SATIPCON with this.w_SATIPCON
    replace t_SANUMRIF with this.w_SANUMRIF
    replace t_SAFLVABD with this.w_SAFLVABD
    replace t_DESCON1 with this.w_DESCON1
    replace t_FLRAGG with this.w_FLRAGG
    replace t_SABANAPP with this.w_SABANAPP
    replace t_SABANNOS with this.w_SABANNOS
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
  func CanAdd()
    local i_res
    i_res=this.w_FLACCESS
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Funzione non utilizzabile"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgscg_mspPag1 as StdContainer
  Width  = 921
  height = 470
  stdWidth  = 921
  stdheight = 470
  resizeXpos=287
  resizeYpos=222
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCAINI_1_16 as StdField with uid="RORIWBCZQB",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SCAINI", cQueryName = "SCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 127716570,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=93, Top=11

  add object oSCAFIN_1_17 as StdField with uid="CNKXGVPCEO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_SCAFIN", cQueryName = "SCAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine selezione scadenze",;
    HelpContextID = 49269978,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=93, Top=34

  add object oCODVAL_1_18 as StdField with uid="CJRVLKZEPY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta di selezione partite (spazio=no selezione)",;
    HelpContextID = 90149338,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=94, Top=59, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAL_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCODVAL_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oCODVAL_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CODVAL
    i_obj.ecpSave()
  endproc

  add object oDINUMERO_1_19 as StdField with uid="XAAECVNHKW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DINUMERO", cQueryName = "DINUMERO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero distinta selezionata",;
    HelpContextID = 195032955,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=687, Top=58

  func oDINUMERO_1_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.pTIPGES<>'B')
    endwith
    endif
  endfunc

  add object oPAGRD_1_20 as StdCheck with uid="TTBNVXXDED",rtseq=20,rtrep=.f.,left=271, top=10, caption="Rim.dir.",;
    ToolTipText = "Se attivo: seleziona le rimesse dirette",;
    HelpContextID = 248287478,;
    cFormVar="w_PAGRD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRD_1_20.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PAGRD,&i_cF..t_PAGRD),this.value)
    return(iif(xVal =1,'RD',;
    '  '))
  endfunc
  func oPAGRD_1_20.GetRadio()
    this.Parent.oContained.w_PAGRD = this.RadioValue()
    return .t.
  endfunc

  func oPAGRD_1_20.ToRadio()
    this.Parent.oContained.w_PAGRD=trim(this.Parent.oContained.w_PAGRD)
    return(;
      iif(this.Parent.oContained.w_PAGRD=='RD',1,;
      0))
  endfunc

  func oPAGRD_1_20.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPAGRI_1_21 as StdCheck with uid="TTJXOXFNJM",rtseq=21,rtrep=.f.,left=271, top=32, caption="R.I.D.",;
    ToolTipText = "Se attivo: seleziona le R.I.D.",;
    HelpContextID = 253530358,;
    cFormVar="w_PAGRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRI_1_21.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PAGRI,&i_cF..t_PAGRI),this.value)
    return(iif(xVal =1,'RI',;
    '  '))
  endfunc
  func oPAGRI_1_21.GetRadio()
    this.Parent.oContained.w_PAGRI = this.RadioValue()
    return .t.
  endfunc

  func oPAGRI_1_21.ToRadio()
    this.Parent.oContained.w_PAGRI=trim(this.Parent.oContained.w_PAGRI)
    return(;
      iif(this.Parent.oContained.w_PAGRI=='RI',1,;
      0))
  endfunc

  func oPAGRI_1_21.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPAGBO_1_22 as StdCheck with uid="HIBUHXMRBT",rtseq=22,rtrep=.f.,left=271, top=54, caption="Bonifico",;
    ToolTipText = "Se attivo: seleziona i bonifici",;
    HelpContextID = 258773238,;
    cFormVar="w_PAGBO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGBO_1_22.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PAGBO,&i_cF..t_PAGBO),this.value)
    return(iif(xVal =1,'BO',;
    '  '))
  endfunc
  func oPAGBO_1_22.GetRadio()
    this.Parent.oContained.w_PAGBO = this.RadioValue()
    return .t.
  endfunc

  func oPAGBO_1_22.ToRadio()
    this.Parent.oContained.w_PAGBO=trim(this.Parent.oContained.w_PAGBO)
    return(;
      iif(this.Parent.oContained.w_PAGBO=='BO',1,;
      0))
  endfunc

  func oPAGBO_1_22.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPAGRB_1_23 as StdCheck with uid="EDTBAGZHEG",rtseq=23,rtrep=.f.,left=379, top=10, caption="Ric.banc.",;
    ToolTipText = "Se attivo: seleziona le ricevute bancarie",;
    HelpContextID = 246190326,;
    cFormVar="w_PAGRB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRB_1_23.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PAGRB,&i_cF..t_PAGRB),this.value)
    return(iif(xVal =1,'RB',;
    '  '))
  endfunc
  func oPAGRB_1_23.GetRadio()
    this.Parent.oContained.w_PAGRB = this.RadioValue()
    return .t.
  endfunc

  func oPAGRB_1_23.ToRadio()
    this.Parent.oContained.w_PAGRB=trim(this.Parent.oContained.w_PAGRB)
    return(;
      iif(this.Parent.oContained.w_PAGRB=='RB',1,;
      0))
  endfunc

  func oPAGRB_1_23.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPAGMA_1_24 as StdCheck with uid="TLGHWKYRYJ",rtseq=24,rtrep=.f.,left=379, top=32, caption="M.AV.",;
    ToolTipText = "Se attivo: seleziona le M.AV.",;
    HelpContextID = 244814070,;
    cFormVar="w_PAGMA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGMA_1_24.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PAGMA,&i_cF..t_PAGMA),this.value)
    return(iif(xVal =1,'MA',;
    '  '))
  endfunc
  func oPAGMA_1_24.GetRadio()
    this.Parent.oContained.w_PAGMA = this.RadioValue()
    return .t.
  endfunc

  func oPAGMA_1_24.ToRadio()
    this.Parent.oContained.w_PAGMA=trim(this.Parent.oContained.w_PAGMA)
    return(;
      iif(this.Parent.oContained.w_PAGMA=='MA',1,;
      0))
  endfunc

  func oPAGMA_1_24.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPAGCA_1_25 as StdCheck with uid="POQKGOOSID",rtseq=25,rtrep=.f.,left=379, top=54, caption="Cambiali",;
    ToolTipText = "Se attivo: seleziona le cambiali/tratte",;
    HelpContextID = 244158710,;
    cFormVar="w_PAGCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGCA_1_25.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PAGCA,&i_cF..t_PAGCA),this.value)
    return(iif(xVal =1,'CA',;
    '  '))
  endfunc
  func oPAGCA_1_25.GetRadio()
    this.Parent.oContained.w_PAGCA = this.RadioValue()
    return .t.
  endfunc

  func oPAGCA_1_25.ToRadio()
    this.Parent.oContained.w_PAGCA=trim(this.Parent.oContained.w_PAGCA)
    return(;
      iif(this.Parent.oContained.w_PAGCA=='CA',1,;
      0))
  endfunc

  func oPAGCA_1_25.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oFLSACC_1_26 as StdCheck with uid="IYYIKQFRWQ",rtseq=26,rtrep=.f.,left=662, top=62, caption="Solo acconti",;
    ToolTipText = "Solo acconti",;
    HelpContextID = 240362666,;
    cFormVar="w_FLSACC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSACC_1_26.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLSACC,&i_cF..t_FLSACC),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oFLSACC_1_26.GetRadio()
    this.Parent.oContained.w_FLSACC = this.RadioValue()
    return .t.
  endfunc

  func oFLSACC_1_26.ToRadio()
    this.Parent.oContained.w_FLSACC=trim(this.Parent.oContained.w_FLSACC)
    return(;
      iif(this.Parent.oContained.w_FLSACC=='S',1,;
      0))
  endfunc

  func oFLSACC_1_26.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oFLSACC_1_26.mCond()
    with this.Parent.oContained
      return (.w_PUNTAT.oParentObject .w_FLAGAC<>'S')
    endwith
  endfunc

  func oFLSACC_1_26.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(.w_CLASS)="TGSCG_MSC")
    endwith
    endif
  endfunc

  add object oSIMVAL_1_27 as StdField with uid="RPHULRAKPF",rtseq=27,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90113754,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=662, Top=11, InputMask=replicate('X',5)

  func oSIMVAL_1_27.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(.w_CLASS) ="TGSCG_MSC")
    endwith
    endif
  endfunc

  add object oTOTDAV_1_28 as StdField with uid="BIRTDJORBY",rtseq=28,rtrep=.f.,;
    cFormVar = "w_TOTDAV", cQueryName = "TOTDAV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo da saldare",;
    HelpContextID = 191926474,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=716, Top=11, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oTOTDAV_1_28.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(.w_CLASS) ="TGSCG_MSC")
    endwith
    endif
  endfunc


  add object oBtn_1_29 as StdButton with uid="CQEHICTOFE",left=662, top=59, width=19,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 171502550;
  , bGlobalFont=.t.

    proc oBtn_1_29.Click()
      do gste_kds with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.pTIPGES<>'B')
    endwith
   endif
  endfunc


  add object oObj_1_30 as cp_runprogram with uid="DIKPEOHCVY",left=524, top=487, width=286,height=21,;
    caption='GSCG_BS1(IMPO)',;
   bGlobalFont=.t.,;
    prg="GSCG_BS1('IMPO')",;
    cEvent = "w_SAIMPSAL Changed",;
    nPag=1;
    , HelpContextID = 11959831


  add object oObj_1_31 as cp_runprogram with uid="TSMLLWGOJX",left=2, top=536, width=265,height=21,;
    caption='GSCG_BS1(FLAG)',;
   bGlobalFont=.t.,;
    prg="GSCG_BS1('FLAG')",;
    cEvent = "w_SAFLSALD Changed",;
    nPag=1;
    , HelpContextID = 2583319


  add object oBtn_1_36 as StdButton with uid="RYUGJGAWVH",left=853, top=11, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rieseguire la lettura delle partite da saldare in base alle selezioni";
    , HelpContextID = 188647146;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSCG_BSA(this.Parent.oContained,"B")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSIMBVA_1_37 as StdField with uid="DELZFCOKWD",rtseq=30,rtrep=.f.,;
    cFormVar = "w_SIMBVA", cQueryName = "SIMBVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 253953754,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=145, Top=59, InputMask=replicate('X',5)


  add object oObj_1_38 as cp_runprogram with uid="EGTHVLBQJQ",left=524, top=509, width=286,height=21,;
    caption='GSCG_BSA(A)',;
   bGlobalFont=.t.,;
    prg="GSCG_BSA('A')",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 227264985


  add object oBtn_1_39 as StdButton with uid="NBIJRHFXID",left=818, top=422, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 171330278;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_40 as StdButton with uid="TLLHYQSYGG",left=869, top=422, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 178618950;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSIMVAL_1_52 as StdField with uid="UTTGXDXPIR",rtseq=39,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90113754,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=91, Top=415, InputMask=replicate('X',5)

  add object oSIMVAL_1_53 as StdField with uid="KWRUXDAFGN",rtseq=40,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90113754,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=91, Top=441, InputMask=replicate('X',5)

  func oSIMVAL_1_53.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.pTipGes = 'B')
    endwith
    endif
  endfunc


  add object oObj_1_57 as cp_runprogram with uid="TPBSZEQMEN",left=3, top=511, width=394,height=20,;
    caption='GSCG_BS1(DIFC)',;
   bGlobalFont=.t.,;
    prg="GSCG_BS1('DIFC')",;
    cEvent = "w_SACAOAPE Changed,w_SACAOVAL Changed",;
    nPag=1;
    , HelpContextID = 267139351

  add object oSIMVAO_1_59 as StdField with uid="LKTFZHGHLC",rtseq=69,rtrep=.f.,;
    cFormVar = "w_SIMVAO", cQueryName = "SIMVAO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 39782106,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=662, Top=34, InputMask=replicate('X',5)

  func oSIMVAO_1_59.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODNAZ=.w_CODVAO OR .w_NUMVAL<>1)
    endwith
    endif
  endfunc

  add object oTOTDAO_1_60 as StdField with uid="VVWJNTZYSQ",rtseq=70,rtrep=.f.,;
    cFormVar = "w_TOTDAO", cQueryName = "TOTDAO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo da saldare",;
    HelpContextID = 40931530,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=716, Top=34, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oTOTDAO_1_60.mCond()
    with this.Parent.oContained
      return (.w_CODNAZ<>.w_CODVAO AND .w_NUMVAL=1)
    endwith
  endfunc

  func oTOTDAO_1_60.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODNAZ=.w_CODVAO OR .w_NUMVAL<>1)
    endwith
    endif
  endfunc


  add object oObj_1_62 as cp_runprogram with uid="YRBRNLASTT",left=524, top=532, width=286,height=20,;
    caption='GSCG_BS1(DAO)',;
   bGlobalFont=.t.,;
    prg="GSCG_BS1('DAO')",;
    cEvent = "w_TOTDAO Changed",;
    nPag=1;
    , HelpContextID = 178996969


  add object oObj_1_72 as cp_calclbl with uid="FIFWCEXESP",left=8, top=12, width=85,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 187571738


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=87, width=912,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="SADATSCA",Label1="AH_Msgformat(IIF(.pTIPGES='B', 'Data valuta','Scadenza'))",Field2="SANUMPAR",Label2="Partita",Field3="SACODCON",Label3="Conto",Field4="SAMODPAG",Label4="Pagamento",Field5="SA_SEGNO",Label5="D/A",Field6="SATOTIMP",Label6="Da saldare",Field7="SACODVAL",Label7="Valuta",Field8="SAIMPSAL",Label8="Saldato",Field9="SAFLSALD",Label9="Fl.salda",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218956922

  add object oSELALL_1_82 as StdRadio with uid="ASHEAQLXZM",rtseq=92,rtrep=.f.,left=807, top=323, width=115,height=40;
    , cFormVar="w_SELALL", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELALL_1_82.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 79960794
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 79960794
      this.Buttons(2).Top=19
      this.SetAll("Width",113)
      this.SetAll("Height",21)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELALL_1_82.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SELALL,&i_cF..t_SELALL),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oSELALL_1_82.GetRadio()
    this.Parent.oContained.w_SELALL = this.RadioValue()
    return .t.
  endfunc

  func oSELALL_1_82.ToRadio()
    this.Parent.oContained.w_SELALL=trim(this.Parent.oContained.w_SELALL)
    return(;
      iif(this.Parent.oContained.w_SELALL=='S',1,;
      iif(this.Parent.oContained.w_SELALL=='N',2,;
      0)))
  endfunc

  func oSELALL_1_82.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oStr_1_33 as StdString with uid="RWOQCBCDPL",Visible=.t., Left=5, Top=415,;
    Alignment=1, Width=84, Height=15,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="XHZYXBLTZG",Visible=.t., Left=23, Top=441,;
    Alignment=1, Width=66, Height=14,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (.pTipGes = 'B')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="FMVFQCPDOV",Visible=.t., Left=61, Top=34,;
    Alignment=1, Width=32, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="XERDCCISNT",Visible=.t., Left=196, Top=10,;
    Alignment=1, Width=72, Height=15,;
    Caption="Pagamenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="LRGRZLBBWH",Visible=.t., Left=389, Top=348,;
    Alignment=0, Width=186, Height=18,;
    Caption="Cambi"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="PRMGDYFNQU",Visible=.t., Left=538, Top=10,;
    Alignment=1, Width=122, Height=15,;
    Caption="Importo primanota:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (UPPER(.w_CLASS) ="TGSCG_MSC")
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="PZQXXMLFWI",Visible=.t., Left=368, Top=372,;
    Alignment=1, Width=70, Height=15,;
    Caption="Origine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="AODMCJHLLQ",Visible=.t., Left=542, Top=372,;
    Alignment=1, Width=47, Height=15,;
    Caption="Attuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="GVGIGOEIQF",Visible=.t., Left=9, Top=325,;
    Alignment=1, Width=68, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="GIGNPJEAVK",Visible=.t., Left=491, Top=34,;
    Alignment=1, Width=169, Height=18,;
    Caption="Importo originario:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_CODNAZ=.w_CODVAO OR .w_NUMVAL<>1)
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="DACJBMHVEF",Visible=.t., Left=38, Top=59,;
    Alignment=1, Width=55, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="GKXVMBXCYJ",Visible=.t., Left=1, Top=490,;
    Alignment=0, Width=515, Height=19,;
    Caption="Attenzione! Questa maschera viene lanciata anche nel saldaconto gscg_msc"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="WEEQHIQWKS",Visible=.t., Left=3, Top=349,;
    Alignment=0, Width=103, Height=18,;
    Caption="Acconti\abbuoni"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="HQKVEQWOUR",Visible=.t., Left=452, Top=325,;
    Alignment=1, Width=45, Height=18,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="TAJDSDEIWG",Visible=.t., Left=561, Top=58,;
    Alignment=1, Width=99, Height=18,;
    Caption="Numero distinta:"  ;
  , bGlobalFont=.t.

  func oStr_1_74.mHide()
    with this.Parent.oContained
      return (.pTIPGES<>'B')
    endwith
  endfunc

  add object oBox_1_56 as StdBox with uid="VTKOLPPFBN",left=389, top=365, width=505,height=3

  add object oBox_1_66 as StdBox with uid="WOPURQGMNE",left=5, top=365, width=267,height=3

  add object oBox_1_68 as StdBox with uid="KDGSEBALDW",left=3, top=5, width=481,height=80

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=106,;
    width=908+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=107,width=907+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDIFCAM_2_12.Refresh()
      this.Parent.oSACAOAPE_2_13.Refresh()
      this.Parent.oSACAOVAL_2_20.Refresh()
      this.Parent.oFLACC_2_25.Refresh()
      this.Parent.oABB_2_27.Refresh()
      this.Parent.oSADESRIG_2_28.Refresh()
      this.Parent.oDESCON1_2_35.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDIFCAM_2_12 as StdTrsField with uid="QMAQFAZCDG",rtseq=52,rtrep=.t.,;
    cFormVar="w_DIFCAM",value=0,enabled=.f.,;
    ToolTipText = "Differenza cambi di riga (espresso nella moneta di conto)",;
    HelpContextID = 74610634,;
    cTotal="", bFixedPos=.t., cQueryName = "DIFCAM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=766, Top=372, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  func oDIFCAM_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EURVAL<>0)
    endwith
    endif
  endfunc

  add object oSACAOAPE_2_13 as StdTrsField with uid="SMYDHWZANY",rtseq=53,rtrep=.t.,;
    cFormVar="w_SACAOAPE",value=0,;
    ToolTipText = "Cambio del documento di apertura",;
    HelpContextID = 261402261,;
    cTotal="", bFixedPos=.t., cQueryName = "SACAOAPE",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=440, Top=372, cSayPict=["99999.999999"], cGetPict=["99999.999999"]

  func oSACAOAPE_2_13.mCond()
    with this.Parent.oContained
      return ((.w_EURVAL=0 OR .w_SACAOAPE=0 )  AND .pTIPGES<>'B')
    endwith
  endfunc

  add object oSACAOVAL_2_20 as StdTrsField with uid="DORFNQVLNJ",rtseq=60,rtrep=.t.,;
    cFormVar="w_SACAOVAL",value=0,;
    ToolTipText = "Cambio attuale della scadenza",;
    HelpContextID = 177516174,;
    cTotal="", bFixedPos=.t., cQueryName = "SACAOVAL",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=591, Top=372, cSayPict=["99999.999999"], cGetPict=["99999.999999"]

  func oSACAOVAL_2_20.mCond()
    with this.Parent.oContained
      return ((.w_EURVAL=0 OR .w_SACAOVAL=0) AND .pTIPGES<>'B')
    endwith
  endfunc

  add object oObj_2_24 as cp_calclbl with uid="BMFURQSQFO",width=71,height=15,;
   left=694, top=372,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 187571738

  add object oFLACC_2_25 as StdTrsCheck with uid="PTQJMXITYI",rtrep=.t.,;
    cFormVar="w_FLACC",  caption="Acconto",;
    ToolTipText = "Se attivo: residuo considerato come acconto",;
    HelpContextID = 246233942,;
    Left=211, Top=375,;
    cTotal="", cQueryName = "FLACC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oFLACC_2_25.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLACC,&i_cF..t_FLACC),this.value)
    return(iif(xVal =1,'S',;
    '  '))
  endfunc
  func oFLACC_2_25.GetRadio()
    this.Parent.oContained.w_FLACC = this.RadioValue()
    return .t.
  endfunc

  func oFLACC_2_25.ToRadio()
    this.Parent.oContained.w_FLACC=trim(this.Parent.oContained.w_FLACC)
    return(;
      iif(this.Parent.oContained.w_FLACC=='S',1,;
      0))
  endfunc

  func oFLACC_2_25.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oFLACC_2_25.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(.w_CLASS) <>"TGSCG_MSC"  OR (.w_SAIMPABB>0 AND NOT EMPTY(.w_SAIMPABB) OR EMPTY(.w_SAIMPABB)) or .pTIPGES='B')
    endwith
    endif
  endfunc

  add object ABB as cp_calclbl with uid="OGXIZIRKKC",width=77,height=20,;
   left=1, top=372,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 187571738

  add object oABB_2_27 as StdTrsField with uid="MFHUZTSVPL",rtseq=67,rtrep=.t.,;
    cFormVar="w_ABB",value=0,enabled=.f.,;
    HelpContextID = 171589894,;
    cTotal="", bFixedPos=.t., cQueryName = "ABB",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=80, Top=372, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oABB_2_27.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.pTIPGES='B')
    endwith
    endif
  endfunc

  add object oSADESRIG_2_28 as StdTrsField with uid="RUUNLCNDBU",rtseq=68,rtrep=.t.,;
    cFormVar="w_SADESRIG",value=space(50),;
    ToolTipText = "Eventuale descrizione aggiuntiva",;
    HelpContextID = 28270957,;
    cTotal="", bFixedPos=.t., cQueryName = "SADESRIG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=80, Top=325, InputMask=replicate('X',50)

  func oSADESRIG_2_28.mCond()
    with this.Parent.oContained
      return (.w_SACAOAPE<>0)
    endwith
  endfunc

  add object oDESCON1_2_35 as StdTrsField with uid="CAQUWICTNM",rtseq=81,rtrep=.t.,;
    cFormVar="w_DESCON1",value=space(40),enabled=.f.,;
    HelpContextID = 43101130,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCON1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=500, Top=325, InputMask=replicate('X',40)

  add object oBox_2_36 as StdBox with uid="ISRNSOUVEY",left=1, top=408, width=497,height=59

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTSAL_3_1 as StdField with uid="HNHTAOTBTB",rtseq=64,rtrep=.f.,;
    cFormVar="w_TOTSAL",value=0,enabled=.f.,;
    HelpContextID = 90280138,;
    cQueryName = "TOTSAL",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=150, Top=415, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oTOTRES_3_2 as StdField with uid="XOFRTMVNBT",rtseq=65,rtrep=.f.,;
    cFormVar="w_TOTRES",value=0,enabled=.f.,;
    HelpContextID = 237146314,;
    cQueryName = "TOTRES",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=150, Top=441, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  func oTOTRES_3_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.pTipGes = 'B')
    endwith
    endif
  endfunc

  add object oVALSAL_3_3 as StdField with uid="NLLSTZKBOL",rtseq=71,rtrep=.f.,;
    cFormVar="w_VALSAL",value=0,enabled=.f.,;
    HelpContextID = 90316458,;
    cQueryName = "VALSAL",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=350, Top=415, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oVALSAL_3_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODNAZ=.w_CODVAO OR .w_NUMVAL<>1)
    endwith
    endif
  endfunc

  add object oVALRES_3_4 as StdField with uid="TQMNYZYYEO",rtseq=72,rtrep=.f.,;
    cFormVar="w_VALRES",value=0,enabled=.f.,;
    HelpContextID = 237182634,;
    cQueryName = "VALRES",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=350, Top=441, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oVALRES_3_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODNAZ=.w_CODVAO OR .w_NUMVAL<>1 OR .pTipGes = 'B')
    endwith
    endif
  endfunc

  add object oSIMVAO_3_5 as StdField with uid="HXMDTETNFE",rtseq=73,rtrep=.f.,;
    cFormVar="w_SIMVAO",value=space(5),enabled=.f.,;
    HelpContextID = 39782106,;
    cQueryName = "SIMVAO",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=295, Top=415, InputMask=replicate('X',5)

  func oSIMVAO_3_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODNAZ=.w_CODVAO OR .w_NUMVAL<>1)
    endwith
    endif
  endfunc

  add object oSIMVAO_3_6 as StdField with uid="AITZPISOMD",rtseq=74,rtrep=.f.,;
    cFormVar="w_SIMVAO",value=space(5),enabled=.f.,;
    HelpContextID = 39782106,;
    cQueryName = "SIMVAO",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=295, Top=441, InputMask=replicate('X',5)

  func oSIMVAO_3_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CODNAZ=.w_CODVAO OR .w_NUMVAL<>1 OR .pTipGes = 'B')
    endwith
    endif
  endfunc

  add object oStr_3_7 as StdString with uid="WQZETUOYSG",Visible=.t., Left=280, Top=415,;
    Alignment=1, Width=12, Height=18,;
    Caption="("  ;
  , bGlobalFont=.t.

  func oStr_3_7.mHide()
    with this.Parent.oContained
      return (.w_CODNAZ=.w_CODVAO OR .w_NUMVAL<>1)
    endwith
  endfunc

  add object oStr_3_8 as StdString with uid="JYDCWBMRJL",Visible=.t., Left=280, Top=441,;
    Alignment=1, Width=12, Height=18,;
    Caption="("  ;
  , bGlobalFont=.t.

  func oStr_3_8.mHide()
    with this.Parent.oContained
      return (.w_CODNAZ=.w_CODVAO OR .w_NUMVAL<>1 OR .pTipGes = 'B')
    endwith
  endfunc

  add object oStr_3_9 as StdString with uid="AMTPAVIKNH",Visible=.t., Left=480, Top=415,;
    Alignment=0, Width=12, Height=18,;
    Caption=")"  ;
  , bGlobalFont=.t.

  func oStr_3_9.mHide()
    with this.Parent.oContained
      return (.w_CODNAZ=.w_CODVAO OR .w_NUMVAL<>1)
    endwith
  endfunc

  add object oStr_3_10 as StdString with uid="DYZQLWCIDI",Visible=.t., Left=480, Top=441,;
    Alignment=0, Width=12, Height=18,;
    Caption=")"  ;
  , bGlobalFont=.t.

  func oStr_3_10.mHide()
    with this.Parent.oContained
      return (.w_CODNAZ=.w_CODVAO OR .w_NUMVAL<>1 OR .pTipGes = 'B')
    endwith
  endfunc
enddefine

* --- Defining Body row
define class tgscg_mspBodyRow as CPBodyRowCnt
  Width=898
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oSADATSCA_2_1 as StdTrsField with uid="MSSGQIDEHU",rtseq=41,rtrep=.t.,;
    cFormVar="w_SADATSCA",value=ctod("  /  /  "),;
    ToolTipText = "Data scadenza",;
    HelpContextID = 45834599,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=-2, Top=0

  add object oSANUMPAR_2_2 as StdTrsField with uid="MFCEBZCIJY",rtseq=42,rtrep=.t.,;
    cFormVar="w_SANUMPAR",value=space(31),enabled=.f.,;
    ToolTipText = "Numero partita",;
    HelpContextID = 10485384,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=230, Left=81, Top=0, InputMask=replicate('X',31)

  add object oSACODCON_2_3 as StdTrsField with uid="KIOAFSQSIX",rtseq=43,rtrep=.t.,;
    cFormVar="w_SACODCON",value=space(15),enabled=.f.,;
    ToolTipText = "Codice cliente/fornitore/conto",;
    HelpContextID = 238464652,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=314, Top=0, InputMask=replicate('X',15), cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SATIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SACODCON"

  func oSACODCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oSAMODPAG_2_4 as StdTrsField with uid="LRCMVFVUKB",rtseq=44,rtrep=.t.,;
    cFormVar="w_SAMODPAG",value=space(10),enabled=.f.,;
    ToolTipText = "Tipo di pagamento",;
    HelpContextID = 20319891,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=74, Left=451, Top=0, InputMask=replicate('X',10)

  add object oSA_SEGNO_2_5 as StdTrsField with uid="UIYWOPPXNB",rtseq=45,rtrep=.t.,;
    cFormVar="w_SA_SEGNO",value=space(1),enabled=.f.,;
    ToolTipText = "Segno importo da saldare (dare o avere)",;
    HelpContextID = 169930379,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=16, Left=527, Top=0, InputMask=replicate('X',1)

  add object oSATOTIMP_2_6 as StdTrsField with uid="TCONXPHNKG",rtseq=46,rtrep=.t.,;
    cFormVar="w_SATOTIMP",value=0,enabled=.f.,;
    ToolTipText = "Totale importo da saldare",;
    HelpContextID = 120954506,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=549, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oSACODVAL_2_7 as StdTrsField with uid="KUUSGYXTMO",rtseq=47,rtrep=.t.,;
    cFormVar="w_SACODVAL",value=space(3),enabled=.f.,;
    ToolTipText = "Codice valuta della scadenza",;
    HelpContextID = 188133006,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=686, Top=0, InputMask=replicate('X',3)

  add object oSAIMPSAL_2_8 as StdTrsField with uid="SCXKYGDVCY",rtseq=48,rtrep=.t.,;
    cFormVar="w_SAIMPSAL",value=0,;
    ToolTipText = "Importo saldato",;
    HelpContextID = 225988238,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, impossibile saldare per un importo superiore",;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=731, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oSAIMPSAL_2_8.mCond()
    with this.Parent.oContained
      return (.pTIPGES<>'B' OR (.pTIPGES='B' AND .w_SAFLSALD<>'S' ))
    endwith
  endfunc

  func oSAIMPSAL_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.pTIPGES<>'B' OR (.pTIPGES='B' AND .w_SAIMPSAL<=.w_SATOTIMP))
    endwith
    return bRes
  endfunc

  add object oSAFLSALD_2_9 as StdTrsCheck with uid="FQCZUSTEEQ",rtrep=.t.,;
    cFormVar="w_SAFLSALD",  caption="",;
    ToolTipText = "Se attivo: partita saldata",;
    HelpContextID = 256474774,;
    Left=871, Top=1, Width=22,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oSAFLSALD_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..SAFLSALD,&i_cF..t_SAFLSALD),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oSAFLSALD_2_9.GetRadio()
    this.Parent.oContained.w_SAFLSALD = this.RadioValue()
    return .t.
  endfunc

  func oSAFLSALD_2_9.ToRadio()
    this.Parent.oContained.w_SAFLSALD=trim(this.Parent.oContained.w_SAFLSALD)
    return(;
      iif(this.Parent.oContained.w_SAFLSALD=='S',1,;
      0))
  endfunc

  func oSAFLSALD_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oSADATSCA_2_1.When()
    return(.t.)
  proc oSADATSCA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oSADATSCA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=10
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_msp','SAL_DACO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SASERIAL=SAL_DACO.SASERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
