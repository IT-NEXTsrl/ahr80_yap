* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kac                                                        *
*              Combinazione categoria codice IVA ricercata                     *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_10]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-26                                                      *
* Last revis.: 2007-06-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kac",oParentObject))

* --- Class definition
define class tgsve_kac as StdForm
  Top    = 23
  Left   = 48

  * --- Standard Properties
  Width  = 521
  Height = 131
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-06-12"
  HelpContextID=132788841
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  TIPCODIV_IDX = 0
  VOCIIVA_IDX = 0
  cPrg = "gsve_kac"
  cComment = "Combinazione categoria codice IVA ricercata"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_MVTIPOPE = space(10)
  w_TIDESCRI = space(30)
  w_ANTIPOPE = space(10)
  w_ANIDESCRI = space(30)
  w_ARTIPOPE = space(10)
  w_ARIDESCRI = space(30)
  w_ARCATOPE = space(1)
  w_ANCATOPE = space(1)
  w_MVCATOPE = space(1)
  w_CODIVA = space(5)
  w_IVDESIVA = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kacPag1","gsve_kac",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIPCODIV'
    this.cWorkTables[2]='VOCIIVA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MVTIPOPE=space(10)
      .w_TIDESCRI=space(30)
      .w_ANTIPOPE=space(10)
      .w_ANIDESCRI=space(30)
      .w_ARTIPOPE=space(10)
      .w_ARIDESCRI=space(30)
      .w_ARCATOPE=space(1)
      .w_ANCATOPE=space(1)
      .w_MVCATOPE=space(1)
      .w_CODIVA=space(5)
      .w_IVDESIVA=space(35)
      .w_MVTIPOPE=oParentObject.w_MVTIPOPE
      .w_ANTIPOPE=oParentObject.w_ANTIPOPE
      .w_ARTIPOPE=oParentObject.w_ARTIPOPE
      .w_ARCATOPE=oParentObject.w_ARCATOPE
      .w_ANCATOPE=oParentObject.w_ANCATOPE
      .w_MVCATOPE=oParentObject.w_MVCATOPE
      .w_CODIVA=oParentObject.w_CODIVA
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_MVTIPOPE))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_ANTIPOPE))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_ARTIPOPE))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,10,.f.)
        if not(empty(.w_CODIVA))
          .link_1_14('Full')
        endif
    endwith
    this.DoRTCalc(11,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MVTIPOPE=.w_MVTIPOPE
      .oParentObject.w_ANTIPOPE=.w_ANTIPOPE
      .oParentObject.w_ARTIPOPE=.w_ARTIPOPE
      .oParentObject.w_ARCATOPE=.w_ARCATOPE
      .oParentObject.w_ANCATOPE=.w_ANCATOPE
      .oParentObject.w_MVCATOPE=.w_MVCATOPE
      .oParentObject.w_CODIVA=.w_CODIVA
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,4,.t.)
          .link_1_5('Full')
        .DoRTCalc(6,9,.t.)
          .link_1_14('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVTIPOPE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_MVTIPOPE);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_MVCATOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_MVCATOPE;
                       ,'TICODICE',this.w_MVTIPOPE)
            select TI__TIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPOPE = NVL(_Link_.TICODICE,space(10))
      this.w_TIDESCRI = NVL(_Link_.TIDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPOPE = space(10)
      endif
      this.w_TIDESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANTIPOPE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANTIPOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANTIPOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_ANTIPOPE);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_ANCATOPE;
                       ,'TICODICE',this.w_ANTIPOPE)
            select TI__TIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANTIPOPE = NVL(_Link_.TICODICE,space(10))
      this.w_ANIDESCRI = NVL(_Link_.TIDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANTIPOPE = space(10)
      endif
      this.w_ANIDESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANTIPOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTIPOPE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTIPOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTIPOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_ARTIPOPE);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ARCATOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_ARCATOPE;
                       ,'TICODICE',this.w_ARTIPOPE)
            select TI__TIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTIPOPE = NVL(_Link_.TICODICE,space(10))
      this.w_ARIDESCRI = NVL(_Link_.TIDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ARTIPOPE = space(10)
      endif
      this.w_ARIDESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTIPOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_IVDESIVA = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_IVDESIVA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMVTIPOPE_1_1.value==this.w_MVTIPOPE)
      this.oPgFrm.Page1.oPag.oMVTIPOPE_1_1.value=this.w_MVTIPOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIDESCRI_1_2.value==this.w_TIDESCRI)
      this.oPgFrm.Page1.oPag.oTIDESCRI_1_2.value=this.w_TIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANTIPOPE_1_3.value==this.w_ANTIPOPE)
      this.oPgFrm.Page1.oPag.oANTIPOPE_1_3.value=this.w_ANTIPOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oANIDESCRI_1_4.value==this.w_ANIDESCRI)
      this.oPgFrm.Page1.oPag.oANIDESCRI_1_4.value=this.w_ANIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPOPE_1_5.value==this.w_ARTIPOPE)
      this.oPgFrm.Page1.oPag.oARTIPOPE_1_5.value=this.w_ARTIPOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oARIDESCRI_1_6.value==this.w_ARIDESCRI)
      this.oPgFrm.Page1.oPag.oARIDESCRI_1_6.value=this.w_ARIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIVA_1_14.value==this.w_CODIVA)
      this.oPgFrm.Page1.oPag.oCODIVA_1_14.value=this.w_CODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oIVDESIVA_1_15.value==this.w_IVDESIVA)
      this.oPgFrm.Page1.oPag.oIVDESIVA_1_15.value=this.w_IVDESIVA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsve_kacPag1 as StdContainer
  Width  = 517
  height = 131
  stdWidth  = 517
  stdheight = 131
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVTIPOPE_1_1 as StdField with uid="HFEAEJDCNW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MVTIPOPE", cQueryName = "MVTIPOPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria operazione",;
    HelpContextID = 60528373,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=174, Top=69, InputMask=replicate('X',10), cLinkFile="TIPCODIV", oKey_1_1="TI__TIPO", oKey_1_2="this.w_MVCATOPE", oKey_2_1="TICODICE", oKey_2_2="this.w_MVTIPOPE"

  func oMVTIPOPE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oTIDESCRI_1_2 as StdField with uid="VCQTSWLLPF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TIDESCRI", cQueryName = "TIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 9395327,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=69, InputMask=replicate('X',30)

  add object oANTIPOPE_1_3 as StdField with uid="DGSCEUJPFP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ANTIPOPE", cQueryName = "ANTIPOPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria cliente / fornitore",;
    HelpContextID = 60530613,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=174, Top=39, InputMask=replicate('X',10), cLinkFile="TIPCODIV", oKey_1_1="TI__TIPO", oKey_1_2="this.w_ANCATOPE", oKey_2_1="TICODICE", oKey_2_2="this.w_ANTIPOPE"

  func oANTIPOPE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oANIDESCRI_1_4 as StdField with uid="SHRRTRBYQL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ANIDESCRI", cQueryName = "ANIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 263107816,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=39, InputMask=replicate('X',30)

  add object oARTIPOPE_1_5 as StdField with uid="IYGFPFCMHQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ARTIPOPE", cQueryName = "ARTIPOPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria articolo",;
    HelpContextID = 60529589,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=174, Top=9, InputMask=replicate('X',10), cLinkFile="TIPCODIV", oKey_1_1="TI__TIPO", oKey_1_2="this.w_ARCATOPE", oKey_2_1="TICODICE", oKey_2_2="this.w_ARTIPOPE"

  func oARTIPOPE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oARIDESCRI_1_6 as StdField with uid="DDXJDFJERO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ARIDESCRI", cQueryName = "ARIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 263108840,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=9, InputMask=replicate('X',30)

  add object oCODIVA_1_14 as StdField with uid="DNKDDYWUHD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODIVA", cQueryName = "CODIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 20750042,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=174, Top=98, InputMask=replicate('X',5), cLinkFile="VOCIIVA", oKey_1_1="IVCODIVA", oKey_1_2="this.w_CODIVA"

  func oCODIVA_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oIVDESIVA_1_15 as StdField with uid="UVKSATNEKP",rtseq=11,rtrep=.f.,;
    cFormVar = "w_IVDESIVA", cQueryName = "IVDESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 110061767,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=263, Top=98, InputMask=replicate('X',35)

  add object oStr_1_10 as StdString with uid="WSWWZPWTEV",Visible=.t., Left=71, Top=9,;
    Alignment=1, Width=100, Height=18,;
    Caption="Categoria articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="GTFPCCJTKJ",Visible=.t., Left=59, Top=39,;
    Alignment=1, Width=112, Height=18,;
    Caption="Categoria cli/for:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="VDCOIUTIWQ",Visible=.t., Left=48, Top=69,;
    Alignment=1, Width=123, Height=18,;
    Caption="Categoria operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="PRAQNBOHSF",Visible=.t., Left=51, Top=98,;
    Alignment=1, Width=120, Height=18,;
    Caption="Codice IVA:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kac','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
