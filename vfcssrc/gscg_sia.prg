* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sia                                                        *
*              Dichiarazione IVA annuale                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_338]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-21                                                      *
* Last revis.: 2015-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sia",oParentObject))

* --- Class definition
define class tgscg_sia as StdForm
  Top    = 2
  Left   = 29

  * --- Standard Properties
  Width  = 615
  Height = 552
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-30"
  HelpContextID=258614633
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=35

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_sia"
  cComment = "Dichiarazione IVA annuale"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_VPCODVAL = space(1)
  w_CONFER = space(10)
  w_VLIMPO08 = 0
  w_VLIMPO04 = 0
  w_VLIMPO07 = 0
  w_VLIMPCRP = 0
  w_DEB08 = 0
  w_CRE08 = 0
  w_VLIMPO09 = 0
  w_VLIMPO14 = 0
  w_VLIMPO10 = 0
  w_VLIMPO11 = 0
  w_VLIMPO23 = 0
  w_VLIMPO12 = 0
  w_VLIMPO13 = 0
  w_APPO1 = 0
  w_VLIMPO15 = 0
  w_VLIMPO16 = 0
  w_VLIMPO17 = 0
  w_VLIMPO18 = 0
  w_VLIMPO19 = 0
  w_APPO2 = 0
  w_TOTALE = 0
  w_VLIMPO20 = 0
  w_VLIMPO21 = 0
  w_VPRORATA = space(1)
  w_VLIMPO36 = 0
  w_ANNDIC = space(4)
  w_DENMAGPI = 0
  w_VE37 = 0
  w_VE38 = 0
  w_INIANNO = ctod('  /  /  ')
  w_FINANNO = ctod('  /  /  ')
  w_VF16 = 0
  w_VF17 = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_siaPag1","gscg_sia",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVLIMPO13_1_39
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_sia
    *This.bUpdated=.t.
    * Imposto il titolo
      this.cComment='Dichiarazione IVA annuale '+IIF(this.oParentObject.oParentObject.w_FLDEFI = 'S' ,'definitiva','in prova')
      this.cComment=  this.cComment+' del '+this.oParentObject.oParentObject.w_ANNO
    
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VPCODVAL=space(1)
      .w_CONFER=space(10)
      .w_VLIMPO08=0
      .w_VLIMPO04=0
      .w_VLIMPO07=0
      .w_VLIMPCRP=0
      .w_DEB08=0
      .w_CRE08=0
      .w_VLIMPO09=0
      .w_VLIMPO14=0
      .w_VLIMPO10=0
      .w_VLIMPO11=0
      .w_VLIMPO23=0
      .w_VLIMPO12=0
      .w_VLIMPO13=0
      .w_APPO1=0
      .w_VLIMPO15=0
      .w_VLIMPO16=0
      .w_VLIMPO17=0
      .w_VLIMPO18=0
      .w_VLIMPO19=0
      .w_APPO2=0
      .w_TOTALE=0
      .w_VLIMPO20=0
      .w_VLIMPO21=0
      .w_VPRORATA=space(1)
      .w_VLIMPO36=0
      .w_ANNDIC=space(4)
      .w_DENMAGPI=0
      .w_VE37=0
      .w_VE38=0
      .w_INIANNO=ctod("  /  /  ")
      .w_FINANNO=ctod("  /  /  ")
      .w_VF16=0
      .w_VF17=0
      .w_VPCODVAL=oParentObject.w_VPCODVAL
      .w_CONFER=oParentObject.w_CONFER
      .w_VLIMPO08=oParentObject.w_VLIMPO08
      .w_VLIMPO04=oParentObject.w_VLIMPO04
      .w_VLIMPO07=oParentObject.w_VLIMPO07
      .w_VLIMPCRP=oParentObject.w_VLIMPCRP
      .w_VLIMPO09=oParentObject.w_VLIMPO09
      .w_VLIMPO14=oParentObject.w_VLIMPO14
      .w_VLIMPO10=oParentObject.w_VLIMPO10
      .w_VLIMPO11=oParentObject.w_VLIMPO11
      .w_VLIMPO23=oParentObject.w_VLIMPO23
      .w_VLIMPO12=oParentObject.w_VLIMPO12
      .w_VLIMPO13=oParentObject.w_VLIMPO13
      .w_APPO1=oParentObject.w_APPO1
      .w_VLIMPO15=oParentObject.w_VLIMPO15
      .w_VLIMPO16=oParentObject.w_VLIMPO16
      .w_VLIMPO17=oParentObject.w_VLIMPO17
      .w_VLIMPO18=oParentObject.w_VLIMPO18
      .w_VLIMPO19=oParentObject.w_VLIMPO19
      .w_APPO2=oParentObject.w_APPO2
      .w_TOTALE=oParentObject.w_TOTALE
      .w_VLIMPO20=oParentObject.w_VLIMPO20
      .w_VLIMPO21=oParentObject.w_VLIMPO21
      .w_VPRORATA=oParentObject.w_VPRORATA
      .w_VLIMPO36=oParentObject.w_VLIMPO36
      .w_ANNDIC=oParentObject.w_ANNDIC
      .w_DENMAGPI=oParentObject.w_DENMAGPI
      .w_VE37=oParentObject.w_VE37
      .w_VE38=oParentObject.w_VE38
      .w_INIANNO=oParentObject.w_INIANNO
      .w_FINANNO=oParentObject.w_FINANNO
      .w_VF16=oParentObject.w_VF16
      .w_VF17=oParentObject.w_VF17
          .DoRTCalc(1,1,.f.)
        .w_CONFER = 'S'
          .DoRTCalc(3,6,.f.)
        .w_DEB08 = IIF(.w_VLIMPO08>=0, .w_VLIMPO08, 0)
        .w_CRE08 = IIF(.w_VLIMPO08>=0,0, ABS(.w_VLIMPO08))
          .DoRTCalc(9,15,.f.)
        .w_APPO1 = .w_VLIMPO09 + .w_VLIMPO10 + .w_VLIMPO11 + .w_VLIMPO12 + .w_VLIMPO13 + .w_VLIMPO23
          .DoRTCalc(17,21,.f.)
        .w_APPO2 = .w_VLIMPO14 + .w_VLIMPO15 + .w_VLIMPO16 + .w_VLIMPO17 + .w_VLIMPO18 + .w_VLIMPO19
        .w_TOTALE = (.w_VLIMPO08 + .w_APPO1) - .w_APPO2
        .w_VLIMPO20 = IIF(.w_TOTALE<0, ABS(.w_TOTALE), 0)
        .w_VLIMPO21 = IIF(.w_TOTALE>=0,.w_TOTALE, 0)
          .DoRTCalc(26,26,.f.)
        .w_VLIMPO36 = IIF(.w_TOTALE>=0 AND g_TIPDEN<>'M' AND .w_DENMAGPI<>0,IIF(g_PERVAL=g_CODEUR,INTROUND(cp_ROUND((.w_TOTALE*.w_DENMAGPI)/100,g_PERPVL),1),cp_ROUND((.w_TOTALE*.w_DENMAGPI)/100,g_PERPVL)), 0)
      .oPgFrm.Page1.oPag.oObj_1_77.Calculate('Cred. IVA risult. dalla dichiar. per il '+ALLTRIM(STR(VAL(.w_ANNDIC)-1)))
      .oPgFrm.Page1.oPag.oObj_1_78.Calculate('Cred. IVA risult. dai primi 3 trim. del '+.w_ANNDIC)
      .oPgFrm.Page1.oPag.oObj_1_79.Calculate('Credito risultante dalla dichiarazione per il '+ALLTRIM(STR(VAL(.w_ANNDIC)-1)))
    endwith
    this.DoRTCalc(28,35,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_VPCODVAL=.w_VPCODVAL
      .oParentObject.w_CONFER=.w_CONFER
      .oParentObject.w_VLIMPO08=.w_VLIMPO08
      .oParentObject.w_VLIMPO04=.w_VLIMPO04
      .oParentObject.w_VLIMPO07=.w_VLIMPO07
      .oParentObject.w_VLIMPCRP=.w_VLIMPCRP
      .oParentObject.w_VLIMPO09=.w_VLIMPO09
      .oParentObject.w_VLIMPO14=.w_VLIMPO14
      .oParentObject.w_VLIMPO10=.w_VLIMPO10
      .oParentObject.w_VLIMPO11=.w_VLIMPO11
      .oParentObject.w_VLIMPO23=.w_VLIMPO23
      .oParentObject.w_VLIMPO12=.w_VLIMPO12
      .oParentObject.w_VLIMPO13=.w_VLIMPO13
      .oParentObject.w_APPO1=.w_APPO1
      .oParentObject.w_VLIMPO15=.w_VLIMPO15
      .oParentObject.w_VLIMPO16=.w_VLIMPO16
      .oParentObject.w_VLIMPO17=.w_VLIMPO17
      .oParentObject.w_VLIMPO18=.w_VLIMPO18
      .oParentObject.w_VLIMPO19=.w_VLIMPO19
      .oParentObject.w_APPO2=.w_APPO2
      .oParentObject.w_TOTALE=.w_TOTALE
      .oParentObject.w_VLIMPO20=.w_VLIMPO20
      .oParentObject.w_VLIMPO21=.w_VLIMPO21
      .oParentObject.w_VPRORATA=.w_VPRORATA
      .oParentObject.w_VLIMPO36=.w_VLIMPO36
      .oParentObject.w_ANNDIC=.w_ANNDIC
      .oParentObject.w_DENMAGPI=.w_DENMAGPI
      .oParentObject.w_VE37=.w_VE37
      .oParentObject.w_VE38=.w_VE38
      .oParentObject.w_INIANNO=.w_INIANNO
      .oParentObject.w_FINANNO=.w_FINANNO
      .oParentObject.w_VF16=.w_VF16
      .oParentObject.w_VF17=.w_VF17
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
            .w_DEB08 = IIF(.w_VLIMPO08>=0, .w_VLIMPO08, 0)
            .w_CRE08 = IIF(.w_VLIMPO08>=0,0, ABS(.w_VLIMPO08))
        .DoRTCalc(9,15,.t.)
            .w_APPO1 = .w_VLIMPO09 + .w_VLIMPO10 + .w_VLIMPO11 + .w_VLIMPO12 + .w_VLIMPO13 + .w_VLIMPO23
        .DoRTCalc(17,21,.t.)
            .w_APPO2 = .w_VLIMPO14 + .w_VLIMPO15 + .w_VLIMPO16 + .w_VLIMPO17 + .w_VLIMPO18 + .w_VLIMPO19
            .w_TOTALE = (.w_VLIMPO08 + .w_APPO1) - .w_APPO2
            .w_VLIMPO20 = IIF(.w_TOTALE<0, ABS(.w_TOTALE), 0)
            .w_VLIMPO21 = IIF(.w_TOTALE>=0,.w_TOTALE, 0)
        .DoRTCalc(26,26,.t.)
            .w_VLIMPO36 = IIF(.w_TOTALE>=0 AND g_TIPDEN<>'M' AND .w_DENMAGPI<>0,IIF(g_PERVAL=g_CODEUR,INTROUND(cp_ROUND((.w_TOTALE*.w_DENMAGPI)/100,g_PERPVL),1),cp_ROUND((.w_TOTALE*.w_DENMAGPI)/100,g_PERPVL)), 0)
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate('Cred. IVA risult. dalla dichiar. per il '+ALLTRIM(STR(VAL(.w_ANNDIC)-1)))
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate('Cred. IVA risult. dai primi 3 trim. del '+.w_ANNDIC)
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate('Credito risultante dalla dichiarazione per il '+ALLTRIM(STR(VAL(.w_ANNDIC)-1)))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,35,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate('Cred. IVA risult. dalla dichiar. per il '+ALLTRIM(STR(VAL(.w_ANNDIC)-1)))
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate('Cred. IVA risult. dai primi 3 trim. del '+.w_ANNDIC)
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate('Credito risultante dalla dichiarazione per il '+ALLTRIM(STR(VAL(.w_ANNDIC)-1)))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oVLIMPCRP_1_10.visible=!this.oPgFrm.Page1.oPag.oVLIMPCRP_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_69.visible=!this.oPgFrm.Page1.oPag.oStr_1_69.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_71.visible=!this.oPgFrm.Page1.oPag.oStr_1_71.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_75.visible=!this.oPgFrm.Page1.oPag.oStr_1_75.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_77.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_78.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_79.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVLIMPO04_1_8.value==this.w_VLIMPO04)
      this.oPgFrm.Page1.oPag.oVLIMPO04_1_8.value=this.w_VLIMPO04
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO07_1_9.value==this.w_VLIMPO07)
      this.oPgFrm.Page1.oPag.oVLIMPO07_1_9.value=this.w_VLIMPO07
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPCRP_1_10.value==this.w_VLIMPCRP)
      this.oPgFrm.Page1.oPag.oVLIMPCRP_1_10.value=this.w_VLIMPCRP
    endif
    if not(this.oPgFrm.Page1.oPag.oDEB08_1_11.value==this.w_DEB08)
      this.oPgFrm.Page1.oPag.oDEB08_1_11.value=this.w_DEB08
    endif
    if not(this.oPgFrm.Page1.oPag.oCRE08_1_13.value==this.w_CRE08)
      this.oPgFrm.Page1.oPag.oCRE08_1_13.value=this.w_CRE08
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO09_1_17.value==this.w_VLIMPO09)
      this.oPgFrm.Page1.oPag.oVLIMPO09_1_17.value=this.w_VLIMPO09
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO14_1_18.value==this.w_VLIMPO14)
      this.oPgFrm.Page1.oPag.oVLIMPO14_1_18.value=this.w_VLIMPO14
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO10_1_30.value==this.w_VLIMPO10)
      this.oPgFrm.Page1.oPag.oVLIMPO10_1_30.value=this.w_VLIMPO10
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO11_1_32.value==this.w_VLIMPO11)
      this.oPgFrm.Page1.oPag.oVLIMPO11_1_32.value=this.w_VLIMPO11
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO23_1_34.value==this.w_VLIMPO23)
      this.oPgFrm.Page1.oPag.oVLIMPO23_1_34.value=this.w_VLIMPO23
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO12_1_36.value==this.w_VLIMPO12)
      this.oPgFrm.Page1.oPag.oVLIMPO12_1_36.value=this.w_VLIMPO12
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO13_1_39.value==this.w_VLIMPO13)
      this.oPgFrm.Page1.oPag.oVLIMPO13_1_39.value=this.w_VLIMPO13
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO15_1_47.value==this.w_VLIMPO15)
      this.oPgFrm.Page1.oPag.oVLIMPO15_1_47.value=this.w_VLIMPO15
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO16_1_49.value==this.w_VLIMPO16)
      this.oPgFrm.Page1.oPag.oVLIMPO16_1_49.value=this.w_VLIMPO16
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO17_1_51.value==this.w_VLIMPO17)
      this.oPgFrm.Page1.oPag.oVLIMPO17_1_51.value=this.w_VLIMPO17
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO18_1_53.value==this.w_VLIMPO18)
      this.oPgFrm.Page1.oPag.oVLIMPO18_1_53.value=this.w_VLIMPO18
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO19_1_55.value==this.w_VLIMPO19)
      this.oPgFrm.Page1.oPag.oVLIMPO19_1_55.value=this.w_VLIMPO19
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO20_1_62.value==this.w_VLIMPO20)
      this.oPgFrm.Page1.oPag.oVLIMPO20_1_62.value=this.w_VLIMPO20
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO21_1_64.value==this.w_VLIMPO21)
      this.oPgFrm.Page1.oPag.oVLIMPO21_1_64.value=this.w_VLIMPO21
    endif
    if not(this.oPgFrm.Page1.oPag.oVLIMPO36_1_74.value==this.w_VLIMPO36)
      this.oPgFrm.Page1.oPag.oVLIMPO36_1_74.value=this.w_VLIMPO36
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_siaPag1 as StdContainer
  Width  = 611
  height = 552
  stdWidth  = 611
  stdheight = 552
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_3 as StdButton with uid="VRQSBCWMBM",left=7, top=504, width=48,height=45,;
    CpPicture="BMP\FATTURA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i valori relativi alle fatture ad esig. differita";
    , HelpContextID = 140510297;
    , caption='\<Altri dati';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      do GSCG_SED with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_4 as StdButton with uid="OWADLCCGCA",left=58, top=504, width=48,height=45,;
    CpPicture="BMP\DATIIVA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i valori relativi alla separata indic. operaz. effettuate della liquidazione annuale";
    , HelpContextID = 249535226;
    , caption='\<Quadri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      do GSCG_SVT with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_5 as StdButton with uid="SCOWEPREFI",left=469, top=504, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare la liquidazione IVA";
    , HelpContextID = 116825050;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_7 as StdButton with uid="DNZCDYSJHL",left=519, top=504, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare l'elaborazione";
    , HelpContextID = 251297210;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oVLIMPO04_1_8 as StdField with uid="VDQTBQGJVU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_VLIMPO04", cQueryName = "VLIMPO04",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ammontare totale dell'IVA esigibile",;
    HelpContextID = 186139510,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=247, Top=47, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO07_1_9 as StdField with uid="ITUAGLNIIO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_VLIMPO07", cQueryName = "VLIMPO07",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ammontare totale dell'IVA detraibile",;
    HelpContextID = 186139507,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=427, Top=67, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPCRP_1_10 as StdField with uid="SEGBCMUPRI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_VLIMPCRP", cQueryName = "VLIMPCRP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 119030618,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=427, Top=87, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVLIMPCRP_1_10.mHide()
    with this.Parent.oContained
      return (.w_VPRORATA<>'S')
    endwith
  endfunc

  add object oDEB08_1_11 as StdField with uid="IVEFZMUTWC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DEB08", cQueryName = "DEB08",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo l'IVA a debito",;
    HelpContextID = 196459466,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=247, Top=108, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oCRE08_1_13 as StdField with uid="HSMTGNOKMR",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CRE08", cQueryName = "CRE08",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo l'IVA a credito",;
    HelpContextID = 196443866,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=427, Top=108, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO09_1_17 as StdField with uid="EDZJPKZNTP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_VLIMPO09", cQueryName = "VLIMPO09",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo dei rimborsi infrannuali regolarmente richiesti",;
    HelpContextID = 186139505,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=247, Top=148, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO14_1_18 as StdField with uid="XTQEMRWQFV",rtseq=10,rtrep=.f.,;
    cFormVar = "w_VLIMPO14", cQueryName = "VLIMPO14",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Credito IVA anno precedente di cui non sia stato chiesto il rimborso",;
    HelpContextID = 186139510,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=427, Top=278, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO10_1_30 as StdField with uid="SFJSMVFDHH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_VLIMPO10", cQueryName = "VLIMPO10",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Crediti trasferiti alla capogruppo, nel caso di liquidazione di societ� aderenti al gruppo",;
    HelpContextID = 186139514,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=247, Top=169, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO11_1_32 as StdField with uid="ZCMVOGYWWC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_VLIMPO11", cQueryName = "VLIMPO11",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Credito IVA risultante dalla dichiarazione precedente e compensata nel mod. F24",;
    HelpContextID = 186139513,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=247, Top=191, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO23_1_34 as StdField with uid="ZORYLPQQWS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_VLIMPO23", cQueryName = "VLIMPO23",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Credito IVA risultante dai primi 3 trimestri del anno corrente compensata nel mod. F24",;
    HelpContextID = 186139511,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=247, Top=213, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO12_1_36 as StdField with uid="ANZPVWSMGN",rtseq=14,rtrep=.f.,;
    cFormVar = "w_VLIMPO12", cQueryName = "VLIMPO12",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Interessi dovuti relativi alle liquidazioni periodiche dei contribuenti trimestrali",;
    HelpContextID = 186139512,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=247, Top=234, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO13_1_39 as StdField with uid="IFIWVOUMON",rtseq=15,rtrep=.f.,;
    cFormVar = "w_VLIMPO13", cQueryName = "VLIMPO13",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Interessi dovuti, da versare unitamente all'imposta, a seguito di ravvedimento",;
    HelpContextID = 186139511,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=247, Top=255, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO15_1_47 as StdField with uid="BFAHEKCCBH",rtseq=17,rtrep=.f.,;
    cFormVar = "w_VLIMPO15", cQueryName = "VLIMPO15",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Rimborsi richiesti in anni precedenti, ma autorizzati sono nell'anno corrente",;
    HelpContextID = 186139509,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=427, Top=298, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO16_1_49 as StdField with uid="NDINZKUKSN",rtseq=18,rtrep=.f.,;
    cFormVar = "w_VLIMPO16", cQueryName = "VLIMPO16",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ammontare complessivo dei particolari crediti d'imposta utilizzati a scomputo dei versamenti",;
    HelpContextID = 186139508,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=427, Top=319, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO17_1_51 as StdField with uid="SIGBICMWQA",rtseq=19,rtrep=.f.,;
    cFormVar = "w_VLIMPO17", cQueryName = "VLIMPO17",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale versamenti periodici (compreso acconto) effettuati durante l'anno",;
    HelpContextID = 186139507,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=427, Top=340, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO18_1_53 as StdField with uid="YXLJVNAHGD",rtseq=20,rtrep=.f.,;
    cFormVar = "w_VLIMPO18", cQueryName = "VLIMPO18",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Debiti trasferiti alla capogruppo, nel caso di liquidazione di societ� aderenti al gruppo",;
    HelpContextID = 186139506,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=427, Top=361, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO19_1_55 as StdField with uid="RGCPXHOXXI",rtseq=21,rtrep=.f.,;
    cFormVar = "w_VLIMPO19", cQueryName = "VLIMPO19",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Versamenti integrativi d'imposta di competenza dell'anno corrente",;
    HelpContextID = 186139505,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=427, Top=382, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO20_1_62 as StdField with uid="IEALMKZAFZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_VLIMPO20", cQueryName = "VLIMPO20",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo a credito",;
    HelpContextID = 186139514,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=427, Top=418, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO21_1_64 as StdField with uid="QPBGTKULGZ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_VLIMPO21", cQueryName = "VLIMPO21",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo a debito [debiti (da VL8 a VL25) - crediti (da VL8 a VL31)]",;
    HelpContextID = 186139513,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=247, Top=439, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVLIMPO36_1_74 as StdField with uid="UGWXRCGTHS",rtseq=27,rtrep=.f.,;
    cFormVar = "w_VLIMPO36", cQueryName = "VLIMPO36",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Interessi dovuti in sede di liquidazione annuale (pari alla percentuale di maggiorazione IVA definita nei parametri)",;
    HelpContextID = 82295948,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=247, Top=463, cSayPict="v_PV(40)", cGetPict="v_GV(40)"


  add object oObj_1_77 as cp_calclbl with uid="KAIBHYDKZW",left=4, top=191, width=233,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 80616986


  add object oObj_1_78 as cp_calclbl with uid="DQJMLXXBFD",left=4, top=213, width=233,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 80616986


  add object oObj_1_79 as cp_calclbl with uid="CMQGRGWBPI",left=4, top=277, width=400,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",Fontbold=.t.,;
    nPag=1;
    , HelpContextID = 80616986

  add object oStr_1_12 as StdString with uid="BLOCXAQKOG",Visible=.t., Left=4, Top=107,;
    Alignment=0, Width=212, Height=18,;
    Caption="IMPOSTA DOVUTA O A CREDITO"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="AKWMSTHERQ",Visible=.t., Left=247, Top=23,;
    Alignment=0, Width=135, Height=15,;
    Caption="DEBITI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="MYDEPBLTLJ",Visible=.t., Left=427, Top=22,;
    Alignment=0, Width=163, Height=15,;
    Caption="CREDITI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="RCZBQPOODV",Visible=.t., Left=4, Top=148,;
    Alignment=0, Width=233, Height=18,;
    Caption="Rimborsi infrannuali richiesti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="DDOORJJLRU",Visible=.t., Left=399, Top=49,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="VXNLKRYKQG",Visible=.t., Left=399, Top=110,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="FDQVZLQKMP",Visible=.t., Left=581, Top=69,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="AILWKRTMMD",Visible=.t., Left=581, Top=110,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="KGJCMGBWZH",Visible=.t., Left=399, Top=155,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="JZNMEZWYEA",Visible=.t., Left=581, Top=284,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="HKCSQPDBJE",Visible=.t., Left=4, Top=67,;
    Alignment=0, Width=112, Height=18,;
    Caption="IVA detraibile"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="AKASXHHDSL",Visible=.t., Left=4, Top=47,;
    Alignment=0, Width=108, Height=18,;
    Caption="IVA a debito"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_29 as StdString with uid="EYWSKDWZTI",Visible=.t., Left=4, Top=169,;
    Alignment=0, Width=233, Height=18,;
    Caption="Ammontare dei crediti trasferiti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_31 as StdString with uid="WJNBIUXDPE",Visible=.t., Left=399, Top=176,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="KHYXHPLFXC",Visible=.t., Left=399, Top=198,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="EAZXZRWDTV",Visible=.t., Left=4, Top=234,;
    Alignment=0, Width=233, Height=18,;
    Caption="Interessi dovuti per le liq.trim."  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_37 as StdString with uid="XVBCNGEXOP",Visible=.t., Left=399, Top=241,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="ORJJFISBNI",Visible=.t., Left=4, Top=255,;
    Alignment=0, Width=233, Height=18,;
    Caption="Interessi dovuti a seg. di ravvedim."    , ForeColor=RGB(27,80,180);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="XYDDYCRXJN",Visible=.t., Left=399, Top=262,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="TKSUEDOSLE",Visible=.t., Left=4, Top=298,;
    Alignment=0, Width=400, Height=18,;
    Caption="Credito richiesto a rimborso in anni precedenti"    , ForeColor=RGB(27,80,180);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_43 as StdString with uid="VGDUXZNOSB",Visible=.t., Left=4, Top=319,;
    Alignment=0, Width=400, Height=18,;
    Caption="Crediti d'imposta utilizzati nelle liquidazioni periodiche/acconto"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_44 as StdString with uid="FDHVWCYLKU",Visible=.t., Left=4, Top=340,;
    Alignment=0, Width=400, Height=18,;
    Caption="Ammontare versamenti periodici, compreso acconto"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="LZDSSLJGWW",Visible=.t., Left=4, Top=361,;
    Alignment=0, Width=400, Height=18,;
    Caption="Ammontare dei debiti trasferiti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="OGQWXTFLHE",Visible=.t., Left=4, Top=382,;
    Alignment=0, Width=400, Height=18,;
    Caption="Versamenti integrativi d'imposta"    , ForeColor=RGB(27,80,180);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_48 as StdString with uid="ISCECCSCQR",Visible=.t., Left=581, Top=305,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="WDGQQXMDCI",Visible=.t., Left=581, Top=326,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="BOKXEQMNNE",Visible=.t., Left=581, Top=347,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="UBFPBDUOMM",Visible=.t., Left=581, Top=368,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="YOANXPRKRA",Visible=.t., Left=581, Top=389,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="HKLDRGAAJM",Visible=.t., Left=4, Top=418,;
    Alignment=0, Width=209, Height=18,;
    Caption="IVA A CREDITO"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_61 as StdString with uid="OKSMBQXMAX",Visible=.t., Left=4, Top=439,;
    Alignment=0, Width=209, Height=18,;
    Caption="IVA A DEBITO"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_63 as StdString with uid="ZAYISHEHZE",Visible=.t., Left=581, Top=418,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="KHZOQMXCHE",Visible=.t., Left=399, Top=439,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_67 as StdString with uid="PQAFHGXTCL",Visible=.t., Left=4, Top=5,;
    Alignment=0, Width=285, Height=13,;
    Caption="In blue le descrizioni associate ad importi editabili."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_68 as StdString with uid="HIISBKIYPU",Visible=.t., Left=429, Top=191,;
    Alignment=0, Width=174, Height=18,;
    Caption="(Compensato mod. F24)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_69 as StdString with uid="AUKXQQCOLM",Visible=.t., Left=399, Top=220,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_69.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_1_70 as StdString with uid="SXPNEWOFLE",Visible=.t., Left=429, Top=213,;
    Alignment=0, Width=174, Height=18,;
    Caption="(Compensato mod. F24)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_71 as StdString with uid="MQRXQBWDZB",Visible=.t., Left=4, Top=87,;
    Alignment=0, Width=112, Height=18,;
    Caption="Prorata"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_71.mHide()
    with this.Parent.oContained
      return (.w_VPRORATA<>'S')
    endwith
  endfunc

  add object oStr_1_73 as StdString with uid="PYPUADFWED",Visible=.t., Left=4, Top=463,;
    Alignment=0, Width=245, Height=18,;
    Caption="Interessi dovuti in sede di liquid. ann."  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_75 as StdString with uid="BGXQVKYGYM",Visible=.t., Left=399, Top=463,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_1_75.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oBox_1_25 as StdBox with uid="UDOZEZJASZ",left=6, top=40, width=599,height=1

  add object oBox_1_28 as StdBox with uid="XRXBZCMCAL",left=6, top=138, width=599,height=1

  add object oBox_1_59 as StdBox with uid="OXJZUAFIAY",left=6, top=411, width=599,height=1

  add object oBox_1_66 as StdBox with uid="ZWXJXVLKBL",left=6, top=499, width=599,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sia','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
