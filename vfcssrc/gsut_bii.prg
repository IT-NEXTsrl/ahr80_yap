* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bii                                                        *
*              Controllo valorizzazione attributi SOS per invio indici         *
*                                                                              *
*      Author: Gianluca B.                                                     *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-02-03                                                      *
* Last revis.: 2011-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bii",oParentObject,m.pOPER)
return(i_retval)

define class tgsut_bii as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_MASK = .NULL.
  w_CURSORE = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico e valorizzo il campo CHKPASS del cursore associato allo zoom presente in GSDM_KII
    this.w_MASK = this.oParentObject
    if "tgsut_kfg" $ lower(this.w_MASK.Class)
      this.w_CURSORE = this.w_MASK.w_ZoomGF.cCursor
    else
      this.w_CURSORE = this.w_MASK.w_PRATICHE.cCursor
    endif
    do case
      case this.pOPER = "C"
         
 Select (this.w_CURSORE) 
 Go Top
        Scan
        * --- Prima verifica - Presenza nell'indice di tutti gli attributi liberi definiti nella classe SOS
        * --- Select from ..\docm\exe\query\GSDM_BII
        do vq_exec with '..\docm\exe\query\GSDM_BII',this,'_Curs__d__d__docm_exe_query_GSDM_BII','',.f.,.t.
        if used('_Curs__d__d__docm_exe_query_GSDM_BII')
          select _Curs__d__d__docm_exe_query_GSDM_BII
          locate for 1=1
          do while not(eof())
          Update (this.w_CURSORE) set CHKATT="KO" where GFCLASDOCU=_Curs__d__d__docm_exe_query_GSDM_BII.CDCODCLA
            select _Curs__d__d__docm_exe_query_GSDM_BII
            continue
          enddo
          use
        endif
        Endscan
      case this.pOPER = "S"
        if "tgsut_kfg" $ lower(this.w_MASK.Class)
          Update (this.w_CURSORE) Set XCHK= IIF( this.oParentObject.w_SELEZIONE="D" , 0 , 1 ) where ISFILE="S"
        else
          Update (this.w_CURSORE) Set XCHK= IIF( this.oParentObject.w_SELEZIONE="D" , 0 , 1 ) where ISFILE="S"
        endif
        if this.oParentObject.w_SELEZIONE="S"
          this.oParentObject.w_SELMULT = .T.
        else
          this.oParentObject.w_SELMULT = .F.
        endif
      case this.pOPER = "Z"
        SELECT (this.w_CURSORE)
        if "tgsut_kfg" $ lower(this.w_MASK.Class)
          if EMPTY(ISFILE) OR ISFILE = "N"
            this.w_MASK.w_ZoomGF.grd.value = 0
            ah_errormsg("Selezione non ammessa, allegato inesistente",48)
          else
            this.oParentObject.w_SELMULT = .T.
          endif
        else
          if EMPTY(ISFILE) OR ISFILE = "N"
            this.w_MASK.w_PRATICHE.grd.value = 0
            ah_errormsg("Selezione non ammessa, allegato inesistente",48)
          else
            this.oParentObject.w_SELMULT = .T.
          endif
        endif
        replace XCHK with 0
      case this.pOPER = "U"
        SELECT count(*) as CONTA from (this.w_CURSORE) where XCHK=1 INTO ARRAY ARRAYSEL
        if Vartype(ARRAYSEL) = "U" or ARRAYSEL[1] < 2
          this.oParentObject.w_SELMULT = .F.
        endif
    endcase
    if "tgsut_kfg" $ lower(this.w_MASK.Class)
      this.w_MASK.w_ZoomGF.grd.refresh()
    else
      this.w_MASK.w_PRATICHE.grd.refresh()
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs__d__d__docm_exe_query_GSDM_BII')
      use in _Curs__d__d__docm_exe_query_GSDM_BII
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
