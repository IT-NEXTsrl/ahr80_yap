* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bgr                                                        *
*              Giroconto IVA indetraibile da pro-rata                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-03-19                                                      *
* Last revis.: 2013-04-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bgr",oParentObject,m.pOper)
return(i_retval)

define class tgscg_bgr as StdBatch
  * --- Local variables
  pOper = space(5)
  w_ANCCTAGG = space(1)
  w_ROWANAL = 0
  w_GPBUATT = space(3)
  w_PNSERIAL = space(10)
  o_PNSERIAL = space(10)
  w_IVIMPIVA = 0
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_PNCODBUN = space(3)
  w_PNINICOM = ctod("  /  /  ")
  w_PNFINCOM = ctod("  /  /  ")
  w_MRCODVOC = space(15)
  w_MRCODICE = space(15)
  w_MRCODCOM = space(15)
  w_MRINICOM = ctod("  /  /  ")
  w_MRFINCOM = ctod("  /  /  ")
  w_MRTOTIMP = 0
  w_PNIMPORTO = 0
  w_PNSERDES = space(10)
  w_PNNUMRER = 0
  w_PNCODESE = space(4)
  w_PNCODUTE = 0
  w_CONTA3 = 0
  o_NUMRIG = 0
  o_CODICE = space(15)
  o_TIPCON = space(1)
  o_CODBUN = space(3)
  o_PNINICOM = ctod("  /  /  ")
  o_PNFINCOM = ctod("  /  /  ")
  w_NUMRIG = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_MRTOTPN = 0
  w_TOTIMPPN = 0
  w_PNDATREG = ctod("  /  /  ")
  w_PNIMPDAR = space(18)
  w_PNIMPAVE = space(18)
  w_NUM = 0
  w_PNCONTRO = space(15)
  w_TOTCONTRO = 0
  w_CCTAGG = space(1)
  w_MR_SEGNO = space(1)
  w_PARAMETRO = 0
  w_PNDESSUP = space(50)
  w_PNDESRIG = space(50)
  w_TOTPAR = 0
  w_MESS = space(100)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_PNROWNUM = 0
  o_PNROWNUM = 0
  w_AnaliticaContoStorno = .f.
  w_PNFLZERO = space(1)
  w_TOTANA = 0
  w_TOTPNT = 0
  w_READAZI = space(0)
  w_PNPRG = space(8)
  w_TOTREG = 0
  w_CONARR = space(15)
  w_STAMPAERR = .f.
  w_oERRORLOG = .NULL.
  * --- WorkFile variables
  CONTI_idx=0
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  MOVICOST_idx=0
  MASTRI_idx=0
  BUSIUNIT_idx=0
  SALDICON_idx=0
  TMPPNT_MAST_idx=0
  AZIENDA_idx=0
  TMPTPNT_DETT_idx=0
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_AnaliticaContoStorno = .f.
    do case
      case this.pOper=="SAVE"
        if (this.oParentObject.w_AZFLGIND<>"S" AND EMPTY(this.oParentObject.w_AZCONPRO)) OR (this.oParentObject.w_AZFLGIND="S" AND EMPTY(this.oParentObject.w_AZCONIND) )
          this.w_MESS = AH_MsgFormat("Attenzione, non � stato specificato nei parametri iva il conto iva acquisti  reverse charge o il conto iva indetraibile%0Operazione annullata")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
        else
          this.w_READAZI = i_CODAZI
          this.w_oERRORLOG=createobject("AH_ErrorLog")
          * --- Read from CONTROPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTROPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "COCONARR"+;
              " from "+i_cTable+" CONTROPA where ";
                  +"COCODAZI = "+cp_ToStrODBC(this.w_READAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              COCONARR;
              from (i_cTable) where;
                  COCODAZI = this.w_READAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONARR = NVL(cp_ToDate(_read_.COCONARR),cp_NullValue(_read_.COCONARR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Empty(this.w_CONARR)
            this.w_MESS = AH_MsgFormat("Conto arrotondamento non valorizzato%0Operazione annullata")
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
            i_retcode = 'stop'
            return
          endif
          this.oParentObject.w_NUMREG = 0
          * --- Estrazione dati prima nota in base a filtri impostati
          * --- Create temporary table TMPPNT_MAST
          i_nIdx=cp_AddTableDef('TMPPNT_MAST') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_JDSQJWCRKR[1]
          indexes_JDSQJWCRKR[1]='PNSERIAL'
          vq_exec('gscg1bgp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_JDSQJWCRKR,.f.)
          this.TMPPNT_MAST_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          if NOT EMPTY(this.oParentObject.w_GPBUSUNI)
            * --- Delete from TMPPNT_MAST
            i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"MINCODBUN <> "+cp_ToStrODBC(this.oParentObject.w_GPBUSUNI);
                     )
            else
              delete from (i_cTable) where;
                    MINCODBUN <> this.oParentObject.w_GPBUSUNI;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
          vq_exec("query\gscg_bgp", this, "PntIVA")
          * --- Create temporary table TMPTPNT_DETT
          i_nIdx=cp_AddTableDef('TMPTPNT_DETT') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          declare indexes_ALXQJJEQKV[1]
          indexes_ALXQJJEQKV[1]='PNSERIAL'
          vq_exec('QUERY\gscg3bgp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_ALXQJJEQKV,.f.)
          this.TMPTPNT_DETT_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          if g_DETCON="S"
            * --- Se dettaglio controparttite attivo elimino codici IVA esenti\esclusi
            * --- Delete from TMPTPNT_DETT
            i_nConn=i_TableProp[this.TMPTPNT_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPTPNT_DETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              local i_cQueryTable,i_cWhere
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_cWhere=i_cTable+".PNSERIAL = "+i_cQueryTable+".PNSERIAL";
                    +" and "+i_cTable+".CODORI = "+i_cQueryTable+".CODORI";
                    +" and "+i_cTable+".PNTIPCON = "+i_cQueryTable+".PNTIPCON";
            
              do vq_exec with 'gscgebgp',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                    +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
          vq_exec("query\gscg0bgp", this, "PntInput")
          if this.oParentObject.w_GPSTORNO="C"
            this.w_PNTIPCON = "G"
            this.w_PNCODCON = this.oParentObject.w_GP_CONTO
            this.w_PNCODBUN = this.oParentObject.w_GPBUSUNI
            VQ_EXEC("GSCA_QCC",this,"ExisteAnalitica")
            if RECCOUNT("ExisteAnalitica")>0
              this.w_AnaliticaContoStorno = .t.
              Select "ExisteAnalitica" 
 use
            endif
            if this.w_AnaliticaContoStorno
              SELECT * FROM "PntInput" INTO CURSOR "PntInput" group by PNSERIAL,PNCODCON,PNTIPCON,PNCODBUN,PNINICOM,PNFINCOM,PNROWNUM
              SELECT PNSERIAL, PNTIPCON, PNCODCON, PNCODBUN, PNINICOM, PNFINCOM, MRCODVOC, MRCODICE, ; 
 MRCODCOM, MRINICOM, MRFINCOM, MRTOTIMP, SUM(PNIMPORTO) AS PNIMPORTO, ; 
 CPROWNUM, PNROWNUM FROM "PntInput" INTO CURSOR "PntInput" group by PNSERIAL,PNCODCON,PNTIPCON,PNCODBUN,PNINICOM,PNFINCOM
            endif
          endif
          * --- Drop temporary table TMPPNT_MAST
          i_nIdx=cp_GetTableDefIdx('TMPPNT_MAST')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPPNT_MAST')
          endif
          * --- Drop temporary table TMPTPNT_DETT
          i_nIdx=cp_GetTableDefIdx('TMPTPNT_DETT')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPTPNT_DETT')
          endif
          if NOT USED("PntIVA") or RECCOUNT("PntIVA")=0
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg="Non ci sono dati da elaborare"
            i_retcode = 'stop'
            return
          endif
          this.w_PNCODESE = this.oParentObject.w_GPCODESE
          this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
          this.w_PNDATREG = this.oParentObject.w_GPDATGEN
          SELECT "PntIVA"
          GO TOP
          SCAN
          this.w_PNSERIAL = PntIVA.PNSERIAL
          this.w_PNCONTRO = PntIVA.PNCODCON
          this.w_IVIMPIVA = PntIVA.IVIMPIVA
          if this.oParentObject.w_GPFLRGSC="S" or not empty(this.oParentObject.w_GPDESCRI)
            this.w_PNDESSUP = this.oParentObject.w_GPDESCRI
            this.w_PNDESRIG = LEFT(this.oParentObject.w_GPDESCRI,50)
          else
            this.w_PNDESSUP = LEFT(AH_MsgFormat("Reg. %1 del %2 %3 %4",ALLTRIM(STR(PNNUMRER))+"/"+ALLTRIM(STR(PNCODUTE))+"("+ALLTRIM(PNCOMPET)+")",DTOC(PNDATREG),ALLTRIM(CCCODCAU)+" "+ALLTRIM(CCDESCRI),PNDESSUP),50)
            this.w_PNDESRIG = LEFT(AH_MsgFormat("Reg. %1 del %2 %3 %4",ALLTRIM(STR(PNNUMRER))+"/"+ALLTRIM(STR(PNCODUTE))+"("+ALLTRIM(PNCOMPET)+")",DTOC(PNDATREG),ALLTRIM(CCCODCAU)+" "+ALLTRIM(CCDESCRI),PNDESSUP),50)
          endif
          if this.oParentObject.w_GPFLDATD="S"
            this.w_PNDATREG = PNDATREG
            this.w_PNCODESE = PNCOMPET
          endif
          SELECT "PntInput"
          GO TOP
          this.o_NUMRIG = 0
          this.o_CODICE = space(15)
          this.o_TIPCON = " "
          this.o_CODBUN = "   "
          this.o_PNINICOM = cp_chartodate("  -  -    ")
          this.o_PNFINCOM = cp_chartodate("  -  -    ")
          SELECT PNCODCON,PNTIPCON,PNCODBUN,PNINICOM,PNFINCOM,PNROWNUM,PNIMPORTO FROM "PntInput" WHERE PNSERIAL=this.w_PNSERIAL INTO CURSOR "CalTotPn"; 
 group by PNCODCON,PNTIPCON,PNCODBUN,PNINICOM,PNFINCOM,PNROWNUM
          SELECT SUM(PNIMPORTO) AS PNIMPORTO FROM "CalTotPn" INTO CURSOR "CalTotPn"
          SELECT "CalTotPn"
          GO TOP
          this.w_TOTIMPPN = CalTotPn.PNIMPORTO
          SELECT SUM(MRTOTIMP) AS MRTOTIMP FROM "PntInput" WHERE PNSERIAL=this.w_PNSERIAL INTO CURSOR "CalTotPn"
          SELECT "CalTotPn"
          GO TOP
          this.w_MRTOTPN = CalTotPn.MRTOTIMP
          USE IN "CalTotPn"
          if this.w_TOTIMPPN<>0
            if (this.w_PNSERIAL<>this.o_PNSERIAL and this.oParentObject.w_GPFLRGSC<>"S") or empty(this.o_PNSERIAL)
              this.o_PNSERIAL = this.w_PNSERIAL
              this.w_PNSERDES = SPACE(10)
              this.w_PNNUMRER = 0
              this.w_CPROWNUM = 0
              this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
              i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
              cp_NextTableProg(this, i_Conn, "SEPNT", "i_CODAZI,w_PNSERDES")
              cp_NextTableProg(this, i_Conn, "PRPNT", "i_CODAZI,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
              * --- se causale prevede intestatario lo valorizzo
              if this.oParentObject.w_CCFLRIFE<>"N"
                this.w_PNTIPCLF = PntIVA.PNTIPCLF
                this.w_PNCODCLF = PntIVA.PNCODCLF
              endif
              * --- Inserimento testata
              * --- creo sempre registrazioni provvisorie
              * --- Insert into PNT_MAST
              i_nConn=i_TableProp[this.PNT_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PNSERIAL"+",PNNUMRER"+",PNDATREG"+",PNCODESE"+",PNCOMPET"+",PNCODCAU"+",PNVALNAZ"+",PNCODVAL"+",PNCAOVAL"+",PNFLPROV"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",PNCODUTE"+",PNTIPREG"+",PNFLIVDF"+",PNTIPDOC"+",PNPRD"+",PNPRP"+",PNGRIVPR"+",PNDESSUP"+",PNTIPCLF"+",PNCODCLF"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PNSERDES),'PNT_MAST','PNSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCOMPET');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPCAUCON),'PNT_MAST','PNCODCAU');
                +","+cp_NullLink(cp_ToStrODBC(g_PERVAL),'PNT_MAST','PNVALNAZ');
                +","+cp_NullLink(cp_ToStrODBC(g_PERVAL),'PNT_MAST','PNCODVAL');
                +","+cp_NullLink(cp_ToStrODBC(g_CAOVAL),'PNT_MAST','PNCAOVAL');
                +","+cp_NullLink(cp_ToStrODBC("S"),'PNT_MAST','PNFLPROV');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','UTCC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','UTCV');
                +","+cp_NullLink(cp_ToStrODBC(i_datsys),'PNT_MAST','UTDC');
                +","+cp_NullLink(cp_ToStrODBC(i_datsys),'PNT_MAST','UTDV');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
                +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNTIPREG');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLIVDF');
                +","+cp_NullLink(cp_ToStrODBC("NO"),'PNT_MAST','PNTIPDOC');
                +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRD');
                +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRP');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPSERIAL),'PNT_MAST','PNGRIVPR');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PNT_MAST','PNTIPCLF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PNT_MAST','PNCODCLF');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERDES,'PNNUMRER',this.w_PNNUMRER,'PNDATREG',this.w_PNDATREG,'PNCODESE',this.w_PNCODESE,'PNCOMPET',this.w_PNCODESE,'PNCODCAU',this.oParentObject.w_GPCAUCON,'PNVALNAZ',g_PERVAL,'PNCODVAL',g_PERVAL,'PNCAOVAL',g_CAOVAL,'PNFLPROV',"S",'UTCC',this.w_PNCODUTE,'UTCV',this.w_PNCODUTE)
                insert into (i_cTable) (PNSERIAL,PNNUMRER,PNDATREG,PNCODESE,PNCOMPET,PNCODCAU,PNVALNAZ,PNCODVAL,PNCAOVAL,PNFLPROV,UTCC,UTCV,UTDC,UTDV,PNCODUTE,PNTIPREG,PNFLIVDF,PNTIPDOC,PNPRD,PNPRP,PNGRIVPR,PNDESSUP,PNTIPCLF,PNCODCLF &i_ccchkf. );
                   values (;
                     this.w_PNSERDES;
                     ,this.w_PNNUMRER;
                     ,this.w_PNDATREG;
                     ,this.w_PNCODESE;
                     ,this.w_PNCODESE;
                     ,this.oParentObject.w_GPCAUCON;
                     ,g_PERVAL;
                     ,g_PERVAL;
                     ,g_CAOVAL;
                     ,"S";
                     ,this.w_PNCODUTE;
                     ,this.w_PNCODUTE;
                     ,i_datsys;
                     ,i_datsys;
                     ,this.w_PNCODUTE;
                     ,"N";
                     ," ";
                     ,"NO";
                     ,"NN";
                     ,"NN";
                     ,this.oParentObject.w_GPSERIAL;
                     ,this.w_PNDESSUP;
                     ,this.w_PNTIPCLF;
                     ,this.w_PNCODCLF;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              this.oParentObject.w_NUMREG = this.oParentObject.w_NUMREG + 1
              this.w_TOTREG = 0
            endif
            SELECT "PntInput"
            SCAN FOR PNSERIAL=this.w_PNSERIAL
            this.w_PNTIPCON = ALLTRIM(PntInput.PNTIPCON )
            this.w_PNCODCON = ALLTRIM(PntInput.PNCODCON )
            this.w_PNCODBUN = ALLTRIM(PntInput.PNCODBUN )
            this.w_PNINICOM = PntInput.PNINICOM
            this.w_PNFINCOM = PntInput.PNFINCOM
            this.w_MRCODVOC = ALLTRIM(PntInput.MRCODVOC )
            this.w_MRCODICE = ALLTRIM(PntInput.MRCODICE )
            this.w_MRCODCOM = ALLTRIM(PntInput.MRCODCOM )
            this.w_MRINICOM = PntInput.MRINICOM
            this.w_MRFINCOM = PntInput.MRFINCOM
            this.w_MRTOTIMP = cp_ROUND( this.w_IVIMPIVA / this.w_MRTOTPN * PntInput.MRTOTIMP, g_PERPVL)
            this.w_MR_SEGNO = iif(this.w_MRTOTIMP>0,"D","A")
            this.w_MRTOTIMP = ABS(this.w_MRTOTIMP)
            this.w_PNIMPORTO = cp_ROUND( this.w_IVIMPIVA / this.w_TOTIMPPN * PntInput.PNIMPORTO, g_PERPVL)
            this.w_PNIMPDAR = IIF(this.w_PNIMPORTO>0, ABS(this.w_PNIMPORTO), 0)
            this.w_PNIMPAVE = IIF(this.w_PNIMPORTO<0, ABS(this.w_PNIMPORTO), 0)
            this.w_PNROWNUM = PntInput.PNROWNUM
            if this.w_PNIMPORTO<>0 and (this.w_PNCODCON<>this.o_CODICE OR this.w_PNTIPCON<>this.o_TIPCON OR this.w_PNCODBUN<>this.o_CODBUN OR this.w_PNINICOM<>this.o_PNINICOM OR this.w_PNFINCOM<>this.o_PNFINCOM OR this.w_PNROWNUM<>this.o_PNROWNUM)
              * --- Controllo squadratura ancalitica conto precedente
              if this.w_NUM <> 0 and this.w_TOTPNT - this.w_TOTANA <> 0
                * --- Write into MOVICOST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.MOVICOST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVICOST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MRTOTIMP =MRTOTIMP+ "+cp_ToStrODBC(this.w_TOTPNT - this.w_TOTANA);
                      +i_ccchkf ;
                  +" where ";
                      +"MRSERIAL = "+cp_ToStrODBC(this.w_PNSERDES);
                      +" and MRROWORD = "+cp_ToStrODBC(this.w_CPROWNUM);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUM);
                         )
                else
                  update (i_cTable) set;
                      MRTOTIMP = MRTOTIMP + this.w_TOTPNT - this.w_TOTANA;
                      &i_ccchkf. ;
                   where;
                      MRSERIAL = this.w_PNSERDES;
                      and MRROWORD = this.w_CPROWNUM;
                      and CPROWNUM = this.w_NUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              * --- Scrittura dettaglio
              this.w_TOTANA = 0
              this.w_TOTPNT = this.w_PNIMPDAR - this.w_PNIMPAVE
              this.w_TOTREG = this.w_TOTREG + this.w_PNIMPDAR - this.w_PNIMPAVE
              this.w_CPROWNUM = this.w_CPROWNUM + 1
              this.w_CPROWORD = this.w_CPROWNUM * 10
              this.w_NUM = 0
              * --- Scrive il Detail
              this.w_PNFLZERO = iif(this.w_PNIMPORTO=0,"S","N")
              * --- Insert into PNT_DETT
              i_nConn=i_TableProp[this.PNT_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNCAURIG"+",PNCODBUN"+",PNINICOM"+",PNFINCOM"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+",PNDESRIG"+",PNFLZERO"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PNSERDES),'PNT_DETT','PNSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
                +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_DETT','PNTIPCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
                +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPCAUCON),'PNT_DETT','PNCAURIG');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPBUSUNI),'PNT_DETT','PNCODBUN');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALD');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERDES,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',"G",'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',"N",'PNCAURIG',this.oParentObject.w_GPCAUCON,'PNCODBUN',this.oParentObject.w_GPBUSUNI,'PNINICOM',this.w_PNINICOM,'PNFINCOM',this.w_PNFINCOM)
                insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNCAURIG,PNCODBUN,PNINICOM,PNFINCOM,PNFLSALD,PNFLSALI,PNFLSALF,PNDESRIG,PNFLZERO &i_ccchkf. );
                   values (;
                     this.w_PNSERDES;
                     ,this.w_CPROWNUM;
                     ,this.w_CPROWORD;
                     ,"G";
                     ,this.w_PNCODCON;
                     ,this.w_PNIMPDAR;
                     ,this.w_PNIMPAVE;
                     ,"N";
                     ,this.oParentObject.w_GPCAUCON;
                     ,this.oParentObject.w_GPBUSUNI;
                     ,this.w_PNINICOM;
                     ,this.w_PNFINCOM;
                     ," ";
                     ," ";
                     ," ";
                     ,this.w_PNDESRIG;
                     ,this.w_PNFLZERO;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              this.o_CODICE = this.w_PNCODCON
              this.o_TIPCON = this.w_PNTIPCON
              this.o_PNINICOM = this.w_PNINICOM
              this.o_PNFINCOM = this.w_PNFINCOM
              this.o_CODBUN = this.w_PNCODBUN
              this.o_PNROWNUM = this.w_PNROWNUM
              if this.w_PNTIPCON="G" AND g_PERCCR="S" AND this.oParentObject.w_CCFLANAL="S"
                * --- Read from CONTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ANCONSUP,ANCCTAGG"+;
                    " from "+i_cTable+" CONTI where ";
                        +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                        +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ANCONSUP,ANCCTAGG;
                    from (i_cTable) where;
                        ANTIPCON = this.w_PNTIPCON;
                        and ANCODICE = this.w_PNCODCON;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
                  this.w_CCTAGG = NVL(cp_ToDate(_read_.ANCCTAGG),cp_NullValue(_read_.ANCCTAGG))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              if this.oParentObject.w_GPSTORNO="C" AND g_PERCCR="S" AND this.oParentObject.w_CCFLANAL="S" AND this.w_CCTAGG $ "AM" AND (this.oParentObject.w_FLBUANAL="S" OR g_PERBUN="N") and this.w_AnaliticaContoStorno
                * --- Se storno su conto specifico recupero i dati del conto indicato
                *     e valuto se gestice e ha l'analitica inserita, se gestisce l'analitica ma
                *     non � indicata allora setto la generazione in modo che prelevi 
                *     l'eventuale analitica specificata nel movimento origine ma forzi
                *     il conto di storno sulle righe
                * --- Lancio routine per valorizzare dettaglio analitica
                GSAR_BRA(this,this.w_PNSERDES, this.w_CPROWNUM, "G", this.w_PNCODCON, abs(this.w_PNIMPDAR-this.w_PNIMPAVE))
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
            if (this.oParentObject.w_GPSTORNO="M" OR (this.oParentObject.w_GPSTORNO="C" and not this.w_AnaliticaContoStorno)) AND this.w_PNTIPCON="G" AND g_PERCCR="S" AND this.oParentObject.w_CCFLANAL="S" AND this.w_CCTAGG $ "AM" AND (this.oParentObject.w_FLBUANAL="S" OR g_PERBUN="N")
              if NOT EMPTY(this.w_MRCODICE)
                this.w_TOTANA = this.w_TOTANA + iif(this.w_MR_SEGNO="D",1,-1)*this.w_MRTOTIMP
                this.w_NUM = this.w_NUM + 1
                this.w_PARAMETRO = cp_ROUND(this.w_MRTOTIMP*IIF(this.w_MR_SEGNO="A",-1,1)/this.w_PNIMPORTO, 4)
                * --- Aggiorna Analitica
                * --- Try
                local bErr_0498EAC0
                bErr_0498EAC0=bTrsErr
                this.Try_0498EAC0()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_0498EAC0
                * --- End
              endif
            endif
            ENDSCAN
            if this.w_NUM <> 0 and this.w_TOTPNT - this.w_TOTANA <> 0
              * --- Write into MOVICOST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MOVICOST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVICOST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MRTOTIMP =MRTOTIMP- "+cp_ToStrODBC(this.w_TOTANA - this.w_TOTPNT);
                    +i_ccchkf ;
                +" where ";
                    +"MRSERIAL = "+cp_ToStrODBC(this.w_PNSERDES);
                    +" and MRROWORD = "+cp_ToStrODBC(this.w_CPROWNUM);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUM);
                       )
              else
                update (i_cTable) set;
                    MRTOTIMP = MRTOTIMP - this.w_TOTANA - this.w_TOTPNT;
                    &i_ccchkf. ;
                 where;
                    MRSERIAL = this.w_PNSERDES;
                    and MRROWORD = this.w_CPROWNUM;
                    and CPROWNUM = this.w_NUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            * --- inserisco contropartita solo se ho almeno un costo
            if this.w_CPROWNUM > 0
              this.w_CPROWNUM = this.w_CPROWNUM + 1
              this.w_CPROWORD = this.w_CPROWNUM * 10
              this.w_PNIMPDAR = iif(this.w_IVIMPIVA<0,abs(this.w_IVIMPIVA),0)
              this.w_PNIMPAVE = iif(this.w_IVIMPIVA>0,abs(this.w_IVIMPIVA),0)
              this.w_TOTREG = this.w_TOTREG + this.w_PNIMPDAR - this.w_PNIMPAVE
              * --- Insert into PNT_DETT
              i_nConn=i_TableProp[this.PNT_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNCAURIG"+",PNCODBUN"+",PNINICOM"+",PNFINCOM"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+",PNDESRIG"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PNSERDES),'PNT_DETT','PNSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
                +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_DETT','PNTIPCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCONTRO),'PNT_DETT','PNCODCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
                +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
                +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLZERO');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPCAUCON),'PNT_DETT','PNCAURIG');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPBUSUNI),'PNT_DETT','PNCODBUN');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALD');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERDES,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',"G",'PNCODCON',this.w_PNCONTRO,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',"N",'PNFLZERO',"N",'PNCAURIG',this.oParentObject.w_GPCAUCON,'PNCODBUN',this.oParentObject.w_GPBUSUNI,'PNINICOM',this.w_PNINICOM)
                insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNCAURIG,PNCODBUN,PNINICOM,PNFINCOM,PNFLSALD,PNFLSALI,PNFLSALF,PNDESRIG &i_ccchkf. );
                   values (;
                     this.w_PNSERDES;
                     ,this.w_CPROWNUM;
                     ,this.w_CPROWORD;
                     ,"G";
                     ,this.w_PNCONTRO;
                     ,this.w_PNIMPDAR;
                     ,this.w_PNIMPAVE;
                     ,"N";
                     ,"N";
                     ,this.oParentObject.w_GPCAUCON;
                     ,this.oParentObject.w_GPBUSUNI;
                     ,this.w_PNINICOM;
                     ,this.w_PNFINCOM;
                     ," ";
                     ," ";
                     ," ";
                     ,this.w_PNDESRIG;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              if cp_round(this.w_TOTREG,6)<>0
                if Not EMPTY(this.w_CONARR) and this.w_TOTREG<>0
                  this.w_CPROWNUM = this.w_CPROWNUM + 1
                  this.w_CPROWORD = this.w_CPROWNUM * 10
                  this.w_PNCODCON = this.w_CONARR
                  this.w_PNIMPDAR = iif(this.w_TOTREG<0,abs(this.w_TOTREG),0)
                  this.w_PNIMPAVE = iif(this.w_TOTREG>0,abs(this.w_TOTREG),0)
                  * --- Insert into PNT_DETT
                  i_nConn=i_TableProp[this.PNT_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNCAURIG"+",PNCODBUN"+",PNINICOM"+",PNFINCOM"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+",PNDESRIG"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PNSERDES),'PNT_DETT','PNSERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
                    +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_DETT','PNTIPCON');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
                    +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLZERO');
                    +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPCAUCON),'PNT_DETT','PNCAURIG');
                    +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPBUSUNI),'PNT_DETT','PNCODBUN');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALD');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERDES,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',"G",'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',"N",'PNFLZERO',"N",'PNCAURIG',this.oParentObject.w_GPCAUCON,'PNCODBUN',this.oParentObject.w_GPBUSUNI,'PNINICOM',this.w_PNINICOM)
                    insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNCAURIG,PNCODBUN,PNINICOM,PNFINCOM,PNFLSALD,PNFLSALI,PNFLSALF,PNDESRIG &i_ccchkf. );
                       values (;
                         this.w_PNSERDES;
                         ,this.w_CPROWNUM;
                         ,this.w_CPROWORD;
                         ,"G";
                         ,this.w_PNCODCON;
                         ,this.w_PNIMPDAR;
                         ,this.w_PNIMPAVE;
                         ,"N";
                         ,"N";
                         ,this.oParentObject.w_GPCAUCON;
                         ,this.oParentObject.w_GPBUSUNI;
                         ,this.w_PNINICOM;
                         ,this.w_PNFINCOM;
                         ," ";
                         ," ";
                         ," ";
                         ,this.w_PNDESRIG;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                  this.w_oERRORLOG.AddMsgLog("Verificare Reg.N.: %1 del.: %2",ALLTRIM(STR(this.w_PNNUMRER,15)),DTOC(this.w_PNDATREG))     
                  this.w_oERRORLOG.AddMsgLog("Inserito conto arrotondamento [%1] per un importo pari a. %2",Alltrim(this.w_CONARR),Alltrim(str(ABS(this.w_TOTREG),18,4)))     
                endif
              endif
            else
              this.oParentObject.w_NUMREG = this.oParentObject.w_NUMREG - 1
              * --- Delete from PNT_MAST
              i_nConn=i_TableProp[this.PNT_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERDES);
                       )
              else
                delete from (i_cTable) where;
                      PNSERIAL = this.w_PNSERDES;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
            endif
          endif
          ENDSCAN
          if this.oParentObject.w_NUMREG=0
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg="Non ci sono dati da elaborare"
          endif
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Segnalazioni in fase di elaborazione")     
        endif
      case this.pOper=="DELET"
        * --- Elimina nei dettagli i riferimenti alle righe di primanota della generazione scritture di assestamento
        if (this.oParentObject.w_GPFLDATD="S" AND this.oParentObject.w_CONCON>=this.oParentObject.w_GPREGFIN) OR (this.oParentObject.w_GPFLDATD<>"S" AND this.oParentObject.w_CONCON>=this.oParentObject.w_GPDATGEN)
          this.w_MESS = AH_MsgFormat("Data registrazione inferiore o uguale alla data di consolidamento o alla data di stampa del libro giornale%0Cancellazione annullata")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
        endif
        this.w_MESS = "La cancellazione della generazione eliminer� anche tutte le registrazioni di primanota associate se sono provvisorie%0Confermi la cancellazione?"
        if ah_YesNo(this.w_MESS)
          VQ_EXEC("QUERY\GSCGABGP",this,"CursDelete")
          SELECT CursDelete
          GO TOP
          COUNT FOR PNFLPROV<>"S" TO w_COUNT
          if w_COUNT = 0
            * --- Try
            local bErr_04A08630
            bErr_04A08630=bTrsErr
            this.Try_04A08630()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              this.w_MESS = AH_MsgFormat("Transazione abbandonata%0Impossibile cancellare alcune registrazioni di primanota")
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=this.w_MESS
            endif
            bTrsErr=bTrsErr or bErr_04A08630
            * --- End
          else
            this.w_MESS = AH_MsgFormat("Transazione abbandonata%0Impossibile cancellare generazione perch� esistono riferimenti a registrazioni confermate")
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
        else
          this.w_MESS = AH_MsgFormat("Transazione abbandonata%0Cancellazione annullata")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pOper=="RECINS"
        this.w_oERRORLOG=createobject("AH_ErrorLog")
        this.w_STAMPAERR = .f.
        this.w_MESS = "Operazione completata.%0%1 registrazioni generate"
        ah_ErrorMsg(this.w_MESS,,,alltrim(str(this.oParentObject.w_NUMREG)))
        * --- Create temporary table TMPPNT_MAST
        i_nIdx=cp_AddTableDef('TMPPNT_MAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_FIDUHRRJLB[1]
        indexes_FIDUHRRJLB[1]='PNSERIAL'
        vq_exec('gscg1bgp',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_FIDUHRRJLB,.f.)
        this.TMPPNT_MAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        vq_exec("query\gscgBbgp", this, "PntInput")
        * --- Drop temporary table TMPPNT_MAST
        i_nIdx=cp_GetTableDefIdx('TMPPNT_MAST')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPPNT_MAST')
        endif
        if USED("PntInput") and RECCOUNT("PntInput")>0
          Select PntInput 
 SCAN
          this.w_oERRORLOG.AddMsgLog("Verificare dati di analitica reg. %1 del %2 riga %3 %4",ALLTRIM(STR(PNNUMRER))+"/"+ALLTRIM(STR(PNCODUTE))+"("+ALLTRIM(PNCOMPET)+")",DTOC(PNDATREG),alltrim(str(CPROWORD)), alltrim(pncodcon)+" "+alltrim(andescri))     
          this.w_oERRORLOG.AddMsgLog("Provvedere in modalit� manuale")     
          Select PntInput
          ENDSCAN
          this.w_STAMPAERR = .t.
        endif
        vq_exec("query\gscg5bgp", this, "PntInput")
        if USED("PntInput") and RECCOUNT("PntInput")>0
          Select PntInput 
 SCAN
          this.w_oERRORLOG=createobject("AH_ErrorLog")
          this.w_oERRORLOG.AddMsgLog("Verificare la reg. %1 del %2: sono presenti conti imputati a business unit diverse",ALLTRIM(STR(PNNUMRER))+"/"+ALLTRIM(STR(PNCODUTE))+"("+ALLTRIM(PNCOMPET)+")",DTOC(PNDATREG))     
          Select PntInput
          ENDSCAN
          this.w_STAMPAERR = .t.
        endif
        if this.w_STAMPAERR
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Segnalazioni in fase di elaborazione")     
        endif
    endcase
    this.w_oERRORLOG = NULL
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_0498EAC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOVICOST
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVICOST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MRSERIAL"+",MRROWORD"+",CPROWNUM"+",MRCODVOC"+",MRCODICE"+",MRCODCOM"+",MR_SEGNO"+",MRTOTIMP"+",MRINICOM"+",MRFINCOM"+",MRFLRIPA"+",MRPARAME"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERDES),'MOVICOST','MRSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MOVICOST','MRROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUM),'MOVICOST','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRCODVOC),'MOVICOST','MRCODVOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRCODICE),'MOVICOST','MRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRCODCOM),'MOVICOST','MRCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MR_SEGNO),'MOVICOST','MR_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRTOTIMP),'MOVICOST','MRTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRINICOM),'MOVICOST','MRINICOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRFINCOM),'MOVICOST','MRFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(" "),'MOVICOST','MRFLRIPA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PARAMETRO),'MOVICOST','MRPARAME');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',this.w_PNSERDES,'MRROWORD',this.w_CPROWNUM,'CPROWNUM',this.w_NUM,'MRCODVOC',this.w_MRCODVOC,'MRCODICE',this.w_MRCODICE,'MRCODCOM',this.w_MRCODCOM,'MR_SEGNO',this.w_MR_SEGNO,'MRTOTIMP',this.w_MRTOTIMP,'MRINICOM',this.w_MRINICOM,'MRFINCOM',this.w_MRFINCOM,'MRFLRIPA'," ",'MRPARAME',this.w_PARAMETRO)
      insert into (i_cTable) (MRSERIAL,MRROWORD,CPROWNUM,MRCODVOC,MRCODICE,MRCODCOM,MR_SEGNO,MRTOTIMP,MRINICOM,MRFINCOM,MRFLRIPA,MRPARAME &i_ccchkf. );
         values (;
           this.w_PNSERDES;
           ,this.w_CPROWNUM;
           ,this.w_NUM;
           ,this.w_MRCODVOC;
           ,this.w_MRCODICE;
           ,this.w_MRCODCOM;
           ,this.w_MR_SEGNO;
           ,this.w_MRTOTIMP;
           ,this.w_MRINICOM;
           ,this.w_MRFINCOM;
           ," ";
           ,this.w_PARAMETRO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04A08630()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    SELECT CursDelete
    GO TOP
    SCAN
    this.w_PNSERIAL = ASRIFEPN
    * --- Delete from MOVICOST
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
            +" and MRROWORD > "+cp_ToStrODBC(0);
             )
    else
      delete from (i_cTable) where;
            MRSERIAL = this.w_PNSERIAL;
            and MRROWORD > 0;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
             )
    else
      delete from (i_cTable) where;
            PNSERIAL = this.w_PNSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
             )
    else
      delete from (i_cTable) where;
            PNSERIAL = this.w_PNSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    ENDSCAN
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED("PntIVA")
      Select PntIVA 
 Use
    endif
    if USED("PntInput")
      Select PntInput 
 Use
    endif
    if USED("CursDelete")
      Select CursDelete 
 Use
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='PNT_DETT'
    this.cWorkTables[4]='MOVICOST'
    this.cWorkTables[5]='MASTRI'
    this.cWorkTables[6]='BUSIUNIT'
    this.cWorkTables[7]='SALDICON'
    this.cWorkTables[8]='*TMPPNT_MAST'
    this.cWorkTables[9]='AZIENDA'
    this.cWorkTables[10]='*TMPTPNT_DETT'
    this.cWorkTables[11]='CONTROPA'
    return(this.OpenAllTables(11))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
