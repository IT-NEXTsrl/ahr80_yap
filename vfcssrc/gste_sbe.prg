* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_sbe                                                        *
*              Stampa brogliaccio effetti/bonifici                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_133]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-16                                                      *
* Last revis.: 2012-08-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_sbe",oParentObject))

* --- Class definition
define class tgste_sbe as StdForm
  Top    = 13
  Left   = 10

  * --- Standard Properties
  Width  = 598
  Height = 466
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-08-27"
  HelpContextID=107623529
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=45

  * --- Constant Properties
  _IDX = 0
  BAN_CHE_IDX = 0
  DIS_TINT_IDX = 0
  CONTI_IDX = 0
  COC_MAST_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gste_sbe"
  cComment = "Stampa brogliaccio effetti/bonifici"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_SCAINI = ctod('  /  /  ')
  o_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_NDOINI = 0
  w_ADOINI = space(10)
  w_NDOFIN = 0
  w_ADOFIN = space(10)
  w_CODBAN = space(10)
  w_DESBAN = space(50)
  w_DINUMERO = 0
  o_DINUMERO = 0
  w_NUMERO = 0
  w_DATDIS = ctod('  /  /  ')
  w_RIFDIS = space(10)
  w_CODCON = space(15)
  w_DESCON = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_EFPRES = space(1)
  w_EFCONT = space(1)
  w_ORDINA = space(1)
  w_DINUMDIS = space(10)
  w_DIRIFCON = space(10)
  w_FLDEFI = space(1)
  w_CONTAB = space(1)
  w_VALORE = space(1)
  w_NUMPAR = space(14)
  w_PAGRD = space(2)
  w_PAGBO = space(2)
  w_PAGRB = space(2)
  w_PAGRI = space(2)
  w_PAGCA = space(2)
  w_PAGMA = space(2)
  w_FLSOSP = space(1)
  w_FLGSOS = space(1)
  w_FLGNSO = space(1)
  w_TIPCON = space(1)
  w_VALUTA = space(3)
  o_VALUTA = space(3)
  w_FLRAGG = space(1)
  w_CODVAL = space(3)
  w_CATPAG = space(2)
  w_GPRIFDIS = space(10)
  w_TIPSBF = space(1)
  w_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  w_REGINI = ctod('  /  /  ')
  w_REGFIN = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_sbePag1","gste_sbe",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCAINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='BAN_CHE'
    this.cWorkTables[2]='DIS_TINT'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='COC_MAST'
    this.cWorkTables[5]='VALUTE'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_NDOINI=0
      .w_ADOINI=space(10)
      .w_NDOFIN=0
      .w_ADOFIN=space(10)
      .w_CODBAN=space(10)
      .w_DESBAN=space(50)
      .w_DINUMERO=0
      .w_NUMERO=0
      .w_DATDIS=ctod("  /  /  ")
      .w_RIFDIS=space(10)
      .w_CODCON=space(15)
      .w_DESCON=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_EFPRES=space(1)
      .w_EFCONT=space(1)
      .w_ORDINA=space(1)
      .w_DINUMDIS=space(10)
      .w_DIRIFCON=space(10)
      .w_FLDEFI=space(1)
      .w_CONTAB=space(1)
      .w_VALORE=space(1)
      .w_NUMPAR=space(14)
      .w_PAGRD=space(2)
      .w_PAGBO=space(2)
      .w_PAGRB=space(2)
      .w_PAGRI=space(2)
      .w_PAGCA=space(2)
      .w_PAGMA=space(2)
      .w_FLSOSP=space(1)
      .w_FLGSOS=space(1)
      .w_FLGNSO=space(1)
      .w_TIPCON=space(1)
      .w_VALUTA=space(3)
      .w_FLRAGG=space(1)
      .w_CODVAL=space(3)
      .w_CATPAG=space(2)
      .w_GPRIFDIS=space(10)
      .w_TIPSBF=space(1)
      .w_DOCINI=ctod("  /  /  ")
      .w_DOCFIN=ctod("  /  /  ")
      .w_REGINI=ctod("  /  /  ")
      .w_REGFIN=ctod("  /  /  ")
        .w_SCAINI = i_datsys
        .w_SCAFIN = i_datsys+365
        .DoRTCalc(3,7,.f.)
        if not(empty(.w_CODBAN))
          .link_1_18('Full')
        endif
          .DoRTCalc(8,9,.f.)
        .w_NUMERO = .w_DINUMERO
        .w_DATDIS = iif(.w_DINUMERO=0,cp_CharToDate('  -  -  '),.w_DATDIS)
        .DoRTCalc(12,13,.f.)
        if not(empty(.w_CODCON))
          .link_1_29('Full')
        endif
          .DoRTCalc(14,15,.f.)
        .w_OBTEST = iif(empty(.w_SCAINI),i_INIDAT,.w_SCAINI)
        .w_EFPRES = 'T'
        .w_EFCONT = 'T'
        .w_ORDINA = 'N'
          .DoRTCalc(20,21,.f.)
        .w_FLDEFI = IIF(.w_EFPRES='T', ' ',.w_EFPRES)
        .w_CONTAB = IIF(.w_EFCONT='T','T',.w_EFCONT)
        .w_VALORE = IIF(EMPTY(.w_DINUMERO),'T','S')
          .DoRTCalc(25,25,.f.)
        .w_PAGRD = 'XX'
        .w_PAGBO = 'BO'
        .w_PAGRB = 'RB'
        .w_PAGRI = 'RI'
        .w_PAGCA = 'CA'
        .w_PAGMA = 'MA'
        .w_FLSOSP = 'T'
        .w_FLGSOS = IIF(.w_FLSOSP $ 'TN', ' ', 'S')
        .w_FLGNSO = IIF(.w_FLSOSP $ 'TS', ' ', 'S')
        .w_TIPCON = 'C'
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_VALUTA))
          .link_1_63('Full')
        endif
        .w_FLRAGG = 'S'
        .w_CODVAL = .w_VALUTA
    endwith
    this.DoRTCalc(39,45,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_80.enabled = this.oPgFrm.Page1.oPag.oBtn_1_80.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_82.enabled = this.oPgFrm.Page1.oPag.oBtn_1_82.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
        if .o_DINUMERO<>.w_DINUMERO
            .w_NUMERO = .w_DINUMERO
        endif
        if .o_DINUMERO<>.w_DINUMERO
            .w_DATDIS = iif(.w_DINUMERO=0,cp_CharToDate('  -  -  '),.w_DATDIS)
        endif
        .DoRTCalc(12,15,.t.)
        if .o_SCAINI<>.w_SCAINI
            .w_OBTEST = iif(empty(.w_SCAINI),i_INIDAT,.w_SCAINI)
        endif
        .DoRTCalc(17,21,.t.)
            .w_FLDEFI = IIF(.w_EFPRES='T', ' ',.w_EFPRES)
            .w_CONTAB = IIF(.w_EFCONT='T','T',.w_EFCONT)
        if .o_DINUMERO<>.w_DINUMERO
            .w_VALORE = IIF(EMPTY(.w_DINUMERO),'T','S')
        endif
        .DoRTCalc(25,32,.t.)
            .w_FLGSOS = IIF(.w_FLSOSP $ 'TN', ' ', 'S')
            .w_FLGNSO = IIF(.w_FLSOSP $ 'TS', ' ', 'S')
        .DoRTCalc(35,37,.t.)
        if .o_VALUTA<>.w_VALUTA
            .w_CODVAL = .w_VALUTA
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(39,45,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODBAN
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CODBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CODBAN))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODBAN) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCODBAN_1_18'),i_cWhere,'GSTE_ACB',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CODBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CODBAN)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBAN = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODBAN = space(10)
      endif
      this.w_DESBAN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_29'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'GSTE_KMS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VALUTA))
          select VACODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VALUTA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"  like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+"  like "+cp_ToStr(trim(this.w_VALUTA)+"%");

            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_VALUTA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVALUTA_1_63'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCAINI_1_3.value==this.w_SCAINI)
      this.oPgFrm.Page1.oPag.oSCAINI_1_3.value=this.w_SCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAFIN_1_4.value==this.w_SCAFIN)
      this.oPgFrm.Page1.oPag.oSCAFIN_1_4.value=this.w_SCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNDOINI_1_7.value==this.w_NDOINI)
      this.oPgFrm.Page1.oPag.oNDOINI_1_7.value=this.w_NDOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oADOINI_1_8.value==this.w_ADOINI)
      this.oPgFrm.Page1.oPag.oADOINI_1_8.value=this.w_ADOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNDOFIN_1_9.value==this.w_NDOFIN)
      this.oPgFrm.Page1.oPag.oNDOFIN_1_9.value=this.w_NDOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oADOFIN_1_10.value==this.w_ADOFIN)
      this.oPgFrm.Page1.oPag.oADOFIN_1_10.value=this.w_ADOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODBAN_1_18.value==this.w_CODBAN)
      this.oPgFrm.Page1.oPag.oCODBAN_1_18.value=this.w_CODBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBAN_1_19.value==this.w_DESBAN)
      this.oPgFrm.Page1.oPag.oDESBAN_1_19.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDINUMERO_1_21.value==this.w_DINUMERO)
      this.oPgFrm.Page1.oPag.oDINUMERO_1_21.value=this.w_DINUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIS_1_23.value==this.w_DATDIS)
      this.oPgFrm.Page1.oPag.oDATDIS_1_23.value=this.w_DATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_29.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_29.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_30.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_30.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oEFPRES_1_36.RadioValue()==this.w_EFPRES)
      this.oPgFrm.Page1.oPag.oEFPRES_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEFCONT_1_39.RadioValue()==this.w_EFCONT)
      this.oPgFrm.Page1.oPag.oEFCONT_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oORDINA_1_41.RadioValue()==this.w_ORDINA)
      this.oPgFrm.Page1.oPag.oORDINA_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRD_1_49.RadioValue()==this.w_PAGRD)
      this.oPgFrm.Page1.oPag.oPAGRD_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGBO_1_50.RadioValue()==this.w_PAGBO)
      this.oPgFrm.Page1.oPag.oPAGBO_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRB_1_51.RadioValue()==this.w_PAGRB)
      this.oPgFrm.Page1.oPag.oPAGRB_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRI_1_52.RadioValue()==this.w_PAGRI)
      this.oPgFrm.Page1.oPag.oPAGRI_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGCA_1_53.RadioValue()==this.w_PAGCA)
      this.oPgFrm.Page1.oPag.oPAGCA_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGMA_1_54.RadioValue()==this.w_PAGMA)
      this.oPgFrm.Page1.oPag.oPAGMA_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSOSP_1_55.RadioValue()==this.w_FLSOSP)
      this.oPgFrm.Page1.oPag.oFLSOSP_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_59.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_63.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_63.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLRAGG_1_65.RadioValue()==this.w_FLRAGG)
      this.oPgFrm.Page1.oPag.oFLRAGG_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_73.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_73.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_74.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_74.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oREGINI_1_77.value==this.w_REGINI)
      this.oPgFrm.Page1.oPag.oREGINI_1_77.value=this.w_REGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oREGFIN_1_78.value==this.w_REGFIN)
      this.oPgFrm.Page1.oPag.oREGFIN_1_78.value=this.w_REGFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_scaini<=.w_scafin) or (empty(.w_scafin)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La scadenza iniziale � maggiore della scadenza finale")
          case   not((.w_scaini<=.w_scafin) or (empty(.w_scaini)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La scadenza iniziale � maggiore della scadenza finale")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   (empty(.w_EFPRES))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFPRES_1_36.SetFocus()
            i_bnoObbl = !empty(.w_EFPRES)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EFCONT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEFCONT_1_39.SetFocus()
            i_bnoObbl = !empty(.w_EFCONT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ORDINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORDINA_1_41.SetFocus()
            i_bnoObbl = !empty(.w_ORDINA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_docini<=.w_docfin) or (empty(.w_docfin)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCINI_1_73.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data documento iniziale � maggiore della data documento finale")
          case   not((.w_docini<=.w_docfin) or (empty(.w_docini)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCFIN_1_74.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data documento iniziale � maggiore della data documento finale")
          case   not((.w_regini<=.w_regfin) or (empty(.w_regfin)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oREGINI_1_77.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data registrazione iniziale � maggiore della data registrazione finale")
          case   not((.w_regini<=.w_regfin) or (empty(.w_regini)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oREGFIN_1_78.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data registrazione iniziale � maggiore della data registrazione finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SCAINI = this.w_SCAINI
    this.o_DINUMERO = this.w_DINUMERO
    this.o_VALUTA = this.w_VALUTA
    return

enddefine

* --- Define pages as container
define class tgste_sbePag1 as StdContainer
  Width  = 594
  height = 466
  stdWidth  = 594
  stdheight = 466
  resizeXpos=287
  resizeYpos=163
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCAINI_1_3 as StdField with uid="BUACHACMJF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_SCAINI", cQueryName = "SCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La scadenza iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data iniziale scadenza da ricercare",;
    HelpContextID = 130229286,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=365, Top=34

  func oSCAINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_scaini<=.w_scafin) or (empty(.w_scafin)))
    endwith
    return bRes
  endfunc

  add object oSCAFIN_1_4 as StdField with uid="AVZCQICAXG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SCAFIN", cQueryName = "SCAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La scadenza iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data finale scadenza da ricercare",;
    HelpContextID = 208675878,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=365, Top=59

  func oSCAFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_scaini<=.w_scafin) or (empty(.w_scaini)))
    endwith
    return bRes
  endfunc

  add object oNDOINI_1_7 as StdField with uid="MFFRYELHLU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NDOINI", cQueryName = "NDOINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 130286806,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=365, Top=85, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOINI_1_8 as StdField with uid="LFYMWAETPI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ADOINI", cQueryName = "ADOINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di inizio selezione",;
    HelpContextID = 130286598,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=502, Top=85, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oNDOFIN_1_9 as StdField with uid="YVXOAZLBNP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NDOFIN", cQueryName = "NDOFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 208733398,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=365, Top=110, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOFIN_1_10 as StdField with uid="WVYMVOQZXQ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ADOFIN", cQueryName = "ADOFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di fine selezione",;
    HelpContextID = 208733190,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=502, Top=110, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oCODBAN_1_18 as StdField with uid="WLHRYQEVPD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODBAN", cQueryName = "CODBAN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice banca di presentazione selezionata",;
    HelpContextID = 200040230,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=98, Top=187, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CODBAN"

  func oCODBAN_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBAN_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBAN_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCODBAN_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenco banche",'',this.parent.oContained
  endproc
  proc oCODBAN_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CODBAN
     i_obj.ecpSave()
  endproc

  add object oDESBAN_1_19 as StdField with uid="KPJIAELGFX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 200099126,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=222, Top=187, InputMask=replicate('X',50)

  add object oDINUMERO_1_21 as StdField with uid="XAAECVNHKW",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DINUMERO", cQueryName = "DINUMERO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero distinta selezionata",;
    HelpContextID = 62912901,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=281, Top=216, cSayPict="'999999'", cGetPict="'999999'"

  proc oDINUMERO_1_21.mAfter
    do GSTE_KDS with this.Parent.oContained
  endproc

  add object oDATDIS_1_23 as StdField with uid="ORPWORPKCP",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATDIS", cQueryName = "DATDIS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data distinta selezionata",;
    HelpContextID = 24072502,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=509, Top=215


  add object oBtn_1_24 as StdButton with uid="CQEHICTOFE",left=365, top=217, width=19,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 107422506;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      do GSTE_KDS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODCON_1_29 as StdField with uid="YISHERJLIQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    ToolTipText = "Codice cliente/fornitore selezionato",;
    HelpContextID = 214785830,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=80, Top=264, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'GSTE_KMS.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_30 as StdField with uid="ZJVLBYVJUK",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 214844726,;
   bGlobalFont=.t.,;
    Height=21, Width=381, Left=204, Top=264, InputMask=replicate('X',40)

  add object oEFPRES_1_36 as StdRadio with uid="DUNJAICEVC",rtseq=17,rtrep=.f.,left=136, top=318, width=145,height=23;
    , ToolTipText = "S�: effetti non in distinta; no: effetti in distinta; tutti: entrambi";
    , cFormVar="w_EFPRES", ButtonCount=3, bObbl=.t., nPag=1;
  , bGlobalFont=.t.

    proc oEFPRES_1_36.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Si"
      this.Buttons(1).HelpContextID = 20780614
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Si","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="No"
      this.Buttons(2).HelpContextID = 20780614
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("No","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Tutti"
      this.Buttons(3).HelpContextID = 20780614
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "S�: effetti non in distinta; no: effetti in distinta; tutti: entrambi")
      StdRadio::init()
    endproc

  func oEFPRES_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oEFPRES_1_36.GetRadio()
    this.Parent.oContained.w_EFPRES = this.RadioValue()
    return .t.
  endfunc

  func oEFPRES_1_36.SetRadio()
    this.Parent.oContained.w_EFPRES=trim(this.Parent.oContained.w_EFPRES)
    this.value = ;
      iif(this.Parent.oContained.w_EFPRES=='S',1,;
      iif(this.Parent.oContained.w_EFPRES=='N',2,;
      iif(this.Parent.oContained.w_EFPRES=='T',3,;
      0)))
  endfunc

  add object oEFCONT_1_39 as StdRadio with uid="HYKEFONHSC",rtseq=18,rtrep=.f.,left=136, top=334, width=145,height=23;
    , ToolTipText = "S�: effetti contabilizzati e indiretta effetti; no: effetti da contabilizzare e al dopo incasso; tutti: entrambi";
    , cFormVar="w_EFCONT", ButtonCount=3, bObbl=.t., nPag=1;
  , bGlobalFont=.t.

    proc oEFCONT_1_39.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Si"
      this.Buttons(1).HelpContextID = 46745158
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Si","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="No"
      this.Buttons(2).HelpContextID = 46745158
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("No","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.Buttons(3).Caption="Tutti"
      this.Buttons(3).HelpContextID = 46745158
      this.Buttons(3).Left=i_coord
      this.Buttons(3).Width=(TxtWidth("Tutti","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(3).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "S�: effetti contabilizzati e indiretta effetti; no: effetti da contabilizzare e al dopo incasso; tutti: entrambi")
      StdRadio::init()
    endproc

  func oEFCONT_1_39.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oEFCONT_1_39.GetRadio()
    this.Parent.oContained.w_EFCONT = this.RadioValue()
    return .t.
  endfunc

  func oEFCONT_1_39.SetRadio()
    this.Parent.oContained.w_EFCONT=trim(this.Parent.oContained.w_EFCONT)
    this.value = ;
      iif(this.Parent.oContained.w_EFCONT=='S',1,;
      iif(this.Parent.oContained.w_EFCONT=='N',2,;
      iif(this.Parent.oContained.w_EFCONT=='T',3,;
      0)))
  endfunc

  add object oORDINA_1_41 as StdRadio with uid="LZPUCDGQOR",rtseq=19,rtrep=.f.,left=431, top=318, width=156,height=66;
    , ToolTipText = "Seleziona il tipo di ordinamento della stampa";
    , cFormVar="w_ORDINA", ButtonCount=4, bObbl=.t., nPag=1;
  , bGlobalFont=.t.

    proc oORDINA_1_41.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Data scadenza"
      this.Buttons(1).HelpContextID = 3972378
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Banca di presentazione"
      this.Buttons(2).HelpContextID = 3972378
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Ragione sociale"
      this.Buttons(3).HelpContextID = 3972378
      this.Buttons(3).Top=32
      this.Buttons(4).Caption="Codice cliente/fornitore"
      this.Buttons(4).HelpContextID = 3972378
      this.Buttons(4).Top=48
      this.SetAll("Width",154)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona il tipo di ordinamento della stampa")
      StdRadio::init()
    endproc

  func oORDINA_1_41.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'R',;
    iif(this.value =4,'C',;
    space(1))))))
  endfunc
  func oORDINA_1_41.GetRadio()
    this.Parent.oContained.w_ORDINA = this.RadioValue()
    return .t.
  endfunc

  func oORDINA_1_41.SetRadio()
    this.Parent.oContained.w_ORDINA=trim(this.Parent.oContained.w_ORDINA)
    this.value = ;
      iif(this.Parent.oContained.w_ORDINA=='N',1,;
      iif(this.Parent.oContained.w_ORDINA=='S',2,;
      iif(this.Parent.oContained.w_ORDINA=='R',3,;
      iif(this.Parent.oContained.w_ORDINA=='C',4,;
      0))))
  endfunc

  add object oPAGRD_1_49 as StdCheck with uid="WYJEWFDOLH",rtseq=26,rtrep=.f.,left=19, top=59, caption="Rimessa diretta",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti con rimessa diretta",;
    HelpContextID = 30637578,;
    cFormVar="w_PAGRD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRD_1_49.RadioValue()
    return(iif(this.value =1,'RD',;
    'XX'))
  endfunc
  func oPAGRD_1_49.GetRadio()
    this.Parent.oContained.w_PAGRD = this.RadioValue()
    return .t.
  endfunc

  func oPAGRD_1_49.SetRadio()
    this.Parent.oContained.w_PAGRD=trim(this.Parent.oContained.w_PAGRD)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRD=='RD',1,;
      0)
  endfunc

  add object oPAGBO_1_50 as StdCheck with uid="UHUFUMTJOS",rtseq=27,rtrep=.f.,left=19, top=78, caption="Bonifico",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite bonifico",;
    HelpContextID = 20151818,;
    cFormVar="w_PAGBO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGBO_1_50.RadioValue()
    return(iif(this.value =1,'BO',;
    'XX'))
  endfunc
  func oPAGBO_1_50.GetRadio()
    this.Parent.oContained.w_PAGBO = this.RadioValue()
    return .t.
  endfunc

  func oPAGBO_1_50.SetRadio()
    this.Parent.oContained.w_PAGBO=trim(this.Parent.oContained.w_PAGBO)
    this.value = ;
      iif(this.Parent.oContained.w_PAGBO=='BO',1,;
      0)
  endfunc

  add object oPAGRB_1_51 as StdCheck with uid="LJJBJQGWOI",rtseq=28,rtrep=.f.,left=19, top=97, caption="Ric.bancaria/Ri.Ba.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite ricevuta bancaria",;
    HelpContextID = 32734730,;
    cFormVar="w_PAGRB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRB_1_51.RadioValue()
    return(iif(this.value =1,'RB',;
    'XX'))
  endfunc
  func oPAGRB_1_51.GetRadio()
    this.Parent.oContained.w_PAGRB = this.RadioValue()
    return .t.
  endfunc

  func oPAGRB_1_51.SetRadio()
    this.Parent.oContained.w_PAGRB=trim(this.Parent.oContained.w_PAGRB)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRB=='RB',1,;
      0)
  endfunc

  add object oPAGRI_1_52 as StdCheck with uid="CIJZFYBCCS",rtseq=29,rtrep=.f.,left=190, top=59, caption="R.I.D.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite R.I.D.",;
    HelpContextID = 25394698,;
    cFormVar="w_PAGRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRI_1_52.RadioValue()
    return(iif(this.value =1,'RI',;
    'XX'))
  endfunc
  func oPAGRI_1_52.GetRadio()
    this.Parent.oContained.w_PAGRI = this.RadioValue()
    return .t.
  endfunc

  func oPAGRI_1_52.SetRadio()
    this.Parent.oContained.w_PAGRI=trim(this.Parent.oContained.w_PAGRI)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRI=='RI',1,;
      0)
  endfunc

  add object oPAGCA_1_53 as StdCheck with uid="WMQWIJEKXN",rtseq=30,rtrep=.f.,left=190, top=78, caption="Cambiale",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite cambiale",;
    HelpContextID = 34766346,;
    cFormVar="w_PAGCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGCA_1_53.RadioValue()
    return(iif(this.value =1,'CA',;
    'XX'))
  endfunc
  func oPAGCA_1_53.GetRadio()
    this.Parent.oContained.w_PAGCA = this.RadioValue()
    return .t.
  endfunc

  func oPAGCA_1_53.SetRadio()
    this.Parent.oContained.w_PAGCA=trim(this.Parent.oContained.w_PAGCA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGCA=='CA',1,;
      0)
  endfunc

  add object oPAGMA_1_54 as StdCheck with uid="OXYWDXMXXW",rtseq=31,rtrep=.f.,left=190, top=97, caption="M.AV.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite M.AV. (mediante avviso)",;
    HelpContextID = 34110986,;
    cFormVar="w_PAGMA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGMA_1_54.RadioValue()
    return(iif(this.value =1,'MA',;
    'XX'))
  endfunc
  func oPAGMA_1_54.GetRadio()
    this.Parent.oContained.w_PAGMA = this.RadioValue()
    return .t.
  endfunc

  func oPAGMA_1_54.SetRadio()
    this.Parent.oContained.w_PAGMA=trim(this.Parent.oContained.w_PAGMA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGMA=='MA',1,;
      0)
  endfunc


  add object oFLSOSP_1_55 as StdCombo with uid="FMCTCNPNEF",rtseq=32,rtrep=.f.,left=477,top=139,width=108,height=21;
    , ToolTipText = "Ricerca le scadenze per il flag sospeso";
    , HelpContextID = 253381718;
    , cFormVar="w_FLSOSP",RowSource=""+"Tutte,"+"Non sospese,"+"Sospese", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSOSP_1_55.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oFLSOSP_1_55.GetRadio()
    this.Parent.oContained.w_FLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oFLSOSP_1_55.SetRadio()
    this.Parent.oContained.w_FLSOSP=trim(this.Parent.oContained.w_FLSOSP)
    this.value = ;
      iif(this.Parent.oContained.w_FLSOSP=='T',1,;
      iif(this.Parent.oContained.w_FLSOSP=='N',2,;
      iif(this.Parent.oContained.w_FLSOSP=='S',3,;
      0)))
  endfunc


  add object oTIPCON_1_59 as StdCombo with uid="WDXDHXNHSV",rtseq=35,rtrep=.f.,left=48,top=35,width=89,height=21;
    , ToolTipText = "Tipo partite/scadenze da selezionare, clienti, fornitori";
    , HelpContextID = 214833718;
    , cFormVar="w_TIPCON",RowSource=""+"Clienti,"+"Fornitori", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_59.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPCON_1_59.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_59.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      0))
  endfunc

  func oTIPCON_1_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_1_29('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oVALUTA_1_63 as StdField with uid="KITUVABRTZ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta, se vuota tutte",;
    HelpContextID = 3134038,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=548, Top=34, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_1_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_63('Part',this)
    endwith
    return bRes
  endfunc

  proc oVALUTA_1_63.ecpDrop(oSource)
    this.Parent.oContained.link_1_63('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVALUTA_1_63.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oVALUTA_1_63'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oVALUTA_1_63.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_VALUTA
     i_obj.ecpSave()
  endproc

  add object oFLRAGG_1_65 as StdCheck with uid="CONWURMEOQ",rtseq=37,rtrep=.f.,left=136, top=357, caption="Raggruppa per effetto",;
    ToolTipText = "Se attivo, raggruppa per effetto",;
    HelpContextID = 88882262,;
    cFormVar="w_FLRAGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLRAGG_1_65.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLRAGG_1_65.GetRadio()
    this.Parent.oContained.w_FLRAGG = this.RadioValue()
    return .t.
  endfunc

  func oFLRAGG_1_65.SetRadio()
    this.Parent.oContained.w_FLRAGG=trim(this.Parent.oContained.w_FLRAGG)
    this.value = ;
      iif(this.Parent.oContained.w_FLRAGG=='S',1,;
      0)
  endfunc

  add object oDOCINI_1_73 as StdField with uid="AFXZPEPPSR",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data documento iniziale � maggiore della data documento finale",;
    ToolTipText = "Data iniziale documento da ricercare",;
    HelpContextID = 130240310,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=129, Top=404

  func oDOCINI_1_73.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_docini<=.w_docfin) or (empty(.w_docfin)))
    endwith
    return bRes
  endfunc

  add object oDOCFIN_1_74 as StdField with uid="YRYVWFRQDQ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data documento iniziale � maggiore della data documento finale",;
    ToolTipText = "Data finale documento da ricercare",;
    HelpContextID = 208686902,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=129, Top=429

  func oDOCFIN_1_74.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_docini<=.w_docfin) or (empty(.w_docini)))
    endwith
    return bRes
  endfunc

  add object oREGINI_1_77 as StdField with uid="BMWKHISGYA",rtseq=44,rtrep=.f.,;
    cFormVar = "w_REGINI", cQueryName = "REGINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data registrazione iniziale � maggiore della data registrazione finale",;
    ToolTipText = "Data iniziale registrazione da ricercare",;
    HelpContextID = 130254358,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=347, Top=404

  func oREGINI_1_77.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_regini<=.w_regfin) or (empty(.w_regfin)))
    endwith
    return bRes
  endfunc

  add object oREGFIN_1_78 as StdField with uid="NLUTMFWTKC",rtseq=45,rtrep=.f.,;
    cFormVar = "w_REGFIN", cQueryName = "REGFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data registrazione iniziale � maggiore della data registrazione finale",;
    ToolTipText = "Data finale registrazione da ricercare",;
    HelpContextID = 208700950,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=347, Top=429

  func oREGFIN_1_78.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_regini<=.w_regfin) or (empty(.w_regini)))
    endwith
    return bRes
  endfunc


  add object oBtn_1_80 as StdButton with uid="SQNLIXYMPH",left=487, top=406, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 107594778;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_80.Click()
      with this.Parent.oContained
        do GSTE_BBE with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_82 as StdButton with uid="VJEYGWVSZN",left=537, top=406, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 100306106;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_82.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="UHULSMOLRM",Visible=.t., Left=12, Top=7,;
    Alignment=0, Width=190, Height=14,;
    Caption="Selez. tipo effetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="HRHDNGMHAE",Visible=.t., Left=293, Top=34,;
    Alignment=1, Width=70, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="PMYSREYHZS",Visible=.t., Left=300, Top=59,;
    Alignment=1, Width=63, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="OJNPKZHYTH",Visible=.t., Left=487, Top=85,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="JTWYBEKTDW",Visible=.t., Left=487, Top=110,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="HPKOJQQYXJ",Visible=.t., Left=280, Top=86,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="XNNPVHYWCF",Visible=.t., Left=288, Top=110,;
    Alignment=1, Width=75, Height=18,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="DKRKMHJMLD",Visible=.t., Left=298, Top=7,;
    Alignment=0, Width=151, Height=18,;
    Caption="Selezione scadenze"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="EWIAZWZDYE",Visible=.t., Left=12, Top=165,;
    Alignment=0, Width=251, Height=18,;
    Caption="Selezioni solo per effetti presentati"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_20 as StdString with uid="VBWDTJVUXZ",Visible=.t., Left=10, Top=187,;
    Alignment=1, Width=87, Height=18,;
    Caption="Cod. banca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="YQEINCWBED",Visible=.t., Left=405, Top=215,;
    Alignment=1, Width=103, Height=18,;
    Caption="Data di pres.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="MBZNGOTEYP",Visible=.t., Left=12, Top=239,;
    Alignment=0, Width=61, Height=18,;
    Caption="Cliente"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="MPWAFMDJQV",Visible=.t., Left=19, Top=264,;
    Alignment=1, Width=60, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="YPFZSQDMGZ",Visible=.t., Left=12, Top=296,;
    Alignment=0, Width=116, Height=18,;
    Caption="Selezione effetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_37 as StdString with uid="DAKPFXVBCW",Visible=.t., Left=36, Top=318,;
    Alignment=1, Width=95, Height=18,;
    Caption="Da presentare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="OGBIVAKJPT",Visible=.t., Left=44, Top=334,;
    Alignment=1, Width=87, Height=18,;
    Caption="Contabilizzati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="QOZLEYYWTG",Visible=.t., Left=332, Top=295,;
    Alignment=0, Width=134, Height=18,;
    Caption="Ordinamento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_42 as StdString with uid="JFPBZZIXEV",Visible=.t., Left=331, Top=318,;
    Alignment=1, Width=96, Height=18,;
    Caption="Ordinamento per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="MJOOVSZMAE",Visible=.t., Left=356, Top=139,;
    Alignment=1, Width=119, Height=15,;
    Caption="Test sospese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="DKKXSABCFP",Visible=.t., Left=12, Top=35,;
    Alignment=1, Width=33, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="UBCDCFLCUS",Visible=.t., Left=12, Top=239,;
    Alignment=0, Width=64, Height=18,;
    Caption="Fornitore"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='C')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="EXBVUIQWIE",Visible=.t., Left=18, Top=264,;
    Alignment=1, Width=61, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='C')
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="BCXAOTEFTP",Visible=.t., Left=451, Top=34,;
    Alignment=1, Width=94, Height=15,;
    Caption="Codice valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="PVIESUUERK",Visible=.t., Left=202, Top=217,;
    Alignment=1, Width=78, Height=18,;
    Caption="Num. distinta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="NMKNIYUOOV",Visible=.t., Left=12, Top=378,;
    Alignment=0, Width=119, Height=18,;
    Caption="Selezioni aggiuntive"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_75 as StdString with uid="UTADMSFMWI",Visible=.t., Left=16, Top=404,;
    Alignment=1, Width=111, Height=18,;
    Caption="Da data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="WGBHSRGKKV",Visible=.t., Left=25, Top=429,;
    Alignment=1, Width=102, Height=18,;
    Caption="A data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="MDJCPHITVJ",Visible=.t., Left=225, Top=404,;
    Alignment=1, Width=120, Height=18,;
    Caption="Da data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="ZVNGSAMTPL",Visible=.t., Left=234, Top=429,;
    Alignment=1, Width=111, Height=18,;
    Caption="A data registrazione:"  ;
  , bGlobalFont=.t.

  add object oBox_1_2 as StdBox with uid="YZGORIRRVI",left=12, top=23, width=573,height=2

  add object oBox_1_16 as StdBox with uid="PFLHEECNLI",left=12, top=179, width=573,height=2

  add object oBox_1_28 as StdBox with uid="LJSYVMPLWD",left=12, top=254, width=573,height=2

  add object oBox_1_35 as StdBox with uid="HJULROVMET",left=12, top=312, width=573,height=2

  add object oBox_1_72 as StdBox with uid="RXEXFZWAOE",left=12, top=395, width=573,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_sbe','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
