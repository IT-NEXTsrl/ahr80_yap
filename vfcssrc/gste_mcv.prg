* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_mcv                                                        *
*              Informazioni CVS per bonifici esteri                            *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_159]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-16                                                      *
* Last revis.: 2015-03-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgste_mcv")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgste_mcv")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgste_mcv")
  return

* --- Class definition
define class tgste_mcv as StdPCForm
  Width  = 736
  Height = 306
  Top    = 3
  Left   = 12
  cComment = "Informazioni CVS per bonifici esteri"
  cPrg = "gste_mcv"
  HelpContextID=97137769
  add object cnt as tcgste_mcv
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgste_mcv as PCContext
  w_DINUMDIS = space(10)
  w_DIROWORD = 0
  w_DIROWNUM = 0
  w_COMBOVECCHIA = space(1)
  w_DICAUUIC = space(5)
  w_DITIPCVS = space(3)
  w_DIIMPORT = 0
  w_DITIPCVS2 = space(3)
  w_DITIPCVS1 = space(3)
  w_DINUMCVS = space(12)
  w_DIDATCVS = space(8)
  w_DIBANEMI = space(5)
  w_DICVSDEC = space(1)
  w_DITARDOG = 0
  w_DISPETRA = 0
  w_DIDATSDO = space(8)
  w_DITIPRES = space(1)
  w_TIPDIS = space(10)
  w_TOTRIG = 0
  w_NOCVS = space(1)
  proc Save(i_oFrom)
    this.w_DINUMDIS = i_oFrom.w_DINUMDIS
    this.w_DIROWORD = i_oFrom.w_DIROWORD
    this.w_DIROWNUM = i_oFrom.w_DIROWNUM
    this.w_COMBOVECCHIA = i_oFrom.w_COMBOVECCHIA
    this.w_DICAUUIC = i_oFrom.w_DICAUUIC
    this.w_DITIPCVS = i_oFrom.w_DITIPCVS
    this.w_DIIMPORT = i_oFrom.w_DIIMPORT
    this.w_DITIPCVS2 = i_oFrom.w_DITIPCVS2
    this.w_DITIPCVS1 = i_oFrom.w_DITIPCVS1
    this.w_DINUMCVS = i_oFrom.w_DINUMCVS
    this.w_DIDATCVS = i_oFrom.w_DIDATCVS
    this.w_DIBANEMI = i_oFrom.w_DIBANEMI
    this.w_DICVSDEC = i_oFrom.w_DICVSDEC
    this.w_DITARDOG = i_oFrom.w_DITARDOG
    this.w_DISPETRA = i_oFrom.w_DISPETRA
    this.w_DIDATSDO = i_oFrom.w_DIDATSDO
    this.w_DITIPRES = i_oFrom.w_DITIPRES
    this.w_TIPDIS = i_oFrom.w_TIPDIS
    this.w_TOTRIG = i_oFrom.w_TOTRIG
    this.w_NOCVS = i_oFrom.w_NOCVS
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DINUMDIS = this.w_DINUMDIS
    i_oTo.w_DIROWORD = this.w_DIROWORD
    i_oTo.w_DIROWNUM = this.w_DIROWNUM
    i_oTo.w_COMBOVECCHIA = this.w_COMBOVECCHIA
    i_oTo.w_DICAUUIC = this.w_DICAUUIC
    i_oTo.w_DITIPCVS = this.w_DITIPCVS
    i_oTo.w_DIIMPORT = this.w_DIIMPORT
    i_oTo.w_DITIPCVS2 = this.w_DITIPCVS2
    i_oTo.w_DITIPCVS1 = this.w_DITIPCVS1
    i_oTo.w_DINUMCVS = this.w_DINUMCVS
    i_oTo.w_DIDATCVS = this.w_DIDATCVS
    i_oTo.w_DIBANEMI = this.w_DIBANEMI
    i_oTo.w_DICVSDEC = this.w_DICVSDEC
    i_oTo.w_DITARDOG = this.w_DITARDOG
    i_oTo.w_DISPETRA = this.w_DISPETRA
    i_oTo.w_DIDATSDO = this.w_DIDATSDO
    i_oTo.w_DITIPRES = this.w_DITIPRES
    i_oTo.w_TIPDIS = this.w_TIPDIS
    i_oTo.w_TOTRIG = this.w_TOTRIG
    i_oTo.w_NOCVS = this.w_NOCVS
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgste_mcv as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 736
  Height = 306
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-09"
  HelpContextID=97137769
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DIS_BECV_IDX = 0
  COD_ABI_IDX = 0
  cFile = "DIS_BECV"
  cKeySelect = "DINUMDIS,DIROWORD,DIROWNUM"
  cKeyWhere  = "DINUMDIS=this.w_DINUMDIS and DIROWORD=this.w_DIROWORD and DIROWNUM=this.w_DIROWNUM"
  cKeyDetail  = "DINUMDIS=this.w_DINUMDIS and DIROWORD=this.w_DIROWORD and DIROWNUM=this.w_DIROWNUM"
  cKeyWhereODBC = '"DINUMDIS="+cp_ToStrODBC(this.w_DINUMDIS)';
      +'+" and DIROWORD="+cp_ToStrODBC(this.w_DIROWORD)';
      +'+" and DIROWNUM="+cp_ToStrODBC(this.w_DIROWNUM)';

  cKeyDetailWhereODBC = '"DINUMDIS="+cp_ToStrODBC(this.w_DINUMDIS)';
      +'+" and DIROWORD="+cp_ToStrODBC(this.w_DIROWORD)';
      +'+" and DIROWNUM="+cp_ToStrODBC(this.w_DIROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DIS_BECV.DINUMDIS="+cp_ToStrODBC(this.w_DINUMDIS)';
      +'+" and DIS_BECV.DIROWORD="+cp_ToStrODBC(this.w_DIROWORD)';
      +'+" and DIS_BECV.DIROWNUM="+cp_ToStrODBC(this.w_DIROWNUM)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DIS_BECV.CPROWNUM '
  cPrg = "gste_mcv"
  cComment = "Informazioni CVS per bonifici esteri"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DINUMDIS = space(10)
  w_DIROWORD = 0
  w_DIROWNUM = 0
  w_COMBOVECCHIA = .F.
  w_DICAUUIC = space(5)
  o_DICAUUIC = space(5)
  w_DITIPCVS = space(3)
  o_DITIPCVS = space(3)
  w_DIIMPORT = 0
  w_DITIPCVS2 = space(3)
  o_DITIPCVS2 = space(3)
  w_DITIPCVS1 = space(3)
  o_DITIPCVS1 = space(3)
  w_DINUMCVS = space(12)
  w_DIDATCVS = ctod('  /  /  ')
  w_DIBANEMI = space(5)
  w_DICVSDEC = space(1)
  w_DITARDOG = 0
  w_DISPETRA = 0
  w_DIDATSDO = ctod('  /  /  ')
  w_DITIPRES = space(1)
  w_TIPDIS = space(10)
  w_TOTRIG = 0
  w_NOCVS = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_mcvPag1","gste_mcv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='COD_ABI'
    this.cWorkTables[2]='DIS_BECV'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIS_BECV_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIS_BECV_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgste_mcv'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DIS_BECV where DINUMDIS=KeySet.DINUMDIS
    *                            and DIROWORD=KeySet.DIROWORD
    *                            and DIROWNUM=KeySet.DIROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DIS_BECV_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_BECV_IDX,2],this.bLoadRecFilter,this.DIS_BECV_IDX,"gste_mcv")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIS_BECV')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIS_BECV.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIS_BECV '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DINUMDIS',this.w_DINUMDIS  ,'DIROWORD',this.w_DIROWORD  ,'DIROWNUM',this.w_DIROWNUM  )
      select * from (i_cTable) DIS_BECV where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_COMBOVECCHIA = .f.
        .w_TOTRIG = 0
        .w_DINUMDIS = NVL(DINUMDIS,space(10))
        .w_DIROWORD = NVL(DIROWORD,0)
        .w_DIROWNUM = NVL(DIROWNUM,0)
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .w_TIPDIS = THIS.OPARENTOBJECT .w_TIPDIS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DIS_BECV')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTRIG = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DICAUUIC = NVL(DICAUUIC,space(5))
          .w_DITIPCVS = NVL(DITIPCVS,space(3))
          .w_DIIMPORT = NVL(DIIMPORT,0)
        .w_DITIPCVS2 = IIF (EMPTY (.w_DITIPCVS) , 'EMI' , .w_DITIPCVS)
        .w_DITIPCVS1 = IIF (EMPTY (.w_DITIPCVS) , 'EMI' , .w_DITIPCVS)
          .w_DINUMCVS = NVL(DINUMCVS,space(12))
          .w_DIDATCVS = NVL(cp_ToDate(DIDATCVS),ctod("  /  /  "))
          .w_DIBANEMI = NVL(DIBANEMI,space(5))
          * evitabile
          *.link_2_8('Load')
          .w_DICVSDEC = NVL(DICVSDEC,space(1))
          .w_DITARDOG = NVL(DITARDOG,0)
          .w_DISPETRA = NVL(DISPETRA,0)
          .w_DIDATSDO = NVL(cp_ToDate(DIDATSDO),ctod("  /  /  "))
          .w_DITIPRES = NVL(DITIPRES,space(1))
        .w_NOCVS = THIS.OPARENTOBJECT .w_NOCVS
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTRIG = .w_TOTRIG+.w_DIIMPORT
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DINUMDIS=space(10)
      .w_DIROWORD=0
      .w_DIROWNUM=0
      .w_COMBOVECCHIA=.f.
      .w_DICAUUIC=space(5)
      .w_DITIPCVS=space(3)
      .w_DIIMPORT=0
      .w_DITIPCVS2=space(3)
      .w_DITIPCVS1=space(3)
      .w_DINUMCVS=space(12)
      .w_DIDATCVS=ctod("  /  /  ")
      .w_DIBANEMI=space(5)
      .w_DICVSDEC=space(1)
      .w_DITARDOG=0
      .w_DISPETRA=0
      .w_DIDATSDO=ctod("  /  /  ")
      .w_DITIPRES=space(1)
      .w_TIPDIS=space(10)
      .w_TOTRIG=0
      .w_NOCVS=space(1)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(1,5,.f.)
        .w_DITIPCVS = 'EMI'
        .w_DIIMPORT = IIF(NOT EMPTY(.w_DICAUUIC),this.oparentobject .w_TOTDIS-.w_TOTRIG,0)
        .w_DITIPCVS2 = IIF (EMPTY (.w_DITIPCVS) , 'EMI' , .w_DITIPCVS)
        .w_DITIPCVS1 = IIF (EMPTY (.w_DITIPCVS) , 'EMI' , .w_DITIPCVS)
        .DoRTCalc(10,12,.f.)
        if not(empty(.w_DIBANEMI))
         .link_2_8('Full')
        endif
        .w_DICVSDEC = ' '
        .DoRTCalc(14,16,.f.)
        .w_DITIPRES = ' '
        .w_TIPDIS = THIS.OPARENTOBJECT .w_TIPDIS
        .DoRTCalc(19,19,.f.)
        .w_NOCVS = THIS.OPARENTOBJECT .w_NOCVS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIS_BECV')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gste_mcv
    this.w_DICVSDEC='D'
    this.w_DITIPRES='C'
    this.SetControlsValue()
    this.w_DICVSDEC=' '
    this.w_DITIPRES=' '
    this.SetControlsValue()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_1_4.enabled = i_bVal
      .Page1.oPag.oObj_1_5.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DIS_BECV',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIS_BECV_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DINUMDIS,"DINUMDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIROWORD,"DIROWORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIROWNUM,"DIROWNUM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DICAUUIC C(5);
      ,t_DIIMPORT N(18,4);
      ,t_DITIPCVS2 N(3);
      ,t_DITIPCVS1 N(3);
      ,t_DINUMCVS C(12);
      ,t_DIDATCVS D(8);
      ,t_DIBANEMI C(5);
      ,t_DICVSDEC N(3);
      ,t_DITARDOG N(18,4);
      ,t_DISPETRA N(18,4);
      ,t_DIDATSDO D(8);
      ,t_DITIPRES N(3);
      ,CPROWNUM N(10);
      ,t_DITIPCVS C(3);
      ,t_NOCVS C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgste_mcvbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDICAUUIC_2_1.controlsource=this.cTrsName+'.t_DICAUUIC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIIMPORT_2_3.controlsource=this.cTrsName+'.t_DIIMPORT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS2_2_4.controlsource=this.cTrsName+'.t_DITIPCVS2'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS1_2_5.controlsource=this.cTrsName+'.t_DITIPCVS1'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDINUMCVS_2_6.controlsource=this.cTrsName+'.t_DINUMCVS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIDATCVS_2_7.controlsource=this.cTrsName+'.t_DIDATCVS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIBANEMI_2_8.controlsource=this.cTrsName+'.t_DIBANEMI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDICVSDEC_2_9.controlsource=this.cTrsName+'.t_DICVSDEC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDITARDOG_2_10.controlsource=this.cTrsName+'.t_DITARDOG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDISPETRA_2_11.controlsource=this.cTrsName+'.t_DISPETRA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIDATSDO_2_12.controlsource=this.cTrsName+'.t_DIDATSDO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPRES_2_13.controlsource=this.cTrsName+'.t_DITIPRES'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(57)
    this.AddVLine(205)
    this.AddVLine(382)
    this.AddVLine(482)
    this.AddVLine(572)
    this.AddVLine(633)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICAUUIC_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIS_BECV_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_BECV_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIS_BECV_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_BECV_IDX,2])
      *
      * insert into DIS_BECV
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIS_BECV')
        i_extval=cp_InsertValODBCExtFlds(this,'DIS_BECV')
        i_cFldBody=" "+;
                  "(DINUMDIS,DIROWORD,DIROWNUM,DICAUUIC,DITIPCVS"+;
                  ",DIIMPORT,DINUMCVS,DIDATCVS,DIBANEMI,DICVSDEC"+;
                  ",DITARDOG,DISPETRA,DIDATSDO,DITIPRES,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DINUMDIS)+","+cp_ToStrODBC(this.w_DIROWORD)+","+cp_ToStrODBC(this.w_DIROWNUM)+","+cp_ToStrODBC(this.w_DICAUUIC)+","+cp_ToStrODBC(this.w_DITIPCVS)+;
             ","+cp_ToStrODBC(this.w_DIIMPORT)+","+cp_ToStrODBC(this.w_DINUMCVS)+","+cp_ToStrODBC(this.w_DIDATCVS)+","+cp_ToStrODBCNull(this.w_DIBANEMI)+","+cp_ToStrODBC(this.w_DICVSDEC)+;
             ","+cp_ToStrODBC(this.w_DITARDOG)+","+cp_ToStrODBC(this.w_DISPETRA)+","+cp_ToStrODBC(this.w_DIDATSDO)+","+cp_ToStrODBC(this.w_DITIPRES)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIS_BECV')
        i_extval=cp_InsertValVFPExtFlds(this,'DIS_BECV')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DINUMDIS',this.w_DINUMDIS,'DIROWORD',this.w_DIROWORD,'DIROWNUM',this.w_DIROWNUM)
        INSERT INTO (i_cTable) (;
                   DINUMDIS;
                  ,DIROWORD;
                  ,DIROWNUM;
                  ,DICAUUIC;
                  ,DITIPCVS;
                  ,DIIMPORT;
                  ,DINUMCVS;
                  ,DIDATCVS;
                  ,DIBANEMI;
                  ,DICVSDEC;
                  ,DITARDOG;
                  ,DISPETRA;
                  ,DIDATSDO;
                  ,DITIPRES;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DINUMDIS;
                  ,this.w_DIROWORD;
                  ,this.w_DIROWNUM;
                  ,this.w_DICAUUIC;
                  ,this.w_DITIPCVS;
                  ,this.w_DIIMPORT;
                  ,this.w_DINUMCVS;
                  ,this.w_DIDATCVS;
                  ,this.w_DIBANEMI;
                  ,this.w_DICVSDEC;
                  ,this.w_DITARDOG;
                  ,this.w_DISPETRA;
                  ,this.w_DIDATSDO;
                  ,this.w_DITIPRES;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DIS_BECV_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_BECV_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_DICAUUIC)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DIS_BECV')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DIS_BECV')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_DICAUUIC)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DIS_BECV
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DIS_BECV')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DICAUUIC="+cp_ToStrODBC(this.w_DICAUUIC)+;
                     ",DITIPCVS="+cp_ToStrODBC(this.w_DITIPCVS)+;
                     ",DIIMPORT="+cp_ToStrODBC(this.w_DIIMPORT)+;
                     ",DINUMCVS="+cp_ToStrODBC(this.w_DINUMCVS)+;
                     ",DIDATCVS="+cp_ToStrODBC(this.w_DIDATCVS)+;
                     ",DIBANEMI="+cp_ToStrODBCNull(this.w_DIBANEMI)+;
                     ",DICVSDEC="+cp_ToStrODBC(this.w_DICVSDEC)+;
                     ",DITARDOG="+cp_ToStrODBC(this.w_DITARDOG)+;
                     ",DISPETRA="+cp_ToStrODBC(this.w_DISPETRA)+;
                     ",DIDATSDO="+cp_ToStrODBC(this.w_DIDATSDO)+;
                     ",DITIPRES="+cp_ToStrODBC(this.w_DITIPRES)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DIS_BECV')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DICAUUIC=this.w_DICAUUIC;
                     ,DITIPCVS=this.w_DITIPCVS;
                     ,DIIMPORT=this.w_DIIMPORT;
                     ,DINUMCVS=this.w_DINUMCVS;
                     ,DIDATCVS=this.w_DIDATCVS;
                     ,DIBANEMI=this.w_DIBANEMI;
                     ,DICVSDEC=this.w_DICVSDEC;
                     ,DITARDOG=this.w_DITARDOG;
                     ,DISPETRA=this.w_DISPETRA;
                     ,DIDATSDO=this.w_DIDATSDO;
                     ,DITIPRES=this.w_DITIPRES;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIS_BECV_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_BECV_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_DICAUUIC)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DIS_BECV
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_DICAUUIC)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIS_BECV_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_BECV_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(1,6,.t.)
        if .o_DICAUUIC<>.w_DICAUUIC
          .w_TOTRIG = .w_TOTRIG-.w_diimport
          .w_DIIMPORT = IIF(NOT EMPTY(.w_DICAUUIC),this.oparentobject .w_TOTDIS-.w_TOTRIG,0)
          .w_TOTRIG = .w_TOTRIG+.w_diimport
        endif
        if .o_DITIPCVS<>.w_DITIPCVS
          .w_DITIPCVS2 = IIF (EMPTY (.w_DITIPCVS) , 'EMI' , .w_DITIPCVS)
        endif
        if .o_DITIPCVS<>.w_DITIPCVS
          .w_DITIPCVS1 = IIF (EMPTY (.w_DITIPCVS) , 'EMI' , .w_DITIPCVS)
        endif
        .DoRTCalc(10,17,.t.)
          .w_TIPDIS = THIS.OPARENTOBJECT .w_TIPDIS
        if .o_DITIPCVS1<>.w_DITIPCVS1
          .Calculate_SGQEKDREFU()
        endif
        if .o_DITIPCVS2<>.w_DITIPCVS2
          .Calculate_NISZYMGWQI()
        endif
        .DoRTCalc(19,19,.t.)
          .w_NOCVS = THIS.OPARENTOBJECT .w_NOCVS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DITIPCVS with this.w_DITIPCVS
      replace t_NOCVS with this.w_NOCVS
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_SGQEKDREFU()
    with this
          * --- Aggirna DITIPCVS
          .w_DITIPCVS = .w_DITIPCVS1
    endwith
  endproc
  proc Calculate_NISZYMGWQI()
    with this
          * --- Aggirna DITIPCVS
          .w_DITIPCVS = .w_DITIPCVS2
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDIIMPORT_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDIIMPORT_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDITIPCVS2_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDITIPCVS2_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDITIPCVS1_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDITIPCVS1_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDINUMCVS_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDINUMCVS_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDIDATCVS_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDIDATCVS_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDIBANEMI_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDIBANEMI_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDICVSDEC_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDICVSDEC_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDITARDOG_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDITARDOG_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDISPETRA_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDISPETRA_2_11.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDIDATSDO_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDIDATSDO_2_12.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDITIPRES_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDITIPRES_2_13.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS2_2_4.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS2_2_4.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS1_2_5.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS1_2_5.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DIBANEMI
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_ABI_IDX,3]
    i_lTable = "COD_ABI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2], .t., this.COD_ABI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIBANEMI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABI',True,'COD_ABI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ABCODABI like "+cp_ToStrODBC(trim(this.w_DIBANEMI)+"%");

          i_ret=cp_SQL(i_nConn,"select ABCODABI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ABCODABI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ABCODABI',trim(this.w_DIBANEMI))
          select ABCODABI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ABCODABI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIBANEMI)==trim(_Link_.ABCODABI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DIBANEMI) and !this.bDontReportError
            deferred_cp_zoom('COD_ABI','*','ABCODABI',cp_AbsName(oSource.parent,'oDIBANEMI_2_8'),i_cWhere,'GSAR_ABI',"Elenco codici ABI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI";
                     +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',oSource.xKey(1))
            select ABCODABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIBANEMI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI";
                   +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(this.w_DIBANEMI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',this.w_DIBANEMI)
            select ABCODABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIBANEMI = NVL(_Link_.ABCODABI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DIBANEMI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])+'\'+cp_ToStr(_Link_.ABCODABI,1)
      cp_ShowWarn(i_cKey,this.COD_ABI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIBANEMI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICAUUIC_2_1.value==this.w_DICAUUIC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICAUUIC_2_1.value=this.w_DICAUUIC
      replace t_DICAUUIC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICAUUIC_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIIMPORT_2_3.value==this.w_DIIMPORT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIIMPORT_2_3.value=this.w_DIIMPORT
      replace t_DIIMPORT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIIMPORT_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS2_2_4.RadioValue()==this.w_DITIPCVS2)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS2_2_4.SetRadio()
      replace t_DITIPCVS2 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS2_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS1_2_5.RadioValue()==this.w_DITIPCVS1)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS1_2_5.SetRadio()
      replace t_DITIPCVS1 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS1_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDINUMCVS_2_6.value==this.w_DINUMCVS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDINUMCVS_2_6.value=this.w_DINUMCVS
      replace t_DINUMCVS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDINUMCVS_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIDATCVS_2_7.value==this.w_DIDATCVS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIDATCVS_2_7.value=this.w_DIDATCVS
      replace t_DIDATCVS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIDATCVS_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIBANEMI_2_8.value==this.w_DIBANEMI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIBANEMI_2_8.value=this.w_DIBANEMI
      replace t_DIBANEMI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIBANEMI_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICVSDEC_2_9.RadioValue()==this.w_DICVSDEC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICVSDEC_2_9.SetRadio()
      replace t_DICVSDEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICVSDEC_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITARDOG_2_10.value==this.w_DITARDOG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITARDOG_2_10.value=this.w_DITARDOG
      replace t_DITARDOG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITARDOG_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDISPETRA_2_11.value==this.w_DISPETRA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDISPETRA_2_11.value=this.w_DISPETRA
      replace t_DISPETRA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDISPETRA_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIDATSDO_2_12.value==this.w_DIDATSDO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIDATSDO_2_12.value=this.w_DIDATSDO
      replace t_DIDATSDO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIDATSDO_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPRES_2_13.RadioValue()==this.w_DITIPRES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPRES_2_13.SetRadio()
      replace t_DITIPRES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPRES_2_13.value
    endif
    cp_SetControlsValueExtFlds(this,'DIS_BECV')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gste_mcv
      *if UPPER(this.cFunction)='LOAD'
      *    return(.t.)
      *endif
      
      * ---- Ripreso da controllo nativo Code Painter numero max (6) e min (1) di righe
      * ---- per modificare messaggio e per condizionare il controllo
      * ---- solo per bonifico estero.
      if this.oparentobject.oparentobject.w_TIPDIS='BE' AND this.oparentobject.oparentobject.w_NOCVS<>'S'
            local i_oldarea
            i_oldarea=alias()
            if type ("i_oldarea")<> 'C'
            RETURN
            ENDIF
            select count(*) as cnt from (this.cTrsName);
               where not(deleted()) and (NOT EMPTY(t_DICAUUIC));
                into cursor __chk__
            if not(1<=cnt and cnt<=6)
              Ah_ErrorMsg("Ci devono essere tra 1 e 6 righe nel dettaglio CVS",'!',"")
              IF USED("__chk__")
              select __chk__
              use
              endif
              IF NOT EMPTY (i_oldarea)
              select (i_oldarea)
              endif
              return(.f.)
            endif
            if USED ("__chk__")
              select __chk__
              use
            endif
            IF NOT EMPTY (i_oldarea)
            select (i_oldarea)
            ENDIF
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not(not empty(.w_DITIPCVS))
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("Inserire Tipo CVS!")
      endcase
      if NOT EMPTY(.w_DICAUUIC)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DICAUUIC = this.w_DICAUUIC
    this.o_DITIPCVS = this.w_DITIPCVS
    this.o_DITIPCVS2 = this.w_DITIPCVS2
    this.o_DITIPCVS1 = this.w_DITIPCVS1
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_DICAUUIC))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DICAUUIC=space(5)
      .w_DITIPCVS=space(3)
      .w_DIIMPORT=0
      .w_DITIPCVS2=space(3)
      .w_DITIPCVS1=space(3)
      .w_DINUMCVS=space(12)
      .w_DIDATCVS=ctod("  /  /  ")
      .w_DIBANEMI=space(5)
      .w_DICVSDEC=space(1)
      .w_DITARDOG=0
      .w_DISPETRA=0
      .w_DIDATSDO=ctod("  /  /  ")
      .w_DITIPRES=space(1)
      .w_NOCVS=space(1)
      .DoRTCalc(1,5,.f.)
        .w_DITIPCVS = 'EMI'
        .w_DIIMPORT = IIF(NOT EMPTY(.w_DICAUUIC),this.oparentobject .w_TOTDIS-.w_TOTRIG,0)
        .w_DITIPCVS2 = IIF (EMPTY (.w_DITIPCVS) , 'EMI' , .w_DITIPCVS)
        .w_DITIPCVS1 = IIF (EMPTY (.w_DITIPCVS) , 'EMI' , .w_DITIPCVS)
      .DoRTCalc(10,12,.f.)
      if not(empty(.w_DIBANEMI))
        .link_2_8('Full')
      endif
        .w_DICVSDEC = ' '
      .DoRTCalc(14,16,.f.)
        .w_DITIPRES = ' '
      .DoRTCalc(18,19,.f.)
        .w_NOCVS = THIS.OPARENTOBJECT .w_NOCVS
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DICAUUIC = t_DICAUUIC
    this.w_DITIPCVS = t_DITIPCVS
    this.w_DIIMPORT = t_DIIMPORT
    this.w_DITIPCVS2 = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS2_2_4.RadioValue(.t.)
    this.w_DITIPCVS1 = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS1_2_5.RadioValue(.t.)
    this.w_DINUMCVS = t_DINUMCVS
    this.w_DIDATCVS = t_DIDATCVS
    this.w_DIBANEMI = t_DIBANEMI
    this.w_DICVSDEC = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICVSDEC_2_9.RadioValue(.t.)
    this.w_DITARDOG = t_DITARDOG
    this.w_DISPETRA = t_DISPETRA
    this.w_DIDATSDO = t_DIDATSDO
    this.w_DITIPRES = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPRES_2_13.RadioValue(.t.)
    this.w_NOCVS = t_NOCVS
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DICAUUIC with this.w_DICAUUIC
    replace t_DITIPCVS with this.w_DITIPCVS
    replace t_DIIMPORT with this.w_DIIMPORT
    replace t_DITIPCVS2 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS2_2_4.ToRadio()
    replace t_DITIPCVS1 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPCVS1_2_5.ToRadio()
    replace t_DINUMCVS with this.w_DINUMCVS
    replace t_DIDATCVS with this.w_DIDATCVS
    replace t_DIBANEMI with this.w_DIBANEMI
    replace t_DICVSDEC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICVSDEC_2_9.ToRadio()
    replace t_DITARDOG with this.w_DITARDOG
    replace t_DISPETRA with this.w_DISPETRA
    replace t_DIDATSDO with this.w_DIDATSDO
    replace t_DITIPRES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPRES_2_13.ToRadio()
    replace t_NOCVS with this.w_NOCVS
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTRIG = .w_TOTRIG-.w_diimport
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgste_mcvPag1 as StdContainer
  Width  = 732
  height = 306
  stdWidth  = 732
  stdheight = 306
  resizeXpos=723
  resizeYpos=257
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_4 as cp_runprogram with uid="OTXYSGVBVR",left=2, top=313, width=323,height=22,;
    caption='GSTE_BCV',;
   bGlobalFont=.t.,;
    prg="GSTE_BCV('IMP')",;
    cEvent = "w_DIIMPORT Changed,Init",;
    nPag=1;
    , HelpContextID = 40919996


  add object oObj_1_5 as cp_runprogram with uid="TPVXQCMTSS",left=2, top=343, width=323,height=22,;
    caption='GSTE_BCV',;
   bGlobalFont=.t.,;
    prg="GSTE_BCV('TIP')",;
    cEvent = "Load",;
    nPag=1;
    , HelpContextID = 40919996


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=7, width=720,height=38,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=12,Field1="DICAUUIC",Label1="Caus.",Field2="DIIMPORT",Label2="Importo",Field3="DITARDOG",Label3="Tariffa doganale",Field4="DITIPCVS2",Label4="Tipo CVS",Field5="DITIPCVS1",Label5="Tipo CVS",Field6="DISPETRA",Label6="Spese trasporto e assicuraz.",Field7="DINUMCVS",Label7="Numero CVS",Field8="DIDATCVS",Label8="Data CVS",Field9="DIDATSDO",Label9="   Data prest.",Field10="DIBANEMI",Label10="Banca emitt.",Field11="DICVSDEC",Label11="CVS decan.",Field12="DITIPRES",Label12="Tipo resa",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218953082

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=46,;
    width=716+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3999999999999999)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=47,width=715+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3999999999999999)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='COD_ABI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='COD_ABI'
        oDropInto=this.oBodyCol.oRow.oDIBANEMI_2_8
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgste_mcvBodyRow as CPBodyRowCnt
  Width=706
  Height=int(fontmetric(1,"Arial",9,"")*2*1.3999999999999999)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDICAUUIC_2_1 as StdTrsField with uid="TBMPSJJNCE",rtseq=5,rtrep=.t.,;
    cFormVar="w_DICAUUIC",value=space(5),;
    ToolTipText = "Causale valutaria UIC",;
    HelpContextID = 188003975,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, InputMask=replicate('X',5)

  add object oDIIMPORT_2_3 as StdTrsField with uid="FINOTKCXBZ",rtseq=7,rtrep=.t.,;
    cFormVar="w_DIIMPORT",value=0,;
    ToolTipText = "Importo CVS",;
    HelpContextID = 243771786,;
    cTotal = "this.Parent.oContained.w_totrig", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=51, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oDIIMPORT_2_3.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUUIC))
    endwith
  endfunc

  add object oDITIPCVS2_2_4 as StdTrsCombo with uid="ARFBGNDSEF",rtrep=.t.,;
    cFormVar="w_DITIPCVS2", RowSource=""+"Emissione e regol.di CVS,"+"Inferiore a limite CVS,"+"Soggetto non residente,"+"Sola causale valutaria,"+"Cessione divisa tra residenti,"+"Regol. di CVS gi� emessa" , ;
    HelpContextID = 42228905,;
    Height=22, Width=170, Left=202, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDITIPCVS2_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DITIPCVS2,&i_cF..t_DITIPCVS2),this.value)
    return(iif(xVal =1,'EMI',;
    iif(xVal =2,'INF',;
    iif(xVal =3,'SNR',;
    iif(xVal =4,'CVA',;
    iif(xVal =5,'CDV',;
    iif(xVal =6,'REG',;
    space(3))))))))
  endfunc
  func oDITIPCVS2_2_4.GetRadio()
    this.Parent.oContained.w_DITIPCVS2 = this.RadioValue()
    return .t.
  endfunc

  func oDITIPCVS2_2_4.ToRadio()
    this.Parent.oContained.w_DITIPCVS2=trim(this.Parent.oContained.w_DITIPCVS2)
    return(;
      iif(this.Parent.oContained.w_DITIPCVS2=='EMI',1,;
      iif(this.Parent.oContained.w_DITIPCVS2=='INF',2,;
      iif(this.Parent.oContained.w_DITIPCVS2=='SNR',3,;
      iif(this.Parent.oContained.w_DITIPCVS2=='CVA',4,;
      iif(this.Parent.oContained.w_DITIPCVS2=='CDV',5,;
      iif(this.Parent.oContained.w_DITIPCVS2=='REG',6,;
      0)))))))
  endfunc

  func oDITIPCVS2_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDITIPCVS2_2_4.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUUIC))
    endwith
  endfunc

  func oDITIPCVS2_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COMBOVECCHIA=.F.)
    endwith
    endif
  endfunc

  add object oDITIPCVS1_2_5 as StdTrsCombo with uid="VWHAGIOMLE",rtrep=.t.,;
    cFormVar="w_DITIPCVS1", RowSource=""+"Emissione e regol.di CVS,"+"Inferiore a limite CVS,"+"Soggetto non residente,"+"Sola causale valutaria,"+"Cessione divisa tra residenti" , ;
    HelpContextID = 42228889,;
    Height=22, Width=170, Left=202, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDITIPCVS1_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DITIPCVS1,&i_cF..t_DITIPCVS1),this.value)
    return(iif(xVal =1,'EMI',;
    iif(xVal =2,'INF',;
    iif(xVal =3,'SNR',;
    iif(xVal =4,'CVA',;
    iif(xVal =5,'CDV',;
    space(3)))))))
  endfunc
  func oDITIPCVS1_2_5.GetRadio()
    this.Parent.oContained.w_DITIPCVS1 = this.RadioValue()
    return .t.
  endfunc

  func oDITIPCVS1_2_5.ToRadio()
    this.Parent.oContained.w_DITIPCVS1=trim(this.Parent.oContained.w_DITIPCVS1)
    return(;
      iif(this.Parent.oContained.w_DITIPCVS1=='EMI',1,;
      iif(this.Parent.oContained.w_DITIPCVS1=='INF',2,;
      iif(this.Parent.oContained.w_DITIPCVS1=='SNR',3,;
      iif(this.Parent.oContained.w_DITIPCVS1=='CVA',4,;
      iif(this.Parent.oContained.w_DITIPCVS1=='CDV',5,;
      0))))))
  endfunc

  func oDITIPCVS1_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDITIPCVS1_2_5.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUUIC))
    endwith
  endfunc

  func oDITIPCVS1_2_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COMBOVECCHIA=.T.)
    endwith
    endif
  endfunc

  add object oDINUMCVS_2_6 as StdTrsField with uid="HVBRSFDIWE",rtseq=10,rtrep=.t.,;
    cFormVar="w_DINUMCVS",value=space(12),;
    ToolTipText = "Numero CVS",;
    HelpContextID = 39844233,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=374, Top=0, InputMask=replicate('X',12)

  func oDINUMCVS_2_6.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUUIC))
    endwith
  endfunc

  add object oDIDATCVS_2_7 as StdTrsField with uid="BYMGAJOENF",rtseq=11,rtrep=.t.,;
    cFormVar="w_DIDATCVS",value=ctod("  /  /  "),;
    ToolTipText = "Data CVS",;
    HelpContextID = 45832585,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=476, Top=0

  func oDIDATCVS_2_7.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUUIC))
    endwith
  endfunc

  add object oDIBANEMI_2_8 as StdTrsField with uid="GNLNYPQOOQ",rtseq=12,rtrep=.t.,;
    cFormVar="w_DIBANEMI",value=space(5),;
    ToolTipText = "Banca emittente",;
    HelpContextID = 195348097,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=574, Top=-1, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_ABI", cZoomOnZoom="GSAR_ABI", oKey_1_1="ABCODABI", oKey_1_2="this.w_DIBANEMI"

  func oDIBANEMI_2_8.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUUIC))
    endwith
  endfunc

  func oDIBANEMI_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIBANEMI_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDIBANEMI_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_ABI','*','ABCODABI',cp_AbsName(this.parent,'oDIBANEMI_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABI',"Elenco codici ABI",'',this.parent.oContained
  endproc
  proc oDIBANEMI_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ABCODABI=this.parent.oContained.w_DIBANEMI
    i_obj.ecpSave()
  endproc

  add object oDICVSDEC_2_9 as StdTrsCombo with uid="ECDNGUHDVJ",rtrep=.t.,;
    cFormVar="w_DICVSDEC", RowSource=""+"Nessuna,"+"S�,"+"No" , ;
    ToolTipText = "CVS decanalizzata",;
    HelpContextID = 62933369,;
    Height=21, Width=74, Left=627, Top=1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDICVSDEC_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DICVSDEC,&i_cF..t_DICVSDEC),this.value)
    return(iif(xVal =1,' ',;
    iif(xVal =2,'D',;
    iif(xVal =3,'N',;
    space(1)))))
  endfunc
  func oDICVSDEC_2_9.GetRadio()
    this.Parent.oContained.w_DICVSDEC = this.RadioValue()
    return .t.
  endfunc

  func oDICVSDEC_2_9.ToRadio()
    this.Parent.oContained.w_DICVSDEC=trim(this.Parent.oContained.w_DICVSDEC)
    return(;
      iif(this.Parent.oContained.w_DICVSDEC=='',1,;
      iif(this.Parent.oContained.w_DICVSDEC=='D',2,;
      iif(this.Parent.oContained.w_DICVSDEC=='N',3,;
      0))))
  endfunc

  func oDICVSDEC_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDICVSDEC_2_9.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUUIC))
    endwith
  endfunc

  add object oDITARDOG_2_10 as StdTrsField with uid="YBFKNLUMGK",rtseq=14,rtrep=.t.,;
    cFormVar="w_DITARDOG",value=0,;
    HelpContextID = 60578173,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=51, Top=21, cSayPict=['9999999'], cGetPict=['9999999']

  func oDITARDOG_2_10.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUUIC))
    endwith
  endfunc

  add object oDISPETRA_2_11 as StdTrsField with uid="NAZLBQNPPV",rtseq=15,rtrep=.t.,;
    cFormVar="w_DISPETRA",value=0,;
    HelpContextID = 47925623,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=220, Top=21, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oDISPETRA_2_11.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUUIC))
    endwith
  endfunc

  add object oDIDATSDO_2_12 as StdTrsField with uid="MTLCCVXUIG",rtseq=16,rtrep=.t.,;
    cFormVar="w_DIDATSDO",value=ctod("  /  /  "),;
    ToolTipText = "Data sdoganamento",;
    HelpContextID = 45832581,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=476, Top=21

  func oDIDATSDO_2_12.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUUIC))
    endwith
  endfunc

  add object oDITIPRES_2_13 as StdTrsCombo with uid="RTCYLBFKKQ",rtrep=.t.,;
    cFormVar="w_DITIPRES", RowSource=""+"Nessuna,"+"CIF,"+"FOB" , ;
    ToolTipText = "Indicatore tipo resa merce",;
    HelpContextID = 25450889,;
    Height=21, Width=74, Left=627, Top=22,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDITIPRES_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DITIPRES,&i_cF..t_DITIPRES),this.value)
    return(iif(xVal =1,' ',;
    iif(xVal =2,'C',;
    iif(xVal =3,'F',;
    space(1)))))
  endfunc
  func oDITIPRES_2_13.GetRadio()
    this.Parent.oContained.w_DITIPRES = this.RadioValue()
    return .t.
  endfunc

  func oDITIPRES_2_13.ToRadio()
    this.Parent.oContained.w_DITIPRES=trim(this.Parent.oContained.w_DITIPRES)
    return(;
      iif(this.Parent.oContained.w_DITIPRES=='',1,;
      iif(this.Parent.oContained.w_DITIPRES=='C',2,;
      iif(this.Parent.oContained.w_DITIPRES=='F',3,;
      0))))
  endfunc

  func oDITIPRES_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDITIPRES_2_13.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICAUUIC))
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oDICAUUIC_2_1.When()
    return(.t.)
  proc oDICAUUIC_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDICAUUIC_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_mcv','DIS_BECV','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DINUMDIS=DIS_BECV.DINUMDIS";
  +" and "+i_cAliasName2+".DIROWORD=DIS_BECV.DIROWORD";
  +" and "+i_cAliasName2+".DIROWNUM=DIS_BECV.DIROWNUM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
