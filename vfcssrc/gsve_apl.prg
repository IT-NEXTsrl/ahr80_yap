* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_apl                                                        *
*              Manutenzione provvigioni liquidate                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_33]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2013-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_apl"))

* --- Class definition
define class tgsve_apl as StdForm
  Top    = 9
  Left   = 57

  * --- Standard Properties
  Width  = 583
  Height = 232+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-03-26"
  HelpContextID=108383639
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=33

  * --- Constant Properties
  PRO_LIQU_IDX = 0
  AGENTI_IDX = 0
  VALUTE_IDX = 0
  cFile = "PRO_LIQU"
  cKeySelect = "PL__ANNO,PLNUMREG"
  cKeyWhere  = "PL__ANNO=this.w_PL__ANNO and PLNUMREG=this.w_PLNUMREG"
  cKeyWhereODBC = '"PL__ANNO="+cp_ToStrODBC(this.w_PL__ANNO)';
      +'+" and PLNUMREG="+cp_ToStrODBC(this.w_PLNUMREG)';

  cKeyWhereODBCqualified = '"PRO_LIQU.PL__ANNO="+cp_ToStrODBC(this.w_PL__ANNO)';
      +'+" and PRO_LIQU.PLNUMREG="+cp_ToStrODBC(this.w_PLNUMREG)';

  cPrg = "gsve_apl"
  cComment = "Manutenzione provvigioni liquidate"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_PL__ANNO = space(4)
  w_PLNUMREG = 0
  w_PLDATREG = ctod('  /  /  ')
  o_PLDATREG = ctod('  /  /  ')
  w_PLCODAGE = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_PLDATINI = ctod('  /  /  ')
  w_PLDATFIN = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_PLCODVAL = space(3)
  o_PLCODVAL = space(3)
  w_PLCAOVAL = 0
  w_CAOVAL = 0
  w_PLTOTIMP = 0
  w_PLRIFAGE = space(5)
  w_PLTOTPRO = 0
  w_PLNUMNOT = 0
  w_PLALFNOT = space(2)
  w_PLDATNOT = ctod('  /  /  ')
  w_PLDATPAG = ctod('  /  /  ')
  w_DESAGE = space(35)
  w_DESAPP = space(35)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_PLSERTRI = space(10)
  w_PLSERFIR = space(10)
  w_DTOBSO = ctod('  /  /  ')
  w_PLVALNOT = space(3)
  o_PLVALNOT = space(3)
  w_DECT1 = 0
  w_CALCPICT2 = 0
  w_PLIMPNOT = 0
  w_PLCONTRI = 0
  w_DTOBS1 = ctod('  /  /  ')
  w_DESRIF = space(35)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PL__ANNO = this.W_PL__ANNO
  op_PLNUMREG = this.W_PLNUMREG
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PRO_LIQU','gsve_apl')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_aplPag1","gsve_apl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Provvigione liquidata")
      .Pages(1).HelpContextID = 37456556
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPL__ANNO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='AGENTI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='PRO_LIQU'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRO_LIQU_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRO_LIQU_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PL__ANNO = NVL(PL__ANNO,space(4))
      .w_PLNUMREG = NVL(PLNUMREG,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_40_joined
    link_1_40_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PRO_LIQU where PL__ANNO=KeySet.PL__ANNO
    *                            and PLNUMREG=KeySet.PLNUMREG
    *
    i_nConn = i_TableProp[this.PRO_LIQU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_LIQU_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRO_LIQU')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRO_LIQU.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PRO_LIQU '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_40_joined=this.AddJoinedLink_1_40(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PL__ANNO',this.w_PL__ANNO  ,'PLNUMREG',this.w_PLNUMREG  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_codazi
        .w_DATOBSO = ctod("  /  /  ")
        .w_CAOVAL = 0
        .w_DESAGE = space(35)
        .w_DESAPP = space(35)
        .w_DECTOT = 0
        .w_DTOBSO = ctod("  /  /  ")
        .w_DECT1 = 0
        .w_DTOBS1 = ctod("  /  /  ")
        .w_DESRIF = space(35)
        .w_PL__ANNO = NVL(PL__ANNO,space(4))
        .op_PL__ANNO = .w_PL__ANNO
        .w_PLNUMREG = NVL(PLNUMREG,0)
        .op_PLNUMREG = .w_PLNUMREG
        .w_PLDATREG = NVL(cp_ToDate(PLDATREG),ctod("  /  /  "))
        .w_PLCODAGE = NVL(PLCODAGE,space(5))
          if link_1_5_joined
            this.w_PLCODAGE = NVL(AGCODAGE105,NVL(this.w_PLCODAGE,space(5)))
            this.w_DESAGE = NVL(AGDESAGE105,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(AGDTOBSO105),ctod("  /  /  "))
          else
          .link_1_5('Load')
          endif
        .w_PLDATINI = NVL(cp_ToDate(PLDATINI),ctod("  /  /  "))
        .w_PLDATFIN = NVL(cp_ToDate(PLDATFIN),ctod("  /  /  "))
        .w_OBTEST = .w_PLDATFIN
        .w_PLCODVAL = NVL(PLCODVAL,space(3))
          if link_1_10_joined
            this.w_PLCODVAL = NVL(VACODVAL110,NVL(this.w_PLCODVAL,space(3)))
            this.w_DESAPP = NVL(VADESVAL110,space(35))
            this.w_DECTOT = NVL(VADECTOT110,0)
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO110),ctod("  /  /  "))
            this.w_CAOVAL = NVL(VACAOVAL110,0)
          else
          .link_1_10('Load')
          endif
        .w_PLCAOVAL = NVL(PLCAOVAL,0)
        .w_PLTOTIMP = NVL(PLTOTIMP,0)
        .w_PLRIFAGE = NVL(PLRIFAGE,space(5))
          if link_1_14_joined
            this.w_PLRIFAGE = NVL(AGCODAGE114,NVL(this.w_PLRIFAGE,space(5)))
            this.w_DESRIF = NVL(AGDESAGE114,space(35))
          else
          .link_1_14('Load')
          endif
        .w_PLTOTPRO = NVL(PLTOTPRO,0)
        .w_PLNUMNOT = NVL(PLNUMNOT,0)
        .w_PLALFNOT = NVL(PLALFNOT,space(2))
        .w_PLDATNOT = NVL(cp_ToDate(PLDATNOT),ctod("  /  /  "))
        .w_PLDATPAG = NVL(cp_ToDate(PLDATPAG),ctod("  /  /  "))
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_PLSERTRI = NVL(PLSERTRI,space(10))
        .w_PLSERFIR = NVL(PLSERFIR,space(10))
        .w_PLVALNOT = NVL(PLVALNOT,space(3))
          if link_1_40_joined
            this.w_PLVALNOT = NVL(VACODVAL140,NVL(this.w_PLVALNOT,space(3)))
            this.w_DECT1 = NVL(VADECTOT140,0)
            this.w_DTOBS1 = NVL(cp_ToDate(VADTOBSO140),ctod("  /  /  "))
          else
          .link_1_40('Load')
          endif
        .w_CALCPICT2 = DEFPIP(.w_DECT1)
        .w_PLIMPNOT = NVL(PLIMPNOT,0)
        .w_PLCONTRI = NVL(PLCONTRI,0)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'PRO_LIQU')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_PL__ANNO = space(4)
      .w_PLNUMREG = 0
      .w_PLDATREG = ctod("  /  /  ")
      .w_PLCODAGE = space(5)
      .w_DATOBSO = ctod("  /  /  ")
      .w_PLDATINI = ctod("  /  /  ")
      .w_PLDATFIN = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_PLCODVAL = space(3)
      .w_PLCAOVAL = 0
      .w_CAOVAL = 0
      .w_PLTOTIMP = 0
      .w_PLRIFAGE = space(5)
      .w_PLTOTPRO = 0
      .w_PLNUMNOT = 0
      .w_PLALFNOT = space(2)
      .w_PLDATNOT = ctod("  /  /  ")
      .w_PLDATPAG = ctod("  /  /  ")
      .w_DESAGE = space(35)
      .w_DESAPP = space(35)
      .w_DECTOT = 0
      .w_CALCPICT = 0
      .w_PLSERTRI = space(10)
      .w_PLSERFIR = space(10)
      .w_DTOBSO = ctod("  /  /  ")
      .w_PLVALNOT = space(3)
      .w_DECT1 = 0
      .w_CALCPICT2 = 0
      .w_PLIMPNOT = 0
      .w_PLCONTRI = 0
      .w_DTOBS1 = ctod("  /  /  ")
      .w_DESRIF = space(35)
      if .cFunction<>"Filter"
        .w_CODAZI = i_codazi
        .w_PL__ANNO = STR(YEAR(IIF(EMPTY(.w_PLDATREG), i_datsys, .w_PLDATREG)),4,0)
          .DoRTCalc(3,3,.f.)
        .w_PLDATREG = i_datsys
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_PLCODAGE))
          .link_1_5('Full')
          endif
          .DoRTCalc(6,8,.f.)
        .w_OBTEST = .w_PLDATFIN
        .w_PLCODVAL = g_perval
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_PLCODVAL))
          .link_1_10('Full')
          endif
        .w_PLCAOVAL = GETCAM(.w_PLCODVAL, .w_PLDATREG,0)
        .DoRTCalc(12,14,.f.)
          if not(empty(.w_PLRIFAGE))
          .link_1_14('Full')
          endif
          .DoRTCalc(15,22,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(24,27,.f.)
          if not(empty(.w_PLVALNOT))
          .link_1_40('Full')
          endif
          .DoRTCalc(28,28,.f.)
        .w_CALCPICT2 = DEFPIP(.w_DECT1)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRO_LIQU')
    this.DoRTCalc(30,33,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsve_apl
    local L_CtrlNum, L_CtrlAnno, L_TabIdxCtrl
    L_CtrlNum=this.getctrl("w_PLNUMREG")
    L_CtrlAnno=this.getctrl("w_PL__ANNO")
    If L_CtrlAnno.TabIndex<L_CtrlNum.TabIndex
       L_TabIdxCtrl = L_CtrlNum.TabIndex
       L_CtrlNum.SetFocus()
       L_CtrlNum.TabIndex=L_CtrlAnno.TabIndex
       L_CtrlAnno.TabIndex=L_TabIdxCtrl
    endif
    L_CtrlNum=.null.
    L_CtrlAnno=.null.
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRO_LIQU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_LIQU_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"PRLIQ","i_codazi,w_PL__ANNO,w_PLNUMREG")
      .op_codazi = .w_codazi
      .op_PL__ANNO = .w_PL__ANNO
      .op_PLNUMREG = .w_PLNUMREG
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPL__ANNO_1_2.enabled = !i_bVal
      .Page1.oPag.oPLNUMREG_1_3.enabled = i_bVal
      .Page1.oPag.oPLDATREG_1_4.enabled = i_bVal
      .Page1.oPag.oPLCODAGE_1_5.enabled = i_bVal
      .Page1.oPag.oPLDATINI_1_7.enabled = i_bVal
      .Page1.oPag.oPLDATFIN_1_8.enabled = i_bVal
      .Page1.oPag.oPLCODVAL_1_10.enabled = i_bVal
      .Page1.oPag.oPLCAOVAL_1_11.enabled = i_bVal
      .Page1.oPag.oPLTOTIMP_1_13.enabled = i_bVal
      .Page1.oPag.oPLRIFAGE_1_14.enabled = i_bVal
      .Page1.oPag.oPLTOTPRO_1_15.enabled = i_bVal
      .Page1.oPag.oPLNUMNOT_1_16.enabled = i_bVal
      .Page1.oPag.oPLALFNOT_1_17.enabled = i_bVal
      .Page1.oPag.oPLDATNOT_1_18.enabled = i_bVal
      .Page1.oPag.oPLDATPAG_1_19.enabled = i_bVal
      .Page1.oPag.oPLVALNOT_1_40.enabled = i_bVal
      .Page1.oPag.oPLIMPNOT_1_43.enabled = i_bVal
      .Page1.oPag.oObj_1_50.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPLNUMREG_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPLNUMREG_1_3.enabled = .t.
        .Page1.oPag.oPLCODAGE_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PRO_LIQU',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRO_LIQU_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PL__ANNO,"PL__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLNUMREG,"PLNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLDATREG,"PLDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLCODAGE,"PLCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLDATINI,"PLDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLDATFIN,"PLDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLCODVAL,"PLCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLCAOVAL,"PLCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLTOTIMP,"PLTOTIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLRIFAGE,"PLRIFAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLTOTPRO,"PLTOTPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLNUMNOT,"PLNUMNOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLALFNOT,"PLALFNOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLDATNOT,"PLDATNOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLDATPAG,"PLDATPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLSERTRI,"PLSERTRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLSERFIR,"PLSERFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLVALNOT,"PLVALNOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLIMPNOT,"PLIMPNOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLCONTRI,"PLCONTRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRO_LIQU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_LIQU_IDX,2])
    i_lTable = "PRO_LIQU"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PRO_LIQU_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRO_LIQU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_LIQU_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PRO_LIQU_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"PRLIQ","i_codazi,w_PL__ANNO,w_PLNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PRO_LIQU
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRO_LIQU')
        i_extval=cp_InsertValODBCExtFlds(this,'PRO_LIQU')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PL__ANNO,PLNUMREG,PLDATREG,PLCODAGE,PLDATINI"+;
                  ",PLDATFIN,PLCODVAL,PLCAOVAL,PLTOTIMP,PLRIFAGE"+;
                  ",PLTOTPRO,PLNUMNOT,PLALFNOT,PLDATNOT,PLDATPAG"+;
                  ",PLSERTRI,PLSERFIR,PLVALNOT,PLIMPNOT,PLCONTRI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PL__ANNO)+;
                  ","+cp_ToStrODBC(this.w_PLNUMREG)+;
                  ","+cp_ToStrODBC(this.w_PLDATREG)+;
                  ","+cp_ToStrODBCNull(this.w_PLCODAGE)+;
                  ","+cp_ToStrODBC(this.w_PLDATINI)+;
                  ","+cp_ToStrODBC(this.w_PLDATFIN)+;
                  ","+cp_ToStrODBCNull(this.w_PLCODVAL)+;
                  ","+cp_ToStrODBC(this.w_PLCAOVAL)+;
                  ","+cp_ToStrODBC(this.w_PLTOTIMP)+;
                  ","+cp_ToStrODBCNull(this.w_PLRIFAGE)+;
                  ","+cp_ToStrODBC(this.w_PLTOTPRO)+;
                  ","+cp_ToStrODBC(this.w_PLNUMNOT)+;
                  ","+cp_ToStrODBC(this.w_PLALFNOT)+;
                  ","+cp_ToStrODBC(this.w_PLDATNOT)+;
                  ","+cp_ToStrODBC(this.w_PLDATPAG)+;
                  ","+cp_ToStrODBC(this.w_PLSERTRI)+;
                  ","+cp_ToStrODBC(this.w_PLSERFIR)+;
                  ","+cp_ToStrODBCNull(this.w_PLVALNOT)+;
                  ","+cp_ToStrODBC(this.w_PLIMPNOT)+;
                  ","+cp_ToStrODBC(this.w_PLCONTRI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRO_LIQU')
        i_extval=cp_InsertValVFPExtFlds(this,'PRO_LIQU')
        cp_CheckDeletedKey(i_cTable,0,'PL__ANNO',this.w_PL__ANNO,'PLNUMREG',this.w_PLNUMREG)
        INSERT INTO (i_cTable);
              (PL__ANNO,PLNUMREG,PLDATREG,PLCODAGE,PLDATINI,PLDATFIN,PLCODVAL,PLCAOVAL,PLTOTIMP,PLRIFAGE,PLTOTPRO,PLNUMNOT,PLALFNOT,PLDATNOT,PLDATPAG,PLSERTRI,PLSERFIR,PLVALNOT,PLIMPNOT,PLCONTRI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PL__ANNO;
                  ,this.w_PLNUMREG;
                  ,this.w_PLDATREG;
                  ,this.w_PLCODAGE;
                  ,this.w_PLDATINI;
                  ,this.w_PLDATFIN;
                  ,this.w_PLCODVAL;
                  ,this.w_PLCAOVAL;
                  ,this.w_PLTOTIMP;
                  ,this.w_PLRIFAGE;
                  ,this.w_PLTOTPRO;
                  ,this.w_PLNUMNOT;
                  ,this.w_PLALFNOT;
                  ,this.w_PLDATNOT;
                  ,this.w_PLDATPAG;
                  ,this.w_PLSERTRI;
                  ,this.w_PLSERFIR;
                  ,this.w_PLVALNOT;
                  ,this.w_PLIMPNOT;
                  ,this.w_PLCONTRI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PRO_LIQU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_LIQU_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PRO_LIQU_IDX,i_nConn)
      *
      * update PRO_LIQU
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_LIQU')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PLDATREG="+cp_ToStrODBC(this.w_PLDATREG)+;
             ",PLCODAGE="+cp_ToStrODBCNull(this.w_PLCODAGE)+;
             ",PLDATINI="+cp_ToStrODBC(this.w_PLDATINI)+;
             ",PLDATFIN="+cp_ToStrODBC(this.w_PLDATFIN)+;
             ",PLCODVAL="+cp_ToStrODBCNull(this.w_PLCODVAL)+;
             ",PLCAOVAL="+cp_ToStrODBC(this.w_PLCAOVAL)+;
             ",PLTOTIMP="+cp_ToStrODBC(this.w_PLTOTIMP)+;
             ",PLRIFAGE="+cp_ToStrODBCNull(this.w_PLRIFAGE)+;
             ",PLTOTPRO="+cp_ToStrODBC(this.w_PLTOTPRO)+;
             ",PLNUMNOT="+cp_ToStrODBC(this.w_PLNUMNOT)+;
             ",PLALFNOT="+cp_ToStrODBC(this.w_PLALFNOT)+;
             ",PLDATNOT="+cp_ToStrODBC(this.w_PLDATNOT)+;
             ",PLDATPAG="+cp_ToStrODBC(this.w_PLDATPAG)+;
             ",PLSERTRI="+cp_ToStrODBC(this.w_PLSERTRI)+;
             ",PLSERFIR="+cp_ToStrODBC(this.w_PLSERFIR)+;
             ",PLVALNOT="+cp_ToStrODBCNull(this.w_PLVALNOT)+;
             ",PLIMPNOT="+cp_ToStrODBC(this.w_PLIMPNOT)+;
             ",PLCONTRI="+cp_ToStrODBC(this.w_PLCONTRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_LIQU')
        i_cWhere = cp_PKFox(i_cTable  ,'PL__ANNO',this.w_PL__ANNO  ,'PLNUMREG',this.w_PLNUMREG  )
        UPDATE (i_cTable) SET;
              PLDATREG=this.w_PLDATREG;
             ,PLCODAGE=this.w_PLCODAGE;
             ,PLDATINI=this.w_PLDATINI;
             ,PLDATFIN=this.w_PLDATFIN;
             ,PLCODVAL=this.w_PLCODVAL;
             ,PLCAOVAL=this.w_PLCAOVAL;
             ,PLTOTIMP=this.w_PLTOTIMP;
             ,PLRIFAGE=this.w_PLRIFAGE;
             ,PLTOTPRO=this.w_PLTOTPRO;
             ,PLNUMNOT=this.w_PLNUMNOT;
             ,PLALFNOT=this.w_PLALFNOT;
             ,PLDATNOT=this.w_PLDATNOT;
             ,PLDATPAG=this.w_PLDATPAG;
             ,PLSERTRI=this.w_PLSERTRI;
             ,PLSERFIR=this.w_PLSERFIR;
             ,PLVALNOT=this.w_PLVALNOT;
             ,PLIMPNOT=this.w_PLIMPNOT;
             ,PLCONTRI=this.w_PLCONTRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PRO_LIQU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_LIQU_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PRO_LIQU_IDX,i_nConn)
      *
      * delete PRO_LIQU
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PL__ANNO',this.w_PL__ANNO  ,'PLNUMREG',this.w_PLNUMREG  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRO_LIQU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_LIQU_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_PLDATREG<>.w_PLDATREG
            .w_PL__ANNO = STR(YEAR(IIF(EMPTY(.w_PLDATREG), i_datsys, .w_PLDATREG)),4,0)
        endif
        .DoRTCalc(3,8,.t.)
            .w_OBTEST = .w_PLDATFIN
        .DoRTCalc(10,10,.t.)
        if .o_PLCODVAL<>.w_PLCODVAL.or. .o_PLDATREG<>.w_PLDATREG
            .w_PLCAOVAL = GETCAM(.w_PLCODVAL, .w_PLDATREG,0)
        endif
        .DoRTCalc(12,22,.t.)
        if .o_PLCODVAL<>.w_PLCODVAL
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(24,28,.t.)
        if .o_PLVALNOT<>.w_PLVALNOT
            .w_CALCPICT2 = DEFPIP(.w_DECT1)
        endif
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi .or. .op_PL__ANNO<>.w_PL__ANNO
           cp_AskTableProg(this,i_nConn,"PRLIQ","i_codazi,w_PL__ANNO,w_PLNUMREG")
          .op_PLNUMREG = .w_PLNUMREG
        endif
        .op_codazi = .w_codazi
        .op_PL__ANNO = .w_PL__ANNO
      endwith
      this.DoRTCalc(30,33,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPLCAOVAL_1_11.enabled = this.oPgFrm.Page1.oPag.oPLCAOVAL_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPLCAOVAL_1_11.visible=!this.oPgFrm.Page1.oPag.oPLCAOVAL_1_11.mHide()
    this.oPgFrm.Page1.oPag.oPLRIFAGE_1_14.visible=!this.oPgFrm.Page1.oPag.oPLRIFAGE_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oDESRIF_1_52.visible=!this.oPgFrm.Page1.oPag.oDESRIF_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PLCODAGE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_PLCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_PLCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_PLCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_PLCODAGE)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PLCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oPLCODAGE_1_5'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PLCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PLCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PLCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Agente inesistente oppure obsoleto")
        endif
        this.w_PLCODAGE = space(5)
        this.w_DESAGE = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.AGCODAGE as AGCODAGE105"+ ",link_1_5.AGDESAGE as AGDESAGE105"+ ",link_1_5.AGDTOBSO as AGDTOBSO105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on PRO_LIQU.PLCODAGE=link_1_5.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and PRO_LIQU.PLCODAGE=link_1_5.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PLCODVAL
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_PLCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VACAOVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_PLCODVAL))
          select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VACAOVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_PLCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_PLCODVAL)+"%");

            select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PLCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oPLCODVAL_1_10'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VACAOVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PLCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PLCODVAL)
            select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_PLCODVAL = space(3)
      endif
      this.w_DESAPP = space(35)
      this.w_DECTOT = 0
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_CAOVAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
        endif
        this.w_PLCODVAL = space(3)
        this.w_DESAPP = space(35)
        this.w_DECTOT = 0
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_CAOVAL = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.VACODVAL as VACODVAL110"+ ",link_1_10.VADESVAL as VADESVAL110"+ ",link_1_10.VADECTOT as VADECTOT110"+ ",link_1_10.VADTOBSO as VADTOBSO110"+ ",link_1_10.VACAOVAL as VACAOVAL110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on PRO_LIQU.PLCODVAL=link_1_10.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and PRO_LIQU.PLCODVAL=link_1_10.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PLRIFAGE
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLRIFAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_PLRIFAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_PLRIFAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLRIFAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PLRIFAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oPLRIFAGE_1_14'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLRIFAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PLRIFAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PLRIFAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLRIFAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESRIF = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PLRIFAGE = space(5)
      endif
      this.w_DESRIF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLRIFAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.AGCODAGE as AGCODAGE114"+ ",link_1_14.AGDESAGE as AGDESAGE114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on PRO_LIQU.PLRIFAGE=link_1_14.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and PRO_LIQU.PLRIFAGE=link_1_14.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PLVALNOT
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLVALNOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_PLVALNOT)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_PLVALNOT))
          select VACODVAL,VADECTOT,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLVALNOT)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PLVALNOT) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oPLVALNOT_1_40'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLVALNOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PLVALNOT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PLVALNOT)
            select VACODVAL,VADECTOT,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLVALNOT = NVL(_Link_.VACODVAL,space(3))
      this.w_DECT1 = NVL(_Link_.VADECTOT,0)
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PLVALNOT = space(3)
      endif
      this.w_DECT1 = 0
      this.w_DTOBS1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBS1,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
        endif
        this.w_PLVALNOT = space(3)
        this.w_DECT1 = 0
        this.w_DTOBS1 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLVALNOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_40(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_40.VACODVAL as VACODVAL140"+ ",link_1_40.VADECTOT as VADECTOT140"+ ",link_1_40.VADTOBSO as VADTOBSO140"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_40 on PRO_LIQU.PLVALNOT=link_1_40.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_40"
          i_cKey=i_cKey+'+" and PRO_LIQU.PLVALNOT=link_1_40.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPL__ANNO_1_2.value==this.w_PL__ANNO)
      this.oPgFrm.Page1.oPag.oPL__ANNO_1_2.value=this.w_PL__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oPLNUMREG_1_3.value==this.w_PLNUMREG)
      this.oPgFrm.Page1.oPag.oPLNUMREG_1_3.value=this.w_PLNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPLDATREG_1_4.value==this.w_PLDATREG)
      this.oPgFrm.Page1.oPag.oPLDATREG_1_4.value=this.w_PLDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPLCODAGE_1_5.value==this.w_PLCODAGE)
      this.oPgFrm.Page1.oPag.oPLCODAGE_1_5.value=this.w_PLCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oPLDATINI_1_7.value==this.w_PLDATINI)
      this.oPgFrm.Page1.oPag.oPLDATINI_1_7.value=this.w_PLDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPLDATFIN_1_8.value==this.w_PLDATFIN)
      this.oPgFrm.Page1.oPag.oPLDATFIN_1_8.value=this.w_PLDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPLCODVAL_1_10.value==this.w_PLCODVAL)
      this.oPgFrm.Page1.oPag.oPLCODVAL_1_10.value=this.w_PLCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPLCAOVAL_1_11.value==this.w_PLCAOVAL)
      this.oPgFrm.Page1.oPag.oPLCAOVAL_1_11.value=this.w_PLCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPLTOTIMP_1_13.value==this.w_PLTOTIMP)
      this.oPgFrm.Page1.oPag.oPLTOTIMP_1_13.value=this.w_PLTOTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oPLRIFAGE_1_14.value==this.w_PLRIFAGE)
      this.oPgFrm.Page1.oPag.oPLRIFAGE_1_14.value=this.w_PLRIFAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oPLTOTPRO_1_15.value==this.w_PLTOTPRO)
      this.oPgFrm.Page1.oPag.oPLTOTPRO_1_15.value=this.w_PLTOTPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPLNUMNOT_1_16.value==this.w_PLNUMNOT)
      this.oPgFrm.Page1.oPag.oPLNUMNOT_1_16.value=this.w_PLNUMNOT
    endif
    if not(this.oPgFrm.Page1.oPag.oPLALFNOT_1_17.value==this.w_PLALFNOT)
      this.oPgFrm.Page1.oPag.oPLALFNOT_1_17.value=this.w_PLALFNOT
    endif
    if not(this.oPgFrm.Page1.oPag.oPLDATNOT_1_18.value==this.w_PLDATNOT)
      this.oPgFrm.Page1.oPag.oPLDATNOT_1_18.value=this.w_PLDATNOT
    endif
    if not(this.oPgFrm.Page1.oPag.oPLDATPAG_1_19.value==this.w_PLDATPAG)
      this.oPgFrm.Page1.oPag.oPLDATPAG_1_19.value=this.w_PLDATPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_32.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_32.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oPLVALNOT_1_40.value==this.w_PLVALNOT)
      this.oPgFrm.Page1.oPag.oPLVALNOT_1_40.value=this.w_PLVALNOT
    endif
    if not(this.oPgFrm.Page1.oPag.oPLIMPNOT_1_43.value==this.w_PLIMPNOT)
      this.oPgFrm.Page1.oPag.oPLIMPNOT_1_43.value=this.w_PLIMPNOT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIF_1_52.value==this.w_DESRIF)
      this.oPgFrm.Page1.oPag.oDESRIF_1_52.value=this.w_DESRIF
    endif
    cp_SetControlsValueExtFlds(this,'PRO_LIQU')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PLDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPLDATREG_1_4.SetFocus()
            i_bnoObbl = !empty(.w_PLDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(empty(.w_PLCODAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPLCODAGE_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Agente inesistente oppure obsoleto")
          case   (empty(.w_PLDATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPLDATINI_1_7.SetFocus()
            i_bnoObbl = !empty(.w_PLDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PLDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPLDATFIN_1_8.SetFocus()
            i_bnoObbl = !empty(.w_PLDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PLCODVAL)) or not(CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPLCODVAL_1_10.SetFocus()
            i_bnoObbl = !empty(.w_PLCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
          case   (empty(.w_PLCAOVAL))  and not(.w_CAOVAL<>0)  and (.w_CAOVAL=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPLCAOVAL_1_11.SetFocus()
            i_bnoObbl = !empty(.w_PLCAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PLTOTIMP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPLTOTIMP_1_13.SetFocus()
            i_bnoObbl = !empty(.w_PLTOTIMP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PLTOTPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPLTOTPRO_1_15.SetFocus()
            i_bnoObbl = !empty(.w_PLTOTPRO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKDTOBS(.w_DTOBS1,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.))  and not(empty(.w_PLVALNOT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPLVALNOT_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PLDATREG = this.w_PLDATREG
    this.o_PLCODVAL = this.w_PLCODVAL
    this.o_PLVALNOT = this.w_PLVALNOT
    return

enddefine

* --- Define pages as container
define class tgsve_aplPag1 as StdContainer
  Width  = 579
  height = 233
  stdWidth  = 579
  stdheight = 233
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPL__ANNO_1_2 as StdField with uid="VMRWSKTENQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PL__ANNO", cQueryName = "PL__ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 149622597,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=203, Top=13, InputMask=replicate('X',4)

  add object oPLNUMREG_1_3 as StdField with uid="KEEMGUBPNN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PLNUMREG", cQueryName = "PL__ANNO,PLNUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di registrazione",;
    HelpContextID = 39846083,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=119, Top=13, cSayPict="'999999'", cGetPict="'999999'"

  add object oPLDATREG_1_4 as StdField with uid="YQCDLKNIUV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PLDATREG", cQueryName = "PLDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 33857731,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=360, Top=13

  add object oPLCODAGE_1_5 as StdField with uid="JOQDEYYNPX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PLCODAGE", cQueryName = "PLCODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Agente inesistente oppure obsoleto",;
    ToolTipText = "Codice agente selezionato",;
    HelpContextID = 66498757,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=119, Top=44, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_PLCODAGE"

  func oPLCODAGE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLCODAGE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPLCODAGE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oPLCODAGE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oPLCODAGE_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_PLCODAGE
     i_obj.ecpSave()
  endproc

  add object oPLDATINI_1_7 as StdField with uid="CALTSXATWB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PLDATINI", cQueryName = "PLDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio periodo",;
    HelpContextID = 83582783,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=119, Top=75

  add object oPLDATFIN_1_8 as StdField with uid="HSLRCBOQEK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PLDATFIN", cQueryName = "PLDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine periodo",;
    HelpContextID = 33251140,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=360, Top=75

  add object oPLCODVAL_1_10 as StdField with uid="ZBDPMGRCNG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PLCODVAL", cQueryName = "PLCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o incongruente o obsoleta",;
    ToolTipText = "Codice valuta selezionato",;
    HelpContextID = 251048126,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=119, Top=106, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_PLCODVAL"

  func oPLCODVAL_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLCODVAL_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPLCODVAL_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oPLCODVAL_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oPLCODVAL_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_PLCODVAL
     i_obj.ecpSave()
  endproc

  add object oPLCAOVAL_1_11 as StdField with uid="VJSOUCFVRI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PLCAOVAL", cQueryName = "PLCAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio di riferimento al documento (serve nel caso di valuta extraeuro)",;
    HelpContextID = 240431294,;
   bGlobalFont=.t.,;
    Height=21, Width=82, Left=250, Top=106, cSayPict='"99999.9999999"', cGetPict='"99999.9999999"'

  func oPLCAOVAL_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL=0)
    endwith
   endif
  endfunc

  func oPLCAOVAL_1_11.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0)
    endwith
  endfunc

  add object oPLTOTIMP_1_13 as StdField with uid="FMRKRCHDMB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PLTOTIMP", cQueryName = "PLTOTIMP",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imponibile selezionato",;
    HelpContextID = 84565830,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=448, Top=106, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oPLRIFAGE_1_14 as StdField with uid="GXNOLRZQDJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PLRIFAGE", cQueryName = "PLRIFAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento agente per il capoarea",;
    HelpContextID = 64733381,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=119, Top=135, InputMask=replicate('X',5), cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_PLRIFAGE"

  func oPLRIFAGE_1_14.mHide()
    with this.Parent.oContained
      return (empty(nvl(.w_PLRIFAGE,SPACE(5))))
    endwith
  endfunc

  func oPLRIFAGE_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLRIFAGE_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oPLTOTPRO_1_15 as StdField with uid="FKDYLKKARU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PLTOTPRO", cQueryName = "PLTOTPRO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale provvigione selezionata",;
    HelpContextID = 66429115,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=448, Top=137, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oPLNUMNOT_1_16 as StdField with uid="GQNQBVJNMG",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PLNUMNOT", cQueryName = "PLNUMNOT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero della nota selezionata",;
    HelpContextID = 161480522,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=119, Top=180, cSayPict='"999999"', cGetPict='"999999"'

  add object oPLALFNOT_1_17 as StdField with uid="XGGVFLNVZG",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PLALFNOT", cQueryName = "PLALFNOT",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Parte alfanumerica della nota",;
    HelpContextID = 153497418,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=203, Top=180, InputMask=replicate('X',2)

  add object oPLDATNOT_1_18 as StdField with uid="AGNLXMTISV",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PLDATNOT", cQueryName = "PLDATNOT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 167468874,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=275, Top=180

  add object oPLDATPAG_1_19 as StdField with uid="JHEKRCEHMK",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PLDATPAG", cQueryName = "PLDATPAG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di pagamento",;
    HelpContextID = 67412163,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=446, Top=180

  add object oDESAGE_1_32 as StdField with uid="LHEPULDRYE",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 265533642,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=185, Top=44, InputMask=replicate('X',35)

  add object oPLVALNOT_1_40 as StdField with uid="VRPPJZIMOV",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PLVALNOT", cQueryName = "PLVALNOT",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o incongruente o obsoleta",;
    ToolTipText = "Codice valuta della nota provvigioni",;
    HelpContextID = 159153994,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=119, Top=211, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_PLVALNOT"

  func oPLVALNOT_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLVALNOT_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPLVALNOT_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oPLVALNOT_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oPLVALNOT_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_PLVALNOT
     i_obj.ecpSave()
  endproc

  add object oPLIMPNOT_1_43 as StdField with uid="FLLRHXZTBZ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PLIMPNOT", cQueryName = "PLIMPNOT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Netto corrisposto nella nota provvigione (nella valuta dell'agente)",;
    HelpContextID = 164081482,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=446, Top=211, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"


  add object oObj_1_50 as cp_runprogram with uid="PCUYAOQYNW",left=10, top=249, width=161,height=19,;
    caption='GSVE_BDL',;
   bGlobalFont=.t.,;
    prg="GSVE_BDL",;
    cEvent = "Delete end",;
    nPag=1;
    , HelpContextID = 21985870

  add object oDESRIF_1_52 as StdField with uid="DNBIHAARRA",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESRIF", cQueryName = "DESRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 245545162,;
   bGlobalFont=.t.,;
    Height=21, Width=145, Left=187, Top=135, InputMask=replicate('X',35)

  func oDESRIF_1_52.mHide()
    with this.Parent.oContained
      return (empty(nvl(.w_PLRIFAGE,SPACE(5))))
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="AQSMCXVBOO",Visible=.t., Left=7, Top=13,;
    Alignment=1, Width=111, Height=15,;
    Caption="Registrazione n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="GBDABYUHWF",Visible=.t., Left=192, Top=13,;
    Alignment=0, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="BHWJPCTXJM",Visible=.t., Left=311, Top=13,;
    Alignment=1, Width=47, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="KSEFDXJUFH",Visible=.t., Left=9, Top=44,;
    Alignment=1, Width=109, Height=15,;
    Caption="Agente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="XBPJOJZUGL",Visible=.t., Left=8, Top=76,;
    Alignment=1, Width=110, Height=15,;
    Caption="Periodo dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="HKSDCTBFZA",Visible=.t., Left=309, Top=76,;
    Alignment=1, Width=49, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="QZBSOVQOEU",Visible=.t., Left=4, Top=107,;
    Alignment=1, Width=114, Height=18,;
    Caption="Valuta documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="ZFMWHDWHJY",Visible=.t., Left=335, Top=109,;
    Alignment=1, Width=108, Height=18,;
    Caption="Totale imponibile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="FWCETADMYR",Visible=.t., Left=330, Top=138,;
    Alignment=1, Width=113, Height=15,;
    Caption="Totale provvigione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="FTAVUSUCFO",Visible=.t., Left=10, Top=180,;
    Alignment=1, Width=108, Height=15,;
    Caption="Emessa nota n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="UHOETUCOUS",Visible=.t., Left=238, Top=180,;
    Alignment=1, Width=35, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="MZVOXGFVUK",Visible=.t., Left=370, Top=180,;
    Alignment=1, Width=71, Height=15,;
    Caption="Pagata il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="XQUAKMANFS",Visible=.t., Left=193, Top=180,;
    Alignment=0, Width=15, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="YIFBSCXANM",Visible=.t., Left=330, Top=211,;
    Alignment=1, Width=111, Height=15,;
    Caption="Netto corrisposto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="JHHKFFDHLE",Visible=.t., Left=19, Top=211,;
    Alignment=1, Width=99, Height=18,;
    Caption="Valuta agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="QUSEHCWJKU",Visible=.t., Left=183, Top=107,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0)
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="YRFELYSBBF",Visible=.t., Left=9, Top=136,;
    Alignment=1, Width=109, Height=18,;
    Caption="Rif.agente:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (empty(nvl(.w_PLRIFAGE,SPACE(5))))
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="YYMULJKAHD",Visible=.t., Left=123, Top=284,;
    Alignment=0, Width=423, Height=18,;
    Caption="La sequenza di PLNUMREG � reimpostata in area manuale BlankRecordEnd"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="XMSZLBYJQM",Visible=.t., Left=123, Top=302,;
    Alignment=0, Width=423, Height=18,;
    Caption="con quella di PL__ANNO per caricare correttamente i dati dell'anno impostato"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oBox_1_49 as StdBox with uid="KOPCBWXTFL",left=6, top=166, width=568,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_apl','PRO_LIQU','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PL__ANNO=PRO_LIQU.PL__ANNO";
  +" and "+i_cAliasName2+".PLNUMREG=PRO_LIQU.PLNUMREG";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
