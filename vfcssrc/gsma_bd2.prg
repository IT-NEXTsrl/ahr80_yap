* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bd2                                                        *
*              Crea movimenti lotti                                            *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_4]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-02-28                                                      *
* Last revis.: 2004-02-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bd2",oParentObject)
return(i_retval)

define class tgsma_bd2 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  MOVILOTT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ad hoc ENTERPRISE - Costruisce Movimenti lotti / Ubicazione
    *     Rettifiche inventariali
    * --- Select from MOVILOTT
    i_nConn=i_TableProp[this.MOVILOTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVILOTT_idx,2],.t.,this.MOVILOTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select max(CPROWNUM) as MaxRowNum  from "+i_cTable+" MOVILOTT ";
          +" where MLSERIAL="+cp_ToStrODBC(this.oParentObject.w_MMSERIAL)+" and MLROWORD="+cp_ToStrODBC(this.oParentObject.w_CPROWNUM)+" and MLNUMRIF="+cp_ToStrODBC(this.oParentObject.w_MMNUMRIF)+"";
           ,"_Curs_MOVILOTT")
    else
      select max(CPROWNUM) as MaxRowNum from (i_cTable);
       where MLSERIAL=this.oParentObject.w_MMSERIAL and MLROWORD=this.oParentObject.w_CPROWNUM and MLNUMRIF=this.oParentObject.w_MMNUMRIF;
        into cursor _Curs_MOVILOTT
    endif
    if used('_Curs_MOVILOTT')
      select _Curs_MOVILOTT
      locate for 1=1
      do while not(eof())
      this.w_ROWNUM = nvl( MaxRowNum , 0)
        select _Curs_MOVILOTT
        continue
      enddo
      use
    endif
    this.w_ROWNUM = this.w_ROWNUM + 1
    * --- Insert into MOVILOTT
    i_nConn=i_TableProp[this.MOVILOTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVILOTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVILOTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MLSERIAL"+",MLROWORD"+",MLNUMRIF"+",CPROWNUM"+",MLCODMAG"+",MLCODUBI"+",MLCODLOT"+",MLFLCASC"+",MLQTAMOV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMSERIAL),'MOVILOTT','MLSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'MOVILOTT','MLROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MMNUMRIF),'MOVILOTT','MLNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'MOVILOTT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODMAG),'MOVILOTT','MLCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODUBI),'MOVILOTT','MLCODUBI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODLOT),'MOVILOTT','MLCODLOT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MLFLCASC),'MOVILOTT','MLFLCASC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_QTAAGGLU),'MOVILOTT','MLQTAMOV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MLSERIAL',this.oParentObject.w_MMSERIAL,'MLROWORD',this.oParentObject.w_CPROWNUM,'MLNUMRIF',this.oParentObject.w_MMNUMRIF,'CPROWNUM',this.w_ROWNUM,'MLCODMAG',this.oParentObject.w_CODMAG,'MLCODUBI',this.oParentObject.w_CODUBI,'MLCODLOT',this.oParentObject.w_CODLOT,'MLFLCASC',this.oParentObject.w_MLFLCASC,'MLQTAMOV',this.oParentObject.w_QTAAGGLU)
      insert into (i_cTable) (MLSERIAL,MLROWORD,MLNUMRIF,CPROWNUM,MLCODMAG,MLCODUBI,MLCODLOT,MLFLCASC,MLQTAMOV &i_ccchkf. );
         values (;
           this.oParentObject.w_MMSERIAL;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_MMNUMRIF;
           ,this.w_ROWNUM;
           ,this.oParentObject.w_CODMAG;
           ,this.oParentObject.w_CODUBI;
           ,this.oParentObject.w_CODLOT;
           ,this.oParentObject.w_MLFLCASC;
           ,this.oParentObject.w_QTAAGGLU;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOVILOTT'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_MOVILOTT')
      use in _Curs_MOVILOTT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
