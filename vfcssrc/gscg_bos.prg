* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bos                                                        *
*              Operazioni superiori a 3000 euro                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-04-12                                                      *
* Last revis.: 2011-08-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bos",oParentObject,m.pOPER)
return(i_retval)

define class tgscg_bos as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_PNNUMRER = 0
  w_PNCOMPET = space(4)
  w_PNDATREG = ctod("  /  /  ")
  w_PNSERIAL = space(10)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_DESCLF = space(40)
  w_SERIALEDIFILTRO = space(10)
  w_CONF_ELAB = space(1)
  w_RIFCON = space(30)
  w_TIPOCONTRATTO = space(1)
  w_TIPOPE = space(1)
  w_PARAME = space(1)
  w_DATAREGISTRAZIONE = ctod("  /  /  ")
  w_PNCODVAL = space(3)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine lanciata dall'anagrafica Gscg_Aos ( Operazioni superiori a 3000 euro )
    do case
      case this.pOper="V" or this.pOper="S"
        this.w_PNNUMRER = IIF(this.pOper="V",this.oParentObject.w_NUMREG,this.oParentObject.w_NUMREG_A)
        this.w_PNCOMPET = IIF(this.pOper="V",this.oParentObject.w_COMPET,this.oParentObject.w_COMPET_A)
        this.w_PNDATREG = IIF(this.pOper="V",this.oParentObject.w_DATREG,this.oParentObject.w_DATREG_A)
        this.w_PNSERIAL = IIF(this.pOper="V",this.oParentObject.w_OSRIFFAT,this.oParentObject.w_OSRIFACC)
        this.w_PNTIPCLF = This.oParentObject.oParentObject.w_PNTIPCLF
        this.w_PNCODCLF = This.oParentObject.oParentObject.w_PNCODCLF
        this.w_PNCODVAL = This.oParentObject.oParentObject.w_PNCODVAL
        this.w_DESCLF = This.oParentObject.oParentObject.w_DESCLF
        this.w_SERIALEDIFILTRO = This.oParentObject.oParentObject.w_PNSERIAL
        this.w_DATAREGISTRAZIONE = This.oParentObject.oParentObject.w_PNDATREG
        this.w_PARAME = this.pOper
        this.w_CONF_ELAB = "N"
        do GSCG_KOS with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.pOper="V"
          this.oParentObject.w_NUMREG = this.w_PNNUMRER
          this.oParentObject.w_DATREG = Cp_todate(this.w_PNDATREG)
          this.oParentObject.w_COMPET = this.w_PNCOMPET
          this.oParentObject.w_OSRIFFAT = this.w_PNSERIAL
        else
          this.oParentObject.w_NUMREG_A = this.w_PNNUMRER
          this.oParentObject.w_DATREG_A = Cp_todate(this.w_PNDATREG)
          this.oParentObject.w_COMPET_A = this.w_PNCOMPET
          this.oParentObject.w_OSRIFACC = this.w_PNSERIAL
        endif
      case this.pOper="A"
        this.oparentObject.ecpsave()
        i_retcode = 'stop'
        return
      case this.pOper="C"
        this.w_PNTIPCLF = This.oParentObject.oParentObject.w_PNTIPCLF
        this.w_PNCODCLF = This.oParentObject.oParentObject.w_PNCODCLF
        this.w_DESCLF = This.oParentObject.oParentObject.w_DESCLF
        this.w_TIPOPE = This.oParentObject.w_Ostipope
        this.w_CONF_ELAB = "N"
        do GSCG_KOC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_CONF_ELAB="S"
          if this.w_TIPOCONTRATTO<>this.w_TIPOPE AND not EMPTY (this.w_TIPOCONTRATTO)
            Ah_ErrorMsg("E' stato selezionato un riferimento contratto con tipologia operazione differente")
          endif
          this.oParentObject.w_OSRIFCON = this.w_RIFCON
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
