* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bam                                                        *
*              Valutazione att. pass. scrittura primanota                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][147]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-12                                                      *
* Last revis.: 2013-04-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bam",oParentObject)
return(i_retval)

define class tgscg_bam as StdBatch
  * --- Local variables
  w_TmpS = space(250)
  w_MESS_ERR = space(200)
  w_OK = .f.
  w_WDOC = 0
  w_oERRORLOG = .NULL.
  w_APPO2 = 0
  w_DATBLO = ctod("  /  /  ")
  w_STALIG = ctod("  /  /  ")
  w_MESS = space(90)
  w_PNSERIAL = space(10)
  w_PNNUMRER = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_PNDATREG = ctod("  /  /  ")
  w_PNCODUTE = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctod("  /  /  ")
  w_UTDV = ctod("  /  /  ")
  w_PNCODCAU = space(5)
  w_PNTIPREG = space(2)
  w_PNDESSUP = space(50)
  w_PNCODESE = space(4)
  w_PNCOMPET = space(4)
  w_PNVALNAZ = space(3)
  w_PNCODVAL = space(3)
  w_PNSIMVAL = space(5)
  w_PNCAOVAL = 0
  w_DECTOT = 0
  w_DESCRI = space(50)
  w_PNCODCON = space(15)
  w_PNTIPCON = space(1)
  w_BUSUNIRI = space(3)
  w_PNIMPDAR = 0
  w_PNIMPAVE = 0
  w_PNFLSALF = space(1)
  w_PNDESRIG = space(50)
  w_PNCAURIG = space(5)
  w_PNFLSALD = space(1)
  w_PNFLPART = space(1)
  w_APPDIFF = 0
  w_SEGNO = space(1)
  w_IMPDCA = 0
  w_IMPDCP = 0
  w_DIFCAMPA = space(15)
  w_DIFCAMAT = space(15)
  w_MODPAG = space(5)
  w_TIPAGGCO = space(1)
  w_FLBUANAL = space(1)
  w_TOTCALC = 0
  w_TOTBIL = 0
  w_TOTIMP = 0
  w_CPROWNUM2 = 0
  w_SEGNOAN = space(1)
  w_TOTIMP = 0
  w_SOMPAR = 0
  w_VOCCOST = space(15)
  w_CENCOST = space(15)
  w_PARAM = 0
  w_IMPORTO = 0
  w_PTTOTIMP = 0
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_PTORIGSA = space(1)
  w_CPROWNUMP = 0
  w_PTCAOOLD = 0
  w_PTCAOVAL = 0
  w_NUMREG = 0
  w_MESS1 = space(100)
  w_CHIUSO = space(1)
  w_NUMSCA = 0
  w_PT_SEGNO = space(1)
  w_CCDESSUP = space(254)
  w_CCDESRIG = space(254)
  w_IMP_APE = 0
  w_DVCAOOLD = 0
  w_PNPRG = space(8)
  w_PNCODBUN = space(3)
  w_OLTIPCON = space(1)
  w_OLROWORD = 0
  w_OLCODBUN = space(3)
  w_FINEMOV = .f.
  w_TESTCAO = 0
  w_DIVIDE = .f.
  w_NU_RIGA = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  PNT_MAST_idx=0
  ESERCIZI_idx=0
  CONTROPA_idx=0
  PNT_DETT_idx=0
  CONTI_idx=0
  BUSIUNIT_idx=0
  MOVICOST_idx=0
  COLLCENT_idx=0
  PAR_TITE_idx=0
  VALDATPA_idx=0
  VALUTE_idx=0
  CAU_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dall'anagrafica Gscg_Aap ( Valutazione Attivit� Passivit� in divisa )
    *     Batch utilizzato per la generazione del movimento di Primanota
    *     I cursori Partite e CursElabora sono costruiti in GSCG_BAP!
    this.w_PNDATREG = this.oParentObject.w_APDATREG
    this.w_PNCODUTE = IIF(g_UNIUTE="S", 0, i_CODUTE)
    this.w_UTCC = i_CODUTE
    this.w_UTDC = SetInfoDate( g_CALUTD )
    this.w_UTCV = 0
    this.w_UTDV = cp_CharToDate("  -  -    ")
    this.w_PNCODCAU = this.oParentObject.w_APCAUCON
    if g_APPLICATION <> "ad hoc ENTERPRISE"
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCDESSUP,CCDESRIG"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCDESSUP,CCDESRIG;
          from (i_cTable) where;
              CCCODICE = this.w_PNCODCAU;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
        this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_PNTIPREG = "N"
    this.w_PNDESSUP = this.oParentObject.w_APDESCRI
    this.w_PNCODESE = CALCESER(this.oParentObject.w_APDATREG)
    this.w_PNCOMPET = this.w_PNCODESE
    this.w_NUMREG = 0
    * --- Calcolo codice valuta
    if g_APPLICATION = "ad hoc ENTERPRISE"
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESVALNAZ,ESCHIUSO"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESVALNAZ,ESCHIUSO;
          from (i_cTable) where;
              ESCODAZI = this.oParentObject.w_CODAZI;
              and ESCODESE = this.w_PNCODESE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PNCODVAL = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
        this.w_CHIUSO = NVL(cp_ToDate(_read_.ESCHIUSO),cp_NullValue(_read_.ESCHIUSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESVALNAZ"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESVALNAZ;
          from (i_cTable) where;
              ESCODAZI = this.oParentObject.w_CODAZI;
              and ESCODESE = this.w_PNCODESE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PNCODVAL = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CHIUSO = "N"
    endif
    this.w_PNVALNAZ = this.w_PNCODVAL
    * --- Leggo il cambio della valuta
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VASIMVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_PNCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VASIMVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_PNCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PNCAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_PNSIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO,AZSTALIG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO,AZSTALIG;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if g_APPLICATION = "ad hoc ENTERPRISE"
      if this.oParentObject.w_APDATREG <= g_CONCON OR this.w_PNDATREG <= this.w_STALIG
        this.w_MESS1 = "Impossibile generare movimento di primanota.%0data registrazione inferiore o uguale alla data di consolidamento o alla data di stampa del libro giornale"
        ah_ErrorMsg(this.w_mess1,"!","")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      if this.w_CHIUSO="S"
        ah_ErrorMsg("Impossibile generare movimento di primanota.%0l'esercizio di competenza risulta chiuso","!","")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      * --- Try
      local bErr_04FF28E0
      bErr_04FF28E0=bTrsErr
      this.Try_04FF28E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        AH_ERRORMSG(this.w_MESS,48)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_04FF28E0
      * --- End
      if not EMPTY (this.w_MESS1)
        ah_ErrorMsg(this.w_mess1,48,"")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
    else
      this.w_MESS = CHKINPNT(this.w_PNDATREG,this.oParentObject.w_FLANAL,"",0,cp_CharToDate("- -"),"NO","Load")
      if not Empty( this.w_MESS )
        ah_ErrorMsg("%1","!","",this.w_MESS)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      else
        this.w_MESS = BLOC_AZI( .T. , this.oParentObject.w_APDATREG ) 
        if not Empty( this.w_MESS )
          ah_ErrorMsg("%1","!","",this.w_MESS)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- Creo un cursore che conterr� il dettaglio delle partite ed il seriale di primanota.
    *     Questo cursore verr� utilizzata nel Gscg_Bap per effettuare l'aggiornamento della tabella VALDATPA
    CREATE CURSOR VALDATPA (DVPNSERI C(10), DVPASERI C(10), DVORIGSA C(1), DVROWORD N(4,0),DVROWNUM N(4), DVCAOOLD N(12,7))
    * --- Try
    local bErr_04FFA270
    bErr_04FFA270=bTrsErr
    this.Try_04FFA270()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg(i_ErrMsg,"!","")
      if g_APPLICATION = "ad hoc ENTERPRISE"
        * --- Try
        local bErr_0501C090
        bErr_0501C090=bTrsErr
        this.Try_0501C090()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Impossibile rimuovere i blocchi. Rimuoverli dai dati azienda","!","")
        endif
        bTrsErr=bTrsErr or bErr_0501C090
        * --- End
      else
        this.w_MESS = BLOC_AZI( .F. ) 
        if not Empty( this.w_MESS )
          ah_ErrorMsg("%1","!","",this.w_MESS)
        endif
      endif
    endif
    bTrsErr=bTrsErr or bErr_04FFA270
    * --- End
    This.oParentObject.ecpsave()
    * --- Elimino i cursori usati
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_04FF28E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    if empty(NVL(this.w_datblo,cp_CharToDate("  -  -  ")))
      * --- Inserisce <Blocco> per Primanota
      * --- Write into AZIENDA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_APDATREG),'AZIENDA','AZDATBLO');
            +i_ccchkf ;
        +" where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
               )
      else
        update (i_cTable) set;
            AZDATBLO = this.oParentObject.w_APDATREG;
            &i_ccchkf. ;
         where;
            AZCODAZI = this.oParentObject.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Un altro utente ha impostato il blocco - controllo concorrenza
      this.w_MESS = "Prima nota bloccata, verificare semaforo bollati in dati azienda.%0Impossibile contabilizzare"
      * --- Raise
      i_Error=this.w_MESS
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04FFA270()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Controllo se l'utente ha specificato Raggruppa Scritture.
    *     Se ha specificato Raggruppa Scritture inseriro un unico record di testata mentre
    *     nell'altro caso i movimenti generati saranno tanti quanti i record selezionati.
    this.w_OK = .T.
    * --- Cursore con i messaggi di Errore / Warning
    this.w_WDOC = 0
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    if this.oParentObject.w_APRAGSCR="S"
      if g_APPLICATION <> "ad hoc ENTERPRISE"
        if Empty(this.w_PNDESSUP) and (Not Empty(this.w_CCDESSUP) OR Not Empty(this.w_CCDESRIG))
          * --- Array elenco parametri per descrizioni di riga e testata
           
 DIMENSION ARPARAM[12,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]="" 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]="" 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]="" 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]="" 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]="" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=""
        endif
        if Not Empty(this.w_CCDESSUP) 
          this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
        endif
      endif
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Ciclo il cursore contenente il dettaglio delle partite. Non scrivo il dettaglio di primanota
      *     solo per le partite che hanno differenza cambi a 0
      Select CursElabora 
 Go Top
      * --- Prendo in considerazione solo le partite che effettivamente hanno una differenza
      *     cambi.
      Scan for CursElabora.Ptdifcam<>0
      * --- Devo verificare che la riga che si st� analizzando sia una riga di differenze di conversione.
      if CursElabora.Tipo="D"
        this.w_PNTIPCON = CursElabora.Pttipcon
        this.w_BUSUNIRI = CursElabora.Pncodbun
        this.w_PNCODCON = CursElabora.Ptcodcon
        this.w_PNIMPDAR = ABS(CursElabora.Pnimpdar)
        this.w_PNIMPAVE = ABS(CursElabora.Pnimpave)
        * --- Scrivo la riga di differenza cambi
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Scrivo la riga del conto da cui deriva la differenza di conversione
        this.w_CPROWNUM = this.w_CPROWNUM+1
        this.w_CPROWORD = this.w_CPROWORD+10
      else
        this.w_APPDIFF = CursElabora.PTDIFCAM
        this.w_IMPDCA = 0
        this.w_IMPDCP = 0
        this.w_PNTIPCON = CursElabora.Pttipcon
        this.w_PNCODCON = nvl( CursElabora.PTCODCON , space(15))
        this.w_BUSUNIRI = IIF(EMPTY(nvl(CursElabora.PNCODBUN,SPACE(3))), g_CODBUN, CursElabora.PNCODBUN)
        this.w_SEGNO = CursElabora.PT_SEGNO
        * --- Scrivo la riga della partita che ha generato una differenza di conversione.
        if this.w_SEGNO="A"
          this.w_IMPDCA = this.w_IMPDCA - IIF(this.w_APPDIFF>0, ABS(this.w_APPDIFF), 0)
          this.w_IMPDCP = this.w_IMPDCP + IIF(this.w_APPDIFF>0, 0, ABS(this.w_APPDIFF))
        else
          this.w_IMPDCA = this.w_IMPDCA - IIF(this.w_APPDIFF>0, 0, ABS(this.w_APPDIFF))
          this.w_IMPDCP = this.w_IMPDCP + IIF(this.w_APPDIFF>0, ABS(this.w_APPDIFF), 0)
        endif
        if this.w_IMPDCA<>0
          this.w_APPO2 = this.w_IMPDCA
        endif
        if this.w_IMPDCP<>0
          this.w_APPO2 = this.w_IMPDCP
        endif
        this.w_PNIMPAVE = IIF(this.w_APPO2>0,this.w_APPO2,0)
        this.w_PNIMPDAR = IIF(this.w_APPO2<0,ABS(this.w_APPO2),0)
        * --- Scrivo la riga di differenza cambi
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Scrivo la riga del conto da cui deriva la differenza di conversione
        this.w_CPROWNUM = this.w_CPROWNUM+1
        this.w_CPROWORD = this.w_CPROWORD+10
      endif
      Select CursElabora
      endscan
      * --- Dopo aver generato il movimento di primanota devo andare ad aggiornare le partite.
      *     Per fare questa operazione devo ciclare il cursore che viene restituito dallo zoom
      Select Partite
      Go Top
      Scan for Partite.Ptdifcam<>0
      * --- Valorizzo i campi chiave della partita che deve essere aggiornata
      this.w_PTSERIAL = Partite.Ptserial
      this.w_PTROWORD = Partite.Ptroword
      this.w_PTORIGSA = Partite.Ptorigsa
      this.w_CPROWNUMP = Partite.Cprownum
      this.w_PTCAOOLD = Partite.Ptcaoape
      if g_APPLICATION = "ad hoc ENTERPRISE"
        this.w_PTTOTIMP = Partite.PTTOTIMP
        this.w_PT_SEGNO = Partite.PT_SEGNO
        this.w_NUMSCA = Partite.Numsca
      endif
      this.w_IMP_APE = Partite.PtTotImp
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if Not this.w_OK
        * --- Raise
        i_Error=this.w_MESS
        return
      endif
      Select Partite
      endscan
    else
      Select CursElabora 
 Go Top
      * --- Prendo in considerazione solo le partite che effettivamente hanno una differenza
      *     cambi.
      Scan for CursElabora.Ptdifcam<>0
      * --- Controllo se l'utente non ha selezionato Raggruppa Scritture. In questo
      *     caso devo inserire tante testate di primanota quante sono le partite selezionate.
      if g_APPLICATION = "ad hoc ENTERPRISE"
        this.w_DESCRI = LEFT("Part:"+alltrim(Ptnumpar)+" "+iif(Pttipcon="C","C:",iif(Pttipcon="F","F:","G:"))+alltrim(Ptcodcon)+" Scad:"+Dtoc(Ptdatsca),50)
        this.w_PNDESSUP = IIF(NOT EMPTY(this.oParentObject.w_APDESCRI),this.oParentObject.w_APDESCRI,this.w_DESCRI)
      else
        if Empty(this.w_PNDESSUP) and (Not Empty(this.w_CCDESSUP) OR Not Empty(this.w_CCDESRIG))
          * --- Array elenco parametri per descrizioni di riga e testata
           
 DIMENSION ARPARAM[12,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]="" 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]="" 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]="" 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(ptcodcon) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]="" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]=ALLTRIM(ptnumpar) 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=dtoc(cp_todate(ptdatsca)) 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=ALLTRIM(pttipcon)
        endif
        if Not Empty(this.w_CCDESSUP) and Empty(this.w_PNDESSUP) 
          this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
        endif
      endif
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Inserisco nel dettaglio di primanota la riga di differenza cambi.
      this.w_APPDIFF = CursElabora.PTDIFCAM
      this.w_IMPDCA = 0
      this.w_IMPDCP = 0
      this.w_SEGNO = CursElabora.PT_SEGNO
      this.w_PNTIPCON = "G"
      this.w_BUSUNIRI = IIF(EMPTY(nvl(CursElabora.PNCODBUN,SPACE(3))), g_CODBUN, CursElabora.PNCODBUN)
      if this.w_SEGNO="A"
        this.w_IMPDCA = this.w_IMPDCA - IIF(this.w_APPDIFF>0, ABS(this.w_APPDIFF), 0)
        this.w_IMPDCP = this.w_IMPDCP + IIF(this.w_APPDIFF>0, 0, ABS(this.w_APPDIFF))
      else
        this.w_IMPDCA = this.w_IMPDCA - IIF(this.w_APPDIFF>0, 0, ABS(this.w_APPDIFF))
        this.w_IMPDCP = this.w_IMPDCP + IIF(this.w_APPDIFF>0, ABS(this.w_APPDIFF), 0)
      endif
      if this.w_IMPDCA<>0
        this.w_APPO2 = this.w_IMPDCA
        this.w_PNCODCON = this.oParentObject.w_DIFPOS
      endif
      if this.w_IMPDCP<>0
        this.w_APPO2 = this.w_IMPDCP
        this.w_PNCODCON = this.oParentObject.w_DIFNEG
      endif
      this.w_PNIMPDAR = IIF(this.w_APPO2>0,this.w_APPO2,0)
      this.w_PNIMPAVE = IIF(this.w_APPO2<0,ABS(this.w_APPO2),0)
      * --- Scrivo la riga di differenza cambi
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Scrivo la riga del conto da cui deriva la differenza di conversione
      this.w_CPROWNUM = this.w_CPROWNUM+1
      this.w_CPROWORD = this.w_CPROWORD+10
      this.w_PNTIPCON = CursElabora.Pttipcon
      this.w_PNCODCON = nvl( CursElabora.PTCODCON , space(15))
      this.w_BUSUNIRI = IIF(EMPTY(nvl(CursElabora.PNCODBUN,SPACE(3))), g_CODBUN, CursElabora.PNCODBUN)
      this.w_PTTOTIMP = (this.w_PNIMPDAR-this.w_PNIMPAVE)
      this.w_PNIMPDAR = IIF(this.w_PTTOTIMP>0,0,ABS(this.w_PTTOTIMP))
      this.w_PNIMPAVE = IIF(this.w_PTTOTIMP<0,0,this.w_PTTOTIMP)
      * --- Scrivo la riga della partita che ha generato una differenza di conversione.
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Aggiorno il cambio della partita che ha originato una differenza di conversione,
      *     ed inserisco i valori all'interno della nuova tabella VALDATPA
      this.w_PTSERIAL = CursElabora.Ptserial
      this.w_PTROWORD = CursElabora.Ptroword
      this.w_PTORIGSA = CursElabora.Ptorigsa
      this.w_CPROWNUMP = CursElabora.Cprownum
      this.w_PTCAOOLD = CursElabora.Ptcaoape
      this.w_IMP_APE = CursElabora.Pttotimp
      if g_APPLICATION = "ad hoc ENTERPRISE"
        this.w_PTTOTIMP = CursElabora.PTTOTIMP
        this.w_PT_SEGNO = CursElabora.PT_SEGNO
        this.w_NUMSCA = CursElabora.Numsca
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if Not this.w_OK
        * --- Raise
        i_Error=this.w_MESS
        return
      endif
      Select CursElabora
      endscan
    endif
    if USED("VALDATPA")
      * --- Aggiorno la tabella VALDATPA
      this.w_CPROWNUM = 0
      Select VALDATPA
      GO TOP
      SCAN
      this.w_PTSERIAL = Valdatpa.Dvpaseri
      this.w_PTROWORD = Valdatpa.Dvroword
      this.w_CPROWNUMP = Valdatpa.Dvrownum
      this.w_PNSERIAL = Valdatpa.Dvpnseri
      if g_APPLICATION = "ad hoc ENTERPRISE"
        this.w_PTORIGSA = Valdatpa.Dvorigsa
        this.w_CPROWNUM = this.w_CPROWNUM+1
        this.w_DVCAOOLD = ValDatpa.DVCAOOLD
        * --- Insert into VALDATPA
        i_nConn=i_TableProp[this.VALDATPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALDATPA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\gscg_bap_3",this.VALDATPA_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      else
        * --- Insert into VALDATPA
        i_nConn=i_TableProp[this.VALDATPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALDATPA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VALDATPA_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DVSERIAL"+",DVPASERI"+",DVROWORD"+",DVROWNUM"+",DVPNSERI"+",DVCAOOLD"+",DVORIGSA"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_APSERIAL),'VALDATPA','DVSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'VALDATPA','DVPASERI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'VALDATPA','DVROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUMP),'VALDATPA','DVROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'VALDATPA','DVPNSERI');
          +","+cp_NullLink(cp_ToStrODBC(ValDatpa.DVCAOOLD),'VALDATPA','DVCAOOLD');
          +","+cp_NullLink(cp_ToStrODBC(" "),'VALDATPA','DVORIGSA');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DVSERIAL',this.oParentObject.w_APSERIAL,'DVPASERI',this.w_PTSERIAL,'DVROWORD',this.w_PTROWORD,'DVROWNUM',this.w_CPROWNUMP,'DVPNSERI',this.w_PNSERIAL,'DVCAOOLD',ValDatpa.DVCAOOLD,'DVORIGSA'," ")
          insert into (i_cTable) (DVSERIAL,DVPASERI,DVROWORD,DVROWNUM,DVPNSERI,DVCAOOLD,DVORIGSA &i_ccchkf. );
             values (;
               this.oParentObject.w_APSERIAL;
               ,this.w_PTSERIAL;
               ,this.w_PTROWORD;
               ,this.w_CPROWNUMP;
               ,this.w_PNSERIAL;
               ,ValDatpa.DVCAOOLD;
               ," ";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      ENDSCAN
      if used("VALDATPA")
        select VALDATPA
        use
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_WDOC<>0
      if ah_YesNo("Registrazioni generate: %1 con %2 avvertimento/i%0si desidera stamparli?",,STR(this.w_NUMREG,6,0),ALLTRIM(STR(this.w_WDOC)))
        this.w_oERRORLOG.PrintLog(this.oParentObject,"Segnalazioni in fase di generazione",.f.)     
      endif
    else
      ah_ErrorMsg( "Registrazioni generate: %1" ,48,"", STR(this.w_NUMREG, 6, 0))
    endif
    if g_APPLICATION = "ad hoc ENTERPRISE"
      * --- Try
      local bErr_04FF4AB0
      bErr_04FF4AB0=bTrsErr
      this.Try_04FF4AB0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        ah_ErrorMsg("Impossibile rimuovere i blocchi. Rimuoverli dai dati azienda","!","")
      endif
      bTrsErr=bTrsErr or bErr_04FF4AB0
      * --- End
    else
      this.w_MESS = BLOC_AZI( .F. ) 
      if not Empty( this.w_MESS )
        ah_ErrorMsg("%1","!","",this.w_MESS)
      endif
    endif
    return
  proc Try_0501C090()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04FF4AB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if used("CursElabora")
      Select CursElabora
      use
    endif
    if used("Parametri")
      Select Parametri
      use
    endif
    if used("Partite")
      Select Partite
      use
    endif
    if used("Totali")
      select Totali
      use
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione nuova registrazione in Primanota
    this.w_PNSERIAL = SPACE(10)
    this.w_PNNUMRER = 0
    this.w_CPROWNUM = 1
    this.w_CPROWORD = 10
    i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
    if g_APPLICATION = "ad hoc ENTERPRISE"
      cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
      cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNNUMRER")
    else
      this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
      cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
      cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
    endif
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL)
      insert into (i_cTable) (PNSERIAL &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Impossibile creare Doc. Prima Nota'
      return
    endif
    if g_APPLICATION = "ad hoc ENTERPRISE"
      * --- Write into PNT_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
        +",PNCODCAU ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
        +",PNCODESE ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
        +",PNCODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
        +",PNCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
        +",PNCOMPET ="+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
        +",PNDATREG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
        +",PNDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
        +",PNFLPROV ="+cp_NullLink(cp_ToStrODBC("S"),'PNT_MAST','PNFLPROV');
        +",PNNUMRER ="+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
        +",PNSIMVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PNSIMVAL),'PNT_MAST','PNSIMVAL');
        +",PNTIPREG ="+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
        +",PNVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
        +",UTCC ="+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'PNT_MAST','UTCC');
        +",UTCV ="+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'PNT_MAST','UTCV');
        +",UTDC ="+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'PNT_MAST','UTDC');
        +",UTDV ="+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'PNT_MAST','UTDV');
        +",PNORIGSA ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNORIGSA');
        +",PNTIPCLF ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNTIPCLF');
        +",PNCODCLF ="+cp_NullLink(cp_ToStrODBC("               "),'PNT_MAST','PNCODCLF');
        +",PNTIPDOC ="+cp_NullLink(cp_ToStrODBC("NO"),'PNT_MAST','PNTIPDOC');
        +",PNPRD ="+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRD');
        +",PNPRP ="+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRP');
        +",PNFLIVDF ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLIVDF');
        +",PNFLLIBG ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLLIBG');
        +",PNFLREGI ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLREGI');
        +",PNSCRASS ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNSCRASS');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
               )
      else
        update (i_cTable) set;
            PNCAOVAL = this.w_PNCAOVAL;
            ,PNCODCAU = this.w_PNCODCAU;
            ,PNCODESE = this.w_PNCODESE;
            ,PNCODUTE = this.w_PNCODUTE;
            ,PNCODVAL = this.w_PNCODVAL;
            ,PNCOMPET = this.w_PNCOMPET;
            ,PNDATREG = this.w_PNDATREG;
            ,PNDESSUP = this.w_PNDESSUP;
            ,PNFLPROV = "S";
            ,PNNUMRER = this.w_PNNUMRER;
            ,PNSIMVAL = this.w_PNSIMVAL;
            ,PNTIPREG = this.w_PNTIPREG;
            ,PNVALNAZ = this.w_PNVALNAZ;
            ,UTCC = this.w_UTCC;
            ,UTCV = this.w_UTCV;
            ,UTDC = this.w_UTDC;
            ,UTDV = this.w_UTDV;
            ,PNORIGSA = " ";
            ,PNTIPCLF = "N";
            ,PNCODCLF = "               ";
            ,PNTIPDOC = "NO";
            ,PNPRD = "NN";
            ,PNPRP = "NN";
            ,PNFLIVDF = " ";
            ,PNFLLIBG = " ";
            ,PNFLREGI = " ";
            ,PNSCRASS = " ";
            &i_ccchkf. ;
         where;
            PNSERIAL = this.w_PNSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into PNT_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
        +",PNCODCAU ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
        +",PNCODESE ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
        +",PNCODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
        +",PNCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
        +",PNCOMPET ="+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
        +",PNDATREG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
        +",PNDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
        +",PNFLPROV ="+cp_NullLink(cp_ToStrODBC("S"),'PNT_MAST','PNFLPROV');
        +",PNNUMRER ="+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
        +",PNTIPREG ="+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
        +",PNVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
        +",UTCC ="+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'PNT_MAST','UTCC');
        +",UTCV ="+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'PNT_MAST','UTCV');
        +",UTDC ="+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'PNT_MAST','UTDC');
        +",UTDV ="+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'PNT_MAST','UTDV');
        +",PNTIPCLF ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNTIPCLF');
        +",PNCODCLF ="+cp_NullLink(cp_ToStrODBC("               "),'PNT_MAST','PNCODCLF');
        +",PNTIPDOC ="+cp_NullLink(cp_ToStrODBC("NO"),'PNT_MAST','PNTIPDOC');
        +",PNPRD ="+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRD');
        +",PNPRP ="+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRP');
        +",PNFLIVDF ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLIVDF');
        +",PNFLLIBG ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLLIBG');
        +",PNFLREGI ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLREGI');
        +",PNSCRASS ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNSCRASS');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
               )
      else
        update (i_cTable) set;
            PNCAOVAL = this.w_PNCAOVAL;
            ,PNCODCAU = this.w_PNCODCAU;
            ,PNCODESE = this.w_PNCODESE;
            ,PNCODUTE = this.w_PNCODUTE;
            ,PNCODVAL = this.w_PNCODVAL;
            ,PNCOMPET = this.w_PNCOMPET;
            ,PNDATREG = this.w_PNDATREG;
            ,PNDESSUP = this.w_PNDESSUP;
            ,PNFLPROV = "S";
            ,PNNUMRER = this.w_PNNUMRER;
            ,PNTIPREG = this.w_PNTIPREG;
            ,PNVALNAZ = this.w_PNVALNAZ;
            ,UTCC = this.w_UTCC;
            ,UTCV = this.w_UTCV;
            ,UTDC = this.w_UTDC;
            ,UTDV = this.w_UTDV;
            ,PNTIPCLF = "N";
            ,PNCODCLF = "               ";
            ,PNTIPDOC = "NO";
            ,PNPRD = "NN";
            ,PNPRP = "NN";
            ,PNFLIVDF = " ";
            ,PNFLLIBG = " ";
            ,PNFLREGI = " ";
            ,PNSCRASS = " ";
            &i_ccchkf. ;
         where;
            PNSERIAL = this.w_PNSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_NUMREG = this.w_NUMREG + 1
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive riga di dettaglio e riga finale di chiusura
    if (NOT EMPTY(this.w_PNTIPCON)) AND (NOT EMPTY(this.w_PNCODCON))
      ah_msg( "Elabora conto: %1",.t.,.f.,.f.,this.w_PNCODCON )
      this.w_PNFLSALF = " "
      if g_APPLICATION = "ad hoc ENTERPRISE"
        this.w_PNDESRIG = this.w_PNDESSUP
      else
        this.w_PNDESRIG = this.oParentObject.w_APDESCRI
        if Not Empty(this.w_CCDESRIG) 
          this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
        endif
      endif
      this.w_PNCAURIG = this.oParentObject.w_APCAUCON
      this.w_PNFLSALD = " "
      this.w_PNFLPART = "N"
      ah_msg( "Scrivo riga del dettaglio" )
      * --- Scrive Primanota (Anagrafica)
      if this.w_PNTIPCON$"CF"
        * --- Leggo la modalit� di pagamento associato al Cliente/Fornitore.
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODPAG"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODPAG;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCON;
                and ANCODICE = this.w_PNCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_TIPAGGCO = " "
      else
        * --- Leggo il mastro di raggruppamento ed il tipo di aggiornamento dell'analitica
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCCTAGG"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCCTAGG;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCON;
                and ANCODICE = this.w_PNCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPAGGCO = NVL(cp_ToDate(_read_.ANCCTAGG),cp_NullValue(_read_.ANCCTAGG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MODPAG = space(5)
      endif
      * --- Scrive Reg.Contabili (Movimentazione)
      * --- Try
      local bErr_03E09A10
      bErr_03E09A10=bTrsErr
      this.Try_03E09A10()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Raise
        i_Error="Impossibile creare dettaglio primanota"
        return
      endif
      bTrsErr=bTrsErr or bErr_03E09A10
      * --- End
      * --- Eventuale gestione analitica...
      if g_APPLICATION = "ad hoc ENTERPRISE"
        * --- Leggo flag gestione analitica
        * --- Read from BUSIUNIT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.BUSIUNIT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BUSIUNIT_idx,2],.t.,this.BUSIUNIT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BUFLANAL"+;
            " from "+i_cTable+" BUSIUNIT where ";
                +"BUCODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and BUCODICE = "+cp_ToStrODBC(this.w_BUSUNIRI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BUFLANAL;
            from (i_cTable) where;
                BUCODAZI = i_CODAZI;
                and BUCODICE = this.w_BUSUNIRI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLBUANAL = NVL(cp_ToDate(_read_.BUFLANAL),cp_NullValue(_read_.BUFLANAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_TIPAGGCO = "A" AND ( this.w_FLBUANAL = "S" OR EMPTY (NVL (this.w_BUSUNIRI," ")))
          if this.oParentObject.w_APDATREG <= g_CONANA
            * --- Raise
            i_Error="Impossibile generare movimento di primanota perch� data registrazione inferiore alla data consolidamento analitica"
            return
          endif
          this.w_TOTCALC = 0
          this.w_TOTBIL = 0
          this.w_TOTIMP = this.w_PNIMPDAR-this.w_PNIMPAVE
          this.w_CPROWNUM2 = 0
          this.w_SEGNOAN = iif(this.w_TOTIMP>=0,"D","A")
          this.w_TOTIMP = ABS(this.w_TOTIMP)
          this.w_FINEMOV = .F.
          this.w_OLCODBUN = "VALORENONAMMESSO"
          this.w_OLROWORD = 0
          this.w_PNCODBUN = this.w_BUSUNIRI
          this.w_OLTIPCON = this.w_PNTIPCON
          this.w_PNTIPCON = "G"
          vq_exec("query\GSTE_BIP.VQR",this,"PARAMETRI")
          if USED("Parametri")
            Select PARAMETRI 
 Go Top
            this.w_SOMPAR = PARAMETRI.TOTPARAM
          endif
          * --- Leggo la voce di costo, il centro di costo ed il parametro associato al conto di quadratura
          * --- Select from GSCA_QCC
          do vq_exec with 'GSCA_QCC',this,'_Curs_GSCA_QCC','',.f.,.t.
          if used('_Curs_GSCA_QCC')
            select _Curs_GSCA_QCC
            locate for 1=1
            do while not(eof())
            this.w_VOCCOST = NVL(_Curs_GSCA_QCC.MRCODVOC,SPACE(15))
            this.w_CENCOST = NVL(_Curs_GSCA_QCC.MRCODCDC,SPACE(15))
            this.w_PARAM = NVL(_Curs_GSCA_QCC.MRPARAME,0)
            this.w_IMPORTO = cp_ROUND(this.w_TOTIMP*this.w_PARAM/this.w_SOMPAR,g_PERPVL)
            this.w_CPROWNUM2 = this.w_CPROWNUM2 + 1
            * --- Controllo se la somma degli importi calcolati con i parametri � uguale all'importo iniziale.
            this.w_TOTCALC = this.w_TOTCALC+this.w_IMPORTO
            * --- Try
            local bErr_03DFA180
            bErr_03DFA180=bTrsErr
            this.Try_03DFA180()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- Raise
              i_Error="Impossibile creare movimenti di analitica"
              return
            endif
            bTrsErr=bTrsErr or bErr_03DFA180
            * --- End
            if _Curs_GSCA_QCC.MRROWORD<>this.w_OLROWORD 
              if this.w_OLROWORD=0
                this.w_OLROWORD = _Curs_GSCA_QCC.MRROWORD
              else
                EXIT
              endif
            endif
              select _Curs_GSCA_QCC
              continue
            enddo
            use
          endif
          * --- Bilancio il calcolo delle percentuali del frazionamento 
          this.w_TOTBIL = ABS(this.w_TOTCALC - this.w_TOTIMP)
          if NOT (EMPTY (this.w_VOCCOST) AND EMPTY (this.w_CENCOST) )
            do case
              case (this.w_TOTCALC - this.w_TOTIMP) > 0
                * --- Write into MOVICOST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.MOVICOST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVICOST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MRTOTIMP =MRTOTIMP- "+cp_ToStrODBC(this.w_TOTBIL);
                      +i_ccchkf ;
                  +" where ";
                      +"MRSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                      +" and MRROWORD = "+cp_ToStrODBC(this.w_CPROWNUM);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM2);
                         )
                else
                  update (i_cTable) set;
                      MRTOTIMP = MRTOTIMP - this.w_TOTBIL;
                      &i_ccchkf. ;
                   where;
                      MRSERIAL = this.w_PNSERIAL;
                      and MRROWORD = this.w_CPROWNUM;
                      and CPROWNUM = this.w_CPROWNUM2;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              case (this.w_TOTCALC - this.w_TOTIMP)< 0
                * --- Write into MOVICOST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.MOVICOST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVICOST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MRTOTIMP =MRTOTIMP+ "+cp_ToStrODBC(this.w_TOTBIL);
                      +i_ccchkf ;
                  +" where ";
                      +"MRSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                      +" and MRROWORD = "+cp_ToStrODBC(this.w_CPROWNUM);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM2);
                         )
                else
                  update (i_cTable) set;
                      MRTOTIMP = MRTOTIMP + this.w_TOTBIL;
                      &i_ccchkf. ;
                   where;
                      MRSERIAL = this.w_PNSERIAL;
                      and MRROWORD = this.w_CPROWNUM;
                      and CPROWNUM = this.w_CPROWNUM2;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
            endcase
          endif
        else
          if this.oParentObject.w_APDATREG <= g_CONANA
            * --- Raise
            i_Error="Impossibile generare movimento di primanota perch� data registrazione inferiore alla data consolidamento analitica"
            return
          endif
        endif
      else
        * --- Gestione Analitica
        if this.oParentObject.w_FLANAL="S" And this.w_PNTIPCON="G" And g_PERCCR="S"
          this.w_TOTIMP = this.w_PNIMPDAR-this.w_PNIMPAVE
          this.w_MESS_ERR = GSAR_BRA(This,this.w_PNSERIAL, this.w_CPROWNUM, this.w_PNTIPCON, this.w_PNCODCON, this.w_TOTIMP)
          if Not EMPTY(this.w_MESS_ERR)
            this.w_oERRORLOG.AddMsgLog("Verificare reg. n. %1 del %2", ALLTRIM(STR(this.w_PNNUMRER)), DTOC(this.w_PNDATREG))     
            this.w_oERRORLOG.AddMsgLogNoTranslate(Repl("=",70))     
            this.w_oERRORLOG.AddMsgLogPartNoTrans(this.w_MESS_ERR,iif( this.w_PNTIPCON="G" , "%1conto %2" , iif( this.w_PNTIPCON="C" , "%1cliente %2" , "%1forntiore %2") ),space(1),this.w_PNCODCON)     
            this.w_oERRORLOG.AddMsgLogNoTranslate(Repl("=",70))     
            this.w_oERRORLOG.AddMsgLogNoTranslate(space(70))     
            this.w_WDOC = this.w_WDOC + 1
          endif
        endif
      endif
    endif
  endproc
  proc Try_03E09A10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if g_APPLICATION = "ad hoc ENTERPRISE"
      * --- Insert into PNT_DETT
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLSALF"+",PNDESRIG"+",PNCAURIG"+",PNFLSALD"+",PNFLSALI"+",PNFLPART"+",PNCODPAG"+",PNCODBUN"+",PNFLZERO"+",PNFLSMNS"+",PNORIGS2"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALF),'PNT_DETT','PNFLSALF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAURIG),'PNT_DETT','PNCAURIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PNT_DETT','PNFLPART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MODPAG),'PNT_DETT','PNCODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_BUSUNIRI),'PNT_DETT','PNCODBUN');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLZERO');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSMNS');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNORIGS2');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLSALF',this.w_PNFLSALF,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCAURIG,'PNFLSALD',this.w_PNFLSALD,'PNFLSALI'," ")
        insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLSALF,PNDESRIG,PNCAURIG,PNFLSALD,PNFLSALI,PNFLPART,PNCODPAG,PNCODBUN,PNFLZERO,PNFLSMNS,PNORIGS2 &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,this.w_PNTIPCON;
             ,this.w_PNCODCON;
             ,this.w_PNIMPDAR;
             ,this.w_PNIMPAVE;
             ,this.w_PNFLSALF;
             ,this.w_PNDESRIG;
             ,this.w_PNCAURIG;
             ,this.w_PNFLSALD;
             ," ";
             ,this.w_PNFLPART;
             ,this.w_MODPAG;
             ,this.w_BUSUNIRI;
             ," ";
             ," ";
             ," ";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into PNT_DETT
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLSALF"+",PNDESRIG"+",PNCAURIG"+",PNFLSALD"+",PNFLSALI"+",PNFLPART"+",PNCODPAG"+",PNCODBUN"+",PNFLZERO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALF),'PNT_DETT','PNFLSALF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAURIG),'PNT_DETT','PNCAURIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PNT_DETT','PNFLPART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MODPAG),'PNT_DETT','PNCODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_BUSUNIRI),'PNT_DETT','PNCODBUN');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLZERO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLSALF',this.w_PNFLSALF,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCAURIG,'PNFLSALD',this.w_PNFLSALD,'PNFLSALI'," ")
        insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLSALF,PNDESRIG,PNCAURIG,PNFLSALD,PNFLSALI,PNFLPART,PNCODPAG,PNCODBUN,PNFLZERO &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,this.w_PNTIPCON;
             ,this.w_PNCODCON;
             ,this.w_PNIMPDAR;
             ,this.w_PNIMPAVE;
             ,this.w_PNFLSALF;
             ,this.w_PNDESRIG;
             ,this.w_PNCAURIG;
             ,this.w_PNFLSALD;
             ," ";
             ,this.w_PNFLPART;
             ,this.w_MODPAG;
             ,this.w_BUSUNIRI;
             ," ";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    return
  proc Try_03DFA180()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Se al conto sono associati dei conti di analitica in una business unit specifica allora non vengono utilizxzzati i conti della business unit di default  (business unit vuota)
    if (NVL (MRCODBUN,"VALORENONAMMESSO")=this.w_OLCODBUN OR this.w_OLCODBUN="VALORENONAMMESSO") AND this.w_FINEMOV=.F.
      if NOT (EMPTY (this.w_VOCCOST) AND EMPTY (this.w_CENCOST) )
        * --- Insert into MOVICOST
        i_nConn=i_TableProp[this.MOVICOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOVICOST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MRSERIAL"+",MRROWORD"+",MRCODVOC"+",MRCODICE"+",MRPARAME"+",MR_SEGNO"+",CPROWNUM"+",MRTOTIMP"+",MRFLRIPA"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'MOVICOST','MRSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MOVICOST','MRROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_VOCCOST),'MOVICOST','MRCODVOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CENCOST),'MOVICOST','MRCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PARAM),'MOVICOST','MRPARAME');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SEGNOAN),'MOVICOST','MR_SEGNO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM2),'MOVICOST','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPORTO),'MOVICOST','MRTOTIMP');
          +","+cp_NullLink(cp_ToStrODBC(" "),'MOVICOST','MRFLRIPA');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',this.w_PNSERIAL,'MRROWORD',this.w_CPROWNUM,'MRCODVOC',this.w_VOCCOST,'MRCODICE',this.w_CENCOST,'MRPARAME',this.w_PARAM,'MR_SEGNO',this.w_SEGNOAN,'CPROWNUM',this.w_CPROWNUM2,'MRTOTIMP',this.w_IMPORTO,'MRFLRIPA'," ")
          insert into (i_cTable) (MRSERIAL,MRROWORD,MRCODVOC,MRCODICE,MRPARAME,MR_SEGNO,CPROWNUM,MRTOTIMP,MRFLRIPA &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_CPROWNUM;
               ,this.w_VOCCOST;
               ,this.w_CENCOST;
               ,this.w_PARAM;
               ,this.w_SEGNOAN;
               ,this.w_CPROWNUM2;
               ,this.w_IMPORTO;
               ," ";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    else
      this.w_FINEMOV = .T.
    endif
    if MRCODBUN<>this.w_OLCODBUN 
      this.w_OLCODBUN = _Curs_GSCA_QCC.MRCODBUN 
    endif
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzo i campi chiave della partita che deve essere aggiornata
    this.w_PTCAOVAL = this.oParentObject.w_APCAOVAL
    * --- Check Multi utenza, verifico se modificato il cambio letto (quelllo che
    *     sar� memorizzato in CAOOLD). Quindi confronto PTCAOOLD con PTCAOAPE
    *     attuale, se modificati qualcun'altro ha confermato la partita...
    if g_APPLICATION = "ad hoc ENTERPRISE"
      * --- Read from PAR_TITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PTCAOAPE"+;
          " from "+i_cTable+" PAR_TITE where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
              +" and PTORIGSA = "+cp_ToStrODBC(this.w_PTORIGSA);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUMP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PTCAOAPE;
          from (i_cTable) where;
              PTSERIAL = this.w_PTSERIAL;
              and PTORIGSA = this.w_PTORIGSA;
              and PTROWORD = this.w_PTROWORD;
              and CPROWNUM = this.w_CPROWNUMP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TESTCAO = NVL(cp_ToDate(_read_.PTCAOAPE),cp_NullValue(_read_.PTCAOAPE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from PAR_TITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PTCAOAPE"+;
          " from "+i_cTable+" PAR_TITE where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUMP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PTCAOAPE;
          from (i_cTable) where;
              PTSERIAL = this.w_PTSERIAL;
              and PTROWORD = this.w_PTROWORD;
              and CPROWNUM = this.w_CPROWNUMP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TESTCAO = NVL(cp_ToDate(_read_.PTCAOAPE),cp_NullValue(_read_.PTCAOAPE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if this.w_PTCAOOLD<>this.w_TESTCAO
      this.w_MESS = ah_MsgFormat("Attenzione un'altro utente ha utilizzato le stesse scadenze in una valutazione. Impossibile generare reg. di prima nota.")
      this.w_OK = .F.
    else
      * --- Possono accadere due casi, la partita di creazione non � mai stata
      *     saldata (neppure parzialmente), oppure la partita e parzialmente saldata.
      *     
      *     In quest'ultimo caso la procedura deve duplicare la partita di creazione per
      *     ottenere due creazioni una chiusa ed una aperta, su quest'ultima si modifica il
      *     cambio mentre sulla prima si modifica l'importo.
      if g_APPLICATION = "ad hoc ENTERPRISE"
        * --- Devo verificare se dividere la partita di creazione con nuovo codice di pagamento
        if this.w_NUMSCA>1
          local prPTSERIAL,prPTORIGSA,prPTROWORD,prCPROWNUM 
 prPTSERIAL=this.w_PTSERIAL 
 prPTORIGSA=this.w_PTORIGSA 
 prPTROWORD=this.w_PTROWORD 
 prCPROWNUM=this.w_CPROWNUMP
          DIVIDESCAD(this,@prPTSERIAL,@prPTORIGSA,@prPTROWORD,@prCPROWNUM,iif(this.w_PT_SEGNO="A",-1,1)*this.w_PTTOTIMP)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_PTSERIAL=prPTSERIAL 
 this.w_PTORIGSA=prPTORIGSA 
 this.w_PTROWORD=prPTROWORD 
 this.w_CPROWNUMP=prCPROWNUM
        endif
        this.w_NU_RIGA = this.w_CPROWNUMP
        * --- Write into PAR_TITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL,PTMODPAG"
          do vq_exec with 'query\gscg_bap_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="PAR_TITE.PTNUMPAR = _t2.PTNUMPAR";
                  +" and "+"PAR_TITE.PTDATSCA = _t2.PTDATSCA";
                  +" and "+"PAR_TITE.PTTIPCON = _t2.PTTIPCON";
                  +" and "+"PAR_TITE.PTCODCON = _t2.PTCODCON";
                  +" and "+"PAR_TITE.PTCODVAL = _t2.PTCODVAL";
                  +" and "+"PAR_TITE.PTMODPAG = _t2.PTMODPAG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
          +",PTCAOAPE ="+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOAPE');
              +i_ccchkf;
              +" from "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="PAR_TITE.PTNUMPAR = _t2.PTNUMPAR";
                  +" and "+"PAR_TITE.PTDATSCA = _t2.PTDATSCA";
                  +" and "+"PAR_TITE.PTTIPCON = _t2.PTTIPCON";
                  +" and "+"PAR_TITE.PTCODCON = _t2.PTCODCON";
                  +" and "+"PAR_TITE.PTCODVAL = _t2.PTCODVAL";
                  +" and "+"PAR_TITE.PTMODPAG = _t2.PTMODPAG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 set ";
          +"PAR_TITE.PTCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
          +",PAR_TITE.PTCAOAPE ="+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOAPE');
              +Iif(Empty(i_ccchkf),"",",PAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="PAR_TITE.PTNUMPAR = t2.PTNUMPAR";
                  +" and "+"PAR_TITE.PTDATSCA = t2.PTDATSCA";
                  +" and "+"PAR_TITE.PTTIPCON = t2.PTTIPCON";
                  +" and "+"PAR_TITE.PTCODCON = t2.PTCODCON";
                  +" and "+"PAR_TITE.PTCODVAL = t2.PTCODVAL";
                  +" and "+"PAR_TITE.PTMODPAG = t2.PTMODPAG";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set (";
              +"PTCAOVAL,";
              +"PTCAOAPE";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL')+",";
              +cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOAPE')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="PAR_TITE.PTNUMPAR = _t2.PTNUMPAR";
                  +" and "+"PAR_TITE.PTDATSCA = _t2.PTDATSCA";
                  +" and "+"PAR_TITE.PTTIPCON = _t2.PTTIPCON";
                  +" and "+"PAR_TITE.PTCODCON = _t2.PTCODCON";
                  +" and "+"PAR_TITE.PTCODVAL = _t2.PTCODVAL";
                  +" and "+"PAR_TITE.PTMODPAG = _t2.PTMODPAG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set ";
          +"PTCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
          +",PTCAOAPE ="+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOAPE');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".PTNUMPAR = "+i_cQueryTable+".PTNUMPAR";
                  +" and "+i_cTable+".PTDATSCA = "+i_cQueryTable+".PTDATSCA";
                  +" and "+i_cTable+".PTTIPCON = "+i_cQueryTable+".PTTIPCON";
                  +" and "+i_cTable+".PTCODCON = "+i_cQueryTable+".PTCODCON";
                  +" and "+i_cTable+".PTCODVAL = "+i_cQueryTable+".PTCODVAL";
                  +" and "+i_cTable+".PTMODPAG = "+i_cQueryTable+".PTMODPAG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
          +",PTCAOAPE ="+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOAPE');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        this.w_NU_RIGA = this.w_CPROWNUMP
        * --- Verifico se devo dividere la partita di creazione, la divido nel caso in cui
        *     sia stata parzialmente sadlata...
        this.w_DIVIDE = GSTE_BSP( This , this.w_PTSERIAL , this.w_PTROWORD, this.w_CPROWNUMP ) 
        if this.w_DIVIDE
          * --- Write into PAR_TITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTTOTIMP =PTTOTIMP- "+cp_ToStrODBC(this.w_IMP_APE);
                +i_ccchkf ;
            +" where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
                +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUMP);
                   )
          else
            update (i_cTable) set;
                PTTOTIMP = PTTOTIMP - this.w_IMP_APE;
                &i_ccchkf. ;
             where;
                PTSERIAL = this.w_PTSERIAL;
                and PTROWORD = this.w_PTROWORD;
                and CPROWNUM = this.w_CPROWNUMP;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Creo la nuova partita...
          *     Determino il CPROWNUM..
          * --- Select from PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select Max( CPROWNUM ) As NU_RIGA  from "+i_cTable+" PAR_TITE ";
                +" where PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL)+" And PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD)+"";
                 ,"_Curs_PAR_TITE")
          else
            select Max( CPROWNUM ) As NU_RIGA from (i_cTable);
             where PTSERIAL = this.w_PTSERIAL And PTROWORD = this.w_PTROWORD;
              into cursor _Curs_PAR_TITE
          endif
          if used('_Curs_PAR_TITE')
            select _Curs_PAR_TITE
            locate for 1=1
            do while not(eof())
            this.w_NU_RIGA = Nvl( _Curs_PAR_TITE.NU_RIGA , 0 ) + 1
              select _Curs_PAR_TITE
              continue
            enddo
            use
          endif
          * --- Creo la partita...
          * --- Select from PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select *  from "+i_cTable+" PAR_TITE ";
                +" where PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL)+" And PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD)+" And CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUMP)+"";
                 ,"_Curs_PAR_TITE")
          else
            select * from (i_cTable);
             where PTSERIAL = this.w_PTSERIAL And PTROWORD = this.w_PTROWORD And CPROWNUM = this.w_CPROWNUMP;
              into cursor _Curs_PAR_TITE
          endif
          if used('_Curs_PAR_TITE')
            select _Curs_PAR_TITE
            locate for 1=1
            do while not(eof())
            * --- Insert into PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTBANNOS"+",PTFLRAGG"+",PTFLRITE"+",PTTOTABB"+",PTFLCRSA"+",PTFLIMPE"+",PTNUMDIS"+",PTNUMEFF"+",PTFLINDI"+",PTDESRIG"+",PTNUMPRO"+",PTCODAGE"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTFLVABD"+",PTDATINT"+",PTRIFIND"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTSERIAL),'PAR_TITE','PTSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTROWORD),'PAR_TITE','PTROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NU_RIGA),'PAR_TITE','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTNUMPAR),'PAR_TITE','PTNUMPAR');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTDATSCA),'PAR_TITE','PTDATSCA');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTTIPCON),'PAR_TITE','PTTIPCON');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTCODCON),'PAR_TITE','PTCODCON');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PT_SEGNO),'PAR_TITE','PT_SEGNO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IMP_APE),'PAR_TITE','PTTOTIMP');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTCODVAL),'PAR_TITE','PTCODVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOAPE');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTDATAPE),'PAR_TITE','PTDATAPE');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTNUMDOC),'PAR_TITE','PTNUMDOC');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTALFDOC),'PAR_TITE','PTALFDOC');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTDATDOC),'PAR_TITE','PTDATDOC');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTIMPDOC),'PAR_TITE','PTIMPDOC');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTMODPAG),'PAR_TITE','PTMODPAG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTFLSOSP),'PAR_TITE','PTFLSOSP');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTBANAPP),'PAR_TITE','PTBANAPP');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTBANNOS),'PAR_TITE','PTBANNOS');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTFLRAGG),'PAR_TITE','PTFLRAGG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTFLRITE),'PAR_TITE','PTFLRITE');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTTOTABB),'PAR_TITE','PTTOTABB');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTFLCRSA),'PAR_TITE','PTFLCRSA');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTFLIMPE),'PAR_TITE','PTFLIMPE');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTNUMDIS),'PAR_TITE','PTNUMDIS');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTNUMEFF),'PAR_TITE','PTNUMEFF');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTFLINDI),'PAR_TITE','PTFLINDI');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTDESRIG),'PAR_TITE','PTDESRIG');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTNUMPRO),'PAR_TITE','PTNUMPRO');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTCODAGE),'PAR_TITE','PTCODAGE');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTSERRIF),'PAR_TITE','PTSERRIF');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTORDRIF),'PAR_TITE','PTORDRIF');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTNUMRIF),'PAR_TITE','PTNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTFLVABD),'PAR_TITE','PTFLVABD');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTDATINT),'PAR_TITE','PTDATINT');
              +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTRIFIND),'PAR_TITE','PTRIFIND');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',_Curs_PAR_TITE.PTSERIAL,'PTROWORD',_Curs_PAR_TITE.PTROWORD,'CPROWNUM',this.w_NU_RIGA,'PTNUMPAR',_Curs_PAR_TITE.PTNUMPAR,'PTDATSCA',_Curs_PAR_TITE.PTDATSCA,'PTTIPCON',_Curs_PAR_TITE.PTTIPCON,'PTCODCON',_Curs_PAR_TITE.PTCODCON,'PT_SEGNO',_Curs_PAR_TITE.PT_SEGNO,'PTTOTIMP',this.w_IMP_APE,'PTCODVAL',_Curs_PAR_TITE.PTCODVAL,'PTCAOVAL',this.w_PTCAOVAL,'PTCAOAPE',this.w_PTCAOVAL)
              insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTBANNOS,PTFLRAGG,PTFLRITE,PTTOTABB,PTFLCRSA,PTFLIMPE,PTNUMDIS,PTNUMEFF,PTFLINDI,PTDESRIG,PTNUMPRO,PTCODAGE,PTSERRIF,PTORDRIF,PTNUMRIF,PTFLVABD,PTDATINT,PTRIFIND &i_ccchkf. );
                 values (;
                   _Curs_PAR_TITE.PTSERIAL;
                   ,_Curs_PAR_TITE.PTROWORD;
                   ,this.w_NU_RIGA;
                   ,_Curs_PAR_TITE.PTNUMPAR;
                   ,_Curs_PAR_TITE.PTDATSCA;
                   ,_Curs_PAR_TITE.PTTIPCON;
                   ,_Curs_PAR_TITE.PTCODCON;
                   ,_Curs_PAR_TITE.PT_SEGNO;
                   ,this.w_IMP_APE;
                   ,_Curs_PAR_TITE.PTCODVAL;
                   ,this.w_PTCAOVAL;
                   ,this.w_PTCAOVAL;
                   ,_Curs_PAR_TITE.PTDATAPE;
                   ,_Curs_PAR_TITE.PTNUMDOC;
                   ,_Curs_PAR_TITE.PTALFDOC;
                   ,_Curs_PAR_TITE.PTDATDOC;
                   ,_Curs_PAR_TITE.PTIMPDOC;
                   ,_Curs_PAR_TITE.PTMODPAG;
                   ,_Curs_PAR_TITE.PTFLSOSP;
                   ,_Curs_PAR_TITE.PTBANAPP;
                   ,_Curs_PAR_TITE.PTBANNOS;
                   ,_Curs_PAR_TITE.PTFLRAGG;
                   ,_Curs_PAR_TITE.PTFLRITE;
                   ,_Curs_PAR_TITE.PTTOTABB;
                   ,_Curs_PAR_TITE.PTFLCRSA;
                   ,_Curs_PAR_TITE.PTFLIMPE;
                   ,_Curs_PAR_TITE.PTNUMDIS;
                   ,_Curs_PAR_TITE.PTNUMEFF;
                   ,_Curs_PAR_TITE.PTFLINDI;
                   ,_Curs_PAR_TITE.PTDESRIG;
                   ,_Curs_PAR_TITE.PTNUMPRO;
                   ,_Curs_PAR_TITE.PTCODAGE;
                   ,_Curs_PAR_TITE.PTSERRIF;
                   ,_Curs_PAR_TITE.PTORDRIF;
                   ,_Curs_PAR_TITE.PTNUMRIF;
                   ,_Curs_PAR_TITE.PTFLVABD;
                   ,_Curs_PAR_TITE.PTDATINT;
                   ,_Curs_PAR_TITE.PTRIFIND;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
              select _Curs_PAR_TITE
              continue
            enddo
            use
          endif
        else
          * --- Write into PAR_TITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
            +",PTCAOAPE ="+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOAPE');
                +i_ccchkf ;
            +" where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
                +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUMP);
                   )
          else
            update (i_cTable) set;
                PTCAOVAL = this.w_PTCAOVAL;
                ,PTCAOAPE = this.w_PTCAOVAL;
                &i_ccchkf. ;
             where;
                PTSERIAL = this.w_PTSERIAL;
                and PTROWORD = this.w_PTROWORD;
                and CPROWNUM = this.w_CPROWNUMP;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      * --- Aggiorno il cursore VALDATPA
       
 SELECT VALDATPA 
 APPEND BLANK 
 REPLACE DVPNSERI WITH this.w_PNSERIAL 
 REPLACE DVPASERI WITH this.w_PTSERIAL 
 REPLACE DVORIGSA WITH this.w_PTORIGSA 
 REPLACE DVROWORD WITH this.w_PTROWORD 
 REPLACE DVROWNUM WITH this.w_NU_RIGA 
 REPLACE DVCAOOLD WITH this.w_PTCAOOLD
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='CONTROPA'
    this.cWorkTables[5]='PNT_DETT'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='BUSIUNIT'
    this.cWorkTables[8]='MOVICOST'
    this.cWorkTables[9]='COLLCENT'
    this.cWorkTables[10]='PAR_TITE'
    this.cWorkTables[11]='VALDATPA'
    this.cWorkTables[12]='VALUTE'
    this.cWorkTables[13]='CAU_CONT'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_GSCA_QCC')
      use in _Curs_GSCA_QCC
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
