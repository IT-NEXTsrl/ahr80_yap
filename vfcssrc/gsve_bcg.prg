* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bcg                                                        *
*              Cancellazione generazione documentale                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-06-22                                                      *
* Last revis.: 2005-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bcg",oParentObject)
return(i_retval)

define class tgsve_bcg as StdBatch
  * --- Local variables
  * --- WorkFile variables
  LDOCGENE_idx=0
  LOG_GEND_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Delete from LDOCGENE
    i_nConn=i_TableProp[this.LDOCGENE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LDOCGENE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"LDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_GDSERIAL);
             )
    else
      delete from (i_cTable) where;
            LDSERIAL = this.oParentObject.w_GDSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Elimino il log associato
    * --- Delete from LOG_GEND
    i_nConn=i_TableProp[this.LOG_GEND_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOG_GEND_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"LGSERIAL = "+cp_ToStrODBC(this.oParentObject.w_GDSERIAL);
             )
    else
      delete from (i_cTable) where;
            LGSERIAL = this.oParentObject.w_GDSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='LDOCGENE'
    this.cWorkTables[2]='LOG_GEND'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
