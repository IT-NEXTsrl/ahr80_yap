* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bto                                                        *
*              TREEVIEW ORGANIGRAMMA                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-11                                                      *
* Last revis.: 2009-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bto",oParentObject,m.pOPER)
return(i_retval)

define class tgsar_bto as StdBatch
  * --- Local variables
  pOPER = space(3)
  w_TMPRIS = space(5)
  w_TMPLIVRI = 0
  w_CHKREAD = .f.
  w_RCLRETVAL = 0
  w_OBJGEST = .NULL.
  w_CURREC = 0
  w_CURLVLKEY = space(254)
  w_CONTA = 0
  w_CHKSTRUT = .f.
  w_ATTCPCCH = space(10)
  w_RESULT = space(10)
  * --- WorkFile variables
  CONTITMP_idx=0
  TRW_ORGA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pOPER=="FTW"
        vq_exec("QUERY\GSAR_BTO.VQR", this, "Organigr")
        if this.oParentObject.w_FLTIPOPE<>"T"
          vq_exec("QUERY\GSARABTO.VQR", this, "RisVis")
          * --- Elimino dalla treeview le persone/risorse che non posso visualizzare
          DELETE FROM "Organigr" WHERE TOTIPRIS<>"G" AND ALLTRIM(TOCODICE)+ALLTRIM(TOGRPBAS) NOT IN (SELECT ALLTRIM(DPCODICE)+ALLTRIM(DPCODGRU) FROM "RisVis")
          * --- Elimino dalla treeviw i livelli non visualizzabili
          DELETE FROM "Organigr" WHERE NOT(TOTIPRIS="G" AND TO_Level=1) AND ((TO_level<this.oParentObject.w_LEVGRPST AND TOTIPRIS<>"G") OR (TO_level<this.oParentObject.w_LEVGRPST-1 AND TOTIPRIS="G") OR (TO_LEVEL>this.oParentObject.w_LEVGRPST+this.oParentObject.w_FLTLIVPR) OR (LVLKEY NOT like "%"+this.oParentObject.w_FLTGRUAP+"%" )) 
          SELECT "Organigr"
          GO TOP
          this.w_CURREC = 1
          this.w_CURLVLKEY = ""
          Local L_DelCod
          L_DelCod=""
          SCAN FOR TOTIPRIS="G"
          this.w_CURREC = RECNO()
          this.w_CURLVLKEY = ALLTRIM(LVLKEY)
          SELECT COUNT(*) AS CONTA FROM Organigr WHERE SUBSTR(LVLKEY,1,LEN(this.w_CURLVLKEY)+1)=this.w_CURLVLKEY+"." INTO CURSOR "ContaRec"
          SELECT "Organigr"
          GO this.w_CURREC
          if USED("ContaRec")
            SELECT "ContaRec"
            GO TOP
            if NVL(CONTA,0)<1
              L_DelCod = L_DelCod + ", '" + ALLTRIM(Organigr.TOCODICE)+"'" 
            endif
            USE IN SELECT("ContaRec")
          endif
          SELECT "Organigr"
          GO this.w_CURREC
          ENDSCAN
          USE IN SELECT("ContaRec")
          USE IN SELECT("RisVis")
          if !EMPTY(L_DelCod)
            L_DelCod = "("+SUBSTR(L_DelCod ,2, LEN(L_DelCod)-1)+")"
            DELETE FROM "Organigr" WHERE TOTIPRIS="G" AND TOCODICE IN &L_DelCod
          endif
        endif
        if !EMPTY(this.oParentObject.w_RisSelez)
          * --- Dobbiamo eliminare i gruppi che non hanno sottogruppi
          *     Nella prima select estraiamo quei gruppi che NON sono foglie e che quindi hanno almeno una persona che li utilizza nel path, 
          *     quindi eliminiamo quelli che non compaiono nell'elenco in modo da avere solo i gruppi che appunto hanno almeno 
          *     una foglia o un altro nodo successivo
          SELECT DISTINCT LVLKEY AS PersPath, LVLKEY AS PATHDEF, RAT(".",LVLKEY)-1 AS PosizPunto FROM ORGANIGR WHERE TOTIPRIS<>"G" INTO CURSOR GrpUsati
          * --- Dobbiamo utilizzare un cursore di appoggio perch� non viene valutata correttamente la SUBSTR(LVLKEY, 1, RAT('.',LVLKEY)-1) nell'ambito della stessa select
          WRCURSOR("GrpUsati")
          * --- La SUBSTR non funziona correttamente nella not in, mentre cos� facendo abbiamo nel cursore direttamente il campo definitivo
          REPLACE ALL PersPath WITH LEFT(PathDEF, PosizPunto)
          DELETE FROM ORGANIGR WHERE TOTIPRIS="G" AND LVLKEY NOT IN (SELECT PersPath FROM GrpUsati) 
          USE IN SELECT("GrpUsati")
          SELECT "Organigr"
        endif
        UPDATE "ORGANIGR" SET cpBmpName="BMP\Kit.bmp" WHERE TOTIPRIS="G"
        UPDATE "ORGANIGR" SET cpBmpName="BMP\Utepos.bmp" WHERE TOTIPRIS="P" AND TOFLRESP="N"
        UPDATE "ORGANIGR" SET cpBmpName="BMP\WEADD.bmp" WHERE TOTIPRIS="P" AND TOFLRESP="S"
        UPDATE "ORGANIGR" SET cpBmpName="BMP\Mostra.bmp" WHERE TOTIPRIS="R"
        INDEX ON LVLKEY TAG LVLKEY COLLATE "MACHINE"
        this.oParentObject.w_ORGANIGR.cCursor = "Organigr"
        This.oParentObject.NotifyEvent("CaricaTree")
        this.oParentObject.w_FLTRWFIL = .T.
        this.oParentObject.w_OLDCPCCH = "@@##ZZ__AU"
        * --- Read from TRW_ORGA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TRW_ORGA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRW_ORGA_idx,2],.t.,this.TRW_ORGA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "cpccchk"+;
            " from "+i_cTable+" TRW_ORGA where ";
                +"TOTIPRIS = "+cp_ToStrODBC("G");
                +" and TO_LEVEL = "+cp_ToStrODBC(1);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            cpccchk;
            from (i_cTable) where;
                TOTIPRIS = "G";
                and TO_LEVEL = 1;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_OLDCPCCH = NVL(cp_ToDate(_read_.cpccchk),cp_NullValue(_read_.cpccchk))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !EMPTY(this.oParentObject.w_RisSelez)
          this.oParentObject.w_ORGANIGR.ExpandAll(.T.)     
        endif
      case this.pOPER=="END"
        USE IN SELECT("Organigr")
      case this.pOPER=="GEN"
        this.w_RESULT = GSAR_BTW(this,"ALL")
        if EMPTY(this.w_RESULT)
          ah_errormsg("Treeview struttura aziendale elaborata correttamente",64)
        else
          ah_errormsg(this.w_RESULT)
        endif
      case this.pOPER=="CRI" OR this.pOPER=="ZRI"
        if this.pOPER=="ZRI"
          this.w_TMPRIS = this.oParentObject.w_FLTCODRI
          this.oParentObject.w_FLTCODRI = ""
          vx_exec("gsarpbia.vzm",this.oParentObject)
          this.w_CHKREAD = ! this.oParentObject.w_FLTCODRI==this.w_TMPRIS
          this.w_TMPRIS = ""
        else
          this.w_CHKREAD = .T.
        endif
        if this.w_CHKREAD
          this.oParentObject.w_TMPCODRI = this.oParentObject.w_FLTCODRI
          this.oParentObject.w_FLTCODRI = ""
          * --- Create temporary table CONTITMP
          i_nIdx=cp_AddTableDef('CONTITMP') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('query\gsarpbia',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.CONTITMP_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          this.oParentObject.w_FLTCODRI = this.oParentObject.w_TMPCODRI
          this.oParentObject.w_TMPCODRI = ""
          * --- Read from CONTITMP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTITMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTITMP_idx,2],.t.,this.CONTITMP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "FLTCODRI"+;
              " from "+i_cTable+" CONTITMP where ";
                  +"1 = "+cp_ToStrODBC(1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              FLTCODRI;
              from (i_cTable) where;
                  1 = 1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TMPRIS = NVL(cp_ToDate(_read_.FLTCODRI),cp_NullValue(_read_.FLTCODRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Drop temporary table CONTITMP
          i_nIdx=cp_GetTableDefIdx('CONTITMP')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('CONTITMP')
          endif
          do case
            case i_Rows<1
              this.w_CHKREAD = .F.
            case i_Rows=1
              this.oParentObject.w_FLTCODRI = this.w_TMPRIS
              this.w_CHKREAD = .T.
            case i_Rows>1
              this.w_CHKREAD = GSAR_BTO(this.oParentObject, "ZRI")
          endcase
        else
          this.oParentObject.w_FLTCODRI = ""
        endif
        i_retcode = 'stop'
        i_retval = this.w_CHKREAD
        return
      case this.pOPER=="CGR" OR this.pOPER=="ZGR"
        if this.pOPER=="ZGR"
          this.w_TMPRIS = this.oParentObject.w_FLTGRUAP
          vx_exec("gsargbia.vzm",this.oParentObject)
          this.w_CHKREAD = ! this.oParentObject.w_FLTGRUAP==this.w_TMPRIS
        else
          this.w_CHKREAD = .T.
        endif
        if this.w_CHKREAD
          * --- Create temporary table CONTITMP
          i_nIdx=cp_AddTableDef('CONTITMP') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('query\gsargbia',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.CONTITMP_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Read from CONTITMP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTITMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTITMP_idx,2],.t.,this.CONTITMP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "FLTGRUAP,RELIVMIN"+;
              " from "+i_cTable+" CONTITMP where ";
                  +"1 = "+cp_ToStrODBC(1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              FLTGRUAP,RELIVMIN;
              from (i_cTable) where;
                  1 = 1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TMPRIS = NVL(cp_ToDate(_read_.FLTGRUAP),cp_NullValue(_read_.FLTGRUAP))
            this.w_TMPLIVRI = NVL(cp_ToDate(_read_.RELIVMIN),cp_NullValue(_read_.RELIVMIN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Drop temporary table CONTITMP
          i_nIdx=cp_GetTableDefIdx('CONTITMP')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('CONTITMP')
          endif
          do case
            case i_Rows<1
              this.w_CHKREAD = .F.
            case i_Rows=1
              this.oParentObject.w_FLTGRUAP = this.w_TMPRIS
              this.oParentObject.w_RELIVMIN = this.w_TMPLIVRI
              this.w_CHKREAD = .T.
            case i_Rows>1
              this.w_CHKREAD = GSAR_BTO(this.oParentObject, "ZGR")
          endcase
        else
          this.oParentObject.w_FLTGRUAP = ""
          this.oParentObject.w_RELIVMIN = 0
        endif
        i_retcode = 'stop'
        i_retval = this.w_CHKREAD
        return
      case this.pOPER=="NRC"
        this.w_RCLRETVAL = 0
        this.w_CHKSTRUT = .F.
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_CHKSTRUT
          if i_VisualTheme = -1
            DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT
            DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Apri")
            DEFINE BAR 2 OF popCmd PROMPT ah_MsgFormat("Modifica")
            DEFINE BAR 3 OF popCmd PROMPT ah_MsgFormat("Nuovo")
            ON SELECTION BAR 1 OF popCmd this.w_RCLRETVAL = 1
            ON SELECTION BAR 2 OF popCmd this.w_RCLRETVAL = 2
            ON SELECTION BAR 3 OF popCmd this.w_RCLRETVAL = 3
            ACTIVATE POPUP popCmd
            DEACTIVATE POPUP popCmd
            RELEASE POPUPS popCmd
          else
            local oBtnMenu, oMenuItem
            oBtnMenu = CreateObject("cbPopupMenu")
            oBtnMenu.oParentObject = This
            oBtnMenu.oPopupMenu.cOnClickProc="ExecScript(NAMEPROC)"
            oMenuItem = oBtnMenu.AddMenuItem()
            oMenuItem.Caption = Ah_Msgformat("Apri")
            oMenuItem.OnClick = ".Parent.oParentObject.w_RCLRETVAL=1"
            oMenuItem.Visible=.T.
            oMenuItem = oBtnMenu.AddMenuItem()
            oMenuItem.Caption = Ah_Msgformat("Modifica")
            oMenuItem.OnClick = ".Parent.oParentObject.w_RCLRETVAL=2"
            oMenuItem.Visible=.T.
            oMenuItem = oBtnMenu.AddMenuItem()
            oMenuItem.Caption = Ah_Msgformat("Nuovo")
            oMenuItem.OnClick = ".Parent.oParentObject.w_RCLRETVAL=3"
            oMenuItem.Visible=.T.
            oBtnMenu.InitMenu()
            oBtnMenu.ShowMenu()
            oBtnMenu = .NULL.
          endif
          if this.w_RCLRETVAL<>0
            this.w_OBJGEST = GSAR_ADP(.F., this.oParentObject.w_TOTIPRIS)
            * --- Verifica test accesso
            if this.w_OBJGEST.bSec1
              if this.w_RCLRETVAL<3
                this.w_OBJGEST.ecpFilter()     
                this.w_OBJGEST.w_DPCODICE = this.oParentObject.w_TOCODICE
                this.w_OBJGEST.w_DPTIPRIS = this.oParentObject.w_TOTIPRIS
                this.w_OBJGEST.ecpSave()     
                if this.w_RCLRETVAL=2
                  this.w_OBJGEST.ecpEdit()     
                endif
              else
                this.w_OBJGEST.ecpLoad()     
              endif
            else
              do case
                case this.oParentObject.w_TOTIPRIS="P"
                  Ah_ErrorMsg("Impossibile accedere alle persone")
                case this.oParentObject.w_TOTIPRIS="G"
                  Ah_ErrorMsg("Impossibile accedere ai gruppi")
                case this.oParentObject.w_TOTIPRIS="R"
                  Ah_ErrorMsg("Impossibile accedere alle risorse")
              endcase
            endif
          endif
        else
          if ah_YesNo("La struttura aziendale � stata modificata, occorre ricaricare la treeview. Procedere con l'aggiornamento adesso?")
            do GSAR_BTO with this.oParentObject, "FTW"
            ah_ErrorMsg("Aggiornamento completato corettamente", 64)
          else
            ah_ErrorMsg("Impossibile procedere senza ricaricare la treeview")
          endif
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_ATTCPCCH = "**********"
    * --- Read from TRW_ORGA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TRW_ORGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRW_ORGA_idx,2],.t.,this.TRW_ORGA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "cpccchk"+;
        " from "+i_cTable+" TRW_ORGA where ";
            +"TOTIPRIS = "+cp_ToStrODBC("G");
            +" and TO_LEVEL = "+cp_ToStrODBC(1);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        cpccchk;
        from (i_cTable) where;
            TOTIPRIS = "G";
            and TO_LEVEL = 1;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ATTCPCCH = NVL(cp_ToDate(_read_.cpccchk),cp_NullValue(_read_.cpccchk))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CHKSTRUT = this.w_ATTCPCCH = this.oParentObject.w_OLDCPCCH
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*CONTITMP'
    this.cWorkTables[2]='TRW_ORGA'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
