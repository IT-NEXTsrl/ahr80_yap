* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kdi                                                        *
*              Elenco dichiarazioni                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-09                                                      *
* Last revis.: 2017-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kdi",oParentObject))

* --- Class definition
define class tgsve_kdi as StdForm
  Top    = 28
  Left   = 80

  * --- Standard Properties
  Width  = 687
  Height = 342
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-11-08"
  HelpContextID=82457193
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  VOCIIVA_IDX = 0
  cPrg = "gsve_kdi"
  cComment = "Elenco dichiarazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_MVDATDOC = ctod('  /  /  ')
  w_MVTIPCON = space(1)
  w_OLDRIF = space(15)
  w_MVCODCON = space(10)
  w_MVRIFDIC = space(15)
  w_MVCODIVE = space(5)
  w_ANNDIC = space(4)
  w_DATDIC = ctod('  /  /  ')
  w_TIPDIC = space(1)
  w_TIPIVA = space(1)
  w_OPEDIC = space(1)
  w_UTIDIC = 0
  w_DESAPP = space(35)
  w_BOLIVE = space(1)
  w_PERIVE = 0
  w_DTOBSO = ctod('  /  /  ')
  w_NUMDIC = 0
  w_SERDOC = space(3)
  w_DICSN = space(1)
  w_DICINTE_INDIRETTA = space(15)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kdiPag1","gsve_kdi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='VOCIIVA'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MVDATDOC=ctod("  /  /  ")
      .w_MVTIPCON=space(1)
      .w_OLDRIF=space(15)
      .w_MVCODCON=space(10)
      .w_MVRIFDIC=space(15)
      .w_MVCODIVE=space(5)
      .w_ANNDIC=space(4)
      .w_DATDIC=ctod("  /  /  ")
      .w_TIPDIC=space(1)
      .w_TIPIVA=space(1)
      .w_OPEDIC=space(1)
      .w_UTIDIC=0
      .w_DESAPP=space(35)
      .w_BOLIVE=space(1)
      .w_PERIVE=0
      .w_DTOBSO=ctod("  /  /  ")
      .w_NUMDIC=0
      .w_SERDOC=space(3)
      .w_DICSN=space(1)
      .w_DICINTE_INDIRETTA=space(15)
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_MVTIPCON=oParentObject.w_MVTIPCON
      .w_MVCODCON=oParentObject.w_MVCODCON
      .w_MVRIFDIC=oParentObject.w_MVRIFDIC
      .w_MVCODIVE=oParentObject.w_MVCODIVE
      .w_ANNDIC=oParentObject.w_ANNDIC
      .w_DATDIC=oParentObject.w_DATDIC
      .w_TIPDIC=oParentObject.w_TIPDIC
      .w_TIPIVA=oParentObject.w_TIPIVA
      .w_OPEDIC=oParentObject.w_OPEDIC
      .w_UTIDIC=oParentObject.w_UTIDIC
      .w_DESAPP=oParentObject.w_DESAPP
      .w_BOLIVE=oParentObject.w_BOLIVE
      .w_PERIVE=oParentObject.w_PERIVE
      .w_DTOBSO=oParentObject.w_DTOBSO
      .w_NUMDIC=oParentObject.w_NUMDIC
      .w_SERDOC=oParentObject.w_SERDOC
      .w_DICSN=oParentObject.w_DICSN
      .w_DICINTE_INDIRETTA=oParentObject.w_DICINTE_INDIRETTA
          .DoRTCalc(1,2,.f.)
        .w_OLDRIF = THIS.OPARENTOBJECT.w_MVRIFDIC
          .DoRTCalc(4,4,.f.)
        .w_MVRIFDIC = .w_Zoom.getVar('DISERIAL')
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .w_MVCODIVE = .w_Zoom.getVar('DICODIVA')
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_MVCODIVE))
          .link_1_7('Full')
        endif
        .w_ANNDIC = .w_Zoom.getVar('DI__ANNO')
        .w_DATDIC = CP_TODATE(.w_Zoom.getVar('DIDATLET'))
        .w_TIPDIC = .w_Zoom.getVar('DITIPCON')
        .w_TIPIVA = .w_Zoom.getVar('DICODIVA')
        .w_OPEDIC = .w_Zoom.getVar('DITIPOPE')
        .w_UTIDIC = .w_Zoom.getVar('DIIMPUTI')
          .DoRTCalc(13,16,.f.)
        .w_NUMDIC = .w_Zoom.getVar('DINUMDOC')
        .w_SERDOC = .w_Zoom.getVar('DISERDOC')
        .w_DICSN = IIF(EMPTY(.w_MVRIFDIC), ' ', 'S')
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate('',RGB(0,255,0),RGB(0,255,0))
        .w_DICINTE_INDIRETTA = Space(15)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_MVTIPCON=.w_MVTIPCON
      .oParentObject.w_MVCODCON=.w_MVCODCON
      .oParentObject.w_MVRIFDIC=.w_MVRIFDIC
      .oParentObject.w_MVCODIVE=.w_MVCODIVE
      .oParentObject.w_ANNDIC=.w_ANNDIC
      .oParentObject.w_DATDIC=.w_DATDIC
      .oParentObject.w_TIPDIC=.w_TIPDIC
      .oParentObject.w_TIPIVA=.w_TIPIVA
      .oParentObject.w_OPEDIC=.w_OPEDIC
      .oParentObject.w_UTIDIC=.w_UTIDIC
      .oParentObject.w_DESAPP=.w_DESAPP
      .oParentObject.w_BOLIVE=.w_BOLIVE
      .oParentObject.w_PERIVE=.w_PERIVE
      .oParentObject.w_DTOBSO=.w_DTOBSO
      .oParentObject.w_NUMDIC=.w_NUMDIC
      .oParentObject.w_SERDOC=.w_SERDOC
      .oParentObject.w_DICSN=.w_DICSN
      .oParentObject.w_DICINTE_INDIRETTA=.w_DICINTE_INDIRETTA
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- gsve_kdi
    this.oparentobject.Notifyevent('CambiaDicinte')
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_OLDRIF = THIS.OPARENTOBJECT.w_MVRIFDIC
        .DoRTCalc(4,4,.t.)
            .w_MVRIFDIC = .w_Zoom.getVar('DISERIAL')
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
            .w_MVCODIVE = .w_Zoom.getVar('DICODIVA')
          .link_1_7('Full')
            .w_ANNDIC = .w_Zoom.getVar('DI__ANNO')
            .w_DATDIC = CP_TODATE(.w_Zoom.getVar('DIDATLET'))
            .w_TIPDIC = .w_Zoom.getVar('DITIPCON')
            .w_TIPIVA = .w_Zoom.getVar('DICODIVA')
            .w_OPEDIC = .w_Zoom.getVar('DITIPOPE')
            .w_UTIDIC = .w_Zoom.getVar('DIIMPUTI')
        .DoRTCalc(13,16,.t.)
            .w_NUMDIC = .w_Zoom.getVar('DINUMDOC')
            .w_SERDOC = .w_Zoom.getVar('DISERDOC')
            .w_DICSN = IIF(EMPTY(.w_MVRIFDIC), ' ', 'S')
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate('',RGB(0,255,0),RGB(0,255,0))
            .w_DICINTE_INDIRETTA = Space(15)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate('',RGB(0,255,0),RGB(0,255,0))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsve_kdi
    If cEvent='w_zoom selected'
       this.ecpsave()
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVCODIVE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODIVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODIVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_MVCODIVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_MVCODIVE)
            select IVCODIVA,IVDESIVA,IVBOLIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODIVE = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESAPP = NVL(_Link_.IVDESIVA,space(35))
      this.w_BOLIVE = NVL(_Link_.IVBOLIVA,space(1))
      this.w_PERIVE = NVL(_Link_.IVPERIVA,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODIVE = space(5)
      endif
      this.w_DESAPP = space(35)
      this.w_BOLIVE = space(1)
      this.w_PERIVE = 0
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODIVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsve_kdiPag1 as StdContainer
  Width  = 683
  height = 342
  stdWidth  = 683
  stdheight = 342
  resizeXpos=338
  resizeYpos=150
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOM as cp_zoombox with uid="TTIEDPMYHQ",left=4, top=-1, width=673,height=291,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DIC_INTE",cZoomFile="GSVE_KFA",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,cZoomOnZoom="",cMenuFile="",bQueryOnLoad=.t.,bRetriveAllRows=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 95540454


  add object oObj_1_22 as cp_calclbl with uid="AQDKGFSOJL",left=19, top=293, width=41,height=20,;
    caption='',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 82457098


  add object oBtn_1_23 as StdButton with uid="KYQESPJULE",left=627, top=293, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75139770;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_21 as StdString with uid="SNNQHBWIMT",Visible=.t., Left=66, Top=293,;
    Alignment=0, Width=421, Height=17,;
    Caption="Lettera di intento associata alla sede"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kdi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
