* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bno                                                        *
*              Menu contestuale nominativi                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-26                                                      *
* Last revis.: 2014-10-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFUNZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bno",oParentObject,m.pFUNZ)
return(i_retval)

define class tgszm_bno as StdBatch
  * --- Local variables
  pFUNZ = space(1)
  w_NOCODICE = space(15)
  w_NOTIFICA = space(1)
  w_OBJ = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per avvio funzionalit� legate ai Nominativi, da men� contestuale (tasto destro)
    do case
      case this.pFUNZ="T" OR this.pFUNZ="F"
        * --- Assegno alla variabile il valore relativo alla chiave primaria 1 (NOCODICE) dei Nominativi
        this.w_NOCODICE = g_oMenu.getbyindexkeyvalue(1)
        this.w_OBJ = g_oMenu.oParentObject
        * --- Creazione nominativo
        if this.pFUNZ="F"
          GSAR_BO1(this, this.w_NOCODICE,,, "F")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          GSAR_BO1(this, this.w_NOCODICE )
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_NOTIFICA<>"N" AND upper(this.w_OBJ.cprg) = "GSAR_ANO"
          * --- Se il men� contestuale viene lanciato dall'anagrafica Nominativi
          * --- Vengono aggiornati i campi del Nominativo
          this.w_OBJ.LoadRec()     
        endif
      case this.pFUNZ="C"
        this.w_NOCODICE = this.oParentObject.w_ATCODNOM
        * --- Creazione nominativo
        GSAR_BO1(this, this.w_NOCODICE )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pFUNZ)
    this.pFUNZ=pFUNZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFUNZ"
endproc
