* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_avf                                                        *
*              MOD.F24 ESTREMI VERSAMENTO                                      *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_104]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-14                                                      *
* Last revis.: 2011-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_avf")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_avf")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_avf")
  return

* --- Class definition
define class tgscg_avf as StdPCForm
  Width  = 772
  Height = 249
  Top    = 41
  Left   = 59
  cComment = "MOD.F24 ESTREMI VERSAMENTO"
  cPrg = "gscg_avf"
  HelpContextID=59385193
  add object cnt as tcgscg_avf
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_avf as PCContext
  w_VFDTPRES = space(8)
  w_AZIENDA = space(5)
  w_AZCONBAN = space(15)
  w_CODABITE = space(5)
  w_CODCABTE = space(5)
  w_OBTEST = space(8)
  w_AZBANPRO = space(15)
  w_VFBANPAS = space(15)
  w_BATIPCON = space(1)
  w_DATOBSO1 = space(8)
  w_VFBANPRO = space(15)
  w_BATIPCON_B = space(15)
  w_DATOBSO2 = space(8)
  w_VFSERIAL = space(10)
  w_TIPOMOD = space(3)
  w_RESCHK = 0
  w_VFCODTES = space(5)
  w_VFCABTES = space(5)
  w_VFNUMASS = space(12)
  w_VFBANCIC = space(1)
  w_VFCODABI = space(5)
  w_VFCODCAB = space(5)
  w_ABITES = space(5)
  w_CABTES = space(5)
  w_CONCOR = space(12)
  w_BADESCRI = space(35)
  w_BADESCRI_B = space(35)
  w_BANPROABI = space(5)
  w_BANPROCAB = space(5)
  w_BANPROSIA = space(5)
  w_BACONSBF = space(1)
  w_BACONSBF_1 = space(1)
  proc Save(oFrom)
    this.w_VFDTPRES = oFrom.w_VFDTPRES
    this.w_AZIENDA = oFrom.w_AZIENDA
    this.w_AZCONBAN = oFrom.w_AZCONBAN
    this.w_CODABITE = oFrom.w_CODABITE
    this.w_CODCABTE = oFrom.w_CODCABTE
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_AZBANPRO = oFrom.w_AZBANPRO
    this.w_VFBANPAS = oFrom.w_VFBANPAS
    this.w_BATIPCON = oFrom.w_BATIPCON
    this.w_DATOBSO1 = oFrom.w_DATOBSO1
    this.w_VFBANPRO = oFrom.w_VFBANPRO
    this.w_BATIPCON_B = oFrom.w_BATIPCON_B
    this.w_DATOBSO2 = oFrom.w_DATOBSO2
    this.w_VFSERIAL = oFrom.w_VFSERIAL
    this.w_TIPOMOD = oFrom.w_TIPOMOD
    this.w_RESCHK = oFrom.w_RESCHK
    this.w_VFCODTES = oFrom.w_VFCODTES
    this.w_VFCABTES = oFrom.w_VFCABTES
    this.w_VFNUMASS = oFrom.w_VFNUMASS
    this.w_VFBANCIC = oFrom.w_VFBANCIC
    this.w_VFCODABI = oFrom.w_VFCODABI
    this.w_VFCODCAB = oFrom.w_VFCODCAB
    this.w_ABITES = oFrom.w_ABITES
    this.w_CABTES = oFrom.w_CABTES
    this.w_CONCOR = oFrom.w_CONCOR
    this.w_BADESCRI = oFrom.w_BADESCRI
    this.w_BADESCRI_B = oFrom.w_BADESCRI_B
    this.w_BANPROABI = oFrom.w_BANPROABI
    this.w_BANPROCAB = oFrom.w_BANPROCAB
    this.w_BANPROSIA = oFrom.w_BANPROSIA
    this.w_BACONSBF = oFrom.w_BACONSBF
    this.w_BACONSBF_1 = oFrom.w_BACONSBF_1
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_VFDTPRES = this.w_VFDTPRES
    oTo.w_AZIENDA = this.w_AZIENDA
    oTo.w_AZCONBAN = this.w_AZCONBAN
    oTo.w_CODABITE = this.w_CODABITE
    oTo.w_CODCABTE = this.w_CODCABTE
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_AZBANPRO = this.w_AZBANPRO
    oTo.w_VFBANPAS = this.w_VFBANPAS
    oTo.w_BATIPCON = this.w_BATIPCON
    oTo.w_DATOBSO1 = this.w_DATOBSO1
    oTo.w_VFBANPRO = this.w_VFBANPRO
    oTo.w_BATIPCON_B = this.w_BATIPCON_B
    oTo.w_DATOBSO2 = this.w_DATOBSO2
    oTo.w_VFSERIAL = this.w_VFSERIAL
    oTo.w_TIPOMOD = this.w_TIPOMOD
    oTo.w_RESCHK = this.w_RESCHK
    oTo.w_VFCODTES = this.w_VFCODTES
    oTo.w_VFCABTES = this.w_VFCABTES
    oTo.w_VFNUMASS = this.w_VFNUMASS
    oTo.w_VFBANCIC = this.w_VFBANCIC
    oTo.w_VFCODABI = this.w_VFCODABI
    oTo.w_VFCODCAB = this.w_VFCODCAB
    oTo.w_ABITES = this.w_ABITES
    oTo.w_CABTES = this.w_CABTES
    oTo.w_CONCOR = this.w_CONCOR
    oTo.w_BADESCRI = this.w_BADESCRI
    oTo.w_BADESCRI_B = this.w_BADESCRI_B
    oTo.w_BANPROABI = this.w_BANPROABI
    oTo.w_BANPROCAB = this.w_BANPROCAB
    oTo.w_BANPROSIA = this.w_BANPROSIA
    oTo.w_BACONSBF = this.w_BACONSBF
    oTo.w_BACONSBF_1 = this.w_BACONSBF_1
    PCContext::Load(oTo)
enddefine

define class tcgscg_avf as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 772
  Height = 249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-09"
  HelpContextID=59385193
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  MODVPAG_IDX = 0
  COD_ABI_IDX = 0
  COD_CAB_IDX = 0
  AZIENDA_IDX = 0
  COC_MAST_IDX = 0
  BAN_CHE_IDX = 0
  DAT_IVAN_IDX = 0
  cFile = "MODVPAG"
  cKeySelect = "VFSERIAL"
  cKeyWhere  = "VFSERIAL=this.w_VFSERIAL"
  cKeyWhereODBC = '"VFSERIAL="+cp_ToStrODBC(this.w_VFSERIAL)';

  cKeyWhereODBCqualified = '"MODVPAG.VFSERIAL="+cp_ToStrODBC(this.w_VFSERIAL)';

  cPrg = "gscg_avf"
  cComment = "MOD.F24 ESTREMI VERSAMENTO"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_VFDTPRES = ctod('  /  /  ')
  w_AZIENDA = space(5)
  w_AZCONBAN = space(15)
  w_CODABITE = space(5)
  w_CODCABTE = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_AZBANPRO = space(15)
  w_VFBANPAS = space(15)
  w_BATIPCON = space(1)
  w_DATOBSO1 = ctod('  /  /  ')
  w_VFBANPRO = space(15)
  w_BATIPCON_B = space(15)
  w_DATOBSO2 = ctod('  /  /  ')
  w_VFSERIAL = space(10)
  w_TIPOMOD = space(3)
  w_RESCHK = 0
  w_VFCODTES = space(5)
  o_VFCODTES = space(5)
  w_VFCABTES = space(5)
  w_VFNUMASS = space(12)
  w_VFBANCIC = space(1)
  w_VFCODABI = space(5)
  o_VFCODABI = space(5)
  w_VFCODCAB = space(5)
  w_ABITES = space(5)
  w_CABTES = space(5)
  w_CONCOR = space(12)
  w_BADESCRI = space(35)
  w_BADESCRI_B = space(35)
  w_BANPROABI = space(5)
  w_BANPROCAB = space(5)
  w_BANPROSIA = space(5)
  w_BACONSBF = space(1)
  w_BACONSBF_1 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_avfPag1","gscg_avf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 937738
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVFDTPRES_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='COD_ABI'
    this.cWorkTables[2]='COD_CAB'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='COC_MAST'
    this.cWorkTables[5]='BAN_CHE'
    this.cWorkTables[6]='DAT_IVAN'
    this.cWorkTables[7]='MODVPAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MODVPAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MODVPAG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_avf'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_11_joined
    link_1_11_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MODVPAG where VFSERIAL=KeySet.VFSERIAL
    *
    i_nConn = i_TableProp[this.MODVPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODVPAG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MODVPAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MODVPAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MODVPAG '
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'VFSERIAL',this.w_VFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AZIENDA = i_CODAZI
        .w_AZCONBAN = space(15)
        .w_CODABITE = space(5)
        .w_CODCABTE = space(5)
        .w_OBTEST = i_datsys
        .w_AZBANPRO = space(15)
        .w_BATIPCON = space(1)
        .w_DATOBSO1 = ctod("  /  /  ")
        .w_BATIPCON_B = space(15)
        .w_DATOBSO2 = ctod("  /  /  ")
        .w_TIPOMOD = this.oParentObject.w_TIPOMOD
        .w_RESCHK = 0
        .w_ABITES = space(5)
        .w_CABTES = space(5)
        .w_CONCOR = space(12)
        .w_BADESCRI = space(35)
        .w_BADESCRI_B = space(35)
        .w_BANPROABI = space(5)
        .w_BANPROCAB = space(5)
        .w_BANPROSIA = space(5)
        .w_BACONSBF = space(1)
        .w_BACONSBF_1 = space(1)
        .w_VFDTPRES = NVL(cp_ToDate(VFDTPRES),ctod("  /  /  "))
          .link_1_2('Load')
          .link_1_3('Load')
        .w_VFBANPAS = NVL(VFBANPAS,space(15))
          if link_1_8_joined
            this.w_VFBANPAS = NVL(BACODBAN108,NVL(this.w_VFBANPAS,space(15)))
            this.w_ABITES = NVL(BACODABI108,space(5))
            this.w_CABTES = NVL(BACODCAB108,space(5))
            this.w_CONCOR = NVL(BACONCOR108,space(12))
            this.w_BATIPCON = NVL(BATIPCON108,space(1))
            this.w_BADESCRI = NVL(BADESCRI108,space(35))
            this.w_DATOBSO1 = NVL(cp_ToDate(BADTOBSO108),ctod("  /  /  "))
            this.w_BACONSBF = NVL(BACONSBF108,space(1))
          else
          .link_1_8('Load')
          endif
        .w_VFBANPRO = NVL(VFBANPRO,space(15))
          if link_1_11_joined
            this.w_VFBANPRO = NVL(BACODBAN111,NVL(this.w_VFBANPRO,space(15)))
            this.w_BANPROABI = NVL(BACODABI111,space(5))
            this.w_BANPROCAB = NVL(BACODCAB111,space(5))
            this.w_BANPROSIA = NVL(BACODSIA111,space(5))
            this.w_BATIPCON_B = NVL(BATIPCON111,space(15))
            this.w_BADESCRI_B = NVL(BADESCRI111,space(35))
            this.w_DATOBSO2 = NVL(cp_ToDate(BADTOBSO111),ctod("  /  /  "))
            this.w_BACONSBF_1 = NVL(BACONSBF111,space(1))
          else
          .link_1_11('Load')
          endif
        .w_VFSERIAL = NVL(VFSERIAL,space(10))
        .w_VFCODTES = NVL(VFCODTES,space(5))
          * evitabile
          *.link_1_17('Load')
        .w_VFCABTES = NVL(VFCABTES,space(5))
          * evitabile
          *.link_1_18('Load')
        .w_VFNUMASS = NVL(VFNUMASS,space(12))
        .w_VFBANCIC = NVL(VFBANCIC,space(1))
        .w_VFCODABI = NVL(VFCODABI,space(5))
          * evitabile
          *.link_1_21('Load')
        .w_VFCODCAB = NVL(VFCODCAB,space(5))
          * evitabile
          *.link_1_22('Load')
        cp_LoadRecExtFlds(this,'MODVPAG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_VFDTPRES = ctod("  /  /  ")
      .w_AZIENDA = space(5)
      .w_AZCONBAN = space(15)
      .w_CODABITE = space(5)
      .w_CODCABTE = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_AZBANPRO = space(15)
      .w_VFBANPAS = space(15)
      .w_BATIPCON = space(1)
      .w_DATOBSO1 = ctod("  /  /  ")
      .w_VFBANPRO = space(15)
      .w_BATIPCON_B = space(15)
      .w_DATOBSO2 = ctod("  /  /  ")
      .w_VFSERIAL = space(10)
      .w_TIPOMOD = space(3)
      .w_RESCHK = 0
      .w_VFCODTES = space(5)
      .w_VFCABTES = space(5)
      .w_VFNUMASS = space(12)
      .w_VFBANCIC = space(1)
      .w_VFCODABI = space(5)
      .w_VFCODCAB = space(5)
      .w_ABITES = space(5)
      .w_CABTES = space(5)
      .w_CONCOR = space(12)
      .w_BADESCRI = space(35)
      .w_BADESCRI_B = space(35)
      .w_BANPROABI = space(5)
      .w_BANPROCAB = space(5)
      .w_BANPROSIA = space(5)
      .w_BACONSBF = space(1)
      .w_BACONSBF_1 = space(1)
      if .cFunction<>"Filter"
        .w_VFDTPRES = i_datsys
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_AZIENDA))
          .link_1_2('Full')
          endif
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_AZCONBAN))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,5,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(7,7,.f.)
        .w_VFBANPAS = .w_AZCONBAN
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_VFBANPAS))
          .link_1_8('Full')
          endif
          .DoRTCalc(9,10,.f.)
        .w_VFBANPRO = .w_AZBANPRO
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_VFBANPRO))
          .link_1_11('Full')
          endif
          .DoRTCalc(12,14,.f.)
        .w_TIPOMOD = this.oParentObject.w_TIPOMOD
          .DoRTCalc(16,16,.f.)
        .w_VFCODTES = .w_CODABITE
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_VFCODTES))
          .link_1_17('Full')
          endif
        .w_VFCABTES = .w_CODCABTE
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_VFCABTES))
          .link_1_18('Full')
          endif
          .DoRTCalc(19,19,.f.)
        .w_VFBANCIC = 'B'
        .DoRTCalc(21,21,.f.)
          if not(empty(.w_VFCODABI))
          .link_1_21('Full')
          endif
        .w_VFCODCAB = space(5)
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_VFCODCAB))
          .link_1_22('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'MODVPAG')
    this.DoRTCalc(23,32,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_avf
    * Forza aggiornamento del database
    this.bupdated=.t.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oVFDTPRES_1_1.enabled = i_bVal
      .Page1.oPag.oVFBANPAS_1_8.enabled = i_bVal
      .Page1.oPag.oVFBANPRO_1_11.enabled = i_bVal
      .Page1.oPag.oVFCODTES_1_17.enabled = i_bVal
      .Page1.oPag.oVFCABTES_1_18.enabled = i_bVal
      .Page1.oPag.oVFNUMASS_1_19.enabled = i_bVal
      .Page1.oPag.oVFBANCIC_1_20.enabled = i_bVal
      .Page1.oPag.oVFCODABI_1_21.enabled = i_bVal
      .Page1.oPag.oVFCODCAB_1_22.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oVFDTPRES_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MODVPAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MODVPAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFDTPRES,"VFDTPRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFBANPAS,"VFBANPAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFBANPRO,"VFBANPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFSERIAL,"VFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFCODTES,"VFCODTES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFCABTES,"VFCABTES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFNUMASS,"VFNUMASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFBANCIC,"VFBANCIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFCODABI,"VFCODABI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VFCODCAB,"VFCODCAB",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MODVPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODVPAG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MODVPAG_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MODVPAG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MODVPAG')
        i_extval=cp_InsertValODBCExtFlds(this,'MODVPAG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(VFDTPRES,VFBANPAS,VFBANPRO,VFSERIAL,VFCODTES"+;
                  ",VFCABTES,VFNUMASS,VFBANCIC,VFCODABI,VFCODCAB "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_VFDTPRES)+;
                  ","+cp_ToStrODBCNull(this.w_VFBANPAS)+;
                  ","+cp_ToStrODBCNull(this.w_VFBANPRO)+;
                  ","+cp_ToStrODBC(this.w_VFSERIAL)+;
                  ","+cp_ToStrODBCNull(this.w_VFCODTES)+;
                  ","+cp_ToStrODBCNull(this.w_VFCABTES)+;
                  ","+cp_ToStrODBC(this.w_VFNUMASS)+;
                  ","+cp_ToStrODBC(this.w_VFBANCIC)+;
                  ","+cp_ToStrODBCNull(this.w_VFCODABI)+;
                  ","+cp_ToStrODBCNull(this.w_VFCODCAB)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MODVPAG')
        i_extval=cp_InsertValVFPExtFlds(this,'MODVPAG')
        cp_CheckDeletedKey(i_cTable,0,'VFSERIAL',this.w_VFSERIAL)
        INSERT INTO (i_cTable);
              (VFDTPRES,VFBANPAS,VFBANPRO,VFSERIAL,VFCODTES,VFCABTES,VFNUMASS,VFBANCIC,VFCODABI,VFCODCAB  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_VFDTPRES;
                  ,this.w_VFBANPAS;
                  ,this.w_VFBANPRO;
                  ,this.w_VFSERIAL;
                  ,this.w_VFCODTES;
                  ,this.w_VFCABTES;
                  ,this.w_VFNUMASS;
                  ,this.w_VFBANCIC;
                  ,this.w_VFCODABI;
                  ,this.w_VFCODCAB;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MODVPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODVPAG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MODVPAG_IDX,i_nConn)
      *
      * update MODVPAG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MODVPAG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " VFDTPRES="+cp_ToStrODBC(this.w_VFDTPRES)+;
             ",VFBANPAS="+cp_ToStrODBCNull(this.w_VFBANPAS)+;
             ",VFBANPRO="+cp_ToStrODBCNull(this.w_VFBANPRO)+;
             ",VFCODTES="+cp_ToStrODBCNull(this.w_VFCODTES)+;
             ",VFCABTES="+cp_ToStrODBCNull(this.w_VFCABTES)+;
             ",VFNUMASS="+cp_ToStrODBC(this.w_VFNUMASS)+;
             ",VFBANCIC="+cp_ToStrODBC(this.w_VFBANCIC)+;
             ",VFCODABI="+cp_ToStrODBCNull(this.w_VFCODABI)+;
             ",VFCODCAB="+cp_ToStrODBCNull(this.w_VFCODCAB)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MODVPAG')
        i_cWhere = cp_PKFox(i_cTable  ,'VFSERIAL',this.w_VFSERIAL  )
        UPDATE (i_cTable) SET;
              VFDTPRES=this.w_VFDTPRES;
             ,VFBANPAS=this.w_VFBANPAS;
             ,VFBANPRO=this.w_VFBANPRO;
             ,VFCODTES=this.w_VFCODTES;
             ,VFCABTES=this.w_VFCABTES;
             ,VFNUMASS=this.w_VFNUMASS;
             ,VFBANCIC=this.w_VFBANCIC;
             ,VFCODABI=this.w_VFCODABI;
             ,VFCODCAB=this.w_VFCODCAB;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MODVPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODVPAG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MODVPAG_IDX,i_nConn)
      *
      * delete MODVPAG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'VFSERIAL',this.w_VFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MODVPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODVPAG_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
          .link_1_3('Full')
        .DoRTCalc(4,17,.t.)
        if .o_VFCODTES<>.w_VFCODTES
            .w_VFCABTES = .w_CODCABTE
          .link_1_18('Full')
        endif
        .DoRTCalc(19,21,.t.)
        if .o_VFCODABI<>.w_VFCODABI
          .link_1_22('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oVFBANCIC_1_20.visible=!this.oPgFrm.Page1.oPag.oVFBANCIC_1_20.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCONBAN,AZBANPRO";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZCONBAN,AZBANPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZCONBAN = NVL(_Link_.AZCONBAN,space(15))
      this.w_AZBANPRO = NVL(_Link_.AZBANPRO,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_AZCONBAN = space(15)
      this.w_AZBANPRO = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZCONBAN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCONBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCONBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_AZCONBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_AZCONBAN)
            select BACODBAN,BACODABI,BACODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCONBAN = NVL(_Link_.BACODBAN,space(15))
      this.w_CODABITE = NVL(_Link_.BACODABI,space(5))
      this.w_CODCABTE = NVL(_Link_.BACODCAB,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_AZCONBAN = space(15)
      endif
      this.w_CODABITE = space(5)
      this.w_CODCABTE = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCONBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VFBANPAS
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VFBANPAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_VFBANPAS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BACONCOR,BATIPCON,BADESCRI,BADTOBSO,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_VFBANPAS))
          select BACODBAN,BACODABI,BACODCAB,BACONCOR,BATIPCON,BADESCRI,BADTOBSO,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VFBANPAS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VFBANPAS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oVFBANPAS_1_8'),i_cWhere,'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BACONCOR,BATIPCON,BADESCRI,BADTOBSO,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BACODABI,BACODCAB,BACONCOR,BATIPCON,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VFBANPAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BACONCOR,BATIPCON,BADESCRI,BADTOBSO,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_VFBANPAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_VFBANPAS)
            select BACODBAN,BACODABI,BACODCAB,BACONCOR,BATIPCON,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VFBANPAS = NVL(_Link_.BACODBAN,space(15))
      this.w_ABITES = NVL(_Link_.BACODABI,space(5))
      this.w_CABTES = NVL(_Link_.BACODCAB,space(5))
      this.w_CONCOR = NVL(_Link_.BACONCOR,space(12))
      this.w_BATIPCON = NVL(_Link_.BATIPCON,space(1))
      this.w_BADESCRI = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO1 = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_BACONSBF = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_VFBANPAS = space(15)
      endif
      this.w_ABITES = space(5)
      this.w_CABTES = space(5)
      this.w_CONCOR = space(12)
      this.w_BATIPCON = space(1)
      this.w_BADESCRI = space(35)
      this.w_DATOBSO1 = ctod("  /  /  ")
      this.w_BACONSBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_BATIPCON<>'S' AND (EMPTY(.w_DATOBSO1) OR .w_DATOBSO1>.w_OBTEST) AND .w_BACONSBF='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
        endif
        this.w_VFBANPAS = space(15)
        this.w_ABITES = space(5)
        this.w_CABTES = space(5)
        this.w_CONCOR = space(12)
        this.w_BATIPCON = space(1)
        this.w_BADESCRI = space(35)
        this.w_DATOBSO1 = ctod("  /  /  ")
        this.w_BACONSBF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VFBANPAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.BACODBAN as BACODBAN108"+ ",link_1_8.BACODABI as BACODABI108"+ ",link_1_8.BACODCAB as BACODCAB108"+ ",link_1_8.BACONCOR as BACONCOR108"+ ",link_1_8.BATIPCON as BATIPCON108"+ ",link_1_8.BADESCRI as BADESCRI108"+ ",link_1_8.BADTOBSO as BADTOBSO108"+ ",link_1_8.BACONSBF as BACONSBF108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on MODVPAG.VFBANPAS=link_1_8.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and MODVPAG.VFBANPAS=link_1_8.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VFBANPRO
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VFBANPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_VFBANPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_VFBANPRO))
          select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VFBANPRO)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VFBANPRO) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oVFBANPRO_1_11'),i_cWhere,'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VFBANPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_VFBANPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_VFBANPRO)
            select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VFBANPRO = NVL(_Link_.BACODBAN,space(15))
      this.w_BANPROABI = NVL(_Link_.BACODABI,space(5))
      this.w_BANPROCAB = NVL(_Link_.BACODCAB,space(5))
      this.w_BANPROSIA = NVL(_Link_.BACODSIA,space(5))
      this.w_BATIPCON_B = NVL(_Link_.BATIPCON,space(15))
      this.w_BADESCRI_B = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO2 = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_BACONSBF_1 = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_VFBANPRO = space(15)
      endif
      this.w_BANPROABI = space(5)
      this.w_BANPROCAB = space(5)
      this.w_BANPROSIA = space(5)
      this.w_BATIPCON_B = space(15)
      this.w_BADESCRI_B = space(35)
      this.w_DATOBSO2 = ctod("  /  /  ")
      this.w_BACONSBF_1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_BATIPCON_B<>'S' AND (EMPTY(.w_DATOBSO2) OR .w_DATOBSO2>.w_OBTEST) AND .w_BACONSBF_1='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
        endif
        this.w_VFBANPRO = space(15)
        this.w_BANPROABI = space(5)
        this.w_BANPROCAB = space(5)
        this.w_BANPROSIA = space(5)
        this.w_BATIPCON_B = space(15)
        this.w_BADESCRI_B = space(35)
        this.w_DATOBSO2 = ctod("  /  /  ")
        this.w_BACONSBF_1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VFBANPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.BACODBAN as BACODBAN111"+ ",link_1_11.BACODABI as BACODABI111"+ ",link_1_11.BACODCAB as BACODCAB111"+ ",link_1_11.BACODSIA as BACODSIA111"+ ",link_1_11.BATIPCON as BATIPCON111"+ ",link_1_11.BADESCRI as BADESCRI111"+ ",link_1_11.BADTOBSO as BADTOBSO111"+ ",link_1_11.BACONSBF as BACONSBF111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on MODVPAG.VFBANPRO=link_1_11.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and MODVPAG.VFBANPRO=link_1_11.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VFCODTES
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_ABI_IDX,3]
    i_lTable = "COD_ABI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2], .t., this.COD_ABI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VFCODTES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABI',True,'COD_ABI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ABCODABI like "+cp_ToStrODBC(trim(this.w_VFCODTES)+"%");

          i_ret=cp_SQL(i_nConn,"select ABCODABI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ABCODABI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ABCODABI',trim(this.w_VFCODTES))
          select ABCODABI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ABCODABI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VFCODTES)==trim(_Link_.ABCODABI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VFCODTES) and !this.bDontReportError
            deferred_cp_zoom('COD_ABI','*','ABCODABI',cp_AbsName(oSource.parent,'oVFCODTES_1_17'),i_cWhere,'GSAR_ABI',"Codici ABI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI";
                     +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',oSource.xKey(1))
            select ABCODABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VFCODTES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI";
                   +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(this.w_VFCODTES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',this.w_VFCODTES)
            select ABCODABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VFCODTES = NVL(_Link_.ABCODABI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VFCODTES = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])+'\'+cp_ToStr(_Link_.ABCODABI,1)
      cp_ShowWarn(i_cKey,this.COD_ABI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VFCODTES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VFCABTES
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_lTable = "COD_CAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2], .t., this.COD_CAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VFCABTES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFI',True,'COD_CAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FICODCAB like "+cp_ToStrODBC(trim(this.w_VFCABTES)+"%");
                   +" and FICODABI="+cp_ToStrODBC(this.w_VFCODTES);

          i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FICODABI,FICODCAB","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FICODABI',this.w_VFCODTES;
                     ,'FICODCAB',trim(this.w_VFCABTES))
          select FICODABI,FICODCAB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FICODABI,FICODCAB into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VFCABTES)==trim(_Link_.FICODCAB) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VFCABTES) and !this.bDontReportError
            deferred_cp_zoom('COD_CAB','*','FICODABI,FICODCAB',cp_AbsName(oSource.parent,'oVFCABTES_1_18'),i_cWhere,'GSAR_AFI',"Codici CAB",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_VFCODTES<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FICODABI,FICODCAB;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB";
                     +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FICODABI="+cp_ToStrODBC(this.w_VFCODTES);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',oSource.xKey(1);
                       ,'FICODCAB',oSource.xKey(2))
            select FICODABI,FICODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VFCABTES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB";
                   +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(this.w_VFCABTES);
                   +" and FICODABI="+cp_ToStrODBC(this.w_VFCODTES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',this.w_VFCODTES;
                       ,'FICODCAB',this.w_VFCABTES)
            select FICODABI,FICODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VFCABTES = NVL(_Link_.FICODCAB,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VFCABTES = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])+'\'+cp_ToStr(_Link_.FICODABI,1)+'\'+cp_ToStr(_Link_.FICODCAB,1)
      cp_ShowWarn(i_cKey,this.COD_CAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VFCABTES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VFCODABI
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_ABI_IDX,3]
    i_lTable = "COD_ABI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2], .t., this.COD_ABI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VFCODABI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABI',True,'COD_ABI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ABCODABI like "+cp_ToStrODBC(trim(this.w_VFCODABI)+"%");

          i_ret=cp_SQL(i_nConn,"select ABCODABI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ABCODABI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ABCODABI',trim(this.w_VFCODABI))
          select ABCODABI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ABCODABI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VFCODABI)==trim(_Link_.ABCODABI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VFCODABI) and !this.bDontReportError
            deferred_cp_zoom('COD_ABI','*','ABCODABI',cp_AbsName(oSource.parent,'oVFCODABI_1_21'),i_cWhere,'GSAR_ABI',"Codici ABI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI";
                     +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',oSource.xKey(1))
            select ABCODABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VFCODABI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI";
                   +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(this.w_VFCODABI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',this.w_VFCODABI)
            select ABCODABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VFCODABI = NVL(_Link_.ABCODABI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VFCODABI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])+'\'+cp_ToStr(_Link_.ABCODABI,1)
      cp_ShowWarn(i_cKey,this.COD_ABI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VFCODABI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VFCODCAB
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_lTable = "COD_CAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2], .t., this.COD_CAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VFCODCAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'COD_CAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FICODCAB like "+cp_ToStrODBC(trim(this.w_VFCODCAB)+"%");
                   +" and FICODABI="+cp_ToStrODBC(this.w_VFCODABI);

          i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FICODABI,FICODCAB","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FICODABI',this.w_VFCODABI;
                     ,'FICODCAB',trim(this.w_VFCODCAB))
          select FICODABI,FICODCAB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FICODABI,FICODCAB into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VFCODCAB)==trim(_Link_.FICODCAB) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VFCODCAB) and !this.bDontReportError
            deferred_cp_zoom('COD_CAB','*','FICODABI,FICODCAB',cp_AbsName(oSource.parent,'oVFCODCAB_1_22'),i_cWhere,'',"Codici CAB",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_VFCODABI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FICODABI,FICODCAB;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB";
                     +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FICODABI="+cp_ToStrODBC(this.w_VFCODABI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',oSource.xKey(1);
                       ,'FICODCAB',oSource.xKey(2))
            select FICODABI,FICODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VFCODCAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB";
                   +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(this.w_VFCODCAB);
                   +" and FICODABI="+cp_ToStrODBC(this.w_VFCODABI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',this.w_VFCODABI;
                       ,'FICODCAB',this.w_VFCODCAB)
            select FICODABI,FICODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VFCODCAB = NVL(_Link_.FICODCAB,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VFCODCAB = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])+'\'+cp_ToStr(_Link_.FICODABI,1)+'\'+cp_ToStr(_Link_.FICODCAB,1)
      cp_ShowWarn(i_cKey,this.COD_CAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VFCODCAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVFDTPRES_1_1.value==this.w_VFDTPRES)
      this.oPgFrm.Page1.oPag.oVFDTPRES_1_1.value=this.w_VFDTPRES
    endif
    if not(this.oPgFrm.Page1.oPag.oVFBANPAS_1_8.value==this.w_VFBANPAS)
      this.oPgFrm.Page1.oPag.oVFBANPAS_1_8.value=this.w_VFBANPAS
    endif
    if not(this.oPgFrm.Page1.oPag.oVFBANPRO_1_11.value==this.w_VFBANPRO)
      this.oPgFrm.Page1.oPag.oVFBANPRO_1_11.value=this.w_VFBANPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oVFCODTES_1_17.value==this.w_VFCODTES)
      this.oPgFrm.Page1.oPag.oVFCODTES_1_17.value=this.w_VFCODTES
    endif
    if not(this.oPgFrm.Page1.oPag.oVFCABTES_1_18.value==this.w_VFCABTES)
      this.oPgFrm.Page1.oPag.oVFCABTES_1_18.value=this.w_VFCABTES
    endif
    if not(this.oPgFrm.Page1.oPag.oVFNUMASS_1_19.value==this.w_VFNUMASS)
      this.oPgFrm.Page1.oPag.oVFNUMASS_1_19.value=this.w_VFNUMASS
    endif
    if not(this.oPgFrm.Page1.oPag.oVFBANCIC_1_20.RadioValue()==this.w_VFBANCIC)
      this.oPgFrm.Page1.oPag.oVFBANCIC_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVFCODABI_1_21.value==this.w_VFCODABI)
      this.oPgFrm.Page1.oPag.oVFCODABI_1_21.value=this.w_VFCODABI
    endif
    if not(this.oPgFrm.Page1.oPag.oVFCODCAB_1_22.value==this.w_VFCODCAB)
      this.oPgFrm.Page1.oPag.oVFCODCAB_1_22.value=this.w_VFCODCAB
    endif
    if not(this.oPgFrm.Page1.oPag.oABITES_1_38.value==this.w_ABITES)
      this.oPgFrm.Page1.oPag.oABITES_1_38.value=this.w_ABITES
    endif
    if not(this.oPgFrm.Page1.oPag.oCABTES_1_39.value==this.w_CABTES)
      this.oPgFrm.Page1.oPag.oCABTES_1_39.value=this.w_CABTES
    endif
    if not(this.oPgFrm.Page1.oPag.oCONCOR_1_40.value==this.w_CONCOR)
      this.oPgFrm.Page1.oPag.oCONCOR_1_40.value=this.w_CONCOR
    endif
    if not(this.oPgFrm.Page1.oPag.oBADESCRI_1_42.value==this.w_BADESCRI)
      this.oPgFrm.Page1.oPag.oBADESCRI_1_42.value=this.w_BADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oBADESCRI_B_1_44.value==this.w_BADESCRI_B)
      this.oPgFrm.Page1.oPag.oBADESCRI_B_1_44.value=this.w_BADESCRI_B
    endif
    if not(this.oPgFrm.Page1.oPag.oBANPROABI_1_45.value==this.w_BANPROABI)
      this.oPgFrm.Page1.oPag.oBANPROABI_1_45.value=this.w_BANPROABI
    endif
    if not(this.oPgFrm.Page1.oPag.oBANPROCAB_1_46.value==this.w_BANPROCAB)
      this.oPgFrm.Page1.oPag.oBANPROCAB_1_46.value=this.w_BANPROCAB
    endif
    if not(this.oPgFrm.Page1.oPag.oBANPROSIA_1_47.value==this.w_BANPROSIA)
      this.oPgFrm.Page1.oPag.oBANPROSIA_1_47.value=this.w_BANPROSIA
    endif
    cp_SetControlsValueExtFlds(this,'MODVPAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_VFDTPRES)) or not((.w_VFDTPRES < CTOD('01-01-2002') and .w_TIPOMOD='OLD') or (.w_VFDTPRES >= CTOD('01-01-2002') and .w_VFDTPRES < CTOD('29-10-2007') and .w_TIPOMOD='NEW') or (.w_VFDTPRES >= CTOD('29-10-2007') and .w_TIPOMOD='007')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVFDTPRES_1_1.SetFocus()
            i_bnoObbl = !empty(.w_VFDTPRES)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di presentazione non valida, utilizzare il modello appropriato")
          case   not(.w_BATIPCON<>'S' AND (EMPTY(.w_DATOBSO1) OR .w_DATOBSO1>.w_OBTEST) AND .w_BACONSBF='C')  and not(empty(.w_VFBANPAS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVFBANPAS_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
          case   not(.w_BATIPCON_B<>'S' AND (EMPTY(.w_DATOBSO2) OR .w_DATOBSO2>.w_OBTEST) AND .w_BACONSBF_1='C')  and not(empty(.w_VFBANPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVFBANPRO_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_avf
      if i_bRes
        .w_RESCHK=0
        .NotifyEvent('ControllaDati')
        if .w_RESCHK<>0
          i_bRes=.f.
        endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VFCODTES = this.w_VFCODTES
    this.o_VFCODABI = this.w_VFCODABI
    return

enddefine

* --- Define pages as container
define class tgscg_avfPag1 as StdContainer
  Width  = 768
  height = 249
  stdWidth  = 768
  stdheight = 249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVFDTPRES_1_1 as StdField with uid="NGZZEQWYFB",rtseq=1,rtrep=.f.,;
    cFormVar = "w_VFDTPRES", cQueryName = "VFDTPRES",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di presentazione non valida, utilizzare il modello appropriato",;
    HelpContextID = 204577111,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=142, Top=32

  func oVFDTPRES_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_VFDTPRES < CTOD('01-01-2002') and .w_TIPOMOD='OLD') or (.w_VFDTPRES >= CTOD('01-01-2002') and .w_VFDTPRES < CTOD('29-10-2007') and .w_TIPOMOD='NEW') or (.w_VFDTPRES >= CTOD('29-10-2007') and .w_TIPOMOD='007'))
    endwith
    return bRes
  endfunc

  add object oVFBANPAS_1_8 as StdField with uid="AKQWUURKBM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_VFBANPAS", cQueryName = "VFBANPAS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice banca inesistente, obsoleta, di compensazione o salvo buon fine",;
    ToolTipText = "Banca passiva",;
    HelpContextID = 26953385,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=404, Top=32, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_VFBANPAS"

  func oVFBANPAS_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oVFBANPAS_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVFBANPAS_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oVFBANPAS_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oVFBANPAS_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_VFBANPAS
     i_obj.ecpSave()
  endproc

  add object oVFBANPRO_1_11 as StdField with uid="UXXNNMENOA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_VFBANPRO", cQueryName = "VFBANPRO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice banca inesistente, obsoleta, di compensazione o salvo buon fine",;
    ToolTipText = "Banca proponente",;
    HelpContextID = 26953381,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=404, Top=124, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_VFBANPRO"

  func oVFBANPRO_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oVFBANPRO_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVFBANPRO_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oVFBANPRO_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oVFBANPRO_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_VFBANPRO
     i_obj.ecpSave()
  endproc

  add object oVFCODTES_1_17 as StdField with uid="OONPYSEEHU",rtseq=17,rtrep=.f.,;
    cFormVar = "w_VFCODTES", cQueryName = "VFCODTES",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 183937367,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=69, Top=89, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_ABI", cZoomOnZoom="GSAR_ABI", oKey_1_1="ABCODABI", oKey_1_2="this.w_VFCODTES"

  func oVFCODTES_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
      if .not. empty(.w_VFCABTES)
        bRes2=.link_1_18('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oVFCODTES_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVFCODTES_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_ABI','*','ABCODABI',cp_AbsName(this.parent,'oVFCODTES_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABI',"Codici ABI",'',this.parent.oContained
  endproc
  proc oVFCODTES_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ABCODABI=this.parent.oContained.w_VFCODTES
     i_obj.ecpSave()
  endproc

  add object oVFCABTES_1_18 as StdField with uid="AHQTOEYEVL",rtseq=18,rtrep=.f.,;
    cFormVar = "w_VFCABTES", cQueryName = "VFCABTES",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 186952023,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=210, Top=89, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_CAB", cZoomOnZoom="GSAR_AFI", oKey_1_1="FICODABI", oKey_1_2="this.w_VFCODTES", oKey_2_1="FICODCAB", oKey_2_2="this.w_VFCABTES"

  func oVFCABTES_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oVFCABTES_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVFCABTES_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.COD_CAB_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FICODABI="+cp_ToStrODBC(this.Parent.oContained.w_VFCODTES)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FICODABI="+cp_ToStr(this.Parent.oContained.w_VFCODTES)
    endif
    do cp_zoom with 'COD_CAB','*','FICODABI,FICODCAB',cp_AbsName(this.parent,'oVFCABTES_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFI',"Codici CAB",'',this.parent.oContained
  endproc
  proc oVFCABTES_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.FICODABI=w_VFCODTES
     i_obj.w_FICODCAB=this.parent.oContained.w_VFCABTES
     i_obj.ecpSave()
  endproc

  add object oVFNUMASS_1_19 as StdField with uid="TOTVLACFEI",rtseq=19,rtrep=.f.,;
    cFormVar = "w_VFNUMASS", cQueryName = "VFNUMASS",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 44041897,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=26, Top=146, InputMask=replicate('X',12)


  add object oVFBANCIC_1_20 as StdCombo with uid="WCDBCYNSRZ",rtseq=20,rtrep=.f.,left=158,top=147,width=107,height=21;
    , ToolTipText = "Tipo assegno";
    , HelpContextID = 191150439;
    , cFormVar="w_VFBANCIC",RowSource=""+"Bancario,"+"Circolare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVFBANCIC_1_20.RadioValue()
    return(iif(this.value =1,'B',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oVFBANCIC_1_20.GetRadio()
    this.Parent.oContained.w_VFBANCIC = this.RadioValue()
    return .t.
  endfunc

  func oVFBANCIC_1_20.SetRadio()
    this.Parent.oContained.w_VFBANCIC=trim(this.Parent.oContained.w_VFBANCIC)
    this.value = ;
      iif(this.Parent.oContained.w_VFBANCIC=='B',1,;
      iif(this.Parent.oContained.w_VFBANCIC=='C',2,;
      0))
  endfunc

  func oVFBANCIC_1_20.mHide()
    with this.Parent.oContained
      return (empty(.w_VFNUMASS))
    endwith
  endfunc

  add object oVFCODABI_1_21 as StdField with uid="CCYQYLCNQN",rtseq=21,rtrep=.f.,;
    cFormVar = "w_VFCODABI", cQueryName = "VFCODABI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 34166431,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=68, Top=201, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_ABI", cZoomOnZoom="GSAR_ABI", oKey_1_1="ABCODABI", oKey_1_2="this.w_VFCODABI"

  func oVFCODABI_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
      if .not. empty(.w_VFCODCAB)
        bRes2=.link_1_22('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oVFCODABI_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVFCODABI_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_ABI','*','ABCODABI',cp_AbsName(this.parent,'oVFCODABI_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABI',"Codici ABI",'',this.parent.oContained
  endproc
  proc oVFCODABI_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ABCODABI=this.parent.oContained.w_VFCODABI
     i_obj.ecpSave()
  endproc

  add object oVFCODCAB_1_22 as StdField with uid="FNRHVSIMXF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_VFCODCAB", cQueryName = "VFCODCAB",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 67720856,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=208, Top=201, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_CAB", oKey_1_1="FICODABI", oKey_1_2="this.w_VFCODABI", oKey_2_1="FICODCAB", oKey_2_2="this.w_VFCODCAB"

  func oVFCODCAB_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oVFCODCAB_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVFCODCAB_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.COD_CAB_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FICODABI="+cp_ToStrODBC(this.Parent.oContained.w_VFCODABI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FICODABI="+cp_ToStr(this.Parent.oContained.w_VFCODABI)
    endif
    do cp_zoom with 'COD_CAB','*','FICODABI,FICODCAB',cp_AbsName(this.parent,'oVFCODCAB_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici CAB",'',this.parent.oContained
  endproc

  add object oABITES_1_38 as StdField with uid="DRVWLZEBEE",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ABITES", cQueryName = "ABITES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 69120262,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=404, Top=75, InputMask=replicate('X',5)

  add object oCABTES_1_39 as StdField with uid="FSKHHRSDGX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CABTES", cQueryName = "CABTES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 69091366,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=514, Top=75, InputMask=replicate('X',5)

  add object oCONCOR_1_40 as StdField with uid="HOFOWRYSPS",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CONCOR", cQueryName = "CONCOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Conto corrente",;
    HelpContextID = 61738534,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=658, Top=75, InputMask=replicate('X',12)

  add object oBADESCRI_1_42 as StdField with uid="UHOHRDJWEA",rtseq=26,rtrep=.f.,;
    cFormVar = "w_BADESCRI", cQueryName = "BADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 82796639,;
   bGlobalFont=.t.,;
    Height=21, Width=234, Left=525, Top=32, InputMask=replicate('X',35)

  add object oBADESCRI_B_1_44 as StdField with uid="TCDRXOQXXY",rtseq=27,rtrep=.f.,;
    cFormVar = "w_BADESCRI_B", cQueryName = "BADESCRI_B",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 82815055,;
   bGlobalFont=.t.,;
    Height=21, Width=234, Left=525, Top=124, InputMask=replicate('X',35)

  add object oBANPROABI_1_45 as StdField with uid="FQQQNXGIIW",rtseq=28,rtrep=.f.,;
    cFormVar = "w_BANPROABI", cQueryName = "BANPROABI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ABI banca proponente",;
    HelpContextID = 15402216,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=404, Top=160, InputMask=replicate('X',5)

  add object oBANPROCAB_1_46 as StdField with uid="UIMUAZCRVN",rtseq=29,rtrep=.f.,;
    cFormVar = "w_BANPROCAB", cQueryName = "BANPROCAB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice CAB banca proponente",;
    HelpContextID = 15402103,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=514, Top=160, InputMask=replicate('X',5)

  add object oBANPROSIA_1_47 as StdField with uid="DQZRJNNZIZ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_BANPROSIA", cQueryName = "BANPROSIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice SIA banca proponente",;
    HelpContextID = 15402095,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=658, Top=160, InputMask=replicate('X',5)

  add object oStr_1_23 as StdString with uid="UNYEMUMCQX",Visible=.t., Left=6, Top=35,;
    Alignment=1, Width=131, Height=15,;
    Caption="Data di presentazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="SSZUHLWGWI",Visible=.t., Left=9, Top=5,;
    Alignment=0, Width=206, Height=18,;
    Caption="Estremi del versamento"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="ITCLDEFYEC",Visible=.t., Left=11, Top=125,;
    Alignment=0, Width=245, Height=15,;
    Caption="Pagamento effettuato con assegno n.ro"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="MJOAICKJAT",Visible=.t., Left=11, Top=177,;
    Alignment=0, Width=135, Height=18,;
    Caption="Tratto\Emesso su"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="ITMUMHGNLX",Visible=.t., Left=5, Top=201,;
    Alignment=1, Width=60, Height=18,;
    Caption="Cod. ABI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="TYECXOINBR",Visible=.t., Left=161, Top=201,;
    Alignment=1, Width=47, Height=18,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="SGAPJACSCX",Visible=.t., Left=8, Top=61,;
    Alignment=0, Width=230, Height=18,;
    Caption="Codice banca\Poste\Concessionario"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="VNIBCLJDSC",Visible=.t., Left=7, Top=91,;
    Alignment=1, Width=58, Height=15,;
    Caption="Azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="QEJWEOKICK",Visible=.t., Left=129, Top=89,;
    Alignment=1, Width=79, Height=18,;
    Caption="CAB\Sportello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="UYUMEFUUSV",Visible=.t., Left=276, Top=35,;
    Alignment=1, Width=126, Height=18,;
    Caption="Banca passiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="JBWVWGTHRD",Visible=.t., Left=276, Top=78,;
    Alignment=1, Width=126, Height=15,;
    Caption="Codice ABI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="JWFHQOOTXU",Visible=.t., Left=458, Top=78,;
    Alignment=1, Width=50, Height=15,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="PKMEARPKQY",Visible=.t., Left=564, Top=78,;
    Alignment=1, Width=91, Height=18,;
    Caption="N. c. corrente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="NOZKRMEMIP",Visible=.t., Left=276, Top=126,;
    Alignment=1, Width=126, Height=18,;
    Caption="Banca proponente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="MNBNDRRUYS",Visible=.t., Left=276, Top=163,;
    Alignment=1, Width=126, Height=15,;
    Caption="Codice ABI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="DLBRTBFBSC",Visible=.t., Left=458, Top=163,;
    Alignment=1, Width=50, Height=15,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="NFKBXGDDCZ",Visible=.t., Left=571, Top=163,;
    Alignment=1, Width=84, Height=15,;
    Caption="Codice SIA:"  ;
  , bGlobalFont=.t.

  add object oBox_1_29 as StdBox with uid="PGIDJFGOMZ",left=3, top=21, width=762,height=0

  add object oBox_1_33 as StdBox with uid="KZKFYMRYQA",left=3, top=80, width=248,height=1

  add object oBox_1_34 as StdBox with uid="FNYPYQQKDN",left=268, top=22, width=3,height=220

  add object oBox_1_51 as StdBox with uid="HAYHJYUIOU",left=3, top=242, width=762,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_avf','MODVPAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".VFSERIAL=MODVPAG.VFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
