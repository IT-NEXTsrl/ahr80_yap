* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bzm                                                        *
*              Schede contabili da primanota                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-26                                                      *
* Last revis.: 2000-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bzm",oParentObject)
return(i_retval)

define class tgscg_bzm as StdBatch
  * --- Local variables
  w_PNOTA = .f.
  w_PTIPCON = space(1)
  w_PCODCON = space(15)
  w_PCODESE = space(4)
  w_PDATFIN = ctod("  /  /  ")
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia la maschera di visualizzazione schede contabili (da GSCG_MPN)
    * --- VARIABILI DALLA PRIMA NOTA
    * --- VARIABILI DA PASSARE ALLA MASCHERA
    this.w_PNOTA = .T.
    this.w_PTIPCON = this.oParentObject.w_PNTIPCON
    this.w_PCODCON = this.oParentObject.w_PNCODCON
    this.w_PCODESE = this.oParentObject.w_PNCODESE
    this.w_PDATFIN = G_FINESE
    * --- lancio la maschera
    do GSCG_SZM with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
