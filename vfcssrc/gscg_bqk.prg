* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bqk                                                        *
*              Check quad.IVA P.N. no transazione                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_124]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-11-19                                                      *
* Last revis.: 2000-06-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bqk",oParentObject)
return(i_retval)

define class tgscg_bqk as StdBatch
  * --- Local variables
  w_TOTMDR1 = 0
  w_DTOBSOV = ctod("  /  /  ")
  w_AbsRowMIV = 0
  w_nRelRowMIV = 0
  w_RECPOSMIV = 0
  w_AbsRowMDR = 0
  w_nRelRowMDR = 0
  w_RECPOSMDR = 0
  w_OK = .f.
  w_SEZDAR = 0
  w_TOTIVCO = 0
  w_TOTIMCO = 0
  w_DATRIF = ctod("  /  /  ")
  w_SEZAVE = 0
  w_IVADET = 0
  w_DIFIVA = 0
  w_TOTIVA = 0
  w_RIGIVA = 0
  w_RIGCNT = 0
  w_APPO = 0
  w_DATBLO = ctod("  /  /  ")
  w_TOTD = 0
  w_STALIG = ctod("  /  /  ")
  w_TOTA = 0
  w_DAVE = 0
  w_MESS = space(10)
  w_SBIL = 0
  w_CFUNC = space(10)
  w_TOTREG = 0
  w_AbsRow = 0
  w_RECPOS = 0
  w_nRelRow = 0
  w_TRFLGSTO = space(1)
  w_TOTRITE = 0
  w_TOTRITD = 0
  w_TOTRITA = 0
  w_TOTDAVER = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_NUMRIGA = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  VALUTE_idx=0
  DATIRITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EFFETTUA CONTROLLI Fuori Transazione (da GSCG_MPN) (A.M. CheckForm)
    this.w_OK = .T.
    this.w_CFUNC = this.oParentObject.cFunction
    this.w_RIGIVA = 0
    this.w_RIGCNT = 0
    this.w_TOTA = 0
    this.w_SEZDAR = 0
    this.w_DAVE = 0
    this.w_SBIL = 0
    this.w_SEZAVE = 0
    this.w_TOTD = 0
    this.w_oMess=createobject("Ah_Message")
    * --- Controllo data obsloescenza su campo VALUTA
    * --- Effettuo la lettura della data di obsolescenza dell'archivio VALUTE.
    *     Effettuo questa operazione perch� altrimenti il valore della data obsolescenza
    *     non viene letta.
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADTOBSO"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_PNCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADTOBSO;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_PNCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DTOBSOV = NVL(cp_ToDate(_read_.VADTOBSO),cp_NullValue(_read_.VADTOBSO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT EMPTY(this.w_DTOBSOV) AND this.w_DTOBSOV<=this.oParentObject.w_OBTEST
      this.w_oMess.AddMsgPartNL("Codice valuta obsoleta")     
      this.w_OK = .F.
    endif
    if this.w_OK
      * --- Controlli su Libro Giornale
      this.w_STALIG = cp_CharToDate("  -  -  ")
      this.w_DATBLO = cp_CharToDate("  -  -  ")
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZSTALIG,AZDATBLO"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZSTALIG,AZDATBLO;
          from (i_cTable) where;
              AZCODAZI = i_codazi;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
        this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do case
        case this.oParentObject.w_PNDATREG<=this.w_STALIG
          * --- data Registrazione Inferiore all' Ultima Data Reg. stampata su L.G.
          this.w_oMess.AddMsgPartNL("Data registrazione inferiore o uguale alla data di stampa del libro giornale")     
          this.w_OK = .F.
        case this.oParentObject.w_PNDATREG <= this.w_DATBLO AND NOT(EMPTY(NVL(this.w_DATBLO,cp_CharToDate("  -  -  "))))
          * --- mentre stampo il giornale non devo inserire registrazioni <= alla data di blocco
          this.w_oMess.AddMsgPartNL("Stampa libro giornale in corso con selezione comprendente la registrazione")     
          this.w_OK = .F.
      endcase
    endif
    * --- Cicla sulle Righe Contabili
    if this.w_OK
      * --- Assegno posizione per ritorno sul fondo dei controlli
      this.w_AbsRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
      this.w_nRelRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
      SELECT (this.oParentObject.cTrsName)
      this.w_RECPOS = IIF( Eof() , RECNO(this.oParentObject.cTrsName)-1 , RECNO(this.oParentObject.cTrsName) )
      GO TOP
      SCAN FOR NOT EMPTY(NVL(t_PNTIPCON," ")) AND NOT EMPTY(NVL(t_PNCODCON," "))
      this.w_TOTD = this.w_TOTD + NVL(t_PNIMPDAR, 0)
      this.w_TOTA = this.w_TOTA + NVL(t_PNIMPAVE, 0)
      if t_PNTIPCON=this.oParentObject.w_PNTIPCLF AND t_PNCODCON=this.oParentObject.w_PNCODCLF AND t_PNCAURIG=this.oParentObject.w_PNCODCAU
        * --- Riga Cli/For di Riferimento
        this.w_SEZDAR = this.w_SEZDAR + t_PNIMPDAR
        this.w_SEZAVE = this.w_SEZAVE + t_PNIMPAVE
      endif
      if (t_TIPSOT="V" OR t_TIPSOT="A") AND t_PNCAURIG=this.oParentObject.w_PNCODCAU
        * --- Riga Contropartita vendita/acquisti
        this.w_RIGCNT = this.w_RIGCNT + (t_PNIMPDAR-t_PNIMPAVE)
      endif
      if t_TIPSOT="I" AND t_PNCAURIG=this.oParentObject.w_PNCODCAU
        * --- Riga Conto IVA
        this.w_RIGIVA = this.w_RIGIVA + (t_PNIMPDAR-t_PNIMPAVE)
      endif
      this.w_DAVE = this.w_DAVE + (t_PNIMPDAR-t_PNIMPAVE)
      if i_SRV="U" AND NVL(t_PNIMPDAR=0,0) AND NVL(t_PNIMPAVE=0,0) AND NVL(t_PNFLZERO," ")<>"S"
        if this.w_CFUNC <> "Load"
          this.w_oMess.AddMsgPartNL("Impossibile azzerare importi di una riga in variazione, occorre eliminarla")     
          this.w_OK = .F.
        endif
      endif
      ENDSCAN
      this.oParentObject.w_TOTDAR = this.w_TOTD
      this.oParentObject.w_TOTAVE = this.w_TOTA
      this.w_RIGIVA = ABS(this.w_RIGIVA)
      this.w_RIGCNT = ABS(this.w_RIGCNT)
      this.w_SBIL = this.w_TOTD - this.w_TOTA
      * --- Controlli Differenza DARE/AVERE
      if this.w_OK AND cp_ROUND(this.w_SBIL,4) <> 0 
        * --- Salta se in Cancellazione
        if g_QUADRA="S"
          this.w_oMess.AddMsgPartNL("Manca quadratura dare/avere")     
          this.w_OK = .F.
        else
          this.w_OK = ah_YesNo("Manca quadratura dare/avere. Confermi ugualmente?")
          if Not this.w_OK
            this.w_oMess.AddMsgPartNL("Manca quadratura dare/avere")     
            this.oParentObject.w_RESCHK = -1
            i_retcode = 'stop'
            return
          endif
        endif
      endif
      * --- Controlli Differenza IVA - solo se editabile il dettaglio IVA
      if this.w_OK AND this.oParentObject.w_PNTIPREG<>"N" AND (this.oParentObject.w_PNTIPREG<>"E" or this.oParentObject.w_PNTIPDOC<>"CO")
        * --- Cicla sulle Righe IVA
        this.w_TOTIVA = 0
        this.w_IVADET = 0
        this.w_NUMRIGA = 0
        * --- Assegno posizione per ritorno sul fondo dei controlli
        With this.oParentObject.GSCG_MIV
        this.w_AbsRowMIV = .oPgFrm.Page1.oPag.oBody.nAbsRow
        this.w_nRelRowMIV = .oPgFrm.Page1.oPag.oBody.nRelRow
        SELECT (.cTrsName)
        this.w_RECPOSMIV = IIF( Eof() , RECNO(.cTrsName)-1 , RECNO(.cTrsName) )
        Go Top
        this.w_TOTIVCO = 0
        SCAN FOR NOT EMPTY(NVL(t_IVCODIVA," ")) AND (t_IVIMPONI<>0 OR t_IVIMPIVA<>0)
        if t_CTIPREG=this.oParentObject.w_PNTIPREG
          this.w_TOTIVA = this.w_TOTIVA + IIF(this.oParentObject.w_CALDOC $ "IZ" OR t_IVCFLOMA $ "IES", 0, t_IVIMPONI)
          this.w_TOTIVA = this.w_TOTIVA + IIF(this.oParentObject.w_CALDOC $ "MZ" OR t_IVCFLOMA$"ES", 0, t_IVIMPIVA)
          if  this.oParentObject.w_PNTIPREG="C" And this.oParentObject.w_PNTIPDOC="FC" 
            this.w_TOTIVCO = this.w_TOTIVCO + IIF(t_IVCFLOMA $ "IES", 0, t_IVIMPONI) + IIF(t_IVCFLOMA$"ES", 0, t_IVIMPIVA)
          endif
          if  this.oParentObject.w_PNTIPREG="C" And this.oParentObject.w_PNTIPDOC<>"FC" 
            this.w_TOTIVCO = this.w_TOTIVCO + IIF(t_IVCFLOMA $ "XEI", t_IVIMPIVA, 0)
            this.w_TOTIMCO = this.w_TOTIMCO + IIF(t_IVCFLOMA = "X", t_IVIMPONI, IIF(t_IVCFLOMA = "E", -t_IVIMPIVA, 0) ) 
          endif
        endif
        * --- Verifico corrispondenza calcolo dell'IVA in automatico
        this.w_NUMRIGA = this.w_NUMRIGA+1
        if t_IVTIPREG=3
          * --- Arrotonda l' IVA all'importo Superiore
          this.w_APPO = t_IVIMPONI - (t_IVIMPONI/(1+(t_PERIVA/100)))
          this.w_APPO = IVAROUND(this.w_APPO, this.oParentObject.w_DECTOP, IIF(this.w_APPO<0, 0, 1),this.oParentObject.w_PNVALNAZ)
        else
          this.w_APPO = (t_IVIMPONI * t_PERIVA) / 100
          this.w_APPO = IVAROUND(this.w_APPO, this.oParentObject.w_DECTOP, IIF(this.w_APPO<0, 0, 1),this.oParentObject.w_PNVALNAZ)
        endif
        if this.w_APPO<>t_IVIMPIVA AND t_IVTIPREG<>3
          this.w_MESS = Ah_MsgFormat("Esistono delle differenze tra l'IVA definita e quella calcolata in automatico:%0Differenza riga %1 codice IVA %2:%3",Alltrim(str(this.w_NUMRIGA)),Alltrim(t_IVCODIVA),Alltrim(Tran(this.w_APPO-t_IVIMPIVA,v_PV(40+VVP))))
        endif
        * --- Se no Riga Omaggio
        if t_CTIPREG="A"
          this.w_APPO = cp_ROUND((t_IVIMPIVA*t_IVPERIND)/100, this.oParentObject.w_DECTOP)
        else
          this.w_APPO = 0
        endif
        this.w_IVADET = this.w_IVADET +iif (t_IVCFLOMA="S" ,0, ((t_IVIMPIVA - this.w_APPO) * IIF(t_CTIPREG="A", 1, -1)) )
        ENDSCAN
        if NOT EMPTY(this.w_MESS)
          this.w_OK = ah_YesNo("%1.%0Confermi ugualmente?",,this.w_MESS)
          this.w_MESS = ""
        endif
        * --- Rimetto il dettaglio IVA nella posizione prima della scansione
         
 SELECT (.cTrsName) 
 GO this.w_RECPOSMIV 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow= this.w_AbsRowMIV 
 .oPgFrm.Page1.oPag.oBody.nRelRow= this.w_nRelRowMIV
        EndWith
        this.w_IVADET = ABS(this.w_IVADET)
        * --- Differenza IVA - eseguo il controllo solo se ho un importo in valuta e se non � una registrazione di tipo Corrispettivo
        if this.oParentObject.w_PNTOTDOC<>0 AND this.oParentObject.w_PNVALNAZ<>this.oParentObject.w_PNCODVAL And this.oParentObject.w_PNTIPDOC<>"CO" And this.w_OK
          * --- Esegue il Controllo Differenza IVA dal Totale Documento in Valuta
          *     Controllo che la somma degli Importi nel castelletto Iva (Imponibile, Imponibile+Iva, IVA ..)
          *     come dice la causale sia equivalente all'importo in valuta della registrazione
          this.w_DATRIF = IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC)
          if  this.oParentObject.w_PNTIPREG="C" And this.oParentObject.w_PNTIPDOC="FC" 
            this.w_TOTIVA = this.w_TOTIVCO
          endif
          this.w_TOTREG = cp_ROUND(VAL2MON(this.oParentObject.w_PNTOTDOC, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATRIF, this.oParentObject.w_PNVALNAZ), this.oParentObject.w_DECTOP)
          if this.w_TOTREG<>this.w_TOTIVA
            this.w_MESS = Ah_MsgFormat("Totale castelleto IVA (%1)%0Totale doc. in valuta di conto (%2)%0Differenza (%3)",Alltrim(Tran(this.w_TOTIVA,v_PV(40+VVP))),Alltrim(Tran(this.w_TOTREG,v_PV(40+VVP))),Alltrim(Tran(this.w_TOTIVA-this.w_TOTREG,v_PV(40+VVP))))
            this.w_OK = ah_YesNo("%1.%0Confermi ugualmente?",,this.w_MESS)
            if not this.w_OK
              this.w_oPart = this.w_oMess.AddMsgPartNL("Totale castelleto IVA (%1)%0Totale doc. in valuta di conto (%2)%0Differenza (%3)")
              this.w_oPart.AddParam(Alltrim(Tran(this.w_TOTIVA,v_PV(40+VVP))))     
              this.w_oPart.AddParam(Alltrim(Tran(this.w_TOTREG,v_PV(40+VVP))))     
              this.w_oPart.AddParam(Alltrim(Tran(this.w_TOTIVA-this.w_TOTREG,v_PV(40+VVP))))     
            endif
          endif
          if this.w_OK
            * --- Calcoli per controlli sul castelleto Contabile
            this.w_TOTIVA = this.w_TOTREG
          else
            this.oParentObject.w_RESCHK = -1
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Se fattura corrispettivi e tipo registro corrispettivi e non ho intestatario
        *     eseguo controlli su totale contropartite con imponibile e totale iva
        if this.oParentObject.w_PNTIPREG="C" And this.oParentObject.w_PNTIPDOC<>"FC" And this.w_OK And (ABS(this.w_TOTIMCO)<>this.w_RIGCNT or ABS(this.w_TOTIVCO)<>this.w_RIGIVA) And this.oParentObject.w_CCCHKIVA="S"
          * --- Quadratura IVA
          if ABS(this.w_TOTIMCO)<>this.w_RIGCNT
            this.w_oMess.AddMsgPartNL("Importo imponibile incongruente con sezione IVA")     
            this.w_oPart = this.w_oMess.AddMsgPartNL("(Differenza imponibile = %1)")
            this.w_oPart.AddParam(ALLTRIM(STR(ABS(this.w_TOTIMCO)-this.w_RIGCNT,18,this.oParentObject.w_DECTOP)))     
          endif
          if ABS(this.w_TOTIVCO)<>this.w_RIGIVA
            this.w_oMess.AddMsgPartNL("Importo IVA incongruente con sezione IVA")     
            this.w_oPart = this.w_oMess.AddMsgPartNL("(Differenza IVA = %1)")
            this.w_oPart.AddParam(ALLTRIM(STR(ABS(this.w_TOTIVCO)-this.w_RIGIVA,18,this.oParentObject.w_DECTOP)))     
          endif
          this.w_OK = .F.
        endif
        * --- Se fattura corrispettivi e tipo registro corrispettivi e ho intestatario non eseguo i controlli 
        if  Not( this.oParentObject.w_PNTIPREG="C" And this.oParentObject.w_PNTIPDOC="FC" )
          this.w_DIFIVA = ABS(IIF(this.oParentObject.w_CFDAVE="D", this.w_SEZDAR - this.w_SEZAVE, this.w_SEZAVE - this.w_SEZDAR )) - ABS(this.w_TOTIVA)
          do case
            case this.w_OK AND this.w_DIFIVA<>0 And this.oParentObject.w_PNTIPCLF<>"N" And Not ( this.oParentObject.w_PNTIPREG ="C" And this.oParentObject.w_PNTIPDOC="CO")
              * --- Quadratura IVA
              this.w_oMess.AddMsgPartNL("Importo su cliente/fornitore incongruente con sezione IVA")     
              this.w_oPart = this.w_oMess.AddMsgPartNL("(Differenza IVA = %1)")
              this.w_oPart.AddParam(ALLTRIM(STR(this.w_DIFIVA,18,this.oParentObject.w_DECTOP)))     
              this.w_OK = .F.
            case this.w_OK AND this.oParentObject.w_CALDOC<>"N" AND (this.w_SEZAVE<>0 OR this.w_SEZDAR<>0) AND ((this.oParentObject.w_CFDAVE="D" AND this.w_SEZDAR=0) OR (this.oParentObject.w_CFDAVE="A" AND this.w_SEZAVE=0)) And this.oParentObject.w_PNTIPCLF<>"N"
              * --- Documento sulla Sezione DARE/AVERE sbagliata
              this.w_oMess.AddMsgPartNL("Importo documento su sezione dare-avere errata")     
              this.w_OK = .F.
            case this.w_OK And this.w_IVADET<>this.w_RIGIVA 
              * --- Importo IVA Detraibile non congruente con il Totale righe Conti IVA
              this.w_oMess.AddMsgPartNL("Importi righe IVA incongruenti con il totale IVA deducibile")     
              this.w_oPart = this.w_oMess.AddMsgPartNL("(Tot. IVA deducibile = %1)")
              this.w_oPart.AddParam(ALLTRIM(STR(this.w_IVADET,18,this.oParentObject.w_DECTOP)))     
              this.w_oPart = this.w_oMess.AddMsgPartNL("(Tot. righe conti IVA= %1)")
              this.w_oPart.AddParam(ALLTRIM(STR(this.w_RIGIVA,18,this.oParentObject.w_DECTOP)))     
              this.w_OK = .F.
          endcase
        endif
      endif
      * --- Riposiziono  il dettaglio contabile
       
 SELECT (this.oParentObject.cTrsName) 
 GO this.w_RECPOS 
 With this.oParentObject 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow= this.w_AbsRow 
 .oPgFrm.Page1.oPag.oBody.nRelRow= this.w_nRelRow 
 EndWith 
 
    endif
    * --- Controllo se � attivo il modulo ritenute se non � vuoto il codice fornitore, se � 
    *     stata selezionata una causale contabile con attivo il flag gestiione ritenute e
    *     se il fornitore � soggetto a ritenute.
    if this.w_OK and g_RITE="S" and not empty(this.oParentObject.w_PNCODCLF) and nvl(this.oParentObject.w_GESRIT," ")="S" and this.oParentObject.w_RITENU$"CS" and this.oParentObject.w_PNTIPCLF="F" and this.oParentObject.w_TOTMDR<>-0.000001
      * --- Verifico se il movimento di primanota ha attivo il flag "Storno immediato". Se non
      *     � valorizzato non effettuo nessun controllo.
      if this.oParentObject.w_PNFLGSTO="S"
        * --- Se sono in caricamento ma non sono ancora entrato nei dati ritenute non
        *     devo effettuare nessun controllo
        if this.w_CFUNC="Load" and this.oParentObject.w_PCCLICK=.F.
          * --- Non effettuo nessun controllo
        else
          * --- Devo verificare che il totale da versare impostato sui dati ritenute
          *     sia identico al totale delle partite legate al fornitore.
          *     Utilizzzo la variabile PCCLICK per verificare se sono entrato all'interno della
          *     gestione dei Dati Ritenute
          if this.oParentObject.w_PCCLICK=.T.
            this.w_TOTDAVER = This.oParentobject.Gscg_Mdr.cnt.w_Drdavers
          else
            * --- Effettuo una select del cursore Datirite per avere il totale delle ritenute
            this.w_TOTRITE = 0
            * --- Select from DATIRITE
            i_nConn=i_TableProp[this.DATIRITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DATIRITE_idx,2],.t.,this.DATIRITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" DATIRITE ";
                  +" where DRSERIAL="+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+"";
                   ,"_Curs_DATIRITE")
            else
              select * from (i_cTable);
               where DRSERIAL=this.oParentObject.w_PNSERIAL;
                into cursor _Curs_DATIRITE
            endif
            if used('_Curs_DATIRITE')
              select _Curs_DATIRITE
              locate for 1=1
              do while not(eof())
              this.w_TOTRITE = this.w_TOTRITE+_Curs_DATIRITE.DRRITENU
                select _Curs_DATIRITE
                continue
              enddo
              use
            endif
            * --- Calcolo il totale da versare
            this.w_TOTRITE = ABS(this.w_TOTRITE)
            this.w_TOTMDR1 = ABS(this.oParentObject.w_TOTMDR)
            this.w_TOTDAVER = this.w_TOTMDR1-this.w_TOTRITE
          endif
          this.w_TOTDAVER = ABS(this.w_TOTDAVER)
          this.w_AbsRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
          this.w_nRelRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
          SELECT (this.oParentObject.cTrsName)
          this.w_RECPOS = IIF( Eof() , RECNO(this.oParentObject.cTrsName)-1 , RECNO(this.oParentObject.cTrsName) )
          GO TOP
          SCAN FOR NVL(t_PNTIPCON," ")="F" AND NOT EMPTY(NVL(t_PNCODCON," ")) AND t_PNFLPART$"CS"
          this.w_TOTRITD = this.w_TOTRITD + NVL(t_PNIMPDAR, 0)
          this.w_TOTRITA = this.w_TOTRITA + NVL(t_PNIMPAVE, 0)
          ENDSCAN
          if ABS(this.w_TOTRITA-this.w_TOTRITD)<>this.w_TOTDAVER
            this.w_MESS = "Totale da versare specificato nei dati ritenute [%1]%0diverso dal totale addebitato sul fornitore [%2]"
            ah_ErrorMsg(this.w_MESS,,"",Alltrim(Tran(this.w_TOTDAVER,v_PV[40+VVP])),Alltrim(Tran(ABS(this.w_TOTRITA-this.w_TOTRITD),v_PV[40+VVP])))
          endif
           
 SELECT (this.oParentObject.cTrsName) 
 GO this.w_RECPOS 
 With this.oParentObject 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow= this.w_AbsRow 
 .oPgFrm.Page1.oPag.oBody.nRelRow= this.w_nRelRow 
 EndWith 
 
        endif
      endif
    endif
    * --- Segnalo eventuali errori alla CHECK FORM
    if Not this.w_OK 
      this.w_MESS = this.w_oMess.ComposeMessage()
      if NOT EMPTY(this.w_MESS)
        ah_ErrorMsg("%1",,"",this.w_MESS)
      endif
      this.oParentObject.w_RESCHK = -1
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='DATIRITE'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_DATIRITE')
      use in _Curs_DATIRITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
