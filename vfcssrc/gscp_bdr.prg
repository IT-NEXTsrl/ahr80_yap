* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_bdr                                                        *
*              Avvio stampe di controllo                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-26                                                      *
* Last revis.: 2005-09-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_bdr",oParentObject)
return(i_retval)

define class tgscp_bdr as StdBatch
  * --- Local variables
  w_OBJ = .NULL.
  * --- WorkFile variables
  ZMP_QCHK_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Avvia la stampa di controllo relativa alla pubblicazione dati CPZ
    this.w_OBJ = GSCP_KQC(THIS)
    this.w_OBJ.HIDE()     
    this.w_OBJ.SHOW(1)     
    * --- Drop temporary table ZMP_QCHK
    i_nIdx=cp_GetTableDefIdx('ZMP_QCHK')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('ZMP_QCHK')
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*ZMP_QCHK'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
