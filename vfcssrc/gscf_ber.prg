* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_ber                                                        *
*              errore controllo flussi (sotto transazione)                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-02-03                                                      *
* Last revis.: 2012-06-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXPR,pMSG,w_RESERIAL,w_REDESCRI,pONERROR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscf_ber",oParentObject,m.pEXPR,m.pMSG,m.w_RESERIAL,m.w_REDESCRI,m.pONERROR)
return(i_retval)

define class tgscf_ber as StdBatch
  * --- Local variables
  pEXPR = space(1)
  pMSG = space(10)
  w_RESERIAL = space(10)
  w_REDESCRI = space(40)
  pONERROR = space(10)
  w_TRSERR = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- errore controllo flussi (sotto transazione)
    * --- Chiudo tutti i cursori del controllo flussi
    if used("_CurRule_")
      use in _CurRule_
    endif
    if used("oldvalues")
      use in oldvalues
    endif
    if used("TMPLOG")
      use in TMPLOG
    endif
    if used("_PKRules_")
      use in _PKRules_
    endif
    if used("_Rules_")
      use in _Rules_
    endif
    if empty(this.pMSG)
      this.pMSG = message()
    endif
    if NOT EMPTY(this.pEXPR)
      this.pEXPR = STRTRAN(this.pEXPR, "this.w_PADRE.", "")
      this.pMSG = AH_MSGFORMAT("Espressione <%1> non valida.%0%2", this.pEXPR , this.pMSG)
    endif
    if EMPTY(NVL(this.w_REDESCRI,""))
      this.w_TRSERR = ah_msgformat("gestione controllo flussi, codice regola %1:%0%2", this.w_RESERIAL, this.pMSG)
    else
      if len(alltrim(this.w_REDESCRI))<35
        this.w_TRSERR = ah_msgformat("gestione controllo flussi, %1:%0%2", alltrim(this.w_REDESCRI), this.pMSG)
      else
        this.w_TRSERR = ah_msgformat("gestione controllo flussi,%0%1:%0%2", alltrim(this.w_REDESCRI), this.pMSG)
      endif
    endif
    * --- transaction error
    bTrsErr=.t.
    i_TrsMsg=this.w_TRSERR
    cOnError = this.pONERROR
    on error &cOnError
    this.oParentObject.w_retcode = "stop"
    i_retcode = 'stop'
    return
  endproc


  proc Init(oParentObject,pEXPR,pMSG,w_RESERIAL,w_REDESCRI,pONERROR)
    this.pEXPR=pEXPR
    this.pMSG=pMSG
    this.w_RESERIAL=w_RESERIAL
    this.w_REDESCRI=w_REDESCRI
    this.pONERROR=pONERROR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXPR,pMSG,w_RESERIAL,w_REDESCRI,pONERROR"
endproc
