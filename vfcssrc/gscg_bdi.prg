* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bdi                                                        *
*              Liquidazione periodica IVA                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_785]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-30                                                      *
* Last revis.: 2018-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bdi",oParentObject)
return(i_retval)

define class tgscg_bdi as StdBatch
  * --- Local variables
  w_REGINI = ctod("  /  /  ")
  w_REGFIN = ctod("  /  /  ")
  w_OK = .f.
  w_FLVAL = space(1)
  w_TIPOPE = space(1)
  w_TOTIMP = 0
  w_TOTIVD = 0
  w_TOTIVI = 0
  w_TOTMAC = 0
  w_GLOMAC = 0
  w_PERMAC = 0
  w_TOTCOV = 0
  w_INDAZI = space(25)
  w_LOCAZI = space(35)
  w_CAPAZI = space(5)
  w_PROAZI = space(2)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_TIPLIQ = space(1)
  w_F2DEFI = space(1)
  w_TIPDIC = space(1)
  w_FLTEST = space(1)
  w_ANNPLA = space(4)
  w_DATINI = ctod("  /  /  ")
  w_DATBLO = ctod("  /  /  ")
  w_DATBLO2 = ctod("  /  /  ")
  w_DATFI2 = ctod("  /  /  ")
  w_DESCRI = space(65)
  w_MESS = space(90)
  w_APPO = space(10)
  w_APPO1 = 0
  w_APPO2 = 0
  w_CONFER = space(1)
  w_CONCES = space(3)
  w_IMPOR1 = 0
  w_CESINT = 0
  w_IMPOR5 = 0
  w_IMPOR2 = 0
  w_ACQINT = 0
  w_IMPON3 = 0
  w_IMPOS3 = 0
  w_IMPOR6 = 0
  w_IMPOR6C = 0
  w_IMPOR6A = 0
  w_IMPOR6B = 0
  w_VPIMPOR1 = 0
  w_VPIMPOR2 = 0
  w_VPCESINT = 0
  w_VPACQINT = 0
  w_VPIMPON3 = 0
  w_VPIMPOS3 = 0
  w_VPIMPOR5 = 0
  w_VPIMPOR6 = 0
  w_VPIMPOR6C = 0
  w_VPIMPOR6A = 0
  w_VPIMPOR7 = 0
  w_VPIMPO7A = 0
  w_VPIMPO7B = 0
  w_VPIMPO7C = 0
  w_VIMPOR8 = 0
  w_VIMPOR9 = 0
  w_VIMPO10 = 0
  w_VIMPO11 = 0
  w_VIMPO12 = 0
  w_VIMPO13 = 0
  w_VIMPO15 = 0
  w_VIMPO16 = 0
  w_VPIMPOR8 = 0
  w_VPIMPOR9 = 0
  w_VPIMPO10 = 0
  w_VPIMPO11 = 0
  w_VPIMPO12 = 0
  w_VPIMPO13 = 0
  w_VPIMPO14 = 0
  w_VPIMPO15 = 0
  w_VPIMPO16 = 0
  w_VPIMPVER = 0
  w_VPCRERIM = 0
  w_VPCREUTI = 0
  w_VPDICSOC = space(1)
  w_VPDATVER = ctod("  /  /  ")
  w_VPCODCAR = space(1)
  w_VPCORTER = space(1)
  w_VPCODFIS = space(16)
  w_VPDICGRU = space(1)
  w_VPCODAZI = space(5)
  w_VPCODCAB = space(5)
  w_VPCODVAL = space(1)
  w_VPEURO19 = space(1)
  w_VPVEREUR = space(1)
  w_VPVARIMP = space(1)
  w_VPVERNEF = space(1)
  w_VPAR74C5 = space(1)
  w_VPAR74C4 = space(1)
  w_VPOPMEDI = space(1)
  w_VPOPNOIM = space(1)
  w_VPACQBEA = space(1)
  w_VPDATPRE = ctod("  /  /  ")
  w_CREVAL = space(3)
  w_CRERES = 0
  w_PRIMPO12 = 0
  w_PRIMPO16 = 0
  w_PRCRERIM = 0
  w_PRCREUTI = 0
  w_KEYATT = space(5)
  w_OLDPER = 0
  w_PERPRO = 0
  w_MINACC = 0
  w_MINVER = 0
  w_FLPROR = space(1)
  w_VP__ANNO = space(4)
  w_VPPERIOD = 0
  w_VPCODATT = space(5)
  w_VPKEYATT = space(5)
  w_MAGTRI = 0
  w_CODIVA = space(5)
  w_DESIVA = space(35)
  w_TIPREG = space(1)
  w_NUMREG = 0
  w_CONTA = 0
  w_FLAGRI = space(1)
  w_PERCOM = 0
  w_TIPAGR = space(1)
  w_ALIAGR = 0
  DETAGR = space(10)
  w_OKCAL = .f.
  w_ATTIVI = space(5)
  w_INIPER = ctod("  /  /  ")
  w_FINPER = ctod("  /  /  ")
  w_FINPE2 = ctod("  /  /  ")
  w_IALIMCOM = ctod("  /  /  ")
  w_PAGINE = 0
  w_PRPARI = 0
  w_INTLIG = space(1)
  w_PREFIS = space(20)
  w_DESABI = space(80)
  w_DESFIL = space(40)
  w_INDIRI = space(50)
  w_CAP = space(5)
  w_NUMOPE = 0
  w_PERINT = 0
  w_REGISTRO = 0
  w_LETAZI = .f.
  w_PRIREG = space(1)
  w_PRINUM = 0
  w_VALNAZ = space(3)
  w_IMPONI = 0
  w_IMPAUT = 0
  w_VPSTAREG = space(1)
  w_CAUGIR = space(5)
  w_CONIVA = space(15)
  w_CREDRIP = space(15)
  w_CREDSPE = space(15)
  w_INTDOV = space(15)
  w_GENVENT = space(1)
  w_GENAUTO = space(1)
  w_LCODATT = space(8)
  w_LDESATT = space(35)
  w_TIPOPE = space(1)
  w_TIPREG = space(1)
  w_TNUMREG = 0
  w_TCODIVA = space(5)
  w_IMPSTA = 0
  w_IMPSEG = 0
  w_IMPIND = 0
  w_IVASTA = 0
  w_IVASEG = 0
  w_IVAIND = 0
  w_IVISTA = 0
  w_IVISEG = 0
  w_IVIIND = 0
  w_MACSTA = 0
  w_MACSEG = 0
  w_MACIND = 0
  w_IMPPRE = 0
  w_IMPFAD = 0
  w_IVAPRE = 0
  w_IVAFAD = 0
  w_IVIPRE = 0
  w_IVIFAD = 0
  w_MACPRE = 0
  w_MACFAD = 0
  w_RESVEN1 = 0
  w_VIMPOR6 = 0
  w_MSG1 = space(10)
  w_FLINTR = space(1)
  w_CODCON = space(20)
  w_TIPDOC = space(2)
  w_CODCAU = space(5)
  w_CODINT = space(5)
  w_TESTPROR = space(1)
  w_oMess = .NULL.
  w_TOTATTIVA = 0
  w_TOTATTPRO = 0
  w_IVSPLPAY = space(1)
  w_TOTACQ = 0
  w_IMPPRO = 0
  w_ROWNUM = 0
  w_CODCAU = space(5)
  w_PNDATREG = ctod("  /  /  ")
  w_FLSALF = space(1)
  w_CODVEN = space(15)
  w_PNDESSUP = space(45)
  w_FLSALI = space(1)
  w_TIPVEN = space(1)
  w_CONFE2 = space(1)
  w_TIPIVA = space(1)
  w_TIPCON = space(1)
  w_PNCODESE = space(4)
  w_CPROWORD = 0
  w_UTCC = 0
  w_PNCODUTE = 0
  w_CPROWNUM = 0
  w_UTDC = ctod("  /  /  ")
  w_PNSERIAL = space(10)
  w_PNCODVAL = space(3)
  w_UTCV = 0
  w_PNNUMRER = 0
  w_PNCAOVAL = 0
  w_UTDV = ctod("  /  /  ")
  w_PNCOMPET = space(4)
  w_PNVALNAZ = space(3)
  w_PNFLSALF = space(1)
  w_PNPRG = space(8)
  w_PNFLSALI = space(1)
  w_PNFLSALD = space(1)
  w_CONSUP = space(15)
  w_SEZBIL = space(1)
  w_IMPONI = 0
  w_CONTA = 0
  w_TOTALE = 0
  w_DATAINIZ = ctod("  /  /  ")
  w_DATAINIP = ctod("  /  /  ")
  w_DATAFINP = ctod("  /  /  ")
  w_FLTRASP = .f.
  w_AGGDOC = space(1)
  w_LCODCAU = space(5)
  w_SERIAL = space(10)
  w_CONIND = space(15)
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_IMPORTODAR = 0
  w_CONTO = space(15)
  w_PNDESRIG = space(50)
  w_CCDESRIG = space(254)
  w_CCDESSUP = space(254)
  * --- WorkFile variables
  ATTIDETT_idx=0
  ATTIMAST_idx=0
  AZIENDA_idx=0
  BAN_CHE_idx=0
  CREDDIVA_idx=0
  CREDMIVA_idx=0
  IVA_PERI_idx=0
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  PRI_DETT_idx=0
  PRI_MAST_idx=0
  PRO_RATA_idx=0
  SALDICON_idx=0
  CONTI_idx=0
  MASTRI_idx=0
  DAT_IVAN_idx=0
  COD_ABI_idx=0
  COD_CAB_idx=0
  PRI_TEMP_idx=0
  CAUIVA1_idx=0
  VOCIIVA_idx=0
  ATT_ALTE_idx=0
  DOC_DETT_idx=0
  CAU_CONT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Liquidazione IVA Periodica (da GSCG_SDI e da GSCG_BAI)
    DIMENSION DETAGR[90, 3],ARPARAM[12,2]
    * --- Variabili Passate dalla Maschera
    this.w_PRPARI = this.oParentObject.w_PRPARI
    this.w_INTLIG = this.oParentObject.w_INTLIG
    this.w_PREFIS = this.oParentObject.w_PREFIS
    this.w_APPO = IIF(g_TIPDEN="M", this.oParentObject.w_NUMPER, (this.oParentObject.w_NUMPER * 3) - 2)
    this.w_APPO = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.oParentObject.w_ANNO
    this.w_DATINI = cp_CharToDate(this.w_APPO)
    this.w_VPSTAREG = this.oParentObject.w_STAREG
    this.w_PRIREG = this.oParentObject.w_ATTIPREG
    this.w_PRINUM = this.oParentObject.w_ATNUMREG
    do case
      case EMPTY(this.oParentObject.w_ANNO)
        ah_ErrorMsg("Anno di riferimento non definito",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_NUMPER)
        ah_ErrorMsg("Periodo di riferimento non definito",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CODATT) AND this.oParentObject.w_MTIPLIQ<>"R"
        ah_ErrorMsg("Codice attivit� non definito",,"")
        i_retcode = 'stop'
        return
      case CALCESER(this.w_DATINI, "####")<>g_CODESE
        ah_ErrorMsg("Il periodo selezionato non � di competenza dell'esercizio corrente",,"")
        i_retcode = 'stop'
        return
    endcase
    * --- Verifiche se Stampa Definitiva
    if this.oParentObject.w_FLDEFI="S"
      this.w_OK = .T.
      this.w_MESS = " "
      if this.oParentObject.w_MTIPLIQ<>"R"
        * --- Singola Attivita' : Verifica che siano stati stampati tutti i registri dell'attivita' considerata
        this.w_DATBLO2 = cp_CharToDate("  -  -  ")
        * --- Data di Fine Periodo
        this.w_APPO = IIF(g_TIPDEN="M", this.oParentObject.w_NUMPER+1, ((this.oParentObject.w_NUMPER+1) * 3) - 2)
        this.w_APPO2 = this.oParentObject.w_ANNO
        if this.w_APPO>12
          this.w_APPO = 1
          this.w_APPO2 = ALLTRIM(STR(VAL(this.oParentObject.w_ANNO)+1,4,0))
        endif
        this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
        this.w_APPO1 = cp_CharToDate(this.w_APPO1)-1
        * --- Select from ATTIDETT
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIDETT ";
              +" where ATCODATT="+cp_ToStrODBC(this.oParentObject.w_CODATT)+"";
               ,"_Curs_ATTIDETT")
        else
          select * from (i_cTable);
           where ATCODATT=this.oParentObject.w_CODATT;
            into cursor _Curs_ATTIDETT
        endif
        if used('_Curs_ATTIDETT')
          select _Curs_ATTIDETT
          locate for 1=1
          do while not(eof())
          this.w_DATBLO2 = CP_TODATE(_Curs_ATTIDETT.ATDATSTA)
          if this.w_OK=.T. AND (EMPTY(this.w_DATBLO2) OR this.w_DATBLO2<this.w_APPO1)
            this.w_OK = .F.
            this.w_MESS = ah_MsgFormat("Impossibile eseguire la liquidazione periodica definitiva.%0Non ancora eseguita la stampa definitiva di tutti i registri associati all'attivit�")
          endif
            select _Curs_ATTIDETT
            continue
          enddo
          use
        endif
      else
        * --- Riepilogativa Attivita' : Verifica che siano state Stampate le Liquidazioni delle Singole Attivita'
        * --- Select from ATTIMAST
        i_nConn=i_TableProp[this.ATTIMAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ATCODATT  from "+i_cTable+" ATTIMAST ";
               ,"_Curs_ATTIMAST")
        else
          select ATCODATT from (i_cTable);
            into cursor _Curs_ATTIMAST
        endif
        if used('_Curs_ATTIMAST')
          select _Curs_ATTIMAST
          locate for 1=1
          do while not(eof())
          this.w_APPO = NVL(_Curs_ATTIMAST.ATCODATT, " ")
          if this.w_OK=.T. AND NOT EMPTY(this.w_APPO)
            this.w_APPO1 = SPACE(5)
            * --- Read from IVA_PERI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.IVA_PERI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2],.t.,this.IVA_PERI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VPCODATT"+;
                " from "+i_cTable+" IVA_PERI where ";
                    +"VP__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                    +" and VPPERIOD = "+cp_ToStrODBC(this.oParentObject.w_NUMPER);
                    +" and VPKEYATT = "+cp_ToStrODBC(this.w_APPO);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VPCODATT;
                from (i_cTable) where;
                    VP__ANNO = this.oParentObject.w_ANNO;
                    and VPPERIOD = this.oParentObject.w_NUMPER;
                    and VPKEYATT = this.w_APPO;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_APPO1 = NVL(cp_ToDate(_read_.VPCODATT),cp_NullValue(_read_.VPCODATT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if EMPTY(this.w_APPO1) OR this.w_APPO1<>this.w_APPO
              this.w_OK = .F.
              this.w_MESS = ah_MsgFormat("Impossibile eseguire la liquidazione riepilogativa%0non ancora eseguita la liquidazione per l'attivit�: %1",this.w_APPO)
            endif
          endif
            select _Curs_ATTIMAST
            continue
          enddo
          use
        endif
      endif
      if this.w_OK=.T.
        this.w_VP__ANNO = this.oParentObject.w_ANNO
        this.w_VPPERIOD = this.oParentObject.w_NUMPER
        this.w_APPO = IIF(this.oParentObject.w_MTIPLIQ="R", "#####", this.oParentObject.w_CODATT)
        this.w_APPO1 = "XXXXX"
        * --- Read from IVA_PERI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.IVA_PERI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2],.t.,this.IVA_PERI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VPCODATT"+;
            " from "+i_cTable+" IVA_PERI where ";
                +"VP__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                +" and VPPERIOD = "+cp_ToStrODBC(this.oParentObject.w_NUMPER);
                +" and VPKEYATT = "+cp_ToStrODBC(this.w_APPO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VPCODATT;
            from (i_cTable) where;
                VP__ANNO = this.oParentObject.w_ANNO;
                and VPPERIOD = this.oParentObject.w_NUMPER;
                and VPKEYATT = this.w_APPO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APPO1 = NVL(cp_ToDate(_read_.VPCODATT),cp_NullValue(_read_.VPCODATT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          this.w_OK = .F.
          this.w_MESS = ah_MsgFormat("Liquidazione gi� eseguita in definitiva per il periodo e attivit�.%0Rielaborare in prova oppure annullare i dati della dichiarazione associata")
        endif
      endif
      if this.w_OK=.F.
        ah_ErrorMsg(this.w_MESS,,"")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Testata Report
    L_TESTATA=""
    if this.oParentObject.w_FLDEFI="N"
      L_TESTATA=ah_MsgFormat("Stampa di prova della liquidazione IVA")
    endif
    L_PERIODO=ah_MsgFormat("Conguaglio liquidazione")
    if g_TIPDEN="T"
      * --- PERIODO TRIMESTRALE
      do case
        case this.oParentObject.w_NUMPER=1
          L_PERIODO=ah_MsgFormat("IVA del periodo gennaio - febbraio - marzo")
        case this.oParentObject.w_NUMPER=2
          L_PERIODO=ah_MsgFormat("IVA del periodo aprile - maggio - giugno")
        case this.oParentObject.w_NUMPER=3
          L_PERIODO=ah_MsgFormat("IVA del periodo luglio - agosto - settembre")
        case this.oParentObject.w_NUMPER=4
          L_PERIODO=ah_MsgFormat("IVA del periodo ottobre - novembre - dicembre")
      endcase
    else
      * --- Periodo Mensile
      if this.oParentObject.w_NUMPER>0 AND this.oParentObject.w_NUMPER<13
        L_PERIODO=g_MESE[ this.oParentObject.w_NUMPER ]
      endif
    endif
    L_DESMESE=ah_MsgFormat("Conguaglio liquidazione %1",this.oParentObject.w_ANNO)
    * --- Numero Periodo e Attivit�
    if this.oParentObject.w_MTIPLIQ="R"
      L_Attivita=ah_MsgFormat("Riepilogo attivit�")
    else
      * --- Applico Codice Attivit� alternativo
      *     Esiste procedura di conversione che crea record di default cod codice attivit�
      *     pari al codice attivit� padre
      this.w_LCODATT = this.oParentObject.w_CODATT
      this.w_LDESATT = this.oParentObject.w_DESATT
      * --- Select from ATT_ALTE
      i_nConn=i_TableProp[this.ATT_ALTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_ALTE_idx,2],.t.,this.ATT_ALTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CAATTIVI,CADESCRI  from "+i_cTable+" ATT_ALTE ";
            +" where CADATATT<="+cp_ToStrODBC(this.w_DATINI)+" AND CACODATT="+cp_ToStrODBC(this.oParentObject.w_CODATT)+"";
            +" order by CADATATT Desc";
             ,"_Curs_ATT_ALTE")
      else
        select CAATTIVI,CADESCRI from (i_cTable);
         where CADATATT<=this.w_DATINI AND CACODATT=this.oParentObject.w_CODATT;
         order by CADATATT Desc;
          into cursor _Curs_ATT_ALTE
      endif
      if used('_Curs_ATT_ALTE')
        select _Curs_ATT_ALTE
        locate for 1=1
        do while not(eof())
        this.w_LCODATT = Nvl(_Curs_ATT_ALTE.CAATTIVI,Space(8))
        this.w_LDESATT = Nvl(_Curs_ATT_ALTE.CADESCRI,Space(35))
        Exit
          select _Curs_ATT_ALTE
          continue
        enddo
        use
      endif
      L_Attivita=this.w_LCODATT + " " + this.w_LDESATT
    endif
    L_NumPer=this.oParentObject.w_NUMPER
    if this.oParentObject.w_NUMPER>0 AND this.oParentObject.w_NUMPER<13
      L_DESMESE=g_MESE[ this.oParentObject.w_NUMPER ]+" "+this.oParentObject.w_ANNO
    endif
    this.w_MINACC = 0
    this.w_MINVER = 0
    this.w_APPO1 = 0
    this.w_APPO2 = 0
    this.w_APPO = g_PERVAL
    * --- Read from DAT_IVAN
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IAACCIVA,IAMINIVA,IAMINIVE,IAVALIVA,IALIMCOM"+;
        " from "+i_cTable+" DAT_IVAN where ";
            +"IACODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
            +" and IA__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IAACCIVA,IAMINIVA,IAMINIVE,IAVALIVA,IALIMCOM;
        from (i_cTable) where;
            IACODAZI = this.oParentObject.w_CODAZI;
            and IA__ANNO = this.oParentObject.w_ANNO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MINACC = NVL(cp_ToDate(_read_.IAACCIVA),cp_NullValue(_read_.IAACCIVA))
      this.w_APPO1 = NVL(cp_ToDate(_read_.IAMINIVA),cp_NullValue(_read_.IAMINIVA))
      this.w_APPO2 = NVL(cp_ToDate(_read_.IAMINIVE),cp_NullValue(_read_.IAMINIVE))
      this.w_APPO = NVL(cp_ToDate(_read_.IAVALIVA),cp_NullValue(_read_.IAVALIVA))
      this.w_IALIMCOM = NVL(cp_ToDate(_read_.IALIMCOM),cp_NullValue(_read_.IALIMCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows=0
      ah_ErrorMsg("Dati relativi ai versamenti minimi non specificati per l'anno selezionato",,"")
      i_retcode = 'stop'
      return
    else
      * --- Versamento Minimo IVA Lire o EURO in funzione dellla Valuta di Conto
      this.w_MINVER = IIF(g_PERVAL=g_CODLIR, this.w_APPO1, this.w_APPO2)
      this.w_APPO = IIF(EMPTY(this.w_APPO), g_PERVAL, this.w_APPO)
      * --- Importo Minimo Acconto IVA
      if this.w_MINACC<>0 AND this.w_APPO<>g_PERVAL AND g_CAOEUR<>0 
        do case
          case this.w_APPO=g_CODLIR
            this.w_MINACC = cp_ROUND(this.w_MINACC/g_CAOEUR, g_PERPVL)
          case this.w_APPO=g_CODEUR
            this.w_MINACC = cp_ROUND(this.w_MINACC*g_CAOEUR, g_PERPVL)
        endcase
      endif
    endif
    * --- Blocco della Primanota e dei registri associati alla Attivita'
    this.w_DATFI2 = i_DATSYS
    * --- Se Stampa reg.IVA la Primanota viene gia' bloccata in GSCG_BRI
    * --- Try
    local bErr_045E7650
    bErr_045E7650=bTrsErr
    this.Try_045E7650()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg(i_errmsg,,"")
      this.oParentObject.w_STOPELA = .F.
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_045E7650
    * --- End
    * --- Se Stampa in prova riesegue la Ricostruzione Saldi IVA
    this.w_OKCAL = .T.
    * --- Calcola Limiti Periodo
    if (this.oParentObject.w_NUMPER<13 AND g_TIPDEN="M") OR (this.oParentObject.w_NUMPER<5 AND g_TIPDEN<>"M")
      this.w_APPO = IIF(g_TIPDEN="M", this.oParentObject.w_NUMPER, (this.oParentObject.w_NUMPER * 3) - 2)
      this.w_APPO2 = this.oParentObject.w_ANNO
      this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
      this.w_INIPER = cp_CharToDate(this.w_APPO1)
      this.w_APPO = IIF(g_TIPDEN="M", this.oParentObject.w_NUMPER+1, ((this.oParentObject.w_NUMPER+1) * 3) - 2)
      this.w_APPO2 = this.oParentObject.w_ANNO
      if this.w_APPO>12
        this.w_APPO = 1
        this.w_APPO2 = ALLTRIM(STR(VAL(this.oParentObject.w_ANNO)+1,4,0))
      endif
      if this.oParentObject.w_FLDEFI="A"
        this.w_FINPER = cp_CharToDate("20-12-" + this.oParentObject.w_ANNO)
      else
        this.w_APPO1 = "01-"+RIGHT("00"+ALLTRIM(STR(this.w_APPO)),2)+"-"+this.w_APPO2
        this.w_FINPER = cp_CharToDate(this.w_APPO1)-1
      endif
      * --- Ricalcolo la data di fine periodo (Utilizzata dalla routine Gscg_Bv2) 
      *     andando a vedere se valorizzata dentro al tabella "Dati IVA annuali"
      if g_TIPDEN<>"M"
        * --- trimestrale
        if this.oParentObject.w_NUMPER<>4 Or Empty(this.w_IALIMCOM)
          this.w_FINPE2 = this.w_FINPER + 15
        else
          this.w_FINPE2 = this.w_IALIMCOM
          this.w_REGINI = this.w_INIPER
          this.w_REGFIN = this.w_IALIMCOM
        endif
      else
        * --- Mensile
        if this.oParentObject.w_NUMPER<>12 Or Empty(this.w_IALIMCOM)
          this.w_FINPE2 = this.w_FINPER + 15
        else
          this.w_FINPE2 = this.w_IALIMCOM
          this.w_REGINI = this.w_INIPER
          this.w_REGFIN = this.w_IALIMCOM
        endif
      endif
    endif
    if this.oParentObject.w_FLDEFI="N" OR this.oParentObject.w_FLDEFI="A"
      * --- Per Compatibilita' con elaborazione Reg.IVA
      * --- Inizia Elaborazione ,Cicla sulle Attivita' (Quella Selezionata o Tutte se Liquidazione Riepilogativa)
      this.w_KEYATT = IIF(this.w_TIPLIQ="R" OR this.oParentObject.w_FLDEFI="A", "     ", this.oParentObject.w_CODATT)
      VQ_EXEC("QUERY\GSCG1BLP.VQR",this,"ATTIVITA")
      if USED("ATTIVITA")
        if this.oParentObject.w_FLDEFI="A"
          * --- Create temporary table PRI_TEMP
          i_nIdx=cp_AddTableDef('PRI_TEMP') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('query\gsar_qti',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.PRI_TEMP_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
        SELECT ATTIVITA
        GO TOP
        SCAN FOR NOT EMPTY(NVL(CODATT," "))
        this.w_ATTIVI = CODATT
        this.w_OKCAL = .F.
        do GSCG_BV2 with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT ATTIVITA
        if USED("CursElab") AND this.oParentObject.w_FLDEFI="A"
          SELECT CursElab
          GO TOP
          SCAN FOR NOT EMPTY(NVL(TIPREG, " ")) AND NVL(NUMREG,0)<>0 AND NOT EMPTY(NVL(CODIVA,""))
          this.w_TIPOPE = TIPOPE
          this.w_TIPREG = TIPREG
          this.w_NUMREG = NUMREG
          this.w_CODIVA = CODIVA
          this.w_CODINT = SPACE(5)
          this.w_IMPSTA = IMPSTA
          this.w_IMPSEG = IMPSEG
          this.w_IMPIND = IMPIND
          this.w_IVASTA = IVASTA
          this.w_IVASEG = IVASEG
          this.w_IVAIND = IVAIND
          this.w_IVISTA = IVISTA
          this.w_IVISEG = IVISEG
          this.w_IVIIND = IVIIND
          this.w_MACSTA = MACSTA
          this.w_MACSEG = MACSEG
          this.w_MACIND = MACIND
          this.w_IMPPRE = IMPPRE
          this.w_IMPFAD = IMPFAD
          this.w_IVAPRE = IVAPRE
          this.w_IVAFAD = IVAFAD
          this.w_IVIPRE = IVIPRE
          this.w_IVIFAD = IVIFAD
          this.w_MACPRE = MACPRE
          this.w_MACFAD = MACFAD
          this.w_CODCON = CODCON
          this.w_TIPDOC = TIPDOC
          this.w_CODCAU = CODCAU
          if this.w_TIPREG="E" AND this.w_RESVEN1<>0 AND this.w_IVASTA<>0
            * --- Eventuale Resto Corrispettivi da Ventilare da Ripartite
            this.w_IVASTA = this.w_IVASTA + this.w_RESVEN1
            this.w_RESVEN1 = 0
          endif
          do case
            case this.w_TIPREG="E"
              this.w_MESS = "Scrittura dettaglio saldi registro IVA: corrisp./ventilazione - num.: %1"
            case this.w_TIPREG="C"
              this.w_MESS = "Scrittura dettaglio saldi registro IVA: corrisp./scorporo - num.: %1"
            case this.w_TIPREG="A"
              this.w_MESS = "Scrittura dettaglio saldi registro IVA: acquisti - num.: %1"
            otherwise
              this.w_MESS = "Scrittura dettaglio saldi registro IVA: vendite - num.: %1"
          endcase
          ah_Msg(this.w_MESS,.T.,.F.,.F.,ALLTR(STR(this.w_NUMREG)))
          if this.w_TIPOPE<>"A" AND this.w_TIPREG="A"
            * --- Solo nel caso di documenti  TIPOPE = 'B' o 'C' si devono creare due record per le Intra UE
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AFFLINTR"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC("F");
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AFFLINTR;
                from (i_cTable) where;
                    ANTIPCON = "F";
                    and ANCODICE = this.w_CODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FLINTR = NVL(cp_ToDate(_read_.AFFLINTR),cp_NullValue(_read_.AFFLINTR))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_FLINTR="S" AND (this.w_TIPDOC="NE" OR this.w_TIPDOC="FE")
              * --- Leggo registro iva per scrittura a pareggio e codice iva associato per Intra
              * --- Read from CAUIVA1
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAUIVA1_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2],.t.,this.CAUIVA1_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "AINUMREG"+;
                  " from "+i_cTable+" CAUIVA1 where ";
                      +"AICODCAU = "+cp_ToStrODBC(this.w_CODCAU);
                      +" and AITIPREG = "+cp_ToStrODBC("V");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  AINUMREG;
                  from (i_cTable) where;
                      AICODCAU = this.w_CODCAU;
                      and AITIPREG = "V";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_NUMREG = NVL(cp_ToDate(_read_.AINUMREG),cp_NullValue(_read_.AINUMREG))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Read from VOCIIVA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VOCIIVA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "IVCODINT"+;
                  " from "+i_cTable+" VOCIIVA where ";
                      +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  IVCODINT;
                  from (i_cTable) where;
                      IVCODIVA = this.w_CODIVA;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODINT = NVL(cp_ToDate(_read_.IVCODINT),cp_NullValue(_read_.IVCODINT))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_CODIVA = IIF(EMPTY(this.w_CODINT), this.w_CODIVA, this.w_CODINT)
              * --- Read from VOCIIVA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VOCIIVA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "IVPERIVA"+;
                  " from "+i_cTable+" VOCIIVA where ";
                      +"IVCODIVA = "+cp_ToStrODBC(this.w_CODINT);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  IVPERIVA;
                  from (i_cTable) where;
                      IVCODIVA = this.w_CODINT;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_PERINT = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_IVASTA = cp_ROUND(((this.w_IMPSTA*this.w_PERINT)/100), IIF(g_PERVAL=g_CODLIR, 0, 2))
              * --- Insert into PRI_TEMP
              i_nConn=i_TableProp[this.PRI_TEMP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRI_TEMP_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRI_TEMP_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"TRTIPOPE"+",TRTIPREG"+",TRNUMREG"+",TRCODIVA"+",TRIMPSTA"+",TRIVASTA"+",TRIVISTA"+",TRMACSTA"+",TRIMPPRE"+",TRIVAPRE"+",TRIVIPRE"+",TRMACPRE"+",TRIMPSEG"+",TRIVASEG"+",TRIVISEG"+",TRMACSEG"+",TRIMPFAD"+",TRIVAFAD"+",TRIVIFAD"+",TRMACFAD"+",TRIMPIND"+",TRIVAIND"+",TRIVIIND"+",TRMACIND"+",TRFLSTAM"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_TIPOPE),'PRI_TEMP','TRTIPOPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREG),'PRI_TEMP','TRTIPREG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_NUMREG),'PRI_TEMP','TRNUMREG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODIVA),'PRI_TEMP','TRCODIVA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSTA),'PRI_TEMP','TRIMPSTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVASTA),'PRI_TEMP','TRIVASTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVISTA),'PRI_TEMP','TRIVISTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACSTA),'PRI_TEMP','TRMACSTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPPRE),'PRI_TEMP','TRIMPPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVAPRE),'PRI_TEMP','TRIVAPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVIPRE),'PRI_TEMP','TRIVIPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACPRE),'PRI_TEMP','TRMACPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSEG),'PRI_TEMP','TRIMPSEG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVASEG),'PRI_TEMP','TRIVASEG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVISEG),'PRI_TEMP','TRIVISEG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACSEG),'PRI_TEMP','TRMACSEG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFAD),'PRI_TEMP','TRIMPFAD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVAFAD),'PRI_TEMP','TRIVAFAD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVIFAD),'PRI_TEMP','TRIVIFAD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACFAD),'PRI_TEMP','TRMACFAD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPIND),'PRI_TEMP','TRIMPIND');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVAIND),'PRI_TEMP','TRIVAIND');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVIIND),'PRI_TEMP','TRIVIIND');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACIND),'PRI_TEMP','TRMACIND');
                +","+cp_NullLink(cp_ToStrODBC("S"),'PRI_TEMP','TRFLSTAM');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'TRTIPOPE',this.w_TIPOPE,'TRTIPREG',this.w_TIPREG,'TRNUMREG',this.w_NUMREG,'TRCODIVA',this.w_CODIVA,'TRIMPSTA',this.w_IMPSTA,'TRIVASTA',this.w_IVASTA,'TRIVISTA',this.w_IVISTA,'TRMACSTA',this.w_MACSTA,'TRIMPPRE',this.w_IMPPRE,'TRIVAPRE',this.w_IVAPRE,'TRIVIPRE',this.w_IVIPRE,'TRMACPRE',this.w_MACPRE)
                insert into (i_cTable) (TRTIPOPE,TRTIPREG,TRNUMREG,TRCODIVA,TRIMPSTA,TRIVASTA,TRIVISTA,TRMACSTA,TRIMPPRE,TRIVAPRE,TRIVIPRE,TRMACPRE,TRIMPSEG,TRIVASEG,TRIVISEG,TRMACSEG,TRIMPFAD,TRIVAFAD,TRIVIFAD,TRMACFAD,TRIMPIND,TRIVAIND,TRIVIIND,TRMACIND,TRFLSTAM &i_ccchkf. );
                   values (;
                     this.w_TIPOPE;
                     ,this.w_TIPREG;
                     ,this.w_NUMREG;
                     ,this.w_CODIVA;
                     ,this.w_IMPSTA;
                     ,this.w_IVASTA;
                     ,this.w_IVISTA;
                     ,this.w_MACSTA;
                     ,this.w_IMPPRE;
                     ,this.w_IVAPRE;
                     ,this.w_IVIPRE;
                     ,this.w_MACPRE;
                     ,this.w_IMPSEG;
                     ,this.w_IVASEG;
                     ,this.w_IVISEG;
                     ,this.w_MACSEG;
                     ,this.w_IMPFAD;
                     ,this.w_IVAFAD;
                     ,this.w_IVIFAD;
                     ,this.w_MACFAD;
                     ,this.w_IMPIND;
                     ,this.w_IVAIND;
                     ,this.w_IVIIND;
                     ,this.w_MACIND;
                     ,"S";
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              this.w_TIPREG = "V"
              * --- Nel caso di Fatture Intra UE devo creare la seconda registrazione sul registro opposto per pareggiare.
              * --- Insert into PRI_TEMP
              i_nConn=i_TableProp[this.PRI_TEMP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRI_TEMP_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRI_TEMP_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"TRTIPOPE"+",TRTIPREG"+",TRNUMREG"+",TRCODIVA"+",TRIMPSTA"+",TRIVASTA"+",TRIVISTA"+",TRMACSTA"+",TRIMPPRE"+",TRIVAPRE"+",TRIVIPRE"+",TRMACPRE"+",TRIMPSEG"+",TRIVASEG"+",TRIVISEG"+",TRMACSEG"+",TRIMPFAD"+",TRIVAFAD"+",TRIVIFAD"+",TRMACFAD"+",TRIMPIND"+",TRIVAIND"+",TRIVIIND"+",TRMACIND"+",TRFLSTAM"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_TIPOPE),'PRI_TEMP','TRTIPOPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREG),'PRI_TEMP','TRTIPREG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_NUMREG),'PRI_TEMP','TRNUMREG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODIVA),'PRI_TEMP','TRCODIVA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSTA),'PRI_TEMP','TRIMPSTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVASTA),'PRI_TEMP','TRIVASTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVISTA),'PRI_TEMP','TRIVISTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACSTA),'PRI_TEMP','TRMACSTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPPRE),'PRI_TEMP','TRIMPPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVAPRE),'PRI_TEMP','TRIVAPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVIPRE),'PRI_TEMP','TRIVIPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACPRE),'PRI_TEMP','TRMACPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSEG),'PRI_TEMP','TRIMPSEG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVASEG),'PRI_TEMP','TRIVASEG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVISEG),'PRI_TEMP','TRIVISEG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACSEG),'PRI_TEMP','TRMACSEG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFAD),'PRI_TEMP','TRIMPFAD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVAFAD),'PRI_TEMP','TRIVAFAD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVIFAD),'PRI_TEMP','TRIVIFAD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACFAD),'PRI_TEMP','TRMACFAD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPIND),'PRI_TEMP','TRIMPIND');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVAIND),'PRI_TEMP','TRIVAIND');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVIIND),'PRI_TEMP','TRIVIIND');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACIND),'PRI_TEMP','TRMACIND');
                +","+cp_NullLink(cp_ToStrODBC("S"),'PRI_TEMP','TRFLSTAM');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'TRTIPOPE',this.w_TIPOPE,'TRTIPREG',this.w_TIPREG,'TRNUMREG',this.w_NUMREG,'TRCODIVA',this.w_CODIVA,'TRIMPSTA',this.w_IMPSTA,'TRIVASTA',this.w_IVASTA,'TRIVISTA',this.w_IVISTA,'TRMACSTA',this.w_MACSTA,'TRIMPPRE',this.w_IMPPRE,'TRIVAPRE',this.w_IVAPRE,'TRIVIPRE',this.w_IVIPRE,'TRMACPRE',this.w_MACPRE)
                insert into (i_cTable) (TRTIPOPE,TRTIPREG,TRNUMREG,TRCODIVA,TRIMPSTA,TRIVASTA,TRIVISTA,TRMACSTA,TRIMPPRE,TRIVAPRE,TRIVIPRE,TRMACPRE,TRIMPSEG,TRIVASEG,TRIVISEG,TRMACSEG,TRIMPFAD,TRIVAFAD,TRIVIFAD,TRMACFAD,TRIMPIND,TRIVAIND,TRIVIIND,TRMACIND,TRFLSTAM &i_ccchkf. );
                   values (;
                     this.w_TIPOPE;
                     ,this.w_TIPREG;
                     ,this.w_NUMREG;
                     ,this.w_CODIVA;
                     ,this.w_IMPSTA;
                     ,this.w_IVASTA;
                     ,this.w_IVISTA;
                     ,this.w_MACSTA;
                     ,this.w_IMPPRE;
                     ,this.w_IVAPRE;
                     ,this.w_IVIPRE;
                     ,this.w_MACPRE;
                     ,this.w_IMPSEG;
                     ,this.w_IVASEG;
                     ,this.w_IVISEG;
                     ,this.w_MACSEG;
                     ,this.w_IMPFAD;
                     ,this.w_IVAFAD;
                     ,this.w_IVIFAD;
                     ,this.w_MACFAD;
                     ,this.w_IMPIND;
                     ,this.w_IVAIND;
                     ,this.w_IVIIND;
                     ,this.w_MACIND;
                     ,"S";
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            else
              * --- Derivanti da Primanota: nel caso di Intra ci sono gi� le due scritture a pareggio
              * --- Insert into PRI_TEMP
              i_nConn=i_TableProp[this.PRI_TEMP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRI_TEMP_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRI_TEMP_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"TRTIPOPE"+",TRTIPREG"+",TRNUMREG"+",TRCODIVA"+",TRIMPSTA"+",TRIVASTA"+",TRIVISTA"+",TRMACSTA"+",TRIMPPRE"+",TRIVAPRE"+",TRIVIPRE"+",TRMACPRE"+",TRIMPSEG"+",TRIVASEG"+",TRIVISEG"+",TRMACSEG"+",TRIMPFAD"+",TRIVAFAD"+",TRIVIFAD"+",TRMACFAD"+",TRIMPIND"+",TRIVAIND"+",TRIVIIND"+",TRMACIND"+",TRFLSTAM"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_TIPOPE),'PRI_TEMP','TRTIPOPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREG),'PRI_TEMP','TRTIPREG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_NUMREG),'PRI_TEMP','TRNUMREG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODIVA),'PRI_TEMP','TRCODIVA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSTA),'PRI_TEMP','TRIMPSTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVASTA),'PRI_TEMP','TRIVASTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVISTA),'PRI_TEMP','TRIVISTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACSTA),'PRI_TEMP','TRMACSTA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPPRE),'PRI_TEMP','TRIMPPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVAPRE),'PRI_TEMP','TRIVAPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVIPRE),'PRI_TEMP','TRIVIPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACPRE),'PRI_TEMP','TRMACPRE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSEG),'PRI_TEMP','TRIMPSEG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVASEG),'PRI_TEMP','TRIVASEG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVISEG),'PRI_TEMP','TRIVISEG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACSEG),'PRI_TEMP','TRMACSEG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFAD),'PRI_TEMP','TRIMPFAD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVAFAD),'PRI_TEMP','TRIVAFAD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVIFAD),'PRI_TEMP','TRIVIFAD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACFAD),'PRI_TEMP','TRMACFAD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IMPIND),'PRI_TEMP','TRIMPIND');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVAIND),'PRI_TEMP','TRIVAIND');
                +","+cp_NullLink(cp_ToStrODBC(this.w_IVIIND),'PRI_TEMP','TRIVIIND');
                +","+cp_NullLink(cp_ToStrODBC(this.w_MACIND),'PRI_TEMP','TRMACIND');
                +","+cp_NullLink(cp_ToStrODBC("S"),'PRI_TEMP','TRFLSTAM');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'TRTIPOPE',this.w_TIPOPE,'TRTIPREG',this.w_TIPREG,'TRNUMREG',this.w_NUMREG,'TRCODIVA',this.w_CODIVA,'TRIMPSTA',this.w_IMPSTA,'TRIVASTA',this.w_IVASTA,'TRIVISTA',this.w_IVISTA,'TRMACSTA',this.w_MACSTA,'TRIMPPRE',this.w_IMPPRE,'TRIVAPRE',this.w_IVAPRE,'TRIVIPRE',this.w_IVIPRE,'TRMACPRE',this.w_MACPRE)
                insert into (i_cTable) (TRTIPOPE,TRTIPREG,TRNUMREG,TRCODIVA,TRIMPSTA,TRIVASTA,TRIVISTA,TRMACSTA,TRIMPPRE,TRIVAPRE,TRIVIPRE,TRMACPRE,TRIMPSEG,TRIVASEG,TRIVISEG,TRMACSEG,TRIMPFAD,TRIVAFAD,TRIVIFAD,TRMACFAD,TRIMPIND,TRIVAIND,TRIVIIND,TRMACIND,TRFLSTAM &i_ccchkf. );
                   values (;
                     this.w_TIPOPE;
                     ,this.w_TIPREG;
                     ,this.w_NUMREG;
                     ,this.w_CODIVA;
                     ,this.w_IMPSTA;
                     ,this.w_IVASTA;
                     ,this.w_IVISTA;
                     ,this.w_MACSTA;
                     ,this.w_IMPPRE;
                     ,this.w_IVAPRE;
                     ,this.w_IVIPRE;
                     ,this.w_MACPRE;
                     ,this.w_IMPSEG;
                     ,this.w_IVASEG;
                     ,this.w_IVISEG;
                     ,this.w_MACSEG;
                     ,this.w_IMPFAD;
                     ,this.w_IVAFAD;
                     ,this.w_IVIFAD;
                     ,this.w_MACFAD;
                     ,this.w_IMPIND;
                     ,this.w_IVAIND;
                     ,this.w_IVIIND;
                     ,this.w_MACIND;
                     ,"S";
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          else
            * --- Insert into PRI_TEMP
            i_nConn=i_TableProp[this.PRI_TEMP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRI_TEMP_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRI_TEMP_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"TRTIPOPE"+",TRTIPREG"+",TRNUMREG"+",TRCODIVA"+",TRIMPSTA"+",TRIVASTA"+",TRIVISTA"+",TRMACSTA"+",TRIMPPRE"+",TRIVAPRE"+",TRIVIPRE"+",TRMACPRE"+",TRIMPSEG"+",TRIVASEG"+",TRIVISEG"+",TRMACSEG"+",TRIMPFAD"+",TRIVAFAD"+",TRIVIFAD"+",TRMACFAD"+",TRIMPIND"+",TRIVAIND"+",TRIVIIND"+",TRMACIND"+",TRFLSTAM"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_TIPOPE),'PRI_TEMP','TRTIPOPE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREG),'PRI_TEMP','TRTIPREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMREG),'PRI_TEMP','TRNUMREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODIVA),'PRI_TEMP','TRCODIVA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSTA),'PRI_TEMP','TRIMPSTA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVASTA),'PRI_TEMP','TRIVASTA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVISTA),'PRI_TEMP','TRIVISTA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACSTA),'PRI_TEMP','TRMACSTA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IMPPRE),'PRI_TEMP','TRIMPPRE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVAPRE),'PRI_TEMP','TRIVAPRE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVIPRE),'PRI_TEMP','TRIVIPRE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACPRE),'PRI_TEMP','TRMACPRE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSEG),'PRI_TEMP','TRIMPSEG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVASEG),'PRI_TEMP','TRIVASEG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVISEG),'PRI_TEMP','TRIVISEG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACSEG),'PRI_TEMP','TRMACSEG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IMPFAD),'PRI_TEMP','TRIMPFAD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVAFAD),'PRI_TEMP','TRIVAFAD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVIFAD),'PRI_TEMP','TRIVIFAD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACFAD),'PRI_TEMP','TRMACFAD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IMPIND),'PRI_TEMP','TRIMPIND');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVAIND),'PRI_TEMP','TRIVAIND');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVIIND),'PRI_TEMP','TRIVIIND');
              +","+cp_NullLink(cp_ToStrODBC(this.w_MACIND),'PRI_TEMP','TRMACIND');
              +","+cp_NullLink(cp_ToStrODBC("S"),'PRI_TEMP','TRFLSTAM');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'TRTIPOPE',this.w_TIPOPE,'TRTIPREG',this.w_TIPREG,'TRNUMREG',this.w_NUMREG,'TRCODIVA',this.w_CODIVA,'TRIMPSTA',this.w_IMPSTA,'TRIVASTA',this.w_IVASTA,'TRIVISTA',this.w_IVISTA,'TRMACSTA',this.w_MACSTA,'TRIMPPRE',this.w_IMPPRE,'TRIVAPRE',this.w_IVAPRE,'TRIVIPRE',this.w_IVIPRE,'TRMACPRE',this.w_MACPRE)
              insert into (i_cTable) (TRTIPOPE,TRTIPREG,TRNUMREG,TRCODIVA,TRIMPSTA,TRIVASTA,TRIVISTA,TRMACSTA,TRIMPPRE,TRIVAPRE,TRIVIPRE,TRMACPRE,TRIMPSEG,TRIVASEG,TRIVISEG,TRMACSEG,TRIMPFAD,TRIVAFAD,TRIVIFAD,TRMACFAD,TRIMPIND,TRIVAIND,TRIVIIND,TRMACIND,TRFLSTAM &i_ccchkf. );
                 values (;
                   this.w_TIPOPE;
                   ,this.w_TIPREG;
                   ,this.w_NUMREG;
                   ,this.w_CODIVA;
                   ,this.w_IMPSTA;
                   ,this.w_IVASTA;
                   ,this.w_IVISTA;
                   ,this.w_MACSTA;
                   ,this.w_IMPPRE;
                   ,this.w_IVAPRE;
                   ,this.w_IVIPRE;
                   ,this.w_MACPRE;
                   ,this.w_IMPSEG;
                   ,this.w_IVASEG;
                   ,this.w_IVISEG;
                   ,this.w_MACSEG;
                   ,this.w_IMPFAD;
                   ,this.w_IVAFAD;
                   ,this.w_IVIFAD;
                   ,this.w_MACFAD;
                   ,this.w_IMPIND;
                   ,this.w_IVAIND;
                   ,this.w_IVIIND;
                   ,this.w_MACIND;
                   ,"S";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          SELECT CursElab
          ENDSCAN
        endif
        if this.w_OKCAL=.F.
          ah_ErrorMsg("Impossibile aggiornare saldi IVA; elaborazione interrotta.",,"")
          EXIT
        endif
        SELECT ATTIVITA
        ENDSCAN
        if USED("ATTIVITA")
          SELECT ATTIVITA
          USE
        endif
      endif
    endif
    if this.w_OKCAL=.T.
      this.w_COFAZI = this.oParentObject.w_MCOFAZI
      this.w_PIVAZI = this.oParentObject.w_MPIVAZI
      this.w_TIPLIQ = this.oParentObject.w_MTIPLIQ
      this.w_F2DEFI = this.oParentObject.w_FLDEFI
      this.w_TIPDIC = this.oParentObject.w_MTIPDIC
      this.w_FLTEST = IIF(VAL(this.oParentObject.w_ANNO)>2001, this.oParentObject.w_MFLTEST, " ")
      this.w_VPDICSOC = IIF(this.w_TIPDIC="S", "S", " ")
      this.w_VPDICGRU = IIF(this.w_TIPDIC="G", "S", " ")
      this.w_VPDATVER = i_DATSYS
      this.w_VPCODCAR = IIF(VAL(this.oParentObject.w_IVACAR)>9, " ", ALLTRIM(this.oParentObject.w_IVACAR))
      this.w_VPCODFIS = this.oParentObject.w_IVACOF
      this.w_VPCODAZI = SPACE(5)
      this.w_VPCODCAB = SPACE(5)
      * --- Tutti gli Importi della Dichiarazione vanno riferiti alla stessa Valuta (Lire o Euro)
      this.w_VPCODVAL = IIF(g_PERVAL=g_CODEUR, "S", "N")
      this.w_VPEURO19 = IIF(g_PERVAL=g_CODEUR, "S", "N")
      this.w_VPVEREUR = IIF(g_PERVAL=g_CODEUR, "S", " ")
      this.w_VPVARIMP = " "
      this.w_VPVERNEF = " "
      this.w_VPCORTER = " "
      this.w_VPAR74C5 = " "
      this.w_VPAR74C4 = " "
      this.w_VPOPMEDI = " "
      this.w_VPOPNOIM = " "
      this.w_VPACQBEA = " "
      this.w_VPDATPRE = cp_CharToDate("  -  -  ")
      this.w_VP__ANNO = this.oParentObject.w_ANNO
      this.w_VPPERIOD = this.oParentObject.w_NUMPER
      this.w_VPCODATT = IIF(this.w_TIPLIQ="R", "     ", this.oParentObject.w_CODATT)
      this.w_VPKEYATT = IIF(this.w_TIPLIQ="R", "#####", this.oParentObject.w_CODATT)
      this.w_MAGTRI = this.oParentObject.w_DENMAG
      this.w_CONCES = this.oParentObject.w_MCONCES
      * --- Crea il Cursore per la Liquidazione IVA
      CREATE CURSOR LIQIVA (CODATT C(5), DESCRI C(65), TIPOPE C(1), TIPREG C(1), NUMREG N(2,0), CODIVA C(5), ;
      DESIVA C(35),TOTIMP N(18,4), TOTIVD N(18,4), TOTIVI N(18,4), TOTMAC N(18,4), PERMAC N(10,6), ;
      FLAGRI C(1), PERCOM N(5,2), TIPAGR C(1), PERPRO N(5,2), FLPROR C(1),IVSPLPAY C(1)) 
      * --- Credito Residuo
      this.w_CRERES = 0
      this.w_CREVAL = g_PERVAL
      * --- Read from CREDMIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CREDMIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CREDMIVA_idx,2],.t.,this.CREDMIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CICREINI,CIVALINI"+;
          " from "+i_cTable+" CREDMIVA where ";
              +"CI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CICREINI,CIVALINI;
          from (i_cTable) where;
              CI__ANNO = this.oParentObject.w_ANNO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CRERES = NVL(cp_ToDate(_read_.CICREINI),cp_NullValue(_read_.CICREINI))
        this.w_CREVAL = NVL(cp_ToDate(_read_.CIVALINI),cp_NullValue(_read_.CIVALINI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Storna gli Utilizzi dell'Anno
      * --- Select from CREDDIVA
      i_nConn=i_TableProp[this.CREDDIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CREDDIVA_idx,2],.t.,this.CREDDIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CICREDET,CICREF24,CICRERIM  from "+i_cTable+" CREDDIVA ";
            +" where CI__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+" AND CINUMPER<="+cp_ToStrODBC(this.oParentObject.w_NUMPER)+"";
             ,"_Curs_CREDDIVA")
      else
        select CICREDET,CICREF24,CICRERIM from (i_cTable);
         where CI__ANNO=this.oParentObject.w_ANNO AND CINUMPER<=this.oParentObject.w_NUMPER;
          into cursor _Curs_CREDDIVA
      endif
      if used('_Curs_CREDDIVA')
        select _Curs_CREDDIVA
        locate for 1=1
        do while not(eof())
        this.w_CRERES = this.w_CRERES - (NVL(_Curs_CREDDIVA.CICREDET,0)+NVL(_Curs_CREDDIVA.CICREF24,0)+NVL(_Curs_CREDDIVA.CICRERIM,0))
          select _Curs_CREDDIVA
          continue
        enddo
        use
      endif
      * --- Arrotondo ai decimali della valuta per problema decimali oltre la sesta posizione
      this.w_CRERES = cp_Round( IIF(this.w_CRERES<0, 0, this.w_CRERES), g_PERPVL)
      * --- Se La valuta dell'esercizio diversa da quella dei Crediti converte
      * --- Prevediamo solo LIRE o EURO
      if this.w_CRERES<>0 AND this.w_CREVAL<>g_PERVAL AND g_CAOEUR<>0 
        do case
          case this.w_CREVAL=g_CODLIR
            this.w_CRERES = cp_ROUND(this.w_CRERES/g_CAOEUR, g_PERPVL)
          case this.w_CREVAL=g_CODEUR
            this.w_CRERES = cp_ROUND(this.w_CRERES*g_CAOEUR, g_PERPVL)
        endcase
      endif
      * --- Dati della Precedente Dichiarazione
      this.w_PRIMPO12 = 0
      this.w_PRIMPO16 = 0
      this.w_PRCRERIM = 0
      this.w_PRCREUTI = 0
      if this.w_VPPERIOD>1
        this.w_OLDPER = this.w_VPPERIOD - 1
        this.w_FLVAL = "N"
        * --- Read from IVA_PERI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.IVA_PERI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2],.t.,this.IVA_PERI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VPCODVAL,VPIMPO12,VPIMPO16,VPCRERIM,VPCREUTI"+;
            " from "+i_cTable+" IVA_PERI where ";
                +"VP__ANNO = "+cp_ToStrODBC(this.w_VP__ANNO);
                +" and VPPERIOD = "+cp_ToStrODBC(this.w_OLDPER);
                +" and VPKEYATT = "+cp_ToStrODBC(this.w_VPKEYATT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VPCODVAL,VPIMPO12,VPIMPO16,VPCRERIM,VPCREUTI;
            from (i_cTable) where;
                VP__ANNO = this.w_VP__ANNO;
                and VPPERIOD = this.w_OLDPER;
                and VPKEYATT = this.w_VPKEYATT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLVAL = NVL(cp_ToDate(_read_.VPCODVAL),cp_NullValue(_read_.VPCODVAL))
          this.w_PRIMPO12 = NVL(cp_ToDate(_read_.VPIMPO12),cp_NullValue(_read_.VPIMPO12))
          this.w_PRIMPO16 = NVL(cp_ToDate(_read_.VPIMPO16),cp_NullValue(_read_.VPIMPO16))
          this.w_PRCRERIM = NVL(cp_ToDate(_read_.VPCRERIM),cp_NullValue(_read_.VPCRERIM))
          this.w_PRCREUTI = NVL(cp_ToDate(_read_.VPCREUTI),cp_NullValue(_read_.VPCREUTI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if ((this.w_FLVAL="S" AND g_PERVAL=g_CODLIR) OR (this.w_FLVAL<>"S" AND g_PERVAL<>g_CODLIR)) AND g_CAOEUR<>0
          * --- Importi Precedente dichiarazione in altra Valuta
          if this.w_FLVAL="S"
            * --- Importi Precedenti espressi in Euro ; Vanno riferiti alle KLire 
            * --- (Situazione gestita ma Improbabile di Passagio da Euro a Lire!!)
            this.w_PRIMPO12 = INTROUND(this.w_PRIMPO12*g_CAOEUR, 1000)/1000
            this.w_PRIMPO16 = INTROUND(this.w_PRIMPO16*g_CAOEUR, 1000)/1000
            this.w_PRCRERIM = INTROUND(this.w_PRCRERIM*g_CAOEUR, 1000)/1000
            this.w_PRCREUTI = INTROUND(this.w_PRCREUTI*g_CAOEUR, 1000)/1000
          else
            * --- Importi Precedenti espressi in KLire ; Vanno riferiti all' Euro
            this.w_PRIMPO12 = cp_ROUND((this.w_PRIMPO12*1000)/g_CAOEUR, 2)
            this.w_PRIMPO16 = cp_ROUND((this.w_PRIMPO16*1000)/g_CAOEUR, 2)
            this.w_PRCRERIM = cp_ROUND((this.w_PRCRERIM*1000)/g_CAOEUR, 2)
            this.w_PRCREUTI = cp_ROUND((this.w_PRCREUTI*1000)/g_CAOEUR, 2)
          endif
        endif
      endif
      * --- Plafond
      L_PLAVAR = " "
      L_PLADIS = 0
      L_PLAUTI = 0
      L_PLARES = 0
      L_PLADIS = 0
      * --- Importi definiti in KiloLire o all'Unita' di Euro
      this.w_APPO1 = IIF(g_PERVAL=g_CODLIR, 1000, IIF(VAL(this.oParentObject.w_ANNO)>2001, 0, 1))
      this.w_APPO = IIF(g_PERVAL=g_CODLIR, 1000, 1)
      this.w_MINACC = (INTROUND(this.w_MINACC, this.w_APPO1) / this.w_APPO)
      this.w_MINVER = (INTROUND(this.w_MINVER, this.w_APPO1) / this.w_APPO)
      this.w_CRERES = (INTROUND(this.w_CRERES, this.w_APPO1) / this.w_APPO)
      * --- Inizia Elaborazione ,Cicla sulle Attivita' (Quella Selezionata o Tutte se Liquidazione Riepilogativa)
      this.w_KEYATT = IIF(this.w_TIPLIQ="R", "     ", this.oParentObject.w_CODATT)
      VQ_EXEC("QUERY\GSCG1BLP.VQR",this,"ATTIVITA")
      if USED("ATTIVITA")
        * --- Try
        local bErr_0462DE30
        bErr_0462DE30=bTrsErr
        this.Try_0462DE30()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if NOT EMPTY(i_errmsg)
            ah_ErrorMsg(i_errmsg,,"")
          else
            ah_ErrorMsg("Errore durante elaborazione. Operazione abbandonata",,"")
          endif
        endif
        bTrsErr=bTrsErr or bErr_0462DE30
        * --- End
      endif
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    if USED("ATTIVITA")
      SELECT ATTIVITA
      USE
    endif
    if USED("SALDIIVA")
      SELECT SALDIIVA
      USE
    endif
    if USED("CursElab")
      SELECT CURSELAB
      USE
    endif
    * --- Nel caso di Acconto IVA viene chiuso nel GSCG_BAI
    if USED("LIQIVA") AND this.oParentObject.w_FLDEFI<>"A"
      SELECT LIQIVA
      USE
    endif
    * --- Toglie Blocchi per Primanota e Attivita'
    * --- Try
    local bErr_04632960
    bErr_04632960=bTrsErr
    this.Try_04632960()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Impossibile rimuovere le date di blocco presenti in primanota e registri IVA.%0Provvedere a rimuoverle manualmente dai dati azienda e attivit�",,"")
    endif
    bTrsErr=bTrsErr or bErr_04632960
    * --- End
  endproc
  proc Try_045E7650()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_DATBLO)
      * --- Verifica le Attivita'
      this.w_DATBLO2 = cp_CharToDate("  -  -  ")
      if this.oParentObject.w_MTIPLIQ="R"
        * --- Se Riepilogativa; Blocca Tutti i Registri
        * --- Select from ATTIDETT
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIDETT ";
              +" where ATNUMREG>0";
               ,"_Curs_ATTIDETT")
        else
          select * from (i_cTable);
           where ATNUMREG>0;
            into cursor _Curs_ATTIDETT
        endif
        if used('_Curs_ATTIDETT')
          select _Curs_ATTIDETT
          locate for 1=1
          do while not(eof())
          this.w_DATBLO2 = IIF(EMPTY(CP_TODATE(_Curs_ATTIDETT.ATDATBLO)), this.w_DATBLO2, CP_TODATE(_Curs_ATTIDETT.ATDATBLO))
            select _Curs_ATTIDETT
            continue
          enddo
          use
        endif
      else
        * --- Select from ATTIDETT
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIDETT ";
              +" where ATCODATT="+cp_ToStrODBC(this.oParentObject.w_CODATT)+"";
               ,"_Curs_ATTIDETT")
        else
          select * from (i_cTable);
           where ATCODATT=this.oParentObject.w_CODATT;
            into cursor _Curs_ATTIDETT
        endif
        if used('_Curs_ATTIDETT')
          select _Curs_ATTIDETT
          locate for 1=1
          do while not(eof())
          this.w_DATBLO2 = IIF(EMPTY(CP_TODATE(_Curs_ATTIDETT.ATDATBLO)), this.w_DATBLO2, CP_TODATE(_Curs_ATTIDETT.ATDATBLO))
            select _Curs_ATTIDETT
            continue
          enddo
          use
        endif
      endif
      if EMPTY(this.w_DATBLO2) 
        * --- Inserisce <Blocco> per Primanota
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_DATFI2),'AZIENDA','AZDATBLO');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZDATBLO = this.w_DATFI2;
              &i_ccchkf. ;
           where;
              AZCODAZI = this.oParentObject.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.oParentObject.w_MTIPLIQ="R"
          * --- Se Riepilogativa, Inserisce <Blocco> in Tutte le Attivita'
          this.w_APPO = "V"
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_DATFI2),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_DATFI2;
                &i_ccchkf. ;
             where;
                ATTIPREG = this.w_APPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_APPO = "A"
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_DATFI2),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_DATFI2;
                &i_ccchkf. ;
             where;
                ATTIPREG = this.w_APPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_APPO = "C"
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_DATFI2),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_DATFI2;
                &i_ccchkf. ;
             where;
                ATTIPREG = this.w_APPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_APPO = "E"
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_DATFI2),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_DATFI2;
                &i_ccchkf. ;
             where;
                ATTIPREG = this.w_APPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Inserisce <Blocco> per Attivita'
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_DATFI2),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_DATFI2;
                &i_ccchkf. ;
             where;
                ATCODATT = this.oParentObject.w_CODATT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      else
        if NOT EMPTY(this.w_DATBLO2)
          this.w_MESS = "Archivio attivit� bloccato - verificare semaforo bollati in attivita"
        endif
        * --- Raise
        i_Error=this.w_MESS
        return
        this.oParentObject.w_STOPELA = .F.
      endif
    else
      * --- Raise
      i_Error="Prima nota bloccata - verificare semaforo bollati in dati azienda"
      return
      this.oParentObject.w_STOPELA = .F.
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0462DE30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_CONFER = " "
    this.w_NUMOPE = 1
    * --- Azzera gli Importi IVA Debito/Credito
    this.w_VPIMPO7A = 0
    this.w_VPIMPO7B = 0
    this.w_VPIMPO7C = 0
    do while this.w_NUMOPE<= IIF(this.oParentObject.w_FLDEFI="A", 3, 1)
      this.w_TIPOPE = IIF(this.w_NUMOPE=1, "A", IIF(this.w_NUMOPE=2, "B", "C"))
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_NUMOPE = this.w_NUMOPE + 1
      if USED("SALDIIVA")
        SELECT SALDIIVA
        USE
      endif
    enddo
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_FLDEFI="A"
      * --- Generata in GSCG_BV2
      this.oParentObject.w_FLPROR=this.w_FLPROR
      this.oParentObject.w_PERPRO=this.w_PERPRO
      this.oParentObject.w_FLAGRI=this.w_FLAGRI
      this.oParentObject.w_VIMPOR6 = this.w_VIMPOR6
      this.oParentObject.w_VPIMPO7A = this.w_VPIMPO7A
      this.oParentObject.w_VPIMPO7B = this.w_VPIMPO7B
      this.oParentObject.w_VPIMPO7C = this.w_VPIMPO7C
      this.oParentObject.w_VIMPOR8 = this.w_VIMPOR8
      this.oParentObject.w_VIMPOR9 = this.w_VIMPOR9
      this.oParentObject.w_VIMPO10 = this.w_VIMPO10
      this.oParentObject.w_VIMPO11 = this.w_VIMPO11
      this.oParentObject.w_VIMPO12= this.w_VIMPO12
      this.oParentObject.w_VIMPO13 = this.w_VIMPO13
      this.oParentObject.w_VIMPO15 = this.w_VIMPO15
      this.oParentObject.w_VIMPO16 = this.w_VIMPO16
      this.oParentObject.w_VDICSOC = this.w_VPDICSOC
      this.oParentObject.w_CRERES = this.w_CRERES
    else
      * --- Aggiorna Archivi se Liquidazione Definitiva
      if this.oParentObject.w_FLDEFI="S" AND this.w_CONFER="S"
        * --- Try
        local bErr_0460B5E0
        bErr_0460B5E0=bTrsErr
        this.Try_0460B5E0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          if NOT EMPTY(i_errmsg)
            ah_ErrorMsg(i_errmsg,,"")
          else
            ah_ErrorMsg("Errore durante elaborazione. Operazione abbandonata",,"")
          endif
        endif
        bTrsErr=bTrsErr or bErr_0460B5E0
        * --- End
      endif
    endif
    if this.oParentObject.w_FLDEFI="S" AND this.w_CONFER="S" AND (this.w_TOTCOV<>0 OR this.oParentObject.w_NEWIVA="S") AND (g_ATTIVI<>"S" OR (g_ATTIVI="S" AND this.oParentObject.w_MTIPLIQ="R"))
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_FLDEFI="S"
      if this.w_CONFER="S"
        ah_ErrorMsg("Elaborazione completata",,"")
      else
        ah_ErrorMsg("Elaborazione interrotta",,"")
      endif
    endif
    return
  proc Try_04632960()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_TIPLIQ="R"
      * --- Se Riepilogativa, Inserisce <Blocco> in Tutte le Attivita'
      this.w_APPO = "V"
      * --- Write into ATTIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
            +i_ccchkf ;
        +" where ";
            +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
               )
      else
        update (i_cTable) set;
            ATDATBLO = cp_CharToDate("  -  -  ");
            &i_ccchkf. ;
         where;
            ATTIPREG = this.w_APPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_APPO = "A"
      * --- Write into ATTIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
            +i_ccchkf ;
        +" where ";
            +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
               )
      else
        update (i_cTable) set;
            ATDATBLO = cp_CharToDate("  -  -  ");
            &i_ccchkf. ;
         where;
            ATTIPREG = this.w_APPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_APPO = "C"
      * --- Write into ATTIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
            +i_ccchkf ;
        +" where ";
            +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
               )
      else
        update (i_cTable) set;
            ATDATBLO = cp_CharToDate("  -  -  ");
            &i_ccchkf. ;
         where;
            ATTIPREG = this.w_APPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_APPO = "E"
      * --- Write into ATTIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
            +i_ccchkf ;
        +" where ";
            +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
               )
      else
        update (i_cTable) set;
            ATDATBLO = cp_CharToDate("  -  -  ");
            &i_ccchkf. ;
         where;
            ATTIPREG = this.w_APPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Inserisce <Blocco> per Attivita'
      * --- Write into ATTIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
            +i_ccchkf ;
        +" where ";
            +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
               )
      else
        update (i_cTable) set;
            ATDATBLO = cp_CharToDate("  -  -  ");
            &i_ccchkf. ;
         where;
            ATCODATT = this.oParentObject.w_CODATT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return
  proc Try_0460B5E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cicla Sui Cursori delle Queries e Calcola i valori per il Temporaneo di Elaborazione
    this.w_TOTCOV = 0
    this.w_VPIMPOR1 = 0
    this.w_VPIMPOR2 = 0
    this.w_VPCESINT = 0
    this.w_VPACQINT = 0
    this.w_VPIMPON3 = 0
    this.w_VPIMPOS3 = 0
    this.w_VPIMPOR5 = 0
    this.w_VPIMPOR6 = 0
    this.w_VPIMPOR6C = 0
    this.w_VPIMPOR6A = 0
    this.w_VPIMPOR7 = 0
    this.w_VPIMPOR8 = 0
    this.w_VPIMPOR9 = 0
    this.w_VPIMPO10 = 0
    this.w_VPIMPO11 = 0
    this.w_VPIMPO12 = 0
    this.w_VPIMPO13 = 0
    this.w_VPIMPO14 = 0
    this.w_VPIMPO15 = 0
    this.w_VPIMPO16 = 0
    this.w_VPIMPVER = 0
    this.w_VPCRERIM = 0
    this.w_VPCREUTI = 0
    * --- Dichiaro variabili per calcolo totale IVA e totale prorate nel caso di calcolo Riepilogativa
    * --- Nel caso di liquidazione lanciata da Acconto Iva devo eseguire l'operazione 3 volte
    this.w_ALIAGR = 0
    this.w_TOTATTIVA = 0
    this.w_TOTATTPRO = 0
    SELECT ATTIVITA
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODATT," "))
    * --- %Prorata (Attenzione: se Dichiarazione Riepilogo ricalcola per le Attivita')
    this.w_KEYATT = CODATT
    this.w_FLPROR = NVL(PERPRO," ")
    this.w_FLAGRI = NVL(FLAGRI, " ")
    this.w_PERPRO = 0
    this.w_IMPOR1 = 0
    this.w_CESINT = 0
    this.w_IMPOR2 = 0
    this.w_ACQINT = 0
    this.w_IMPON3 = 0
    this.w_IMPOS3 = 0
    this.w_IMPOR6 = 0
    this.w_IMPOR6C = 0
    this.w_IMPOR6A = 0
    this.w_IMPOR6B = 0
    this.w_IMPOR5 = 0
    this.w_ALIAGR = 0
    this.w_TESTPROR = "N"
    if this.w_FLPROR="S"
      * --- Read from PRO_RATA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRO_RATA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRO_RATA_idx,2],.t.,this.PRO_RATA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AIPERPRO"+;
          " from "+i_cTable+" PRO_RATA where ";
              +"AICODATT = "+cp_ToStrODBC(this.w_KEYATT);
              +" and AI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AIPERPRO;
          from (i_cTable) where;
              AICODATT = this.w_KEYATT;
              and AI__ANNO = this.oParentObject.w_ANNO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERPRO = NVL(cp_ToDate(_read_.AIPERPRO),cp_NullValue(_read_.AIPERPRO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows=0
        * --- Se non trova prorata relativo all'anno lo metto a 100 cos� rimane invariato.
        this.w_PERPRO = 100
      else
        this.w_TESTPROR = "S"
      endif
    else
      this.w_PERPRO = 100
    endif
    * --- Azzera dettagli delle Singole Attivita'
    if this.oParentObject.w_FLDEFI="A"
      VQ_EXEC("QUERY\GSCG_BLT.VQR",this,"SALDIIVA")
    else
      VQ_EXEC("QUERY\GSCG_BLP.VQR",this,"SALDIIVA")
    endif
    if USED("SALDIIVA")
      SELECT SALDIIVA
      GO TOP
      SCAN FOR NVL(TIPREG," ") $ "VACE" AND NVL(NUMREG, 0)<>0 AND NOT EMPTY(NVL(CODIVA, " "))
      this.w_IVSPLPAY = Nvl(SPLPAY," ")
      * --- Per Dettagli Liquidazione
      this.w_DESCRI = DESCRI
      this.w_TIPREG = TIPREG
      this.w_NUMREG = NUMREG
      this.w_CODIVA = CODIVA
      this.w_DESIVA = NVL(DESIVA, " ")
      this.w_PERCOM = NVL(PERCOM, 0)
      this.w_TIPAGR = NVL(TIPAGR, "N")
      this.w_TOTIMP = (NVL(IMPSTA,0)+NVL(IMPSEG,0)+NVL(IMPIND,0))-(NVL(IMPPRE,0)+NVL(IMPFAD,0))
      * --- Non valorizzo imposta se registro acquisti e codice iva escluso e gestito monteacquisti
      if this.w_TIPREG="E"
        this.w_TOTIVD = (NVL(IVASTA,0)+NVL(IVASEG,0))-NVL(IVAPRE,0)
        this.w_TOTIVI = (NVL(IVISTA,0)+NVL(IVISEG,0))-NVL(IVIPRE,0)
        this.w_TOTMAC = (NVL(MACSTA,0)+NVL(MACSEG,0))-NVL(MACPRE,0)
      else
        this.w_TOTIVD = IIF( (NVL(FLVAFF,"N") <>"N" AND NVL(MACIVA,"N") ="S" AND TIPREG="A"),0,(NVL(IVASTA,0)+NVL(IVASEG,0)+NVL(IVAIND,0))-(NVL(IVAPRE,0)+NVL(IVAFAD,0)))
        this.w_TOTIVI = (NVL(IVISTA,0)+NVL(IVISEG,0)+NVL(IVIIND,0))-(NVL(IVIPRE,0)+NVL(IVIFAD,0))
        this.w_TOTMAC = (NVL(MACSTA,0)+NVL(MACSEG,0)+NVL(MACIND,0))-(NVL(MACPRE,0)+NVL(MACFAD,0))
      endif
      this.w_PERMAC = 0
      * --- Se Definitiva Aggiorna Totali per Giroconto IVA Ventilata (Se Riepilogativa)
      if this.oParentObject.w_FLDEFI="S" AND this.w_TIPREG="E" AND (g_ATTIVI<>"S" OR this.w_TIPLIQ="R")
        this.w_TOTCOV = this.w_TOTCOV + (this.w_TOTIVD + this.w_TOTIVI)
      endif
      * --- Operazioni Attive (Codici IVA Normali e No Acquisti/Importazioni)
      *     Codci iva non esclusi
      *     Codici iva esclusi e registro in ventilazione e gestito monteacuisti
      if TIPREG<>"A" AND ( NVL(FLVAFF,"N")<> "E" OR (NVL(FLVAFF,"N")<>"N" AND NVL(MACIVA,"N") ="S" AND TIPREG="E"))
        if (this.w_TIPREG="V" AND NVL(SPLPAY,"N")<>"S") OR this.w_TIPREG<>"V" OR (this.oParentObject.w_FLDEFI="A" and (NVL(SPLPAY,"N")<>"S" or this.oParentObject.w_NOSPLIT <>"S"))
          this.w_IMPOR1 = this.w_IMPOR1 + IIF(NOT NVL(ACQINT," ") $ "AIO", (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
          * --- Di cui Cessioni INTRA
          this.w_CESINT = this.w_CESINT + IIF(NVL(ACQINT," ")="C", (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
          * --- IVA Esigibile a Debito del Periodo
          this.w_IMPOR5 = this.w_IMPOR5 + ((NVL(IVASTA,0)+NVL(IVASEG,0)+NVL(IVAIND,0))-(NVL(IVAPRE,0)+NVL(IVAFAD,0)))
          if this.w_TIPREG $ "VC" AND this.w_TOTIMP<>0 AND this.w_FLAGRI="P" AND this.w_PERCOM>0
            * --- Se Tipo Reg.IVA Vendite o Corrisp.Scorporo e Attivita Produttore Agricolo e Aliq.IVA Compensazione, calcola IVA Detraibile 
            this.w_APPO1 = cp_ROUND(((this.w_TOTIMP * this.w_PERCOM) / 100), IIF(g_PERVAL=g_CODLIR, 0, 2))
            this.w_IMPOR6C = this.w_IMPOR6C + this.w_APPO1
            * --- Aggiorna Array per Riepilogo Stampa (se Liquodazione Periodica sing. Attivita')
            this.w_ALIAGR = this.w_ALIAGR + 1
            DETAGR[this.w_ALIAGR, 1] = this.w_PERCOM
            DETAGR[this.w_ALIAGR, 2] = this.w_TOTIMP
            DETAGR[this.w_ALIAGR, 3] = this.w_APPO1
          endif
        endif
      endif
      * --- Operazioni Passive (Codici IVA Normali)
      *     Codice  di tipo normale o esclusi con monte acquisti
      if TIPREG="A" AND (NVL(FLVAFF,"N")<>"E" OR (NVL(FLVAFF,"N")<>"N" AND NVL(MACIVA,"N") ="S" ))
        this.w_IMPOR2 = this.w_IMPOR2 + IIF(NVL(ACQINT," ") <> "C", (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
        * --- Di cui Acquisti INTRA
        this.w_ACQINT = this.w_ACQINT + IIF(NVL(ACQINT," ") $ "AO", (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
        * --- Imponibili Importazione ORO e ARGENTO
        this.w_IMPON3 = this.w_IMPON3 + IIF(NVL(ACQINT," ") $ "IO", (NVL(IMPSTA,0)+NVL(IMPSEG,0))-NVL(IMPPRE,0), 0)
        if NVL(FLVAFF,"N")="N" 
          * --- Imposta Importazioni ORO e ARGENTO
          this.w_IMPOS3 = this.w_IMPOS3 + IIF(NVL(ACQINT," ") $ "IO", (NVL(IVASTA,0)+NVL(IVISTA,0)+NVL(IVASEG,0)+NVL(IVISEG,0))-(NVL(IVAPRE,0)+NVL(IVIPRE,0)), 0)
          * --- IVA Detraibile a Credito del Periodo
          if this.w_FLAGRI="P" AND this.w_TIPAGR="A"
            * --- Aggiorna Totalizzatore IVA Acquisti Agevolata x Reg.IVA Agricola
            this.w_IMPOR6A = this.w_IMPOR6A + ((NVL(IVASTA,0)+NVL(IVASEG,0)+NVL(IVAIND,0))-(NVL(IVAPRE,0)+NVL(IVAFAD,0)))
          else
            * --- Se Attivita no Produttore Agricolo o Prod.Agricolo e Aliq.IVA non Agevolata)
            this.w_IMPOR6 = this.w_IMPOR6 + ((NVL(IVASTA,0)+NVL(IVASEG,0)+NVL(IVAIND,0))-(NVL(IVAPRE,0)+NVL(IVAFAD,0)))
          endif
        endif
      endif
      * --- Se i valori sono presenti aggiorno il cursore
      if (this.w_TOTIMP+this.w_TOTIVD+this.w_TOTIVI+this.w_TOTMAC+this.w_PERMAC)<>0
        * --- Scrive il Cursore di Elaborazione
        INSERT INTO LIQIVA ;
        (CODATT, DESCRI, TIPOPE, TIPREG, NUMREG, CODIVA, DESIVA, TOTIMP, TOTIVD, TOTIVI, TOTMAC, PERMAC, FLAGRI, PERCOM, TIPAGR,FLPROR,IVSPLPAY) ;
        VALUES (this.w_KEYATT, this.w_DESCRI, this.w_TIPOPE, this.w_TIPREG, this.w_NUMREG, this.w_CODIVA, this.w_DESIVA, ;
        this.w_TOTIMP, this.w_TOTIVD, this.w_TOTIVI, this.w_TOTMAC, this.w_PERMAC, ;
        this.w_FLAGRI, this.w_PERCOM, this.w_TIPAGR, this.w_TESTPROR,this.w_IVSPLPAY)
      endif
      SELECT SALDIIVA
      ENDSCAN
      * --- Se IVA Agricola: Eventuale IVA Detraibile NO Regime Speciale Agricolo
      this.w_IMPOR6B = this.w_IMPOR6
      * --- Se IVA Agricola Aggiunge all'IVA a Credito l'eventuale Compensazione Vendite. 
      this.w_IMPOR6 = this.w_IMPOR6 + this.w_IMPOR6C
      * --- Se Gestito Prorata Storna IVA Detraibile a Credito per la % Prorata (Per ciascuna Attivita')
      this.w_TOTACQ = this.w_IMPOR6 
      if this.w_FLPROR="S" AND this.w_IMPOR6<>0 AND this.w_PERPRO<>100
        this.w_IMPOR6 = cp_ROUND((this.w_IMPOR6 * this.w_PERPRO) / 100, g_PERPVL)
      endif
      this.w_IMPPRO = this.w_IMPOR6 
      * --- Dalle operazioni attive devono essere tolte le registrazioni di tipo autofattura (che movimentano il registro vendite)
      VQ_EXEC("QUERY\GSCG_BAU.VQR",this,"AUTOFATT")
      this.w_IMPAUT = 0
      if USED("AUTOFATT")
        SELECT AUTOFATT
        GO TOP
        SCAN
        this.w_IMPONI = NVL(IMPONI, 0)
        this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
        * --- Se La valuta dell'esercizio della Riga e' diversa da quella dell' Esercizio Attuale, converte
        * --- Prevediamo solo LIRE o EURO
        if this.w_VALNAZ<>g_PERVAL AND g_CAOEUR<>0
          do case
            case this.w_VALNAZ=g_CODLIR
              this.w_IMPONI = cp_ROUND(this.w_IMPONI/g_CAOEUR, g_PERPVL)
            case this.w_VALNAZ=g_CODEUR
              this.w_IMPONI = cp_ROUND(this.w_IMPONI*g_CAOEUR, g_PERPVL)
          endcase
        endif
        this.w_IMPAUT = this.w_IMPAUT+this.w_IMPONI
        SELECT AUTOFATT
        ENDSCAN
        * --- Aggiorno il valore delle operazioni attive della singola attivit�
        this.w_IMPOR1 = this.w_IMPOR1 - this.w_IMPAUT
        * --- Chiusura Cursore
        SELECT AUTOFATT
        USE
      endif
      * --- Importi definiti in KiloLire o all'Unita' di Euro
      this.w_APPO1 = IIF(g_PERVAL=g_CODLIR, 1000, IIF(VAL(this.oParentObject.w_ANNO)>2001, 0, 1))
      this.w_APPO = IIF(g_PERVAL=g_CODLIR, 1000, 1)
      this.w_VPIMPOR1 = this.w_VPIMPOR1 + (INTROUND(this.w_IMPOR1, this.w_APPO1) / this.w_APPO)
      this.w_VPCESINT = this.w_VPCESINT + (INTROUND(this.w_CESINT, this.w_APPO1) / this.w_APPO)
      this.w_VPIMPOR5 = this.w_VPIMPOR5 +(INTROUND(this.w_IMPOR5, this.w_APPO1) / this.w_APPO)
      this.w_VPIMPOR2 = this.w_VPIMPOR2 + (INTROUND(this.w_IMPOR2, this.w_APPO1) / this.w_APPO)
      this.w_VPACQINT = this.w_VPACQINT + (INTROUND(this.w_ACQINT, this.w_APPO1) / this.w_APPO)
      this.w_VPIMPON3 = this.w_VPIMPON3 + (INTROUND(this.w_IMPON3, this.w_APPO1) / this.w_APPO)
      this.w_VPIMPOS3 = this.w_VPIMPOS3 + (INTROUND(this.w_IMPOS3, this.w_APPO1) / this.w_APPO)
      this.w_VPIMPOR6C = this.w_VPIMPOR6C + (INTROUND(this.w_IMPOR6C, this.w_APPO1) / this.w_APPO)
      this.w_VPIMPOR6A = this.w_VPIMPOR6A + (INTROUND(this.w_IMPOR6A, this.w_APPO1) / this.w_APPO)
      this.w_VPIMPOR6 = this.w_VPIMPOR6 + (INTROUND(this.w_IMPOR6, this.w_APPO1) / this.w_APPO)
      this.w_VIMPOR6 = this.w_VIMPOR6+this.w_VPIMPOR6
      * --- IVA Del Periodo (VP5 - VP6 ; se Positiva a Debito altrimenti a Credito)
      do case
        case this.w_TIPOPE="A"
          this.w_VPIMPO7A = this.w_VPIMPOR5 - this.w_VPIMPOR6
        case this.w_TIPOPE="B"
          this.w_VPIMPO7B = this.w_VPIMPOR5 - this.w_VPIMPOR6
        case this.w_TIPOPE="C"
          this.w_VPIMPO7C = this.w_VPIMPOR5 - this.w_VPIMPOR6
      endcase
      * --- Debito / Credito Precedente
      if this.oParentObject.w_FLDEFI="A"
        SELECT LIQIVA
        REPLACE PERPRO WITH this.w_PERPRO FOR CODATT= this.w_KEYATT
        SELECT ATTIVITA
      endif
      * --- Aggiorno in caso di Riepilogativa i totali
      if this.oParentObject.w_MTIPLIQ="R"
        this.w_TOTATTIVA = this.w_TOTATTIVA + this.w_TOTACQ
        this.w_TOTATTPRO = this.w_TOTATTPRO + this.w_IMPPRO
      endif
    endif
    SELECT ATTIVITA
    ENDSCAN
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento Archivi (se Definitiva)
    * --- 1) Dichiarazione Periodica IVA (Sempre)
    * --- Try
    local bErr_0454C1F0
    bErr_0454C1F0=bTrsErr
    this.Try_0454C1F0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0454C1F0
    * --- End
    * --- Write into IVA_PERI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.IVA_PERI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.IVA_PERI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"VPCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_VPCODVAL),'IVA_PERI','VPCODVAL');
      +",VPVARIMP ="+cp_NullLink(cp_ToStrODBC(this.w_VPVARIMP),'IVA_PERI','VPVARIMP');
      +",VPDICGRU ="+cp_NullLink(cp_ToStrODBC(this.w_VPDICGRU),'IVA_PERI','VPDICGRU');
      +",VPDICSOC ="+cp_NullLink(cp_ToStrODBC(this.w_VPDICSOC),'IVA_PERI','VPDICSOC');
      +",VPIMPOR1 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPOR1),'IVA_PERI','VPIMPOR1');
      +",VPIMPOR2 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPOR2),'IVA_PERI','VPIMPOR2');
      +",VPCESINT ="+cp_NullLink(cp_ToStrODBC(this.w_VPCESINT),'IVA_PERI','VPCESINT');
      +",VPACQINT ="+cp_NullLink(cp_ToStrODBC(this.w_VPACQINT),'IVA_PERI','VPACQINT');
      +",VPIMPON3 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPON3),'IVA_PERI','VPIMPON3');
      +",VPIMPOS3 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPOS3),'IVA_PERI','VPIMPOS3');
      +",VPIMPOR5 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPOR5),'IVA_PERI','VPIMPOR5');
      +",VPIMPOR6 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPOR6),'IVA_PERI','VPIMPOR6');
      +",VPIMPOR7 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPOR7),'IVA_PERI','VPIMPOR7');
      +",VPIMPOR8 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPOR8),'IVA_PERI','VPIMPOR8');
      +",VPIMPOR9 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPOR9),'IVA_PERI','VPIMPOR9');
      +",VPIMPO10 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPO10),'IVA_PERI','VPIMPO10');
      +",VPIMPO11 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPO11),'IVA_PERI','VPIMPO11');
      +",VPIMPO12 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPO12),'IVA_PERI','VPIMPO12');
      +",VPIMPO13 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPO13),'IVA_PERI','VPIMPO13');
      +",VPIMPO14 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPO14),'IVA_PERI','VPIMPO14');
      +",VPIMPO15 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPO15),'IVA_PERI','VPIMPO15');
      +",VPIMPO16 ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPO16),'IVA_PERI','VPIMPO16');
      +",VPDATVER ="+cp_NullLink(cp_ToStrODBC(this.w_VPDATVER),'IVA_PERI','VPDATVER');
      +",VPCODAZI ="+cp_NullLink(cp_ToStrODBC(this.w_VPCODAZI),'IVA_PERI','VPCODAZI');
      +",VPCODCAB ="+cp_NullLink(cp_ToStrODBC(this.w_VPCODCAB),'IVA_PERI','VPCODCAB');
      +",VPVERNEF ="+cp_NullLink(cp_ToStrODBC(this.w_VPVERNEF),'IVA_PERI','VPVERNEF');
      +",VPAR74C5 ="+cp_NullLink(cp_ToStrODBC(this.w_VPAR74C5),'IVA_PERI','VPAR74C5');
      +",VPAR74C4 ="+cp_NullLink(cp_ToStrODBC(this.w_VPAR74C4),'IVA_PERI','VPAR74C4');
      +",VPOPMEDI ="+cp_NullLink(cp_ToStrODBC(this.w_VPOPMEDI),'IVA_PERI','VPOPMEDI');
      +",VPOPNOIM ="+cp_NullLink(cp_ToStrODBC(this.w_VPOPNOIM),'IVA_PERI','VPOPNOIM');
      +",VPCRERIM ="+cp_NullLink(cp_ToStrODBC(this.w_VPCRERIM),'IVA_PERI','VPCRERIM');
      +",VPEURO19 ="+cp_NullLink(cp_ToStrODBC(this.w_VPEURO19),'IVA_PERI','VPEURO19');
      +",VPCREUTI ="+cp_NullLink(cp_ToStrODBC(this.w_VPCREUTI),'IVA_PERI','VPCREUTI');
      +",VPCODCAR ="+cp_NullLink(cp_ToStrODBC(this.w_VPCODCAR),'IVA_PERI','VPCODCAR');
      +",VPCODFIS ="+cp_NullLink(cp_ToStrODBC(this.w_VPCODFIS),'IVA_PERI','VPCODFIS');
      +",VPDATPRE ="+cp_NullLink(cp_ToStrODBC(this.w_VPDATPRE),'IVA_PERI','VPDATPRE');
      +",VPIMPVER ="+cp_NullLink(cp_ToStrODBC(this.w_VPIMPVER),'IVA_PERI','VPIMPVER');
      +",VPVEREUR ="+cp_NullLink(cp_ToStrODBC(this.w_VPVEREUR),'IVA_PERI','VPVEREUR');
      +",VPCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_VPCODATT),'IVA_PERI','VPCODATT');
      +",VPACQBEA ="+cp_NullLink(cp_ToStrODBC(this.w_VPACQBEA),'IVA_PERI','VPACQBEA');
      +",VPCORTER ="+cp_NullLink(cp_ToStrODBC(this.w_VPCORTER),'IVA_PERI','VPCORTER');
          +i_ccchkf ;
      +" where ";
          +"VP__ANNO = "+cp_ToStrODBC(this.w_VP__ANNO);
          +" and VPPERIOD = "+cp_ToStrODBC(this.w_VPPERIOD);
          +" and VPKEYATT = "+cp_ToStrODBC(this.w_VPKEYATT);
             )
    else
      update (i_cTable) set;
          VPCODVAL = this.w_VPCODVAL;
          ,VPVARIMP = this.w_VPVARIMP;
          ,VPDICGRU = this.w_VPDICGRU;
          ,VPDICSOC = this.w_VPDICSOC;
          ,VPIMPOR1 = this.w_VPIMPOR1;
          ,VPIMPOR2 = this.w_VPIMPOR2;
          ,VPCESINT = this.w_VPCESINT;
          ,VPACQINT = this.w_VPACQINT;
          ,VPIMPON3 = this.w_VPIMPON3;
          ,VPIMPOS3 = this.w_VPIMPOS3;
          ,VPIMPOR5 = this.w_VPIMPOR5;
          ,VPIMPOR6 = this.w_VPIMPOR6;
          ,VPIMPOR7 = this.w_VPIMPOR7;
          ,VPIMPOR8 = this.w_VPIMPOR8;
          ,VPIMPOR9 = this.w_VPIMPOR9;
          ,VPIMPO10 = this.w_VPIMPO10;
          ,VPIMPO11 = this.w_VPIMPO11;
          ,VPIMPO12 = this.w_VPIMPO12;
          ,VPIMPO13 = this.w_VPIMPO13;
          ,VPIMPO14 = this.w_VPIMPO14;
          ,VPIMPO15 = this.w_VPIMPO15;
          ,VPIMPO16 = this.w_VPIMPO16;
          ,VPDATVER = this.w_VPDATVER;
          ,VPCODAZI = this.w_VPCODAZI;
          ,VPCODCAB = this.w_VPCODCAB;
          ,VPVERNEF = this.w_VPVERNEF;
          ,VPAR74C5 = this.w_VPAR74C5;
          ,VPAR74C4 = this.w_VPAR74C4;
          ,VPOPMEDI = this.w_VPOPMEDI;
          ,VPOPNOIM = this.w_VPOPNOIM;
          ,VPCRERIM = this.w_VPCRERIM;
          ,VPEURO19 = this.w_VPEURO19;
          ,VPCREUTI = this.w_VPCREUTI;
          ,VPCODCAR = this.w_VPCODCAR;
          ,VPCODFIS = this.w_VPCODFIS;
          ,VPDATPRE = this.w_VPDATPRE;
          ,VPIMPVER = this.w_VPIMPVER;
          ,VPVEREUR = this.w_VPVEREUR;
          ,VPCODATT = this.w_VPCODATT;
          ,VPACQBEA = this.w_VPACQBEA;
          ,VPCORTER = this.w_VPCORTER;
          &i_ccchkf. ;
       where;
          VP__ANNO = this.w_VP__ANNO;
          and VPPERIOD = this.w_VPPERIOD;
          and VPKEYATT = this.w_VPKEYATT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- 2) Flag Stampa Saldi IVA (Sempre)
    * --- Inserisco flag Liquidazione nei saldi di Tutti i registri collegati all'attivit�
    if this.w_TIPLIQ="R"
      if g_ATTIVI="S" 
        * --- Scrive Flag Stampata Dichiarazione di Riepilogo se gestite  Pi� attivit�
        * --- Write into PRI_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRI_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRI_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TRFLSTAR ="+cp_NullLink(cp_ToStrODBC("S"),'PRI_MAST','TRFLSTAR');
              +i_ccchkf ;
          +" where ";
              +"TR__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
              +" and TRNUMPER = "+cp_ToStrODBC(this.oParentObject.w_NUMPER);
                 )
        else
          update (i_cTable) set;
              TRFLSTAR = "S";
              &i_ccchkf. ;
           where;
              TR__ANNO = this.oParentObject.w_ANNO;
              and TRNUMPER = this.oParentObject.w_NUMPER;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    else
      * --- Select from gscg2blp
      do vq_exec with 'gscg2blp',this,'_Curs_gscg2blp','',.f.,.t.
      if used('_Curs_gscg2blp')
        select _Curs_gscg2blp
        locate for 1=1
        do while not(eof())
        this.w_TIPREG = NVL(_Curs_GSCG2BLP.TIPREG, " ")
        this.w_NUMREG = NVL(_Curs_GSCG2BLP.NUMREG, 0)
        if this.w_NUMREG<>0 AND NOT EMPTY(this.w_TIPREG)
          * --- Write into PRI_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PRI_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRI_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PRI_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TRFLSTAM ="+cp_NullLink(cp_ToStrODBC("S"),'PRI_MAST','TRFLSTAM');
                +i_ccchkf ;
            +" where ";
                +"TR__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                +" and TRNUMPER = "+cp_ToStrODBC(this.oParentObject.w_NUMPER);
                +" and TRTIPREG = "+cp_ToStrODBC(this.w_TIPREG);
                +" and TRNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
                   )
          else
            update (i_cTable) set;
                TRFLSTAM = "S";
                &i_ccchkf. ;
             where;
                TR__ANNO = this.oParentObject.w_ANNO;
                and TRNUMPER = this.oParentObject.w_NUMPER;
                and TRTIPREG = this.w_TIPREG;
                and TRNUMREG = this.w_NUMREG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
          select _Curs_gscg2blp
          continue
        enddo
        use
      endif
    endif
    * --- 3) Credito del Periodo
    if this.w_CRERES>0 AND this.w_VPIMPO11<>0 AND (g_ATTIVI<>"S" OR this.w_TIPLIQ="R")
      * --- Select from CREDDIVA
      do vq_exec with 'CREDDIVA',this,'_Curs_CREDDIVA','',.f.,.t.
      if used('_Curs_CREDDIVA')
        select _Curs_CREDDIVA
        locate for 1=1
        do while not(eof())
        this.w_ROWNUM = _Curs_CREDDIVA.CPROWNUM + 1
          select _Curs_CREDDIVA
          continue
        enddo
        use
      endif
      * --- Try
      local bErr_04549610
      bErr_04549610=bTrsErr
      this.Try_04549610()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04549610
      * --- End
      this.w_APPO = g_PERVAL
      * --- Attenzione: se g_PERVAL=Lire,  gli Importi di VP11 sono espressi in KLire
      this.w_APPO1 = this.w_VPIMPO11 * IIF(g_PERVAL=g_CODLIR, 1000, 1)
      * --- Write into CREDDIVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CREDDIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CREDDIVA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CREDDIVA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CIVALPER ="+cp_NullLink(cp_ToStrODBC(this.w_APPO),'CREDDIVA','CIVALPER');
        +",CICREDET ="+cp_NullLink(cp_ToStrODBC(this.w_APPO1),'CREDDIVA','CICREDET');
        +",CIDATUTI ="+cp_NullLink(cp_ToStrODBC(this.w_VPDATVER),'CREDDIVA','CIDATUTI');
        +",CIFLTEST ="+cp_NullLink(cp_ToStrODBC("S"),'CREDDIVA','CIFLTEST');
            +i_ccchkf ;
        +" where ";
            +"CI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
            +" and CINUMPER = "+cp_ToStrODBC(this.oParentObject.w_NUMPER);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
               )
      else
        update (i_cTable) set;
            CIVALPER = this.w_APPO;
            ,CICREDET = this.w_APPO1;
            ,CIDATUTI = this.w_VPDATVER;
            ,CIFLTEST = "S";
            &i_ccchkf. ;
         where;
            CI__ANNO = this.oParentObject.w_ANNO;
            and CINUMPER = this.oParentObject.w_NUMPER;
            and CPROWNUM = this.w_ROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_INTLIG="S"
      * --- 4) Aggionamento Progressivo pagine ATTIVITA'
      this.w_PAGINE = IIF(this.w_PAGINE=0,this.w_PRPARI,this.w_PAGINE)
      * --- Riscrive il progressivo pagine passato dalla Maschera
      this.oParentObject.w_PRPARI = this.w_PAGINE
      * --- In caso di Liquidazione Periodica Riepilogativa, recupero l'attivit� principale per aggiornamento numero pagine stampate
      this.w_LETAZI = .F.
      if this.oParentObject.w_MTIPLIQ="R" AND this.oParentObject.w_CODATT=SPACE(5)
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZCATAZI"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZCATAZI;
            from (i_cTable) where;
                AZCODAZI = this.oParentObject.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_CODATT = NVL(cp_ToDate(_read_.AZCATAZI),cp_NullValue(_read_.AZCATAZI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_LETAZI = .T.
      endif
      * --- Try
      local bErr_0454AB70
      bErr_0454AB70=bTrsErr
      this.Try_0454AB70()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0454AB70
      * --- End
      * --- In caso di Liquidazione Periodica Riepilogativa, sbianco la variabile attivit� letta dai Dati Azienda
      if this.oParentObject.w_MTIPLIQ="R" AND this.w_LETAZI
        this.oParentObject.w_CODATT = SPACE(5)
      endif
    endif
  endproc
  proc Try_0454C1F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into IVA_PERI
    i_nConn=i_TableProp[this.IVA_PERI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.IVA_PERI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"VP__ANNO"+",VPPERIOD"+",VPKEYATT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_VP__ANNO),'IVA_PERI','VP__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VPPERIOD),'IVA_PERI','VPPERIOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VPKEYATT),'IVA_PERI','VPKEYATT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'VP__ANNO',this.w_VP__ANNO,'VPPERIOD',this.w_VPPERIOD,'VPKEYATT',this.w_VPKEYATT)
      insert into (i_cTable) (VP__ANNO,VPPERIOD,VPKEYATT &i_ccchkf. );
         values (;
           this.w_VP__ANNO;
           ,this.w_VPPERIOD;
           ,this.w_VPKEYATT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04549610()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CREDDIVA
    i_nConn=i_TableProp[this.CREDDIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CREDDIVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CREDDIVA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CI__ANNO"+",CPROWNUM"+",CINUMPER"+",CIFLTEST"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANNO),'CREDDIVA','CI__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'CREDDIVA','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUMPER),'CREDDIVA','CINUMPER');
      +","+cp_NullLink(cp_ToStrODBC("S"),'CREDDIVA','CIFLTEST');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CI__ANNO',this.oParentObject.w_ANNO,'CPROWNUM',this.w_ROWNUM,'CINUMPER',this.oParentObject.w_NUMPER,'CIFLTEST',"S")
      insert into (i_cTable) (CI__ANNO,CPROWNUM,CINUMPER,CIFLTEST &i_ccchkf. );
         values (;
           this.oParentObject.w_ANNO;
           ,this.w_ROWNUM;
           ,this.oParentObject.w_NUMPER;
           ,"S";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0454AB70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ATTIDETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ATTIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATPRPARI ="+cp_NullLink(cp_ToStrODBC(this.w_PAGINE),'ATTIDETT','ATPRPARI');
          +i_ccchkf ;
      +" where ";
          +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
          +" and ATNUMREG = "+cp_ToStrODBC(this.oParentObject.w_ATNUMREG);
          +" and ATTIPREG = "+cp_ToStrODBC(this.oParentObject.w_ATTIPREG);
             )
    else
      update (i_cTable) set;
          ATPRPARI = this.w_PAGINE;
          &i_ccchkf. ;
       where;
          ATCODATT = this.oParentObject.w_CODATT;
          and ATNUMREG = this.oParentObject.w_ATNUMREG;
          and ATTIPREG = this.oParentObject.w_ATTIPREG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Registrazione Contabile di Giroconto IVA Ventilata (se TOTCOV<>0)
    * --- Aggiorna Registrazione contabile per Iva in Ventilazione
    this.w_CODCAU = SPACE(5)
    this.w_CODIVA = SPACE(15)
    this.w_PNDATREG = i_DatSys
    this.w_FLSALF = " "
    this.w_CODVEN = SPACE(15)
    this.w_PNDESSUP = SPACE(45)
    this.w_FLSALI = " "
    this.w_TIPVEN = " "
    this.w_CONFE2 = "N"
    this.w_TIPREG = " "
    this.w_TIPIVA = " "
    this.w_TIPCON = "G"
    this.w_FLTRASP = IIF(g_TIPDEN="T" OR MOD(this.oParentObject.w_NUMPER, 3)=0, .T., .F.)
    if this.w_FLTRASP
      this.w_DATAINIZ = this.w_DATINI
      do trim_prec with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- SE � trimestrale o del Mese 3,6,9,12 deve elaborare le registrazioni AUTOTRASPORTO
      * --- Determino la data di inizio anno (relativo alle fatture autotrasportatori)
      * --- Aggiorna documenti: attivabile sulla maschera.
      *     Se Attiva verr� aggiornato il valore fiscale dei documenti
    endif
    * --- Maschera Giroconto IVA Ventilata
    do GSCG_KCI with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_CONFE2<>"S"
      * --- No Conferma maschera
      ah_ErrorMsg("Aggiornare manualmente registrazione contabile","!","")
    else
      * --- Aggiorna Registrazione contabile per Iva in Ventilazione (Se non 13 periodo)
      if this.w_TOTCOV<>0 AND this.w_GENVENT="S"
        * --- Try
        local bErr_0454F430
        bErr_0454F430=bTrsErr
        this.Try_0454F430()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_0454F430
        * --- End
      endif
      * --- Se attivo il flag Genera giroconto Automatico, la procedura genera una registrazione di Giroconto.
      if this.w_GENAUTO="S"
        * --- Try
        local bErr_04554590
        bErr_04554590=bTrsErr
        this.Try_04554590()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_04554590
        * --- End
      endif
    endif
  endproc
  proc Try_0454F430()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_LCODCAU = this.w_CODCAU
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TOTALE = 0
    * --- Se ho impostato la nuova opzione sui parametri IVA 
    *     effettuo una prima select per determinare il totale importi.
    if this.oParentObject.w_GIRIVA="SI"
      * --- Select from gscg1bia
      do vq_exec with 'gscg1bia',this,'_Curs_gscg1bia','',.f.,.t.
      if used('_Curs_gscg1bia')
        select _Curs_gscg1bia
        locate for 1=1
        do while not(eof())
        this.w_IMPONI = _Curs_gscg1bia.IMPONI
        this.w_CONTA = _Curs_gscg1bia.CONTA
          select _Curs_gscg1bia
          continue
        enddo
        use
      endif
      * --- Eseguo la select per ciclare sui conti da inserire in primanota
      * --- Select from gscg_bia
      do vq_exec with 'gscg_bia',this,'_Curs_gscg_bia','',.f.,.t.
      if used('_Curs_gscg_bia')
        select _Curs_gscg_bia
        locate for 1=1
        do while not(eof())
        this.w_CONTA = this.w_CONTA-1
        this.w_CONTO = _Curs_gscg_bia.PNCODCON
        if this.w_CONTA<>0
          this.w_IMPORTODAR = cp_ROUND((this.w_TOTCOV/this.w_IMPONI) * _Curs_gscg_bia.IMPONI,g_PERPVL)
        else
          * --- Metto il resto sull'ultima riga creata
          this.w_IMPORTODAR = this.w_TOTCOV-this.w_TOTALE
        endif
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_gscg_bia
          continue
        enddo
        use
      endif
      if this.w_TOTCOV<>this.w_TOTALE
        * --- Raise
        i_Error=AH_MSGFORMAT("Importo IVA ventilata diverso dal totale documento%0Impossibile generare la registrazione")
        return
      endif
    else
      this.w_CONTO = this.w_CODVEN
      this.w_IMPORTODAR = this.w_TOTCOV
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Scrive Conto IVA su Corrispettivi
    *     =================================================
    this.w_CONTO = this.w_CODIVA
    this.w_IMPORTODAR = -this.w_TOTALE
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiornamento Valore Fiscale Corrispettivi In Ventilazione
    *     Poich� la Percentuale Iva dei Corrispettivi Ventilati si conosce solo 
    *     dopo la Liquidazione del periodo, il valore fiscale delle righe dei Corrispettivi Ventilati
    *     Viene Scorporato in questa fase
    if this.w_AGGDOC = "S"
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL,CPROWNUM"
        do vq_exec with 'gscg3qcv',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVIMPNAZ = _t2.NETTO";
            +i_ccchkf;
            +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
            +"DOC_DETT.MVIMPNAZ = _t2.NETTO";
            +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
            +"MVIMPNAZ";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.NETTO";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
            +"MVIMPNAZ = _t2.NETTO";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVIMPNAZ = (select NETTO from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Memorizzo seriale da utilizzare come filtro nella query gscg2bia
    this.w_SERIAL = this.w_PNSERIAL
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04554590()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_LCODCAU = this.w_CAUGIR
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Select from gscg3bia
    do vq_exec with 'gscg3bia',this,'_Curs_gscg3bia','',.f.,.t.
    if used('_Curs_gscg3bia')
      select _Curs_gscg3bia
      locate for 1=1
      do while not(eof())
      this.w_IMPONI = _Curs_gscg3bia.IMPONI
        select _Curs_gscg3bia
        continue
      enddo
      use
    endif
    * --- Eseguo la select per ciclare sui conti da inserire in primanota
    * --- Select from gscg2bia
    do vq_exec with 'gscg2bia',this,'_Curs_gscg2bia','',.f.,.t.
    if used('_Curs_gscg2bia')
      select _Curs_gscg2bia
      locate for 1=1
      do while not(eof())
      this.w_CONTO = _Curs_gscg2bia.PNCODCON
      this.w_IMPORTODAR = cp_ROUND(_Curs_gscg2bia.IMPONI,g_PERPVL)
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
        select _Curs_gscg2bia
        continue
      enddo
      use
    endif
    * --- Scrive le contropartite giroconto IVA:
    *     ==============================================
    if this.w_VPIMPO10<>0
      this.w_CONTO = this.w_CREDRIP
      this.w_IMPORTODAR = this.w_VPIMPO10
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_VPIMPO13<>0
      this.w_CONTO = this.w_CREDSPE
      this.w_IMPORTODAR = -this.w_VPIMPO13
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_VPIMPO14<>0
      this.w_CONTO = this.w_INTDOV
      this.w_IMPORTODAR = Abs(this.w_VPIMPO14)
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Scrive Conto IVA 
    *     ==============================================
    if (this.w_TOTALE+this.w_TOTACQ-this.w_IMPPRO)<>0 or (this.w_TOTALE+this.w_TOTATTIVA - this.w_TOTATTPRO)<>0
      this.w_CONTO = this.w_CONIVA
      if this.oParentObject.w_MTIPLIQ="R"
        this.w_IMPORTODAR = -(this.w_TOTALE+this.w_TOTATTIVA-this.w_TOTATTPRO)
      else
        this.w_IMPORTODAR = -(this.w_TOTALE+this.w_TOTACQ-this.w_IMPPRO)
      endif
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if (this.w_TOTACQ-this.w_IMPPRO)<>0 or (this.w_TOTATTIVA - this.w_TOTATTPRO)<>0
      this.w_CONTO = this.w_CONIND
      if this.oParentObject.w_MTIPLIQ="R"
        this.w_IMPORTODAR = this.w_TOTATTIVA - this.w_TOTATTPRO
      else
        this.w_IMPORTODAR = this.w_TOTACQ-this.w_IMPPRO
      endif
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Operazioni Finali sul Cursore
    * --- Select from ATTIDETT
    i_nConn=i_TableProp[this.ATTIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select ATNUMREG  from "+i_cTable+" ATTIDETT ";
          +" where ATCODATT="+cp_ToStrODBC(this.oParentObject.w_CODATT)+" AND ATTIPREG='E'";
          +" order by ATNUMREG";
           ,"_Curs_ATTIDETT")
    else
      select ATNUMREG from (i_cTable);
       where ATCODATT=this.oParentObject.w_CODATT AND ATTIPREG="E";
       order by ATNUMREG;
        into cursor _Curs_ATTIDETT
    endif
    if used('_Curs_ATTIDETT')
      select _Curs_ATTIDETT
      locate for 1=1
      do while not(eof())
      SELECT LIQIVA
      GO TOP
      this.w_REGISTRO = _Curs_ATTIDETT.ATNUMREG
      this.w_GLOMAC = 0
      * --- Calcola il Totale Monte Acquisti dei Corrispettivi in Ventilazione
      SUM TOTMAC TO this.w_GLOMAC FOR TIPREG="E" AND TIPOPE="A" AND NUMREG=this.w_REGISTRO AND TOTMAC>=0
      if this.w_GLOMAC<>0
        * --- Calcola le percentuali per ciascuna aliquota
        this.w_APPO = 0
        GO TOP
        SCAN FOR TIPREG="E" AND TIPOPE="A" AND NUMREG=this.w_REGISTRO AND TOTMAC>=0
        this.w_PERMAC = cp_ROUND((TOTMAC * 100) / this.w_GLOMAC, 6)
        this.w_APPO = this.w_APPO + this.w_PERMAC
        REPLACE PERMAC WITH this.w_PERMAC
        ENDSCAN
        if this.w_APPO<>100
          * --- Inserisce l'eventuale resto sulla prima % Monte Acquisti valorizzata
          GO TOP
          LOCATE FOR TIPREG="E" AND TIPOPE="A" AND PERMAC<>0 AND NUMREG=this.w_REGISTRO
          if FOUND()
            this.w_PERMAC = PERMAC + (100-this.w_APPO)
            REPLACE PERMAC WITH this.w_PERMAC
          endif
        endif
      endif
        select _Curs_ATTIDETT
        continue
      enddo
      use
    endif
    * --- Se non ci sono records aggiunge un record fittizio per la stampa
    SELECT LIQIVA
    if RECCOUNT()=0
      INSERT INTO LIQIVA (CODATT, TIPREG, NUMREG, CODIVA) VALUES ("xxxxx", "B", 0, "     ")
    else
      L_FLAGRI=IIF(this.oParentObject.w_MTIPLIQ="R", " ", this.w_FLAGRI)
      if this.oParentObject.w_MTIPLIQ<>"R" AND L_FLAGRI="P"
        * --- Se Attivita' Liquidazione IVA Agricola aggiunge il Dettaglio Acquisti IVA a Regime Agricolo
        * --- IVA Acquisti a Regime Speciale Agricolo
        INSERT INTO LIQIVA (CODATT, TIPREG, NUMREG, CODIVA, TOTIMP, TOTIVD) ;
        VALUES (this.w_KEYATT, "Z", -1, "     ", 0, this.w_IMPOR6A)
        * --- IVA Acquisti NO Regime Speciale Agricolo
        INSERT INTO LIQIVA (CODATT, TIPREG, NUMREG, CODIVA, TOTIMP, TOTIVD) ;
        VALUES (this.w_KEYATT, "Z", 0, "     ", 0, this.w_IMPOR6B)
        if this.w_ALIAGR>0
          * --- Vendite Aliquota con % Compensazione
          FOR L_I= 1 TO this.w_ALIAGR
          INSERT INTO LIQIVA (CODATT, TIPREG, NUMREG, CODIVA, TOTIMP, TOTIVD, PERCOM) ;
          VALUES (this.w_KEYATT, "Z", L_I, "     ", DETAGR[L_I, 2], DETAGR[L_I, 3], DETAGR[L_I, 1])
          ENDFOR
        endif
      endif
    endif
    * --- Alla Fine Raggruppa tutti i Valori presenti nel Cursore LIQIVA e  Ordina
    SELECT CODATT, DESCRI, TIPOPE, TIPREG, NUMREG, CODIVA, MAX(DESIVA) AS DESIVA, ;
    SUM(TOTIMP) AS TOTIMP, SUM(TOTIVD) AS TOTIVD, SUM(TOTIVI) AS TOTIVI, SUM(TOTMAC) AS TOTMAC, SUM(PERMAC) AS PERMAC, ;
    MAX(FLAGRI) AS FLAGRI, MAX(PERCOM) AS PERCOM, MAX(TIPAGR) AS TIPAGR, MAX(PERPRO) AS PERPRO, MIN(FLPROR) AS FLPROR,Max(IVSPLPAY) as IVSPLPAY ;
    FROM LIQIVA GROUP BY CODATT, TIPOPE, TIPREG, NUMREG, CODIVA ORDER BY TIPREG,NUMREG, CODIVA INTO CURSOR LIQIVA
    * --- Questi Campi non Vengono Calcolati ma Inseriti Direttamente dall'Utente sulla Maschera di Riepilogo Dichiarazione
    this.w_VPIMPOR8 = 0
    this.w_VPIMPOR9 = 0
    this.w_VPIMPO11 = 0
    this.w_VPIMPO13 = 0
    this.w_VPCRERIM = 0
    this.w_VPCREUTI = 0
    * --- VP15 - Debito/Credito Precedente Riportato ; Viene letto dalla Dichiarazione Precedente (Della Singola Attivita' o Riepilogativa)
    this.w_VPIMPO10 = 0
    * --- Calcolato solo se no primo periodo e no societa' del gruppo e riepilogo o monoattivita'
    if this.oParentObject.w_NUMPER>1 AND this.w_VPDICSOC<>"S" AND (g_ATTIVI<>"S" OR this.oParentObject.w_MTIPLIQ="R")
      if this.w_PRIMPO16>0
        * --- Precedente dichiarazione a Debito (VP21 importo positivo) ; se Minore (o uguale) del versamento Minimo riporta il debito
        this.w_VPIMPO10 = IIF(this.w_PRIMPO16 <= this.w_MINVER, this.w_PRIMPO16 , 0)
      else
        * --- Altrimenti se Precedente dichiarazione IVA a Credito (VP17 importo negativo) ; riporta il credito diminuito dei Rimborsi
        if this.w_PRIMPO12<0
          * --- Attenzione, per i crediti, PRIMPO12 e' di segno negativo mentre CRERIM e CREUTI e' sempre in valore assoluto
          this.w_VPIMPO10 = this.w_PRIMPO12 + (this.w_PRCRERIM + this.w_PRCREUTI)
        endif
      endif
    endif
    * --- IVA Dovuta/Credito (proposta e calcolata anche in Maschera)
    this.w_VPIMPOR7 = this.w_VPIMPO7A + this.w_VPIMPO7B + this.w_VPIMPO7C
    this.w_VPIMPO12 = (this.w_VPIMPOR7+this.w_VPIMPOR8+this.w_VPIMPOR9+this.w_VPIMPO10)-this.w_VPIMPO11
    * --- Interessi Dovuti (Calcolato solo Show)
    this.w_VPIMPO14 = 0
    if g_TIPDEN<>"M" AND this.oParentObject.w_NUMPER<>4
      this.w_APPO = IIF(VAL(this.oParentObject.w_ANNO)>2001, 2, 0)
      this.w_VPIMPO14 = cp_ROUND(((this.w_VPIMPO12-(this.w_VPIMPO13+IIF(this.w_VPIMPO10>0,this.w_VPIMPO10,0))) * this.oParentObject.w_DENMAG) / 100, this.w_APPO)
      * --- Non possono essere di segno negativo
      this.w_VPIMPO14 = IIF(this.w_VPIMPO14<0, 0, this.w_VPIMPO14)
    endif
    * --- Acconto Versato (Calcolato solo Show)
    this.w_VPIMPO15 = 0
    if this.w_VPDICSOC<>"S" AND ((g_TIPDEN<>"M" AND this.oParentObject.w_NUMPER=4) OR (g_TIPDEN="M" AND this.oParentObject.w_NUMPER=12))
      * --- Calcolato in Base al'Acconto IVA
      this.w_VPIMPO15 = IIF(this.w_TIPLIQ="R" OR g_ATTIVI<>"S", this.w_MINACC, 0)
    endif
    * --- Importo da Versare (Calcolato solo Show)
    this.w_VPIMPO16 = 0
    if NOT (g_TIPDEN<>"M" AND this.oParentObject.w_NUMPER=4)
      this.w_VPIMPO16 = (this.w_VPIMPO12 + this.w_VPIMPO14) - (this.w_VPIMPO13 + this.w_VPIMPO15)
      * --- Se a Credito mette a 0
      this.w_VPIMPO16 = IIF(this.w_VPIMPO16<0, 0, this.w_VPIMPO16)
    endif
    * --- Importo Versato (Proposto di Default)
    this.w_VPIMPVER = IIF(this.w_VPDICSOC<>"S" AND this.w_VPIMPO16>this.w_MINVER, this.w_VPIMPO16, 0)
    if this.oParentObject.w_FLDEFI="R"
      * --- Se Ristampa rilegge i dati della Dichiarazione
      * --- Read from IVA_PERI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.IVA_PERI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2],.t.,this.IVA_PERI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" IVA_PERI where ";
              +"VP__ANNO = "+cp_ToStrODBC(this.w_VP__ANNO);
              +" and VPPERIOD = "+cp_ToStrODBC(this.w_VPPERIOD);
              +" and VPKEYATT = "+cp_ToStrODBC(this.w_VPKEYATT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              VP__ANNO = this.w_VP__ANNO;
              and VPPERIOD = this.w_VPPERIOD;
              and VPKEYATT = this.w_VPKEYATT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_VPACQBEA = NVL(cp_ToDate(_read_.VPACQBEA),cp_NullValue(_read_.VPACQBEA))
        this.w_VPACQINT = NVL(cp_ToDate(_read_.VPACQINT),cp_NullValue(_read_.VPACQINT))
        this.w_VPAR74C4 = NVL(cp_ToDate(_read_.VPAR74C4),cp_NullValue(_read_.VPAR74C4))
        this.w_VPAR74C5 = NVL(cp_ToDate(_read_.VPAR74C5),cp_NullValue(_read_.VPAR74C5))
        this.w_VPCESINT = NVL(cp_ToDate(_read_.VPCESINT),cp_NullValue(_read_.VPCESINT))
        this.w_VPCODATT = NVL(cp_ToDate(_read_.VPCODATT),cp_NullValue(_read_.VPCODATT))
        this.w_VPCODAZI = NVL(cp_ToDate(_read_.VPCODAZI),cp_NullValue(_read_.VPCODAZI))
        this.w_VPCODCAB = NVL(cp_ToDate(_read_.VPCODCAB),cp_NullValue(_read_.VPCODCAB))
        this.w_VPCODCAR = NVL(cp_ToDate(_read_.VPCODCAR),cp_NullValue(_read_.VPCODCAR))
        this.w_VPCODFIS = NVL(cp_ToDate(_read_.VPCODFIS),cp_NullValue(_read_.VPCODFIS))
        this.w_VPCODVAL = NVL(cp_ToDate(_read_.VPCODVAL),cp_NullValue(_read_.VPCODVAL))
        this.w_VPCORTER = NVL(cp_ToDate(_read_.VPCORTER),cp_NullValue(_read_.VPCORTER))
        this.w_VPCRERIM = NVL(cp_ToDate(_read_.VPCRERIM),cp_NullValue(_read_.VPCRERIM))
        this.w_VPCREUTI = NVL(cp_ToDate(_read_.VPCREUTI),cp_NullValue(_read_.VPCREUTI))
        this.w_VPDATPRE = NVL(cp_ToDate(_read_.VPDATPRE),cp_NullValue(_read_.VPDATPRE))
        this.w_VPDATVER = NVL(cp_ToDate(_read_.VPDATVER),cp_NullValue(_read_.VPDATVER))
        this.w_VPDICGRU = NVL(cp_ToDate(_read_.VPDICGRU),cp_NullValue(_read_.VPDICGRU))
        this.w_VPDICSOC = NVL(cp_ToDate(_read_.VPDICSOC),cp_NullValue(_read_.VPDICSOC))
        this.w_VPDICSOC = NVL(cp_ToDate(_read_.VPDICSOC),cp_NullValue(_read_.VPDICSOC))
        this.w_VPEURO19 = NVL(cp_ToDate(_read_.VPEURO19),cp_NullValue(_read_.VPEURO19))
        this.w_VPIMPO10 = NVL(cp_ToDate(_read_.VPIMPO10),cp_NullValue(_read_.VPIMPO10))
        this.w_VPIMPO11 = NVL(cp_ToDate(_read_.VPIMPO11),cp_NullValue(_read_.VPIMPO11))
        this.w_VPIMPO12 = NVL(cp_ToDate(_read_.VPIMPO12),cp_NullValue(_read_.VPIMPO12))
        this.w_VPIMPO13 = NVL(cp_ToDate(_read_.VPIMPO13),cp_NullValue(_read_.VPIMPO13))
        this.w_VPIMPO14 = NVL(cp_ToDate(_read_.VPIMPO14),cp_NullValue(_read_.VPIMPO14))
        this.w_VPIMPO15 = NVL(cp_ToDate(_read_.VPIMPO15),cp_NullValue(_read_.VPIMPO15))
        this.w_VPIMPO16 = NVL(cp_ToDate(_read_.VPIMPO16),cp_NullValue(_read_.VPIMPO16))
        this.w_VPIMPON3 = NVL(cp_ToDate(_read_.VPIMPON3),cp_NullValue(_read_.VPIMPON3))
        this.w_VPIMPOR1 = NVL(cp_ToDate(_read_.VPIMPOR1),cp_NullValue(_read_.VPIMPOR1))
        this.w_VPIMPOR2 = NVL(cp_ToDate(_read_.VPIMPOR2),cp_NullValue(_read_.VPIMPOR2))
        this.w_VPIMPOR5 = NVL(cp_ToDate(_read_.VPIMPOR5),cp_NullValue(_read_.VPIMPOR5))
        this.w_VPIMPOR6 = NVL(cp_ToDate(_read_.VPIMPOR6),cp_NullValue(_read_.VPIMPOR6))
        this.w_VPIMPOR7 = NVL(cp_ToDate(_read_.VPIMPOR7),cp_NullValue(_read_.VPIMPOR7))
        this.w_VPIMPOR8 = NVL(cp_ToDate(_read_.VPIMPOR8),cp_NullValue(_read_.VPIMPOR8))
        this.w_VPIMPOR9 = NVL(cp_ToDate(_read_.VPIMPOR9),cp_NullValue(_read_.VPIMPOR9))
        this.w_VPIMPOS3 = NVL(cp_ToDate(_read_.VPIMPOS3),cp_NullValue(_read_.VPIMPOS3))
        this.w_VPIMPVER = NVL(cp_ToDate(_read_.VPIMPVER),cp_NullValue(_read_.VPIMPVER))
        this.w_VPOPMEDI = NVL(cp_ToDate(_read_.VPOPMEDI),cp_NullValue(_read_.VPOPMEDI))
        this.w_VPOPNOIM = NVL(cp_ToDate(_read_.VPOPNOIM),cp_NullValue(_read_.VPOPNOIM))
        this.w_VPVARIMP = NVL(cp_ToDate(_read_.VPVARIMP),cp_NullValue(_read_.VPVARIMP))
        this.w_VPVEREUR = NVL(cp_ToDate(_read_.VPVEREUR),cp_NullValue(_read_.VPVEREUR))
        this.w_VPVERNEF = NVL(cp_ToDate(_read_.VPVERNEF),cp_NullValue(_read_.VPVERNEF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo le descrizioni dei codici ABI e CAB
      if NOT EMPTY(this.w_VPCODAZI) AND NOT EMPTY(this.w_VPCODCAB)
        * --- Read from COD_ABI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COD_ABI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COD_ABI_idx,2],.t.,this.COD_ABI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ABDESABI"+;
            " from "+i_cTable+" COD_ABI where ";
                +"ABCODABI = "+cp_ToStrODBC(this.w_VPCODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ABDESABI;
            from (i_cTable) where;
                ABCODABI = this.w_VPCODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESABI = NVL(cp_ToDate(_read_.ABDESABI),cp_NullValue(_read_.ABDESABI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from COD_CAB
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COD_CAB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COD_CAB_idx,2],.t.,this.COD_CAB_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "FIDESFIL,FIINDIRI,FI___CAP"+;
            " from "+i_cTable+" COD_CAB where ";
                +"FICODABI = "+cp_ToStrODBC(this.w_VPCODAZI);
                +" and FICODCAB = "+cp_ToStrODBC(this.w_VPCODCAB);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            FIDESFIL,FIINDIRI,FI___CAP;
            from (i_cTable) where;
                FICODABI = this.w_VPCODAZI;
                and FICODCAB = this.w_VPCODCAB;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESFIL = NVL(cp_ToDate(_read_.FIDESFIL),cp_NullValue(_read_.FIDESFIL))
          this.w_INDIRI = NVL(cp_ToDate(_read_.FIINDIRI),cp_NullValue(_read_.FIINDIRI))
          this.w_CAP = NVL(cp_ToDate(_read_.FI___CAP),cp_NullValue(_read_.FI___CAP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    * --- Per acconto IVA
    this.w_VIMPOR8 = this.w_VPIMPOR8
    this.w_VIMPOR9 = this.w_VPIMPOR9
    this.w_VIMPO10 = this.w_VPIMPO10
    this.w_VIMPO11 = this.w_VPIMPO11
    this.w_VIMPO12 = this.w_VPIMPO12
    this.w_VIMPO13 = this.w_VPIMPO13
    this.w_VIMPO15 = this.w_VPIMPO15
    this.w_VIMPO16 = this.w_VPIMPO16
    if this.oParentObject.w_FLDEFI<>"A"
      * --- Variabili per Stampa Liquidazione
      L_FLPROR=IIF(this.w_TIPLIQ="R", " ", this.w_FLPROR)
      L_PERPRO=IIF(this.w_TIPLIQ="R", 0, this.w_PERPRO)
      L_FLAGRI=IIF(this.oParentObject.w_MTIPLIQ="R", " ", this.w_FLAGRI)
      this.w_PAGINE = 0
      * --- PER NUMERAZIONE PAGINE IN TESTATA
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
          from (i_cTable) where;
              AZCODAZI = this.oParentObject.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
        this.w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
        this.w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
        this.w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
        this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
        this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      L_PAGINE=0
      L_PRPARI=this.w_PRPARI
      L_INTLIG=this.w_INTLIG
      L_PREFIS=this.w_PREFIS
      L_INDAZI=this.w_INDAZI
      L_LOCAZI=this.w_LOCAZI
      L_CAPAZI=this.w_CAPAZI
      L_PROAZI=this.w_PROAZI
      L_COFAZI=this.w_COFAZI
      L_PIVAZI=this.w_PIVAZI
      if g_ATTIVI="S" AND this.oParentObject.w_MTIPLIQ<>"R"
        * --- Inserisco riga vuota con ORDINE = 2 per problemi di stampa. Raggruppamento su Ordine in pagina nuova stampa i totali.
        *     ORDINE = 1 le normali registraizoni Iva.
        SELECT * , 1 AS ORDINE FROM LIQIVA INTO CURSOR LIQIVA
        * --- Se 2002 o piu' Attivita' e non riepilogativa passa direttamente alla Liquidazione
        Select *, ORDINE AS ORDINE1, SPACE(35) AS D1, SPACE(35) AS D2, SPACE(35) AS D3, SPACE(35) AS D4 ; 
 From LIQIVA Into Cursor __Tmp__ NoFilter
        * --- Stampa Normale (Prima Solo Testo seconda Grafica)
        this.w_CONFER = "S"
        L_DICHGRU1=this.w_TIPDIC
        L_VPIMPOR1=this.w_VPIMPOR1
        L_VPIMPOR2=this.w_VPIMPOR2
        L_VPCESINT= this.w_VPCESINT
        L_VPACQINT= this.w_VPACQINT
        L_VPIMPON3= this.w_VPIMPON3
        L_VPIMPOS3= this.w_VPIMPOS3
        L_VPIMPOR5= this.w_VPIMPOR5
        L_VPIMPOR6C= this.w_VPIMPOR6C
        L_VPIMPOR6= this.w_VPIMPOR6
        L_VPIMPOR7= this.w_VPIMPOR7
        L_VPIMPOR8= this.w_VPIMPOR8
        L_VPIMPOR9= this.w_VPIMPOR9
        L_VPIMPO10=this.w_VPIMPO10
        L_VPIMPO11=this.w_VPIMPO11
        L_VPIMPO12=this.w_VPIMPO12
        L_VPIMPO13=this.w_VPIMPO13
        L_VPIMPO14=this.w_VPIMPO14
        L_VPIMPO15= this.w_VPIMPO15
        L_VPIMPO16= this.w_VPIMPO16
        L_VPOPNOIM=this.w_VPOPNOIM
        L_VPPAR74C4=this.w_VPAR74C4
        L_VPVARIMP=this.w_VPVARIMP
        L_VPVERNEF=this.w_VPVERNEF
        L_VPCORTER=this.w_VPCORTER
        L_VPACQBEA=this.w_VPACQBEA
        L_IMPVAL=g_PERVAL
        L_VPCODFIS=this.w_VPCODFIS
        L_VPOPMEDI=this.w_VPOPMEDI
        L_VPCODCAR=this.w_VPCODCAR
        L_VPCRERIM=this.w_VPCRERIM
        L_VPCREUTI= this.w_VPCREUTI
        L_VPEURO19=this.w_VPEURO19
        L_VPEURO17=this.w_VPVEREUR
        L_VPIMPVER=this.w_VPIMPVER
        L_ANNO=this.oParentObject.w_ANNO
        L_ATNUMREG= this.w_PRINUM
        L_ATTIPREG= this.w_PRIREG
        * --- Aggiungo in append al cursore __tmp__ i records per il riepilogo...
         
 SELECT __tmp__ 
 WRCURSOR("__tmp__") 
        APPEND BLANK
        REPLACE ORDINE1 WITH 2,DESCRI WITH ah_MsgFormat("Liquidazione %1",L_PERIODO)
        APPEND BLANK
        REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("Totale IVA a debito"),TOTIMP WITH L_VPIMPOR5
        APPEND BLANK
        if L_FLPROR="S"
          REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("Totale IVA a credito prorata di detraibilit� (%1%)",ALLTRIM(str(L_PERPRO,6,IIF(L_PERPRO=INT(L_PERPRO),0,2)))),TOTIMP WITH L_VPIMPOR6
        else
          REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("Totale IVA a credito"),TOTIMP WITH L_VPIMPOR6
        endif
        APPEND BLANK
        if L_VPIMPOR8<0
          REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("Variazione d'imposta a credito"),TOTIMP WITH ABS(L_VPIMPOR8)
        else
          REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("Variazione d'imposta a debito"),TOTIMP WITH ABS(L_VPIMPOR8)
        endif
        APPEND BLANK
        if L_VPIMPOR9>=0
          REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("IVA non versata periodi precedenti"),TOTIMP WITH ABS(L_VPIMPOR9)
        else
          REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("IVA in eccesso"),TOTIMP WITH ABS(L_VPIMPOR9)
        endif
        APPEND BLANK
        if L_VPIMPO10>=0
          REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("Debito art. 27 dpr. 633"),TOTIMP WITH ABS(L_VPIMPO10)
        else
          REPLACE ORDINE1 WITH 3, DESCRI WITH Ah_MsgFormat("Credito Iva precedente"), TOTIMP WITH ABS(L_VPIMPO10)
        endif
        APPEND BLANK
        REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("Credito IVA compensabile"),TOTIMP WITH L_VPIMPO11
        APPEND BLANK
        if L_VPIMPO12<0
          REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("IVA credito del periodo"),TOTIMP WITH ABS(L_VPIMPO12)
        else
          REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("IVA dovuta del periodo"),TOTIMP WITH ABS(L_VPIMPO12)
        endif
        APPEND BLANK
        REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("Crediti speciali"),TOTIMP WITH L_VPIMPO13
        APPEND BLANK
        REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("Acconto IVA gi� versato"),TOTIMP WITH L_VPIMPO15
        APPEND BLANK
        if L_VPIMPO12-(L_VPIMPO13+L_VPIMPO15)>=0
          REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("Totale IVA a debito"),TOTIMP WITH ABS(L_VPIMPO12-(L_VPIMPO13+L_VPIMPO15))
        else
          REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("Totale IVA a credito"),TOTIMP WITH ABS(L_VPIMPO12-(L_VPIMPO13+L_VPIMPO15))
        endif
        if g_TIPDEN<>"M"
          APPEND BLANK
          REPLACE ORDINE1 WITH 3,DESCRI WITH ah_MsgFormat("Interessi trimestrali"),TOTIMP WITH L_VPIMPO14
        endif
        if L_DICHGRU1="S"
          this.w_DESCRI = ah_MsgFormat("Il saldo � trasferito alla societ� controllante")
        else
          if L_VPIMPVER=0
            if (g_TIPDEN="T" AND this.oParentObject.w_NUMPER=4) OR (g_TIPDEN="M" AND this.oParentObject.w_NUMPER=12)
              this.w_DESCRI = ""
            else
              if (L_VPIMPO12 - (L_VPIMPO13 + L_VPIMPO15) >= 0) AND L_VPIMPO12<>0
                this.w_DESCRI = ah_MsgFormat("Imp. da trasferire a periodo succ.")
              else
                this.w_DESCRI = ""
              endif
            endif
          else
            this.w_DESCRI = ah_MsgFormat("Importo da versare")
          endif
        endif
        if NOT EMPTY(this.w_DESCRI)
          APPEND BLANK
          REPLACE ORDINE1 WITH 3, DESCRI WITH this.w_DESCRI, TOTIMP WITH L_VPIMPO16
        endif
        APPEND BLANK
        REPLACE ORDINE1 WITH 4,DESCRI WITH ah_MsgFormat("Credito chiesto a rimborso"),TOTIMP WITH L_VPCRERIM
        APPEND BLANK
        REPLACE ORDINE1 WITH 4,DESCRI WITH ah_MsgFormat("Credito da utilizzare per comp. mod. F24"),TOTIMP WITH L_VPCREUTI
        APPEND BLANK
        REPLACE ORDINE1 WITH 5,D1 WITH ah_MsgFormat("Descrizione ABI della banca:"),D2 WITH IIF(TYPE("L_DESABI")<>"U",L_DESABI," "),D3 WITH ah_MsgFormat("Cod. ABI:"),D4 WITH IIF(TYPE("L_ABI")<>"U",L_ABI," ")
        APPEND BLANK
        REPLACE ORDINE1 WITH 5,D1 WITH ah_MsgFormat("Agenzia:"),D2 WITH IIF(TYPE("L_DESFIL")<>"U",L_DESFIL," "),D3 WITH ah_MsgFormat("Cod. CAB:"),D4 WITH IIF(TYPE("L_CAB")<>"U",L_CAB," ")
        APPEND BLANK
        REPLACE ORDINE1 WITH 5,D1 WITH ah_MsgFormat("Indirizzo:"),D2 WITH IIF(TYPE("L_INDIRI")<>"U",L_INDIRI," "),D3 WITH "CAP:",D4 WITH IIF(TYPE("L_CAP")<>"U",L_CAP," ")
        if L_VPIMPVER>0
          APPEND BLANK
          REPLACE ORDINE1 WITH 6,D1 WITH ah_MsgFormat("Data versamento:"),D2 WITH IIF(TYPE("L_DATVERS")<>"U",DTOC(L_DATVERS)," "),D3 WITH ah_MsgFormat("Codice concessione:"),D4 WITH IIF(TYPE("L_CONCES")<>"U",L_CONCES," ")
        endif
        if this.oParentObject.w_STAREG="V"
          if this.oParentObject.w_MFLTEST="S"
            Cp_ChPrn("QUERY\LIQIVA1.FRX", " ", this)
            this.w_PAGINE = L_PAGINE+this.w_PRPARI
          else
            Cp_ChPrn("QUERY\LIQIVA.FRX", " ", this)
            this.w_PAGINE = L_PAGINE+this.w_PRPARI
          endif
        else
          if this.oParentObject.w_MFLTEST="S"
            Cp_ChPrn("QUERY\LIQIVA1O.FRX", " ", this)
            this.w_PAGINE = L_PAGINE+this.w_PRPARI
          else
            Cp_ChPrn("QUERY\LIQIVAO.FRX", " ", this)
            this.w_PAGINE = L_PAGINE+this.w_PRPARI
          endif
        endif
      else
        this.w_VPDATVER = IIF(this.w_VPIMPVER>0, this.w_VPDATVER, cp_CharToDate("  -  -  "))
        if VAL(this.oParentObject.w_ANNO)>2001
          if this.oParentObject.w_FLDEFI="R"
            L_IMPVAL=g_PERVAL
            L_DESBAN=""
            L_DESABI=this.w_DESABI
            L_DESFIL=this.w_DESFIL
            L_INDIRI=this.w_INDIRI
            L_CAP=this.w_CAP
            GSCG_BDV(this,"B")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            do gscg_sd2 with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          do gscg_sim with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Conto Vendite su Corrispettivi
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_PNFLSALF = iif(this.w_FLSALF = "S", "+", " ")
    this.w_PNFLSALI = iif(this.w_FLSALI = "S", "+", " ")
    if this.w_IMPORTODAR>=0
      this.w_IMPDAR = this.w_IMPORTODAR
      this.w_IMPAVE = 0
    else
      this.w_IMPAVE = -this.w_IMPORTODAR
      this.w_IMPDAR = 0
    endif
    this.w_TOTALE = this.w_TOTALE+this.w_IMPORTODAR
    if this.w_PNFLSALF<>" " OR this.w_PNFLSALI<>" "
      * --- Verifica se Conto Transitorio (Non aggiorna Saldi Iniziale/Finale)
      this.w_CONSUP = SPACE(15)
      this.w_SEZBIL = " "
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCONSUP"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CONTO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCONSUP;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPCON;
              and ANCODICE = this.w_CONTO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCSEZBIL"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCSEZBIL;
          from (i_cTable) where;
              MCCODICE = this.w_CONSUP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEZBIL = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SEZBIL="T"
        this.w_PNFLSALF = " "
        this.w_PNFLSALI = " "
      endif
    endif
    if Not EMPTY(this.w_CCDESRIG)
      this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
    endif
    * --- FARE UNA NUOVA VARIABILE PER LA CAUSALE DI RIGA
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNCAURIG"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+",PNDESRIG"+",PNFLPART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONTO),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LCODCAU),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALI),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALF),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_TIPCON,'PNCODCON',this.w_CONTO,'PNIMPDAR',this.w_IMPDAR,'PNIMPAVE',this.w_IMPAVE,'PNCAURIG',this.w_LCODCAU,'PNFLSALD',this.w_PNFLSALD,'PNFLSALI',this.w_PNFLSALI,'PNFLSALF',this.w_PNFLSALF,'PNDESRIG',this.w_PNDESRIG)
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNCAURIG,PNFLSALD,PNFLSALI,PNFLSALF,PNDESRIG,PNFLPART &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_TIPCON;
           ,this.w_CONTO;
           ,this.w_IMPDAR;
           ,this.w_IMPAVE;
           ,this.w_LCODCAU;
           ,this.w_PNFLSALD;
           ,this.w_PNFLSALI;
           ,this.w_PNFLSALF;
           ,this.w_PNDESRIG;
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Fallita Inserzione Dettaglio Prima Nota'
      return
    endif
    * --- Aggiorna Saldo
    * --- Try
    local bErr_049072F8
    bErr_049072F8=bTrsErr
    this.Try_049072F8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_049072F8
    * --- End
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_cOp3=cp_SetTrsOp(this.w_PNFLSALI,'SLDARINI','this.w_IMPDAR',this.w_IMPDAR,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_PNFLSALI,'SLAVEINI','this.w_IMPAVE',this.w_IMPAVE,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_PNFLSALF,'SLDARFIN','this.w_IMPDAR',this.w_IMPDAR,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_PNFLSALF,'SLAVEFIN','this.w_IMPAVE',this.w_IMPAVE,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_IMPDAR);
      +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_IMPAVE);
      +",SLDARINI ="+cp_NullLink(i_cOp3,'SALDICON','SLDARINI');
      +",SLAVEINI ="+cp_NullLink(i_cOp4,'SALDICON','SLAVEINI');
      +",SLDARFIN ="+cp_NullLink(i_cOp5,'SALDICON','SLDARFIN');
      +",SLAVEFIN ="+cp_NullLink(i_cOp6,'SALDICON','SLAVEFIN');
          +i_ccchkf ;
      +" where ";
          +"SLTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
          +" and SLCODICE = "+cp_ToStrODBC(this.w_CONTO);
          +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
             )
    else
      update (i_cTable) set;
          SLDARPER = SLDARPER + this.w_IMPDAR;
          ,SLAVEPER = SLAVEPER + this.w_IMPAVE;
          ,SLDARINI = &i_cOp3.;
          ,SLAVEINI = &i_cOp4.;
          ,SLDARFIN = &i_cOp5.;
          ,SLAVEFIN = &i_cOp6.;
          &i_ccchkf. ;
       where;
          SLTIPCON = this.w_TIPCON;
          and SLCODICE = this.w_CONTO;
          and SLCODESE = this.w_PNCODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Fallita Modifica Saldi Contabili'
      return
    endif
  endproc
  proc Try_049072F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONTO),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_TIPCON,'SLCODICE',this.w_CONTO,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_TIPCON;
           ,this.w_CONTO;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PNCODESE = g_CODESE
    this.w_PNCOMPET = g_CODESE
    this.w_PNFLSALD = "+"
    this.w_UTCC = i_CODUTE
    this.w_UTDC = SetInfoDate( g_CALUTD )
    this.w_UTCV = 0
    this.w_UTDV = cp_CharToDate("  -  -    ")
    this.w_PNCODVAL = g_PERVAL
    this.w_PNCAOVAL = g_CAOVAL
    this.w_PNVALNAZ = g_PERVAL
    this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
    this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
    * --- Descrizione Parametrica
    * --- Read from CAU_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCDESSUP,CCDESRIG"+;
        " from "+i_cTable+" CAU_CONT where ";
            +"CCCODICE = "+cp_ToStrODBC(this.w_CODCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCDESSUP,CCDESRIG;
        from (i_cTable) where;
            CCCODICE = this.w_CODCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
      this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Empty(this.w_PNDESSUP) and (Not Empty(this.w_CCDESSUP) OR Not Empty(this.w_CCDESRIG))
      * --- Array elenco parametri per descrizioni di riga e testata
       
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]="" 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]="" 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]="" 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]="" 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]="" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=""
      this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
    endif
    * --- Calcola PNSERIAL e PNNUMRER
    this.w_PNSERIAL = SPACE(10)
    this.w_PNNUMRER = 0
    this.w_CPROWNUM = 0
    this.w_CPROWORD = 0
    i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
    cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
    * --- Scrive Master Registrazione Contabile
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",PNCODESE"+",PNCODUTE"+",PNNUMRER"+",PNDATREG"+",PNCODCAU"+",PNCOMPET"+",PNTIPREG"+",PNVALNAZ"+",PNCODVAL"+",PNCAOVAL"+",PNPRG"+",PNDESSUP"+",PNFLPROV"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LCODCAU),'PNT_MAST','PNCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'PNT_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'PNT_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'PNT_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'PNT_MAST','UTDV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNCODESE',this.w_PNCODESE,'PNCODUTE',this.w_PNCODUTE,'PNNUMRER',this.w_PNNUMRER,'PNDATREG',this.w_PNDATREG,'PNCODCAU',this.w_LCODCAU,'PNCOMPET',this.w_PNCOMPET,'PNTIPREG',"N",'PNVALNAZ',this.w_PNVALNAZ,'PNCODVAL',this.w_PNCODVAL,'PNCAOVAL',this.w_PNCAOVAL,'PNPRG',this.w_PNPRG)
      insert into (i_cTable) (PNSERIAL,PNCODESE,PNCODUTE,PNNUMRER,PNDATREG,PNCODCAU,PNCOMPET,PNTIPREG,PNVALNAZ,PNCODVAL,PNCAOVAL,PNPRG,PNDESSUP,PNFLPROV,UTCC,UTCV,UTDC,UTDV &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_PNCODESE;
           ,this.w_PNCODUTE;
           ,this.w_PNNUMRER;
           ,this.w_PNDATREG;
           ,this.w_LCODCAU;
           ,this.w_PNCOMPET;
           ,"N";
           ,this.w_PNVALNAZ;
           ,this.w_PNCODVAL;
           ,this.w_PNCAOVAL;
           ,this.w_PNPRG;
           ,this.w_PNDESSUP;
           ,"N";
           ,this.w_UTCC;
           ,this.w_UTCV;
           ,this.w_UTDC;
           ,this.w_UTDV;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Fallita Inserzione Testata Prima Nota'
      return
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,24)]
    this.cWorkTables[1]='ATTIDETT'
    this.cWorkTables[2]='ATTIMAST'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='BAN_CHE'
    this.cWorkTables[5]='CREDDIVA'
    this.cWorkTables[6]='CREDMIVA'
    this.cWorkTables[7]='IVA_PERI'
    this.cWorkTables[8]='PNT_DETT'
    this.cWorkTables[9]='PNT_MAST'
    this.cWorkTables[10]='PRI_DETT'
    this.cWorkTables[11]='PRI_MAST'
    this.cWorkTables[12]='PRO_RATA'
    this.cWorkTables[13]='SALDICON'
    this.cWorkTables[14]='CONTI'
    this.cWorkTables[15]='MASTRI'
    this.cWorkTables[16]='DAT_IVAN'
    this.cWorkTables[17]='COD_ABI'
    this.cWorkTables[18]='COD_CAB'
    this.cWorkTables[19]='*PRI_TEMP'
    this.cWorkTables[20]='CAUIVA1'
    this.cWorkTables[21]='VOCIIVA'
    this.cWorkTables[22]='ATT_ALTE'
    this.cWorkTables[23]='DOC_DETT'
    this.cWorkTables[24]='CAU_CONT'
    return(this.OpenAllTables(24))

  proc CloseCursors()
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_ATTIMAST')
      use in _Curs_ATTIMAST
    endif
    if used('_Curs_ATT_ALTE')
      use in _Curs_ATT_ALTE
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_CREDDIVA')
      use in _Curs_CREDDIVA
    endif
    if used('_Curs_gscg2blp')
      use in _Curs_gscg2blp
    endif
    if used('_Curs_CREDDIVA')
      use in _Curs_CREDDIVA
    endif
    if used('_Curs_gscg1bia')
      use in _Curs_gscg1bia
    endif
    if used('_Curs_gscg_bia')
      use in _Curs_gscg_bia
    endif
    if used('_Curs_gscg3bia')
      use in _Curs_gscg3bia
    endif
    if used('_Curs_gscg2bia')
      use in _Curs_gscg2bia
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
