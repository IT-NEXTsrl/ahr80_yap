* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kml                                                        *
*              Manutenzione listini                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_303]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-26                                                      *
* Last revis.: 2013-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kml",oParentObject))

* --- Class definition
define class tgsma_kml as StdForm
  Top    = 11
  Left   = 24

  * --- Standard Properties
  Width  = 693
  Height = 510+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-12-05"
  HelpContextID=68519063
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=33

  * --- Constant Properties
  _IDX = 0
  DOC_DETT_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  SALDIART_IDX = 0
  UNIMIS_IDX = 0
  MAGAZZIN_IDX = 0
  VALUTE_IDX = 0
  FAM_ARTI_IDX = 0
  CATEGOMO_IDX = 0
  GRUMERC_IDX = 0
  VOCIIVA_IDX = 0
  LISTINI_IDX = 0
  ESERCIZI_IDX = 0
  LINGUE_IDX = 0
  cPrg = "gsma_kml"
  cComment = "Manutenzione listini"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODARTIN = space(20)
  w_CODARTFI = space(20)
  w_DESART = space(40)
  w_DESART1 = space(40)
  w_CODIVA = space(5)
  w_IVDESCR = space(35)
  w_GRUMERC = space(5)
  w_CATOMO = space(5)
  w_FAMIGLIA = space(5)
  w_GDESCRI = space(35)
  w_CTDESCRI = space(35)
  w_FDESCRI = space(35)
  w_LISTINO = space(5)
  w_LIDESCRI = space(40)
  w_DATVAL = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DATOBSOL = ctod('  /  /  ')
  w_CODLIS = space(10)
  w_DATATT = ctod('  /  /  ')
  w_DATDIS = ctod('  /  /  ')
  w_CODART = space(20)
  o_CODART = space(20)
  w_ROWNUM = 0
  o_ROWNUM = 0
  w_FLSCO = space(1)
  w_DESCRI = space(35)
  w_SOVRAPPO = space(1)
  w_OREP = space(50)
  w_OQRY = space(50)
  w_ONUME = 0
  w_tipolingua = space(3)
  w_Desclingua = space(30)
  w_UNIMIS = space(10)
  w_SAVAUT = space(10)

  * --- Children pointers
  GSMA_MLG = .NULL.
  w_ZoomLis = .NULL.
  w_ZoomArt = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsma_kml
  * Disabilito la croce in alto a destra, per evitare la
  * chiamata alla Queryunload che provocherebbe la chiamata
  * alla SAVE_gsma_mlg con conseguente messaggio (Salva modifiche..)
  closable=.f.
  
  * Serve per bloccare il messaggio se si preme esc
  * Nella ecpQuit() cFunction vale Filter e con la propriet� assign
  * la cFunction assume questo valore
  * Testando xvalue viene data la possibilit� di modificare il valore delle propriet�
  PROC cFunction_assign(xvalue)
     this.cFunction=xvalue
       if xvalue='Filter'
         this.GSMA_MLG.bUpdated=.f.
       endif
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSMA_MLG additive
    with this
      .Pages(1).addobject("oPag","tgsma_kmlPag1","gsma_kml",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni principali")
      .Pages(2).addobject("oPag","tgsma_kmlPag2","gsma_kml",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Ulteriori selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLISTINO_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSMA_MLG
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomLis = this.oPgFrm.Pages(1).oPag.ZoomLis
    this.w_ZoomArt = this.oPgFrm.Pages(1).oPag.ZoomArt
    DoDefault()
    proc Destroy()
      this.w_ZoomLis = .NULL.
      this.w_ZoomArt = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='SALDIART'
    this.cWorkTables[5]='UNIMIS'
    this.cWorkTables[6]='MAGAZZIN'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='FAM_ARTI'
    this.cWorkTables[9]='CATEGOMO'
    this.cWorkTables[10]='GRUMERC'
    this.cWorkTables[11]='VOCIIVA'
    this.cWorkTables[12]='LISTINI'
    this.cWorkTables[13]='ESERCIZI'
    this.cWorkTables[14]='LINGUE'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSMA_MLG = CREATEOBJECT('stdDynamicChild',this,'GSMA_MLG',this.oPgFrm.Page1.oPag.oLinkPC_1_25)
    this.GSMA_MLG.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSMA_MLG)
      this.GSMA_MLG.DestroyChildrenChain()
      this.GSMA_MLG=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_25')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMA_MLG.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMA_MLG.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMA_MLG.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSMA_MLG.SetKey(;
            .w_CODART,"LICODART";
            ,.w_ROWNUM,"LIROWNUM";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSMA_MLG.ChangeRow(this.cRowID+'      1',1;
             ,.w_CODART,"LICODART";
             ,.w_ROWNUM,"LIROWNUM";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSMA_MLG)
        i_f=.GSMA_MLG.BuildFilter()
        if !(i_f==.GSMA_MLG.cQueryFilter)
          i_fnidx=.GSMA_MLG.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSMA_MLG.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSMA_MLG.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSMA_MLG.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSMA_MLG.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSMA_MLG(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSMA_MLG.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSMA_MLG(i_ask)
    if this.GSMA_MLG.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (Scaglioni listini)'))
      cp_BeginTrs()
      this.GSMA_MLG.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODARTIN=space(20)
      .w_CODARTFI=space(20)
      .w_DESART=space(40)
      .w_DESART1=space(40)
      .w_CODIVA=space(5)
      .w_IVDESCR=space(35)
      .w_GRUMERC=space(5)
      .w_CATOMO=space(5)
      .w_FAMIGLIA=space(5)
      .w_GDESCRI=space(35)
      .w_CTDESCRI=space(35)
      .w_FDESCRI=space(35)
      .w_LISTINO=space(5)
      .w_LIDESCRI=space(40)
      .w_DATVAL=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DATOBSOL=ctod("  /  /  ")
      .w_CODLIS=space(10)
      .w_DATATT=ctod("  /  /  ")
      .w_DATDIS=ctod("  /  /  ")
      .w_CODART=space(20)
      .w_ROWNUM=0
      .w_FLSCO=space(1)
      .w_DESCRI=space(35)
      .w_SOVRAPPO=space(1)
      .w_OREP=space(50)
      .w_OQRY=space(50)
      .w_ONUME=0
      .w_tipolingua=space(3)
      .w_Desclingua=space(30)
      .w_UNIMIS=space(10)
      .w_SAVAUT=space(10)
      .oPgFrm.Page1.oPag.ZoomLis.Calculate()
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODARTIN))
          .link_2_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODARTFI))
          .link_2_2('Full')
        endif
        .DoRTCalc(3,5,.f.)
        if not(empty(.w_CODIVA))
          .link_2_7('Full')
        endif
        .DoRTCalc(6,7,.f.)
        if not(empty(.w_GRUMERC))
          .link_2_10('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CATOMO))
          .link_2_11('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_FAMIGLIA))
          .link_2_12('Full')
        endif
        .DoRTCalc(10,13,.f.)
        if not(empty(.w_LISTINO))
          .link_1_5('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomArt.Calculate()
          .DoRTCalc(14,14,.f.)
        .w_DATVAL = i_DATSYS
        .w_OBTEST = i_datsys
          .DoRTCalc(17,18,.f.)
        .w_CODLIS = .w_ZoomLis.getVar('LICODLIS')
        .w_DATATT = .w_ZoomLis.getVar('LIDATATT')
        .w_DATDIS = .w_ZoomLis.getVar('LIDATDIS')
        .w_CODART = .w_ZoomArt.getVar('ARCODART')
        .w_ROWNUM = .w_ZoomArt.getVar('LIROWNUM')
        .w_FLSCO = .w_ZoomLis.getVar('LSFLSCON')
      .GSMA_MLG.NewDocument()
      .GSMA_MLG.ChangeRow('1',1,.w_CODART,"LICODART",.w_ROWNUM,"LIROWNUM")
      if not(.GSMA_MLG.bLoaded)
        .GSMA_MLG.SetKey(.w_CODART,"LICODART",.w_ROWNUM,"LIROWNUM")
      endif
        .w_DESCRI = .w_ZoomArt.getVar('ARDESART')
        .w_SOVRAPPO = 'N'
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate('',RGB(255,255,0),RGB(255,255,0))
        .w_OREP = "QUERY\GSMA_KML.FRX"
        .w_OQRY = "QUERY\GSMA3KML.VQR"
          .DoRTCalc(29,29,.f.)
        .w_tipolingua = G_CODLIN
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_tipolingua))
          .link_2_20('Full')
        endif
          .DoRTCalc(31,31,.f.)
        .w_UNIMIS = .w_ZoomLis.getVar('LIUNIMIS')
        .w_SAVAUT = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSMA_MLG.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSMA_MLG.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomLis.Calculate()
        .oPgFrm.Page1.oPag.ZoomArt.Calculate()
        .DoRTCalc(1,18,.t.)
            .w_CODLIS = .w_ZoomLis.getVar('LICODLIS')
            .w_DATATT = .w_ZoomLis.getVar('LIDATATT')
            .w_DATDIS = .w_ZoomLis.getVar('LIDATDIS')
            .w_CODART = .w_ZoomArt.getVar('ARCODART')
            .w_ROWNUM = .w_ZoomArt.getVar('LIROWNUM')
            .w_FLSCO = .w_ZoomLis.getVar('LSFLSCON')
            .w_DESCRI = .w_ZoomArt.getVar('ARDESART')
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate('',RGB(255,255,0),RGB(255,255,0))
        .DoRTCalc(26,31,.t.)
            .w_UNIMIS = .w_ZoomLis.getVar('LIUNIMIS')
        * --- Area Manuale = Calculate
        * --- gsma_kml
        **spostata questa area in calc per disabilitare la chiamata al salvataggio 
        **di scaglioni listini quando si scorre lo zoom
                if .w_CODART<>.o_CODART or .w_ROWNUM<>.o_ROWNUM
                 if .w_SAVAUT='S'
                   .Save_GSMA_MLG(.f.)
                 Endif
                  .GSMA_MLG.NewDocument()
                  .GSMA_MLG.ChangeRow('1',1,.w_CODART,"LICODART",.w_ROWNUM,"LIROWNUM")
                  if not(.GSMA_MLG.bLoaded)
                    .GSMA_MLG.SetKey(.w_CODART,"LICODART",.w_ROWNUM,"LIROWNUM")
                  endif
                endif
              
              this.SetControlsValue()
              if inlist(this.cFunction,'Edit','Load')
                this.bUpdated=.t.
                this.mEnableControls()
              endif
            
            this.bCalculating=.f.
          return
        * --- Fine Area Manuale
        if .w_CODART<>.o_CODART or .w_ROWNUM<>.o_ROWNUM
          .Save_GSMA_MLG(.t.)
          .GSMA_MLG.NewDocument()
          .GSMA_MLG.ChangeRow('1',1,.w_CODART,"LICODART",.w_ROWNUM,"LIROWNUM")
          if not(.GSMA_MLG.bLoaded)
            .GSMA_MLG.SetKey(.w_CODART,"LICODART",.w_ROWNUM,"LIROWNUM")
          endif
        endif
      endwith
      this.DoRTCalc(33,33,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomLis.Calculate()
        .oPgFrm.Page1.oPag.ZoomArt.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate('',RGB(255,255,0),RGB(255,255,0))
    endwith
  return

  proc Calculate_MDFVHUBRMM()
    with this
          * --- Lancia zoom articoli
          Esegui(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODART_1_21.visible=!this.oPgFrm.Page1.oPag.oCODART_1_21.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI_1_26.visible=!this.oPgFrm.Page1.oPag.oDESCRI_1_26.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsma_kml
    *salva le modifiche del figlio ma non chiude la maschera
    If cevent="Salva"
        if this.TerminateEdit() and this.CheckForm()
          this.Save_GSMA_MLG(.f.)
          this.GSMA_MLG.NewDocument()
          this.GSMA_MLG.ChangeRow('1',1,this.w_CODART,"LICODART",this.w_ROWNUM,"LIROWNUM")
          if not(this.GSMA_MLG.bLoaded)
             this.GSMA_MLG.SetKey(this.w_CODART,"LICODART",this.w_ROWNUM,"LIROWNUM")
          endif
          this.lockscreen=.t.
          this.mReplace(.t.)
          this.lockscreen=.f.
          this.bupdated=.f.
          this.GSMA_MLG.bupdated=.f.
          if not empty(this.w_codart)
            ah_ErrorMsg("Aggiornamento listino effettuato")
          endif
        endif
    Endif
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_zoomlis selected")
          .Calculate_MDFVHUBRMM()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.ZoomLis.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomArt.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODARTIN
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODARTIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODARTIN))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARTIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODARTIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODARTIN)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODARTIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODARTIN_2_1'),i_cWhere,'',"Elenco articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODARTIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODARTIN)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARTIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODARTIN = space(20)
      endif
      this.w_DESART = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_CODARTFI)) OR  (.w_CODARTIN<=.w_CODARTFI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � superiore al codice finale oppure obsoleto")
        endif
        this.w_CODARTIN = space(20)
        this.w_DESART = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODARTFI
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARTFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODARTFI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODARTFI))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARTFI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODARTFI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODARTFI)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODARTFI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODARTFI_2_2'),i_cWhere,'',"Elenco articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARTFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODARTFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODARTFI)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARTFI = NVL(_Link_.ARCODART,space(20))
      this.w_DESART1 = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODARTFI = space(20)
      endif
      this.w_DESART1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_CODARTFI>=.w_CODARTIN) or (empty(.w_CODARTIN))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � superiore al codice finale oppure obsoleto")
        endif
        this.w_CODARTFI = space(20)
        this.w_DESART1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARTFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_CODIVA))
          select IVCODIVA,IVDESIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_CODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCODIVA_2_7'),i_cWhere,'GSAR_AIV',"Voci IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_IVDESCR = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_IVDESCR = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODIVA = space(5)
        this.w_IVDESCR = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMERC
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMERC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMERC)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMERC))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMERC)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( GMDESCRI like "+cp_ToStrODBC(trim(this.w_GRUMERC)+"%")+cp_TransWhereFldName('GMDESCRI',trim(this.w_GRUMERC))+")";

            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GMDESCRI like "+cp_ToStr(trim(this.w_GRUMERC)+"%");

            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GRUMERC) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMERC_2_10'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMERC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMERC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMERC)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMERC = NVL(_Link_.GMCODICE,space(5))
      this.w_GDESCRI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMERC = space(5)
      endif
      this.w_GDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMERC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATOMO
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATOMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATOMO)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATOMO))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATOMO)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( OMDESCRI like "+cp_ToStrODBC(trim(this.w_CATOMO)+"%")+cp_TransWhereFldName('OMDESCRI',trim(this.w_CATOMO))+")";

            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" OMDESCRI like "+cp_ToStr(trim(this.w_CATOMO)+"%");

            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATOMO) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATOMO_2_11'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATOMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATOMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATOMO)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATOMO = NVL(_Link_.OMCODICE,space(5))
      this.w_CTDESCRI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATOMO = space(5)
      endif
      this.w_CTDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATOMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMIGLIA
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMIGLIA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMIGLIA)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMIGLIA))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMIGLIA)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( FADESCRI like "+cp_ToStrODBC(trim(this.w_FAMIGLIA)+"%")+cp_TransWhereFldName('FADESCRI',trim(this.w_FAMIGLIA))+")";

            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStr(trim(this.w_FAMIGLIA)+"%");

            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FAMIGLIA) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMIGLIA_2_12'),i_cWhere,'GSAR_AFA',"Famiglia articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMIGLIA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMIGLIA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMIGLIA)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMIGLIA = NVL(_Link_.FACODICE,space(5))
      this.w_FDESCRI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMIGLIA = space(5)
      endif
      this.w_FDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMIGLIA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LISTINO
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LISTINO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LISTINO)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LISTINO))
          select LSCODLIS,LSDESLIS,LSDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LISTINO)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LISTINO) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLISTINO_1_5'),i_cWhere,'GSAR_ALI',"Anagrafica listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LISTINO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LISTINO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LISTINO)
            select LSCODLIS,LSDESLIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LISTINO = NVL(_Link_.LSCODLIS,space(5))
      this.w_LIDESCRI = NVL(_Link_.LSDESLIS,space(40))
      this.w_DATOBSOL = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_LISTINO = space(5)
      endif
      this.w_LIDESCRI = space(40)
      this.w_DATOBSOL = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSOL) OR .w_DATOBSOL>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino da variare errato, obsoleto o inesistente")
        endif
        this.w_LISTINO = space(5)
        this.w_LIDESCRI = space(40)
        this.w_DATOBSOL = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LISTINO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=tipolingua
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_tipolingua) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_tipolingua)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_tipolingua))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_tipolingua)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_tipolingua) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'otipolingua_2_20'),i_cWhere,'',"",'LINGUE.LINGUE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_tipolingua)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_tipolingua);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_tipolingua)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_tipolingua = NVL(_Link_.LUCODICE,space(3))
      this.w_Desclingua = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_tipolingua = space(3)
      endif
      this.w_Desclingua = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_tipolingua Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page2.oPag.oCODARTIN_2_1.value==this.w_CODARTIN)
      this.oPgFrm.Page2.oPag.oCODARTIN_2_1.value=this.w_CODARTIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCODARTFI_2_2.value==this.w_CODARTFI)
      this.oPgFrm.Page2.oPag.oCODARTFI_2_2.value=this.w_CODARTFI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESART_2_5.value==this.w_DESART)
      this.oPgFrm.Page2.oPag.oDESART_2_5.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page2.oPag.oDESART1_2_6.value==this.w_DESART1)
      this.oPgFrm.Page2.oPag.oDESART1_2_6.value=this.w_DESART1
    endif
    if not(this.oPgFrm.Page2.oPag.oCODIVA_2_7.value==this.w_CODIVA)
      this.oPgFrm.Page2.oPag.oCODIVA_2_7.value=this.w_CODIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oIVDESCR_2_9.value==this.w_IVDESCR)
      this.oPgFrm.Page2.oPag.oIVDESCR_2_9.value=this.w_IVDESCR
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUMERC_2_10.value==this.w_GRUMERC)
      this.oPgFrm.Page2.oPag.oGRUMERC_2_10.value=this.w_GRUMERC
    endif
    if not(this.oPgFrm.Page2.oPag.oCATOMO_2_11.value==this.w_CATOMO)
      this.oPgFrm.Page2.oPag.oCATOMO_2_11.value=this.w_CATOMO
    endif
    if not(this.oPgFrm.Page2.oPag.oFAMIGLIA_2_12.value==this.w_FAMIGLIA)
      this.oPgFrm.Page2.oPag.oFAMIGLIA_2_12.value=this.w_FAMIGLIA
    endif
    if not(this.oPgFrm.Page2.oPag.oGDESCRI_2_16.value==this.w_GDESCRI)
      this.oPgFrm.Page2.oPag.oGDESCRI_2_16.value=this.w_GDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCTDESCRI_2_17.value==this.w_CTDESCRI)
      this.oPgFrm.Page2.oPag.oCTDESCRI_2_17.value=this.w_CTDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oFDESCRI_2_18.value==this.w_FDESCRI)
      this.oPgFrm.Page2.oPag.oFDESCRI_2_18.value=this.w_FDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oLISTINO_1_5.value==this.w_LISTINO)
      this.oPgFrm.Page1.oPag.oLISTINO_1_5.value=this.w_LISTINO
    endif
    if not(this.oPgFrm.Page1.oPag.oLIDESCRI_1_7.value==this.w_LIDESCRI)
      this.oPgFrm.Page1.oPag.oLIDESCRI_1_7.value=this.w_LIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATVAL_1_11.value==this.w_DATVAL)
      this.oPgFrm.Page1.oPag.oDATVAL_1_11.value=this.w_DATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_21.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_21.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_26.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_26.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSOVRAPPO_1_27.RadioValue()==this.w_SOVRAPPO)
      this.oPgFrm.Page1.oPag.oSOVRAPPO_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.otipolingua_2_20.value==this.w_tipolingua)
      this.oPgFrm.Page2.oPag.otipolingua_2_20.value=this.w_tipolingua
    endif
    if not(this.oPgFrm.Page2.oPag.oDesclingua_2_21.value==this.w_Desclingua)
      this.oPgFrm.Page2.oPag.oDesclingua_2_21.value=this.w_Desclingua
    endif
    if not(this.oPgFrm.Page1.oPag.oSAVAUT_1_34.RadioValue()==this.w_SAVAUT)
      this.oPgFrm.Page1.oPag.oSAVAUT_1_34.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_CODARTFI)) OR  (.w_CODARTIN<=.w_CODARTFI)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODARTIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODARTIN_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � superiore al codice finale oppure obsoleto")
          case   not(((.w_CODARTFI>=.w_CODARTIN) or (empty(.w_CODARTIN))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODARTFI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODARTFI_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � superiore al codice finale oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODIVA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODIVA_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_LISTINO)) or not((EMPTY(.w_DATOBSOL) OR .w_DATOBSOL>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLISTINO_1_5.SetFocus()
            i_bnoObbl = !empty(.w_LISTINO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino da variare errato, obsoleto o inesistente")
          case   (empty(.w_DATVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATVAL_1_11.SetFocus()
            i_bnoObbl = !empty(.w_DATVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_tipolingua))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.otipolingua_2_20.SetFocus()
            i_bnoObbl = !empty(.w_tipolingua)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSMA_MLG.CheckForm()
      if i_bres
        i_bres=  .GSMA_MLG.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODART = this.w_CODART
    this.o_ROWNUM = this.w_ROWNUM
    * --- GSMA_MLG : Depends On
    this.GSMA_MLG.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsma_kmlPag1 as StdContainer
  Width  = 689
  height = 511
  stdWidth  = 689
  stdheight = 511
  resizeXpos=485
  resizeYpos=325
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomLis as cp_zoombox with uid="IINBHLOGZY",left=-6, top=89, width=298,height=142,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSMA_KML",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="LIS_TINI",cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,;
    cEvent = "ZoomLisExec",;
    nPag=1;
    , HelpContextID = 21918746

  add object oLISTINO_1_5 as StdField with uid="YNJAORDGVJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_LISTINO", cQueryName = "LISTINO",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino da variare errato, obsoleto o inesistente",;
    ToolTipText = "Codice di listino da variare",;
    HelpContextID = 117375670,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=12, Top=31, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LISTINO"

  func oLISTINO_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oLISTINO_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLISTINO_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLISTINO_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Anagrafica listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oLISTINO_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_LISTINO
     i_obj.ecpSave()
  endproc


  add object ZoomArt as cp_zoombox with uid="XZQLVYBPSS",left=288, top=89, width=402,height=344,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSMA1KML",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="ART_ICOL",cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,;
    cEvent = "ZoomArtExec",;
    nPag=1;
    , HelpContextID = 21918746

  add object oLIDESCRI_1_7 as StdField with uid="VHVFQRRGPI",rtseq=14,rtrep=.f.,;
    cFormVar = "w_LIDESCRI", cQueryName = "LIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 57732353,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=73, Top=31, InputMask=replicate('X',40)

  add object oDATVAL_1_11 as StdField with uid="QGILLBYITL",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATVAL", cQueryName = "DATVAL",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data validit� di riferimento",;
    HelpContextID = 192869834,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=373, Top=31


  add object oBtn_1_13 as StdButton with uid="YBVSEVVVIR",left=639, top=47, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Ricerca listini validi";
    , HelpContextID = 22994154;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do Lanciazoom with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_LISTINO))
      endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="MWLMSVFRBH",left=225, top=466, width=48,height=45,;
    CpPicture="BMP\AUTO.BMP", caption="", nPag=1;
    , ToolTipText = "Aggiorna prezzi listini";
    , HelpContextID = 42639257;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        .NotifyEvent("Salva")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CODART))
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="WKCHZJVTVN",left=588, top=466, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 58126810;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_LISTINO))
      endwith
    endif
  endfunc

  add object oCODART_1_21 as StdField with uid="GWTHCHLOXW",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articolo selezionato",;
    HelpContextID = 42264538,;
   bGlobalFont=.t.,;
    Height=21, Width=112, Left=296, Top=438, InputMask=replicate('X',20)

  func oCODART_1_21.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODART))
    endwith
  endfunc


  add object oBtn_1_22 as StdButton with uid="KYQESPJULE",left=639, top=466, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75836486;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oLinkPC_1_25 as stdDynamicChildContainer with uid="ADTOCPVPZD",left=14, top=236, width=255, height=224, bOnScreen=.t.;


  add object oDESCRI_1_26 as StdField with uid="APLPCNQROG",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 226623946,;
   bGlobalFont=.t.,;
    Height=21, Width=269, Left=410, Top=438, InputMask=replicate('X',35)

  func oDESCRI_1_26.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODART))
    endwith
  endfunc

  add object oSOVRAPPO_1_27 as StdCheck with uid="TFDDGIQWVE",rtseq=26,rtrep=.f.,left=468, top=31, caption="Solo prezzi sovrapposti",;
    ToolTipText = "Se attivo: seleziona solo i prezzi sovrapposti alla data di riferimento",;
    HelpContextID = 126011019,;
    cFormVar="w_SOVRAPPO", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oSOVRAPPO_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSOVRAPPO_1_27.GetRadio()
    this.Parent.oContained.w_SOVRAPPO = this.RadioValue()
    return .t.
  endfunc

  func oSOVRAPPO_1_27.SetRadio()
    this.Parent.oContained.w_SOVRAPPO=trim(this.Parent.oContained.w_SOVRAPPO)
    this.value = ;
      iif(this.Parent.oContained.w_SOVRAPPO=='S',1,;
      0)
  endfunc


  add object oObj_1_29 as cp_calclbl with uid="PNKQPMDGQW",left=416, top=66, width=33,height=17,;
    caption='',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 68519158

  add object oSAVAUT_1_34 as StdCheck with uid="FQMTJVSHKF",rtseq=33,rtrep=.f.,left=16, top=473, caption="Salvataggio automatico",;
    ToolTipText = "Se attivo, consente il salvataggio degli scaglioni modificati in automatico senza l'utilizzo dell'apposito tasto di aggiornamento prezzo listini",;
    HelpContextID = 39048410,;
    cFormVar="w_SAVAUT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSAVAUT_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSAVAUT_1_34.GetRadio()
    this.Parent.oContained.w_SAVAUT = this.RadioValue()
    return .t.
  endfunc

  func oSAVAUT_1_34.SetRadio()
    this.Parent.oContained.w_SAVAUT=trim(this.Parent.oContained.w_SAVAUT)
    this.value = ;
      iif(this.Parent.oContained.w_SAVAUT=='S',1,;
      0)
  endfunc

  add object oStr_1_2 as StdString with uid="QFCPCCTGOJ",Visible=.t., Left=13, Top=72,;
    Alignment=0, Width=259, Height=18,;
    Caption="Listini - doppio click per selezionare"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_3 as StdString with uid="UMKYOCLPTE",Visible=.t., Left=299, Top=72,;
    Alignment=0, Width=115, Height=18,;
    Caption="Articoli"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="GPCTDMFHWZ",Visible=.t., Left=13, Top=7,;
    Alignment=0, Width=239, Height=15,;
    Caption="Listino da aggiornare"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="EWACOKHGOF",Visible=.t., Left=315, Top=31,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alla data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="WASNBKROBS",Visible=.t., Left=454, Top=72,;
    Alignment=0, Width=113, Height=17,;
    Caption="Prezzo sovrapposto"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="QWGHZDBBIA",Visible=.t., Left=-3, Top=560,;
    Alignment=0, Width=696, Height=18,;
    Caption="Attenzione, nell'area calculate ridefinito il calculate della chiave che consente la visualizzazione contestuale degli scaglioni listini"  ;
  , bGlobalFont=.t.

  add object oBox_1_10 as StdBox with uid="BNUIOSCOHT",left=30, top=24, width=643,height=2
enddefine
define class tgsma_kmlPag2 as StdContainer
  Width  = 689
  height = 511
  stdWidth  = 689
  stdheight = 511
  resizeXpos=353
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODARTIN_2_1 as StdField with uid="STYAQECGSX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODARTIN", cQueryName = "CODARTIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � superiore al codice finale oppure obsoleto",;
    ToolTipText = "Codice articolo/servizio di inizio selezione",;
    HelpContextID = 226170996,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=127, Top=20, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODARTIN"

  func oCODARTIN_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARTIN_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARTIN_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODARTIN_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco articoli",'',this.parent.oContained
  endproc

  add object oCODARTFI_2_2 as StdField with uid="VJWSSZAIJQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODARTFI", cQueryName = "CODARTFI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � superiore al codice finale oppure obsoleto",;
    ToolTipText = "Codice articolo/servizio di fine selezione",;
    HelpContextID = 42264465,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=127, Top=49, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODARTFI"

  func oCODARTFI_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARTFI_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARTFI_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODARTFI_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco articoli",'',this.parent.oContained
  endproc

  add object oDESART_2_5 as StdField with uid="XIGGDSMEYY",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42205642,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=284, Top=20, InputMask=replicate('X',40)

  add object oDESART1_2_6 as StdField with uid="UQMDLCZMPO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESART1", cQueryName = "DESART1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42205642,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=284, Top=49, InputMask=replicate('X',40)

  add object oCODIVA_2_7 as StdField with uid="QMIJMBDYFI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODIVA", cQueryName = "CODIVA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA articolo di selezione",;
    HelpContextID = 180557862,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=127, Top=78, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_CODIVA"

  func oCODIVA_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIVA_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIVA_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCODIVA_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'',this.parent.oContained
  endproc
  proc oCODIVA_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_CODIVA
     i_obj.ecpSave()
  endproc

  add object oIVDESCR_2_9 as StdField with uid="BOHSKZBYUS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_IVDESCR", cQueryName = "IVDESCR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 57729146,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=194, Top=78, InputMask=replicate('X',35)

  add object oGRUMERC_2_10 as StdField with uid="MPJRKBZWIT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_GRUMERC", cQueryName = "GRUMERC",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Aggiorna i listini del gruppo merceologico selezionato",;
    HelpContextID = 88593562,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=127, Top=107, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMERC"

  func oGRUMERC_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMERC_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMERC_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMERC_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oGRUMERC_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUMERC
     i_obj.ecpSave()
  endproc

  add object oCATOMO_2_11 as StdField with uid="ENAZMQDZCY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CATOMO", cQueryName = "CATOMO",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Aggiorna i listini della categoria omogenea selezionata",;
    HelpContextID = 130414042,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=127, Top=136, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATOMO"

  func oCATOMO_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATOMO_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATOMO_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATOMO_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCATOMO_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CATOMO
     i_obj.ecpSave()
  endproc

  add object oFAMIGLIA_2_12 as StdField with uid="KVJFFQQJPD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_FAMIGLIA", cQueryName = "FAMIGLIA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Aggiorna i listini della famiglia selezionata",;
    HelpContextID = 80976535,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=127, Top=165, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMIGLIA"

  func oFAMIGLIA_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMIGLIA_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMIGLIA_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMIGLIA_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglia articoli",'',this.parent.oContained
  endproc
  proc oFAMIGLIA_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMIGLIA
     i_obj.ecpSave()
  endproc

  add object oGDESCRI_2_16 as StdField with uid="BNUJWCXTTZ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_GDESCRI", cQueryName = "GDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 178068838,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=194, Top=107, InputMask=replicate('X',35)

  add object oCTDESCRI_2_17 as StdField with uid="CMMWUUMNTN",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CTDESCRI", cQueryName = "CTDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 57729681,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=194, Top=136, InputMask=replicate('X',35)

  add object oFDESCRI_2_18 as StdField with uid="DQMRSERNJJ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FDESCRI", cQueryName = "FDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 178068822,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=194, Top=165, InputMask=replicate('X',35)

  add object otipolingua_2_20 as StdField with uid="VQCGRTTKAY",rtseq=30,rtrep=.f.,;
    cFormVar = "w_tipolingua", cQueryName = "tipolingua",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Stampa descrizioni articoli nella lingua selezionata (se esistente)",;
    HelpContextID = 196321811,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=127, Top=195, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", oKey_1_1="LUCODICE", oKey_1_2="this.w_tipolingua"

  func otipolingua_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc otipolingua_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc otipolingua_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'otipolingua_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'LINGUE.LINGUE_VZM',this.parent.oContained
  endproc

  add object oDesclingua_2_21 as StdField with uid="UEHQVPMQTC",rtseq=31,rtrep=.f.,;
    cFormVar = "w_Desclingua", cQueryName = "Desclingua",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 197097747,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=194, Top=195, InputMask=replicate('X',30)

  add object oStr_2_3 as StdString with uid="WWWTRAIHUP",Visible=.t., Left=17, Top=20,;
    Alignment=1, Width=108, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="VXROUPAFHF",Visible=.t., Left=17, Top=49,;
    Alignment=1, Width=108, Height=15,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="IFMIEQVNIM",Visible=.t., Left=17, Top=78,;
    Alignment=1, Width=108, Height=15,;
    Caption="Cod. IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="NIRPFDDQPW",Visible=.t., Left=17, Top=107,;
    Alignment=1, Width=108, Height=15,;
    Caption="Gruppo merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="NYRMMNNGVA",Visible=.t., Left=17, Top=136,;
    Alignment=1, Width=108, Height=15,;
    Caption="Cat.omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="WUPLPUXSMQ",Visible=.t., Left=17, Top=165,;
    Alignment=1, Width=108, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="JRUHHAUNUP",Visible=.t., Left=38, Top=195,;
    Alignment=1, Width=87, Height=15,;
    Caption="Lingua:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kml','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsma_kml
*Esegue lo zoom articoli al doppio click sullo zoom listini
proc Esegui(pParent)
   pParent.NotifyEvent("ZoomArtExec")
    pParent.w_ZoomArt.Refresh()
    pParent.mCalc(.t.)
endproc

*Esegue lo zoom listini e zoom articoli alla pressione del bottone Ricerca
proc Lanciazoom(pParent)
   pParent.NotifyEvent("ZoomLisExec")
   pParent.w_ZoomLis.Refresh()
   if reccount(pParent.w_ZoomLis.cCursor)>0
    goto (1) IN (pParent.w_ZoomLis.cCursor)
    pParent.w_ZoomLis.grd.SetFocus()
    pParent.NotifyEvent("ZoomArtExec")
    pParent.w_ZoomArt.Refresh()
    pParent.mCalc(.t.)
   Else
    Ah_ErrorMsg("Per i filtri impostati non ci sono listini validi")
    pParent.NotifyEvent("ZoomArtExec")
    pParent.w_ZoomArt.Refresh()
    pParent.mCalc(.t.)
   endif
endproc

* --- Fine Area Manuale
