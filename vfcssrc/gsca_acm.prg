* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_acm                                                        *
*              Movimenti di analitica                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_13]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-11                                                      *
* Last revis.: 2014-12-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsca_acm"))

* --- Class definition
define class tgsca_acm as StdForm
  Top    = 8
  Left   = 7

  * --- Standard Properties
  Width  = 748
  Height = 363+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-29"
  HelpContextID=158694039
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  CDC_MANU_IDX = 0
  MOVICOST_IDX = 0
  ESERCIZI_IDX = 0
  cFile = "CDC_MANU"
  cKeySelect = "CMCODICE,CMCODSEC"
  cKeyWhere  = "CMCODICE=this.w_CMCODICE and CMCODSEC=this.w_CMCODSEC"
  cKeyWhereODBC = '"CMCODICE="+cp_ToStrODBC(this.w_CMCODICE)';
      +'+" and CMCODSEC="+cp_ToStrODBC(this.w_CMCODSEC)';

  cKeyWhereODBCqualified = '"CDC_MANU.CMCODICE="+cp_ToStrODBC(this.w_CMCODICE)';
      +'+" and CDC_MANU.CMCODSEC="+cp_ToStrODBC(this.w_CMCODSEC)';

  cPrg = "gsca_acm"
  cComment = "Movimenti di analitica"
  icon = "anag.ico"
  cAutoZoom = 'GSCA0ACM'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CMCODICE = space(10)
  w_CMCODSEC = 0
  w_CODAZI = space(5)
  w_CMNUMREG = 0
  w_CMCOMPET = space(4)
  w_CMDATREG = ctod('  /  /  ')
  o_CMDATREG = ctod('  /  /  ')
  w_CMFLGMOV = space(1)
  w_CMNUMDOC = 0
  w_CMALFDOC = space(10)
  w_CMDATDOC = ctod('  /  /  ')
  w_CMDESAGG = space(50)
  w_CMVALNAZ = space(3)
  w_OBTEST = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CMCODICE = this.W_CMCODICE
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_CMCOMPET = this.W_CMCOMPET
  op_CMNUMREG = this.W_CMNUMREG

  * --- Children pointers
  GSCA_MMC = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CDC_MANU','gsca_acm')
    stdPageFrame::Init()
    *set procedure to GSCA_MMC additive
    with this
      .Pages(1).addobject("oPag","tgsca_acmPag1","gsca_acm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Movimento di analitica")
      .Pages(1).HelpContextID = 13002315
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCA_MMC
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='MOVICOST'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='CDC_MANU'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CDC_MANU_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CDC_MANU_IDX,3]
  return

  function CreateChildren()
    this.GSCA_MMC = CREATEOBJECT('stdDynamicChild',this,'GSCA_MMC',this.oPgFrm.Page1.oPag.oLinkPC_1_12)
    this.GSCA_MMC.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCA_MMC)
      this.GSCA_MMC.DestroyChildrenChain()
      this.GSCA_MMC=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_12')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCA_MMC.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCA_MMC.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCA_MMC.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCA_MMC.SetKey(;
            .w_CMCODICE,"MRSERIAL";
            ,.w_CMCODSEC,"MRROWORD";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCA_MMC.ChangeRow(this.cRowID+'      1',1;
             ,.w_CMCODICE,"MRSERIAL";
             ,.w_CMCODSEC,"MRROWORD";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCA_MMC)
        i_f=.GSCA_MMC.BuildFilter()
        if !(i_f==.GSCA_MMC.cQueryFilter)
          i_fnidx=.GSCA_MMC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCA_MMC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCA_MMC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCA_MMC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCA_MMC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CMCODICE = NVL(CMCODICE,space(10))
      .w_CMCODSEC = NVL(CMCODSEC,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CDC_MANU where CMCODICE=KeySet.CMCODICE
    *                            and CMCODSEC=KeySet.CMCODSEC
    *
    i_nConn = i_TableProp[this.CDC_MANU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CDC_MANU_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CDC_MANU')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CDC_MANU.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CDC_MANU '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CMCODICE',this.w_CMCODICE  ,'CMCODSEC',this.w_CMCODSEC  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CMCODICE = NVL(CMCODICE,space(10))
        .op_CMCODICE = .w_CMCODICE
        .w_CMCODSEC = NVL(CMCODSEC,0)
        .w_CODAZI = i_CODAZI
        .w_CMNUMREG = NVL(CMNUMREG,0)
        .op_CMNUMREG = .w_CMNUMREG
        .w_CMCOMPET = NVL(CMCOMPET,space(4))
        .op_CMCOMPET = .w_CMCOMPET
          * evitabile
          *.link_1_5('Load')
        .w_CMDATREG = NVL(cp_ToDate(CMDATREG),ctod("  /  /  "))
        .w_CMFLGMOV = NVL(CMFLGMOV,space(1))
        .w_CMNUMDOC = NVL(CMNUMDOC,0)
        .w_CMALFDOC = NVL(CMALFDOC,space(10))
        .w_CMDATDOC = NVL(cp_ToDate(CMDATDOC),ctod("  /  /  "))
        .w_CMDESAGG = NVL(CMDESAGG,space(50))
        .w_CMVALNAZ = NVL(CMVALNAZ,space(3))
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .w_OBTEST = .w_CMDATREG
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CDC_MANU')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CMCODICE = space(10)
      .w_CMCODSEC = 0
      .w_CODAZI = space(5)
      .w_CMNUMREG = 0
      .w_CMCOMPET = space(4)
      .w_CMDATREG = ctod("  /  /  ")
      .w_CMFLGMOV = space(1)
      .w_CMNUMDOC = 0
      .w_CMALFDOC = space(10)
      .w_CMDATDOC = ctod("  /  /  ")
      .w_CMDESAGG = space(50)
      .w_CMVALNAZ = space(3)
      .w_OBTEST = ctod("  /  /  ")
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_CMCODSEC = -1
        .w_CODAZI = i_CODAZI
          .DoRTCalc(4,4,.f.)
        .w_CMCOMPET = g_CODESE
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_CMCOMPET))
          .link_1_5('Full')
          endif
        .w_CMDATREG = i_datsys
        .w_CMFLGMOV = 'E'
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
          .DoRTCalc(8,12,.f.)
        .w_OBTEST = .w_CMDATREG
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CDC_MANU')
    this.DoRTCalc(14,17,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CDC_MANU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CDC_MANU_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SECDC","i_codazi,w_CMCODICE")
      cp_AskTableProg(this,i_nConn,"PRCDC","i_codazi,w_CMCOMPET,w_CMNUMREG")
      .op_codazi = .w_codazi
      .op_CMCODICE = .w_CMCODICE
      .op_codazi = .w_codazi
      .op_CMCOMPET = .w_CMCOMPET
      .op_CMNUMREG = .w_CMNUMREG
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCMNUMREG_1_4.enabled = i_bVal
      .Page1.oPag.oCMDATREG_1_6.enabled = i_bVal
      .Page1.oPag.oCMFLGMOV_1_7.enabled = i_bVal
      .Page1.oPag.oCMNUMDOC_1_8.enabled = i_bVal
      .Page1.oPag.oCMALFDOC_1_9.enabled = i_bVal
      .Page1.oPag.oCMDATDOC_1_10.enabled = i_bVal
      .Page1.oPag.oCMDESAGG_1_11.enabled = i_bVal
      .Page1.oPag.oObj_1_21.enabled = i_bVal
      .Page1.oPag.oObj_1_24.enabled = i_bVal
      .Page1.oPag.oObj_1_25.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oCMNUMREG_1_4.enabled = .t.
      endif
    endwith
    this.GSCA_MMC.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CDC_MANU',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCA_MMC.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CDC_MANU_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMCODICE,"CMCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMCODSEC,"CMCODSEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMNUMREG,"CMNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMCOMPET,"CMCOMPET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMDATREG,"CMDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMFLGMOV,"CMFLGMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMNUMDOC,"CMNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMALFDOC,"CMALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMDATDOC,"CMDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMDESAGG,"CMDESAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CMVALNAZ,"CMVALNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CDC_MANU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CDC_MANU_IDX,2])
    i_lTable = "CDC_MANU"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CDC_MANU_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCA_SMA with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CDC_MANU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CDC_MANU_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CDC_MANU_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SECDC","i_codazi,w_CMCODICE")
          cp_NextTableProg(this,i_nConn,"PRCDC","i_codazi,w_CMCOMPET,w_CMNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CDC_MANU
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CDC_MANU')
        i_extval=cp_InsertValODBCExtFlds(this,'CDC_MANU')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CMCODICE,CMCODSEC,CMNUMREG,CMCOMPET,CMDATREG"+;
                  ",CMFLGMOV,CMNUMDOC,CMALFDOC,CMDATDOC,CMDESAGG"+;
                  ",CMVALNAZ,UTCC,UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CMCODICE)+;
                  ","+cp_ToStrODBC(this.w_CMCODSEC)+;
                  ","+cp_ToStrODBC(this.w_CMNUMREG)+;
                  ","+cp_ToStrODBCNull(this.w_CMCOMPET)+;
                  ","+cp_ToStrODBC(this.w_CMDATREG)+;
                  ","+cp_ToStrODBC(this.w_CMFLGMOV)+;
                  ","+cp_ToStrODBC(this.w_CMNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_CMALFDOC)+;
                  ","+cp_ToStrODBC(this.w_CMDATDOC)+;
                  ","+cp_ToStrODBC(this.w_CMDESAGG)+;
                  ","+cp_ToStrODBC(this.w_CMVALNAZ)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CDC_MANU')
        i_extval=cp_InsertValVFPExtFlds(this,'CDC_MANU')
        cp_CheckDeletedKey(i_cTable,0,'CMCODICE',this.w_CMCODICE,'CMCODSEC',this.w_CMCODSEC)
        INSERT INTO (i_cTable);
              (CMCODICE,CMCODSEC,CMNUMREG,CMCOMPET,CMDATREG,CMFLGMOV,CMNUMDOC,CMALFDOC,CMDATDOC,CMDESAGG,CMVALNAZ,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CMCODICE;
                  ,this.w_CMCODSEC;
                  ,this.w_CMNUMREG;
                  ,this.w_CMCOMPET;
                  ,this.w_CMDATREG;
                  ,this.w_CMFLGMOV;
                  ,this.w_CMNUMDOC;
                  ,this.w_CMALFDOC;
                  ,this.w_CMDATDOC;
                  ,this.w_CMDESAGG;
                  ,this.w_CMVALNAZ;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CDC_MANU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CDC_MANU_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CDC_MANU_IDX,i_nConn)
      *
      * update CDC_MANU
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CDC_MANU')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CMNUMREG="+cp_ToStrODBC(this.w_CMNUMREG)+;
             ",CMCOMPET="+cp_ToStrODBCNull(this.w_CMCOMPET)+;
             ",CMDATREG="+cp_ToStrODBC(this.w_CMDATREG)+;
             ",CMFLGMOV="+cp_ToStrODBC(this.w_CMFLGMOV)+;
             ",CMNUMDOC="+cp_ToStrODBC(this.w_CMNUMDOC)+;
             ",CMALFDOC="+cp_ToStrODBC(this.w_CMALFDOC)+;
             ",CMDATDOC="+cp_ToStrODBC(this.w_CMDATDOC)+;
             ",CMDESAGG="+cp_ToStrODBC(this.w_CMDESAGG)+;
             ",CMVALNAZ="+cp_ToStrODBC(this.w_CMVALNAZ)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CDC_MANU')
        i_cWhere = cp_PKFox(i_cTable  ,'CMCODICE',this.w_CMCODICE  ,'CMCODSEC',this.w_CMCODSEC  )
        UPDATE (i_cTable) SET;
              CMNUMREG=this.w_CMNUMREG;
             ,CMCOMPET=this.w_CMCOMPET;
             ,CMDATREG=this.w_CMDATREG;
             ,CMFLGMOV=this.w_CMFLGMOV;
             ,CMNUMDOC=this.w_CMNUMDOC;
             ,CMALFDOC=this.w_CMALFDOC;
             ,CMDATDOC=this.w_CMDATDOC;
             ,CMDESAGG=this.w_CMDESAGG;
             ,CMVALNAZ=this.w_CMVALNAZ;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCA_MMC : Saving
      this.GSCA_MMC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CMCODICE,"MRSERIAL";
             ,this.w_CMCODSEC,"MRROWORD";
             )
      this.GSCA_MMC.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsca_acm
    this.NotifyEvent('ControlliFinali')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCA_MMC : Deleting
    this.GSCA_MMC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CMCODICE,"MRSERIAL";
           ,this.w_CMCODSEC,"MRROWORD";
           )
    this.GSCA_MMC.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CDC_MANU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CDC_MANU_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CDC_MANU_IDX,i_nConn)
      *
      * delete CDC_MANU
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CMCODICE',this.w_CMCODICE  ,'CMCODSEC',this.w_CMCODSEC  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsca_acm
    this.NotifyEvent('ControlliFinali')
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CDC_MANU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CDC_MANU_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_CODAZI = i_CODAZI
        .DoRTCalc(4,4,.t.)
          .link_1_5('Full')
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .DoRTCalc(6,12,.t.)
        if .o_CMDATREG<>.w_CMDATREG
            .w_OBTEST = .w_CMDATREG
        endif
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SECDC","i_codazi,w_CMCODICE")
          .op_CMCODICE = .w_CMCODICE
        endif
        if .op_codazi<>.w_codazi .or. .op_CMCOMPET<>.w_CMCOMPET
           cp_AskTableProg(this,i_nConn,"PRCDC","i_codazi,w_CMCOMPET,w_CMNUMREG")
          .op_CMNUMREG = .w_CMNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_CMCOMPET = .w_CMCOMPET
      endwith
      this.DoRTCalc(14,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CMCOMPET
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CMCOMPET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CMCOMPET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CMCOMPET);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CMCOMPET)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CMCOMPET = NVL(_Link_.ESCODESE,space(4))
      this.w_CMVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CMCOMPET = space(4)
      endif
      this.w_CMVALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CMCOMPET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCMNUMREG_1_4.value==this.w_CMNUMREG)
      this.oPgFrm.Page1.oPag.oCMNUMREG_1_4.value=this.w_CMNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCMCOMPET_1_5.value==this.w_CMCOMPET)
      this.oPgFrm.Page1.oPag.oCMCOMPET_1_5.value=this.w_CMCOMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oCMDATREG_1_6.value==this.w_CMDATREG)
      this.oPgFrm.Page1.oPag.oCMDATREG_1_6.value=this.w_CMDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCMFLGMOV_1_7.RadioValue()==this.w_CMFLGMOV)
      this.oPgFrm.Page1.oPag.oCMFLGMOV_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMNUMDOC_1_8.value==this.w_CMNUMDOC)
      this.oPgFrm.Page1.oPag.oCMNUMDOC_1_8.value=this.w_CMNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCMALFDOC_1_9.value==this.w_CMALFDOC)
      this.oPgFrm.Page1.oPag.oCMALFDOC_1_9.value=this.w_CMALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCMDATDOC_1_10.value==this.w_CMDATDOC)
      this.oPgFrm.Page1.oPag.oCMDATDOC_1_10.value=this.w_CMDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCMDESAGG_1_11.value==this.w_CMDESAGG)
      this.oPgFrm.Page1.oPag.oCMDESAGG_1_11.value=this.w_CMDESAGG
    endif
    cp_SetControlsValueExtFlds(this,'CDC_MANU')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CMNUMREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCMNUMREG_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CMNUMREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CMDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCMDATREG_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CMDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCA_MMC.CheckForm()
      if i_bres
        i_bres=  .GSCA_MMC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CMDATREG = this.w_CMDATREG
    * --- GSCA_MMC : Depends On
    this.GSCA_MMC.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsca_acmPag1 as StdContainer
  Width  = 744
  height = 363
  stdWidth  = 744
  stdheight = 363
  resizeYpos=260
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCMNUMREG_1_4 as StdField with uid="ZHWGTJWBWQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CMNUMREG", cQueryName = "CMNUMREG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo numero registrazione",;
    HelpContextID = 257971091,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=107, Top=10, cSayPict='"999999"', cGetPict='"999999"'

  add object oCMCOMPET_1_5 as StdField with uid="CYDSMRXAVH",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CMCOMPET", cQueryName = "CMCOMPET",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza contabile della registrazione",;
    HelpContextID = 23528326,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=187, Top=10, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CMCOMPET"

  func oCMCOMPET_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCMDATREG_1_6 as StdField with uid="WDZXZDYHDA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CMDATREG", cQueryName = "CMDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione",;
    HelpContextID = 251982739,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=265, Top=10


  add object oCMFLGMOV_1_7 as StdCombo with uid="LPOAOHNMQY",rtseq=7,rtrep=.f.,left=624,top=10,width=115,height=21;
    , ToolTipText = "Tipo movimento: effettivo o previsionale";
    , HelpContextID = 188099708;
    , cFormVar="w_CMFLGMOV",RowSource=""+"Effettivo,"+"Previsionale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCMFLGMOV_1_7.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oCMFLGMOV_1_7.GetRadio()
    this.Parent.oContained.w_CMFLGMOV = this.RadioValue()
    return .t.
  endfunc

  func oCMFLGMOV_1_7.SetRadio()
    this.Parent.oContained.w_CMFLGMOV=trim(this.Parent.oContained.w_CMFLGMOV)
    this.value = ;
      iif(this.Parent.oContained.w_CMFLGMOV=='E',1,;
      iif(this.Parent.oContained.w_CMFLGMOV=='P',2,;
      0))
  endfunc

  add object oCMNUMDOC_1_8 as StdField with uid="LAVHSANUET",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CMNUMDOC", cQueryName = "CMNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento associato",;
    HelpContextID = 44018793,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=107, Top=40, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue= 999999999999999

  add object oCMALFDOC_1_9 as StdField with uid="VHLKNAVEIV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CMALFDOC", cQueryName = "CMALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Alfa documento associato",;
    HelpContextID = 36035689,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=233, Top=40, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oCMDATDOC_1_10 as StdField with uid="HNKCARTQZE",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CMDATDOC", cQueryName = "CMDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento associato",;
    HelpContextID = 50007145,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=380, Top=40

  add object oCMDESAGG_1_11 as StdField with uid="LORHUCLMHA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CMDESAGG", cQueryName = "CMDESAGG",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva",;
    HelpContextID = 1110931,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=107, Top=70, InputMask=replicate('X',50)


  add object oLinkPC_1_12 as stdDynamicChildContainer with uid="NXFOXZUFXZ",left=1, top=104, width=739, height=257, bOnScreen=.t.;



  add object oObj_1_21 as cp_runprogram with uid="HLWZSJYIVP",left=348, top=377, width=94,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCA_BCK('R')",;
    cEvent = "Insert start,Update start",;
    nPag=1;
    , HelpContextID = 200179226


  add object oObj_1_24 as cp_runprogram with uid="IGHIKLEHIU",left=513, top=378, width=94,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCA_BCK('C')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 200179226


  add object oObj_1_25 as cp_runprogram with uid="DCVCQHEOBA",left=633, top=380, width=94,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCA_BCK('W')",;
    cEvent = "Edit Started",;
    nPag=1;
    , HelpContextID = 200179226

  add object oStr_1_13 as StdString with uid="FGWQVOJEVI",Visible=.t., Left=1, Top=10,;
    Alignment=1, Width=103, Height=15,;
    Caption="Registrazione n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="OSACTCNNZJ",Visible=.t., Left=232, Top=10,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="UAROQAMNFN",Visible=.t., Left=1, Top=40,;
    Alignment=1, Width=103, Height=15,;
    Caption="Documento n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="RUQSPHTZHJ",Visible=.t., Left=224, Top=40,;
    Alignment=2, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="HIXIRDLXWX",Visible=.t., Left=346, Top=40,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="ABKVBCPEQR",Visible=.t., Left=1, Top=70,;
    Alignment=1, Width=103, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="TFUBYNKPLO",Visible=.t., Left=178, Top=10,;
    Alignment=0, Width=11, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="TPXAKGXHQS",Visible=.t., Left=534, Top=10,;
    Alignment=1, Width=87, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsca_acm','CDC_MANU','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CMCODICE=CDC_MANU.CMCODICE";
  +" and "+i_cAliasName2+".CMCODSEC=CDC_MANU.CMCODSEC";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
