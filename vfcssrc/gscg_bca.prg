* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bca                                                        *
*              Caricamento servizi corrispondenti ai conti                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-05-15                                                      *
* Last revis.: 2007-05-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bca",oParentObject)
return(i_retval)

define class tgscg_bca as StdBatch
  * --- Local variables
  w_CODICE = space(15)
  w_CODAZI = space(5)
  w_CODIVA = space(5)
  w_MESS = space(100)
  w_ErrorLog = .NULL.
  * --- WorkFile variables
  PAR_ARCO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da GSCG_KCS
    this.w_CODAZI = i_CODAZI
    this.w_ErrorLog=createobject("AH_ErrorLog")
    this.w_MESS = ""
    * --- Read from PAR_ARCO
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_ARCO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_ARCO_idx,2],.t.,this.PAR_ARCO_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PCCODIVA"+;
        " from "+i_cTable+" PAR_ARCO where ";
            +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PCCODIVA;
        from (i_cTable) where;
            PCCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODIVA = NVL(cp_ToDate(_read_.PCCODIVA),cp_NullValue(_read_.PCCODIVA))
      use
      if i_Rows=0
        this.w_MESS = AH_errormsg("Attenzione parametri archivi collegati non presenti. Impossibile caricare archivi collegati." )
        i_retcode = 'stop'
        return
      endif
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Select from gscg_kcs
    do vq_exec with 'gscg_kcs',this,'_Curs_gscg_kcs','',.f.,.t.
    if used('_Curs_gscg_kcs')
      select _Curs_gscg_kcs
      locate for 1=1
      do while not(eof())
      * --- Try
      local bErr_035CA578
      bErr_035CA578=bTrsErr
      this.Try_035CA578()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_035CA578
      * --- End
        select _Curs_gscg_kcs
        continue
      enddo
      use
    endif
    this.w_ErrorLog.PrintLog(this,"Segnalazioni in fase di caricamento")     
  endproc
  proc Try_035CA578()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_CODICE = _Curs_gscg_kcs.ANCODICE
    gscg_bps(this,"CI",this.w_CODICE,"B",this.w_ErrorLog,"")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Ah_msg("Caricamento servizi corrispondenti al Conto %1",.T.,.F.,.F., ALLTRIM(this.w_CODICE))
    * --- commit
    cp_EndTrs(.t.)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_ARCO'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_gscg_kcs')
      use in _Curs_gscg_kcs
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
