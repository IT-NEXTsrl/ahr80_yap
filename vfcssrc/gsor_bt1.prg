* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_bt1                                                        *
*              Filtro su campo descrittivo                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_166]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2000-03-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsor_bt1",oParentObject)
return(i_retval)

define class tgsor_bt1 as StdBatch
  * --- Local variables
  w_Zoom = space(10)
  w_RIFCLI = space(30)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna zoom con codice descrittivo
    this.w_Zoom = this.oParentObject.w_docu
    This.OparentObject.NotifyEvent("Ricerca")
    NC = this.w_Zoom.cCursor
    * --- Filtro sul campo riferimento descrittivo
    this.w_RIFCLI = this.oParentObject.oParentObject.w_MVRIFCLI
    if NOT EMPTY(this.w_RIFCLI) 
      FIL = "'%"+ALLTRIM(UPPER(this.w_RIFCLI))+"%'"
      SELECT (NC)
      GO TOP
      delete from &NC where UPPER(NVL(MVRIFCLI, SPACE(30))) not like &FIL
    endif
    SELECT (NC)
    GO TOP
    * --- Refresh della griglia
    this.oParentObject.oParentObject.w_NUMDOC=0
    this.oParentObject.oParentObject.w_NUMRIF=0
    this.oParentObject.oParentObject.w_ALFRIF=SPACE(2)
    this.oParentObject.oParentObject.w_ALFDOC=SPACE(2)
    this.oParentObject.oParentObject.w_DATDOC=cp_CharToDate("  -  -    ")
    this.oParentObject.oParentObject.w_DATRIF=cp_CharToDate("  -  -    ")
    this.w_Zoom.refresh()
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
