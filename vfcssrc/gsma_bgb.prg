* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bgb                                                        *
*              Generazione massiva barcode                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-03-07                                                      *
* Last revis.: 2018-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bgb",oParentObject,m.pOper)
return(i_retval)

define class tgsma_bgb as StdBatch
  * --- Local variables
  pOper = space(1)
  w_PADRE = .NULL.
  w_ZC = space(10)
  w_COUNT = 0
  w_CODBAR = space(10)
  w___TIPO = space(1)
  w_KEYBAR = space(20)
  w_OLDBAR = space(41)
  w_CODART = space(20)
  w_DESART = space(40)
  w_DESSUP = space(0)
  w_DTINVA = ctod("  /  /  ")
  w_DTOBSO = ctod("  /  /  ")
  w_OKINS = .f.
  w_MAXCOD = 0
  w_TmpN = 0
  w_i = 0
  w_CODTIP = space(35)
  w_CODVAL = space(35)
  w_CODCLF = space(5)
  w_FLGESC = space(1)
  * --- WorkFile variables
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per la generazione massiva dei barcode
    *     chiamato da GSMA_KGB
    *     
    *     pOper: 'A' :  Visualizza la seconda pagina
    *                  'S' :  Seleziona / Deseleziona tutto
    *                  'G' :  Generazione barcode
    this.w_PADRE = This.oParentObject
    this.w_ZC = this.oParentObject.w_ZoomArt.cCursor
    do case
      case this.pOper="A"
        this.w_PADRE.opgfrm.activepage = 2
      case this.pOper="S"
        Update (this.w_ZC) Set xchk = iif(this.oParentObject.w_SELECTALL="S",1,0)
      case this.pOper="G"
        SELECT ( this.w_ZC ) 
 COUNT FOR XCHK = 1 TO this.w_COUNT
        if this.w_COUNT>0
          this.w_i = 0
          SCAN FOR Not Empty(Nvl(ARCODART,"")) And XCHK=1
          * --- Inizializza Campi da Aggiornare in KEY_ARTI
          this.w_KEYBAR = ""
          this.w___TIPO = "R"
          this.w_CODART = Alltrim(ARCODART)
          this.w_DESART = Alltrim(Nvl(ARDESART,""))
          this.w_DESSUP = Alltrim(Nvl(ARDESSUP,""))
          this.w_DTINVA = ARDTINVA
          this.w_DTOBSO = ARDTOBSO
          this.w_CODTIP = ALLTRIM(ARCODTIP)
          this.w_CODVAL = ALLTRIM(ARCODVAL)
          this.w_CODCLF = ALLTRIM(ARCODCLF)
          this.w_FLGESC = ALLTRIM(ARFLGESC)
          * --- Aggiunge il Barcode calcolato a partire dall'AutoNumber
          this.w_OLDBAR = "@@@@@@@@@@"
          this.w_OKINS = .T.
          this.w_TmpN = 0
          do while this.w_OKINS
            this.w_CODBAR = SPACE(10)
            i_nConn=i_TableProp[this.KEY_ARTI_IDX, 3]
            this.w_KEYBAR = RIGHT("99"+ALLTRIM(g_PREEAN), 2)
            do case
              case this.oParentObject.w_TIPBAR="1"
                * --- EAN 8
                cp_NextTableProg(this,i_nConn,"COBAR8","i_codazi,w_CODBAR")
                * --- Calcolo la chiave del codice a barre - Aggiungo uno 0 per simulare il CheckSum
                this.w_KEYBAR = this.w_KEYBAR+RIGHT("00000"+ALLTRIM(this.w_CODBAR), 5)+"0"
                this.w_MAXCOD = 99999
              case this.oParentObject.w_TIPBAR="2"
                * --- EAN 13
                cp_NextTableProg(this,i_nConn,"COBAR13","i_codazi,w_CODBAR")
                * --- Calcolo la chiave del codice a barre - Aggiungo uno 0 per simulare il CheckSum
                if this.w_KEYBAR="99"
                  * --- Se i primi 2 Caratteri sono 99 il Codice e' di tipo Interno, non usa il Cod.Produttore, sfrutta tutto l' Autonumber (10 cifre)
                  this.w_KEYBAR = this.w_KEYBAR+RIGHT("0000000000"+ALLTRIM(this.w_CODBAR), 10)+"0"
                  this.w_MAXCOD = 99999999
                else
                  if LEN(ALLTRIM(g_BARPRO))=5
                    this.w_KEYBAR = this.w_KEYBAR+RIGHT("00000"+ALLTRIM(g_BARPRO), 5)
                    this.w_KEYBAR = this.w_KEYBAR+RIGHT("00000"+ALLTRIM(this.w_CODBAR), 5)+"0"
                    this.w_MAXCOD = 99999
                  else
                    this.w_KEYBAR = this.w_KEYBAR+RIGHT("0000000"+ALLTRIM(g_BARPRO), 7)
                    this.w_KEYBAR = this.w_KEYBAR+RIGHT("000"+ALLTRIM(this.w_CODBAR), 3)+"0"
                    this.w_MAXCOD = 999
                  endif
                endif
            endcase
            * --- Calcolo il Checksum Corretto
            this.w_KEYBAR = CALCBAR(this.w_KEYBAR, this.oParentObject.w_TIPBAR, 1)
            * --- Controllo di non aver generato due codici uguali consecutivamente per essere sicuro di non incappare in un loop
            if RIGHT(SPACE(41)+this.w_KEYBAR,41)<>RIGHT(SPACE(41)+this.w_OLDBAR,41)
              ah_Msg("Scrittura barcode...%1",.T.,.F.,.F.,this.w_KEYBAR)
              * --- Try
              local bErr_05384F18
              bErr_05384F18=bTrsErr
              this.Try_05384F18()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
                this.w_TmpN = this.w_TmpN+1
                this.w_i = this.w_i-1
                this.w_OKINS = .T.
              endif
              bTrsErr=bTrsErr or bErr_05384F18
              * --- End
              this.w_OLDBAR = this.w_KEYBAR
            else
              this.w_TmpN = this.w_MAXCOD+1
            endif
            if this.w_TmpN>this.w_MAXCOD
              this.w_OKINS = .F.
              * --- Errore: utilizzati tutti i codici a barre disponibili
              ah_errormsg('Impossibile generare il barcode per "%1" e articoli successivi:'+CHR(10)+"utilizzati tutti i codici disponibili.","!", "", this.w_CODART)
            endif
          enddo
          if this.w_TmpN>this.w_MAXCOD
            exit
          endif
          ENDSCAN
          ah_errormsg("Generazione completata."+CHR(10)+"Inseriti %1 barcode." ,"i","",Alltrim(Str(this.w_i)))
        else
          ah_errormsg("Nessun codice da generare","!")
        endif
      case this.pOper="F"
        this.w_PADRE.NotifyEvent("Filtri")     
        this.oParentObject.w_ZoomArt.Refresh() 
 this.oParentObject.w_ZoomArt.SetFocus()
    endcase
  endproc
  proc Try_05384F18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_OKINS = .F.
    this.w_i = this.w_i+1
    * --- Insert into KEY_ARTI
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CACODICE"+",CADESART"+",CADESSUP"+",CACODART"+",CATIPCON"+",CA__TIPO"+",CATIPBAR"+",CAFLSTAM"+",CAOPERAT"+",CAMOLTIP"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",CADTOBSO"+",CADTINVA"+",CACODTIP"+",CACODVAL"+",CACODCLF"+",CAFLGESC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KEYBAR),'KEY_ARTI','CACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESART),'KEY_ARTI','CADESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'KEY_ARTI','CADESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'KEY_ARTI','CACODART');
      +","+cp_NullLink(cp_ToStrODBC("R"),'KEY_ARTI','CATIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w___TIPO),'KEY_ARTI','CA__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPBAR),'KEY_ARTI','CATIPBAR');
      +","+cp_NullLink(cp_ToStrODBC("S"),'KEY_ARTI','CAFLSTAM');
      +","+cp_NullLink(cp_ToStrODBC("*"),'KEY_ARTI','CAOPERAT');
      +","+cp_NullLink(cp_ToStrODBC(1),'KEY_ARTI','CAMOLTIP');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'KEY_ARTI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(0),'KEY_ARTI','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'KEY_ARTI','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'KEY_ARTI','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DTOBSO),'KEY_ARTI','CADTOBSO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DTINVA),'KEY_ARTI','CADTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODTIP),'KEY_ARTI','CACODTIP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'KEY_ARTI','CACODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCLF),'KEY_ARTI','CACODCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLGESC),'KEY_ARTI','CAFLGESC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_KEYBAR,'CADESART',this.w_DESART,'CADESSUP',this.w_DESSUP,'CACODART',this.w_CODART,'CATIPCON',"R",'CA__TIPO',this.w___TIPO,'CATIPBAR',this.oParentObject.w_TIPBAR,'CAFLSTAM',"S",'CAOPERAT',"*",'CAMOLTIP',1,'UTCC',i_CODUTE,'UTCV',0)
      insert into (i_cTable) (CACODICE,CADESART,CADESSUP,CACODART,CATIPCON,CA__TIPO,CATIPBAR,CAFLSTAM,CAOPERAT,CAMOLTIP,UTCC,UTCV,UTDC,UTDV,CADTOBSO,CADTINVA,CACODTIP,CACODVAL,CACODCLF,CAFLGESC &i_ccchkf. );
         values (;
           this.w_KEYBAR;
           ,this.w_DESART;
           ,this.w_DESSUP;
           ,this.w_CODART;
           ,"R";
           ,this.w___TIPO;
           ,this.oParentObject.w_TIPBAR;
           ,"S";
           ,"*";
           ,1;
           ,i_CODUTE;
           ,0;
           ,SetInfoDate( g_CALUTD );
           ,cp_CharToDate("  -  -    ");
           ,this.w_DTOBSO;
           ,this.w_DTINVA;
           ,this.w_CODTIP;
           ,this.w_CODVAL;
           ,this.w_CODCLF;
           ,this.w_FLGESC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
