* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bc5                                                        *
*              Carica automatismi c.di costo                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_7]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-05-27                                                      *
* Last revis.: 2008-11-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bc5",oParentObject)
return(i_retval)

define class tgscg_bc5 as StdBatch
  * --- Local variables
  w_RTOT = 0
  w_CODCON = space(15)
  w_TROV = .f.
  w_IMPTOT = 0
  w_TIPCON = space(1)
  w_NURIG = 0
  * --- WorkFile variables
  COLLCENT_idx=0
  CENCOST_idx=0
  VOC_COST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica temporaneo Centri di Costo da Primanota dalla Tabella Ripartizioni (da GSCG_MCA)
    * --- Cicla Sulla Tabella Ripartizioni
    this.w_RTOT = 0
    WITH this.oParentObject.oParentObject
    .w_CDCSAL = "."
    .TrsFromWork()
    this.w_TIPCON = .w_PNTIPCON
    this.w_CODCON = .w_PNCODCON
    this.w_IMPTOT = iif(.w_PNIMPAVE<>0,.w_PNIMPAVE,.w_PNIMPDAR)
    this.oParentObject.w_MR_SEGNO = IIF(.w_PNIMPAVE<>0, "A", "D")
    this.oParentObject.w_TOTRIG = .w_PNIMPDAR - .w_PNIMPAVE
    ENDWITH
    * --- Calcola Totale Parametri
    this.oParentObject.w_TOTPAR = 0
    * --- Select from GSCG_BC5
    do vq_exec with 'GSCG_BC5',this,'_Curs_GSCG_BC5','',.f.,.t.
    if used('_Curs_GSCG_BC5')
      select _Curs_GSCG_BC5
      locate for 1=1
      do while not(eof())
      this.oParentObject.w_TOTPAR = NVL(_Curs_GSCG_BC5.TOTALE, 0)
        select _Curs_GSCG_BC5
        continue
      enddo
      use
    endif
    * --- Azzera il Transitorio
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    DELETE ALL
    this.w_TROV = .F.
    this.w_NURIG = 0
    * --- Select from COLLCENT
    i_nConn=i_TableProp[this.COLLCENT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2],.t.,this.COLLCENT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" COLLCENT ";
          +" where MRTIPCON="+cp_ToStrODBC(this.w_TIPCON)+" AND MRCODICE="+cp_ToStrODBC(this.w_CODCON)+"";
          +" order by MRCODVOC,MRCODCDC";
           ,"_Curs_COLLCENT")
    else
      select * from (i_cTable);
       where MRTIPCON=this.w_TIPCON AND MRCODICE=this.w_CODCON;
       order by MRCODVOC,MRCODCDC;
        into cursor _Curs_COLLCENT
    endif
    if used('_Curs_COLLCENT')
      select _Curs_COLLCENT
      locate for 1=1
      do while not(eof())
      * --- Nuova Riga del Temporaneo
      this.oParentObject.InitRow()
      this.oParentObject.w_MRCODVOC = NVL(_Curs_COLLCENT.MRCODVOC, SPACE(15))
      this.oParentObject.w_MRCODICE = NVL(_Curs_COLLCENT.MRCODCDC, SPACE(15))
      this.oParentObject.w_MRPARAME = NVL(_Curs_COLLCENT.MRPARAME, 0)
      this.oParentObject.w_PERCEN = IIF(this.oParentObject.w_TOTPAR=0 OR this.oParentObject.w_MRPARAME=0, 0, cp_round((this.oParentObject.w_MRPARAME/this.oParentObject.w_TOTPAR)*100,2))
      if NOT EMPTY(this.oParentObject.w_MRCODVOC)
        * --- Legge Voce di Costo
        * --- Read from VOC_COST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOC_COST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2],.t.,this.VOC_COST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VCDESCRI"+;
            " from "+i_cTable+" VOC_COST where ";
                +"VCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MRCODVOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VCDESCRI;
            from (i_cTable) where;
                VCCODICE = this.oParentObject.w_MRCODVOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESVOC = NVL(cp_ToDate(_read_.VCDESCRI),cp_NullValue(_read_.VCDESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if NOT EMPTY(this.oParentObject.w_MRCODICE)
        * --- Legge Centro di Costo
        * --- Read from CENCOST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CENCOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CENCOST_idx,2],.t.,this.CENCOST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CCDESPIA,CCNUMLIV"+;
            " from "+i_cTable+" CENCOST where ";
                +"CC_CONTO = "+cp_ToStrODBC(this.oParentObject.w_MRCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CCDESPIA,CCNUMLIV;
            from (i_cTable) where;
                CC_CONTO = this.oParentObject.w_MRCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESPIA = NVL(cp_ToDate(_read_.CCDESPIA),cp_NullValue(_read_.CCDESPIA))
          this.oParentObject.w_NUMLIV = NVL(cp_ToDate(_read_.CCNUMLIV),cp_NullValue(_read_.CCNUMLIV))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Ripartisce gli Importi
      if this.oParentObject.w_TOTPAR=0 OR this.oParentObject.w_MRPARAME=0 OR this.w_IMPTOT=0
        this.oParentObject.w_MRTOTIMP = 0
        this.oParentObject.w_IMPRIG = 0
      else
        this.oParentObject.w_MRTOTIMP = cp_ROUND((this.w_IMPTOT * this.oParentObject.w_MRPARAME) / this.oParentObject.w_TOTPAR, g_PERPVL)
        this.oParentObject.w_IMPRIG = this.oParentObject.w_MRTOTIMP * IIF(this.oParentObject.w_MR_SEGNO="D", 1, -1)
      endif
      * --- Per calcolo resto
      this.w_RTOT = this.w_RTOT + this.oParentObject.w_MRTOTIMP
      this.w_NURIG = this.w_NURIG + 1
      * --- Carica il Temporaneo dei Dati e skippa al record successivo
      this.oParentObject.w_CDCSAL = "."
      this.oParentObject.TrsFromWork()
      this.w_TROV = .T.
        select _Curs_COLLCENT
        continue
      enddo
      use
    endif
    if this.w_TROV=.F.
      * --- Carica almeno la prima Riga
      this.oParentObject.BlankRec()
    endif
    * --- Questa Parte derivata dal Metodo LoadRec
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    if this.w_RTOT<>this.w_IMPTOT
      * --- Ripartisce il resto sulla prima riga
      this.oParentObject.WorkFromTrs()
      if this.oParentObject.w_MRPARAME<>0 OR this.w_NURIG=1
        this.oParentObject.w_MRTOTIMP = this.oParentObject.w_MRTOTIMP + (this.w_IMPTOT - this.w_RTOT)
        this.oParentObject.w_MRPARAME = IIF(this.oParentObject.w_MRPARAME=0, 1, this.oParentObject.w_MRPARAME)
        this.oParentObject.w_IMPRIG = this.oParentObject.w_MRTOTIMP * IIF(this.oParentObject.w_MR_SEGNO="D", 1, -1)
      endif
      this.oParentObject.TrsFromWork()
    endif
    if this.w_RTOT=0
      * --- Se l'importo ripartito � zero allora il totale deve valere 0 (tutte le righe valgono 0)
      this.oParentObject.w_TOTRIG = this.w_RTOT
    endif
    With this.oParentObject
    .WorkFromTrs()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=1
    .oPgFrm.Page1.oPag.oBody.nRelRow=1
    EndWith
    this.oParentObject.w_GIACAR = "S"
    this.oParentObject.w_SEGTOT = IIF(this.oParentObject.w_TOTRIG<0, "A","D")
    this.oParentObject.w_TOTVIS = ABS(this.oParentObject.w_TOTRIG)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='COLLCENT'
    this.cWorkTables[2]='CENCOST'
    this.cWorkTables[3]='VOC_COST'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GSCG_BC5')
      use in _Curs_GSCG_BC5
    endif
    if used('_Curs_COLLCENT')
      use in _Curs_COLLCENT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
