* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bpk                                                        *
*              Funzioni per Packing List                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_185]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-16                                                      *
* Last revis.: 2001-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO,pSERIAL,pPARAME
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bpk",oParentObject,m.pTIPO,m.pSERIAL,m.pPARAME)
return(i_retval)

define class tgsma_bpk as StdBatch
  * --- Local variables
  pTIPO = space(1)
  pSERIAL = space(10)
  pPARAME = space(3)
  w_SERIAL = space(10)
  w_ZOOM = space(10)
  w_OREP = space(50)
  w_TROV = .f.
  w_UMVOLU = space(3)
  w_SERRIF = space(10)
  w_TESTDIV = .f.
  w_PROG = .NULL.
  * --- WorkFile variables
  PACKTEMP_idx=0
  PAC_CONF_idx=0
  PAC_DETT_idx=0
  PAC_MAST_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato dalla maschera Packing List GSMA_KPK per tutte le sue funzioni
    do case
      case this.pTIPO="R"
        this.oParentObject.NotifyEvent("Ricerca")
      case this.pTIPO="D"
        * --- lancia i Documenti da Packing List (da GSMA_KPK)
        * --- Questo oggetto sar� definito come Documento
        this.w_SERIAL = nvl(this.pSERIAL,space(10))
        gsar_bzm(this,this.w_SERIAL,-20)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTIPO="E"
        this.w_ZOOM = this.oParentObject.w_ZoomDoc
        NC = this.w_Zoom.cCursor
        SELECT (NC)
        SELECT * FROM (NC) WHERE XCHK=1 INTO CURSOR APPO1
        SELECT APPO1
        if RECCOUNT()>0
          SCAN
          this.w_SERIAL = PLSERIAL
          * --- Delete from PAC_CONF
          i_nConn=i_TableProp[this.PAC_CONF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAC_CONF_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PCSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            delete from (i_cTable) where;
                  PCSERIAL = this.w_SERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from PAC_DETT
          i_nConn=i_TableProp[this.PAC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAC_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PLSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            delete from (i_cTable) where;
                  PLSERIAL = this.w_SERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from PAC_MAST
          i_nConn=i_TableProp[this.PAC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAC_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PLSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            delete from (i_cTable) where;
                  PLSERIAL = this.w_SERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          ENDSCAN
        else
          ah_ErrorMsg("Selezionare una o pi� righe")
          i_retcode = 'stop'
          return
        endif
        if used("APPO1")
          select APPO1
          use
        endif
        this.oParentObject.NotifyEvent("Ricerca")
      case this.pTIPO="P"
        this.w_PROG = GSMA_MPK()
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Entro in Interrogazione
        this.w_PROG.w_PLSERIAL = this.pSERIAL
        this.w_SERIAL = nvl(this.pSERIAL,space(10))
        this.w_PROG.QueryKeySet("PLSERIAL="+CP_TOSTRODBC(this.w_SERIAL),"")     
        this.w_PROG.LoadRec()     
      case this.pTIPO="S"
        * --- Evento w_SELEZI Changed
        this.w_ZOOM = this.oParentObject.w_ZoomDoc
        NC = this.w_Zoom.cCursor
        * --- Seleziona/Deselezione la Righe Dettaglio (se consentito)
        if this.oParentObject.w_SELEZI = "S"
          * --- Seleziona Tutto
          UPDATE (NC) SET XCHK=1
        else
          * --- deseleziona Tutto
          UPDATE (NC) SET XCHK=0
        endif
      case this.pTIPO="C"
        if this.oParentObject.w_PLSTATUS="N"
          if ah_YesNo("Vuoi confermare la Packing List?")
            * --- Setto queste propriet� del Padre a True altrimenti nel caso vado in modifica e salvo senza cambiare
            *     niente non aggiorna lo Status anche se confermo
            this.oParentObject.w_PLSTATUS = "S"
            this.oParentObject.bUpdated=.t.
            this.oParentObject.bHeaderUpdated=.t.
          endif
        endif
      case this.pTIPO="F"
        * --- Creo temporaneo di stampa sulla base delle selezioni della maschera, le stesse dello zoom
        * --- Create temporary table PACKTEMP
        i_nIdx=cp_AddTableDef('PACKTEMP') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('query\gsma4qpk',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.PACKTEMP_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        this.w_OREP = "QUERY\GSMA4QPK.FRX"
        this.w_ZOOM = this.oParentObject.w_ZoomDoc
        this.w_TROV = .F.
        NC = this.w_Zoom.cCursor
        SELECT * FROM (NC) WHERE XCHK=1 INTO CURSOR APPO5
        SELECT APPO5
        * --- Controllo se sono state selezionate Packing List per la stampa
        if RECCOUNT()=0
          ah_ErrorMsg("Selezionare una o pi� righe")
        else
          SELECT (NC)
          GO TOP
          SCAN FOR NOT EMPTY(NVL(PLSERIAL,"")) AND XCHK<>1
          this.w_SERIAL = PLSERIAL
          * --- Eliminodal cursore di stampa le packing list non selezionate.
          * --- Delete from PACKTEMP
          i_nConn=i_TableProp[this.PACKTEMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PACKTEMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PLSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            delete from (i_cTable) where;
                  PLSERIAL = this.w_SERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          SELECT (NC)
          ENDSCAN
          VQ_EXEC("QUERY\GSMA5QPK.VQR", this, "__TMP__")
          select __TMP__
          GO TOP
          * --- Controllo per volume collo. Per avere un totale del volume dei colli le unit� di misura di questi devono essere gli stessi
          do while NOT EOF()
            this.w_SERRIF = PLSERIAL
            this.w_UMVOLU = NVL(PLUMVOLU," ")
            this.w_TESTDIV = .F.
            do while PLSERIAL=this.w_SERRIF
              if nvl(PLUMVOLU," ")<>this.w_UMVOLU OR this.w_TESTDIV
                * --- Metto a 'S' il campo fittizio VOLDIV se non devo stampare il  Volume Totale
                REPLACE VOLDIV WITH "S"
                this.w_TESTDIV = .T.
              endif
              SKIP
            enddo
          enddo
          select __TMP__
          SELECT * FROM __TMP__ INTO CURSOR __TMP__ ORDER BY PLDATDOC, PLNUMDOC, PLRIFDOC, PLNUMCOL, CPROWORD
          L_LOGO=GETBMP(I_CODAZI)
          CP_CHPRN(this.w_OREP, " ", this)
        endif
        if used("APPO5")
          select APPO5
          use
        endif
        if used("__TMP__")
          select __TMP__
          use
        endif
        * --- Drop temporary table PACKTEMP
        i_nIdx=cp_GetTableDefIdx('PACKTEMP')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('PACKTEMP')
        endif
      case this.pTIPO="Z"
        this.w_OREP = "QUERY\GSMA4QPK.FRX"
        VQ_EXEC("QUERY\GSMA4APK.VQR", this, "__TMP__")
        select __TMP__
        GO TOP
        this.w_UMVOLU = NVL(PLUMVOLU," ")
        LOCATE FOR PLUMVOLU<>this.w_UMVOLU
        if FOUND()
          GO BOTTOM
          REPLACE VOLDIV WITH "S"
        endif
        select __TMP__
        GO TOP
        L_LOGO=GETBMP(I_CODAZI)
        CP_CHPRN(this.w_OREP, " ", this)
        if used("__TMP__")
          select __TMP__
          use
        endif
    endcase
  endproc


  proc Init(oParentObject,pTIPO,pSERIAL,pPARAME)
    this.pTIPO=pTIPO
    this.pSERIAL=pSERIAL
    this.pPARAME=pPARAME
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='*PACKTEMP'
    this.cWorkTables[2]='PAC_CONF'
    this.cWorkTables[3]='PAC_DETT'
    this.cWorkTables[4]='PAC_MAST'
    this.cWorkTables[5]='DOC_MAST'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO,pSERIAL,pPARAME"
endproc
