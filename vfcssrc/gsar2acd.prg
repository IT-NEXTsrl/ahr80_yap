* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar2acd                                                        *
*              Modello com..ann.IVA 2008                                       *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_312]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-30                                                      *
* Last revis.: 2012-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsar2acd
**pAnno indica l anno a cui si riferisce la comunicazione dati iva (2008 oppure 2009)
PARAMETERS pAnno
L_descri5='1) Rappr. legale - negoziale o socio amm.'
L_descri2='2) Rappr. di minore inabilitato - interdetto - curatore/amm. eredit�'
L_descri6='2) Socio amministratore'
L_descri7='3) Curatore fallimentare'
L_descri8='4) Commissario liquidatore'
L_descri9='5) Comm. giudiziale - custode/amm. giudiziario'
L_descri10='6) Rappresentate fiscale'
L_descri11='7) Erede'
L_descri12='8) Liquidatore'
L_descri13='9) Societ� beneficiaria o incorporante'
L_descri3='9) Soggetti per operaz. straord.'
L_comb1='Persona fisica'
L_comb2='Altri Soggetti'
L_combo1='01: Sogg. che inviano la propria dich.'
L_combo2a='02: Soggetti che utilizzano il canale Entratel'
L_combo2='03: C.A.F. dipendenti e pensionati'
L_combo3='05: C.A.F: imprese'
L_combo4='09: Art 3 comma 2'
L_combo5='10: Altri intermediari'

* --- Fine Area Manuale
return(createobject("tgsar2acd"))

* --- Class definition
define class tgsar2acd as StdForm
  Top    = 2
  Left   = 10

  * --- Standard Properties
  Width  = 755
  Height = 415+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-06-04"
  HelpContextID=112621417
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=77

  * --- Constant Properties
  MOD_COAN_IDX = 0
  AZIENDA_IDX = 0
  ATTIDETT_IDX = 0
  TITOLARI_IDX = 0
  ESERCIZI_IDX = 0
  cFile = "MOD_COAN"
  cKeySelect = "AIANNIMP,AIDATINV"
  cKeyWhere  = "AIANNIMP=this.w_AIANNIMP and AIDATINV=this.w_AIDATINV"
  cKeyWhereODBC = '"AIANNIMP="+cp_ToStrODBC(this.w_AIANNIMP)';
      +'+" and AIDATINV="+cp_ToStrODBC(this.w_AIDATINV,"D")';

  cKeyWhereODBCqualified = '"MOD_COAN.AIANNIMP="+cp_ToStrODBC(this.w_AIANNIMP)';
      +'+" and MOD_COAN.AIDATINV="+cp_ToStrODBC(this.w_AIDATINV,"D")';

  cPrg = "gsar2acd"
  cComment = "Modello com..ann.IVA 2008"
  icon = "anag.ico"
  cAutoZoom = 'GSAR_2008'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_CODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  w_AZCODCAR = space(2)
  w_AZPIVAZI = space(12)
  w_AZIVACOF = space(16)
  w_AZPERAZI = space(1)
  w_AZCODFIS = space(16)
  w_AZCODNAZ = space(3)
  w_AIANNIMP = 0
  w_AIDATINV = ctod('  /  /  ')
  w_CODFIS = space(16)
  w_AIPARIVA = space(11)
  w_AICODAT1 = space(6)
  w_AITIPFOR = space(2)
  o_AITIPFOR = space(2)
  w_AICONSEP = space(1)
  w_RAGSOC = space(80)
  w_AISOCGRU = space(1)
  w_AIEVEECC = space(1)
  w_EDITADICH = space(1)
  o_EDITADICH = space(1)
  w_AICODFIS = space(16)
  o_AICODFIS = space(16)
  w_AIFLCODF = space(1)
  w_AICODIVA = space(11)
  w_AICODCAR = space(2)
  w_AIMANCOR = space(1)
  w_AIINOLTRO = space(1)
  w_NUMREG = 0
  w_CODAZI1 = space(5)
  w_COGTIT = space(25)
  o_COGTIT = space(25)
  w_NOMTIT = space(25)
  o_NOMTIT = space(25)
  w_AITOTATT = 0
  w_AITA_NIM = 0
  w_AITA_ESE = 0
  w_AITA_CIN = 0
  w_AITA_CBS = 0
  w_AITOTPAS = 0
  w_AITP_NIM = 0
  w_AITP_ESE = 0
  w_AITP_CIN = 0
  w_AITP_CBS = 0
  w_AIIMPORO = 0
  w_AIIVAORO = 0
  w_AIIMPROT = 0
  w_AIIVAROT = 0
  w_AIIVAESI = 0
  o_AIIVAESI = 0
  w_AIIVADET = 0
  o_AIIVADET = 0
  w_AIIVADOV = 0
  w_AIIVACRE = 0
  w_AIFIRMAC = space(1)
  w_AICOFINT = space(16)
  o_AICOFINT = space(16)
  w_AIALBCAF = space(5)
  w_AIINVCON = space(1)
  o_AIINVCON = space(1)
  w_AIINVSOG = space(1)
  o_AIINVSOG = space(1)
  w_AIDATIMP = ctod('  /  /  ')
  w_AIFIRMA = space(1)
  w_AIPERAZI = space(1)
  o_AIPERAZI = space(1)
  w_NOMEFILEFIS = space(20)
  w_NOMEFILE = space(200)
  w_AIDENOMI = space(60)
  w_AICOGNOM = space(24)
  w_AINOME = space(20)
  w_AISESSO = space(1)
  w_AISE_CAP = space(9)
  w_AISECOMU = space(40)
  w_AICOMUNE = space(40)
  w_AISESIGL = space(2)
  w_AISEIND2 = space(35)
  w_AIDATNAS = ctod('  /  /  ')
  w_AISIGLA = space(2)
  w_AIINDIRI = space(35)
  w_AI___CAP = space(9)
  w_AIRESSIG = space(2)
  w_AISERCAP = space(9)
  w_AISERSIG = space(2)
  w_AIRESCOM = space(40)
  w_AISERCOM = space(40)
  w_AISERIND = space(35)
  w_GSAR2BCD = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar2acd
  w_ANNO=0
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOD_COAN','gsar2acd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar2acdPag1","gsar2acd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Sez. I")
      .Pages(1).HelpContextID = 192965850
      .Pages(2).addobject("oPag","tgsar2acdPag2","gsar2acd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Sez. II")
      .Pages(2).HelpContextID = 192965850
      .Pages(3).addobject("oPag","tgsar2acdPag3","gsar2acd",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Sez. III")
      .Pages(3).HelpContextID = 192965777
      .Pages(4).addobject("oPag","tgsar2acdPag4","gsar2acd",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Inol.telem.")
      .Pages(4).HelpContextID = 10501954
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAIANNIMP_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsar2acd
    WITH THIS.PARENT
    .w_ANNO=pAnno
    .cComment = ah_msgformat('Modello comunicazione annuale dati IVA %1',alltrim (str (nvl (.w_ANNO,0))))
    DO CASE
     CASE .w_ANNO =2008
       .cAutoZoom = 'GSAR_2008'
      CASE  .w_ANNO = 2009
       .cAutoZoom = 'GSAR_2009'
     CASE  .w_ANNO = 2010
       .cAutoZoom = 'GSAR_2010'
      CASE  .w_ANNO = 2011
       .cAutoZoom = 'GSAR_2011' 
    ENDCASE
    ENDWITH
    
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_GSAR2BCD = this.oPgFrm.Pages(1).oPag.GSAR2BCD
      DoDefault()
    proc Destroy()
      this.w_GSAR2BCD = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ATTIDETT'
    this.cWorkTables[3]='TITOLARI'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='MOD_COAN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_COAN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_COAN_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_AIANNIMP = NVL(AIANNIMP,0)
      .w_AIDATINV = NVL(AIDATINV,ctod("  /  /  "))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- gsar2acd
    this.w_AIDATINV=iif(empty(this.w_AIDATINV), i_datsys, this.w_AIDATINV)
    
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOD_COAN where AIANNIMP=KeySet.AIANNIMP
    *                            and AIDATINV=KeySet.AIDATINV
    *
    i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_COAN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_COAN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_COAN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_COAN '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AIANNIMP',this.w_AIANNIMP  ,'AIDATINV',this.w_AIDATINV  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_CODESE = g_CODESE
        .w_INIESE = ctod("  /  /  ")
        .w_AZCODCAR = space(2)
        .w_AZPIVAZI = space(12)
        .w_AZIVACOF = space(16)
        .w_AZPERAZI = space(1)
        .w_AZCODFIS = space(16)
        .w_AZCODNAZ = space(3)
        .w_RAGSOC = g_RAGAZI
        .w_NUMREG = 0
        .w_CODAZI1 = i_CODAZI
        .w_NOMEFILEFIS = 'COMANNIVA'
          .link_1_1('Load')
          .link_1_2('Load')
        .w_AIANNIMP = NVL(AIANNIMP,0)
        .w_AIDATINV = NVL(cp_ToDate(AIDATINV),ctod("  /  /  "))
        .w_CODFIS = IIF(EMPTY(.w_CODFIS),IIF(EMPTY(.w_AZCODFIS),.w_AZPIVAZI,.w_AZCODFIS),.w_CODFIS)
        .w_AIPARIVA = NVL(AIPARIVA,space(11))
        .w_AICODAT1 = NVL(AICODAT1,space(6))
        .w_AITIPFOR = NVL(AITIPFOR,space(2))
        .w_AICONSEP = NVL(AICONSEP,space(1))
        .oPgFrm.Page1.oPag.GSAR2BCD.Calculate()
        .w_AISOCGRU = NVL(AISOCGRU,space(1))
        .w_AIEVEECC = NVL(AIEVEECC,space(1))
        .w_EDITADICH = NVL(EDITADICH,space(1))
        .w_AICODFIS = NVL(AICODFIS,space(16))
        .w_AIFLCODF = NVL(AIFLCODF,space(1))
        .w_AICODIVA = NVL(AICODIVA,space(11))
        .w_AICODCAR = NVL(AICODCAR,space(2))
        .w_AIMANCOR = NVL(AIMANCOR,space(1))
        .w_AIINOLTRO = NVL(AIINOLTRO,space(1))
          .link_1_44('Load')
        .w_COGTIT = LEFT(ALLTRIM(.w_COGTIT),24)
        .w_NOMTIT = LEFT(ALLTRIM(.w_NOMTIT),20)
        .w_AITOTATT = NVL(AITOTATT,0)
        .w_AITA_NIM = NVL(AITA_NIM,0)
        .w_AITA_ESE = NVL(AITA_ESE,0)
        .w_AITA_CIN = NVL(AITA_CIN,0)
        .w_AITA_CBS = NVL(AITA_CBS,0)
        .w_AITOTPAS = NVL(AITOTPAS,0)
        .w_AITP_NIM = NVL(AITP_NIM,0)
        .w_AITP_ESE = NVL(AITP_ESE,0)
        .w_AITP_CIN = NVL(AITP_CIN,0)
        .w_AITP_CBS = NVL(AITP_CBS,0)
        .w_AIIMPORO = NVL(AIIMPORO,0)
        .w_AIIVAORO = NVL(AIIVAORO,0)
        .w_AIIMPROT = NVL(AIIMPROT,0)
        .w_AIIVAROT = NVL(AIIVAROT,0)
        .w_AIIVAESI = NVL(AIIVAESI,0)
        .w_AIIVADET = NVL(AIIVADET,0)
        .w_AIIVADOV = NVL(AIIVADOV,0)
        .w_AIIVACRE = NVL(AIIVACRE,0)
        .w_AIFIRMAC = NVL(AIFIRMAC,space(1))
        .w_AICOFINT = NVL(AICOFINT,space(16))
        .w_AIALBCAF = NVL(AIALBCAF,space(5))
        .w_AIINVCON = NVL(AIINVCON,space(1))
        .w_AIINVSOG = NVL(AIINVSOG,space(1))
        .w_AIDATIMP = NVL(cp_ToDate(AIDATIMP),ctod("  /  /  "))
        .w_AIFIRMA = NVL(AIFIRMA,space(1))
        .w_AIPERAZI = NVL(AIPERAZI,space(1))
        .oPgFrm.Page4.oPag.oObj_4_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .w_NOMEFILE = NVL(NOMEFILE,space(200))
        .w_AIDENOMI = NVL(AIDENOMI,space(60))
        .w_AICOGNOM = NVL(AICOGNOM,space(24))
        .w_AINOME = NVL(AINOME,space(20))
        .w_AISESSO = NVL(AISESSO,space(1))
        .w_AISE_CAP = NVL(AISE_CAP,space(9))
        .w_AISECOMU = NVL(AISECOMU,space(40))
        .w_AICOMUNE = NVL(AICOMUNE,space(40))
        .w_AISESIGL = NVL(AISESIGL,space(2))
        .w_AISEIND2 = NVL(AISEIND2,space(35))
        .w_AIDATNAS = NVL(cp_ToDate(AIDATNAS),ctod("  /  /  "))
        .w_AISIGLA = NVL(AISIGLA,space(2))
        .w_AIINDIRI = NVL(AIINDIRI,space(35))
        .w_AI___CAP = NVL(AI___CAP,space(9))
        .w_AIRESSIG = NVL(AIRESSIG,space(2))
        .w_AISERCAP = NVL(AISERCAP,space(9))
        .w_AISERSIG = NVL(AISERSIG,space(2))
        .w_AIRESCOM = NVL(AIRESCOM,space(40))
        .w_AISERCOM = NVL(AISERCOM,space(40))
        .w_AISERIND = NVL(AISERIND,space(35))
        cp_LoadRecExtFlds(this,'MOD_COAN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_CODESE = space(4)
      .w_INIESE = ctod("  /  /  ")
      .w_AZCODCAR = space(2)
      .w_AZPIVAZI = space(12)
      .w_AZIVACOF = space(16)
      .w_AZPERAZI = space(1)
      .w_AZCODFIS = space(16)
      .w_AZCODNAZ = space(3)
      .w_AIANNIMP = 0
      .w_AIDATINV = ctod("  /  /  ")
      .w_CODFIS = space(16)
      .w_AIPARIVA = space(11)
      .w_AICODAT1 = space(6)
      .w_AITIPFOR = space(2)
      .w_AICONSEP = space(1)
      .w_RAGSOC = space(80)
      .w_AISOCGRU = space(1)
      .w_AIEVEECC = space(1)
      .w_EDITADICH = space(1)
      .w_AICODFIS = space(16)
      .w_AIFLCODF = space(1)
      .w_AICODIVA = space(11)
      .w_AICODCAR = space(2)
      .w_AIMANCOR = space(1)
      .w_AIINOLTRO = space(1)
      .w_NUMREG = 0
      .w_CODAZI1 = space(5)
      .w_COGTIT = space(25)
      .w_NOMTIT = space(25)
      .w_AITOTATT = 0
      .w_AITA_NIM = 0
      .w_AITA_ESE = 0
      .w_AITA_CIN = 0
      .w_AITA_CBS = 0
      .w_AITOTPAS = 0
      .w_AITP_NIM = 0
      .w_AITP_ESE = 0
      .w_AITP_CIN = 0
      .w_AITP_CBS = 0
      .w_AIIMPORO = 0
      .w_AIIVAORO = 0
      .w_AIIMPROT = 0
      .w_AIIVAROT = 0
      .w_AIIVAESI = 0
      .w_AIIVADET = 0
      .w_AIIVADOV = 0
      .w_AIIVACRE = 0
      .w_AIFIRMAC = space(1)
      .w_AICOFINT = space(16)
      .w_AIALBCAF = space(5)
      .w_AIINVCON = space(1)
      .w_AIINVSOG = space(1)
      .w_AIDATIMP = ctod("  /  /  ")
      .w_AIFIRMA = space(1)
      .w_AIPERAZI = space(1)
      .w_NOMEFILEFIS = space(20)
      .w_NOMEFILE = space(200)
      .w_AIDENOMI = space(60)
      .w_AICOGNOM = space(24)
      .w_AINOME = space(20)
      .w_AISESSO = space(1)
      .w_AISE_CAP = space(9)
      .w_AISECOMU = space(40)
      .w_AICOMUNE = space(40)
      .w_AISESIGL = space(2)
      .w_AISEIND2 = space(35)
      .w_AIDATNAS = ctod("  /  /  ")
      .w_AISIGLA = space(2)
      .w_AIINDIRI = space(35)
      .w_AI___CAP = space(9)
      .w_AIRESSIG = space(2)
      .w_AISERCAP = space(9)
      .w_AISERSIG = space(2)
      .w_AIRESCOM = space(40)
      .w_AISERCOM = space(40)
      .w_AISERIND = space(35)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
        .w_CODESE = g_CODESE
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_CODESE))
          .link_1_2('Full')
          endif
          .DoRTCalc(3,9,.f.)
        .w_AIANNIMP = IIF(.cFunction='Load',YEAR(.w_INIESE)-1,.w_AIANNIMP)
        .w_AIDATINV = IIF(.cFunction='Load',i_DATSYS,.w_AIDATINV)
        .w_CODFIS = IIF(EMPTY(.w_CODFIS),IIF(EMPTY(.w_AZCODFIS),.w_AZPIVAZI,.w_AZCODFIS),.w_CODFIS)
        .w_AIPARIVA = left(.w_AZPIVAZI,11)
        .w_AICODAT1 = IIF(EMPTY(.w_AICODAT1),IIF(g_ATTIVI='S', space(6), LEFT(Alltrim(CALATTSO(g_CATAZI,Alltrim(STR(year(.w_AIDATINV))),12)),6)),.w_AICODAT1)
        .w_AITIPFOR = '01'
        .w_AICONSEP = '0'
        .w_RAGSOC = g_RAGAZI
        .oPgFrm.Page1.oPag.GSAR2BCD.Calculate()
          .DoRTCalc(18,18,.f.)
        .w_AIEVEECC = '0'
        .w_EDITADICH = IIF(.w_AZPERAZI='S', 'N', 'S')
        .w_AICODFIS = IIF(.w_EDITADICH='S' and .cFunction<>'Query',.w_AZIVACOF,space(16))
        .w_AIFLCODF = '0'
        .w_AICODIVA = space(11)
        .w_AICODCAR = IIF(val(.w_AZCODCAR)==1,'1',IIF(val(.w_AZCODCAR)==2,'2',IIF(val(.w_AZCODCAR)==5,'5',IIF(val(.w_AZCODCAR)==6,'6',IIF(val(.w_AZCODCAR)==7,'7',IIF(val(.w_AZCODCAR)==8,'8','00'))))))
        .w_AIMANCOR = '0'
          .DoRTCalc(26,27,.f.)
        .w_CODAZI1 = i_CODAZI
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_CODAZI1))
          .link_1_44('Full')
          endif
        .w_COGTIT = LEFT(ALLTRIM(.w_COGTIT),24)
        .w_NOMTIT = LEFT(ALLTRIM(.w_NOMTIT),20)
          .DoRTCalc(31,46,.f.)
        .w_AIIVADOV = IIF(.w_AIIVAESI>=.w_AIIVADET,.w_AIIVAESI-.w_AIIVADET,0)
        .w_AIIVACRE = IIF(.w_AIIVAESI<.w_AIIVADET,.w_AIIVADET-.w_AIIVAESI,0)
        .w_AIFIRMAC = '0'
        .w_AICOFINT = iif(.w_AITIPFOR<>'01' AND .w_AITIPFOR<>'02',.w_AICOFINT,space(16))
        .w_AIALBCAF = IIF(empty(.w_AICOFINT),space(5),.w_AIALBCAF)
        .w_AIINVCON = IIF(NOT EMPTY(.w_AICOFINT) AND .w_AIINVSOG='0','1','0')
        .w_AIINVSOG = IIF(NOT EMPTY(.w_AICOFINT) AND .w_AIINVCON='0','1','0')
        .w_AIDATIMP = IIF(empty(.w_AICOFINT),cp_CharToDate('  -  -    '),.w_AIDATIMP)
        .w_AIFIRMA = IIF(NOT EMPTY(.w_AICOFINT),.w_AIFIRMA,'0')
        .w_AIPERAZI = 'S'
        .oPgFrm.Page4.oPag.oObj_4_2.Calculate()
        .w_NOMEFILEFIS = 'COMANNIVA'
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .w_NOMEFILE = left(sys(5)+sys(2003)+'\'+.w_NOMEFILEFIS+space(200),200)
        .w_AIDENOMI = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AIDENOMI,' ')
        .w_AICOGNOM = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AICOGNOM,' ')
        .w_AINOME = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AINOME,' ')
        .w_AISESSO = 'M'
        .w_AISE_CAP = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISE_CAP,' ')
        .w_AISECOMU = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISECOMU,' ')
        .w_AICOMUNE = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AICOMUNE,' ')
        .w_AISESIGL = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISESIGL,' ')
        .w_AISEIND2 = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISEIND2,' ')
        .w_AIDATNAS = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIDATNAS,cp_CharToDate('  -  -  '))
        .w_AISIGLA = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AISIGLA,' ')
        .w_AIINDIRI = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIINDIRI,' ')
        .w_AI___CAP = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AI___CAP,' ')
        .w_AIRESSIG = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIRESSIG,' ')
        .w_AISERCAP = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERCAP,' ')
        .w_AISERSIG = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERSIG,' ')
        .w_AIRESCOM = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIRESCOM,' ')
        .w_AISERCOM = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERCOM,' ')
        .w_AISERIND = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERIND,' ')
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_COAN')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oAIANNIMP_1_10.enabled = i_bVal
      .Page1.oPag.oAIDATINV_1_11.enabled = i_bVal
      .Page1.oPag.oCODFIS_1_12.enabled = i_bVal
      .Page1.oPag.oAIPARIVA_1_13.enabled = i_bVal
      .Page1.oPag.oAICODAT1_1_14.enabled = i_bVal
      .Page1.oPag.oAITIPFOR_1_15.enabled = i_bVal
      .Page1.oPag.oAICONSEP_1_16.enabled = i_bVal
      .Page1.oPag.oAISOCGRU_1_29.enabled = i_bVal
      .Page1.oPag.oAIEVEECC_1_30.enabled = i_bVal
      .Page1.oPag.oEDITADICH_1_31.enabled = i_bVal
      .Page1.oPag.oAICODFIS_1_33.enabled = i_bVal
      .Page1.oPag.oAIFLCODF_1_34.enabled = i_bVal
      .Page1.oPag.oAICODIVA_1_35.enabled = i_bVal
      .Page1.oPag.oAICODCAR_1_36.enabled = i_bVal
      .Page1.oPag.oAIMANCOR_1_37.enabled = i_bVal
      .Page2.oPag.oAITOTATT_2_9.enabled = i_bVal
      .Page2.oPag.oAITA_NIM_2_12.enabled = i_bVal
      .Page2.oPag.oAITA_ESE_2_14.enabled = i_bVal
      .Page2.oPag.oAITA_CIN_2_16.enabled = i_bVal
      .Page2.oPag.oAITA_CBS_2_17.enabled = i_bVal
      .Page2.oPag.oAITOTPAS_2_28.enabled = i_bVal
      .Page2.oPag.oAITP_NIM_2_31.enabled = i_bVal
      .Page2.oPag.oAITP_ESE_2_33.enabled = i_bVal
      .Page2.oPag.oAITP_CIN_2_35.enabled = i_bVal
      .Page2.oPag.oAITP_CBS_2_36.enabled = i_bVal
      .Page2.oPag.oAIIMPORO_2_38.enabled = i_bVal
      .Page2.oPag.oAIIVAORO_2_39.enabled = i_bVal
      .Page2.oPag.oAIIMPROT_2_40.enabled = i_bVal
      .Page2.oPag.oAIIVAROT_2_41.enabled = i_bVal
      .Page3.oPag.oAIIVAESI_3_8.enabled = i_bVal
      .Page3.oPag.oAIIVADET_3_12.enabled = i_bVal
      .Page3.oPag.oAIFIRMAC_3_19.enabled = i_bVal
      .Page3.oPag.oAICOFINT_3_21.enabled = i_bVal
      .Page3.oPag.oAIALBCAF_3_25.enabled = i_bVal
      .Page3.oPag.oAIINVCON_3_30.enabled = i_bVal
      .Page3.oPag.oAIINVSOG_3_33.enabled = i_bVal
      .Page3.oPag.oAIDATIMP_3_36.enabled = i_bVal
      .Page3.oPag.oAIFIRMA_3_40.enabled = i_bVal
      .Page4.oPag.oAIPERAZI_4_1.enabled = i_bVal
      .Page4.oPag.oNOMEFILE_4_10.enabled = i_bVal
      .Page4.oPag.oAIDENOMI_4_12.enabled = i_bVal
      .Page4.oPag.oAICOGNOM_4_13.enabled = i_bVal
      .Page4.oPag.oAINOME_4_14.enabled = i_bVal
      .Page4.oPag.oAISESSO_4_15.enabled = i_bVal
      .Page4.oPag.oAISE_CAP_4_17.enabled = i_bVal
      .Page4.oPag.oAISECOMU_4_18.enabled = i_bVal
      .Page4.oPag.oAICOMUNE_4_19.enabled = i_bVal
      .Page4.oPag.oAISESIGL_4_20.enabled = i_bVal
      .Page4.oPag.oAISEIND2_4_21.enabled = i_bVal
      .Page4.oPag.oAIDATNAS_4_22.enabled = i_bVal
      .Page4.oPag.oAISIGLA_4_23.enabled = i_bVal
      .Page4.oPag.oAIINDIRI_4_24.enabled = i_bVal
      .Page4.oPag.oAI___CAP_4_25.enabled = i_bVal
      .Page4.oPag.oAIRESSIG_4_26.enabled = i_bVal
      .Page4.oPag.oAISERCAP_4_27.enabled = i_bVal
      .Page4.oPag.oAISERSIG_4_28.enabled = i_bVal
      .Page4.oPag.oAIRESCOM_4_29.enabled = i_bVal
      .Page4.oPag.oAISERCOM_4_30.enabled = i_bVal
      .Page4.oPag.oAISERIND_4_31.enabled = i_bVal
      .Page2.oPag.oBtn_2_42.enabled = i_bVal
      .Page4.oPag.oBtn_4_6.enabled = i_bVal
      .Page4.oPag.oBtn_4_8.enabled = i_bVal
      .Page4.oPag.oBtn_4_9.enabled = i_bVal
      .Page1.oPag.GSAR2BCD.enabled = i_bVal
      .Page4.oPag.oObj_4_2.enabled = i_bVal
      .Page1.oPag.oObj_1_47.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oAIANNIMP_1_10.enabled = .f.
        .Page1.oPag.oAIDATINV_1_11.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oAIANNIMP_1_10.enabled = .t.
        .Page1.oPag.oAIDATINV_1_11.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MOD_COAN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIANNIMP,"AIANNIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDATINV,"AIDATINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPARIVA,"AIPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODAT1,"AICODAT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITIPFOR,"AITIPFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICONSEP,"AICONSEP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISOCGRU,"AISOCGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIEVEECC,"AIEVEECC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDITADICH,"EDITADICH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODFIS,"AICODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIFLCODF,"AIFLCODF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODIVA,"AICODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODCAR,"AICODCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIMANCOR,"AIMANCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIINOLTRO,"AIINOLTRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITOTATT,"AITOTATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITA_NIM,"AITA_NIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITA_ESE,"AITA_ESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITA_CIN,"AITA_CIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITA_CBS,"AITA_CBS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITOTPAS,"AITOTPAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITP_NIM,"AITP_NIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITP_ESE,"AITP_ESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITP_CIN,"AITP_CIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITP_CBS,"AITP_CBS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIMPORO,"AIIMPORO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIVAORO,"AIIVAORO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIMPROT,"AIIMPROT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIVAROT,"AIIVAROT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIVAESI,"AIIVAESI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIVADET,"AIIVADET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIVADOV,"AIIVADOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIIVACRE,"AIIVACRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIFIRMAC,"AIFIRMAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICOFINT,"AICOFINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIALBCAF,"AIALBCAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIINVCON,"AIINVCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIINVSOG,"AIINVSOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDATIMP,"AIDATIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIFIRMA,"AIFIRMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIPERAZI,"AIPERAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOMEFILE,"NOMEFILE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDENOMI,"AIDENOMI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICOGNOM,"AICOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AINOME,"AINOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISESSO,"AISESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISE_CAP,"AISE_CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISECOMU,"AISECOMU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICOMUNE,"AICOMUNE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISESIGL,"AISESIGL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISEIND2,"AISEIND2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIDATNAS,"AIDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISIGLA,"AISIGLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIINDIRI,"AIINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AI___CAP,"AI___CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIRESSIG,"AIRESSIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISERCAP,"AISERCAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISERSIG,"AISERSIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AIRESCOM,"AIRESCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISERCOM,"AISERCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AISERIND,"AISERIND",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_COAN_IDX,2])
    i_lTable = "MOD_COAN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOD_COAN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gsar2acd
    this.bUpdated = .t.
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_COAN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOD_COAN_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOD_COAN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_COAN')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_COAN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AIANNIMP,AIDATINV,AIPARIVA,AICODAT1,AITIPFOR"+;
                  ",AICONSEP,AISOCGRU,AIEVEECC,EDITADICH,AICODFIS"+;
                  ",AIFLCODF,AICODIVA,AICODCAR,AIMANCOR,AIINOLTRO"+;
                  ",AITOTATT,AITA_NIM,AITA_ESE,AITA_CIN,AITA_CBS"+;
                  ",AITOTPAS,AITP_NIM,AITP_ESE,AITP_CIN,AITP_CBS"+;
                  ",AIIMPORO,AIIVAORO,AIIMPROT,AIIVAROT,AIIVAESI"+;
                  ",AIIVADET,AIIVADOV,AIIVACRE,AIFIRMAC,AICOFINT"+;
                  ",AIALBCAF,AIINVCON,AIINVSOG,AIDATIMP,AIFIRMA"+;
                  ",AIPERAZI,NOMEFILE,AIDENOMI,AICOGNOM,AINOME"+;
                  ",AISESSO,AISE_CAP,AISECOMU,AICOMUNE,AISESIGL"+;
                  ",AISEIND2,AIDATNAS,AISIGLA,AIINDIRI,AI___CAP"+;
                  ",AIRESSIG,AISERCAP,AISERSIG,AIRESCOM,AISERCOM"+;
                  ",AISERIND "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AIANNIMP)+;
                  ","+cp_ToStrODBC(this.w_AIDATINV)+;
                  ","+cp_ToStrODBC(this.w_AIPARIVA)+;
                  ","+cp_ToStrODBC(this.w_AICODAT1)+;
                  ","+cp_ToStrODBC(this.w_AITIPFOR)+;
                  ","+cp_ToStrODBC(this.w_AICONSEP)+;
                  ","+cp_ToStrODBC(this.w_AISOCGRU)+;
                  ","+cp_ToStrODBC(this.w_AIEVEECC)+;
                  ","+cp_ToStrODBC(this.w_EDITADICH)+;
                  ","+cp_ToStrODBC(this.w_AICODFIS)+;
                  ","+cp_ToStrODBC(this.w_AIFLCODF)+;
                  ","+cp_ToStrODBC(this.w_AICODIVA)+;
                  ","+cp_ToStrODBC(this.w_AICODCAR)+;
                  ","+cp_ToStrODBC(this.w_AIMANCOR)+;
                  ","+cp_ToStrODBC(this.w_AIINOLTRO)+;
                  ","+cp_ToStrODBC(this.w_AITOTATT)+;
                  ","+cp_ToStrODBC(this.w_AITA_NIM)+;
                  ","+cp_ToStrODBC(this.w_AITA_ESE)+;
                  ","+cp_ToStrODBC(this.w_AITA_CIN)+;
                  ","+cp_ToStrODBC(this.w_AITA_CBS)+;
                  ","+cp_ToStrODBC(this.w_AITOTPAS)+;
                  ","+cp_ToStrODBC(this.w_AITP_NIM)+;
                  ","+cp_ToStrODBC(this.w_AITP_ESE)+;
                  ","+cp_ToStrODBC(this.w_AITP_CIN)+;
                  ","+cp_ToStrODBC(this.w_AITP_CBS)+;
                  ","+cp_ToStrODBC(this.w_AIIMPORO)+;
                  ","+cp_ToStrODBC(this.w_AIIVAORO)+;
                  ","+cp_ToStrODBC(this.w_AIIMPROT)+;
                  ","+cp_ToStrODBC(this.w_AIIVAROT)+;
                  ","+cp_ToStrODBC(this.w_AIIVAESI)+;
                  ","+cp_ToStrODBC(this.w_AIIVADET)+;
                  ","+cp_ToStrODBC(this.w_AIIVADOV)+;
                  ","+cp_ToStrODBC(this.w_AIIVACRE)+;
                  ","+cp_ToStrODBC(this.w_AIFIRMAC)+;
                  ","+cp_ToStrODBC(this.w_AICOFINT)+;
                  ","+cp_ToStrODBC(this.w_AIALBCAF)+;
                  ","+cp_ToStrODBC(this.w_AIINVCON)+;
                  ","+cp_ToStrODBC(this.w_AIINVSOG)+;
                  ","+cp_ToStrODBC(this.w_AIDATIMP)+;
                  ","+cp_ToStrODBC(this.w_AIFIRMA)+;
                  ","+cp_ToStrODBC(this.w_AIPERAZI)+;
                  ","+cp_ToStrODBC(this.w_NOMEFILE)+;
                  ","+cp_ToStrODBC(this.w_AIDENOMI)+;
                  ","+cp_ToStrODBC(this.w_AICOGNOM)+;
                  ","+cp_ToStrODBC(this.w_AINOME)+;
                  ","+cp_ToStrODBC(this.w_AISESSO)+;
                  ","+cp_ToStrODBC(this.w_AISE_CAP)+;
                  ","+cp_ToStrODBC(this.w_AISECOMU)+;
                  ","+cp_ToStrODBC(this.w_AICOMUNE)+;
                  ","+cp_ToStrODBC(this.w_AISESIGL)+;
                  ","+cp_ToStrODBC(this.w_AISEIND2)+;
                  ","+cp_ToStrODBC(this.w_AIDATNAS)+;
                  ","+cp_ToStrODBC(this.w_AISIGLA)+;
                  ","+cp_ToStrODBC(this.w_AIINDIRI)+;
                  ","+cp_ToStrODBC(this.w_AI___CAP)+;
                  ","+cp_ToStrODBC(this.w_AIRESSIG)+;
                  ","+cp_ToStrODBC(this.w_AISERCAP)+;
                  ","+cp_ToStrODBC(this.w_AISERSIG)+;
                  ","+cp_ToStrODBC(this.w_AIRESCOM)+;
                  ","+cp_ToStrODBC(this.w_AISERCOM)+;
                  ","+cp_ToStrODBC(this.w_AISERIND)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_COAN')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_COAN')
        cp_CheckDeletedKey(i_cTable,0,'AIANNIMP',this.w_AIANNIMP,'AIDATINV',this.w_AIDATINV)
        INSERT INTO (i_cTable);
              (AIANNIMP,AIDATINV,AIPARIVA,AICODAT1,AITIPFOR,AICONSEP,AISOCGRU,AIEVEECC,EDITADICH,AICODFIS,AIFLCODF,AICODIVA,AICODCAR,AIMANCOR,AIINOLTRO,AITOTATT,AITA_NIM,AITA_ESE,AITA_CIN,AITA_CBS,AITOTPAS,AITP_NIM,AITP_ESE,AITP_CIN,AITP_CBS,AIIMPORO,AIIVAORO,AIIMPROT,AIIVAROT,AIIVAESI,AIIVADET,AIIVADOV,AIIVACRE,AIFIRMAC,AICOFINT,AIALBCAF,AIINVCON,AIINVSOG,AIDATIMP,AIFIRMA,AIPERAZI,NOMEFILE,AIDENOMI,AICOGNOM,AINOME,AISESSO,AISE_CAP,AISECOMU,AICOMUNE,AISESIGL,AISEIND2,AIDATNAS,AISIGLA,AIINDIRI,AI___CAP,AIRESSIG,AISERCAP,AISERSIG,AIRESCOM,AISERCOM,AISERIND  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AIANNIMP;
                  ,this.w_AIDATINV;
                  ,this.w_AIPARIVA;
                  ,this.w_AICODAT1;
                  ,this.w_AITIPFOR;
                  ,this.w_AICONSEP;
                  ,this.w_AISOCGRU;
                  ,this.w_AIEVEECC;
                  ,this.w_EDITADICH;
                  ,this.w_AICODFIS;
                  ,this.w_AIFLCODF;
                  ,this.w_AICODIVA;
                  ,this.w_AICODCAR;
                  ,this.w_AIMANCOR;
                  ,this.w_AIINOLTRO;
                  ,this.w_AITOTATT;
                  ,this.w_AITA_NIM;
                  ,this.w_AITA_ESE;
                  ,this.w_AITA_CIN;
                  ,this.w_AITA_CBS;
                  ,this.w_AITOTPAS;
                  ,this.w_AITP_NIM;
                  ,this.w_AITP_ESE;
                  ,this.w_AITP_CIN;
                  ,this.w_AITP_CBS;
                  ,this.w_AIIMPORO;
                  ,this.w_AIIVAORO;
                  ,this.w_AIIMPROT;
                  ,this.w_AIIVAROT;
                  ,this.w_AIIVAESI;
                  ,this.w_AIIVADET;
                  ,this.w_AIIVADOV;
                  ,this.w_AIIVACRE;
                  ,this.w_AIFIRMAC;
                  ,this.w_AICOFINT;
                  ,this.w_AIALBCAF;
                  ,this.w_AIINVCON;
                  ,this.w_AIINVSOG;
                  ,this.w_AIDATIMP;
                  ,this.w_AIFIRMA;
                  ,this.w_AIPERAZI;
                  ,this.w_NOMEFILE;
                  ,this.w_AIDENOMI;
                  ,this.w_AICOGNOM;
                  ,this.w_AINOME;
                  ,this.w_AISESSO;
                  ,this.w_AISE_CAP;
                  ,this.w_AISECOMU;
                  ,this.w_AICOMUNE;
                  ,this.w_AISESIGL;
                  ,this.w_AISEIND2;
                  ,this.w_AIDATNAS;
                  ,this.w_AISIGLA;
                  ,this.w_AIINDIRI;
                  ,this.w_AI___CAP;
                  ,this.w_AIRESSIG;
                  ,this.w_AISERCAP;
                  ,this.w_AISERSIG;
                  ,this.w_AIRESCOM;
                  ,this.w_AISERCOM;
                  ,this.w_AISERIND;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_COAN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOD_COAN_IDX,i_nConn)
      *
      * update MOD_COAN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_COAN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " AIPARIVA="+cp_ToStrODBC(this.w_AIPARIVA)+;
             ",AICODAT1="+cp_ToStrODBC(this.w_AICODAT1)+;
             ",AITIPFOR="+cp_ToStrODBC(this.w_AITIPFOR)+;
             ",AICONSEP="+cp_ToStrODBC(this.w_AICONSEP)+;
             ",AISOCGRU="+cp_ToStrODBC(this.w_AISOCGRU)+;
             ",AIEVEECC="+cp_ToStrODBC(this.w_AIEVEECC)+;
             ",EDITADICH="+cp_ToStrODBC(this.w_EDITADICH)+;
             ",AICODFIS="+cp_ToStrODBC(this.w_AICODFIS)+;
             ",AIFLCODF="+cp_ToStrODBC(this.w_AIFLCODF)+;
             ",AICODIVA="+cp_ToStrODBC(this.w_AICODIVA)+;
             ",AICODCAR="+cp_ToStrODBC(this.w_AICODCAR)+;
             ",AIMANCOR="+cp_ToStrODBC(this.w_AIMANCOR)+;
             ",AIINOLTRO="+cp_ToStrODBC(this.w_AIINOLTRO)+;
             ",AITOTATT="+cp_ToStrODBC(this.w_AITOTATT)+;
             ",AITA_NIM="+cp_ToStrODBC(this.w_AITA_NIM)+;
             ",AITA_ESE="+cp_ToStrODBC(this.w_AITA_ESE)+;
             ",AITA_CIN="+cp_ToStrODBC(this.w_AITA_CIN)+;
             ",AITA_CBS="+cp_ToStrODBC(this.w_AITA_CBS)+;
             ",AITOTPAS="+cp_ToStrODBC(this.w_AITOTPAS)+;
             ",AITP_NIM="+cp_ToStrODBC(this.w_AITP_NIM)+;
             ",AITP_ESE="+cp_ToStrODBC(this.w_AITP_ESE)+;
             ",AITP_CIN="+cp_ToStrODBC(this.w_AITP_CIN)+;
             ",AITP_CBS="+cp_ToStrODBC(this.w_AITP_CBS)+;
             ",AIIMPORO="+cp_ToStrODBC(this.w_AIIMPORO)+;
             ",AIIVAORO="+cp_ToStrODBC(this.w_AIIVAORO)+;
             ",AIIMPROT="+cp_ToStrODBC(this.w_AIIMPROT)+;
             ",AIIVAROT="+cp_ToStrODBC(this.w_AIIVAROT)+;
             ",AIIVAESI="+cp_ToStrODBC(this.w_AIIVAESI)+;
             ",AIIVADET="+cp_ToStrODBC(this.w_AIIVADET)+;
             ",AIIVADOV="+cp_ToStrODBC(this.w_AIIVADOV)+;
             ",AIIVACRE="+cp_ToStrODBC(this.w_AIIVACRE)+;
             ",AIFIRMAC="+cp_ToStrODBC(this.w_AIFIRMAC)+;
             ",AICOFINT="+cp_ToStrODBC(this.w_AICOFINT)+;
             ",AIALBCAF="+cp_ToStrODBC(this.w_AIALBCAF)+;
             ",AIINVCON="+cp_ToStrODBC(this.w_AIINVCON)+;
             ",AIINVSOG="+cp_ToStrODBC(this.w_AIINVSOG)+;
             ",AIDATIMP="+cp_ToStrODBC(this.w_AIDATIMP)+;
             ",AIFIRMA="+cp_ToStrODBC(this.w_AIFIRMA)+;
             ",AIPERAZI="+cp_ToStrODBC(this.w_AIPERAZI)+;
             ",NOMEFILE="+cp_ToStrODBC(this.w_NOMEFILE)+;
             ",AIDENOMI="+cp_ToStrODBC(this.w_AIDENOMI)+;
             ",AICOGNOM="+cp_ToStrODBC(this.w_AICOGNOM)+;
             ",AINOME="+cp_ToStrODBC(this.w_AINOME)+;
             ",AISESSO="+cp_ToStrODBC(this.w_AISESSO)+;
             ",AISE_CAP="+cp_ToStrODBC(this.w_AISE_CAP)+;
             ",AISECOMU="+cp_ToStrODBC(this.w_AISECOMU)+;
             ",AICOMUNE="+cp_ToStrODBC(this.w_AICOMUNE)+;
             ",AISESIGL="+cp_ToStrODBC(this.w_AISESIGL)+;
             ",AISEIND2="+cp_ToStrODBC(this.w_AISEIND2)+;
             ",AIDATNAS="+cp_ToStrODBC(this.w_AIDATNAS)+;
             ",AISIGLA="+cp_ToStrODBC(this.w_AISIGLA)+;
             ",AIINDIRI="+cp_ToStrODBC(this.w_AIINDIRI)+;
             ",AI___CAP="+cp_ToStrODBC(this.w_AI___CAP)+;
             ",AIRESSIG="+cp_ToStrODBC(this.w_AIRESSIG)+;
             ",AISERCAP="+cp_ToStrODBC(this.w_AISERCAP)+;
             ",AISERSIG="+cp_ToStrODBC(this.w_AISERSIG)+;
             ",AIRESCOM="+cp_ToStrODBC(this.w_AIRESCOM)+;
             ",AISERCOM="+cp_ToStrODBC(this.w_AISERCOM)+;
             ",AISERIND="+cp_ToStrODBC(this.w_AISERIND)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_COAN')
        i_cWhere = cp_PKFox(i_cTable  ,'AIANNIMP',this.w_AIANNIMP  ,'AIDATINV',this.w_AIDATINV  )
        UPDATE (i_cTable) SET;
              AIPARIVA=this.w_AIPARIVA;
             ,AICODAT1=this.w_AICODAT1;
             ,AITIPFOR=this.w_AITIPFOR;
             ,AICONSEP=this.w_AICONSEP;
             ,AISOCGRU=this.w_AISOCGRU;
             ,AIEVEECC=this.w_AIEVEECC;
             ,EDITADICH=this.w_EDITADICH;
             ,AICODFIS=this.w_AICODFIS;
             ,AIFLCODF=this.w_AIFLCODF;
             ,AICODIVA=this.w_AICODIVA;
             ,AICODCAR=this.w_AICODCAR;
             ,AIMANCOR=this.w_AIMANCOR;
             ,AIINOLTRO=this.w_AIINOLTRO;
             ,AITOTATT=this.w_AITOTATT;
             ,AITA_NIM=this.w_AITA_NIM;
             ,AITA_ESE=this.w_AITA_ESE;
             ,AITA_CIN=this.w_AITA_CIN;
             ,AITA_CBS=this.w_AITA_CBS;
             ,AITOTPAS=this.w_AITOTPAS;
             ,AITP_NIM=this.w_AITP_NIM;
             ,AITP_ESE=this.w_AITP_ESE;
             ,AITP_CIN=this.w_AITP_CIN;
             ,AITP_CBS=this.w_AITP_CBS;
             ,AIIMPORO=this.w_AIIMPORO;
             ,AIIVAORO=this.w_AIIVAORO;
             ,AIIMPROT=this.w_AIIMPROT;
             ,AIIVAROT=this.w_AIIVAROT;
             ,AIIVAESI=this.w_AIIVAESI;
             ,AIIVADET=this.w_AIIVADET;
             ,AIIVADOV=this.w_AIIVADOV;
             ,AIIVACRE=this.w_AIIVACRE;
             ,AIFIRMAC=this.w_AIFIRMAC;
             ,AICOFINT=this.w_AICOFINT;
             ,AIALBCAF=this.w_AIALBCAF;
             ,AIINVCON=this.w_AIINVCON;
             ,AIINVSOG=this.w_AIINVSOG;
             ,AIDATIMP=this.w_AIDATIMP;
             ,AIFIRMA=this.w_AIFIRMA;
             ,AIPERAZI=this.w_AIPERAZI;
             ,NOMEFILE=this.w_NOMEFILE;
             ,AIDENOMI=this.w_AIDENOMI;
             ,AICOGNOM=this.w_AICOGNOM;
             ,AINOME=this.w_AINOME;
             ,AISESSO=this.w_AISESSO;
             ,AISE_CAP=this.w_AISE_CAP;
             ,AISECOMU=this.w_AISECOMU;
             ,AICOMUNE=this.w_AICOMUNE;
             ,AISESIGL=this.w_AISESIGL;
             ,AISEIND2=this.w_AISEIND2;
             ,AIDATNAS=this.w_AIDATNAS;
             ,AISIGLA=this.w_AISIGLA;
             ,AIINDIRI=this.w_AIINDIRI;
             ,AI___CAP=this.w_AI___CAP;
             ,AIRESSIG=this.w_AIRESSIG;
             ,AISERCAP=this.w_AISERCAP;
             ,AISERSIG=this.w_AISERSIG;
             ,AIRESCOM=this.w_AIRESCOM;
             ,AISERCOM=this.w_AISERCOM;
             ,AISERIND=this.w_AISERIND;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_COAN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOD_COAN_IDX,i_nConn)
      *
      * delete MOD_COAN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AIANNIMP',this.w_AIANNIMP  ,'AIDATINV',this.w_AIDATINV  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_COAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_COAN_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
          .link_1_2('Full')
        .DoRTCalc(3,11,.t.)
        if .o_CODAZI<>.w_CODAZI
            .w_CODFIS = IIF(EMPTY(.w_CODFIS),IIF(EMPTY(.w_AZCODFIS),.w_AZPIVAZI,.w_AZCODFIS),.w_CODFIS)
        endif
        .oPgFrm.Page1.oPag.GSAR2BCD.Calculate()
        .DoRTCalc(13,20,.t.)
        if .o_EDITADICH<>.w_EDITADICH
            .w_AICODFIS = IIF(.w_EDITADICH='S' and .cFunction<>'Query',.w_AZIVACOF,space(16))
        endif
        if .o_AICODFIS<>.w_AICODFIS
            .w_AIFLCODF = '0'
        endif
        if .o_EDITADICH<>.w_EDITADICH
            .w_AICODIVA = space(11)
        endif
        .DoRTCalc(24,27,.t.)
          .link_1_44('Full')
        if .o_COGTIT<>.w_COGTIT
            .w_COGTIT = LEFT(ALLTRIM(.w_COGTIT),24)
        endif
        if .o_NOMTIT<>.w_NOMTIT
            .w_NOMTIT = LEFT(ALLTRIM(.w_NOMTIT),20)
        endif
        .DoRTCalc(31,46,.t.)
        if .o_AIIVAESI<>.w_AIIVAESI.or. .o_AIIVADET<>.w_AIIVADET
            .w_AIIVADOV = IIF(.w_AIIVAESI>=.w_AIIVADET,.w_AIIVAESI-.w_AIIVADET,0)
        endif
        if .o_AIIVAESI<>.w_AIIVAESI.or. .o_AIIVADET<>.w_AIIVADET
            .w_AIIVACRE = IIF(.w_AIIVAESI<.w_AIIVADET,.w_AIIVADET-.w_AIIVAESI,0)
        endif
        .DoRTCalc(49,49,.t.)
        if .o_AITIPFOR<>.w_AITIPFOR
            .w_AICOFINT = iif(.w_AITIPFOR<>'01' AND .w_AITIPFOR<>'02',.w_AICOFINT,space(16))
        endif
        if .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIALBCAF = IIF(empty(.w_AICOFINT),space(5),.w_AIALBCAF)
        endif
        if .o_AICOFINT<>.w_AICOFINT.or. .o_AIINVSOG<>.w_AIINVSOG.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIINVCON = IIF(NOT EMPTY(.w_AICOFINT) AND .w_AIINVSOG='0','1','0')
        endif
        if .o_AIINVCON<>.w_AIINVCON.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIINVSOG = IIF(NOT EMPTY(.w_AICOFINT) AND .w_AIINVCON='0','1','0')
        endif
        if .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIDATIMP = IIF(empty(.w_AICOFINT),cp_CharToDate('  -  -    '),.w_AIDATIMP)
        endif
        if .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIFIRMA = IIF(NOT EMPTY(.w_AICOFINT),.w_AIFIRMA,'0')
        endif
        .oPgFrm.Page4.oPag.oObj_4_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .DoRTCalc(56,58,.t.)
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIDENOMI = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AIDENOMI,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AICOGNOM = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AICOGNOM,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AINOME = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AINOME,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI
            .w_AISESSO = 'M'
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISE_CAP = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISE_CAP,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISECOMU = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISECOMU,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AICOMUNE = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AICOMUNE,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT
            .w_AISESIGL = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISESIGL,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AITIPFOR<>.w_AITIPFOR.or. .o_AICOFINT<>.w_AICOFINT
            .w_AISEIND2 = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISEIND2,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIDATNAS = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIDATNAS,cp_CharToDate('  -  -  '))
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISIGLA = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AISIGLA,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIINDIRI = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIINDIRI,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AI___CAP = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AI___CAP,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIRESSIG = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIRESSIG,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISERCAP = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERCAP,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICODFIS<>.w_AICODFIS.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISERSIG = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERSIG,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AIRESCOM = iif(.w_AIPERAZI='S' and !empty(.w_AICOFINT),.w_AIRESCOM,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISERCOM = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERCOM,' ')
        endif
        if .o_AIPERAZI<>.w_AIPERAZI.or. .o_AICOFINT<>.w_AICOFINT.or. .o_AITIPFOR<>.w_AITIPFOR
            .w_AISERIND = iif(.w_AIPERAZI<>'S' and !empty(.w_AICOFINT),.w_AISERIND,' ')
        endif
        * --- Area Manuale = Calculate
        * --- gsar2acd
        if .o_EDITADICH<>.w_EDITADICH
              .o_EDITADICH=.w_EDITADICH
        endif
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.GSAR2BCD.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
    endwith
  return

  proc Calculate_DSSETUBQAG()
    with this
          * --- Valorizza ragione sociale
          .w_RAGSOC = g_RAGAZI
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODFIS_1_12.enabled = this.oPgFrm.Page1.oPag.oCODFIS_1_12.mCond()
    this.oPgFrm.Page1.oPag.oAIPARIVA_1_13.enabled = this.oPgFrm.Page1.oPag.oAIPARIVA_1_13.mCond()
    this.oPgFrm.Page1.oPag.oAITIPFOR_1_15.enabled = this.oPgFrm.Page1.oPag.oAITIPFOR_1_15.mCond()
    this.oPgFrm.Page1.oPag.oAICONSEP_1_16.enabled = this.oPgFrm.Page1.oPag.oAICONSEP_1_16.mCond()
    this.oPgFrm.Page1.oPag.oAISOCGRU_1_29.enabled = this.oPgFrm.Page1.oPag.oAISOCGRU_1_29.mCond()
    this.oPgFrm.Page1.oPag.oAIEVEECC_1_30.enabled = this.oPgFrm.Page1.oPag.oAIEVEECC_1_30.mCond()
    this.oPgFrm.Page1.oPag.oAICODFIS_1_33.enabled = this.oPgFrm.Page1.oPag.oAICODFIS_1_33.mCond()
    this.oPgFrm.Page1.oPag.oAIFLCODF_1_34.enabled = this.oPgFrm.Page1.oPag.oAIFLCODF_1_34.mCond()
    this.oPgFrm.Page1.oPag.oAICODIVA_1_35.enabled = this.oPgFrm.Page1.oPag.oAICODIVA_1_35.mCond()
    this.oPgFrm.Page1.oPag.oAICODCAR_1_36.enabled = this.oPgFrm.Page1.oPag.oAICODCAR_1_36.mCond()
    this.oPgFrm.Page1.oPag.oAIMANCOR_1_37.enabled = this.oPgFrm.Page1.oPag.oAIMANCOR_1_37.mCond()
    this.oPgFrm.Page2.oPag.oAITOTATT_2_9.enabled = this.oPgFrm.Page2.oPag.oAITOTATT_2_9.mCond()
    this.oPgFrm.Page2.oPag.oAITA_NIM_2_12.enabled = this.oPgFrm.Page2.oPag.oAITA_NIM_2_12.mCond()
    this.oPgFrm.Page2.oPag.oAITA_ESE_2_14.enabled = this.oPgFrm.Page2.oPag.oAITA_ESE_2_14.mCond()
    this.oPgFrm.Page2.oPag.oAITA_CIN_2_16.enabled = this.oPgFrm.Page2.oPag.oAITA_CIN_2_16.mCond()
    this.oPgFrm.Page2.oPag.oAITA_CBS_2_17.enabled = this.oPgFrm.Page2.oPag.oAITA_CBS_2_17.mCond()
    this.oPgFrm.Page2.oPag.oAITOTPAS_2_28.enabled = this.oPgFrm.Page2.oPag.oAITOTPAS_2_28.mCond()
    this.oPgFrm.Page2.oPag.oAITP_NIM_2_31.enabled = this.oPgFrm.Page2.oPag.oAITP_NIM_2_31.mCond()
    this.oPgFrm.Page2.oPag.oAITP_ESE_2_33.enabled = this.oPgFrm.Page2.oPag.oAITP_ESE_2_33.mCond()
    this.oPgFrm.Page2.oPag.oAITP_CIN_2_35.enabled = this.oPgFrm.Page2.oPag.oAITP_CIN_2_35.mCond()
    this.oPgFrm.Page2.oPag.oAITP_CBS_2_36.enabled = this.oPgFrm.Page2.oPag.oAITP_CBS_2_36.mCond()
    this.oPgFrm.Page2.oPag.oAIIMPORO_2_38.enabled = this.oPgFrm.Page2.oPag.oAIIMPORO_2_38.mCond()
    this.oPgFrm.Page2.oPag.oAIIVAORO_2_39.enabled = this.oPgFrm.Page2.oPag.oAIIVAORO_2_39.mCond()
    this.oPgFrm.Page2.oPag.oAIIMPROT_2_40.enabled = this.oPgFrm.Page2.oPag.oAIIMPROT_2_40.mCond()
    this.oPgFrm.Page2.oPag.oAIIVAROT_2_41.enabled = this.oPgFrm.Page2.oPag.oAIIVAROT_2_41.mCond()
    this.oPgFrm.Page3.oPag.oAIIVAESI_3_8.enabled = this.oPgFrm.Page3.oPag.oAIIVAESI_3_8.mCond()
    this.oPgFrm.Page3.oPag.oAIIVADET_3_12.enabled = this.oPgFrm.Page3.oPag.oAIIVADET_3_12.mCond()
    this.oPgFrm.Page3.oPag.oAIFIRMAC_3_19.enabled = this.oPgFrm.Page3.oPag.oAIFIRMAC_3_19.mCond()
    this.oPgFrm.Page3.oPag.oAICOFINT_3_21.enabled = this.oPgFrm.Page3.oPag.oAICOFINT_3_21.mCond()
    this.oPgFrm.Page3.oPag.oAIALBCAF_3_25.enabled = this.oPgFrm.Page3.oPag.oAIALBCAF_3_25.mCond()
    this.oPgFrm.Page3.oPag.oAIINVCON_3_30.enabled = this.oPgFrm.Page3.oPag.oAIINVCON_3_30.mCond()
    this.oPgFrm.Page3.oPag.oAIINVSOG_3_33.enabled = this.oPgFrm.Page3.oPag.oAIINVSOG_3_33.mCond()
    this.oPgFrm.Page3.oPag.oAIDATIMP_3_36.enabled = this.oPgFrm.Page3.oPag.oAIDATIMP_3_36.mCond()
    this.oPgFrm.Page3.oPag.oAIFIRMA_3_40.enabled = this.oPgFrm.Page3.oPag.oAIFIRMA_3_40.mCond()
    this.oPgFrm.Page4.oPag.oAIPERAZI_4_1.enabled = this.oPgFrm.Page4.oPag.oAIPERAZI_4_1.mCond()
    this.oPgFrm.Page4.oPag.oAIDENOMI_4_12.enabled = this.oPgFrm.Page4.oPag.oAIDENOMI_4_12.mCond()
    this.oPgFrm.Page4.oPag.oAICOGNOM_4_13.enabled = this.oPgFrm.Page4.oPag.oAICOGNOM_4_13.mCond()
    this.oPgFrm.Page4.oPag.oAINOME_4_14.enabled = this.oPgFrm.Page4.oPag.oAINOME_4_14.mCond()
    this.oPgFrm.Page4.oPag.oAISESSO_4_15.enabled = this.oPgFrm.Page4.oPag.oAISESSO_4_15.mCond()
    this.oPgFrm.Page4.oPag.oAISE_CAP_4_17.enabled = this.oPgFrm.Page4.oPag.oAISE_CAP_4_17.mCond()
    this.oPgFrm.Page4.oPag.oAISECOMU_4_18.enabled = this.oPgFrm.Page4.oPag.oAISECOMU_4_18.mCond()
    this.oPgFrm.Page4.oPag.oAICOMUNE_4_19.enabled = this.oPgFrm.Page4.oPag.oAICOMUNE_4_19.mCond()
    this.oPgFrm.Page4.oPag.oAISESIGL_4_20.enabled = this.oPgFrm.Page4.oPag.oAISESIGL_4_20.mCond()
    this.oPgFrm.Page4.oPag.oAISEIND2_4_21.enabled = this.oPgFrm.Page4.oPag.oAISEIND2_4_21.mCond()
    this.oPgFrm.Page4.oPag.oAIDATNAS_4_22.enabled = this.oPgFrm.Page4.oPag.oAIDATNAS_4_22.mCond()
    this.oPgFrm.Page4.oPag.oAISIGLA_4_23.enabled = this.oPgFrm.Page4.oPag.oAISIGLA_4_23.mCond()
    this.oPgFrm.Page4.oPag.oAIINDIRI_4_24.enabled = this.oPgFrm.Page4.oPag.oAIINDIRI_4_24.mCond()
    this.oPgFrm.Page4.oPag.oAI___CAP_4_25.enabled = this.oPgFrm.Page4.oPag.oAI___CAP_4_25.mCond()
    this.oPgFrm.Page4.oPag.oAIRESSIG_4_26.enabled = this.oPgFrm.Page4.oPag.oAIRESSIG_4_26.mCond()
    this.oPgFrm.Page4.oPag.oAISERCAP_4_27.enabled = this.oPgFrm.Page4.oPag.oAISERCAP_4_27.mCond()
    this.oPgFrm.Page4.oPag.oAISERSIG_4_28.enabled = this.oPgFrm.Page4.oPag.oAISERSIG_4_28.mCond()
    this.oPgFrm.Page4.oPag.oAIRESCOM_4_29.enabled = this.oPgFrm.Page4.oPag.oAIRESCOM_4_29.mCond()
    this.oPgFrm.Page4.oPag.oAISERCOM_4_30.enabled = this.oPgFrm.Page4.oPag.oAISERCOM_4_30.mCond()
    this.oPgFrm.Page4.oPag.oAISERIND_4_31.enabled = this.oPgFrm.Page4.oPag.oAISERIND_4_31.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_42.enabled = this.oPgFrm.Page2.oPag.oBtn_2_42.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_8.enabled = this.oPgFrm.Page4.oPag.oBtn_4_8.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_9.enabled = this.oPgFrm.Page4.oPag.oBtn_4_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oRAGSOC_1_21.visible=!this.oPgFrm.Page1.oPag.oRAGSOC_1_21.mHide()
    this.oPgFrm.Page1.oPag.oEDITADICH_1_31.visible=!this.oPgFrm.Page1.oPag.oEDITADICH_1_31.mHide()
    this.oPgFrm.Page1.oPag.oAIFLCODF_1_34.visible=!this.oPgFrm.Page1.oPag.oAIFLCODF_1_34.mHide()
    this.oPgFrm.Page1.oPag.oCOGTIT_1_45.visible=!this.oPgFrm.Page1.oPag.oCOGTIT_1_45.mHide()
    this.oPgFrm.Page1.oPag.oNOMTIT_1_46.visible=!this.oPgFrm.Page1.oPag.oNOMTIT_1_46.mHide()
    this.oPgFrm.Page2.oPag.oAITA_CBS_2_17.visible=!this.oPgFrm.Page2.oPag.oAITA_CBS_2_17.mHide()
    this.oPgFrm.Page2.oPag.oAITP_CBS_2_36.visible=!this.oPgFrm.Page2.oPag.oAITP_CBS_2_36.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_11.visible=!this.oPgFrm.Page4.oPag.oStr_4_11.mHide()
    this.oPgFrm.Page4.oPag.oAIDENOMI_4_12.visible=!this.oPgFrm.Page4.oPag.oAIDENOMI_4_12.mHide()
    this.oPgFrm.Page4.oPag.oAICOGNOM_4_13.visible=!this.oPgFrm.Page4.oPag.oAICOGNOM_4_13.mHide()
    this.oPgFrm.Page4.oPag.oAINOME_4_14.visible=!this.oPgFrm.Page4.oPag.oAINOME_4_14.mHide()
    this.oPgFrm.Page4.oPag.oAISESSO_4_15.visible=!this.oPgFrm.Page4.oPag.oAISESSO_4_15.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_16.visible=!this.oPgFrm.Page4.oPag.oStr_4_16.mHide()
    this.oPgFrm.Page4.oPag.oAISE_CAP_4_17.visible=!this.oPgFrm.Page4.oPag.oAISE_CAP_4_17.mHide()
    this.oPgFrm.Page4.oPag.oAISECOMU_4_18.visible=!this.oPgFrm.Page4.oPag.oAISECOMU_4_18.mHide()
    this.oPgFrm.Page4.oPag.oAICOMUNE_4_19.visible=!this.oPgFrm.Page4.oPag.oAICOMUNE_4_19.mHide()
    this.oPgFrm.Page4.oPag.oAISESIGL_4_20.visible=!this.oPgFrm.Page4.oPag.oAISESIGL_4_20.mHide()
    this.oPgFrm.Page4.oPag.oAISEIND2_4_21.visible=!this.oPgFrm.Page4.oPag.oAISEIND2_4_21.mHide()
    this.oPgFrm.Page4.oPag.oAIDATNAS_4_22.visible=!this.oPgFrm.Page4.oPag.oAIDATNAS_4_22.mHide()
    this.oPgFrm.Page4.oPag.oAISIGLA_4_23.visible=!this.oPgFrm.Page4.oPag.oAISIGLA_4_23.mHide()
    this.oPgFrm.Page4.oPag.oAIINDIRI_4_24.visible=!this.oPgFrm.Page4.oPag.oAIINDIRI_4_24.mHide()
    this.oPgFrm.Page4.oPag.oAI___CAP_4_25.visible=!this.oPgFrm.Page4.oPag.oAI___CAP_4_25.mHide()
    this.oPgFrm.Page4.oPag.oAIRESSIG_4_26.visible=!this.oPgFrm.Page4.oPag.oAIRESSIG_4_26.mHide()
    this.oPgFrm.Page4.oPag.oAISERCAP_4_27.visible=!this.oPgFrm.Page4.oPag.oAISERCAP_4_27.mHide()
    this.oPgFrm.Page4.oPag.oAISERSIG_4_28.visible=!this.oPgFrm.Page4.oPag.oAISERSIG_4_28.mHide()
    this.oPgFrm.Page4.oPag.oAIRESCOM_4_29.visible=!this.oPgFrm.Page4.oPag.oAIRESCOM_4_29.mHide()
    this.oPgFrm.Page4.oPag.oAISERCOM_4_30.visible=!this.oPgFrm.Page4.oPag.oAISERCOM_4_30.mHide()
    this.oPgFrm.Page4.oPag.oAISERIND_4_31.visible=!this.oPgFrm.Page4.oPag.oAISERIND_4_31.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_32.visible=!this.oPgFrm.Page4.oPag.oStr_4_32.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_33.visible=!this.oPgFrm.Page4.oPag.oStr_4_33.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_34.visible=!this.oPgFrm.Page4.oPag.oStr_4_34.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_35.visible=!this.oPgFrm.Page4.oPag.oStr_4_35.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_36.visible=!this.oPgFrm.Page4.oPag.oStr_4_36.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_37.visible=!this.oPgFrm.Page4.oPag.oStr_4_37.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_38.visible=!this.oPgFrm.Page4.oPag.oStr_4_38.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_39.visible=!this.oPgFrm.Page4.oPag.oStr_4_39.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_40.visible=!this.oPgFrm.Page4.oPag.oStr_4_40.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_41.visible=!this.oPgFrm.Page4.oPag.oStr_4_41.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_42.visible=!this.oPgFrm.Page4.oPag.oStr_4_42.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_43.visible=!this.oPgFrm.Page4.oPag.oStr_4_43.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_44.visible=!this.oPgFrm.Page4.oPag.oStr_4_44.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_45.visible=!this.oPgFrm.Page4.oPag.oStr_4_45.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_46.visible=!this.oPgFrm.Page4.oPag.oStr_4_46.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_47.visible=!this.oPgFrm.Page4.oPag.oStr_4_47.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_48.visible=!this.oPgFrm.Page4.oPag.oStr_4_48.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_46.visible=!this.oPgFrm.Page2.oPag.oStr_2_46.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_47.visible=!this.oPgFrm.Page2.oPag.oStr_2_47.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.GSAR2BCD.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_2.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
        if lower(cEvent)==lower("Edit Started") or lower(cEvent)==lower("New record")
          .Calculate_DSSETUBQAG()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZIVACAR,AZPIVAZI,AZCOFAZI,AZPERAZI,AZIVACOF,AZCODNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZIVACAR,AZPIVAZI,AZCOFAZI,AZPERAZI,AZIVACOF,AZCODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZCODCAR = NVL(_Link_.AZIVACAR,space(2))
      this.w_AZPIVAZI = NVL(_Link_.AZPIVAZI,space(12))
      this.w_AZCODFIS = NVL(_Link_.AZCOFAZI,space(16))
      this.w_AZPERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_AZIVACOF = NVL(_Link_.AZIVACOF,space(16))
      this.w_AZCODNAZ = NVL(_Link_.AZCODNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZCODCAR = space(2)
      this.w_AZPIVAZI = space(12)
      this.w_AZCODFIS = space(16)
      this.w_AZPERAZI = space(1)
      this.w_AZIVACOF = space(16)
      this.w_AZCODNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI1
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTCOGTIT,TTNOMTIT";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_CODAZI1)
            select TTCODAZI,TTCOGTIT,TTNOMTIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.TTCODAZI,space(5))
      this.w_COGTIT = NVL(_Link_.TTCOGTIT,space(25))
      this.w_NOMTIT = NVL(_Link_.TTNOMTIT,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(5)
      endif
      this.w_COGTIT = space(25)
      this.w_NOMTIT = space(25)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAIANNIMP_1_10.value==this.w_AIANNIMP)
      this.oPgFrm.Page1.oPag.oAIANNIMP_1_10.value=this.w_AIANNIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oAIDATINV_1_11.value==this.w_AIDATINV)
      this.oPgFrm.Page1.oPag.oAIDATINV_1_11.value=this.w_AIDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIS_1_12.value==this.w_CODFIS)
      this.oPgFrm.Page1.oPag.oCODFIS_1_12.value=this.w_CODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oAIPARIVA_1_13.value==this.w_AIPARIVA)
      this.oPgFrm.Page1.oPag.oAIPARIVA_1_13.value=this.w_AIPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oAICODAT1_1_14.value==this.w_AICODAT1)
      this.oPgFrm.Page1.oPag.oAICODAT1_1_14.value=this.w_AICODAT1
    endif
    if not(this.oPgFrm.Page1.oPag.oAITIPFOR_1_15.RadioValue()==this.w_AITIPFOR)
      this.oPgFrm.Page1.oPag.oAITIPFOR_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAICONSEP_1_16.RadioValue()==this.w_AICONSEP)
      this.oPgFrm.Page1.oPag.oAICONSEP_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOC_1_21.value==this.w_RAGSOC)
      this.oPgFrm.Page1.oPag.oRAGSOC_1_21.value=this.w_RAGSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oAISOCGRU_1_29.RadioValue()==this.w_AISOCGRU)
      this.oPgFrm.Page1.oPag.oAISOCGRU_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAIEVEECC_1_30.RadioValue()==this.w_AIEVEECC)
      this.oPgFrm.Page1.oPag.oAIEVEECC_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEDITADICH_1_31.RadioValue()==this.w_EDITADICH)
      this.oPgFrm.Page1.oPag.oEDITADICH_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAICODFIS_1_33.value==this.w_AICODFIS)
      this.oPgFrm.Page1.oPag.oAICODFIS_1_33.value=this.w_AICODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oAIFLCODF_1_34.RadioValue()==this.w_AIFLCODF)
      this.oPgFrm.Page1.oPag.oAIFLCODF_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAICODIVA_1_35.value==this.w_AICODIVA)
      this.oPgFrm.Page1.oPag.oAICODIVA_1_35.value=this.w_AICODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oAICODCAR_1_36.RadioValue()==this.w_AICODCAR)
      this.oPgFrm.Page1.oPag.oAICODCAR_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAIMANCOR_1_37.RadioValue()==this.w_AIMANCOR)
      this.oPgFrm.Page1.oPag.oAIMANCOR_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOGTIT_1_45.value==this.w_COGTIT)
      this.oPgFrm.Page1.oPag.oCOGTIT_1_45.value=this.w_COGTIT
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMTIT_1_46.value==this.w_NOMTIT)
      this.oPgFrm.Page1.oPag.oNOMTIT_1_46.value=this.w_NOMTIT
    endif
    if not(this.oPgFrm.Page2.oPag.oAITOTATT_2_9.value==this.w_AITOTATT)
      this.oPgFrm.Page2.oPag.oAITOTATT_2_9.value=this.w_AITOTATT
    endif
    if not(this.oPgFrm.Page2.oPag.oAITA_NIM_2_12.value==this.w_AITA_NIM)
      this.oPgFrm.Page2.oPag.oAITA_NIM_2_12.value=this.w_AITA_NIM
    endif
    if not(this.oPgFrm.Page2.oPag.oAITA_ESE_2_14.value==this.w_AITA_ESE)
      this.oPgFrm.Page2.oPag.oAITA_ESE_2_14.value=this.w_AITA_ESE
    endif
    if not(this.oPgFrm.Page2.oPag.oAITA_CIN_2_16.value==this.w_AITA_CIN)
      this.oPgFrm.Page2.oPag.oAITA_CIN_2_16.value=this.w_AITA_CIN
    endif
    if not(this.oPgFrm.Page2.oPag.oAITA_CBS_2_17.value==this.w_AITA_CBS)
      this.oPgFrm.Page2.oPag.oAITA_CBS_2_17.value=this.w_AITA_CBS
    endif
    if not(this.oPgFrm.Page2.oPag.oAITOTPAS_2_28.value==this.w_AITOTPAS)
      this.oPgFrm.Page2.oPag.oAITOTPAS_2_28.value=this.w_AITOTPAS
    endif
    if not(this.oPgFrm.Page2.oPag.oAITP_NIM_2_31.value==this.w_AITP_NIM)
      this.oPgFrm.Page2.oPag.oAITP_NIM_2_31.value=this.w_AITP_NIM
    endif
    if not(this.oPgFrm.Page2.oPag.oAITP_ESE_2_33.value==this.w_AITP_ESE)
      this.oPgFrm.Page2.oPag.oAITP_ESE_2_33.value=this.w_AITP_ESE
    endif
    if not(this.oPgFrm.Page2.oPag.oAITP_CIN_2_35.value==this.w_AITP_CIN)
      this.oPgFrm.Page2.oPag.oAITP_CIN_2_35.value=this.w_AITP_CIN
    endif
    if not(this.oPgFrm.Page2.oPag.oAITP_CBS_2_36.value==this.w_AITP_CBS)
      this.oPgFrm.Page2.oPag.oAITP_CBS_2_36.value=this.w_AITP_CBS
    endif
    if not(this.oPgFrm.Page2.oPag.oAIIMPORO_2_38.value==this.w_AIIMPORO)
      this.oPgFrm.Page2.oPag.oAIIMPORO_2_38.value=this.w_AIIMPORO
    endif
    if not(this.oPgFrm.Page2.oPag.oAIIVAORO_2_39.value==this.w_AIIVAORO)
      this.oPgFrm.Page2.oPag.oAIIVAORO_2_39.value=this.w_AIIVAORO
    endif
    if not(this.oPgFrm.Page2.oPag.oAIIMPROT_2_40.value==this.w_AIIMPROT)
      this.oPgFrm.Page2.oPag.oAIIMPROT_2_40.value=this.w_AIIMPROT
    endif
    if not(this.oPgFrm.Page2.oPag.oAIIVAROT_2_41.value==this.w_AIIVAROT)
      this.oPgFrm.Page2.oPag.oAIIVAROT_2_41.value=this.w_AIIVAROT
    endif
    if not(this.oPgFrm.Page3.oPag.oAIIVAESI_3_8.value==this.w_AIIVAESI)
      this.oPgFrm.Page3.oPag.oAIIVAESI_3_8.value=this.w_AIIVAESI
    endif
    if not(this.oPgFrm.Page3.oPag.oAIIVADET_3_12.value==this.w_AIIVADET)
      this.oPgFrm.Page3.oPag.oAIIVADET_3_12.value=this.w_AIIVADET
    endif
    if not(this.oPgFrm.Page3.oPag.oAIIVADOV_3_16.value==this.w_AIIVADOV)
      this.oPgFrm.Page3.oPag.oAIIVADOV_3_16.value=this.w_AIIVADOV
    endif
    if not(this.oPgFrm.Page3.oPag.oAIIVACRE_3_18.value==this.w_AIIVACRE)
      this.oPgFrm.Page3.oPag.oAIIVACRE_3_18.value=this.w_AIIVACRE
    endif
    if not(this.oPgFrm.Page3.oPag.oAIFIRMAC_3_19.RadioValue()==this.w_AIFIRMAC)
      this.oPgFrm.Page3.oPag.oAIFIRMAC_3_19.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAICOFINT_3_21.value==this.w_AICOFINT)
      this.oPgFrm.Page3.oPag.oAICOFINT_3_21.value=this.w_AICOFINT
    endif
    if not(this.oPgFrm.Page3.oPag.oAIALBCAF_3_25.value==this.w_AIALBCAF)
      this.oPgFrm.Page3.oPag.oAIALBCAF_3_25.value=this.w_AIALBCAF
    endif
    if not(this.oPgFrm.Page3.oPag.oAIINVCON_3_30.RadioValue()==this.w_AIINVCON)
      this.oPgFrm.Page3.oPag.oAIINVCON_3_30.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAIINVSOG_3_33.RadioValue()==this.w_AIINVSOG)
      this.oPgFrm.Page3.oPag.oAIINVSOG_3_33.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAIDATIMP_3_36.value==this.w_AIDATIMP)
      this.oPgFrm.Page3.oPag.oAIDATIMP_3_36.value=this.w_AIDATIMP
    endif
    if not(this.oPgFrm.Page3.oPag.oAIFIRMA_3_40.RadioValue()==this.w_AIFIRMA)
      this.oPgFrm.Page3.oPag.oAIFIRMA_3_40.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAIPERAZI_4_1.RadioValue()==this.w_AIPERAZI)
      this.oPgFrm.Page4.oPag.oAIPERAZI_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oNOMEFILE_4_10.value==this.w_NOMEFILE)
      this.oPgFrm.Page4.oPag.oNOMEFILE_4_10.value=this.w_NOMEFILE
    endif
    if not(this.oPgFrm.Page4.oPag.oAIDENOMI_4_12.value==this.w_AIDENOMI)
      this.oPgFrm.Page4.oPag.oAIDENOMI_4_12.value=this.w_AIDENOMI
    endif
    if not(this.oPgFrm.Page4.oPag.oAICOGNOM_4_13.value==this.w_AICOGNOM)
      this.oPgFrm.Page4.oPag.oAICOGNOM_4_13.value=this.w_AICOGNOM
    endif
    if not(this.oPgFrm.Page4.oPag.oAINOME_4_14.value==this.w_AINOME)
      this.oPgFrm.Page4.oPag.oAINOME_4_14.value=this.w_AINOME
    endif
    if not(this.oPgFrm.Page4.oPag.oAISESSO_4_15.RadioValue()==this.w_AISESSO)
      this.oPgFrm.Page4.oPag.oAISESSO_4_15.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAISE_CAP_4_17.value==this.w_AISE_CAP)
      this.oPgFrm.Page4.oPag.oAISE_CAP_4_17.value=this.w_AISE_CAP
    endif
    if not(this.oPgFrm.Page4.oPag.oAISECOMU_4_18.value==this.w_AISECOMU)
      this.oPgFrm.Page4.oPag.oAISECOMU_4_18.value=this.w_AISECOMU
    endif
    if not(this.oPgFrm.Page4.oPag.oAICOMUNE_4_19.value==this.w_AICOMUNE)
      this.oPgFrm.Page4.oPag.oAICOMUNE_4_19.value=this.w_AICOMUNE
    endif
    if not(this.oPgFrm.Page4.oPag.oAISESIGL_4_20.value==this.w_AISESIGL)
      this.oPgFrm.Page4.oPag.oAISESIGL_4_20.value=this.w_AISESIGL
    endif
    if not(this.oPgFrm.Page4.oPag.oAISEIND2_4_21.value==this.w_AISEIND2)
      this.oPgFrm.Page4.oPag.oAISEIND2_4_21.value=this.w_AISEIND2
    endif
    if not(this.oPgFrm.Page4.oPag.oAIDATNAS_4_22.value==this.w_AIDATNAS)
      this.oPgFrm.Page4.oPag.oAIDATNAS_4_22.value=this.w_AIDATNAS
    endif
    if not(this.oPgFrm.Page4.oPag.oAISIGLA_4_23.value==this.w_AISIGLA)
      this.oPgFrm.Page4.oPag.oAISIGLA_4_23.value=this.w_AISIGLA
    endif
    if not(this.oPgFrm.Page4.oPag.oAIINDIRI_4_24.value==this.w_AIINDIRI)
      this.oPgFrm.Page4.oPag.oAIINDIRI_4_24.value=this.w_AIINDIRI
    endif
    if not(this.oPgFrm.Page4.oPag.oAI___CAP_4_25.value==this.w_AI___CAP)
      this.oPgFrm.Page4.oPag.oAI___CAP_4_25.value=this.w_AI___CAP
    endif
    if not(this.oPgFrm.Page4.oPag.oAIRESSIG_4_26.value==this.w_AIRESSIG)
      this.oPgFrm.Page4.oPag.oAIRESSIG_4_26.value=this.w_AIRESSIG
    endif
    if not(this.oPgFrm.Page4.oPag.oAISERCAP_4_27.value==this.w_AISERCAP)
      this.oPgFrm.Page4.oPag.oAISERCAP_4_27.value=this.w_AISERCAP
    endif
    if not(this.oPgFrm.Page4.oPag.oAISERSIG_4_28.value==this.w_AISERSIG)
      this.oPgFrm.Page4.oPag.oAISERSIG_4_28.value=this.w_AISERSIG
    endif
    if not(this.oPgFrm.Page4.oPag.oAIRESCOM_4_29.value==this.w_AIRESCOM)
      this.oPgFrm.Page4.oPag.oAIRESCOM_4_29.value=this.w_AIRESCOM
    endif
    if not(this.oPgFrm.Page4.oPag.oAISERCOM_4_30.value==this.w_AISERCOM)
      this.oPgFrm.Page4.oPag.oAISERCOM_4_30.value=this.w_AISERCOM
    endif
    if not(this.oPgFrm.Page4.oPag.oAISERIND_4_31.value==this.w_AISERIND)
      this.oPgFrm.Page4.oPag.oAISERIND_4_31.value=this.w_AISERIND
    endif
    cp_SetControlsValueExtFlds(this,'MOD_COAN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_AIANNIMP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIANNIMP_1_10.SetFocus()
            i_bnoObbl = !empty(.w_AIANNIMP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(YEAR(.w_AIDATINV) >2007)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIDATINV_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("questa maschera gestisce le dichiarazioni IVA relative ad anni successivi al 2007")
          case   not(CHKCFP(.w_CODFIS,"CF"))  and (EMPTY(.w_CODFIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIS_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_AIPARIVA,"PI","","", .w_AZCODNAZ))  and (.w_AIINOLTRO<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAIPARIVA_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AICODAT1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAICODAT1_1_14.SetFocus()
            i_bnoObbl = !empty(.w_AICODAT1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF(EMPTY(.w_AICODFIS),.t.,CHKCFP(.w_AICODFIS,"CF")))  and (.w_AIINOLTRO<>'S' and .w_EDITADICH='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAICODFIS_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF(EMPTY(.w_AICODIVA),.t.,CHKCFP(.w_AICODIVA,"PI","","", .w_AZCODNAZ)))  and (.w_AIINOLTRO<>'S' and .w_EDITADICH='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAICODIVA_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_AICOFINT,iif( .w_AITIPFOR='10',"CF"," ")))  and (.w_AIINOLTRO<>'S' AND .w_AITIPFOR<>'01' AND .w_AITIPFOR<>'02')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oAICOFINT_3_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_AITIPFOR = this.w_AITIPFOR
    this.o_EDITADICH = this.w_EDITADICH
    this.o_AICODFIS = this.w_AICODFIS
    this.o_COGTIT = this.w_COGTIT
    this.o_NOMTIT = this.w_NOMTIT
    this.o_AIIVAESI = this.w_AIIVAESI
    this.o_AIIVADET = this.w_AIIVADET
    this.o_AICOFINT = this.w_AICOFINT
    this.o_AIINVCON = this.w_AIINVCON
    this.o_AIINVSOG = this.w_AIINVSOG
    this.o_AIPERAZI = this.w_AIPERAZI
    return

enddefine

* --- Define pages as container
define class tgsar2acdPag1 as StdContainer
  Width  = 751
  height = 415
  stdWidth  = 751
  stdheight = 415
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAIANNIMP_1_10 as StdField with uid="AAATTTHPCX",rtseq=10,rtrep=.f.,;
    cFormVar = "w_AIANNIMP", cQueryName = "AIANNIMP",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno di imposta",;
    HelpContextID = 142875050,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=53, Left=222, Top=72, cSayPict='"9999"', cGetPict='"9999"'

  add object oAIDATINV_1_11 as StdField with uid="XIPFKJYRPB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_AIDATINV", cQueryName = "AIANNIMP,AIDATINV",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "questa maschera gestisce le dichiarazioni IVA relative ad anni successivi al 2007",;
    ToolTipText = "Data invio (per determinare chiave primaria)",;
    HelpContextID = 137423268,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=389, Top=72

  func oAIDATINV_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (YEAR(.w_AIDATINV) >2007)
    endwith
    return bRes
  endfunc

  add object oCODFIS_1_12 as StdField with uid="OCYCKRBZUY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODFIS", cQueryName = "CODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'azienda",;
    HelpContextID = 19143718,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=511, Top=22, cSayPict='"!!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!!"', InputMask=replicate('X',16)

  func oCODFIS_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_CODFIS))
    endwith
   endif
  endfunc

  func oCODFIS_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_CODFIS,"CF"))
    endwith
    return bRes
  endfunc

  add object oAIPARIVA_1_13 as StdField with uid="UYFPYTHNVF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_AIPARIVA", cQueryName = "AIPARIVA",;
    bObbl = .f. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA contribuente",;
    HelpContextID = 128964167,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=208, Top=137, cSayPict="'99999999999'", cGetPict="'99999999999'", InputMask=replicate('X',11)

  func oAIPARIVA_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAIPARIVA_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_AIPARIVA,"PI","","", .w_AZCODNAZ))
    endwith
    return bRes
  endfunc

  add object oAICODAT1_1_14 as StdField with uid="DQEKQXPJDA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_AICODAT1", cQueryName = "AICODAT1",;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 249366071,;
   bGlobalFont=.t.,;
    Height=21, Width=77, Left=496, Top=137, InputMask=replicate('X',6), bHasZoom = .t. 

  proc oAICODAT1_1_14.mZoom
    vx_exec("gsar1acd.vzm",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oAITIPFOR_1_15 as StdCombo with uid="XVVXXOWQAR",rtseq=15,rtrep=.f.,left=208,top=171,width=244,height=21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 191359400;
    , cFormVar="w_AITIPFOR",RowSource=""+""+L_combo1+","+""+L_combo5+"", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAITIPFOR_1_15.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'10',;
    space(2))))
  endfunc
  func oAITIPFOR_1_15.GetRadio()
    this.Parent.oContained.w_AITIPFOR = this.RadioValue()
    return .t.
  endfunc

  func oAITIPFOR_1_15.SetRadio()
    this.Parent.oContained.w_AITIPFOR=trim(this.Parent.oContained.w_AITIPFOR)
    this.value = ;
      iif(this.Parent.oContained.w_AITIPFOR=='01',1,;
      iif(this.Parent.oContained.w_AITIPFOR=='10',2,;
      0))
  endfunc

  func oAITIPFOR_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAICONSEP_1_16 as StdCheck with uid="UTQYYBJQWW",rtseq=16,rtrep=.f.,left=120, top=200, caption="Contabilit� separate",;
    ToolTipText = "Contabilit� separate",;
    HelpContextID = 24970838,;
    cFormVar="w_AICONSEP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAICONSEP_1_16.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAICONSEP_1_16.GetRadio()
    this.Parent.oContained.w_AICONSEP = this.RadioValue()
    return .t.
  endfunc

  func oAICONSEP_1_16.SetRadio()
    this.Parent.oContained.w_AICONSEP=trim(this.Parent.oContained.w_AICONSEP)
    this.value = ;
      iif(this.Parent.oContained.w_AICONSEP=='1',1,;
      0)
  endfunc

  func oAICONSEP_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oRAGSOC_1_21 as StdField with uid="ADZVIXCOEV",rtseq=17,rtrep=.f.,;
    cFormVar = "w_RAGSOC", cQueryName = "RAGSOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale dell'azienda",;
    HelpContextID = 242139370,;
   bGlobalFont=.t.,;
    Height=39, Width=486, Left=5, Top=22, InputMask=replicate('X',80)

  func oRAGSOC_1_21.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
  endfunc


  add object GSAR2BCD as cp_runprogram with uid="MORMIQQDYR",left=5, top=427, width=243,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR2BCD("INIT")',;
    cEvent = "w_AIANNIMP Changed,New record",;
    nPag=1;
    , HelpContextID = 65376230

  add object oAISOCGRU_1_29 as StdCheck with uid="YNLWMJGXMF",rtseq=18,rtrep=.f.,left=319, top=200, caption="Soc.ader.gruppo IVA",;
    ToolTipText = "Soc. aderente ad un gruppo IVA",;
    HelpContextID = 80610907,;
    cFormVar="w_AISOCGRU", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAISOCGRU_1_29.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAISOCGRU_1_29.GetRadio()
    this.Parent.oContained.w_AISOCGRU = this.RadioValue()
    return .t.
  endfunc

  func oAISOCGRU_1_29.SetRadio()
    this.Parent.oContained.w_AISOCGRU=trim(this.Parent.oContained.w_AISOCGRU)
    this.value = ;
      iif(this.Parent.oContained.w_AISOCGRU=='1',1,;
      0)
  endfunc

  func oAISOCGRU_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIEVEECC_1_30 as StdCheck with uid="YTJCWZOSFK",rtseq=19,rtrep=.f.,left=496, top=201, caption="Eventi eccezionali",;
    ToolTipText = "Eventi eccezionali",;
    HelpContextID = 49555017,;
    cFormVar="w_AIEVEECC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAIEVEECC_1_30.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIEVEECC_1_30.GetRadio()
    this.Parent.oContained.w_AIEVEECC = this.RadioValue()
    return .t.
  endfunc

  func oAIEVEECC_1_30.SetRadio()
    this.Parent.oContained.w_AIEVEECC=trim(this.Parent.oContained.w_AIEVEECC)
    this.value = ;
      iif(this.Parent.oContained.w_AIEVEECC=='1',1,;
      0)
  endfunc

  func oAIEVEECC_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oEDITADICH_1_31 as StdCheck with uid="XMGDLNIPAU",rtseq=20,rtrep=.f.,left=496, top=250, caption="Modifica dichiarante",;
    ToolTipText = "Rende editabili i campi del dichiarante",;
    HelpContextID = 239966711,;
    cFormVar="w_EDITADICH", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEDITADICH_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oEDITADICH_1_31.GetRadio()
    this.Parent.oContained.w_EDITADICH = this.RadioValue()
    return .t.
  endfunc

  func oEDITADICH_1_31.SetRadio()
    this.Parent.oContained.w_EDITADICH=trim(this.Parent.oContained.w_EDITADICH)
    this.value = ;
      iif(this.Parent.oContained.w_EDITADICH=='S',1,;
      0)
  endfunc

  func oEDITADICH_1_31.mHide()
    with this.Parent.oContained
      return (.w_AIINOLTRO='S' OR NVL(.w_AZPERAZI,' ')<>'S')
    endwith
  endfunc

  add object oAICODFIS_1_33 as StdField with uid="OYTZSWNBGL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_AICODFIS", cQueryName = "AICODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice Fiscale Dichiarante",;
    HelpContextID = 203618727,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=208, Top=275, cSayPict='"!!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!!"', InputMask=replicate('X',16)

  func oAICODFIS_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and .w_EDITADICH='S')
    endwith
   endif
  endfunc

  func oAICODFIS_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(EMPTY(.w_AICODFIS),.t.,CHKCFP(.w_AICODFIS,"CF")))
    endwith
    return bRes
  endfunc

  add object oAIFLCODF_1_34 as StdCheck with uid="XGHZGEKKON",rtseq=22,rtrep=.f.,left=399, top=275, caption="Codice fiscale dichiarante non registrato",;
    ToolTipText = "Se attivo, � possibile procedere alla trasmissione telematica della dichiarazione anche in caso di codice fiscale del dichiarante non registrato in anagrafe tributaria",;
    HelpContextID = 214578764,;
    cFormVar="w_AIFLCODF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAIFLCODF_1_34.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIFLCODF_1_34.GetRadio()
    this.Parent.oContained.w_AIFLCODF = this.RadioValue()
    return .t.
  endfunc

  func oAIFLCODF_1_34.SetRadio()
    this.Parent.oContained.w_AIFLCODF=trim(this.Parent.oContained.w_AIFLCODF)
    this.value = ;
      iif(this.Parent.oContained.w_AIFLCODF=='1',1,;
      0)
  endfunc

  func oAIFLCODF_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and Not Empty(.w_AICODFIS))
    endwith
   endif
  endfunc

  func oAIFLCODF_1_34.mHide()
    with this.Parent.oContained
      return (.w_ANNO>=2010)
    endwith
  endfunc

  add object oAICODIVA_1_35 as StdField with uid="JVTALGTPXU",rtseq=23,rtrep=.f.,;
    cFormVar = "w_AICODIVA", cQueryName = "AICODIVA",;
    bObbl = .f. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice Fiscale Societ� Dichiarante",;
    HelpContextID = 115148359,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=264, Top=303, cSayPict="'99999999999'", cGetPict="'99999999999'", InputMask=replicate('X',11)

  func oAICODIVA_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and .w_EDITADICH='S')
    endwith
   endif
  endfunc

  func oAICODIVA_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(EMPTY(.w_AICODIVA),.t.,CHKCFP(.w_AICODIVA,"PI","","", .w_AZCODNAZ)))
    endwith
    return bRes
  endfunc


  add object oAICODCAR_1_36 as StdCombo with uid="XMRZKEIEDU",rtseq=24,rtrep=.f.,left=399,top=303,width=284,height=21;
    , ToolTipText = "Codice Carica";
    , HelpContextID = 14485080;
    , cFormVar="w_AICODCAR",RowSource=""+""+l_descri5+","+""+l_descri2+","+""+l_descri9+","+""+l_descri10+","+""+l_descri11+","+""+l_descri12+","+""+l_descri3+","+"", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAICODCAR_1_36.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'5',;
    iif(this.value =4,'6',;
    iif(this.value =5,'7',;
    iif(this.value =6,'8',;
    iif(this.value =7,'9',;
    iif(this.value =8,'00',;
    space(2))))))))))
  endfunc
  func oAICODCAR_1_36.GetRadio()
    this.Parent.oContained.w_AICODCAR = this.RadioValue()
    return .t.
  endfunc

  func oAICODCAR_1_36.SetRadio()
    this.Parent.oContained.w_AICODCAR=trim(this.Parent.oContained.w_AICODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_AICODCAR=='1',1,;
      iif(this.Parent.oContained.w_AICODCAR=='2',2,;
      iif(this.Parent.oContained.w_AICODCAR=='5',3,;
      iif(this.Parent.oContained.w_AICODCAR=='6',4,;
      iif(this.Parent.oContained.w_AICODCAR=='7',5,;
      iif(this.Parent.oContained.w_AICODCAR=='8',6,;
      iif(this.Parent.oContained.w_AICODCAR=='9',7,;
      iif(this.Parent.oContained.w_AICODCAR=='00',8,;
      0))))))))
  endfunc

  func oAICODCAR_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and .w_EDITADICH='S')
    endwith
   endif
  endfunc

  add object oAIMANCOR_1_37 as StdCheck with uid="ENQCKAFMQO",rtseq=25,rtrep=.f.,left=120, top=386, caption="Flag conferma",;
    ToolTipText = "Mancata corrispondenza dei dati da trasmettere",;
    HelpContextID = 244341160,;
    cFormVar="w_AIMANCOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAIMANCOR_1_37.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIMANCOR_1_37.GetRadio()
    this.Parent.oContained.w_AIMANCOR = this.RadioValue()
    return .t.
  endfunc

  func oAIMANCOR_1_37.SetRadio()
    this.Parent.oContained.w_AIMANCOR=trim(this.Parent.oContained.w_AIMANCOR)
    this.value = ;
      iif(this.Parent.oContained.w_AIMANCOR=='1',1,;
      0)
  endfunc

  func oAIMANCOR_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oCOGTIT_1_45 as StdField with uid="HHYPETRQFJ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_COGTIT", cQueryName = "COGTIT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Cognome titolare",;
    HelpContextID = 36850726,;
   bGlobalFont=.t.,;
    Height=21, Width=184, Left=5, Top=22, InputMask=replicate('X',25)

  func oCOGTIT_1_45.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc

  add object oNOMTIT_1_46 as StdField with uid="CGDZTRJWBA",rtseq=30,rtrep=.f.,;
    cFormVar = "w_NOMTIT", cQueryName = "NOMTIT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Nome titolare",;
    HelpContextID = 36875478,;
   bGlobalFont=.t.,;
    Height=21, Width=184, Left=192, Top=22, InputMask=replicate('X',25)

  func oNOMTIT_1_46.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc


  add object oObj_1_47 as cp_runprogram with uid="VLUPQYCMCX",left=249, top=427, width=243,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR2BCD('TIPF')",;
    cEvent = "w_AITIPFOR Changed",;
    nPag=1;
    , HelpContextID = 65376230

  add object oStr_1_17 as StdString with uid="YUSQZCSSTK",Visible=.t., Left=121, Top=72,;
    Alignment=1, Width=94, Height=15,;
    Caption="Anno di imposta:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="NEDYJYUGVP",Visible=.t., Left=134, Top=137,;
    Alignment=1, Width=72, Height=15,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="PMAPUHRRHC",Visible=.t., Left=5, Top=5,;
    Alignment=0, Width=398, Height=15,;
    Caption="Denominazione, ragione sociale ovvero cognome e nome"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_20 as StdString with uid="ELERXBUKNP",Visible=.t., Left=511, Top=5,;
    Alignment=0, Width=94, Height=15,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="NZEQUPJJMK",Visible=.t., Left=6, Top=75,;
    Alignment=0, Width=54, Height=15,;
    Caption="Sez. I"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="HYAKLYMUYU",Visible=.t., Left=6, Top=88,;
    Alignment=0, Width=113, Height=15,;
    Caption="DATI GENERALI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_26 as StdString with uid="QNZZRJPHPX",Visible=.t., Left=121, Top=111,;
    Alignment=0, Width=95, Height=15,;
    Caption="- Contribuente -"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_28 as StdString with uid="MGQUKYVRSZ",Visible=.t., Left=396, Top=137,;
    Alignment=1, Width=96, Height=15,;
    Caption="Codice attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="BJTHQXWZOF",Visible=.t., Left=121, Top=250,;
    Alignment=0, Width=340, Height=15,;
    Caption="- Dichiarante (compilare se diverso dal contribuente) -"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="WZPWBBFOGR",Visible=.t., Left=121, Top=369,;
    Alignment=1, Width=425, Height=15,;
    Caption="- Mancata corrispond. dei dati da trasmettere con quelli della dichiarazione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="PBZPKGZNDI",Visible=.t., Left=118, Top=174,;
    Alignment=1, Width=87, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="ISFFISILZU",Visible=.t., Left=287, Top=72,;
    Alignment=1, Width=97, Height=15,;
    Caption="Data invio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="UJGFOJFPAR",Visible=.t., Left=120, Top=276,;
    Alignment=1, Width=85, Height=15,;
    Caption="Codice Fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="KCULFJQOMV",Visible=.t., Left=9, Top=306,;
    Alignment=1, Width=250, Height=15,;
    Caption="Codice Fiscale Societ� Dichiarante:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="RUBJOEVBYA",Visible=.t., Left=358, Top=306,;
    Alignment=0, Width=39, Height=15,;
    Caption="Carica:"  ;
  , bGlobalFont=.t.

  add object oBox_1_22 as StdBox with uid="NNINIBIMDD",left=6, top=66, width=665,height=1

  add object oBox_1_25 as StdBox with uid="AGTVLHADQF",left=121, top=110, width=564,height=1

  add object oBox_1_39 as StdBox with uid="YBAISFLTRY",left=121, top=366, width=562,height=1
enddefine
define class tgsar2acdPag2 as StdContainer
  Width  = 751
  height = 415
  stdWidth  = 751
  stdheight = 415
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAITOTATT_2_9 as StdField with uid="FXKWZJLOAC",rtseq=31,rtrep=.f.,;
    cFormVar = "w_AITOTATT", cQueryName = "AITOTATT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni attive (al netto dell'IVA)",;
    HelpContextID = 266212954,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=461, Top=28, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITOTATT_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITA_NIM_2_12 as StdField with uid="EVQQKSMDPH",rtseq=32,rtrep=.f.,;
    cFormVar = "w_AITA_NIM", cQueryName = "AITA_NIM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "operazioni non imponibili",;
    HelpContextID = 41937325,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=610, Top=55, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITA_NIM_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITA_ESE_2_14 as StdField with uid="UEUKYODYCB",rtseq=33,rtrep=.f.,;
    cFormVar = "w_AITA_ESE", cQueryName = "AITA_ESE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "operazioni esenti",;
    HelpContextID = 75503179,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=610, Top=77, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITA_ESE_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITA_CIN_2_16 as StdField with uid="GSRCBTEZQZ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_AITA_CIN", cQueryName = "AITA_CIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "cessioni intracomunitarie di beni",;
    HelpContextID = 226486700,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=610, Top=99, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITA_CIN_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITA_CBS_2_17 as StdField with uid="QVURYKXHOM",rtseq=35,rtrep=.f.,;
    cFormVar = "w_AITA_CBS", cQueryName = "AITA_CBS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cessioni beni strumentali",;
    HelpContextID = 41948761,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=457, Top=123, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITA_CBS_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAITA_CBS_2_17.mHide()
    with this.Parent.oContained
      return (.w_ANNO<2010)
    endwith
  endfunc

  add object oAITOTPAS_2_28 as StdField with uid="ZJWEWPUSTU",rtseq=36,rtrep=.f.,;
    cFormVar = "w_AITOTPAS", cQueryName = "AITOTPAS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale operazioni passive (al netto dell'IVA)",;
    HelpContextID = 249435737,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=461, Top=170, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITOTPAS_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITP_NIM_2_31 as StdField with uid="DUSIPJRUKF",rtseq=37,rtrep=.f.,;
    cFormVar = "w_AITP_NIM", cQueryName = "AITP_NIM",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "acquisti non imponibili",;
    HelpContextID = 40954285,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=610, Top=197, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITP_NIM_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITP_ESE_2_33 as StdField with uid="RFNDDYZTRE",rtseq=38,rtrep=.f.,;
    cFormVar = "w_AITP_ESE", cQueryName = "AITP_ESE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "acquisti esenti",;
    HelpContextID = 76486219,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=610, Top=220, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITP_ESE_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITP_CIN_2_35 as StdField with uid="URVVIEBZZE",rtseq=39,rtrep=.f.,;
    cFormVar = "w_AITP_CIN", cQueryName = "AITP_CIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "acquisti intracomunitari di beni",;
    HelpContextID = 225503660,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=610, Top=243, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITP_CIN_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAITP_CBS_2_36 as StdField with uid="UFZPNHVEEQ",rtseq=40,rtrep=.f.,;
    cFormVar = "w_AITP_CBS", cQueryName = "AITP_CBS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Acquisti beni strumentali",;
    HelpContextID = 42931801,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=457, Top=270, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAITP_CBS_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAITP_CBS_2_36.mHide()
    with this.Parent.oContained
      return (.w_ANNO<2010)
    endwith
  endfunc

  add object oAIIMPORO_2_38 as StdField with uid="ZCEISVAEWM",rtseq=41,rtrep=.f.,;
    cFormVar = "w_AIIMPORO", cQueryName = "AIIMPORO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importazione oro e argento",;
    HelpContextID = 228288085,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=358, Top=345, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAIIMPORO_2_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIIVAORO_2_39 as StdField with uid="UTOPEPFQCK",rtseq=42,rtrep=.f.,;
    cFormVar = "w_AIIVAORO", cQueryName = "AIIVAORO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imposta importazione oro e argento",;
    HelpContextID = 213149269,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=610, Top=345, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAIIVAORO_2_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIIMPROT_2_40 as StdField with uid="LWBUPSMJPO",rtseq=43,rtrep=.f.,;
    cFormVar = "w_AIIMPROT", cQueryName = "AIIMPROT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importazione rottami e altri materiali di recupero",;
    HelpContextID = 258251174,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=358, Top=392, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAIIMPROT_2_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIIVAROT_2_41 as StdField with uid="CVIPGKLPMA",rtseq=44,rtrep=.f.,;
    cFormVar = "w_AIIVAROT", cQueryName = "AIIVAROT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imposta importazione rottami e altri materiali di recupero",;
    HelpContextID = 4954534,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=610, Top=392, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAIIVAROT_2_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc


  add object oBtn_2_42 as StdButton with uid="MISXTYVFNG",left=10, top=369, width=48,height=45,;
    CpPicture="BMP\CALCOLA.BMP", caption="", nPag=2;
    , ToolTipText = "Conferma";
    , HelpContextID = 212053542;
    , Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_42.Click()
      with this.Parent.oContained
        GSAR2BCD(this.Parent.oContained,"CALC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_42.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_AIANNIMP) and not empty(.w_NOMEFILE) and .w_AIINOLTRO<>'S')
      endwith
    endif
  endfunc

  add object oStr_2_2 as StdString with uid="TXSCNQZRPO",Visible=.t., Left=7, Top=9,;
    Alignment=0, Width=57, Height=15,;
    Caption="Sez. II"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_3 as StdString with uid="AAXPWXMPHW",Visible=.t., Left=7, Top=26,;
    Alignment=0, Width=114, Height=15,;
    Caption="DATI RELATIVI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_4 as StdString with uid="IADGATZQAJ",Visible=.t., Left=170, Top=9,;
    Alignment=0, Width=152, Height=15,;
    Caption="- OPERAZIONI ATTIVE -"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_5 as StdString with uid="TLZBPAXWTX",Visible=.t., Left=122, Top=28,;
    Alignment=0, Width=29, Height=15,;
    Caption="CD1"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_6 as StdString with uid="VBWOMBQDID",Visible=.t., Left=7, Top=41,;
    Alignment=0, Width=135, Height=15,;
    Caption="ALLE OPERAZIONI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="NHBLHRHQKX",Visible=.t., Left=7, Top=55,;
    Alignment=0, Width=77, Height=15,;
    Caption="EFFETTUATE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_8 as StdString with uid="DUDRPXQVWR",Visible=.t., Left=157, Top=28,;
    Alignment=1, Width=198, Height=15,;
    Caption="Totale operazioni attive:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="WCQAYFMUMV",Visible=.t., Left=434, Top=55,;
    Alignment=1, Width=173, Height=15,;
    Caption="operazioni non imponibili:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="YMBGPTMEUX",Visible=.t., Left=434, Top=77,;
    Alignment=1, Width=173, Height=15,;
    Caption="operazioni esenti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="JYQFNGVJJN",Visible=.t., Left=393, Top=99,;
    Alignment=1, Width=214, Height=15,;
    Caption="cessioni intracomunitarie di beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="YZGADIHYKQ",Visible=.t., Left=343, Top=55,;
    Alignment=1, Width=50, Height=15,;
    Caption="di cui:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="PFTFXPHCJU",Visible=.t., Left=170, Top=152,;
    Alignment=0, Width=465, Height=15,;
    Caption="- OPERAZIONI PASSIVE -"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_21 as StdString with uid="KTZXHWLCTE",Visible=.t., Left=122, Top=321,;
    Alignment=0, Width=29, Height=15,;
    Caption="CD3"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_22 as StdString with uid="TOEXMFMTNP",Visible=.t., Left=227, Top=345,;
    Alignment=1, Width=128, Height=15,;
    Caption="Imponibile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="FFOFAKHHTH",Visible=.t., Left=170, Top=302,;
    Alignment=0, Width=465, Height=18,;
    Caption="IMPORTAZIONE SENZA PAGAMENTO DELL'IVA IN DOGANA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_25 as StdString with uid="MCYLSKEZOU",Visible=.t., Left=550, Top=345,;
    Alignment=1, Width=57, Height=15,;
    Caption="Imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="LPAYTKJWJM",Visible=.t., Left=122, Top=170,;
    Alignment=0, Width=29, Height=15,;
    Caption="CD2"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_27 as StdString with uid="THATHJKVPC",Visible=.t., Left=157, Top=170,;
    Alignment=1, Width=198, Height=15,;
    Caption="Totale operazioni passive:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="WDVWPKYLSW",Visible=.t., Left=434, Top=197,;
    Alignment=1, Width=173, Height=15,;
    Caption="acquisti non imponibili:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="VQKCUVQPOW",Visible=.t., Left=434, Top=221,;
    Alignment=1, Width=173, Height=15,;
    Caption="acquisti esenti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="TGWPQCBTHJ",Visible=.t., Left=396, Top=245,;
    Alignment=1, Width=211, Height=15,;
    Caption="acquisti intracomunitari di beni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="ZSJMPJBSUC",Visible=.t., Left=343, Top=197,;
    Alignment=1, Width=50, Height=15,;
    Caption="di cui:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="TKCESVFDLX",Visible=.t., Left=131, Top=371,;
    Alignment=1, Width=224, Height=18,;
    Caption="Rottami e altri materiali di recupero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="MMLHDBUJQI",Visible=.t., Left=227, Top=396,;
    Alignment=1, Width=128, Height=15,;
    Caption="Imponibile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_45 as StdString with uid="BUVIHHHXBL",Visible=.t., Left=550, Top=396,;
    Alignment=1, Width=57, Height=15,;
    Caption="Imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="HJIVTSGKDH",Visible=.t., Left=252, Top=127,;
    Alignment=1, Width=203, Height=18,;
    Caption="di cui cessioni beni strumentali:"  ;
  , bGlobalFont=.t.

  func oStr_2_46.mHide()
    with this.Parent.oContained
      return (.w_ANNO<2010)
    endwith
  endfunc

  add object oStr_2_47 as StdString with uid="WRWKNTNEVS",Visible=.t., Left=252, Top=273,;
    Alignment=1, Width=203, Height=18,;
    Caption="di cui acquisti beni strumentali:"  ;
  , bGlobalFont=.t.

  func oStr_2_47.mHide()
    with this.Parent.oContained
      return (.w_ANNO<2010)
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="OUUHMITNBV",Visible=.t., Left=156, Top=321,;
    Alignment=1, Width=199, Height=18,;
    Caption="Oro industriale e argento puro:"  ;
  , bGlobalFont=.t.

  add object oBox_2_1 as StdBox with uid="VBPHDMPQWM",left=7, top=6, width=686,height=1

  add object oBox_2_10 as StdBox with uid="YJUYHQOJVK",left=360, top=51, width=388,height=1

  add object oBox_2_19 as StdBox with uid="OGGQMNZBBB",left=170, top=147, width=579,height=1

  add object oBox_2_23 as StdBox with uid="AYYPAJTVHB",left=170, top=300, width=580,height=1

  add object oBox_2_29 as StdBox with uid="SGQXXLXJYC",left=360, top=193, width=389,height=1
enddefine
define class tgsar2acdPag3 as StdContainer
  Width  = 751
  height = 415
  stdWidth  = 751
  stdheight = 415
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAIIVAESI_3_8 as StdField with uid="YSQHCNNRFG",rtseq=45,rtrep=.f.,;
    cFormVar = "w_AIIVAESI", cQueryName = "AIIVAESI",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA esigibile",;
    HelpContextID = 45377103,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=310, Top=13, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAIIVAESI_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIIVADET_3_12 as StdField with uid="VLWRQVFKQB",rtseq=46,rtrep=.f.,;
    cFormVar = "w_AIIVADET", cQueryName = "AIIVADET",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA detratta",;
    HelpContextID = 28599898,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=533, Top=51, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"'

  func oAIIVADET_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIIVADOV_3_16 as StdField with uid="CIUONFALZD",rtseq=47,rtrep=.f.,;
    cFormVar = "w_AIIVADOV", cQueryName = "AIIVADOV",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA dovuta",;
    HelpContextID = 239835556,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=311, Top=100, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"', tabstop=.f.

  add object oAIIVACRE_3_18 as StdField with uid="UCFFQVBDBC",rtseq=48,rtrep=.f.,;
    cFormVar = "w_AIIVACRE", cQueryName = "AIIVACRE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA a credito",;
    HelpContextID = 11822667,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=533, Top=100, cSayPict='"99,999,999,999.00"', cGetPict='"99,999,999,999.00"', tabstop=.f.

  add object oAIFIRMAC_3_19 as StdCheck with uid="EBXIVLKYQK",rtseq=49,rtrep=.f.,left=168, top=153, caption="Firma",;
    ToolTipText = "Firma comunicazione presente se il flag � valorizzato",;
    HelpContextID = 196556361,;
    cFormVar="w_AIFIRMAC", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oAIFIRMAC_3_19.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIFIRMAC_3_19.GetRadio()
    this.Parent.oContained.w_AIFIRMAC = this.RadioValue()
    return .t.
  endfunc

  func oAIFIRMAC_3_19.SetRadio()
    this.Parent.oContained.w_AIFIRMAC=trim(this.Parent.oContained.w_AIFIRMAC)
    this.value = ;
      iif(this.Parent.oContained.w_AIFIRMAC=='1',1,;
      0)
  endfunc

  func oAIFIRMAC_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAICOFINT_3_21 as StdField with uid="CNXKMRFHEG",rtseq=50,rtrep=.f.,;
    cFormVar = "w_AICOFINT", cQueryName = "AICOFINT",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'intermediario",;
    HelpContextID = 151189926,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=355, Top=219, cSayPict='"!!!!!!!!!!!!!!!!"', cGetPict='"!!!!!!!!!!!!!!!!"', InputMask=replicate('X',16)

  func oAICOFINT_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' AND .w_AITIPFOR<>'01' AND .w_AITIPFOR<>'02')
    endwith
   endif
  endfunc

  func oAICOFINT_3_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_AICOFINT,iif( .w_AITIPFOR='10',"CF"," ")))
    endwith
    return bRes
  endfunc

  add object oAIALBCAF_3_25 as StdField with uid="EVJZTMALPD",rtseq=51,rtrep=.f.,;
    cFormVar = "w_AIALBCAF", cQueryName = "AIALBCAF",nZero=5,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Numero iscrizione all'albo dei C.A.F.",;
    HelpContextID = 12183116,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=629, Top=219, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oAIALBCAF_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and !empty(.w_AICOFINT))
    endwith
   endif
  endfunc

  add object oAIINVCON_3_30 as StdCheck with uid="PWNQJWDNCW",rtseq=52,rtrep=.f.,left=519, top=263, caption="",;
    ToolTipText = "Impegno a presentare la comun. predisposta dal contribuente",;
    HelpContextID = 235116972,;
    cFormVar="w_AIINVCON", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oAIINVCON_3_30.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIINVCON_3_30.GetRadio()
    this.Parent.oContained.w_AIINVCON = this.RadioValue()
    return .t.
  endfunc

  func oAIINVCON_3_30.SetRadio()
    this.Parent.oContained.w_AIINVCON=trim(this.Parent.oContained.w_AIINVCON)
    this.value = ;
      iif(this.Parent.oContained.w_AIINVCON=='1',1,;
      0)
  endfunc

  func oAIINVCON_3_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and !empty(.w_AICOFINT))
    endwith
   endif
  endfunc

  add object oAIINVSOG_3_33 as StdCheck with uid="CUENWICIZG",rtseq=53,rtrep=.f.,left=519, top=308, caption="",;
    ToolTipText = "Impegno a presentare la comun. del contribuente predisposta dal soggetto",;
    HelpContextID = 235116979,;
    cFormVar="w_AIINVSOG", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oAIINVSOG_3_33.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIINVSOG_3_33.GetRadio()
    this.Parent.oContained.w_AIINVSOG = this.RadioValue()
    return .t.
  endfunc

  func oAIINVSOG_3_33.SetRadio()
    this.Parent.oContained.w_AIINVSOG=trim(this.Parent.oContained.w_AIINVSOG)
    this.value = ;
      iif(this.Parent.oContained.w_AIINVSOG=='1',1,;
      0)
  endfunc

  func oAIINVSOG_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIINOLTRO<>'S' and !empty(.w_AICOFINT))
    endwith
   endif
  endfunc

  add object oAIDATIMP_3_36 as StdField with uid="ZRWZBFZIHN",rtseq=54,rtrep=.f.,;
    cFormVar = "w_AIDATIMP", cQueryName = "AIDATIMP",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'impegno",;
    HelpContextID = 137423274,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=275, Top=364

  func oAIDATIMP_3_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oAIFIRMA_3_40 as StdCheck with uid="PDPPGRMLBD",rtseq=55,rtrep=.f.,left=420, top=363, caption="Firma",;
    ToolTipText = "Firma intermediario presente se il flag � valorizzato",;
    HelpContextID = 196556294,;
    cFormVar="w_AIFIRMA", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oAIFIRMA_3_40.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oAIFIRMA_3_40.GetRadio()
    this.Parent.oContained.w_AIFIRMA = this.RadioValue()
    return .t.
  endfunc

  func oAIFIRMA_3_40.SetRadio()
    this.Parent.oContained.w_AIFIRMA=trim(this.Parent.oContained.w_AIFIRMA)
    this.value = ;
      iif(this.Parent.oContained.w_AIFIRMA=='1',1,;
      0)
  endfunc

  func oAIFIRMA_3_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  add object oStr_3_2 as StdString with uid="RMJZWJTQOV",Visible=.t., Left=7, Top=9,;
    Alignment=0, Width=60, Height=15,;
    Caption="Sez. III"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_3 as StdString with uid="EBDAYAJJFI",Visible=.t., Left=7, Top=22,;
    Alignment=0, Width=100, Height=15,;
    Caption="DETERMINAZIONE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_4 as StdString with uid="OQACNORRGQ",Visible=.t., Left=168, Top=12,;
    Alignment=0, Width=29, Height=15,;
    Caption="CD4"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_5 as StdString with uid="BMIIHVZGBP",Visible=.t., Left=7, Top=37,;
    Alignment=0, Width=153, Height=15,;
    Caption="DELL'IVA DOVUTA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_6 as StdString with uid="FDDPEDHAOR",Visible=.t., Left=7, Top=51,;
    Alignment=0, Width=91, Height=15,;
    Caption="O A CREDITO"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_7 as StdString with uid="WRYJUBGGWS",Visible=.t., Left=229, Top=13,;
    Alignment=1, Width=76, Height=15,;
    Caption="IVA esigibile:"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="SMYQITFPKR",Visible=.t., Left=168, Top=50,;
    Alignment=0, Width=29, Height=15,;
    Caption="CD5"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_11 as StdString with uid="IVTORGEFKB",Visible=.t., Left=200, Top=51,;
    Alignment=1, Width=105, Height=15,;
    Caption="IVA detratta:"  ;
  , bGlobalFont=.t.

  add object oStr_3_14 as StdString with uid="SNFDTCEZAK",Visible=.t., Left=169, Top=100,;
    Alignment=0, Width=29, Height=15,;
    Caption="CD6"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_15 as StdString with uid="CXEYBTUDBS",Visible=.t., Left=203, Top=100,;
    Alignment=1, Width=103, Height=15,;
    Caption="IVA dovuta:"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="XSVEBBPESF",Visible=.t., Left=444, Top=100,;
    Alignment=1, Width=85, Height=15,;
    Caption="o a credito:"  ;
  , bGlobalFont=.t.

  add object oStr_3_20 as StdString with uid="EDYHSBFJMD",Visible=.t., Left=168, Top=219,;
    Alignment=1, Width=183, Height=15,;
    Caption="Codice fiscale dell'intermediario:"  ;
  , bGlobalFont=.t.

  add object oStr_3_23 as StdString with uid="ODJPTQPLQI",Visible=.t., Left=7, Top=215,;
    Alignment=0, Width=114, Height=15,;
    Caption="IMPEGNO ALLA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_24 as StdString with uid="TTJFHXMJXO",Visible=.t., Left=504, Top=219,;
    Alignment=1, Width=121, Height=15,;
    Caption="Num. iscr. C.A.F.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_26 as StdString with uid="LXOQKWZARZ",Visible=.t., Left=7, Top=230,;
    Alignment=0, Width=95, Height=15,;
    Caption="PRESENTAZIONE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_27 as StdString with uid="SYFBNJLGZH",Visible=.t., Left=7, Top=245,;
    Alignment=0, Width=91, Height=15,;
    Caption="TELEMATICA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_28 as StdString with uid="BLSZTJGKCM",Visible=.t., Left=189, Top=263,;
    Alignment=1, Width=313, Height=15,;
    Caption="predisposta dal contribuente:"  ;
  , bGlobalFont=.t.

  add object oStr_3_31 as StdString with uid="TAUYYJEDXQ",Visible=.t., Left=169, Top=293,;
    Alignment=0, Width=316, Height=15,;
    Caption="Impegno a presentare in via telematica la comunicazione"  ;
  , bGlobalFont=.t.

  add object oStr_3_34 as StdString with uid="REKYKFYKFY",Visible=.t., Left=180, Top=308,;
    Alignment=1, Width=322, Height=15,;
    Caption="del contribuente predisposta dal soggetto che la trasmette:"  ;
  , bGlobalFont=.t.

  add object oStr_3_35 as StdString with uid="UEIMGJSZCI",Visible=.t., Left=170, Top=247,;
    Alignment=0, Width=316, Height=15,;
    Caption="Impegno a presentare in via telematica la comunicazione"  ;
  , bGlobalFont=.t.

  add object oStr_3_37 as StdString with uid="FGGRWLAFDY",Visible=.t., Left=135, Top=364,;
    Alignment=1, Width=137, Height=15,;
    Caption="Data dell'impegno:"  ;
  , bGlobalFont=.t.

  add object oStr_3_39 as StdString with uid="BURNFGFAQT",Visible=.t., Left=8, Top=264,;
    Alignment=0, Width=71, Height=15,;
    Caption="(Rec. tipo B)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_42 as StdString with uid="OHWMHBZSCY",Visible=.t., Left=7, Top=149,;
    Alignment=0, Width=114, Height=15,;
    Caption="FIRMA DELLA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_43 as StdString with uid="BLKSXVVDQW",Visible=.t., Left=7, Top=167,;
    Alignment=0, Width=101, Height=15,;
    Caption="COMUNICAZIONE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_1 as StdBox with uid="CUUCPPGSJO",left=7, top=6, width=685,height=1

  add object oBox_3_9 as StdBox with uid="DVFJPANOYV",left=168, top=39, width=514,height=1

  add object oBox_3_13 as StdBox with uid="AMDVBONWYM",left=168, top=85, width=514,height=1

  add object oBox_3_22 as StdBox with uid="UCXOJHREQX",left=7, top=142, width=623,height=1

  add object oBox_3_29 as StdBox with uid="KTKRBGSORJ",left=168, top=243, width=514,height=1

  add object oBox_3_32 as StdBox with uid="XDFEQFIDGW",left=167, top=285, width=514,height=1

  add object oBox_3_38 as StdBox with uid="ERBOLHGCXV",left=165, top=355, width=514,height=1

  add object oBox_3_41 as StdBox with uid="QFSRKXIJKT",left=7, top=205, width=623,height=1
enddefine
define class tgsar2acdPag4 as StdContainer
  Width  = 751
  height = 415
  stdWidth  = 751
  stdheight = 415
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oAIPERAZI_4_1 as StdCombo with uid="ROSXVJMFVI",rtseq=56,rtrep=.f.,left=7,top=9,width=136,height=21;
    , ToolTipText = "Test se persona fisica";
    , HelpContextID = 4991409;
    , cFormVar="w_AIPERAZI",RowSource=""+""+L_comb1+","+""+L_comb2+"", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAIPERAZI_4_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oAIPERAZI_4_1.GetRadio()
    this.Parent.oContained.w_AIPERAZI = this.RadioValue()
    return .t.
  endfunc

  func oAIPERAZI_4_1.SetRadio()
    this.Parent.oContained.w_AIPERAZI=trim(this.Parent.oContained.w_AIPERAZI)
    this.value = ;
      iif(this.Parent.oContained.w_AIPERAZI=='S',1,;
      iif(this.Parent.oContained.w_AIPERAZI=='N',2,;
      0))
  endfunc

  func oAIPERAZI_4_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc


  add object oObj_4_2 as cp_runprogram with uid="KASFICCGHH",left=-4, top=427, width=243,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR2BCD('PERF')",;
    cEvent = "w_AIPERAZI Changed",;
    nPag=4;
    , HelpContextID = 65376230


  add object oBtn_4_6 as StdButton with uid="TQEMIMWBDV",left=482, top=372, width=21,height=20,;
    caption="...", nPag=4;
    , ToolTipText = "Seleziona la cartella dove si vuole salvare il file trasmiva.";
    , HelpContextID = 112420394;
  , bGlobalFont=.t.

    proc oBtn_4_6.Click()
      with this.Parent.oContained
        GSAR2BCD(this.Parent.oContained,"FILE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_8 as StdButton with uid="AYLATODQCL",left=647, top=369, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=4;
    , ToolTipText = "Conferma";
    , HelpContextID = 30790246;
    , Caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_8.Click()
      with this.Parent.oContained
        GSAR2BCD(this.Parent.oContained,"GENE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_AIANNIMP) and not empty(.w_NOMEFILE) AND Isinoltel())
      endwith
    endif
  endfunc


  add object oBtn_4_9 as StdButton with uid="JPJRCCQWDZ",left=700, top=369, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per lanciare la stampa";
    , HelpContextID = 29168166;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_9.Click()
      with this.Parent.oContained
        GSAR2BCD(this.Parent.oContained,"STAM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_AIANNIMP))
      endwith
    endif
  endfunc

  add object oNOMEFILE_4_10 as StdField with uid="OKVJAIJGZX",rtseq=58,rtrep=.f.,;
    cFormVar = "w_NOMEFILE", cQueryName = "NOMEFILE",;
    bObbl = .f. , nPag = 4, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Nome del percorso e del file per la creazione memorizzazione dati dell'IVA",;
    HelpContextID = 151802597,;
   bGlobalFont=.t.,;
    Height=21, Width=310, Left=169, Top=371, InputMask=replicate('X',200)

  add object oAIDENOMI_4_12 as StdField with uid="UJKRUDNGLA",rtseq=59,rtrep=.f.,;
    cFormVar = "w_AIDENOMI", cQueryName = "AIDENOMI",;
    bObbl = .f. , nPag = 4, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione del fornitore",;
    HelpContextID = 42789297,;
   bGlobalFont=.t.,;
    Height=21, Width=436, Left=172, Top=45, InputMask=replicate('X',60)

  func oAIDENOMI_4_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S'and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAIDENOMI_4_12.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oAICOGNOM_4_13 as StdField with uid="JDNRKIZSLG",rtseq=60,rtrep=.f.,;
    cFormVar = "w_AICOGNOM", cQueryName = "AICOGNOM",;
    bObbl = .f. , nPag = 4, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del fornitore",;
    HelpContextID = 66255277,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=172, Top=45, InputMask=replicate('X',24)

  func oAICOGNOM_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAICOGNOM_4_13.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAINOME_4_14 as StdField with uid="QTVLSFBHQY",rtseq=61,rtrep=.f.,;
    cFormVar = "w_AINOME", cQueryName = "AINOME",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del fornitore",;
    HelpContextID = 210913786,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=398, Top=45, InputMask=replicate('X',20)

  func oAINOME_4_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAINOME_4_14.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc


  add object oAISESSO_4_15 as StdCombo with uid="XZOVQUMAWB",rtseq=62,rtrep=.f.,left=601,top=45,width=92,height=21;
    , ToolTipText = "Sesso del fornitore";
    , HelpContextID = 238811642;
    , cFormVar="w_AISESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAISESSO_4_15.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oAISESSO_4_15.GetRadio()
    this.Parent.oContained.w_AISESSO = this.RadioValue()
    return .t.
  endfunc

  func oAISESSO_4_15.SetRadio()
    this.Parent.oContained.w_AISESSO=trim(this.Parent.oContained.w_AISESSO)
    this.value = ;
      iif(this.Parent.oContained.w_AISESSO=='M',1,;
      iif(this.Parent.oContained.w_AISESSO=='F',2,;
      0))
  endfunc

  func oAISESSO_4_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISESSO_4_15.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAISE_CAP_4_17 as StdField with uid="EAHXVXRRTY",rtseq=63,rtrep=.f.,;
    cFormVar = "w_AISE_CAP", cQueryName = "AISE_CAP",;
    bObbl = .f. , nPag = 4, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della sede legale del fornitore",;
    HelpContextID = 42206806,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=171, Top=85, InputMask=replicate('X',9), bHasZoom = .t. 

  func oAISE_CAP_4_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISE_CAP_4_17.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  proc oAISE_CAP_4_17.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AISE_CAP",".w_AISECOMU",".w_AISESIGL")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAISECOMU_4_18 as StdField with uid="BKBJRKVTWJ",rtseq=64,rtrep=.f.,;
    cFormVar = "w_AISECOMU", cQueryName = "AISECOMU",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune della sede legale del fornitore",;
    HelpContextID = 54262181,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=171, Top=120, InputMask=replicate('X',40)

  func oAISECOMU_4_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISECOMU_4_18.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oAICOMUNE_4_19 as StdField with uid="XDHWWDJPBR",rtseq=65,rtrep=.f.,;
    cFormVar = "w_AICOMUNE", cQueryName = "AICOMUNE",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (oppure stato estero) di nascita del fornitore",;
    HelpContextID = 210958773,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=171, Top=85, InputMask=replicate('X',40)

  func oAICOMUNE_4_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAICOMUNE_4_19.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAISESIGL_4_20 as StdField with uid="VSOLXNAMDS",rtseq=66,rtrep=.f.,;
    cFormVar = "w_AISESIGL", cQueryName = "AISESIGL",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia della sede legale del fornitore",;
    HelpContextID = 130287186,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=395, Top=85, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oAISESIGL_4_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISESIGL_4_20.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  proc oAISESIGL_4_20.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_AISESIGL")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAISEIND2_4_21 as StdField with uid="MDTNMVBSXW",rtseq=67,rtrep=.f.,;
    cFormVar = "w_AISEIND2", cQueryName = "AISEIND2",;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede legale del fornitore",;
    HelpContextID = 203687480,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=171, Top=157, InputMask=replicate('X',35)

  func oAISEIND2_4_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISEIND2_4_21.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oAIDATNAS_4_22 as StdField with uid="YYRQQYMXRH",rtseq=68,rtrep=.f.,;
    cFormVar = "w_AIDATNAS", cQueryName = "AIDATNAS",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del fornitore",;
    HelpContextID = 214898265,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=171, Top=121

  func oAIDATNAS_4_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAIDATNAS_4_22.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAISIGLA_4_23 as StdField with uid="LCTQLXNIKY",rtseq=69,rtrep=.f.,;
    cFormVar = "w_AISIGLA", cQueryName = "AISIGLA",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del fornitore",;
    HelpContextID = 168297990,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=437, Top=119, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oAISIGLA_4_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISIGLA_4_23.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAIINDIRI_4_24 as StdField with uid="QDNHPLRXOY",rtseq=70,rtrep=.f.,;
    cFormVar = "w_AIINDIRI", cQueryName = "AIINDIRI",;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 115107407,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=169, Top=226, InputMask=replicate('X',35)

  func oAIINDIRI_4_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAIINDIRI_4_24.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAI___CAP_4_25 as StdField with uid="UKNJUVWKDY",rtseq=71,rtrep=.f.,;
    cFormVar = "w_AI___CAP", cQueryName = "AI___CAP",;
    bObbl = .f. , nPag = 4, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    HelpContextID = 43959894,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=169, Top=157, InputMask=replicate('X',9), bHasZoom = .t. 

  func oAI___CAP_4_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAI___CAP_4_25.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  proc oAI___CAP_4_25.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AI___CAP",".w_AIRESCOM",".w_AIRESSIG")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAIRESSIG_4_26 as StdField with uid="UDWAUBRUGW",rtseq=72,rtrep=.f.,;
    cFormVar = "w_AIRESSIG", cQueryName = "AIRESSIG",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 238815667,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=584, Top=157, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oAIRESSIG_4_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAIRESSIG_4_26.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  proc oAIRESSIG_4_26.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_AIRESSIG")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAISERCAP_4_27 as StdField with uid="LEBJRKCUXZ",rtseq=73,rtrep=.f.,;
    cFormVar = "w_AISERCAP", cQueryName = "AISERCAP",;
    bObbl = .f. , nPag = 4, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. del domicilio fiscale del fornitore",;
    HelpContextID = 28575318,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=169, Top=193, InputMask=replicate('X',9), bHasZoom = .t. 

  func oAISERCAP_4_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISERCAP_4_27.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  proc oAISERCAP_4_27.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AISERCAP",".w_AISERCOM",".w_AISERSIG")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAISERSIG_4_28 as StdField with uid="UVTZWHBNBI",rtseq=74,rtrep=.f.,;
    cFormVar = "w_AISERSIG", cQueryName = "AISERSIG",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di domicilio fiscale del fornitore:",;
    HelpContextID = 239860147,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=395, Top=193, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oAISERSIG_4_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISERSIG_4_28.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  proc oAISERSIG_4_28.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_AISERSIG")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAIRESCOM_4_29 as StdField with uid="AZCCQBHYHJ",rtseq=75,rtrep=.f.,;
    cFormVar = "w_AIRESCOM", cQueryName = "AIRESCOM",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del fornitore",;
    HelpContextID = 238815661,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=325, Top=193, InputMask=replicate('X',40)

  func oAIRESCOM_4_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI='S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAIRESCOM_4_29.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oAISERCOM_4_30 as StdField with uid="JMDZCBXAPQ",rtseq=76,rtrep=.f.,;
    cFormVar = "w_AISERCOM", cQueryName = "AISERCOM",;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di domicilio fiscale del fornitore",;
    HelpContextID = 239860141,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=169, Top=226, InputMask=replicate('X',40)

  func oAISERCOM_4_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISERCOM_4_30.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oAISERIND_4_31 as StdField with uid="ILQRWWIPUH",rtseq=77,rtrep=.f.,;
    cFormVar = "w_AISERIND", cQueryName = "AISERIND",;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di domicilio fiscale del fornitore",;
    HelpContextID = 139196854,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=169, Top=259, InputMask=replicate('X',35)

  func oAISERIND_4_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S' and !empty(.w_AICOFINT) and .w_AIINOLTRO<>'S')
    endwith
   endif
  endfunc

  func oAISERIND_4_31.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_7 as StdString with uid="SORSEKCKVG",Visible=.t., Left=97, Top=373,;
    Alignment=1, Width=69, Height=15,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_4_11 as StdString with uid="NFCEYUFYCL",Visible=.t., Left=110, Top=48,;
    Alignment=1, Width=58, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_4_11.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_16 as StdString with uid="RCOARSDNRS",Visible=.t., Left=558, Top=48,;
    Alignment=1, Width=39, Height=15,;
    Caption="Sesso:"  ;
  , bGlobalFont=.t.

  func oStr_4_16.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_32 as StdString with uid="QZIQTOUJSH",Visible=.t., Left=357, Top=48,;
    Alignment=1, Width=37, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_4_32.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_33 as StdString with uid="YSDSMJPCUB",Visible=.t., Left=266, Top=120,;
    Alignment=1, Width=170, Height=15,;
    Caption="Sigla provincia di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_4_33.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_34 as StdString with uid="GKJCARIUFY",Visible=.t., Left=26, Top=87,;
    Alignment=1, Width=142, Height=15,;
    Caption="Comune di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_4_34.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_35 as StdString with uid="PHEDRNQNJC",Visible=.t., Left=84, Top=194,;
    Alignment=1, Width=238, Height=15,;
    Caption="Comune di residenza o domicilio fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_4_35.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_36 as StdString with uid="XILHRWUEWW",Visible=.t., Left=109, Top=229,;
    Alignment=1, Width=57, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  func oStr_4_36.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_37 as StdString with uid="DUKZPDUBKV",Visible=.t., Left=406, Top=159,;
    Alignment=1, Width=176, Height=15,;
    Caption="Sigla provincia di residenza:"  ;
  , bGlobalFont=.t.

  func oStr_4_37.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_38 as StdString with uid="HDAULOJTTR",Visible=.t., Left=127, Top=159,;
    Alignment=1, Width=39, Height=15,;
    Caption="C.A.P.:"  ;
  , bGlobalFont=.t.

  func oStr_4_38.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_39 as StdString with uid="EAAJCSCSZQ",Visible=.t., Left=72, Top=122,;
    Alignment=1, Width=96, Height=15,;
    Caption="Data di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_4_39.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI<>'S')
    endwith
  endfunc

  add object oStr_4_40 as StdString with uid="HBBEQDXSVV",Visible=.t., Left=78, Top=48,;
    Alignment=1, Width=90, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  func oStr_4_40.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_41 as StdString with uid="KORLGBTOUD",Visible=.t., Left=13, Top=123,;
    Alignment=1, Width=155, Height=15,;
    Caption="Comune della sede legale:"  ;
  , bGlobalFont=.t.

  func oStr_4_41.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_42 as StdString with uid="HACQCYBOIB",Visible=.t., Left=280, Top=87,;
    Alignment=1, Width=113, Height=15,;
    Caption="Sigla provincia:"  ;
  , bGlobalFont=.t.

  func oStr_4_42.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_43 as StdString with uid="AMEXRKXOGO",Visible=.t., Left=20, Top=159,;
    Alignment=1, Width=148, Height=15,;
    Caption="Indirizzo della sede legale:"  ;
  , bGlobalFont=.t.

  func oStr_4_43.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_44 as StdString with uid="ODWVYESOUX",Visible=.t., Left=43, Top=87,;
    Alignment=1, Width=125, Height=15,;
    Caption="C.A.P. sede legale:"  ;
  , bGlobalFont=.t.

  func oStr_4_44.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_45 as StdString with uid="YTPGEBOZJU",Visible=.t., Left=3, Top=229,;
    Alignment=1, Width=163, Height=15,;
    Caption="Comune di domicilio fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_4_45.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_46 as StdString with uid="XEGQVMBKOF",Visible=.t., Left=280, Top=194,;
    Alignment=1, Width=113, Height=15,;
    Caption="Sigla provincia:"  ;
  , bGlobalFont=.t.

  func oStr_4_46.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_47 as StdString with uid="KZTYYYGGXI",Visible=.t., Left=7, Top=262,;
    Alignment=1, Width=159, Height=15,;
    Caption="Indirizzo di domicilio fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_4_47.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oStr_4_48 as StdString with uid="KJHEMCJKTM",Visible=.t., Left=19, Top=194,;
    Alignment=1, Width=147, Height=15,;
    Caption="C.A.P. domic.fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_4_48.mHide()
    with this.Parent.oContained
      return (.w_AIPERAZI='S')
    endwith
  endfunc

  add object oBox_4_3 as StdBox with uid="NLQRMJDLUL",left=7, top=36, width=622,height=0

  add object oBox_4_4 as StdBox with uid="MVNLUETLMF",left=11, top=36, width=577,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar2acd','MOD_COAN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AIANNIMP=MOD_COAN.AIANNIMP";
  +" and "+i_cAliasName2+".AIDATINV=MOD_COAN.AIDATINV";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
