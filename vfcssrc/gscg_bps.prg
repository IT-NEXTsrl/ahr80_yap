* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bps                                                        *
*              Sincronizza piano dei conti di analitica e contabilit�          *
*                                                                              *
*      Author: ZUCCHETTI SPA: CS                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-14                                                      *
* Last revis.: 2016-11-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo,pCodice,pGest,pLog,pPadre
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bps",oParentObject,m.pTipo,m.pCodice,m.pGest,m.pLog,m.pPadre)
return(i_retval)

define class tgscg_bps as StdBatch
  * --- Local variables
  pTipo = space(2)
  pCodice = space(15)
  pGest = space(1)
  pLog = .NULL.
  pPadre = .NULL.
  w_AZCOACOG = space(1)
  w_CODAZI = space(5)
  w_MASRAG = 0
  w_ANTIPCON = space(1)
  w_MESS = space(100)
  w_ANCONSUP1 = space(15)
  w_CONTA = 0
  w_RIGHEANALITICA = 0
  w_AZFLGEAC = space(1)
  w_UNIMIS = space(3)
  w_CODIVA = space(5)
  w_ANCODICE = space(15)
  w_ANDESCRI = space(40)
  w_ANCONSUP = space(15)
  w_SEZBIL = space(1)
  w_ANCCTAGG = space(1)
  w_ANDTINVA = ctod("  /  /  ")
  w_ANDTOBSO = ctod("  /  /  ")
  w_MCCODICE = space(15)
  w_MCDESCRI = space(40)
  w_MCCONSUP = space(15)
  w_MCSEZBIL = space(1)
  w_MCNUMLIV = 0
  w_MCSEQSTA = 0
  w_MCFLGESE = space(1)
  w_SUFQTA = space(5)
  w_SUFVAL = space(5)
  w_TIPCOS = space(5)
  w_CODICE = space(5)
  w_C1CODICE = space(5)
  w_CODICEQ = space(20)
  w_CODICEV = space(20)
  w_VOCE = space(15)
  w_ARCODART = space(20)
  w_ARDESART = space(40)
  w_ARDESSUP = space(10)
  w_OMODKEY = space(10)
  w_ARTIPBAR = space(1)
  w_ARTIPART = space(2)
  w_ARDTINVA = ctod("  /  /  ")
  w_ARDTOBSO = ctod("  /  /  ")
  w_TIPBAR = space(1)
  w_CACONFIG = space(1)
  w_CACONCAR = space(1)
  w_CACMPCAR = space(1)
  w_NMODKEY = space(10)
  w_NDATKEY = space(10)
  w_ODATKEY = space(10)
  w_ARCODVAR = space(5)
  w_ARCENCOS = space(15)
  w_ARFLUMVA = space(1)
  w_ARPREZUM = space(1)
  w_ESISTE = .f.
  w_DESCRI = space(35)
  w_CONTOR = space(15)
  w_CONTOC = space(15)
  w_VOCEC = space(15)
  w_VOCER = space(15)
  w_OKANALITICA = .f.
  w_NOTEXISTMASTRO = .f.
  * --- WorkFile variables
  VOC_COST_idx=0
  MASTVOCI_idx=0
  COLLCENT_idx=0
  CONTI_idx=0
  MASTRI_idx=0
  AZIENDA_idx=0
  PAR_ARCO_idx=0
  CAANARTI_idx=0
  CACOARTI_idx=0
  ART_ICOL_idx=0
  CONTVEAC_idx=0
  CACOCLFO_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Consente la sincronizzazione fra piano dei conti di analitica e contabilit� generale.
    *     La sincronizzazione avviene se il flag AZCOACOG nei dati azienda � impostato ad "S"
    *     Il parametro pTipo � composto da due caratteri:
    *     il primo � C o M per conti o mastri
    *     il secondo � l'azione I = Insert, U = Update, D = Delete
    *     In fase di modifica il mastro non � editabile in modo da non dovere gestire cancellazioni o inserimenti.
    * --- Codice 
    *     Mastro se lanciato con parametro MU,MI,MD
    *     Conto  se lanciato con parametro CU,CI,CD
    * --- B se eseguito da batch 
    *     G se eseguito da gestione
    * --- Log errori
    * --- Oggetto padre
    this.w_CODAZI = i_CODAZI
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCOACOG,AZFLGEAC"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCOACOG,AZFLGEAC;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZCOACOG = NVL(cp_ToDate(_read_.AZCOACOG),cp_NullValue(_read_.AZCOACOG))
      this.w_AZFLGEAC = NVL(cp_ToDate(_read_.AZFLGEAC),cp_NullValue(_read_.AZFLGEAC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MASRAG = 0
    this.w_ANTIPCON = "G"
    if this.pTipo $ "CI-CD-CU"
      if (this.pTipo $ "CI" or this.pTipo $ "CU") and this.pGEST="G"
        this.w_ANCODICE = this.pCODICE
        this.w_ANTIPCON = this.pPADRE.w_ANTIPCON
        this.w_ANDESCRI = left(this.pPADRE.w_ANDESCRI,40)
        this.w_ANCONSUP = this.pPADRE.w_ANCONSUP
        this.w_ANDTINVA = this.pPADRE.w_ANDTINVA
        this.w_ANDTOBSO = this.pPADRE.w_ANDTOBSO
        this.w_SEZBIL = CALCSEZ(this.w_ANCONSUP)
        this.w_MCCODICE = this.w_ANCONSUP
        this.w_ANCCTAGG = this.pPADRE.w_ANCCTAGG
      else
        this.w_ANCODICE = this.pCODICE
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDESCRI,ANCONSUP,ANDTINVA,ANDTOBSO,ANCCTAGG"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDESCRI,ANCONSUP,ANDTINVA,ANDTOBSO,ANCCTAGG;
            from (i_cTable) where;
                ANTIPCON = this.w_ANTIPCON;
                and ANCODICE = this.w_ANCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANDESCRI = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
          this.w_ANCONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
          this.w_ANDTINVA = NVL(cp_ToDate(_read_.ANDTINVA),cp_NullValue(_read_.ANDTINVA))
          this.w_ANDTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
          this.w_ANCCTAGG = NVL(cp_ToDate(_read_.ANCCTAGG),cp_NullValue(_read_.ANCCTAGG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_ANDESCRI = left(this.w_ANDESCRI,40)
        this.w_SEZBIL = CALCSEZ(this.w_ANCONSUP)
        this.w_MCCODICE = this.w_ANCONSUP
      endif
      * --- Read from MASTRI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MASTRI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MCFLGESE"+;
          " from "+i_cTable+" MASTRI where ";
              +"MCCODICE = "+cp_ToStrODBC(this.w_MCCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MCFLGESE;
          from (i_cTable) where;
              MCCODICE = this.w_MCCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MCFLGESE = NVL(cp_ToDate(_read_.MCFLGESE),cp_NullValue(_read_.MCFLGESE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_SEZBIL = iif(this.w_SEZBIL="A" OR this.w_SEZBIL="P", "P", this.w_SEZBIL)
    else
      if (this.pTipo $ "MI" or this.pTipo $ "MU") and this.pGEST="G"
        this.w_MCCODICE = this.pPADRE.w_MCCODICE
        this.w_MCDESCRI = this.pPADRE.w_MCDESCRI
        this.w_MCCONSUP = this.pPADRE.w_MCCONSUP
        this.w_MCSEZBIL = this.pPADRE.w_MCSEZBIL
        this.w_MCNUMLIV = this.pPADRE.w_MCNUMLIV
        this.w_MCSEQSTA = this.pPADRE.w_MCSEQSTA
        this.w_MCFLGESE = this.pPADRE.w_MCFLGESE
      else
        this.w_MCCODICE = this.pCODICE
        * --- Read from MASTRI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MASTRI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MCDESCRI,MCCONSUP,MCSEZBIL,MCSEQSTA,MCFLGESE,MCNUMLIV"+;
            " from "+i_cTable+" MASTRI where ";
                +"MCCODICE = "+cp_ToStrODBC(this.w_MCCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MCDESCRI,MCCONSUP,MCSEZBIL,MCSEQSTA,MCFLGESE,MCNUMLIV;
            from (i_cTable) where;
                MCCODICE = this.w_MCCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MCDESCRI = NVL(cp_ToDate(_read_.MCDESCRI),cp_NullValue(_read_.MCDESCRI))
          this.w_MCCONSUP = NVL(cp_ToDate(_read_.MCCONSUP),cp_NullValue(_read_.MCCONSUP))
          this.w_MCSEZBIL = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
          this.w_MCSEQSTA = NVL(cp_ToDate(_read_.MCSEQSTA),cp_NullValue(_read_.MCSEQSTA))
          this.w_MCFLGESE = NVL(cp_ToDate(_read_.MCFLGESE),cp_NullValue(_read_.MCFLGESE))
          this.w_MCNUMLIV = NVL(cp_ToDate(_read_.MCNUMLIV),cp_NullValue(_read_.MCNUMLIV))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    if this.w_AZFLGEAC="S" and this.pTipo $ "CI-CU" 
      if this.pGEST="G"
        this.w_MESS = ""
        * --- Read from PAR_ARCO
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_ARCO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_ARCO_idx,2],.t.,this.PAR_ARCO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PCCODIVA,PCUNIMIS,PCSUFQTA,PCSUFVAL,PSTIPCOS"+;
            " from "+i_cTable+" PAR_ARCO where ";
                +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PCCODIVA,PCUNIMIS,PCSUFQTA,PCSUFVAL,PSTIPCOS;
            from (i_cTable) where;
                PCCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODIVA = NVL(cp_ToDate(_read_.PCCODIVA),cp_NullValue(_read_.PCCODIVA))
          this.w_UNIMIS = NVL(cp_ToDate(_read_.PCUNIMIS),cp_NullValue(_read_.PCUNIMIS))
          this.w_SUFQTA = NVL(cp_ToDate(_read_.PCSUFQTA),cp_NullValue(_read_.PCSUFQTA))
          this.w_SUFVAL = NVL(cp_ToDate(_read_.PCSUFVAL),cp_NullValue(_read_.PCSUFVAL))
          this.w_TIPCOS = NVL(cp_ToDate(_read_.PSTIPCOS),cp_NullValue(_read_.PSTIPCOS))
          use
          if i_Rows=0
            this.w_MESS = AH_MSGFORMAT ("Attenzione parametri archivi collegati non presenti. Impossibile caricare archivi collegati." )
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Not empty(this.w_MESS)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
        endif
      else
        * --- Leggo solo i dati il controllo lo faccio dalla routine chimante
        * --- Read from PAR_ARCO
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_ARCO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_ARCO_idx,2],.t.,this.PAR_ARCO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PCCODIVA,PCUNIMIS,PCSUFQTA,PCSUFVAL,PSTIPCOS"+;
            " from "+i_cTable+" PAR_ARCO where ";
                +"PCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PCCODIVA,PCUNIMIS,PCSUFQTA,PCSUFVAL,PSTIPCOS;
            from (i_cTable) where;
                PCCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODIVA = NVL(cp_ToDate(_read_.PCCODIVA),cp_NullValue(_read_.PCCODIVA))
          this.w_UNIMIS = NVL(cp_ToDate(_read_.PCUNIMIS),cp_NullValue(_read_.PCUNIMIS))
          this.w_SUFQTA = NVL(cp_ToDate(_read_.PCSUFQTA),cp_NullValue(_read_.PCSUFQTA))
          this.w_SUFVAL = NVL(cp_ToDate(_read_.PCSUFVAL),cp_NullValue(_read_.PCSUFVAL))
          this.w_TIPCOS = NVL(cp_ToDate(_read_.PSTIPCOS),cp_NullValue(_read_.PSTIPCOS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    * --- Se la transazione ha gi� dato un errore non si eseguono i controlli aggiuntivi
    if btrserr OR g_COAN<>"S"
      i_retcode = 'stop'
      return
    endif
    * --- Verifico se nei dati azienda � stato impostato il flag di sincronizzazione
    if this.w_AZCOACOG="T" OR this.w_AZCOACOG="S" AND left(this.pTipo,1)="M" OR this.w_AZCOACOG="C" AND left(this.pTipo,1)="C"
      do case
        case this.pTipo = "CI"
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.pTipo = "CU"
          * --- Se il conto non esiste in analitica allora viene creato
          this.w_CONTA = 0
          * --- Select from VOC_COST
          i_nConn=i_TableProp[this.VOC_COST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2],.t.,this.VOC_COST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select count (*) AS CONTA  from "+i_cTable+" VOC_COST ";
                +" where VCCODICE="+cp_ToStrODBC(this.w_ANCODICE)+"";
                 ,"_Curs_VOC_COST")
          else
            select count (*) AS CONTA from (i_cTable);
             where VCCODICE=this.w_ANCODICE;
              into cursor _Curs_VOC_COST
          endif
          if used('_Curs_VOC_COST')
            select _Curs_VOC_COST
            locate for 1=1
            do while not(eof())
            this.w_CONTA = CONTA
              select _Curs_VOC_COST
              continue
            enddo
            use
          endif
          if this.w_CONTA=0
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            if this.w_ANCCTAGG $ "AM" AND (this.w_SEZBIL $ "CR" OR g_APPLICATION <> "ADHOC REVOLUTION") 
              this.w_ANCONSUP1 = this.w_ANCONSUP
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Write into VOC_COST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.VOC_COST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.VOC_COST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"VCDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_ANDESCRI),'VOC_COST','VCDESCRI');
                +",VCDTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_ANDTINVA),'VOC_COST','VCDTINVA');
                +",VCDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_ANDTOBSO),'VOC_COST','VCDTOBSO');
                +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'VOC_COST','UTCV');
                +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'VOC_COST','UTDV');
                +",VCCODMAS ="+cp_NullLink(cp_ToStrODBC(this.w_ANCONSUP),'VOC_COST','VCCODMAS');
                +",VCTIPVOC ="+cp_NullLink(cp_ToStrODBC(this.w_SEZBIL),'VOC_COST','VCTIPVOC');
                    +i_ccchkf ;
                +" where ";
                    +"VCCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                       )
              else
                update (i_cTable) set;
                    VCDESCRI = this.w_ANDESCRI;
                    ,VCDTINVA = this.w_ANDTINVA;
                    ,VCDTOBSO = this.w_ANDTOBSO;
                    ,UTCV = i_CODUTE;
                    ,UTDV = SetInfoDate(g_CALUTD);
                    ,VCCODMAS = this.w_ANCONSUP;
                    ,VCTIPVOC = this.w_SEZBIL;
                    &i_ccchkf. ;
                 where;
                    VCCODICE = this.w_ANCODICE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        case this.pTipo = "CD"
          if this.w_ANCCTAGG $ "AM" AND (this.w_SEZBIL $ "CR" OR g_APPLICATION <> "ADHOC REVOLUTION") 
            * --- Delete from COLLCENT
            i_nConn=i_TableProp[this.COLLCENT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"MRTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                    +" and MRCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                     )
            else
              delete from (i_cTable) where;
                    MRTIPCON = this.w_ANTIPCON;
                    and MRCODICE = this.w_ANCODICE;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            * --- Se la voce di costo � stata usata in altre tabelle (per esempio come voce d ripartizione di altri conti ) allora non viene cancellata
            * --- Try
            local bErr_052342A8
            bErr_052342A8=bTrsErr
            this.Try_052342A8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_052342A8
            * --- End
          endif
        case this.pTipo = "MI"
          if EMPTY(this.w_MCCONSUP) AND this.w_MCNUMLIV<g_MAXLIV
            this.w_MESS = ah_msgformat("Occorre inserire mastro di raggruppamento contabile")
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
            i_retcode = 'stop'
            return
          endif
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.pTipo = "MU"
          if EMPTY(this.w_MCCONSUP) AND this.w_MCNUMLIV<g_MAXLIV
            this.w_MESS = ah_msgformat("Occorre inserire mastro di raggruppamento contabile")
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
            i_retcode = 'stop'
            return
          endif
          * --- Se il mastro  non esiste in analitica allora viene creato
          this.w_CONTA = 0
          * --- Select from MASTVOCI
          i_nConn=i_TableProp[this.MASTVOCI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MASTVOCI_idx,2],.t.,this.MASTVOCI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select count (*) AS CONTA  from "+i_cTable+" MASTVOCI ";
                +" where MCCODICE="+cp_ToStrODBC(this.w_MCCODICE)+"";
                 ,"_Curs_MASTVOCI")
          else
            select count (*) AS CONTA from (i_cTable);
             where MCCODICE=this.w_MCCODICE;
              into cursor _Curs_MASTVOCI
          endif
          if used('_Curs_MASTVOCI')
            select _Curs_MASTVOCI
            locate for 1=1
            do while not(eof())
            this.w_CONTA = CONTA
              select _Curs_MASTVOCI
              continue
            enddo
            use
          endif
          if this.w_CONTA=0
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            if this.w_MCSEZBIL $ "CR"
              this.w_ANCONSUP1 = this.w_MCCONSUP
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Write into MASTVOCI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MASTVOCI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MASTVOCI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MASTVOCI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MCDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_MCDESCRI),'MASTVOCI','MCDESCRI');
                +",MCNUMLIV ="+cp_NullLink(cp_ToStrODBC(this.w_MCNUMLIV),'MASTVOCI','MCNUMLIV');
                +",MCSEQSTA ="+cp_NullLink(cp_ToStrODBC(this.w_MCSEQSTA),'MASTVOCI','MCSEQSTA');
                +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MASTVOCI','UTCV');
                +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'MASTVOCI','UTDV');
                +",MCCONSUP ="+cp_NullLink(cp_ToStrODBC(this.w_MCCONSUP),'MASTVOCI','MCCONSUP');
                +",MCSEZBIL ="+cp_NullLink(cp_ToStrODBC(this.w_MCSEZBIL),'MASTVOCI','MCSEZBIL');
                    +i_ccchkf ;
                +" where ";
                    +"MCCODICE = "+cp_ToStrODBC(this.w_MCCODICE);
                       )
              else
                update (i_cTable) set;
                    MCDESCRI = this.w_MCDESCRI;
                    ,MCNUMLIV = this.w_MCNUMLIV;
                    ,MCSEQSTA = this.w_MCSEQSTA;
                    ,UTCV = i_CODUTE;
                    ,UTDV = SetInfoDate(g_CALUTD);
                    ,MCCONSUP = this.w_MCCONSUP;
                    ,MCSEZBIL = this.w_MCSEZBIL;
                    &i_ccchkf. ;
                 where;
                    MCCODICE = this.w_MCCODICE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Aggiorno i conti di analitia collegato se esiste
              * --- Try
              local bErr_05231A28
              bErr_05231A28=bTrsErr
              this.Try_05231A28()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_05231A28
              * --- End
            endif
          endif
        case this.pTipo = "MD"
          if this.w_MCSEZBIL $ "CR"
            * --- Se il mastro della voce di costo � stato usato in altre tabelle allora non viene cancellato
            * --- Try
            local bErr_05230018
            bErr_05230018=bTrsErr
            this.Try_05230018()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_05230018
            * --- End
          endif
      endcase
      this.w_VOCE = this.w_ANCODICE
    else
      * --- Read from VOC_COST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOC_COST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2],.t.,this.VOC_COST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VCCODICE"+;
          " from "+i_cTable+" VOC_COST where ";
              +"VCCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VCCODICE;
          from (i_cTable) where;
              VCCODICE = this.w_ANCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_VOCE = NVL(cp_ToDate(_read_.VCCODICE),cp_NullValue(_read_.VCCODICE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if this.w_AZFLGEAC="S" AND this.w_MCFLGESE $ "E-Q-V"
      do case
        case this.pTipo $ "CI-CU"
          if this.w_MCFLGESE$ "E-Q" 
            this.w_ESISTE = .F.
            * --- Costruisco codice servizio a quantit� per verificare esistenza del servizio
            this.w_ARCODART = Alltrim(this.w_ANCODICE) + Alltrim(this.w_SUFVAL)
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARTIPART"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARTIPART;
                from (i_cTable) where;
                    ARCODART = this.w_ARCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ARTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if I_rows<>0
              this.w_ESISTE = .T.
            endif
          endif
          if this.w_MCFLGESE$ "E-V" AND this.w_ESISTE
            this.w_ESISTE = .F.
            * --- Costruisco codice servizio a quantit� per verificare esistenza del servizio
            this.w_ARCODART = Alltrim(this.w_ANCODICE)+Alltrim(this.w_SUFQTA)
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARTIPART"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARTIPART;
                from (i_cTable) where;
                    ARCODART = this.w_ARCODART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ARTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if I_rows<>0
              this.w_ESISTE = .T.
            endif
          endif
          if Not this.w_ESISTE
            this.w_DESCRI = LEFT(this.w_ANDESCRI,35)
            * --- Genero categoria contabile articoli collegata
            *     Controllo prima che non esista gi� un incrocio con tale conto 
            *     per creare la categoria articolo
            if this.w_SEZBIL="R"
              * --- Ricavo
              * --- Read from CONTVEAC
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTVEAC_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2],.t.,this.CONTVEAC_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CVCODART"+;
                  " from "+i_cTable+" CONTVEAC where ";
                      +"CVCONRIC = "+cp_ToStrODBC(this.w_ANCODICE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CVCODART;
                  from (i_cTable) where;
                      CVCONRIC = this.w_ANCODICE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_C1CODICE = NVL(cp_ToDate(_read_.CVCODART),cp_NullValue(_read_.CVCODART))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_CONTOR = this.w_ANCODICE
              this.w_CONTOC = space(15)
            else
              * --- Costo
              * --- Read from CONTVEAC
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTVEAC_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2],.t.,this.CONTVEAC_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CVCODART"+;
                  " from "+i_cTable+" CONTVEAC where ";
                      +"CVCONACQ = "+cp_ToStrODBC(this.w_ANCODICE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CVCODART;
                  from (i_cTable) where;
                      CVCONACQ = this.w_ANCODICE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_C1CODICE = NVL(cp_ToDate(_read_.CVCODART),cp_NullValue(_read_.CVCODART))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_CONTOR = space(15)
              this.w_CONTOC = this.w_ANCODICE
            endif
            if i_rows=0
              this.w_C1CODICE = Space(5)
               
 i_nConn=i_TableProp[this.CACOARTI_IDX, 3] 
 cp_NextTableProg(this,i_nConn,"PROCA","i_codazi,w_C1CODICE")
              * --- Try
              local bErr_0523BEC8
              bErr_0523BEC8=bTrsErr
              this.Try_0523BEC8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_0523BEC8
              * --- End
            endif
            * --- Creo combinazioni contropartite Vendite\Acquisti
            * --- Select from CACOCLFO
            i_nConn=i_TableProp[this.CACOCLFO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CACOCLFO_idx,2],.t.,this.CACOCLFO_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select C2CODICE  from "+i_cTable+" CACOCLFO ";
                   ,"_Curs_CACOCLFO")
            else
              select C2CODICE from (i_cTable);
                into cursor _Curs_CACOCLFO
            endif
            if used('_Curs_CACOCLFO')
              select _Curs_CACOCLFO
              locate for 1=1
              do while not(eof())
              * --- Try
              local bErr_05261198
              bErr_05261198=bTrsErr
              this.Try_05261198()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- accept error
                bTrsErr=.f.
              endif
              bTrsErr=bTrsErr or bErr_05261198
              * --- End
                select _Curs_CACOCLFO
                continue
              enddo
              use
            endif
            * --- genero categoria analtica collegata
            *     Controllo prima che non esista gi� una categoria con la voce pari al conto
            *     per creare la categoria analtica 
            if Not Empty(this.w_VOCE)
              if this.w_SEZBIL="R"
                this.w_VOCEC = SPACE(15)
                this.w_VOCER = this.w_VOCE
                * --- Read from CAANARTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CAANARTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CAANARTI_idx,2],.t.,this.CAANARTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CACODICE"+;
                    " from "+i_cTable+" CAANARTI where ";
                        +"CAVOCCOS = "+cp_ToStrODBC(this.w_VOCER);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CACODICE;
                    from (i_cTable) where;
                        CAVOCCOS = this.w_VOCER;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CODICE = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              else
                this.w_VOCEC = this.w_VOCE
                this.w_VOCER = SPACE(15)
                * --- Read from CAANARTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CAANARTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CAANARTI_idx,2],.t.,this.CAANARTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CACODICE"+;
                    " from "+i_cTable+" CAANARTI where ";
                        +"CAVOCCOS = "+cp_ToStrODBC(this.w_VOCEC);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CACODICE;
                    from (i_cTable) where;
                        CAVOCCOS = this.w_VOCEC;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CODICE = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
              if i_ROWS=0
                this.w_CODICE = Space(5)
                 
 i_nConn=i_TableProp[this.CAANARTI_IDX, 3] 
 cp_NextTableProg(this,i_nConn,"PROCN","i_codazi,w_CODICE")
                * --- Try
                local bErr_0525A9E8
                bErr_0525A9E8=bTrsErr
                this.Try_0525A9E8()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_0525A9E8
                * --- End
              endif
            endif
          endif
          this.w_TIPBAR = "0"
          this.w_ARDESART = this.w_ANDESCRI
          this.w_OMODKEY = this.w_ARDESART
          this.w_NMODKEY = this.w_ARDESART
          this.w_ARDTINVA = this.w_ANDTINVA
          this.w_ARDTOBSO = this.w_ANDTOBSO
          this.w_ARPREZUM = "N"
          this.w_ODATKEY = DTOC(this.w_ARDTOBSO)+DTOC(this.w_ARDTINVA)
          this.w_NDATKEY = DTOC(this.w_ARDTOBSO)+DTOC(this.w_ARDTINVA)
          this.w_ARFLUMVA = "N"
          if this.w_MCFLGESE$ "E-V"
            * --- Costruisco codice servizio a quantit�
            this.w_ARCODART = Alltrim(this.w_ANCODICE) + Alltrim(this.w_SUFVAL)
            this.w_ARTIPART = "FM"
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if this.w_MCFLGESE$ "E-Q"
            * --- Costruisco codice servizio a quantit�
            this.w_ARTIPART = "FO"
            this.w_ARCODART = Alltrim(this.w_ANCODICE) + Alltrim(this.w_SUFQTA)
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
      endcase
    endif
  endproc
  proc Try_052342A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from VOC_COST
    i_nConn=i_TableProp[this.VOC_COST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"VCCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
             )
    else
      delete from (i_cTable) where;
            VCCODICE = this.w_ANCODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_05231A28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into VOC_COST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VOC_COST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.VOC_COST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"VCTIPVOC ="+cp_NullLink(cp_ToStrODBC(this.w_MCSEZBIL ),'VOC_COST','VCTIPVOC');
          +i_ccchkf ;
      +" where ";
          +"VCCODMAS = "+cp_ToStrODBC(this.w_MCCODICE);
             )
    else
      update (i_cTable) set;
          VCTIPVOC = this.w_MCSEZBIL ;
          &i_ccchkf. ;
       where;
          VCCODMAS = this.w_MCCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_05230018()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from MASTVOCI
    i_nConn=i_TableProp[this.MASTVOCI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MASTVOCI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MCCODICE = "+cp_ToStrODBC(this.w_MCCODICE);
             )
    else
      delete from (i_cTable) where;
            MCCODICE = this.w_MCCODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_0523BEC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CACOARTI
    i_nConn=i_TableProp[this.CACOARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CACOARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CACOARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"C1CODICE"+",C1DESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_C1CODICE),'CACOARTI','C1CODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'CACOARTI','C1DESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'C1CODICE',this.w_C1CODICE,'C1DESCRI',this.w_DESCRI)
      insert into (i_cTable) (C1CODICE,C1DESCRI &i_ccchkf. );
         values (;
           this.w_C1CODICE;
           ,this.w_DESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    Ah_msg("Generazione categoria contabile %1",.f.,.f.,1,alltrim(this.w_C1CODICE))
    return
  proc Try_05261198()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CONTVEAC
    i_nConn=i_TableProp[this.CONTVEAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTVEAC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CVCODART"+",CVCODCLI"+",CVTIPCON"+",CVCONRIC"+",CVCONOMV"+",CVCONACQ"+",CVCONOMA"+",CVIVAOMA"+",CVIVAOMC"+",CVCONNCC"+",CVCONNCF"+",CVCODCAU"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_C1CODICE),'CONTVEAC','CVCODART');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_CACOCLFO.C2CODICE),'CONTVEAC','CVCODCLI');
      +","+cp_NullLink(cp_ToStrODBC("G"),'CONTVEAC','CVTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONTOR),'CONTVEAC','CVCONRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONTOR),'CONTVEAC','CVCONOMV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONTOC),'CONTVEAC','CVCONACQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONTOC),'CONTVEAC','CVCONOMA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONTOR),'CONTVEAC','CVIVAOMA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONTOC),'CONTVEAC','CVIVAOMC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONTOR),'CONTVEAC','CVCONNCC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONTOC),'CONTVEAC','CVCONNCF');
      +","+cp_NullLink(cp_ToStrODBC(space(5)),'CONTVEAC','CVCODCAU');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CVCODART',this.w_C1CODICE,'CVCODCLI',_Curs_CACOCLFO.C2CODICE,'CVTIPCON',"G",'CVCONRIC',this.w_CONTOR,'CVCONOMV',this.w_CONTOR,'CVCONACQ',this.w_CONTOC,'CVCONOMA',this.w_CONTOC,'CVIVAOMA',this.w_CONTOR,'CVIVAOMC',this.w_CONTOC,'CVCONNCC',this.w_CONTOR,'CVCONNCF',this.w_CONTOC,'CVCODCAU',space(5))
      insert into (i_cTable) (CVCODART,CVCODCLI,CVTIPCON,CVCONRIC,CVCONOMV,CVCONACQ,CVCONOMA,CVIVAOMA,CVIVAOMC,CVCONNCC,CVCONNCF,CVCODCAU &i_ccchkf. );
         values (;
           this.w_C1CODICE;
           ,_Curs_CACOCLFO.C2CODICE;
           ,"G";
           ,this.w_CONTOR;
           ,this.w_CONTOR;
           ,this.w_CONTOC;
           ,this.w_CONTOC;
           ,this.w_CONTOR;
           ,this.w_CONTOC;
           ,this.w_CONTOR;
           ,this.w_CONTOC;
           ,space(5);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    Ah_msg("Generazione contropartite vendite\acquisti  cat. articolo %1 cat. cli\for %2",.f.,.f.,1,alltrim(this.w_C1CODICE),alltrim(_Curs_CACOCLFO.C2CODICE))
    return
  proc Try_0525A9E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAANARTI
    i_nConn=i_TableProp[this.CAANARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAANARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAANARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CACODICE"+",CAVOCCOS"+",CAVOCRIC"+",CADESCRI"+",CAVCCSOM"+",CAVCRCOM"+",CATIPCOS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICE),'CAANARTI','CACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VOCEC),'CAANARTI','CAVOCCOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VOCER),'CAANARTI','CAVOCRIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'CAANARTI','CADESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VOCEC),'CAANARTI','CAVCCSOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VOCER),'CAANARTI','CAVCRCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCOS),'CAANARTI','CATIPCOS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_CODICE,'CAVOCCOS',this.w_VOCEC,'CAVOCRIC',this.w_VOCER,'CADESCRI',this.w_DESCRI,'CAVCCSOM',this.w_VOCEC,'CAVCRCOM',this.w_VOCER,'CATIPCOS',this.w_TIPCOS)
      insert into (i_cTable) (CACODICE,CAVOCCOS,CAVOCRIC,CADESCRI,CAVCCSOM,CAVCRCOM,CATIPCOS &i_ccchkf. );
         values (;
           this.w_CODICE;
           ,this.w_VOCEC;
           ,this.w_VOCER;
           ,this.w_DESCRI;
           ,this.w_VOCEC;
           ,this.w_VOCER;
           ,this.w_TIPCOS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    Ah_msg("Generazione categoria analitica %1",.f.,.f.,1,alltrim(this.w_CODICE))
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se il mastro di raggruppamento delle voci di costo non esiste allora si blocca il salvataggio del conto 
    *     altrimneti crea la voce di costo in modo da mantenere sincronizzato il piano dei conti generale con quello di analitica
    this.w_OKANALITICA = .t.
    this.w_NOTEXISTMASTRO = .F.
    if NOT EMPTY (this.w_ANCONSUP1)
      * --- Select from MASTVOCI
      i_nConn=i_TableProp[this.MASTVOCI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MASTVOCI_idx,2],.t.,this.MASTVOCI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select count (*) as MASRAG  from "+i_cTable+" MASTVOCI ";
            +" where MCCODICE="+cp_ToStrODBC(this.w_ANCONSUP1)+"";
             ,"_Curs_MASTVOCI")
      else
        select count (*) as MASRAG from (i_cTable);
         where MCCODICE=this.w_ANCONSUP1;
          into cursor _Curs_MASTVOCI
      endif
      if used('_Curs_MASTVOCI')
        select _Curs_MASTVOCI
        locate for 1=1
        do while not(eof())
        this.w_MASRAG = MASRAG
          select _Curs_MASTVOCI
          continue
        enddo
        use
      endif
      if this.w_MASRAG=0
        if this.w_AZCOACOG<>"C"
          if this.pGEST="G"
            this.w_MESS = AH_MSGFORMAT ("Non esiste il mastro di raggruppamento %1 nell'archivio mastri voci di costo (analitica). Impossibile confermare." , alltrim(this.w_ANCONSUP1) )
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
            i_retcode = 'stop'
            return
          else
            * --- Aggiorno cursore di errore
            this.w_MESS = AH_MSGFORMAT ("Per la voce %2 non esiste il mastro di raggruppamento %1 nell'archivio mastri voci di costo (analitica). Voce con caricata." , alltrim(this.w_ANCONSUP1) , alltrim(this.w_ANCODICE))
            this.pLOG.AddMsgLogNoTranslate(this.w_MESS)     
            this.w_OKANALITICA = .f.
          endif
        else
          if this.pGEST="G"
            this.w_MESS = AH_MSGFORMAT ("Non esiste il mastro di raggruppamento %1 nell'archivio mastri voci di costo (analitica): inserire il mastro della voce manualmente." , alltrim(this.w_ANCONSUP1) )
          else
            * --- Aggiorno cursore di errore
            this.w_MESS = AH_MSGFORMAT ("Per la voce %2 non esiste il mastro di raggruppamento %1 nell'archivio mastri voci di costo (analitica): inserire il mastro della voce manualmente." , alltrim(this.w_ANCONSUP1) , alltrim(this.w_ANCODICE))
            this.pLOG.AddMsgLogNoTranslate(this.w_MESS)     
          endif
          this.w_NOTEXISTMASTRO = .T.
          this.w_ANCONSUP = space(15)
        endif
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_ANCCTAGG $ "AM" AND (this.w_SEZBIL $ "CR" OR g_APPLICATION <> "ADHOC REVOLUTION") 
      this.w_ANCONSUP1 = this.w_ANCONSUP
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Se la voce di costo esiste gia non la ricrea
      if this.w_OKANALITICA
        * --- Try
        local bErr_05212690
        bErr_05212690=bTrsErr
        this.Try_05212690()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_05212690
        * --- End
        this.w_RIGHEANALITICA = 0
        * --- verifica inserimento modello se non gi� specificato
        if UPPER (this.oparentobject.class)="TGSAR_API"
          if g_APPLICATION="ADHOC REVOLUTION"
            this.w_RIGHEANALITICA = this.oparentobject.GSAR_mrc.NumRow()
          else
            this.w_RIGHEANALITICA = this.oparentobject.GSAR_mrb.GSAR_mrc.NumRow()
          endif
        else
          * --- Read from COLLCENT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COLLCENT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2],.t.,this.COLLCENT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CPROWNUM"+;
              " from "+i_cTable+" COLLCENT where ";
                  +"MRTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                  +" and MRCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CPROWNUM;
              from (i_cTable) where;
                  MRTIPCON = this.w_ANTIPCON;
                  and MRCODICE = this.w_ANCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_RIGHEANALITICA = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if this.w_RIGHEANALITICA=0
          * --- Select from COLLCENT
          i_nConn=i_TableProp[this.COLLCENT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2],.t.,this.COLLCENT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select Count(MRCODICE) as Numocc  from "+i_cTable+" COLLCENT ";
                +" where MRTIPCON = 'G' AND MRCODICE = "+cp_ToStrODBC(this.w_ANCODICE)+"";
                 ,"_Curs_COLLCENT")
          else
            select Count(MRCODICE) as Numocc from (i_cTable);
             where MRTIPCON = "G" AND MRCODICE = this.w_ANCODICE;
              into cursor _Curs_COLLCENT
          endif
          if used('_Curs_COLLCENT')
            select _Curs_COLLCENT
            locate for 1=1
            do while not(eof())
            if Nvl(_Curs_COLLCENT.Numocc, 0) = 0
              if g_APPLICATION<>"ADHOC REVOLUTION"
                GSAR1BMC(this,this.w_ANTIPCON,this.w_ANCODICE)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                * --- Insert into COLLCENT
                i_nConn=i_TableProp[this.COLLCENT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COLLCENT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"MRTIPCON"+",MRPARAME"+",CPROWNUM"+",MRCODICE"+",MRCODVOC"+",MRROWORD"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_ANTIPCON),'COLLCENT','MRTIPCON');
                  +","+cp_NullLink(cp_ToStrODBC(1),'COLLCENT','MRPARAME');
                  +","+cp_NullLink(cp_ToStrODBC(1),'COLLCENT','CPROWNUM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'COLLCENT','MRCODICE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'COLLCENT','MRCODVOC');
                  +","+cp_NullLink(cp_ToStrODBC(1),'COLLCENT','MRROWORD');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'MRTIPCON',this.w_ANTIPCON,'MRPARAME',1,'CPROWNUM',1,'MRCODICE',this.w_ANCODICE,'MRCODVOC',this.w_ANCODICE,'MRROWORD',1)
                  insert into (i_cTable) (MRTIPCON,MRPARAME,CPROWNUM,MRCODICE,MRCODVOC,MRROWORD &i_ccchkf. );
                     values (;
                       this.w_ANTIPCON;
                       ,1;
                       ,1;
                       ,this.w_ANCODICE;
                       ,this.w_ANCODICE;
                       ,1;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              else
                * --- Insert into COLLCENT
                i_nConn=i_TableProp[this.COLLCENT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COLLCENT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"MRTIPCON"+",MRPARAME"+",CPROWNUM"+",MRCODICE"+",MRCODVOC"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_ANTIPCON),'COLLCENT','MRTIPCON');
                  +","+cp_NullLink(cp_ToStrODBC(1),'COLLCENT','MRPARAME');
                  +","+cp_NullLink(cp_ToStrODBC(1),'COLLCENT','CPROWNUM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'COLLCENT','MRCODICE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'COLLCENT','MRCODVOC');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'MRTIPCON',this.w_ANTIPCON,'MRPARAME',1,'CPROWNUM',1,'MRCODICE',this.w_ANCODICE,'MRCODVOC',this.w_ANCODICE)
                  insert into (i_cTable) (MRTIPCON,MRPARAME,CPROWNUM,MRCODICE,MRCODVOC &i_ccchkf. );
                     values (;
                       this.w_ANTIPCON;
                       ,1;
                       ,1;
                       ,this.w_ANCODICE;
                       ,this.w_ANCODICE;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              endif
              * --- Imposta il flag automatismo a manuale
              * --- Write into CONTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"ANCCTAGG ="+cp_NullLink(cp_ToStrODBC("M"),'CONTI','ANCCTAGG');
                    +i_ccchkf ;
                +" where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                       )
              else
                update (i_cTable) set;
                    ANCCTAGG = "M";
                    &i_ccchkf. ;
                 where;
                    ANTIPCON = this.w_ANTIPCON;
                    and ANCODICE = this.w_ANCODICE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
              select _Curs_COLLCENT
              continue
            enddo
            use
          endif
        endif
      endif
    endif
  endproc
  proc Try_05212690()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VOC_COST
    i_nConn=i_TableProp[this.VOC_COST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VOC_COST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"VCCODICE"+",VCDESCRI"+",VCCODMAS"+",VCTIPVOC"+",VCDTINVA"+",VCDTOBSO"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'VOC_COST','VCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANDESCRI),'VOC_COST','VCDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCONSUP),'VOC_COST','VCCODMAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SEZBIL),'VOC_COST','VCTIPVOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANDTINVA),'VOC_COST','VCDTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANDTOBSO),'VOC_COST','VCDTOBSO');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'VOC_COST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'VOC_COST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'VOC_COST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'VOC_COST','UTDV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'VCCODICE',this.w_ANCODICE,'VCDESCRI',this.w_ANDESCRI,'VCCODMAS',this.w_ANCONSUP,'VCTIPVOC',this.w_SEZBIL,'VCDTINVA',this.w_ANDTINVA,'VCDTOBSO',this.w_ANDTOBSO,'UTCC',i_CODUTE,'UTCV',i_CODUTE,'UTDC',SetInfoDate(g_CALUTD),'UTDV',SetInfoDate(g_CALUTD))
      insert into (i_cTable) (VCCODICE,VCDESCRI,VCCODMAS,VCTIPVOC,VCDTINVA,VCDTOBSO,UTCC,UTCV,UTDC,UTDV &i_ccchkf. );
         values (;
           this.w_ANCODICE;
           ,this.w_ANDESCRI;
           ,this.w_ANCONSUP;
           ,this.w_SEZBIL;
           ,this.w_ANDTINVA;
           ,this.w_ANDTOBSO;
           ,i_CODUTE;
           ,i_CODUTE;
           ,SetInfoDate(g_CALUTD);
           ,SetInfoDate(g_CALUTD);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if UPPER (this.oparentobject.class)="TGSAR_API"
      this.w_NOTEXISTMASTRO = .T.
      if this.w_NOTEXISTMASTRO
        ah_ErrorMsg("Creata voce di analitica %1 di tipo %2%0%3", "","",alltrim (this.w_ANCODICE) , ICASE (alltrim(this.w_SEZBIL)="C","Costo",alltrim(this.w_SEZBIL)="R","Ricavo","Patrimoniale" ),this.w_MESS)
      else
        ah_ErrorMsg("Creata voce di analitica %1 di tipo %2", "","",alltrim (this.w_ANCODICE) , ICASE (alltrim(this.w_SEZBIL)="C","Costo",alltrim(this.w_SEZBIL)="R","Ricavo","Patrimoniale" ))
      endif
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_MCSEZBIL $ "CR"
      this.w_ANCONSUP1 = this.w_MCCONSUP
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Se il mastro voce di costo esiste gia non lo ricrea
      * --- Try
      local bErr_0520E9A0
      bErr_0520E9A0=bTrsErr
      this.Try_0520E9A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0520E9A0
      * --- End
    endif
  endproc
  proc Try_0520E9A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MASTVOCI
    i_nConn=i_TableProp[this.MASTVOCI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MASTVOCI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MASTVOCI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MCCODICE"+",MCDESCRI"+",MCCONSUP"+",MCSEZBIL"+",MCNUMLIV"+",MCSEQSTA"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MCCODICE),'MASTVOCI','MCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCDESCRI),'MASTVOCI','MCDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCCONSUP),'MASTVOCI','MCCONSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCSEZBIL),'MASTVOCI','MCSEZBIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCNUMLIV),'MASTVOCI','MCNUMLIV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCSEQSTA),'MASTVOCI','MCSEQSTA');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MASTVOCI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'MASTVOCI','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'MASTVOCI','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'MASTVOCI','UTDV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MCCODICE',this.w_MCCODICE,'MCDESCRI',this.w_MCDESCRI,'MCCONSUP',this.w_MCCONSUP,'MCSEZBIL',this.w_MCSEZBIL,'MCNUMLIV',this.w_MCNUMLIV,'MCSEQSTA',this.w_MCSEQSTA,'UTCC',i_CODUTE,'UTCV',i_CODUTE,'UTDC',SetInfoDate(g_CALUTD),'UTDV',SetInfoDate(g_CALUTD))
      insert into (i_cTable) (MCCODICE,MCDESCRI,MCCONSUP,MCSEZBIL,MCNUMLIV,MCSEQSTA,UTCC,UTCV,UTDC,UTDV &i_ccchkf. );
         values (;
           this.w_MCCODICE;
           ,this.w_MCDESCRI;
           ,this.w_MCCONSUP;
           ,this.w_MCSEZBIL;
           ,this.w_MCNUMLIV;
           ,this.w_MCSEQSTA;
           ,i_CODUTE;
           ,i_CODUTE;
           ,SetInfoDate(g_CALUTD);
           ,SetInfoDate(g_CALUTD);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_ErrorMsg("Creato mastro voci di costo analitica %1 di tipo %2", "","",alltrim (this.w_MCCODICE) , ICASE (alltrim(this.w_MCSEZBIL)="C","Costo",alltrim(this.w_MCSEZBIL)="R","Ricavo","Patrimoniale" ))
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione servizio
    * --- Try
    local bErr_0520DE00
    bErr_0520DE00=bTrsErr
    this.Try_0520DE00()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Aggiorno descrizione
      if this.pGEST="G" and Ah_yesno("Si desidera aggiornare il servizio %1 collegato?","",alltrim(this.w_ARCODART)) 
 
        * --- Write into ART_ICOL
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ARDESART ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'ART_ICOL','ARDESART');
              +i_ccchkf ;
          +" where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
                 )
        else
          update (i_cTable) set;
              ARDESART = this.w_ARDESART;
              &i_ccchkf. ;
           where;
              ARCODART = this.w_ARCODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Write into KEY_ARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CADESART ="+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'KEY_ARTI','CADESART');
              +i_ccchkf ;
          +" where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_ARCODART);
                 )
        else
          update (i_cTable) set;
              CADESART = this.w_ARDESART;
              &i_ccchkf. ;
           where;
              CACODICE = this.w_ARCODART;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    bTrsErr=bTrsErr or bErr_0520DE00
    * --- End
  endproc
  proc Try_0520DE00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ART_ICOL
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ARCODART"+",ARDESART"+",ARUNMIS1"+",AROPERAT"+",ARCODIVA"+",ARCODCAA"+",ARCATCON"+",ARTIPBAR"+",ARCATOPE"+",ARTIPART"+",ARTIPPKR"+",ARPREZUM"+",ARFLUMVA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ARCODART),'ART_ICOL','ARCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARDESART),'ART_ICOL','ARDESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'ART_ICOL','ARUNMIS1');
      +","+cp_NullLink(cp_ToStrODBC(0),'ART_ICOL','AROPERAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODIVA),'ART_ICOL','ARCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'ART_ICOL','ARCODCAA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_C1CODICE),'ART_ICOL','ARCATCON');
      +","+cp_NullLink(cp_ToStrODBC("0"),'ART_ICOL','ARTIPBAR');
      +","+cp_NullLink(cp_ToStrODBC("AR"),'ART_ICOL','ARCATOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPART),'ART_ICOL','ARTIPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARTIPART),'ART_ICOL','ARTIPPKR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARPREZUM),'ART_ICOL','ARPREZUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ARFLUMVA),'ART_ICOL','ARFLUMVA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ARCODART',this.w_ARCODART,'ARDESART',this.w_ARDESART,'ARUNMIS1',this.w_UNIMIS,'AROPERAT',0,'ARCODIVA',this.w_CODIVA,'ARCODCAA',this.w_CODICE,'ARCATCON',this.w_C1CODICE,'ARTIPBAR',"0",'ARCATOPE',"AR",'ARTIPART',this.w_ARTIPART,'ARTIPPKR',this.w_ARTIPART,'ARPREZUM',this.w_ARPREZUM)
      insert into (i_cTable) (ARCODART,ARDESART,ARUNMIS1,AROPERAT,ARCODIVA,ARCODCAA,ARCATCON,ARTIPBAR,ARCATOPE,ARTIPART,ARTIPPKR,ARPREZUM,ARFLUMVA &i_ccchkf. );
         values (;
           this.w_ARCODART;
           ,this.w_ARDESART;
           ,this.w_UNIMIS;
           ,0;
           ,this.w_CODIVA;
           ,this.w_CODICE;
           ,this.w_C1CODICE;
           ,"0";
           ,"AR";
           ,this.w_ARTIPART;
           ,this.w_ARTIPART;
           ,this.w_ARPREZUM;
           ,this.w_ARFLUMVA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    Ah_msg("Generazione servizio %1",.f.,.f.,1,alltrim(this.w_ARCODART))
    * --- Creo relativo codice di ricerca
    GSMA_BCV(this,"I")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo,pCodice,pGest,pLog,pPadre)
    this.pTipo=pTipo
    this.pCodice=pCodice
    this.pGest=pGest
    this.pLog=pLog
    this.pPadre=pPadre
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='VOC_COST'
    this.cWorkTables[2]='MASTVOCI'
    this.cWorkTables[3]='COLLCENT'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='MASTRI'
    this.cWorkTables[6]='AZIENDA'
    this.cWorkTables[7]='PAR_ARCO'
    this.cWorkTables[8]='CAANARTI'
    this.cWorkTables[9]='CACOARTI'
    this.cWorkTables[10]='ART_ICOL'
    this.cWorkTables[11]='CONTVEAC'
    this.cWorkTables[12]='CACOCLFO'
    this.cWorkTables[13]='KEY_ARTI'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_VOC_COST')
      use in _Curs_VOC_COST
    endif
    if used('_Curs_MASTVOCI')
      use in _Curs_MASTVOCI
    endif
    if used('_Curs_CACOCLFO')
      use in _Curs_CACOCLFO
    endif
    if used('_Curs_MASTVOCI')
      use in _Curs_MASTVOCI
    endif
    if used('_Curs_COLLCENT')
      use in _Curs_COLLCENT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo,pCodice,pGest,pLog,pPadre"
endproc
