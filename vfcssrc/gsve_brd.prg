* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_brd                                                        *
*              Ristampa documenti                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_116]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOpe,pAUTOMATIZZA,pNOMEFILE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_brd",oParentObject,m.pTipOpe,m.pAUTOMATIZZA,m.pNOMEFILE)
return(i_retval)

define class tgsve_brd as StdBatch
  * --- Local variables
  pTipOpe = space(1)
  pAUTOMATIZZA = 0
  pNOMEFILE = space(254)
  w_STAMPA1 = space(8)
  w_QUERY = space(50)
  w_OTES = space(1)
  w_MVCODMAG = space(5)
  w_CODCLA = space(3)
  w_MVSERIAL = space(10)
  w_OLDMVSERIAL = space(10)
  w_REPORT = space(50)
  w_MVTCAMAG = space(5)
  w_CODSTA = space(3)
  w_MESS = space(50)
  w_OLDMAGA = space(5)
  w_MVCAUMAG = space(5)
  w_CAUMAG = space(5)
  w_COND = .f.
  w_OLDCAUSA = space(5)
  w_TIPCON = space(1)
  w_CODMAG = space(5)
  w_DATINI = ctod("  /  /  ")
  w_MVTCOMAG = space(5)
  w_DESSUP = space(0)
  w_CODICE1 = space(20)
  w_BIANCO = .f.
  w_OLDMVSER = space(10)
  w_CODCON = space(15)
  w_CODART = space(20)
  w_DESART = space(40)
  w_FLINCA = space(1)
  w_OKMAG = .f.
  w_PRIMAG = space(5)
  w_OLDREC = 0
  w_RECO = 0
  w_TIPCLF = space(1)
  w_TRPRGST = 0
  w_KEYOBS = ctod("  /  /  ")
  w_TIPCON1 = space(1)
  w_CODCON1 = space(15)
  w_DATDOC = ctod("  /  /  ")
  w_DTOBS1 = ctod("  /  /  ")
  w_codic = space(15)
  w_nmdes = space(40)
  w_ind = space(40)
  w_cap = space(8)
  w_loca = space(30)
  w_prov = space(2)
  w_rif = space(2)
  w_PRIMAT = space(5)
  w_DESART1 = space(40)
  w_DESSUP1 = space(0)
  w_MVCODICE = space(20)
  w_OLDSER = space(10)
  w_CARMEM = 0
  w_NOMEFILE = space(254)
  w_PATHFILE = space(254)
  w_SHOWMSG = .f.
  w_oMultiReport = .NULL.
  w_TIPDOC = space(5)
  w_CntCausali = 0
  w_FIND = .f.
  w_DPSERIAL = space(10)
  w_LFLPROV = space(1)
  w_CODDES = space(5)
  w_REPSEC1 = space(1)
  w_NO_RISTAMPA_PROD = .f.
  w_EMAIL = space(10)
  w_EMPEC = space(10)
  w_FAXNO = space(10)
  w_NomeRep = space(10)
  w_EXT = space(10)
  w_EXTFORMAT = space(10)
  w_DOCM = space(1)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  OUT_PUTS_idx=0
  VALUTE_idx=0
  CONTI_idx=0
  TRADDOCU_idx=0
  DES_DIVE_idx=0
  TRADARTI_idx=0
  OUTPUTMP_idx=0
  TMPPROV_idx=0
  TIP_DOCU_idx=0
  TMPMOVIMAST_idx=0
  PAR_PARC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia la Ristampa Documenti (da GSVE_SDV, GSVE_AFD, GSCO_BMG, GSCO_BGD, GSCO_BDP, GSVA_BPS)
    *     Il parametro �  B quando il batch � lanciato da GSCO_BDP
    *     P quando il batch � lanciato da GSVA_BPS e A per tutti gli altri
    *     Se parametro = F Stampa solo i documenti che hanno una causale di tipo w_TIPOIN
    * --- pAUTOMATIZZA: param opzionale passato alla cp_chprn
    * --- pNOMEFILE: param opzionale per creare un file dalla cp_chprn
    this.w_SHOWMSG = vartype(this.pAUTOMATIZZA)<>"N" OR this.pAUTOMATIZZA<=0
    * --- Controllo date esercizio
    if this.oParentObject.w_DATAIN<this.oParentObject.w_DATA1 OR this.oParentObject.w_DATAFI>this.oParentObject.w_DATA2
      AH_ErrorMsg("Le date selezionate non sono contenute nell'esercizio")
      i_retcode = 'stop'
      return
    endif
    if this.w_SHOWMSG
      AH_MSG("Elaborazione stampa in corso...", .t., .t.)
    endif
    this.w_oMultiReport=createobject("MultiReport")
    * --- Tabella che contiene le causali da stampare
    * --- Create temporary table TMPPROV
    i_nIdx=cp_AddTableDef('TMPPROV') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"TDTIPDOC "," from "+i_cTable;
          +" where 1=0";
          )
    this.TMPPROV_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Ciclo sul cursore dello zoom per inserire le causali selezionate
    this.w_CntCausali = 0
    if this.pTipOpe<>"P"
      if this.pTipOpe<>"F" AND Empty(this.oParentObject.w_TIPOIN)
         
 LOCAL NC 
 NC=This.oParentObject.w_ZoomTD.cCursor
        SELECT(NC)
        GO TOP
        SCAN
        this.w_TIPDOC = TDTIPDOC
        if XCHK=1
          this.w_FIND = .T.
          * --- Insert into TMPPROV
          i_nConn=i_TableProp[this.TMPPROV_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPPROV_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPPROV_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"TDTIPDOC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_TIPDOC),'TMPPROV','TDTIPDOC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'TDTIPDOC',this.w_TIPDOC)
            insert into (i_cTable) (TDTIPDOC &i_ccchkf. );
               values (;
                 this.w_TIPDOC;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_CntCausali = this.w_CntCausali + 1
        endif
        SELECT(NC)
        ENDSCAN
      else
        * --- Inserisco l' unica causale w_TIPOIN
        this.w_FIND = .T.
        * --- Insert into TMPPROV
        i_nConn=i_TableProp[this.TMPPROV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPROV_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPPROV_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TDTIPDOC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPOIN),'TMPPROV','TDTIPDOC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TDTIPDOC',this.oParentObject.w_TIPOIN)
          insert into (i_cTable) (TDTIPDOC &i_ccchkf. );
             values (;
               this.oParentObject.w_TIPOIN;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        if Type("This.oParentObject.w_MVSERIAL")="C"
          this.w_MVSERIAL = This.oParentObject.w_MVSERIAL
        endif
        this.w_CntCausali = 1
      endif
    else
      this.w_FIND = .T.
    endif
    * --- Se non ho selezionato nessuna causale esco
    if !this.w_FIND
      AH_ErrorMsg("Selezionare almeno una causale documento")
      WAIT CLEAR
      * --- Drop temporary table TMPPROV
      i_nIdx=cp_GetTableDefIdx('TMPPROV')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPPROV')
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Estraggo i documenti che dovr� stampare, ora non s�  ancora quali report 
    *     utilizzare, per ogni documento seleziono la lingua, gruppo di default, tipo doc
    do case
      case UPPER(ALLTRIM(this.oParentObject.Class))="TGSCO_BMG"
        * --- Richiamato dai paini di generazione della produzione deve filtrare solo i movimenti selezionati
        * --- Create temporary table TMPMOVIMAST
        i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMPMOVIMAST_proto';
              )
        this.TMPMOVIMAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        if USED("SPUNTA")
          SELECT SPUNTA
          GO TOP
          SCAN
          * --- Insert into TMPMOVIMAST
          i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPMOVIMAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CCSERIAL"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(SPUNTA.PDSERDOC),'TMPMOVIMAST','CCSERIAL');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',SPUNTA.PDSERDOC)
            insert into (i_cTable) (CCSERIAL &i_ccchkf. );
               values (;
                 SPUNTA.PDSERDOC;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          SELECT SPUNTA
          ENDSCAN
        endif
        * --- Create temporary table OUTPUTMP
        i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSVE1BRD',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.OUTPUTMP_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Drop temporary table TMPMOVIMAST
        i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPMOVIMAST')
        endif
      case UPPER(ALLTRIM(this.oParentObject.Class))="TGSVA_BPS"
        this.w_DPSERIAL = this.oParentObject.oParentObject.w_PSSERIAL
        * --- Create temporary table OUTPUTMP
        i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('..\VEFA\EXE\QUERY\GSVA_BRD',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.OUTPUTMP_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      otherwise
        this.w_LFLPROV = "T"
        if this.pTipOpe="A" and vartype( this.oParentObject.w_FLPROV)="C"
          this.w_LFLPROV = this.oParentObject.w_FLPROV
        endif
        if this.pTipOpe="F"
          this.w_CODDES = ""
        endif
        * --- Create temporary table OUTPUTMP
        i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSVE_BRD',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.OUTPUTMP_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endcase
    if this.w_SHOWMSG
      AH_MSG("Elaborazione stampa in corso...", .t., .t.)
    endif
    * --- Controllo w_REPSEC, se non esiste stampo sempre i report secondari
    if TYPE("This.oParentObject.w_REPSEC") <> "C"
      if this.pTipOpe<>"F"
        this.w_REPSEC1 = "S"
      else
        * --- Se REPSEC non esiste e devo stampare solo un tipo di causale leggo le impostazioni 
        *     sul report secondario dalla causale
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLSTLM"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_TIPOIN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLSTLM;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_TIPOIN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_REPSEC1 = NVL(cp_ToDate(_read_.TDFLSTLM),cp_NullValue(_read_.TDFLSTLM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_REPSEC1="B"
          this.w_REPSEC1 = "D"
        endif
      endif
    else
      this.w_REPSEC1 = this.oParentObject.w_REPSEC
    endif
    * --- Creo il multireport per la stampa
    this.w_NO_RISTAMPA_PROD = .F.
    if !IsAlt()
      if NOT GSAR_BSD(this,this.w_oMultiReport, This.oParentObject, "", "", "", this.w_REPSEC1, .F.,IIF(upper(this.oParentObject.Class)="TGSVE_SDV",.T.,.F. ))
        * --- Drop temporary table OUTPUTMP
        i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('OUTPUTMP')
        endif
        * --- Drop temporary table TMPPROV
        i_nIdx=cp_GetTableDefIdx('TMPPROV')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPPROV')
        endif
        this.bUpdateParentObject=.F.
        i_retcode = 'stop'
        return
      endif
    else
      * --- Se c'� una sola causale da ristampare, eventualmente chiede quale report principale utilizzare
      GSAR_BSD(this,this.w_oMultiReport, This.oParentObject, "", "", "", this.w_REPSEC1, !(this.w_CntCausali = 1) )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_SHOWMSG
      AH_MSG("Elaborazione stampa in corso...", .t., .t.)
    endif
    * --- Drop temporary table OUTPUTMP
    i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('OUTPUTMP')
    endif
    * --- Drop temporary table TMPPROV
    i_nIdx=cp_GetTableDefIdx('TMPPROV')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPPROV')
    endif
     
 dimension iva(6,2) 
 dimension doc_rate(999,3) 
 dimension doc_imba(20,7) 
 dimension doc_rite(6,5) 
 iva="" 
 doc_rate="" 
 doc_rite="" 
 doc_imba=""
    dimension aCONT_ACC(6,3)
    aCONT_ACC=""
    * --- Leggo il cambio della valuta per poi utilizzarlo nelle varie stampe
    if IsAhe()
      l_cambio=g_CAOEUR
       
 dimension contocli (60,6) 
 contocli="" 
 dimension sedefat(12) 
 sedefat=""
    else
      l_cambio=GETCAM(g_PERVAL, I_DATSYS)
    endif
    * --- Ricerco gli indirizzi Email ed il numero di Fax
    if NOT EMPTY(this.oParentObject.w_CLIFOR) AND this.oParentObject.w_CATDOC $ ("DT-FA-NC"-"OR")
      if this.oParentObject.w_FLVEAC = "V"
        * --- Cliente
        this.w_EMAIL = GSAR_BRM(THIS,"E_M","C", this.oParentObject.w_CLIFOR, ICASE(this.oParentObject.w_CATDOC="DT", "CO", this.oParentObject.w_CATDOC$"FA-NC", "FA"," "), "R")
        this.w_EMPEC = GSAR_BRM(THIS,"PEC","C", this.oParentObject.w_CLIFOR, ICASE(this.oParentObject.w_CATDOC="DT", "CO", this.oParentObject.w_CATDOC$"FA-NC", "FA"," "), "R")
        this.w_FAXNO = GSAR_BRM(THIS,"E_M","C", this.oParentObject.w_CLIFOR, ICASE(this.oParentObject.w_CATDOC="DT", "CO", this.oParentObject.w_CATDOC$"FA-NC", "FA"," "), "X")
        * --- Valorizzo le variabili globali per il Fax (per la selezione del numero di telefono)
        i_CLIFORDES = "C" 
 i_CODDES = this.oParentObject.w_CLIFOR 
 i_TIPDES = ICASE(this.oParentObject.w_CATDOC="DT", "CO", this.oParentObject.w_CATDOC="FA","FA", "")
      else
        * --- Fornitore
        this.w_EMAIL = GSAR_BRM(THIS,"E_M","F",this.oParentObject.w_CLIFOR, ICASE(this.oParentObject.w_CATDOC="DT", "CO", this.oParentObject.w_CATDOC$"FA-NC","FA", " "), "R")
        this.w_EMPEC = GSAR_BRM(THIS,"PEC","F",this.oParentObject.w_CLIFOR, ICASE(this.oParentObject.w_CATDOC="DT", "CO", this.oParentObject.w_CATDOC$"FA-NC","FA", " "), "R")
        this.w_FAXNO = GSAR_BRM(THIS,"E_M","F",this.oParentObject.w_CLIFOR, ICASE(this.oParentObject.w_CATDOC="DT", "CO", this.oParentObject.w_CATDOC$"FA-NC","FA", " "), "X")
        * --- Valorizzo le variabili globali per il Fax (per la selezione del numero di telefono)
        i_CLIFORDES = "F" 
 i_CODDES = this.oParentObject.w_CLIFOR 
 i_TIPDES = ICASE(this.oParentObject.w_CATDOC="DT", "CO", this.oParentObject.w_CATDOC="FA","FA", "")
      endif
    endif
    i_EMAIL = this.w_EMAIL
    i_EMAIL_PEC = this.w_EMPEC
    i_FAXNO = this.w_FAXNO
    * --- Lancio la Stampa
    if this.w_NO_RISTAMPA_PROD and this.w_SHOWMSG
      ah_ErrorMsg("Sono presenti report di produzione che non possono essere ristampati.%0Utilizzare l'apposita voce di menu 'Documenti di produzione'.")
    endif
    if this.w_SHOWMSG
      CP_CHPRN(this.w_oMultiReport, , this)
      WAIT CLEAR
    else
      if VARTYPE(this.pNOMEFILE)="C" AND NOT EMPTY(this.pNOMEFILE)
        this.w_NOMEFILE = JUSTFNAME(this.pNOMEFILE)
        this.w_PATHFILE = ADDBS(JUSTPATH(this.pNOMEFILE))
      endif
      if this.pAUTOMATIZZA=10
        * --- Crea PDF: per velocizzare l'operazione non apro la print system
        this.w_NOMEFILE = this.pNOMEFILE
        this.w_NomeRep = this.w_oMultiReport.FirstReport()
        if not empty(this.w_NomeRep)
          * --- Verifica Estensione Corretta
          this.w_EXT = "PDF"
          * --- Estensione del formato
          this.w_EXTFORMAT = this.w_EXT
          * --- Chiama la Funzione di Generazione dei File secondo il Formato 
          L_PagGen = -1
          RET = Print_To_File(this.w_NOMEFILE, this.w_oMultiReport, this.w_EXT, "", @L_PagGen)
          if not RET or L_PagGen>0
            ah_ErrorMsg("Errore durante la generazione del file","stop","")
            i_retcode = 'stop'
            return
          endif
        endif
      else
        * --- disattivo l'eventuale processo documentale
        this.w_DOCM = g_DOCM
        g_DOCM = "N"
        CP_CHPRN(this.w_oMultiReport, , this, this.pAUTOMATIZZA)
        g_DOCM = this.w_DOCM
      endif
    endif
    * --- Sbianco gli eventuali valori assegnati alle variabili globali per Email e Fax
    i_EMAIL="" 
 i_EMAIL_PEC="" 
 i_FAXNO=""
  endproc


  proc Init(oParentObject,pTipOpe,pAUTOMATIZZA,pNOMEFILE)
    this.pTipOpe=pTipOpe
    this.pAUTOMATIZZA=pAUTOMATIZZA
    this.pNOMEFILE=pNOMEFILE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,12)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='OUT_PUTS'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='TRADDOCU'
    this.cWorkTables[6]='DES_DIVE'
    this.cWorkTables[7]='TRADARTI'
    this.cWorkTables[8]='*OUTPUTMP'
    this.cWorkTables[9]='*TMPPROV'
    this.cWorkTables[10]='TIP_DOCU'
    this.cWorkTables[11]='*TMPMOVIMAST'
    this.cWorkTables[12]='PAR_PARC'
    return(this.OpenAllTables(12))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOpe,pAUTOMATIZZA,pNOMEFILE"
endproc
