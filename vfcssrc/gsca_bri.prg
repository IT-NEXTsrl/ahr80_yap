* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bri                                                        *
*              Elabora ripartizioni C/C                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_294]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-03-11                                                      *
* Last revis.: 2014-02-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bri",oParentObject)
return(i_retval)

define class tgsca_bri as StdBatch
  * --- Local variables
  w_GENERA = .f.
  w_FLRIPA = space(1)
  w_SERRIF = space(10)
  w_ORDRIF = 0
  w_NUMRIF = 0
  w_UNIVOC = 0
  w_ORILIV = 0
  w_OK = .f.
  w_MESS = space(10)
  w_NLIV = 0
  w_TOTREC = 0
  w_ESEPRE = space(4)
  w_CODAZI = space(5)
  w_VALNAZ = space(3)
  w_DECTOT = 0
  w_DIFDAT = 0
  w_OLDREC = space(10)
  w_NEWREC = space(10)
  w_NUMLIV = 0
  w_NUMRIG = 0
  w_DATREG = ctod("  /  /  ")
  w_CODVOC = space(15)
  w_CODICE = space(15)
  w_SEGNO = space(1)
  w_TOTIMP = 0
  w_TIPREG = space(2)
  w_CODESE = space(4)
  w_INICOM = ctod("  /  /  ")
  w_FINCOM = ctod("  /  /  ")
  w_FLDIRE = space(1)
  w_CODCOM = space(15)
  w_CODSUP = space(15)
  w_IMPSUP = 0
  w_TOTRIP = 0
  w_CREATT = space(1)
  w_NUMERO = 0
  w_ObjInfo = .NULL.
  w_DBVersion = space(254)
  * --- WorkFile variables
  DOC_DETT_idx=0
  MOVICOST_idx=0
  MOVIRIPA_idx=0
  MVM_DETT_idx=0
  RIPABIDONE_idx=0
  RIPATMP1_idx=0
  RIPATMP2_idx=0
  RIPATMP3_idx=0
  VALUTE_idx=0
  ESERCIZI_idx=0
  TMP_ANA_idx=0
  RIPATMP4_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Ripartizioni Centri di Costo/Ricavo per Attivita' (da GSCA_KRI)
    this.w_CREATT = "S"
    this.w_ObjInfo = GSUT_BOS()
    this.w_DBVersion = substr(this.w_ObjInfo.DBVersion,1,1)
    * --- Visual Query Utilizzate:
    * --- GSCA_QRM - Query sui Movimenti di Analitica Manuali - Effettivi
    * --- GSCA_QRP - Query sui Movimenti di Analitica Primanota - Effettivi
    * --- GSCA_QRD - Query sui Movimenti di Analitica Documenti - Effettivi
    * --- GSCA1QRM - Query sui Movimenti di Analitica Manuali - Previsionali
    * --- GSCAOQRD - Query Movimenti di storno Righe omaggio ripartite - Effettivi per Competenza
    * --- NGSCA_QRM - Query sui Movimenti di Analitica Manuali - Effettivi per competenza
    * --- NGSCA1QRP - Query sui Movimenti di Analitica Primanota - Effettivi per competenza
    * --- NGSCA1QRD - Query sui Movimenti di Analitica Documenti - Effettivi per Competenza
    * --- NGSCAOQRD - Query Movimenti di storno Righe omaggio ripartite - Effettivi per Competenza
    * --- w_GENERA: se .t. trovato almeno un record da ripartite
    this.w_GENERA = .T.
    * --- Leggere esercizio del periodo considerato
    this.w_CODAZI = i_CODAZI
    this.w_VALNAZ = g_PERVAL
    this.w_DECTOT = g_PERPVL
    this.w_ESEPRE = CALCESER(this.oParentObject.w_DATINI, g_CODESE)
    if NOT EMPTY(this.w_ESEPRE) AND this.w_ESEPRE<>g_CODESE
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESVALNAZ"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.w_ESEPRE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESVALNAZ;
          from (i_cTable) where;
              ESCODAZI = this.w_CODAZI;
              and ESCODESE = this.w_ESEPRE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_VALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Lettura decimali Valuta eserizio (per arrotondamenti in CREATMP2)
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_VALNAZ);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT;
          from (i_cTable) where;
              VACODVAL = this.w_VALNAZ;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Controllo se esistono gi� dei movimenti di ripartizione con lo stesso periodo
    * --- Create temporary table RIPATMP3
    i_nIdx=cp_AddTableDef('RIPATMP3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\CREATMP3',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if this.oParentObject.w_SUCOMMES="S"
      * --- Controllo se esistono gi� dei movimenti di ripartizione con lo stesso periodo
      * --- Create temporary table TMP_ANA
      i_nIdx=cp_AddTableDef('TMP_ANA') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\TMP_ANA',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_ANA_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    ah_Msg("Lettura movimenti da ripartire...",.T.)
    if this.oParentObject.w_PERCOMPE="S"
      if this.oParentObject.w_TIPMOV="E"
        * --- Query sui Movimenti Effettivi (EX MOVRIP)
        * --- Create temporary table RIPATMP2
        i_nIdx=cp_AddTableDef('RIPATMP2') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\NGSCA2QRM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.RIPATMP2_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Query sui Movimenti Previsionali (EX MOVRIP)
        * --- Create temporary table RIPATMP2
        i_nIdx=cp_AddTableDef('RIPATMP2') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\NGSCA3QRM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.RIPATMP2_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      * --- Modifico gli importi in base al periodo di competenza
      ah_Msg("Rivalorizzazione per competenza...",.T.)
      * --- Write into RIPATMP2
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.RIPATMP2_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP2_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SERRIF,ORDRIF,NUMRIF"
        do vq_exec with 'NGSCA7QRM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP2_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="RIPATMP2.SERRIF = _t2.SERRIF";
                +" and "+"RIPATMP2.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPATMP2.NUMRIF = _t2.NUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TOTIMP = _t2.TOTIMP";
            +",INICOM = _t2.INICOM";
            +",FINCOM = _t2.FINCOM";
            +i_ccchkf;
            +" from "+i_cTable+" RIPATMP2, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="RIPATMP2.SERRIF = _t2.SERRIF";
                +" and "+"RIPATMP2.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPATMP2.NUMRIF = _t2.NUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP2, "+i_cQueryTable+" _t2 set ";
            +"RIPATMP2.TOTIMP = _t2.TOTIMP";
            +",RIPATMP2.INICOM = _t2.INICOM";
            +",RIPATMP2.FINCOM = _t2.FINCOM";
            +Iif(Empty(i_ccchkf),"",",RIPATMP2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="RIPATMP2.SERRIF = t2.SERRIF";
                +" and "+"RIPATMP2.ORDRIF = t2.ORDRIF";
                +" and "+"RIPATMP2.NUMRIF = t2.NUMRIF";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP2 set (";
            +"TOTIMP,";
            +"INICOM,";
            +"FINCOM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.TOTIMP,";
            +"t2.INICOM,";
            +"t2.FINCOM";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="RIPATMP2.SERRIF = _t2.SERRIF";
                +" and "+"RIPATMP2.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPATMP2.NUMRIF = _t2.NUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP2 set ";
            +"TOTIMP = _t2.TOTIMP";
            +",INICOM = _t2.INICOM";
            +",FINCOM = _t2.FINCOM";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SERRIF = "+i_cQueryTable+".SERRIF";
                +" and "+i_cTable+".ORDRIF = "+i_cQueryTable+".ORDRIF";
                +" and "+i_cTable+".NUMRIF = "+i_cQueryTable+".NUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TOTIMP = (select TOTIMP from "+i_cQueryTable+" where "+i_cWhere+")";
            +",INICOM = (select INICOM from "+i_cQueryTable+" where "+i_cWhere+")";
            +",FINCOM = (select FINCOM from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Verifico problemi su arrotondamenti
      * --- Write into RIPATMP2
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.RIPATMP2_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP2_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SERRIF,ORDRIF,NUMRIF"
        do vq_exec with 'NGSCA9QRM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPATMP2_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="RIPATMP2.SERRIF = _t2.SERRIF";
                +" and "+"RIPATMP2.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPATMP2.NUMRIF = _t2.NUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TOTIMP = RIPATMP2.TOTIMP+_t2.TOTALE-_t2.RIPART";
            +i_ccchkf;
            +" from "+i_cTable+" RIPATMP2, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="RIPATMP2.SERRIF = _t2.SERRIF";
                +" and "+"RIPATMP2.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPATMP2.NUMRIF = _t2.NUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP2, "+i_cQueryTable+" _t2 set ";
            +"RIPATMP2.TOTIMP = RIPATMP2.TOTIMP+_t2.TOTALE-_t2.RIPART";
            +Iif(Empty(i_ccchkf),"",",RIPATMP2.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="RIPATMP2.SERRIF = t2.SERRIF";
                +" and "+"RIPATMP2.ORDRIF = t2.ORDRIF";
                +" and "+"RIPATMP2.NUMRIF = t2.NUMRIF";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP2 set (";
            +"TOTIMP";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"TOTIMP+t2.TOTALE-t2.RIPART";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="RIPATMP2.SERRIF = _t2.SERRIF";
                +" and "+"RIPATMP2.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPATMP2.NUMRIF = _t2.NUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPATMP2 set ";
            +"TOTIMP = RIPATMP2.TOTIMP+_t2.TOTALE-_t2.RIPART";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SERRIF = "+i_cQueryTable+".SERRIF";
                +" and "+i_cTable+".ORDRIF = "+i_cQueryTable+".ORDRIF";
                +" and "+i_cTable+".NUMRIF = "+i_cQueryTable+".NUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TOTIMP = (select "+i_cTable+".TOTIMP+TOTALE-RIPART from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      if this.oParentObject.w_TIPMOV="E"
        * --- Query sui Movimenti Effettivi (EX MOVRIP)
        * --- Create temporary table RIPATMP2
        i_nIdx=cp_AddTableDef('RIPATMP2') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSCA_QRM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.RIPATMP2_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Query sui Movimenti Previsionali
        * --- Create temporary table RIPATMP2
        i_nIdx=cp_AddTableDef('RIPATMP2') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSCA1QRM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.RIPATMP2_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    endif
    * --- Create temporary table RIPABIDONE
    i_nIdx=cp_AddTableDef('RIPABIDONE') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSCA_SEG',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPABIDONE_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    WAIT CLEAR
    * --- Scansione Livelli (dal Piu' alto al piu' basso)
    vq_exec("query\GSCA_LIV.VQR",this,"LIVELLI")
    * --- La query ritorna i livelli per cui � necessario ripartire (ad eccezione del livello 1, per cui � possibile avere una ripartizione su commessa ma non per C.C./R.)
    this.w_NUMERO = 1
    if USED("LIVELLI")
      SCAN
      * --- w_NLIV � usato come parametro nelle query
      this.w_NLIV = NVL(CCNUMLIV,0)
      if this.oParentObject.w_SUCOMMES="S"
        * --- Ripartizione su commessa per i c./c.r. di livello maggiore di 1
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Selezione da Ripabidone dei record da ripartire
      * --- Create temporary table RIPATMP1
      i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.RIPABIDONE_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            +" where NUMLIV = "+cp_ToStrODBC(this.w_NLIV)+" AND FLRIPA<>'S'";
            )
      this.RIPATMP1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Calcola le ripartizioni
      * --- Controllo la versione di sql.
      *     Se � uguale o maggiore di sql 2005 viene eseguito il nuovo codice velocizzato.
      *     In caso contrario viene lanciato il vecchio codice.
      *     Se w_DBVERSION>=9 siamo in presenza di sql 2005 o superiore.
      this.w_OLDREC = "ZZZZZZZZZZ"
      this.w_TOTREC = 0
      this.w_TOTRIP = 0
      if ((val(this.w_DBVersion)>=9 AND Upper(CP_DBTYPE)="SQLSERVER") OR Upper(CP_DBTYPE)="DB2" OR Upper(CP_DBTYPE)="ORACLE" OR Upper(CP_DBTYPE)="POSTGRESQL") 
        * --- Insert into RIPABIDONE
        i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\CREATMP2",this.RIPABIDONE_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Drop temporary table RIPATMP1
        i_nIdx=cp_GetTableDefIdx('RIPATMP1')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('RIPATMP1')
        endif
        * --- Write into RIPABIDONE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="CODICE,CODSUP,TIPREG,SERRIF,ORDRIF,NUMRIF,UNIVOC,ORILIV,IMPSUP,CODVOC,SEGNO,NUMERO,NUMSUP,NUMPROG"
          do vq_exec with 'CREATMP4',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPABIDONE_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
                  +" and "+"RIPABIDONE.CODSUP = _t2.CODSUP";
                  +" and "+"RIPABIDONE.TIPREG = _t2.TIPREG";
                  +" and "+"RIPABIDONE.SERRIF = _t2.SERRIF";
                  +" and "+"RIPABIDONE.ORDRIF = _t2.ORDRIF";
                  +" and "+"RIPABIDONE.NUMRIF = _t2.NUMRIF";
                  +" and "+"RIPABIDONE.UNIVOC = _t2.UNIVOC";
                  +" and "+"RIPABIDONE.ORILIV = _t2.ORILIV";
                  +" and "+"RIPABIDONE.IMPSUP = _t2.IMPSUP";
                  +" and "+"RIPABIDONE.CODVOC = _t2.CODVOC";
                  +" and "+"RIPABIDONE.SEGNO = _t2.SEGNO";
                  +" and "+"RIPABIDONE.NUMERO = _t2.NUMERO";
                  +" and "+"RIPABIDONE.NUMSUP = _t2.NUMSUP";
                  +" and "+"RIPABIDONE.NUMPROG = _t2.NUMPROG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TOTIMP = RIPABIDONE.TOTIMP+_t2.DIFRIP";
              +i_ccchkf;
              +" from "+i_cTable+" RIPABIDONE, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
                  +" and "+"RIPABIDONE.CODSUP = _t2.CODSUP";
                  +" and "+"RIPABIDONE.TIPREG = _t2.TIPREG";
                  +" and "+"RIPABIDONE.SERRIF = _t2.SERRIF";
                  +" and "+"RIPABIDONE.ORDRIF = _t2.ORDRIF";
                  +" and "+"RIPABIDONE.NUMRIF = _t2.NUMRIF";
                  +" and "+"RIPABIDONE.UNIVOC = _t2.UNIVOC";
                  +" and "+"RIPABIDONE.ORILIV = _t2.ORILIV";
                  +" and "+"RIPABIDONE.IMPSUP = _t2.IMPSUP";
                  +" and "+"RIPABIDONE.CODVOC = _t2.CODVOC";
                  +" and "+"RIPABIDONE.SEGNO = _t2.SEGNO";
                  +" and "+"RIPABIDONE.NUMERO = _t2.NUMERO";
                  +" and "+"RIPABIDONE.NUMSUP = _t2.NUMSUP";
                  +" and "+"RIPABIDONE.NUMPROG = _t2.NUMPROG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE, "+i_cQueryTable+" _t2 set ";
              +"RIPABIDONE.TOTIMP = RIPABIDONE.TOTIMP+_t2.DIFRIP";
              +Iif(Empty(i_ccchkf),"",",RIPABIDONE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="RIPABIDONE.CODICE = t2.CODICE";
                  +" and "+"RIPABIDONE.CODSUP = t2.CODSUP";
                  +" and "+"RIPABIDONE.TIPREG = t2.TIPREG";
                  +" and "+"RIPABIDONE.SERRIF = t2.SERRIF";
                  +" and "+"RIPABIDONE.ORDRIF = t2.ORDRIF";
                  +" and "+"RIPABIDONE.NUMRIF = t2.NUMRIF";
                  +" and "+"RIPABIDONE.UNIVOC = t2.UNIVOC";
                  +" and "+"RIPABIDONE.ORILIV = t2.ORILIV";
                  +" and "+"RIPABIDONE.IMPSUP = t2.IMPSUP";
                  +" and "+"RIPABIDONE.CODVOC = t2.CODVOC";
                  +" and "+"RIPABIDONE.SEGNO = t2.SEGNO";
                  +" and "+"RIPABIDONE.NUMERO = t2.NUMERO";
                  +" and "+"RIPABIDONE.NUMSUP = t2.NUMSUP";
                  +" and "+"RIPABIDONE.NUMPROG = t2.NUMPROG";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE set (";
              +"TOTIMP";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"TOTIMP+t2.DIFRIP";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
                  +" and "+"RIPABIDONE.CODSUP = _t2.CODSUP";
                  +" and "+"RIPABIDONE.TIPREG = _t2.TIPREG";
                  +" and "+"RIPABIDONE.SERRIF = _t2.SERRIF";
                  +" and "+"RIPABIDONE.ORDRIF = _t2.ORDRIF";
                  +" and "+"RIPABIDONE.NUMRIF = _t2.NUMRIF";
                  +" and "+"RIPABIDONE.UNIVOC = _t2.UNIVOC";
                  +" and "+"RIPABIDONE.ORILIV = _t2.ORILIV";
                  +" and "+"RIPABIDONE.IMPSUP = _t2.IMPSUP";
                  +" and "+"RIPABIDONE.CODVOC = _t2.CODVOC";
                  +" and "+"RIPABIDONE.SEGNO = _t2.SEGNO";
                  +" and "+"RIPABIDONE.NUMERO = _t2.NUMERO";
                  +" and "+"RIPABIDONE.NUMSUP = _t2.NUMSUP";
                  +" and "+"RIPABIDONE.NUMPROG = _t2.NUMPROG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE set ";
              +"TOTIMP = RIPABIDONE.TOTIMP+_t2.DIFRIP";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".CODICE = "+i_cQueryTable+".CODICE";
                  +" and "+i_cTable+".CODSUP = "+i_cQueryTable+".CODSUP";
                  +" and "+i_cTable+".TIPREG = "+i_cQueryTable+".TIPREG";
                  +" and "+i_cTable+".SERRIF = "+i_cQueryTable+".SERRIF";
                  +" and "+i_cTable+".ORDRIF = "+i_cQueryTable+".ORDRIF";
                  +" and "+i_cTable+".NUMRIF = "+i_cQueryTable+".NUMRIF";
                  +" and "+i_cTable+".UNIVOC = "+i_cQueryTable+".UNIVOC";
                  +" and "+i_cTable+".ORILIV = "+i_cQueryTable+".ORILIV";
                  +" and "+i_cTable+".IMPSUP = "+i_cQueryTable+".IMPSUP";
                  +" and "+i_cTable+".CODVOC = "+i_cQueryTable+".CODVOC";
                  +" and "+i_cTable+".SEGNO = "+i_cQueryTable+".SEGNO";
                  +" and "+i_cTable+".NUMERO = "+i_cQueryTable+".NUMERO";
                  +" and "+i_cTable+".NUMSUP = "+i_cQueryTable+".NUMSUP";
                  +" and "+i_cTable+".NUMPROG = "+i_cQueryTable+".NUMPROG";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TOTIMP = (select "+i_cTable+".TOTIMP+DIFRIP from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_NUMERO = this.w_NUMERO+1
      else
        * --- Create temporary table RIPATMP2
        i_nIdx=cp_AddTableDef('RIPATMP2') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\CREATMP2_VM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.RIPATMP2_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Drop temporary table RIPATMP1
        i_nIdx=cp_GetTableDefIdx('RIPATMP1')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('RIPATMP1')
        endif
        * --- Select from RIPATMP2
        i_nConn=i_TableProp[this.RIPATMP2_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP2_idx,2],.t.,this.RIPATMP2_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIPATMP2 ";
              +" order by CODSUP,TIPREG,SERRIF,ORDRIF,NUMRIF,UNIVOC,ORILIV,IMPSUP,SEGNO";
               ,"_Curs_RIPATMP2")
        else
          select * from (i_cTable);
           order by CODSUP,TIPREG,SERRIF,ORDRIF,NUMRIF,UNIVOC,ORILIV,IMPSUP,SEGNO;
            into cursor _Curs_RIPATMP2
        endif
        if used('_Curs_RIPATMP2')
          select _Curs_RIPATMP2
          locate for 1=1
          do while not(eof())
          this.w_TOTREC = this.w_TOTREC + 1
          this.w_NEWREC = _Curs_RIPATMP2.CODSUP + _Curs_RIPATMP2.TIPREG
          this.w_NEWREC = this.w_NEWREC + NVL(_Curs_RIPATMP2.SERRIF, SPACE(10)) + STR(NVL(_Curs_RIPATMP2.ORDRIF,0),6,0) + STR(NVL(_Curs_RIPATMP2.NUMRIF,0), 6,0)
          this.w_NEWREC = this.w_NEWREC + STR(_Curs_RIPATMP2.UNIVOC,6,0) + STR(_Curs_RIPATMP2.ORILIV,6,0) + STR(_Curs_RIPATMP2.IMPSUP,18,4) + Nvl(_Curs_RIPATMP2.CODVOC,Space(15))+Nvl(SEGNO," ")
          if this.w_OLDREC<>this.w_NEWREC
            if this.w_TOTRIP<>0
              * --- Write into RIPABIDONE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPABIDONE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"TOTIMP =TOTIMP+ "+cp_ToStrODBC(this.w_TOTRIP);
                    +i_ccchkf ;
                +" where ";
                    +"CODICE = "+cp_ToStrODBC(this.w_CODICE);
                    +" and CODSUP = "+cp_ToStrODBC(this.w_CODSUP);
                    +" and TIPREG = "+cp_ToStrODBC(this.w_TIPREG);
                    +" and SERRIF = "+cp_ToStrODBC(this.w_SERRIF);
                    +" and ORDRIF = "+cp_ToStrODBC(this.w_ORDRIF);
                    +" and NUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                    +" and UNIVOC = "+cp_ToStrODBC(this.w_UNIVOC);
                    +" and ORILIV = "+cp_ToStrODBC(this.w_ORILIV);
                    +" and CODVOC = "+cp_ToStrODBC(this.w_CODVOC);
                       )
              else
                update (i_cTable) set;
                    TOTIMP = TOTIMP + this.w_TOTRIP;
                    &i_ccchkf. ;
                 where;
                    CODICE = this.w_CODICE;
                    and CODSUP = this.w_CODSUP;
                    and TIPREG = this.w_TIPREG;
                    and SERRIF = this.w_SERRIF;
                    and ORDRIF = this.w_ORDRIF;
                    and NUMRIF = this.w_NUMRIF;
                    and UNIVOC = this.w_UNIVOC;
                    and ORILIV = this.w_ORILIV;
                    and CODVOC = this.w_CODVOC;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            this.w_TOTRIP = IMPSUP
          endif
          this.w_NUMLIV = NUMLIV
          this.w_NUMRIG = NUMRIG
          this.w_DATREG = DATREG
          this.w_CODVOC = CODVOC
          this.w_CODICE = CODICE
          this.w_VALNAZ = VALNAZ
          this.w_SEGNO = SEGNO
          this.w_TIPREG = TIPREG
          this.w_CODESE = CODESE
          this.w_INICOM = INICOM
          this.w_FINCOM = FINCOM
          this.w_FLRIPA = FLRIPA
          this.w_SERRIF = SERRIF
          this.w_ORDRIF = ORDRIF
          this.w_NUMRIF = NUMRIF
          this.w_FLDIRE = FLDIRE
          this.w_CODCOM = CODCOM
          this.w_CODSUP = CODSUP
          this.w_TOTIMP = TOTIMP
          this.w_IMPSUP = IMPSUP
          this.w_ORILIV = this.w_NLIV
          this.w_UNIVOC = this.w_TOTREC
          this.w_TOTRIP = this.w_TOTRIP - this.w_TOTIMP
          * --- Insert into RIPABIDONE
          i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RIPABIDONE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"UNIVOC"+",ORILIV"+",NUMRIG"+",DATREG"+",CODVOC"+",VALNAZ"+",CODICE"+",NUMLIV"+",SEGNO"+",CODESE"+",TIPREG"+",FINCOM"+",INICOM"+",SERRIF"+",FLRIPA"+",NUMRIF"+",ORDRIF"+",TOTIMP"+",CODCOM"+",FLDIRE"+",IMPSUP"+",CODSUP"+",NUMERO"+",NUMSUP"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_UNIVOC),'RIPABIDONE','UNIVOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ORILIV),'RIPABIDONE','ORILIV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'RIPABIDONE','NUMRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'RIPABIDONE','DATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODVOC),'RIPABIDONE','CODVOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_VALNAZ),'RIPABIDONE','VALNAZ');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'RIPABIDONE','CODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMLIV),'RIPABIDONE','NUMLIV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SEGNO),'RIPABIDONE','SEGNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODESE),'RIPABIDONE','CODESE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREG),'RIPABIDONE','TIPREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FINCOM),'RIPABIDONE','FINCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_INICOM),'RIPABIDONE','INICOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SERRIF),'RIPABIDONE','SERRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FLRIPA),'RIPABIDONE','FLRIPA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIF),'RIPABIDONE','NUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ORDRIF),'RIPABIDONE','ORDRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TOTIMP),'RIPABIDONE','TOTIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'RIPABIDONE','CODCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FLDIRE),'RIPABIDONE','FLDIRE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSUP),'RIPABIDONE','IMPSUP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODSUP),'RIPABIDONE','CODSUP');
            +","+cp_NullLink(cp_ToStrODBC(0),'RIPABIDONE','NUMERO');
            +","+cp_NullLink(cp_ToStrODBC(0),'RIPABIDONE','NUMSUP');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'UNIVOC',this.w_UNIVOC,'ORILIV',this.w_ORILIV,'NUMRIG',this.w_NUMRIG,'DATREG',this.w_DATREG,'CODVOC',this.w_CODVOC,'VALNAZ',this.w_VALNAZ,'CODICE',this.w_CODICE,'NUMLIV',this.w_NUMLIV,'SEGNO',this.w_SEGNO,'CODESE',this.w_CODESE,'TIPREG',this.w_TIPREG,'FINCOM',this.w_FINCOM)
            insert into (i_cTable) (UNIVOC,ORILIV,NUMRIG,DATREG,CODVOC,VALNAZ,CODICE,NUMLIV,SEGNO,CODESE,TIPREG,FINCOM,INICOM,SERRIF,FLRIPA,NUMRIF,ORDRIF,TOTIMP,CODCOM,FLDIRE,IMPSUP,CODSUP,NUMERO,NUMSUP &i_ccchkf. );
               values (;
                 this.w_UNIVOC;
                 ,this.w_ORILIV;
                 ,this.w_NUMRIG;
                 ,this.w_DATREG;
                 ,this.w_CODVOC;
                 ,this.w_VALNAZ;
                 ,this.w_CODICE;
                 ,this.w_NUMLIV;
                 ,this.w_SEGNO;
                 ,this.w_CODESE;
                 ,this.w_TIPREG;
                 ,this.w_FINCOM;
                 ,this.w_INICOM;
                 ,this.w_SERRIF;
                 ,this.w_FLRIPA;
                 ,this.w_NUMRIF;
                 ,this.w_ORDRIF;
                 ,this.w_TOTIMP;
                 ,this.w_CODCOM;
                 ,this.w_FLDIRE;
                 ,this.w_IMPSUP;
                 ,this.w_CODSUP;
                 ,0;
                 ,0;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Errore in inserimento tabella temporanea ripartizioni'
            return
          endif
          this.w_OLDREC = this.w_NEWREC
          this.w_GENERA = .T.
            select _Curs_RIPATMP2
            continue
          enddo
          use
        endif
        * --- Resto
        if this.w_TOTRIP<>0
          * --- Write into RIPABIDONE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPABIDONE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TOTIMP =TOTIMP+ "+cp_ToStrODBC(this.w_TOTRIP);
                +i_ccchkf ;
            +" where ";
                +"CODICE = "+cp_ToStrODBC(this.w_CODICE);
                +" and CODSUP = "+cp_ToStrODBC(this.w_CODSUP);
                +" and TIPREG = "+cp_ToStrODBC(this.w_TIPREG);
                +" and SERRIF = "+cp_ToStrODBC(this.w_SERRIF);
                +" and ORDRIF = "+cp_ToStrODBC(this.w_ORDRIF);
                +" and NUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                +" and UNIVOC = "+cp_ToStrODBC(this.w_UNIVOC);
                +" and ORILIV = "+cp_ToStrODBC(this.w_ORILIV);
                +" and CODVOC = "+cp_ToStrODBC(this.w_CODVOC);
                   )
          else
            update (i_cTable) set;
                TOTIMP = TOTIMP + this.w_TOTRIP;
                &i_ccchkf. ;
             where;
                CODICE = this.w_CODICE;
                and CODSUP = this.w_CODSUP;
                and TIPREG = this.w_TIPREG;
                and SERRIF = this.w_SERRIF;
                and ORDRIF = this.w_ORDRIF;
                and NUMRIF = this.w_NUMRIF;
                and UNIVOC = this.w_UNIVOC;
                and ORILIV = this.w_ORILIV;
                and CODVOC = this.w_CODVOC;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      ah_Msg("Esplosione nei sottolivelli di ripartizione livello: %1...",.T.,.F.,.F., STR(this.w_NLIV) )
      SELECT LIVELLI
      ENDSCAN
    endif
    this.w_NLIV = 1
    if this.oParentObject.w_SUCOMMES="S"
      * --- Ripartizione su commessa per i c./c.r. di livello 1
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Drop temporary table TMP_ANA
      i_nIdx=cp_GetTableDefIdx('TMP_ANA')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_ANA')
      endif
    endif
    if USED("LIVELLI")
      * --- Elimina Cursore di Appoggio se esiste ancora
      SELECT LIVELLI
      USE
    endif
    * --- Write into RIPABIDONE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.RIPATMP3_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPABIDONE_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA');
          +i_ccchkf;
          +" from "+i_cTable+" RIPABIDONE, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE, "+i_cQueryTable+" _t2 set ";
      +"RIPABIDONE.FLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA');
          +Iif(Empty(i_ccchkf),"",",RIPABIDONE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE set ";
      +"FLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".CODICE = "+i_cQueryTable+".CODICE";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Drop temporary table RIPATMP1
    i_nIdx=cp_GetTableDefIdx('RIPATMP1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP1')
    endif
    * --- Drop temporary table RIPATMP2
    i_nIdx=cp_GetTableDefIdx('RIPATMP2')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP2')
    endif
    this.w_OK = .T.
    if this.w_GENERA=.F.
      this.w_MESS = "Per il periodo selezionato non esistono movimenti da ripartire%0Elimino comunque i movimenti da ripartizione generati in precedenza?"
      this.w_OK = ah_YesNo(this.w_MESS)
    endif
    if this.w_OK=.T.
      * --- Try
      local bErr_0498EDC0
      bErr_0498EDC0=bTrsErr
      this.Try_0498EDC0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Elaborazione annullata",,"")
      endif
      bTrsErr=bTrsErr or bErr_0498EDC0
      * --- End
    endif
    * --- Drop temporary table RIPABIDONE
    i_nIdx=cp_GetTableDefIdx('RIPABIDONE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPABIDONE')
    endif
    * --- Drop temporary table RIPATMP3
    i_nIdx=cp_GetTableDefIdx('RIPATMP3')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP3')
    endif
    * --- Drop temporary table RIPATMP4
    i_nIdx=cp_GetTableDefIdx('RIPATMP4')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('RIPATMP4')
    endif
  endproc
  proc Try_0498EDC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Elaborazione completata",,"")
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Archivi
    * --- Cancella Eventuali Movimenti da Ripartire precedentemente calcolati in base al periodo impostato
    ah_Msg("Elimino movimenti precedentemente inseriti...",.T.)
    if this.oParentObject.w_PERCOMPE="S"
      * --- Cancello per date competenza
      * --- Delete from MOVIRIPA
      i_nConn=i_TableProp[this.MOVIRIPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".MRPERIOD = "+i_cQueryTable+".MRPERIOD";
              +" and "+i_cTable+".MRTIPREG = "+i_cQueryTable+".MRTIPREG";
              +" and "+i_cTable+".MRFLGMOV = "+i_cQueryTable+".MRFLGMOV";
              +" and "+i_cTable+".MRCODICE = "+i_cQueryTable+".MRCODICE";
              +" and "+i_cTable+".MRSERRIF = "+i_cQueryTable+".MRSERRIF";
              +" and "+i_cTable+".MRORDRIF = "+i_cQueryTable+".MRORDRIF";
              +" and "+i_cTable+".MRNUMRIF = "+i_cQueryTable+".MRNUMRIF";
      
        do vq_exec with 'query\NGSCA_BRI',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in Cancellazione MOVIRIPA'
        return
      endif
    else
      * --- Cancello per data registrazione
      * --- Delete from MOVIRIPA
      i_nConn=i_TableProp[this.MOVIRIPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MRDATREG >= "+cp_ToStrODBC(this.oParentObject.w_DATINI);
              +" and MRDATREG <= "+cp_ToStrODBC(this.oParentObject.w_DATFIN);
              +" and MRFLGMOV = "+cp_ToStrODBC(this.oParentObject.w_TIPMOV);
               )
      else
        delete from (i_cTable) where;
              MRDATREG >= this.oParentObject.w_DATINI;
              and MRDATREG <= this.oParentObject.w_DATFIN;
              and MRFLGMOV = this.oParentObject.w_TIPMOV;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error='Errore in Cancellazione MOVIRIPA'
        return
      endif
    endif
    * --- Try
    local bErr_0476D6B0
    bErr_0476D6B0=bTrsErr
    this.Try_0476D6B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0476D6B0
    * --- End
    WAIT CLEAR
  endproc
  proc Try_0476D6B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Marcatura dei movimenti ripartiti
    this.w_FLRIPA = " "
    ah_Msg("Elimino ripartiti da documenti...",.T.)
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
      do vq_exec with 'QUERY\RIPADOCU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
      +"DOC_DETT.MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
          +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
          +"MVFLRIPA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
              +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
              +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
      +"MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
              +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore in Scrittura DOC_DETT (1)'
      return
    endif
    ah_Msg("Elimino ripartiti da movimenti contabili...",.T.)
    * --- Write into MOVICOST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOVICOST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MRSERIAL,MRROWORD,CPROWNUM"
      do vq_exec with 'QUERY\RIPAMOVI',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVICOST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MOVICOST.MRSERIAL = _t2.MRSERIAL";
              +" and "+"MOVICOST.MRROWORD = _t2.MRROWORD";
              +" and "+"MOVICOST.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'MOVICOST','MRFLRIPA');
          +i_ccchkf;
          +" from "+i_cTable+" MOVICOST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MOVICOST.MRSERIAL = _t2.MRSERIAL";
              +" and "+"MOVICOST.MRROWORD = _t2.MRROWORD";
              +" and "+"MOVICOST.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVICOST, "+i_cQueryTable+" _t2 set ";
      +"MOVICOST.MRFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'MOVICOST','MRFLRIPA');
          +Iif(Empty(i_ccchkf),"",",MOVICOST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MOVICOST.MRSERIAL = t2.MRSERIAL";
              +" and "+"MOVICOST.MRROWORD = t2.MRROWORD";
              +" and "+"MOVICOST.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVICOST set (";
          +"MRFLRIPA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(" "),'MOVICOST','MRFLRIPA')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MOVICOST.MRSERIAL = _t2.MRSERIAL";
              +" and "+"MOVICOST.MRROWORD = _t2.MRROWORD";
              +" and "+"MOVICOST.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVICOST set ";
      +"MRFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'MOVICOST','MRFLRIPA');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
              +" and "+i_cTable+".MRROWORD = "+i_cQueryTable+".MRROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'MOVICOST','MRFLRIPA');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errorre in Scrittura MOVICOST (1)'
      return
    endif
    if this.w_GENERA=.T.
      this.w_FLRIPA = "S"
      ah_Msg("Aggiorno ripartiti da documenti...",.T.)
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
        do vq_exec with 'QUERY\RIPADOCU',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLRIPA');
            +i_ccchkf;
            +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
        +"DOC_DETT.MVFLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLRIPA');
            +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
            +"MVFLRIPA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLRIPA')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
        +"MVFLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLRIPA');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLRIPA');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore in Scrittura DOC_DETT (2)'
        return
      endif
      ah_Msg("Aggiorno ripartiti da movimenti contabili...",.T.)
      * --- Write into MOVICOST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOVICOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MRSERIAL,MRROWORD,CPROWNUM"
        do vq_exec with 'QUERY\RIPAMOVI',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVICOST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="MOVICOST.MRSERIAL = _t2.MRSERIAL";
                +" and "+"MOVICOST.MRROWORD = _t2.MRROWORD";
                +" and "+"MOVICOST.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRFLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'MOVICOST','MRFLRIPA');
            +i_ccchkf;
            +" from "+i_cTable+" MOVICOST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="MOVICOST.MRSERIAL = _t2.MRSERIAL";
                +" and "+"MOVICOST.MRROWORD = _t2.MRROWORD";
                +" and "+"MOVICOST.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVICOST, "+i_cQueryTable+" _t2 set ";
        +"MOVICOST.MRFLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'MOVICOST','MRFLRIPA');
            +Iif(Empty(i_ccchkf),"",",MOVICOST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="MOVICOST.MRSERIAL = t2.MRSERIAL";
                +" and "+"MOVICOST.MRROWORD = t2.MRROWORD";
                +" and "+"MOVICOST.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVICOST set (";
            +"MRFLRIPA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'MOVICOST','MRFLRIPA')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="MOVICOST.MRSERIAL = _t2.MRSERIAL";
                +" and "+"MOVICOST.MRROWORD = _t2.MRROWORD";
                +" and "+"MOVICOST.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVICOST set ";
        +"MRFLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'MOVICOST','MRFLRIPA');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MRSERIAL = "+i_cQueryTable+".MRSERIAL";
                +" and "+i_cTable+".MRROWORD = "+i_cQueryTable+".MRROWORD";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRFLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'MOVICOST','MRFLRIPA');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errorre in Scrittura MOVICOST (2)'
        return
      endif
      * --- Scrive Riga Movimenti Ripartiti
      ah_Msg("Inserimento movimenti ripartiti...",.T.)
      * --- Insert into MOVIRIPA
      i_nConn=i_TableProp[this.MOVIRIPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\RIPARIPA",this.MOVIRIPA_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore in Inserimento MOVIRIPA'
        return
      endif
      if this.oParentObject.w_SUCOMMES="S"
        * --- Inserisce gli storni dei movimenti senza commessa
        * --- Insert into MOVIRIPA
        i_nConn=i_TableProp[this.MOVIRIPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVIRIPA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\COMMTMP5",this.MOVIRIPA_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore in Inserimento MOVIRIPA'
          return
        endif
      endif
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ripartizione su Commessa
    * --- Selezione da Ripabidone dei record senza Commessa 
    * --- Create temporary table RIPATMP1
    i_nIdx=cp_AddTableDef('RIPATMP1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\COMMTMP1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Ripartizione su Commessa
    * --- Scrive Storno movimenti senza commessa
    if this.w_CREATT="S"
      * --- La prima volta crea la tabella temporanea
      * --- Create temporary table RIPATMP4
      i_nIdx=cp_AddTableDef('RIPATMP4') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\COMMTMP4',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.RIPATMP4_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      this.w_CREATT = "N"
    else
      * --- Le volte successive aggiunge i nuovi storni
      * --- Insert into RIPATMP4
      i_nConn=i_TableProp[this.RIPATMP4_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP4_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\COMMTMP4",this.RIPATMP4_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore in Inserimento MOVIRIPA'
        return
      endif
    endif
    * --- Calcola le ripartizioni
    this.w_OLDREC = "ZZZZZZZZZZ"
    this.w_TOTREC = 0
    this.w_TOTRIP = 0
    * --- Create temporary table RIPATMP2
    i_nIdx=cp_AddTableDef('RIPATMP2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\COMMTMP2_VM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.RIPATMP2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if ((val(this.w_DBVersion)>=9 AND Upper(CP_DBTYPE)="SQLSERVER") OR Upper(CP_DBTYPE)="DB2" OR Upper(CP_DBTYPE)="ORACLE" OR Upper(CP_DBTYPE)="POSTGRESQL" ) 
      this.w_NUMERO = this.w_NUMERO+1
      * --- Scrittura in ripabidone del record privo di Commessa per marcare il fatto che � stato ripartito
      * --- Write into RIPABIDONE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CODICE,TIPREG,SERRIF,ORDRIF,NUMRIF,SEGNO"
        do vq_exec with 'query\COMMTMP3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPABIDONE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
                +" and "+"RIPABIDONE.TIPREG = _t2.TIPREG";
                +" and "+"RIPABIDONE.SERRIF = _t2.SERRIF";
                +" and "+"RIPABIDONE.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPABIDONE.NUMRIF = _t2.NUMRIF";
                +" and "+"RIPABIDONE.SEGNO = _t2.SEGNO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA');
            +i_ccchkf;
            +" from "+i_cTable+" RIPABIDONE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
                +" and "+"RIPABIDONE.TIPREG = _t2.TIPREG";
                +" and "+"RIPABIDONE.SERRIF = _t2.SERRIF";
                +" and "+"RIPABIDONE.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPABIDONE.NUMRIF = _t2.NUMRIF";
                +" and "+"RIPABIDONE.SEGNO = _t2.SEGNO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE, "+i_cQueryTable+" _t2 set ";
        +"RIPABIDONE.FLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA');
            +Iif(Empty(i_ccchkf),"",",RIPABIDONE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="RIPABIDONE.CODICE = t2.CODICE";
                +" and "+"RIPABIDONE.TIPREG = t2.TIPREG";
                +" and "+"RIPABIDONE.SERRIF = t2.SERRIF";
                +" and "+"RIPABIDONE.ORDRIF = t2.ORDRIF";
                +" and "+"RIPABIDONE.NUMRIF = t2.NUMRIF";
                +" and "+"RIPABIDONE.SEGNO = t2.SEGNO";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE set (";
            +"FLRIPA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
                +" and "+"RIPABIDONE.TIPREG = _t2.TIPREG";
                +" and "+"RIPABIDONE.SERRIF = _t2.SERRIF";
                +" and "+"RIPABIDONE.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPABIDONE.NUMRIF = _t2.NUMRIF";
                +" and "+"RIPABIDONE.SEGNO = _t2.SEGNO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE set ";
        +"FLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CODICE = "+i_cQueryTable+".CODICE";
                +" and "+i_cTable+".TIPREG = "+i_cQueryTable+".TIPREG";
                +" and "+i_cTable+".SERRIF = "+i_cQueryTable+".SERRIF";
                +" and "+i_cTable+".ORDRIF = "+i_cQueryTable+".ORDRIF";
                +" and "+i_cTable+".NUMRIF = "+i_cQueryTable+".NUMRIF";
                +" and "+i_cTable+".SEGNO = "+i_cQueryTable+".SEGNO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Insert into RIPABIDONE
      i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\COMMTMP2",this.RIPABIDONE_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Drop temporary table RIPATMP1
      i_nIdx=cp_GetTableDefIdx('RIPATMP1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP1')
      endif
      * --- Write into RIPABIDONE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CODICE,CODSUP,TIPREG,SERRIF,ORDRIF,NUMRIF,IMPSUP,CODVOC,SEGNO,NUMERO,NUMSUP,NUMPROG"
        do vq_exec with 'CREATMP4',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPABIDONE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
                +" and "+"RIPABIDONE.CODSUP = _t2.CODSUP";
                +" and "+"RIPABIDONE.TIPREG = _t2.TIPREG";
                +" and "+"RIPABIDONE.SERRIF = _t2.SERRIF";
                +" and "+"RIPABIDONE.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPABIDONE.NUMRIF = _t2.NUMRIF";
                +" and "+"RIPABIDONE.IMPSUP = _t2.IMPSUP";
                +" and "+"RIPABIDONE.CODVOC = _t2.CODVOC";
                +" and "+"RIPABIDONE.SEGNO = _t2.SEGNO";
                +" and "+"RIPABIDONE.NUMERO = _t2.NUMERO";
                +" and "+"RIPABIDONE.NUMSUP = _t2.NUMSUP";
                +" and "+"RIPABIDONE.NUMPROG = _t2.NUMPROG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TOTIMP = RIPABIDONE.TOTIMP+_t2.DIFRIP";
            +i_ccchkf;
            +" from "+i_cTable+" RIPABIDONE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
                +" and "+"RIPABIDONE.CODSUP = _t2.CODSUP";
                +" and "+"RIPABIDONE.TIPREG = _t2.TIPREG";
                +" and "+"RIPABIDONE.SERRIF = _t2.SERRIF";
                +" and "+"RIPABIDONE.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPABIDONE.NUMRIF = _t2.NUMRIF";
                +" and "+"RIPABIDONE.IMPSUP = _t2.IMPSUP";
                +" and "+"RIPABIDONE.CODVOC = _t2.CODVOC";
                +" and "+"RIPABIDONE.SEGNO = _t2.SEGNO";
                +" and "+"RIPABIDONE.NUMERO = _t2.NUMERO";
                +" and "+"RIPABIDONE.NUMSUP = _t2.NUMSUP";
                +" and "+"RIPABIDONE.NUMPROG = _t2.NUMPROG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE, "+i_cQueryTable+" _t2 set ";
            +"RIPABIDONE.TOTIMP = RIPABIDONE.TOTIMP+_t2.DIFRIP";
            +Iif(Empty(i_ccchkf),"",",RIPABIDONE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="RIPABIDONE.CODICE = t2.CODICE";
                +" and "+"RIPABIDONE.CODSUP = t2.CODSUP";
                +" and "+"RIPABIDONE.TIPREG = t2.TIPREG";
                +" and "+"RIPABIDONE.SERRIF = t2.SERRIF";
                +" and "+"RIPABIDONE.ORDRIF = t2.ORDRIF";
                +" and "+"RIPABIDONE.NUMRIF = t2.NUMRIF";
                +" and "+"RIPABIDONE.IMPSUP = t2.IMPSUP";
                +" and "+"RIPABIDONE.CODVOC = t2.CODVOC";
                +" and "+"RIPABIDONE.SEGNO = t2.SEGNO";
                +" and "+"RIPABIDONE.NUMERO = t2.NUMERO";
                +" and "+"RIPABIDONE.NUMSUP = t2.NUMSUP";
                +" and "+"RIPABIDONE.NUMPROG = t2.NUMPROG";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE set (";
            +"TOTIMP";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"TOTIMP+t2.DIFRIP";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
                +" and "+"RIPABIDONE.CODSUP = _t2.CODSUP";
                +" and "+"RIPABIDONE.TIPREG = _t2.TIPREG";
                +" and "+"RIPABIDONE.SERRIF = _t2.SERRIF";
                +" and "+"RIPABIDONE.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPABIDONE.NUMRIF = _t2.NUMRIF";
                +" and "+"RIPABIDONE.IMPSUP = _t2.IMPSUP";
                +" and "+"RIPABIDONE.CODVOC = _t2.CODVOC";
                +" and "+"RIPABIDONE.SEGNO = _t2.SEGNO";
                +" and "+"RIPABIDONE.NUMERO = _t2.NUMERO";
                +" and "+"RIPABIDONE.NUMSUP = _t2.NUMSUP";
                +" and "+"RIPABIDONE.NUMPROG = _t2.NUMPROG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE set ";
            +"TOTIMP = RIPABIDONE.TOTIMP+_t2.DIFRIP";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CODICE = "+i_cQueryTable+".CODICE";
                +" and "+i_cTable+".CODSUP = "+i_cQueryTable+".CODSUP";
                +" and "+i_cTable+".TIPREG = "+i_cQueryTable+".TIPREG";
                +" and "+i_cTable+".SERRIF = "+i_cQueryTable+".SERRIF";
                +" and "+i_cTable+".ORDRIF = "+i_cQueryTable+".ORDRIF";
                +" and "+i_cTable+".NUMRIF = "+i_cQueryTable+".NUMRIF";
                +" and "+i_cTable+".IMPSUP = "+i_cQueryTable+".IMPSUP";
                +" and "+i_cTable+".CODVOC = "+i_cQueryTable+".CODVOC";
                +" and "+i_cTable+".SEGNO = "+i_cQueryTable+".SEGNO";
                +" and "+i_cTable+".NUMERO = "+i_cQueryTable+".NUMERO";
                +" and "+i_cTable+".NUMSUP = "+i_cQueryTable+".NUMSUP";
                +" and "+i_cTable+".NUMPROG = "+i_cQueryTable+".NUMPROG";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TOTIMP = (select "+i_cTable+".TOTIMP+DIFRIP from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Scrittura in ripabidone del record privo di Commessa per marcare il fatto che � stato ripartito
      * --- Write into RIPABIDONE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="CODICE,TIPREG,SERRIF,ORDRIF,NUMRIF,UNIVOC,ORILIV,SEGNO"
        do vq_exec with 'query\COMMTMP3_VM',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPABIDONE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
                +" and "+"RIPABIDONE.TIPREG = _t2.TIPREG";
                +" and "+"RIPABIDONE.SERRIF = _t2.SERRIF";
                +" and "+"RIPABIDONE.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPABIDONE.NUMRIF = _t2.NUMRIF";
                +" and "+"RIPABIDONE.UNIVOC = _t2.UNIVOC";
                +" and "+"RIPABIDONE.ORILIV = _t2.ORILIV";
                +" and "+"RIPABIDONE.SEGNO = _t2.SEGNO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA');
            +i_ccchkf;
            +" from "+i_cTable+" RIPABIDONE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
                +" and "+"RIPABIDONE.TIPREG = _t2.TIPREG";
                +" and "+"RIPABIDONE.SERRIF = _t2.SERRIF";
                +" and "+"RIPABIDONE.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPABIDONE.NUMRIF = _t2.NUMRIF";
                +" and "+"RIPABIDONE.UNIVOC = _t2.UNIVOC";
                +" and "+"RIPABIDONE.ORILIV = _t2.ORILIV";
                +" and "+"RIPABIDONE.SEGNO = _t2.SEGNO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE, "+i_cQueryTable+" _t2 set ";
        +"RIPABIDONE.FLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA');
            +Iif(Empty(i_ccchkf),"",",RIPABIDONE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="RIPABIDONE.CODICE = t2.CODICE";
                +" and "+"RIPABIDONE.TIPREG = t2.TIPREG";
                +" and "+"RIPABIDONE.SERRIF = t2.SERRIF";
                +" and "+"RIPABIDONE.ORDRIF = t2.ORDRIF";
                +" and "+"RIPABIDONE.NUMRIF = t2.NUMRIF";
                +" and "+"RIPABIDONE.UNIVOC = t2.UNIVOC";
                +" and "+"RIPABIDONE.ORILIV = t2.ORILIV";
                +" and "+"RIPABIDONE.SEGNO = t2.SEGNO";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE set (";
            +"FLRIPA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="RIPABIDONE.CODICE = _t2.CODICE";
                +" and "+"RIPABIDONE.TIPREG = _t2.TIPREG";
                +" and "+"RIPABIDONE.SERRIF = _t2.SERRIF";
                +" and "+"RIPABIDONE.ORDRIF = _t2.ORDRIF";
                +" and "+"RIPABIDONE.NUMRIF = _t2.NUMRIF";
                +" and "+"RIPABIDONE.UNIVOC = _t2.UNIVOC";
                +" and "+"RIPABIDONE.ORILIV = _t2.ORILIV";
                +" and "+"RIPABIDONE.SEGNO = _t2.SEGNO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" RIPABIDONE set ";
        +"FLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".CODICE = "+i_cQueryTable+".CODICE";
                +" and "+i_cTable+".TIPREG = "+i_cQueryTable+".TIPREG";
                +" and "+i_cTable+".SERRIF = "+i_cQueryTable+".SERRIF";
                +" and "+i_cTable+".ORDRIF = "+i_cQueryTable+".ORDRIF";
                +" and "+i_cTable+".NUMRIF = "+i_cQueryTable+".NUMRIF";
                +" and "+i_cTable+".UNIVOC = "+i_cQueryTable+".UNIVOC";
                +" and "+i_cTable+".ORILIV = "+i_cQueryTable+".ORILIV";
                +" and "+i_cTable+".SEGNO = "+i_cQueryTable+".SEGNO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLRIPA ="+cp_NullLink(cp_ToStrODBC("S"),'RIPABIDONE','FLRIPA');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Drop temporary table RIPATMP1
      i_nIdx=cp_GetTableDefIdx('RIPATMP1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('RIPATMP1')
      endif
      * --- Select from RIPATMP2
      i_nConn=i_TableProp[this.RIPATMP2_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIPATMP2_idx,2],.t.,this.RIPATMP2_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIPATMP2 ";
            +" order by CODSUP,TIPREG,SERRIF,ORDRIF,NUMRIF,UNIVOC,ORILIV,IMPSUP,SEGNO";
             ,"_Curs_RIPATMP2")
      else
        select * from (i_cTable);
         order by CODSUP,TIPREG,SERRIF,ORDRIF,NUMRIF,UNIVOC,ORILIV,IMPSUP,SEGNO;
          into cursor _Curs_RIPATMP2
      endif
      if used('_Curs_RIPATMP2')
        select _Curs_RIPATMP2
        locate for 1=1
        do while not(eof())
        this.w_TOTREC = this.w_TOTREC + 1
        this.w_NEWREC = _Curs_RIPATMP2.CODSUP + _Curs_RIPATMP2.TIPREG
        this.w_NEWREC = this.w_NEWREC + NVL(_Curs_RIPATMP2.SERRIF, SPACE(10)) + STR(NVL(_Curs_RIPATMP2.ORDRIF,0),6,0) + STR(NVL(_Curs_RIPATMP2.NUMRIF,0), 6,0)
        this.w_NEWREC = this.w_NEWREC + STR(_Curs_RIPATMP2.UNIVOC,6,0) + STR(_Curs_RIPATMP2.ORILIV,6,0) + STR(_Curs_RIPATMP2.IMPSUP,18,4) + Nvl(_Curs_RIPATMP2.CODVOC,Space(15))+Nvl(SEGNO," ")
        if this.w_OLDREC<>this.w_NEWREC
          if this.w_TOTRIP<>0
            * --- Write into RIPABIDONE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPABIDONE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TOTIMP =TOTIMP+ "+cp_ToStrODBC(this.w_TOTRIP);
                  +i_ccchkf ;
              +" where ";
                  +"CODICE = "+cp_ToStrODBC(this.w_CODICE);
                  +" and CODSUP = "+cp_ToStrODBC(this.w_CODSUP);
                  +" and TIPREG = "+cp_ToStrODBC(this.w_TIPREG);
                  +" and SERRIF = "+cp_ToStrODBC(this.w_SERRIF);
                  +" and ORDRIF = "+cp_ToStrODBC(this.w_ORDRIF);
                  +" and NUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                  +" and UNIVOC = "+cp_ToStrODBC(this.w_UNIVOC);
                  +" and ORILIV = "+cp_ToStrODBC(this.w_ORILIV);
                  +" and CODVOC = "+cp_ToStrODBC(this.w_CODVOC);
                     )
            else
              update (i_cTable) set;
                  TOTIMP = TOTIMP + this.w_TOTRIP;
                  &i_ccchkf. ;
               where;
                  CODICE = this.w_CODICE;
                  and CODSUP = this.w_CODSUP;
                  and TIPREG = this.w_TIPREG;
                  and SERRIF = this.w_SERRIF;
                  and ORDRIF = this.w_ORDRIF;
                  and NUMRIF = this.w_NUMRIF;
                  and UNIVOC = this.w_UNIVOC;
                  and ORILIV = this.w_ORILIV;
                  and CODVOC = this.w_CODVOC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          this.w_TOTRIP = IMPSUP
        endif
        this.w_NUMLIV = NUMLIV
        this.w_NUMRIG = NUMRIG
        this.w_DATREG = DATREG
        this.w_CODVOC = CODVOC
        this.w_CODICE = CODICE
        this.w_VALNAZ = VALNAZ
        this.w_SEGNO = SEGNO
        this.w_TIPREG = TIPREG
        this.w_CODESE = CODESE
        this.w_INICOM = INICOM
        this.w_FINCOM = FINCOM
        this.w_FLRIPA = FLRIPA
        this.w_SERRIF = SERRIF
        this.w_ORDRIF = ORDRIF
        this.w_NUMRIF = NUMRIF
        this.w_FLDIRE = FLDIRE
        this.w_CODCOM = CODCOM
        this.w_CODSUP = CODSUP
        this.w_TOTIMP = TOTIMP
        this.w_IMPSUP = IMPSUP
        this.w_ORILIV = this.w_NLIV
        this.w_UNIVOC = this.w_TOTREC
        this.w_TOTRIP = this.w_TOTRIP - this.w_TOTIMP
        * --- Insert into RIPABIDONE
        i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RIPABIDONE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"UNIVOC"+",ORILIV"+",NUMRIG"+",DATREG"+",CODVOC"+",VALNAZ"+",CODICE"+",NUMLIV"+",SEGNO"+",CODESE"+",TIPREG"+",FINCOM"+",INICOM"+",SERRIF"+",FLRIPA"+",NUMRIF"+",ORDRIF"+",TOTIMP"+",CODCOM"+",FLDIRE"+",IMPSUP"+",CODSUP"+",NUMERO"+",NUMSUP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_UNIVOC),'RIPABIDONE','UNIVOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ORILIV),'RIPABIDONE','ORILIV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'RIPABIDONE','NUMRIG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'RIPABIDONE','DATREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODVOC),'RIPABIDONE','CODVOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_VALNAZ),'RIPABIDONE','VALNAZ');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'RIPABIDONE','CODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMLIV),'RIPABIDONE','NUMLIV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SEGNO),'RIPABIDONE','SEGNO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODESE),'RIPABIDONE','CODESE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREG),'RIPABIDONE','TIPREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FINCOM),'RIPABIDONE','FINCOM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_INICOM),'RIPABIDONE','INICOM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SERRIF),'RIPABIDONE','SERRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FLRIPA),'RIPABIDONE','FLRIPA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIF),'RIPABIDONE','NUMRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ORDRIF),'RIPABIDONE','ORDRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_TOTIMP),'RIPABIDONE','TOTIMP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'RIPABIDONE','CODCOM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FLDIRE),'RIPABIDONE','FLDIRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPSUP),'RIPABIDONE','IMPSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODSUP),'RIPABIDONE','CODSUP');
          +","+cp_NullLink(cp_ToStrODBC(0),'RIPABIDONE','NUMERO');
          +","+cp_NullLink(cp_ToStrODBC(0),'RIPABIDONE','NUMSUP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'UNIVOC',this.w_UNIVOC,'ORILIV',this.w_ORILIV,'NUMRIG',this.w_NUMRIG,'DATREG',this.w_DATREG,'CODVOC',this.w_CODVOC,'VALNAZ',this.w_VALNAZ,'CODICE',this.w_CODICE,'NUMLIV',this.w_NUMLIV,'SEGNO',this.w_SEGNO,'CODESE',this.w_CODESE,'TIPREG',this.w_TIPREG,'FINCOM',this.w_FINCOM)
          insert into (i_cTable) (UNIVOC,ORILIV,NUMRIG,DATREG,CODVOC,VALNAZ,CODICE,NUMLIV,SEGNO,CODESE,TIPREG,FINCOM,INICOM,SERRIF,FLRIPA,NUMRIF,ORDRIF,TOTIMP,CODCOM,FLDIRE,IMPSUP,CODSUP,NUMERO,NUMSUP &i_ccchkf. );
             values (;
               this.w_UNIVOC;
               ,this.w_ORILIV;
               ,this.w_NUMRIG;
               ,this.w_DATREG;
               ,this.w_CODVOC;
               ,this.w_VALNAZ;
               ,this.w_CODICE;
               ,this.w_NUMLIV;
               ,this.w_SEGNO;
               ,this.w_CODESE;
               ,this.w_TIPREG;
               ,this.w_FINCOM;
               ,this.w_INICOM;
               ,this.w_SERRIF;
               ,this.w_FLRIPA;
               ,this.w_NUMRIF;
               ,this.w_ORDRIF;
               ,this.w_TOTIMP;
               ,this.w_CODCOM;
               ,this.w_FLDIRE;
               ,this.w_IMPSUP;
               ,this.w_CODSUP;
               ,0;
               ,0;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore in Inserimento Tabella Temporanea Ripartizioni'
          return
        endif
        this.w_OLDREC = this.w_NEWREC
        this.w_GENERA = .T.
          select _Curs_RIPATMP2
          continue
        enddo
        use
      endif
      * --- Resto
      if this.w_TOTRIP<>0
        * --- Write into RIPABIDONE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.RIPABIDONE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIPABIDONE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.RIPABIDONE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTIMP =TOTIMP+ "+cp_ToStrODBC(this.w_TOTRIP);
              +i_ccchkf ;
          +" where ";
              +"CODICE = "+cp_ToStrODBC(this.w_CODICE);
              +" and CODSUP = "+cp_ToStrODBC(this.w_CODSUP);
              +" and TIPREG = "+cp_ToStrODBC(this.w_TIPREG);
              +" and SERRIF = "+cp_ToStrODBC(this.w_SERRIF);
              +" and ORDRIF = "+cp_ToStrODBC(this.w_ORDRIF);
              +" and NUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
              +" and UNIVOC = "+cp_ToStrODBC(this.w_UNIVOC);
              +" and ORILIV = "+cp_ToStrODBC(this.w_ORILIV);
              +" and CODVOC = "+cp_ToStrODBC(this.w_CODVOC);
                 )
        else
          update (i_cTable) set;
              TOTIMP = TOTIMP + this.w_TOTRIP;
              &i_ccchkf. ;
           where;
              CODICE = this.w_CODICE;
              and CODSUP = this.w_CODSUP;
              and TIPREG = this.w_TIPREG;
              and SERRIF = this.w_SERRIF;
              and ORDRIF = this.w_ORDRIF;
              and NUMRIF = this.w_NUMRIF;
              and UNIVOC = this.w_UNIVOC;
              and ORILIV = this.w_ORILIV;
              and CODVOC = this.w_CODVOC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,12)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='MOVICOST'
    this.cWorkTables[3]='MOVIRIPA'
    this.cWorkTables[4]='MVM_DETT'
    this.cWorkTables[5]='*RIPABIDONE'
    this.cWorkTables[6]='*RIPATMP1'
    this.cWorkTables[7]='*RIPATMP2'
    this.cWorkTables[8]='*RIPATMP3'
    this.cWorkTables[9]='VALUTE'
    this.cWorkTables[10]='ESERCIZI'
    this.cWorkTables[11]='*TMP_ANA'
    this.cWorkTables[12]='*RIPATMP4'
    return(this.OpenAllTables(12))

  proc CloseCursors()
    if used('_Curs_RIPATMP2')
      use in _Curs_RIPATMP2
    endif
    if used('_Curs_RIPATMP2')
      use in _Curs_RIPATMP2
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
