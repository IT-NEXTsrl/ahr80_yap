* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_szi                                                        *
*              Visualizzazione inventario                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_54]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-29                                                      *
* Last revis.: 2008-10-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_szi",oParentObject))

* --- Class definition
define class tgsma_szi as StdForm
  Top    = 6
  Left   = 8

  * --- Standard Properties
  Width  = 790
  Height = 471
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-06"
  HelpContextID=26576023
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  ESERCIZI_IDX = 0
  INVEDETT_IDX = 0
  INVENTAR_IDX = 0
  MAGAZZIN_IDX = 0
  cPrg = "gsma_szi"
  cComment = "Visualizzazione inventario"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(10)
  w_INNUMINV = space(6)
  w_INCODESE = space(4)
  o_INCODESE = space(4)
  w_INDESINV = space(40)
  w_CODART = space(20)
  w_TIPO = space(2)
  w_DESART = space(40)
  w_INDATINV = ctod('  /  /  ')
  w_ZICODICE = space(40)
  w_INCODMAG = space(5)
  w_INTIPINV = space(1)
  w_INSTAINV = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DESMAG = space(30)
  w_QTAESI = space(1)
  w_ZoomInv = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_sziPag1","gsma_szi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oINNUMINV_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomInv = this.oPgFrm.Pages(1).oPag.ZoomInv
    DoDefault()
    proc Destroy()
      this.w_ZoomInv = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='INVEDETT'
    this.cWorkTables[4]='INVENTAR'
    this.cWorkTables[5]='MAGAZZIN'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(10)
      .w_INNUMINV=space(6)
      .w_INCODESE=space(4)
      .w_INDESINV=space(40)
      .w_CODART=space(20)
      .w_TIPO=space(2)
      .w_DESART=space(40)
      .w_INDATINV=ctod("  /  /  ")
      .w_ZICODICE=space(40)
      .w_INCODMAG=space(5)
      .w_INTIPINV=space(1)
      .w_INSTAINV=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESMAG=space(30)
      .w_QTAESI=space(1)
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_INNUMINV))
          .link_1_2('Full')
        endif
        .w_INCODESE = g_CODESE
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_INCODESE))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_CODART))
          .link_1_5('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomInv.Calculate()
          .DoRTCalc(6,8,.f.)
        .w_ZICODICE = .w_ZoomInv.getVar('DICODART')
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_INCODMAG))
          .link_1_19('Full')
        endif
        .w_INTIPINV = 'G'
        .w_INSTAINV = 'P'
        .w_OBTEST = .w_INDATINV
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
          .DoRTCalc(14,14,.f.)
        .w_QTAESI = 'T'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_INCODESE<>.w_INCODESE
          .link_1_2('Full')
        endif
        .oPgFrm.Page1.oPag.ZoomInv.Calculate()
        .DoRTCalc(3,8,.t.)
            .w_ZICODICE = .w_ZoomInv.getVar('DICODART')
          .link_1_19('Full')
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomInv.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oINCODMAG_1_19.visible=!this.oPgFrm.Page1.oPag.oINCODMAG_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDESMAG_1_25.visible=!this.oPgFrm.Page1.oPag.oDESMAG_1_25.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomInv.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=INNUMINV
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INNUMINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsma_ain',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_INNUMINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_INCODESE);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INCODMAG,INTIPINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_INCODESE;
                     ,'INNUMINV',trim(this.w_INNUMINV))
          select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INCODMAG,INTIPINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INNUMINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INNUMINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oINNUMINV_1_2'),i_cWhere,'gsma_ain',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_INCODESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INCODMAG,INTIPINV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INCODMAG,INTIPINV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INCODMAG,INTIPINV";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_INCODESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INCODMAG,INTIPINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INNUMINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INCODMAG,INTIPINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_INNUMINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_INCODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_INCODESE;
                       ,'INNUMINV',this.w_INNUMINV)
            select INCODESE,INNUMINV,INDESINV,INDATINV,INSTAINV,INCODMAG,INTIPINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INNUMINV = NVL(_Link_.INNUMINV,space(6))
      this.w_INDESINV = NVL(_Link_.INDESINV,space(40))
      this.w_INDATINV = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
      this.w_INSTAINV = NVL(_Link_.INSTAINV,space(1))
      this.w_INCODMAG = NVL(_Link_.INCODMAG,space(5))
      this.w_INTIPINV = NVL(_Link_.INTIPINV,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_INNUMINV = space(6)
      endif
      this.w_INDESINV = space(40)
      this.w_INDATINV = ctod("  /  /  ")
      this.w_INSTAINV = space(1)
      this.w_INCODMAG = space(5)
      this.w_INTIPINV = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INNUMINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INCODESE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_INCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_INCODESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oINCODESE_1_3'),i_cWhere,'GSAR_KES',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_INCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_INCODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INCODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_INCODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_5'),i_cWhere,'GSMA_BZA',"Codici articoli",'GSMA0ADI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_TIPO = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_TIPO = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPO $ 'PF-SE-MP-PH-MC-MA-IM-FS' OR EMPTY(.w_CODART)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di tipo non articolo")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_TIPO = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INCODMAG
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_INCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_INCODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_INCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oINNUMINV_1_2.value==this.w_INNUMINV)
      this.oPgFrm.Page1.oPag.oINNUMINV_1_2.value=this.w_INNUMINV
    endif
    if not(this.oPgFrm.Page1.oPag.oINCODESE_1_3.value==this.w_INCODESE)
      this.oPgFrm.Page1.oPag.oINCODESE_1_3.value=this.w_INCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oINDESINV_1_4.value==this.w_INDESINV)
      this.oPgFrm.Page1.oPag.oINDESINV_1_4.value=this.w_INDESINV
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_5.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_5.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_7.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_7.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oINDATINV_1_8.value==this.w_INDATINV)
      this.oPgFrm.Page1.oPag.oINDATINV_1_8.value=this.w_INDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oINCODMAG_1_19.value==this.w_INCODMAG)
      this.oPgFrm.Page1.oPag.oINCODMAG_1_19.value=this.w_INCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oINTIPINV_1_20.RadioValue()==this.w_INTIPINV)
      this.oPgFrm.Page1.oPag.oINTIPINV_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINSTAINV_1_21.RadioValue()==this.w_INSTAINV)
      this.oPgFrm.Page1.oPag.oINSTAINV_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_25.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_25.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAESI_1_27.RadioValue()==this.w_QTAESI)
      this.oPgFrm.Page1.oPag.oQTAESI_1_27.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPO $ 'PF-SE-MP-PH-MC-MA-IM-FS' OR EMPTY(.w_CODART))  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di tipo non articolo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_INCODESE = this.w_INCODESE
    return

enddefine

* --- Define pages as container
define class tgsma_sziPag1 as StdContainer
  Width  = 786
  height = 471
  stdWidth  = 786
  stdheight = 471
  resizeXpos=401
  resizeYpos=348
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oINNUMINV_1_2 as StdField with uid="FBUMHDYOWB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_INNUMINV", cQueryName = "INNUMINV",nZero=6,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero dell'inventario da visualizzare",;
    HelpContextID = 264222684,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=218, Top=10, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="gsma_ain", oKey_1_1="INCODESE", oKey_1_2="this.w_INCODESE", oKey_2_1="INNUMINV", oKey_2_2="this.w_INNUMINV"

  func oINNUMINV_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oINNUMINV_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINNUMINV_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_INCODESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_INCODESE)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oINNUMINV_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'gsma_ain',"",'',this.parent.oContained
  endproc
  proc oINNUMINV_1_2.mZoomOnZoom
    local i_obj
    i_obj=gsma_ain()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_INCODESE
     i_obj.w_INNUMINV=this.parent.oContained.w_INNUMINV
     i_obj.ecpSave()
  endproc

  add object oINCODESE_1_3 as StdField with uid="QMVHHJRRTS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_INCODESE", cQueryName = "INCODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio di riferimento",;
    HelpContextID = 81197109,;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=66, Top=10, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_INCODESE"

  func oINCODESE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
      if .not. empty(.w_INNUMINV)
        bRes2=.link_1_2('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oINCODESE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINCODESE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oINCODESE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"",'',this.parent.oContained
  endproc
  proc oINCODESE_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_INCODESE
     i_obj.ecpSave()
  endproc

  add object oINDESINV_1_4 as StdField with uid="MGUAKMVKWV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_INDESINV", cQueryName = "INDESINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 989148,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=283, Top=10, InputMask=replicate('X',40)

  add object oCODART_1_5 as StdField with uid="ZLHSTXFNHF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di tipo non articolo",;
    ToolTipText = "Codice dell'articolo da visualizzare (vuoto = tutti)",;
    HelpContextID = 184227878,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=66, Top=37, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Codici articoli",'GSMA0ADI.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oDESART_1_7 as StdField with uid="LAVBLRKKSP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 184286774,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=218, Top=37, InputMask=replicate('X',40)

  add object oINDATINV_1_8 as StdField with uid="EVSBTTCBCG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_INDATINV", cQueryName = "INDATINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fino alla quale sono stati considerati i movimenti",;
    HelpContextID = 1775580,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=695, Top=10


  add object oBtn_1_9 as StdButton with uid="CCCVAGDESO",left=726, top=65, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 64937194;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        gsma_bzi(this.Parent.oContained,"ESEGUI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_INCODESE) AND NOT EMPTY(.w_INNUMINV))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="FHRZELUQCD",left=17, top=423, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Richiama il dettaglio relativo alla riga selezionata";
    , HelpContextID = 253832863;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        gsma_bzi(this.Parent.oContained,"LANCIA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_ZICODICE,'')))
      endwith
    endif
  endfunc


  add object oBtn_1_11 as StdButton with uid="QBYJNNWOVZ",left=726, top=423, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 26604774;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomInv as cp_zoombox with uid="DESBYFROYQ",left=-4, top=104, width=791,height=314,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="INVEDETT",cZoomFile="GSMA_SZI",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.t.,bAdvOptions=.t.,cMenuFile="",cZoomOnZoom="GSZM_BZI",bRetriveAllRows=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 63861786

  add object oINCODMAG_1_19 as StdField with uid="GCJURJOVBO",rtseq=10,rtrep=.f.,;
    cFormVar = "w_INCODMAG", cQueryName = "INCODMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del magazzino elaborato",;
    HelpContextID = 53020621,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=145, Top=442, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_INCODMAG"

  func oINCODMAG_1_19.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_INCODMAG))
    endwith
  endfunc

  func oINCODMAG_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oINTIPINV_1_20 as StdCombo with uid="WAYFJNRTKJ",rtseq=11,rtrep=.f.,left=462,top=442,width=98,height=21, enabled=.f.;
    , ToolTipText = "Indica se riguarda tutti gli articoli o solo quelli compresi nei criteri";
    , HelpContextID = 266606556;
    , cFormVar="w_INTIPINV",RowSource=""+"Globale,"+"Parziale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oINTIPINV_1_20.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oINTIPINV_1_20.GetRadio()
    this.Parent.oContained.w_INTIPINV = this.RadioValue()
    return .t.
  endfunc

  func oINTIPINV_1_20.SetRadio()
    this.Parent.oContained.w_INTIPINV=trim(this.Parent.oContained.w_INTIPINV)
    this.value = ;
      iif(this.Parent.oContained.w_INTIPINV=='G',1,;
      iif(this.Parent.oContained.w_INTIPINV=='P',2,;
      0))
  endfunc


  add object oINSTAINV_1_21 as StdCombo with uid="TIFXDSANSE",rtseq=12,rtrep=.f.,left=604,top=442,width=98,height=21, enabled=.f.;
    , ToolTipText = "Indica se l'inventario � confermato o deve essere ulteriormente elaborato";
    , HelpContextID = 251594716;
    , cFormVar="w_INSTAINV",RowSource=""+"Provvisorio,"+"Confermato,"+"Storico", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oINSTAINV_1_21.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'D',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oINSTAINV_1_21.GetRadio()
    this.Parent.oContained.w_INSTAINV = this.RadioValue()
    return .t.
  endfunc

  func oINSTAINV_1_21.SetRadio()
    this.Parent.oContained.w_INSTAINV=trim(this.Parent.oContained.w_INSTAINV)
    this.value = ;
      iif(this.Parent.oContained.w_INSTAINV=='P',1,;
      iif(this.Parent.oContained.w_INSTAINV=='D',2,;
      iif(this.Parent.oContained.w_INSTAINV=='S',3,;
      0)))
  endfunc

  add object oDESMAG_1_25 as StdField with uid="HWHMVGYZZH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 217579062,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=208, Top=442, InputMask=replicate('X',30)

  func oDESMAG_1_25.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_INCODMAG))
    endwith
  endfunc


  add object oObj_1_26 as cp_runprogram with uid="MPBOCHJZNO",left=41, top=505, width=251,height=19,;
    caption='gsma_bzi(LANCIA)',;
   bGlobalFont=.t.,;
    prg="gsma_bzi('LANCIA')",;
    cEvent = "w_zoominv selected",;
    nPag=1;
    , HelpContextID = 124792184


  add object oQTAESI_1_27 as StdCombo with uid="SNUTZZLXWR",rtseq=15,rtrep=.f.,left=666,top=37,width=108,height=21;
    , ToolTipText = "Seleziona articoli in base all'esistenza";
    , HelpContextID = 978438;
    , cFormVar="w_QTAESI",RowSource=""+"Tutti,"+"Positiva,"+"Negativa,"+"Zero", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oQTAESI_1_27.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'P',;
    iif(this.value =3,'N',;
    iif(this.value =4,'Z',;
    space(1))))))
  endfunc
  func oQTAESI_1_27.GetRadio()
    this.Parent.oContained.w_QTAESI = this.RadioValue()
    return .t.
  endfunc

  func oQTAESI_1_27.SetRadio()
    this.Parent.oContained.w_QTAESI=trim(this.Parent.oContained.w_QTAESI)
    this.value = ;
      iif(this.Parent.oContained.w_QTAESI=='T',1,;
      iif(this.Parent.oContained.w_QTAESI=='P',2,;
      iif(this.Parent.oContained.w_QTAESI=='N',3,;
      iif(this.Parent.oContained.w_QTAESI=='Z',4,;
      0))))
  endfunc

  add object oStr_1_13 as StdString with uid="IVOCQXDMJI",Visible=.t., Left=-6, Top=37,;
    Alignment=1, Width=70, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KCPHZWIKXX",Visible=.t., Left=145, Top=10,;
    Alignment=1, Width=70, Height=15,;
    Caption="Inventario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="POYHAIGMPK",Visible=.t., Left=74, Top=442,;
    Alignment=1, Width=68, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_INCODMAG))
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="VZJVOYYJGH",Visible=.t., Left=640, Top=10,;
    Alignment=1, Width=54, Height=15,;
    Caption="Fino al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="GRRRLBASRS",Visible=.t., Left=3, Top=10,;
    Alignment=1, Width=61, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="CNILQOLHNK",Visible=.t., Left=558, Top=442,;
    Alignment=1, Width=44, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="NQCXARNHOT",Visible=.t., Left=430, Top=442,;
    Alignment=1, Width=31, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="KJQLAHUWKL",Visible=.t., Left=577, Top=38,;
    Alignment=1, Width=85, Height=18,;
    Caption="Esistenza:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_szi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
