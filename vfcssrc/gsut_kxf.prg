* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kxf                                                        *
*              Configurazione xfrx                                             *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_74]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-07-12                                                      *
* Last revis.: 2010-04-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kxf",oParentObject))

* --- Class definition
define class tgsut_kxf as StdForm
  Top    = 5
  Left   = 1

  * --- Standard Properties
  Width  = 664
  Height = 382
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-04-26"
  HelpContextID=15287145
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kxf"
  cComment = "Configurazione xfrx"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_RANGE = space(254)
  o_RANGE = space(254)
  w_NOTOPENVIEWER = .F.
  o_NOTOPENVIEWER = .F.
  w_AUTHOR = space(254)
  o_AUTHOR = space(254)
  w_TITLE = space(254)
  o_TITLE = space(254)
  w_SUBJECT = space(254)
  o_SUBJECT = space(254)
  w_KEYWORDS = space(254)
  o_KEYWORDS = space(254)
  w_PDFENCRYPTION = .F.
  o_PDFENCRYPTION = .F.
  w_OwnerPassword = space(254)
  o_OwnerPassword = space(254)
  w_OwnerPasswordConf = space(254)
  w_UserPassword = space(254)
  o_UserPassword = space(254)
  w_UserPasswordConf = space(254)
  w_PrintDocument = .F.
  o_PrintDocument = .F.
  w_ModifyDocument = .F.
  o_ModifyDocument = .F.
  w_CopyTextAndGraphics = .F.
  o_CopyTextAndGraphics = .F.
  w_AddOrModifyAnnotations = .F.
  o_AddOrModifyAnnotations = .F.
  w_Archive = space(254)
  o_Archive = space(254)
  w_Additive = .F.
  o_Additive = .F.
  w_DeleteFileAfter = .F.
  o_DeleteFileAfter = .F.
  w_FORMAT = space(10)
  w_cAdditive = space(3)
  o_cAdditive = space(3)
  w_cNOTOPENVIEWER = space(3)
  o_cNOTOPENVIEWER = space(3)
  w_cPDFENCRYPTION = space(3)
  o_cPDFENCRYPTION = space(3)
  w_cPrintDocument = space(3)
  o_cPrintDocument = space(3)
  w_cModifyDocument = space(3)
  o_cModifyDocument = space(3)
  w_cCopyTextAndGraphics = space(3)
  o_cCopyTextAndGraphics = space(3)
  w_cAddOrModifyAnnotations = space(3)
  o_cAddOrModifyAnnotations = space(3)
  w_cDeleteFileAfter = space(3)
  o_cDeleteFileAfter = space(3)
  w_OPTPDF = space(254)
  w_OPTGLB = space(254)
  w_OPZIONI = space(254)
  w_ABILITA = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kxfPag1","gsut_kxf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRANGE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RANGE=space(254)
      .w_NOTOPENVIEWER=.f.
      .w_AUTHOR=space(254)
      .w_TITLE=space(254)
      .w_SUBJECT=space(254)
      .w_KEYWORDS=space(254)
      .w_PDFENCRYPTION=.f.
      .w_OwnerPassword=space(254)
      .w_OwnerPasswordConf=space(254)
      .w_UserPassword=space(254)
      .w_UserPasswordConf=space(254)
      .w_PrintDocument=.f.
      .w_ModifyDocument=.f.
      .w_CopyTextAndGraphics=.f.
      .w_AddOrModifyAnnotations=.f.
      .w_Archive=space(254)
      .w_Additive=.f.
      .w_DeleteFileAfter=.f.
      .w_FORMAT=space(10)
      .w_cAdditive=space(3)
      .w_cNOTOPENVIEWER=space(3)
      .w_cPDFENCRYPTION=space(3)
      .w_cPrintDocument=space(3)
      .w_cModifyDocument=space(3)
      .w_cCopyTextAndGraphics=space(3)
      .w_cAddOrModifyAnnotations=space(3)
      .w_cDeleteFileAfter=space(3)
      .w_OPTPDF=space(254)
      .w_OPTGLB=space(254)
      .w_OPZIONI=space(254)
      .w_ABILITA=.f.
      .w_FORMAT=oParentObject.w_FORMAT
      .w_OPZIONI=oParentObject.w_OPZIONI
          .DoRTCalc(1,1,.f.)
        .w_NOTOPENVIEWER = .f.
        .w_AUTHOR = i_MSGTITLE
          .DoRTCalc(4,6,.f.)
        .w_PDFENCRYPTION = .f.
          .DoRTCalc(8,11,.f.)
        .w_PrintDocument = .f.
        .w_ModifyDocument = .f.
        .w_CopyTextAndGraphics = .f.
        .w_AddOrModifyAnnotations = .f.
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
          .DoRTCalc(16,16,.f.)
        .w_Additive = .f.
        .w_DeleteFileAfter = iif(.w_NOTOPENVIEWER,.F.,.w_DeleteFileAfter)
          .DoRTCalc(19,19,.f.)
        .w_cAdditive = iif(.w_Additive,".t.",".f.")
        .w_cNOTOPENVIEWER = iif(.w_NOTOPENVIEWER,".f.",".t.")
        .w_cPDFENCRYPTION = iif(.w_PDFENCRYPTION,".t.",".f.")
        .w_cPrintDocument = iif(!.w_PrintDocument,".t.",".f.")
        .w_cModifyDocument = iif(!.w_ModifyDocument,".t.",".f.")
        .w_cCopyTextAndGraphics = iif(!.w_CopyTextAndGraphics,".t.",".f.")
        .w_cAddOrModifyAnnotations = iif(!.w_AddOrModifyAnnotations,".t.",".f.")
        .w_cDeleteFileAfter = iif(.w_DeleteFileAfter,".t.",".f.")
        .w_OPTPDF = ";lPDFEncryption="+.w_cPDFENCRYPTION+";cOwnerPassword='"+Alltrim(.w_OwnerPassword)+"';cUserPassword='"+Alltrim(.w_UserPassword)+"';lPrintDocument="+.w_cPrintDocument+";lModifyDocument="+.w_cModifyDocument+";lCopyTextAndGraphics="+.w_cCopyTextAndGraphics+";lAddOrModifyAnnotations="+.w_cAddOrModifyAnnotations
        .w_OPTGLB = "cRange='"+alltrim(.w_RANGE)+"';lNotOpenViewer="+.w_cNOTOPENVIEWER+";cAuthor='"+Alltrim(.w_AUTHOR)+"';cTitle='"+Alltrim(.w_TITLE)+"';cSubject='"+Alltrim(.w_SUBJECT)+"';cKeywords='"+Alltrim(.w_KEYWORDS)+"';cArchive='"+Alltrim(.w_Archive)+"';lAdditive="+.w_cAdditive+";lDeleteFileAfter="+.w_cDeleteFileAfter
        .w_OPZIONI = .w_OPTGLB+.w_OPTPDF
      .oPgFrm.Page1.oPag.oObj_1_53.Calculate('*')
      .oPgFrm.Page1.oPag.oObj_1_54.Calculate('*')
      .oPgFrm.Page1.oPag.oObj_1_55.Calculate('*')
      .oPgFrm.Page1.oPag.oObj_1_56.Calculate('*')
        .w_ABILITA = IIF(Upper(alltrim(.w_FORMAT))='PDF' OR Upper(alltrim(.w_FORMAT))='DOC',.t.,.f.)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_FORMAT=.w_FORMAT
      .oParentObject.w_OPZIONI=.w_OPZIONI
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .DoRTCalc(1,17,.t.)
        if .o_NOTOPENVIEWER<>.w_NOTOPENVIEWER
            .w_DeleteFileAfter = iif(.w_NOTOPENVIEWER,.F.,.w_DeleteFileAfter)
        endif
        .DoRTCalc(19,19,.t.)
        if .o_Additive<>.w_Additive
            .w_cAdditive = iif(.w_Additive,".t.",".f.")
        endif
        if .o_NOTOPENVIEWER<>.w_NOTOPENVIEWER
            .w_cNOTOPENVIEWER = iif(.w_NOTOPENVIEWER,".f.",".t.")
        endif
        if .o_PDFENCRYPTION<>.w_PDFENCRYPTION
            .w_cPDFENCRYPTION = iif(.w_PDFENCRYPTION,".t.",".f.")
        endif
        if .o_PrintDocument<>.w_PrintDocument
            .w_cPrintDocument = iif(!.w_PrintDocument,".t.",".f.")
        endif
        if .o_ModifyDocument<>.w_ModifyDocument
            .w_cModifyDocument = iif(!.w_ModifyDocument,".t.",".f.")
        endif
        if .o_CopyTextAndGraphics<>.w_CopyTextAndGraphics
            .w_cCopyTextAndGraphics = iif(!.w_CopyTextAndGraphics,".t.",".f.")
        endif
        if .o_AddOrModifyAnnotations<>.w_AddOrModifyAnnotations
            .w_cAddOrModifyAnnotations = iif(!.w_AddOrModifyAnnotations,".t.",".f.")
        endif
        if .o_DeleteFileAfter<>.w_DeleteFileAfter
            .w_cDeleteFileAfter = iif(.w_DeleteFileAfter,".t.",".f.")
        endif
        Local l_Dep1,l_Dep2
        l_Dep1= .o_cPDFENCRYPTION<>.w_cPDFENCRYPTION .or. .o_OwnerPassword<>.w_OwnerPassword .or. .o_UserPassword<>.w_UserPassword .or. .o_cPrintDocument<>.w_cPrintDocument .or. .o_cModifyDocument<>.w_cModifyDocument
        l_Dep2= .o_cCopyTextAndGraphics<>.w_cCopyTextAndGraphics .or. .o_cAddOrModifyAnnotations<>.w_cAddOrModifyAnnotations
        if m.l_Dep1 .or. m.l_Dep2
            .w_OPTPDF = ";lPDFEncryption="+.w_cPDFENCRYPTION+";cOwnerPassword='"+Alltrim(.w_OwnerPassword)+"';cUserPassword='"+Alltrim(.w_UserPassword)+"';lPrintDocument="+.w_cPrintDocument+";lModifyDocument="+.w_cModifyDocument+";lCopyTextAndGraphics="+.w_cCopyTextAndGraphics+";lAddOrModifyAnnotations="+.w_cAddOrModifyAnnotations
        endif
        Local l_Dep1,l_Dep2
        l_Dep1= .o_RANGE<>.w_RANGE .or. .o_cNOTOPENVIEWER<>.w_cNOTOPENVIEWER .or. .o_AUTHOR<>.w_AUTHOR .or. .o_TITLE<>.w_TITLE .or. .o_SUBJECT<>.w_SUBJECT
        l_Dep2= .o_KEYWORDS<>.w_KEYWORDS .or. .o_Archive<>.w_Archive .or. .o_cAdditive<>.w_cAdditive .or. .o_cDeleteFileAfter<>.w_cDeleteFileAfter
        if m.l_Dep1 .or. m.l_Dep2
            .w_OPTGLB = "cRange='"+alltrim(.w_RANGE)+"';lNotOpenViewer="+.w_cNOTOPENVIEWER+";cAuthor='"+Alltrim(.w_AUTHOR)+"';cTitle='"+Alltrim(.w_TITLE)+"';cSubject='"+Alltrim(.w_SUBJECT)+"';cKeywords='"+Alltrim(.w_KEYWORDS)+"';cArchive='"+Alltrim(.w_Archive)+"';lAdditive="+.w_cAdditive+";lDeleteFileAfter="+.w_cDeleteFileAfter
        endif
            .w_OPZIONI = .w_OPTGLB+.w_OPTPDF
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate('*')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(31,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate('*')
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAUTHOR_1_3.enabled = this.oPgFrm.Page1.oPag.oAUTHOR_1_3.mCond()
    this.oPgFrm.Page1.oPag.oTITLE_1_4.enabled = this.oPgFrm.Page1.oPag.oTITLE_1_4.mCond()
    this.oPgFrm.Page1.oPag.oSUBJECT_1_5.enabled = this.oPgFrm.Page1.oPag.oSUBJECT_1_5.mCond()
    this.oPgFrm.Page1.oPag.oKEYWORDS_1_6.enabled = this.oPgFrm.Page1.oPag.oKEYWORDS_1_6.mCond()
    this.oPgFrm.Page1.oPag.oPDFENCRYPTION_1_7.enabled = this.oPgFrm.Page1.oPag.oPDFENCRYPTION_1_7.mCond()
    this.oPgFrm.Page1.oPag.oOwnerPassword_1_8.enabled = this.oPgFrm.Page1.oPag.oOwnerPassword_1_8.mCond()
    this.oPgFrm.Page1.oPag.oOwnerPasswordConf_1_9.enabled = this.oPgFrm.Page1.oPag.oOwnerPasswordConf_1_9.mCond()
    this.oPgFrm.Page1.oPag.oUserPassword_1_10.enabled = this.oPgFrm.Page1.oPag.oUserPassword_1_10.mCond()
    this.oPgFrm.Page1.oPag.oUserPasswordConf_1_11.enabled = this.oPgFrm.Page1.oPag.oUserPasswordConf_1_11.mCond()
    this.oPgFrm.Page1.oPag.oPrintDocument_1_12.enabled = this.oPgFrm.Page1.oPag.oPrintDocument_1_12.mCond()
    this.oPgFrm.Page1.oPag.oModifyDocument_1_13.enabled = this.oPgFrm.Page1.oPag.oModifyDocument_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCopyTextAndGraphics_1_14.enabled = this.oPgFrm.Page1.oPag.oCopyTextAndGraphics_1_14.mCond()
    this.oPgFrm.Page1.oPag.oAddOrModifyAnnotations_1_15.enabled = this.oPgFrm.Page1.oPag.oAddOrModifyAnnotations_1_15.mCond()
    this.oPgFrm.Page1.oPag.oAdditive_1_18.enabled = this.oPgFrm.Page1.oPag.oAdditive_1_18.mCond()
    this.oPgFrm.Page1.oPag.oDeleteFileAfter_1_19.enabled = this.oPgFrm.Page1.oPag.oDeleteFileAfter_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRANGE_1_1.value==this.w_RANGE)
      this.oPgFrm.Page1.oPag.oRANGE_1_1.value=this.w_RANGE
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTOPENVIEWER_1_2.RadioValue()==this.w_NOTOPENVIEWER)
      this.oPgFrm.Page1.oPag.oNOTOPENVIEWER_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAUTHOR_1_3.value==this.w_AUTHOR)
      this.oPgFrm.Page1.oPag.oAUTHOR_1_3.value=this.w_AUTHOR
    endif
    if not(this.oPgFrm.Page1.oPag.oTITLE_1_4.value==this.w_TITLE)
      this.oPgFrm.Page1.oPag.oTITLE_1_4.value=this.w_TITLE
    endif
    if not(this.oPgFrm.Page1.oPag.oSUBJECT_1_5.value==this.w_SUBJECT)
      this.oPgFrm.Page1.oPag.oSUBJECT_1_5.value=this.w_SUBJECT
    endif
    if not(this.oPgFrm.Page1.oPag.oKEYWORDS_1_6.value==this.w_KEYWORDS)
      this.oPgFrm.Page1.oPag.oKEYWORDS_1_6.value=this.w_KEYWORDS
    endif
    if not(this.oPgFrm.Page1.oPag.oPDFENCRYPTION_1_7.RadioValue()==this.w_PDFENCRYPTION)
      this.oPgFrm.Page1.oPag.oPDFENCRYPTION_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOwnerPassword_1_8.value==this.w_OwnerPassword)
      this.oPgFrm.Page1.oPag.oOwnerPassword_1_8.value=this.w_OwnerPassword
    endif
    if not(this.oPgFrm.Page1.oPag.oOwnerPasswordConf_1_9.value==this.w_OwnerPasswordConf)
      this.oPgFrm.Page1.oPag.oOwnerPasswordConf_1_9.value=this.w_OwnerPasswordConf
    endif
    if not(this.oPgFrm.Page1.oPag.oUserPassword_1_10.value==this.w_UserPassword)
      this.oPgFrm.Page1.oPag.oUserPassword_1_10.value=this.w_UserPassword
    endif
    if not(this.oPgFrm.Page1.oPag.oUserPasswordConf_1_11.value==this.w_UserPasswordConf)
      this.oPgFrm.Page1.oPag.oUserPasswordConf_1_11.value=this.w_UserPasswordConf
    endif
    if not(this.oPgFrm.Page1.oPag.oPrintDocument_1_12.RadioValue()==this.w_PrintDocument)
      this.oPgFrm.Page1.oPag.oPrintDocument_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oModifyDocument_1_13.RadioValue()==this.w_ModifyDocument)
      this.oPgFrm.Page1.oPag.oModifyDocument_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCopyTextAndGraphics_1_14.RadioValue()==this.w_CopyTextAndGraphics)
      this.oPgFrm.Page1.oPag.oCopyTextAndGraphics_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAddOrModifyAnnotations_1_15.RadioValue()==this.w_AddOrModifyAnnotations)
      this.oPgFrm.Page1.oPag.oAddOrModifyAnnotations_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oArchive_1_16.value==this.w_Archive)
      this.oPgFrm.Page1.oPag.oArchive_1_16.value=this.w_Archive
    endif
    if not(this.oPgFrm.Page1.oPag.oAdditive_1_18.RadioValue()==this.w_Additive)
      this.oPgFrm.Page1.oPag.oAdditive_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDeleteFileAfter_1_19.RadioValue()==this.w_DeleteFileAfter)
      this.oPgFrm.Page1.oPag.oDeleteFileAfter_1_19.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_OwnerPassword))  and (.w_PDFENCRYPTION=.t.)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOwnerPassword_1_8.SetFocus()
            i_bnoObbl = !empty(.w_OwnerPassword)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_OwnerPasswordConf)) or not(.w_OwnerPasswordConf = .w_OwnerPassword))  and (.w_PDFENCRYPTION=.t. and not empty(.w_OwnerPassword))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOwnerPasswordConf_1_9.SetFocus()
            i_bnoObbl = !empty(.w_OwnerPasswordConf)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Le password inserite sono diverse")
          case   not(.w_UserPassword = .w_UserPasswordConf)  and (.w_PDFENCRYPTION=.t. and not empty(.w_UserPassword))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUserPasswordConf_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Le password inserite sono diverse")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RANGE = this.w_RANGE
    this.o_NOTOPENVIEWER = this.w_NOTOPENVIEWER
    this.o_AUTHOR = this.w_AUTHOR
    this.o_TITLE = this.w_TITLE
    this.o_SUBJECT = this.w_SUBJECT
    this.o_KEYWORDS = this.w_KEYWORDS
    this.o_PDFENCRYPTION = this.w_PDFENCRYPTION
    this.o_OwnerPassword = this.w_OwnerPassword
    this.o_UserPassword = this.w_UserPassword
    this.o_PrintDocument = this.w_PrintDocument
    this.o_ModifyDocument = this.w_ModifyDocument
    this.o_CopyTextAndGraphics = this.w_CopyTextAndGraphics
    this.o_AddOrModifyAnnotations = this.w_AddOrModifyAnnotations
    this.o_Archive = this.w_Archive
    this.o_Additive = this.w_Additive
    this.o_DeleteFileAfter = this.w_DeleteFileAfter
    this.o_cAdditive = this.w_cAdditive
    this.o_cNOTOPENVIEWER = this.w_cNOTOPENVIEWER
    this.o_cPDFENCRYPTION = this.w_cPDFENCRYPTION
    this.o_cPrintDocument = this.w_cPrintDocument
    this.o_cModifyDocument = this.w_cModifyDocument
    this.o_cCopyTextAndGraphics = this.w_cCopyTextAndGraphics
    this.o_cAddOrModifyAnnotations = this.w_cAddOrModifyAnnotations
    this.o_cDeleteFileAfter = this.w_cDeleteFileAfter
    return

enddefine

* --- Define pages as container
define class tgsut_kxfPag1 as StdContainer
  Width  = 660
  height = 382
  stdWidth  = 660
  stdheight = 382
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRANGE_1_1 as StdField with uid="KCEUYBNUHN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RANGE", cQueryName = "RANGE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Intervallo di stampa",;
    HelpContextID = 206380266,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=156, Top=7, InputMask=replicate('X',254)

  add object oNOTOPENVIEWER_1_2 as StdCheck with uid="YQANDIVQKG",rtseq=2,rtrep=.f.,left=436, top=7, caption="Apri dopo la generazione",;
    ToolTipText = "Apre il file dopo la generazione con l'applicazione associata",;
    HelpContextID = 248908476,;
    cFormVar="w_NOTOPENVIEWER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOTOPENVIEWER_1_2.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oNOTOPENVIEWER_1_2.GetRadio()
    this.Parent.oContained.w_NOTOPENVIEWER = this.RadioValue()
    return .t.
  endfunc

  func oNOTOPENVIEWER_1_2.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_NOTOPENVIEWER==.T.,1,;
      0)
  endfunc

  add object oAUTHOR_1_3 as StdField with uid="DKUSPXJCXE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_AUTHOR", cQueryName = "AUTHOR",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Autore",;
    HelpContextID = 106190342,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=111, Top=62, InputMask=replicate('X',254)

  func oAUTHOR_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ABILITA)
    endwith
   endif
  endfunc

  add object oTITLE_1_4 as StdField with uid="AZRBMZTQGC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TITLE", cQueryName = "TITLE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Titolo",;
    HelpContextID = 206025930,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=111, Top=92, InputMask=replicate('X',254)

  func oTITLE_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ABILITA)
    endwith
   endif
  endfunc

  add object oSUBJECT_1_5 as StdField with uid="OFHIIPOQHZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SUBJECT", cQueryName = "SUBJECT",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Soggetto",;
    HelpContextID = 112539430,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=111, Top=122, InputMask=replicate('X',254)

  func oSUBJECT_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ABILITA)
    endwith
   endif
  endfunc

  add object oKEYWORDS_1_6 as StdField with uid="MMYQGVNUZY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_KEYWORDS", cQueryName = "KEYWORDS",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Parole chiave",;
    HelpContextID = 107190009,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=111, Top=152, InputMask=replicate('X',254)

  func oKEYWORDS_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ABILITA)
    endwith
   endif
  endfunc

  add object oPDFENCRYPTION_1_7 as StdCheck with uid="SHSVVHPKSB",rtseq=7,rtrep=.f.,left=303, top=63, caption="Abilita protezione",;
    ToolTipText = "Abilita la sicurezza sul file",;
    HelpContextID = 208949071,;
    cFormVar="w_PDFENCRYPTION", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPDFENCRYPTION_1_7.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oPDFENCRYPTION_1_7.GetRadio()
    this.Parent.oContained.w_PDFENCRYPTION = this.RadioValue()
    return .t.
  endfunc

  func oPDFENCRYPTION_1_7.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PDFENCRYPTION==.t.,1,;
      0)
  endfunc

  func oPDFENCRYPTION_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Upper(alltrim(.w_FORMAT))== 'PDF')
    endwith
   endif
  endfunc

  add object oOwnerPassword_1_8 as StdField with uid="THSVGPFHQA",rtseq=8,rtrep=.f.,;
    cFormVar = "w_OwnerPassword", cQueryName = "OwnerPassword",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Password del proprietario",;
    HelpContextID = 224167817,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=500, Top=112, InputMask=replicate('X',254)

  func oOwnerPassword_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PDFENCRYPTION=.t.)
    endwith
   endif
  endfunc

  add object oOwnerPasswordConf_1_9 as StdField with uid="WSGSOSLXDP",rtseq=9,rtrep=.f.,;
    cFormVar = "w_OwnerPasswordConf", cQueryName = "OwnerPasswordConf",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Le password inserite sono diverse",;
    ToolTipText = "Password del proprietario",;
    HelpContextID = 262369705,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=500, Top=135, InputMask=replicate('X',254)

  func oOwnerPasswordConf_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PDFENCRYPTION=.t. and not empty(.w_OwnerPassword))
    endwith
   endif
  endfunc

  func oOwnerPasswordConf_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_OwnerPasswordConf = .w_OwnerPassword)
    endwith
    return bRes
  endfunc

  add object oUserPassword_1_10 as StdField with uid="IBQPXIVQOF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_UserPassword", cQueryName = "UserPassword",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Password dell'utente",;
    HelpContextID = 100342825,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=500, Top=165, InputMask=replicate('X',254)

  func oUserPassword_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PDFENCRYPTION=.t.)
    endwith
   endif
  endfunc

  add object oUserPasswordConf_1_11 as StdField with uid="XAHRNQBONR",rtseq=11,rtrep=.f.,;
    cFormVar = "w_UserPasswordConf", cQueryName = "UserPasswordConf",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Le password inserite sono diverse",;
    ToolTipText = "Password dell'utente",;
    HelpContextID = 114615153,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=500, Top=188, InputMask=replicate('X',254)

  func oUserPasswordConf_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PDFENCRYPTION=.t. and not empty(.w_UserPassword))
    endwith
   endif
  endfunc

  func oUserPasswordConf_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_UserPassword = .w_UserPasswordConf)
    endwith
    return bRes
  endfunc

  add object oPrintDocument_1_12 as StdCheck with uid="FYDQSVIDEI",rtseq=12,rtrep=.f.,left=303, top=231, caption="Stampa del documento",;
    ToolTipText = "Rende stampabile il file dall'utente",;
    HelpContextID = 41978025,;
    cFormVar="w_PrintDocument", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPrintDocument_1_12.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oPrintDocument_1_12.GetRadio()
    this.Parent.oContained.w_PrintDocument = this.RadioValue()
    return .t.
  endfunc

  func oPrintDocument_1_12.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PrintDocument==.t.,1,;
      0)
  endfunc

  func oPrintDocument_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PDFENCRYPTION=.t.)
    endwith
   endif
  endfunc

  add object oModifyDocument_1_13 as StdCheck with uid="GWFOHXNUYH",rtseq=13,rtrep=.f.,left=303, top=252, caption="Modifica del documento",;
    ToolTipText = "Rende modificabile il file dall'utente",;
    HelpContextID = 171097189,;
    cFormVar="w_ModifyDocument", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oModifyDocument_1_13.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oModifyDocument_1_13.GetRadio()
    this.Parent.oContained.w_ModifyDocument = this.RadioValue()
    return .t.
  endfunc

  func oModifyDocument_1_13.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ModifyDocument==.t.,1,;
      0)
  endfunc

  func oModifyDocument_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PDFENCRYPTION=.t.)
    endwith
   endif
  endfunc

  add object oCopyTextAndGraphics_1_14 as StdCheck with uid="HBTRFGBGWG",rtseq=14,rtrep=.f.,left=303, top=273, caption="Selezione di testo e immagini",;
    ToolTipText = "Permette all'utente di copiare il testo del file",;
    HelpContextID = 38567074,;
    cFormVar="w_CopyTextAndGraphics", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCopyTextAndGraphics_1_14.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oCopyTextAndGraphics_1_14.GetRadio()
    this.Parent.oContained.w_CopyTextAndGraphics = this.RadioValue()
    return .t.
  endfunc

  func oCopyTextAndGraphics_1_14.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CopyTextAndGraphics==.t.,1,;
      0)
  endfunc

  func oCopyTextAndGraphics_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PDFENCRYPTION=.t.)
    endwith
   endif
  endfunc

  add object oAddOrModifyAnnotations_1_15 as StdCheck with uid="RRCYZWNQJY",rtseq=15,rtrep=.f.,left=303, top=294, caption="Aggiunta o modifica di note e campi modulo",;
    ToolTipText = "Permette all'utente di aggiungere/modificare le annotazioni",;
    HelpContextID = 51080318,;
    cFormVar="w_AddOrModifyAnnotations", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAddOrModifyAnnotations_1_15.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oAddOrModifyAnnotations_1_15.GetRadio()
    this.Parent.oContained.w_AddOrModifyAnnotations = this.RadioValue()
    return .t.
  endfunc

  func oAddOrModifyAnnotations_1_15.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AddOrModifyAnnotations==.t.,1,;
      0)
  endfunc

  func oAddOrModifyAnnotations_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PDFENCRYPTION=.t.)
    endwith
   endif
  endfunc

  add object oArchive_1_16 as StdField with uid="PAPDLCHGWM",rtseq=16,rtrep=.f.,;
    cFormVar = "w_Archive", cQueryName = "Archive",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome dello zip, completo di path, che conterr� il file da generare",;
    HelpContextID = 65707258,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=84, Top=247, InputMask=replicate('X',254)


  add object oObj_1_17 as cp_askfile with uid="IOSZOIAMBB",left=233, top=247, width=19,height=22,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_ARCHIVE",;
    nPag=1;
    , ToolTipText = "Premere per selezionare l'archivio di destinazione";
    , HelpContextID = 15086122

  add object oAdditive_1_18 as StdCheck with uid="DESNAPTEHB",rtseq=17,rtrep=.f.,left=84, top=273, caption="Archivio gi� esistente",;
    ToolTipText = "Aggiunge il file ad un archivio gi� esistente",;
    HelpContextID = 3775125,;
    cFormVar="w_Additive", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAdditive_1_18.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oAdditive_1_18.GetRadio()
    this.Parent.oContained.w_Additive = this.RadioValue()
    return .t.
  endfunc

  func oAdditive_1_18.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_Additive==.t.,1,;
      0)
  endfunc

  func oAdditive_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_Archive))
    endwith
   endif
  endfunc

  add object oDeleteFileAfter_1_19 as StdCheck with uid="YHMKRRZYOP",rtseq=18,rtrep=.f.,left=84, top=296, caption="Cancella dopo l'archiviazione",;
    ToolTipText = "Cancella il file generato dopo l'archiviazione",;
    HelpContextID = 141386335,;
    cFormVar="w_DeleteFileAfter", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDeleteFileAfter_1_19.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oDeleteFileAfter_1_19.GetRadio()
    this.Parent.oContained.w_DeleteFileAfter = this.RadioValue()
    return .t.
  endfunc

  func oDeleteFileAfter_1_19.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_DeleteFileAfter==.t.,1,;
      0)
  endfunc

  func oDeleteFileAfter_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_Archive) and !.w_NOTOPENVIEWER)
    endwith
   endif
  endfunc


  add object oBtn_1_36 as StdButton with uid="BKUECBXPSF",left=553, top=332, width=48,height=45,;
    CpPicture="bmp\save.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per salvare su file";
    , HelpContextID = 75658774;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_PDFENCRYPTION=.f. or (.w_PDFENCRYPTION=.t. and .w_OwnerPasswordConf == .w_OwnerPassword and .w_UserPasswordConf == .w_UserPassword))
      endwith
    endif
  endfunc


  add object oBtn_1_37 as StdButton with uid="OFLKLPHFIY",left=605, top=332, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza salvare";
    , HelpContextID = 75658774;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_53 as cp_setobjprop with uid="AUWPJKIPCT",left=3, top=386, width=204,height=19,;
    caption='OwnerPassword',;
   bGlobalFont=.t.,;
    cObj="w_OwnerPassword",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 224167817


  add object oObj_1_54 as cp_setobjprop with uid="PAOYGCRXIJ",left=3, top=404, width=204,height=19,;
    caption='OwnerPasswordConf',;
   bGlobalFont=.t.,;
    cObj="w_OwnerPasswordConf",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 262369705


  add object oObj_1_55 as cp_setobjprop with uid="PTGVDZSOSH",left=3, top=422, width=204,height=19,;
    caption='UserPassword',;
   bGlobalFont=.t.,;
    cObj="w_UserPassword",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 100342825


  add object oObj_1_56 as cp_setobjprop with uid="QKJPXEWCCW",left=3, top=440, width=204,height=19,;
    caption='UserPasswordConf',;
   bGlobalFont=.t.,;
    cObj="w_UserPasswordConf",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 114615153

  add object oStr_1_20 as StdString with uid="EURJLAGEOV",Visible=.t., Left=9, Top=7,;
    Alignment=1, Width=145, Height=18,;
    Caption="Intervallo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="NNMZGDWGNW",Visible=.t., Left=14, Top=62,;
    Alignment=1, Width=94, Height=18,;
    Caption="Autore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="AZRPCXHIGD",Visible=.t., Left=14, Top=92,;
    Alignment=1, Width=94, Height=18,;
    Caption="Titolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="BDECNLRKUK",Visible=.t., Left=14, Top=122,;
    Alignment=1, Width=94, Height=18,;
    Caption="Soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ORUYUZAIDL",Visible=.t., Left=14, Top=152,;
    Alignment=1, Width=94, Height=18,;
    Caption="Parole chiave:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="KBRDBFXHQV",Visible=.t., Left=283, Top=39,;
    Alignment=0, Width=75, Height=17,;
    Caption="Opzioni PDF"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_27 as StdString with uid="BMFFWDFQIW",Visible=.t., Left=11, Top=247,;
    Alignment=1, Width=73, Height=18,;
    Caption="Archivio zip:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="MMEGHLPNJJ",Visible=.t., Left=306, Top=9,;
    Alignment=0, Width=74, Height=18,;
    Caption="( Es. 1, 5 )"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_29 as StdString with uid="XLSKWXICKI",Visible=.t., Left=289, Top=112,;
    Alignment=1, Width=209, Height=18,;
    Caption="Modificare le opzioni di protezione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="QJDWJPGDZO",Visible=.t., Left=373, Top=165,;
    Alignment=1, Width=125, Height=18,;
    Caption="Aprire il documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="UCUEFPSYLS",Visible=.t., Left=10, Top=224,;
    Alignment=0, Width=109, Height=17,;
    Caption="Opzioni archiviazione"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_34 as StdString with uid="XCKZTXCWDT",Visible=.t., Left=11, Top=39,;
    Alignment=0, Width=127, Height=17,;
    Caption="Informazioni generali"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="NHIJKFHPYY",Visible=.t., Left=290, Top=216,;
    Alignment=1, Width=86, Height=16,;
    Caption="Non consentito:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_39 as StdString with uid="IMMXAFTUEA",Visible=.t., Left=283, Top=95,;
    Alignment=1, Width=137, Height=16,;
    Caption="Specifica password per:"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_51 as StdString with uid="ITWKFXAMKU",Visible=.t., Left=366, Top=135,;
    Alignment=1, Width=132, Height=18,;
    Caption="Conferma password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="PZSRRQDDBS",Visible=.t., Left=366, Top=188,;
    Alignment=1, Width=132, Height=18,;
    Caption="Conferma password:"  ;
  , bGlobalFont=.t.

  add object oBox_1_25 as StdBox with uid="WNIIVFUWZV",left=280, top=55, width=373,height=269

  add object oBox_1_31 as StdBox with uid="JYATNGITAF",left=10, top=240, width=264,height=84

  add object oBox_1_33 as StdBox with uid="YFPRDTETAJ",left=11, top=55, width=264,height=126
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kxf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
