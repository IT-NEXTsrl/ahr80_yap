* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bbf                                                        *
*              CONTROLLI SUI CAMPI F24                                         *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-06                                                      *
* Last revis.: 2013-06-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,TIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bbf",oParentObject,m.TIPO)
return(i_retval)

define class tgscg_bbf as StdBatch
  * --- Local variables
  TIPO = space(2)
  w_MESS = space(200)
  w_ERRORE = .f.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_NUMERR = 0
  w_NUMERR2 = 0
  w_NUMERR3 = 0
  w_NUMERR4 = 0
  w_ALFANUM = .f.
  w_oMESS2 = .NULL.
  w_oPART2 = .NULL.
  w_oMESS3 = .NULL.
  w_oPART3 = .NULL.
  w_oMESS4 = .NULL.
  w_oPART4 = .NULL.
  w_oMESS5 = .NULL.
  w_oPART5 = .NULL.
  w_INSTESTATA = .f.
  w_RIGCONTR = 0
  w_DAREMSG = .f.
  w_CODTRIB = space(60)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per il controllo sull'obbligatoriet� dei dati relativi al Modello F24
    this.w_oMESS=createobject("AH_MESSAGE")
    this.w_NUMERR = 0
    this.w_NUMERR2 = 0
    this.w_NUMERR3 = 0
    this.w_NUMERR4 = 0
    do case
      case this.TIPO="DC"
        this.w_ALFANUM = .F.
        this.w_oPART = this.w_oMESS.AddMsgPartNL("Sez. contribuente:")
        this.w_ERRORE = .F.
        * --- Controllo campi obbligatori relativi al contribuente
        if EMPTY(this.oParentObject.w_CFCODFIS)
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Manca il codice fiscale")
          this.w_ERRORE = .T.
        endif
        * --- Controolo se il C.F. � alfanumerico
        if ! empty(this.oParentObject.w_CFCODFIS)
          FOR I = 1 TO LEN(this.oParentObject.w_CFCODFIS)
          CARATTERE=SUBSTR(this.oParentObject.w_CFCODFIS,I,1)
          this.w_ALFANUM = ISALPHA(CARATTERE)
          if this.w_ALFANUM
            EXIT
          endif
          ENDFOR
        endif
        if (EMPTY(this.oParentObject.w_CFLOCNAS) OR EMPTY(this.oParentObject.w_CFPRONAS)) and ((this.w_ALFANUM and not empty(this.oParentObject.w_CFCODFIS))) and !this.w_ERRORE
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Manca il comune e/o la provincia di nascita nei dati relativi al contribuente")
          this.w_ERRORE = .T.
        endif
        if this.oParentObject.w_CFPERFIS="S"
          if EMPTY(this.oParentObject.w_CFRAGSOC) 
            this.w_oPART = this.w_oMESS.AddMsgPartNL("Manca il cognome nei dati relativi al contribuente")
            this.w_ERRORE = .T.
          endif
        else
          if empty(this.oParentObject.w_CFRAGSOC) 
            this.w_oPART = this.w_oMESS.AddMsgPartNL("Manca la ragione sociale")
            this.w_ERRORE = .T.
          endif
        endif
        if empty(this.oParentObject.w_CFINDIRI) OR EMPTY(this.oParentObject.w_CFLOCALI) OR EMPTY(this.oParentObject.w_CFPROVIN)
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Mancano alcuni dati relativi al domicilio fiscale")
          this.w_ERRORE = .T.
        endif
        if this.w_ERRORE
          this.oParentObject.w_RESCHK=-1
          this.w_oMESS.AH_ERRORMSG()     
          i_retcode = 'stop'
          return
        endif
      case this.TIPO="MP"
        * --- dati INAIL
        * --- Campi relativi alla sezione 'Altri Enti'
        * --- Campi relativi a sezione Regioni
        * --- --
        this.w_oMESS2=createobject("AH_MESSAGE")
        * --- --
        this.w_oMESS3=createobject("AH_MESSAGE")
        * --- --
        this.w_oMESS4=createobject("AH_MESSAGE")
        * --- --
        * --- --
        this.w_oPART = this.w_oMESS.AddMsgPartNL("Sez. INPS:")
        this.w_oPART2 = this.w_oMESS2.AddMsgPartNL("Sez. INAIL:")
        this.w_oPART3 = this.w_oMESS3.AddMsgPartNL("Sez. altri enti:")
        this.w_oPART4 = this.w_oMESS4.AddMsgPartNL("Sez. regioni:")
        this.w_ERRORE = .F.
        * --- Variabili per effettuare un controllo selettivo per i dati relativi alla sez. Altri Enti
        this.w_INSTESTATA = .F.
        this.w_RIGCONTR = 0
        * --- Controllo obbligatoriet� dati INPS
        I="1"
        do while VAL(I) <= 4
          if NOT EMPTY(this.oParentObject.w_MFCCONT&I) 
            do case
              case INLIST(ALLTRIM(this.oParentObject.w_MFCCONT&I),"AD","AF","AFP","AP","API","APP","APMF","APR","AR","ARI","ARIN","ARM","ARMN","ARN","LAS","LASP","LASS","LPMF","RC01","RLAA") AND NOT ALLTRIM(this.oParentObject.w_MFCCONT&I)=="ART1"
                if (val(this.oParentObject.w_MF_AMES&I)=0) OR (val(this.oParentObject.w_MFAN&I)=0) OR (val(this.oParentObject.w_MFDAMES&I)=0) OR (val(this.oParentObject.w_MFDAANN&I)=0) or empty(this.oParentObject.w_MF_AMES&I) OR empty(this.oParentObject.w_MFAN&I) OR empty(this.oParentObject.w_MFDAMES&I) OR empty(this.oParentObject.w_MFDAANN&I)
                  this.w_oPART = this.w_oMESS.AddMsgPartNL("Selezionare un mese valido (MM) e/o un anno (AAAA) per la causale contributo %1")
                  this.w_oPART.AddParam(this.oParentObject.w_MFCCONT&I)     
                  this.w_ERRORE = .T.
                  this.w_NUMERR = 1
                endif
              case INLIST(ALLTRIM(this.oParentObject.w_MFCCONT&I),"CD","CF","CFP","CP","CPI","CPMF","CPP","CPR","CR","CRI","CRIN","CRM","CRMN","CRN","MOBL","P10","P10R","PSCO","RLAS","RPCF")
                if (val(this.oParentObject.w_MF_AMES&I)=0) OR (val(this.oParentObject.w_MFAN&I)=0) OR (val(this.oParentObject.w_MFDAMES&I)=0) OR (val(this.oParentObject.w_MFDAANN&I)=0) or empty(this.oParentObject.w_MF_AMES&I) OR empty(this.oParentObject.w_MFAN&I) OR empty(this.oParentObject.w_MFDAMES&I) OR empty(this.oParentObject.w_MFDAANN&I)
                  this.w_oPART = this.w_oMESS.AddMsgPartNL( "Selezionare un mese valido (MM) e/o un anno (AAAA) per la causale contributo %1" )
                  this.w_oPART.AddParam(this.oParentObject.w_MFCCONT&I)     
                  this.w_ERRORE = .T.
                  this.w_NUMERR = 1
                endif
              case INLIST(ALLTRIM(this.oParentObject.w_MFCCONT&I),"DINS","DMAC","DMDD","DMDP","DMV","DOM1","DOM3","DPP","DPPI","DPPR","KLAA","KLAS","KPCF","PXX","PXXR","RARC","SSDD","SSDP")
                if (val(this.oParentObject.w_MF_AMES&I)=0) OR (val(this.oParentObject.w_MFAN&I)=0) OR (val(this.oParentObject.w_MFDAMES&I)=0) OR (val(this.oParentObject.w_MFDAANN&I)=0) or empty(this.oParentObject.w_MF_AMES&I) OR empty(this.oParentObject.w_MFAN&I) OR empty(this.oParentObject.w_MFDAMES&I) OR empty(this.oParentObject.w_MFDAANN&I)
                  this.w_oPART = this.w_oMESS.AddMsgPartNL( "Selezionare un mese valido (MM) e/o un anno (AAAA) per la causale contributo %1" )
                  this.w_oPART.AddParam(this.oParentObject.w_MFCCONT&I)     
                  this.w_ERRORE = .T.
                  this.w_NUMERR = 1
                endif
              case INLIST(ALLTRIM(this.oParentObject.w_MFCCONT&I),"C10","CXX","DM10","DPC","EBCM","EBTU","EMCO","EMDM","LAA","LAAP","PCF","PCFP","PCFS","PESC")
                if (val(this.oParentObject.w_MFDAMES&I)=0) OR (val(this.oParentObject.w_MFDAANN&I)=0) or empty(this.oParentObject.w_MFDAMES&I) OR empty(this.oParentObject.w_MFDAANN&I)
                  this.w_oPART = this.w_oMESS.AddMsgPartNL( "Selezionare un mese valido (MM) e/o un anno valido (AAAA) nella data iniziale per la causale contributo %1" )
                  this.w_oPART.AddParam(this.oParentObject.w_MFCCONT&I)     
                  this.w_ERRORE = .T.
                  this.w_NUMERR = 1
                endif
                * --- Metto a zero la data di fine periodo
                this.oParentObject.w_MF_AMES&I="0"
                this.oParentObject.w_MFAN&I="0"
              case INLIST(ALLTRIM(this.oParentObject.w_MFCCONT&I),"EMLA","FIPP","AGRU","ASS","DOM4","DMRA") AND NOT (ALLTRIM(this.oParentObject.w_MFCCONT&I)=="ASSP" )
                if (val(this.oParentObject.w_MFDAMES&I)=0) OR (val(this.oParentObject.w_MFDAANN&I)=0) or empty(this.oParentObject.w_MFDAMES&I) OR empty(this.oParentObject.w_MFDAANN&I)
                  this.w_oPART = this.w_oMESS.AddMsgPartNL( "Selezionare un mese valido (MM) e/o un anno valido (AAAA) nella data iniziale per la causale contributo %1" )
                  this.w_oPART.AddParam(this.oParentObject.w_MFCCONT&I)     
                  this.w_ERRORE = .T.
                  this.w_NUMERR = 1
                endif
                * --- Metto a zero la data di fine periodo
                this.oParentObject.w_MF_AMES&I="0"
                this.oParentObject.w_MFAN&I="0"
            endcase
            if empty(this.oParentObject.w_MFMINPS&I)
              this.w_oPART = this.w_oMESS.AddMsgPartNL( "Manca la matricola INPS per la causale contributo %1" )
              this.w_oPART.AddParam(this.oParentObject.w_MFCCONT&I)     
              this.w_ERRORE = .T.
              this.w_NUMERR = 1
            endif
          endif
          I=ALLTRIM(STR(VAL(I)+1))
        enddo
        * --- Controllo obbligatoriet� dati INAIL
        I="1"
        do while VAL(I) <= 3
          if NOT EMPTY(this.oParentObject.w_MFSINAI&I) AND (EMPTY(this.oParentObject.w_MF_NPOS&I) OR EMPTY(this.oParentObject.w_MF_PACC&I) OR EMPTY(this.oParentObject.w_MF_NRIF&I) OR EMPTY(this.oParentObject.w_MFCAUSA&I))
            this.w_oPART2 = this.w_oMESS2.AddMsgPartNL( "Manca il numero della posizione assicurativa e/o il codice di controllo %1 e/o il n. riferimento e/o la causale per il codice sede %1" )
            this.w_oPART2.AddParam(this.oParentObject.w_MFSINAI&I)     
            this.w_ERRORE = .T.
            this.w_NUMERR2 = 1
          endif
          I=ALLTRIM(STR(VAL(I)+1))
        enddo
        * --- Controllo obbligatoriet� dati 'Altri Enti'
        I="1"
        if UPPER (this.oparentobject.class)<>"TGSCG_AON"
          do while VAL(I) <= 2
            do case
              case ! EMPTY(this.oParentObject.w_MFCDENTE) AND this.oParentObject.w_MFCDENTE<>"0002" AND (EMPTY(this.oParentObject.w_MFCCOAE&I) or EMPTY(this.oParentObject.w_MFSDENT&I)) 
                * --- L'if successivo serve per non inserire pi� volte la stessa stringa
                if ! this.w_INSTESTATA AND (val(I)=1 or (!EMPTY(this.oParentObject.w_MFCCOAE&I) or !EMPTY(this.oParentObject.w_MFSDENT&I)))
                  if this.oParentObject.w_MFCDENTE="0003" OR this.oParentObject.w_MFCDENTE="0005" OR this.oParentObject.w_MFCDENTE="0006"
                    this.w_oPART3 = this.w_oMESS3.AddMsgPartNL( "Manca il cod. provincia e/o la causale contributo" )
                  else
                    this.w_oPART3 = this.w_oMESS3.AddMsgPartNL( "Manca il cod. sede e/o la causale contributo" )
                  endif
                  this.w_INSTESTATA = .T.
                  this.w_NUMERR3 = 1
                endif
                if this.w_RIGCONTR=0 OR (!EMPTY(this.oParentObject.w_MFCCOAE&I) or !EMPTY(this.oParentObject.w_MFSDENT&I))
                  this.w_ERRORE = .T.
                endif
              case ALLTRIM(this.oParentObject.w_MFCDENTE)="0002" AND EMPTY(this.oParentObject.w_MFCCOAE&I) AND this.w_RIGCONTR=0
                if ! this.w_INSTESTATA
                  this.w_oPART3 = this.w_oMESS3.AddMsgPartNL("Manca la causale contributo")
                  this.w_INSTESTATA = .T.
                  this.w_NUMERR3 = 1
                endif
                this.w_ERRORE = .T.
            endcase
            if NOT EMPTY(this.oParentObject.w_MFCCOAE&I) 
              if EMPTY(this.oParentObject.w_MFCDPOS&I) and this.oParentObject.w_MFCDENTE<>"0003"
                this.w_oPART3 = this.w_oMESS3.AddMsgPartNL( "Manca il cod. posizione assicurativa per la causale contributo %1" )
                this.w_oPART3.AddParam(this.oParentObject.w_MFCCOAE&I)     
                this.w_ERRORE = .T.
                this.w_NUMERR3 = 1
              endif
              if INLIST(ALLTRIM(this.oParentObject.w_MFCCOAE&I),"RCLS","RCSP","60CO","61SP","63MO","64MM","INT","RV","SA","SB","SC","SNZ","VP","VT")
                if (val(this.oParentObject.w_MFMSINE&I)=0) OR (val(this.oParentObject.w_MFANINE&I)=0) OR (val(this.oParentObject.w_MFMSFIE&I)=0) OR (val(this.oParentObject.w_MFANF&I)=0) or empty(this.oParentObject.w_MFMSINE&I) OR empty(this.oParentObject.w_MFANINE&I) OR empty(this.oParentObject.w_MFMSFIE&I) OR empty(this.oParentObject.w_MFANF&I)
                  this.w_oPART3 = this.w_oMESS3.AddMsgPartNL( "Selezionare un mese valido (MM) e/o un anno (AAAA) per la causale contributo %1" )
                  this.w_oPART3.AddParam(this.oParentObject.w_MFCCOAE&I)     
                  this.w_ERRORE = .T.
                  this.w_NUMERR3 = 1
                endif
              endif
              if INLIST(ALLTRIM(this.oParentObject.w_MFCCOAE&I),"CALS","CCLS","CCSP")
                if (val(this.oParentObject.w_MFMSINE&I)=0) OR (val(this.oParentObject.w_MFANINE&I)=0) or empty(this.oParentObject.w_MFMSINE&I) OR empty(this.oParentObject.w_MFANINE&I)
                  this.w_oPART3 = this.w_oMESS3.AddMsgPartNL( "Selezionare un mese valido (MM) e/o un anno valido (AAAA) nella data iniziale per la causale contributo %1" )
                  this.w_oPART3.AddParam(this.oParentObject.w_MFCCOAE&I)     
                  this.w_ERRORE = .T.
                  this.w_NUMERR3 = 1
                endif
                * --- Metto a zero la data di fine periodo
                this.oParentObject.w_MFMSFIE&I="0"
                this.oParentObject.w_MFANF&I="0"
              endif
              this.w_RIGCONTR = this.w_RIGCONTR + 1
            endif
            I=ALLTRIM(STR(VAL(I)+1))
          enddo
        endif
        * --- Controllo obbligatoriet� dati 'Regioni'
        I="1"
        if vartype(this.oParentObject.w_CHKOBBME1) = "C"
          do while VAL(I) <= 4
            if this.oParentObject.w_CHKOBBME&I="S"
              if empty(this.oParentObject.w_MFMESER&I)
                this.w_oPART4 = this.w_oMESS4.AddMsgPartNL("Attenzione: non � stato valorizzato il campo 'Mese di riferimento' per il codice tributo: %1")
                this.w_oPART4.AddParam(this.oParentObject.w_MFTRIRE&I)     
                this.w_ERRORE = .T.
                this.w_NUMERR4 = 1
              endif
              if ! empty(this.oParentObject.w_MFRATRE&I)
                this.w_oPART4 = this.w_oMESS4.AddMsgPartNL("Attenzione: errata valorizzazione del campo 'rateazione' in corrispondenza del codice tributo: %1")
                this.w_oPART4.AddParam(this.oParentObject.w_MFTRIRE&I)     
                this.w_ERRORE = .T.
                this.w_NUMERR4 = 1
              endif
            endif
            I=ALLTRIM(STR(VAL(I)+1))
          enddo
        endif
        if this.w_ERRORE
          this.oParentObject.w_RESCHK=-1
          if this.w_NUMERR>0
            this.w_oMESS.AH_ERRORMSG()     
          endif
          if this.w_NUMERR2>0
            this.w_oMESS2.AH_ERRORMSG()     
          endif
          if this.w_NUMERR3>0
            this.w_oMESS3.AH_ERRORMSG()     
          endif
          if this.w_NUMERR4>0
            this.w_oMESS4.AH_ERRORMSG()     
          endif
          i_retcode = 'stop'
          return
        endif
      case this.TIPO="BA"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.TIPO="IC"
        * --- Controllo immobili variati per tributo ICI
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.TIPO="ER"
        * --- Controllo obbligatoriet� dati 'Sezione erario'
        this.w_ERRORE = .F.
        this.w_oMESS5=createobject("AH_MESSAGE")
        this.w_oPART5 = this.w_oMESS5.AddMsgPartNL("Sez. erario:")
        if vartype(this.oParentObject.w_CHKOBBME1)="C"
          I="1"
          do while VAL(I) <= 6
            if this.oParentObject.w_CHKOBBME&I="S"
              if empty(this.oParentObject.w_EFMESER&I)
                this.w_oPART5 = this.w_oMESS5.AddMsgPartNL("Attenzione: non � stato valorizzato il campo 'Mese di riferimento' per il codice tributo: %1")
                this.w_oPART5.AddParam(this.oParentObject.w_EFTRIER&I)     
                this.w_ERRORE = .T.
              endif
              if ! empty(this.oParentObject.w_EFRATER&I)
                this.w_oPART5 = this.w_oMESS5.AddMsgPartNL("Attenzione: errata valorizzazione del campo 'rateazione' in corrispondenza del codice tributo: %1")
                this.w_oPART5.AddParam(this.oParentObject.w_EFTRIER&I)     
                this.w_ERRORE = .T.
              endif
            endif
            I=ALLTRIM(STR(VAL(I)+1))
          enddo
        endif
        if this.w_ERRORE
          this.oParentObject.w_RESCHK = -1
          this.w_oMESS5.AH_ERRORMSG()     
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli sulla obbligatoriet� relativa ai codici ABI e CAB banca passiva
    this.w_ERRORE = .F.
    if ((empty(this.oParentObject.w_VFCODTES) OR EMPTY(this.oParentObject.w_VFCABTES)) and g_TESO="S") OR ((EMPTY(this.oParentObject.w_VFCABBAN) OR EMPTY(this.oParentObject.w_VFCODBAN)) AND g_TESO<>"S")
      this.w_MESS = "Sezione estremi versamento: manca codice ABI e/o CAB banca passiva"
      this.w_ERRORE = .T.
    endif
    if this.w_ERRORE
      this.oParentObject.w_RESCHK=-1
      ah_errormsg(this.w_MESS) 
 
      i_retcode = 'stop'
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo che siano valorizzzati i campi "immobili variati" per i tributi ICI
    this.w_DAREMSG = .F.
    this.w_CODTRIB = space(25)+"- "
    if INLIST (this.oParentObject.w_IFTRIEL1,"3901","3902","3903","3904","3905") and empty(this.oParentObject.w_IFNUMFA1)
      this.w_DAREMSG = .T.
      this.w_CODTRIB = this.w_CODTRIB+this.oParentObject.w_IFTRIEL1+" - "
    endif
    if INLIST (this.oParentObject.w_IFTRIEL2,"3901","3902","3903","3904","3905") and empty(this.oParentObject.w_IFNUMFA2)
      this.w_DAREMSG = .T.
      this.w_CODTRIB = this.w_CODTRIB+this.oParentObject.w_IFTRIEL2+" - "
    endif
    if INLIST (this.oParentObject.w_IFTRIEL3,"3901","3902","3903","3904","3905") and empty(this.oParentObject.w_IFNUMFA3)
      this.w_DAREMSG = .T.
      this.w_CODTRIB = this.w_CODTRIB+this.oParentObject.w_IFTRIEL3+" - "
    endif
    if INLIST (this.oParentObject.w_IFTRIEL4,"3901","3902","3903","3904","3905") and empty(this.oParentObject.w_IFNUMFA4)
      this.w_DAREMSG = .T.
      this.w_CODTRIB = this.w_CODTRIB+this.oParentObject.w_IFTRIEL4+" - "
    endif
    if this.w_DAREMSG
      AH_ERRORMSG("Non � stato impostato il campo numero immobili nei seguenti cod. tributo: %1",48,,chr(13)+this.w_CODTRIB)
    endif
  endproc


  proc Init(oParentObject,TIPO)
    this.TIPO=TIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="TIPO"
endproc
