* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_brt                                                        *
*              Ricostruzione saldi commessa                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-01-15                                                      *
* Last revis.: 2018-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_brt",oParentObject)
return(i_retval)

define class tgsma_brt as StdBatch
  * --- Local variables
  w_COMMDEFA = space(15)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_CODART = space(20)
  GSMA_KRS = .NULL.
  w_FLDELSLD = space(1)
  w_CODARTIN = space(20)
  w_CODARTFI = space(20)
  w_Msg = .NULL.
  w_EMPTYCOM = space(15)
  * --- WorkFile variables
  PAR_PROD_idx=0
  CAN_TIER_idx=0
  SALDICOM_idx=0
  TMPSALDI_idx=0
  SALOTCOM_idx=0
  TMPSALAGG_idx=0
  SALDIART_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- -Ricostruzione saldi di commessa (SALDICOM-SALOTCOM)
    *     -Creazione commessa di default
    this.GSMA_KRS = This.oParentObject.oParentObject
    this.w_FLDELSLD = this.GSMA_KRS.w_FLDELSLD
    this.w_CODARTIN = this.GSMA_KRS.w_CODARTIN
    this.w_CODARTFI = this.GSMA_KRS.w_CODARTFI
    this.w_Msg = this.GSMA_KRS.w_Msg
    * --- Prima fase
    *     -creo una commessa di default, se esiste la aggiorno
    * --- Leggo la commessa di default dai parametri produzione
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCODCOM"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCODCOM;
        from (i_cTable) where;
            PPCODICE = "PP";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COMMDEFA = NVL(cp_ToDate(_read_.PPCODCOM),cp_NullValue(_read_.PPCODCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(nvl(this.w_COMMDEFA,""))
      this.w_COMMDEFA = "ZZDEFAULT"
      * --- Write into PAR_PROD
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_PROD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PPCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_COMMDEFA),'PAR_PROD','PPCODCOM');
            +i_ccchkf ;
        +" where ";
            +"PPCODICE = "+cp_ToStrODBC("PP");
               )
      else
        update (i_cTable) set;
            PPCODCOM = this.w_COMMDEFA;
            &i_ccchkf. ;
         where;
            PPCODICE = "PP";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Se la commessa di default non � vuota elimino il saldo in modo da non lasciare dati incongruenti
      this.w_EMPTYCOM = SPACE(15)
      * --- Delete from SALDICOM
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"SCCODCAN = "+cp_ToStrODBC(this.w_EMPTYCOM);
               )
      else
        delete from (i_cTable) where;
              SCCODCAN = this.w_EMPTYCOM;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    this.w_DATINI = ctod("01/01/1900")
    this.w_DATFIN = ctod("31/12/2099")
    * --- Creo la commessa di default, se non esiste
    * --- Tolgo per sicurezza il default a qualsiasi commessa l'avesse prima della presente procedura (ne devo avere sempre una sola)
    * --- Write into CAN_TIER
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAN_TIER_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CNDFAULT ="+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNDFAULT');
          +i_ccchkf ;
             )
    else
      update (i_cTable) set;
          CNDFAULT = "N";
          &i_ccchkf. ;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Read from CAN_TIER
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" CAN_TIER where ";
            +"CNCODCAN = "+cp_ToStrODBC(this.w_COMMDEFA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            CNCODCAN = this.w_COMMDEFA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_rows>0
      * --- Se la commessa di default esiste gi� allora faccio solo la update del flag
      * --- Write into CAN_TIER
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CAN_TIER_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CNDFAULT ="+cp_NullLink(cp_ToStrODBC("S"),'CAN_TIER','CNDFAULT');
            +i_ccchkf ;
        +" where ";
            +"CNCODCAN = "+cp_ToStrODBC(this.w_COMMDEFA);
               )
      else
        update (i_cTable) set;
            CNDFAULT = "S";
            &i_ccchkf. ;
         where;
            CNCODCAN = this.w_COMMDEFA;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Creo la commessa di default
      * --- Insert into CAN_TIER
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAN_TIER_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CNCODCAN"+",CNDESCAN"+",CNTIPCON"+",CNCODVAL"+",CNDATINI"+",CNDATFIN"+",CNIMPORT"+",CN_STATO"+",CNFLAGIM"+",CNORIGIN"+",CNPARASS"+",CNCOECAL"+",CNCONUNI"+",CNIMPEGN"+",CNALFN01"+",CNALFN02"+",CNPUBWEB"+",CNFLRISE"+",CNTARTEM"+",CNTARCON"+",CNFLPATT"+",CNPERPAT"+",CN_RICAV"+",CNFLESEN"+",CNFLSOSP"+",CNIMPCTR"+",CNIMPBLO"+",CNFLAMPA"+",CNFLRECU"+",CNMATOBB"+",CNASSCTP"+",CNCOMPLX"+",CNPROFMT"+",CNESIPOS"+",CNPERPLX"+",CNPERPOS"+",CNFLVMLQ"+",CNDFAULT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_COMMDEFA),'CAN_TIER','CNCODCAN');
        +","+cp_NullLink(cp_ToStrODBC("Commessa di default"),'CAN_TIER','CNDESCAN');
        +","+cp_NullLink(cp_ToStrODBC("C"),'CAN_TIER','CNTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(g_PERVAL),'CAN_TIER','CNCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATINI),'CAN_TIER','CNDATINI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATFIN),'CAN_TIER','CNDATFIN');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNIMPORT');
        +","+cp_NullLink(cp_ToStrODBC("A"),'CAN_TIER','CN_STATO');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNFLAGIM');
        +","+cp_NullLink(cp_ToStrODBC("P"),'CAN_TIER','CNORIGIN');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNPARASS');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNCOECAL');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNCONUNI');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNIMPEGN');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNALFN01');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNALFN02');
        +","+cp_NullLink(cp_ToStrODBC("S"),'CAN_TIER','CNPUBWEB');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNFLRISE');
        +","+cp_NullLink(cp_ToStrODBC("T"),'CAN_TIER','CNTARTEM');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNTARCON');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNFLPATT');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNPERPAT');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CN_RICAV');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNFLESEN');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNFLSOSP');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNIMPCTR');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNIMPBLO');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNFLAMPA');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNFLRECU');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNMATOBB');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNASSCTP');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNCOMPLX');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNPROFMT');
        +","+cp_NullLink(cp_ToStrODBC("N"),'CAN_TIER','CNESIPOS');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNPERPLX');
        +","+cp_NullLink(cp_ToStrODBC(0),'CAN_TIER','CNPERPOS');
        +","+cp_NullLink(cp_ToStrODBC("S"),'CAN_TIER','CNFLVMLQ');
        +","+cp_NullLink(cp_ToStrODBC("S"),'CAN_TIER','CNDFAULT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CNCODCAN',this.w_COMMDEFA,'CNDESCAN',"Commessa di default",'CNTIPCON',"C",'CNCODVAL',g_PERVAL,'CNDATINI',this.w_DATINI,'CNDATFIN',this.w_DATFIN,'CNIMPORT',0,'CN_STATO',"A",'CNFLAGIM',"N",'CNORIGIN',"P",'CNPARASS',0,'CNCOECAL',0)
        insert into (i_cTable) (CNCODCAN,CNDESCAN,CNTIPCON,CNCODVAL,CNDATINI,CNDATFIN,CNIMPORT,CN_STATO,CNFLAGIM,CNORIGIN,CNPARASS,CNCOECAL,CNCONUNI,CNIMPEGN,CNALFN01,CNALFN02,CNPUBWEB,CNFLRISE,CNTARTEM,CNTARCON,CNFLPATT,CNPERPAT,CN_RICAV,CNFLESEN,CNFLSOSP,CNIMPCTR,CNIMPBLO,CNFLAMPA,CNFLRECU,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNFLVMLQ,CNDFAULT &i_ccchkf. );
           values (;
             this.w_COMMDEFA;
             ,"Commessa di default";
             ,"C";
             ,g_PERVAL;
             ,this.w_DATINI;
             ,this.w_DATFIN;
             ,0;
             ,"A";
             ,"N";
             ,"P";
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,0;
             ,"S";
             ,"N";
             ,"T";
             ,0;
             ,"N";
             ,0;
             ,0;
             ,"N";
             ,"N";
             ,0;
             ,0;
             ,"N";
             ,"N";
             ,"N";
             ,"N";
             ,"N";
             ,"N";
             ,"N";
             ,0;
             ,0;
             ,"S";
             ,"S";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- Seconda fase
    *     -aggiorno i saldi della commessa
    * --- Azzera dati del periodo
    ADDMSGNL("Fase azzeramento saldi commessa  ", this.GSMA_KRS)
    if this.w_FLDELSLD="S"
      * --- Delete from SALDICOM
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
      
        do vq_exec with 'GSMADSLDC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Write into SALDICOM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SCCODICE"
        do vq_exec with 'GSMADSLDC',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTAPER');
        +",SCQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTRPER');
        +",SCQTOPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTOPER');
        +",SCQTIPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTIPER');
            +i_ccchkf;
            +" from "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 set ";
        +"SALDICOM.SCQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTAPER');
        +",SALDICOM.SCQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTRPER');
        +",SALDICOM.SCQTOPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTOPER');
        +",SALDICOM.SCQTIPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTIPER');
            +Iif(Empty(i_ccchkf),"",",SALDICOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="SALDICOM.SCCODICE = t2.SCCODICE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set (";
            +"SCQTAPER,";
            +"SCQTRPER,";
            +"SCQTOPER,";
            +"SCQTIPER";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTAPER')+",";
            +cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTRPER')+",";
            +cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTOPER')+",";
            +cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTIPER')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set ";
        +"SCQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTAPER');
        +",SCQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTRPER');
        +",SCQTOPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTOPER');
        +",SCQTIPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTIPER');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTAPER');
        +",SCQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTRPER');
        +",SCQTOPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTOPER');
        +",SCQTIPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICOM','SCQTIPER');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Aggiorna Saldi commessa
    * --- Drop temporary table TMPSALDI
    i_nIdx=cp_GetTableDefIdx('TMPSALDI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALDI')
    endif
    * --- Estrazione dati
    ADDMSGNL("Fase estrazione dati", this.GSMA_KRS)
    * --- Create temporary table TMPSALAGG
    i_nIdx=cp_AddTableDef('TMPSALAGG') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSMA2CQSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPSALAGG_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if g_PROD="S"
      * --- Dati Produzione  ODL + Movimenti di Magazzino + Documenti
      if g_GPOS="S"
        * --- Create temporary table TMPSALDI
        i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSDB3CQRS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPSALDI_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Create temporary table TMPSALDI
        i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSDBCQRS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPSALDI_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    else
      * --- Movimenti di Magazzino + Documenti
      if g_GPOS="S"
        * --- Create temporary table TMPSALDI
        i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSMA2CQRS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPSALDI_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Create temporary table TMPSALDI
        i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('GSMACQQS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPSALDI_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    endif
    * --- Drop temporary table TMPSALAGG
    i_nIdx=cp_GetTableDefIdx('TMPSALAGG')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALAGG')
    endif
    ADDMSGNL("Fase scrittura saldi commessa", this.GSMA_KRS)
    * --- Insert dati non presenti in SALDICOM
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsmaaqrc",this.SALDICOM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorna dati esistenti in SALDICOM
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSALDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPER = _t2.TOTCASC";
          +",SCQTRPER = _t2.TOTRISE";
          +",SCQTOPER = _t2.TOTORDI";
          +",SCQTIPER = _t2.TOTIMPE";
          +i_ccchkf;
          +" from "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 set ";
          +"SALDICOM.SCQTAPER = _t2.TOTCASC";
          +",SALDICOM.SCQTRPER = _t2.TOTRISE";
          +",SALDICOM.SCQTOPER = _t2.TOTORDI";
          +",SALDICOM.SCQTIPER = _t2.TOTIMPE";
          +Iif(Empty(i_ccchkf),"",",SALDICOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="PostgreSQL"
        i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set ";
          +"SCQTAPER = _t2.TOTCASC";
          +",SCQTRPER = _t2.TOTRISE";
          +",SCQTOPER = _t2.TOTORDI";
          +",SCQTIPER = _t2.TOTIMPE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
              +" and "+i_cTable+".SCCODMAG = "+i_cQueryTable+".SCCODMAG";
              +" and "+i_cTable+".SCCODCAN = "+i_cQueryTable+".SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPER = (select TOTCASC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SCQTRPER = (select TOTRISE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SCQTOPER = (select TOTORDI from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SCQTIPER = (select TOTIMPE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiungo le giacenze fuori linea
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMA8BRS",this.SALDICOM_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SCCODICE,SCCODMAG,SCCODCAN"
      do vq_exec with 'gsmacqrc',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPER = SALDICOM.SCQTAPER+_t2.SCQTAPRO";
          +",SCQTRPER = SALDICOM.SCQTRPER+_t2.SCQTRPRO";
          +",SCQTAPRO = _t2.SCQTAPRO";
          +",SCQTRPRO = _t2.SCQTRPRO";
          +i_ccchkf;
          +" from "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 set ";
          +"SALDICOM.SCQTAPER = SALDICOM.SCQTAPER+_t2.SCQTAPRO";
          +",SALDICOM.SCQTRPER = SALDICOM.SCQTRPER+_t2.SCQTRPRO";
          +",SALDICOM.SCQTAPRO = _t2.SCQTAPRO";
          +",SALDICOM.SCQTRPRO = _t2.SCQTRPRO";
          +Iif(Empty(i_ccchkf),"",",SALDICOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SALDICOM.SCCODICE = t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = t2.SCCODCAN";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set (";
          +"SCQTAPER,";
          +"SCQTRPER,";
          +"SCQTAPRO,";
          +"SCQTRPRO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"SCQTAPER+t2.SCQTAPRO,";
          +"SCQTRPER+t2.SCQTRPRO,";
          +"t2.SCQTAPRO,";
          +"t2.SCQTRPRO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
              +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
              +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set ";
          +"SCQTAPER = SALDICOM.SCQTAPER+_t2.SCQTAPRO";
          +",SCQTRPER = SALDICOM.SCQTRPER+_t2.SCQTRPRO";
          +",SCQTAPRO = _t2.SCQTAPRO";
          +",SCQTRPRO = _t2.SCQTRPRO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
              +" and "+i_cTable+".SCCODMAG = "+i_cQueryTable+".SCCODMAG";
              +" and "+i_cTable+".SCCODCAN = "+i_cQueryTable+".SCCODCAN";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPER = (select "+i_cTable+".SCQTAPER+SCQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SCQTRPER = (select "+i_cTable+".SCQTRPER+SCQTRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SCQTAPRO = (select SCQTAPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",SCQTRPRO = (select SCQTRPRO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Errore aggiunta giacenze fuori linea'
      return
    endif
    if .F.
      * --- Terza fase
      *     -aggiorno i saldi commessa per lotti ed ubicazione (se il modulo � attivo)
      if this.oparentobject.oparentobject.w_AGCLOTUBI ="S"
        if g_MADV="S"
          if g_PERUBI = "S" OR g_PERLOT = "S"
            * --- Aggiornamento saldi commessa lotti/ubicazioni
            * --- Write into SALOTCOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALOTCOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SMQTAPER ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTAPER');
              +",SMQTRPER ="+cp_NullLink(cp_ToStrODBC(0),'SALOTCOM','SMQTRPER');
                  +i_ccchkf ;
              +" where ";
                  +"1 = "+cp_ToStrODBC(1);
                     )
            else
              update (i_cTable) set;
                  SMQTAPER = 0;
                  ,SMQTRPER = 0;
                  &i_ccchkf. ;
               where;
                  1 = 1;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            * --- Aggiorna Saldi commessa
            * --- Drop temporary table TMPSALDI
            i_nIdx=cp_GetTableDefIdx('TMPSALDI')
            if i_nIdx<>0
              cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
              cp_RemoveTableDef('TMPSALDI')
            endif
            if g_GPOS="S"
              * --- Create temporary table TMPSALDI
              i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              vq_exec('GSMA3MQRS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
              this.TMPSALDI_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            else
              * --- Create temporary table TMPSALDI
              i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
              i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
              vq_exec('GSMA0MQRS',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
              this.TMPSALDI_idx=i_nIdx
              i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            endif
            * --- Insert Dati Non Presenti in SALDILOT
            * --- Insert into SALOTCOM
            i_nConn=i_TableProp[this.SALOTCOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSMAIMQRS",this.SALOTCOM_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            * --- Aggiorna Dati Esistenti in SALDILOT
            * --- Write into SALOTCOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALOTCOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSALDI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
                      +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
                      +" and "+"SALOTCOM.SMCODCAN = _t2.SMCODCAN";
                      +" and "+"SALOTCOM.SMCODLOT = _t2.SMCODLOT";
                      +" and "+"SALOTCOM.SMCODUBI = _t2.SMCODUBI";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SMQTAPER = _t2.SMQTAPER";
                  +",SMQTRPER = _t2.SMQTRPER";
                  +i_ccchkf;
                  +" from "+i_cTable+" SALOTCOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
                      +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
                      +" and "+"SALOTCOM.SMCODCAN = _t2.SMCODCAN";
                      +" and "+"SALOTCOM.SMCODLOT = _t2.SMCODLOT";
                      +" and "+"SALOTCOM.SMCODUBI = _t2.SMCODUBI";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM, "+i_cQueryTable+" _t2 set ";
                  +"SALOTCOM.SMQTAPER = _t2.SMQTAPER";
                  +",SALOTCOM.SMQTRPER = _t2.SMQTRPER";
                  +Iif(Empty(i_ccchkf),"",",SALOTCOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="PostgreSQL"
                i_cWhere="SALOTCOM.SMCODART = _t2.SMCODART";
                      +" and "+"SALOTCOM.SMCODMAG = _t2.SMCODMAG";
                      +" and "+"SALOTCOM.SMCODCAN = _t2.SMCODCAN";
                      +" and "+"SALOTCOM.SMCODLOT = _t2.SMCODLOT";
                      +" and "+"SALOTCOM.SMCODUBI = _t2.SMCODUBI";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALOTCOM set ";
                  +"SMQTAPER = _t2.SMQTAPER";
                  +",SMQTRPER = _t2.SMQTRPER";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".SMCODART = "+i_cQueryTable+".SMCODART";
                      +" and "+i_cTable+".SMCODMAG = "+i_cQueryTable+".SMCODMAG";
                      +" and "+i_cTable+".SMCODCAN = "+i_cQueryTable+".SMCODCAN";
                      +" and "+i_cTable+".SMCODLOT = "+i_cQueryTable+".SMCODLOT";
                      +" and "+i_cTable+".SMCODUBI = "+i_cQueryTable+".SMCODUBI";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SMQTAPER = (select SMQTAPER from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",SMQTRPER = (select SMQTRPER from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='SALDICOM'
    this.cWorkTables[4]='*TMPSALDI'
    this.cWorkTables[5]='SALOTCOM'
    this.cWorkTables[6]='*TMPSALAGG'
    this.cWorkTables[7]='SALDIART'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
