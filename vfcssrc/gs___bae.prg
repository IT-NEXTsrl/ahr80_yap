* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gs___bae                                                        *
*              CAMBIA CONTESTO                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-03                                                      *
* Last revis.: 2017-02-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_EntParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgs___bae",oParentObject,m.w_EntParam)
return(i_retval)

define class tgs___bae as StdBatch
  * --- Local variables
  w_EntParam = space(3)
  w_ESCENABLED = .f.
  w_OLDAZI = space(5)
  b_VALUTE = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CAMBIA CONTESTO
    this.w_EntParam = IIF(Type("w_EntParam")<>"C","XXX", this.w_EntParam)
    if this.w_EntParam<>"AVV"
      Createobject("deferred_login")
      i_retcode = 'stop'
      return
    endif
    this.w_OLDAZI = i_codazi
    do gs___kbe with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.b_VALUTE = CAVALUTE()
    * --- desktop menu
    if VarType(i_oDeskmenu)="O"
      i_oDeskmenu.bLoadedMenu=.F.
    endif
    cp_backgroundmask()
    * --- Lettura variabili pubblice relative ai moduli
    do GSUT_BRP with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    CP_MENU()
    * --- RevInfinity
    if IsRevi()
      revi_ddeserver()
    endif
    * --- Sistemo toolbar
    if Vartype(odesktopbar)="O"
      odesktopbar.SetAfterCpToolbar()
    endif
    if Vartype(oCpToolbar)="O" and not isnull(oCpToolbar)
      oCpToolbar.ChangeSettings()
    endif
    if not(this.w_OLDAZI==i_codazi) And Vartype(bEnableAHService)="L" And bEnableAHService
      * --- Riavvio il Servizio
      ah_startservice()
    endif
  endproc


  proc Init(oParentObject,w_EntParam)
    this.w_EntParam=w_EntParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- gs___bae
  Enddefine
  
  Define Class deferred_login As Timer
      * --- solito trucco per poter eseguire delle operazioni in ritardo
      Interval=200
      oThis=.Null.
      Proc Init()
          This.oThis=This
      Proc Timer()
          do GS___BAE with .F.,'AVV'
          This.oThis=.Null.
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_EntParam"
endproc
