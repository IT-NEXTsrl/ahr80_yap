* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sri                                                        *
*              Stampa registri IVA                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_64]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-23                                                      *
* Last revis.: 2014-08-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sri",oParentObject))

* --- Class definition
define class tgscg_sri as StdForm
  Top    = 20
  Left   = 65

  * --- Standard Properties
  Width  = 457
  Height = 270
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-08-06"
  HelpContextID=160815767
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gscg_sri"
  cComment = "Stampa registri IVA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_CODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_TIPREG = space(1)
  w_NUMREG = 0
  w_CODATT = space(5)
  w_ANNO = space(4)
  w_NUMPER = 0
  o_NUMPER = 0
  w_DESCRI = space(30)
  w_FINPER = ctod('  /  /  ')
  w_ULTDAT = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_FLDEFI = space(1)
  w_STAREG = space(1)
  w_FLTEST = space(1)
  w_FLSTOT = space(1)
  o_FLSTOT = space(1)
  w_FLNOALI = space(1)
  w_DATFI2 = ctod('  /  /  ')
  w_INTLIG = space(1)
  w_PRPARI = 0
  w_PREFIS = space(20)
  w_RAGCOR = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sriPag1","gscg_sri",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPREG_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='AZIENDA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_sri
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_CODESE=space(4)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_TIPREG=space(1)
      .w_NUMREG=0
      .w_CODATT=space(5)
      .w_ANNO=space(4)
      .w_NUMPER=0
      .w_DESCRI=space(30)
      .w_FINPER=ctod("  /  /  ")
      .w_ULTDAT=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_FLDEFI=space(1)
      .w_STAREG=space(1)
      .w_FLTEST=space(1)
      .w_FLSTOT=space(1)
      .w_FLNOALI=space(1)
      .w_DATFI2=ctod("  /  /  ")
      .w_INTLIG=space(1)
      .w_PRPARI=0
      .w_PREFIS=space(20)
      .w_RAGCOR=space(1)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_CODESE = g_CODESE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODESE))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_TIPREG = 'V'
        .w_NUMREG = 1
          .DoRTCalc(7,7,.f.)
        .w_ANNO = ALLTRIM(STR(YEAR(i_DATSYS),4,0))
          .DoRTCalc(9,9,.f.)
        .w_DESCRI = IIF(.w_NUMPER>0, CALCPER(.w_NUMPER, g_TIPDEN), SPACE(30))
          .DoRTCalc(11,14,.f.)
        .w_FLDEFI = 'N'
          .DoRTCalc(16,16,.f.)
        .w_FLTEST = ' '
        .w_FLSTOT = 'S'
        .w_FLNOALI = ' '
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .w_DATFI2 = .w_DATFIN+15
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
    endwith
    this.DoRTCalc(21,24,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_CODAZI<>.w_CODAZI
          .link_1_1('Full')
        endif
          .link_1_2('Full')
        .DoRTCalc(3,9,.t.)
        if .o_NUMPER<>.w_NUMPER
            .w_DESCRI = IIF(.w_NUMPER>0, CALCPER(.w_NUMPER, g_TIPDEN), SPACE(30))
        endif
        .DoRTCalc(11,18,.t.)
        if .o_FLSTOT<>.w_FLSTOT
            .w_FLNOALI = ' '
        endif
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
            .w_DATFI2 = .w_DATFIN+15
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLSTOT_1_18.enabled = this.oPgFrm.Page1.oPag.oFLSTOT_1_18.mCond()
    this.oPgFrm.Page1.oPag.oPRPARI_1_38.enabled = this.oPgFrm.Page1.oPag.oPRPARI_1_38.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLNOALI_1_19.visible=!this.oPgFrm.Page1.oPag.oFLNOALI_1_19.mHide()
    this.oPgFrm.Page1.oPag.oRAGCOR_1_42.visible=!this.oPgFrm.Page1.oPag.oRAGCOR_1_42.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZSTAREG,AZRAGCOR";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZSTAREG,AZRAGCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_STAREG = NVL(_Link_.AZSTAREG,space(1))
      this.w_RAGCOR = NVL(_Link_.AZRAGCOR,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_STAREG = space(1)
      this.w_RAGCOR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPREG_1_5.RadioValue()==this.w_TIPREG)
      this.oPgFrm.Page1.oPag.oTIPREG_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMREG_1_6.value==this.w_NUMREG)
      this.oPgFrm.Page1.oPag.oNUMREG_1_6.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_8.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_8.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPER_1_9.value==this.w_NUMPER)
      this.oPgFrm.Page1.oPag.oNUMPER_1_9.value=this.w_NUMPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_10.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_10.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oULTDAT_1_12.value==this.w_ULTDAT)
      this.oPgFrm.Page1.oPag.oULTDAT_1_12.value=this.w_ULTDAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_13.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_13.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_14.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_14.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDEFI_1_15.RadioValue()==this.w_FLDEFI)
      this.oPgFrm.Page1.oPag.oFLDEFI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAREG_1_16.RadioValue()==this.w_STAREG)
      this.oPgFrm.Page1.oPag.oSTAREG_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTEST_1_17.RadioValue()==this.w_FLTEST)
      this.oPgFrm.Page1.oPag.oFLTEST_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSTOT_1_18.RadioValue()==this.w_FLSTOT)
      this.oPgFrm.Page1.oPag.oFLSTOT_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNOALI_1_19.RadioValue()==this.w_FLNOALI)
      this.oPgFrm.Page1.oPag.oFLNOALI_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPARI_1_38.value==this.w_PRPARI)
      this.oPgFrm.Page1.oPag.oPRPARI_1_38.value=this.w_PRPARI
    endif
    if not(this.oPgFrm.Page1.oPag.oPREFIS_1_40.value==this.w_PREFIS)
      this.oPgFrm.Page1.oPag.oPREFIS_1_40.value=this.w_PREFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGCOR_1_42.RadioValue()==this.w_RAGCOR)
      this.oPgFrm.Page1.oPag.oRAGCOR_1_42.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_NUMREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMREG_1_6.SetFocus()
            i_bnoObbl = !empty(.w_NUMREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ANNO)) or not(VAL(.w_ANNO)>1900))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_8.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NUMPER)) or not(.w_NUMPER<5 OR (.w_NUMPER<13 AND g_TIPDEN="M")))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMPER_1_9.SetFocus()
            i_bnoObbl = !empty(.w_NUMPER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero periodo non congruente")
          case   (empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_14.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_NUMPER = this.w_NUMPER
    this.o_FLSTOT = this.w_FLSTOT
    return

enddefine

* --- Define pages as container
define class tgscg_sriPag1 as StdContainer
  Width  = 453
  height = 270
  stdWidth  = 453
  stdheight = 270
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPREG_1_5 as StdCombo with uid="ARGSWPJTSO",rtseq=5,rtrep=.f.,left=105,top=11,width=157,height=21;
    , ToolTipText = "Tipo registro IVA selezionato";
    , HelpContextID = 87894326;
    , cFormVar="w_TIPREG",RowSource=""+"Vendite,"+"Acquisti,"+"Corr.scorporo,"+"Corr.ventilazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPREG_1_5.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,'E',;
    space(1))))))
  endfunc
  func oTIPREG_1_5.GetRadio()
    this.Parent.oContained.w_TIPREG = this.RadioValue()
    return .t.
  endfunc

  func oTIPREG_1_5.SetRadio()
    this.Parent.oContained.w_TIPREG=trim(this.Parent.oContained.w_TIPREG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPREG=='V',1,;
      iif(this.Parent.oContained.w_TIPREG=='A',2,;
      iif(this.Parent.oContained.w_TIPREG=='C',3,;
      iif(this.Parent.oContained.w_TIPREG=='E',4,;
      0))))
  endfunc

  add object oNUMREG_1_6 as StdField with uid="DRHWZSEZDP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registro IVA selezionato",;
    HelpContextID = 87885014,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=414, Top=11, cSayPict='"99"', cGetPict='"99"'

  add object oANNO_1_8 as StdField with uid="DITKXJLBMK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento dei saldi IVA da calcolare",;
    HelpContextID = 166333702,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=105, Top=37, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oANNO_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ANNO)>1900)
    endwith
    return bRes
  endfunc

  add object oNUMPER_1_9 as StdField with uid="GNUQORRVDZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_NUMPER", cQueryName = "NUMPER",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero periodo non congruente",;
    ToolTipText = "Periodo: 1..4 per trimestrale; 1..12 per mensile",;
    HelpContextID = 3867862,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=105, Top=64, cSayPict='"99"', cGetPict='"99"'

  func oNUMPER_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMPER<5 OR (.w_NUMPER<13 AND g_TIPDEN="M"))
    endwith
    return bRes
  endfunc

  add object oDESCRI_1_10 as StdField with uid="KINVSYYWJX",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 134108214,;
   bGlobalFont=.t.,;
    Height=21, Width=266, Left=140, Top=64, InputMask=replicate('X',30)

  add object oULTDAT_1_12 as StdField with uid="EWQTFOMHJM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ULTDAT", cQueryName = "ULTDAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di stampa del registro IVA selezionato",;
    HelpContextID = 32468038,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=105, Top=145

  add object oDATINI_1_13 as StdField with uid="NJHANDKUHN",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Usata nella query per selezionare le date di registrazione",;
    HelpContextID = 130310198,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=105, Top=91

  add object oDATFIN_1_14 as StdField with uid="CWRCHIOAWF",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Usata nel batch per filtrare le date di registrazione",;
    HelpContextID = 208756790,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=105, Top=118


  add object oFLDEFI_1_15 as StdCombo with uid="MFGVVLHQVS",rtseq=15,rtrep=.f.,left=286,top=91,width=122,height=21;
    , ToolTipText = "Tipo di elaborazione: se definitiva, aggiorna l'ultima data di stampa dei registri";
    , HelpContextID = 121596758;
    , cFormVar="w_FLDEFI",RowSource=""+"Definitiva,"+"In prova", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLDEFI_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oFLDEFI_1_15.GetRadio()
    this.Parent.oContained.w_FLDEFI = this.RadioValue()
    return .t.
  endfunc

  func oFLDEFI_1_15.SetRadio()
    this.Parent.oContained.w_FLDEFI=trim(this.Parent.oContained.w_FLDEFI)
    this.value = ;
      iif(this.Parent.oContained.w_FLDEFI=='S',1,;
      iif(this.Parent.oContained.w_FLDEFI=='N',2,;
      0))
  endfunc


  add object oSTAREG_1_16 as StdCombo with uid="ODCBMECLIH",rtseq=16,rtrep=.f.,left=286,top=118,width=122,height=21;
    , ToolTipText = "Formato dei moduli di stampa";
    , HelpContextID = 87835686;
    , cFormVar="w_STAREG",RowSource=""+"Orizzontale,"+"Verticale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTAREG_1_16.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'V',;
    space(1))))
  endfunc
  func oSTAREG_1_16.GetRadio()
    this.Parent.oContained.w_STAREG = this.RadioValue()
    return .t.
  endfunc

  func oSTAREG_1_16.SetRadio()
    this.Parent.oContained.w_STAREG=trim(this.Parent.oContained.w_STAREG)
    this.value = ;
      iif(this.Parent.oContained.w_STAREG=='O',1,;
      iif(this.Parent.oContained.w_STAREG=='V',2,;
      0))
  endfunc

  add object oFLTEST_1_17 as StdCheck with uid="DXYVVIOBTQ",rtseq=17,rtrep=.f.,left=286, top=143, caption="Stampa solo testo",;
    ToolTipText = "Se attivo: stampa su modulo solo testo",;
    HelpContextID = 51407702,;
    cFormVar="w_FLTEST", bObbl = .f. , nPag = 1;
    , TabStop=.F.;
   , bGlobalFont=.t.


  func oFLTEST_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLTEST_1_17.GetRadio()
    this.Parent.oContained.w_FLTEST = this.RadioValue()
    return .t.
  endfunc

  func oFLTEST_1_17.SetRadio()
    this.Parent.oContained.w_FLTEST=trim(this.Parent.oContained.w_FLTEST)
    this.value = ;
      iif(this.Parent.oContained.w_FLTEST=='S',1,;
      0)
  endfunc

  add object oFLSTOT_1_18 as StdCheck with uid="ROBZUKFXMU",rtseq=18,rtrep=.f.,left=286, top=167, caption="Stampa totali",;
    ToolTipText = "Se attivo: stampa anche i saldi IVA del periodo selezionato",;
    HelpContextID = 48192342,;
    cFormVar="w_FLSTOT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSTOT_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLSTOT_1_18.GetRadio()
    this.Parent.oContained.w_FLSTOT = this.RadioValue()
    return .t.
  endfunc

  func oFLSTOT_1_18.SetRadio()
    this.Parent.oContained.w_FLSTOT=trim(this.Parent.oContained.w_FLSTOT)
    this.value = ;
      iif(this.Parent.oContained.w_FLSTOT=='S',1,;
      0)
  endfunc

  func oFLSTOT_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DATFIN=.w_FINPER)
    endwith
   endif
  endfunc

  add object oFLNOALI_1_19 as StdCheck with uid="BMTSPDBAAM",rtseq=19,rtrep=.f.,left=286, top=191, caption="Stampa aliquote mov.",;
    ToolTipText = "Se attivo stampa solo le aliquote movimentate nel periodo",;
    HelpContextID = 101053610,;
    cFormVar="w_FLNOALI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLNOALI_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLNOALI_1_19.GetRadio()
    this.Parent.oContained.w_FLNOALI = this.RadioValue()
    return .t.
  endfunc

  func oFLNOALI_1_19.SetRadio()
    this.Parent.oContained.w_FLNOALI=trim(this.Parent.oContained.w_FLNOALI)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOALI=='S',1,;
      0)
  endfunc

  func oFLNOALI_1_19.mHide()
    with this.Parent.oContained
      return (.w_FLSTOT<>'S')
    endwith
  endfunc


  add object oBtn_1_22 as StdButton with uid="SENKBIZSYY",left=347, top=219, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare l'elaborazione";
    , HelpContextID = 160844518;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        do GSCG_BRI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="BYDIHEHRWY",left=396, top=219, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 168133190;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_24 as cp_runprogram with uid="ESTGJZMWKG",left=0, top=306, width=276,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCG_BCR("R")',;
    cEvent = "w_TIPREG Changed,w_NUMREG Changed,Init",;
    nPag=1;
    , HelpContextID = 198057498


  add object oObj_1_27 as cp_runprogram with uid="BUWBAZLXOK",left=1, top=332, width=259,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCG_BCR("P")',;
    cEvent = "w_ANNO Changed,w_NUMPER Changed",;
    nPag=1;
    , HelpContextID = 198057498


  add object oObj_1_31 as cp_runprogram with uid="IRJPNHTWYI",left=1, top=352, width=161,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCG_BCR("I")',;
    cEvent = "w_DATINI Changed",;
    nPag=1;
    , HelpContextID = 198057498


  add object oObj_1_32 as cp_runprogram with uid="CBVZBJDXYI",left=1, top=372, width=161,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCG_BCR("F")',;
    cEvent = "w_DATFIN Changed",;
    nPag=1;
    , HelpContextID = 198057498


  add object oObj_1_33 as cp_runprogram with uid="CONJQDNCBZ",left=1, top=393, width=161,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCG_BCR("Z")',;
    cEvent = "w_FLDEFI Changed",;
    nPag=1;
    , HelpContextID = 198057498

  add object oPRPARI_1_38 as StdField with uid="GZVSBCMEQD",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PRPARI", cQueryName = "PRPARI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultima pagina stampata nel precedente registro IVA",;
    HelpContextID = 133968374,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=105, Top=172, cSayPict='"9999999"', cGetPict='"9999999"', tabstop=.f.

  func oPRPARI_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INTLIG='S')
    endwith
   endif
  endfunc

  add object oPREFIS_1_40 as StdField with uid="SJBUMXHJGU",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PREFIS", cQueryName = "PREFIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso che comparirÓ nella stampa prima del numero di pagina",;
    HelpContextID = 24150518,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=105, Top=199, InputMask=replicate('X',20)

  add object oRAGCOR_1_42 as StdCheck with uid="UIGHOJGCBY",rtseq=24,rtrep=.f.,left=105, top=226, caption="Raggruppa corrispettivi", enabled=.f.,;
    ToolTipText = "Se attivo, effettua il raggruppamento dei corrispettivi giornalieri in fase di stampa registri IVA",;
    HelpContextID = 13472022,;
    cFormVar="w_RAGCOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRAGCOR_1_42.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oRAGCOR_1_42.GetRadio()
    this.Parent.oContained.w_RAGCOR = this.RadioValue()
    return .t.
  endfunc

  func oRAGCOR_1_42.SetRadio()
    this.Parent.oContained.w_RAGCOR=trim(this.Parent.oContained.w_RAGCOR)
    this.value = ;
      iif(this.Parent.oContained.w_RAGCOR=='S',1,;
      0)
  endfunc

  func oRAGCOR_1_42.mHide()
    with this.Parent.oContained
      return (.w_TIPREG$'VA')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="MEREFFIYQA",Visible=.t., Left=5, Top=146,;
    Alignment=1, Width=98, Height=15,;
    Caption="Ultima stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="WRMIESLCDW",Visible=.t., Left=312, Top=11,;
    Alignment=1, Width=99, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="PUHMGWINIU",Visible=.t., Left=14, Top=37,;
    Alignment=1, Width=89, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="RDYUWVGEWE",Visible=.t., Left=14, Top=64,;
    Alignment=1, Width=89, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="RXKNOHLYVI",Visible=.t., Left=14, Top=91,;
    Alignment=1, Width=89, Height=15,;
    Caption="Dalla data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="YFTYYCRCQJ",Visible=.t., Left=14, Top=118,;
    Alignment=1, Width=89, Height=15,;
    Caption="Alla data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="IZCTKFJGKB",Visible=.t., Left=195, Top=118,;
    Alignment=1, Width=89, Height=15,;
    Caption="Modulo stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="ZRWOXBUQOU",Visible=.t., Left=195, Top=91,;
    Alignment=1, Width=89, Height=15,;
    Caption="Elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="JKCGCPZJDW",Visible=.t., Left=14, Top=11,;
    Alignment=1, Width=89, Height=15,;
    Caption="Tipo registro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="FWNKOATCCI",Visible=.t., Left=8, Top=173,;
    Alignment=1, Width=95, Height=15,;
    Caption="Ultima pagina:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="QBIQLOVEJE",Visible=.t., Left=7, Top=199,;
    Alignment=1, Width=96, Height=18,;
    Caption="Pref. num. pag.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sri','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
