* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bgd                                                        *
*              Genera mov. ad esig.differita                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_193]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-27                                                      *
* Last revis.: 2017-02-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bgd",oParentObject)
return(i_retval)

define class tgscg_bgd as StdBatch
  * --- Local variables
  w_NUMFAT = space(50)
  w_TIPMOV = space(1)
  w_BLOCCO = .f.
  w_TIPIVA = space(1)
  w_OLDSERIAL = space(10)
  w_TOTCONTIVA = 0
  w_TIPO = space(1)
  w_OLDTIPO = space(1)
  w_ROWNUM = 0
  w_DATREG = ctod("  /  /  ")
  w_STALIG = ctod("  /  /  ")
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_APPVAL = 0
  w_DATBLO = ctod("  /  /  ")
  w_APPSEZ = space(1)
  w_FINESE = ctod("  /  /  ")
  w_MESS = space(50)
  w_APPO = space(10)
  w_NUDOC = 0
  w_ASDOC = 0
  w_FLERR = .f.
  w_OSERIAL = space(10)
  w_OTIPDOC = space(2)
  w_OKDOC = 0
  MES = space(10)
  w_PNSERIAL = space(10)
  w_PNCODCLF = space(15)
  w_PNFLIVDF = space(1)
  w_IVCODIVA = space(5)
  w_PNNUMRER = 0
  w_PNTIPCLF = space(1)
  w_PNCODCAU = space(5)
  w_IVCODCON = space(15)
  w_IVCONTRO = space(15)
  w_PNANNDOC = space(4)
  w_CCCONIVA = space(15)
  w_IVPERIND = 0
  w_PNINICOM = ctod("  /  /  ")
  w_PNFLREGI = space(1)
  w_CCCONDIF = space(15)
  w_IVTIPREG = 0
  w_PNFINCOM = ctod("  /  /  ")
  w_PNFLPROV = space(1)
  w_PARTSN = space(1)
  w_IVNUMREG = 0
  w_PNFLABAN = space(6)
  w_PNTIPDOC = space(2)
  w_CAONAZ = 0
  w_IVIMPONI = 0
  w_CPROWNUM = 0
  w_PNDATREG = ctod("  /  /  ")
  w_DECTOP = 0
  w_IVIMPIVA = 0
  w_CPROWORD = 0
  w_PNCODUTE = 0
  w_PNTOTDOC = 0
  w_IVFLOMAG = space(1)
  w_PNNUMREG = 0
  w_PNPRG = space(8)
  w_SERDOC = space(10)
  w_PNIMPDAR = 0
  w_PNTIPREG = space(1)
  w_PNPRP = space(2)
  w_PNTIPCON = space(1)
  w_PNIMPAVE = 0
  w_PNVALNAZ = space(3)
  w_PNPRD = space(2)
  w_PNCODCON = space(15)
  w_PNFLZERO = space(1)
  w_PNCODVAL = space(3)
  w_PNANNPRO = space(4)
  w_PTDATSCA = ctod("  /  /  ")
  w_CODPAG = space(5)
  w_PNDESSUP = space(50)
  w_PNNUMDOC = 0
  w_PTNUMPAR = space(31)
  w_PNFLPART = space(1)
  w_PNCAOVAL = 0
  w_PNALFDOC = space(10)
  w_PTCODVAL = space(3)
  w_VALORI = space(3)
  w_PNCODPAG = space(5)
  w_PNCODESE = space(4)
  w_PNDATDOC = ctod("  /  /  ")
  w_CAOAPE = 0
  w_PTTOTIMP = 0
  w_PNCOMPET = space(4)
  w_PNALFPRO = space(10)
  w_IMPINC = 0
  w_PTIMPDOC = 0
  w_PNCOMIVA = ctod("  /  /  ")
  w_PNNUMPRO = 0
  w_IMPDOC = 0
  w_PTMODPAG = space(10)
  w_SERIAL = space(10)
  w_TIPSCA = 0
  w_NUMORI = 0
  w_FLPDOC = space(1)
  w_FLPPRO = space(1)
  w_ROWORI = 0
  w_ROWACC = 0
  w_IMPACC = 0
  w_CONTA = 0
  w_TOTIVA = 0
  w_CCDESRIG = space(254)
  w_CCDESSUP = space(254)
  w_MAXROW = 0
  w_TOTSAL = 0
  w_OLDCODCAU = space(5)
  w_RESTO = 0
  w_TIPREG = space(1)
  w_DATAFILTRO = ctod("  /  /  ")
  w_DPIMPINC = 0
  w_ANIVASOS = space(1)
  w_DATAFATTURA = ctod("  /  /  ")
  w_CCFLAUTR = space(1)
  w_ANNODATAREG = space(4)
  w_ANNODATAFAT = space(4)
  w_TRIM_INCASSO = 0
  w_TRIM_FATTURA = 0
  w_PNRIFDIS = space(10)
  w_PNFLGDIF = space(1)
  w_SERIAL_ORIG = space(10)
  w_ROWORD_ORIG = 0
  w_RIGA_ORIG = 0
  w_SERIAL_INC = space(10)
  w_ROWORD_INC = 0
  w_RIGA_INC = 0
  w_IMPSCA = 0
  w_DATSCI = ctod("  /  /  ")
  w_DATSCF = ctod("  /  /  ")
  w_PNDESRIG = space(50)
  w_ROWORD = 0
  w_ROWPAR = 0
  w_NUMRIF = 0
  w_CODAGE = space(5)
  w_FLVABD = space(1)
  w_TOTIMP = 0
  w_TOTPAR = 0
  w_TIPREG1 = space(1)
  * --- WorkFile variables
  AZIENDA_idx=0
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  PNT_IVA_idx=0
  SALDICON_idx=0
  PAR_TITE_idx=0
  VALUTE_idx=0
  ESI_DETT_idx=0
  ESI_DIF_idx=0
  CAU_CONT_idx=0
  TMPPNT_MAST_idx=0
  TMP_PART_APE_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Movimenti di Rilevazione Incassi/Pagamenti ad Esigib.Differita (da GSCG_KGD)
    this.w_DATINI = this.oParentObject.w_EDDATINI
    this.w_DATFIN = this.oParentObject.w_EDDATFIN
    this.w_SERIAL = this.oParentObject.w_EDSERIAL
    * --- Array Messaggi
    DIMENSION MES[18],ARPARAM[13,2]
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_FINESE = g_FINESE
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    * --- blocco Prima Nota
    * --- Try
    local bErr_05208A98
    bErr_05208A98=bTrsErr
    this.Try_05208A98()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      do case
        case i_ErrMsg="giornale"
          ah_ErrorMsg("Prima nota bloccata - semaforo bollati in dati azienda -. Operazione annullata",,"")
        case i_ErrMsg=""
          ah_ErrorMsg("Impossibile bloccare la prima nota - semaforo bollati in dati azienda -. Operazione annullata.",,"")
      endcase
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_05208A98
    * --- End
    * --- Legge Date di Stampa e Blocco per i vari Registri
    vq_exec("query\GSCG2BGD.VQR",this,"REG_IVA")
    * --- Legge Incassi/Pagamenti da Generare
    ah_Msg("Ricerca incassi/pagamenti...",.T.)
    this.w_TIPMOV = this.oParentObject.w_EDTIPMOV
    this.oParentObject.w_EDTIPMOV = "E"
    * --- Create temporary table TMPPNT_MAST
    i_nIdx=cp_AddTableDef('TMPPNT_MAST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_RJZRDCPPJW[6]
    indexes_RJZRDCPPJW[1]='SERDOC'
    indexes_RJZRDCPPJW[2]='ROWORI'
    indexes_RJZRDCPPJW[3]='NUMORI'
    indexes_RJZRDCPPJW[4]='SERINC'
    indexes_RJZRDCPPJW[5]='ROWINC'
    indexes_RJZRDCPPJW[6]='NUMINC'
    vq_exec('query\GSCG_BGD.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_RJZRDCPPJW,.f.)
    this.TMPPNT_MAST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Controllo se esistono partite presenti in distinta che sono insolute.
    *     Effettuo questo controllo solo se il modulo "Solleciti" � attivo. Nel caso
    *     siano presenti incassi per quelle fatture devo aggiungere i nuovi incassi
    if "SOLL" $ UPPER(i_cModules)
      * --- Insert into TMPPNT_MAST
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"Gscg8bgd",this.TMPPNT_MAST_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Delete from TMPPNT_MAST
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".SERINC = "+i_cQueryTable+".SERINC";
              +" and "+i_cTable+".SERDOC = "+i_cQueryTable+".SERDOC";
              +" and "+i_cTable+".ROWORI = "+i_cQueryTable+".ROWORI";
              +" and "+i_cTable+".NUMORI = "+i_cQueryTable+".NUMORI";
              +" and "+i_cTable+".ROWINC = "+i_cQueryTable+".ROWINC";
              +" and "+i_cTable+".NUMINC = "+i_cQueryTable+".NUMINC";
      
        do vq_exec with 'Gscg7bgd',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Elimino gli incassi che non sono all'interno del periodo specificato
    *     in anagrafica "Piano generazione movimenti ad esigibilit� differita"
    * --- Delete from TMPPNT_MAST
    i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".SERINC = "+i_cQueryTable+".SERINC";
    
      do vq_exec with 'Gscg10bgd',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Se � stato attivato il filtro sulla tipologia distinte, devo verificare che non
    *     esistano incassi da distinta incongruenti con il filtro impostato
    if this.oParentObject.w_EDTIPDIS $ "APN"
      * --- Delete from TMPPNT_MAST
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".SERINC = "+i_cQueryTable+".SERINC";
      
        do vq_exec with 'Gscg9bgd',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Controllo se l'utente ha attivato il flag "Considera scadenze in distinta come esposte"
    *     in questo caso devo eliminare tutte gli incassi che hanno data scadenza
    *     pi� giorni di tolleranza maggiori della data di generazione
    if this.oParentObject.w_EDSCADIS="S"
      * --- Delete from TMPPNT_MAST
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".SERINC = "+i_cQueryTable+".SERINC";
      
        do vq_exec with 'Gscg6bgd',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Elimino le scadenze che hanno data scadenza non compresa
      *     nell'intervallo. Se sono presenti pi� scadenze all'interno dello stesso
      *     movimento di primanota generato viene filtrata la data scadenza massima.
      *     Effettuo questa operazione perch� il movimento di primanota � unico
      *     e quindi il flag di esagibilit� differita pu� essere messo solo una volta 
      * --- Delete from TMPPNT_MAST
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".SERINC = "+i_cQueryTable+".SERINC";
      
        do vq_exec with 'Gscg13bgd',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    if this.w_TIPMOV<>"E"
      * --- Nel momento in cui viene generato un giroconto iva ad esigibilit� differita sulla registrazione di pagamento\incasso 
      *     che ha dato origine al giroconto viene valorizzato a S il campo di testata PNFLGDIF.
      *     Nel caso di registrazione di compensazione che salda contemporaneamente la fattura di vendita e la fattura di acquisto
      *     generando separatamente il piano ad esigibilit� differita per l'incasso e successivamente per il pagamento (o viceversa) 
      *     la procedura non riesce a generare due giroconti in quanto lanciando il secondo piano, il flag PNFLGDIF � gi� valorizzato a S.
      vq_exec("query\GSCG_BGD_19.VQR",this,"CONTROLLO")
      if RECCOUNT("CONTROLLO")<>0
        if ah_YesNo("Generazione fallita.%0Stampo situazione messaggi di errore?")
          SELECT * FROM CONTROLLO INTO CURSOR __TMP__
          CP_CHPRN("QUERY\GSCG_BGD_19.FRX", " ", this)
        endif
        this.oParentObject.w_EDTIPMOV = this.w_TIPMOV
        this.w_BLOCCO = .T.
      else
        if this.w_TIPMOV="C"
          * --- Delete from TMPPNT_MAST
          i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"TIPCON = "+cp_ToStrODBC("F");
                   )
          else
            delete from (i_cTable) where;
                  TIPCON = "F";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        else
          * --- Delete from TMPPNT_MAST
          i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"TIPCON = "+cp_ToStrODBC("C");
                   )
          else
            delete from (i_cTable) where;
                  TIPCON = "C";

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      endif
      this.oParentObject.w_EDTIPMOV = this.w_TIPMOV
    endif
    this.w_SERIAL_ORIG = Space(10)
    this.w_IMPSCA = 0
    * --- Devo stornare incassi precedenti gi� collegati ad un piano
    * --- Write into TMPPNT_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SERDOC,ROWORI,NUMORI"
      do vq_exec with 'gscg12bgd',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPPNT_MAST.SERDOC = _t2.SERDOC";
              +" and "+"TMPPNT_MAST.ROWORI = _t2.ROWORI";
              +" and "+"TMPPNT_MAST.NUMORI = _t2.NUMORI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IMPSCA = TMPPNT_MAST.IMPSCA-_t2.INCPRE";
          +i_ccchkf;
          +" from "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPPNT_MAST.SERDOC = _t2.SERDOC";
              +" and "+"TMPPNT_MAST.ROWORI = _t2.ROWORI";
              +" and "+"TMPPNT_MAST.NUMORI = _t2.NUMORI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 set ";
          +"TMPPNT_MAST.IMPSCA = TMPPNT_MAST.IMPSCA-_t2.INCPRE";
          +Iif(Empty(i_ccchkf),"",",TMPPNT_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPPNT_MAST.SERDOC = t2.SERDOC";
              +" and "+"TMPPNT_MAST.ROWORI = t2.ROWORI";
              +" and "+"TMPPNT_MAST.NUMORI = t2.NUMORI";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set (";
          +"IMPSCA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"IMPSCA-t2.INCPRE";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPPNT_MAST.SERDOC = _t2.SERDOC";
              +" and "+"TMPPNT_MAST.ROWORI = _t2.ROWORI";
              +" and "+"TMPPNT_MAST.NUMORI = _t2.NUMORI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set ";
          +"IMPSCA = TMPPNT_MAST.IMPSCA-_t2.INCPRE";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SERDOC = "+i_cQueryTable+".SERDOC";
              +" and "+i_cTable+".ROWORI = "+i_cQueryTable+".ROWORI";
              +" and "+i_cTable+".NUMORI = "+i_cQueryTable+".NUMORI";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IMPSCA = (select "+i_cTable+".IMPSCA-INCPRE from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Select from Gscg11bgd
    do vq_exec with 'Gscg11bgd',this,'_Curs_Gscg11bgd','',.f.,.t.
    if used('_Curs_Gscg11bgd')
      select _Curs_Gscg11bgd
      locate for 1=1
      do while not(eof())
      this.w_SERIAL_INC = _Curs_Gscg11bgd.Serinc
      this.w_ROWORD_INC = _Curs_Gscg11bgd.Rowinc
      this.w_RIGA_INC = _Curs_Gscg11bgd.Numinc
      if this.w_Serial_Orig<>_Curs_Gscg11bgd.Serdoc Or this.w_Roword_Orig<>_Curs_Gscg11bgd.Rowori Or this.w_Riga_Orig<>_Curs_Gscg11bgd.Numori 
        this.w_IMPSCA = Nvl(_Curs_Gscg11bgd.Impsca,0)
        this.w_SERIAL_ORIG = _Curs_Gscg11bgd.Serdoc
        this.w_ROWORD_ORIG = _Curs_Gscg11bgd.Rowori
        this.w_RIGA_ORIG = _Curs_Gscg11bgd.Numori 
      endif
      if this.w_Impsca<>0
        * --- Consumo importo della scadenza
        do case
          case this.w_Impsca>Nvl(_Curs_Gscg11bgd.Impinc,0)
            this.w_IMPSCA = this.w_Impsca-Nvl(_Curs_Gscg11bgd.Impinc,0)
          case this.w_Impsca=Nvl(_Curs_Gscg11bgd.Impinc,0)
            this.w_IMPSCA = 0
          otherwise
            * --- Write into TMPPNT_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"Impinc ="+cp_NullLink(cp_ToStrODBC(this.w_Impsca),'TMPPNT_MAST','Impinc');
                  +i_ccchkf ;
              +" where ";
                  +"SERINC = "+cp_ToStrODBC(this.w_SERIAL_INC);
                  +" and ROWINC = "+cp_ToStrODBC(this.w_ROWORD_INC);
                  +" and NUMINC = "+cp_ToStrODBC(this.w_RIGA_INC);
                     )
            else
              update (i_cTable) set;
                  Impinc = this.w_Impsca;
                  &i_ccchkf. ;
               where;
                  SERINC = this.w_SERIAL_INC;
                  and ROWINC = this.w_ROWORD_INC;
                  and NUMINC = this.w_RIGA_INC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
        endcase
      else
        * --- Write into TMPPNT_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GENERA ="+cp_NullLink(cp_ToStrODBC("N"),'TMPPNT_MAST','GENERA');
              +i_ccchkf ;
          +" where ";
              +"SERINC = "+cp_ToStrODBC(this.w_SERIAL_INC);
              +" and ROWINC = "+cp_ToStrODBC(this.w_ROWORD_INC);
              +" and NUMINC = "+cp_ToStrODBC(this.w_RIGA_INC);
                 )
        else
          update (i_cTable) set;
              GENERA = "N";
              &i_ccchkf. ;
           where;
              SERINC = this.w_SERIAL_INC;
              and ROWINC = this.w_ROWORD_INC;
              and NUMINC = this.w_RIGA_INC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_Gscg11bgd
        continue
      enddo
      use
    endif
    * --- Se l'utente ha attivato il flag "Maturazione temporale IVA in sospensione"
    *     verifico se esistono movimenti di primanota con flag "IVA ad esigibilit� differita"
    *     attivata la cui data competenza IVA (data documento/registrazione) non sia
    *     minore della data di controllo meno i giorni
    if this.oParentObject.w_EDMATEMP ="S"
      this.w_DATAFILTRO = this.oParentObject.w_EDDATRIF-this.oParentObject.w_EDGIORNO
      * --- Creazione tabella temporanea che contenga le registrazioni scadute
      * --- Create temporary table TMP_PART_APE
      i_nIdx=cp_AddTableDef('TMP_PART_APE') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('GSCG_BGD_4.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_PART_APE_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Aggiorno l'importo incassato
      * --- Write into TMP_PART_APE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMP_PART_APE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_APE_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SERDOC,ROWORI,NUMORI"
        do vq_exec with 'Gscg_bgd_8',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART_APE_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMP_PART_APE.SERDOC = _t2.SERDOC";
                +" and "+"TMP_PART_APE.ROWORI = _t2.ROWORI";
                +" and "+"TMP_PART_APE.NUMORI = _t2.NUMORI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMPINC = _t2.IMPINC";
            +i_ccchkf;
            +" from "+i_cTable+" TMP_PART_APE, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMP_PART_APE.SERDOC = _t2.SERDOC";
                +" and "+"TMP_PART_APE.ROWORI = _t2.ROWORI";
                +" and "+"TMP_PART_APE.NUMORI = _t2.NUMORI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART_APE, "+i_cQueryTable+" _t2 set ";
            +"TMP_PART_APE.IMPINC = _t2.IMPINC";
            +Iif(Empty(i_ccchkf),"",",TMP_PART_APE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMP_PART_APE.SERDOC = t2.SERDOC";
                +" and "+"TMP_PART_APE.ROWORI = t2.ROWORI";
                +" and "+"TMP_PART_APE.NUMORI = t2.NUMORI";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART_APE set (";
            +"IMPINC";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.IMPINC";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMP_PART_APE.SERDOC = _t2.SERDOC";
                +" and "+"TMP_PART_APE.ROWORI = _t2.ROWORI";
                +" and "+"TMP_PART_APE.NUMORI = _t2.NUMORI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART_APE set ";
            +"IMPINC = _t2.IMPINC";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SERDOC = "+i_cQueryTable+".SERDOC";
                +" and "+i_cTable+".ROWORI = "+i_cQueryTable+".ROWORI";
                +" and "+i_cTable+".NUMORI = "+i_cQueryTable+".NUMORI";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMPINC = (select IMPINC from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if "SOLL" $ UPPER(i_cModules)
        * --- Write into TMP_PART_APE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMP_PART_APE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_APE_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="SERDOC,ROWORI,NUMORI"
          do vq_exec with 'Gscg_bgd_14',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART_APE_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMP_PART_APE.SERDOC = _t2.SERDOC";
                  +" and "+"TMP_PART_APE.ROWORI = _t2.ROWORI";
                  +" and "+"TMP_PART_APE.NUMORI = _t2.NUMORI";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"IMPINC = _t2.IMPINC";
              +i_ccchkf;
              +" from "+i_cTable+" TMP_PART_APE, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMP_PART_APE.SERDOC = _t2.SERDOC";
                  +" and "+"TMP_PART_APE.ROWORI = _t2.ROWORI";
                  +" and "+"TMP_PART_APE.NUMORI = _t2.NUMORI";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART_APE, "+i_cQueryTable+" _t2 set ";
              +"TMP_PART_APE.IMPINC = _t2.IMPINC";
              +Iif(Empty(i_ccchkf),"",",TMP_PART_APE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="TMP_PART_APE.SERDOC = t2.SERDOC";
                  +" and "+"TMP_PART_APE.ROWORI = t2.ROWORI";
                  +" and "+"TMP_PART_APE.NUMORI = t2.NUMORI";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART_APE set (";
              +"IMPINC";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.IMPINC";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="TMP_PART_APE.SERDOC = _t2.SERDOC";
                  +" and "+"TMP_PART_APE.ROWORI = _t2.ROWORI";
                  +" and "+"TMP_PART_APE.NUMORI = _t2.NUMORI";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART_APE set ";
              +"IMPINC = _t2.IMPINC";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".SERDOC = "+i_cQueryTable+".SERDOC";
                  +" and "+i_cTable+".ROWORI = "+i_cQueryTable+".ROWORI";
                  +" and "+i_cTable+".NUMORI = "+i_cQueryTable+".NUMORI";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"IMPINC = (select IMPINC from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Inserisco i record presenti nella tabella temporanea "TMP_PART_APE"
      *     all'interno della tabella temporanea "TMPPNT_MAST"
      * --- Insert into TMPPNT_MAST
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"Gscg_bgd_7",this.TMPPNT_MAST_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Drop temporary table TMP_PART_APE
      i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_PART_APE')
      endif
    endif
    * --- Inserisco fatture non incassate che hanno codice IVA da non differire
    * --- Insert into TMPPNT_MAST
    i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"Gscgbgd_n",this.TMPPNT_MAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='errore inserimento fatture non incassate da non differire'
      return
    endif
    * --- Marco incassi le cui partite collegate al conto iva sono gi� state chiuse con un giroconto
    *     manuale
    * --- Write into TMPPNT_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SERDOC"
      do vq_exec with 'gscg_bgd_3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPPNT_MAST.SERDOC = _t2.SERDOC";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TIPSCA ="+cp_NullLink(cp_ToStrODBC("C"),'TMPPNT_MAST','TIPSCA');
          +",SERSTO = _t2.SERSTO";
          +",NUMRER = _t2.NUMRER";
          +",DATAREG = _t2.DATAREG";
          +",COMPET = _t2.COMPET";
          +",CODCAU = _t2.CODCAU";
          +i_ccchkf;
          +" from "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPPNT_MAST.SERDOC = _t2.SERDOC";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 set ";
      +"TMPPNT_MAST.TIPSCA ="+cp_NullLink(cp_ToStrODBC("C"),'TMPPNT_MAST','TIPSCA');
          +",TMPPNT_MAST.SERSTO = _t2.SERSTO";
          +",TMPPNT_MAST.NUMRER = _t2.NUMRER";
          +",TMPPNT_MAST.DATAREG = _t2.DATAREG";
          +",TMPPNT_MAST.COMPET = _t2.COMPET";
          +",TMPPNT_MAST.CODCAU = _t2.CODCAU";
          +Iif(Empty(i_ccchkf),"",",TMPPNT_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPPNT_MAST.SERDOC = t2.SERDOC";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set (";
          +"TIPSCA,";
          +"SERSTO,";
          +"NUMRER,";
          +"DATAREG,";
          +"COMPET,";
          +"CODCAU";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("C"),'TMPPNT_MAST','TIPSCA')+",";
          +"t2.SERSTO,";
          +"t2.NUMRER,";
          +"t2.DATAREG,";
          +"t2.COMPET,";
          +"t2.CODCAU";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPPNT_MAST.SERDOC = _t2.SERDOC";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set ";
      +"TIPSCA ="+cp_NullLink(cp_ToStrODBC("C"),'TMPPNT_MAST','TIPSCA');
          +",SERSTO = _t2.SERSTO";
          +",NUMRER = _t2.NUMRER";
          +",DATAREG = _t2.DATAREG";
          +",COMPET = _t2.COMPET";
          +",CODCAU = _t2.CODCAU";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SERDOC = "+i_cQueryTable+".SERDOC";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TIPSCA ="+cp_NullLink(cp_ToStrODBC("C"),'TMPPNT_MAST','TIPSCA');
          +",SERSTO = (select SERSTO from "+i_cQueryTable+" where "+i_cWhere+")";
          +",NUMRER = (select NUMRER from "+i_cQueryTable+" where "+i_cWhere+")";
          +",DATAREG = (select DATAREG from "+i_cQueryTable+" where "+i_cWhere+")";
          +",COMPET = (select COMPET from "+i_cQueryTable+" where "+i_cWhere+")";
          +",CODCAU = (select CODCAU from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Elimino registrazioni completamente a iva a zero gi� inserite in un piano
    * --- Delete from TMPPNT_MAST
    i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".SERDOC = "+i_cQueryTable+".SERDOC";
    
      do vq_exec with 'gscgnbgd',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    vq_exec("query\GSCG_BGD_2.VQR",this,"CONTADOC")
    if NOT this.w_BLOCCO
      if USED("CONTADOC")
        * --- Inizio Aggiornamento vero e proprio
        this.w_NUDOC = 0
        this.w_ASDOC = 0
        this.w_OKDOC = 0
        if RECCOUNT("CONTADOC") > 0
          ah_Msg("Inizio fase di generazione...",.T.)
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          ah_ErrorMsg("Operazione completata%0N. %1 documenti generati su %2 documenti da generare di cui %3 associati",,"",ALLTRIM(STR(this.w_OKDOC)),ALLTRIM(STR(this.w_NUDOC)),ALLTRIM(STR(this.w_ASDOC)))
          * --- LOG Errori
          if this.w_OKDOC=0 AND this.w_NUDOC>0
            * --- Delete from ESI_DIF
            i_nConn=i_TableProp[this.ESI_DIF_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ESI_DIF_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"EDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_EDSERIAL);
                     )
            else
              delete from (i_cTable) where;
                    EDSERIAL = this.oParentObject.w_EDSERIAL;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
          if this.w_NUDOC>0 AND this.w_OKDOC<>this.w_NUDOC AND RECCOUNT("MessErr") > 0
            if ah_YesNo("Stampo situazione messaggi di errore?")
              SELECT * FROM MessErr INTO CURSOR __TMP__
              CP_CHPRN("QUERY\GSVE_BCV.FRX", " ", this)
            endif
          endif
        else
          ah_ErrorMsg("Per l'intervallo selezionato%0non esistono incassi/pagamenti da generare",,"")
          * --- Delete from ESI_DIF
          i_nConn=i_TableProp[this.ESI_DIF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ESI_DIF_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"EDSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            delete from (i_cTable) where;
                  EDSERIAL = this.w_SERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      endif
    endif
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Drop temporary table TMPPNT_MAST
    i_nIdx=cp_GetTableDefIdx('TMPPNT_MAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPPNT_MAST')
    endif
    * --- Sblocca Primanota
    * --- Try
    local bErr_0520D1D8
    bErr_0520D1D8=bTrsErr
    this.Try_0520D1D8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Impossibile sbloccare la prima nota - semaforo bollati in dati azienda -. Sbloccarlo manualmente",,"")
    endif
    bTrsErr=bTrsErr or bErr_0520D1D8
    * --- End
    * --- Refresh dello ZOOM
  endproc
  proc Try_05208A98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Controlli Preliminari
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZSTALIG,AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZSTALIG,AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT EMPTY(this.w_DATBLO)
      * --- Raise
      i_Error="giornale"
      return
    endif
    * --- Blocca Primanota
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_FINESE),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = this.w_FINESE;
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0520D1D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizio Fase di Contabilizzazione
    * --- Crea il File delle Messaggistiche di Errore
    CREATE CURSOR MessErr (MSG C(120))
    * --- Testa Cambio Documento
    SELECT CONTADOC
    GO TOP
    SCAN FOR NOT EMPTY(NVL(SERINC,"")) AND NVL(IMPDOC,0)<>0
    * --- Try
    local bErr_051F0F78
    bErr_051F0F78=bTrsErr
    this.Try_051F0F78()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if this.w_FLERR=.F.
        * --- Errore non Compreso nei Controlli
        this.w_APPO = Ah_MsgFormat("Verificare doc.n.: %1 del.: %2",ALLTRIM(STR(this.w_PNNUMDOC,15))+IIF(EMPTY(this.w_PNALFDOC),"","/"+Alltrim(this.w_PNALFDOC)),DTOC(this.w_PNDATDOC))
        INSERT INTO MessErr (MSG) VALUES (this.w_APPO)
        INSERT INTO MessErr (MSG) VALUES (SPACE(50))
      endif
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_051F0F78
    * --- End
    SELECT CONTADOC
    ENDSCAN
  endproc
  proc Try_051F0F78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Inizializza Messaggistica di Errore
    this.w_FLERR = .F.
    FOR L_i = 1 TO 18
    MES[l_i] = ""
    ENDFOR
    * --- Testa il Cambio di Documento
    this.w_TIPIVA = NVL(TIPIVA, " ")
    this.w_TIPSCA = TIPSCA
    this.w_TIPREG = TIPREG
    this.w_OSERIAL = SERINC
    this.w_ROWORI = NVL(ROWORI, 0)
    this.w_SERDOC = NVL(SERDOC, SPACE(10))
    this.w_NUMORI = NVL(NUMORI, 0)
    this.w_OTIPDOC = NVL(TIPDOC, "  ")
    this.w_PNINICOM = cp_CharToDate("  -  -  ")
    this.w_PNFINCOM = cp_CharToDate("  -  -  ")
    this.w_PNFLABAN = SPACE(6)
    this.w_PNCODCLF = NVL(CODCON, SPACE(15))
    this.w_PNTIPCLF = NVL(TIPCON, " ")
    this.w_PNRIFDIS = NVL(PNRIFDIS,SPACE(10))
    if this.oParentObject.w_EDSCADIS="S" AND NOT EMPTY(this.w_PNRIFDIS)
      this.w_PNDATREG = CP_TODATE(NVL(DATSCA, i_DATSYS))
      this.w_PNCODESE = CALCESER(this.w_PNDATREG,SPACE(4))
    else
      this.w_PNDATREG = CP_TODATE(NVL(DATREG, i_DATSYS))
      this.w_PNCODESE = NVL(CODESE, SPACE(4))
    endif
    this.w_DATREG = this.w_PNDATREG
    this.w_DATAFATTURA = CP_TODATE(NVL(DATAFATTURA, i_DATSYS))
    this.w_CCFLAUTR = NVL(CCFLAUTR," ")
    * --- Controllo se la registrazione contabile ha il flag di "IVA autotrasportatori"
    if this.w_CCFLAUTR="S" AND this.oParentObject.w_EDRILTRI="S"
      this.w_ANNODATAREG = Str(Year(this.w_Pndatreg),4,0)
      this.w_ANNODATAFAT = Str(Year(this.w_DataFattura),4,0)
      * --- Controllo se la data registrazione della fattura e la data registrazione
      *     dell'eventuale incasso appartengono allo stesso trimestre
      this.w_TRIM_INCASSO = iif(this.w_Pndatreg>=Cp_CharToDate("01-01-"+this.w_AnnoDataReg) And this.w_Pndatreg<=Cp_CharToDate("31-03-"+this.w_AnnoDataReg),1,iif(this.w_Pndatreg>=Cp_CharToDate("01-04-"+this.w_AnnoDataReg) And this.w_Pndatreg<=Cp_CharToDate("30-06-"+this.w_AnnoDataReg),2,iif(this.w_Pndatreg>=Cp_CharToDate("01-07-"+this.w_AnnoDataReg) And this.w_Pndatreg<=Cp_CharToDate("30-09-"+this.w_AnnoDataReg),3,4)))
      this.w_TRIM_FATTURA = iif(this.w_DataFattura>=Cp_CharToDate("01-01-"+this.w_AnnoDataFat) And this.w_DataFattura<=Cp_CharToDate("31-03-"+this.w_AnnoDataFat),1,iif(this.w_DataFattura>=Cp_CharToDate("01-04-"+this.w_AnnoDataFat) And this.w_DataFattura<=Cp_CharToDate("30-06-"+this.w_AnnoDataFat),2,iif(this.w_DataFattura>=Cp_CharToDate("01-07-"+this.w_AnnoDataFat) And this.w_DataFattura<=Cp_CharToDate("30-09-"+this.w_AnnoDataFat),3,4)))
      if this.w_TRIM_INCASSO=this.w_TRIM_FATTURA AND this.w_ANNODATAREG=this.w_ANNODATAFAT
        * --- Ricalcolo la data registrazione
        do case
          case this.w_TRIM_INCASSO=1
            this.w_PNDATREG = cp_CharToDate("30-06-"+this.w_AnnoDataReg)
          case this.w_TRIM_INCASSO=2
            this.w_PNDATREG = cp_CharToDate("30-09-" + this.w_AnnoDataReg)
          case this.w_TRIM_INCASSO=3
            this.w_PNDATREG = cp_CharToDate("31-12-" +this.w_AnnoDataReg)
          case this.w_TRIM_INCASSO=4
            this.w_PNDATREG = cp_CharToDate("31-03-" +Str(Year(this.w_Pndatreg)+1,4,0))
        endcase
      else
        this.w_PNDATREG = IIF(EMPTY(this.oParentObject.w_EDDATREG),this.w_PNDATREG,this.oParentObject.w_EDDATREG)
      endif
      this.w_PNCODESE = CALCESER(this.w_PNDATREG,SPACE(4))
    else
      this.w_PNDATREG = IIF(EMPTY(this.oParentObject.w_EDDATREG),this.w_PNDATREG,this.oParentObject.w_EDDATREG)
      this.w_PNCODESE = CALCESER(this.w_PNDATREG,SPACE(4))
    endif
    this.w_PNTIPDOC = IIF(this.w_TIPREG="N",IIF(this.w_PNTIPCLF="C", this.oParentObject.w_TIPINC, this.oParentObject.w_TIPPAG),IIF(this.w_PNTIPCLF="C", this.oParentObject.w_TIPINCAT, this.oParentObject.w_TIPPAGAP))
    this.w_PNNUMREG = IIF(this.w_TIPREG="N",IIF(this.w_PNTIPCLF="C", this.oParentObject.w_NUMINC, this.oParentObject.w_NUMPAG),IIF(this.w_PNTIPCLF="C", this.oParentObject.w_NUMINCAT, this.oParentObject.w_NUMPAGAP))
    this.w_PNTIPREG = IIF(this.w_TIPREG="N",IIF(this.w_PNTIPCLF="C", this.oParentObject.w_REGINC, this.oParentObject.w_REGPAG),IIF(this.w_PNTIPCLF="C", this.oParentObject.w_REGINCAT, this.oParentObject.w_REGPAGAP))
    this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
    this.w_PNNUMDOC = NVL(NUMDOC, 0)
    this.w_PNALFDOC = NVL(ALFDOC, Space(10))
    this.w_PNDATDOC = CP_TODATE(DATDOC)
    this.w_PNALFPRO = NVL(ALFPRO, Space(10))
    this.w_PNNUMPRO = NVL(NUMPRO, 0)
    this.w_PNVALNAZ = NVL(VALNAZ, g_PERVAL)
    this.w_PNCOMPET = this.w_PNCODESE
    this.w_PNCODVAL = NVL(CODVAL, g_PERVAL)
    this.w_PNDESSUP = NVL(DESSUP, SPACE(50))
    this.w_PNCAOVAL = NVL(CAOVAL, 1)
    this.w_PNCOMIVA = this.w_PNDATREG
    this.w_CCCONIVA = IIF(this.w_TIPREG="N",IIF(this.w_PNTIPCLF="C", this.oParentObject.w_IVAINC, this.oParentObject.w_IVAPAG),IIF(this.w_PNTIPCLF="C", this.oParentObject.w_IVAINCAT, this.oParentObject.w_IVAPAGAP))
    this.w_PNCODCAU = IIF(this.w_TIPREG="N",IIF(this.w_PNTIPCLF="C", this.oParentObject.w_CAUINC, this.oParentObject.w_CAUPAG),IIF(this.w_PNTIPCLF="C", this.oParentObject.w_CAUMTA, this.oParentObject.w_CAUMTP))
    this.w_NUMFAT = NVL(PNNUMFAT,"")
    * --- Read from CAU_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCFLPDOC,CCFLPPRO,CCDESSUP,CCDESRIG"+;
        " from "+i_cTable+" CAU_CONT where ";
            +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCFLPDOC,CCFLPPRO,CCDESSUP,CCDESRIG;
        from (i_cTable) where;
            CCCODICE = this.w_PNCODCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLPDOC = NVL(cp_ToDate(_read_.CCFLPDOC),cp_NullValue(_read_.CCFLPDOC))
      this.w_FLPPRO = NVL(cp_ToDate(_read_.CCFLPPRO),cp_NullValue(_read_.CCFLPPRO))
      this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
      this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PNANNDOC = CALPRO(IIF(EMPTY(this.w_PNDATDOC), this.w_PNDATREG, this.w_PNDATDOC), this.w_PNCOMPET, this.w_FLPDOC)
    this.w_PNANNPRO = CALPRO(this.w_PNDATREG, this.w_PNCOMPET, this.w_FLPPRO)
    this.w_CAONAZ = GETCAM(g_PERVAL,i_DATSYS)
    this.w_DECTOP = g_PERPVL
    this.w_VALORI = NVL(VALORI, g_PERVAL)
    this.w_IMPDOC = ABS(NVL(IMPDOC, 0))
    this.w_CAOAPE = NVL(CAOAPE, GETCAM(g_PERVAL,i_DATSYS))
    this.w_IMPINC = ABS(NVL(IMPINC, 0))
    this.w_PNFLGDIF = NVL(PNFLGDIF,"S")
    * --- Verifico Presenza di Acconti generici da decurtare caso di Acconto con importo superiore alla Fattura
    * --- Read from PAR_TITE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PTTOTIMP,CPROWNUM"+;
        " from "+i_cTable+" PAR_TITE where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.w_SERDOC );
            +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORI);
            +" and PTFLCRSA = "+cp_ToStrODBC("A");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PTTOTIMP,CPROWNUM;
        from (i_cTable) where;
            PTSERIAL = this.w_SERDOC ;
            and PTROWORD = this.w_ROWORI;
            and PTFLCRSA = "A";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_IMPACC = NVL(cp_ToDate(_read_.PTTOTIMP),cp_NullValue(_read_.PTTOTIMP))
      this.w_ROWACC = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_IMPACC<>0 AND this.w_IMPINC>this.w_IMPACC AND this.w_ROWACC-1=this.w_NUMORI
      this.w_IMPINC = this.w_IMPINC- this.w_IMPACC
    endif
    * --- L'importo Incassato Proviene dalle partite (cliente) e puo' essere in Valuta
    if this.w_VALORI<>this.w_PNCODVAL
      * --- Converte nella Valuta di Conto del Documento di Origine (attenzione puo' essere diversa da quella del Doc. di Incasso/Pagamento)
      * --- Se altra Valuta di Esercizio
      if this.w_VALORI<>g_PERVAL
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VACAOVAL,VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_VALORI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VACAOVAL,VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_VALORI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CAONAZ = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
          this.w_DECTOP = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT CONTADOC
      endif
      this.w_IMPINC = IIF(this.w_TIPREG="S",this.w_IMPINC,VAL2MON(this.w_IMPINC,this.w_CAOAPE, this.w_CAONAZ, IIF(EMPTY(this.w_PNDATDOC), this.w_PNDATREG, this.w_PNDATDOC), this.w_VALORI, this.w_DECTOP))
      SELECT CONTADOC
    endif
    if this.w_TIPSCA="A"
      * --- Se altra Valuta di Esercizio
      if this.w_PNVALNAZ<>g_PERVAL
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VACAOVAL,VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_PNVALNAZ);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VACAOVAL,VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_PNVALNAZ;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CAONAZ = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
          this.w_DECTOP = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT CONTADOC
      else
        this.w_CAONAZ = GETCAM(g_PERVAL,i_DATSYS)
        this.w_DECTOP = g_PERPVL
      endif
      do case
        case this.w_PNDATREG<=this.w_STALIG AND NOT EMPTY(this.w_STALIG)
          MES[5] = Ah_MsgFormat("Data registrazione del relativo incasso %1 inferiore o uguale a ultima stampa libro giornale.",DTOC(this.w_PNDATREG))
          this.w_FLERR = .T.
        case Not Empty(CHKCONS("P",this.w_PNDATREG,"B","N"))
          MES[5] =CHKCONS("P",this.w_PNDATREG,"B","N") 
          this.w_FLERR = .T.
      endcase
      if EMPTY(this.w_PNCODCLF) OR EMPTY (this.w_PNTIPCLF)
        if w_FLVEAC="V"
          MES[1] = Ah_MsgFormat("Cliente non definito")
        else
          MES[1] = Ah_MsgFormat("Fornitore non definito")
        endif
        this.w_FLERR = .T.
      endif
      * --- Legge Conto IVA Esig.Differita
      * --- Carica I Conti IVA ad Esig.Differita dai Documenti di Origine
      vq_exec("query\GSCG1BGD.VQR",this,"CONTIIVA")
      if USED("CONTIIVA") AND Reccount("CONTIIVA") <>0
        SELECT CONTIIVA
        GO TOP
        LOCATE FOR PNSERIAL = this.w_SERDOC AND (NVL(ANPARTSN, "N")<>"S" or Nvl(CONTA,0)=0 or EMPTY(NVL(PNCODCON,"")))
        if FOUND()
          do case
            case EMPTY(NVL(PNCODCON,"")) 
              MES[3] = Ah_MsgFormat("Conto IVA ad esigib.differita non definito.")
            case NVL(ANPARTSN, "N")<>"S" 
              MES[3] = Ah_MsgFormat("Conto IVA : %1 non gestito a partite.", ALLTRIM(NVL(PNCODCON,"")))
            otherwise
              MES[3] = Ah_MsgFormat("Riga conto %1 senza dettaglio partite.", ALLTRIM(NVL(PNCODCON,"")))
          endcase
          this.w_FLERR = .T.
        endif
      else
        MES[3] = Ah_MsgFormat("Conto IVA ad esigib.differita non definito.")
        this.w_FLERR = .T.
      endif
    else
      this.w_PNSERIAL = CONTADOC.SERSTO
      this.w_PNCODCAU = CONTADOC.CODCAU
      this.w_PNNUMRER = CONTADOC.NUMRER
      this.w_PNDATREG = CP_TODATE(NVL(CONTADOC.DATAREG, i_DATSYS))
      this.w_PNCOMPET = NVL(CONTADOC.COMPET, SPACE(4))
      this.w_ASDOC = this.w_ASDOC + 1
    endif
    if this.w_FLERR=.F. AND this.w_TIPSCA="A"
      this.w_TOTCONTIVA = 0
      this.w_OLDSERIAL = repl("@",10)
      this.w_OLDTIPO = "@"
      SELECT CONTIIVA
      GO TOP
      SCAN FOR Nvl(IVFLSTOD,"N") <> "S"
      this.w_ROWNUM = CPROWNUM
      this.w_PTTOTIMP = NVL(PTTOTIMP,0)
      this.w_TIPO = TIPO
      if this.w_TIPO="N"
        * --- Devo riassegnare data registrazione in funzione del campo PNDATREG per lo storno iva a zero 
        *     e quindi devo rieseguire controlli sulla data
        this.w_DATREG = CONTIIVA.PNDATREG
        this.w_PNCOMIVA = this.w_DATREG
        this.w_PNANNDOC = CALPRO(IIF(EMPTY(this.w_PNDATDOC), this.w_DATREG, this.w_PNDATDOC), this.w_PNCOMPET, this.w_FLPDOC)
        this.w_PNANNPRO = CALPRO(this.w_DATREG, this.w_PNCOMPET, this.w_FLPPRO)
        do case
          case this.w_DATREG<=this.w_STALIG AND NOT EMPTY(this.w_STALIG)
            MES[5] = Ah_MsgFormat("Data registrazione del relativo incasso %1 inferiore o uguale a ultima stampa libro giornale.",DTOC(this.w_PNDATREG))
            this.w_FLERR = .T.
          case Not Empty(CHKCONS("P",this.w_DATREG,"B","N"))
            MES[5] =CHKCONS("P",this.w_DATREG,"B","N") 
            this.w_FLERR = .T.
        endcase
      else
        this.w_DATREG = this.w_PNDATREG
      endif
      if this.w_SERDOC <> this.w_OLDSERIAL or this.w_TIPO <> this.w_OLDTIPO
        this.w_NUDOC = this.w_NUDOC + 1
        this.w_CPROWNUM = 0
        this.w_CPROWORD = 0
        this.w_PNNUMRER = 0
        this.w_PNSERIAL = SPACE(10)
        i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
        cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
        cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
        SELECT CONTIIVA
        this.w_CCCONDIF = PNCODCON
        this.w_CODPAG = NVL(PNCODPAG, " ")
        this.w_TOTIVA = 0
        this.w_CONTA = NVL(CONTA,0)
        * --- La Valuta della Registrazione e' sempre riferita alla Valuta del Conto IVA e non del Cliente
        this.w_PNCODVAL = NVL(PTCODVAL," ")
        this.w_PNCAOVAL = NVL(PTCAOVAL, 1)
        this.w_APPO = ALLTRIM(STR(this.w_PNNUMRER))+"Del.:"+DTOC(this.w_PNDATREG)
        ah_Msg("Scrivo reg. n.:%1",.T.,.F.,.F.,this.w_APPO)
        if Empty(this.w_PNDESSUP) and (Not Empty(this.w_CCDESSUP) OR Not Empty(this.w_CCDESRIG))
          * --- Array elenco parametri per descrizioni di riga e testata
           
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_PNNUMDOC)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.w_PNALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_PNDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]=Alltrim(this.w_PNALFPRO) 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=Alltrim(this.w_PNCODCLF) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]=ALLTRIM(STR(this.w_PNNUMPRO)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=this.w_PNTIPCLF 
 ARPARAM[13,1]="NUMFAT" 
 ARPARAM[13,2]=this.w_NUMFAT
          this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
        endif
        * --- Scrive la Testata
        * --- Insert into PNT_MAST
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PNSERIAL"+",PNCODESE"+",PNCODUTE"+",PNNUMRER"+",PNDATREG"+",PNCODCAU"+",PNCOMPET"+",PNTIPREG"+",PNFLIVDF"+",PNNUMREG"+",PNTIPDOC"+",PNPRD"+",PNALFDOC"+",PNNUMDOC"+",PNDATDOC"+",PNALFPRO"+",PNNUMPRO"+",PNVALNAZ"+",PNCODVAL"+",PNPRG"+",PNCAOVAL"+",PNDESSUP"+",PNTIPCLF"+",PNCODCLF"+",PNTOTDOC"+",PNCOMIVA"+",PNFLREGI"+",PNFLPROV"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",PNRIFDOC"+",PNANNDOC"+",PNANNPRO"+",PNPRP"+",PNNUMFAT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'PNT_MAST','PNDATREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLIVDF),'PNT_MAST','PNFLIVDF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_MAST','PNNUMREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPDOC),'PNT_MAST','PNTIPDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRD),'PNT_MAST','PNPRD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PNT_MAST','PNALFDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PNT_MAST','PNNUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PNT_MAST','PNDATDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFPRO),'PNT_MAST','PNALFPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMPRO),'PNT_MAST','PNNUMPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PNT_MAST','PNTIPCLF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PNT_MAST','PNCODCLF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PNT_MAST','PNTOTDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMIVA),'PNT_MAST','PNCOMIVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLREGI),'PNT_MAST','PNFLREGI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPROV),'PNT_MAST','PNFLPROV');
          +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PNT_MAST','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PNT_MAST','UTDC');
          +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','UTCV');
          +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'PNT_MAST','UTDV');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PNT_MAST','PNRIFDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNDOC),'PNT_MAST','PNANNDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNPRO),'PNT_MAST','PNANNPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRP),'PNT_MAST','PNPRP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMFAT),'PNT_MAST','PNNUMFAT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNCODESE',this.w_PNCODESE,'PNCODUTE',this.w_PNCODUTE,'PNNUMRER',this.w_PNNUMRER,'PNDATREG',this.w_DATREG,'PNCODCAU',this.w_PNCODCAU,'PNCOMPET',this.w_PNCOMPET,'PNTIPREG',this.w_PNTIPREG,'PNFLIVDF',this.w_PNFLIVDF,'PNNUMREG',this.w_PNNUMREG,'PNTIPDOC',this.w_PNTIPDOC,'PNPRD',this.w_PNPRD)
          insert into (i_cTable) (PNSERIAL,PNCODESE,PNCODUTE,PNNUMRER,PNDATREG,PNCODCAU,PNCOMPET,PNTIPREG,PNFLIVDF,PNNUMREG,PNTIPDOC,PNPRD,PNALFDOC,PNNUMDOC,PNDATDOC,PNALFPRO,PNNUMPRO,PNVALNAZ,PNCODVAL,PNPRG,PNCAOVAL,PNDESSUP,PNTIPCLF,PNCODCLF,PNTOTDOC,PNCOMIVA,PNFLREGI,PNFLPROV,UTCC,UTDC,UTCV,UTDV,PNRIFDOC,PNANNDOC,PNANNPRO,PNPRP,PNNUMFAT &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_PNCODESE;
               ,this.w_PNCODUTE;
               ,this.w_PNNUMRER;
               ,this.w_DATREG;
               ,this.w_PNCODCAU;
               ,this.w_PNCOMPET;
               ,this.w_PNTIPREG;
               ,this.w_PNFLIVDF;
               ,this.w_PNNUMREG;
               ,this.w_PNTIPDOC;
               ,this.w_PNPRD;
               ,this.w_PNALFDOC;
               ,this.w_PNNUMDOC;
               ,this.w_PNDATDOC;
               ,this.w_PNALFPRO;
               ,this.w_PNNUMPRO;
               ,this.w_PNVALNAZ;
               ,this.w_PNCODVAL;
               ,this.w_PNPRG;
               ,this.w_PNCAOVAL;
               ,this.w_PNDESSUP;
               ,this.w_PNTIPCLF;
               ,this.w_PNCODCLF;
               ,this.w_PNTOTDOC;
               ,this.w_PNCOMIVA;
               ,this.w_PNFLREGI;
               ,this.w_PNFLPROV;
               ,i_CODUTE;
               ,SetInfoDate( g_CALUTD );
               ,0;
               ,cp_CharToDate("  -  -    ");
               ,SPACE(10);
               ,this.w_PNANNDOC;
               ,this.w_PNANNPRO;
               ,this.w_PNPRP;
               ,this.w_NUMFAT;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Carica le Righe IVA del Doc. di Origine
        vq_exec("query\GSCG3BGD.VQR",this,"RIGHEIVA")
        if USED("RIGHEIVA")
          SELECT RIGHEIVA
          GO TOP
          SCAN FOR NOT EMPTY(NVL(CODIVA," "))
          this.w_IVCODIVA = NVL(CODIVA," ")
          this.w_IVCODCON = CONIVA
          this.w_IVCONTRO = IIF(g_DETCON="S", NVL(CONTRO, SPACE(15)), SPACE(15))
          this.w_IVPERIND = NVL(PERIND,0)
          this.w_IVTIPREG = TIPREG
          this.w_IVNUMREG = NUMREG
          this.w_IVIMPONI = NVL(IMPONI,0)
          this.w_IVIMPIVA = NVL(IMPIVA,0)
          this.w_IVFLOMAG = NVL(FLOMAG, "X")
          * --- Se la Valuta di Conto del Doc. di Origine e' Cambiata Riconverte (Prevediamo solo Lire o EURO)
          if this.w_PNVALNAZ<>this.w_VALORI
            this.w_IVIMPONI = VAL2MON(this.w_IVIMPONI,GETCAM(g_PERVAL,I_DATSYS), 1,I_DATSYS,g_PERVAL, this.w_DECTOP)
            this.w_IVIMPIVA = VAL2MON(this.w_IVIMPIVA,GETCAM(g_PERVAL,I_DATSYS), 1,I_DATSYS,g_PERVAL, this.w_DECTOP)
          endif
          * --- Se Incasso/Pagamento diverso da Totale Documento calcola in Proporzione
          if this.w_TIPO="D"
            if this.w_IMPINC<>this.w_IMPDOC
              this.w_IVIMPONI = cp_ROUND((this.w_IVIMPONI*this.w_IMPINC)/this.w_IMPDOC, this.w_DECTOP)
              this.w_IVIMPIVA = cp_ROUND((this.w_IVIMPIVA*this.w_IMPINC)/this.w_IMPDOC, this.w_DECTOP)
            endif
          endif
          * --- Se Nota di Credito inverte i Segni
          this.w_IVIMPONI = this.w_IVIMPONI * IIF(this.w_OTIPDOC $ "NC-NU-NE", -1, 1)
          this.w_IVIMPIVA = this.w_IVIMPIVA * IIF(this.w_OTIPDOC $ "NC-NU-NE", -1, 1)
          * --- Inserisce Righe castelletto IVA
          if this.w_FLERR=.F. AND NOT EMPTY(this.w_IVCODIVA) AND NOT EMPTY(this.w_IVTIPREG) AND NOT EMPTY(this.w_IVNUMREG)
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            if this.w_IVIMPIVA>0
              * --- Resto solo su righe con iva significativa
              this.w_MAXROW = this.w_CPROWNUM
            endif
            this.w_TOTIVA = this.w_TOTIVA + this.w_IVIMPIVA
            * --- Insert into PNT_IVA
            i_nConn=i_TableProp[this.PNT_IVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_IVA_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"IVSERIAL"+",CPROWNUM"+",IVCODIVA"+",IVTIPCON"+",IVCODCON"+",IVPERIND"+",IVTIPREG"+",IVNUMREG"+",IVIMPONI"+",IVIMPIVA"+",IVFLOMAG"+",IVCFLOMA"+",IVCONTRO"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_IVA','IVSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_IVA','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVCODIVA),'PNT_IVA','IVCODIVA');
              +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVCODCON),'PNT_IVA','IVCODCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVPERIND),'PNT_IVA','IVPERIND');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPREG),'PNT_IVA','IVTIPREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVNUMREG),'PNT_IVA','IVNUMREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVIMPONI),'PNT_IVA','IVIMPONI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVIMPIVA),'PNT_IVA','IVIMPIVA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVFLOMAG),'PNT_IVA','IVFLOMAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVFLOMAG),'PNT_IVA','IVCFLOMA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVCONTRO),'PNT_IVA','IVCONTRO');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'IVSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'IVCODIVA',this.w_IVCODIVA,'IVTIPCON',"G",'IVCODCON',this.w_IVCODCON,'IVPERIND',this.w_IVPERIND,'IVTIPREG',this.w_IVTIPREG,'IVNUMREG',this.w_IVNUMREG,'IVIMPONI',this.w_IVIMPONI,'IVIMPIVA',this.w_IVIMPIVA,'IVFLOMAG',this.w_IVFLOMAG,'IVCFLOMA',this.w_IVFLOMAG)
              insert into (i_cTable) (IVSERIAL,CPROWNUM,IVCODIVA,IVTIPCON,IVCODCON,IVPERIND,IVTIPREG,IVNUMREG,IVIMPONI,IVIMPIVA,IVFLOMAG,IVCFLOMA,IVCONTRO &i_ccchkf. );
                 values (;
                   this.w_PNSERIAL;
                   ,this.w_CPROWNUM;
                   ,this.w_IVCODIVA;
                   ,"G";
                   ,this.w_IVCODCON;
                   ,this.w_IVPERIND;
                   ,this.w_IVTIPREG;
                   ,this.w_IVNUMREG;
                   ,this.w_IVIMPONI;
                   ,this.w_IVIMPIVA;
                   ,this.w_IVFLOMAG;
                   ,this.w_IVFLOMAG;
                   ,this.w_IVCONTRO;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            if USED("REG_IVA")
              * --- Controlla se gia' stampato su Reg.IVA
              SELECT REG_IVA
              GO TOP
              LOCATE FOR ATTIPREG=this.w_IVTIPREG AND ATNUMREG=this.w_IVNUMREG
              if FOUND()
                do case
                  case this.w_PNCOMIVA<=CP_TODATE(ATDATBLO) AND NOT EMPTY(ATDATBLO)
                    MES[4] = Ah_MsgFormat("Stampa registri IVA in corso o bloccato reg.IVA %1 n. %2",this.w_IVTIPREG,STR(this.w_IVNUMREG))
                    this.w_FLERR = .T.
                  case this.w_PNCOMIVA<=CP_TODATE(ATDATSTA) AND NOT EMPTY(ATDATSTA)
                    MES[4] = Ah_MsgFormat("Data competenza IVA minore o uguale ultima stampa reg.IVA %1 n. %2",this.w_IVTIPREG,STR(this.w_IVNUMREG))
                    this.w_FLERR = .T.
                endcase
              endif
            endif
          endif
          SELECT RIGHEIVA
          ENDSCAN
          SELECT CONTADOC
        endif
        * --- Scrive il Detail di Primanota
        this.w_CPROWNUM = 0
        this.w_CPROWORD = 0
      endif
      if this.w_FLERR=.F. AND this.w_TIPSCA="A"
        * --- Se Incasso/Pagamento diverso da Totale Documento calcola in Proporzione
        if this.w_TIPO="D"
          if this.w_IMPINC<>this.w_IMPDOC
            this.w_PTTOTIMP = cp_ROUND((this.w_PTTOTIMP*this.w_IMPINC)/this.w_IMPDOC, this.w_DECTOP)
          endif
          * --- Select from GSCG5BGD
          do vq_exec with 'GSCG5BGD',this,'_Curs_GSCG5BGD','',.f.,.t.
          if used('_Curs_GSCG5BGD')
            select _Curs_GSCG5BGD
            locate for 1=1
            do while not(eof())
            this.w_TOTSAL = cp_Round(Nvl(_Curs_GSCG5BGD.IMPORI,0)-Nvl(_Curs_GSCG5BGD.IMPINC,0),this.w_DECTOP)
            exit
              select _Curs_GSCG5BGD
              continue
            enddo
            use
          endif
          if Abs(this.w_TOTSAL-this.w_PTTOTIMP)<=0.01
            * --- Inserisco resto sull'utlimo incasso
            this.w_PTTOTIMP = this.w_TOTSAL
          endif
          this.w_APPVAL = ABS(this.w_PTTOTIMP)
          * --- L'importo Proviene dalle partite (Conto IVA) e puo' essere in Valuta
          if this.w_PNVALNAZ<>this.w_PNCODVAL
            this.w_APPVAL = VAL2MON(this.w_APPVAL,this.w_CAOAPE,this.w_CAONAZ,this.w_PNDATREG,this.w_PNVALNAZ, this.w_DECTOP)
          endif
          this.w_TOTCONTIVA = this.w_TOTCONTIVA + this.w_APPVAL
        else
          this.w_APPVAL = 0
        endif
        * --- Verifico se � installato/abilitato il modulo 'Trasferimento studio', se attivo utilizzo come causale di riga
        *     la causale di giroconto impostata in 'Tabella trasferimento studio'
        if LMAVANZATO()
          * --- Verifico se il cliente/fornitore ha il flag "Maturazione temporale". Se ha il flag disattivo
          *     deve essere riportata nei dati di riga la causale letta nel campo "Causale
          *     giroconto" della tabella "Trasferimento Studio" altrimenti deve essere riportata
          *     la "Causale di riga storno fatt. con IVA sospesa"
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANIVASOS"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANIVASOS;
              from (i_cTable) where;
                  ANTIPCON = this.w_PNTIPCLF;
                  and ANCODICE = this.w_PNCODCLF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ANIVASOS = NVL(cp_ToDate(_read_.ANIVASOS),cp_NullValue(_read_.ANIVASOS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_OLDCODCAU = this.w_PNCODCAU
          * --- Select from QUERY\CAUGIR
          do vq_exec with 'QUERY\CAUGIR',this,'_Curs_QUERY_CAUGIR','',.f.,.t.
          if used('_Curs_QUERY_CAUGIR')
            select _Curs_QUERY_CAUGIR
            locate for 1=1
            do while not(eof())
            this.w_PNCODCAU = IIF(this.w_ANIVASOS<>"S",NVL(_Curs_QUERY_CAUGIR.LMCAUGIR," "),NVL(_Curs_QUERY_CAUGIR.LMCAUSED," "))
              select _Curs_QUERY_CAUGIR
              continue
            enddo
            use
          endif
          if EMPTY(this.w_PNCODCAU)
            Ah_Errormsg("Causale di giroconto non specificata in 'Tabella trasferimento studio',%0nella causale di riga verr� mantenuta la causale presente in testata ",48,"")
            this.w_PNCODCAU = this.w_OLDCODCAU
          endif
        endif
      endif
      * --- Conto IVA Esig.Differita
      this.w_PNTIPCON = "G"
      this.w_PNCODCON = this.w_CCCONDIF
      * --- Se nota di Credito inverte le Sezioni D/A
      if this.w_OTIPDOC $ "NC-NU-NE"
        this.w_APPSEZ = IIF(this.w_PNTIPCLF="C", "A", "D")
      else
        this.w_APPSEZ = IIF(this.w_PNTIPCLF="C", "D", "A")
      endif
      this.w_PNFLPART = iif(this.w_TIPO="D","S","N")
      this.w_PNCODPAG = this.w_CODPAG
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_TIPO="D"
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Conto IVA di Contropartita
      if this.w_SERDOC <> this.w_OLDSERIAL or this.w_TIPO <> this.w_OLDTIPO
        this.w_PNCODCON = this.w_CCCONIVA
        this.w_APPVAL = iif(this.w_TIPO="D",this.w_TOTCONTIVA,0)
        * --- Se nota di Credito inverte le Sezioni D/A
        if this.w_OTIPDOC $ "NC-NU-NE"
          this.w_APPSEZ = IIF(this.w_PNTIPCLF="C", "D", "A")
        else
          this.w_APPSEZ = IIF(this.w_PNTIPCLF="C", "A", "D")
        endif
        this.w_PNFLPART = "N"
        this.w_PNCODPAG = SPACE(5)
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_TIPO="D" and Abs(Abs(this.w_TOTIVA)-Abs(this.w_APPVAL))<=0.01 and this.w_MAXROW>0
          * --- Metto il resto sulla prima riga
          this.w_RESTO = cp_Round(Abs(this.w_APPVAL)-Abs(this.w_TOTIVA),g_PERPVL)*IIF(this.w_OTIPDOC $ "NC-NU-NE", -1, 1)
          * --- Write into PNT_IVA
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PNT_IVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_IVA_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IVIMPIVA =IVIMPIVA+ "+cp_ToStrODBC(this.w_RESTO);
                +i_ccchkf ;
            +" where ";
                +"IVSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_MAXROW);
                   )
          else
            update (i_cTable) set;
                IVIMPIVA = IVIMPIVA + this.w_RESTO;
                &i_ccchkf. ;
             where;
                IVSERIAL = this.w_PNSERIAL;
                and CPROWNUM = this.w_MAXROW;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_OLDSERIAL = this.w_SERDOC
      this.w_OLDTIPO = this.w_TIPO
      ENDSCAN
    else
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza Variabili
    this.w_PNFLREGI = " "
    this.w_PNFLPROV = "N"
    this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
    this.w_PNPRP = "NN"
    this.w_PNPRD = "NN"
    this.w_PNFLIVDF = " "
    this.w_PARTSN = "S"
    this.w_PNTOTDOC = 0
    this.w_DATSCI = IIF(EMPTY(this.oParentObject.w_EDDATSCI),Cp_CharToDate("01-01-1900"),this.oParentObject.w_EDDATSCI)
    this.w_DATSCF = IIF(EMPTY(this.oParentObject.w_EDDATSCF),Cp_CharToDate("31-12-2999"),this.oParentObject.w_EDDATSCF)
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Dettaglio P.N. e Saldi
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    if this.w_APPSEZ="D"
      this.w_PNIMPDAR = ABS(this.w_APPVAL)
      this.w_PNIMPAVE = 0
    else
      this.w_PNIMPDAR = 0
      this.w_PNIMPAVE = ABS(this.w_APPVAL)
    endif
    if Not EMPTY(this.w_CCDESRIG)
      this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
    endif
    * --- Eventuali Righe non Valorizzate
    this.w_PNFLZERO = IIF(this.w_PNIMPDAR=0 AND this.w_PNIMPAVE=0, "S", " ")
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNDESRIG"+",PNCAURIG"+",PNCODPAG"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+",PNINICOM"+",PNFINCOM"+",PNIMPIND"+",PNFLABAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PNT_DETT','PNFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODPAG),'PNT_DETT','PNCODPAG');
      +","+cp_NullLink(cp_ToStrODBC("+"),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PNT_DETT','PNINICOM');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'PNT_DETT','PNFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_DETT','PNIMPIND');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(6)),'PNT_DETT','PNFLABAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',this.w_PNFLPART,'PNFLZERO',this.w_PNFLZERO,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCODCAU,'PNCODPAG',this.w_PNCODPAG)
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNDESRIG,PNCAURIG,PNCODPAG,PNFLSALD,PNFLSALI,PNFLSALF,PNINICOM,PNFINCOM,PNIMPIND,PNFLABAN &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,this.w_PNFLPART;
           ,this.w_PNFLZERO;
           ,this.w_PNDESRIG;
           ,this.w_PNCODCAU;
           ,this.w_PNCODPAG;
           ,"+";
           ," ";
           ," ";
           ,cp_CharToDate("  -  -  ");
           ,cp_CharToDate("  -  -  ");
           ,0;
           ,SPACE(6);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorna Saldo
    * --- Try
    local bErr_0517BE40
    bErr_0517BE40=bTrsErr
    this.Try_0517BE40()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0517BE40
    * --- End
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
      +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
          +i_ccchkf ;
      +" where ";
          +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
          +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
          +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
             )
    else
      update (i_cTable) set;
          SLDARPER = SLDARPER + this.w_PNIMPDAR;
          ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
          &i_ccchkf. ;
       where;
          SLTIPCON = this.w_PNTIPCON;
          and SLCODICE = this.w_PNCODCON;
          and SLCODESE = this.w_PNCODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc
  proc Try_0517BE40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Cursori di Appoggio
    if USED("CONTADOC")
      SELECT CONTADOC
      USE
    endif
    if USED("CONTIIVA")
      SELECT CONTIIVA
      USE
    endif
    if USED("RIGHEIVA")
      SELECT RIGHEIVA
      USE
    endif
    if USED("MessErr")
      SELECT MessErr
      USE
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    if USED("REG_IVA")
      SELECT REG_IVA
      USE
    endif
    if USED("ESIGI")
      SELECT ESIGI
      USE
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Not Empty(this.w_SERDOC)
      * --- Creo le Partite
      this.w_ROWORD = 0
      this.w_ROWPAR = 0
      this.w_NUMRIF = 0
      this.w_TOTIMP = 0
      this.w_TOTPAR = 0
      * --- Leggo riferimento riga Iva esigibilit� differita
      * --- Read from PNT_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2],.t.,this.PNT_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CPROWNUM"+;
          " from "+i_cTable+" PNT_DETT where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
              +" and PNFLPART = "+cp_ToStrODBC("C");
              +" and PNTIPCON = "+cp_ToStrODBC("G");
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CPROWNUM;
          from (i_cTable) where;
              PNSERIAL = this.w_SERDOC;
              and PNFLPART = "C";
              and PNTIPCON = "G";
              and CPROWNUM = this.w_ROWNUM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ROWORD = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_ROWORD<>0
        * --- Inserisco dettaglio partite iva esigibilit� differita
        this.w_TOTPAR = this.w_PTTOTIMP
        * --- Select from PAR_TITE
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
              +" where PTSERIAL="+cp_ToStrODBC(this.w_SERDOC)+" AND PTROWORD="+cp_ToStrODBC(this.w_ROWORD)+"";
               ,"_Curs_PAR_TITE")
        else
          select * from (i_cTable);
           where PTSERIAL=this.w_SERDOC AND PTROWORD=this.w_ROWORD;
            into cursor _Curs_PAR_TITE
        endif
        if used('_Curs_PAR_TITE')
          select _Curs_PAR_TITE
          locate for 1=1
          do while not(eof())
          this.w_CONTA = this.w_CONTA-1
          this.w_ROWPAR = this.w_ROWPAR + 1
          this.w_PTCODVAL = _Curs_PAR_TITE.PTCODVAL
          this.w_PTDATSCA = _Curs_PAR_TITE.PTDATSCA
          this.w_PTIMPDOC = Nvl(_Curs_PAR_TITE.PTIMPDOC,0)
          this.w_PTMODPAG = _Curs_PAR_TITE.PTMODPAG
          this.w_PTNUMPAR = _Curs_PAR_TITE.PTNUMPAR
          if this.w_CONTA=0
            * --- Ultima partita
            this.w_TOTIMP = this.w_TOTPAR
          else
            this.w_TOTIMP = cp_ROUND((Nvl(_Curs_PAR_TITE.PTTOTIMP,0)*this.w_PTTOTIMP)/this.w_PTIMPDOC,this.w_DECTOP)
          endif
          this.w_TOTPAR = this.w_TOTPAR-this.w_TOTIMP
          this.w_NUMRIF = _Curs_PAR_TITE.CPROWNUM
          this.w_CODAGE = _Curs_PAR_TITE.PTCODAGE
          this.w_FLVABD = Nvl(_Curs_PAR_TITE.PTFLVABD," ")
          * --- Insert into PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMDIS"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTFLCRSA"+",PTFLIMPE"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTCODAGE"+",PTFLVABD"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWPAR),'PAR_TITE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PAR_TITE','PTTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PAR_TITE','PTCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TOTIMP),'PAR_TITE','PTTOTIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAOAPE),'PAR_TITE','PTCAOAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTBANAPP');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTBANNOS');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLCRSA');
            +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SERDOC),'PAR_TITE','PTSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'PAR_TITE','PTORDRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIF),'PAR_TITE','PTNUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_FLVABD),'PAR_TITE','PTFLVABD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(25)),'PAR_TITE','PTNUMCOR');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_CPROWNUM,'CPROWNUM',this.w_ROWPAR,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_PNTIPCON,'PTCODCON',this.w_PNCODCON,'PT_SEGNO',this.w_APPSEZ,'PTTOTIMP',this.w_TOTIMP,'PTCODVAL',this.w_PTCODVAL,'PTCAOVAL',this.w_PNCAOVAL,'PTCAOAPE',this.w_CAOAPE)
            insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMDIS,PTFLINDI,PTBANNOS,PTFLRAGG,PTFLCRSA,PTFLIMPE,PTSERRIF,PTORDRIF,PTNUMRIF,PTCODAGE,PTFLVABD,PTDATREG,PTNUMCOR &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 ,this.w_CPROWNUM;
                 ,this.w_ROWPAR;
                 ,this.w_PTNUMPAR;
                 ,this.w_PTDATSCA;
                 ,this.w_PNTIPCON;
                 ,this.w_PNCODCON;
                 ,this.w_APPSEZ;
                 ,this.w_TOTIMP;
                 ,this.w_PTCODVAL;
                 ,this.w_PNCAOVAL;
                 ,this.w_CAOAPE;
                 ,this.w_PNDATDOC;
                 ,this.w_PNNUMDOC;
                 ,this.w_PNALFDOC;
                 ,this.w_PNDATDOC;
                 ,this.w_PTIMPDOC;
                 ,this.w_PTMODPAG;
                 ," ";
                 ," ";
                 ," ";
                 ," ";
                 ," ";
                 ," ";
                 ,"S";
                 ,"  ";
                 ,this.w_SERDOC;
                 ,this.w_ROWORD;
                 ,this.w_NUMRIF;
                 ,this.w_CODAGE;
                 ,this.w_FLVABD;
                 ,this.w_PNDATREG;
                 ,SPACE(25);
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
            select _Curs_PAR_TITE
            continue
          enddo
          use
        endif
      endif
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_FLERR
      this.w_APPO = Ah_MsgFormat("Verificare doc. n.: %1 del.: %2",ALLTRIM(STR(this.w_PNNUMDOC,15))+IIF(EMPTY(this.w_PNALFDOC),"","/"+Alltrim(this.w_PNALFDOC)),DTOC(this.w_PNDATDOC))
      INSERT INTO MessErr (MSG) VALUES (this.w_APPO)
      FOR L_i = 1 TO 18
      if NOT EMPTY(MES[L_i])
        INSERT INTO MessErr (MSG) VALUES (SPACE(10)+MES[L_i])
      endif
      ENDFOR
      INSERT INTO MessErr (MSG) VALUES (SPACE(50))
      * --- Abbandona la Registrazione
      * --- Raise
      i_Error="Errore"
      return
    else
      * --- Aggiorna Flag Esig.Differita sulla Reg. Originaria di Incasso/Pagamento 
      * --- Write into PNT_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNFLGDIF ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLGDIF),'PNT_MAST','PNFLGDIF');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_OSERIAL);
               )
      else
        update (i_cTable) set;
            PNFLGDIF = this.w_PNFLGDIF;
            &i_ccchkf. ;
         where;
            PNSERIAL = this.w_OSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorno righe IVA a zero per storno alternativo
      if this.w_TIPO="N"
        * --- Write into PNT_IVA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="IVSERIAL,CPROWNUM"
          do vq_exec with 'gscgebgd',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_IVA_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="PNT_IVA.IVSERIAL = _t2.IVSERIAL";
                  +" and "+"PNT_IVA.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IVFLSTOD ="+cp_NullLink(cp_ToStrODBC("S"),'PNT_IVA','IVFLSTOD');
              +i_ccchkf;
              +" from "+i_cTable+" PNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="PNT_IVA.IVSERIAL = _t2.IVSERIAL";
                  +" and "+"PNT_IVA.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_IVA, "+i_cQueryTable+" _t2 set ";
          +"PNT_IVA.IVFLSTOD ="+cp_NullLink(cp_ToStrODBC("S"),'PNT_IVA','IVFLSTOD');
              +Iif(Empty(i_ccchkf),"",",PNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="PNT_IVA.IVSERIAL = t2.IVSERIAL";
                  +" and "+"PNT_IVA.CPROWNUM = t2.CPROWNUM";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_IVA set (";
              +"IVFLSTOD";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC("S"),'PNT_IVA','IVFLSTOD')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="PNT_IVA.IVSERIAL = _t2.IVSERIAL";
                  +" and "+"PNT_IVA.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_IVA set ";
          +"IVFLSTOD ="+cp_NullLink(cp_ToStrODBC("S"),'PNT_IVA','IVFLSTOD');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".IVSERIAL = "+i_cQueryTable+".IVSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IVFLSTOD ="+cp_NullLink(cp_ToStrODBC("S"),'PNT_IVA','IVFLSTOD');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Se � installato/abilitato il modulo 'Trasferimento studio', aggiorno il campo
      *     w_Pncodcau con la causale di testata, dato che la variabile contiene
      *     la causale di giroconto impostata in "Tabella trasferimento studio"
      if LMAVANZATO()
        this.w_PNCODCAU = this.w_OLDCODCAU
      endif
      * --- Memorizzazione in ESI_DETT dei dati necessari per il dattaglio del Piano dei movimenti ad Esig. Differita. 
      this.w_DPIMPINC = IIF(this.w_TIPREG="S",0,this.w_IMPINC)
      * --- Devo marcare il record per non eseguire smarcamento IVFLGSTO
      this.w_TIPREG1 = IIF(this.w_TIPO="N","E",this.w_TIPREG)
      * --- Try
      local bErr_05152FC8
      bErr_05152FC8=bTrsErr
      this.Try_05152FC8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_05152FC8
      * --- End
      * --- commit
      cp_EndTrs(.t.)
      this.w_OKDOC = this.w_OKDOC + 1
    endif
  endproc
  proc Try_05152FC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ESI_DETT
    i_nConn=i_TableProp[this.ESI_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESI_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ESI_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DPSERIAL"+",DPSERINC"+",DPSERMOV"+",DPIMPINC"+",DPIMPDOC"+",DPCAUMOV"+",DPNUMREG"+",DPDATREG"+",DPCOMPET"+",DPTIPREG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'ESI_DETT','DPSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'ESI_DETT','DPSERINC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'ESI_DETT','DPSERMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DPIMPINC),'ESI_DETT','DPIMPINC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDOC),'ESI_DETT','DPIMPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'ESI_DETT','DPCAUMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'ESI_DETT','DPNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'ESI_DETT','DPDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'ESI_DETT','DPCOMPET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREG1),'ESI_DETT','DPTIPREG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DPSERIAL',this.w_SERIAL,'DPSERINC',this.w_OSERIAL,'DPSERMOV',this.w_PNSERIAL,'DPIMPINC',this.w_DPIMPINC,'DPIMPDOC',this.w_IMPDOC,'DPCAUMOV',this.w_PNCODCAU,'DPNUMREG',this.w_PNNUMRER,'DPDATREG',this.w_DATREG,'DPCOMPET',this.w_PNCOMPET,'DPTIPREG',this.w_TIPREG1)
      insert into (i_cTable) (DPSERIAL,DPSERINC,DPSERMOV,DPIMPINC,DPIMPDOC,DPCAUMOV,DPNUMREG,DPDATREG,DPCOMPET,DPTIPREG &i_ccchkf. );
         values (;
           this.w_SERIAL;
           ,this.w_OSERIAL;
           ,this.w_PNSERIAL;
           ,this.w_DPIMPINC;
           ,this.w_IMPDOC;
           ,this.w_PNCODCAU;
           ,this.w_PNNUMRER;
           ,this.w_DATREG;
           ,this.w_PNCOMPET;
           ,this.w_TIPREG1;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in Inserimento ESI_DETT'
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='PNT_DETT'
    this.cWorkTables[4]='PNT_IVA'
    this.cWorkTables[5]='SALDICON'
    this.cWorkTables[6]='PAR_TITE'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='ESI_DETT'
    this.cWorkTables[9]='ESI_DIF'
    this.cWorkTables[10]='CAU_CONT'
    this.cWorkTables[11]='*TMPPNT_MAST'
    this.cWorkTables[12]='*TMP_PART_APE'
    this.cWorkTables[13]='CONTI'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_Gscg11bgd')
      use in _Curs_Gscg11bgd
    endif
    if used('_Curs_GSCG5BGD')
      use in _Curs_GSCG5BGD
    endif
    if used('_Curs_QUERY_CAUGIR')
      use in _Curs_QUERY_CAUGIR
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
