* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bim                                                        *
*              Invio mail supporto SMTP                                        *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-31                                                      *
* Last revis.: 2017-05-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bim",oParentObject,m.pTipo)
return(i_retval)

define class tgsut_bim as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_PATH = space(250)
  w_FILEH = space(250)
  w_FILEB = space(250)
  w_FILEL = space(250)
  w_FILELT = space(250)
  w_FILEHANDLE = 0
  w_RES = 0
  w_NUMARRAY = 0
  w_NUMALLEG = 0
  w_APPSTR = space(254)
  w_FILELEN = 0
  w_MESSAGE = space(254)
  w_MskMESS = .NULL.
  w_ERRNO = 0
  w_RET = 0
  w_oOutlook = .NULL.
  w_oNameSpace = .NULL.
  w_oFolderSent = .NULL.
  w_oNewMail = .NULL.
  w_oAttachments = .NULL.
  w_oPropMail = .NULL.
  w_oItems = .NULL.
  w_LICLADOC = space(100)
  w_MODALLEG = space(1)
  w_TIPOARCH = space(1)
  w_CLAALLE = space(5)
  w_TIPPATH = space(1)
  w_TIPOALLE = space(5)
  w_TIPCLA = space(1)
  w_TIMEINIT = ctot("")
  w_FOUND = .f.
  w_PropName = space(254)
  w_PropValue = space(254)
  w_CategoriesList = space(254)
  w_OKCREATE = .f.
  w_AUTSRV = space(1)
  w_PEMODCON = space(10)
  w_PEMODAUT = space(15)
  w_Conta = 0
  w_TIPOCONTO = space(1)
  w_CODICECONTO = space(15)
  w_ANLOCALI = space(30)
  w_ANTELEFO = space(18)
  w_ANDESCRI = space(40)
  w_FileOut = space(250)
  w_FileWriteHandle = 0
  w_OGGETTO = space(250)
  w_ANTELFAX = space(18)
  w_COMMENTO = space(254)
  w_TO = space(0)
  w_Risultato = 0
  w_NOMEFILE = space(254)
  w_NUMREC = 0
  w_POS = 0
  w_MAILADDRESSES = space(0)
  w_POS = 0
  w_MAILADDRESSES = space(0)
  * --- WorkFile variables
  CONTI_idx=0
  PROMCLAS_idx=0
  PROMINDI_idx=0
  ACC_MAIL_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invio Mail Supporto SMTP, lanciato da GSUT_KIM
    * --- INVIO   - Invia Mail
    *     ALLEG - Selezione File allegati
    do case
      case this.pTipo="INVIO"
        if TYPE("w_Result_EMAIL")<>"U"
          release w_Result_EMAIL
        endif
        if this.oParentObject.w_UseOutlook
          * --- Istanzio oggetto Outlook
          private L_bErr, L_OLDERR
          private L_SerIndex
          L_SerIndex=""
          L_OLDERR = ON("ERROR")
          ON ERROR L_bErr=.T.
          L_bErr = .F.
          this.w_MskMESS = GSUT_KEL()
          addmsgnl("Inizializzazione oggetto Microsoft Outlook", this.w_MskMESS)
          * --- Disabilita il Decoartor altrimento si generano errori quando l utente muove il mouse sul form
          DecoratorState ("D")
          this.w_OKCREATE = .f.
          * --- Try
          local bErr_043432F8
          bErr_043432F8=bTrsErr
          this.Try_043432F8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
          endif
          bTrsErr=bTrsErr or bErr_043432F8
          * --- End
          if VARTYPE(this.w_oOutlook) <> "O"
            L_bErr = .F.
            this.w_oOutlook = CreateObject("Outlook.Application")
          endif
          if L_bERR or VARTYPE(this.w_oOutlook)<>"O"
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          addmsgnl("Inizializzazione oggetto area di lavoro MAPI", this.w_MskMESS)
          this.w_oNameSpace = this.w_oOutlook.GetNamespace("MAPI")
          if L_bErr OR VARTYPE(this.w_oNameSpace)<>"O"
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Riferimento a cartella Posta Inviata
          addmsgnl("Inizializzazione riferimento a cartella 'Posta inviata'", this.w_MskMESS)
          this.w_oFolderSent = this.w_oNameSpace.GetDefaultfolder(5)
          if L_bErr OR VARTYPE(this.w_oFolderSent)<>"O"
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Creazione nuovo messaggio
          this.w_oNewMail = this.w_oOutlook.CreateItem(0)
          * --- inserimento destinatari
          this.w_oNewMail.To = ALLTRIM(this.oParentObject.w_SMTP__TO)
          if !EMPTY(this.oParentObject.w_SMTP__CC)
            this.w_oNewMail.CC = ALLTRIM(this.oParentObject.w_SMTP__CC)
          endif
          if !EMPTY(this.oParentObject.w_SMTP_CCN)
            this.w_oNewMail.BCC = ALLTRIM(this.oParentObject.w_SMTP_CCN)
          endif
          if this.oParentObject.w_SMTPPRIO>0
            * --- Select Case objTask.Importance
            *             Case 0
            *                 Wscript.Echo "Importance: Low"
            *             Case 1
            *                 Wscript.Echo "Importance: Normal"
            *             Case 2
            *                 Wscript.Echo "Importance: High"
            *             End Select
            this.w_oNewMail.Importance = ICASE(this.oParentObject.w_SMTPPRIO=5,0,this.oParentObject.w_SMTPPRIO=3,1,2)
            if L_bERR
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          addmsgnl("Verifica destinatari", this.w_MskMESS)
          if this.w_OKCREATE
            * --- Faccio solo se creazione
            this.w_oNewMail.Recipients.ResolveAll()     
          endif
          if L_bERR
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          addmsgnl("Inserimento oggetto e testo e-mail", this.w_MskMESS)
          this.w_oNewMail.Subject = ALLTRIM(this.oParentObject.w_SMTPSUBJ)
          this.w_oNewMail.Body = ALLTRIM(this.oParentObject.w_SMTPBODY)
          if !EMPTY(this.oParentObject.w_AR_TABLE) AND EVL(this.oParentObject.w_AROPERAZ, "M") = "A"
            addmsgnl("Creazione indice", this.w_MskMESS)
            addmsgnl("Lettura informazioni classe documentale", this.w_MskMESS)
            * --- Legge, se c'�, la classe di archiviazione standard
            * --- Read from PROMCLAS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PROMCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL"+;
                " from "+i_cTable+" PROMCLAS where ";
                    +"CDRIFTAB = "+cp_ToStrODBC(this.oParentObject.w_AR_TABLE);
                    +" and CDRIFDEF = "+cp_ToStrODBC("S");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL;
                from (i_cTable) where;
                    CDRIFTAB = this.oParentObject.w_AR_TABLE;
                    and CDRIFDEF = "S";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
              this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
              this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
              this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
              this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
              this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
              this.w_TIPOALLE = NVL(cp_ToDate(_read_.CDTIPALL),cp_NullValue(_read_.CDTIPALL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if empty(this.w_LICLADOC)
              * --- Verifica se c'� almeno una classe
              * --- Read from PROMCLAS
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PROMCLAS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL"+;
                  " from "+i_cTable+" PROMCLAS where ";
                      +"CDRIFTAB = "+cp_ToStrODBC(this.oParentObject.w_AR_TABLE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL;
                  from (i_cTable) where;
                      CDRIFTAB = this.oParentObject.w_AR_TABLE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
                this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
                this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
                this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
                this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
                this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
                this.w_TIPOALLE = NVL(cp_ToDate(_read_.CDTIPALL),cp_NullValue(_read_.CDTIPALL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_rows=0
                addmsgnl("ATTENZIONE!! Classe documentale non definita", this.w_MskMESS)
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
            * --- Verifica se l'utente pu� archiviare ....
            if SUBSTR( CHKPECLA( this.w_LICLADOC, i_CODUTE, this.oParentObject.w_AR_TABLE), 2, 1 ) <> "S"
              addmsgnl("ATTENZIONE!! Non si possiedono i diritti per archiviare documenti con la classe documentale %1", this, ALLTRIM(this.w_LICLADOC))
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            private L_SerIndex
            L_SerIndex=""
            Private L_ArrParam
            dimension L_ArrParam(30)
            L_ArrParam(1)="AR"
            L_ArrParam(2)=ADDBS(ALLTRIM(this.oParentObject.w_AR__PATH))+ALLTRIM(this.oParentObject.w_ARFILNAM)
            L_ArrParam(3)=this.w_LICLADOC
            L_ArrParam(4)=i_CODUTE
            L_ArrParam(5)=this
            L_ArrParam(6)=this.oParentObject.w_AR_TABLE
            L_ArrParam(7)=ALLTRIM(STRTRAN(this.oParentObject.w_ARTABKEY, "\", ""))
            L_ArrParam(8)=ALLTRIM(this.oParentObject.w_AROGGETT)
            L_ArrParam(9)=this.oParentObject.w_AR__NOTE
            L_ArrParam(10)=.T.
            L_ArrParam(11)=.T.
            L_ArrParam(12)=" "
            L_ArrParam(13)=" "
            L_ArrParam(14)=this
            L_ArrParam(15)=this.oParentObject.w_ARTIPALL
            L_ArrParam(16)=this.oParentObject.w_ARCLAALL
            L_ArrParam(17)=" "
            L_ArrParam(18)=" "
            L_ArrParam(19)="A"
            GSUT_BBA(this,@L_ArrParam, @L_SerIndex)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            L_SerIndex=LEFT(L_SerIndex, AT("*", L_SerIndex)-1)
            if VARTYPE(g_OUTLOOKPECNOPROPERTIES)=="L" AND g_OUTLOOKPECNOPROPERTIES
              addmsgnl("Aggiunta categorie personalizzate messaggio", this.w_MskMESS)
              addmsgnl("%1Aggiunta categoria adhocTable", this.w_MskMESS, CHR(9))
              this.w_CategoriesList = "adhocTable=" + ALLTRIM(this.oParentObject.w_AR_TABLE)
              addmsgnl("%1Aggiunta categoria adhocTableKey", this.w_MskMESS, CHR(9))
              this.w_CategoriesList = this.w_CategoriesList + ";adhocTableKey=" + ALLTRIM(this.oParentObject.w_ARTABKEY)
              addmsgnl("%1Aggiunta categoria adhocProcessed", this.w_MskMESS, CHR(9))
              this.w_CategoriesList = this.w_CategoriesList + ";adhocProcessed=N"
              addmsgnl("%1Aggiunta categoria adhocIndexID", this.w_MskMESS, CHR(9))
              this.w_CategoriesList = this.w_CategoriesList + ";adhocIndexID=" + ALLTRIM(L_SerIndex)
              addmsgnl("%1Aggiunta categoria adhocCompany", this.w_MskMESS, CHR(9))
              this.w_CategoriesList = this.w_CategoriesList + ";adhocCompany=" + ALLTRIM(i_CodAzi)
              addmsgnl("%1Inserimento categorie personalizzate messaggio", this.w_MskMESS, CHR(9) )
              this.w_oNewMail.Categories = this.w_CategoriesList
              if L_bERR
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              addmsgnl("Aggiunta propriet� personalizzate messaggio", this.w_MskMESS)
              addmsgnl("%1Aggiunta propriet� adhocTable", this.w_MskMESS, CHR(9))
              this.w_oPropMail = this.w_oNewMail.UserProperties.Add("adhocTable",1)
              this.w_oPropMail.Value = ALLTRIM(this.oParentObject.w_AR_TABLE)
              if L_bERR
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              addmsgnl("%1Aggiunta propriet� adhocTableKey", this.w_MskMESS, CHR(9))
              this.w_oPropMail = this.w_oNewMail.UserProperties.Add("adhocTableKey",1)
              this.w_oPropMail.Value = ALLTRIM(this.oParentObject.w_ARTABKEY)
              if L_bERR
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              addmsgnl("%1Aggiunta propriet� adhocProcessed", this.w_MskMESS, CHR(9))
              this.w_oPropMail = this.w_oNewMail.UserProperties.Add("adhocProcessed",1)
              this.w_oPropMail.Value = "N"
              if L_bERR
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              addmsgnl("%1Aggiunta propriet� adhocIndexID", this.w_MskMESS, CHR(9))
              this.w_oPropMail = this.w_oNewMail.UserProperties.Add("adhocIndexID",1)
              this.w_oPropMail.Value = ALLTRIM(L_SerIndex)
              if L_bERR
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              addmsgnl("%1Aggiunta propriet� adhocCompany", this.w_MskMESS, CHR(9))
              this.w_oPropMail = this.w_oNewMail.UserProperties.Add("adhocCompany",1)
              this.w_oPropMail.Value = ALLTRIM(i_CodAzi)
              if L_bERR
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
          endif
          if !EMPTY(this.oParentObject.w_SMTP_ATT)
            addmsgnl("Inserimento allegati", this.w_MskMESS)
            this.w_oAttachments = this.w_oNewMail.Attachments
            if L_bERR or VARTYPE(this.w_oAttachments)<>"O"
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.w_oAttachments = this.w_oNewMail.Attachments
            ALINES(ArrATT, this.oParentObject.w_SMTP_ATT, ";")
            this.w_NUMALLEG = 1
            do while this.w_NUMALLEG<=ALEN(ArrATT, 1) AND !L_bERR
              addmsgnl("%1Inserimento allegato %2", this, CHR(9), ALLTRIM(ArrATT(this.w_NUMALLEG)))
              this.w_oAttachments.Add(ALLTRIM(ArrATT(this.w_NUMALLEG)))     
              this.w_NUMALLEG = this.w_NUMALLEG + 1
            enddo
            if L_bERR
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          addmsgnl("Invio mail", this.w_MskMESS)
          * --- La w_VISMAIL viene utilizzata per memorizzare il valore della w_ShowDialog (definita nella GestMail) passato alla GSUT_KIM.
          *     Nel caso non si provenga dalla GestMail() la w_VISMAIL contiene il valore della g_UEDIAL.
          if this.oParentObject.w_VISMAIL="A" Or (p_Dialog="P" And Vartype(g_DOCM_EMAILSN)="L" And g_DOCM_EMAILSN)
            this.w_oNewMail.Send()     
          else
            this.w_oNewMail.Display()     
          endif
          if VARTYPE(g_MAILSYNC)="C" AND g_MAILSYNC="S" AND !EMPTY(this.oParentObject.w_AR_TABLE)
            this.w_FOUND = .F.
            addmsg("Attesa invio e-mail...", this.w_MskMESS)
            if VARTYPE(g_OUTLOOKPECNOPROPERTIES)=="L" AND g_OUTLOOKPECNOPROPERTIES
              * --- Suddivido la lista delle categorie in un array
              this.w_NUMARRAY = ALINES( aCategories, this.w_CategoriesList, 5, ";" )
              this.w_CategoriesList = "@SQL="
              do while this.w_NUMARRAY > 0
                this.w_CategoriesList = this.w_CategoriesList + CHR(34)+"http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/Keywords/0x0000101F"+CHR(34) +" = '" + aCategories(this.w_NUMARRAY) + "' AND "
                this.w_NUMARRAY = this.w_NUMARRAY - 1
              enddo
              this.w_CategoriesList = LEFT(this.w_CategoriesList, LEN(this.w_CategoriesList) - 5)
            endif
            this.w_TIMEINIT = DATETIME()
            do while !this.w_FOUND AND DATETIME()<this.w_TIMEINIT + g_MAILSYNC_TIMEOUT
              if VARTYPE(g_OUTLOOKPECNOPROPERTIES)=="L" AND g_OUTLOOKPECNOPROPERTIES
                this.w_oItems = this.w_oFolderSent.Items.Restrict(this.w_CategoriesList)
              else
                this.w_oItems = this.w_oFolderSent.Items.Restrict("@SQL=" + Chr(34) + "http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/adhocTable" + Chr(34) + " is not null AND "+CHR(34)+"http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/adhocProcessed"+CHR(34)+"='N' AND "+CHR(34)+"http://schemas.microsoft.com/mapi/string/{00020329-0000-0000-C000-000000000046}/adhocIndexID"+CHR(34)+"='"+ALLTRIM(L_SerIndex)+"'")
              endif
              if L_bErr
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              this.w_FOUND = this.w_oItems.Count<>0
              INKEY(1, "H")
              addmsg(".", this.w_MskMESS)
            enddo
            addmsgnl("", this.w_MskMESS)
            if this.w_FOUND
              if VARTYPE(g_OUTLOOKPECNOPROPERTIES)=="L" AND g_OUTLOOKPECNOPROPERTIES
                this.w_oNewMail = this.w_oItems.Item(1)
                this.w_CategoriesList = this.w_oNewMail.Categories
                * --- Suddivido la lista delle categorie in un array
                this.w_NUMARRAY = ALINES( aCategories, this.w_CategoriesList, 5, ";" )
                addmsgnl("Aggiunta propriet� personalizzate messaggio", this.w_MskMESS)
                do while this.w_NUMARRAY > 0
                  * --- Suddivido la propriet� dal suo valore e inserisco le nuove propriet� personalizzate
                  this.w_PropName = LEFT( aCategories(this.w_NUMARRAY), AT("=", aCategories(this.w_NUMARRAY)) - 1 )
                  this.w_PropValue = RIGHT( aCategories(this.w_NUMARRAY), LEN(aCategories(this.w_NUMARRAY)) - LEN(this.w_PropName) - 1 )
                  addmsgnl("%1Aggiunta propriet� %2 con valore %3", this.w_MskMESS, CHR(9), this.w_PropName, this.w_PropValue)
                  this.w_oPropMail = this.w_oNewMail.UserProperties.Add(this.w_PropName,1)
                  this.w_oPropMail.Value = this.w_PropValue
                  if L_bERR
                    this.Page_4()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  this.w_NUMARRAY = this.w_NUMARRAY - 1
                enddo
                * --- Elimino le categorie dal messaggio
                this.w_oNewMail.Categories = ""
                this.w_oNewMail.Save()     
              endif
              addmsgnl("E-mail inviata salvataggio e aggiornamento indice...", this.w_MskMESS)
              * --- Ritardo per errore Outlook salvataggio messaggio
              INKEY(5, "H")
              if GSUT_BEM(this, L_SerIndex)
                addmsgnl("errore", this.w_MskMESS)
                L_bErr = .T.
                this.Page_4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                addmsgnl("Ok", this.w_MskMESS)
              endif
            endif
          endif
          ON ERROR &L_OLDERR
          if L_bERR
            this.w_Risultato = -1
            public w_Result_EMAIL 
 w_Result_EMAIL=this.w_Risultato
          else
            this.w_Risultato = 0
          endif
          this.w_MskMESS.ecpQuit()     
          this.w_MskMESS = .NULL.
          * --- Al termine del esportazion riattiva il decorator dei form 
          DecoratorState ("A")
          i_retcode = 'stop'
          i_retval = this.w_Risultato
          return
        else
          * --- Read from ACC_MAIL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ACC_MAIL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ACC_MAIL_idx,2],.t.,this.ACC_MAIL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AMMODAUT,AMMODCON,AMAUTSRV"+;
              " from "+i_cTable+" ACC_MAIL where ";
                  +"AMSERIAL = "+cp_ToStrODBC(g_AccountMail);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AMMODAUT,AMMODCON,AMAUTSRV;
              from (i_cTable) where;
                  AMSERIAL = g_AccountMail;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PEMODAUT = NVL(cp_ToDate(_read_.AMMODAUT),cp_NullValue(_read_.AMMODAUT))
            this.w_PEMODCON = NVL(cp_ToDate(_read_.AMMODCON),cp_NullValue(_read_.AMMODCON))
            this.w_AUTSRV = NVL(cp_ToDate(_read_.AMAUTSRV),cp_NullValue(_read_.AMAUTSRV))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if g_INVIO="P"
            ALINES(ArrATT, this.oParentObject.w_SMTP_ATT, ";")
            this.w_MskMESS = GSUT_KEL()
            this.w_Risultato = smtpssl( g_AccountMail, g_SRVMAIL, g_SRVPORTA, g_MITTEN, this.oParentObject.w_SMTP__TO, this.oParentObject.w_SMTP__CC, this.oParentObject.w_SMTP_CCN, this.oParentObject.w_SMTPSUBJ, this.oParentObject.w_SMTPBODY, @ArrATT, this.w_MskMESS, alltrim(this.w_PEMODCON), alltrim(this.w_PEMODAUT), "", "")
            if this.w_Risultato=0
              cp_msg("Inoltro mail al server riuscito")
            else
              cp_msg("Errore durante l'invio della mail")
            endif
            public w_Result_EMAIL 
 w_Result_EMAIL=this.w_Risultato
            if this.w_Risultato=0 OR (TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S")
              this.w_MskMESS.ecpQuit()     
              this.w_MskMESS = .NULL.
            endif
            this.w_RET = this.w_Risultato
          else
            if type("g_SMTPDLL")<>"U" And g_SMTPDLL
              * --- Massimo numero di elementi nelle varie sezioni 
              *     TO,CC,CCn
              *     Non e' possibile aumentare tale limite.
              this.w_NUMARRAY = 100
              * --- Numero Massimo di allegati
              *     Occorre considerare che alcuni server di posta impongono limiti sul 
              *     numero e soprattutto sulla dimensione totale degli allegati.
              this.w_NUMALLEG = 10
              * --- Recupero la cartella temporanea su cui saranno creati i file d'appoggio
              this.w_PATH = GETENV("TEMP")
              * --- File Header, conterr�:
              *     From
              *     To
              *     CC
              *     CCn
              *     Subject
              *     Attachment
              this.w_FILEH = ALLTRIM(this.w_PATH+"\head.smtp")
              this.w_FILEHANDLE = FCREATE(this.w_FILEH)
              FPUTS(this.w_FILEHANDLE,"@@FRO@@")
              FPUTS(this.w_FILEHANDLE,ALLTRIM(this.oParentObject.w_SMTPFROM))
              * --- Ogni elenco � prima convertito in un Array e poi scritto su File.
              *     Il file si trova nella cartella temporanea dell'utente.
              DECLARE TOARRAY(this.w_NUMARRAY,1)
              TOARRAY(1) = ""
              * --- ----------------- Scrittura primo File
              FPUTS(this.w_FILEHANDLE,"@@TO_@@")
              this.w_RES = STRTOARR(this.oParentObject.w_SMTP__TO,@TOARRAY)
              this.w_RES = ARRTOFILE(this.w_FILEHANDLE,@TOARRAY)
              TOARRAY(1) = ""
              FPUTS(this.w_FILEHANDLE,"@@CC_@@")
              this.w_RES = STRTOARR(this.oParentObject.w_SMTP__CC,@TOARRAY)
              this.w_RES = ARRTOFILE(this.w_FILEHANDLE,@TOARRAY)
              TOARRAY(1) = ""
              FPUTS(this.w_FILEHANDLE,"@@CCN@@")
              this.w_RES = STRTOARR(this.oParentObject.w_SMTP_CCN,@TOARRAY)
              this.w_RES = ARRTOFILE(this.w_FILEHANDLE,@TOARRAY)
              TOARRAY(1) = ""
              FPUTS(this.w_FILEHANDLE,"@@SUB@@")
              FPUTS(this.w_FILEHANDLE,ALLTRIM(this.oParentObject.w_SMTPSUBJ))
              DECLARE TOARRAY(this.w_NUMALLEG,1)
              TOARRAY(1) = ""
              FPUTS(this.w_FILEHANDLE,"@@ATT@@")
              this.w_RES = STRTOARR(this.oParentObject.w_SMTP_ATT,@TOARRAY)
              this.w_RES = ARRTOFILE(this.w_FILEHANDLE,@TOARRAY)
              FPUTS(this.w_FILEHANDLE,"@@SER@@")
              FPUTS(this.w_FILEHANDLE,ALLTRIM(this.oParentObject.w_SMTPSERV))
              FPUTS(this.w_FILEHANDLE,"@@POR@@")
              FPUTS(this.w_FILEHANDLE,ALLTRIM(STR(this.oParentObject.w_SMTPPORT)))
              FPUTS(this.w_FILEHANDLE,"@@END@@")
              * --- Chiudo primo file
              FCLOSE(this.w_FILEHANDLE)
              * --- ----------------- Fine Scrittura primo File
              this.w_FILEB = ALLTRIM(this.w_PATH+"\body.smtp")
              this.w_FILEHANDLE = FCREATE(this.w_FILEB)
              FPUTS(this.w_FILEHANDLE,this.oParentObject.w_SMTPBODY)
              FCLOSE(this.w_FILEHANDLE)
              * --- Richiamo il metodo sendmail della dll ah_smtp.dll per inviare l'E-Mail
              *     (la dll deve essere in EXE di ad hoc)
              *     Il metodo riceve come parametri due stringhe contenenti il path assoluto
              *     di due file di testo (quelli appena creati) contenenti i vari indirizzi e opzioni il primo
              *     e il testo della mail, il secondo.
              DECLARE INTEGER sendmail IN "ah_smtp.dll"; 
 string @ head, string @ body 
 head=this.w_FILEH 
 body=this.w_FILEB 
 sendmail(head,body)
              w_EXECUTECMD = "DELETE FILE "+ this.w_FILEH
              &w_EXECUTECMD
              w_EXECUTECMD = "DELETE FILE "+ this.w_FILEB
              &w_EXECUTECMD
              * --- Leggo il file di testo sul quale l'eseguibile mi avr� restituito l'esito dell'invio al server:
              *     0  = Tutto Ok
              *     -1 = Caduta connessione 
              *     -2 = Server inesistente/non trovato oppure porta non valida
              *     >0 indice array attachment non trovato
              this.w_FILEL = ALLTRIM(this.w_PATH+"\log.smtp")
              this.w_FILEHANDLE = FOPEN(this.w_FILEL, 0)
              this.w_ERRNO = Val ( FGETS(this.w_FILEHANDLE) )
              FCLOSE(this.w_FILEHANDLE)
              * --- Elimino file di log
              w_EXECUTECMD = "DELETE FILE "+ this.w_FILEL
              &w_EXECUTECMD
              do case
                case this.w_ERRNO = -2
                  this.w_MESSAGE = "Impossibile inviare e-mail%0Server <%1> inesistente, non trovato oppure porta (%2) non valida"
                case this.w_ERRNO = -1
                  this.w_MESSAGE = "Impossibile inviare e-mail%0Server <%1> porta (%2) connessione caduta"
                case this.w_ERRNO = 0
                  this.w_MESSAGE = "E-mail inviata al server con successo"
                case this.w_ERRNO >0
                  this.w_MESSAGE = "Impossibile inviare e-mail%0Il file allegato %3 non esiste o � inaccessibile"
              endcase
              if NOT UPPER(this.oParentObject.Class) = "TGSDM_BPD"
                ah_ErrorMsg( this.w_MESSAGE,48,"",g_SRVMAIL,Alltrim(Str(g_SRVPORTA)),TOARRAY[ this.w_ERRNO ])
              endif
              * --- Se E-Mail inviata con successo chiudo la maschera di invio
              if this.w_ERRNO = 0 
                if NOT (TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S")
                  if ah_YesNo("%1%0Vuoi visualizzare il file di log?","", this.w_MESSAGE)
                    this.w_FILELT = ALLTRIM(this.w_PATH+"\logtot.smtp")
                    do GSUT_KLL with this
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  this.oparentobject.ecpquit()
                endif
                * --- =================================
                * --- Assegno a w_RET l'esito dell' invio, w_RET sar� poi restituito
                this.w_RET = this.w_ERRNO
              else
                this.w_Risultato = this.w_ERRNO
                public w_Result_EMAIL 
 w_Result_EMAIL=this.w_Risultato
              endif
            else
              * --- Nel caso di invio tramite 'FAX' modifico il codice di invio aggiungendo
              *     la stringa '@fax.zuum.it
              if this.oParentObject.w_IsFax
                * --- Verifico eventuali caratteri non corretti nel numero di fax dei clienti selezionati
                this.w_TO = this.oParentObject.w_SMTP__TO
                this.w_Conta = 1
                this.oParentObject.w_SMTP__TO = ""
                do while Len(this.w_TO)>=this.w_CONTA
                  if SUBSTR(this.w_TO,this.w_Conta,1)$"0123456789;+"
                    this.oParentObject.w_SMTP__TO = this.oParentObject.w_SMTP__TO+SUBSTR(this.w_TO,this.w_Conta,1)
                  endif
                  this.w_Conta = this.w_Conta+1
                enddo
                * --- Controllo se l'utente ha settato il flag "Utilizza frontespizio per FAX"
                if this.oParentObject.w_COPERTINA=1
                  * --- Devo scrivere il contenuto del frontespizio
                  this.w_FileOut = tempadhoc()+"\"+"copertine.csv"
                  this.w_FileWriteHandle = FCREATE(this.w_FileOut)
                  if this.w_FileWriteHandle<0
                    ah_errormsg("Impossibile aprire il file in scrittura",48)
                    * --- Al termine del esportazion riattiva il decorator dei form 
                    DecoratorState ("A")
                    i_retcode = 'stop'
                    return
                  else
                    ALINES(ArrCH, this.oParentObject.w_SMTP__TO, ";")
                    if ALEN(ArrCh)>0 and NOT EMPTY(ArrCh)
                      * --- Leggo i dati anagrafici dall'elenco dei codici conto selezionati
                      this.w_Conta = 1
                      do while this.w_Conta<=ALEN(ArrCh)
                        this.w_ANTELFAX = Arrch(this.w_Conta)
                        this.w_COMMENTO = STRTRAN(this.oParentObject.w_SMTPCOMM,"\","\\")
                        this.w_COMMENTO = STRTRAN(this.w_COMMENTO,",","\,")
                        L_RAGAZI=STRTRAN(g_RAGAZI,"\","\\")
                        L_RAGAZI=STRTRAN(L_RAGAZI,",","\,")
                        L_LOCAZI=STRTRAN(g_LOCAZI,"\","\\")
                        L_LOCAZI=STRTRAN(L_LOCAZI,",","\,")
                        L_TELFAX=STRTRAN(g_TELFAX,"\","\\")
                        L_TELFAX=STRTRAN(L_TELFAX,",","\,")
                        L_TELEFO=STRTRAN(g_TELEFO,"\","\\")
                        L_TELEFO=STRTRAN(L_TELEFO,",","\,")
                        this.w_OGGETTO = STRTRAN(this.oParentObject.w_SMTPSUBJ,"\","\\")
                        this.w_OGGETTO = STRTRAN(this.w_OGGETTO,",","\,")
                        L_RECCOP=this.w_Antelfax+","+"predefinita"+","+iif(empty(this.w_Commento),",",Alltrim(this.w_Commento)+",")+Alltrim(l_Ragazi)+","; 
 +Alltrim(l_Locazi)+","+Alltrim(l_Telfax)+","+iif(empty(this.w_Oggetto),",",Alltrim(this.w_Oggetto)+","); 
 +","+","+","+","+","
                        w_t = fwrite(this.w_FileWriteHandle,L_Reccop+chr(10))
                        this.w_Conta = this.w_Conta+1
                      enddo
                    endif
                    * --- Chiudo il file
                    =FCLOSE(this.w_FileWriteHandle)
                    this.oParentObject.w_SMTP_ATT = IIF(EMPTY(this.oParentObject.w_SMTP_ATT),this.w_FileOut,this.oParentObject.w_SMTP_ATT+";"+this.w_FileOut)
                  endif
                endif
                * --- Nel caso di invio Fax tramite "mailtofax" modifico la stringa di invio
                this.oParentObject.w_SMTP__TO = ALLTRIM(STRTRAN(this.oParentObject.w_SMTP__TO,";","@fax.zuum.it;"))+"@fax.zuum.it"
              endif
              ALINES(ArrTO, this.oParentObject.w_SMTP__TO, ";")
              ALINES(ArrCC, this.oParentObject.w_SMTP__CC, ";")
              ALINES(ArrCCN, this.oParentObject.w_SMTP_CCN, ";")
              ALINES(ArrATT, this.oParentObject.w_SMTP_ATT, ";")
              this.w_MskMESS = GSUT_KEL()
              this.w_Risultato = vfpMail(ALLTRIM(this.oParentObject.w_SMTPSERV)+":"+ALLTRIM(STR(this.oParentObject.w_SMTPPORT)), i_CODUTE, this.oParentObject.w_SMTPSUBJ, this.oParentObject.w_SMTPPRIO, this.oParentObject.w_SMTPBODY, @ArrTo, @ArrCC, @ArrCCN, @ArrATT, this.w_MskMESS, g_MITTEN, this.w_AUTSRV, g_AccountMail)
              public w_Result_EMAIL 
 w_Result_EMAIL=this.w_Risultato
              if this.w_Risultato=0 OR (TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S")
                this.w_MskMESS.ecpQuit()     
                this.w_MskMESS = .NULL.
              endif
              this.w_RET = this.w_Risultato
            endif
          endif
          * --- Restituisco l'esito dell'invio
          i_retcode = 'stop'
          i_retval = this.w_RET
          return
        endif
      case this.pTipo="TO"
        this.w_MAILADDRESSES = this.oParentObject.w_SMTP__TO
        do case
          case !this.oParentObject.w_IsFax
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_IsFax
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
        this.oParentObject.w_SMTP__TO = this.w_MAILADDRESSES
      case this.pTipo="ALLEG"
        this.w_NOMEFILE = GetFile()
        if Not Empty( this.w_NOMEFILE )
          this.oParentObject.w_SMTP_ATT = iif(!this.oParentObject.w_IsFax,Alltrim(this.oParentObject.w_SMTP_ATT)+IIF( Empty(Alltrim(this.oParentObject.w_SMTP_ATT)),"",";")+this.w_NOMEFILE,this.w_NOMEFILE)
        endif
      case this.pTipo="CC"
        this.w_MAILADDRESSES = this.oParentObject.w_SMTP__CC
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_SMTP__CC = this.w_MAILADDRESSES
      case this.pTipo="CN"
        this.w_MAILADDRESSES = this.oParentObject.w_SMTP_CCN
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.oParentObject.w_SMTP_CCN = this.w_MAILADDRESSES
      case this.pTipo="ZAL"
        this.w_NOMEFILE = GetFile()
        if Not Empty( this.w_NOMEFILE )
          if this.oParentObject.w_IsFax
            UPDATE (this.oParentObject.w_ZOOM.cCursor) SET XCHK=0
            INSERT INTO (this.oParentObject.w_Zoom.cCursor) VALUES ( ALLTRIM(this.w_NOMEFILE) , 1 )
          else
            INSERT INTO (this.oParentObject.w_Zoom.cCursor) VALUES ( ALLTRIM(this.w_NOMEFILE) , 1 )
          endif
          this.oParentObject.w_ZOOM.Refresh()     
        endif
      case this.pTipo="AGA"
        SELECT ALLEGATO FROM ( this.oParentObject.w_Zoom.cCursor ) Where Xchk=1 INTO ARRAY L_ArrAtt
        this.oParentObject.w_SMTP_ATT = ""
        if TYPE("L_ArrAtt")<>"U" 
          this.w_FILEHANDLE = 1
          do while this.w_FILEHANDLE<=ALEN(L_ArrAtt,1)
            this.oParentObject.w_SMTP_ATT = Alltrim(this.oParentObject.w_SMTP_ATT)+ ALLTRIM(L_ArrAtt( this.w_FileHandle)) + ";" 
            this.w_FILEHANDLE = this.w_FILEHANDLE + 1
          enddo
          this.oParentObject.w_SMTP_ATT = LEFT(this.oParentObject.w_SMTP_ATT, LEN( this.oParentObject.w_SMTP_ATT ) -1)
        endif
        This.oParentobject.oParentObject.w_SMTP_ATT = this.oParentObject.w_SMTP_ATT
        this.oParentObject.ecpQuit
      case this.pTipo="SDC"
        SELECT ( this.oParentObject.w_ZOOM.cCursor)
        if USED(this.oParentObject.w_Zoom.cCursor) And RECCOUNT(this.oParentObject.w_Zoom.cCursor)>0
          this.w_NUMREC = RECNO()
          if RIGHT(this.pTipo,1)="C" 
            if this.oParentObject.w_IsFax
              UPDATE (this.oParentObject.w_ZOOM.cCursor) SET XCHK=0
              GO this.w_NUMREC
              REPLACE XCHK with 1
            endif
          else
            UPDATE (this.oParentObject.w_ZOOM.cCursor) SET XCHK=ICASE(RIGHT(this.pTipo,1)="S",1,RIGHT(this.pTipo,1)="D",0,IIF(XCHK=1,0,1))
            GO this.w_NUMREC
          endif
        endif
      case this.pTipo="CDZ"
        ALINES(L_ArrAtt, this.oParentObject.w_SMTP_ATT, ";")
        if ALEN(L_ArrAtt)>0 And NOT EMPTY(L_ArrAtt)
          SELECT ( this.oParentObject.w_ZOOM.cCursor)
          DELETE ALL
          PACK
          this.w_FILEHANDLE = 1
          do while this.w_FILEHANDLE<=ALEN(L_ArrAtt,1)
            INSERT INTO (this.oParentObject.w_ZOOM.cCursor) VALUES ( L_ArrAtt(this.w_FileHandle) , 1 )
            this.w_FILEHANDLE = this.w_FILEHANDLE + 1
          enddo
        endif
      case this.pTipo="DONE"
        i_EMAILSUBJECT=""
      case this.pTipo="BLANK"
        * --- Select from PROMINDI
        i_nConn=i_TableProp[this.PROMINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select IDORIGIN,IDFLPROV  from "+i_cTable+" PROMINDI ";
              +" where IDFLPROV='S'";
               ,"_Curs_PROMINDI")
        else
          select IDORIGIN,IDFLPROV from (i_cTable);
           where IDFLPROV="S";
            into cursor _Curs_PROMINDI
        endif
        if used('_Curs_PROMINDI')
          select _Curs_PROMINDI
          locate for 1=1
          do while not(eof())
          if Upper(Alltrim(this.oParentObject.w_NOMALLEG))=Upper(Alltrim(_Curs_PROMINDI.IDORIGIN))
            this.oParentObject.w_FLEDIT = .F.
            exit
          endif
            select _Curs_PROMINDI
            continue
          enddo
          use
        endif
    endcase
  endproc
  proc Try_043432F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_oOutlook = GETOBJECT(,"Outlook.Application")
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if RecCount(this.oParentObject.w_ZOOM.cCursor)>0
      this.w_POS = IIF( Eof() , RECNO(this.oParentObject.w_ZOOM.cCursor)-1 , RECNO(this.oParentObject.w_ZOOM.cCursor) )
       
 Select ( this.oParentObject.w_ZOOM.cCursor ) 
 Go Top
      do while Not Eof ( this.oParentObject.w_ZOOM.cCursor )
        if Xchk=1
          this.w_MAILADDRESSES = Alltrim(this.w_MAILADDRESSES)+IIF( Empty(Alltrim(this.w_MAILADDRESSES)),"",";")+ Nvl( AN_EMAIL , "" )
          * --- Deseleziono l'indirizzo appena passato
          Replace Xchk with 0
        endif
        if Not Eof( this.oParentObject.w_ZOOM.cCursor )
          Skip
        endif
      enddo
       
 Select ( this.oParentObject.w_ZOOM.cCursor ) 
 Go this.w_POS
      this.oParentObject.w_Zoom.grd.Columns[this.oParentObject.w_Zoom.grd.ColumnCount].chk.Value = 0
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if RecCount(this.oParentObject.w_ZoomFax.cCursor)>0
      this.w_POS = IIF( Eof() , RECNO(this.oParentObject.w_ZoomFax.cCursor)-1 , RECNO(this.oParentObject.w_ZoomFax.cCursor) )
       
 Select ( this.oParentObject.w_ZoomFax.cCursor ) 
 Go Top
      do while Not Eof ( this.oParentObject.w_ZoomFax.cCursor )
        if Xchk=1
          this.w_MAILADDRESSES = Alltrim(this.w_MAILADDRESSES)+IIF( Empty(Alltrim(this.w_MAILADDRESSES)),"",";")+ Nvl( ANTELFAX , "" )
          * --- Deseleziono l'indirizzo appena passato
          Replace Xchk with 0
        endif
        if Not Eof( this.oParentObject.w_ZoomFax.cCursor )
          Skip
        endif
      enddo
       
 Select ( this.oParentObject.w_ZoomFax.cCursor ) 
 Go this.w_POS
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_oItems = .NULL.
    this.w_oFolderSent = .NULL.
    if this.oParentObject.w_UseOutlook
      this.w_oOutlook.Quit()     
    endif
    this.w_oOutlook = .NULL.
    this.w_oNameSpace = .NULL.
    if L_bErr
      this.w_Risultato = -1
      addmsgnl("ATTENZIONE!!! Procedura terminata con errori%0%1", this.w_MskMESS, MESSAGE())
      public w_Result_EMAIL 
 w_Result_EMAIL=this.w_Risultato
    else
      this.w_Risultato = 0
    endif
    if (TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S")
      this.w_MskMESS.ecpQuit()     
      this.w_MskMESS = .NULL.
    endif
     
 ON ERROR &L_OLDERR
    * --- Al termine del esportazion riattiva il decorator dei form 
    DecoratorState ("A")
    i_retcode = 'stop'
    i_retval = this.w_Risultato
    return
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='PROMCLAS'
    this.cWorkTables[3]='PROMINDI'
    this.cWorkTables[4]='ACC_MAIL'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_PROMINDI')
      use in _Curs_PROMINDI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
