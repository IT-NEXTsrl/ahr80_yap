* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bup                                                        *
*              Ult.vendite/acquisti per C/F                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_155]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-24                                                      *
* Last revis.: 2013-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_FLVEAC,w_TIPCON,w_MVCODCON,w_MVCODART,w_DECUNI,w_MVCODVAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bup",oParentObject,m.w_FLVEAC,m.w_TIPCON,m.w_MVCODCON,m.w_MVCODART,m.w_DECUNI,m.w_MVCODVAL)
return(i_retval)

define class tgsar_bup as StdBatch
  * --- Local variables
  w_FLVEAC = space(1)
  w_TIPCON = space(1)
  w_MVCODCON = space(15)
  w_MVCODART = space(20)
  w_DECUNI = 0
  w_MVCODVAL = space(3)
  w_IMPCONSM = 0
  w_IMPCONU1 = 0
  w_PREZZO = 0
  w_PREZZOCON = 0
  w_PREZZOSM = 0
  w_PREZZOU1 = 0
  w_DATDOC = ctod("  /  /  ")
  w_VALDOC = space(3)
  w_CHIAVE = space(10)
  * --- WorkFile variables
  VALUTE_idx=0
  CAM_BI_idx=0
  GSAR_BUP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora la situazione degli Ultimi Prezzi a Cliente o Costi da Fornitore (da GSAR_KUP,GSAR_KUF) o gli ultimi Costi per tutti i Fornitori (da GSAR_KUC)
    * --- Al termine della Elaborazione Popola il Cursore dello Zoom della Maschera chiamante
    * --- Parametri da passare alla query
    * --- --Viene eseguita la query per popolare la tabella temporanea che contiene 
    *     i documenti selezionati per la visualizzazione ultimi costi ultimi prezzi
    if varTYPE(this.w_FLVEAC)<>"C"
      * --- Create temporary table GSAR_BUP
      i_nIdx=cp_AddTableDef('GSAR_BUP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('gsvepkca1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.GSAR_BUP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table GSAR_BUP
      i_nIdx=cp_AddTableDef('GSAR_BUP') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('gsvepkca',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.GSAR_BUP_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Select from GSAR_BUP
    i_nConn=i_TableProp[this.GSAR_BUP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GSAR_BUP_idx,2],.t.,this.GSAR_BUP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MVPREZZOSM,MVPREZZOU1,MVDATREG,MVCODVAL,MVSERIAL,MVPREZZO  from "+i_cTable+" GSAR_BUP ";
          +" where MVCODVAL<>"+cp_ToStrODBC(this.w_MVCODVAL)+"";
           ,"_Curs_GSAR_BUP")
    else
      select MVPREZZOSM,MVPREZZOU1,MVDATREG,MVCODVAL,MVSERIAL,MVPREZZO from (i_cTable);
       where MVCODVAL<>this.w_MVCODVAL;
        into cursor _Curs_GSAR_BUP
    endif
    if used('_Curs_GSAR_BUP')
      select _Curs_GSAR_BUP
      locate for 1=1
      do while not(eof())
      * --- --Occorre controllare se la valuta del documento differisce dalla valuta degli altri documenti
      *     riportati all'interno dell'elenco.
      *     In questo caso occorrera convertire il prezzo nella valuta del documento
      this.w_PREZZOSM = _Curs_GSAR_BUP.MVPREZZOSM
      this.w_PREZZOU1 = _Curs_GSAR_BUP.MVPREZZOU1
      this.w_PREZZO = _Curs_GSAR_BUP.MVPREZZO
      this.w_DATDOC = _Curs_GSAR_BUP.MVDATREG
      this.w_VALDOC = _Curs_GSAR_BUP.MVCODVAL
      * --- --Calcoli di conversione
      *     VALCAM(IMPORTO, VALUTA_PARTENZA, VALUTA_DESTINAZIONE,DATA_DOC,0)
      this.w_PREZZOCON = VALCAM(this.w_PREZZO, this.w_VALDOC, this.w_MVCODVAL, this.w_DATDOC, 0)
      this.w_IMPCONSM = VALCAM(this.w_PREZZOSM, this.w_VALDOC, this.w_MVCODVAL, this.w_DATDOC, 0)
      this.w_IMPCONU1 = VALCAM(this.w_PREZZOU1, this.w_VALDOC, this.w_MVCODVAL, this.w_DATDOC, 0)
      this.w_CHIAVE = _Curs_GSAR_BUP.MVSERIAL
      * --- --Occorre aggiornare la valuta del documento ed il prezzo e il prezzo unitario
      * --- Write into GSAR_BUP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.GSAR_BUP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GSAR_BUP_idx,2])
        i_cOp1=cp_SetTrsOp("=",'MVPREZZOSM','this.w_IMPCONSM',this.w_IMPCONSM,'update',i_nConn)
        i_cOp2=cp_SetTrsOp("=",'MVPREZZOU1','this.w_IMPCONU1',this.w_IMPCONU1,'update',i_nConn)
        i_cOp3=cp_SetTrsOp("=",'MVCODVAL','this.w_MVCODVAL',this.w_MVCODVAL,'update',i_nConn)
        i_cOp4=cp_SetTrsOp("=",'MVPREZZO','this.w_PREZZOCON',this.w_PREZZOCON,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.GSAR_BUP_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVPREZZOSM ="+cp_NullLink(i_cOp1,'GSAR_BUP','MVPREZZOSM');
        +",MVPREZZOU1 ="+cp_NullLink(i_cOp2,'GSAR_BUP','MVPREZZOU1');
        +",MVCODVAL ="+cp_NullLink(i_cOp3,'GSAR_BUP','MVCODVAL');
        +",MVPREZZO ="+cp_NullLink(i_cOp4,'GSAR_BUP','MVPREZZO');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_CHIAVE);
               )
      else
        update (i_cTable) set;
            MVPREZZOSM = &i_cOp1.;
            ,MVPREZZOU1 = &i_cOp2.;
            ,MVCODVAL = &i_cOp3.;
            ,MVPREZZO = &i_cOp4.;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_CHIAVE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_GSAR_BUP
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,w_FLVEAC,w_TIPCON,w_MVCODCON,w_MVCODART,w_DECUNI,w_MVCODVAL)
    this.w_FLVEAC=w_FLVEAC
    this.w_TIPCON=w_TIPCON
    this.w_MVCODCON=w_MVCODCON
    this.w_MVCODART=w_MVCODART
    this.w_DECUNI=w_DECUNI
    this.w_MVCODVAL=w_MVCODVAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CAM_BI'
    this.cWorkTables[3]='*GSAR_BUP'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GSAR_BUP')
      use in _Curs_GSAR_BUP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_FLVEAC,w_TIPCON,w_MVCODCON,w_MVCODART,w_DECUNI,w_MVCODVAL"
endproc
