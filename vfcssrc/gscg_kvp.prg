* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kvp                                                        *
*              Visualizza primanota                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_220]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-26                                                      *
* Last revis.: 2014-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kvp",oParentObject))

* --- Class definition
define class tgscg_kvp as StdForm
  Top    = 5
  Left   = 14

  * --- Standard Properties
  Width  = 751
  Height = 481+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-18"
  HelpContextID=219536023
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=67

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  PNT_MAST_IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  cPrg = "gscg_kvp"
  cComment = "Visualizza primanota"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PARAMETRO = space(1)
  w_DECTOT = 0
  w_NREGINI = 0
  w_CODUTINI = 0
  w_DREGINI = ctod('  /  /  ')
  w_NREGFIN = 0
  w_CODUTFIN = 0
  w_DREGFIN = ctod('  /  /  ')
  w_DOCINI = 0
  w_ALFDOCINI = space(10)
  w_DADOCINI = ctod('  /  /  ')
  w_DOCFIN = 0
  w_ALFDOCFIN = space(10)
  w_DADOCFIN = ctod('  /  /  ')
  w_CODCAU = space(5)
  o_CODCAU = space(5)
  w_DESCAU = space(35)
  w_CODESE = space(4)
  w_FLRIFE = space(1)
  o_FLRIFE = space(1)
  w_CODCLF = space(15)
  w_PNDESSUP = space(45)
  w_CODAZI = space(5)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_NUPRINI = 0
  w_ALFPRINI = space(10)
  w_NUPRFIN = 0
  w_ALFPRFIN = space(10)
  w_FLPROV = space(1)
  w_CODVAL = space(3)
  o_CODVAL = space(3)
  w_VADESCRI = space(35)
  w_TOTDOCINI = 0
  w_TOTDOCFIN = 0
  w_NOTE = space(50)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_DESCRI = space(40)
  w_CODVAL1 = space(3)
  w_TIPCON1 = space(1)
  w_INIEUR = 0
  w_FINEUR = 0
  w_INILIR = 0
  w_FINLIR = 0
  w_CODEUR = space(3)
  w_CODLIR = space(3)
  w_ALFA = space(10)
  w_NUMERO = space(15)
  w_DATA = ctod('  /  /  ')
  w_SERIALP = space(10)
  w_RIFDOC = space(10)
  w_RIFINC = space(10)
  w_RIFDIS = space(10)
  w_DECTOP = 0
  w_CALCPICP = 0
  w_IDAVINI = 0
  w_IDAVFIN = 0
  w_RIFCES = space(1)
  w_DESCLF = space(60)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_CODVAL2 = space(3)
  w_FLRIFE2 = space(1)
  w_TOTDAR = 0
  w_TOTAVE = 0
  w_SALDOD = 0
  w_SALDOA = 0
  w_ZoomPNot = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kvpPag1","gscg_kvp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgscg_kvpPag2","gscg_kvp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNREGINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomPNot = this.oPgFrm.Pages(1).oPag.ZoomPNot
    DoDefault()
    proc Destroy()
      this.w_ZoomPNot = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='PNT_MAST'
    this.cWorkTables[4]='CAU_CONT'
    this.cWorkTables[5]='CONTI'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARAMETRO=space(1)
      .w_DECTOT=0
      .w_NREGINI=0
      .w_CODUTINI=0
      .w_DREGINI=ctod("  /  /  ")
      .w_NREGFIN=0
      .w_CODUTFIN=0
      .w_DREGFIN=ctod("  /  /  ")
      .w_DOCINI=0
      .w_ALFDOCINI=space(10)
      .w_DADOCINI=ctod("  /  /  ")
      .w_DOCFIN=0
      .w_ALFDOCFIN=space(10)
      .w_DADOCFIN=ctod("  /  /  ")
      .w_CODCAU=space(5)
      .w_DESCAU=space(35)
      .w_CODESE=space(4)
      .w_FLRIFE=space(1)
      .w_CODCLF=space(15)
      .w_PNDESSUP=space(45)
      .w_CODAZI=space(5)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_NUPRINI=0
      .w_ALFPRINI=space(10)
      .w_NUPRFIN=0
      .w_ALFPRFIN=space(10)
      .w_FLPROV=space(1)
      .w_CODVAL=space(3)
      .w_VADESCRI=space(35)
      .w_TOTDOCINI=0
      .w_TOTDOCFIN=0
      .w_NOTE=space(50)
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_DESCRI=space(40)
      .w_CODVAL1=space(3)
      .w_TIPCON1=space(1)
      .w_INIEUR=0
      .w_FINEUR=0
      .w_INILIR=0
      .w_FINLIR=0
      .w_CODEUR=space(3)
      .w_CODLIR=space(3)
      .w_ALFA=space(10)
      .w_NUMERO=space(15)
      .w_DATA=ctod("  /  /  ")
      .w_SERIALP=space(10)
      .w_RIFDOC=space(10)
      .w_RIFINC=space(10)
      .w_RIFDIS=space(10)
      .w_DECTOP=0
      .w_CALCPICP=0
      .w_IDAVINI=0
      .w_IDAVFIN=0
      .w_RIFCES=space(1)
      .w_DESCLF=space(60)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_CODVAL2=space(3)
      .w_FLRIFE2=space(1)
      .w_TOTDAR=0
      .w_TOTAVE=0
      .w_SALDOD=0
      .w_SALDOA=0
        .w_PARAMETRO = IIF(VARTYPE(this.oParentObject)='C', this.oParentObject, ' ' )
        .w_DECTOT = g_PERPVL
        .DoRTCalc(3,15,.f.)
        if not(empty(.w_CODCAU))
          .link_1_15('Full')
        endif
          .DoRTCalc(16,16,.f.)
        .w_CODESE = g_CODESE
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODESE))
          .link_1_17('Full')
        endif
        .w_FLRIFE = IIF(EMPTY(.w_CODCAU), 'N', .w_FLRIFE2)
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODCLF))
          .link_1_19('Full')
        endif
        .w_PNDESSUP = .w_ZoomPNot.getVar('PNDESSUP')
        .w_CODAZI = i_CODAZI
      .oPgFrm.Page1.oPag.ZoomPNot.Calculate()
          .DoRTCalc(22,23,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(25,29,.f.)
        .w_FLPROV = 'N'
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_CODVAL))
          .link_2_13('Full')
        endif
          .DoRTCalc(32,35,.f.)
        .w_TIPCON = 'N'
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_CODCON))
          .link_2_19('Full')
        endif
          .DoRTCalc(38,38,.f.)
        .w_CODVAL1 = ALLTR(g_PERVAL)
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_CODVAL1))
          .link_2_21('Full')
        endif
        .w_TIPCON1 = IIF(.w_TIPCON='N',' ',.w_TIPCON)
        .w_INIEUR = VAL2MON(.w_IDAVINI,GETCAM(.w_CODVAL1, I_DATSYS), 1, I_DATSYS, .w_CODVAL1,GETVALUT(.w_CODVAL1, 'VADECTOT'))
        .w_FINEUR = VAL2MON(.w_IDAVFIN,GETCAM(.w_CODVAL1, I_DATSYS), 1, I_DATSYS, .w_CODVAL1,GETVALUT(.w_CODVAL1, 'VADECTOT'))
        .w_INILIR = VAL2MON(.w_IDAVINI,GETCAM(.w_CODVAL1, I_DATSYS), 1, I_DATSYS, .w_CODVAL1,GETVALUT(.w_CODVAL1, 'VADECTOT'))
        .w_FINLIR = VAL2MON(.w_IDAVFIN,GETCAM(.w_CODVAL1, I_DATSYS), 1, I_DATSYS, .w_CODVAL1,GETVALUT(.w_CODVAL1, 'VADECTOT'))
          .DoRTCalc(45,46,.f.)
        .w_ALFA = .w_ZoomPNot.getVar('PNALFDOC')
        .w_NUMERO = .w_ZoomPNot.getVar('PNNUMDOC')
        .w_DATA = .w_ZoomPNot.getVar('PNDATDOC')
        .w_SERIALP = .w_ZoomPNot.getVar('PNSERIAL')
        .w_RIFDOC = .w_ZoomPNot.getVar('PNRIFDOC')
        .w_RIFINC = .w_ZoomPNot.getVar('PNRIFINC')
        .w_RIFDIS = .w_ZoomPNot.getVar('PNRIFDIS')
          .DoRTCalc(54,54,.f.)
        .w_CALCPICP = DEFPIP(.w_DECTOP)
          .DoRTCalc(56,57,.f.)
        .w_RIFCES = .w_ZoomPNot.getVar('PNRIFCES')
      .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
          .DoRTCalc(59,60,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CODVAL2 = .w_CODVAL
      .oPgFrm.Page2.oPag.oObj_2_43.Calculate()
          .DoRTCalc(63,65,.f.)
        .w_SALDOD = .w_TOTDAR-.w_TOTAVE
        .w_SALDOA = ABS(.w_TOTDAR-.w_TOTAVE)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_22.enabled = this.oPgFrm.Page2.oPag.oBtn_2_22.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_42.enabled = this.oPgFrm.Page2.oPag.oBtn_2_42.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_DECTOT = g_PERPVL
        .DoRTCalc(3,17,.t.)
        if .o_CODCAU<>.w_CODCAU
            .w_FLRIFE = IIF(EMPTY(.w_CODCAU), 'N', .w_FLRIFE2)
        endif
        .DoRTCalc(19,19,.t.)
            .w_PNDESSUP = .w_ZoomPNot.getVar('PNDESSUP')
        .oPgFrm.Page1.oPag.ZoomPNot.Calculate()
        .DoRTCalc(21,39,.t.)
            .w_TIPCON1 = IIF(.w_TIPCON='N',' ',.w_TIPCON)
            .w_INIEUR = VAL2MON(.w_IDAVINI,GETCAM(.w_CODVAL1, I_DATSYS), 1, I_DATSYS, .w_CODVAL1,GETVALUT(.w_CODVAL1, 'VADECTOT'))
            .w_FINEUR = VAL2MON(.w_IDAVFIN,GETCAM(.w_CODVAL1, I_DATSYS), 1, I_DATSYS, .w_CODVAL1,GETVALUT(.w_CODVAL1, 'VADECTOT'))
            .w_INILIR = VAL2MON(.w_IDAVINI,GETCAM(.w_CODVAL1, I_DATSYS), 1, I_DATSYS, .w_CODVAL1,GETVALUT(.w_CODVAL1, 'VADECTOT'))
            .w_FINLIR = VAL2MON(.w_IDAVFIN,GETCAM(.w_CODVAL1, I_DATSYS), 1, I_DATSYS, .w_CODVAL1,GETVALUT(.w_CODVAL1, 'VADECTOT'))
        .DoRTCalc(45,46,.t.)
            .w_ALFA = .w_ZoomPNot.getVar('PNALFDOC')
            .w_NUMERO = .w_ZoomPNot.getVar('PNNUMDOC')
            .w_DATA = .w_ZoomPNot.getVar('PNDATDOC')
            .w_SERIALP = .w_ZoomPNot.getVar('PNSERIAL')
            .w_RIFDOC = .w_ZoomPNot.getVar('PNRIFDOC')
            .w_RIFINC = .w_ZoomPNot.getVar('PNRIFINC')
            .w_RIFDIS = .w_ZoomPNot.getVar('PNRIFDIS')
        .DoRTCalc(54,54,.t.)
            .w_CALCPICP = DEFPIP(.w_DECTOP)
        .DoRTCalc(56,57,.t.)
            .w_RIFCES = .w_ZoomPNot.getVar('PNRIFCES')
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        if .o_CODCAU<>.w_CODCAU.or. .o_FLRIFE<>.w_FLRIFE
          .Calculate_CPSBMXIRII()
        endif
        if .o_CODVAL<>.w_CODVAL
          .Calculate_ZKWRVVNJLA()
        endif
        .DoRTCalc(59,60,.t.)
        if .o_CODVAL<>.w_CODVAL
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_CODVAL<>.w_CODVAL
            .w_CODVAL2 = .w_CODVAL
        endif
        .oPgFrm.Page2.oPag.oObj_2_43.Calculate()
        .DoRTCalc(63,65,.t.)
            .w_SALDOD = .w_TOTDAR-.w_TOTAVE
            .w_SALDOA = ABS(.w_TOTDAR-.w_TOTAVE)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomPNot.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_57.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_43.Calculate()
    endwith
  return

  proc Calculate_CPSBMXIRII()
    with this
          * --- Azzera cli/for
          .w_CODCLF = SPACE(15)
          .link_1_19('Full')
    endwith
  endproc
  proc Calculate_ZKWRVVNJLA()
    with this
          * --- Azzera totali documento
          .w_TOTDOCINI = 0
          .w_TOTDOCFIN = 0
    endwith
  endproc
  proc Calculate_UXZYYWFHYA()
    with this
          * --- Modifica zoom
          GSCG_BVP(this;
              ,'Z';
             )
    endwith
  endproc
  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .cComment = IIF(.w_PARAMETRO='P', AH_MSGFORMAT("Visualizza primanota"), AH_MSGFORMAT("Visualizza movimenti per intestatario"))
          .Caption = .cComment
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oTOTDOCINI_2_15.enabled = this.oPgFrm.Page2.oPag.oTOTDOCINI_2_15.mCond()
    this.oPgFrm.Page2.oPag.oTOTDOCFIN_2_16.enabled = this.oPgFrm.Page2.oPag.oTOTDOCFIN_2_16.mCond()
    this.oPgFrm.Page2.oPag.oCODCON_2_19.enabled = this.oPgFrm.Page2.oPag.oCODCON_2_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODCLF_1_19.visible=!this.oPgFrm.Page1.oPag.oCODCLF_1_19.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDESCLF_1_59.visible=!this.oPgFrm.Page1.oPag.oDESCLF_1_59.mHide()
    this.oPgFrm.Page1.oPag.oTOTDAR_1_64.visible=!this.oPgFrm.Page1.oPag.oTOTDAR_1_64.mHide()
    this.oPgFrm.Page1.oPag.oTOTAVE_1_65.visible=!this.oPgFrm.Page1.oPag.oTOTAVE_1_65.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_66.visible=!this.oPgFrm.Page1.oPag.oStr_1_66.mHide()
    this.oPgFrm.Page1.oPag.oSALDOD_1_67.visible=!this.oPgFrm.Page1.oPag.oSALDOD_1_67.mHide()
    this.oPgFrm.Page1.oPag.oSALDOA_1_68.visible=!this.oPgFrm.Page1.oPag.oSALDOA_1_68.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_69.visible=!this.oPgFrm.Page1.oPag.oStr_1_69.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomPNot.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_57.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_43.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_UXZYYWFHYA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gscg_kvp
    If Cevent='Aggiorna'
       * Se eseguo ricerca da selezioni aggiuntive mi sposto di pagina
       This.oPgFrm.ActivePage = 1
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCAU
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRIFE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CODCAU))
          select CCCODICE,CCDESCRI,CCFLRIFE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCODCAU_1_15'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRIFE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLRIFE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRIFE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAU)
            select CCCODICE,CCDESCRI,CCFLRIFE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLRIFE2 = NVL(_Link_.CCFLRIFE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_FLRIFE2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_17'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLF
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLRIFE);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_FLRIFE;
                     ,'ANCODICE',trim(this.w_CODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLRIFE);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_FLRIFE);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDTOBSO like "+cp_ToStrODBC(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLRIFE);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDTOBSO like "+cp_ToStr(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_FLRIFE);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLF_1_19'),i_cWhere,'GSAR_BZC',"Anagrafica clienti/fornitori",'GSCG1MPN.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLRIFE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_FLRIFE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLRIFE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_FLRIFE;
                       ,'ANCODICE',this.w_CODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(60))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLF = space(15)
      endif
      this.w_DESCLF = space(60)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_CODCLF = space(15)
        this.w_DESCLF = space(60)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL))
          select VACODVAL,VADESVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL_2_13'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADESVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_VADESCRI = NVL(_Link_.VADESVAL,space(35))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_VADESCRI = space(35)
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_2_19'),i_cWhere,'GSAR_BZC',"Clienti/fornitori/conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_DESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL1
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL1)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL1))
          select VACODVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL1)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL1) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL1_2_21'),i_cWhere,'GSAR_AVL',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL1)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL1 = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOP = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL1 = space(3)
      endif
      this.w_DECTOP = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNREGINI_1_3.value==this.w_NREGINI)
      this.oPgFrm.Page1.oPag.oNREGINI_1_3.value=this.w_NREGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTINI_1_4.value==this.w_CODUTINI)
      this.oPgFrm.Page1.oPag.oCODUTINI_1_4.value=this.w_CODUTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDREGINI_1_5.value==this.w_DREGINI)
      this.oPgFrm.Page1.oPag.oDREGINI_1_5.value=this.w_DREGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNREGFIN_1_6.value==this.w_NREGFIN)
      this.oPgFrm.Page1.oPag.oNREGFIN_1_6.value=this.w_NREGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTFIN_1_7.value==this.w_CODUTFIN)
      this.oPgFrm.Page1.oPag.oCODUTFIN_1_7.value=this.w_CODUTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDREGFIN_1_8.value==this.w_DREGFIN)
      this.oPgFrm.Page1.oPag.oDREGFIN_1_8.value=this.w_DREGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_9.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_9.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOCINI_1_10.value==this.w_ALFDOCINI)
      this.oPgFrm.Page1.oPag.oALFDOCINI_1_10.value=this.w_ALFDOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDADOCINI_1_11.value==this.w_DADOCINI)
      this.oPgFrm.Page1.oPag.oDADOCINI_1_11.value=this.w_DADOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_12.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_12.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDOCFIN_1_13.value==this.w_ALFDOCFIN)
      this.oPgFrm.Page1.oPag.oALFDOCFIN_1_13.value=this.w_ALFDOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDADOCFIN_1_14.value==this.w_DADOCFIN)
      this.oPgFrm.Page1.oPag.oDADOCFIN_1_14.value=this.w_DADOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_15.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_15.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_16.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_16.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_17.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_17.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oFLRIFE_1_18.RadioValue()==this.w_FLRIFE)
      this.oPgFrm.Page1.oPag.oFLRIFE_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLF_1_19.value==this.w_CODCLF)
      this.oPgFrm.Page1.oPag.oCODCLF_1_19.value=this.w_CODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oPNDESSUP_1_21.value==this.w_PNDESSUP)
      this.oPgFrm.Page1.oPag.oPNDESSUP_1_21.value=this.w_PNDESSUP
    endif
    if not(this.oPgFrm.Page2.oPag.oNUPRINI_2_4.value==this.w_NUPRINI)
      this.oPgFrm.Page2.oPag.oNUPRINI_2_4.value=this.w_NUPRINI
    endif
    if not(this.oPgFrm.Page2.oPag.oALFPRINI_2_5.value==this.w_ALFPRINI)
      this.oPgFrm.Page2.oPag.oALFPRINI_2_5.value=this.w_ALFPRINI
    endif
    if not(this.oPgFrm.Page2.oPag.oNUPRFIN_2_6.value==this.w_NUPRFIN)
      this.oPgFrm.Page2.oPag.oNUPRFIN_2_6.value=this.w_NUPRFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oALFPRFIN_2_7.value==this.w_ALFPRFIN)
      this.oPgFrm.Page2.oPag.oALFPRFIN_2_7.value=this.w_ALFPRFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oFLPROV_2_12.RadioValue()==this.w_FLPROV)
      this.oPgFrm.Page2.oPag.oFLPROV_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODVAL_2_13.value==this.w_CODVAL)
      this.oPgFrm.Page2.oPag.oCODVAL_2_13.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oVADESCRI_2_14.value==this.w_VADESCRI)
      this.oPgFrm.Page2.oPag.oVADESCRI_2_14.value=this.w_VADESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTDOCINI_2_15.value==this.w_TOTDOCINI)
      this.oPgFrm.Page2.oPag.oTOTDOCINI_2_15.value=this.w_TOTDOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oTOTDOCFIN_2_16.value==this.w_TOTDOCFIN)
      this.oPgFrm.Page2.oPag.oTOTDOCFIN_2_16.value=this.w_TOTDOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTE_2_17.value==this.w_NOTE)
      this.oPgFrm.Page2.oPag.oNOTE_2_17.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPCON_2_18.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page2.oPag.oTIPCON_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON_2_19.value==this.w_CODCON)
      this.oPgFrm.Page2.oPag.oCODCON_2_19.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCRI_2_20.value==this.w_DESCRI)
      this.oPgFrm.Page2.oPag.oDESCRI_2_20.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODVAL1_2_21.RadioValue()==this.w_CODVAL1)
      this.oPgFrm.Page2.oPag.oCODVAL1_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIDAVINI_2_30.value==this.w_IDAVINI)
      this.oPgFrm.Page2.oPag.oIDAVINI_2_30.value=this.w_IDAVINI
    endif
    if not(this.oPgFrm.Page2.oPag.oIDAVFIN_2_31.value==this.w_IDAVFIN)
      this.oPgFrm.Page2.oPag.oIDAVFIN_2_31.value=this.w_IDAVFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_59.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_59.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDAR_1_64.value==this.w_TOTDAR)
      this.oPgFrm.Page1.oPag.oTOTDAR_1_64.value=this.w_TOTDAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTAVE_1_65.value==this.w_TOTAVE)
      this.oPgFrm.Page1.oPag.oTOTAVE_1_65.value=this.w_TOTAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDOD_1_67.value==this.w_SALDOD)
      this.oPgFrm.Page1.oPag.oSALDOD_1_67.value=this.w_SALDOD
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDOA_1_68.value==this.w_SALDOA)
      this.oPgFrm.Page1.oPag.oSALDOA_1_68.value=this.w_SALDOA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(NOT .w_FLRIFE $ "CF")  and not(empty(.w_CODCLF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCLF_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_TIPCON$ 'CFG')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODCAU = this.w_CODCAU
    this.o_FLRIFE = this.w_FLRIFE
    this.o_CODVAL = this.w_CODVAL
    return

enddefine

* --- Define pages as container
define class tgscg_kvpPag1 as StdContainer
  Width  = 747
  height = 483
  stdWidth  = 747
  stdheight = 483
  resizeXpos=553
  resizeYpos=268
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNREGINI_1_3 as StdField with uid="FQSYFHTUWP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NREGINI", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione iniziale",;
    HelpContextID = 267485654,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=90, Top=8

  add object oCODUTINI_1_4 as StdField with uid="OQVCQVZTTC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODUTINI", cQueryName = "CODUTINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente iniziale",;
    HelpContextID = 72389009,;
   bGlobalFont=.t.,;
    Height=21, Width=28, Left=154, Top=8

  add object oDREGINI_1_5 as StdField with uid="NQNJUWAZII",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DREGINI", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione iniziale",;
    HelpContextID = 267485494,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=220, Top=8

  add object oNREGFIN_1_6 as StdField with uid="LDKQYKHTSH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NREGFIN", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione finale",;
    HelpContextID = 87981610,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=90, Top=33

  add object oCODUTFIN_1_7 as StdField with uid="TLJLRZVVGF",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODUTFIN", cQueryName = "CODUTFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente finale",;
    HelpContextID = 145714804,;
   bGlobalFont=.t.,;
    Height=21, Width=28, Left=154, Top=33

  add object oDREGFIN_1_8 as StdField with uid="IIJOYWFVAO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DREGFIN", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione finale",;
    HelpContextID = 87981770,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=220, Top=33

  add object oDOCINI_1_9 as StdField with uid="ZBNZTFUKTZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Num. documento iniziale",;
    HelpContextID = 79471050,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=377, Top=8, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oALFDOCINI_1_10 as StdField with uid="IQDHLHPBVF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ALFDOCINI", cQueryName = "ALFDOCINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento iniziale",;
    HelpContextID = 89034724,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=521, Top=8, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDADOCINI_1_11 as StdField with uid="KTUFDNNKXZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DADOCINI", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data emissione del documento iniziale",;
    HelpContextID = 90611585,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=660, Top=8

  add object oDOCFIN_1_12 as StdField with uid="GDBIXIPPHU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Num.documento finale",;
    HelpContextID = 1024458,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=377, Top=33, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oALFDOCFIN_1_13 as StdField with uid="HFHATSWFWM",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ALFDOCFIN", cQueryName = "ALFDOCFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento finale",;
    HelpContextID = 89034799,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=521, Top=33, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDADOCFIN_1_14 as StdField with uid="VNSTMBLDUE",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DADOCFIN", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data emissione del documento finale",;
    HelpContextID = 127492228,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=660, Top=33

  add object oCODCAU_1_15 as StdField with uid="PFSAHQALUO",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della causale contabile",;
    HelpContextID = 160600538,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=90, Top=59, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAU"

  func oCODCAU_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAU_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAU_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCODCAU_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oCODCAU_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CODCAU
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_16 as StdField with uid="DXUTUUQNDQ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della causale contabile",;
    HelpContextID = 160541642,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=154, Top=59, InputMask=replicate('X',35)

  add object oCODESE_1_17 as StdField with uid="ZMIJKNZDWM",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 141595098,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=521, Top=59, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc


  add object oFLRIFE_1_18 as StdCombo with uid="ZKYBZYDVIK",rtseq=18,rtrep=.f.,left=90,top=84,width=73,height=21;
    , ToolTipText = "Tipo intestatario";
    , HelpContextID = 154907818;
    , cFormVar="w_FLRIFE",RowSource=""+"Cliente,"+"Fornitore,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLRIFE_1_18.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oFLRIFE_1_18.GetRadio()
    this.Parent.oContained.w_FLRIFE = this.RadioValue()
    return .t.
  endfunc

  func oFLRIFE_1_18.SetRadio()
    this.Parent.oContained.w_FLRIFE=trim(this.Parent.oContained.w_FLRIFE)
    this.value = ;
      iif(this.Parent.oContained.w_FLRIFE=='C',1,;
      iif(this.Parent.oContained.w_FLRIFE=='F',2,;
      iif(this.Parent.oContained.w_FLRIFE=='N',3,;
      0)))
  endfunc

  func oFLRIFE_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCLF)
        bRes2=.link_1_19('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCLF_1_19 as StdField with uid="UWOGIJXOGJ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODCLF", cQueryName = "CODCLF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    ToolTipText = "Cliente/fornitore intestatario del documento",;
    HelpContextID = 132288986,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=169, Top=84, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_FLRIFE", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLF"

  func oCODCLF_1_19.mHide()
    with this.Parent.oContained
      return (NOT .w_FLRIFE $ "CF")
    endwith
  endfunc

  func oCODCLF_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLF_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLF_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_FLRIFE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_FLRIFE)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLF_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti/fornitori",'GSCG1MPN.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCLF_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_FLRIFE
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCLF
     i_obj.ecpSave()
  endproc


  add object oBtn_1_20 as StdButton with uid="ISKAWSSZTY",left=696, top=61, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 140412650;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSCG_BVP(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPNDESSUP_1_21 as StdField with uid="XGUPNRKBHI",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PNDESSUP", cQueryName = "PNDESSUP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva",;
    HelpContextID = 93285958,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=133, Top=383, InputMask=replicate('X',45)


  add object oBtn_1_22 as StdButton with uid="OUEOXFACNW",left=454, top=438, width=48,height=45,;
    CpPicture="BMP\teso.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza la registrazione di prima nota";
    , HelpContextID = 241571718;
    , Caption='\<Primanota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_SERIALP)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_COGE='S' AND NOT EMPTY(NVL(.w_SERIALP,' ')))
      endwith
    endif
  endfunc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COGE<>'S')
     endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="XFXJCJDEUZ",left=504, top=438, width=48,height=45,;
    CpPicture="bmp\origine.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento di origine";
    , HelpContextID = 34857446;
    , Caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        do GSAR_BZO with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_MAGA='S')
      endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="CYFCFNPBIO",left=554, top=438, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Stampa le registrazioni che appaiono nell'elenco";
    , HelpContextID = 175545306;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSCG_BVP(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_SERIALP))
      endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="ZVARLVJFTI",left=696, top=438, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 226853446;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomPNot as cp_zoombox with uid="UDDJAIFUNC",left=-2, top=107, width=753,height=274,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PNT_MAST",cZoomFile="GSCG_KVP",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bAdvOptions=.t.,cMenuFile="",cZoomOnZoom="GSZM_BCC",bRetriveAllRows=.f.,;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 139337242


  add object oObj_1_57 as cp_runprogram with uid="KHMWZGXAFG",left=-6, top=498, width=268,height=20,;
    caption='GSAR_BZP(w_SERIALP)',;
   bGlobalFont=.t.,;
    prg="GSAR_BZP(w_SERIALP)",;
    cEvent = "w_zoompnot selected",;
    nPag=1;
    , HelpContextID = 201888055

  add object oDESCLF_1_59 as StdField with uid="TGOTDIQNVZ",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 132230090,;
   bGlobalFont=.t.,;
    Height=21, Width=377, Left=306, Top=84, InputMask=replicate('X',60)

  func oDESCLF_1_59.mHide()
    with this.Parent.oContained
      return (NOT .w_FLRIFE $ "CF")
    endwith
  endfunc

  add object oTOTDAR_1_64 as StdField with uid="SLBJBHJGPQ",rtseq=64,rtrep=.f.,;
    cFormVar = "w_TOTDAR", cQueryName = "TOTDAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale dare",;
    HelpContextID = 210800842,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=454, Top=383, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oTOTDAR_1_64.mHide()
    with this.Parent.oContained
      return (.w_PARAMETRO='P')
    endwith
  endfunc

  add object oTOTAVE_1_65 as StdField with uid="ATRRCDXXLF",rtseq=65,rtrep=.f.,;
    cFormVar = "w_TOTAVE", cQueryName = "TOTAVE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale avere",;
    HelpContextID = 138645706,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=585, Top=383, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oTOTAVE_1_65.mHide()
    with this.Parent.oContained
      return (.w_PARAMETRO='P')
    endwith
  endfunc

  add object oSALDOD_1_67 as StdField with uid="AAOJRHPSYS",rtseq=66,rtrep=.f.,;
    cFormVar = "w_SALDOD", cQueryName = "SALDOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo movimenti visualizzati in dare",;
    HelpContextID = 162602714,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=454, Top=407, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oSALDOD_1_67.mHide()
    with this.Parent.oContained
      return (.w_TOTDAR-.w_TOTAVE<0 OR .w_PARAMETRO='P')
    endwith
  endfunc

  add object oSALDOA_1_68 as StdField with uid="UOLAWPLNYX",rtseq=67,rtrep=.f.,;
    cFormVar = "w_SALDOA", cQueryName = "SALDOA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo movimenti visualizzati in avere",;
    HelpContextID = 212934362,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=585, Top=407, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oSALDOA_1_68.mHide()
    with this.Parent.oContained
      return (.w_TOTDAR-.w_TOTAVE>0 OR .w_PARAMETRO='P')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="WMPAYGEVJZ",Visible=.t., Left=450, Top=63,;
    Alignment=1, Width=67, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="LAXTICQNIN",Visible=.t., Left=4, Top=384,;
    Alignment=1, Width=126, Height=18,;
    Caption="Desc.supplementare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="KTYIROCBER",Visible=.t., Left=5, Top=8,;
    Alignment=1, Width=81, Height=18,;
    Caption="Da reg. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="HKYRVJNMCC",Visible=.t., Left=18, Top=37,;
    Alignment=1, Width=68, Height=18,;
    Caption="A reg. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="AFLCJPWLZT",Visible=.t., Left=142, Top=8,;
    Alignment=2, Width=18, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="OJYAHTVKFG",Visible=.t., Left=142, Top=37,;
    Alignment=2, Width=18, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="VAQQTVQCJN",Visible=.t., Left=190, Top=8,;
    Alignment=1, Width=26, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="MSYFITATCU",Visible=.t., Left=190, Top=37,;
    Alignment=1, Width=26, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="DKPVJOYCGC",Visible=.t., Left=294, Top=8,;
    Alignment=1, Width=80, Height=18,;
    Caption="Da doc. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="XCVVRPDOFL",Visible=.t., Left=305, Top=37,;
    Alignment=1, Width=69, Height=18,;
    Caption="A doc. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="DLGWUZZOTK",Visible=.t., Left=612, Top=8,;
    Alignment=1, Width=44, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="JGWMBFEVVL",Visible=.t., Left=612, Top=37,;
    Alignment=1, Width=44, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="YAAATJHSEK",Visible=.t., Left=510, Top=11,;
    Alignment=2, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="BJWEUSWIMR",Visible=.t., Left=510, Top=37,;
    Alignment=2, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="HWOKDBRQFA",Visible=.t., Left=10, Top=63,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="UQRJZMKXIX",Visible=.t., Left=22, Top=85,;
    Alignment=1, Width=64, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="SIXLQFSAWO",Visible=.t., Left=412, Top=384,;
    Alignment=1, Width=40, Height=18,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.

  func oStr_1_66.mHide()
    with this.Parent.oContained
      return (.w_PARAMETRO='P')
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="KDIHYMVUFU",Visible=.t., Left=348, Top=408,;
    Alignment=1, Width=104, Height=18,;
    Caption="Saldo movimenti:"  ;
  , bGlobalFont=.t.

  func oStr_1_69.mHide()
    with this.Parent.oContained
      return (.w_PARAMETRO='P')
    endwith
  endfunc
enddefine
define class tgscg_kvpPag2 as StdContainer
  Width  = 747
  height = 483
  stdWidth  = 747
  stdheight = 483
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUPRINI_2_4 as StdField with uid="KMMWLTGCPZ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_NUPRINI", cQueryName = "",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Num.protocollo iniziale",;
    HelpContextID = 268252374,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=161, Top=22, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oALFPRINI_2_5 as StdField with uid="YUBBOUQDCF",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ALFPRINI", cQueryName = "ALFPRINI",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie protocollo iniziale",;
    HelpContextID = 74806449,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=294, Top=22, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oNUPRFIN_2_6 as StdField with uid="FQKLDKBVTK",rtseq=28,rtrep=.f.,;
    cFormVar = "w_NUPRFIN", cQueryName = "",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Num.protocollo finale",;
    HelpContextID = 87214890,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=161, Top=47, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oALFPRFIN_2_7 as StdField with uid="OLIUERLLEQ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ALFPRFIN", cQueryName = "ALFPRFIN",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie protocollo finale",;
    HelpContextID = 143297364,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=294, Top=47, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)


  add object oFLPROV_2_12 as StdCombo with uid="VZJIVUBLRK",value=3,rtseq=30,rtrep=.f.,left=161,top=75,width=104,height=22;
    , ToolTipText = "Stato dei movimenti da visualizzare";
    , HelpContextID = 128111786;
    , cFormVar="w_FLPROV",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFLPROV_2_12.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    ''))))
  endfunc
  func oFLPROV_2_12.GetRadio()
    this.Parent.oContained.w_FLPROV = this.RadioValue()
    return .t.
  endfunc

  func oFLPROV_2_12.SetRadio()
    this.Parent.oContained.w_FLPROV=trim(this.Parent.oContained.w_FLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_FLPROV=='N',1,;
      iif(this.Parent.oContained.w_FLPROV=='S',2,;
      iif(this.Parent.oContained.w_FLPROV=='',3,;
      0)))
  endfunc

  add object oCODVAL_2_13 as StdField with uid="TMMMDEZAEX",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta della registrazione",;
    HelpContextID = 41914842,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=161, Top=108, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAL_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCODVAL_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oCODVAL_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CODVAL
     i_obj.ecpSave()
  endproc

  add object oVADESCRI_2_14 as StdField with uid="GFEMTIBHLK",rtseq=32,rtrep=.f.,;
    cFormVar = "w_VADESCRI", cQueryName = "VADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione valuta",;
    HelpContextID = 175152737,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=217, Top=108, InputMask=replicate('X',35)

  add object oTOTDOCINI_2_15 as StdField with uid="XFCJISSKEK",rtseq=33,rtrep=.f.,;
    cFormVar = "w_TOTDOCINI", cQueryName = "TOTDOCINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo totale documento iniziale (in valuta)",;
    HelpContextID = 89093140,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=161, Top=137, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oTOTDOCINI_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not EMPTY(.w_CODVAL))
    endwith
   endif
  endfunc

  add object oTOTDOCFIN_2_16 as StdField with uid="JRJDDFQFHS",rtseq=34,rtrep=.f.,;
    cFormVar = "w_TOTDOCFIN", cQueryName = "TOTDOCFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo totale documento finale (in valuta)",;
    HelpContextID = 89093215,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=336, Top=137, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oTOTDOCFIN_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not EMPTY(.w_CODVAL))
    endwith
   endif
  endfunc

  add object oNOTE_2_17 as StdField with uid="FDTVSFWNAH",rtseq=35,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento contenuto nella descrizione supplementare",;
    HelpContextID = 224423638,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=161, Top=166, InputMask=replicate('X',50)


  add object oTIPCON_2_18 as StdCombo with uid="TKQYZYBPVV",rtseq=36,rtrep=.f.,left=161,top=194,width=97,height=21;
    , ToolTipText = "Tipo di conto selezionato";
    , HelpContextID = 263313098;
    , cFormVar="w_TIPCON",RowSource=""+"Cliente,"+"Fornitore,"+"Conto,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPCON_2_18.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    iif(this.value =4,'N',;
    ' ')))))
  endfunc
  func oTIPCON_2_18.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_2_18.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      iif(this.Parent.oContained.w_TIPCON=='G',3,;
      iif(this.Parent.oContained.w_TIPCON=='N',4,;
      0))))
  endfunc

  func oTIPCON_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_2_19('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCON_2_19 as StdField with uid="ZZFIMKSUCD",rtseq=37,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Conto selezionato",;
    HelpContextID = 263360986,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=265, Top=194, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCON$ 'CFG')
    endwith
   endif
  endfunc

  func oCODCON_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori/conti",'',this.parent.oContained
  endproc
  proc oCODCON_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oDESCRI_2_20 as StdField with uid="YISYJVIDXY",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del conto selezionato",;
    HelpContextID = 75606986,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=411, Top=194, InputMask=replicate('X',40)


  add object oCODVAL1_2_21 as StdCombo with uid="WSOQQPSMYI",value=3,rtseq=39,rtrep=.f.,left=551,top=226,width=125,height=21;
    , ToolTipText = "Valuta di conto";
    , HelpContextID = 41914842;
    , cFormVar="w_CODVAL1",RowSource=""+"Valuta di conto,"+"Valuta nazionale,"+"", bObbl = .f. , nPag = 2;
    , cLinkFile="VALUTE";
  , bGlobalFont=.t.


  func oCODVAL1_2_21.RadioValue()
    return(iif(this.value =1,ALLTR(g_PERVAL),;
    iif(this.value =2,ALLTR(g_CODLIR),;
    iif(this.value =3,' ',;
    ' '))))
  endfunc
  func oCODVAL1_2_21.GetRadio()
    this.Parent.oContained.w_CODVAL1 = this.RadioValue()
    return .t.
  endfunc

  func oCODVAL1_2_21.SetRadio()
    this.Parent.oContained.w_CODVAL1=trim(this.Parent.oContained.w_CODVAL1)
    this.value = ;
      iif(this.Parent.oContained.w_CODVAL1==ALLTR(g_PERVAL),1,;
      iif(this.Parent.oContained.w_CODVAL1==ALLTR(g_CODLIR),2,;
      iif(this.Parent.oContained.w_CODVAL1=='',3,;
      0)))
  endfunc

  func oCODVAL1_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL1_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oBtn_2_22 as StdButton with uid="JODKRIMSQL",left=696, top=437, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 226853446;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oIDAVINI_2_30 as StdField with uid="LMHHIQCYRY",rtseq=56,rtrep=.f.,;
    cFormVar = "w_IDAVINI", cQueryName = "IDAVINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo dare/avere iniziale",;
    HelpContextID = 13190,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=161, Top=225, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oIDAVFIN_2_31 as StdField with uid="ROMAUOMKHU",rtseq=57,rtrep=.f.,;
    cFormVar = "w_IDAVFIN", cQueryName = "IDAVFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo dare/avere finale",;
    HelpContextID = 87018618,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=336, Top=225, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"


  add object oBtn_2_42 as StdButton with uid="TSHHXJIGTW",left=696, top=7, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 140412650;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_42.Click()
      this.parent.oContained.NotifyEvent("Aggiorna")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_2_43 as cp_runprogram with uid="HNPYATUTAL",left=6, top=510, width=165,height=24,;
    caption='GSCG_BVP(R)',;
   bGlobalFont=.t.,;
    prg="GSCG_BVP('R')",;
    cEvent = "Aggiorna",;
    nPag=2;
    , HelpContextID = 89409334

  add object oStr_2_1 as StdString with uid="VMDITUXAKY",Visible=.t., Left=6, Top=198,;
    Alignment=1, Width=150, Height=18,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="ZUAYJAPCQJ",Visible=.t., Left=6, Top=77,;
    Alignment=1, Width=150, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="HCEVWHRVQD",Visible=.t., Left=46, Top=111,;
    Alignment=1, Width=110, Height=18,;
    Caption="Codice valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="GFOWXSCDQY",Visible=.t., Left=6, Top=170,;
    Alignment=1, Width=150, Height=18,;
    Caption="Contiene note:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="IZSSKCUXQW",Visible=.t., Left=6, Top=226,;
    Alignment=1, Width=150, Height=18,;
    Caption="Importo dare/avere da:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="JZGPNUPDTD",Visible=.t., Left=303, Top=229,;
    Alignment=1, Width=28, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="DPNKSNTRZT",Visible=.t., Left=481, Top=228,;
    Alignment=1, Width=64, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="OHFSOFENUP",Visible=.t., Left=6, Top=22,;
    Alignment=1, Width=150, Height=18,;
    Caption="Da prot.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="XMPIFOHOAM",Visible=.t., Left=6, Top=51,;
    Alignment=1, Width=150, Height=18,;
    Caption="A prot.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="HLYCRSGMAB",Visible=.t., Left=280, Top=22,;
    Alignment=2, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="EKTCLFXOGW",Visible=.t., Left=280, Top=47,;
    Alignment=2, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="YRCKBFFDXR",Visible=.t., Left=37, Top=140,;
    Alignment=1, Width=119, Height=18,;
    Caption="Totale documento da:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="FZLTDHGNPW",Visible=.t., Left=303, Top=140,;
    Alignment=1, Width=28, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kvp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
