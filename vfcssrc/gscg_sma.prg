* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sma                                                        *
*              Stampa schede contabili                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_130]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-26                                                      *
* Last revis.: 2013-07-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sma",oParentObject))

* --- Class definition
define class tgscg_sma as StdForm
  Top    = 9
  Left   = 25

  * --- Standard Properties
  Width  = 642
  Height = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-07-10"
  HelpContextID=191505769
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=43

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  ESERCIZI_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  MASTRI_IDX = 0
  CACOCLFO_IDX = 0
  cPrg = "gscg_sma"
  cComment = "Stampa schede contabili"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_CODESE = space(4)
  o_CODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  w_FINESE2 = ctod('  /  /  ')
  w_DATSAL = space(1)
  o_DATSAL = space(1)
  w_FINESE = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_FINES2 = ctod('  /  /  ')
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_FLAPER = space(1)
  w_CODINI = space(15)
  o_CODINI = space(15)
  w_DESINI = space(40)
  w_CODFIN = space(15)
  w_RAGSOCINI = space(60)
  o_RAGSOCINI = space(60)
  w_RAGSOCFIN = space(60)
  w_DESFIN = space(40)
  w_CODCAU = space(5)
  w_CONSU = space(15)
  w_CONSU2 = space(15)
  w_CONSU1 = space(15)
  w_CATCON = space(5)
  w_DESCAU = space(35)
  w_FLPROV = space(1)
  w_SEZBIL = space(1)
  o_SEZBIL = space(1)
  w_PERCOM = space(10)
  w_TIPVAL = space(1)
  o_TIPVAL = space(1)
  w_CODVAL = space(3)
  o_CODVAL = space(3)
  w_DESVAL = space(35)
  w_CAOVAL1 = 0
  w_CAMBGIOR = 0
  w_SIMVAL = space(10)
  w_DECTOT = 0
  w_CAOVAL = 0
  o_CAOVAL = 0
  w_FLCONT = space(1)
  o_FLCONT = space(1)
  w_FLSTCONT = space(10)
  w_FLSPAG = space(1)
  w_CAMREG = space(1)
  w_FLDESC = space(1)
  w_DETSCA = space(1)
  w_DESSUP = space(40)
  w_DESCON = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_smaPag1","gscg_sma",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODESE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='CAU_CONT'
    this.cWorkTables[5]='MASTRI'
    this.cWorkTables[6]='CACOCLFO'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_sma
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODESE=space(4)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE2=ctod("  /  /  ")
      .w_DATSAL=space(1)
      .w_FINESE=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_FINES2=ctod("  /  /  ")
      .w_TIPCON=space(1)
      .w_FLAPER=space(1)
      .w_CODINI=space(15)
      .w_DESINI=space(40)
      .w_CODFIN=space(15)
      .w_RAGSOCINI=space(60)
      .w_RAGSOCFIN=space(60)
      .w_DESFIN=space(40)
      .w_CODCAU=space(5)
      .w_CONSU=space(15)
      .w_CONSU2=space(15)
      .w_CONSU1=space(15)
      .w_CATCON=space(5)
      .w_DESCAU=space(35)
      .w_FLPROV=space(1)
      .w_SEZBIL=space(1)
      .w_PERCOM=space(10)
      .w_TIPVAL=space(1)
      .w_CODVAL=space(3)
      .w_DESVAL=space(35)
      .w_CAOVAL1=0
      .w_CAMBGIOR=0
      .w_SIMVAL=space(10)
      .w_DECTOT=0
      .w_CAOVAL=0
      .w_FLCONT=space(1)
      .w_FLSTCONT=space(10)
      .w_FLSPAG=space(1)
      .w_CAMREG=space(1)
      .w_FLDESC=space(1)
      .w_DETSCA=space(1)
      .w_DESSUP=space(40)
      .w_DESCON=space(35)
        .w_CODAZI = i_CODAZI
        .w_OBTEST = i_INIDAT
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODESE))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,5,.f.)
        .w_DATSAL = 'N'
        .w_FINESE = cp_CharToDate((STR(DAY(.w_FINESE2))) + '-' + STR(MONTH(.w_FINESE2)) + '-' + STR(YEAR(.w_FINESE2)+1))
        .w_DATINI = IIF(EMPTY(.w_CODESE), g_INIESE, .w_INIESE)
        .w_DATFIN = IIF(EMPTY(.w_FINESE2), g_FINESE, .w_FINESE)
        .w_FINES2 = .w_DATFIN
        .w_TIPCON = 'G'
        .w_FLAPER = 'N'
        .w_CODINI = SPACE(15)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODINI))
          .link_1_13('Full')
        endif
          .DoRTCalc(14,14,.f.)
        .w_CODFIN = .w_CODINI
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CODFIN))
          .link_1_15('Full')
        endif
        .w_RAGSOCINI = SPACE(40)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_RAGSOCINI))
          .link_1_16('Full')
        endif
        .w_RAGSOCFIN = .w_RAGSOCINI
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_RAGSOCFIN))
          .link_1_17('Full')
        endif
        .DoRTCalc(18,19,.f.)
        if not(empty(.w_CODCAU))
          .link_1_19('Full')
        endif
        .w_CONSU = space(15)
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CONSU))
          .link_1_20('Full')
        endif
        .w_CONSU2 = space(15)
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_CONSU2))
          .link_1_21('Full')
        endif
        .w_CONSU1 = space(15)
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_CONSU1))
          .link_1_22('Full')
        endif
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_CATCON))
          .link_1_23('Full')
        endif
          .DoRTCalc(24,24,.f.)
        .w_FLPROV = 'N'
        .w_SEZBIL = 'S'
        .w_PERCOM = ' '
        .w_TIPVAL = 'C'
        .w_CODVAL = g_PERVAL
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_CODVAL))
          .link_1_29('Full')
        endif
          .DoRTCalc(30,31,.f.)
        .w_CAMBGIOR = GETCAM(.w_CODVAL, i_datsys, 7)
        .w_SIMVAL = IIF(EMPTY(.w_CODVAL),' ',.w_SIMVAL)
          .DoRTCalc(34,34,.f.)
        .w_CAOVAL = IIF(.w_CAOVAL1=0, GETCAM(.w_CODVAL, i_DATSYS, 0), .w_CAOVAL1)
        .w_FLCONT = IIF(.w_DATSAL='S','N', IIF(EMPTY(.w_FLCONT),'N',.w_FLCONT))
        .w_FLSTCONT = 'N'
        .w_FLSPAG = IIF(.w_DATSAL='S',' ', .w_FLSPAG)
        .w_CAMREG = 'N'
        .w_FLDESC = IIF(.w_DATSAL='S',' ', .w_FLDESC)
        .w_DETSCA = IIF(.w_DATSAL='S',' ', .w_DETSCA)
    endwith
    this.DoRTCalc(42,43,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
            .w_FINESE = cp_CharToDate((STR(DAY(.w_FINESE2))) + '-' + STR(MONTH(.w_FINESE2)) + '-' + STR(YEAR(.w_FINESE2)+1))
        if .o_CODESE<>.w_CODESE
            .w_DATINI = IIF(EMPTY(.w_CODESE), g_INIESE, .w_INIESE)
        endif
        if .o_CODESE<>.w_CODESE
            .w_DATFIN = IIF(EMPTY(.w_FINESE2), g_FINESE, .w_FINESE)
        endif
        .DoRTCalc(10,11,.t.)
        if .o_CODESE<>.w_CODESE
            .w_FLAPER = 'N'
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_CODINI = SPACE(15)
          .link_1_13('Full')
        endif
        .DoRTCalc(14,14,.t.)
        if .o_TIPCON<>.w_TIPCON.or. .o_CODINI<>.w_CODINI
            .w_CODFIN = .w_CODINI
          .link_1_15('Full')
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_RAGSOCINI = SPACE(40)
          .link_1_16('Full')
        endif
        if .o_TIPCON<>.w_TIPCON.or. .o_RAGSOCINI<>.w_RAGSOCINI
            .w_RAGSOCFIN = .w_RAGSOCINI
          .link_1_17('Full')
        endif
        .DoRTCalc(18,19,.t.)
        if .o_TIPCON<>.w_TIPCON
            .w_CONSU = space(15)
          .link_1_20('Full')
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_CONSU2 = space(15)
          .link_1_21('Full')
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_CONSU1 = space(15)
          .link_1_22('Full')
        endif
        .DoRTCalc(23,26,.t.)
        if .o_SEZBIL<>.w_SEZBIL
            .w_PERCOM = ' '
        endif
        .DoRTCalc(28,28,.t.)
        if .o_TIPVAL<>.w_TIPVAL
            .w_CODVAL = g_PERVAL
          .link_1_29('Full')
        endif
        .DoRTCalc(30,31,.t.)
        if .o_CODVAL<>.w_CODVAL
            .w_CAMBGIOR = GETCAM(.w_CODVAL, i_datsys, 7)
        endif
            .w_SIMVAL = IIF(EMPTY(.w_CODVAL),' ',.w_SIMVAL)
        .DoRTCalc(34,34,.t.)
        if .o_TIPVAL<>.w_TIPVAL.or. .o_CODVAL<>.w_CODVAL
            .w_CAOVAL = IIF(.w_CAOVAL1=0, GETCAM(.w_CODVAL, i_DATSYS, 0), .w_CAOVAL1)
        endif
        if .o_DATSAL<>.w_DATSAL
            .w_FLCONT = IIF(.w_DATSAL='S','N', IIF(EMPTY(.w_FLCONT),'N',.w_FLCONT))
        endif
        if .o_FLCONT<>.w_FLCONT
            .w_FLSTCONT = 'N'
        endif
        if .o_DATSAL<>.w_DATSAL
            .w_FLSPAG = IIF(.w_DATSAL='S',' ', .w_FLSPAG)
        endif
        if .o_TIPVAL<>.w_TIPVAL.or. .o_CODVAL<>.w_CODVAL.or. .o_FLCONT<>.w_FLCONT.or. .o_DATSAL<>.w_DATSAL
            .w_CAMREG = 'N'
        endif
        if .o_DATSAL<>.w_DATSAL
            .w_FLDESC = IIF(.w_DATSAL='S',' ', .w_FLDESC)
        endif
        if .o_DATSAL<>.w_DATSAL
            .w_DETSCA = IIF(.w_DATSAL='S',' ', .w_DETSCA)
        endif
        if .o_CAOVAL<>.w_CAOVAL
          .Calculate_SYFBHBBWHP()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(42,43,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_SYFBHBBWHP()
    with this
          * --- Inserisce cambio giornaliero
     if not empty(.w_CODVAL) and .w_CODVAL<>g_perval and .w_CAMBGIOR<>.w_CAOVAL
          GSAR_BIC(this;
              ,.w_CODVAL;
              ,.w_CAOVAL;
              ,i_datsys;
             )
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPERCOM_1_27.enabled = this.oPgFrm.Page1.oPag.oPERCOM_1_27.mCond()
    this.oPgFrm.Page1.oPag.oCODVAL_1_29.enabled = this.oPgFrm.Page1.oPag.oCODVAL_1_29.mCond()
    this.oPgFrm.Page1.oPag.oCAOVAL_1_35.enabled = this.oPgFrm.Page1.oPag.oCAOVAL_1_35.mCond()
    this.oPgFrm.Page1.oPag.oFLCONT_1_36.enabled = this.oPgFrm.Page1.oPag.oFLCONT_1_36.mCond()
    this.oPgFrm.Page1.oPag.oFLSPAG_1_38.enabled = this.oPgFrm.Page1.oPag.oFLSPAG_1_38.mCond()
    this.oPgFrm.Page1.oPag.oCAMREG_1_39.enabled = this.oPgFrm.Page1.oPag.oCAMREG_1_39.mCond()
    this.oPgFrm.Page1.oPag.oFLDESC_1_40.enabled = this.oPgFrm.Page1.oPag.oFLDESC_1_40.mCond()
    this.oPgFrm.Page1.oPag.oDETSCA_1_41.enabled = this.oPgFrm.Page1.oPag.oDETSCA_1_41.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLAPER_1_12.visible=!this.oPgFrm.Page1.oPag.oFLAPER_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCONSU_1_20.visible=!this.oPgFrm.Page1.oPag.oCONSU_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCONSU2_1_21.visible=!this.oPgFrm.Page1.oPag.oCONSU2_1_21.mHide()
    this.oPgFrm.Page1.oPag.oCONSU1_1_22.visible=!this.oPgFrm.Page1.oPag.oCONSU1_1_22.mHide()
    this.oPgFrm.Page1.oPag.oCATCON_1_23.visible=!this.oPgFrm.Page1.oPag.oCATCON_1_23.mHide()
    this.oPgFrm.Page1.oPag.oCODVAL_1_29.visible=!this.oPgFrm.Page1.oPag.oCODVAL_1_29.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAL_1_33.visible=!this.oPgFrm.Page1.oPag.oSIMVAL_1_33.mHide()
    this.oPgFrm.Page1.oPag.oFLSTCONT_1_37.visible=!this.oPgFrm.Page1.oPag.oFLSTCONT_1_37.mHide()
    this.oPgFrm.Page1.oPag.oCAMREG_1_39.visible=!this.oPgFrm.Page1.oPag.oCAMREG_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_58.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_63.visible=!this.oPgFrm.Page1.oPag.oStr_1_63.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_3'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE2 = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODINI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODINI_1_13'),i_cWhere,'GSAR_BZC',"Elenco",'CONTIZOOM.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODINI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESINI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(15)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODFIN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_15'),i_cWhere,'GSAR_BZC',"Elenco",'CONTIZOOM.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o codice finale minore del codice iniziale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODFIN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFIN = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(15)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CODFIN) OR .w_CODFIN>=.w_CODINI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o codice finale minore del codice iniziale")
        endif
        this.w_CODFIN = space(15)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAGSOCINI
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAGSOCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_RAGSOCINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANDESCRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANDESCRI',trim(this.w_RAGSOCINI))
          select ANTIPCON,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANDESCRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RAGSOCINI)==trim(_Link_.ANDESCRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RAGSOCINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(oSource.parent,'oRAGSOCINI_1_16'),i_cWhere,'GSAR_BZC',"Clienti/Fornitori/Conti",'CONTI1ZOOM.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("La desc. conto iniziale � pi� grande di quella finale oppure obsoleta")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANDESCRI',oSource.xKey(2))
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAGSOCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(this.w_RAGSOCINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANDESCRI',this.w_RAGSOCINI)
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAGSOCINI = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_RAGSOCINI = space(60)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_RAGSOCFIN)) OR  (.w_RAGSOCINI<=.w_RAGSOCFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La desc. conto iniziale � pi� grande di quella finale oppure obsoleta")
        endif
        this.w_RAGSOCINI = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANDESCRI,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAGSOCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAGSOCFIN
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAGSOCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_RAGSOCFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANDESCRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANDESCRI',trim(this.w_RAGSOCFIN))
          select ANTIPCON,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANDESCRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RAGSOCFIN)==trim(_Link_.ANDESCRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RAGSOCFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(oSource.parent,'oRAGSOCFIN_1_17'),i_cWhere,'GSAR_BZC',"Clienti/Fornitori/Conti",'CONTI1ZOOM.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("La desc. conto iniziale � pi� grande di quella finale oppure obsoleta")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANDESCRI',oSource.xKey(2))
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAGSOCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANDESCRI="+cp_ToStrODBC(this.w_RAGSOCFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANDESCRI',this.w_RAGSOCFIN)
            select ANTIPCON,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAGSOCFIN = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_RAGSOCFIN = space(60)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_RAGSOCFIN>=.w_RAGSOCINI) or (empty(.w_RAGSOCINI)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La desc. conto iniziale � pi� grande di quella finale oppure obsoleta")
        endif
        this.w_RAGSOCFIN = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANDESCRI,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAGSOCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAU
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CODCAU))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCODCAU_1_19'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAU)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONSU
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONSU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CONSU)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CONSU))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONSU)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONSU) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCONSU_1_20'),i_cWhere,'GSAR_AMC',"Mastri contabili (clienti)",'GSAR_ACL.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONSU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CONSU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CONSU)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONSU = NVL(_Link_.MCCODICE,space(15))
      this.w_DESSUP = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CONSU = space(15)
      endif
      this.w_DESSUP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONSU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONSU2
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONSU2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CONSU2)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CONSU2))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONSU2)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONSU2) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCONSU2_1_21'),i_cWhere,'GSAR_AMC',"Mastri contabili (fornitori)",'GSAR_AFR.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONSU2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CONSU2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CONSU2)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONSU2 = NVL(_Link_.MCCODICE,space(15))
      this.w_DESSUP = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CONSU2 = space(15)
      endif
      this.w_DESSUP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONSU2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONSU1
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONSU1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CONSU1)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CONSU1))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONSU1)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONSU1) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCONSU1_1_22'),i_cWhere,'GSAR_AMC',"Mastri contabili (conti)",'GSAR_API.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONSU1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CONSU1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CONSU1)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONSU1 = NVL(_Link_.MCCODICE,space(15))
      this.w_DESSUP = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CONSU1 = space(15)
      endif
      this.w_DESSUP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONSU1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_1_23'),i_cWhere,'GSAR_AC2',"Categorie contabili clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCON = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(5)
      endif
      this.w_DESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VACAOVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL))
          select VACODVAL,VADESVAL,VASIMVAL,VACAOVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_CODVAL)+"%");

            select VACODVAL,VADESVAL,VASIMVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL_1_29'),i_cWhere,'GSAR_AVL',"Divise",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VACAOVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VASIMVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADESVAL,VASIMVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(10))
      this.w_CAOVAL1 = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_SIMVAL = space(10)
      this.w_CAOVAL1 = 0
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_3.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_3.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSAL_1_6.RadioValue()==this.w_DATSAL)
      this.oPgFrm.Page1.oPag.oDATSAL_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_8.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_8.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_9.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_9.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_11.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAPER_1_12.RadioValue()==this.w_FLAPER)
      this.oPgFrm.Page1.oPag.oFLAPER_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_13.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_13.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_14.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_14.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_15.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_15.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOCINI_1_16.value==this.w_RAGSOCINI)
      this.oPgFrm.Page1.oPag.oRAGSOCINI_1_16.value=this.w_RAGSOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOCFIN_1_17.value==this.w_RAGSOCFIN)
      this.oPgFrm.Page1.oPag.oRAGSOCFIN_1_17.value=this.w_RAGSOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_18.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_18.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_19.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_19.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCONSU_1_20.value==this.w_CONSU)
      this.oPgFrm.Page1.oPag.oCONSU_1_20.value=this.w_CONSU
    endif
    if not(this.oPgFrm.Page1.oPag.oCONSU2_1_21.value==this.w_CONSU2)
      this.oPgFrm.Page1.oPag.oCONSU2_1_21.value=this.w_CONSU2
    endif
    if not(this.oPgFrm.Page1.oPag.oCONSU1_1_22.value==this.w_CONSU1)
      this.oPgFrm.Page1.oPag.oCONSU1_1_22.value=this.w_CONSU1
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCON_1_23.value==this.w_CATCON)
      this.oPgFrm.Page1.oPag.oCATCON_1_23.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_24.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_24.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPROV_1_25.RadioValue()==this.w_FLPROV)
      this.oPgFrm.Page1.oPag.oFLPROV_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSEZBIL_1_26.RadioValue()==this.w_SEZBIL)
      this.oPgFrm.Page1.oPag.oSEZBIL_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERCOM_1_27.RadioValue()==this.w_PERCOM)
      this.oPgFrm.Page1.oPag.oPERCOM_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPVAL_1_28.RadioValue()==this.w_TIPVAL)
      this.oPgFrm.Page1.oPag.oTIPVAL_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_29.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_29.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_33.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_33.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCAOVAL_1_35.value==this.w_CAOVAL)
      this.oPgFrm.Page1.oPag.oCAOVAL_1_35.value=this.w_CAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCONT_1_36.RadioValue()==this.w_FLCONT)
      this.oPgFrm.Page1.oPag.oFLCONT_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSTCONT_1_37.RadioValue()==this.w_FLSTCONT)
      this.oPgFrm.Page1.oPag.oFLSTCONT_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSPAG_1_38.RadioValue()==this.w_FLSPAG)
      this.oPgFrm.Page1.oPag.oFLSPAG_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMREG_1_39.RadioValue()==this.w_CAMREG)
      this.oPgFrm.Page1.oPag.oCAMREG_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDESC_1_40.RadioValue()==this.w_FLDESC)
      this.oPgFrm.Page1.oPag.oFLDESC_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDETSCA_1_41.RadioValue()==this.w_DETSCA)
      this.oPgFrm.Page1.oPag.oDETSCA_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSUP_1_56.value==this.w_DESSUP)
      this.oPgFrm.Page1.oPag.oDESSUP_1_56.value=this.w_DESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_58.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_58.value=this.w_DESCON
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DATINI)) or not(((.w_DATINI<=.w_DATFIN OR (EMPTY(.w_DATFIN))) AND .w_DATINI>=.w_INIESE)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_8.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale o precedente all'inizio dell'esercizio")
          case   ((empty(.w_DATFIN)) or not((.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATINI))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(EMPTY(.w_CODFIN) OR .w_CODFIN>=.w_CODINI)  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o codice finale minore del codice iniziale")
          case   not((empty(.w_RAGSOCFIN)) OR  (.w_RAGSOCINI<=.w_RAGSOCFIN))  and not(empty(.w_RAGSOCINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRAGSOCINI_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La desc. conto iniziale � pi� grande di quella finale oppure obsoleta")
          case   not(((.w_RAGSOCFIN>=.w_RAGSOCINI) or (empty(.w_RAGSOCINI))))  and not(empty(.w_RAGSOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRAGSOCFIN_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La desc. conto iniziale � pi� grande di quella finale oppure obsoleta")
          case   (empty(.w_CODVAL))  and not(.w_TIPVAL='C')  and (.w_TIPVAL<>'C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODVAL_1_29.SetFocus()
            i_bnoObbl = !empty(.w_CODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAOVAL))  and (.w_CAOVAL1=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAOVAL_1_35.SetFocus()
            i_bnoObbl = !empty(.w_CAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODESE = this.w_CODESE
    this.o_DATSAL = this.w_DATSAL
    this.o_TIPCON = this.w_TIPCON
    this.o_CODINI = this.w_CODINI
    this.o_RAGSOCINI = this.w_RAGSOCINI
    this.o_SEZBIL = this.w_SEZBIL
    this.o_TIPVAL = this.w_TIPVAL
    this.o_CODVAL = this.w_CODVAL
    this.o_CAOVAL = this.w_CAOVAL
    this.o_FLCONT = this.w_FLCONT
    return

enddefine

* --- Define pages as container
define class tgscg_smaPag1 as StdContainer
  Width  = 638
  height = 390
  stdWidth  = 638
  stdheight = 390
  resizeXpos=401
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODESE_1_3 as StdField with uid="ZMIJKNZDWM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 15765978,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=120, Top=8, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDATSAL_1_6 as StdCheck with uid="UNABNQHVBX",rtseq=6,rtrep=.f.,left=254, top=8, caption="Solo saldi contabili",;
    ToolTipText = "se attivato stampa solo saldi contabili",;
    HelpContextID = 184655818,;
    cFormVar="w_DATSAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDATSAL_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDATSAL_1_6.GetRadio()
    this.Parent.oContained.w_DATSAL = this.RadioValue()
    return .t.
  endfunc

  func oDATSAL_1_6.SetRadio()
    this.Parent.oContained.w_DATSAL=trim(this.Parent.oContained.w_DATSAL)
    this.value = ;
      iif(this.Parent.oContained.w_DATSAL=='S',1,;
      0)
  endfunc

  add object oDATINI_1_8 as StdField with uid="VPJLNVRTYN",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale o precedente all'inizio dell'esercizio",;
    ToolTipText = "Data di registrazione di inizio selezione",;
    HelpContextID = 222011338,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=439, Top=8

  func oDATINI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_DATINI<=.w_DATFIN OR (EMPTY(.w_DATFIN))) AND .w_DATINI>=.w_INIESE))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_9 as StdField with uid="MCOUDCLKFB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data di registrazione di fine selezione",;
    HelpContextID = 143564746,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=555, Top=8

  func oDATFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATINI)))
    endwith
    return bRes
  endfunc


  add object oTIPCON_1_11 as StdCombo with uid="TKQYZYBPVV",rtseq=11,rtrep=.f.,left=120,top=36,width=80,height=21;
    , ToolTipText = "Tipo conto (clienti/fornitori/generici) di selezione";
    , HelpContextID = 137483978;
    , cFormVar="w_TIPCON",RowSource=""+"Cliente,"+"Fornitore,"+"Conto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_11.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    ' '))))
  endfunc
  func oTIPCON_1_11.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_11.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      iif(this.Parent.oContained.w_TIPCON=='G',3,;
      0)))
  endfunc

  func oTIPCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODINI)
        bRes2=.link_1_13('Full')
      endif
      if .not. empty(.w_CODFIN)
        bRes2=.link_1_15('Full')
      endif
      if .not. empty(.w_RAGSOCINI)
        bRes2=.link_1_16('Full')
      endif
      if .not. empty(.w_RAGSOCFIN)
        bRes2=.link_1_17('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oFLAPER_1_12 as StdCheck with uid="SFDRKNVLEM",rtseq=12,rtrep=.f.,left=254, top=34, caption="Nei totali dare/avere considera anche mov. apertura/chiusura",;
    ToolTipText = "se attivato nei totali dare/avere verranno conteggiati anche i movimenti di apertura/chiusura (che quindi non verranno visualizzati in azzurro)",;
    HelpContextID = 80069802,;
    cFormVar="w_FLAPER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLAPER_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLAPER_1_12.GetRadio()
    this.Parent.oContained.w_FLAPER = this.RadioValue()
    return .t.
  endfunc

  func oFLAPER_1_12.SetRadio()
    this.Parent.oContained.w_FLAPER=trim(this.Parent.oContained.w_FLAPER)
    this.value = ;
      iif(this.Parent.oContained.w_FLAPER=='S',1,;
      0)
  endfunc

  func oFLAPER_1_12.mHide()
    with this.Parent.oContained
      return (Empty(.w_CODESE))
    endwith
  endfunc

  add object oCODINI_1_13 as StdField with uid="ZZFIMKSUCD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice conto di inizio selezione (vuoto=primo)",;
    HelpContextID = 222073306,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=120, Top=62, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODINI"

  func oCODINI_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODINI_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco",'CONTIZOOM.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODINI_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_14 as StdField with uid="YISYJVIDXY",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 222014410,;
   bGlobalFont=.t.,;
    Height=21, Width=376, Left=254, Top=62, InputMask=replicate('X',40)

  add object oCODFIN_1_15 as StdField with uid="LPIAICZXJM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o codice finale minore del codice iniziale",;
    ToolTipText = "Codice conto di fine selezione (vuoto=ultimo)",;
    HelpContextID = 143626714,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=120, Top=90, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFIN"

  func oCODFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFIN_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco",'CONTIZOOM.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oRAGSOCINI_1_16 as StdField with uid="VKPUFKLWYU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_RAGSOCINI", cQueryName = "RAGSOCINI",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    sErrorMsg = "La desc. conto iniziale � pi� grande di quella finale oppure obsoleta",;
    ToolTipText = "Ragione sociale di inizio selezione",;
    HelpContextID = 215848436,;
   bGlobalFont=.t.,;
    Height=21, Width=399, Left=120, Top=118, InputMask=replicate('X',60), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANDESCRI", oKey_2_2="this.w_RAGSOCINI"

  func oRAGSOCINI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oRAGSOCINI_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRAGSOCINI_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(this.parent,'oRAGSOCINI_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/Fornitori/Conti",'CONTI1ZOOM.CONTI_VZM',this.parent.oContained
  endproc
  proc oRAGSOCINI_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANDESCRI=this.parent.oContained.w_RAGSOCINI
     i_obj.ecpSave()
  endproc

  add object oRAGSOCFIN_1_17 as StdField with uid="OSOHGLOZBC",rtseq=17,rtrep=.f.,;
    cFormVar = "w_RAGSOCFIN", cQueryName = "RAGSOCFIN",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    sErrorMsg = "La desc. conto iniziale � pi� grande di quella finale oppure obsoleta",;
    ToolTipText = "Ragione sociale di fine selezione",;
    HelpContextID = 215848511,;
   bGlobalFont=.t.,;
    Height=21, Width=399, Left=120, Top=146, InputMask=replicate('X',60), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANDESCRI", oKey_2_2="this.w_RAGSOCFIN"

  func oRAGSOCFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oRAGSOCFIN_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRAGSOCFIN_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANDESCRI',cp_AbsName(this.parent,'oRAGSOCFIN_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/Fornitori/Conti",'CONTI1ZOOM.CONTI_VZM',this.parent.oContained
  endproc
  proc oRAGSOCFIN_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANDESCRI=this.parent.oContained.w_RAGSOCFIN
     i_obj.ecpSave()
  endproc

  add object oDESFIN_1_18 as StdField with uid="YGZHACUMMY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 143567818,;
   bGlobalFont=.t.,;
    Height=21, Width=376, Left=254, Top=90, InputMask=replicate('X',40)

  add object oCODCAU_1_19 as StdField with uid="PMCRSQDQQD",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contabile di selezione (vuoto=no selezione)",;
    HelpContextID = 34771418,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=120, Top=174, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAU"

  func oCODCAU_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAU_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAU_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCODCAU_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oCODCAU_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CODCAU
     i_obj.ecpSave()
  endproc

  add object oCONSU_1_20 as StdField with uid="MUVELETNXS",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CONSU", cQueryName = "CONSU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento del cliente di selezione",;
    HelpContextID = 96596442,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=120, Top=202, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CONSU"

  func oCONSU_1_20.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc

  func oCONSU_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONSU_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONSU_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCONSU_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (clienti)",'GSAR_ACL.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCONSU_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CONSU
     i_obj.ecpSave()
  endproc

  add object oCONSU2_1_21 as StdField with uid="QFGXAJJVCQ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CONSU2", cQueryName = "CONSU2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento del fornitore di selezione",;
    HelpContextID = 63042010,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=120, Top=202, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CONSU2"

  func oCONSU2_1_21.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'F')
    endwith
  endfunc

  func oCONSU2_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONSU2_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONSU2_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCONSU2_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (fornitori)",'GSAR_AFR.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCONSU2_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CONSU2
     i_obj.ecpSave()
  endproc

  add object oCONSU1_1_22 as StdField with uid="WZBPIJQSXL",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CONSU1", cQueryName = "CONSU1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento del conto di selezione",;
    HelpContextID = 79819226,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=120, Top=202, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CONSU1"

  func oCONSU1_1_22.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'G')
    endwith
  endfunc

  func oCONSU1_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONSU1_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONSU1_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCONSU1_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (conti)",'GSAR_API.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCONSU1_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CONSU1
     i_obj.ecpSave()
  endproc

  add object oCATCON_1_23 as StdField with uid="ZSEKMTOOXL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria contabile del cliente di selezione",;
    HelpContextID = 137469914,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=120, Top=230, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_1_23.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='G')
    endwith
  endfunc

  func oCATCON_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili clienti/fornitori",'',this.parent.oContained
  endproc
  proc oCATCON_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_CATCON
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_24 as StdField with uid="TFMDCUTVMQ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 34712522,;
   bGlobalFont=.t.,;
    Height=21, Width=376, Left=254, Top=174, InputMask=replicate('X',35)


  add object oFLPROV_1_25 as StdCombo with uid="VZJIVUBLRK",value=3,rtseq=25,rtrep=.f.,left=120,top=258,width=87,height=21;
    , ToolTipText = "Stato dei movimenti da stampare";
    , HelpContextID = 2282666;
    , cFormVar="w_FLPROV",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLPROV_1_25.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    ''))))
  endfunc
  func oFLPROV_1_25.GetRadio()
    this.Parent.oContained.w_FLPROV = this.RadioValue()
    return .t.
  endfunc

  func oFLPROV_1_25.SetRadio()
    this.Parent.oContained.w_FLPROV=trim(this.Parent.oContained.w_FLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_FLPROV=='N',1,;
      iif(this.Parent.oContained.w_FLPROV=='S',2,;
      iif(this.Parent.oContained.w_FLPROV=='',3,;
      0)))
  endfunc


  add object oSEZBIL_1_26 as StdCombo with uid="EYGGPLBDRQ",rtseq=26,rtrep=.f.,left=318,top=258,width=89,height=21;
    , ToolTipText = "Sezione di bilancio da selezionare";
    , HelpContextID = 177355482;
    , cFormVar="w_SEZBIL",RowSource=""+"Attivit�,"+"Passivit�,"+"Costi,"+"Ricavi,"+"Ordine,"+"Transitori,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSEZBIL_1_26.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'P',;
    iif(this.value =3,'C',;
    iif(this.value =4,'R',;
    iif(this.value =5,'O',;
    iif(this.value =6,'T',;
    iif(this.value =7,'S',;
    space(1)))))))))
  endfunc
  func oSEZBIL_1_26.GetRadio()
    this.Parent.oContained.w_SEZBIL = this.RadioValue()
    return .t.
  endfunc

  func oSEZBIL_1_26.SetRadio()
    this.Parent.oContained.w_SEZBIL=trim(this.Parent.oContained.w_SEZBIL)
    this.value = ;
      iif(this.Parent.oContained.w_SEZBIL=='A',1,;
      iif(this.Parent.oContained.w_SEZBIL=='P',2,;
      iif(this.Parent.oContained.w_SEZBIL=='C',3,;
      iif(this.Parent.oContained.w_SEZBIL=='R',4,;
      iif(this.Parent.oContained.w_SEZBIL=='O',5,;
      iif(this.Parent.oContained.w_SEZBIL=='T',6,;
      iif(this.Parent.oContained.w_SEZBIL=='S',7,;
      0)))))))
  endfunc

  add object oPERCOM_1_27 as StdCheck with uid="UPXKATLKVM",rtseq=27,rtrep=.f.,left=492, top=259, caption="Per competenza",;
    ToolTipText = "Se attivo viene stampato solo il rateo di competenza",;
    HelpContextID = 154254090,;
    cFormVar="w_PERCOM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPERCOM_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPERCOM_1_27.GetRadio()
    this.Parent.oContained.w_PERCOM = this.RadioValue()
    return .t.
  endfunc

  func oPERCOM_1_27.SetRadio()
    this.Parent.oContained.w_PERCOM=trim(this.Parent.oContained.w_PERCOM)
    this.value = ;
      iif(this.Parent.oContained.w_PERCOM=='S',1,;
      0)
  endfunc

  func oPERCOM_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZBIL $ 'CR')
    endwith
   endif
  endfunc


  add object oTIPVAL_1_28 as StdCombo with uid="OAANFTWTLX",rtseq=28,rtrep=.f.,left=120,top=284,width=110,height=21;
    , ToolTipText = "Stampa in valuta di conto o in altra valuta";
    , HelpContextID = 184473290;
    , cFormVar="w_TIPVAL",RowSource=""+"Valuta di conto,"+"Altra valuta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPVAL_1_28.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oTIPVAL_1_28.GetRadio()
    this.Parent.oContained.w_TIPVAL = this.RadioValue()
    return .t.
  endfunc

  func oTIPVAL_1_28.SetRadio()
    this.Parent.oContained.w_TIPVAL=trim(this.Parent.oContained.w_TIPVAL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPVAL=='C',1,;
      iif(this.Parent.oContained.w_TIPVAL=='A',2,;
      0))
  endfunc

  add object oCODVAL_1_29 as StdField with uid="QEOBINSEQY",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 184521178,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=318, Top=284, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPVAL<>'C')
    endwith
   endif
  endfunc

  func oCODVAL_1_29.mHide()
    with this.Parent.oContained
      return (.w_TIPVAL='C')
    endwith
  endfunc

  func oCODVAL_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAL_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCODVAL_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Divise",'',this.parent.oContained
  endproc
  proc oCODVAL_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CODVAL
     i_obj.ecpSave()
  endproc

  add object oSIMVAL_1_33 as StdField with uid="XLWGPFFSWM",rtseq=33,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 184485594,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=365, Top=284, InputMask=replicate('X',10)

  func oSIMVAL_1_33.mHide()
    with this.Parent.oContained
      return (.w_TIPVAL='C')
    endwith
  endfunc

  add object oCAOVAL_1_35 as StdField with uid="ATPOYHLHOC",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CAOVAL", cQueryName = "CAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio in uscita dalla moneta di conto",;
    HelpContextID = 184479706,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=492, Top=284, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCAOVAL_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL1=0)
    endwith
   endif
  endfunc


  add object oFLCONT_1_36 as StdCombo with uid="XHTFNYCPIK",rtseq=36,rtrep=.f.,left=120,top=310,width=164,height=21;
    , ToolTipText = "Tipologia delle contropartite nelle registrazioni di primanota da stampare";
    , HelpContextID = 37135530;
    , cFormVar="w_FLCONT",RowSource=""+"Nessuna,"+"Solo sezione opposta,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLCONT_1_36.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLCONT_1_36.GetRadio()
    this.Parent.oContained.w_FLCONT = this.RadioValue()
    return .t.
  endfunc

  func oFLCONT_1_36.SetRadio()
    this.Parent.oContained.w_FLCONT=trim(this.Parent.oContained.w_FLCONT)
    this.value = ;
      iif(this.Parent.oContained.w_FLCONT=='N',1,;
      iif(this.Parent.oContained.w_FLCONT=='S',2,;
      iif(this.Parent.oContained.w_FLCONT=='T',3,;
      0)))
  endfunc

  func oFLCONT_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DETSCA<>'S' AND .w_DATSAL='N')
    endwith
   endif
  endfunc

  add object oFLSTCONT_1_37 as StdCheck with uid="PELZNQWMQD",rtseq=37,rtrep=.f.,left=318, top=316, caption="Escludi controp. su reg. apertura/chiusura",;
    ToolTipText = "Se attivo vengono escluse le contropartite relative a registrazioni di apertura/chiusura",;
    HelpContextID = 132162646,;
    cFormVar="w_FLSTCONT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSTCONT_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLSTCONT_1_37.GetRadio()
    this.Parent.oContained.w_FLSTCONT = this.RadioValue()
    return .t.
  endfunc

  func oFLSTCONT_1_37.SetRadio()
    this.Parent.oContained.w_FLSTCONT=trim(this.Parent.oContained.w_FLSTCONT)
    this.value = ;
      iif(this.Parent.oContained.w_FLSTCONT=='S',1,;
      0)
  endfunc

  func oFLSTCONT_1_37.mHide()
    with this.Parent.oContained
      return (.w_FLCONT = 'N')
    endwith
  endfunc

  add object oFLSPAG_1_38 as StdCheck with uid="ILWVRUCXFC",rtseq=38,rtrep=.f.,left=120, top=336, caption="Salto pagina",;
    ToolTipText = "Salto pagina dopo ogni scheda",;
    HelpContextID = 304298,;
    cFormVar="w_FLSPAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSPAG_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLSPAG_1_38.GetRadio()
    this.Parent.oContained.w_FLSPAG = this.RadioValue()
    return .t.
  endfunc

  func oFLSPAG_1_38.SetRadio()
    this.Parent.oContained.w_FLSPAG=trim(this.Parent.oContained.w_FLSPAG)
    this.value = ;
      iif(this.Parent.oContained.w_FLSPAG=='S',1,;
      0)
  endfunc

  func oFLSPAG_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DATSAL='N')
    endwith
   endif
  endfunc

  add object oCAMREG_1_39 as StdCheck with uid="MIBCIUHACZ",rtseq=39,rtrep=.f.,left=318, top=336, caption="Cambio registrazioni",;
    ToolTipText = "Se attivo per le registrazioni fatte nella stessa valuta di stampa utilizza il cambio della registrazione",;
    HelpContextID = 264441818,;
    cFormVar="w_CAMREG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAMREG_1_39.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAMREG_1_39.GetRadio()
    this.Parent.oContained.w_CAMREG = this.RadioValue()
    return .t.
  endfunc

  func oCAMREG_1_39.SetRadio()
    this.Parent.oContained.w_CAMREG=trim(this.Parent.oContained.w_CAMREG)
    this.value = ;
      iif(this.Parent.oContained.w_CAMREG=='S',1,;
      0)
  endfunc

  func oCAMREG_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL1=0 AND .w_DATSAL='N')
    endwith
   endif
  endfunc

  func oCAMREG_1_39.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL1<>0 OR .w_FLCONT='S')
    endwith
  endfunc

  add object oFLDESC_1_40 as StdCheck with uid="XCNOGCHNTK",rtseq=40,rtrep=.f.,left=120, top=361, caption="Stampa descrizione di riga",;
    ToolTipText = "Stampa anche le descrizioni di riga presenti in primanota",;
    HelpContextID = 49321130,;
    cFormVar="w_FLDESC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDESC_1_40.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLDESC_1_40.GetRadio()
    this.Parent.oContained.w_FLDESC = this.RadioValue()
    return .t.
  endfunc

  func oFLDESC_1_40.SetRadio()
    this.Parent.oContained.w_FLDESC=trim(this.Parent.oContained.w_FLDESC)
    this.value = ;
      iif(this.Parent.oContained.w_FLDESC=='S',1,;
      0)
  endfunc

  func oFLDESC_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DATSAL='N')
    endwith
   endif
  endfunc

  add object oDETSCA_1_41 as StdCheck with uid="YFPZQUEFCV",rtseq=41,rtrep=.f.,left=318, top=361, caption="Dettaglio scadenze",;
    ToolTipText = "Se attivo stampa il dettaglio partite/scadenze",;
    HelpContextID = 98671562,;
    cFormVar="w_DETSCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDETSCA_1_41.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDETSCA_1_41.GetRadio()
    this.Parent.oContained.w_DETSCA = this.RadioValue()
    return .t.
  endfunc

  func oDETSCA_1_41.SetRadio()
    this.Parent.oContained.w_DETSCA=trim(this.Parent.oContained.w_DETSCA)
    this.value = ;
      iif(this.Parent.oContained.w_DETSCA=='S',1,;
      0)
  endfunc

  func oDETSCA_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCONT= 'N' AND .w_DATSAL='N')
    endwith
   endif
  endfunc


  add object oBtn_1_42 as StdButton with uid="LYQUKVBVLZ",left=533, top=339, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 191477018;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        GSCG_BSV(this.Parent.oContained,"Esegui", "S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_43 as StdButton with uid="JRDPRFJHDB",left=583, top=339, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 184188346;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESSUP_1_56 as StdField with uid="XTUQOXXACB",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESSUP", cQueryName = "DESSUP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 96578506,;
   bGlobalFont=.t.,;
    Height=21, Width=376, Left=254, Top=202, InputMask=replicate('X',40)

  add object oDESCON_1_58 as StdField with uid="OOUDAPDZFQ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 137472970,;
   bGlobalFont=.t.,;
    Height=21, Width=376, Left=254, Top=230, InputMask=replicate('X',35)

  func oDESCON_1_58.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='G')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="SYOTLFXSQB",Visible=.t., Left=25, Top=288,;
    Alignment=1, Width=93, Height=15,;
    Caption="Stampa in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="BUKDPWVDMG",Visible=.t., Left=34, Top=65,;
    Alignment=1, Width=84, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="PYOVKAQWZL",Visible=.t., Left=39, Top=92,;
    Alignment=1, Width=79, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="YVBEGTRRYY",Visible=.t., Left=261, Top=286,;
    Alignment=1, Width=55, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_TIPVAL='C')
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="YFOGZKBWOY",Visible=.t., Left=423, Top=286,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL1<>0)
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="RTGZDIRCER",Visible=.t., Left=74, Top=261,;
    Alignment=1, Width=44, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="OYBJXJRMSL",Visible=.t., Left=16, Top=175,;
    Alignment=1, Width=102, Height=18,;
    Caption="Cau. Contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="XEDVCNYCOV",Visible=.t., Left=234, Top=261,;
    Alignment=1, Width=82, Height=15,;
    Caption="Sezione bil:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="VMDITUXAKY",Visible=.t., Left=66, Top=38,;
    Alignment=1, Width=52, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="WMPAYGEVJZ",Visible=.t., Left=27, Top=10,;
    Alignment=1, Width=91, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="EHYDVQMOWB",Visible=.t., Left=402, Top=10,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="ORMUKDZOZP",Visible=.t., Left=519, Top=10,;
    Alignment=1, Width=32, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="CVKIIGYPMO",Visible=.t., Left=15, Top=203,;
    Alignment=1, Width=103, Height=18,;
    Caption="Mastro contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="KPNJQFOXKW",Visible=.t., Left=6, Top=231,;
    Alignment=1, Width=112, Height=18,;
    Caption="Cat. contabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='G')
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="BRHUODDKLA",Visible=.t., Left=2, Top=311,;
    Alignment=1, Width=116, Height=18,;
    Caption="Stampa Controp.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="IBNXRMOAOU",Visible=.t., Left=3, Top=119,;
    Alignment=1, Width=115, Height=18,;
    Caption="Da rag.sociale:"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='G')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="QAKHWAGHPC",Visible=.t., Left=3, Top=146,;
    Alignment=1, Width=115, Height=18,;
    Caption="A rag.sociale:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='G')
    endwith
  endfunc

  add object oStr_1_63 as StdString with uid="DQRTVAWQQG",Visible=.t., Left=3, Top=119,;
    Alignment=1, Width=115, Height=18,;
    Caption="Da desc. conto:"  ;
  , bGlobalFont=.t.

  func oStr_1_63.mHide()
    with this.Parent.oContained
      return (.w_TIPCON#'G')
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="WGUWPMVXNK",Visible=.t., Left=3, Top=146,;
    Alignment=1, Width=115, Height=18,;
    Caption="A desc. conto:"  ;
  , bGlobalFont=.t.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (.w_TIPCON#'G')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sma','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
