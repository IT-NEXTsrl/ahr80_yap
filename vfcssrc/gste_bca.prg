* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bca                                                        *
*              Analisi scaduto per partita                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_876]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-02                                                      *
* Last revis.: 2017-01-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bca",oParentObject)
return(i_retval)

define class tgste_bca as StdBatch
  * --- Local variables
  w_FLSOS = space(1)
  w_CAUINS = space(5)
  w_GIORIS = 0
  w_CAODES = 0
  w_MESS = space(18)
  w_ERROR = .f.
  w_CODVAL = space(3)
  w_TIPRECO = space(1)
  w_TOTIMP = 0
  w_CSERIAL = space(10)
  w_CROWORD = 0
  w_CROWNUM = 0
  w_CONTA = 0
  w_TOT = 0
  w_SSERIAL = space(10)
  w_SROWORD = 0
  w_SROWNUM = 0
  w_DATST2 = ctod("  /  /  ")
  w_LTIPCON = space(1)
  w_LCODCON = space(15)
  w_LDATSCA = ctod("  /  /  ")
  w_LNUMPAR = space(14)
  w_LCODVAL = space(3)
  w_CODEUR = space(3)
  w_DATA = ctod("  /  /  ")
  w_SERIAL = space(10)
  w_ORDINE = 0
  * --- WorkFile variables
  CONTROPA_idx=0
  AZIENDA_idx=0
  TMP_PART_APE_idx=0
  TMP_PART_idx=0
  TMP_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora stampa Partite/Scadenze (da GSTE_SCA)
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZGIORIS"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZGIORIS;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_GIORIS = NVL(cp_ToDate(_read_.AZGIORIS),cp_NullValue(_read_.AZGIORIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Ordine di Stampa
    this.oParentObject.w_FLGSOS = IIF(this.oParentObject.w_FLSOSP $ "TN", " ", "S")
    this.oParentObject.w_FLGNSO = IIF(this.oParentObject.w_FLSOSP $ "TS", " ", "S")
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCAUINS"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCAUINS;
        from (i_cTable) where;
            COCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUINS = NVL(cp_ToDate(_read_.COCAUINS),cp_NullValue(_read_.COCAUINS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura Partite 
    ah_Msg("Recupero insoluti...",.T.)
    if g_SOLL="S" AND "SOLL" $ UPPER(i_cModules) 
      * --- Se Esiste il Contenzioso legge le partite riportate negli Insoluti
      *     Batch per lettura da archivio persente solo nel modulo insoluti...
      do GSSO_BCI with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Create temporary table TMP_PART_APE
      i_nIdx=cp_AddTableDef('TMP_PART_APE') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSTEISCA',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_PART_APE_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Altrimenti legge le Partite associate a Registrazioni Contabili con Causale associata ad un Insoluto.
      * --- Create temporary table TMP_PART_APE
      i_nIdx=cp_AddTableDef('TMP_PART_APE') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('query\GSTEPSCA',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMP_PART_APE_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Recupero tutte le partite di base per l'elaborazione, tutte le query
    *     si basano su questo temporaneo...
    ah_Msg("Recupero partite/scadenze base per elaborazione...",.T.)
    * --- Create temporary table TMP_MAST
    i_nIdx=cp_AddTableDef('TMP_MAST') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSTE_SCA.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_MAST_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Fase 1 - Determino la somma delle partite di creazione escluso insoluti
    *     
    *     (GLI INSOLUTI POSSONO ESSERE ESCLUSI FIN DALLA PRIMA QUERY ?)
    ah_Msg("Recupero record di testata...",.T.)
    * --- Create temporary table TMP_PART
    i_nIdx=cp_AddTableDef('TMP_PART') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSTE0SCA',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_PART_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Elimino le scadenze raggruppate che sono gia presenti fra quelle di creazione
    *     come non raggruppate....
    ah_Msg("Elimino scadenze raggruppate gia presenti...",.T.)
    * --- Delete from TMP_PART
    i_nConn=i_TableProp[this.TMP_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
            +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
            +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
            +" and "+i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
            +" and "+i_cTable+".FLRAGG = "+i_cQueryTable+".FLRAGG";
    
      do vq_exec with 'QUERY\GSTESCA_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Storno dalle partite di creazione l'importo delle partite di saldo che le
    *     puntano tramite PTSERRIF....
    ah_Msg("Storno partite di creazione...",.T.)
    * --- Write into TMP_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'QUERY\GSTESCA_2',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_MAST.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_MAST.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_MAST.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTIMP = TMP_MAST.TOTIMP-_t2.IMPORTO";
          +i_ccchkf;
          +" from "+i_cTable+" TMP_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_MAST.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_MAST.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_MAST.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_MAST, "+i_cQueryTable+" _t2 set ";
          +"TMP_MAST.TOTIMP = TMP_MAST.TOTIMP-_t2.IMPORTO";
          +Iif(Empty(i_ccchkf),"",",TMP_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_MAST.PTSERIAL = t2.PTSERIAL";
              +" and "+"TMP_MAST.PTROWORD = t2.PTROWORD";
              +" and "+"TMP_MAST.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_MAST set (";
          +"TOTIMP";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"TOTIMP-t2.IMPORTO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_MAST.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_MAST.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_MAST.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_MAST set ";
          +"TOTIMP = TMP_MAST.TOTIMP-_t2.IMPORTO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTIMP = (select "+i_cTable+".TOTIMP-IMPORTO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Scrivo il campo CONTA nella TMP_MAST per marcare le partite di saldo senza i riferimenti.
    *     Questo nel caso di una creazione con due saldi di partite identiche.
    * --- Write into TMP_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'QUERY\GSTESCA_16',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_MAST.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_MAST.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_MAST.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CONTA = _t2.CONTA";
          +i_ccchkf;
          +" from "+i_cTable+" TMP_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_MAST.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_MAST.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_MAST.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_MAST, "+i_cQueryTable+" _t2 set ";
          +"TMP_MAST.CONTA = _t2.CONTA";
          +Iif(Empty(i_ccchkf),"",",TMP_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_MAST.PTSERIAL = t2.PTSERIAL";
              +" and "+"TMP_MAST.PTROWORD = t2.PTROWORD";
              +" and "+"TMP_MAST.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_MAST set (";
          +"CONTA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.CONTA";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_MAST.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_MAST.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_MAST.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_MAST set ";
          +"CONTA = _t2.CONTA";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CONTA = (select CONTA from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='ERRORE GSTESCA_16'
      return
    endif
    * --- Storno dalle partite di creazione l'importo delle partite di saldo che le
    *     non hanno PTSERRIF quindi in base a DATSCA,CODCON,TIPCON...
    * --- Write into TMP_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="TIPCON,CODCON,CODVAL,NUMPAR,DATSCA,FLCRSA"
      do vq_exec with 'QUERY\GSTESCA_3',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_MAST.TIPCON = _t2.TIPCON";
              +" and "+"TMP_MAST.CODCON = _t2.CODCON";
              +" and "+"TMP_MAST.CODVAL = _t2.CODVAL";
              +" and "+"TMP_MAST.NUMPAR = _t2.NUMPAR";
              +" and "+"TMP_MAST.DATSCA = _t2.DATSCA";
              +" and "+"TMP_MAST.FLCRSA = _t2.FLCRSA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTIMP = TMP_MAST.TOTIMP-_t2.IMPORTO";
          +i_ccchkf;
          +" from "+i_cTable+" TMP_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_MAST.TIPCON = _t2.TIPCON";
              +" and "+"TMP_MAST.CODCON = _t2.CODCON";
              +" and "+"TMP_MAST.CODVAL = _t2.CODVAL";
              +" and "+"TMP_MAST.NUMPAR = _t2.NUMPAR";
              +" and "+"TMP_MAST.DATSCA = _t2.DATSCA";
              +" and "+"TMP_MAST.FLCRSA = _t2.FLCRSA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_MAST, "+i_cQueryTable+" _t2 set ";
          +"TMP_MAST.TOTIMP = TMP_MAST.TOTIMP-_t2.IMPORTO";
          +Iif(Empty(i_ccchkf),"",",TMP_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_MAST.TIPCON = t2.TIPCON";
              +" and "+"TMP_MAST.CODCON = t2.CODCON";
              +" and "+"TMP_MAST.CODVAL = t2.CODVAL";
              +" and "+"TMP_MAST.NUMPAR = t2.NUMPAR";
              +" and "+"TMP_MAST.DATSCA = t2.DATSCA";
              +" and "+"TMP_MAST.FLCRSA = t2.FLCRSA";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_MAST set (";
          +"TOTIMP";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"TOTIMP-t2.IMPORTO";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_MAST.TIPCON = _t2.TIPCON";
              +" and "+"TMP_MAST.CODCON = _t2.CODCON";
              +" and "+"TMP_MAST.CODVAL = _t2.CODVAL";
              +" and "+"TMP_MAST.NUMPAR = _t2.NUMPAR";
              +" and "+"TMP_MAST.DATSCA = _t2.DATSCA";
              +" and "+"TMP_MAST.FLCRSA = _t2.FLCRSA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_MAST set ";
          +"TOTIMP = TMP_MAST.TOTIMP-_t2.IMPORTO";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
              +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
              +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
              +" and "+i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
              +" and "+i_cTable+".DATSCA = "+i_cQueryTable+".DATSCA";
              +" and "+i_cTable+".FLCRSA = "+i_cQueryTable+".FLCRSA";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTIMP = (select "+i_cTable+".TOTIMP-IMPORTO from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='ERRORE GSTESCA_3'
      return
    endif
    * --- Seleziono le partite di creazione con CONTA >1
    * --- Select from TMP_MAST
    i_nConn=i_TableProp[this.TMP_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAST_idx,2],.t.,this.TMP_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP_MAST ";
          +" where CONTA>1 AND FLCRSA='C' AND TOTIMP<>0";
           ,"_Curs_TMP_MAST")
    else
      select * from (i_cTable);
       where CONTA>1 AND FLCRSA="C" AND TOTIMP<>0;
        into cursor _Curs_TMP_MAST
    endif
    if used('_Curs_TMP_MAST')
      select _Curs_TMP_MAST
      locate for 1=1
      do while not(eof())
      this.w_TOTIMP = _Curs_TMP_MAST.TOTIMP
      this.w_CONTA = _Curs_TMP_MAST.CONTA
      this.w_CSERIAL = _Curs_TMP_MAST.PTSERIAL
      this.w_CROWORD = _Curs_TMP_MAST.PTROWORD
      this.w_CROWNUM = _Curs_TMP_MAST.CPROWNUM
      do while this.w_TOTIMP<>0
        * --- Leggo le partite di saldo con CONTA del saldo uguale al conta della Creazione
        * --- Read from TMP_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TMP_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAST_idx,2],.t.,this.TMP_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PTSERIAL,PTROWORD,CPROWNUM,TOTIMP"+;
            " from "+i_cTable+" TMP_MAST where ";
                +"NUMPAR = "+cp_ToStrODBC(_Curs_TMP_MAST.NUMPAR);
                +" and DATSCA = "+cp_ToStrODBC(_Curs_TMP_MAST.DATSCA);
                +" and CODCON = "+cp_ToStrODBC(_Curs_TMP_MAST.CODCON);
                +" and CONTA = "+cp_ToStrODBC(this.w_CONTA);
                +" and FLCRSA = "+cp_ToStrODBC("S");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PTSERIAL,PTROWORD,CPROWNUM,TOTIMP;
            from (i_cTable) where;
                NUMPAR = _Curs_TMP_MAST.NUMPAR;
                and DATSCA = _Curs_TMP_MAST.DATSCA;
                and CODCON = _Curs_TMP_MAST.CODCON;
                and CONTA = this.w_CONTA;
                and FLCRSA = "S";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SSERIAL = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
          this.w_SROWORD = NVL(cp_ToDate(_read_.PTROWORD),cp_NullValue(_read_.PTROWORD))
          this.w_SROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
          this.w_TOT = NVL(cp_ToDate(_read_.TOTIMP),cp_NullValue(_read_.TOTIMP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if I_ROWS<>0
          * --- Se trovo almeno un record, vado a scrivere CONTA=-1 sulle partite di saldo
          *      e vado a stornare in quelle di creazione l'importo.
          * --- Write into TMP_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMP_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CONTA ="+cp_NullLink(cp_ToStrODBC(-1),'TMP_MAST','CONTA');
                +i_ccchkf ;
            +" where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_SSERIAL);
                +" and PTROWORD = "+cp_ToStrODBC(this.w_SROWORD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_SROWNUM);
                   )
          else
            update (i_cTable) set;
                CONTA = -1;
                &i_ccchkf. ;
             where;
                PTSERIAL = this.w_SSERIAL;
                and PTROWORD = this.w_SROWORD;
                and CPROWNUM = this.w_SROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into TMP_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMP_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TOTIMP =TOTIMP- "+cp_ToStrODBC(_Curs_TMP_MAST.TOTIMP);
                +i_ccchkf ;
            +" where ";
                +"PTSERIAL = "+cp_ToStrODBC(this.w_CSERIAL);
                +" and PTROWORD = "+cp_ToStrODBC(this.w_CROWORD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CROWNUM);
                   )
          else
            update (i_cTable) set;
                TOTIMP = TOTIMP - _Curs_TMP_MAST.TOTIMP;
                &i_ccchkf. ;
             where;
                PTSERIAL = this.w_CSERIAL;
                and PTROWORD = this.w_CROWORD;
                and CPROWNUM = this.w_CROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          this.w_TOTIMP = 0
        endif
      enddo
        select _Curs_TMP_MAST
        continue
      enddo
      use
    endif
    * --- Occorre gestire il caso in cui il totale delle partite di saldo supera l'importo della
    *     partita di creazione (occorre una WRITE ulteriore ).
    *     Uno per gli importi in dare negativi ed uno per gli importi negativi
    *     in avere
    * --- Write into TMP_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'QUERY\GSTESCA_11',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_MAST.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_MAST.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_MAST.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FLDAVE = _t2.FLDAVE";
          +",TOTIMP = _t2.TOTIMP*-1";
          +i_ccchkf;
          +" from "+i_cTable+" TMP_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_MAST.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_MAST.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_MAST.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_MAST, "+i_cQueryTable+" _t2 set ";
          +"TMP_MAST.FLDAVE = _t2.FLDAVE";
          +",TMP_MAST.TOTIMP = _t2.TOTIMP*-1";
          +Iif(Empty(i_ccchkf),"",",TMP_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_MAST.PTSERIAL = t2.PTSERIAL";
              +" and "+"TMP_MAST.PTROWORD = t2.PTROWORD";
              +" and "+"TMP_MAST.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_MAST set (";
          +"FLDAVE,";
          +"TOTIMP";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.FLDAVE,";
          +"t2.TOTIMP*-1";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_MAST.PTSERIAL = _t2.PTSERIAL";
              +" and "+"TMP_MAST.PTROWORD = _t2.PTROWORD";
              +" and "+"TMP_MAST.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_MAST set ";
          +"FLDAVE = _t2.FLDAVE";
          +",TOTIMP = _t2.TOTIMP*-1";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FLDAVE = (select FLDAVE from "+i_cQueryTable+" where "+i_cWhere+")";
          +",TOTIMP = (select TOTIMP*-1 from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Elimino tutte le partite di acconto che puntano una partita di creazione ad
    *      importo zero..
    ah_Msg("Elimino partite di acconto che saldano...",.T.)
    * --- Delete from TMP_MAST
    i_nConn=i_TableProp[this.TMP_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
            +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
      do vq_exec with 'QUERY\GSTESCA_4',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Fase 2 - Determino l'elenco delle scadenze aperte...
    *     Interrogo l'archvio cercando le scadenze di tipo creazione.
    ah_Msg("Inserisco partite di saldo...",.T.)
    * --- Insert into TMP_PART
    i_nConn=i_TableProp[this.TMP_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSTESCA_5",this.TMP_PART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Fase 3 - Determino l'elenco delle scadenze di saldo
    * --- Origine della Partita:
    *     C = Proveniente da Contabilizzazione Indiretta (RIFDIS <0)
    *     R = Raggruppata (FLRAGG=S)
    *     D = Proveniente da Contabilizzazione Distinte (RIFDIS >0)
    ah_Msg("Inserisco partite di saldo...",.T.)
    * --- Insert into TMP_PART
    i_nConn=i_TableProp[this.TMP_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSTESCA_6",this.TMP_PART_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Azzero IMPESP anche per le scadenze da distinta o contabilizzazione
    *     indiretta che hanno data scadenza..
    if this.oParentObject.w_TIPCON = "C"
      this.w_DATST2 = this.oParentObject.w_DATSTA - this.w_GIORIS
    else
      this.w_DATST2 = this.oParentObject.w_DATSTA 
    endif
    ah_Msg("Determino esposizione effetti...",.T.)
    * --- Write into TMP_PART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMP_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="SERIAL,ROWORD,NUMROW,TIPREC"
      do vq_exec with 'QUERY\GSTESCA_7',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMP_PART.SERIAL = _t2.SERIAL";
              +" and "+"TMP_PART.ROWORD = _t2.ROWORD";
              +" and "+"TMP_PART.NUMROW = _t2.NUMROW";
              +" and "+"TMP_PART.TIPREC = _t2.TIPREC";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IMPESP = _t2.TOTIMP";
          +i_ccchkf;
          +" from "+i_cTable+" TMP_PART, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMP_PART.SERIAL = _t2.SERIAL";
              +" and "+"TMP_PART.ROWORD = _t2.ROWORD";
              +" and "+"TMP_PART.NUMROW = _t2.NUMROW";
              +" and "+"TMP_PART.TIPREC = _t2.TIPREC";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART, "+i_cQueryTable+" _t2 set ";
          +"TMP_PART.IMPESP = _t2.TOTIMP";
          +Iif(Empty(i_ccchkf),"",",TMP_PART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMP_PART.SERIAL = t2.SERIAL";
              +" and "+"TMP_PART.ROWORD = t2.ROWORD";
              +" and "+"TMP_PART.NUMROW = t2.NUMROW";
              +" and "+"TMP_PART.TIPREC = t2.TIPREC";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART set (";
          +"IMPESP";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.TOTIMP";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMP_PART.SERIAL = _t2.SERIAL";
              +" and "+"TMP_PART.ROWORD = _t2.ROWORD";
              +" and "+"TMP_PART.NUMROW = _t2.NUMROW";
              +" and "+"TMP_PART.TIPREC = _t2.TIPREC";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART set ";
          +"IMPESP = _t2.TOTIMP";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".SERIAL = "+i_cQueryTable+".SERIAL";
              +" and "+i_cTable+".ROWORD = "+i_cQueryTable+".ROWORD";
              +" and "+i_cTable+".NUMROW = "+i_cQueryTable+".NUMROW";
              +" and "+i_cTable+".TIPREC = "+i_cQueryTable+".TIPREC";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IMPESP = (select TOTIMP from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Azzero IMPESP per tutte le scadenze, in base a TIPCON,CODCON,CODVAL, per
    *     le quali vi � un'insoluto in prima nota. 
    ah_Msg("Sbianco importi insoluti...",.T.)
    * --- Utilizzo la select per recuperare i riferimenti delle partite riaperte 
    *     con un insoluto e successivamente inserite in distinta o Indiretta effetti
    * --- Select from GSTERSCA
    do vq_exec with 'GSTERSCA',this,'_Curs_GSTERSCA','',.f.,.t.
    if used('_Curs_GSTERSCA')
      select _Curs_GSTERSCA
      locate for 1=1
      do while not(eof())
      this.w_LTIPCON = _Curs_GSTERSCA.TIPCON
      this.w_LCODCON = _Curs_GSTERSCA.CODCON
      this.w_LDATSCA = _Curs_GSTERSCA.DATSCA
      this.w_LNUMPAR = _Curs_GSTERSCA.NUMPAR
      this.w_LCODVAL = _Curs_GSTERSCA.CODVAL
      * --- Ordino le scadenze di saldo per seriale, se sono distinte (IMPESP<>0)
      *     le azzerro tranne l'ultima se ha seriale pi� alta del seriale pi� alto della
      *     contabilizzazione insoluto..
      * --- Select from TMP_MAST
      i_nConn=i_TableProp[this.TMP_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAST_idx,2],.t.,this.TMP_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PTSERIAL , PTROWORD , CPROWNUM  from "+i_cTable+" TMP_MAST ";
            +" where PTROWORD>0 And DATSCA="+cp_ToStrODBC(this.w_LDATSCA)+" And NUMPAR="+cp_ToStrODBC(this.w_LNUMPAR)+" And TIPCON="+cp_ToStrODBC(this.w_LTIPCON)+" And CODCON="+cp_ToStrODBC(this.w_LCODCON)+" And CODVAL="+cp_ToStrODBC(this.w_LCODVAL)+" AND FLCRSA='S' AND RIFDIS<>' '   ";
            +" order by PTSERIAL";
             ,"_Curs_TMP_MAST")
      else
        select PTSERIAL , PTROWORD , CPROWNUM from (i_cTable);
         where PTROWORD>0 And DATSCA=this.w_LDATSCA And NUMPAR=this.w_LNUMPAR And TIPCON=this.w_LTIPCON And CODCON=this.w_LCODCON And CODVAL=this.w_LCODVAL AND FLCRSA="S" AND RIFDIS<>" "   ;
         order by PTSERIAL;
          into cursor _Curs_TMP_MAST
      endif
      if used('_Curs_TMP_MAST')
        select _Curs_TMP_MAST
        locate for 1=1
        do while not(eof())
        if RecNo()<>RecCount() Or _Curs_GSTERSCA.SERIAL>= Nvl( _Curs_TMP_MAST.PTSERIAL , "0000000000" )
          * --- Write into TMP_PART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMP_PART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IMPESP ="+cp_NullLink(cp_ToStrODBC(0),'TMP_PART','IMPESP');
            +",TIPSCA ="+cp_NullLink(cp_ToStrODBC("P"),'TMP_PART','TIPSCA');
                +i_ccchkf ;
            +" where ";
                +"SERIAL = "+cp_ToStrODBC(_Curs_TMP_MAST.PTSERIAL);
                +" and ROWORD = "+cp_ToStrODBC(_Curs_TMP_MAST.PTROWORD);
                +" and NUMROW = "+cp_ToStrODBC(_Curs_TMP_MAST.CPROWNUM);
                   )
          else
            update (i_cTable) set;
                IMPESP = 0;
                ,TIPSCA = "P";
                &i_ccchkf. ;
             where;
                SERIAL = _Curs_TMP_MAST.PTSERIAL;
                and ROWORD = _Curs_TMP_MAST.PTROWORD;
                and NUMROW = _Curs_TMP_MAST.CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
          select _Curs_TMP_MAST
          continue
        enddo
        use
      endif
        select _Curs_GSTERSCA
        continue
      enddo
      use
    endif
    * --- Modifico il SERIAL delle partite in distinta raggruppate (scadenze raggruppate finite in distinta) recuperando
    *     quello della partite raggruppata di partenza (C)
    if this.oParentObject.w_FLORIG="S"
      ah_Msg("Gestione serial raggruppate...",.T.)
      * --- Write into TMP_PART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMP_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SERIAL,ROWORD,NUMROW,TIPREC"
        do vq_exec with 'QUERY\GSTESCA_8',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMP_PART.SERIAL = _t2.SERIAL";
                +" and "+"TMP_PART.ROWORD = _t2.ROWORD";
                +" and "+"TMP_PART.NUMROW = _t2.NUMROW";
                +" and "+"TMP_PART.TIPREC = _t2.TIPREC";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SERIAL = _t2.SER2";
        +",ROWORD ="+cp_NullLink(cp_ToStrODBC(-1),'TMP_PART','ROWORD');
            +i_ccchkf;
            +" from "+i_cTable+" TMP_PART, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMP_PART.SERIAL = _t2.SERIAL";
                +" and "+"TMP_PART.ROWORD = _t2.ROWORD";
                +" and "+"TMP_PART.NUMROW = _t2.NUMROW";
                +" and "+"TMP_PART.TIPREC = _t2.TIPREC";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART, "+i_cQueryTable+" _t2 set ";
            +"TMP_PART.SERIAL = _t2.SER2";
        +",TMP_PART.ROWORD ="+cp_NullLink(cp_ToStrODBC(-1),'TMP_PART','ROWORD');
            +Iif(Empty(i_ccchkf),"",",TMP_PART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMP_PART.SERIAL = t2.SERIAL";
                +" and "+"TMP_PART.ROWORD = t2.ROWORD";
                +" and "+"TMP_PART.NUMROW = t2.NUMROW";
                +" and "+"TMP_PART.TIPREC = t2.TIPREC";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART set (";
            +"SERIAL,";
            +"ROWORD";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.SER2,";
            +cp_NullLink(cp_ToStrODBC(-1),'TMP_PART','ROWORD')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMP_PART.SERIAL = _t2.SERIAL";
                +" and "+"TMP_PART.ROWORD = _t2.ROWORD";
                +" and "+"TMP_PART.NUMROW = _t2.NUMROW";
                +" and "+"TMP_PART.TIPREC = _t2.TIPREC";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMP_PART set ";
            +"SERIAL = _t2.SER2";
        +",ROWORD ="+cp_NullLink(cp_ToStrODBC(-1),'TMP_PART','ROWORD');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SERIAL = "+i_cQueryTable+".SERIAL";
                +" and "+i_cTable+".ROWORD = "+i_cQueryTable+".ROWORD";
                +" and "+i_cTable+".NUMROW = "+i_cQueryTable+".NUMROW";
                +" and "+i_cTable+".TIPREC = "+i_cQueryTable+".TIPREC";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SERIAL = (select SER2 from "+i_cQueryTable+" where "+i_cWhere+")";
        +",ROWORD ="+cp_NullLink(cp_ToStrODBC(-1),'TMP_PART','ROWORD');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Per capire se una partita � chiusa, la somma degli importi vari deve essere 0.
    *     Se 0, vengono escluse dal cursore definitivo
    ah_Msg("Elimino scadenze a 0...",.T.)
    * --- Delete from TMP_PART
    i_nConn=i_TableProp[this.TMP_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
            +" and "+i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
            +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
            +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
    
      do vq_exec with 'QUERY\GSTESCA_9',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Questa ulteriore select serve per andare a intercettare le partite con valuta extra emu.
    *     Per le colonne in scadenza e scaduto, vado a ricalcolare il totale aggiornato al cambio giornaliero, 
    *     mentre per le altre uso il cambio di origine.
    CREATE CURSOR MessErr (COD C(3), MSG C(120))
    this.w_ERROR = .F.
    this.w_CODEUR = g_PERVAL
    ah_Msg("Check cambi valuta extra Euro...",.T.)
    * --- Select from TMP_PART
    i_nConn=i_TableProp[this.TMP_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2],.t.,this.TMP_PART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CODVAL, CAOVAL, Min(TIPREC) As TIPREC  from "+i_cTable+" TMP_PART ";
          +" where TIPREC<>'A' And CODVAL<>"+cp_ToStrODBC(this.w_CODEUR)+"";
          +" group by CODVAL, CAOVAL";
           ,"_Curs_TMP_PART")
    else
      select CODVAL, CAOVAL, Min(TIPREC) As TIPREC from (i_cTable);
       where TIPREC<>"A" And CODVAL<>this.w_CODEUR;
       group by CODVAL, CAOVAL;
        into cursor _Curs_TMP_PART
    endif
    if used('_Curs_TMP_PART')
      select _Curs_TMP_PART
      locate for 1=1
      do while not(eof())
      this.w_CAODES = GETCAM(NVL(CODVAL, SPACE(5)),this.oParentObject.w_DATSTA, 0)
      if this.w_CAODES<>NVL(_Curs_TMP_PART.CAOVAL, 0)
        this.w_CODVAL = NVL(CODVAL, SPACE(3))
        this.w_TIPRECO = TIPREC
        this.w_DATA = IIF(EMPTY(g_APPDAT), this.oParentObject.w_DATSTA, g_APPDAT)
        do case
          case this.w_CAODES=0
            this.w_CAODES = 1
            this.w_ERROR = .T.
             
 Select MessErr 
 INSERT INTO MessErr (COD, MSG) VALUES (this.w_CODVAL, ah_Msgformat("Manca il cambio per %1",this.w_CODVAL) )
          case (this.oParentObject.w_DATSTA-7)>this.w_DATA
            this.w_ERROR = .T.
             
 Select MessErr 
 INSERT INTO MessErr (COD, MSG) VALUES (this.w_CODVAL, ah_Msgformat("Cambio %1 non aggiornato dal %2", this.w_CODVAL, DTOC(this.w_DATA)) )
        endcase
        if this.w_CAODES<>0 AND this.w_TIPRECO="B"
          * --- Write into TMP_PART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMP_PART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMP_PART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_CAODES),'TMP_PART','CAOVAL');
                +i_ccchkf ;
            +" where ";
                +"CODVAL = "+cp_ToStrODBC(_Curs_TMP_PART.CODVAL);
                +" and CAOVAL = "+cp_ToStrODBC(_Curs_TMP_PART.CAOVAL);
                +" and TIPREC = "+cp_ToStrODBC("B");
                   )
          else
            update (i_cTable) set;
                CAOVAL = this.w_CAODES;
                &i_ccchkf. ;
             where;
                CODVAL = _Curs_TMP_PART.CODVAL;
                and CAOVAL = _Curs_TMP_PART.CAOVAL;
                and TIPREC = "B";

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
        select _Curs_TMP_PART
        continue
      enddo
      use
    endif
    if this.w_ERROR
      ah_ErrorMsg("Nell'elaborazione sono presenti cambi inesistenti o non aggiornati da pi� di 7 giorni%0Al termine della stampa estratto conto verr� eseguito un report della situazione",,"")
    endif
    * --- Passo le variabili al report
    if this.oParentObject.w_FLORIG="S"
      ah_Msg("Ricerca partite di origine...",.T.)
      * --- Determino le partite di origine delle raggruppate...
      *     
      *     Verifico se ho partite di tipo diverso da 'A' con flag raggruppata a 'S'
      *     Se ne trovo accodo al temporaneo le loro origini con un ordine maggiorato di 1.
      *     
      *     A T T E N Z I O N E 
      *     ================
      *     La stampa recupera solo le prime partite ragruppate non itera su queste.
      *     Per farlo occorre utilizzare l'algoritmo presente in GSTE_BPS (Stampa Partite Scadenze)
      * --- Passato alla query per identificare le scadenze raggruppate (w_ORDINE=2)
      this.w_ORDINE = 1
      * --- Read from TMP_PART
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TMP_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2],.t.,this.TMP_PART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SERIAL"+;
          " from "+i_cTable+" TMP_PART where ";
              +"FLRAGG = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SERIAL;
          from (i_cTable) where;
              FLRAGG = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SERIAL = NVL(cp_ToDate(_read_.SERIAL),cp_NullValue(_read_.SERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0
        * --- Insert into TMP_PART
        i_nConn=i_TableProp[this.TMP_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSTESCA_12",this.TMP_PART_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    endif
    * --- Elimino le partite con tiprec='A' rimaste appese dopo aver fatto un acconto che chiude totalmente una fattura
    * --- Delete from TMP_PART
    i_nConn=i_TableProp[this.TMP_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
            +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
            +" and "+i_cTable+".CODVAL = "+i_cQueryTable+".CODVAL";
            +" and "+i_cTable+".NUMPAR = "+i_cQueryTable+".NUMPAR";
            +" and "+i_cTable+".FLRAGG = "+i_cQueryTable+".FLRAGG";
    
      do vq_exec with 'QUERY\GSTESCA_17',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    ah_Msg("Avvio recupero dati per stampa...",.T.)
    * --- Lancio la stampa
    L_SOSP=this.oParentObject.w_FLSOSP
    L_NUMPAR=this.oParentObject.w_NUMPAR
    L_ADOINI=this.oParentObject.w_ADOFIN
    L_ADOFIN=this.oParentObject.w_ADOFIN
    L_NDOINI=this.oParentObject.w_NDOINI
    L_SCAINI=this.oParentObject.w_SCAINI
    L_NDOFIN=this.oParentObject.w_NDOFIN
    L_SCAFIN=this.oParentObject.w_SCAFIN
    L_DDOINI=this.oParentObject.w_DDOINI
    L_BANNOS=this.oParentObject.w_BANNOS
    L_DDOFIN=this.oParentObject.w_DDOFIN
    L_VALUTA=this.oParentObject.w_VALUTA
    L_AGENTE=this.oParentObject.w_AGENTE
    L_BANAPP=this.oParentObject.w_BANAPP
    L_FORSEL=IIF(this.oParentObject.w_TIPCON="F", this.oParentObject.w_CODCON, " ")
    L_FORSEL1=IIF(this.oParentObject.w_TIPCON="F", this.oParentObject.w_CODFIN, " ")
    L_CLISEL=IIF(this.oParentObject.w_TIPCON="C", this.oParentObject.w_CODCON, " ")
    L_CLISEL1=IIF(this.oParentObject.w_TIPCON="C", this.oParentObject.w_CODFIN, " ")
    L_SCACLF=this.oParentObject.w_TIPCON
    L_DATSTA=this.oParentObject.w_DATSTA
    L_FLDESC=this.oParentObject.w_FLDESC
    L_FLORIG=this.oParentObject.w_FLORIG
    L_CAODES=this.w_CAODES
    * --- Seleziono il bitmap da stampare
    L_LOGO=GETBMP(I_CODAZI)
    vx_exec(""+alltrim(this.oParentObject.w_OQRY)+", "+alltrim(this.oParentObject.w_orep)+"",this)
    if this.w_ERROR
      if ah_YesNo("Stampo riepilogo errori?")
        SELECT MAX(MSG) AS MSG FROM MessErr GROUP BY COD INTO CURSOR __TMP__
        CP_CHPRN("QUERY\GSVE_BCV.FRX", " ", this)
      endif
    endif
    * --- Drop temporary table TMP_PART_APE
    i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART_APE')
    endif
    * --- Drop temporary table TMP_PART
    i_nIdx=cp_GetTableDefIdx('TMP_PART')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART')
    endif
    * --- Drop temporary table TMP_MAST
    i_nIdx=cp_GetTableDefIdx('TMP_MAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_MAST')
    endif
    if USED("MessErr")
       
 SELECT MessErr 
 USe
    endif
    if USED("__TMP__")
       
 SELECT __TMP__ 
 Use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='*TMP_PART_APE'
    this.cWorkTables[4]='*TMP_PART'
    this.cWorkTables[5]='*TMP_MAST'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_TMP_MAST')
      use in _Curs_TMP_MAST
    endif
    if used('_Curs_GSTERSCA')
      use in _Curs_GSTERSCA
    endif
    if used('_Curs_TMP_MAST')
      use in _Curs_TMP_MAST
    endif
    if used('_Curs_TMP_PART')
      use in _Curs_TMP_PART
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
