* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_krb                                                        *
*              Elenco voci                                                     *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_33]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-18                                                      *
* Last revis.: 2005-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_krb",oParentObject))

* --- Class definition
define class tgscg_krb as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 736
  Height = 358
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2005-09-13"
  HelpContextID=116008297
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_krb"
  cComment = "Elenco voci"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_VBROWORD = 0
  w_VBCODVOC = space(15)
  w_AGG_ZOOM = space(20)
  w_DVFLMACO = space(1)
  w_CODICE = space(15)
  w_Zoom = .NULL.
  w_ZoomD = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_krbPag1","gscg_krb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    this.w_ZoomD = this.oPgFrm.Pages(1).oPag.ZoomD
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      this.w_ZoomD = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VBROWORD=0
      .w_VBCODVOC=space(15)
      .w_AGG_ZOOM=space(20)
      .w_DVFLMACO=space(1)
      .w_CODICE=space(15)
      .w_VBROWORD=oParentObject.w_VBROWORD
      .w_VBCODVOC=oParentObject.w_VBCODVOC
      .oPgFrm.Page1.oPag.Zoom.Calculate()
        .w_VBROWORD = .w_Zoom.GetVar( 'CPROWNUM' )
        .w_VBCODVOC = .w_Zoom.GetVar( 'TRCODICE' )
        .w_AGG_ZOOM = strtran(Str( .w_VBROWORD) ,' ','@') + .w_VBCODVOC
      .oPgFrm.Page1.oPag.ZoomD.Calculate(.w_AGG_ZOOM)
        .w_DVFLMACO = .w_ZoomD.GetVar( 'FLMACO' )
        .w_CODICE = NVL(.w_ZoomD.GetVar( 'CODICE' ) , SPACE(15))
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_VBROWORD=.w_VBROWORD
      .oParentObject.w_VBCODVOC=.w_VBCODVOC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
            .w_VBROWORD = .w_Zoom.GetVar( 'CPROWNUM' )
            .w_VBCODVOC = .w_Zoom.GetVar( 'TRCODICE' )
            .w_AGG_ZOOM = strtran(Str( .w_VBROWORD) ,' ','@') + .w_VBCODVOC
        .oPgFrm.Page1.oPag.ZoomD.Calculate(.w_AGG_ZOOM)
            .w_DVFLMACO = .w_ZoomD.GetVar( 'FLMACO' )
            .w_CODICE = NVL(.w_ZoomD.GetVar( 'CODICE' ) , SPACE(15))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.ZoomD.Calculate(.w_AGG_ZOOM)
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
  return

  proc Calculate_WFIXWGHMXF()
    with this
          * --- Inizializza codice bilancio
          .w_VBCODVOC = IIF(  g_APPLICATION = "ADHOC REVOLUTION"  ,  'BILANCOGE' , .w_VBCODVOC )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomD.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_WFIXWGHMXF()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gscg_krb
    * appena l'utente seleziona un elemento chiudo la maschera..
    if inlist(cEvent,"w_zoom selected")
      this.EcpSave()
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_krbPag1 as StdContainer
  Width  = 732
  height = 358
  stdWidth  = 732
  stdheight = 358
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object Zoom as cp_zoombox with uid="BJTWWEVAVG",left=0, top=3, width=478,height=350,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="TIR_DETT",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cZoomFile="GSCG_KRB",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 61989350


  add object ZoomD as cp_zoombox with uid="GZQGIPITYJ",left=472, top=20, width=257,height=333,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="TIR_DETT",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cZoomFile="GSCG1KRB",cMenuFile="",cZoomOnZoom="GSZM_BRB",bRetriveAllRows=.f.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 61989350


  add object oObj_1_10 as cp_runprogram with uid="EVPVCDELGM",left=200, top=364, width=185,height=24,;
    caption='GSZM_BRB',;
   bGlobalFont=.t.,;
    prg="GSZM_BRB",;
    cEvent = "w_zoomd selected",;
    nPag=1;
    , HelpContextID = 22598312

  add object oStr_1_4 as StdString with uid="QMJWLMCRTT",Visible=.t., Left=482, Top=3,;
    Alignment=2, Width=239, Height=18,;
    Caption="Dettaglio voce"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_krb','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
