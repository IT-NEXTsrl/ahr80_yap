* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bak                                                        *
*              Lancia kit articoli/distinta                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-18                                                      *
* Last revis.: 2000-02-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bak",oParentObject,m.pTipo)
return(i_retval)

define class tgsar_bak as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_CODKIT = space(20)
  w_TIPKIT = space(2)
  w_PROG = space(8)
  w_DXBTN = .f.
  w_CODKIT = space(20)
  * --- WorkFile variables
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  DIS_COMP_idx=0
  DISMBASE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia l'anagrafica kit articoli o distinta da campo Distinta base kit su articoli
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      this.w_CODKIT = Nvl(g_oMenu.oKey(1,3),"")
      * --- Read from DISMBASE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DISMBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2],.t.,this.DISMBASE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DBDISKIT"+;
          " from "+i_cTable+" DISMBASE where ";
              +"DBCODICE = "+cp_ToStrODBC(this.w_CODKIT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DBDISKIT;
          from (i_cTable) where;
              DBCODICE = this.w_CODKIT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPKIT = NVL(cp_ToDate(_read_.DBDISKIT),cp_NullValue(_read_.DBDISKIT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      * --- Tipo Kit/Distinta
      this.w_CODKIT = &cCurs..DBCODICE
      do case
        case Upper(&cCurs..TIPO) = "KIT"
          this.w_TIPKIT = "K"
        case Upper(&cCurs..TIPO) = "DISTINTA"
          this.w_TIPKIT = "D"
        case Upper(&cCurs..TIPO) = "IMBALLO"
          this.w_TIPKIT = "I"
      endcase
    endif
    if NOT EMPTY(NVL(this.w_CODKIT," "))
      do case
        case this.w_TIPKIT="K"
          * --- Lancia Articoli Kit
          this.w_PROG = "GSAR_MAK"
        case this.w_TIPKIT="D"
          * --- Lancia Distinta Base
          this.w_PROG = "GSDS_MDB"
        case this.w_TIPKIT="I"
          * --- Lancia Kit Imballo
          this.w_PROG = "GSVA_MKI"
      endcase
    else
      if Type("pTipo") = "C"
        do case
          case this.pTipo = "D"
            * --- Lancia Distinta Base
            this.w_PROG = "GSDS_MDB"
          case this.pTipo = "K"
            * --- Lancia Articoli Kit
            this.w_PROG = "GSAR_MAK"
          case this.pTipo = "I"
            * --- Lancia Kit Imballo
            this.w_PROG = "GSVA_MKI"
        endcase
        * --- Indico che deve entrare nella gestione in caricamento
        g_oMenu.cBatchType = "L"
      else
        * --- Se lancio il carica normale lancia Articoli Kit
        this.w_PROG = "GSAR_MAK"
      endif
    endif
    * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
    *     presente nella variabile
    OpenGest(IIF(this.w_DXBTN,g_oMenu.cBatchType,"S"),this.w_PROG,"DBCODICE",this.w_CODKIT)
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='DIS_COMP'
    this.cWorkTables[4]='DISMBASE'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
