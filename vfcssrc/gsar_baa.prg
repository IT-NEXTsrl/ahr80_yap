* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_baa                                                        *
*              Dati reg.assestamento                                           *
*                                                                              *
*      Author: Zucchetti TAM S.p.a.                                            *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_66]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-07                                                      *
* Last revis.: 2006-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_baa",oParentObject,m.pParam)
return(i_retval)

define class tgsar_baa as StdBatch
  * --- Local variables
  pParam = space(4)
  * --- WorkFile variables
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Assegnazione e salvataggio DATI REG.ASSESTAMENTO
    do case
      case this.pParam="INIT"
        this.oParentObject.w_COCODAZI = i_CODAZI
        * --- Try
        local bErr_03844E28
        bErr_03844E28=bTrsErr
        this.Try_03844E28()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03844E28
        * --- End
      case this.pParam="OK"
        * --- Write into CONTROPA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTROPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTROPA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COFATEME ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COFATEME),'CONTROPA','COFATEME');
          +",COIVADEB ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COIVADEB),'CONTROPA','COIVADEB');
          +",COCAUFDE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCAUFDE),'CONTROPA','COCAUFDE');
          +",COCASFDE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCASFDE),'CONTROPA','COCASFDE');
          +",COFATRIC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COFATRIC),'CONTROPA','COFATRIC');
          +",COIVACRE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COIVACRE),'CONTROPA','COIVACRE');
          +",COCAUFDR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCAUFDR),'CONTROPA','COCAUFDR');
          +",COCASFDR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCASFDR),'CONTROPA','COCASFDR');
          +",COCAURIM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCAURIM),'CONTROPA','COCAURIM');
          +",COCASRIM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCASRIM),'CONTROPA','COCASRIM');
          +",CORATATT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CORATATT),'CONTROPA','CORATATT');
          +",CORATPAS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CORATPAS),'CONTROPA','CORATPAS');
          +",CORISATT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CORISATT),'CONTROPA','CORISATT');
          +",CORISPAS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CORISPAS),'CONTROPA','CORISPAS');
          +",COCAURER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCAURER),'CONTROPA','COCAURER');
          +",COCASRER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCASRER),'CONTROPA','COCASRER');
          +",COCAUCES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCAUCES),'CONTROPA','COCAUCES');
          +",COCASCES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCASCES),'CONTROPA','COCASCES');
          +",CODEBMED ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODEBMED),'CONTROPA','CODEBMED');
          +",CODEBBRE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODEBBRE),'CONTROPA','CODEBBRE');
          +",COCREMED ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCREMED),'CONTROPA','COCREMED');
          +",COCREBRE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCREBRE),'CONTROPA','COCREBRE');
          +",COCAFRER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCAFRER),'CONTROPA','COCAFRER');
          +",COINDIVA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COINDIVA),'CONTROPA','COINDIVA');
              +i_ccchkf ;
          +" where ";
              +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 )
        else
          update (i_cTable) set;
              COFATEME = this.oParentObject.w_COFATEME;
              ,COIVADEB = this.oParentObject.w_COIVADEB;
              ,COCAUFDE = this.oParentObject.w_COCAUFDE;
              ,COCASFDE = this.oParentObject.w_COCASFDE;
              ,COFATRIC = this.oParentObject.w_COFATRIC;
              ,COIVACRE = this.oParentObject.w_COIVACRE;
              ,COCAUFDR = this.oParentObject.w_COCAUFDR;
              ,COCASFDR = this.oParentObject.w_COCASFDR;
              ,COCAURIM = this.oParentObject.w_COCAURIM;
              ,COCASRIM = this.oParentObject.w_COCASRIM;
              ,CORATATT = this.oParentObject.w_CORATATT;
              ,CORATPAS = this.oParentObject.w_CORATPAS;
              ,CORISATT = this.oParentObject.w_CORISATT;
              ,CORISPAS = this.oParentObject.w_CORISPAS;
              ,COCAURER = this.oParentObject.w_COCAURER;
              ,COCASRER = this.oParentObject.w_COCASRER;
              ,COCAUCES = this.oParentObject.w_COCAUCES;
              ,COCASCES = this.oParentObject.w_COCASCES;
              ,CODEBMED = this.oParentObject.w_CODEBMED;
              ,CODEBBRE = this.oParentObject.w_CODEBBRE;
              ,COCREMED = this.oParentObject.w_COCREMED;
              ,COCREBRE = this.oParentObject.w_COCREBRE;
              ,COCAFRER = this.oParentObject.w_COCAFRER;
              ,COINDIVA = this.oParentObject.w_COINDIVA;
              &i_ccchkf. ;
           where;
              COCODAZI = i_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.oParentObject.ecpQuit
    endcase
  endproc
  proc Try_03844E28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CONTROPA
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTROPA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"COCODAZI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(i_CODAZI),'CONTROPA','COCODAZI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'COCODAZI',i_CODAZI)
      insert into (i_cTable) (COCODAZI &i_ccchkf. );
         values (;
           i_CODAZI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTROPA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
