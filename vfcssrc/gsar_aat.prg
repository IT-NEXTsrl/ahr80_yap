* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_aat                                                        *
*              Famiglie attributi                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-12                                                      *
* Last revis.: 2016-10-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsar_aat
* pParam='P' indica se se la gestione � utilizzata per il modulo Premi di fine anno
PARAM pParam
* --- Fine Area Manuale
return(createobject("tgsar_aat"))

* --- Class definition
define class tgsar_aat as StdForm
  Top    = 12
  Left   = 10

  * --- Standard Properties
  Width  = 531
  Height = 422+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-10-14"
  HelpContextID=143226729
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  FAM_ATTR_IDX = 0
  XDC_TABLE_IDX = 0
  XDC_FIELDS_IDX = 0
  GRUDATTR_IDX = 0
  cFile = "FAM_ATTR"
  cKeySelect = "FRCODICE"
  cKeyWhere  = "FRCODICE=this.w_FRCODICE"
  cKeyWhereODBC = '"FRCODICE="+cp_ToStrODBC(this.w_FRCODICE)';

  cKeyWhereODBCqualified = '"FAM_ATTR.FRCODICE="+cp_ToStrODBC(this.w_FRCODICE)';

  cPrg = "gsar_aat"
  cComment = "Famiglie attributi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FRCODICE = space(10)
  w_FRDESCRI = space(40)
  w_FR_TABLE = space(30)
  o_FR_TABLE = space(30)
  w_FR__ZOOM = space(254)
  w_CHIAVE = space(40)
  w_FR_CAMPO = space(50)
  o_FR_CAMPO = space(50)
  w_FRZOOMOZ = space(20)
  w_FRCAMCOL = space(50)
  w_FRCODFAM = space(10)
  w_FRTIPOLO = space(1)
  o_FRTIPOLO = space(1)
  w_FRINPUTA = space(1)
  o_FRINPUTA = space(1)
  w_FROBBLIG = space(1)
  w_FRMULTIP = space(1)
  w_FRDIMENS = 0
  o_FRDIMENS = 0
  w_FRNUMDEC = 0
  o_FRNUMDEC = 0
  w_FRAUTOMA = space(1)
  o_FRAUTOMA = space(1)
  w_TABELLA = space(20)
  w_ESTENSIONE = space(254)
  w_RESCHK = space(10)
  w_CALCPIC = 0
  w_FRATTRIC = space(1)
  o_FRATTRIC = space(1)
  w_HASEVCOP = space(15)
  w_HASEVENT = .F.

  * --- Children pointers
  GSAR_MAF = .NULL.
  GSAR_MAZ = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar_aat
  proc EcpDelete()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
      * --- Read from GRUDATTR
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
       endif
        i_nConn = i_TableProp[this.GRUDATTR_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.GRUDATTR_IDX,2])
      if i_nConn<>0
        sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" where ";
              +"GRFAMATT= "+cp_ToStrODBC(this.w_FRCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
       else
        select;
          *;
          from (i_cTable) where;
              GRFAMATT = this.w_FRCODICE;
           into cursor _read_
        i_Rows=_tally
       endif
       if used('_read_')
        use
       endif
       select (i_nOldArea)
     if i_Rows>0
      if ah_yesno("Confermi cancellazione?")
        ah_ERRORMSG("Il record � presente in altre tabelle. Impossibile cancellare %0%0(Tabella: 'GRUDATTR', column 'GRFAMATT')")
      endif
     else
       DoDefault()
     endif
  Endproc
  Proc F6()
  	if inlist(this.cFunction,'Edit','Load')
        bOK=.t.
  			this.NotifyEvent('F6')  && in caso sia tutto a posto setta bOK=.T. altrimenti notifica un errore
  			if bOK
  				dodefault()
  			endif
    endif
  EndProc
  w_FLEDIT = ' '
  * --- Gestiti i soli eventi Modifica, cancellazione, inserimento
  * --- Lanciato solo se in stato di Query per prevenire il controllo quando si
  * --- ri preme il tasto in modifica / cariacamento
  * --- F6 controlla evasione righe associate alla stessa riga di origine
  Func ah_HasCPEvents(i_cOp)
  	This.w_HASEVCOP=i_cop
  	If (this.cFunction='Query' And (Upper(i_cop)='ECPEDIT' )) Or (Upper(This.cFunction)="EDIT" )
        if (Upper(i_cop)='ECPEDIT' And (seconds()>this.nLoadTime+60 or this.bupdated)) Or(Upper(i_cop)='ECPDELETE' And this.nLoadTime=0)
          this.LoadRecWarn()  && rilettura se sono passati piu' di 60 secondi dall' ultima lettura o il record � stato variato in Interroga
          This.w_HASEVCOP=i_cop
        ENDIF
  		this.NotifyEvent('HasEvent')
  		return(this.w_HASEVENT)
  	Else
  		* --- Risetto sempre il Flag per editabilit�
  		This.w_FLEDIT = ' '
  		return(.t.)
  	endif
    return(.t.)
  EndFunc
  
  * Se per gestione premi fine anno, w_PREMIFA = 'P' in init page frame
  w_PREMIFA = ' '
  
  func GetSecurityCode()
      if empty(this.w_PREMIFA)
        return 'gsar_aat'
      else
        return 'gsar_aat,'+lower(this.w_PREMIFA)
      endif
  endfunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'FAM_ATTR','gsar_aat')
    stdPageFrame::Init()
    *set procedure to GSAR_MAF additive
    *set procedure to GSAR_MAZ additive
    with this
      .Pages(1).addobject("oPag","tgsar_aatPag1","gsar_aat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Attributi")
      .Pages(1).HelpContextID = 8407050
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFRCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSAR_MAF
    *release procedure GSAR_MAZ
    * --- Area Manuale = Init Page Frame
    * --- gsar_aat
    WITH THIS.PARENT
    IF Isahe()
       .cComment = CP_TRANSLATE('Famiglie attributo')
    ELSE
       .cComment = CP_TRANSLATE('Definizione attributo')
    ENDIF
    ENDWIT
    
    If not empty(pParam)
       this.parent.w_PREMIFA = pParam
    else
       this.parent.w_PREMIFA = ' '
    endif
    
    WITH THIS.PARENT
     .cAutoZoom = IIF(.w_PREMIFA = 'P', 'GSAR1AAT','DEFAULT')
    ENDWITH
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='XDC_TABLE'
    this.cWorkTables[2]='XDC_FIELDS'
    this.cWorkTables[3]='GRUDATTR'
    this.cWorkTables[4]='FAM_ATTR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.FAM_ATTR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.FAM_ATTR_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MAF = CREATEOBJECT('stdDynamicChild',this,'GSAR_MAF',this.oPgFrm.Page1.oPag.oLinkPC_1_1)
    this.GSAR_MAF.createrealchild()
    this.GSAR_MAZ = CREATEOBJECT('stdDynamicChild',this,'GSAR_MAZ',this.oPgFrm.Page1.oPag.oLinkPC_1_35)
    this.GSAR_MAZ.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MAF)
      this.GSAR_MAF.DestroyChildrenChain()
      this.GSAR_MAF=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_1')
    if !ISNULL(this.GSAR_MAZ)
      this.GSAR_MAZ.DestroyChildrenChain()
      this.GSAR_MAZ=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_35')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MAF.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MAZ.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MAF.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MAZ.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MAF.NewDocument()
    this.GSAR_MAZ.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAR_MAF.SetKey(;
            .w_FRCODICE,"FACODICE";
            )
      this.GSAR_MAZ.SetKey(;
            .w_FRCODICE,"ATCODFAM";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAR_MAF.ChangeRow(this.cRowID+'      1',1;
             ,.w_FRCODICE,"FACODICE";
             )
      .WriteTo_GSAR_MAF()
      .GSAR_MAZ.ChangeRow(this.cRowID+'      1',1;
             ,.w_FRCODICE,"ATCODFAM";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAR_MAF)
        i_f=.GSAR_MAF.BuildFilter()
        if !(i_f==.GSAR_MAF.cQueryFilter)
          i_fnidx=.GSAR_MAF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MAF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MAF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MAF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MAF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MAZ)
        i_f=.GSAR_MAZ.BuildFilter()
        if !(i_f==.GSAR_MAZ.cQueryFilter)
          i_fnidx=.GSAR_MAZ.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MAZ.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MAZ.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MAZ.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MAZ.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSAR_MAF()
  if at('gsar_maf',lower(this.GSAR_MAF.class))<>0
    if this.GSAR_MAF.w_CAMPO<>this.w_FR_CAMPO or this.GSAR_MAF.w_DIMENS<>this.w_FRDIMENS or this.GSAR_MAF.w_INPUTA<>this.w_FRINPUTA or this.GSAR_MAF.w_AUTOMA<>this.w_FRAUTOMA or this.GSAR_MAF.w_TIPOLO<>this.w_FRTIPOLO
      this.GSAR_MAF.w_CAMPO = this.w_FR_CAMPO
      this.GSAR_MAF.w_DIMENS = this.w_FRDIMENS
      this.GSAR_MAF.w_INPUTA = this.w_FRINPUTA
      this.GSAR_MAF.w_AUTOMA = this.w_FRAUTOMA
      this.GSAR_MAF.w_TIPOLO = this.w_FRTIPOLO
      this.GSAR_MAF.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_FRCODICE = NVL(FRCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from FAM_ATTR where FRCODICE=KeySet.FRCODICE
    *
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('FAM_ATTR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "FAM_ATTR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' FAM_ATTR '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FRCODICE',this.w_FRCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RESCHK = 0
        .w_HASEVCOP = space(15)
        .w_HASEVENT = .t.
        .w_FRCODICE = NVL(FRCODICE,space(10))
        .w_FRDESCRI = NVL(FRDESCRI,space(40))
        .w_FR_TABLE = NVL(FR_TABLE,space(30))
          * evitabile
          *.link_1_5('Load')
        .w_FR__ZOOM = NVL(FR__ZOOM,space(254))
        .w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( .w_FR_TABLE ,1 ) )
        .w_FR_CAMPO = NVL(FR_CAMPO,space(50))
          * evitabile
          *.link_1_9('Load')
        .w_FRZOOMOZ = NVL(FRZOOMOZ,space(20))
        .w_FRCAMCOL = NVL(FRCAMCOL,space(50))
          * evitabile
          *.link_1_12('Load')
        .w_FRCODFAM = NVL(FRCODFAM,space(10))
          * evitabile
          *.link_1_13('Load')
        .w_FRTIPOLO = NVL(FRTIPOLO,space(1))
        .w_FRINPUTA = NVL(FRINPUTA,space(1))
        .w_FROBBLIG = NVL(FROBBLIG,space(1))
        .w_FRMULTIP = NVL(FRMULTIP,space(1))
        .w_FRDIMENS = NVL(FRDIMENS,0)
        .w_FRNUMDEC = NVL(FRNUMDEC,0)
        .w_FRAUTOMA = NVL(FRAUTOMA,space(1))
        .w_TABELLA = IIF(G_APPLICATION='ADHOC REVOLUTION', Left( .w_FR_TABLE,15 ), Left( .w_FR_TABLE,20))
        .w_ESTENSIONE = ALLTRIM(.w_FR_TABLE)+'_vzm'
        .w_CALCPIC = IIF(.w_FRINPUTA = 'N',DEFPICAT(.w_FRDIMENS,.w_FRNUMDEC),0)
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .w_FRATTRIC = NVL(FRATTRIC,space(1))
        cp_LoadRecExtFlds(this,'FAM_ATTR')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_aat
    *--Gestisco la condizione di hide sui campi di griglia del figlio integrato--*
    this.GSAR_MAF.mHideControls()
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FRCODICE = space(10)
      .w_FRDESCRI = space(40)
      .w_FR_TABLE = space(30)
      .w_FR__ZOOM = space(254)
      .w_CHIAVE = space(40)
      .w_FR_CAMPO = space(50)
      .w_FRZOOMOZ = space(20)
      .w_FRCAMCOL = space(50)
      .w_FRCODFAM = space(10)
      .w_FRTIPOLO = space(1)
      .w_FRINPUTA = space(1)
      .w_FROBBLIG = space(1)
      .w_FRMULTIP = space(1)
      .w_FRDIMENS = 0
      .w_FRNUMDEC = 0
      .w_FRAUTOMA = space(1)
      .w_TABELLA = space(20)
      .w_ESTENSIONE = space(254)
      .w_RESCHK = space(10)
      .w_CALCPIC = 0
      .w_FRATTRIC = space(1)
      .w_HASEVCOP = space(15)
      .w_HASEVENT = .f.
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
          if not(empty(.w_FR_TABLE))
          .link_1_5('Full')
          endif
          .DoRTCalc(4,4,.f.)
        .w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( .w_FR_TABLE ,1 ) )
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_FR_CAMPO))
          .link_1_9('Full')
          endif
        .w_FRZOOMOZ = IIF(.w_FR_TABLE='ART_ICOL', 'GSMA_BZA', IIF(.w_FR_TABLE='CONTI', 'GSAR_BZC', IIF(.w_FR_TABLE='LOTTIART', iif(isahe(),'GSAR_ALO','GSMD_ALO'), IIF(.w_FR_TABLE='MATRICOL',IIF(isahe(),'GSVE_MDT','GSMD_AMT'),''))))
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_FRCAMCOL))
          .link_1_12('Full')
          endif
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_FRCODFAM))
          .link_1_13('Full')
          endif
        .w_FRTIPOLO = 'F'
        .w_FRINPUTA = 'C'
        .w_FROBBLIG = 'S'
        .w_FRMULTIP = 'S'
        .w_FRDIMENS = iif(.w_FRINPUTA='C',20,12)
        .w_FRNUMDEC = 3
        .w_FRAUTOMA = 'N'
        .w_TABELLA = IIF(G_APPLICATION='ADHOC REVOLUTION', Left( .w_FR_TABLE,15 ), Left( .w_FR_TABLE,20))
        .w_ESTENSIONE = ALLTRIM(.w_FR_TABLE)+'_vzm'
        .w_RESCHK = 0
        .w_CALCPIC = IIF(.w_FRINPUTA = 'N',DEFPICAT(.w_FRDIMENS,.w_FRNUMDEC),0)
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .w_FRATTRIC = 'N'
          .DoRTCalc(22,22,.f.)
        .w_HASEVENT = .t.
      endif
    endwith
    cp_BlankRecExtFlds(this,'FAM_ATTR')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oFRCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oFRDESCRI_1_4.enabled = i_bVal
      .Page1.oPag.oFR_TABLE_1_5.enabled = i_bVal
      .Page1.oPag.oFR__ZOOM_1_6.enabled = i_bVal
      .Page1.oPag.oFR_CAMPO_1_9.enabled = i_bVal
      .Page1.oPag.oFRZOOMOZ_1_11.enabled = i_bVal
      .Page1.oPag.oFRCAMCOL_1_12.enabled = i_bVal
      .Page1.oPag.oFRCODFAM_1_13.enabled = i_bVal
      .Page1.oPag.oFRTIPOLO_1_15.enabled = i_bVal
      .Page1.oPag.oFRINPUTA_1_17.enabled = i_bVal
      .Page1.oPag.oFROBBLIG_1_18.enabled = i_bVal
      .Page1.oPag.oFRMULTIP_1_19.enabled = i_bVal
      .Page1.oPag.oFRDIMENS_1_20.enabled = i_bVal
      .Page1.oPag.oFRNUMDEC_1_21.enabled = i_bVal
      .Page1.oPag.oFRATTRIC_1_40.enabled = i_bVal
      .Page1.oPag.oObj_1_38.enabled = i_bVal
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oFRCODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oFRCODICE_1_2.enabled = .t.
        .Page1.oPag.oFRDESCRI_1_4.enabled = .t.
      endif
    endwith
    this.GSAR_MAF.SetStatus(i_cOp)
    this.GSAR_MAZ.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'FAM_ATTR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MAF.SetChildrenStatus(i_cOp)
  *  this.GSAR_MAZ.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRCODICE,"FRCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRDESCRI,"FRDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FR_TABLE,"FR_TABLE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FR__ZOOM,"FR__ZOOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FR_CAMPO,"FR_CAMPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRZOOMOZ,"FRZOOMOZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRCAMCOL,"FRCAMCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRCODFAM,"FRCODFAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRTIPOLO,"FRTIPOLO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRINPUTA,"FRINPUTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FROBBLIG,"FROBBLIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRMULTIP,"FRMULTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRDIMENS,"FRDIMENS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRNUMDEC,"FRNUMDEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRAUTOMA,"FRAUTOMA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FRATTRIC,"FRATTRIC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
    i_lTable = "FAM_ATTR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.FAM_ATTR_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SAT with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.FAM_ATTR_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into FAM_ATTR
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'FAM_ATTR')
        i_extval=cp_InsertValODBCExtFlds(this,'FAM_ATTR')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(FRCODICE,FRDESCRI,FR_TABLE,FR__ZOOM,FR_CAMPO"+;
                  ",FRZOOMOZ,FRCAMCOL,FRCODFAM,FRTIPOLO,FRINPUTA"+;
                  ",FROBBLIG,FRMULTIP,FRDIMENS,FRNUMDEC,FRAUTOMA"+;
                  ",FRATTRIC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_FRCODICE)+;
                  ","+cp_ToStrODBC(this.w_FRDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_FR_TABLE)+;
                  ","+cp_ToStrODBC(this.w_FR__ZOOM)+;
                  ","+cp_ToStrODBCNull(this.w_FR_CAMPO)+;
                  ","+cp_ToStrODBC(this.w_FRZOOMOZ)+;
                  ","+cp_ToStrODBCNull(this.w_FRCAMCOL)+;
                  ","+cp_ToStrODBCNull(this.w_FRCODFAM)+;
                  ","+cp_ToStrODBC(this.w_FRTIPOLO)+;
                  ","+cp_ToStrODBC(this.w_FRINPUTA)+;
                  ","+cp_ToStrODBC(this.w_FROBBLIG)+;
                  ","+cp_ToStrODBC(this.w_FRMULTIP)+;
                  ","+cp_ToStrODBC(this.w_FRDIMENS)+;
                  ","+cp_ToStrODBC(this.w_FRNUMDEC)+;
                  ","+cp_ToStrODBC(this.w_FRAUTOMA)+;
                  ","+cp_ToStrODBC(this.w_FRATTRIC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'FAM_ATTR')
        i_extval=cp_InsertValVFPExtFlds(this,'FAM_ATTR')
        cp_CheckDeletedKey(i_cTable,0,'FRCODICE',this.w_FRCODICE)
        INSERT INTO (i_cTable);
              (FRCODICE,FRDESCRI,FR_TABLE,FR__ZOOM,FR_CAMPO,FRZOOMOZ,FRCAMCOL,FRCODFAM,FRTIPOLO,FRINPUTA,FROBBLIG,FRMULTIP,FRDIMENS,FRNUMDEC,FRAUTOMA,FRATTRIC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_FRCODICE;
                  ,this.w_FRDESCRI;
                  ,this.w_FR_TABLE;
                  ,this.w_FR__ZOOM;
                  ,this.w_FR_CAMPO;
                  ,this.w_FRZOOMOZ;
                  ,this.w_FRCAMCOL;
                  ,this.w_FRCODFAM;
                  ,this.w_FRTIPOLO;
                  ,this.w_FRINPUTA;
                  ,this.w_FROBBLIG;
                  ,this.w_FRMULTIP;
                  ,this.w_FRDIMENS;
                  ,this.w_FRNUMDEC;
                  ,this.w_FRAUTOMA;
                  ,this.w_FRATTRIC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.FAM_ATTR_IDX,i_nConn)
      *
      * update FAM_ATTR
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'FAM_ATTR')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " FRDESCRI="+cp_ToStrODBC(this.w_FRDESCRI)+;
             ",FR_TABLE="+cp_ToStrODBCNull(this.w_FR_TABLE)+;
             ",FR__ZOOM="+cp_ToStrODBC(this.w_FR__ZOOM)+;
             ",FR_CAMPO="+cp_ToStrODBCNull(this.w_FR_CAMPO)+;
             ",FRZOOMOZ="+cp_ToStrODBC(this.w_FRZOOMOZ)+;
             ",FRCAMCOL="+cp_ToStrODBCNull(this.w_FRCAMCOL)+;
             ",FRCODFAM="+cp_ToStrODBCNull(this.w_FRCODFAM)+;
             ",FRTIPOLO="+cp_ToStrODBC(this.w_FRTIPOLO)+;
             ",FRINPUTA="+cp_ToStrODBC(this.w_FRINPUTA)+;
             ",FROBBLIG="+cp_ToStrODBC(this.w_FROBBLIG)+;
             ",FRMULTIP="+cp_ToStrODBC(this.w_FRMULTIP)+;
             ",FRDIMENS="+cp_ToStrODBC(this.w_FRDIMENS)+;
             ",FRNUMDEC="+cp_ToStrODBC(this.w_FRNUMDEC)+;
             ",FRAUTOMA="+cp_ToStrODBC(this.w_FRAUTOMA)+;
             ",FRATTRIC="+cp_ToStrODBC(this.w_FRATTRIC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'FAM_ATTR')
        i_cWhere = cp_PKFox(i_cTable  ,'FRCODICE',this.w_FRCODICE  )
        UPDATE (i_cTable) SET;
              FRDESCRI=this.w_FRDESCRI;
             ,FR_TABLE=this.w_FR_TABLE;
             ,FR__ZOOM=this.w_FR__ZOOM;
             ,FR_CAMPO=this.w_FR_CAMPO;
             ,FRZOOMOZ=this.w_FRZOOMOZ;
             ,FRCAMCOL=this.w_FRCAMCOL;
             ,FRCODFAM=this.w_FRCODFAM;
             ,FRTIPOLO=this.w_FRTIPOLO;
             ,FRINPUTA=this.w_FRINPUTA;
             ,FROBBLIG=this.w_FROBBLIG;
             ,FRMULTIP=this.w_FRMULTIP;
             ,FRDIMENS=this.w_FRDIMENS;
             ,FRNUMDEC=this.w_FRNUMDEC;
             ,FRAUTOMA=this.w_FRAUTOMA;
             ,FRATTRIC=this.w_FRATTRIC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAR_MAF : Saving
      this.GSAR_MAF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_FRCODICE,"FACODICE";
             )
      this.GSAR_MAF.mReplace()
      * --- GSAR_MAZ : Saving
      this.GSAR_MAZ.ChangeRow(this.cRowID+'      1',0;
             ,this.w_FRCODICE,"ATCODFAM";
             )
      this.GSAR_MAZ.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsar_aat
    if not(bTrsErr)
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAR_MAF : Deleting
    this.GSAR_MAF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_FRCODICE,"FACODICE";
           )
    this.GSAR_MAF.mDelete()
    * --- GSAR_MAZ : Deleting
    this.GSAR_MAZ.ChangeRow(this.cRowID+'      1',0;
           ,this.w_FRCODICE,"ATCODFAM";
           )
    this.GSAR_MAZ.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.FAM_ATTR_IDX,i_nConn)
      *
      * delete FAM_ATTR
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'FRCODICE',this.w_FRCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
    if i_bUpd
      with this
        if  .o_FR_CAMPO<>.w_FR_CAMPO.or. .o_FRDIMENS<>.w_FRDIMENS.or. .o_FRINPUTA<>.w_FRINPUTA.or. .o_FRAUTOMA<>.w_FRAUTOMA.or. .o_FRTIPOLO<>.w_FRTIPOLO
          .WriteTo_GSAR_MAF()
        endif
        .DoRTCalc(1,4,.t.)
        if .o_FR_TABLE<>.w_FR_TABLE
            .w_CHIAVE = cp_KeyToSQL ( I_DCX.GetIdxDef( .w_FR_TABLE ,1 ) )
        endif
        .DoRTCalc(6,6,.t.)
        if .o_FR_TABLE<>.w_FR_TABLE
            .w_FRZOOMOZ = IIF(.w_FR_TABLE='ART_ICOL', 'GSMA_BZA', IIF(.w_FR_TABLE='CONTI', 'GSAR_BZC', IIF(.w_FR_TABLE='LOTTIART', iif(isahe(),'GSAR_ALO','GSMD_ALO'), IIF(.w_FR_TABLE='MATRICOL',IIF(isahe(),'GSVE_MDT','GSMD_AMT'),''))))
        endif
        .DoRTCalc(8,13,.t.)
        if .o_FRINPUTA<>.w_FRINPUTA
            .w_FRDIMENS = iif(.w_FRINPUTA='C',20,12)
        endif
        .DoRTCalc(15,16,.t.)
        if .o_FR_TABLE<>.w_FR_TABLE
            .w_TABELLA = IIF(G_APPLICATION='ADHOC REVOLUTION', Left( .w_FR_TABLE,15 ), Left( .w_FR_TABLE,20))
        endif
            .w_ESTENSIONE = ALLTRIM(.w_FR_TABLE)+'_vzm'
        .DoRTCalc(19,19,.t.)
        if .o_FRNUMDEC<>.w_FRNUMDEC.or. .o_FRDIMENS<>.w_FRDIMENS.or. .o_FRINPUTA<>.w_FRINPUTA
            .w_CALCPIC = IIF(.w_FRINPUTA = 'N',DEFPICAT(.w_FRDIMENS,.w_FRNUMDEC),0)
        endif
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFR_TABLE_1_5.enabled = this.oPgFrm.Page1.oPag.oFR_TABLE_1_5.mCond()
    this.oPgFrm.Page1.oPag.oFR__ZOOM_1_6.enabled = this.oPgFrm.Page1.oPag.oFR__ZOOM_1_6.mCond()
    this.oPgFrm.Page1.oPag.oFR_CAMPO_1_9.enabled = this.oPgFrm.Page1.oPag.oFR_CAMPO_1_9.mCond()
    this.oPgFrm.Page1.oPag.oFRZOOMOZ_1_11.enabled = this.oPgFrm.Page1.oPag.oFRZOOMOZ_1_11.mCond()
    this.oPgFrm.Page1.oPag.oFRCAMCOL_1_12.enabled = this.oPgFrm.Page1.oPag.oFRCAMCOL_1_12.mCond()
    this.oPgFrm.Page1.oPag.oFRCODFAM_1_13.enabled = this.oPgFrm.Page1.oPag.oFRCODFAM_1_13.mCond()
    this.oPgFrm.Page1.oPag.oFRTIPOLO_1_15.enabled = this.oPgFrm.Page1.oPag.oFRTIPOLO_1_15.mCond()
    this.oPgFrm.Page1.oPag.oFRINPUTA_1_17.enabled = this.oPgFrm.Page1.oPag.oFRINPUTA_1_17.mCond()
    this.oPgFrm.Page1.oPag.oFROBBLIG_1_18.enabled = this.oPgFrm.Page1.oPag.oFROBBLIG_1_18.mCond()
    this.oPgFrm.Page1.oPag.oFRMULTIP_1_19.enabled = this.oPgFrm.Page1.oPag.oFRMULTIP_1_19.mCond()
    this.oPgFrm.Page1.oPag.oFRDIMENS_1_20.enabled = this.oPgFrm.Page1.oPag.oFRDIMENS_1_20.mCond()
    this.oPgFrm.Page1.oPag.oFRNUMDEC_1_21.enabled = this.oPgFrm.Page1.oPag.oFRNUMDEC_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oLinkPC_1_1.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_1.mHide()
    this.oPgFrm.Page1.oPag.oFR_TABLE_1_5.visible=!this.oPgFrm.Page1.oPag.oFR_TABLE_1_5.mHide()
    this.oPgFrm.Page1.oPag.oFR__ZOOM_1_6.visible=!this.oPgFrm.Page1.oPag.oFR__ZOOM_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oFR_CAMPO_1_9.visible=!this.oPgFrm.Page1.oPag.oFR_CAMPO_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oFRZOOMOZ_1_11.visible=!this.oPgFrm.Page1.oPag.oFRZOOMOZ_1_11.mHide()
    this.oPgFrm.Page1.oPag.oFRCAMCOL_1_12.visible=!this.oPgFrm.Page1.oPag.oFRCAMCOL_1_12.mHide()
    this.oPgFrm.Page1.oPag.oFRCODFAM_1_13.visible=!this.oPgFrm.Page1.oPag.oFRCODFAM_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oFRTIPOLO_1_15.visible=!this.oPgFrm.Page1.oPag.oFRTIPOLO_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oFRINPUTA_1_17.visible=!this.oPgFrm.Page1.oPag.oFRINPUTA_1_17.mHide()
    this.oPgFrm.Page1.oPag.oFRMULTIP_1_19.visible=!this.oPgFrm.Page1.oPag.oFRMULTIP_1_19.mHide()
    this.oPgFrm.Page1.oPag.oFRDIMENS_1_20.visible=!this.oPgFrm.Page1.oPag.oFRDIMENS_1_20.mHide()
    this.oPgFrm.Page1.oPag.oFRNUMDEC_1_21.visible=!this.oPgFrm.Page1.oPag.oFRNUMDEC_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_35.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_35.mHide()
    this.oPgFrm.Page1.oPag.oFRATTRIC_1_40.visible=!this.oPgFrm.Page1.oPag.oFRATTRIC_1_40.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=FR_TABLE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FR_TABLE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_FR_TABLE)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_FR_TABLE))
          select TBNAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FR_TABLE)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FR_TABLE) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oFR_TABLE_1_5'),i_cWhere,'',"Archivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FR_TABLE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_FR_TABLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_FR_TABLE)
            select TBNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FR_TABLE = NVL(_Link_.TBNAME,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_FR_TABLE = space(30)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKNFILE(.w_FR_TABLE,'C')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_FR_TABLE = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FR_TABLE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FR_CAMPO
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FR_CAMPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_FR_CAMPO)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_TABELLA);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_TABELLA;
                     ,'FLNAME',trim(this.w_FR_CAMPO))
          select TBNAME,FLNAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FR_CAMPO)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FR_CAMPO) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oFR_CAMPO_1_9'),i_cWhere,'',"Elenco campi",'GSAR_XDC.XDC_FIELDS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TABELLA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione: inserire solo campi chiave!")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_TABELLA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FR_CAMPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_FR_CAMPO);
                   +" and TBNAME="+cp_ToStrODBC(this.w_TABELLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_TABELLA;
                       ,'FLNAME',this.w_FR_CAMPO)
            select TBNAME,FLNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FR_CAMPO = NVL(_Link_.FLNAME,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_FR_CAMPO = space(50)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=ALLTRIM(.w_FR_CAMPO) $ (cp_KeyToSQL ( I_DCX.GetIdxDef( Alltrim(.w_FR_TABLE) ,1 ) ))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione: inserire solo campi chiave!")
        endif
        this.w_FR_CAMPO = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FR_CAMPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FRCAMCOL
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FRCAMCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_FRCAMCOL)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_TABELLA);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_TABELLA;
                     ,'FLNAME',trim(this.w_FRCAMCOL))
          select TBNAME,FLNAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FRCAMCOL)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FRCAMCOL) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oFRCAMCOL_1_12'),i_cWhere,'',"Elenco campi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TABELLA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_TABELLA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FRCAMCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_FRCAMCOL);
                   +" and TBNAME="+cp_ToStrODBC(this.w_TABELLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_TABELLA;
                       ,'FLNAME',this.w_FRCAMCOL)
            select TBNAME,FLNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FRCAMCOL = NVL(_Link_.FLNAME,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_FRCAMCOL = space(50)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FRCAMCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FRCODFAM
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_lTable = "FAM_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2], .t., this.FAM_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FRCODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AAT',True,'FAM_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FRCODICE like "+cp_ToStrODBC(trim(this.w_FRCODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FRCODICE',trim(this.w_FRCODFAM))
          select FRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FRCODFAM)==trim(_Link_.FRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FRCODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ATTR','*','FRCODICE',cp_AbsName(oSource.parent,'oFRCODFAM_1_13'),i_cWhere,'GSAR_AAT',"Attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',oSource.xKey(1))
            select FRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FRCODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(this.w_FRCODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',this.w_FRCODFAM)
            select FRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FRCODFAM = NVL(_Link_.FRCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_FRCODFAM = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.FRCODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FRCODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFRCODICE_1_2.value==this.w_FRCODICE)
      this.oPgFrm.Page1.oPag.oFRCODICE_1_2.value=this.w_FRCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oFRDESCRI_1_4.value==this.w_FRDESCRI)
      this.oPgFrm.Page1.oPag.oFRDESCRI_1_4.value=this.w_FRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oFR_TABLE_1_5.value==this.w_FR_TABLE)
      this.oPgFrm.Page1.oPag.oFR_TABLE_1_5.value=this.w_FR_TABLE
    endif
    if not(this.oPgFrm.Page1.oPag.oFR__ZOOM_1_6.value==this.w_FR__ZOOM)
      this.oPgFrm.Page1.oPag.oFR__ZOOM_1_6.value=this.w_FR__ZOOM
    endif
    if not(this.oPgFrm.Page1.oPag.oFR_CAMPO_1_9.value==this.w_FR_CAMPO)
      this.oPgFrm.Page1.oPag.oFR_CAMPO_1_9.value=this.w_FR_CAMPO
    endif
    if not(this.oPgFrm.Page1.oPag.oFRZOOMOZ_1_11.value==this.w_FRZOOMOZ)
      this.oPgFrm.Page1.oPag.oFRZOOMOZ_1_11.value=this.w_FRZOOMOZ
    endif
    if not(this.oPgFrm.Page1.oPag.oFRCAMCOL_1_12.value==this.w_FRCAMCOL)
      this.oPgFrm.Page1.oPag.oFRCAMCOL_1_12.value=this.w_FRCAMCOL
    endif
    if not(this.oPgFrm.Page1.oPag.oFRCODFAM_1_13.value==this.w_FRCODFAM)
      this.oPgFrm.Page1.oPag.oFRCODFAM_1_13.value=this.w_FRCODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oFRTIPOLO_1_15.RadioValue()==this.w_FRTIPOLO)
      this.oPgFrm.Page1.oPag.oFRTIPOLO_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFRINPUTA_1_17.RadioValue()==this.w_FRINPUTA)
      this.oPgFrm.Page1.oPag.oFRINPUTA_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFROBBLIG_1_18.RadioValue()==this.w_FROBBLIG)
      this.oPgFrm.Page1.oPag.oFROBBLIG_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFRMULTIP_1_19.RadioValue()==this.w_FRMULTIP)
      this.oPgFrm.Page1.oPag.oFRMULTIP_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFRDIMENS_1_20.value==this.w_FRDIMENS)
      this.oPgFrm.Page1.oPag.oFRDIMENS_1_20.value=this.w_FRDIMENS
    endif
    if not(this.oPgFrm.Page1.oPag.oFRNUMDEC_1_21.value==this.w_FRNUMDEC)
      this.oPgFrm.Page1.oPag.oFRNUMDEC_1_21.value=this.w_FRNUMDEC
    endif
    if not(this.oPgFrm.Page1.oPag.oFRATTRIC_1_40.RadioValue()==this.w_FRATTRIC)
      this.oPgFrm.Page1.oPag.oFRATTRIC_1_40.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'FAM_ATTR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FRCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFRCODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_FRCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKNFILE(.w_FR_TABLE,'C'))  and not(.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')  and (Empty(.w_FLEDIT))  and not(empty(.w_FR_TABLE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFR_TABLE_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(ALLTRIM(.w_FR_CAMPO) $ (cp_KeyToSQL ( I_DCX.GetIdxDef( Alltrim(.w_FR_TABLE) ,1 ) )))  and not(.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')  and (Empty(.w_FLEDIT))  and not(empty(.w_FR_CAMPO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFR_CAMPO_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione: inserire solo campi chiave!")
          case   not(iif(.w_FRINPUTA='N', .w_FRDIMENS<13 AND .w_FRDIMENS>0 , .w_FRDIMENS<21 AND .w_FRDIMENS>0))  and not(.w_FRTIPOLO='L' OR .w_FRINPUTA='D' OR .w_FRAUTOMA ='S' OR .w_PREMIFA = 'P')  and (EMPTY(ALLTRIM(.w_FR_CAMPO)) AND Empty(.w_FLEDIT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFRDIMENS_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FRNUMDEC<=3 and .w_FRNUMDEC>=0)  and not(.w_FRINPUTA<>'N' OR .w_FRAUTOMA ='S')  and (EMPTY(ALLTRIM(.w_FR_CAMPO)) AND Empty(.w_FLEDIT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFRNUMDEC_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAR_MAF.CheckForm()
      if i_bres
        i_bres=  .GSAR_MAF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MAZ.CheckForm()
      if i_bres
        i_bres=  .GSAR_MAZ.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsar_aat
      IF i_bRes=.T. AND (.w_FRTIPOLO='A' OR .w_FRTIPOLO='B') AND (EMPTY(.w_FR_TABLE) or EMPTY(.w_FR_CAMPO))
            AH_ERRORMSG("Inserire l'archivio e il campo collegati")
            i_bRes=.f.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FR_TABLE = this.w_FR_TABLE
    this.o_FR_CAMPO = this.w_FR_CAMPO
    this.o_FRTIPOLO = this.w_FRTIPOLO
    this.o_FRINPUTA = this.w_FRINPUTA
    this.o_FRDIMENS = this.w_FRDIMENS
    this.o_FRNUMDEC = this.w_FRNUMDEC
    this.o_FRAUTOMA = this.w_FRAUTOMA
    this.o_FRATTRIC = this.w_FRATTRIC
    * --- GSAR_MAF : Depends On
    this.GSAR_MAF.SaveDependsOn()
    * --- GSAR_MAZ : Depends On
    this.GSAR_MAZ.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsar_aatPag1 as StdContainer
  Width  = 527
  height = 423
  stdWidth  = 527
  stdheight = 423
  resizeXpos=264
  resizeYpos=333
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_1_1 as stdDynamicChildContainer with uid="XVRPNECNJX",left=1, top=154, width=393, height=269, bOnScreen=.t.;


  func oLinkPC_1_1.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FRTIPOLO='A' OR .w_FRTIPOLO='B' Or .cFunction='Filter')
     endwith
    endif
  endfunc

  add object oFRCODICE_1_2 as StdField with uid="CIKSXWSVEQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FRCODICE", cQueryName = "FRCODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 84545435,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=89, Top=14, InputMask=replicate('X',10)

  add object oFRDESCRI_1_4 as StdField with uid="UAYNZWTGFE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FRDESCRI", cQueryName = "FRDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 267394975,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=176, Top=14, InputMask=replicate('X',40)

  add object oFR_TABLE_1_5 as StdField with uid="BYWVAJLZWA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FR_TABLE", cQueryName = "FR_TABLE",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nome archivio collegato",;
    HelpContextID = 35598437,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=115, Top=162, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_FR_TABLE"

  func oFR_TABLE_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_FLEDIT))
    endwith
   endif
  endfunc

  func oFR_TABLE_1_5.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')
    endwith
  endfunc

  func oFR_TABLE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oFR_TABLE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFR_TABLE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oFR_TABLE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Archivi",'',this.parent.oContained
  endproc

  add object oFR__ZOOM_1_6 as StdField with uid="KZAYRWYJHO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FR__ZOOM", cQueryName = "FR__ZOOM",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Zoom da selezionare",;
    HelpContextID = 58994781,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=115, Top=188, InputMask=replicate('X',254), bHasZoom = .t. 

  func oFR__ZOOM_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_FLEDIT))
    endwith
   endif
  endfunc

  func oFR__ZOOM_1_6.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')
    endwith
  endfunc

  proc oFR__ZOOM_1_6.mZoom
    AskZoom(this.parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFR_CAMPO_1_9 as StdField with uid="IGSAYUKDLD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FR_CAMPO", cQueryName = "FR_CAMPO",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione: inserire solo campi chiave!",;
    ToolTipText = "Campo archivio collegato",;
    HelpContextID = 120598619,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=115, Top=214, InputMask=replicate('X',50), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_TABELLA", oKey_2_1="FLNAME", oKey_2_2="this.w_FR_CAMPO"

  func oFR_CAMPO_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_FLEDIT))
    endwith
   endif
  endfunc

  func oFR_CAMPO_1_9.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')
    endwith
  endfunc

  func oFR_CAMPO_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oFR_CAMPO_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFR_CAMPO_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_TABELLA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_TABELLA)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oFR_CAMPO_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco campi",'GSAR_XDC.XDC_FIELDS_VZM',this.parent.oContained
  endproc

  add object oFRZOOMOZ_1_11 as StdField with uid="LDOXPIYBUY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FRZOOMOZ", cQueryName = "FRZOOMOZ",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Zoom on zoom collegato allo zoom",;
    HelpContextID = 105152592,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=115, Top=240, InputMask=replicate('X',20)

  func oFRZOOMOZ_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_FLEDIT))
    endwith
   endif
  endfunc

  func oFRZOOMOZ_1_11.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')
    endwith
  endfunc

  add object oFRCAMCOL_1_12 as StdField with uid="QMDXSTTKYG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_FRCAMCOL", cQueryName = "FRCAMCOL",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Campo archivio collegato per filltro",;
    HelpContextID = 7598174,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=115, Top=292, InputMask=replicate('X',50), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_TABELLA", oKey_2_1="FLNAME", oKey_2_2="this.w_FRCAMCOL"

  func oFRCAMCOL_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_FLEDIT) AND NOT EMPTY(.w_FRCODFAM))
    endwith
   endif
  endfunc

  func oFRCAMCOL_1_12.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')
    endwith
  endfunc

  func oFRCAMCOL_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oFRCAMCOL_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFRCAMCOL_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_TABELLA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_TABELLA)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oFRCAMCOL_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco campi",'',this.parent.oContained
  endproc

  add object oFRCODFAM_1_13 as StdField with uid="TTWHWISIKB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_FRCODFAM", cQueryName = "FRCODFAM",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia collegata",;
    HelpContextID = 34213795,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=115, Top=266, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="FAM_ATTR", cZoomOnZoom="GSAR_AAT", oKey_1_1="FRCODICE", oKey_1_2="this.w_FRCODFAM"

  func oFRCODFAM_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_FLEDIT) )
    endwith
   endif
  endfunc

  func oFRCODFAM_1_13.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')
    endwith
  endfunc

  func oFRCODFAM_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oFRCODFAM_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFRCODFAM_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ATTR','*','FRCODICE',cp_AbsName(this.parent,'oFRCODFAM_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AAT',"Attributi",'',this.parent.oContained
  endproc
  proc oFRCODFAM_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AAT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FRCODICE=this.parent.oContained.w_FRCODFAM
     i_obj.ecpSave()
  endproc


  add object oFRTIPOLO_1_15 as StdCombo with uid="TGUVPINOKA",rtseq=10,rtrep=.f.,left=89,top=43,width=167,height=22;
    , ToolTipText = "Tipologia attributo";
    , HelpContextID = 70967387;
    , cFormVar="w_FRTIPOLO",RowSource=""+"Libero,"+"Fisso da elenco,"+"Misto da elenco,"+"Fisso da archivio,"+"Misto da archivio", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFRTIPOLO_1_15.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'F',;
    iif(this.value =3,'M',;
    iif(this.value =4,'A',;
    iif(this.value =5,'B',;
    space(1)))))))
  endfunc
  func oFRTIPOLO_1_15.GetRadio()
    this.Parent.oContained.w_FRTIPOLO = this.RadioValue()
    return .t.
  endfunc

  func oFRTIPOLO_1_15.SetRadio()
    this.Parent.oContained.w_FRTIPOLO=trim(this.Parent.oContained.w_FRTIPOLO)
    this.value = ;
      iif(this.Parent.oContained.w_FRTIPOLO=='L',1,;
      iif(this.Parent.oContained.w_FRTIPOLO=='F',2,;
      iif(this.Parent.oContained.w_FRTIPOLO=='M',3,;
      iif(this.Parent.oContained.w_FRTIPOLO=='A',4,;
      iif(this.Parent.oContained.w_FRTIPOLO=='B',5,;
      0)))))
  endfunc

  func oFRTIPOLO_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_FLEDIT))
    endwith
   endif
  endfunc

  func oFRTIPOLO_1_15.mHide()
    with this.Parent.oContained
      return (.w_PREMIFA = 'P')
    endwith
  endfunc


  add object oFRINPUTA_1_17 as StdCombo with uid="LEQUGUGTFC",rtseq=11,rtrep=.f.,left=403,top=43,width=83,height=21;
    , ToolTipText = "Input attributo";
    , HelpContextID = 29978519;
    , cFormVar="w_FRINPUTA",RowSource=""+"Carattere,"+"Data,"+"Numerico", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFRINPUTA_1_17.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'D',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oFRINPUTA_1_17.GetRadio()
    this.Parent.oContained.w_FRINPUTA = this.RadioValue()
    return .t.
  endfunc

  func oFRINPUTA_1_17.SetRadio()
    this.Parent.oContained.w_FRINPUTA=trim(this.Parent.oContained.w_FRINPUTA)
    this.value = ;
      iif(this.Parent.oContained.w_FRINPUTA=='C',1,;
      iif(this.Parent.oContained.w_FRINPUTA=='D',2,;
      iif(this.Parent.oContained.w_FRINPUTA=='N',3,;
      0)))
  endfunc

  func oFRINPUTA_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_FLEDIT))
    endwith
   endif
  endfunc

  func oFRINPUTA_1_17.mHide()
    with this.Parent.oContained
      return (.w_PREMIFA = 'P')
    endwith
  endfunc


  add object oFROBBLIG_1_18 as StdCombo with uid="ULLWJBGSBS",rtseq=12,rtrep=.f.,left=89,top=71,width=40,height=22;
    , ToolTipText = "Tipologia attributo";
    , HelpContextID = 136458339;
    , cFormVar="w_FROBBLIG",RowSource=""+"Si,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFROBBLIG_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oFROBBLIG_1_18.GetRadio()
    this.Parent.oContained.w_FROBBLIG = this.RadioValue()
    return .t.
  endfunc

  func oFROBBLIG_1_18.SetRadio()
    this.Parent.oContained.w_FROBBLIG=trim(this.Parent.oContained.w_FROBBLIG)
    this.value = ;
      iif(this.Parent.oContained.w_FROBBLIG=='S',1,;
      iif(this.Parent.oContained.w_FROBBLIG=='N',2,;
      0))
  endfunc

  func oFROBBLIG_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_FLEDIT))
    endwith
   endif
  endfunc


  add object oFRMULTIP_1_19 as StdCombo with uid="ZKFCLNRQIR",rtseq=13,rtrep=.f.,left=403,top=71,width=83,height=21;
    , ToolTipText = "Input attributo";
    , HelpContextID = 258953306;
    , cFormVar="w_FRMULTIP",RowSource=""+"Singolo,"+"Multiplo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFRMULTIP_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oFRMULTIP_1_19.GetRadio()
    this.Parent.oContained.w_FRMULTIP = this.RadioValue()
    return .t.
  endfunc

  func oFRMULTIP_1_19.SetRadio()
    this.Parent.oContained.w_FRMULTIP=trim(this.Parent.oContained.w_FRMULTIP)
    this.value = ;
      iif(this.Parent.oContained.w_FRMULTIP=='S',1,;
      iif(this.Parent.oContained.w_FRMULTIP=='M',2,;
      0))
  endfunc

  func oFRMULTIP_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_FLEDIT))
    endwith
   endif
  endfunc

  func oFRMULTIP_1_19.mHide()
    with this.Parent.oContained
      return (.w_PREMIFA = 'P')
    endwith
  endfunc

  add object oFRDIMENS_1_20 as StdField with uid="CALWXNAGZJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FRDIMENS", cQueryName = "FRDIMENS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensioni dell'attributo",;
    HelpContextID = 241950807,;
   bGlobalFont=.t.,;
    Height=21, Width=33, Left=89, Top=99, cSayPict='"99"', cGetPict='"99"'

  func oFRDIMENS_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(ALLTRIM(.w_FR_CAMPO)) AND Empty(.w_FLEDIT))
    endwith
   endif
  endfunc

  func oFRDIMENS_1_20.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='L' OR .w_FRINPUTA='D' OR .w_FRAUTOMA ='S' OR .w_PREMIFA = 'P')
    endwith
  endfunc

  func oFRDIMENS_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(.w_FRINPUTA='N', .w_FRDIMENS<13 AND .w_FRDIMENS>0 , .w_FRDIMENS<21 AND .w_FRDIMENS>0))
    endwith
    return bRes
  endfunc

  add object oFRNUMDEC_1_21 as StdField with uid="GBUMITDPYI",rtseq=15,rtrep=.f.,;
    cFormVar = "w_FRNUMDEC", cQueryName = "FRNUMDEC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero decimali",;
    HelpContextID = 10534809,;
   bGlobalFont=.t.,;
    Height=20, Width=38, Left=403, Top=99, cSayPict='"9"', cGetPict='"9"'

  func oFRNUMDEC_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(ALLTRIM(.w_FR_CAMPO)) AND Empty(.w_FLEDIT))
    endwith
   endif
  endfunc

  func oFRNUMDEC_1_21.mHide()
    with this.Parent.oContained
      return (.w_FRINPUTA<>'N' OR .w_FRAUTOMA ='S')
    endwith
  endfunc

  func oFRNUMDEC_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FRNUMDEC<=3 and .w_FRNUMDEC>=0)
    endwith
    return bRes
  endfunc


  add object oLinkPC_1_35 as stdDynamicChildContainer with uid="MCNYKKVCVC",left=400, top=154, width=112, height=213, bOnScreen=.t.;


  func oLinkPC_1_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_APPLICATION='ADHOC REVOLUTION'  Or .cFunction='Filter')
     endwith
    endif
  endfunc


  add object oObj_1_38 as cp_runprogram with uid="KRDHAQRQWE",left=-1, top=445, width=247,height=19,;
    caption='GSAR1BMF(TIP)',;
   bGlobalFont=.t.,;
    prg="gsar1bmf('TIP')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 4073684


  add object oObj_1_39 as cp_runprogram with uid="CBCNNMHMVQ",left=-1, top=468, width=247,height=19,;
    caption='GSAR1BMF(CAF)',;
   bGlobalFont=.t.,;
    prg="gsar1bmf('CAF')",;
    cEvent = "HasEvent",;
    nPag=1;
    , HelpContextID = 4766164

  add object oFRATTRIC_1_40 as StdCheck with uid="MYTJKOOEIU",rtseq=21,rtrep=.f.,left=368, top=122, caption="Attributo di ricerca",;
    ToolTipText = "Se attivo permette di effettuare ricerche mirate",;
    HelpContextID = 15798375,;
    cFormVar="w_FRATTRIC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFRATTRIC_1_40.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFRATTRIC_1_40.GetRadio()
    this.Parent.oContained.w_FRATTRIC = this.RadioValue()
    return .t.
  endfunc

  func oFRATTRIC_1_40.SetRadio()
    this.Parent.oContained.w_FRATTRIC=trim(this.Parent.oContained.w_FRATTRIC)
    this.value = ;
      iif(this.Parent.oContained.w_FRATTRIC=='S',1,;
      0)
  endfunc

  func oFRATTRIC_1_40.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S' OR .w_PREMIFA = 'P')
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="JHMEJVFKGE",Visible=.t., Left=34, Top=14,;
    Alignment=1, Width=51, Height=18,;
    Caption="Attributo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="LKEDIVZWNT",Visible=.t., Left=56, Top=162,;
    Alignment=1, Width=54, Height=18,;
    Caption="Archivio:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="MUEEDVGVPG",Visible=.t., Left=59, Top=214,;
    Alignment=1, Width=51, Height=18,;
    Caption="Campo:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="PZENVEPMLI",Visible=.t., Left=69, Top=188,;
    Alignment=1, Width=41, Height=18,;
    Caption="Zoom:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="OJYWUNCQKW",Visible=.t., Left=17, Top=43,;
    Alignment=1, Width=68, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_PREMIFA = 'P')
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="NMBOIOBHRA",Visible=.t., Left=271, Top=43,;
    Alignment=1, Width=126, Height=21,;
    Caption="Input attributo:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_PREMIFA = 'P')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="VFPUZUDRYB",Visible=.t., Left=5, Top=99,;
    Alignment=1, Width=80, Height=18,;
    Caption="Dimensioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='L' OR .w_FRINPUTA='D' OR .w_FRAUTOMA ='S' OR .w_PREMIFA = 'P')
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="AFWXCADHFE",Visible=.t., Left=286, Top=97,;
    Alignment=1, Width=111, Height=21,;
    Caption="Numero decimali:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_FRINPUTA<>'N' OR .w_FRAUTOMA ='S')
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="EZEXNWRQLC",Visible=.t., Left=7, Top=71,;
    Alignment=1, Width=78, Height=18,;
    Caption="Obbligatorio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="KVDOXBUFIR",Visible=.t., Left=342, Top=70,;
    Alignment=1, Width=55, Height=21,;
    Caption="Multiplo:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (.w_PREMIFA = 'P')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="BHIZXUNECB",Visible=.t., Left=18, Top=240,;
    Alignment=1, Width=92, Height=18,;
    Caption="Zoom on zoom:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="INWLXYAEMY",Visible=.t., Left=57, Top=266,;
    Alignment=1, Width=53, Height=18,;
    Caption="Attributo:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="AGRFNQBLZP",Visible=.t., Left=10, Top=292,;
    Alignment=1, Width=100, Height=18,;
    Caption="Campo per filtro:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (.w_FRTIPOLO='F' OR .w_FRTIPOLO='M' OR .w_FRTIPOLO='L')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="PFNJPHXSCP",Visible=.t., Left=10, Top=133,;
    Alignment=0, Width=102, Height=18,;
    Caption="Fonte dati"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_37 as StdBox with uid="BLHSUKGPKM",left=8, top=148, width=514,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_aat','FAM_ATTR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FRCODICE=FAM_ATTR.FRCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_aat
Func AskZoom(pParent)
  local l_path, l_customPath
  l_path = sys(5)+sys(2003)
  l_customPath = addbs(l_Path)+'custom'
  cd (l_customPath)
  pParent.w_FR__ZOOM= JUSTSTEM(GETFILE(pParent.w_ESTENSIONE))
  cd (l_path)
EndFunc
* --- Fine Area Manuale
