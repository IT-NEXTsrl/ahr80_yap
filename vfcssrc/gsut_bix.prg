* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bix                                                        *
*              Interpreta file xml                                             *
*                                                                              *
*      Author: Pollina Fabrizio                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_60]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-11-07                                                      *
* Last revis.: 2003-11-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_NomeFile,w_Key,w_Numero
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bix",oParentObject,m.w_NomeFile,m.w_Key,m.w_Numero)
return(i_retval)

define class tgsut_bix as StdBatch
  * --- Local variables
  w_NomeFile = space(254)
  w_Key = space(254)
  w_Numero = 0
  oXML = .NULL.
  oNodeList = .NULL.
  oFieldNodeList = .NULL.
  oNode = .NULL.
  obj = .NULL.
  w_SeekedKey = space(254)
  w_Padre = space(254)
  w_KeyAtt = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Descrizione parametri:
    *        w_NomeFile = Nome completo di percorso del file XML da leggere
    *        w_Key          = Chiave ricercata
    *        w_Numero    = Occorrenza della chiave (In caso di ricerca di valori nei Filgi)
    * --- --------------------------------------------------------------------------------------------------------------------------------------------------------
    * --- w_ErrCode:
    *          0 = Chiave Trovata, valida e valorizzata
    *          1 = Chiave Trovata, valida ma non valorizzata (� un padre)
    *          2 = Chiave Trovata, ma non valida (i padri sono differenti)
    *          3 = Chiave non trovata
    *     
    *     w_Risultato 
    *          Se w_ErrCode � uguale a 0 riporta il valore della chiave ricercata
    *          4 = Impossibile Aprire il file indicato
    * --- --------------------------------------------------------------------------------------------------------------------------------------------------------
    L_OLDERR=ON("ERROR")
    L_Err=.F.
    ON ERROR L_Err=.T.
    this.oXML=createobject("Msxml2.DOMDocument")
    if L_Err
      AH_ERRORMSG("Impossibile trovare componente MSXML2",48)
      i_retcode = 'stop'
      return
    endif
    ON ERROR &L_OLDERR
    * --- Load XML
    this.oXML.Load(this.w_NomeFile)     
    if not isnul(this.oXML.DocumentElement)
      if this.w_Numero=0
        this.w_Padre = LEFT(this.w_Key,RAT("/",this.w_Key)-1)
        this.w_SeekedKey = RIGHT(this.w_KEY,LEN(this.w_Key)-RAT("/",this.w_Key))
        this.oNodeList = this.oXML.DocumentElement.selectNodes(this.w_Padre)
        this.oNode = this.oNodeList.item(this.w_Numero)
        this.obj = this.oNode.selectSingleNode(this.w_SeekedKey)
        if isnul(this.obj)
          this.oParentObject.w_Risultato = ""
          this.oParentObject.w_ErrCode = 3
        else
          this.oParentObject.w_Risultato = this.obj.text
          this.oParentObject.w_ErrCode = 0
        endif
      else
        * --- Select ItemTable
        this.w_Padre = LEFT(this.w_Key,RAT("/",this.w_Key)-1)
        this.w_SeekedKey = RIGHT(this.w_KEY,LEN(this.w_Key)-RAT("/",this.w_Key))
        this.oNodeList = this.oXML.DocumentElement.selectNodes(this.w_Padre)
        this.w_KeyAtt = 0
        do while (this.w_KeyAtt < this.oNodeList.Length) AND (this.w_KeyAtt<>this.w_Numero)
          this.oNode = this.oNodeList.item(this.w_KeyAtt)
          this.w_KeyAtt = this.w_KeyAtt+1
        enddo
        if this.w_KeyAtt=this.w_Numero
          this.obj = this.oNode.selectSingleNode(this.w_SeekedKey)
          if isnul(this.obj)
            this.oParentObject.w_Risultato = ""
            this.oParentObject.w_ErrCode = 3
          else
            this.oParentObject.w_Risultato = this.obj.text
            this.oParentObject.w_ErrCode = 0
          endif
        endif
      endif
    else
      this.oParentObject.w_Risultato = ""
      this.oParentObject.w_ErrCode = 4
    endif
  endproc


  proc Init(oParentObject,w_NomeFile,w_Key,w_Numero)
    this.w_NomeFile=w_NomeFile
    this.w_Key=w_Key
    this.w_Numero=w_Numero
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_NomeFile,w_Key,w_Numero"
endproc
