* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_aas                                                        *
*              Servizi                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_159]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-16                                                      *
* Last revis.: 2018-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_aas"))

* --- Class definition
define class tgsma_aas as StdForm
  Top    = -1
  Left   = 7

  * --- Standard Properties
  Width  = 698
  Height = 570+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-27"
  HelpContextID=143293289
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=142

  * --- Constant Properties
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  VOCIIVA_IDX = 0
  CACOARTI_IDX = 0
  GRUMERC_IDX = 0
  GRUPRO_IDX = 0
  CAT_SCMA_IDX = 0
  CLA_RIGD_IDX = 0
  NOT_ARTI_IDX = 0
  LIS_TINI_IDX = 0
  VOC_COST_IDX = 0
  REP_ARTI_IDX = 0
  TIT_SEZI_IDX = 0
  SOT_SEZI_IDX = 0
  CAT_CESP_IDX = 0
  TIPCODIV_IDX = 0
  TIPCONTR_IDX = 0
  NOMENCLA_IDX = 0
  TIPI_PRE_IDX = 0
  PAR_ALTE_IDX = 0
  NUMAUT_M_IDX = 0
  KEY_ARTI_IDX = 0
  FAS_TARI_IDX = 0
  cFile = "ART_ICOL"
  cKeySelect = "ARCODART"
  cQueryFilter="ARTIPART<>'PF'"
  cKeyWhere  = "ARCODART=this.w_ARCODART"
  cKeyWhereODBC = '"ARCODART="+cp_ToStrODBC(this.w_ARCODART)';

  cKeyWhereODBCqualified = '"ART_ICOL.ARCODART="+cp_ToStrODBC(this.w_ARCODART)';

  cPrg = "gsma_aas"
  cComment = "Servizi"
  icon = "anag.ico"
  cAutoZoom = 'GSMA0AAS'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AUTOAZI = space(5)
  w_AUTO = space(1)
  o_AUTO = space(1)
  w_PARALTE = space(10)
  o_PARALTE = space(10)
  w_NOEDES = space(1)
  w_TIPOPE = space(4)
  w_ARCODART = space(20)
  o_ARCODART = space(20)
  w_ARDESART = space(40)
  w_VALATT = space(20)
  w_TABKEY = space(8)
  w_DELIMM = .F.
  w_ARTIPART = space(2)
  o_ARTIPART = space(2)
  w_ARFLSERG = space(1)
  o_ARFLSERG = space(1)
  w_ARTIPSER = space(1)
  o_ARTIPSER = space(1)
  w_ARASSIVA = space(1)
  w_ARPRESTA = space(1)
  o_ARPRESTA = space(1)
  w_ARDESSUP = space(0)
  w_ARUNMIS1 = space(3)
  w_ARUNMIS2 = space(3)
  o_ARUNMIS2 = space(3)
  w_AROPERAT = space(1)
  w_ARMOLTIP = 0
  w_ARFLUSEP = space(1)
  w_ARFLUNIV = space(1)
  o_ARFLUNIV = space(1)
  w_ARNUMPRE = 0
  w_ARTIPOPE = space(10)
  w_ARCODIVA = space(5)
  w_DESIVA = space(35)
  w_ARCATCON = space(5)
  w_ARPREZUM = space(1)
  w_ARGRUMER = space(5)
  w_ARSTASUP = space(1)
  o_ARSTASUP = space(1)
  w_ARSTACOD = space(1)
  w_ARSTASUP = space(1)
  w_DESCAT = space(35)
  w_ARFLSPAN = space(1)
  o_ARFLSPAN = space(1)
  w_ARSCRITT = space(1)
  o_ARSCRITT = space(1)
  w_ARSTACOD = space(1)
  w_ARFLPRPE = space(1)
  o_ARFLPRPE = space(1)
  w_ARFLESCT = space(1)
  w_DESGRU = space(30)
  w_ARVOCCEN = space(15)
  w_ARVOCRIC = space(15)
  w_ARDTINVA = ctod('  /  /  ')
  w_ARDTOBSO = ctod('  /  /  ')
  w_ARTIPRES = space(5)
  w_ARTIPRIG = space(1)
  o_ARTIPRIG = space(1)
  w_ARTIPRI2 = space(1)
  o_ARTIPRI2 = space(1)
  w_ARCODFAS = space(10)
  w_ARUTISER = space(1)
  o_ARUTISER = space(1)
  w_ARPERSER = space(1)
  w_ARNOMENC = space(8)
  w_UMSUPP = space(3)
  w_ARMOLSUP = 0
  w_ARDATINT = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_CODESE = space(4)
  w_FLSCM = space(1)
  w_FLPRO = space(1)
  w_ARTIPBAR = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESVOC = space(40)
  w_TIPVOC = space(1)
  w_DESRIC = space(40)
  w_TIPRIC = space(1)
  w_ARFLLOTT = space(1)
  w_TIPBAR = space(1)
  w_PERIVA = 0
  w_ARFLESUL = space(1)
  w_ARCATOPE = space(2)
  w_TIDESCRI = space(30)
  w_OMODKEY = space(10)
  w_NMODKEY = space(10)
  w_FILT_UNIMIS = space(1)
  w_UMFLTEMP = space(1)
  w_ODATKEY = ctod('  /  /  ')
  w_NDATKEY = ctod('  /  /  ')
  w_ARMOLSUP = 0
  w_ARUMSUPP = space(3)
  o_ARUMSUPP = space(3)
  w_DESNOM = space(35)
  w_ARCODSPE = space(20)
  w_ARCODANT = space(20)
  w_ARIMPSPE = 0
  w_ARIMPANT = 0
  w_PAPREACC = space(20)
  w_PAPREFOR = space(20)
  w_PAPREACC_ART = space(20)
  w_PAPREFOR_ART = space(20)
  w_LeggePAR_ALTE = space(20)
  w_ARPARAME = space(1)
  w_ARSPORIG = 0
  w_ARSPCOPY = 0
  w_ARSALCOM = space(1)
  w_FLFRAZ = space(1)
  w_F2FRAZ = space(1)
  w_ARFLPECO = space(1)
  w_ARTIPPKR = space(2)
  w_CODI = space(20)
  w_DESC = space(40)
  w_ARGRUPRO = space(5)
  w_DESGPP = space(35)
  w_ARCATSCM = space(5)
  w_DESSCM = space(35)
  w_ARCODCLA = space(3)
  w_DESCLA = space(30)
  w_ARCODREP = space(3)
  o_ARCODREP = space(3)
  w_DESREP = space(40)
  w_ARCODGRU = space(5)
  w_ARCODSOT = space(5)
  w_ARFLCESP = space(1)
  o_ARFLCESP = space(1)
  w_ARCATCES = space(15)
  w_ARCONTRI = space(15)
  o_ARCONTRI = space(15)
  w_ARMINVEN = 0
  w_TPAPPPES = space(1)
  o_TPAPPPES = space(1)
  w_ARRIPCON = space(1)
  w_ARARTPOS = space(1)
  o_ARARTPOS = space(1)
  w_ARPUBWEB = space(1)
  w_ARPUBWEB = space(1)
  w_ARPUBWEB = space(1)
  w_DESSOT = space(35)
  w_DESTIT = space(35)
  w_IVAREP = space(5)
  w_PERIVR = 0
  w_DESCACES = space(40)
  w_DESTIPCO = space(40)
  w_OLDPUBWEB = space(1)
  w_GESTGUID = space(14)
  w_GEST = space(10)
  o_GEST = space(10)
  w_CODATT = space(10)
  w_IMMODATR = space(10)
  w_CODI = space(20)
  w_DESC = space(40)
  w_CODI = space(20)
  w_DESC = space(40)
  w_ARCODTIP = space(35)
  w_ARCODCLF = space(5)
  w_ARCODVAL = space(35)
  w_OFATELE = space(75)
  w_NFATELE = space(75)
  w_ARFLGESC = space(1)

  * --- Children pointers
  GSMA_MLI = .NULL.
  GSAR_MR2 = .NULL.
  gsma_mtt = .NULL.
  GSMA_ANO = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=6, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ART_ICOL','gsma_aas')
    stdPageFrame::Init()
    *set procedure to GSMA_MLI additive
    with this
      .Pages(1).addobject("oPag","tgsma_aasPag1","gsma_aas",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generali")
      .Pages(1).HelpContextID = 268316977
      .Pages(2).addobject("oPag","tgsma_aasPag2","gsma_aas",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Commerciali")
      .Pages(2).HelpContextID = 4246175
      .Pages(3).addobject("oPag","tgsma_aasPag3","gsma_aas",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Premi")
      .Pages(3).HelpContextID = 25605130
      .Pages(4).addobject("oPag","tgsma_aasPag4","gsma_aas",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Listini")
      .Pages(4).HelpContextID = 58645834
      .Pages(5).addobject("oPag","tgsma_aasPag5","gsma_aas",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Note")
      .Pages(5).HelpContextID = 136169258
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oARCODART_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSMA_MLI
    * --- Area Manuale = Init Page Frame
    * --- gsma_aas
    WITH THIS.PARENT
     If IsAlt()
       .cComment = CP_TRANSLATE('Tariffario')
     Else
       .cComment = CP_TRANSLATE('Servizi')
     Endif
    Endwith
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[23]
    this.cWorkTables[1]='UNIMIS'
    this.cWorkTables[2]='VOCIIVA'
    this.cWorkTables[3]='CACOARTI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='GRUPRO'
    this.cWorkTables[6]='CAT_SCMA'
    this.cWorkTables[7]='CLA_RIGD'
    this.cWorkTables[8]='NOT_ARTI'
    this.cWorkTables[9]='LIS_TINI'
    this.cWorkTables[10]='VOC_COST'
    this.cWorkTables[11]='REP_ARTI'
    this.cWorkTables[12]='TIT_SEZI'
    this.cWorkTables[13]='SOT_SEZI'
    this.cWorkTables[14]='CAT_CESP'
    this.cWorkTables[15]='TIPCODIV'
    this.cWorkTables[16]='TIPCONTR'
    this.cWorkTables[17]='NOMENCLA'
    this.cWorkTables[18]='TIPI_PRE'
    this.cWorkTables[19]='PAR_ALTE'
    this.cWorkTables[20]='NUMAUT_M'
    this.cWorkTables[21]='KEY_ARTI'
    this.cWorkTables[22]='FAS_TARI'
    this.cWorkTables[23]='ART_ICOL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(23))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ART_ICOL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ART_ICOL_IDX,3]
  return

  function CreateChildren()
    this.GSMA_MLI = CREATEOBJECT('stdLazyChild',this,'GSMA_MLI')
    this.GSAR_MR2 = CREATEOBJECT('stdDynamicChild',this,'GSAR_MR2',this.oPgFrm.Page3.oPag.oLinkPC_3_3)
    this.GSAR_MR2.createrealchild()
    this.gsma_mtt = CREATEOBJECT('stdDynamicChild',this,'gsma_mtt',this.oPgFrm.Page4.oPag.oLinkPC_4_4)
    this.GSMA_ANO = CREATEOBJECT('stdDynamicChild',this,'GSMA_ANO',this.oPgFrm.Page5.oPag.oLinkPC_5_4)
    this.GSMA_ANO.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSMA_MLI)
      this.GSMA_MLI.DestroyChildrenChain()
      this.GSMA_MLI=.NULL.
    endif
    if !ISNULL(this.GSAR_MR2)
      this.GSAR_MR2.DestroyChildrenChain()
      this.GSAR_MR2=.NULL.
    endif
    this.oPgFrm.Page3.oPag.RemoveObject('oLinkPC_3_3')
    if !ISNULL(this.gsma_mtt)
      this.gsma_mtt.DestroyChildrenChain()
      this.gsma_mtt=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_4')
    if !ISNULL(this.GSMA_ANO)
      this.GSMA_ANO.DestroyChildrenChain()
      this.GSMA_ANO=.NULL.
    endif
    this.oPgFrm.Page5.oPag.RemoveObject('oLinkPC_5_4')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMA_MLI.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MR2.IsAChildUpdated()
      i_bRes = i_bRes .or. this.gsma_mtt.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSMA_ANO.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMA_MLI.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MR2.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.gsma_mtt.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSMA_ANO.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMA_MLI.NewDocument()
    this.GSAR_MR2.NewDocument()
    this.gsma_mtt.NewDocument()
    this.GSMA_ANO.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSMA_MLI.SetKey(;
            .w_ARCODART,"LICODART";
            )
      this.GSAR_MR2.SetKey(;
            .w_GESTGUID,"GUID";
            )
      this.gsma_mtt.SetKey(;
            .w_ARCODART,"TACODART";
            )
      this.GSMA_ANO.SetKey(;
            .w_ARCODART,"ARCODART";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSMA_MLI.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"LICODART";
             )
      .GSAR_MR2.ChangeRow(this.cRowID+'      1',1;
             ,.w_GESTGUID,"GUID";
             )
      .WriteTo_GSAR_MR2()
      .gsma_mtt.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"TACODART";
             )
      .WriteTo_gsma_mtt()
      .GSMA_ANO.ChangeRow(this.cRowID+'      1',1;
             ,.w_ARCODART,"ARCODART";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAR_MR2)
        i_f=.GSAR_MR2.BuildFilter()
        if !(i_f==.GSAR_MR2.cQueryFilter)
          i_fnidx=.GSAR_MR2.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MR2.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MR2.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MR2.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MR2.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.gsma_mtt)
        i_f=.gsma_mtt.BuildFilter()
        if !(i_f==.gsma_mtt.cQueryFilter)
          i_fnidx=.gsma_mtt.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.gsma_mtt.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.gsma_mtt.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.gsma_mtt.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.gsma_mtt.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSMA_ANO)
        i_f=.GSMA_ANO.BuildFilter()
        if !(i_f==.GSMA_ANO.cQueryFilter)
          i_fnidx=.GSMA_ANO.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSMA_ANO.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSMA_ANO.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSMA_ANO.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSMA_ANO.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSAR_MR2()
  if at('gsar_mr2',lower(this.GSAR_MR2.class))<>0
    if this.GSAR_MR2.w_GEST<>this.w_GEST
      this.GSAR_MR2.w_GEST = this.w_GEST
      this.GSAR_MR2.mCalc(.t.)
    endif
  endif
  return

procedure WriteTo_gsma_mtt()
  if at('gsma_mtt',lower(this.gsma_mtt.class))<>0
    if this.gsma_mtt.w_PRESTA<>this.w_ARPRESTA
      this.gsma_mtt.w_PRESTA = this.w_ARPRESTA
      this.gsma_mtt.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ARCODART = NVL(ARCODART,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_26_joined
    link_1_26_joined=.f.
    local link_1_28_joined
    link_1_28_joined=.f.
    local link_1_30_joined
    link_1_30_joined=.f.
    local link_1_41_joined
    link_1_41_joined=.f.
    local link_1_42_joined
    link_1_42_joined=.f.
    local link_1_51_joined
    link_1_51_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    local link_2_10_joined
    link_2_10_joined=.f.
    local link_2_13_joined
    link_2_13_joined=.f.
    local link_2_16_joined
    link_2_16_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    local link_2_19_joined
    link_2_19_joined=.f.
    local link_2_20_joined
    link_2_20_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ART_ICOL where ARCODART=KeySet.ARCODART
    *
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ART_ICOL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ART_ICOL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ART_ICOL '
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_28_joined=this.AddJoinedLink_1_28(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_30_joined=this.AddJoinedLink_1_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_41_joined=this.AddJoinedLink_1_41(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_42_joined=this.AddJoinedLink_1_42(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_51_joined=this.AddJoinedLink_1_51(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_10_joined=this.AddJoinedLink_2_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_16_joined=this.AddJoinedLink_2_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_19_joined=this.AddJoinedLink_2_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_20_joined=this.AddJoinedLink_2_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ARCODART',this.w_ARCODART  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AUTOAZI = i_CODAZI
        .w_AUTO = space(1)
        .w_PARALTE = i_CODAZI
        .w_NOEDES = space(1)
        .w_VALATT = .w_ARCODART
        .w_TABKEY = 'ART_ICOL'
        .w_DELIMM = .f.
        .w_DESIVA = space(35)
        .w_DESCAT = space(35)
        .w_DESGRU = space(30)
        .w_UMSUPP = space(3)
        .w_FLSCM = space(1)
        .w_FLPRO = space(1)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESVOC = space(40)
        .w_TIPVOC = space(1)
        .w_DESRIC = space(40)
        .w_TIPRIC = space(1)
        .w_TIPBAR = '0'
        .w_PERIVA = 0
        .w_TIDESCRI = space(30)
        .w_OMODKEY = space(10)
        .w_UMFLTEMP = space(1)
        .w_ODATKEY = ctod("  /  /  ")
        .w_DESNOM = space(35)
        .w_PAPREACC = space(20)
        .w_PAPREFOR = space(20)
        .w_PAPREACC_ART = space(20)
        .w_PAPREFOR_ART = space(20)
        .w_LeggePAR_ALTE = i_CodAzi
        .w_FLFRAZ = space(1)
        .w_F2FRAZ = space(1)
        .w_DESGPP = space(35)
        .w_DESSCM = space(35)
        .w_DESCLA = space(30)
        .w_DESREP = space(40)
        .w_TPAPPPES = space(1)
        .w_DESSOT = space(35)
        .w_DESTIT = space(35)
        .w_IVAREP = space(5)
        .w_PERIVR = 0
        .w_DESCACES = space(40)
        .w_DESTIPCO = space(40)
        .w_GEST = IIF( g_GPFA<> "S", SPACE(10), get_GPFA_code("MODELLO") )
        .w_CODATT = IIF( g_GPFA<> "S", SPACE(10), get_GPFA_code("GRUPPO") )
        .w_IMMODATR = IIF( g_GPFA<> "S", SPACE(10), get_GPFA_code("MODELLO") )
          .link_1_1('Load')
          .link_1_3('Load')
        .w_TIPOPE = this.cFunction
        .w_ARCODART = NVL(ARCODART,space(20))
        .w_ARDESART = NVL(ARDESART,space(40))
        .w_ARTIPART = NVL(ARTIPART,space(2))
        .w_ARFLSERG = NVL(ARFLSERG,space(1))
        .w_ARTIPSER = NVL(ARTIPSER,space(1))
        .w_ARASSIVA = NVL(ARASSIVA,space(1))
        .w_ARPRESTA = NVL(ARPRESTA,space(1))
        .w_ARDESSUP = NVL(ARDESSUP,space(0))
        .w_ARUNMIS1 = NVL(ARUNMIS1,space(3))
          if link_1_17_joined
            this.w_ARUNMIS1 = NVL(UMCODICE117,NVL(this.w_ARUNMIS1,space(3)))
            this.w_UMFLTEMP = NVL(UMFLTEMP117,space(1))
            this.w_FLFRAZ = NVL(UMFLFRAZ117,space(1))
          else
          .link_1_17('Load')
          endif
        .w_ARUNMIS2 = NVL(ARUNMIS2,space(3))
          if link_1_18_joined
            this.w_ARUNMIS2 = NVL(UMCODICE118,NVL(this.w_ARUNMIS2,space(3)))
            this.w_F2FRAZ = NVL(UMFLFRAZ118,space(1))
          else
          .link_1_18('Load')
          endif
        .w_AROPERAT = NVL(AROPERAT,space(1))
        .w_ARMOLTIP = NVL(ARMOLTIP,0)
        .w_ARFLUSEP = NVL(ARFLUSEP,space(1))
        .w_ARFLUNIV = NVL(ARFLUNIV,space(1))
        .w_ARNUMPRE = NVL(ARNUMPRE,0)
        .w_ARTIPOPE = NVL(ARTIPOPE,space(10))
          .link_1_25('Load')
        .w_ARCODIVA = NVL(ARCODIVA,space(5))
          if link_1_26_joined
            this.w_ARCODIVA = NVL(IVCODIVA126,NVL(this.w_ARCODIVA,space(5)))
            this.w_DESIVA = NVL(IVDESIVA126,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO126),ctod("  /  /  "))
            this.w_PERIVA = NVL(IVPERIVA126,0)
          else
          .link_1_26('Load')
          endif
        .w_ARCATCON = NVL(ARCATCON,space(5))
          if link_1_28_joined
            this.w_ARCATCON = NVL(C1CODICE128,NVL(this.w_ARCATCON,space(5)))
            this.w_DESCAT = NVL(C1DESCRI128,space(35))
          else
          .link_1_28('Load')
          endif
        .w_ARPREZUM = NVL(ARPREZUM,space(1))
        .w_ARGRUMER = NVL(ARGRUMER,space(5))
          if link_1_30_joined
            this.w_ARGRUMER = NVL(GMCODICE130,NVL(this.w_ARGRUMER,space(5)))
            this.w_DESGRU = NVL(GMDESCRI130,space(30))
          else
          .link_1_30('Load')
          endif
        .w_ARSTASUP = NVL(ARSTASUP,space(1))
        .w_ARSTACOD = NVL(ARSTACOD,space(1))
        .w_ARSTASUP = NVL(ARSTASUP,space(1))
        .w_ARFLSPAN = NVL(ARFLSPAN,space(1))
        .w_ARSCRITT = NVL(ARSCRITT,space(1))
        .w_ARSTACOD = NVL(ARSTACOD,space(1))
        .w_ARFLPRPE = NVL(ARFLPRPE,space(1))
        .w_ARFLESCT = NVL(ARFLESCT,space(1))
        .w_ARVOCCEN = NVL(ARVOCCEN,space(15))
          if link_1_41_joined
            this.w_ARVOCCEN = NVL(VCCODICE141,NVL(this.w_ARVOCCEN,space(15)))
            this.w_DESVOC = NVL(VCDESCRI141,space(40))
            this.w_TIPVOC = NVL(VCTIPVOC141,space(1))
          else
          .link_1_41('Load')
          endif
        .w_ARVOCRIC = NVL(ARVOCRIC,space(15))
          if link_1_42_joined
            this.w_ARVOCRIC = NVL(VCCODICE142,NVL(this.w_ARVOCRIC,space(15)))
            this.w_DESRIC = NVL(VCDESCRI142,space(40))
            this.w_TIPRIC = NVL(VCTIPVOC142,space(1))
          else
          .link_1_42('Load')
          endif
        .w_ARDTINVA = NVL(cp_ToDate(ARDTINVA),ctod("  /  /  "))
        .w_ARDTOBSO = NVL(cp_ToDate(ARDTOBSO),ctod("  /  /  "))
        .w_ARTIPRES = NVL(ARTIPRES,space(5))
          * evitabile
          *.link_1_45('Load')
        .w_ARTIPRIG = NVL(ARTIPRIG,space(1))
        .w_ARTIPRI2 = NVL(ARTIPRI2,space(1))
        .w_ARCODFAS = NVL(ARCODFAS,space(10))
          * evitabile
          *.link_1_48('Load')
        .w_ARUTISER = NVL(ARUTISER,space(1))
        .w_ARPERSER = NVL(ARPERSER,space(1))
        .w_ARNOMENC = NVL(ARNOMENC,space(8))
          if link_1_51_joined
            this.w_ARNOMENC = NVL(NMCODICE151,NVL(this.w_ARNOMENC,space(8)))
            this.w_DESNOM = NVL(NMDESCRI151,space(35))
            this.w_ARUMSUPP = NVL(NMUNISUP151,space(3))
            this.w_UMSUPP = NVL(NMUNISUP151,space(3))
          else
          .link_1_51('Load')
          endif
        .w_ARMOLSUP = NVL(ARMOLSUP,0)
        .w_ARDATINT = NVL(ARDATINT,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_CODESE = g_CODESE
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .w_ARTIPBAR = NVL(ARTIPBAR,space(1))
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .w_ARFLLOTT = NVL(ARFLLOTT,space(1))
        .w_ARFLESUL = NVL(ARFLESUL,space(1))
        .w_ARCATOPE = NVL(ARCATOPE,space(2))
        .w_NMODKEY = .w_ARDESART+ .w_ARDESSUP+ DTOC(.w_ARDTINVA)
        .w_FILT_UNIMIS = IIF(.w_ARPRESTA='P', 'S', ' ')
        .w_NDATKEY = .w_ARDTOBSO
        .w_ARMOLSUP = NVL(ARMOLSUP,0)
        .w_ARUMSUPP = NVL(ARUMSUPP,space(3))
          * evitabile
          *.link_1_112('Load')
        .w_ARCODSPE = NVL(ARCODSPE,space(20))
        .w_ARCODANT = NVL(ARCODANT,space(20))
        .w_ARIMPSPE = NVL(ARIMPSPE,0)
        .w_ARIMPANT = NVL(ARIMPANT,0)
          .link_1_132('Load')
          .link_1_133('Load')
          .link_1_136('Load')
        .w_ARPARAME = NVL(ARPARAME,space(1))
        .w_ARSPORIG = NVL(ARSPORIG,0)
        .w_ARSPCOPY = NVL(ARSPCOPY,0)
        .w_ARSALCOM = NVL(ARSALCOM,space(1))
        .w_ARFLPECO = NVL(ARFLPECO,space(1))
        .w_ARTIPPKR = NVL(ARTIPPKR,space(2))
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_ARGRUPRO = NVL(ARGRUPRO,space(5))
          if link_2_4_joined
            this.w_ARGRUPRO = NVL(GPCODICE204,NVL(this.w_ARGRUPRO,space(5)))
            this.w_DESGPP = NVL(GPDESCRI204,space(35))
            this.w_FLPRO = NVL(GPTIPPRO204,space(1))
          else
          .link_2_4('Load')
          endif
        .w_ARCATSCM = NVL(ARCATSCM,space(5))
          if link_2_6_joined
            this.w_ARCATSCM = NVL(CSCODICE206,NVL(this.w_ARCATSCM,space(5)))
            this.w_DESSCM = NVL(CSDESCRI206,space(35))
            this.w_FLSCM = NVL(CSTIPCAT206,space(1))
          else
          .link_2_6('Load')
          endif
        .w_ARCODCLA = NVL(ARCODCLA,space(3))
          if link_2_10_joined
            this.w_ARCODCLA = NVL(TRCODCLA210,NVL(this.w_ARCODCLA,space(3)))
            this.w_DESCLA = NVL(TRDESCLA210,space(30))
          else
          .link_2_10('Load')
          endif
        .w_ARCODREP = NVL(ARCODREP,space(3))
          if link_2_13_joined
            this.w_ARCODREP = NVL(RECODREP213,NVL(this.w_ARCODREP,space(3)))
            this.w_DESREP = NVL(REDESREP213,space(40))
            this.w_IVAREP = NVL(RECODIVA213,space(5))
          else
          .link_2_13('Load')
          endif
        .w_ARCODGRU = NVL(ARCODGRU,space(5))
          if link_2_16_joined
            this.w_ARCODGRU = NVL(TSCODICE216,NVL(this.w_ARCODGRU,space(5)))
            this.w_DESTIT = NVL(TSDESCRI216,space(35))
          else
          .link_2_16('Load')
          endif
        .w_ARCODSOT = NVL(ARCODSOT,space(5))
          if link_2_17_joined
            this.w_ARCODSOT = NVL(SGCODSOT217,NVL(this.w_ARCODSOT,space(5)))
            this.w_DESSOT = NVL(SGDESCRI217,space(35))
          else
          .link_2_17('Load')
          endif
        .w_ARFLCESP = NVL(ARFLCESP,space(1))
        .w_ARCATCES = NVL(ARCATCES,space(15))
          if link_2_19_joined
            this.w_ARCATCES = NVL(CCCODICE219,NVL(this.w_ARCATCES,space(15)))
            this.w_DESCACES = NVL(CCDESCRI219,space(40))
          else
          .link_2_19('Load')
          endif
        .w_ARCONTRI = NVL(ARCONTRI,space(15))
          if link_2_20_joined
            this.w_ARCONTRI = NVL(TPCODICE220,NVL(this.w_ARCONTRI,space(15)))
            this.w_DESTIPCO = NVL(TPDESCRI220,space(40))
            this.w_TPAPPPES = NVL(TPAPPPES220,space(1))
          else
          .link_2_20('Load')
          endif
        .w_ARMINVEN = NVL(ARMINVEN,0)
        .w_ARRIPCON = NVL(ARRIPCON,space(1))
        .w_ARARTPOS = NVL(ARARTPOS,space(1))
        .w_ARPUBWEB = NVL(ARPUBWEB,space(1))
        .w_ARPUBWEB = NVL(ARPUBWEB,space(1))
        .w_ARPUBWEB = NVL(ARPUBWEB,space(1))
          .link_2_32('Load')
        .w_OLDPUBWEB = .w_ARPUBWEB
        .w_GESTGUID = NVL(GESTGUID,space(14))
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_ARCODTIP = NVL(ARCODTIP,space(35))
        .w_ARCODCLF = NVL(ARCODCLF,space(5))
        .w_ARCODVAL = NVL(ARCODVAL,space(35))
        .w_OFATELE = '#'+ALLTRIM(.w_ARCODTIP)+'#'+ALLTRIM(.w_ARCODVAL)+'#'+ALLTRIM(.w_ARCODCLF)+'#'+ALLTRIM(.w_ARFLGESC)
        .w_NFATELE = '#'+ALLTRIM(.w_ARCODTIP)+'#'+ALLTRIM(.w_ARCODVAL)+'#'+ALLTRIM(.w_ARCODCLF)+'#'+ALLTRIM(.w_ARFLGESC)
        .w_ARFLGESC = NVL(ARFLGESC,space(1))
        cp_LoadRecExtFlds(this,'ART_ICOL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_72.enabled = this.oPgFrm.Page1.oPag.oBtn_1_72.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_98.enabled = this.oPgFrm.Page1.oPag.oBtn_1_98.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_99.enabled = this.oPgFrm.Page1.oPag.oBtn_1_99.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_100.enabled = this.oPgFrm.Page1.oPag.oBtn_1_100.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_101.enabled = this.oPgFrm.Page1.oPag.oBtn_1_101.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_103.enabled = this.oPgFrm.Page1.oPag.oBtn_1_103.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_114.enabled = this.oPgFrm.Page1.oPag.oBtn_1_114.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsma_aas
    if NOT this.w_ARTIPART $ 'FM-FO-DE'
      this.BlankRec()
    endif
    
    * -- Per inizializzare il vecchio valore.
    * ---Per controllo cambiamento valori e aggiornamento chiave di ricerca
    with this
    .w_OMODKEY = .w_ARDESART+.w_ARDESSUP+DTOC(.w_ARDTINVA)
    .w_ODATKEY = .w_ARDTOBSO
    endwith
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsma_aas
    * --- Per caricamento rapido: eseguo i metodi usati nella BlankRec senza per�
    * --- sbiancare le variabili
    IF g_PERART='S' AND this.cFunction='Load' AND NOT EMPTY(this.w_ARCODART)
       This.w_ARCODART=iif(this.cFunction='Load' AND this.w_AUTO='S', 'AUTO',Space(20)) 
       This.w_ARNOMENC=Space(6)
       This.w_DESNOM=Space(10)
    
       * --- sbianco la data obsolescenza se inferiore ad oggi, e rivalorizzo data creazione
       * --- e utente che ha creato l'articolo
       this.w_ARDTOBSO=IIf( this.w_ARDTOBSO<=i_DATSYS , cp_CharToDate('  -  -    ') , this.w_ARDTOBSO )
       this.w_UTDC=SetInfoDate(this.cCALUTD)
       this.w_UTCC=i_CODUTE
       this.w_UTCV=0
       this.w_UTDV={}
       this.w_GESTGUID=space(14)
        
       * --- Inizio: assegnamenti per far si che i figli integrati vengano salvati
       this.GSMA_ANO.bLoaded=.f.
       this.GSMA_ANO.bUpdated=.t.
       * --- Non carico gli attributi (Premi di fine anno) del vecchio articolo
       this.GSAR_MR2.NewDocument()
       * --- Non carico i Listini del vecchio articolo
       this.GSMA_MLI.NewDocument()
       IF IsAlt()
          this.GSMA_MTT.NewDocument()
       Endif
          * --- Fine
       this.SaveDependsOn()
       this.SetControlsValue()
       this.mHideControls()
       this.ChildrenChangeRow()
       this.NotifyEvent('Blank')
       this.mCalc(.t.)
       return
    Endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AUTOAZI = space(5)
      .w_AUTO = space(1)
      .w_PARALTE = space(10)
      .w_NOEDES = space(1)
      .w_TIPOPE = space(4)
      .w_ARCODART = space(20)
      .w_ARDESART = space(40)
      .w_VALATT = space(20)
      .w_TABKEY = space(8)
      .w_DELIMM = .f.
      .w_ARTIPART = space(2)
      .w_ARFLSERG = space(1)
      .w_ARTIPSER = space(1)
      .w_ARASSIVA = space(1)
      .w_ARPRESTA = space(1)
      .w_ARDESSUP = space(0)
      .w_ARUNMIS1 = space(3)
      .w_ARUNMIS2 = space(3)
      .w_AROPERAT = space(1)
      .w_ARMOLTIP = 0
      .w_ARFLUSEP = space(1)
      .w_ARFLUNIV = space(1)
      .w_ARNUMPRE = 0
      .w_ARTIPOPE = space(10)
      .w_ARCODIVA = space(5)
      .w_DESIVA = space(35)
      .w_ARCATCON = space(5)
      .w_ARPREZUM = space(1)
      .w_ARGRUMER = space(5)
      .w_ARSTASUP = space(1)
      .w_ARSTACOD = space(1)
      .w_ARSTASUP = space(1)
      .w_DESCAT = space(35)
      .w_ARFLSPAN = space(1)
      .w_ARSCRITT = space(1)
      .w_ARSTACOD = space(1)
      .w_ARFLPRPE = space(1)
      .w_ARFLESCT = space(1)
      .w_DESGRU = space(30)
      .w_ARVOCCEN = space(15)
      .w_ARVOCRIC = space(15)
      .w_ARDTINVA = ctod("  /  /  ")
      .w_ARDTOBSO = ctod("  /  /  ")
      .w_ARTIPRES = space(5)
      .w_ARTIPRIG = space(1)
      .w_ARTIPRI2 = space(1)
      .w_ARCODFAS = space(10)
      .w_ARUTISER = space(1)
      .w_ARPERSER = space(1)
      .w_ARNOMENC = space(8)
      .w_UMSUPP = space(3)
      .w_ARMOLSUP = 0
      .w_ARDATINT = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_CODESE = space(4)
      .w_FLSCM = space(1)
      .w_FLPRO = space(1)
      .w_ARTIPBAR = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DESVOC = space(40)
      .w_TIPVOC = space(1)
      .w_DESRIC = space(40)
      .w_TIPRIC = space(1)
      .w_ARFLLOTT = space(1)
      .w_TIPBAR = space(1)
      .w_PERIVA = 0
      .w_ARFLESUL = space(1)
      .w_ARCATOPE = space(2)
      .w_TIDESCRI = space(30)
      .w_OMODKEY = space(10)
      .w_NMODKEY = space(10)
      .w_FILT_UNIMIS = space(1)
      .w_UMFLTEMP = space(1)
      .w_ODATKEY = ctod("  /  /  ")
      .w_NDATKEY = ctod("  /  /  ")
      .w_ARMOLSUP = 0
      .w_ARUMSUPP = space(3)
      .w_DESNOM = space(35)
      .w_ARCODSPE = space(20)
      .w_ARCODANT = space(20)
      .w_ARIMPSPE = 0
      .w_ARIMPANT = 0
      .w_PAPREACC = space(20)
      .w_PAPREFOR = space(20)
      .w_PAPREACC_ART = space(20)
      .w_PAPREFOR_ART = space(20)
      .w_LeggePAR_ALTE = space(20)
      .w_ARPARAME = space(1)
      .w_ARSPORIG = 0
      .w_ARSPCOPY = 0
      .w_ARSALCOM = space(1)
      .w_FLFRAZ = space(1)
      .w_F2FRAZ = space(1)
      .w_ARFLPECO = space(1)
      .w_ARTIPPKR = space(2)
      .w_CODI = space(20)
      .w_DESC = space(40)
      .w_ARGRUPRO = space(5)
      .w_DESGPP = space(35)
      .w_ARCATSCM = space(5)
      .w_DESSCM = space(35)
      .w_ARCODCLA = space(3)
      .w_DESCLA = space(30)
      .w_ARCODREP = space(3)
      .w_DESREP = space(40)
      .w_ARCODGRU = space(5)
      .w_ARCODSOT = space(5)
      .w_ARFLCESP = space(1)
      .w_ARCATCES = space(15)
      .w_ARCONTRI = space(15)
      .w_ARMINVEN = 0
      .w_TPAPPPES = space(1)
      .w_ARRIPCON = space(1)
      .w_ARARTPOS = space(1)
      .w_ARPUBWEB = space(1)
      .w_ARPUBWEB = space(1)
      .w_ARPUBWEB = space(1)
      .w_DESSOT = space(35)
      .w_DESTIT = space(35)
      .w_IVAREP = space(5)
      .w_PERIVR = 0
      .w_DESCACES = space(40)
      .w_DESTIPCO = space(40)
      .w_OLDPUBWEB = space(1)
      .w_GESTGUID = space(14)
      .w_GEST = space(10)
      .w_CODATT = space(10)
      .w_IMMODATR = space(10)
      .w_CODI = space(20)
      .w_DESC = space(40)
      .w_CODI = space(20)
      .w_DESC = space(40)
      .w_ARCODTIP = space(35)
      .w_ARCODCLF = space(5)
      .w_ARCODVAL = space(35)
      .w_OFATELE = space(75)
      .w_NFATELE = space(75)
      .w_ARFLGESC = space(1)
      if .cFunction<>"Filter"
        .w_AUTOAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AUTOAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_PARALTE = i_CODAZI
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_PARALTE))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,4,.f.)
        .w_TIPOPE = this.cFunction
        .w_ARCODART = iif(.cFunction='Load' AND .w_AUTO='S', 'AUTO',.w_ARCODART)
          .DoRTCalc(7,7,.f.)
        .w_VALATT = .w_ARCODART
        .w_TABKEY = 'ART_ICOL'
        .w_DELIMM = .f.
        .w_ARTIPART = IIF(IsAlt(),'FM','FO')
        .w_ARFLSERG = IIF(.w_ARTIPART='FM',.w_ARFLSERG,' ')
        .w_ARTIPSER = IIF(.w_ARTIPART='DE' , '' ,.w_ARTIPSER )
        .w_ARASSIVA = 'S'
        .w_ARPRESTA = 'G'
          .DoRTCalc(16,16,.f.)
        .w_ARUNMIS1 = SPACE(3)
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_ARUNMIS1))
          .link_1_17('Full')
          endif
        .w_ARUNMIS2 = SPACE(3)
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_ARUNMIS2))
          .link_1_18('Full')
          endif
        .w_AROPERAT = '*'
        .w_ARMOLTIP = 0
        .w_ARFLUSEP = ' '
        .w_ARFLUNIV = 'N'
        .w_ARNUMPRE = 1
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_ARTIPOPE))
          .link_1_25('Full')
          endif
        .w_ARCODIVA = IIF( .w_ARTIPART<>"DE" , .w_ARCODIVA , SPACE( 5 ) )
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_ARCODIVA))
          .link_1_26('Full')
          endif
          .DoRTCalc(26,26,.f.)
        .w_ARCATCON = IIF( .w_ARTIPART<>"DE" , .w_ARCATCON , SPACE( 5 ) )
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_ARCATCON))
          .link_1_28('Full')
          endif
        .w_ARPREZUM = IIF(.w_ARFLSERG='S', 'S','N')
        .w_ARGRUMER = IIF( .w_ARTIPART<>'DE' , .w_ARGRUMER , SPACE( 5 ) )
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_ARGRUMER))
          .link_1_30('Full')
          endif
        .w_ARSTASUP = ' '
          .DoRTCalc(31,31,.f.)
        .w_ARSTASUP = ' '
          .DoRTCalc(33,33,.f.)
        .w_ARFLSPAN = 'N'
        .w_ARSCRITT = 'N'
        .w_ARSTACOD = ' '
        .w_ARFLPRPE = IIF(.w_ARSTASUP='S',' ',.w_ARFLPRPE)
        .w_ARFLESCT = iif(.w_ARPRESTA $ 'S-A','N','S')
          .DoRTCalc(39,39,.f.)
        .w_ARVOCCEN = IIF( .w_ARTIPART<>"DE" , .w_ARVOCCEN , SPACE( 15 ) )
        .DoRTCalc(40,40,.f.)
          if not(empty(.w_ARVOCCEN))
          .link_1_41('Full')
          endif
        .w_ARVOCRIC = IIF( .w_ARTIPART<>"DE" , .w_ARVOCRIC , SPACE( 15 ) )
        .DoRTCalc(41,41,.f.)
          if not(empty(.w_ARVOCRIC))
          .link_1_42('Full')
          endif
          .DoRTCalc(42,43,.f.)
        .w_ARTIPRES = Tippred('P')
        .DoRTCalc(44,44,.f.)
          if not(empty(.w_ARTIPRES))
          .link_1_45('Full')
          endif
        .w_ARTIPRIG = IIF(EMPTY(.w_ARTIPRIG),'D',.w_ARTIPRIG)
        .w_ARTIPRI2 = IIF(EMPTY(.w_ARTIPRI2),'D',.w_ARTIPRI2)
        .DoRTCalc(47,47,.f.)
          if not(empty(.w_ARCODFAS))
          .link_1_48('Full')
          endif
        .w_ARUTISER = 'N'
        .w_ARPERSER = 'I'
        .w_ARNOMENC = ''
        .DoRTCalc(50,50,.f.)
          if not(empty(.w_ARNOMENC))
          .link_1_51('Full')
          endif
          .DoRTCalc(51,51,.f.)
        .w_ARMOLSUP = 0
        .w_ARDATINT = IIF(.w_ARUTISER='N', IIF(EMPTY(.w_ARDATINT), 'S', .w_ARDATINT ), 'N')
          .DoRTCalc(54,57,.f.)
        .w_CODESE = g_CODESE
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
          .DoRTCalc(59,60,.f.)
        .w_ARTIPBAR = '0'
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
          .DoRTCalc(63,67,.f.)
        .w_ARFLLOTT = ' '
        .w_TIPBAR = '0'
          .DoRTCalc(70,70,.f.)
        .w_ARFLESUL = 'S'
        .w_ARCATOPE = 'AR'
          .DoRTCalc(73,74,.f.)
        .w_NMODKEY = .w_ARDESART+ .w_ARDESSUP+ DTOC(.w_ARDTINVA)
        .w_FILT_UNIMIS = IIF(.w_ARPRESTA='P', 'S', ' ')
          .DoRTCalc(77,78,.f.)
        .w_NDATKEY = .w_ARDTOBSO
        .w_ARMOLSUP = 0
        .DoRTCalc(81,81,.f.)
          if not(empty(.w_ARUMSUPP))
          .link_1_112('Full')
          endif
          .DoRTCalc(82,82,.f.)
        .w_ARCODSPE = iif(.w_ARFLSPAN<>'S',Space(20),.w_ARCODSPE)
        .w_ARCODANT = iif(.w_ARFLSPAN<>'S',Space(20),.w_ARCODANT)
        .w_ARIMPSPE = iif(.w_ARFLSPAN<>'S',0,.w_ARIMPSPE)
        .w_ARIMPANT = iif(.w_ARFLSPAN<>'S',0,.w_ARIMPANT)
        .DoRTCalc(87,87,.f.)
          if not(empty(.w_PAPREACC))
          .link_1_132('Full')
          endif
        .DoRTCalc(88,88,.f.)
          if not(empty(.w_PAPREFOR))
          .link_1_133('Full')
          endif
          .DoRTCalc(89,90,.f.)
        .w_LeggePAR_ALTE = i_CodAzi
        .DoRTCalc(91,91,.f.)
          if not(empty(.w_LeggePAR_ALTE))
          .link_1_136('Full')
          endif
        .w_ARPARAME = 'N'
        .w_ARSPORIG = IIF(.w_ARSCRITT<>'S', 0, .w_ARSPORIG)
        .w_ARSPCOPY = IIF(.w_ARSCRITT<>'S', 0, .w_ARSPCOPY)
          .DoRTCalc(95,98,.f.)
        .w_ARTIPPKR = iif(.cFunction='Load', .w_ARTIPART, .w_ARTIPPKR)
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_ARGRUPRO = IIF( .w_ARTIPART<>"DE" , .w_ARGRUPRO , SPACE( 5 ) )
        .DoRTCalc(102,102,.f.)
          if not(empty(.w_ARGRUPRO))
          .link_2_4('Full')
          endif
          .DoRTCalc(103,103,.f.)
        .w_ARCATSCM = IIF( .w_ARTIPART<>"DE" , .w_ARCATSCM , SPACE( 5 ) )
        .DoRTCalc(104,104,.f.)
          if not(empty(.w_ARCATSCM))
          .link_2_6('Full')
          endif
        .DoRTCalc(105,106,.f.)
          if not(empty(.w_ARCODCLA))
          .link_2_10('Full')
          endif
          .DoRTCalc(107,107,.f.)
        .w_ARCODREP = IIF(.w_ARARTPOS<>'S' OR .w_ARTIPART='DE',SPACE(3),.w_ARCODREP)
        .DoRTCalc(108,108,.f.)
          if not(empty(.w_ARCODREP))
          .link_2_13('Full')
          endif
        .DoRTCalc(109,110,.f.)
          if not(empty(.w_ARCODGRU))
          .link_2_16('Full')
          endif
        .DoRTCalc(111,111,.f.)
          if not(empty(.w_ARCODSOT))
          .link_2_17('Full')
          endif
        .w_ARFLCESP = IIF( .w_ARTIPART<>"DE", .w_ARFLCESP , ' ' )
        .DoRTCalc(113,113,.f.)
          if not(empty(.w_ARCATCES))
          .link_2_19('Full')
          endif
        .w_ARCONTRI = SPACE(15)
        .DoRTCalc(114,114,.f.)
          if not(empty(.w_ARCONTRI))
          .link_2_20('Full')
          endif
          .DoRTCalc(115,116,.f.)
        .w_ARRIPCON = IIF( EMPTY( .w_ARCONTRI ) OR .w_TPAPPPES="S", " " , .w_ARRIPCON )
        .w_ARARTPOS = ' '
        .w_ARPUBWEB = ' '
        .w_ARPUBWEB = 'N'
        .w_ARPUBWEB = 'N'
        .DoRTCalc(122,124,.f.)
          if not(empty(.w_IVAREP))
          .link_2_32('Full')
          endif
          .DoRTCalc(125,127,.f.)
        .w_OLDPUBWEB = .w_ARPUBWEB
          .DoRTCalc(129,129,.f.)
        .w_GEST = IIF( g_GPFA<> "S", SPACE(10), get_GPFA_code("MODELLO") )
        .w_CODATT = IIF( g_GPFA<> "S", SPACE(10), get_GPFA_code("GRUPPO") )
        .w_IMMODATR = IIF( g_GPFA<> "S", SPACE(10), get_GPFA_code("MODELLO") )
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
        .w_CODI = .w_ARCODART
        .w_DESC = .w_ARDESART
          .DoRTCalc(137,139,.f.)
        .w_OFATELE = '#'+ALLTRIM(.w_ARCODTIP)+'#'+ALLTRIM(.w_ARCODVAL)+'#'+ALLTRIM(.w_ARCODCLF)+'#'+ALLTRIM(.w_ARFLGESC)
        .w_NFATELE = '#'+ALLTRIM(.w_ARCODTIP)+'#'+ALLTRIM(.w_ARCODVAL)+'#'+ALLTRIM(.w_ARCODCLF)+'#'+ALLTRIM(.w_ARFLGESC)
      endif
    endwith
    cp_BlankRecExtFlds(this,'ART_ICOL')
    this.DoRTCalc(142,142,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_72.enabled = this.oPgFrm.Page1.oPag.oBtn_1_72.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_98.enabled = this.oPgFrm.Page1.oPag.oBtn_1_98.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_99.enabled = this.oPgFrm.Page1.oPag.oBtn_1_99.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_100.enabled = this.oPgFrm.Page1.oPag.oBtn_1_100.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_101.enabled = this.oPgFrm.Page1.oPag.oBtn_1_101.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_103.enabled = this.oPgFrm.Page1.oPag.oBtn_1_103.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_114.enabled = this.oPgFrm.Page1.oPag.oBtn_1_114.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oARCODART_1_6.enabled = i_bVal
      .Page1.oPag.oARDESART_1_7.enabled = i_bVal
      .Page1.oPag.oARTIPART_1_11.enabled = i_bVal
      .Page1.oPag.oARFLSERG_1_12.enabled = i_bVal
      .Page1.oPag.oARTIPSER_1_13.enabled = i_bVal
      .Page1.oPag.oARASSIVA_1_14.enabled = i_bVal
      .Page1.oPag.oARPRESTA_1_15.enabled = i_bVal
      .Page1.oPag.oARDESSUP_1_16.enabled = i_bVal
      .Page1.oPag.oARUNMIS1_1_17.enabled = i_bVal
      .Page1.oPag.oARUNMIS2_1_18.enabled = i_bVal
      .Page1.oPag.oAROPERAT_1_19.enabled = i_bVal
      .Page1.oPag.oARMOLTIP_1_20.enabled = i_bVal
      .Page1.oPag.oARFLUSEP_1_21.enabled = i_bVal
      .Page1.oPag.oARFLUNIV_1_22.enabled = i_bVal
      .Page1.oPag.oARNUMPRE_1_24.enabled = i_bVal
      .Page1.oPag.oARTIPOPE_1_25.enabled = i_bVal
      .Page1.oPag.oARCODIVA_1_26.enabled = i_bVal
      .Page1.oPag.oARCATCON_1_28.enabled = i_bVal
      .Page1.oPag.oARPREZUM_1_29.enabled = i_bVal
      .Page1.oPag.oARGRUMER_1_30.enabled = i_bVal
      .Page1.oPag.oARSTASUP_1_31.enabled = i_bVal
      .Page1.oPag.oARSTACOD_1_32.enabled = i_bVal
      .Page1.oPag.oARSTASUP_1_33.enabled = i_bVal
      .Page1.oPag.oARFLSPAN_1_35.enabled = i_bVal
      .Page1.oPag.oARSCRITT_1_36.enabled = i_bVal
      .Page1.oPag.oARSTACOD_1_37.enabled = i_bVal
      .Page1.oPag.oARFLPRPE_1_38.enabled = i_bVal
      .Page1.oPag.oARFLESCT_1_39.enabled = i_bVal
      .Page1.oPag.oARVOCCEN_1_41.enabled = i_bVal
      .Page1.oPag.oARVOCRIC_1_42.enabled = i_bVal
      .Page1.oPag.oARDTINVA_1_43.enabled = i_bVal
      .Page1.oPag.oARDTOBSO_1_44.enabled = i_bVal
      .Page1.oPag.oARTIPRES_1_45.enabled = i_bVal
      .Page1.oPag.oARTIPRIG_1_46.enabled = i_bVal
      .Page1.oPag.oARTIPRI2_1_47.enabled = i_bVal
      .Page1.oPag.oARCODFAS_1_48.enabled = i_bVal
      .Page1.oPag.oARUTISER_1_49.enabled = i_bVal
      .Page1.oPag.oARPERSER_1_50.enabled = i_bVal
      .Page1.oPag.oARNOMENC_1_51.enabled = i_bVal
      .Page1.oPag.oARMOLSUP_1_53.enabled = i_bVal
      .Page1.oPag.oARDATINT_1_54.enabled = i_bVal
      .Page1.oPag.oARPARAME_1_140.enabled = i_bVal
      .Page2.oPag.oARGRUPRO_2_4.enabled = i_bVal
      .Page2.oPag.oARCATSCM_2_6.enabled = i_bVal
      .Page2.oPag.oARCODCLA_2_10.enabled = i_bVal
      .Page2.oPag.oARCODREP_2_13.enabled = i_bVal
      .Page2.oPag.oARCODGRU_2_16.enabled = i_bVal
      .Page2.oPag.oARCODSOT_2_17.enabled = i_bVal
      .Page2.oPag.oARFLCESP_2_18.enabled = i_bVal
      .Page2.oPag.oARCATCES_2_19.enabled = i_bVal
      .Page2.oPag.oARCONTRI_2_20.enabled = i_bVal
      .Page2.oPag.oARMINVEN_2_21.enabled = i_bVal
      .Page2.oPag.oARRIPCON_2_24.enabled = i_bVal
      .Page2.oPag.oARARTPOS_2_25.enabled = i_bVal
      .Page2.oPag.oARPUBWEB_2_27.enabled = i_bVal
      .Page2.oPag.oARPUBWEB_2_28.enabled = i_bVal
      .Page2.oPag.oARPUBWEB_2_29.enabled = i_bVal
      .Page2.oPag.oARCODTIP_2_43.enabled = i_bVal
      .Page2.oPag.oARCODCLF_2_44.enabled = i_bVal
      .Page2.oPag.oARCODVAL_2_45.enabled = i_bVal
      .Page2.oPag.oARFLGESC_2_51.enabled = i_bVal
      .Page1.oPag.oBtn_1_72.enabled = .Page1.oPag.oBtn_1_72.mCond()
      .Page1.oPag.oBtn_1_98.enabled = .Page1.oPag.oBtn_1_98.mCond()
      .Page1.oPag.oBtn_1_99.enabled = .Page1.oPag.oBtn_1_99.mCond()
      .Page1.oPag.oBtn_1_100.enabled = .Page1.oPag.oBtn_1_100.mCond()
      .Page1.oPag.oBtn_1_101.enabled = .Page1.oPag.oBtn_1_101.mCond()
      .Page1.oPag.oBtn_1_103.enabled = .Page1.oPag.oBtn_1_103.mCond()
      .Page1.oPag.oBtn_1_114.enabled = .Page1.oPag.oBtn_1_114.mCond()
      .Page1.oPag.oBtn_1_131.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oARCODART_1_6.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oARCODART_1_6.enabled = .t.
        .Page1.oPag.oARDESART_1_7.enabled = .t.
      endif
    endwith
    this.GSMA_MLI.SetStatus(i_cOp)
    this.GSAR_MR2.SetStatus(i_cOp)
    this.gsma_mtt.SetStatus(i_cOp)
    this.GSMA_ANO.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'ART_ICOL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSMA_MLI.SetChildrenStatus(i_cOp)
  *  this.GSAR_MR2.SetChildrenStatus(i_cOp)
  *  this.gsma_mtt.SetChildrenStatus(i_cOp)
  *  this.GSMA_ANO.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODART,"ARCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDESART,"ARDESART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPART,"ARTIPART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLSERG,"ARFLSERG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPSER,"ARTIPSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARASSIVA,"ARASSIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPRESTA,"ARPRESTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDESSUP,"ARDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUNMIS1,"ARUNMIS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUNMIS2,"ARUNMIS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AROPERAT,"AROPERAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARMOLTIP,"ARMOLTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLUSEP,"ARFLUSEP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLUNIV,"ARFLUNIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARNUMPRE,"ARNUMPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPOPE,"ARTIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODIVA,"ARCODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCATCON,"ARCATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPREZUM,"ARPREZUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARGRUMER,"ARGRUMER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARSTASUP,"ARSTASUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARSTACOD,"ARSTACOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARSTASUP,"ARSTASUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLSPAN,"ARFLSPAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARSCRITT,"ARSCRITT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARSTACOD,"ARSTACOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLPRPE,"ARFLPRPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLESCT,"ARFLESCT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARVOCCEN,"ARVOCCEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARVOCRIC,"ARVOCRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDTINVA,"ARDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDTOBSO,"ARDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPRES,"ARTIPRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPRIG,"ARTIPRIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPRI2,"ARTIPRI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODFAS,"ARCODFAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUTISER,"ARUTISER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPERSER,"ARPERSER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARNOMENC,"ARNOMENC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARMOLSUP,"ARMOLSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARDATINT,"ARDATINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPBAR,"ARTIPBAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLLOTT,"ARFLLOTT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLESUL,"ARFLESUL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCATOPE,"ARCATOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARMOLSUP,"ARMOLSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARUMSUPP,"ARUMSUPP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODSPE,"ARCODSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODANT,"ARCODANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARIMPSPE,"ARIMPSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARIMPANT,"ARIMPANT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPARAME,"ARPARAME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARSPORIG,"ARSPORIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARSPCOPY,"ARSPCOPY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARSALCOM,"ARSALCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLPECO,"ARFLPECO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARTIPPKR,"ARTIPPKR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARGRUPRO,"ARGRUPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCATSCM,"ARCATSCM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODCLA,"ARCODCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODREP,"ARCODREP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODGRU,"ARCODGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODSOT,"ARCODSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLCESP,"ARFLCESP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCATCES,"ARCATCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCONTRI,"ARCONTRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARMINVEN,"ARMINVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARRIPCON,"ARRIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARARTPOS,"ARARTPOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPUBWEB,"ARPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPUBWEB,"ARPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARPUBWEB,"ARPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GESTGUID,"GESTGUID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODTIP,"ARCODTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODCLF,"ARCODCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARCODVAL,"ARCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ARFLGESC,"ARFLGESC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsma_aas
    * --- aggiunge alla Chiave ulteriore filtro su Ciclo
    IF NOT EMPTY(i_cWhere)
       IF AT('ARTIPART', UPPER(i_cWhere))=0
          i_cWhere=i_cWhere+" and ARTIPART in ('FM','FO','DE')"
       ENDIF
    ENDIF
    
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    i_lTable = "ART_ICOL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ART_ICOL_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSMA_SSE with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ART_ICOL_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ART_ICOL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ART_ICOL')
        i_extval=cp_InsertValODBCExtFlds(this,'ART_ICOL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ARCODART,ARDESART,ARTIPART,ARFLSERG,ARTIPSER"+;
                  ",ARASSIVA,ARPRESTA,ARDESSUP,ARUNMIS1,ARUNMIS2"+;
                  ",AROPERAT,ARMOLTIP,ARFLUSEP,ARFLUNIV,ARNUMPRE"+;
                  ",ARTIPOPE,ARCODIVA,ARCATCON,ARPREZUM,ARGRUMER"+;
                  ",ARSTASUP,ARSTACOD,ARFLSPAN,ARSCRITT,ARFLPRPE"+;
                  ",ARFLESCT,ARVOCCEN,ARVOCRIC,ARDTINVA,ARDTOBSO"+;
                  ",ARTIPRES,ARTIPRIG,ARTIPRI2,ARCODFAS,ARUTISER"+;
                  ",ARPERSER,ARNOMENC,ARMOLSUP,ARDATINT,UTCC"+;
                  ",UTCV,UTDC,UTDV,ARTIPBAR,ARFLLOTT"+;
                  ",ARFLESUL,ARCATOPE,ARUMSUPP,ARCODSPE,ARCODANT"+;
                  ",ARIMPSPE,ARIMPANT,ARPARAME,ARSPORIG,ARSPCOPY"+;
                  ",ARSALCOM,ARFLPECO,ARTIPPKR,ARGRUPRO,ARCATSCM"+;
                  ",ARCODCLA,ARCODREP,ARCODGRU,ARCODSOT,ARFLCESP"+;
                  ",ARCATCES,ARCONTRI,ARMINVEN,ARRIPCON,ARARTPOS"+;
                  ",ARPUBWEB,GESTGUID,ARCODTIP,ARCODCLF,ARCODVAL"+;
                  ",ARFLGESC "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ARCODART)+;
                  ","+cp_ToStrODBC(this.w_ARDESART)+;
                  ","+cp_ToStrODBC(this.w_ARTIPART)+;
                  ","+cp_ToStrODBC(this.w_ARFLSERG)+;
                  ","+cp_ToStrODBC(this.w_ARTIPSER)+;
                  ","+cp_ToStrODBC(this.w_ARASSIVA)+;
                  ","+cp_ToStrODBC(this.w_ARPRESTA)+;
                  ","+cp_ToStrODBC(this.w_ARDESSUP)+;
                  ","+cp_ToStrODBCNull(this.w_ARUNMIS1)+;
                  ","+cp_ToStrODBCNull(this.w_ARUNMIS2)+;
                  ","+cp_ToStrODBC(this.w_AROPERAT)+;
                  ","+cp_ToStrODBC(this.w_ARMOLTIP)+;
                  ","+cp_ToStrODBC(this.w_ARFLUSEP)+;
                  ","+cp_ToStrODBC(this.w_ARFLUNIV)+;
                  ","+cp_ToStrODBC(this.w_ARNUMPRE)+;
                  ","+cp_ToStrODBCNull(this.w_ARTIPOPE)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODIVA)+;
                  ","+cp_ToStrODBCNull(this.w_ARCATCON)+;
                  ","+cp_ToStrODBC(this.w_ARPREZUM)+;
                  ","+cp_ToStrODBCNull(this.w_ARGRUMER)+;
                  ","+cp_ToStrODBC(this.w_ARSTASUP)+;
                  ","+cp_ToStrODBC(this.w_ARSTACOD)+;
                  ","+cp_ToStrODBC(this.w_ARFLSPAN)+;
                  ","+cp_ToStrODBC(this.w_ARSCRITT)+;
                  ","+cp_ToStrODBC(this.w_ARFLPRPE)+;
                  ","+cp_ToStrODBC(this.w_ARFLESCT)+;
                  ","+cp_ToStrODBCNull(this.w_ARVOCCEN)+;
                  ","+cp_ToStrODBCNull(this.w_ARVOCRIC)+;
                  ","+cp_ToStrODBC(this.w_ARDTINVA)+;
                  ","+cp_ToStrODBC(this.w_ARDTOBSO)+;
                  ","+cp_ToStrODBCNull(this.w_ARTIPRES)+;
                  ","+cp_ToStrODBC(this.w_ARTIPRIG)+;
                  ","+cp_ToStrODBC(this.w_ARTIPRI2)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODFAS)+;
                  ","+cp_ToStrODBC(this.w_ARUTISER)+;
                  ","+cp_ToStrODBC(this.w_ARPERSER)+;
                  ","+cp_ToStrODBCNull(this.w_ARNOMENC)+;
                  ","+cp_ToStrODBC(this.w_ARMOLSUP)+;
                  ","+cp_ToStrODBC(this.w_ARDATINT)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_ARTIPBAR)+;
                  ","+cp_ToStrODBC(this.w_ARFLLOTT)+;
                  ","+cp_ToStrODBC(this.w_ARFLESUL)+;
                  ","+cp_ToStrODBC(this.w_ARCATOPE)+;
                  ","+cp_ToStrODBCNull(this.w_ARUMSUPP)+;
                  ","+cp_ToStrODBC(this.w_ARCODSPE)+;
                  ","+cp_ToStrODBC(this.w_ARCODANT)+;
                  ","+cp_ToStrODBC(this.w_ARIMPSPE)+;
                  ","+cp_ToStrODBC(this.w_ARIMPANT)+;
                  ","+cp_ToStrODBC(this.w_ARPARAME)+;
                  ","+cp_ToStrODBC(this.w_ARSPORIG)+;
                  ","+cp_ToStrODBC(this.w_ARSPCOPY)+;
                  ","+cp_ToStrODBC(this.w_ARSALCOM)+;
                  ","+cp_ToStrODBC(this.w_ARFLPECO)+;
                  ","+cp_ToStrODBC(this.w_ARTIPPKR)+;
                  ","+cp_ToStrODBCNull(this.w_ARGRUPRO)+;
                  ","+cp_ToStrODBCNull(this.w_ARCATSCM)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODCLA)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODREP)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODGRU)+;
                  ","+cp_ToStrODBCNull(this.w_ARCODSOT)+;
                  ","+cp_ToStrODBC(this.w_ARFLCESP)+;
                  ","+cp_ToStrODBCNull(this.w_ARCATCES)+;
                  ","+cp_ToStrODBCNull(this.w_ARCONTRI)+;
                  ","+cp_ToStrODBC(this.w_ARMINVEN)+;
                  ","+cp_ToStrODBC(this.w_ARRIPCON)+;
                  ","+cp_ToStrODBC(this.w_ARARTPOS)+;
                  ","+cp_ToStrODBC(this.w_ARPUBWEB)+;
                  ","+cp_ToStrODBC(this.w_GESTGUID)+;
                  ","+cp_ToStrODBC(this.w_ARCODTIP)+;
                  ","+cp_ToStrODBC(this.w_ARCODCLF)+;
                  ","+cp_ToStrODBC(this.w_ARCODVAL)+;
                  ","+cp_ToStrODBC(this.w_ARFLGESC)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ART_ICOL')
        i_extval=cp_InsertValVFPExtFlds(this,'ART_ICOL')
        cp_CheckDeletedKey(i_cTable,0,'ARCODART',this.w_ARCODART)
        INSERT INTO (i_cTable);
              (ARCODART,ARDESART,ARTIPART,ARFLSERG,ARTIPSER,ARASSIVA,ARPRESTA,ARDESSUP,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARFLUSEP,ARFLUNIV,ARNUMPRE,ARTIPOPE,ARCODIVA,ARCATCON,ARPREZUM,ARGRUMER,ARSTASUP,ARSTACOD,ARFLSPAN,ARSCRITT,ARFLPRPE,ARFLESCT,ARVOCCEN,ARVOCRIC,ARDTINVA,ARDTOBSO,ARTIPRES,ARTIPRIG,ARTIPRI2,ARCODFAS,ARUTISER,ARPERSER,ARNOMENC,ARMOLSUP,ARDATINT,UTCC,UTCV,UTDC,UTDV,ARTIPBAR,ARFLLOTT,ARFLESUL,ARCATOPE,ARUMSUPP,ARCODSPE,ARCODANT,ARIMPSPE,ARIMPANT,ARPARAME,ARSPORIG,ARSPCOPY,ARSALCOM,ARFLPECO,ARTIPPKR,ARGRUPRO,ARCATSCM,ARCODCLA,ARCODREP,ARCODGRU,ARCODSOT,ARFLCESP,ARCATCES,ARCONTRI,ARMINVEN,ARRIPCON,ARARTPOS,ARPUBWEB,GESTGUID,ARCODTIP,ARCODCLF,ARCODVAL,ARFLGESC  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ARCODART;
                  ,this.w_ARDESART;
                  ,this.w_ARTIPART;
                  ,this.w_ARFLSERG;
                  ,this.w_ARTIPSER;
                  ,this.w_ARASSIVA;
                  ,this.w_ARPRESTA;
                  ,this.w_ARDESSUP;
                  ,this.w_ARUNMIS1;
                  ,this.w_ARUNMIS2;
                  ,this.w_AROPERAT;
                  ,this.w_ARMOLTIP;
                  ,this.w_ARFLUSEP;
                  ,this.w_ARFLUNIV;
                  ,this.w_ARNUMPRE;
                  ,this.w_ARTIPOPE;
                  ,this.w_ARCODIVA;
                  ,this.w_ARCATCON;
                  ,this.w_ARPREZUM;
                  ,this.w_ARGRUMER;
                  ,this.w_ARSTASUP;
                  ,this.w_ARSTACOD;
                  ,this.w_ARFLSPAN;
                  ,this.w_ARSCRITT;
                  ,this.w_ARFLPRPE;
                  ,this.w_ARFLESCT;
                  ,this.w_ARVOCCEN;
                  ,this.w_ARVOCRIC;
                  ,this.w_ARDTINVA;
                  ,this.w_ARDTOBSO;
                  ,this.w_ARTIPRES;
                  ,this.w_ARTIPRIG;
                  ,this.w_ARTIPRI2;
                  ,this.w_ARCODFAS;
                  ,this.w_ARUTISER;
                  ,this.w_ARPERSER;
                  ,this.w_ARNOMENC;
                  ,this.w_ARMOLSUP;
                  ,this.w_ARDATINT;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_ARTIPBAR;
                  ,this.w_ARFLLOTT;
                  ,this.w_ARFLESUL;
                  ,this.w_ARCATOPE;
                  ,this.w_ARUMSUPP;
                  ,this.w_ARCODSPE;
                  ,this.w_ARCODANT;
                  ,this.w_ARIMPSPE;
                  ,this.w_ARIMPANT;
                  ,this.w_ARPARAME;
                  ,this.w_ARSPORIG;
                  ,this.w_ARSPCOPY;
                  ,this.w_ARSALCOM;
                  ,this.w_ARFLPECO;
                  ,this.w_ARTIPPKR;
                  ,this.w_ARGRUPRO;
                  ,this.w_ARCATSCM;
                  ,this.w_ARCODCLA;
                  ,this.w_ARCODREP;
                  ,this.w_ARCODGRU;
                  ,this.w_ARCODSOT;
                  ,this.w_ARFLCESP;
                  ,this.w_ARCATCES;
                  ,this.w_ARCONTRI;
                  ,this.w_ARMINVEN;
                  ,this.w_ARRIPCON;
                  ,this.w_ARARTPOS;
                  ,this.w_ARPUBWEB;
                  ,this.w_GESTGUID;
                  ,this.w_ARCODTIP;
                  ,this.w_ARCODCLF;
                  ,this.w_ARCODVAL;
                  ,this.w_ARFLGESC;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ART_ICOL_IDX,i_nConn)
      *
      * update ART_ICOL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ART_ICOL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ARDESART="+cp_ToStrODBC(this.w_ARDESART)+;
             ",ARTIPART="+cp_ToStrODBC(this.w_ARTIPART)+;
             ",ARFLSERG="+cp_ToStrODBC(this.w_ARFLSERG)+;
             ",ARTIPSER="+cp_ToStrODBC(this.w_ARTIPSER)+;
             ",ARASSIVA="+cp_ToStrODBC(this.w_ARASSIVA)+;
             ",ARPRESTA="+cp_ToStrODBC(this.w_ARPRESTA)+;
             ",ARDESSUP="+cp_ToStrODBC(this.w_ARDESSUP)+;
             ",ARUNMIS1="+cp_ToStrODBCNull(this.w_ARUNMIS1)+;
             ",ARUNMIS2="+cp_ToStrODBCNull(this.w_ARUNMIS2)+;
             ",AROPERAT="+cp_ToStrODBC(this.w_AROPERAT)+;
             ",ARMOLTIP="+cp_ToStrODBC(this.w_ARMOLTIP)+;
             ",ARFLUSEP="+cp_ToStrODBC(this.w_ARFLUSEP)+;
             ",ARFLUNIV="+cp_ToStrODBC(this.w_ARFLUNIV)+;
             ",ARNUMPRE="+cp_ToStrODBC(this.w_ARNUMPRE)+;
             ",ARTIPOPE="+cp_ToStrODBCNull(this.w_ARTIPOPE)+;
             ",ARCODIVA="+cp_ToStrODBCNull(this.w_ARCODIVA)+;
             ",ARCATCON="+cp_ToStrODBCNull(this.w_ARCATCON)+;
             ",ARPREZUM="+cp_ToStrODBC(this.w_ARPREZUM)+;
             ",ARGRUMER="+cp_ToStrODBCNull(this.w_ARGRUMER)+;
             ",ARSTASUP="+cp_ToStrODBC(this.w_ARSTASUP)+;
             ",ARSTACOD="+cp_ToStrODBC(this.w_ARSTACOD)+;
             ",ARFLSPAN="+cp_ToStrODBC(this.w_ARFLSPAN)+;
             ",ARSCRITT="+cp_ToStrODBC(this.w_ARSCRITT)+;
             ",ARFLPRPE="+cp_ToStrODBC(this.w_ARFLPRPE)+;
             ",ARFLESCT="+cp_ToStrODBC(this.w_ARFLESCT)+;
             ",ARVOCCEN="+cp_ToStrODBCNull(this.w_ARVOCCEN)+;
             ",ARVOCRIC="+cp_ToStrODBCNull(this.w_ARVOCRIC)+;
             ",ARDTINVA="+cp_ToStrODBC(this.w_ARDTINVA)+;
             ",ARDTOBSO="+cp_ToStrODBC(this.w_ARDTOBSO)+;
             ",ARTIPRES="+cp_ToStrODBCNull(this.w_ARTIPRES)+;
             ",ARTIPRIG="+cp_ToStrODBC(this.w_ARTIPRIG)+;
             ",ARTIPRI2="+cp_ToStrODBC(this.w_ARTIPRI2)+;
             ",ARCODFAS="+cp_ToStrODBCNull(this.w_ARCODFAS)+;
             ",ARUTISER="+cp_ToStrODBC(this.w_ARUTISER)+;
             ",ARPERSER="+cp_ToStrODBC(this.w_ARPERSER)+;
             ",ARNOMENC="+cp_ToStrODBCNull(this.w_ARNOMENC)+;
             ",ARMOLSUP="+cp_ToStrODBC(this.w_ARMOLSUP)+;
             ",ARDATINT="+cp_ToStrODBC(this.w_ARDATINT)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",ARTIPBAR="+cp_ToStrODBC(this.w_ARTIPBAR)+;
             ",ARFLLOTT="+cp_ToStrODBC(this.w_ARFLLOTT)+;
             ",ARFLESUL="+cp_ToStrODBC(this.w_ARFLESUL)+;
             ",ARCATOPE="+cp_ToStrODBC(this.w_ARCATOPE)+;
             ",ARUMSUPP="+cp_ToStrODBCNull(this.w_ARUMSUPP)+;
             ",ARCODSPE="+cp_ToStrODBC(this.w_ARCODSPE)+;
             ",ARCODANT="+cp_ToStrODBC(this.w_ARCODANT)+;
             ",ARIMPSPE="+cp_ToStrODBC(this.w_ARIMPSPE)+;
             ",ARIMPANT="+cp_ToStrODBC(this.w_ARIMPANT)+;
             ",ARPARAME="+cp_ToStrODBC(this.w_ARPARAME)+;
             ",ARSPORIG="+cp_ToStrODBC(this.w_ARSPORIG)+;
             ",ARSPCOPY="+cp_ToStrODBC(this.w_ARSPCOPY)+;
             ",ARSALCOM="+cp_ToStrODBC(this.w_ARSALCOM)+;
             ",ARFLPECO="+cp_ToStrODBC(this.w_ARFLPECO)+;
             ",ARTIPPKR="+cp_ToStrODBC(this.w_ARTIPPKR)+;
             ",ARGRUPRO="+cp_ToStrODBCNull(this.w_ARGRUPRO)+;
             ",ARCATSCM="+cp_ToStrODBCNull(this.w_ARCATSCM)+;
             ",ARCODCLA="+cp_ToStrODBCNull(this.w_ARCODCLA)+;
             ",ARCODREP="+cp_ToStrODBCNull(this.w_ARCODREP)+;
             ",ARCODGRU="+cp_ToStrODBCNull(this.w_ARCODGRU)+;
             ",ARCODSOT="+cp_ToStrODBCNull(this.w_ARCODSOT)+;
             ",ARFLCESP="+cp_ToStrODBC(this.w_ARFLCESP)+;
             ",ARCATCES="+cp_ToStrODBCNull(this.w_ARCATCES)+;
             ",ARCONTRI="+cp_ToStrODBCNull(this.w_ARCONTRI)+;
             ",ARMINVEN="+cp_ToStrODBC(this.w_ARMINVEN)+;
             ",ARRIPCON="+cp_ToStrODBC(this.w_ARRIPCON)+;
             ",ARARTPOS="+cp_ToStrODBC(this.w_ARARTPOS)+;
             ",ARPUBWEB="+cp_ToStrODBC(this.w_ARPUBWEB)+;
             ",GESTGUID="+cp_ToStrODBC(this.w_GESTGUID)+;
             ",ARCODTIP="+cp_ToStrODBC(this.w_ARCODTIP)+;
             ",ARCODCLF="+cp_ToStrODBC(this.w_ARCODCLF)+;
             ",ARCODVAL="+cp_ToStrODBC(this.w_ARCODVAL)+;
             ",ARFLGESC="+cp_ToStrODBC(this.w_ARFLGESC)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ART_ICOL')
        i_cWhere = cp_PKFox(i_cTable  ,'ARCODART',this.w_ARCODART  )
        UPDATE (i_cTable) SET;
              ARDESART=this.w_ARDESART;
             ,ARTIPART=this.w_ARTIPART;
             ,ARFLSERG=this.w_ARFLSERG;
             ,ARTIPSER=this.w_ARTIPSER;
             ,ARASSIVA=this.w_ARASSIVA;
             ,ARPRESTA=this.w_ARPRESTA;
             ,ARDESSUP=this.w_ARDESSUP;
             ,ARUNMIS1=this.w_ARUNMIS1;
             ,ARUNMIS2=this.w_ARUNMIS2;
             ,AROPERAT=this.w_AROPERAT;
             ,ARMOLTIP=this.w_ARMOLTIP;
             ,ARFLUSEP=this.w_ARFLUSEP;
             ,ARFLUNIV=this.w_ARFLUNIV;
             ,ARNUMPRE=this.w_ARNUMPRE;
             ,ARTIPOPE=this.w_ARTIPOPE;
             ,ARCODIVA=this.w_ARCODIVA;
             ,ARCATCON=this.w_ARCATCON;
             ,ARPREZUM=this.w_ARPREZUM;
             ,ARGRUMER=this.w_ARGRUMER;
             ,ARSTASUP=this.w_ARSTASUP;
             ,ARSTACOD=this.w_ARSTACOD;
             ,ARFLSPAN=this.w_ARFLSPAN;
             ,ARSCRITT=this.w_ARSCRITT;
             ,ARFLPRPE=this.w_ARFLPRPE;
             ,ARFLESCT=this.w_ARFLESCT;
             ,ARVOCCEN=this.w_ARVOCCEN;
             ,ARVOCRIC=this.w_ARVOCRIC;
             ,ARDTINVA=this.w_ARDTINVA;
             ,ARDTOBSO=this.w_ARDTOBSO;
             ,ARTIPRES=this.w_ARTIPRES;
             ,ARTIPRIG=this.w_ARTIPRIG;
             ,ARTIPRI2=this.w_ARTIPRI2;
             ,ARCODFAS=this.w_ARCODFAS;
             ,ARUTISER=this.w_ARUTISER;
             ,ARPERSER=this.w_ARPERSER;
             ,ARNOMENC=this.w_ARNOMENC;
             ,ARMOLSUP=this.w_ARMOLSUP;
             ,ARDATINT=this.w_ARDATINT;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,ARTIPBAR=this.w_ARTIPBAR;
             ,ARFLLOTT=this.w_ARFLLOTT;
             ,ARFLESUL=this.w_ARFLESUL;
             ,ARCATOPE=this.w_ARCATOPE;
             ,ARUMSUPP=this.w_ARUMSUPP;
             ,ARCODSPE=this.w_ARCODSPE;
             ,ARCODANT=this.w_ARCODANT;
             ,ARIMPSPE=this.w_ARIMPSPE;
             ,ARIMPANT=this.w_ARIMPANT;
             ,ARPARAME=this.w_ARPARAME;
             ,ARSPORIG=this.w_ARSPORIG;
             ,ARSPCOPY=this.w_ARSPCOPY;
             ,ARSALCOM=this.w_ARSALCOM;
             ,ARFLPECO=this.w_ARFLPECO;
             ,ARTIPPKR=this.w_ARTIPPKR;
             ,ARGRUPRO=this.w_ARGRUPRO;
             ,ARCATSCM=this.w_ARCATSCM;
             ,ARCODCLA=this.w_ARCODCLA;
             ,ARCODREP=this.w_ARCODREP;
             ,ARCODGRU=this.w_ARCODGRU;
             ,ARCODSOT=this.w_ARCODSOT;
             ,ARFLCESP=this.w_ARFLCESP;
             ,ARCATCES=this.w_ARCATCES;
             ,ARCONTRI=this.w_ARCONTRI;
             ,ARMINVEN=this.w_ARMINVEN;
             ,ARRIPCON=this.w_ARRIPCON;
             ,ARARTPOS=this.w_ARARTPOS;
             ,ARPUBWEB=this.w_ARPUBWEB;
             ,GESTGUID=this.w_GESTGUID;
             ,ARCODTIP=this.w_ARCODTIP;
             ,ARCODCLF=this.w_ARCODCLF;
             ,ARCODVAL=this.w_ARCODVAL;
             ,ARFLGESC=this.w_ARFLGESC;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSMA_MLI : Saving
      this.GSMA_MLI.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"LICODART";
             )
      this.GSMA_MLI.mReplace()
      * --- GSAR_MR2 : Saving
      this.GSAR_MR2.ChangeRow(this.cRowID+'      1',0;
             ,this.w_GESTGUID,"GUID";
             )
      this.GSAR_MR2.mReplace()
      * --- gsma_mtt : Saving
      this.gsma_mtt.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"TACODART";
             )
      this.gsma_mtt.mReplace()
      * --- GSMA_ANO : Saving
      this.GSMA_ANO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ARCODART,"ARCODART";
             )
      this.GSMA_ANO.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSMA_MLI : Deleting
    this.GSMA_MLI.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"LICODART";
           )
    this.GSMA_MLI.mDelete()
    * --- GSAR_MR2 : Deleting
    this.GSAR_MR2.ChangeRow(this.cRowID+'      1',0;
           ,this.w_GESTGUID,"GUID";
           )
    this.GSAR_MR2.mDelete()
    * --- gsma_mtt : Deleting
    this.gsma_mtt.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"TACODART";
           )
    this.gsma_mtt.mDelete()
    * --- GSMA_ANO : Deleting
    this.GSMA_ANO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ARCODART,"ARCODART";
           )
    this.GSMA_ANO.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ART_ICOL_IDX,i_nConn)
      *
      * delete ART_ICOL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ARCODART',this.w_ARCODART  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
        if .o_PARALTE<>.w_PARALTE
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.t.)
            .w_TIPOPE = this.cFunction
        if .o_AUTO<>.w_AUTO
            .w_ARCODART = iif(.cFunction='Load' AND .w_AUTO='S', 'AUTO',.w_ARCODART)
        endif
        .DoRTCalc(7,11,.t.)
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARFLSERG = IIF(.w_ARTIPART='FM',.w_ARFLSERG,' ')
        endif
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARTIPSER = IIF(.w_ARTIPART='DE' , '' ,.w_ARTIPSER )
        endif
        .DoRTCalc(14,16,.t.)
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARUNMIS1 = SPACE(3)
          .link_1_17('Full')
        endif
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARUNMIS2 = SPACE(3)
          .link_1_18('Full')
        endif
        .DoRTCalc(19,19,.t.)
        if .o_ARUNMIS2<>.w_ARUNMIS2
            .w_ARMOLTIP = 0
        endif
        .DoRTCalc(21,22,.t.)
        if .o_ARFLUNIV<>.w_ARFLUNIV
            .w_ARNUMPRE = 1
        endif
        .DoRTCalc(24,24,.t.)
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARCODIVA = IIF( .w_ARTIPART<>"DE" , .w_ARCODIVA , SPACE( 5 ) )
          .link_1_26('Full')
        endif
        .DoRTCalc(26,26,.t.)
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARCATCON = IIF( .w_ARTIPART<>"DE" , .w_ARCATCON , SPACE( 5 ) )
          .link_1_28('Full')
        endif
        if .o_ARFLSERG<>.w_ARFLSERG
            .w_ARPREZUM = IIF(.w_ARFLSERG='S', 'S','N')
        endif
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARGRUMER = IIF( .w_ARTIPART<>'DE' , .w_ARGRUMER , SPACE( 5 ) )
          .link_1_30('Full')
        endif
        .DoRTCalc(30,31,.t.)
        if .o_ARPRESTA<>.w_ARPRESTA.or. .o_ARFLPRPE<>.w_ARFLPRPE
            .w_ARSTASUP = ' '
        endif
        .DoRTCalc(33,33,.t.)
        if .o_ARPRESTA<>.w_ARPRESTA
            .w_ARFLSPAN = 'N'
        endif
        if .o_ARPRESTA<>.w_ARPRESTA.or. .o_ARFLSPAN<>.w_ARFLSPAN
            .w_ARSCRITT = 'N'
        endif
        if .o_ARPRESTA<>.w_ARPRESTA
            .w_ARSTACOD = ' '
        endif
        if .o_ARSTASUP<>.w_ARSTASUP
            .w_ARFLPRPE = IIF(.w_ARSTASUP='S',' ',.w_ARFLPRPE)
        endif
        .DoRTCalc(38,39,.t.)
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARVOCCEN = IIF( .w_ARTIPART<>"DE" , .w_ARVOCCEN , SPACE( 15 ) )
          .link_1_41('Full')
        endif
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARVOCRIC = IIF( .w_ARTIPART<>"DE" , .w_ARVOCRIC , SPACE( 15 ) )
          .link_1_42('Full')
        endif
        .DoRTCalc(42,44,.t.)
        if .o_ARTIPART<>.w_ARTIPART.or. .o_ARTIPRIG<>.w_ARTIPRIG
            .w_ARTIPRIG = IIF(EMPTY(.w_ARTIPRIG),'D',.w_ARTIPRIG)
        endif
        if .o_ARTIPART<>.w_ARTIPART.or. .o_ARTIPRI2<>.w_ARTIPRI2
            .w_ARTIPRI2 = IIF(EMPTY(.w_ARTIPRI2),'D',.w_ARTIPRI2)
        endif
        .DoRTCalc(47,49,.t.)
        if .o_ARUTISER<>.w_ARUTISER
            .w_ARNOMENC = ''
          .link_1_51('Full')
        endif
        .DoRTCalc(51,51,.t.)
        if .o_ARUMSUPP<>.w_ARUMSUPP
            .w_ARMOLSUP = 0
        endif
        if .o_ARUTISER<>.w_ARUTISER
            .w_ARDATINT = IIF(.w_ARUTISER='N', IIF(EMPTY(.w_ARDATINT), 'S', .w_ARDATINT ), 'N')
        endif
        .DoRTCalc(54,57,.t.)
            .w_CODESE = g_CODESE
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .DoRTCalc(59,74,.t.)
            .w_NMODKEY = .w_ARDESART+ .w_ARDESSUP+ DTOC(.w_ARDTINVA)
        if .o_ARPRESTA<>.w_ARPRESTA
            .w_FILT_UNIMIS = IIF(.w_ARPRESTA='P', 'S', ' ')
        endif
        .DoRTCalc(77,78,.t.)
            .w_NDATKEY = .w_ARDTOBSO
        if .o_ARUMSUPP<>.w_ARUMSUPP
            .w_ARMOLSUP = 0
        endif
          .link_1_112('Full')
        if .o_ARFLSPAN<>.w_ARFLSPAN
          .Calculate_PNNTHVCWIU()
        endif
        .DoRTCalc(82,82,.t.)
        if .o_ARFLSPAN<>.w_ARFLSPAN
            .w_ARCODSPE = iif(.w_ARFLSPAN<>'S',Space(20),.w_ARCODSPE)
        endif
        if .o_ARFLSPAN<>.w_ARFLSPAN
            .w_ARCODANT = iif(.w_ARFLSPAN<>'S',Space(20),.w_ARCODANT)
        endif
        if .o_ARFLSPAN<>.w_ARFLSPAN
            .w_ARIMPSPE = iif(.w_ARFLSPAN<>'S',0,.w_ARIMPSPE)
        endif
        if .o_ARFLSPAN<>.w_ARFLSPAN
            .w_ARIMPANT = iif(.w_ARFLSPAN<>'S',0,.w_ARIMPANT)
        endif
          .link_1_132('Full')
          .link_1_133('Full')
        .DoRTCalc(89,90,.t.)
          .link_1_136('Full')
        if .o_ARPRESTA<>.w_ARPRESTA
          .Calculate_FIFRDECGCG()
        endif
        .DoRTCalc(92,92,.t.)
        if .o_ARSCRITT<>.w_ARSCRITT
            .w_ARSPORIG = IIF(.w_ARSCRITT<>'S', 0, .w_ARSPORIG)
        endif
        if .o_ARSCRITT<>.w_ARSCRITT
            .w_ARSPCOPY = IIF(.w_ARSCRITT<>'S', 0, .w_ARSPCOPY)
        endif
        .DoRTCalc(95,98,.t.)
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARTIPPKR = iif(.cFunction='Load', .w_ARTIPART, .w_ARTIPPKR)
        endif
            .w_CODI = .w_ARCODART
            .w_DESC = .w_ARDESART
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARGRUPRO = IIF( .w_ARTIPART<>"DE" , .w_ARGRUPRO , SPACE( 5 ) )
          .link_2_4('Full')
        endif
        .DoRTCalc(103,103,.t.)
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARCATSCM = IIF( .w_ARTIPART<>"DE" , .w_ARCATSCM , SPACE( 5 ) )
          .link_2_6('Full')
        endif
        .DoRTCalc(105,107,.t.)
        if .o_ARARTPOS<>.w_ARARTPOS.or. .o_ARTIPART<>.w_ARTIPART
            .w_ARCODREP = IIF(.w_ARARTPOS<>'S' OR .w_ARTIPART='DE',SPACE(3),.w_ARCODREP)
          .link_2_13('Full')
        endif
        .DoRTCalc(109,111,.t.)
        if .o_ARTIPART<>.w_ARTIPART
            .w_ARFLCESP = IIF( .w_ARTIPART<>"DE", .w_ARFLCESP , ' ' )
        endif
        if .o_ARFLCESP<>.w_ARFLCESP
          .link_2_19('Full')
        endif
        if .o_ARFLSERG<>.w_ARFLSERG.or. .o_ARTIPSER<>.w_ARTIPSER.or. .o_ARTIPART<>.w_ARTIPART
            .w_ARCONTRI = SPACE(15)
          .link_2_20('Full')
        endif
        .DoRTCalc(115,116,.t.)
        if .o_ARCONTRI<>.w_ARCONTRI.or. .o_TPAPPPES<>.w_TPAPPPES
            .w_ARRIPCON = IIF( EMPTY( .w_ARCONTRI ) OR .w_TPAPPPES="S", " " , .w_ARRIPCON )
        endif
        .DoRTCalc(118,123,.t.)
        if .o_ARCODREP<>.w_ARCODREP
          .link_2_32('Full')
        endif
        .DoRTCalc(125,127,.t.)
        if .o_ARCODART<>.w_ARCODART
            .w_OLDPUBWEB = .w_ARPUBWEB
        endif
        if  .o_GEST<>.w_GEST
          .WriteTo_GSAR_MR2()
        endif
        .DoRTCalc(129,132,.t.)
            .w_CODI = .w_ARCODART
            .w_DESC = .w_ARDESART
        if  .o_ARPRESTA<>.w_ARPRESTA
          .WriteTo_gsma_mtt()
        endif
            .w_CODI = .w_ARCODART
            .w_DESC = .w_ARDESART
        .DoRTCalc(137,139,.t.)
        if .o_ARCODART<>.w_ARCODART
            .w_OFATELE = '#'+ALLTRIM(.w_ARCODTIP)+'#'+ALLTRIM(.w_ARCODVAL)+'#'+ALLTRIM(.w_ARCODCLF)+'#'+ALLTRIM(.w_ARFLGESC)
        endif
            .w_NFATELE = '#'+ALLTRIM(.w_ARCODTIP)+'#'+ALLTRIM(.w_ARCODVAL)+'#'+ALLTRIM(.w_ARCODCLF)+'#'+ALLTRIM(.w_ARFLGESC)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(142,142,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
    endwith
  return

  proc Calculate_OLYGVFQXTR()
    with this
          * --- Calcola Auto
          GSAR_BSS(this;
             )
    endwith
  endproc
  proc Calculate_PNNTHVCWIU()
    with this
          * --- GSPR_BCO (ARFLSPAN) - Fatturabile se spesa e anticipazione collegate
          gspr_bco(this;
              ,'ARFLSPAN';
             )
    endwith
  endproc
  proc Calculate_CZGHMRBITS()
    with this
          * --- Cancellazione allegati collegati
          gsma_bae(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_FIFRDECGCG()
    with this
          * --- Aggiorna tipo
          .w_ARTIPART = IIF(UPPER(.w_TIPOPE)='LOAD' AND .w_ARPRESTA='P', 'FM', .w_ARTIPART)
          .w_ARPARAME = IIF(.w_ARPRESTA$"SA", "N", .w_ARPARAME)
          .w_ARCODFAS = IIF(.w_ARPRESTA$"SA", SPACE(10), .w_ARCODFAS)
    endwith
  endproc
  proc Calculate_YBHCACTYYL()
    with this
          * --- Tipo articolo (PK I.revolution)
          .w_ARTIPPKR = evl(.w_ARTIPPKR, .w_ARTIPART)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oARCODART_1_6.enabled = this.oPgFrm.Page1.oPag.oARCODART_1_6.mCond()
    this.oPgFrm.Page1.oPag.oARDESART_1_7.enabled = this.oPgFrm.Page1.oPag.oARDESART_1_7.mCond()
    this.oPgFrm.Page1.oPag.oARTIPART_1_11.enabled = this.oPgFrm.Page1.oPag.oARTIPART_1_11.mCond()
    this.oPgFrm.Page1.oPag.oARFLSERG_1_12.enabled = this.oPgFrm.Page1.oPag.oARFLSERG_1_12.mCond()
    this.oPgFrm.Page1.oPag.oARPRESTA_1_15.enabled = this.oPgFrm.Page1.oPag.oARPRESTA_1_15.mCond()
    this.oPgFrm.Page1.oPag.oARUNMIS1_1_17.enabled = this.oPgFrm.Page1.oPag.oARUNMIS1_1_17.mCond()
    this.oPgFrm.Page1.oPag.oARUNMIS2_1_18.enabled = this.oPgFrm.Page1.oPag.oARUNMIS2_1_18.mCond()
    this.oPgFrm.Page1.oPag.oAROPERAT_1_19.enabled = this.oPgFrm.Page1.oPag.oAROPERAT_1_19.mCond()
    this.oPgFrm.Page1.oPag.oARMOLTIP_1_20.enabled = this.oPgFrm.Page1.oPag.oARMOLTIP_1_20.mCond()
    this.oPgFrm.Page1.oPag.oARFLUSEP_1_21.enabled = this.oPgFrm.Page1.oPag.oARFLUSEP_1_21.mCond()
    this.oPgFrm.Page1.oPag.oARNUMPRE_1_24.enabled = this.oPgFrm.Page1.oPag.oARNUMPRE_1_24.mCond()
    this.oPgFrm.Page1.oPag.oARCODIVA_1_26.enabled = this.oPgFrm.Page1.oPag.oARCODIVA_1_26.mCond()
    this.oPgFrm.Page1.oPag.oARCATCON_1_28.enabled = this.oPgFrm.Page1.oPag.oARCATCON_1_28.mCond()
    this.oPgFrm.Page1.oPag.oARPREZUM_1_29.enabled = this.oPgFrm.Page1.oPag.oARPREZUM_1_29.mCond()
    this.oPgFrm.Page1.oPag.oARGRUMER_1_30.enabled = this.oPgFrm.Page1.oPag.oARGRUMER_1_30.mCond()
    this.oPgFrm.Page1.oPag.oARSTASUP_1_33.enabled = this.oPgFrm.Page1.oPag.oARSTASUP_1_33.mCond()
    this.oPgFrm.Page1.oPag.oARSTACOD_1_37.enabled = this.oPgFrm.Page1.oPag.oARSTACOD_1_37.mCond()
    this.oPgFrm.Page1.oPag.oARVOCCEN_1_41.enabled = this.oPgFrm.Page1.oPag.oARVOCCEN_1_41.mCond()
    this.oPgFrm.Page1.oPag.oARVOCRIC_1_42.enabled = this.oPgFrm.Page1.oPag.oARVOCRIC_1_42.mCond()
    this.oPgFrm.Page1.oPag.oARTIPRIG_1_46.enabled = this.oPgFrm.Page1.oPag.oARTIPRIG_1_46.mCond()
    this.oPgFrm.Page1.oPag.oARTIPRI2_1_47.enabled = this.oPgFrm.Page1.oPag.oARTIPRI2_1_47.mCond()
    this.oPgFrm.Page1.oPag.oARCODFAS_1_48.enabled = this.oPgFrm.Page1.oPag.oARCODFAS_1_48.mCond()
    this.oPgFrm.Page1.oPag.oARUTISER_1_49.enabled = this.oPgFrm.Page1.oPag.oARUTISER_1_49.mCond()
    this.oPgFrm.Page1.oPag.oARNOMENC_1_51.enabled = this.oPgFrm.Page1.oPag.oARNOMENC_1_51.mCond()
    this.oPgFrm.Page1.oPag.oARMOLSUP_1_53.enabled = this.oPgFrm.Page1.oPag.oARMOLSUP_1_53.mCond()
    this.oPgFrm.Page1.oPag.oARDATINT_1_54.enabled = this.oPgFrm.Page1.oPag.oARDATINT_1_54.mCond()
    this.oPgFrm.Page1.oPag.oARPARAME_1_140.enabled = this.oPgFrm.Page1.oPag.oARPARAME_1_140.mCond()
    this.oPgFrm.Page2.oPag.oARGRUPRO_2_4.enabled = this.oPgFrm.Page2.oPag.oARGRUPRO_2_4.mCond()
    this.oPgFrm.Page2.oPag.oARCATSCM_2_6.enabled = this.oPgFrm.Page2.oPag.oARCATSCM_2_6.mCond()
    this.oPgFrm.Page2.oPag.oARCODREP_2_13.enabled = this.oPgFrm.Page2.oPag.oARCODREP_2_13.mCond()
    this.oPgFrm.Page2.oPag.oARCODGRU_2_16.enabled = this.oPgFrm.Page2.oPag.oARCODGRU_2_16.mCond()
    this.oPgFrm.Page2.oPag.oARCODSOT_2_17.enabled = this.oPgFrm.Page2.oPag.oARCODSOT_2_17.mCond()
    this.oPgFrm.Page2.oPag.oARCONTRI_2_20.enabled = this.oPgFrm.Page2.oPag.oARCONTRI_2_20.mCond()
    this.oPgFrm.Page2.oPag.oARARTPOS_2_25.enabled = this.oPgFrm.Page2.oPag.oARARTPOS_2_25.mCond()
    this.oPgFrm.Page2.oPag.oARPUBWEB_2_27.enabled = this.oPgFrm.Page2.oPag.oARPUBWEB_2_27.mCond()
    this.oPgFrm.Page2.oPag.oARPUBWEB_2_28.enabled = this.oPgFrm.Page2.oPag.oARPUBWEB_2_28.mCond()
    this.oPgFrm.Page2.oPag.oARPUBWEB_2_29.enabled = this.oPgFrm.Page2.oPag.oARPUBWEB_2_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_72.enabled = this.oPgFrm.Page1.oPag.oBtn_1_72.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_98.enabled = this.oPgFrm.Page1.oPag.oBtn_1_98.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_99.enabled = this.oPgFrm.Page1.oPag.oBtn_1_99.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_100.enabled = this.oPgFrm.Page1.oPag.oBtn_1_100.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_101.enabled = this.oPgFrm.Page1.oPag.oBtn_1_101.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_103.enabled = this.oPgFrm.Page1.oPag.oBtn_1_103.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_114.enabled = this.oPgFrm.Page1.oPag.oBtn_1_114.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_75.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_75.mCond()
    this.gsma_mtt.enabled = this.oPgFrm.Page4.oPag.oLinkPC_4_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(3).enabled=not(g_GPFA<>"S")
    local i_show2
    i_show2=not(g_GPFA<>"S")
    this.oPgFrm.Pages(3).enabled=i_show2 and not(g_GPFA<>"S")
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Premi"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    local i_show3
    i_show3=not(NOT IsAlt())
    this.oPgFrm.Pages(4).enabled=i_show3
    this.oPgFrm.Pages(4).caption=iif(i_show3,cp_translate("Listini"),"")
    this.oPgFrm.Pages(4).oPag.visible=this.oPgFrm.Pages(4).enabled
    local i_show4
    i_show4=not(IsAlt())
    this.oPgFrm.Pages(5).enabled=i_show4
    this.oPgFrm.Pages(5).caption=iif(i_show4,cp_translate("Note"),"")
    this.oPgFrm.Pages(5).oPag.visible=this.oPgFrm.Pages(5).enabled
    this.oPgFrm.Page1.oPag.oARFLSERG_1_12.visible=!this.oPgFrm.Page1.oPag.oARFLSERG_1_12.mHide()
    this.oPgFrm.Page1.oPag.oARTIPSER_1_13.visible=!this.oPgFrm.Page1.oPag.oARTIPSER_1_13.mHide()
    this.oPgFrm.Page1.oPag.oARPRESTA_1_15.visible=!this.oPgFrm.Page1.oPag.oARPRESTA_1_15.mHide()
    this.oPgFrm.Page1.oPag.oARFLUSEP_1_21.visible=!this.oPgFrm.Page1.oPag.oARFLUSEP_1_21.mHide()
    this.oPgFrm.Page1.oPag.oARFLUNIV_1_22.visible=!this.oPgFrm.Page1.oPag.oARFLUNIV_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oARNUMPRE_1_24.visible=!this.oPgFrm.Page1.oPag.oARNUMPRE_1_24.mHide()
    this.oPgFrm.Page1.oPag.oARGRUMER_1_30.visible=!this.oPgFrm.Page1.oPag.oARGRUMER_1_30.mHide()
    this.oPgFrm.Page1.oPag.oARSTASUP_1_31.visible=!this.oPgFrm.Page1.oPag.oARSTASUP_1_31.mHide()
    this.oPgFrm.Page1.oPag.oARSTACOD_1_32.visible=!this.oPgFrm.Page1.oPag.oARSTACOD_1_32.mHide()
    this.oPgFrm.Page1.oPag.oARSTASUP_1_33.visible=!this.oPgFrm.Page1.oPag.oARSTASUP_1_33.mHide()
    this.oPgFrm.Page1.oPag.oARFLSPAN_1_35.visible=!this.oPgFrm.Page1.oPag.oARFLSPAN_1_35.mHide()
    this.oPgFrm.Page1.oPag.oARSCRITT_1_36.visible=!this.oPgFrm.Page1.oPag.oARSCRITT_1_36.mHide()
    this.oPgFrm.Page1.oPag.oARSTACOD_1_37.visible=!this.oPgFrm.Page1.oPag.oARSTACOD_1_37.mHide()
    this.oPgFrm.Page1.oPag.oARFLPRPE_1_38.visible=!this.oPgFrm.Page1.oPag.oARFLPRPE_1_38.mHide()
    this.oPgFrm.Page1.oPag.oARFLESCT_1_39.visible=!this.oPgFrm.Page1.oPag.oARFLESCT_1_39.mHide()
    this.oPgFrm.Page1.oPag.oDESGRU_1_40.visible=!this.oPgFrm.Page1.oPag.oDESGRU_1_40.mHide()
    this.oPgFrm.Page1.oPag.oARDTINVA_1_43.visible=!this.oPgFrm.Page1.oPag.oARDTINVA_1_43.mHide()
    this.oPgFrm.Page1.oPag.oARTIPRES_1_45.visible=!this.oPgFrm.Page1.oPag.oARTIPRES_1_45.mHide()
    this.oPgFrm.Page1.oPag.oARTIPRIG_1_46.visible=!this.oPgFrm.Page1.oPag.oARTIPRIG_1_46.mHide()
    this.oPgFrm.Page1.oPag.oARTIPRI2_1_47.visible=!this.oPgFrm.Page1.oPag.oARTIPRI2_1_47.mHide()
    this.oPgFrm.Page1.oPag.oARCODFAS_1_48.visible=!this.oPgFrm.Page1.oPag.oARCODFAS_1_48.mHide()
    this.oPgFrm.Page1.oPag.oARPERSER_1_50.visible=!this.oPgFrm.Page1.oPag.oARPERSER_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_75.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_75.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_76.visible=!this.oPgFrm.Page1.oPag.oStr_1_76.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_95.visible=!this.oPgFrm.Page1.oPag.oStr_1_95.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_98.visible=!this.oPgFrm.Page1.oPag.oBtn_1_98.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_99.visible=!this.oPgFrm.Page1.oPag.oBtn_1_99.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_100.visible=!this.oPgFrm.Page1.oPag.oBtn_1_100.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_101.visible=!this.oPgFrm.Page1.oPag.oBtn_1_101.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_102.visible=!this.oPgFrm.Page1.oPag.oStr_1_102.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_103.visible=!this.oPgFrm.Page1.oPag.oBtn_1_103.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_113.visible=!this.oPgFrm.Page1.oPag.oStr_1_113.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_114.visible=!this.oPgFrm.Page1.oPag.oBtn_1_114.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_115.visible=!this.oPgFrm.Page1.oPag.oStr_1_115.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_118.visible=!this.oPgFrm.Page1.oPag.oStr_1_118.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_119.visible=!this.oPgFrm.Page1.oPag.oStr_1_119.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_123.visible=!this.oPgFrm.Page1.oPag.oStr_1_123.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_125.visible=!this.oPgFrm.Page1.oPag.oStr_1_125.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_131.visible=!this.oPgFrm.Page1.oPag.oBtn_1_131.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_137.visible=!this.oPgFrm.Page1.oPag.oStr_1_137.mHide()
    this.oPgFrm.Page1.oPag.oARPARAME_1_140.visible=!this.oPgFrm.Page1.oPag.oARPARAME_1_140.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_141.visible=!this.oPgFrm.Page1.oPag.oStr_1_141.mHide()
    this.oPgFrm.Page2.oPag.oARGRUPRO_2_4.visible=!this.oPgFrm.Page2.oPag.oARGRUPRO_2_4.mHide()
    this.oPgFrm.Page2.oPag.oDESGPP_2_5.visible=!this.oPgFrm.Page2.oPag.oDESGPP_2_5.mHide()
    this.oPgFrm.Page2.oPag.oARCATSCM_2_6.visible=!this.oPgFrm.Page2.oPag.oARCATSCM_2_6.mHide()
    this.oPgFrm.Page2.oPag.oDESSCM_2_7.visible=!this.oPgFrm.Page2.oPag.oDESSCM_2_7.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_8.visible=!this.oPgFrm.Page2.oPag.oStr_2_8.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_9.visible=!this.oPgFrm.Page2.oPag.oStr_2_9.mHide()
    this.oPgFrm.Page2.oPag.oARCODREP_2_13.visible=!this.oPgFrm.Page2.oPag.oARCODREP_2_13.mHide()
    this.oPgFrm.Page2.oPag.oDESREP_2_14.visible=!this.oPgFrm.Page2.oPag.oDESREP_2_14.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_15.visible=!this.oPgFrm.Page2.oPag.oStr_2_15.mHide()
    this.oPgFrm.Page2.oPag.oARCODGRU_2_16.visible=!this.oPgFrm.Page2.oPag.oARCODGRU_2_16.mHide()
    this.oPgFrm.Page2.oPag.oARCODSOT_2_17.visible=!this.oPgFrm.Page2.oPag.oARCODSOT_2_17.mHide()
    this.oPgFrm.Page2.oPag.oARFLCESP_2_18.visible=!this.oPgFrm.Page2.oPag.oARFLCESP_2_18.mHide()
    this.oPgFrm.Page2.oPag.oARCATCES_2_19.visible=!this.oPgFrm.Page2.oPag.oARCATCES_2_19.mHide()
    this.oPgFrm.Page2.oPag.oARCONTRI_2_20.visible=!this.oPgFrm.Page2.oPag.oARCONTRI_2_20.mHide()
    this.oPgFrm.Page2.oPag.oARMINVEN_2_21.visible=!this.oPgFrm.Page2.oPag.oARMINVEN_2_21.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_22.visible=!this.oPgFrm.Page2.oPag.oStr_2_22.mHide()
    this.oPgFrm.Page2.oPag.oARRIPCON_2_24.visible=!this.oPgFrm.Page2.oPag.oARRIPCON_2_24.mHide()
    this.oPgFrm.Page2.oPag.oARARTPOS_2_25.visible=!this.oPgFrm.Page2.oPag.oARARTPOS_2_25.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_26.visible=!this.oPgFrm.Page2.oPag.oStr_2_26.mHide()
    this.oPgFrm.Page2.oPag.oARPUBWEB_2_27.visible=!this.oPgFrm.Page2.oPag.oARPUBWEB_2_27.mHide()
    this.oPgFrm.Page2.oPag.oARPUBWEB_2_28.visible=!this.oPgFrm.Page2.oPag.oARPUBWEB_2_28.mHide()
    this.oPgFrm.Page2.oPag.oARPUBWEB_2_29.visible=!this.oPgFrm.Page2.oPag.oARPUBWEB_2_29.mHide()
    this.oPgFrm.Page2.oPag.oDESSOT_2_30.visible=!this.oPgFrm.Page2.oPag.oDESSOT_2_30.mHide()
    this.oPgFrm.Page2.oPag.oDESTIT_2_31.visible=!this.oPgFrm.Page2.oPag.oDESTIT_2_31.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_34.visible=!this.oPgFrm.Page2.oPag.oStr_2_34.mHide()
    this.oPgFrm.Page2.oPag.oDESCACES_2_35.visible=!this.oPgFrm.Page2.oPag.oDESCACES_2_35.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_36.visible=!this.oPgFrm.Page2.oPag.oStr_2_36.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_37.visible=!this.oPgFrm.Page2.oPag.oStr_2_37.mHide()
    this.oPgFrm.Page2.oPag.oDESTIPCO_2_38.visible=!this.oPgFrm.Page2.oPag.oDESTIPCO_2_38.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_40.visible=!this.oPgFrm.Page2.oPag.oStr_2_40.mHide()
    this.oPgFrm.Page4.oPag.oCODI_4_1.visible=!this.oPgFrm.Page4.oPag.oCODI_4_1.mHide()
    this.oPgFrm.Page4.oPag.oDESC_4_2.visible=!this.oPgFrm.Page4.oPag.oDESC_4_2.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_3.visible=!this.oPgFrm.Page4.oPag.oStr_4_3.mHide()
    this.oPgFrm.Page4.oPag.oLinkPC_4_4.visible=!this.oPgFrm.Page4.oPag.oLinkPC_4_4.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_71.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_80.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_81.Event(cEvent)
        if lower(cEvent)==lower("Insert start")
          .Calculate_OLYGVFQXTR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete start")
          .Calculate_CZGHMRBITS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Insert start") or lower(cEvent)==lower("Update start")
          .Calculate_YBHCACTYYL()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsma_aas
    IF (CEVENT='w_ARPARAME Changed' OR CEVENT='w_ARCODFAS Changed') AND Not Empty(This.w_ARCODFAS) and This.w_ARPARAME$'4S'
        Local cursore
        * eseguo controllo presenza altro parametro con la stessa fase
        cursore=readtable('ART_ICOL','ARCODART',' ',null,.f.," (ARPARAME='S' OR ARPARAME='4') AND ARCODFAS = " + cp_ToStr( This.w_ARCODFAS) + " and ARCODART <> " + cp_ToStr( This.w_ARCODART))
        if  Reccount(cursore)>0
          Ah_errormsg('Attenzione esiste gi� un parametro collegato alla stessa fase')
          This.w_ARPARAME='N'
        endif
        if used((cursore))
          Select ((cursore))
          Use
        Endif
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AUTOAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
    i_lTable = "NUMAUT_M"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2], .t., this.NUMAUT_M_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AUTOAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AUTOAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NATIPGES,NACODAZI,NAACTIVE";
                   +" from "+i_cTable+" "+i_lTable+" where NACODAZI="+cp_ToStrODBC(this.w_AUTOAZI);
                   +" and NATIPGES="+cp_ToStrODBC('AS');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NATIPGES','AS';
                       ,'NACODAZI',this.w_AUTOAZI)
            select NATIPGES,NACODAZI,NAACTIVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AUTOAZI = NVL(_Link_.NACODAZI,space(5))
      this.w_AUTO = NVL(_Link_.NAACTIVE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AUTOAZI = space(5)
      endif
      this.w_AUTO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])+'\'+cp_ToStr(_Link_.NATIPGES,1)+'\'+cp_ToStr(_Link_.NACODAZI,1)
      cp_ShowWarn(i_cKey,this.NUMAUT_M_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AUTOAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PARALTE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PARALTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PARALTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PANOEDES,PAPREACC,PAPREFOR";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_PARALTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_PARALTE)
            select PACODAZI,PANOEDES,PAPREACC,PAPREFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PARALTE = NVL(_Link_.PACODAZI,space(10))
      this.w_NOEDES = NVL(_Link_.PANOEDES,space(1))
      this.w_PAPREACC = NVL(_Link_.PAPREACC,space(20))
      this.w_PAPREFOR = NVL(_Link_.PAPREFOR,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PARALTE = space(10)
      endif
      this.w_NOEDES = space(1)
      this.w_PAPREACC = space(20)
      this.w_PAPREFOR = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PARALTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARUNMIS1
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARUNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ARUNMIS1)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLTEMP,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ARUNMIS1))
          select UMCODICE,UMFLTEMP,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARUNMIS1)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARUNMIS1) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oARUNMIS1_1_17'),i_cWhere,'GSAR_AUM',"Unita di misura",'Tariffario.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLTEMP,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLTEMP,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARUNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLTEMP,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ARUNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ARUNMIS1)
            select UMCODICE,UMFLTEMP,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARUNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_UMFLTEMP = NVL(_Link_.UMFLTEMP,space(1))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARUNMIS1 = space(3)
      endif
      this.w_UMFLTEMP = space(1)
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF(.w_ARPRESTA='P', .w_UMFLTEMP='S', .T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un'unit� di misura gestita a tempo!")
        endif
        this.w_ARUNMIS1 = space(3)
        this.w_UMFLTEMP = space(1)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARUNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.UMCODICE as UMCODICE117"+ ",link_1_17.UMFLTEMP as UMFLTEMP117"+ ",link_1_17.UMFLFRAZ as UMFLFRAZ117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on ART_ICOL.ARUNMIS1=link_1_17.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and ART_ICOL.ARUNMIS1=link_1_17.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARUNMIS2
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARUNMIS2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_ARUNMIS2)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_ARUNMIS2))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARUNMIS2)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARUNMIS2) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oARUNMIS2_1_18'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARUNMIS2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ARUNMIS2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ARUNMIS2)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARUNMIS2 = NVL(_Link_.UMCODICE,space(3))
      this.w_F2FRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARUNMIS2 = space(3)
      endif
      this.w_F2FRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARUNMIS2<>.w_ARUNMIS1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ARUNMIS2 = space(3)
        this.w_F2FRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARUNMIS2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.UMCODICE as UMCODICE118"+ ",link_1_18.UMFLFRAZ as UMFLFRAZ118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on ART_ICOL.ARUNMIS2=link_1_18.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and ART_ICOL.ARUNMIS2=link_1_18.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARTIPOPE
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTIPOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATO',True,'TIPCODIV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_ARTIPOPE)+"%");
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ARCATOPE);

          i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TI__TIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TI__TIPO',this.w_ARCATOPE;
                     ,'TICODICE',trim(this.w_ARTIPOPE))
          select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TI__TIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTIPOPE)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTIPOPE) and !this.bDontReportError
            deferred_cp_zoom('TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(oSource.parent,'oARTIPOPE_1_25'),i_cWhere,'GSAR_ATO',"Operazioni IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ARCATOPE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice obsoleto oppure inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TI__TIPO="+cp_ToStrODBC(this.w_ARCATOPE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTIPOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_ARTIPOPE);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ARCATOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_ARCATOPE;
                       ,'TICODICE',this.w_ARTIPOPE)
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTIPOPE = NVL(_Link_.TICODICE,space(10))
      this.w_TIDESCRI = NVL(_Link_.TIDESCRI,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.TIDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTIPOPE = space(10)
      endif
      this.w_TIDESCRI = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice obsoleto oppure inesistente")
        endif
        this.w_ARTIPOPE = space(10)
        this.w_TIDESCRI = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTIPOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCODIVA
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_ARCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_ARCODIVA))
          select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_ARCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_ARCODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oARCODIVA_1_26'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_ARCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_ARCODIVA)
            select IVCODIVA,IVDESIVA,IVDTOBSO,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_ARCODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PERIVA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
        endif
        this.w_ARCODIVA = space(5)
        this.w_DESIVA = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PERIVA = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.IVCODIVA as IVCODIVA126"+ ",link_1_26.IVDESIVA as IVDESIVA126"+ ",link_1_26.IVDTOBSO as IVDTOBSO126"+ ",link_1_26.IVPERIVA as IVPERIVA126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on ART_ICOL.ARCODIVA=link_1_26.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODIVA=link_1_26.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCATCON
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_lTable = "CACOARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2], .t., this.CACOARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC1',True,'CACOARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_ARCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_ARCATCON))
          select C1CODICE,C1DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCATCON)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOARTI','*','C1CODICE',cp_AbsName(oSource.parent,'oARCATCON_1_28'),i_cWhere,'GSAR_AC1',"Categorie contabili articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_ARCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_ARCATCON)
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCATCON = NVL(_Link_.C1CODICE,space(5))
      this.w_DESCAT = NVL(_Link_.C1DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARCATCON = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_28(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOARTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_28.C1CODICE as C1CODICE128"+ ",link_1_28.C1DESCRI as C1DESCRI128"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_28 on ART_ICOL.ARCATCON=link_1_28.C1CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_28"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCATCON=link_1_28.C1CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARGRUMER
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARGRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_ARGRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_ARGRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARGRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( GMDESCRI like "+cp_ToStrODBC(trim(this.w_ARGRUMER)+"%")+cp_TransWhereFldName('GMDESCRI',trim(this.w_ARGRUMER))+")";

            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GMDESCRI like "+cp_ToStr(trim(this.w_ARGRUMER)+"%");

            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARGRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oARGRUMER_1_30'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARGRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_ARGRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_ARGRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARGRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ARGRUMER = space(5)
      endif
      this.w_DESGRU = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARGRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_30.GMCODICE as GMCODICE130"+ ","+cp_TransLinkFldName('link_1_30.GMDESCRI')+" as GMDESCRI130"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_30 on ART_ICOL.ARGRUMER=link_1_30.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_30"
          i_cKey=i_cKey+'+" and ART_ICOL.ARGRUMER=link_1_30.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARVOCCEN
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARVOCCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_ARVOCCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_ARVOCCEN))
          select VCCODICE,VCDESCRI,VCTIPVOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARVOCCEN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARVOCCEN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oARVOCCEN_1_41'),i_cWhere,'GSCA_AVC',"Voci di costo",'GSMACAAR.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARVOCCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_ARVOCCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_ARVOCCEN)
            select VCCODICE,VCDESCRI,VCTIPVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARVOCCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARVOCCEN = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_TIPVOC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOC<>'R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di costo incongruente")
        endif
        this.w_ARVOCCEN = space(15)
        this.w_DESVOC = space(40)
        this.w_TIPVOC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARVOCCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_41(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_41.VCCODICE as VCCODICE141"+ ",link_1_41.VCDESCRI as VCDESCRI141"+ ",link_1_41.VCTIPVOC as VCTIPVOC141"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_41 on ART_ICOL.ARVOCCEN=link_1_41.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_41"
          i_cKey=i_cKey+'+" and ART_ICOL.ARVOCCEN=link_1_41.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARVOCRIC
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARVOCRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_ARVOCRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_ARVOCRIC))
          select VCCODICE,VCDESCRI,VCTIPVOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARVOCRIC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARVOCRIC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oARVOCRIC_1_42'),i_cWhere,'GSCA_AVC',"Voci di ricavo",'GSMARAAR.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCTIPVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARVOCRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCTIPVOC";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_ARVOCRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_ARVOCRIC)
            select VCCODICE,VCDESCRI,VCTIPVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARVOCRIC = NVL(_Link_.VCCODICE,space(15))
      this.w_DESRIC = NVL(_Link_.VCDESCRI,space(40))
      this.w_TIPRIC = NVL(_Link_.VCTIPVOC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARVOCRIC = space(15)
      endif
      this.w_DESRIC = space(40)
      this.w_TIPRIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPRIC<>'C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di ricavo incongruente")
        endif
        this.w_ARVOCRIC = space(15)
        this.w_DESRIC = space(40)
        this.w_TIPRIC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARVOCRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_42(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_42.VCCODICE as VCCODICE142"+ ",link_1_42.VCDESCRI as VCDESCRI142"+ ",link_1_42.VCTIPVOC as VCTIPVOC142"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_42 on ART_ICOL.ARVOCRIC=link_1_42.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_42"
          i_cKey=i_cKey+'+" and ART_ICOL.ARVOCRIC=link_1_42.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARTIPRES
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPI_PRE_IDX,3]
    i_lTable = "TIPI_PRE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPI_PRE_IDX,2], .t., this.TIPI_PRE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPI_PRE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTIPRES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIPI_PRE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TPCODICE like "+cp_ToStrODBC(trim(this.w_ARTIPRES)+"%");

          i_ret=cp_SQL(i_nConn,"select TPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TPCODICE',trim(this.w_ARTIPRES))
          select TPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTIPRES)==trim(_Link_.TPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTIPRES) and !this.bDontReportError
            deferred_cp_zoom('TIPI_PRE','*','TPCODICE',cp_AbsName(oSource.parent,'oARTIPRES_1_45'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',oSource.xKey(1))
            select TPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTIPRES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(this.w_ARTIPRES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',this.w_ARTIPRES)
            select TPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTIPRES = NVL(_Link_.TPCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ARTIPRES = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPI_PRE_IDX,2])+'\'+cp_ToStr(_Link_.TPCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPI_PRE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTIPRES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCODFAS
  func Link_1_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAS_TARI_IDX,3]
    i_lTable = "FAS_TARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAS_TARI_IDX,2], .t., this.FAS_TARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAS_TARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODFAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AFT',True,'FAS_TARI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FTCODICE like "+cp_ToStrODBC(trim(this.w_ARCODFAS)+"%");

          i_ret=cp_SQL(i_nConn,"select FTCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FTCODICE',trim(this.w_ARCODFAS))
          select FTCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODFAS)==trim(_Link_.FTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODFAS) and !this.bDontReportError
            deferred_cp_zoom('FAS_TARI','*','FTCODICE',cp_AbsName(oSource.parent,'oARCODFAS_1_48'),i_cWhere,'GSMA_AFT',"Fasi per liquidazione compensi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where FTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FTCODICE',oSource.xKey(1))
            select FTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODFAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where FTCODICE="+cp_ToStrODBC(this.w_ARCODFAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FTCODICE',this.w_ARCODFAS)
            select FTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODFAS = NVL(_Link_.FTCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODFAS = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAS_TARI_IDX,2])+'\'+cp_ToStr(_Link_.FTCODICE,1)
      cp_ShowWarn(i_cKey,this.FAS_TARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODFAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARNOMENC
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
    i_lTable = "NOMENCLA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2], .t., this.NOMENCLA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARNOMENC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANM',True,'NOMENCLA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NMCODICE like "+cp_ToStrODBC(trim(this.w_ARNOMENC)+"%");

          i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI,NMUNISUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NMCODICE',trim(this.w_ARNOMENC))
          select NMCODICE,NMDESCRI,NMUNISUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARNOMENC)==trim(_Link_.NMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARNOMENC) and !this.bDontReportError
            deferred_cp_zoom('NOMENCLA','*','NMCODICE',cp_AbsName(oSource.parent,'oARNOMENC_1_51'),i_cWhere,'GSAR_ANM',"",'GSMA_AZN.NOMENCLA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI,NMUNISUP";
                     +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',oSource.xKey(1))
            select NMCODICE,NMDESCRI,NMUNISUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARNOMENC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMDESCRI,NMUNISUP";
                   +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(this.w_ARNOMENC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',this.w_ARNOMENC)
            select NMCODICE,NMDESCRI,NMUNISUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARNOMENC = NVL(_Link_.NMCODICE,space(8))
      this.w_DESNOM = NVL(_Link_.NMDESCRI,space(35))
      this.w_ARUMSUPP = NVL(_Link_.NMUNISUP,space(3))
      this.w_UMSUPP = NVL(_Link_.NMUNISUP,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ARNOMENC = space(8)
      endif
      this.w_DESNOM = space(35)
      this.w_ARUMSUPP = space(3)
      this.w_UMSUPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ARUTISER='N' OR LEN(ALLTRIM(.w_ARNOMENC))=6 OR LEN(ALLTRIM(.w_ARNOMENC))=5
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o incongruente")
        endif
        this.w_ARNOMENC = space(8)
        this.w_DESNOM = space(35)
        this.w_ARUMSUPP = space(3)
        this.w_UMSUPP = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])+'\'+cp_ToStr(_Link_.NMCODICE,1)
      cp_ShowWarn(i_cKey,this.NOMENCLA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARNOMENC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_51(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NOMENCLA_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_51.NMCODICE as NMCODICE151"+ ",link_1_51.NMDESCRI as NMDESCRI151"+ ",link_1_51.NMUNISUP as NMUNISUP151"+ ",link_1_51.NMUNISUP as NMUNISUP151"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_51 on ART_ICOL.ARNOMENC=link_1_51.NMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_51"
          i_cKey=i_cKey+'+" and ART_ICOL.ARNOMENC=link_1_51.NMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARUMSUPP
  func Link_1_112(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARUMSUPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARUMSUPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_ARUMSUPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_ARUMSUPP)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARUMSUPP = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ARUMSUPP = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARUMSUPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PAPREACC
  func Link_1_132(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAPREACC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAPREACC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PAPREACC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PAPREACC)
            select CACODICE,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAPREACC = NVL(_Link_.CACODICE,space(20))
      this.w_PAPREACC_ART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PAPREACC = space(20)
      endif
      this.w_PAPREACC_ART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAPREACC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PAPREFOR
  func Link_1_133(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAPREFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAPREFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PAPREFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PAPREFOR)
            select CACODICE,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAPREFOR = NVL(_Link_.CACODICE,space(20))
      this.w_PAPREFOR_ART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PAPREFOR = space(20)
      endif
      this.w_PAPREFOR_ART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAPREFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LeggePAR_ALTE
  func Link_1_136(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LeggePAR_ALTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LeggePAR_ALTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAPREACC,PAPREFOR";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_LeggePAR_ALTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_LeggePAR_ALTE)
            select PACODAZI,PAPREACC,PAPREFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LeggePAR_ALTE = NVL(_Link_.PACODAZI,space(20))
      this.w_PAPREACC = NVL(_Link_.PAPREACC,space(20))
      this.w_PAPREFOR = NVL(_Link_.PAPREFOR,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LeggePAR_ALTE = space(20)
      endif
      this.w_PAPREACC = space(20)
      this.w_PAPREFOR = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LeggePAR_ALTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARGRUPRO
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_lTable = "GRUPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2], .t., this.GRUPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARGRUPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGP',True,'GRUPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_ARGRUPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_ARGRUPRO))
          select GPCODICE,GPDESCRI,GPTIPPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARGRUPRO)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARGRUPRO) and !this.bDontReportError
            deferred_cp_zoom('GRUPRO','*','GPCODICE',cp_AbsName(oSource.parent,'oARGRUPRO_2_4'),i_cWhere,'GSAR_AGP',"Categorie provvigioni",'GSMA_AAR.GRUPRO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARGRUPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_ARGRUPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_ARGRUPRO)
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARGRUPRO = NVL(_Link_.GPCODICE,space(5))
      this.w_DESGPP = NVL(_Link_.GPDESCRI,space(35))
      this.w_FLPRO = NVL(_Link_.GPTIPPRO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARGRUPRO = space(5)
      endif
      this.w_DESGPP = space(35)
      this.w_FLPRO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLPRO $ ' A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria provvigioni inesistente o non di tipo articolo")
        endif
        this.w_ARGRUPRO = space(5)
        this.w_DESGPP = space(35)
        this.w_FLPRO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARGRUPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUPRO_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.GPCODICE as GPCODICE204"+ ",link_2_4.GPDESCRI as GPDESCRI204"+ ",link_2_4.GPTIPPRO as GPTIPPRO204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on ART_ICOL.ARGRUPRO=link_2_4.GPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and ART_ICOL.ARGRUPRO=link_2_4.GPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCATSCM
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_lTable = "CAT_SCMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2], .t., this.CAT_SCMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCATSCM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASM',True,'CAT_SCMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_ARCATSCM)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_ARCATSCM))
          select CSCODICE,CSDESCRI,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCATSCM)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCATSCM) and !this.bDontReportError
            deferred_cp_zoom('CAT_SCMA','*','CSCODICE',cp_AbsName(oSource.parent,'oARCATSCM_2_6'),i_cWhere,'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSMA_AAR.CAT_SCMA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCATSCM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_ARCATSCM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_ARCATSCM)
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCATSCM = NVL(_Link_.CSCODICE,space(5))
      this.w_DESSCM = NVL(_Link_.CSDESCRI,space(35))
      this.w_FLSCM = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARCATSCM = space(5)
      endif
      this.w_DESSCM = space(35)
      this.w_FLSCM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLSCM $ ' A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria sconti / maggiorazioni inesistente o non di tipo articolo")
        endif
        this.w_ARCATSCM = space(5)
        this.w_DESSCM = space(35)
        this.w_FLSCM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_SCMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCATSCM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_SCMA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.CSCODICE as CSCODICE206"+ ",link_2_6.CSDESCRI as CSDESCRI206"+ ",link_2_6.CSTIPCAT as CSTIPCAT206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on ART_ICOL.ARCATSCM=link_2_6.CSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCATSCM=link_2_6.CSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODCLA
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_lTable = "CLA_RIGD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2], .t., this.CLA_RIGD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATR',True,'CLA_RIGD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODCLA like "+cp_ToStrODBC(trim(this.w_ARCODCLA)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODCLA,TRDESCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODCLA',trim(this.w_ARCODCLA))
          select TRCODCLA,TRDESCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODCLA)==trim(_Link_.TRCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODCLA) and !this.bDontReportError
            deferred_cp_zoom('CLA_RIGD','*','TRCODCLA',cp_AbsName(oSource.parent,'oARCODCLA_2_10'),i_cWhere,'GSAR_ATR',"Tipologie righe documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA,TRDESCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',oSource.xKey(1))
            select TRCODCLA,TRDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODCLA,TRDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODCLA="+cp_ToStrODBC(this.w_ARCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODCLA',this.w_ARCODCLA)
            select TRCODCLA,TRDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODCLA = NVL(_Link_.TRCODCLA,space(3))
      this.w_DESCLA = NVL(_Link_.TRDESCLA,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODCLA = space(3)
      endif
      this.w_DESCLA = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])+'\'+cp_ToStr(_Link_.TRCODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_RIGD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLA_RIGD_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_10.TRCODCLA as TRCODCLA210"+ ",link_2_10.TRDESCLA as TRDESCLA210"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_10 on ART_ICOL.ARCODCLA=link_2_10.TRCODCLA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_10"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODCLA=link_2_10.TRCODCLA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODREP
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_lTable = "REP_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2], .t., this.REP_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODREP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ARE',True,'REP_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODREP like "+cp_ToStrODBC(trim(this.w_ARCODREP)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP,RECODIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODREP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODREP',trim(this.w_ARCODREP))
          select RECODREP,REDESREP,RECODIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODREP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODREP)==trim(_Link_.RECODREP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODREP) and !this.bDontReportError
            deferred_cp_zoom('REP_ARTI','*','RECODREP',cp_AbsName(oSource.parent,'oARCODREP_2_13'),i_cWhere,'GSPS_ARE',"Reparti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP,RECODIVA";
                     +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',oSource.xKey(1))
            select RECODREP,REDESREP,RECODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODREP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP,RECODIVA";
                   +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(this.w_ARCODREP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',this.w_ARCODREP)
            select RECODREP,REDESREP,RECODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODREP = NVL(_Link_.RECODREP,space(3))
      this.w_DESREP = NVL(_Link_.REDESREP,space(40))
      this.w_IVAREP = NVL(_Link_.RECODIVA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODREP = space(3)
      endif
      this.w_DESREP = space(40)
      this.w_IVAREP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.RECODREP,1)
      cp_ShowWarn(i_cKey,this.REP_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODREP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.REP_ARTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.RECODREP as RECODREP213"+ ",link_2_13.REDESREP as REDESREP213"+ ",link_2_13.RECODIVA as RECODIVA213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on ART_ICOL.ARCODREP=link_2_13.RECODREP"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODREP=link_2_13.RECODREP(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODGRU
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIT_SEZI_IDX,3]
    i_lTable = "TIT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2], .t., this.TIT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ATS',True,'TIT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TSCODICE like "+cp_ToStrODBC(trim(this.w_ARCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TSCODICE',trim(this.w_ARCODGRU))
          select TSCODICE,TSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODGRU)==trim(_Link_.TSCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TSDESCRI like "+cp_ToStrODBC(trim(this.w_ARCODGRU)+"%");

            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TSDESCRI like "+cp_ToStr(trim(this.w_ARCODGRU)+"%");

            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARCODGRU) and !this.bDontReportError
            deferred_cp_zoom('TIT_SEZI','*','TSCODICE',cp_AbsName(oSource.parent,'oARCODGRU_2_16'),i_cWhere,'GSOF_ATS',"Codici gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',oSource.xKey(1))
            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TSCODICE,TSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TSCODICE="+cp_ToStrODBC(this.w_ARCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TSCODICE',this.w_ARCODGRU)
            select TSCODICE,TSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODGRU = NVL(_Link_.TSCODICE,space(5))
      this.w_DESTIT = NVL(_Link_.TSDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODGRU = space(5)
      endif
      this.w_DESTIT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.TSCODICE,1)
      cp_ShowWarn(i_cKey,this.TIT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIT_SEZI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIT_SEZI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_16.TSCODICE as TSCODICE216"+ ",link_2_16.TSDESCRI as TSDESCRI216"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_16 on ART_ICOL.ARCODGRU=link_2_16.TSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_16"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODGRU=link_2_16.TSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCODSOT
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SOT_SEZI_IDX,3]
    i_lTable = "SOT_SEZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2], .t., this.SOT_SEZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODSOT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_ASF',True,'SOT_SEZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SGCODSOT like "+cp_ToStrODBC(trim(this.w_ARCODSOT)+"%");
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_ARCODGRU);

          i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SGCODGRU,SGCODSOT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SGCODGRU',this.w_ARCODGRU;
                     ,'SGCODSOT',trim(this.w_ARCODSOT))
          select SGCODGRU,SGCODSOT,SGDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SGCODGRU,SGCODSOT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODSOT)==trim(_Link_.SGCODSOT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" SGDESCRI like "+cp_ToStrODBC(trim(this.w_ARCODSOT)+"%");
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_ARCODGRU);

            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" SGDESCRI like "+cp_ToStr(trim(this.w_ARCODSOT)+"%");
                   +" and SGCODGRU="+cp_ToStr(this.w_ARCODGRU);

            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARCODSOT) and !this.bDontReportError
            deferred_cp_zoom('SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(oSource.parent,'oARCODSOT_2_17'),i_cWhere,'GSOF_ASF',"Codici sottogruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ARCODGRU<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SGCODGRU="+cp_ToStrODBC(this.w_ARCODGRU);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',oSource.xKey(1);
                       ,'SGCODSOT',oSource.xKey(2))
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODSOT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SGCODGRU,SGCODSOT,SGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SGCODSOT="+cp_ToStrODBC(this.w_ARCODSOT);
                   +" and SGCODGRU="+cp_ToStrODBC(this.w_ARCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SGCODGRU',this.w_ARCODGRU;
                       ,'SGCODSOT',this.w_ARCODSOT)
            select SGCODGRU,SGCODSOT,SGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODSOT = NVL(_Link_.SGCODSOT,space(5))
      this.w_DESSOT = NVL(_Link_.SGDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODSOT = space(5)
      endif
      this.w_DESSOT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])+'\'+cp_ToStr(_Link_.SGCODGRU,1)+'\'+cp_ToStr(_Link_.SGCODSOT,1)
      cp_ShowWarn(i_cKey,this.SOT_SEZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODSOT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.SOT_SEZI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.SOT_SEZI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.SGCODSOT as SGCODSOT217"+ ",link_2_17.SGDESCRI as SGDESCRI217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on ART_ICOL.ARCODSOT=link_2_17.SGCODSOT"+" and ART_ICOL.ARCODGRU=link_2_17.SGCODGRU"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCODSOT=link_2_17.SGCODSOT(+)"'+'+" and ART_ICOL.ARCODGRU=link_2_17.SGCODGRU(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCATCES
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_CESP_IDX,3]
    i_lTable = "CAT_CESP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2], .t., this.CAT_CESP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCATCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCE_ACT',True,'CAT_CESP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_ARCATCES)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_ARCATCES))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCATCES)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCATCES) and !this.bDontReportError
            deferred_cp_zoom('CAT_CESP','*','CCCODICE',cp_AbsName(oSource.parent,'oARCATCES_2_19'),i_cWhere,'GSCE_ACT',"Categorie cespiti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCATCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_ARCATCES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_ARCATCES)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCATCES = NVL(_Link_.CCCODICE,space(15))
      this.w_DESCACES = NVL(_Link_.CCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ARCATCES = space(15)
      endif
      this.w_DESCACES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_CESP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCATCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_CESP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_CESP_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_19.CCCODICE as CCCODICE219"+ ",link_2_19.CCDESCRI as CCDESCRI219"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_19 on ART_ICOL.ARCATCES=link_2_19.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_19"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCATCES=link_2_19.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ARCONTRI
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCONTR_IDX,3]
    i_lTable = "TIPCONTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2], .t., this.TIPCONTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCONTRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATP',True,'TIPCONTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TPCODICE like "+cp_ToStrODBC(trim(this.w_ARCONTRI)+"%");

          i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TPCODICE',trim(this.w_ARCONTRI))
          select TPCODICE,TPDESCRI,TPAPPPES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCONTRI)==trim(_Link_.TPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TPDESCRI like "+cp_ToStrODBC(trim(this.w_ARCONTRI)+"%");

            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TPDESCRI like "+cp_ToStr(trim(this.w_ARCONTRI)+"%");

            select TPCODICE,TPDESCRI,TPAPPPES;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARCONTRI) and !this.bDontReportError
            deferred_cp_zoom('TIPCONTR','*','TPCODICE',cp_AbsName(oSource.parent,'oARCONTRI_2_20'),i_cWhere,'GSAR_ATP',"Tipi contributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES";
                     +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',oSource.xKey(1))
            select TPCODICE,TPDESCRI,TPAPPPES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCONTRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES";
                   +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(this.w_ARCONTRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',this.w_ARCONTRI)
            select TPCODICE,TPDESCRI,TPAPPPES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCONTRI = NVL(_Link_.TPCODICE,space(15))
      this.w_DESTIPCO = NVL(_Link_.TPDESCRI,space(40))
      this.w_TPAPPPES = NVL(_Link_.TPAPPPES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARCONTRI = space(15)
      endif
      this.w_DESTIPCO = space(40)
      this.w_TPAPPPES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])+'\'+cp_ToStr(_Link_.TPCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCONTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCONTRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIPCONTR_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_20.TPCODICE as TPCODICE220"+ ",link_2_20.TPDESCRI as TPDESCRI220"+ ",link_2_20.TPAPPPES as TPAPPPES220"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_20 on ART_ICOL.ARCONTRI=link_2_20.TPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_20"
          i_cKey=i_cKey+'+" and ART_ICOL.ARCONTRI=link_2_20.TPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IVAREP
  func Link_2_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVAREP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVAREP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_IVAREP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_IVAREP)
            select IVCODIVA,IVPERIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVAREP = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIVR = NVL(_Link_.IVPERIVA,0)
    else
      if i_cCtrl<>'Load'
        this.w_IVAREP = space(5)
      endif
      this.w_PERIVR = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVAREP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oARCODART_1_6.value==this.w_ARCODART)
      this.oPgFrm.Page1.oPag.oARCODART_1_6.value=this.w_ARCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESART_1_7.value==this.w_ARDESART)
      this.oPgFrm.Page1.oPag.oARDESART_1_7.value=this.w_ARDESART
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPART_1_11.RadioValue()==this.w_ARTIPART)
      this.oPgFrm.Page1.oPag.oARTIPART_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLSERG_1_12.RadioValue()==this.w_ARFLSERG)
      this.oPgFrm.Page1.oPag.oARFLSERG_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPSER_1_13.RadioValue()==this.w_ARTIPSER)
      this.oPgFrm.Page1.oPag.oARTIPSER_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARASSIVA_1_14.RadioValue()==this.w_ARASSIVA)
      this.oPgFrm.Page1.oPag.oARASSIVA_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARPRESTA_1_15.RadioValue()==this.w_ARPRESTA)
      this.oPgFrm.Page1.oPag.oARPRESTA_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESSUP_1_16.value==this.w_ARDESSUP)
      this.oPgFrm.Page1.oPag.oARDESSUP_1_16.value=this.w_ARDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS1_1_17.value==this.w_ARUNMIS1)
      this.oPgFrm.Page1.oPag.oARUNMIS1_1_17.value=this.w_ARUNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS2_1_18.value==this.w_ARUNMIS2)
      this.oPgFrm.Page1.oPag.oARUNMIS2_1_18.value=this.w_ARUNMIS2
    endif
    if not(this.oPgFrm.Page1.oPag.oAROPERAT_1_19.RadioValue()==this.w_AROPERAT)
      this.oPgFrm.Page1.oPag.oAROPERAT_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARMOLTIP_1_20.value==this.w_ARMOLTIP)
      this.oPgFrm.Page1.oPag.oARMOLTIP_1_20.value=this.w_ARMOLTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLUSEP_1_21.RadioValue()==this.w_ARFLUSEP)
      this.oPgFrm.Page1.oPag.oARFLUSEP_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLUNIV_1_22.RadioValue()==this.w_ARFLUNIV)
      this.oPgFrm.Page1.oPag.oARFLUNIV_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARNUMPRE_1_24.value==this.w_ARNUMPRE)
      this.oPgFrm.Page1.oPag.oARNUMPRE_1_24.value=this.w_ARNUMPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPOPE_1_25.value==this.w_ARTIPOPE)
      this.oPgFrm.Page1.oPag.oARTIPOPE_1_25.value=this.w_ARTIPOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oARCODIVA_1_26.value==this.w_ARCODIVA)
      this.oPgFrm.Page1.oPag.oARCODIVA_1_26.value=this.w_ARCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_27.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_27.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oARCATCON_1_28.value==this.w_ARCATCON)
      this.oPgFrm.Page1.oPag.oARCATCON_1_28.value=this.w_ARCATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oARPREZUM_1_29.RadioValue()==this.w_ARPREZUM)
      this.oPgFrm.Page1.oPag.oARPREZUM_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARGRUMER_1_30.value==this.w_ARGRUMER)
      this.oPgFrm.Page1.oPag.oARGRUMER_1_30.value=this.w_ARGRUMER
    endif
    if not(this.oPgFrm.Page1.oPag.oARSTASUP_1_31.RadioValue()==this.w_ARSTASUP)
      this.oPgFrm.Page1.oPag.oARSTASUP_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARSTACOD_1_32.RadioValue()==this.w_ARSTACOD)
      this.oPgFrm.Page1.oPag.oARSTACOD_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARSTASUP_1_33.RadioValue()==this.w_ARSTASUP)
      this.oPgFrm.Page1.oPag.oARSTASUP_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_34.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_34.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLSPAN_1_35.RadioValue()==this.w_ARFLSPAN)
      this.oPgFrm.Page1.oPag.oARFLSPAN_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARSCRITT_1_36.RadioValue()==this.w_ARSCRITT)
      this.oPgFrm.Page1.oPag.oARSCRITT_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARSTACOD_1_37.RadioValue()==this.w_ARSTACOD)
      this.oPgFrm.Page1.oPag.oARSTACOD_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLPRPE_1_38.RadioValue()==this.w_ARFLPRPE)
      this.oPgFrm.Page1.oPag.oARFLPRPE_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARFLESCT_1_39.RadioValue()==this.w_ARFLESCT)
      this.oPgFrm.Page1.oPag.oARFLESCT_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_40.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_40.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oARVOCCEN_1_41.value==this.w_ARVOCCEN)
      this.oPgFrm.Page1.oPag.oARVOCCEN_1_41.value=this.w_ARVOCCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oARVOCRIC_1_42.value==this.w_ARVOCRIC)
      this.oPgFrm.Page1.oPag.oARVOCRIC_1_42.value=this.w_ARVOCRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oARDTINVA_1_43.value==this.w_ARDTINVA)
      this.oPgFrm.Page1.oPag.oARDTINVA_1_43.value=this.w_ARDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oARDTOBSO_1_44.value==this.w_ARDTOBSO)
      this.oPgFrm.Page1.oPag.oARDTOBSO_1_44.value=this.w_ARDTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPRES_1_45.RadioValue()==this.w_ARTIPRES)
      this.oPgFrm.Page1.oPag.oARTIPRES_1_45.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPRIG_1_46.RadioValue()==this.w_ARTIPRIG)
      this.oPgFrm.Page1.oPag.oARTIPRIG_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARTIPRI2_1_47.RadioValue()==this.w_ARTIPRI2)
      this.oPgFrm.Page1.oPag.oARTIPRI2_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARCODFAS_1_48.value==this.w_ARCODFAS)
      this.oPgFrm.Page1.oPag.oARCODFAS_1_48.value=this.w_ARCODFAS
    endif
    if not(this.oPgFrm.Page1.oPag.oARUTISER_1_49.RadioValue()==this.w_ARUTISER)
      this.oPgFrm.Page1.oPag.oARUTISER_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARPERSER_1_50.RadioValue()==this.w_ARPERSER)
      this.oPgFrm.Page1.oPag.oARPERSER_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARNOMENC_1_51.value==this.w_ARNOMENC)
      this.oPgFrm.Page1.oPag.oARNOMENC_1_51.value=this.w_ARNOMENC
    endif
    if not(this.oPgFrm.Page1.oPag.oUMSUPP_1_52.value==this.w_UMSUPP)
      this.oPgFrm.Page1.oPag.oUMSUPP_1_52.value=this.w_UMSUPP
    endif
    if not(this.oPgFrm.Page1.oPag.oARMOLSUP_1_53.value==this.w_ARMOLSUP)
      this.oPgFrm.Page1.oPag.oARMOLSUP_1_53.value=this.w_ARMOLSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oARDATINT_1_54.RadioValue()==this.w_ARDATINT)
      this.oPgFrm.Page1.oPag.oARDATINT_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVOC_1_83.value==this.w_DESVOC)
      this.oPgFrm.Page1.oPag.oDESVOC_1_83.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIC_1_86.value==this.w_DESRIC)
      this.oPgFrm.Page1.oPag.oDESRIC_1_86.value=this.w_DESRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oTIDESCRI_1_94.value==this.w_TIDESCRI)
      this.oPgFrm.Page1.oPag.oTIDESCRI_1_94.value=this.w_TIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOM_1_116.value==this.w_DESNOM)
      this.oPgFrm.Page1.oPag.oDESNOM_1_116.value=this.w_DESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oARPARAME_1_140.RadioValue()==this.w_ARPARAME)
      this.oPgFrm.Page1.oPag.oARPARAME_1_140.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_1.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESC_2_2.value==this.w_DESC)
      this.oPgFrm.Page2.oPag.oDESC_2_2.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page2.oPag.oARGRUPRO_2_4.value==this.w_ARGRUPRO)
      this.oPgFrm.Page2.oPag.oARGRUPRO_2_4.value=this.w_ARGRUPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGPP_2_5.value==this.w_DESGPP)
      this.oPgFrm.Page2.oPag.oDESGPP_2_5.value=this.w_DESGPP
    endif
    if not(this.oPgFrm.Page2.oPag.oARCATSCM_2_6.value==this.w_ARCATSCM)
      this.oPgFrm.Page2.oPag.oARCATSCM_2_6.value=this.w_ARCATSCM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSCM_2_7.value==this.w_DESSCM)
      this.oPgFrm.Page2.oPag.oDESSCM_2_7.value=this.w_DESSCM
    endif
    if not(this.oPgFrm.Page2.oPag.oARCODCLA_2_10.value==this.w_ARCODCLA)
      this.oPgFrm.Page2.oPag.oARCODCLA_2_10.value=this.w_ARCODCLA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLA_2_11.value==this.w_DESCLA)
      this.oPgFrm.Page2.oPag.oDESCLA_2_11.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page2.oPag.oARCODREP_2_13.value==this.w_ARCODREP)
      this.oPgFrm.Page2.oPag.oARCODREP_2_13.value=this.w_ARCODREP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESREP_2_14.value==this.w_DESREP)
      this.oPgFrm.Page2.oPag.oDESREP_2_14.value=this.w_DESREP
    endif
    if not(this.oPgFrm.Page2.oPag.oARCODGRU_2_16.value==this.w_ARCODGRU)
      this.oPgFrm.Page2.oPag.oARCODGRU_2_16.value=this.w_ARCODGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oARCODSOT_2_17.value==this.w_ARCODSOT)
      this.oPgFrm.Page2.oPag.oARCODSOT_2_17.value=this.w_ARCODSOT
    endif
    if not(this.oPgFrm.Page2.oPag.oARFLCESP_2_18.RadioValue()==this.w_ARFLCESP)
      this.oPgFrm.Page2.oPag.oARFLCESP_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARCATCES_2_19.value==this.w_ARCATCES)
      this.oPgFrm.Page2.oPag.oARCATCES_2_19.value=this.w_ARCATCES
    endif
    if not(this.oPgFrm.Page2.oPag.oARCONTRI_2_20.value==this.w_ARCONTRI)
      this.oPgFrm.Page2.oPag.oARCONTRI_2_20.value=this.w_ARCONTRI
    endif
    if not(this.oPgFrm.Page2.oPag.oARMINVEN_2_21.value==this.w_ARMINVEN)
      this.oPgFrm.Page2.oPag.oARMINVEN_2_21.value=this.w_ARMINVEN
    endif
    if not(this.oPgFrm.Page2.oPag.oARRIPCON_2_24.RadioValue()==this.w_ARRIPCON)
      this.oPgFrm.Page2.oPag.oARRIPCON_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARARTPOS_2_25.RadioValue()==this.w_ARARTPOS)
      this.oPgFrm.Page2.oPag.oARARTPOS_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARPUBWEB_2_27.RadioValue()==this.w_ARPUBWEB)
      this.oPgFrm.Page2.oPag.oARPUBWEB_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARPUBWEB_2_28.RadioValue()==this.w_ARPUBWEB)
      this.oPgFrm.Page2.oPag.oARPUBWEB_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARPUBWEB_2_29.RadioValue()==this.w_ARPUBWEB)
      this.oPgFrm.Page2.oPag.oARPUBWEB_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSOT_2_30.value==this.w_DESSOT)
      this.oPgFrm.Page2.oPag.oDESSOT_2_30.value=this.w_DESSOT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESTIT_2_31.value==this.w_DESTIT)
      this.oPgFrm.Page2.oPag.oDESTIT_2_31.value=this.w_DESTIT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCACES_2_35.value==this.w_DESCACES)
      this.oPgFrm.Page2.oPag.oDESCACES_2_35.value=this.w_DESCACES
    endif
    if not(this.oPgFrm.Page2.oPag.oDESTIPCO_2_38.value==this.w_DESTIPCO)
      this.oPgFrm.Page2.oPag.oDESTIPCO_2_38.value=this.w_DESTIPCO
    endif
    if not(this.oPgFrm.Page4.oPag.oCODI_4_1.value==this.w_CODI)
      this.oPgFrm.Page4.oPag.oCODI_4_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page4.oPag.oDESC_4_2.value==this.w_DESC)
      this.oPgFrm.Page4.oPag.oDESC_4_2.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page5.oPag.oCODI_5_1.value==this.w_CODI)
      this.oPgFrm.Page5.oPag.oCODI_5_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page5.oPag.oDESC_5_2.value==this.w_DESC)
      this.oPgFrm.Page5.oPag.oDESC_5_2.value=this.w_DESC
    endif
    if not(this.oPgFrm.Page2.oPag.oARCODTIP_2_43.value==this.w_ARCODTIP)
      this.oPgFrm.Page2.oPag.oARCODTIP_2_43.value=this.w_ARCODTIP
    endif
    if not(this.oPgFrm.Page2.oPag.oARCODCLF_2_44.value==this.w_ARCODCLF)
      this.oPgFrm.Page2.oPag.oARCODCLF_2_44.value=this.w_ARCODCLF
    endif
    if not(this.oPgFrm.Page2.oPag.oARCODVAL_2_45.value==this.w_ARCODVAL)
      this.oPgFrm.Page2.oPag.oARCODVAL_2_45.value=this.w_ARCODVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oARFLGESC_2_51.RadioValue()==this.w_ARFLGESC)
      this.oPgFrm.Page2.oPag.oARFLGESC_2_51.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'ART_ICOL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ARCODART)) or not(IIF(.cFunction='Load'  AND .w_AUTO<>'S' ,chkcodar('A',.w_ARCODART),.T.)))  and (.w_AUTO<>'S' AND .cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCODART_1_6.SetFocus()
            i_bnoObbl = !empty(.w_ARCODART)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ARPRESTA)) or not(IIF(.w_UMFLTEMP#'S' AND NOT EMPTY(.w_ARUNMIS1), .w_ARPRESTA#'P', .T.)))  and not(!isAlt())  and (IsAlt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARPRESTA_1_15.SetFocus()
            i_bnoObbl = !empty(.w_ARPRESTA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classificazione non congruente con l'unit� di misura")
          case   ((empty(.w_ARUNMIS1)) or not(IIF(.w_ARPRESTA='P', .w_UMFLTEMP='S', .T.)))  and (.w_ARTIPART<>'DE' AND NOT (IsAlt() AND .w_ARTIPART='FO'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARUNMIS1_1_17.SetFocus()
            i_bnoObbl = !empty(.w_ARUNMIS1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un'unit� di misura gestita a tempo!")
          case   not(.w_ARUNMIS2<>.w_ARUNMIS1)  and (.w_ARTIPART<>'DE' AND NOT (IsAlt() AND .w_ARTIPART='FO'))  and not(empty(.w_ARUNMIS2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARUNMIS2_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ARMOLTIP))  and (NOT EMPTY(.w_ARUNMIS2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARMOLTIP_1_20.SetFocus()
            i_bnoObbl = !empty(.w_ARMOLTIP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ARNUMPRE>0)  and not(Not Isalt() or .w_ARFLUNIV='N')  and (.w_ARFLUNIV='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARNUMPRE_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero massimo di occorrenze deve essere maggiore di 0")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_ARTIPOPE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTIPOPE_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice obsoleto oppure inesistente")
          case   ((empty(.w_ARCODIVA)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_ARTIPART<>'DE')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCODIVA_1_26.SetFocus()
            i_bnoObbl = !empty(.w_ARCODIVA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
          case   (empty(.w_ARCATCON))  and (.w_ARTIPART<>'DE')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARCATCON_1_28.SetFocus()
            i_bnoObbl = !empty(.w_ARCATCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ARFLSPAN='N' OR !(.w_ARCODART=.w_PAPREACC_ART OR .w_ARCODART=.w_PAPREFOR_ART))  and not(Not Isalt() OR .w_ARPRESTA $ 'S-A')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARFLSPAN_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile attivare l'opzione, tariffa presente nei parametri generali")
          case   not(.w_TIPVOC<>'R')  and ((g_PERCCR='S' OR g_COMM='S') AND .w_ARTIPART<>'DE')  and not(empty(.w_ARVOCCEN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARVOCCEN_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di costo incongruente")
          case   not(.w_TIPRIC<>'C')  and ((g_PERCCR='S' OR g_COMM='S') AND .w_ARTIPART<>'DE')  and not(empty(.w_ARVOCRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARVOCRIC_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di ricavo incongruente")
          case   (empty(.w_ARTIPRES))  and not(g_AGEN<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTIPRES_1_45.SetFocus()
            i_bnoObbl = !empty(.w_ARTIPRES)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ARUTISER='N' OR LEN(ALLTRIM(.w_ARNOMENC))=6 OR LEN(ALLTRIM(.w_ARNOMENC))=5)  and (g_INTR='S' and (.w_ARDATINT<>'N' OR .w_ARUTISER<>'N'))  and not(empty(.w_ARNOMENC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARNOMENC_1_51.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o incongruente")
          case   not(.w_FLPRO $ ' A')  and not(isAlt())  and (.w_ARTIPART<>'DE')  and not(empty(.w_ARGRUPRO))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARGRUPRO_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria provvigioni inesistente o non di tipo articolo")
          case   not(.w_FLSCM $ ' A')  and not(isAlt())  and (.w_ARTIPART<>'DE')  and not(empty(.w_ARCATSCM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARCATSCM_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria sconti / maggiorazioni inesistente o non di tipo articolo")
          case   not(g_REVICRM='S' Or (.w_OLDPUBWEB='N' or (.w_OLDPUBWEB $ 'S-T' and .w_ARPUBWEB $ 'T-S')))  and not(g_CPIN<>'S' And g_REVICRM<>'S')  and (g_CPIN='S' Or g_REVICRM='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARPUBWEB_2_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Cambio modalit� Web non consentita!")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSMA_MLI.CheckForm()
      if i_bres
        i_bres=  .GSMA_MLI.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MR2.CheckForm()
      if i_bres
        i_bres=  .GSAR_MR2.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .gsma_mtt.CheckForm()
      if i_bres
        i_bres=  .gsma_mtt.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .GSMA_ANO.CheckForm()
      if i_bres
        i_bres=  .GSMA_ANO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=5
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsma_aas
      if i_bRes and empty(.w_ARCODART)
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = Ah_MsgFormat("Inserire codice servizio")
      endif
      if i_bRes and IsAlt()
        if .w_PERIVA<>0 and .w_ARPRESTA='A'
          if not Ah_YesNo("La classificazione � uguale anticipazione e aliquota del codice IVA � diversa da 0. %0Si vuole confermare?")
            i_bRes = .f.
          endif
        endif
        if .w_PERIVA=0 and .w_ARPRESTA<>'A'
          if not Ah_YesNo("La classificazione � diversa da anticipazione e aliquota del codice IVA � 0. %0Si vuole confermare?")
            i_bRes = .f.
          endif
        endif
      endif
       if i_bRes and .w_ARTIPART<>'DE' and g_GPOS='S' AND .w_ARARTPOS='S'
        if empty(.w_ARCODREP)
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = Ah_MsgFormat("Codice reparto inesistente")
        else
          if .w_PERIVA<>.w_PERIVR
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = Ah_MsgFormat("Aliquota IVA reparto incongruente con aliquota IVA articolo")
          endif
        endif
      endif
      if Isalt()
          if i_bRes and This.cFunction='Load' and Type('This.gsma_mtt.w_TACODLIS')<>'C' and !Ah_yesno("Non sono stati definiti gli importi. Confermi ugualmente ?")
            i_bRes = .f.
          endif
          if  i_bRes and Empty(This.w_ARCODFAS) and This.w_ARPARAME $'4S'
            if not Ah_YesNo("Non hai inserito una fase legata ad un parametro, quindi non potrai usufruire della stampa nota spese raggruppata per fase. Vuoi continuare ?")
              i_bRes = .f.
            endif
          endif
      endif
      if i_bRes
        i_bRes =CHKMEMO(.w_ARDESSUP)
      endif
       if i_bRes and  This.cFunction='Load' and Isalt() and Type('This.gsma_mtt.w_TACODLIS')<>'C' and !Ah_yesno("Non sono stati definiti gli importi. Confermi ugualmente ?")
         i_bRes = .f.
        endif
      if i_bRes and g_GPFA="S" and empty(nvl(this.w_GESTGUID,"")) and this.GSAR_MR2.numrow() > 0
          this.w_GESTGUID = SUBSTR(DTOC(DATE(),1),4)+SUBSTR(SYS(2015),2)
      endif
      if i_bRes and g_GPFA="S" and this.GSAR_MR2.numrow() > 0
          L_MESSAGE = CHK_ELE_ATTR( "D", this.GSAR_MR2 )
          if not empty (L_MESSAGE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = L_MESSAGE
          endif
      ENDIF
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AUTO = this.w_AUTO
    this.o_PARALTE = this.w_PARALTE
    this.o_ARCODART = this.w_ARCODART
    this.o_ARTIPART = this.w_ARTIPART
    this.o_ARFLSERG = this.w_ARFLSERG
    this.o_ARTIPSER = this.w_ARTIPSER
    this.o_ARPRESTA = this.w_ARPRESTA
    this.o_ARUNMIS2 = this.w_ARUNMIS2
    this.o_ARFLUNIV = this.w_ARFLUNIV
    this.o_ARSTASUP = this.w_ARSTASUP
    this.o_ARFLSPAN = this.w_ARFLSPAN
    this.o_ARSCRITT = this.w_ARSCRITT
    this.o_ARFLPRPE = this.w_ARFLPRPE
    this.o_ARTIPRIG = this.w_ARTIPRIG
    this.o_ARTIPRI2 = this.w_ARTIPRI2
    this.o_ARUTISER = this.w_ARUTISER
    this.o_ARUMSUPP = this.w_ARUMSUPP
    this.o_ARCODREP = this.w_ARCODREP
    this.o_ARFLCESP = this.w_ARFLCESP
    this.o_ARCONTRI = this.w_ARCONTRI
    this.o_TPAPPPES = this.w_TPAPPPES
    this.o_ARARTPOS = this.w_ARARTPOS
    this.o_GEST = this.w_GEST
    * --- GSMA_MLI : Depends On
    this.GSMA_MLI.SaveDependsOn()
    * --- GSAR_MR2 : Depends On
    this.GSAR_MR2.SaveDependsOn()
    * --- gsma_mtt : Depends On
    this.gsma_mtt.SaveDependsOn()
    * --- GSMA_ANO : Depends On
    this.GSMA_ANO.SaveDependsOn()
    return

  func CanDelete()
    local i_res
    i_res=GSMA_BAE(this,'G')
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione annullata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsma_aasPag1 as StdContainer
  Width  = 694
  height = 570
  stdWidth  = 694
  stdheight = 570
  resizeXpos=460
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oARCODART_1_6 as StdField with uid="KCSNYIQQDI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ARCODART", cQueryName = "ARCODART",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 49738918,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=75, Top=10, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20)

  func oARCODART_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AUTO<>'S' AND .cFunction='Load')
    endwith
   endif
  endfunc

  func oARCODART_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(.cFunction='Load'  AND .w_AUTO<>'S' ,chkcodar('A',.w_ARCODART),.T.))
    endwith
    return bRes
  endfunc

  add object oARDESART_1_7 as StdField with uid="DJJYVBVRVC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ARDESART", cQueryName = "ARDESART",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 34661542,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=259, Top=10, InputMask=replicate('X',40)

  func oARDESART_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_NOEDES='N' OR !Isalt() or cp_IsAdministrator() or .cFunction='Load'))
    endwith
   endif
  endfunc


  add object oARTIPART_1_11 as StdCombo with uid="XIBNPHRQFC",rtseq=11,rtrep=.f.,left=106,top=36,width=146,height=21;
    , ToolTipText = "Tipo (a quantit� e valore, a valore, descrittivo)";
    , HelpContextID = 37479590;
    , cFormVar="w_ARTIPART",RowSource=""+"A quantit� e valore,"+"A valore,"+"Descrittivo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARTIPART_1_11.RadioValue()
    return(iif(this.value =1,'FM',;
    iif(this.value =2,'FO',;
    iif(this.value =3,'DE',;
    space(2)))))
  endfunc
  func oARTIPART_1_11.GetRadio()
    this.Parent.oContained.w_ARTIPART = this.RadioValue()
    return .t.
  endfunc

  func oARTIPART_1_11.SetRadio()
    this.Parent.oContained.w_ARTIPART=trim(this.Parent.oContained.w_ARTIPART)
    this.value = ;
      iif(this.Parent.oContained.w_ARTIPART=='FM',1,;
      iif(this.Parent.oContained.w_ARTIPART=='FO',2,;
      iif(this.Parent.oContained.w_ARTIPART=='DE',3,;
      0)))
  endfunc

  func oARTIPART_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER(.w_TIPOPE)='LOAD')
    endwith
   endif
  endfunc

  add object oARFLSERG_1_12 as StdCheck with uid="IPQHAZONWN",rtseq=12,rtrep=.f.,left=73, top=62, caption="Servizio generico",;
    ToolTipText = "Flag servizio generico: se attivo il servizio � movimentabile con una qualsiasi unit� di misura",;
    HelpContextID = 32914253,;
    cFormVar="w_ARFLSERG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARFLSERG_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARFLSERG_1_12.GetRadio()
    this.Parent.oContained.w_ARFLSERG = this.RadioValue()
    return .t.
  endfunc

  func oARFLSERG_1_12.SetRadio()
    this.Parent.oContained.w_ARFLSERG=trim(this.Parent.oContained.w_ARFLSERG)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLSERG=='S',1,;
      0)
  endfunc

  func oARFLSERG_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPART='FM')
    endwith
   endif
  endfunc

  func oARFLSERG_1_12.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART<>'FM')
    endwith
  endfunc

  add object oARTIPSER_1_13 as StdCheck with uid="XRMVGKNJSQ",rtseq=13,rtrep=.f.,left=73, top=86, caption="Servizio accessorio",;
    ToolTipText = "Flag servizio accessorio: se attivo non ripartisce le spese accessorie e non considera gli sconti di piede documento",;
    HelpContextID = 264510296,;
    cFormVar="w_ARTIPSER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARTIPSER_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARTIPSER_1_13.GetRadio()
    this.Parent.oContained.w_ARTIPSER = this.RadioValue()
    return .t.
  endfunc

  func oARTIPSER_1_13.SetRadio()
    this.Parent.oContained.w_ARTIPSER=trim(this.Parent.oContained.w_ARTIPSER)
    this.value = ;
      iif(this.Parent.oContained.w_ARTIPSER=='S',1,;
      0)
  endfunc

  func oARTIPSER_1_13.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART='DE')
    endwith
  endfunc

  add object oARASSIVA_1_14 as StdCheck with uid="JHZUIBADBA",rtseq=14,rtrep=.f.,left=73, top=114, caption="No IVA per assestamento",;
    ToolTipText = "Se attivo il servizio non partecipa alla determinazione IVA per fatture da emettere",;
    HelpContextID = 100461383,;
    cFormVar="w_ARASSIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARASSIVA_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oARASSIVA_1_14.GetRadio()
    this.Parent.oContained.w_ARASSIVA = this.RadioValue()
    return .t.
  endfunc

  func oARASSIVA_1_14.SetRadio()
    this.Parent.oContained.w_ARASSIVA=trim(this.Parent.oContained.w_ARASSIVA)
    this.value = ;
      iif(this.Parent.oContained.w_ARASSIVA=='S',1,;
      0)
  endfunc


  add object oARPRESTA_1_15 as StdCombo with uid="RXCPHJQEYB",rtseq=15,rtrep=.f.,left=135,top=143,width=146,height=21;
    , ToolTipText = "Classificazione della prestazione (onorario civile, onorario penale, tariffa a tempo ecc)";
    , HelpContextID = 253549383;
    , cFormVar="w_ARPRESTA",RowSource=""+"Onorario civile,"+"Onorario penale,"+"Onorario stragiudiziale,"+"Diritto stragiudiziale,"+"Diritto civile,"+"Prestazione generica,"+"Prestazione a tempo,"+"Spesa,"+"Anticipazione", bObbl = .t. , nPag = 1;
    , sErrorMsg = "Classificazione non congruente con l'unit� di misura";
  , bGlobalFont=.t.


  func oARPRESTA_1_15.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    iif(this.value =3,'T',;
    iif(this.value =4,'R',;
    iif(this.value =5,'C',;
    iif(this.value =6,'G',;
    iif(this.value =7,'P',;
    iif(this.value =8,'S',;
    iif(this.value =9,'A',;
    space(1)))))))))))
  endfunc
  func oARPRESTA_1_15.GetRadio()
    this.Parent.oContained.w_ARPRESTA = this.RadioValue()
    return .t.
  endfunc

  func oARPRESTA_1_15.SetRadio()
    this.Parent.oContained.w_ARPRESTA=trim(this.Parent.oContained.w_ARPRESTA)
    this.value = ;
      iif(this.Parent.oContained.w_ARPRESTA=='I',1,;
      iif(this.Parent.oContained.w_ARPRESTA=='E',2,;
      iif(this.Parent.oContained.w_ARPRESTA=='T',3,;
      iif(this.Parent.oContained.w_ARPRESTA=='R',4,;
      iif(this.Parent.oContained.w_ARPRESTA=='C',5,;
      iif(this.Parent.oContained.w_ARPRESTA=='G',6,;
      iif(this.Parent.oContained.w_ARPRESTA=='P',7,;
      iif(this.Parent.oContained.w_ARPRESTA=='S',8,;
      iif(this.Parent.oContained.w_ARPRESTA=='A',9,;
      0)))))))))
  endfunc

  func oARPRESTA_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oARPRESTA_1_15.mHide()
    with this.Parent.oContained
      return (!isAlt())
    endwith
  endfunc

  func oARPRESTA_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(.w_UMFLTEMP#'S' AND NOT EMPTY(.w_ARUNMIS1), .w_ARPRESTA#'P', .T.))
    endwith
    return bRes
  endfunc

  add object oARDESSUP_1_16 as StdMemo with uid="FBPDZKFHMX",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ARDESSUP", cQueryName = "ARDESSUP",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione aggiuntiva",;
    HelpContextID = 267328342,;
   bGlobalFont=.t.,;
    Height=67, Width=328, Left=259, Top=35

  add object oARUNMIS1_1_17 as StdField with uid="TQCGLNMKWQ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ARUNMIS1", cQueryName = "ARUNMIS1",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un'unit� di misura gestita a tempo!",;
    ToolTipText = "Unit� di misura principale",;
    HelpContextID = 93924151,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=135, Top=167, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ARUNMIS1"

  func oARUNMIS1_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPART<>'DE' AND NOT (IsAlt() AND .w_ARTIPART='FO'))
    endwith
   endif
  endfunc

  func oARUNMIS1_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oARUNMIS1_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARUNMIS1_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oARUNMIS1_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'Tariffario.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oARUNMIS1_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ARUNMIS1
     i_obj.ecpSave()
  endproc

  add object oARUNMIS2_1_18 as StdField with uid="WKFDTJKPAA",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ARUNMIS2", cQueryName = "ARUNMIS2",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale seconda unit� di misura",;
    HelpContextID = 93924152,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=260, Top=167, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_ARUNMIS2"

  func oARUNMIS2_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPART<>'DE' AND NOT (IsAlt() AND .w_ARTIPART='FO'))
    endwith
   endif
  endfunc

  func oARUNMIS2_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oARUNMIS2_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARUNMIS2_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oARUNMIS2_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oARUNMIS2_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_ARUNMIS2
     i_obj.ecpSave()
  endproc


  add object oAROPERAT_1_19 as StdCombo with uid="CFCIRNGPVC",rtseq=19,rtrep=.f.,left=316,top=167,width=35,height=21;
    , ToolTipText = "Operatore (moltip. o divis.) tra la 1^U.M. e la 2^U.M.";
    , HelpContextID = 31798438;
    , cFormVar="w_AROPERAT",RowSource=""+"X,"+"/", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAROPERAT_1_19.RadioValue()
    return(iif(this.value =1,'*',;
    iif(this.value =2,'/',;
    space(1))))
  endfunc
  func oAROPERAT_1_19.GetRadio()
    this.Parent.oContained.w_AROPERAT = this.RadioValue()
    return .t.
  endfunc

  func oAROPERAT_1_19.SetRadio()
    this.Parent.oContained.w_AROPERAT=trim(this.Parent.oContained.w_AROPERAT)
    this.value = ;
      iif(this.Parent.oContained.w_AROPERAT=='*',1,;
      iif(this.Parent.oContained.w_AROPERAT=='/',2,;
      0))
  endfunc

  func oAROPERAT_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc

  add object oARMOLTIP_1_20 as StdField with uid="ZYCYQTJIVY",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ARMOLTIP", cQueryName = "ARMOLTIP",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente di rapporto tra la 1^U.M. e la 2^U.M.",;
    HelpContextID = 259413162,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=364, Top=167, cSayPict='"99999.9999"', cGetPict='"99999.9999"'

  func oARMOLTIP_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUNMIS2))
    endwith
   endif
  endfunc


  add object oARFLUSEP_1_21 as StdCombo with uid="WZECSIGKXE",value=3,rtseq=21,rtrep=.f.,left=462,top=167,width=136,height=21;
    , ToolTipText = "Per quantit� e prezzo: viene ricalcolato il prezzo al cambio della quantit�";
    , HelpContextID = 1456982;
    , cFormVar="w_ARFLUSEP",RowSource=""+"Per quantit�,"+"Per quantit� e prezzo,"+"No", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARFLUSEP_1_21.RadioValue()
    return(iif(this.value =1,'Q',;
    iif(this.value =2,'S',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oARFLUSEP_1_21.GetRadio()
    this.Parent.oContained.w_ARFLUSEP = this.RadioValue()
    return .t.
  endfunc

  func oARFLUSEP_1_21.SetRadio()
    this.Parent.oContained.w_ARFLUSEP=trim(this.Parent.oContained.w_ARFLUSEP)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLUSEP=='Q',1,;
      iif(this.Parent.oContained.w_ARFLUSEP=='S',2,;
      iif(this.Parent.oContained.w_ARFLUSEP=='',3,;
      0)))
  endfunc

  func oARFLUSEP_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
   endif
  endfunc

  func oARFLUSEP_1_21.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oARFLUNIV_1_22 as StdCheck with uid="VGFXLMTCAC",rtseq=22,rtrep=.f.,left=461, top=143, caption="Controllo univocit�",;
    ToolTipText = "Se attivo, abilita il controllo univocit� prestazione per pratica",;
    HelpContextID = 186006364,;
    cFormVar="w_ARFLUNIV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARFLUNIV_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oARFLUNIV_1_22.GetRadio()
    this.Parent.oContained.w_ARFLUNIV = this.RadioValue()
    return .t.
  endfunc

  func oARFLUNIV_1_22.SetRadio()
    this.Parent.oContained.w_ARFLUNIV=trim(this.Parent.oContained.w_ARFLUNIV)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLUNIV=='S',1,;
      0)
  endfunc

  func oARFLUNIV_1_22.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  add object oARNUMPRE_1_24 as StdField with uid="YRVAHGMIEG",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ARNUMPRE", cQueryName = "ARNUMPRE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero massimo di occorrenze deve essere maggiore di 0",;
    ToolTipText = "Numero massimo di occorrenze della prestazione per pratica",;
    HelpContextID = 211794763,;
   bGlobalFont=.t.,;
    Height=21, Width=46, Left=584, Top=167, cSayPict='"9999"', cGetPict='"9999"'

  func oARNUMPRE_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARFLUNIV='S')
    endwith
   endif
  endfunc

  func oARNUMPRE_1_24.mHide()
    with this.Parent.oContained
      return (Not Isalt() or .w_ARFLUNIV='N')
    endwith
  endfunc

  func oARNUMPRE_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ARNUMPRE>0)
    endwith
    return bRes
  endfunc

  add object oARTIPOPE_1_25 as StdField with uid="UUUXVOUCFN",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ARTIPOPE", cQueryName = "ARTIPOPE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice obsoleto oppure inesistente",;
    ToolTipText = "Tipo operazione IVA",;
    HelpContextID = 71034037,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=135, Top=194, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPCODIV", cZoomOnZoom="GSAR_ATO", oKey_1_1="TI__TIPO", oKey_1_2="this.w_ARCATOPE", oKey_2_1="TICODICE", oKey_2_2="this.w_ARTIPOPE"

  func oARTIPOPE_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTIPOPE_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTIPOPE_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIPCODIV_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_ARCATOPE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStr(this.Parent.oContained.w_ARCATOPE)
    endif
    do cp_zoom with 'TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(this.parent,'oARTIPOPE_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATO',"Operazioni IVA",'',this.parent.oContained
  endproc
  proc oARTIPOPE_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TI__TIPO=w_ARCATOPE
     i_obj.w_TICODICE=this.parent.oContained.w_ARTIPOPE
     i_obj.ecpSave()
  endproc

  add object oARCODIVA_1_26 as StdField with uid="IBBJFDSVLE",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ARCODIVA", cQueryName = "ARCODIVA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto oppure inesistente",;
    ToolTipText = "Codice IVA",;
    HelpContextID = 84478791,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=135, Top=221, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_ARCODIVA"

  func oARCODIVA_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPART<>'DE')
    endwith
   endif
  endfunc

  func oARCODIVA_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODIVA_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODIVA_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oARCODIVA_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oARCODIVA_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_ARCODIVA
     i_obj.ecpSave()
  endproc

  add object oDESIVA_1_27 as StdField with uid="KYLGRMJOQR",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 31195594,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=203, Top=221, InputMask=replicate('X',35)

  add object oARCATCON_1_28 as StdField with uid="QSRLMVAGVQ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ARCATCON", cQueryName = "ARCATCON",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria contabile",;
    HelpContextID = 324780,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=135, Top=249, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOARTI", cZoomOnZoom="GSAR_AC1", oKey_1_1="C1CODICE", oKey_1_2="this.w_ARCATCON"

  func oARCATCON_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPART<>'DE')
    endwith
   endif
  endfunc

  func oARCATCON_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCATCON_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCATCON_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOARTI','*','C1CODICE',cp_AbsName(this.parent,'oARCATCON_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC1',"Categorie contabili articoli",'',this.parent.oContained
  endproc
  proc oARCATCON_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_ARCATCON
     i_obj.ecpSave()
  endproc


  add object oARPREZUM_1_29 as StdCombo with uid="SWVMEMHUHQ",rtseq=28,rtrep=.f.,left=462,top=206,width=136,height=21;
    , ToolTipText = "Applica prezzo valido per UM";
    , HelpContextID = 102554451;
    , cFormVar="w_ARPREZUM",RowSource=""+"Principale,"+"Filtra,"+"Converti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARPREZUM_1_29.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oARPREZUM_1_29.GetRadio()
    this.Parent.oContained.w_ARPREZUM = this.RadioValue()
    return .t.
  endfunc

  func oARPREZUM_1_29.SetRadio()
    this.Parent.oContained.w_ARPREZUM=trim(this.Parent.oContained.w_ARPREZUM)
    this.value = ;
      iif(this.Parent.oContained.w_ARPREZUM=='N',1,;
      iif(this.Parent.oContained.w_ARPREZUM=='S',2,;
      iif(this.Parent.oContained.w_ARPREZUM=='C',3,;
      0)))
  endfunc

  func oARPREZUM_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARFLSERG<>'S' AND NOT IsAlt())
    endwith
   endif
  endfunc

  add object oARGRUMER_1_30 as StdField with uid="HSSIETFAPR",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ARGRUMER", cQueryName = "ARGRUMER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico di appartenenza",;
    HelpContextID = 169626456,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=135, Top=277, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_ARGRUMER"

  func oARGRUMER_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPART<>'DE')
    endwith
   endif
  endfunc

  func oARGRUMER_1_30.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oARGRUMER_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oARGRUMER_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARGRUMER_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oARGRUMER_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oARGRUMER_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_ARGRUMER
     i_obj.ecpSave()
  endproc

  add object oARSTASUP_1_31 as StdCheck with uid="ZQWLSOBSON",rtseq=30,rtrep=.f.,left=461, top=229, caption="Stampa descrizione suppl.",;
    ToolTipText = "Se attivo stampa descrizione supplementare nei documenti abilitati",;
    HelpContextID = 249498454,;
    cFormVar="w_ARSTASUP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARSTASUP_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARSTASUP_1_31.GetRadio()
    this.Parent.oContained.w_ARSTASUP = this.RadioValue()
    return .t.
  endfunc

  func oARSTASUP_1_31.SetRadio()
    this.Parent.oContained.w_ARSTASUP=trim(this.Parent.oContained.w_ARSTASUP)
    this.value = ;
      iif(this.Parent.oContained.w_ARSTASUP=='S',1,;
      0)
  endfunc

  func oARSTASUP_1_31.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oARSTACOD_1_32 as StdCheck with uid="IALCIUSQZA",rtseq=31,rtrep=.f.,left=461, top=247, caption="Non stampa il codice",;
    ToolTipText = "Se attivo non stampa il codice nei documenti",;
    HelpContextID = 18937014,;
    cFormVar="w_ARSTACOD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARSTACOD_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARSTACOD_1_32.GetRadio()
    this.Parent.oContained.w_ARSTACOD = this.RadioValue()
    return .t.
  endfunc

  func oARSTACOD_1_32.SetRadio()
    this.Parent.oContained.w_ARSTACOD=trim(this.Parent.oContained.w_ARSTACOD)
    this.value = ;
      iif(this.Parent.oContained.w_ARSTACOD=='S',1,;
      0)
  endfunc

  func oARSTACOD_1_32.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oARSTASUP_1_33 as StdCheck with uid="BMTKFHICFZ",rtseq=32,rtrep=.f.,left=461, top=229, caption="Onorario arbitrale",;
    ToolTipText = "Se attivo onorario arbitrale (solo per onorario stragiudiziale)",;
    HelpContextID = 249498454,;
    cFormVar="w_ARSTASUP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARSTASUP_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARSTASUP_1_33.GetRadio()
    this.Parent.oContained.w_ARSTASUP = this.RadioValue()
    return .t.
  endfunc

  func oARSTASUP_1_33.SetRadio()
    this.Parent.oContained.w_ARSTASUP=trim(this.Parent.oContained.w_ARSTASUP)
    this.value = ;
      iif(this.Parent.oContained.w_ARSTASUP=='S',1,;
      0)
  endfunc

  func oARSTASUP_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARPRESTA='T')
    endwith
   endif
  endfunc

  func oARSTASUP_1_33.mHide()
    with this.Parent.oContained
      return (Not IsAlt() or .w_ARPRESTA<>'T')
    endwith
  endfunc

  add object oDESCAT_1_34 as StdField with uid="ZZVTVYWWNO",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 3277258,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=203, Top=249, InputMask=replicate('X',35)

  add object oARFLSPAN_1_35 as StdCheck with uid="UEIEMVXSKA",rtseq=34,rtrep=.f.,left=136, top=282, caption="Spesa e anticipazione collegate",;
    ToolTipText = "Se attivo, l'utilizzo di questa tariffa permette l'inserimento contestuale di una spesa e/o un'anticipazione",;
    HelpContextID = 50971820,;
    cFormVar="w_ARFLSPAN", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Impossibile attivare l'opzione, tariffa presente nei parametri generali";
   , bGlobalFont=.t.


  func oARFLSPAN_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oARFLSPAN_1_35.GetRadio()
    this.Parent.oContained.w_ARFLSPAN = this.RadioValue()
    return .t.
  endfunc

  func oARFLSPAN_1_35.SetRadio()
    this.Parent.oContained.w_ARFLSPAN=trim(this.Parent.oContained.w_ARFLSPAN)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLSPAN=='S',1,;
      0)
  endfunc

  func oARFLSPAN_1_35.mHide()
    with this.Parent.oContained
      return (Not Isalt() OR .w_ARPRESTA $ 'S-A')
    endwith
  endfunc

  func oARFLSPAN_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ARFLSPAN='N' OR !(.w_ARCODART=.w_PAPREACC_ART OR .w_ARCODART=.w_PAPREFOR_ART))
    endwith
    return bRes
  endfunc

  add object oARSCRITT_1_36 as StdCheck with uid="ENLHLJQLVT",rtseq=35,rtrep=.f.,left=136, top=299, caption="Gestione dell'importo di rimborso spese di scritturazione",;
    ToolTipText = "Se attivo, l'utilizzo di questa tariffa permette la gestione dell'importo di rimborso spese di scritturazione",;
    HelpContextID = 98437978,;
    cFormVar="w_ARSCRITT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARSCRITT_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oARSCRITT_1_36.GetRadio()
    this.Parent.oContained.w_ARSCRITT = this.RadioValue()
    return .t.
  endfunc

  func oARSCRITT_1_36.SetRadio()
    this.Parent.oContained.w_ARSCRITT=trim(this.Parent.oContained.w_ARSCRITT)
    this.value = ;
      iif(this.Parent.oContained.w_ARSCRITT=='S',1,;
      0)
  endfunc

  func oARSCRITT_1_36.mHide()
    with this.Parent.oContained
      return (Not Isalt() OR .w_ARPRESTA $ 'S-A' OR .w_ARFLSPAN='N')
    endwith
  endfunc

  add object oARSTACOD_1_37 as StdCheck with uid="NCLVGNAOXU",rtseq=36,rtrep=.f.,left=461, top=247, caption="Notificazione",;
    ToolTipText = "Se attivo notificazione",;
    HelpContextID = 18937014,;
    cFormVar="w_ARSTACOD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARSTACOD_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARSTACOD_1_37.GetRadio()
    this.Parent.oContained.w_ARSTACOD = this.RadioValue()
    return .t.
  endfunc

  func oARSTACOD_1_37.SetRadio()
    this.Parent.oContained.w_ARSTACOD=trim(this.Parent.oContained.w_ARSTACOD)
    this.value = ;
      iif(this.Parent.oContained.w_ARSTACOD=='S',1,;
      0)
  endfunc

  func oARSTACOD_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARPRESTA='C')
    endwith
   endif
  endfunc

  func oARSTACOD_1_37.mHide()
    with this.Parent.oContained
      return (Not IsAlt() or .w_ARPRESTA<>'C')
    endwith
  endfunc


  add object oARFLPRPE_1_38 as StdCombo with uid="IZICFTMOBU",value=1,rtseq=37,rtrep=.f.,left=462,top=283,width=168,height=21;
    , ToolTipText = "Da utilizzare nel caso di calcoli particolari, ad esempio voce D4 (sul valore) oppure D2f, D2f1, D6 (sul maggior valore)";
    , HelpContextID = 20563125;
    , cFormVar="w_ARFLPRPE",RowSource=""+"Nessuno,"+"In % sul valore della pratica,"+"In % sul maggior valore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARFLPRPE_1_38.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'S',;
    iif(this.value =3,'V',;
    space(1)))))
  endfunc
  func oARFLPRPE_1_38.GetRadio()
    this.Parent.oContained.w_ARFLPRPE = this.RadioValue()
    return .t.
  endfunc

  func oARFLPRPE_1_38.SetRadio()
    this.Parent.oContained.w_ARFLPRPE=trim(this.Parent.oContained.w_ARFLPRPE)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLPRPE=='',1,;
      iif(this.Parent.oContained.w_ARFLPRPE=='S',2,;
      iif(this.Parent.oContained.w_ARFLPRPE=='V',3,;
      0)))
  endfunc

  func oARFLPRPE_1_38.mHide()
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
  endfunc

  add object oARFLESCT_1_39 as StdCheck with uid="BXQQTTXZKJ",rtseq=38,rtrep=.f.,left=575, top=318, caption="Escludi ricalcolo",;
    ToolTipText = "Se attivo, esclude voce del tariffario dal ricalcolo",;
    HelpContextID = 253115226,;
    cFormVar="w_ARFLESCT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARFLESCT_1_39.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oARFLESCT_1_39.GetRadio()
    this.Parent.oContained.w_ARFLESCT = this.RadioValue()
    return .t.
  endfunc

  func oARFLESCT_1_39.SetRadio()
    this.Parent.oContained.w_ARFLESCT=trim(this.Parent.oContained.w_ARFLESCT)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLESCT=='S',1,;
      0)
  endfunc

  func oARFLESCT_1_39.mHide()
    with this.Parent.oContained
      return (! Isalt() or  .w_ARPRESTA $ 'S-A')
    endwith
  endfunc

  add object oDESGRU_1_40 as StdField with uid="DRDYXOEFWO",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 236847562,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=203, Top=277, InputMask=replicate('X',30)

  func oDESGRU_1_40.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oARVOCCEN_1_41 as StdField with uid="GBXFEFTUHQ",rtseq=40,rtrep=.f.,;
    cFormVar = "w_ARVOCCEN", cQueryName = "ARVOCCEN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di costo incongruente",;
    ToolTipText = "Codice della voce di costo utilizzata per la contabilit� analitica dei documenti di acquisto",;
    HelpContextID = 251280212,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=135, Top=318, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_ARVOCCEN"

  func oARVOCCEN_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCCR='S' OR g_COMM='S') AND .w_ARTIPART<>'DE')
    endwith
   endif
  endfunc

  func oARVOCCEN_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oARVOCCEN_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARVOCCEN_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oARVOCCEN_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo",'GSMACAAR.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oARVOCCEN_1_41.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_ARVOCCEN
     i_obj.ecpSave()
  endproc

  add object oARVOCRIC_1_42 as StdField with uid="XMWZPBYNPN",rtseq=41,rtrep=.f.,;
    cFormVar = "w_ARVOCRIC", cQueryName = "ARVOCRIC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di ricavo incongruente",;
    ToolTipText = "Codice della voce di ricavo utilizzata per la contabilit� analitica dei documenti di vendita",;
    HelpContextID = 234502985,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=135, Top=346, cSayPict="p_MCE", cGetPict="p_MCE", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_ARVOCRIC"

  func oARVOCRIC_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCCR='S' OR g_COMM='S') AND .w_ARTIPART<>'DE')
    endwith
   endif
  endfunc

  func oARVOCRIC_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oARVOCRIC_1_42.ecpDrop(oSource)
    this.Parent.oContained.link_1_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARVOCRIC_1_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oARVOCRIC_1_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di ricavo",'GSMARAAR.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oARVOCRIC_1_42.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_ARVOCRIC
     i_obj.ecpSave()
  endproc

  add object oARDTINVA_1_43 as StdField with uid="OGSGXZXVBK",rtseq=42,rtrep=.f.,;
    cFormVar = "w_ARDTINVA", cQueryName = "ARDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 173939527,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=135, Top=372, tabstop=.f.

  func oARDTINVA_1_43.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oARDTOBSO_1_44 as StdField with uid="NBQZGWDFPN",rtseq=43,rtrep=.f.,;
    cFormVar = "w_ARDTOBSO", cQueryName = "ARDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data a partire dalla quale l'articolo � obsoleto",;
    HelpContextID = 247339861,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=366, Top=374, tabstop=.f.


  add object oARTIPRES_1_45 as StdTableCombo with uid="YTGUICDABF",rtseq=44,rtrep=.f.,left=575,top=374,width=117,height=21;
    , ToolTipText = "Tipo prestazione";
    , HelpContextID = 247733081;
    , cFormVar="w_ARTIPRES",tablefilter="", bObbl = .t. , nPag = 1;
    , cLinkFile="TIPI_PRE";
    , cTable='TIPI_PRE',cKey='TPCODICE',cValue='TPDESCRI',cOrderBy='TPDESCRI',xDefault=space(5);
  , bGlobalFont=.t.


  func oARTIPRES_1_45.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S')
    endwith
  endfunc

  func oARTIPRES_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTIPRES_1_45.ecpDrop(oSource)
    this.Parent.oContained.link_1_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oARTIPRIG_1_46 as StdCombo with uid="AZBHLUKGXU",rtseq=45,rtrep=.f.,left=135,top=401,width=132,height=21;
    , ToolTipText = "Tipologia riga per la generazione del documento";
    , HelpContextID = 247733069;
    , cFormVar="w_ARTIPRIG",RowSource=""+"Non fatturabile,"+"Fatturabile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARTIPRIG_1_46.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oARTIPRIG_1_46.GetRadio()
    this.Parent.oContained.w_ARTIPRIG = this.RadioValue()
    return .t.
  endfunc

  func oARTIPRIG_1_46.SetRadio()
    this.Parent.oContained.w_ARTIPRIG=trim(this.Parent.oContained.w_ARTIPRIG)
    this.value = ;
      iif(this.Parent.oContained.w_ARTIPRIG=='N',1,;
      iif(this.Parent.oContained.w_ARTIPRIG=='D',2,;
      0))
  endfunc

  func oARTIPRIG_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARFLSPAN<>'S')
    endwith
   endif
  endfunc

  func oARTIPRIG_1_46.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc


  add object oARTIPRI2_1_47 as StdCombo with uid="OMNXIGJIWQ",rtseq=46,rtrep=.f.,left=366,top=401,width=112,height=21;
    , ToolTipText = "Tipologia riga per la generazione della nota spese";
    , HelpContextID = 247733048;
    , cFormVar="w_ARTIPRI2",RowSource=""+"Senza nota spese,"+"Con nota spese", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARTIPRI2_1_47.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oARTIPRI2_1_47.GetRadio()
    this.Parent.oContained.w_ARTIPRI2 = this.RadioValue()
    return .t.
  endfunc

  func oARTIPRI2_1_47.SetRadio()
    this.Parent.oContained.w_ARTIPRI2=trim(this.Parent.oContained.w_ARTIPRI2)
    this.value = ;
      iif(this.Parent.oContained.w_ARTIPRI2=='N',1,;
      iif(this.Parent.oContained.w_ARTIPRI2=='D',2,;
      0))
  endfunc

  func oARTIPRI2_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARFLSPAN<>'S')
    endwith
   endif
  endfunc

  func oARTIPRI2_1_47.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  add object oARCODFAS_1_48 as StdField with uid="NLJBKNKROF",rtseq=47,rtrep=.f.,;
    cFormVar = "w_ARCODFAS", cQueryName = "ARCODFAS",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Inserire la fase per la liquidazione dei compensi a cui appartiene la voce di tariffa",;
    HelpContextID = 234288295,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=575, Top=401, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="FAS_TARI", cZoomOnZoom="GSMA_AFT", oKey_1_1="FTCODICE", oKey_1_2="this.w_ARCODFAS"

  func oARCODFAS_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_ARPRESTA$"SA")
    endwith
   endif
  endfunc

  func oARCODFAS_1_48.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  func oARCODFAS_1_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_48('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODFAS_1_48.ecpDrop(oSource)
    this.Parent.oContained.link_1_48('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODFAS_1_48.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAS_TARI','*','FTCODICE',cp_AbsName(this.parent,'oARCODFAS_1_48'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AFT',"Fasi per liquidazione compensi",'',this.parent.oContained
  endproc
  proc oARCODFAS_1_48.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AFT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FTCODICE=this.parent.oContained.w_ARCODFAS
     i_obj.ecpSave()
  endproc


  add object oARUTISER_1_49 as StdCombo with uid="EICJONXYKX",rtseq=48,rtrep=.f.,left=110,top=452,width=137,height=21;
    , ToolTipText = "Tipologia utilizzo dati intra";
    , HelpContextID = 257895256;
    , cFormVar="w_ARUTISER",RowSource=""+"Servizi,"+"Beni", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARUTISER_1_49.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oARUTISER_1_49.GetRadio()
    this.Parent.oContained.w_ARUTISER = this.RadioValue()
    return .t.
  endfunc

  func oARUTISER_1_49.SetRadio()
    this.Parent.oContained.w_ARUTISER=trim(this.Parent.oContained.w_ARUTISER)
    this.value = ;
      iif(this.Parent.oContained.w_ARUTISER=='S',1,;
      iif(this.Parent.oContained.w_ARUTISER=='N',2,;
      0))
  endfunc

  func oARUTISER_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_INTR='S')
    endwith
   endif
  endfunc


  add object oARPERSER_1_50 as StdCombo with uid="CUTAEYTLIG",rtseq=49,rtrep=.f.,left=509,top=452,width=180,height=21;
    , ToolTipText = "Modalit� di erogazione";
    , HelpContextID = 266328920;
    , cFormVar="w_ARPERSER",RowSource=""+"Istantanea,"+"A pi� riprese", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARPERSER_1_50.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oARPERSER_1_50.GetRadio()
    this.Parent.oContained.w_ARPERSER = this.RadioValue()
    return .t.
  endfunc

  func oARPERSER_1_50.SetRadio()
    this.Parent.oContained.w_ARPERSER=trim(this.Parent.oContained.w_ARPERSER)
    this.value = ;
      iif(this.Parent.oContained.w_ARPERSER=='I',1,;
      iif(this.Parent.oContained.w_ARPERSER=='R',2,;
      0))
  endfunc

  func oARPERSER_1_50.mHide()
    with this.Parent.oContained
      return (.w_ARUTISER='N' )
    endwith
  endfunc

  add object oARNOMENC_1_51 as StdField with uid="IYSHDAFNEO",rtseq=50,rtrep=.f.,;
    cFormVar = "w_ARNOMENC", cQueryName = "ARNOMENC",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o incongruente",;
    ToolTipText = "Codice nomenclatura combinata dell'articolo",;
    HelpContextID = 241583287,;
   bGlobalFont=.t.,;
    Height=21, Width=84, Left=110, Top=478, InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="NOMENCLA", cZoomOnZoom="GSAR_ANM", oKey_1_1="NMCODICE", oKey_1_2="this.w_ARNOMENC"

  func oARNOMENC_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_INTR='S' and (.w_ARDATINT<>'N' OR .w_ARUTISER<>'N'))
    endwith
   endif
  endfunc

  func oARNOMENC_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_51('Part',this)
    endwith
    return bRes
  endfunc

  proc oARNOMENC_1_51.ecpDrop(oSource)
    this.Parent.oContained.link_1_51('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARNOMENC_1_51.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NOMENCLA','*','NMCODICE',cp_AbsName(this.parent,'oARNOMENC_1_51'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANM',"",'GSMA_AZN.NOMENCLA_VZM',this.parent.oContained
  endproc
  proc oARNOMENC_1_51.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NMCODICE=this.parent.oContained.w_ARNOMENC
     i_obj.ecpSave()
  endproc

  add object oUMSUPP_1_52 as StdField with uid="JDQDKYKMMA",rtseq=51,rtrep=.f.,;
    cFormVar = "w_UMSUPP", cQueryName = "UMSUPP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 53475514,;
   bGlobalFont=.t.,;
    Height=21, Width=46, Left=110, Top=508, InputMask=replicate('X',3)

  add object oARMOLSUP_1_53 as StdField with uid="ZQNEQJJEVA",rtseq=52,rtrep=.f.,;
    cFormVar = "w_ARMOLSUP", cQueryName = "ARMOLSUP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Moltiplicatore U.M. supplementare rispetto a U.M. principale",;
    HelpContextID = 260680534,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=234, Top=508, cSayPict='"9999.99999"', cGetPict='"9999.99999"'

  func oARMOLSUP_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ARUMSUPP) AND g_INTR='S' AND .w_ARUTISER<>'S')
    endwith
   endif
  endfunc


  add object oARDATINT_1_54 as StdCombo with uid="FSVPVUIYSJ",rtseq=53,rtrep=.f.,left=197,top=537,width=162,height=21;
    , ToolTipText = "Compilazione campi dati Intrastat: nessuno - solo statistici - statistici e fiscali - solo fiscali";
    , HelpContextID = 168092838;
    , cFormVar="w_ARDATINT",RowSource=""+"Nessuna valorizzazione,"+"Solo dati statistici,"+"Dati statistici e fiscali,"+"Solo dati fiscali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARDATINT_1_54.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'F',;
    iif(this.value =4,'I',;
    space(1))))))
  endfunc
  func oARDATINT_1_54.GetRadio()
    this.Parent.oContained.w_ARDATINT = this.RadioValue()
    return .t.
  endfunc

  func oARDATINT_1_54.SetRadio()
    this.Parent.oContained.w_ARDATINT=trim(this.Parent.oContained.w_ARDATINT)
    this.value = ;
      iif(this.Parent.oContained.w_ARDATINT=='N',1,;
      iif(this.Parent.oContained.w_ARDATINT=='S',2,;
      iif(this.Parent.oContained.w_ARDATINT=='F',3,;
      iif(this.Parent.oContained.w_ARDATINT=='I',4,;
      0))))
  endfunc

  func oARDATINT_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARUTISER='N')
    endwith
   endif
  endfunc


  add object oObj_1_71 as cp_runprogram with uid="PXTEXKYICD",left=3, top=589, width=173,height=17,;
    caption='GSMA_BDV',;
   bGlobalFont=.t.,;
    prg="GSMA_BDV",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 262909116


  add object oBtn_1_72 as StdButton with uid="BLRVQQZDHH",left=644, top=53, width=48,height=45,;
    CpPicture="BMP\CODICI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai codici di ricerca";
    , HelpContextID = 138248486;
    , tabstop=.f., Caption='C\<odici';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_72.Click()
      with this.Parent.oContained
        do GSMA_KCA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_72.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ARCODART) AND .cFunction<>'Load')
      endwith
    endif
  endfunc


  add object oLinkPC_1_75 as StdButton with uid="UPOTGIPSPX",left=644, top=100, width=48,height=45,;
    CpPicture="BMP\INVENTAR.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla manutenzione dei listini";
    , HelpContextID = 58645834;
    , tabstop=.f., Caption='\<Listini';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_75.Click()
      this.Parent.oContained.GSMA_MLI.LinkPCClick()
    endproc

  func oLinkPC_1_75.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc

  func oLinkPC_1_75.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (isAlt())
     endwith
    endif
  endfunc


  add object oObj_1_80 as cp_runprogram with uid="SFHLWEXFLI",left=179, top=589, width=168,height=18,;
    caption='GSMA_BCV(I)',;
   bGlobalFont=.t.,;
    prg='GSMA_BCV("I")',;
    cEvent = "Insert end",;
    nPag=1;
    , ToolTipText = "Inserisce le varianti alla conferma del caricamento";
    , HelpContextID = 263096380


  add object oObj_1_81 as cp_runprogram with uid="WGBTKDUCSH",left=350, top=589, width=201,height=18,;
    caption='GSMA_BCV(U)',;
   bGlobalFont=.t.,;
    prg='GSMA_BCV("U")',;
    cEvent = "Update start",;
    nPag=1;
    , ToolTipText = "Inserisce le varianti alla conferma del caricamento";
    , HelpContextID = 263099452

  add object oDESVOC_1_83 as StdField with uid="OGVJQXXPYK",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 4129226,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=279, Top=318, InputMask=replicate('X',40)

  add object oDESRIC_1_86 as StdField with uid="CHWDGBJFBY",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESRIC", cQueryName = "DESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 10682826,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=279, Top=346, InputMask=replicate('X',40)

  add object oTIDESCRI_1_94 as StdField with uid="VJWAEWROHS",rtseq=73,rtrep=.f.,;
    cFormVar = "w_TIDESCRI", cQueryName = "TIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 1109121,;
   bGlobalFont=.t.,;
    Height=21, Width=231, Left=225, Top=194, InputMask=replicate('X',30)


  add object oBtn_1_98 as StdButton with uid="ANLYFEHJWV",left=591, top=6, width=48,height=45,;
    CpPicture="bmp\cattura.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da unit� disco";
    , HelpContextID = 21047846;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'CATTURA', disabledpicture = 'bmp\catturad.bmp', caption='\<Cattura';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_98.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"ART_ICOL",.w_ARCODART,"GSMA_AAR","C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_98.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ARCODART) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_98.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT(g_FLARDO='S') or isAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_99 as StdButton with uid="UVCTHODNGY",left=644, top=6, width=48,height=45,;
    CpPicture="BMP\visualicat.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza allegati";
    , HelpContextID = 100496272;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_99.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"ART_ICOL",.w_ARCODART,"GSMA_AAR","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_99.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ARCODART) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_99.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT(g_FLARDO='S') or isAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_100 as StdButton with uid="BJRFWTRDLX",left=591, top=53, width=48,height=45,;
    CpPicture="bmp\scanner.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da periferica di acquisizione immagini";
    , HelpContextID = 63569190;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'SCANNER', disabledpicture = 'bmp\scannerd.bmp', caption='\<Scanner';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_100.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"ART_ICOL",.w_ARCODART,"GSMA_AAR","S",MROW(),MCOL(),.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_100.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ARCODART) And .cFunction='Query')
      endwith
    endif
  endfunc

  func oBtn_1_100.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT(g_FLARDO='S' AND g_DOCM='S') OR IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_101 as StdButton with uid="YPRRGQGCBA",left=644, top=147, width=48,height=45,;
    CpPicture="BMP\avanzate.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla manutenzione basi di calcolo";
    , HelpContextID = 216420018;
    , tabstop=.f., Caption='\<Basi calc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_101.Click()
      do GSVA_KBC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_101.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc

  func oBtn_1_101.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_VEFA<>'S')
     endwith
    endif
  endfunc


  add object oBtn_1_103 as StdButton with uid="VMAVLCWAFG",left=644, top=6, width=48,height=45,;
    CpPicture="COPY.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per duplicare la prestazione";
    , HelpContextID = 24706614;
    , tabstop=.f., Caption='\<Duplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_103.Click()
      do GSAL_KDS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_103.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (IsAlt() AND NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc

  func oBtn_1_103.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!isAlt() OR .cFunction#"Query" OR (.cFunction="Query" AND empty(.w_ARCODART)))
     endwith
    endif
  endfunc


  add object oBtn_1_114 as StdButton with uid="WQIYLXYYJX",left=644, top=100, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il dettaglio attivit�";
    , HelpContextID = 182843962;
    , tabstop=.f., Caption='\<Tipi atti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_114.Click()
      do GSAG_KTR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_114.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (IsAlt() AND NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc

  func oBtn_1_114.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!isAlt() OR .cFunction#"Query" OR (.cFunction="Query" AND empty(.w_ARCODART)))
     endwith
    endif
  endfunc

  add object oDESNOM_1_116 as StdField with uid="JIVDUUPNUG",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DESNOM", cQueryName = "DESNOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 105316810,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=197, Top=478, InputMask=replicate('X',35)


  add object oBtn_1_131 as StdButton with uid="ZKTFGYXQCP",left=644, top=194, width=48,height=45,;
    CpPicture="bmp\pagat.ico", caption="", nPag=1;
    , ToolTipText = "Premere per inserire o visualizzare la spesa e l'anticipazione collegata a questa tariffa con un eventuale importo predefinito";
    , HelpContextID = 29406682;
    , TabStop=.f.,Caption='\<Spese';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_131.Click()
      do gsal_ksa with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_131.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (! Isalt() or .w_ARFLSPAN<>'S')
     endwith
    endif
  endfunc


  add object oARPARAME_1_140 as StdCombo with uid="LRDFQQISGN",rtseq=92,rtrep=.f.,left=575,top=346,width=117,height=22;
    , ToolTipText = "Se attivo, attribuisce alla voce di tariffa il significato di parametro di riferimento per la liquidazione dei compensi da parte del giudice";
    , HelpContextID = 35923125;
    , cFormVar="w_ARPARAME",RowSource=""+"Parametro 2012,"+"Parametro 2014,"+"Nessun parametro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARPARAME_1_140.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'4',;
    iif(this.value =3,'N' ,;
    space(1)))))
  endfunc
  func oARPARAME_1_140.GetRadio()
    this.Parent.oContained.w_ARPARAME = this.RadioValue()
    return .t.
  endfunc

  func oARPARAME_1_140.SetRadio()
    this.Parent.oContained.w_ARPARAME=trim(this.Parent.oContained.w_ARPARAME)
    this.value = ;
      iif(this.Parent.oContained.w_ARPARAME=='S',1,;
      iif(this.Parent.oContained.w_ARPARAME=='4',2,;
      iif(this.Parent.oContained.w_ARPARAME=='N',3,;
      0)))
  endfunc

  func oARPARAME_1_140.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_ARPRESTA$"SA")
    endwith
   endif
  endfunc

  func oARPARAME_1_140.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="ZBHXXJRAGC",Visible=.t., Left=461, Top=167,;
    Alignment=1, Width=120, Height=15,;
    Caption="N. max di occorrenze:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (Not Isalt() or .w_ARFLUNIV='N')
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="BJVOKBFXHF",Visible=.t., Left=18, Top=10,;
    Alignment=1, Width=55, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_56 as StdString with uid="UUGCEEDUNP",Visible=.t., Left=2, Top=277,;
    Alignment=1, Width=130, Height=15,;
    Caption="Gruppo merceologico:"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="JACSGMCZTA",Visible=.t., Left=61, Top=35,;
    Alignment=1, Width=42, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="EQPTALKJPA",Visible=.t., Left=239, Top=377,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="XPNXOWBZDG",Visible=.t., Left=2, Top=167,;
    Alignment=1, Width=130, Height=15,;
    Caption="1^Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="TKMQGLPBTA",Visible=.t., Left=2, Top=249,;
    Alignment=1, Width=130, Height=15,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="GDQZAMWCMT",Visible=.t., Left=2, Top=221,;
    Alignment=1, Width=130, Height=15,;
    Caption="Codice IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="NIIKQIFGPY",Visible=.t., Left=364, Top=151,;
    Alignment=0, Width=93, Height=15,;
    Caption="Parametro"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="TCDIMPFJEQ",Visible=.t., Left=316, Top=151,;
    Alignment=0, Width=41, Height=15,;
    Caption="Oper."  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="JOLDYLLJVA",Visible=.t., Left=211, Top=167,;
    Alignment=1, Width=45, Height=15,;
    Caption="2^U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="JDBGCEGQYV",Visible=.t., Left=197, Top=167,;
    Alignment=0, Width=15, Height=15,;
    Caption="="  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="DTWDMDVJVG",Visible=.t., Left=15, Top=375,;
    Alignment=1, Width=117, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_76.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_82 as StdString with uid="PTMAYYSUMN",Visible=.t., Left=2, Top=318,;
    Alignment=1, Width=130, Height=18,;
    Caption="Voce di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="XNLSPCSINJ",Visible=.t., Left=11, Top=346,;
    Alignment=1, Width=121, Height=18,;
    Caption="Voce di ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="IIRGQPDFCE",Visible=.t., Left=19, Top=194,;
    Alignment=1, Width=113, Height=18,;
    Caption="Tipo operazione IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_95 as StdString with uid="BXQFSNENXS",Visible=.t., Left=462, Top=151,;
    Alignment=0, Width=91, Height=15,;
    Caption="U.M. separate"  ;
  , bGlobalFont=.t.

  func oStr_1_95.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_102 as StdString with uid="OMDZQICQKE",Visible=.t., Left=37, Top=143,;
    Alignment=1, Width=95, Height=18,;
    Caption="Classificazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_102.mHide()
    with this.Parent.oContained
      return (!isAlt())
    endwith
  endfunc

  add object oStr_1_104 as StdString with uid="CXLMQNGTZK",Visible=.t., Left=464, Top=190,;
    Alignment=0, Width=177, Height=18,;
    Caption="Appl. prezzo valido per U.M."  ;
  , bGlobalFont=.t.

  add object oStr_1_111 as StdString with uid="URNJJIULVL",Visible=.t., Left=7, Top=424,;
    Alignment=0, Width=41, Height=15,;
    Caption="INTRA"  ;
  , bGlobalFont=.t.

  add object oStr_1_113 as StdString with uid="XRXAJDFVVN",Visible=.t., Left=462, Top=265,;
    Alignment=0, Width=91, Height=15,;
    Caption="Calcoli in %"  ;
  , bGlobalFont=.t.

  func oStr_1_113.mHide()
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
  endfunc

  add object oStr_1_115 as StdString with uid="BRPQNIYQYS",Visible=.t., Left=477, Top=377,;
    Alignment=1, Width=94, Height=18,;
    Caption="Tipo prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_115.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S')
    endwith
  endfunc

  add object oStr_1_117 as StdString with uid="HYSZWATHRW",Visible=.t., Left=18, Top=452,;
    Alignment=1, Width=91, Height=15,;
    Caption="Utilizzo intra:"  ;
  , bGlobalFont=.t.

  add object oStr_1_118 as StdString with uid="TAKRNMAYZI",Visible=.t., Left=276, Top=452,;
    Alignment=1, Width=226, Height=18,;
    Caption="Modalit� di erogazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_118.mHide()
    with this.Parent.oContained
      return (.w_ARUTISER='N' )
    endwith
  endfunc

  add object oStr_1_119 as StdString with uid="XNQIHVIHXH",Visible=.t., Left=5, Top=480,;
    Alignment=1, Width=104, Height=15,;
    Caption="Codice servizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_119.mHide()
    with this.Parent.oContained
      return (.w_ARUTISER<>'S' )
    endwith
  endfunc

  add object oStr_1_120 as StdString with uid="PGZRSKEAFC",Visible=.t., Left=31, Top=539,;
    Alignment=1, Width=160, Height=18,;
    Caption="Manutenzione elenchi INTRA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_121 as StdString with uid="KJORVBXBLI",Visible=.t., Left=38, Top=508,;
    Alignment=1, Width=71, Height=15,;
    Caption="U.M.suppl.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_122 as StdString with uid="JTILIHSXSX",Visible=.t., Left=182, Top=508,;
    Alignment=1, Width=50, Height=15,;
    Caption="Moltipl.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_123 as StdString with uid="MZJPFIKXGM",Visible=.t., Left=28, Top=480,;
    Alignment=1, Width=81, Height=18,;
    Caption="Nomenclatura:"  ;
  , bGlobalFont=.t.

  func oStr_1_123.mHide()
    with this.Parent.oContained
      return (.w_ARUTISER<>'N' )
    endwith
  endfunc

  add object oStr_1_125 as StdString with uid="HAVUQAKWDJ",Visible=.t., Left=32, Top=401,;
    Alignment=1, Width=100, Height=18,;
    Caption="Documento:"  ;
  , bGlobalFont=.t.

  func oStr_1_125.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  add object oStr_1_137 as StdString with uid="YIEMTRMAPB",Visible=.t., Left=272, Top=401,;
    Alignment=1, Width=90, Height=18,;
    Caption="Nota spese:"  ;
  , bGlobalFont=.t.

  func oStr_1_137.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  add object oStr_1_141 as StdString with uid="IWIXQSDFOL",Visible=.t., Left=481, Top=401,;
    Alignment=1, Width=90, Height=18,;
    Caption="Fase:"  ;
  , bGlobalFont=.t.

  func oStr_1_141.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oBox_1_110 as StdBox with uid="ENRGRELBDC",left=44, top=432, width=641,height=1
enddefine
define class tgsma_aasPag2 as StdContainer
  Width  = 694
  height = 570
  stdWidth  = 694
  stdheight = 570
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_2_1 as StdField with uid="QFCAJNQIYG",rtseq=100,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 138209242,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=72, Top=10, InputMask=replicate('X',20)

  add object oDESC_2_2 as StdField with uid="PRJUKFFCAV",rtseq=101,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 138543562,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=259, Top=10, InputMask=replicate('X',40)

  add object oARGRUPRO_2_4 as StdField with uid="LOSIBUQPIC",rtseq=102,rtrep=.f.,;
    cFormVar = "w_ARGRUPRO", cQueryName = "ARGRUPRO",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria provvigioni inesistente o non di tipo articolo",;
    ToolTipText = "Codice della categoria provvigioni associata all'articolo",;
    HelpContextID = 219958101,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=172, Top=48, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUPRO", cZoomOnZoom="GSAR_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_ARGRUPRO"

  func oARGRUPRO_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPART<>'DE')
    endwith
   endif
  endfunc

  func oARGRUPRO_2_4.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  func oARGRUPRO_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oARGRUPRO_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARGRUPRO_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPRO','*','GPCODICE',cp_AbsName(this.parent,'oARGRUPRO_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGP',"Categorie provvigioni",'GSMA_AAR.GRUPRO_VZM',this.parent.oContained
  endproc
  proc oARGRUPRO_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_ARGRUPRO
     i_obj.ecpSave()
  endproc

  add object oDESGPP_2_5 as StdField with uid="WRONIASVZO",rtseq=103,rtrep=.f.,;
    cFormVar = "w_DESGPP", cQueryName = "DESGPP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 54395338,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=240, Top=48, InputMask=replicate('X',35)

  func oDESGPP_2_5.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oARCATSCM_2_6 as StdField with uid="WITZSUTKNQ",rtseq=104,rtrep=.f.,;
    cFormVar = "w_ARCATSCM", cQueryName = "ARCATSCM",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria sconti / maggiorazioni inesistente o non di tipo articolo",;
    ToolTipText = "Codice della categoria sconti / maggiorazioni associata all'articolo",;
    HelpContextID = 268110675,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=172, Top=78, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_SCMA", cZoomOnZoom="GSAR_ASM", oKey_1_1="CSCODICE", oKey_1_2="this.w_ARCATSCM"

  func oARCATSCM_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPART<>'DE')
    endwith
   endif
  endfunc

  func oARCATSCM_2_6.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  func oARCATSCM_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCATSCM_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCATSCM_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_SCMA','*','CSCODICE',cp_AbsName(this.parent,'oARCATSCM_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSMA_AAR.CAT_SCMA_VZM',this.parent.oContained
  endproc
  proc oARCATSCM_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_ARCATSCM
     i_obj.ecpSave()
  endproc

  add object oDESSCM_2_7 as StdField with uid="RCJUNNMRPS",rtseq=105,rtrep=.f.,;
    cFormVar = "w_DESSCM", cQueryName = "DESSCM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 117572042,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=240, Top=78, InputMask=replicate('X',35)

  func oDESSCM_2_7.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oARCODCLA_2_10 as StdField with uid="XSQYQZUGHO",rtseq=106,rtrep=.f.,;
    cFormVar = "w_ARCODCLA", cQueryName = "ARCODCLA",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia documento gestita come filtro di stampa/esclusione a livello documenti",;
    HelpContextID = 16184505,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=172, Top=108, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CLA_RIGD", cZoomOnZoom="GSAR_ATR", oKey_1_1="TRCODCLA", oKey_1_2="this.w_ARCODCLA"

  func oARCODCLA_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODCLA_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODCLA_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RIGD','*','TRCODCLA',cp_AbsName(this.parent,'oARCODCLA_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATR',"Tipologie righe documenti",'',this.parent.oContained
  endproc
  proc oARCODCLA_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODCLA=this.parent.oContained.w_ARCODCLA
     i_obj.ecpSave()
  endproc

  add object oDESCLA_2_11 as StdField with uid="WQDANZWHEN",rtseq=107,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 42074570,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=240, Top=108, InputMask=replicate('X',30)

  add object oARCODREP_2_13 as StdField with uid="BUVKTSKIZC",rtseq=108,rtrep=.f.,;
    cFormVar = "w_ARCODREP", cQueryName = "ARCODREP",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice reparto inesistente",;
    ToolTipText = "Codice reparto di vendita dettaglio",;
    HelpContextID = 235473750,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=172, Top=138, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="REP_ARTI", cZoomOnZoom="GSPS_ARE", oKey_1_1="RECODREP", oKey_1_2="this.w_ARCODREP"

  func oARCODREP_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GPOS='S' AND .w_ARTIPART<>'DE' AND .w_ARARTPOS='S')
    endwith
   endif
  endfunc

  func oARCODREP_2_13.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  func oARCODREP_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODREP_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODREP_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REP_ARTI','*','RECODREP',cp_AbsName(this.parent,'oARCODREP_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ARE',"Reparti",'',this.parent.oContained
  endproc
  proc oARCODREP_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ARE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODREP=this.parent.oContained.w_ARCODREP
     i_obj.ecpSave()
  endproc

  add object oDESREP_2_14 as StdField with uid="SPMJXBJXHJ",rtseq=109,rtrep=.f.,;
    cFormVar = "w_DESREP", cQueryName = "DESREP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 65208778,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=240, Top=138, InputMask=replicate('X',40)

  func oDESREP_2_14.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oARCODGRU_2_16 as StdField with uid="ATHJTOKSON",rtseq=110,rtrep=.f.,;
    cFormVar = "w_ARCODGRU", cQueryName = "ARCODGRU",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo di default proposto in caricamento documenti di offerta e kit promozionali",;
    HelpContextID = 50924379,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=172, Top=168, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIT_SEZI", cZoomOnZoom="GSOF_ATS", oKey_1_1="TSCODICE", oKey_1_2="this.w_ARCODGRU"

  func oARCODGRU_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_OFFE='S')
    endwith
   endif
  endfunc

  func oARCODGRU_2_16.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S')
    endwith
  endfunc

  func oARCODGRU_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
      if .not. empty(.w_ARCODSOT)
        bRes2=.link_2_17('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oARCODGRU_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODGRU_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIT_SEZI','*','TSCODICE',cp_AbsName(this.parent,'oARCODGRU_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ATS',"Codici gruppi",'',this.parent.oContained
  endproc
  proc oARCODGRU_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ATS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TSCODICE=this.parent.oContained.w_ARCODGRU
     i_obj.ecpSave()
  endproc

  add object oARCODSOT_2_17 as StdField with uid="YXLZZCRUEL",rtseq=111,rtrep=.f.,;
    cFormVar = "w_ARCODSOT", cQueryName = "ARCODSOT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sottogruppo di default proposto in caricamento documenti di offerta e kit promozionali",;
    HelpContextID = 16184486,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=172, Top=198, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SOT_SEZI", cZoomOnZoom="GSOF_ASF", oKey_1_1="SGCODGRU", oKey_1_2="this.w_ARCODGRU", oKey_2_1="SGCODSOT", oKey_2_2="this.w_ARCODSOT"

  func oARCODSOT_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_OFFE='S' AND NOT EMPTY(.w_ARCODGRU))
    endwith
   endif
  endfunc

  func oARCODSOT_2_17.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S')
    endwith
  endfunc

  func oARCODSOT_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODSOT_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODSOT_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SOT_SEZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStrODBC(this.Parent.oContained.w_ARCODGRU)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SGCODGRU="+cp_ToStr(this.Parent.oContained.w_ARCODGRU)
    endif
    do cp_zoom with 'SOT_SEZI','*','SGCODGRU,SGCODSOT',cp_AbsName(this.parent,'oARCODSOT_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_ASF',"Codici sottogruppi",'',this.parent.oContained
  endproc
  proc oARCODSOT_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSOF_ASF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SGCODGRU=w_ARCODGRU
     i_obj.w_SGCODSOT=this.parent.oContained.w_ARCODSOT
     i_obj.ecpSave()
  endproc

  add object oARFLCESP_2_18 as StdCheck with uid="LSMEHKIMHW",rtseq=112,rtrep=.f.,left=603, top=230, caption="Cespite",;
    ToolTipText = "Se attivo: identifica un cespite",;
    HelpContextID = 16137046,;
    cFormVar="w_ARFLCESP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oARFLCESP_2_18.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARFLCESP_2_18.GetRadio()
    this.Parent.oContained.w_ARFLCESP = this.RadioValue()
    return .t.
  endfunc

  func oARFLCESP_2_18.SetRadio()
    this.Parent.oContained.w_ARFLCESP=trim(this.Parent.oContained.w_ARFLCESP)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLCESP=='S',1,;
      0)
  endfunc

  func oARFLCESP_2_18.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S' OR .w_ARTIPART='DE')
    endwith
  endfunc

  add object oARCATCES_2_19 as StdField with uid="NOJTQHANIW",rtseq=113,rtrep=.f.,;
    cFormVar = "w_ARCATCES", cQueryName = "ARCATCES",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Categoria cespite associata",;
    HelpContextID = 268110681,;
   bGlobalFont=.t.,;
    Height=21, Width=122, Left=172, Top=230, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAT_CESP", cZoomOnZoom="GSCE_ACT", oKey_1_1="CCCODICE", oKey_1_2="this.w_ARCATCES"

  func oARCATCES_2_19.mHide()
    with this.Parent.oContained
      return (.w_ARFLCESP<>'S')
    endwith
  endfunc

  func oARCATCES_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCATCES_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCATCES_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_CESP','*','CCCODICE',cp_AbsName(this.parent,'oARCATCES_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCE_ACT',"Categorie cespiti",'',this.parent.oContained
  endproc
  proc oARCATCES_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSCE_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_ARCATCES
     i_obj.ecpSave()
  endproc

  add object oARCONTRI_2_20 as StdField with uid="IMFRFTYBGB",rtseq=114,rtrep=.f.,;
    cFormVar = "w_ARCONTRI", cQueryName = "ARCONTRI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 11078479,;
   bGlobalFont=.t.,;
    Height=21, Width=122, Left=172, Top=259, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TIPCONTR", cZoomOnZoom="GSAR_ATP", oKey_1_1="TPCODICE", oKey_1_2="this.w_ARCONTRI"

  func oARCONTRI_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARTIPSER = 'S' AND .w_ARTIPART = 'FM')
    endwith
   endif
  endfunc

  func oARCONTRI_2_20.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  func oARCONTRI_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCONTRI_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCONTRI_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPCONTR','*','TPCODICE',cp_AbsName(this.parent,'oARCONTRI_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATP',"Tipi contributo",'',this.parent.oContained
  endproc
  proc oARCONTRI_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TPCODICE=this.parent.oContained.w_ARCONTRI
     i_obj.ecpSave()
  endproc

  add object oARMINVEN_2_21 as StdField with uid="TMJJVADDCE",rtseq=115,rtrep=.f.,;
    cFormVar = "w_ARMINVEN", cQueryName = "ARMINVEN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantitativo minimo vendibile/ordinabile espresso nell'unit� di misura principale",;
    HelpContextID = 44280660,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=172, Top=288, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oARMINVEN_2_21.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART $  'FO-DE')
    endwith
  endfunc

  add object oARRIPCON_2_24 as StdCheck with uid="JMNXNHJJEK",rtseq=117,rtrep=.f.,left=172, top=317, caption="Ripartizione contributo su articolo",;
    ToolTipText = "Permette la ripartizione del valore del contributo accessorio sul valore fiscale dell'articolo",;
    HelpContextID = 3933356,;
    cFormVar="w_ARRIPCON", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oARRIPCON_2_24.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oARRIPCON_2_24.GetRadio()
    this.Parent.oContained.w_ARRIPCON = this.RadioValue()
    return .t.
  endfunc

  func oARRIPCON_2_24.SetRadio()
    this.Parent.oContained.w_ARRIPCON=trim(this.Parent.oContained.w_ARRIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_ARRIPCON=='S',1,;
      0)
  endfunc

  func oARRIPCON_2_24.mHide()
    with this.Parent.oContained
      return (EMPTY( .w_ARCONTRI ) OR .w_TPAPPPES="S")
    endwith
  endfunc

  add object oARARTPOS_2_25 as StdCheck with uid="XTLQKQYUKN",rtseq=118,rtrep=.f.,left=172, top=346, caption="Servizio P.O.S.",;
    ToolTipText = "Se attivo: servizio gestito nelle funzioni di vendita dettaglio",;
    HelpContextID = 49550503,;
    cFormVar="w_ARARTPOS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oARARTPOS_2_25.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARARTPOS_2_25.GetRadio()
    this.Parent.oContained.w_ARARTPOS = this.RadioValue()
    return .t.
  endfunc

  func oARARTPOS_2_25.SetRadio()
    this.Parent.oContained.w_ARARTPOS=trim(this.Parent.oContained.w_ARARTPOS)
    this.value = ;
      iif(this.Parent.oContained.w_ARARTPOS=='S',1,;
      0)
  endfunc

  func oARARTPOS_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GPOS='S')
    endwith
   endif
  endfunc

  func oARARTPOS_2_25.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oARPUBWEB_2_27 as StdCheck with uid="DFATIZXQVI",rtseq=119,rtrep=.f.,left=172, top=377, caption="Servizio eCRM Zucchetti",;
    ToolTipText = "Se attivo: servizio pubblicato nell'applicativo eCRM Zucchetti",;
    HelpContextID = 49273672,;
    cFormVar="w_ARPUBWEB", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oARPUBWEB_2_27.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oARPUBWEB_2_27.GetRadio()
    this.Parent.oContained.w_ARPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oARPUBWEB_2_27.SetRadio()
    this.Parent.oContained.w_ARPUBWEB=trim(this.Parent.oContained.w_ARPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_ARPUBWEB=='S',1,;
      0)
  endfunc

  func oARPUBWEB_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ECRM='S' AND g_IZCP<>'S' AND .w_ARTIPART='FM' AND .w_ARFLSERG=' ')
    endwith
   endif
  endfunc

  func oARPUBWEB_2_27.mHide()
    with this.Parent.oContained
      return (NOT (g_ECRM='S'   AND g_IZCP='N' AND .w_ARTIPART='FM' AND .w_ARFLSERG=' ') or Isalt())
    endwith
  endfunc


  add object oARPUBWEB_2_28 as StdCombo with uid="BQJGYMHQVU",rtseq=120,rtrep=.f.,left=172,top=407,width=144,height=21;
    , ToolTipText = "Se attivo: servizio gestito dall'applicazione Web";
    , HelpContextID = 49273672;
    , cFormVar="w_ARPUBWEB",RowSource=""+"Pubblica su web,"+"Non pubblicare su web,"+"Trasferisci su web", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oARPUBWEB_2_28.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oARPUBWEB_2_28.GetRadio()
    this.Parent.oContained.w_ARPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oARPUBWEB_2_28.SetRadio()
    this.Parent.oContained.w_ARPUBWEB=trim(this.Parent.oContained.w_ARPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_ARPUBWEB=='S',1,;
      iif(this.Parent.oContained.w_ARPUBWEB=='N',2,;
      iif(this.Parent.oContained.w_ARPUBWEB=='T',3,;
      0)))
  endfunc

  func oARPUBWEB_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP $ 'SA' )
    endwith
   endif
  endfunc

  func oARPUBWEB_2_28.mHide()
    with this.Parent.oContained
      return (g_IZCP='N' OR g_CPIN='S')
    endwith
  endfunc


  add object oARPUBWEB_2_29 as StdCombo with uid="VBTIIQEBBR",rtseq=121,rtrep=.f.,left=172,top=407,width=144,height=21;
    , ToolTipText = "Se attivo: servizio gestito dall'applicazione Web";
    , HelpContextID = 49273672;
    , cFormVar="w_ARPUBWEB",RowSource=""+"Pubblica su web,"+"Non pubblicare su web,"+"Trasferisci su web", bObbl = .f. , nPag = 2;
    , sErrorMsg = "Cambio modalit� Web non consentita!";
  , bGlobalFont=.t.


  func oARPUBWEB_2_29.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oARPUBWEB_2_29.GetRadio()
    this.Parent.oContained.w_ARPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oARPUBWEB_2_29.SetRadio()
    this.Parent.oContained.w_ARPUBWEB=trim(this.Parent.oContained.w_ARPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_ARPUBWEB=='S',1,;
      iif(this.Parent.oContained.w_ARPUBWEB=='N',2,;
      iif(this.Parent.oContained.w_ARPUBWEB=='T',3,;
      0)))
  endfunc

  func oARPUBWEB_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_CPIN='S' Or g_REVICRM='S')
    endwith
   endif
  endfunc

  func oARPUBWEB_2_29.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S' And g_REVICRM<>'S')
    endwith
  endfunc

  func oARPUBWEB_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (g_REVICRM='S' Or (.w_OLDPUBWEB='N' or (.w_OLDPUBWEB $ 'S-T' and .w_ARPUBWEB $ 'T-S')))
    endwith
    return bRes
  endfunc

  add object oDESSOT_2_30 as StdField with uid="DTTIXTENZG",rtseq=122,rtrep=.f.,;
    cFormVar = "w_DESSOT", cQueryName = "DESSOT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 255984074,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=240, Top=198, InputMask=replicate('X',35)

  func oDESSOT_2_30.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S')
    endwith
  endfunc

  add object oDESTIT_2_31 as StdField with uid="UOFQAKEFTS",rtseq=123,rtrep=.f.,;
    cFormVar = "w_DESTIT", cQueryName = "DESTIT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 262209994,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=240, Top=168, InputMask=replicate('X',35)

  func oDESTIT_2_31.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S')
    endwith
  endfunc

  add object oDESCACES_2_35 as StdField with uid="TMELCRNFEB",rtseq=126,rtrep=.f.,;
    cFormVar = "w_DESCACES", cQueryName = "DESCACES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 248381065,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=299, Top=230, InputMask=replicate('X',40)

  func oDESCACES_2_35.mHide()
    with this.Parent.oContained
      return (.w_ARFLCESP<>'S')
    endwith
  endfunc

  add object oDESTIPCO_2_38 as StdField with uid="XVSCAKDQNC",rtseq=127,rtrep=.f.,;
    cFormVar = "w_DESTIPCO", cQueryName = "DESTIPCO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 207552133,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=299, Top=259, InputMask=replicate('X',40)

  func oDESTIPCO_2_38.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  add object oARCODTIP_2_43 as StdField with uid="UWNCKSOVUI",rtseq=137,rtrep=.f.,;
    cFormVar = "w_ARCODTIP", cQueryName = "ARCODTIP",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indica la tipologia di codice articolo (per esempio, TARIC, CPV, EAN, SSC, ...)",;
    HelpContextID = 267842730,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=172, Top=467, InputMask=replicate('X',35)

  add object oARCODCLF_2_44 as StdField with uid="MJWYUTIROE",rtseq=138,rtrep=.f.,;
    cFormVar = "w_ARCODCLF", cQueryName = "ARCODCLF",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice di classificazione per fatturazione elettronica",;
    HelpContextID = 16184500,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=603, Top=467, InputMask=replicate('X',5)

  add object oARCODVAL_2_45 as StdField with uid="VCGAGDOJNU",rtseq=139,rtrep=.f.,;
    cFormVar = "w_ARCODVAL", cQueryName = "ARCODVAL",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indica il valore del codice articolo corrispondente alla tipologia riportata nell'elemento informativo 2.2.1.3.1 <CodiceTipo>",;
    HelpContextID = 34147154,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=172, Top=497, InputMask=replicate('X',35)

  add object oARFLGESC_2_51 as StdCheck with uid="IKXQKROTUZ",rtseq=142,rtrep=.f.,left=514, top=494, caption="Codice articolo esclusivo",;
    ToolTipText = "Codice articolo esclusivo",;
    HelpContextID = 20331337,;
    cFormVar="w_ARFLGESC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oARFLGESC_2_51.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oARFLGESC_2_51.GetRadio()
    this.Parent.oContained.w_ARFLGESC = this.RadioValue()
    return .t.
  endfunc

  func oARFLGESC_2_51.SetRadio()
    this.Parent.oContained.w_ARFLGESC=trim(this.Parent.oContained.w_ARFLGESC)
    this.value = ;
      iif(this.Parent.oContained.w_ARFLGESC=='S',1,;
      0)
  endfunc

  add object oStr_2_3 as StdString with uid="PRAHFXIDWQ",Visible=.t., Left=17, Top=10,;
    Alignment=1, Width=53, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_8 as StdString with uid="VRSREDJTDQ",Visible=.t., Left=38, Top=48,;
    Alignment=1, Width=133, Height=15,;
    Caption="Categoria provvigioni:"  ;
  , bGlobalFont=.t.

  func oStr_2_8.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oStr_2_9 as StdString with uid="LIDQMIQHDT",Visible=.t., Left=41, Top=78,;
    Alignment=1, Width=130, Height=15,;
    Caption="Categoria sconti/mag.:"  ;
  , bGlobalFont=.t.

  func oStr_2_9.mHide()
    with this.Parent.oContained
      return (isAlt())
    endwith
  endfunc

  add object oStr_2_12 as StdString with uid="HKBKNYEKMA",Visible=.t., Left=7, Top=105,;
    Alignment=1, Width=164, Height=18,;
    Caption="Tipologia riga documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="JZIMXQWCON",Visible=.t., Left=41, Top=138,;
    Alignment=1, Width=130, Height=15,;
    Caption="Codice reparto:"  ;
  , bGlobalFont=.t.

  func oStr_2_15.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oStr_2_22 as StdString with uid="AIFYDUGJCD",Visible=.t., Left=41, Top=168,;
    Alignment=1, Width=130, Height=15,;
    Caption="Codice gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_2_22.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S')
    endwith
  endfunc

  add object oStr_2_26 as StdString with uid="ZTPICWREOH",Visible=.t., Left=41, Top=198,;
    Alignment=1, Width=130, Height=15,;
    Caption="Sottogruppo:"  ;
  , bGlobalFont=.t.

  func oStr_2_26.mHide()
    with this.Parent.oContained
      return (g_OFFE<>'S')
    endwith
  endfunc

  add object oStr_2_34 as StdString with uid="QPALHKAJEE",Visible=.t., Left=49, Top=230,;
    Alignment=1, Width=122, Height=18,;
    Caption="Categoria cespite:"  ;
  , bGlobalFont=.t.

  func oStr_2_34.mHide()
    with this.Parent.oContained
      return (.w_ARFLCESP<>'S')
    endwith
  endfunc

  add object oStr_2_36 as StdString with uid="NVIRRERPAB",Visible=.t., Left=26, Top=407,;
    Alignment=1, Width=145, Height=18,;
    Caption="Web Application:"  ;
  , bGlobalFont=.t.

  func oStr_2_36.mHide()
    with this.Parent.oContained
      return (g_IZCP='N' and g_CPIN#'S' And g_REVICRM<>'S')
    endwith
  endfunc

  add object oStr_2_37 as StdString with uid="BBZSGFKIBJ",Visible=.t., Left=38, Top=259,;
    Alignment=1, Width=133, Height=18,;
    Caption="Tipo contributo:"  ;
  , bGlobalFont=.t.

  func oStr_2_37.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  add object oStr_2_40 as StdString with uid="GPMNXDHWWA",Visible=.t., Left=7, Top=288,;
    Alignment=1, Width=164, Height=18,;
    Caption="Qt� min. vendibile/ordinabile:"  ;
  , bGlobalFont=.t.

  func oStr_2_40.mHide()
    with this.Parent.oContained
      return (.w_ARTIPART $  'FO-DE')
    endwith
  endfunc

  add object oStr_2_41 as StdString with uid="XQRKIKRCPA",Visible=.t., Left=7, Top=439,;
    Alignment=0, Width=214, Height=18,;
    Caption="Informazioni per fatturazione elettronica"  ;
  , bGlobalFont=.t.

  add object oStr_2_46 as StdString with uid="YTFHXMDYIQ",Visible=.t., Left=59, Top=468,;
    Alignment=1, Width=112, Height=18,;
    Caption="Codice tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="CJHWPVYXQL",Visible=.t., Left=59, Top=498,;
    Alignment=1, Width=112, Height=18,;
    Caption="Codice valore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_48 as StdString with uid="BRQAAFGCCM",Visible=.t., Left=464, Top=469,;
    Alignment=1, Width=137, Height=18,;
    Caption="Classificazione FE:"  ;
  , bGlobalFont=.t.

  add object oBox_2_42 as StdBox with uid="SJYDLIWYJC",left=8, top=457, width=649,height=2
enddefine
define class tgsma_aasPag3 as StdContainer
  Width  = 694
  height = 570
  stdWidth  = 694
  stdheight = 570
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_3_3 as stdDynamicChildContainer with uid="AJFPMSPOZX",left=64, top=68, width=530, height=423, bOnScreen=.t.;


  add object oStr_3_6 as StdString with uid="CRYQVUJHGC",Visible=.t., Left=35, Top=42,;
    Alignment=0, Width=191, Height=18,;
    Caption="Selezione attributi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_7 as StdBox with uid="GJJJEAUAYD",left=9, top=58, width=678,height=2
enddefine
define class tgsma_aasPag4 as StdContainer
  Width  = 694
  height = 570
  stdWidth  = 694
  stdheight = 570
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_4_1 as StdField with uid="ZZRIMMSTLS",rtseq=133,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 138209242,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=58, Top=8, InputMask=replicate('X',20)

  func oCODI_4_1.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oDESC_4_2 as StdField with uid="HWIJGHODQA",rtseq=134,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 138543562,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=245, Top=8, InputMask=replicate('X',40)

  func oDESC_4_2.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc


  add object oLinkPC_4_4 as stdDynamicChildContainer with uid="AETDVLLLTI",left=43, top=33, width=648, height=534, bOnScreen=.t.;
    , tabstop=.f., Caption='\<Tariffe'

  func oLinkPC_4_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (IsAlt() AND NOT EMPTY(.w_ARCODART))
      endwith
    endif
  endfunc

  func oLinkPC_4_4.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!isAlt())
     endwith
    endif
  endfunc

  add object oStr_4_3 as StdString with uid="TVOOOYOUOF",Visible=.t., Left=3, Top=8,;
    Alignment=1, Width=53, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_4_3.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsma_mtt",lower(this.oContained.gsma_mtt.class))=0
        this.oContained.gsma_mtt.createrealchild()
        this.oContained.WriteTo_gsma_mtt()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsma_aasPag5 as StdContainer
  Width  = 694
  height = 570
  stdWidth  = 694
  stdheight = 570
  resizeXpos=663
  resizeYpos=366
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_5_1 as StdField with uid="MWVNNJIKGF",rtseq=135,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 138209242,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=58, Top=20, InputMask=replicate('X',20)

  add object oDESC_5_2 as StdField with uid="APLBKAOBCH",rtseq=136,rtrep=.f.,;
    cFormVar = "w_DESC", cQueryName = "DESC",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 138543562,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=245, Top=20, InputMask=replicate('X',40)


  add object oLinkPC_5_4 as stdDynamicChildContainer with uid="BIUQXZHFCF",left=6, top=56, width=677, height=400, bOnScreen=.t.;


  add object oStr_5_3 as StdString with uid="LEWWLXUNRM",Visible=.t., Left=3, Top=20,;
    Alignment=1, Width=53, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="ARTIPART<>'PF'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".ARCODART=ART_ICOL.ARCODART";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsma_aas','ART_ICOL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ARCODART=ART_ICOL.ARCODART";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
