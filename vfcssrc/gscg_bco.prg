* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bco                                                        *
*              Check sui saldi conti                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_4]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2000-01-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bco",oParentObject)
return(i_retval)

define class tgscg_bco as StdBatch
  * --- Local variables
  w_MESS = space(50)
  w_OK = .f.
  w_COND = .f.
  w_oMess = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messaggio di avviso se cancello un conto movimentato (da GSCG_ASP, GSCG_ASC, GSCG_ASF)
    this.w_oMess=createobject("Ah_Message")
    this.w_OK = .T.
    this.w_COND = this.oParentObject.w_SLDARINI <>0 OR this.oParentObject.w_SLDARFIN<>0 OR this.oParentObject.w_SLDARPRO<>0 OR this.oParentObject.w_SLDARPER<>0
    this.w_COND = this.w_COND OR this.oParentObject.w_SLAVEINI <>0 OR this.oParentObject.w_SLAVEFIN<>0 OR this.oParentObject.w_SLAVEPRO<>0 OR this.oParentObject.w_SLAVEPER<>0
    if this.w_COND=.T.
      do case
        case this.oParentObject.w_SLTIPCON="G"
          this.w_oMess.AddMsgPartNL("Attenzione: conto movimentato")     
        case this.oParentObject.w_SLTIPCON="C"
          this.w_oMess.AddMsgPartNL("Attenzione: cliente movimentato")     
        case this.oParentObject.w_SLTIPCON="F"
          this.w_oMess.AddMsgPartNL("Attenzione: fornitore movimentato")     
      endcase
      this.w_oMess.AddMsgPartNL("Cancellare comunque?")     
      this.w_OK = this.w_oMess.ah_YesNo()
    endif
    if this.w_OK=.F.
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=MSG_TRANSACTION_ERROR
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
