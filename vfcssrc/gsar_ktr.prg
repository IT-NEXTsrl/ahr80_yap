* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ktr                                                        *
*              Tabella ritenute                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_1]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-11-08                                                      *
* Last revis.: 2007-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_ktr",oParentObject))

* --- Class definition
define class tgsar_ktr as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 441
  Height = 312
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-10-18"
  HelpContextID=82409321
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsar_ktr"
  cComment = "Tabella ritenute"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPRIT = space(1)
  o_TIPRIT = space(1)
  w_CODAZI = space(10)
  o_CODAZI = space(10)
  w_RAGAZI = space(40)

  * --- Children pointers
  GSAR_MTB = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSAR_MTB additive
    with this
      .Pages(1).addobject("oPag","tgsar_ktrPag1","gsar_ktr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSAR_MTB
    * --- Area Manuale = Init Page Frame
    * --- gsar_ktr
    * --- Imposta il Titolo della Finestra della Gestione in Base al tipo
    WITH THIS.PARENT
       DO CASE
             CASE Oparentobject = 'A'
                   .cComment = CP_TRANSLATE('Tabella ritenute operate')
             CASE Oparentobject = 'V'
                   .cComment = CP_TRANSLATE('Tabella ritenute subite')
       ENDCASE
    ENDWIT
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSAR_MTB = CREATEOBJECT('stdDynamicChild',this,'GSAR_MTB',this.oPgFrm.Page1.oPag.oLinkPC_1_3)
    this.GSAR_MTB.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MTB)
      this.GSAR_MTB.DestroyChildrenChain()
      this.GSAR_MTB=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_3')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MTB.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MTB.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MTB.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAR_MTB.SetKey(;
            .w_CODAZI,"TRCODAZI";
            ,.w_TIPRIT,"TRTIPRIT";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAR_MTB.ChangeRow(this.cRowID+'      1',1;
             ,.w_CODAZI,"TRCODAZI";
             ,.w_TIPRIT,"TRTIPRIT";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAR_MTB)
        i_f=.GSAR_MTB.BuildFilter()
        if !(i_f==.GSAR_MTB.cQueryFilter)
          i_fnidx=.GSAR_MTB.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MTB.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MTB.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MTB.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MTB.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSAR_MTB(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSAR_MTB.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSAR_MTB(i_ask)
    if this.GSAR_MTB.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (Tabella ritenute)'))
      cp_BeginTrs()
      this.GSAR_MTB.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPRIT=space(1)
      .w_CODAZI=space(10)
      .w_RAGAZI=space(40)
        .w_TIPRIT = this.oParentObject
        .w_CODAZI = i_CODAZI
      .GSAR_MTB.NewDocument()
      .GSAR_MTB.ChangeRow('1',1,.w_CODAZI,"TRCODAZI",.w_TIPRIT,"TRTIPRIT")
      if not(.GSAR_MTB.bLoaded)
        .GSAR_MTB.SetKey(.w_CODAZI,"TRCODAZI",.w_TIPRIT,"TRTIPRIT")
      endif
        .w_RAGAZI = g_RAGAZI
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSAR_MTB.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MTB.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TIPRIT = this.oParentObject
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_CODAZI<>.o_CODAZI or .w_TIPRIT<>.o_TIPRIT
          .Save_GSAR_MTB(.t.)
          .GSAR_MTB.NewDocument()
          .GSAR_MTB.ChangeRow('1',1,.w_CODAZI,"TRCODAZI",.w_TIPRIT,"TRTIPRIT")
          if not(.GSAR_MTB.bLoaded)
            .GSAR_MTB.SetKey(.w_CODAZI,"TRCODAZI",.w_TIPRIT,"TRTIPRIT")
          endif
        endif
      endwith
      this.DoRTCalc(2,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODAZI_1_2.value==this.w_CODAZI)
      this.oPgFrm.Page1.oPag.oCODAZI_1_2.value=this.w_CODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_5.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_5.value=this.w_RAGAZI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSAR_MTB.CheckForm()
      if i_bres
        i_bres=  .GSAR_MTB.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPRIT = this.w_TIPRIT
    this.o_CODAZI = this.w_CODAZI
    * --- GSAR_MTB : Depends On
    this.GSAR_MTB.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsar_ktrPag1 as StdContainer
  Width  = 437
  height = 312
  stdWidth  = 437
  stdheight = 312
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODAZI_1_2 as StdField with uid="WBABVOXBTY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODAZI", cQueryName = "CODAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 100918234,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=69, Top=3, InputMask=replicate('X',10)


  add object oLinkPC_1_3 as stdDynamicChildContainer with uid="AGYBPUKOTM",left=6, top=28, width=422, height=278, bOnScreen=.t.;


  add object oRAGAZI_1_5 as StdField with uid="AIHAGSHJLM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 100909290,;
   bGlobalFont=.t.,;
    Height=21, Width=271, Left=155, Top=3, InputMask=replicate('X',40)

  add object oStr_1_4 as StdString with uid="SRSKPBIUPH",Visible=.t., Left=8, Top=7,;
    Alignment=1, Width=56, Height=18,;
    Caption="Azienda:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ktr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
