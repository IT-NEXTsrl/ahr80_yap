* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_sco                                                        *
*              Carica/salva dati esterni                                       *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-14                                                      *
* Last revis.: 2016-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_sco
*Dichiaro alcune variabili globali relativi a moduli che non sono presenti in tutti prodotti
*(AHE/AHR/AETOP) per ovviare ad errori in esecuzione della vqr associata alla combobox 'Archivio'
if ISAHR() or ISALT()
     *Manca il modulo remote Banking
     public g_REBA
     g_REBA='N'
     *Manca il modulo Work FLow
     public g_WFLD
     g_WFLD='N' 
     *Manca il modulo Bilancio consolidato
     public g_BCAN, g_BCON
     g_BCAN='N' 
     g_BCON='N'     
endif
if ISAHE()
     *Manca il modulo remote Banking
     public g_SOLL
     g_SOLL='N'
endif
*Assegno il codice in base all'applicazione in uso
public CODAPP
CODAPP=iif(ISALT(),'%AET%',iif(ISAHR(),'%AHR%','%AHE%'))
* --- Fine Area Manuale
return(createobject("tgsut_sco",oParentObject))

* --- Class definition
define class tgsut_sco as StdForm
  Top    = 32
  Left   = 35

  * --- Standard Properties
  Width  = 576
  Height = 202+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-17"
  HelpContextID=177650839
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  CONTROPA_IDX = 0
  COD_IMP_IDX = 0
  PAR_ALTE_IDX = 0
  cPrg = "gsut_sco"
  cComment = "Carica/salva dati esterni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_COPATHDE = space(200)
  w_TIPARC = space(2)
  o_TIPARC = space(2)
  w_STANDARD = space(1)
  w_TIPMDF = space(2)
  w_TIPMDF = space(2)
  w_NOTE = space(0)
  w_PATH = space(200)
  o_PATH = space(200)
  w_ABICABOBSO = space(1)
  w_FLGANGRP = space(1)
  w_FILENAME = space(50)
  w_PROMOD = space(20)
  w_LPATH = .F.
  w_COD_AZI = space(5)
  o_COD_AZI = space(5)
  w_DesPreRagg = space(1)
  w_TIPARC = space(2)
  w_TIPPROP = space(1)
  w_TIPMDF = space(2)
  w_TIPMDF = space(2)
  w_NOTE = space(0)
  w_PATH = space(200)
  w_RESET = space(1)
  w_EKCHKFIL = space(1)
  w_ABICABOBSO = space(1)
  w_EKCHKLOG = space(1)
  w_GAUPDATE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_scoPag1","gsut_sco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Caricamento")
      .Pages(2).addobject("oPag","tgsut_scoPag2","gsut_sco",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Salvataggio")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPARC_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsut_sco
    * Spostamento maschera per evitare visualizzazione con g_Scheduler attiva
    if VARTYPE(g_SCHEDULER)='C' AND g_SCHEDULER='S'
       this.parent.left=_screen.width+1
    endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='COD_IMP'
    this.cWorkTables[3]='PAR_ALTE'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_COPATHDE=space(200)
      .w_TIPARC=space(2)
      .w_STANDARD=space(1)
      .w_TIPMDF=space(2)
      .w_TIPMDF=space(2)
      .w_NOTE=space(0)
      .w_PATH=space(200)
      .w_ABICABOBSO=space(1)
      .w_FLGANGRP=space(1)
      .w_FILENAME=space(50)
      .w_PROMOD=space(20)
      .w_LPATH=.f.
      .w_COD_AZI=space(5)
      .w_DesPreRagg=space(1)
      .w_TIPARC=space(2)
      .w_TIPPROP=space(1)
      .w_TIPMDF=space(2)
      .w_TIPMDF=space(2)
      .w_NOTE=space(0)
      .w_PATH=space(200)
      .w_RESET=space(1)
      .w_EKCHKFIL=space(1)
      .w_ABICABOBSO=space(1)
      .w_EKCHKLOG=space(1)
      .w_GAUPDATE=space(1)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_2('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_TIPARC = iif(ISALT(),'MG','NM')
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_TIPARC))
          .link_1_5('Full')
        endif
        .w_STANDARD = 'T'
        .w_TIPMDF = 'TR'
        .w_TIPMDF = 'TR'
          .DoRTCalc(7,7,.f.)
        .w_PATH = FILE_TO_BE_LOADED( .w_TIPARC, .w_TIPMDF, .w_FILENAME)
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .w_ABICABOBSO = 'S'
        .w_FLGANGRP = iif(.w_TIPARC='GI','G','A')
          .DoRTCalc(11,13,.f.)
        .w_COD_AZI = i_codazi
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_COD_AZI))
          .link_1_25('Full')
        endif
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_TIPARC))
          .link_2_2('Full')
        endif
        .w_TIPPROP = 'S'
        .w_TIPMDF = 'TR'
        .w_TIPMDF = 'TR'
          .DoRTCalc(20,20,.f.)
        .w_PATH = FILE_TO_BE_LOADED( .w_TIPARC, .w_TIPMDF, .w_FILENAME)
      .oPgFrm.Page2.oPag.oObj_2_11.Calculate()
        .w_RESET = 'S'
        .w_EKCHKFIL = 'S'
        .w_ABICABOBSO = 'S'
        .w_EKCHKLOG = 'N'
        .w_GAUPDATE = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_18.enabled = this.oPgFrm.Page2.oPag.oBtn_2_18.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_19.enabled = this.oPgFrm.Page2.oPag.oBtn_2_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_2('Full')
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .DoRTCalc(2,9,.t.)
        if .o_TIPARC<>.w_TIPARC
            .w_FLGANGRP = iif(.w_TIPARC='GI','G','A')
        endif
        if .o_PATH<>.w_PATH
          .Calculate_LMZUISUYVI()
        endif
        .DoRTCalc(11,13,.t.)
        if .o_COD_AZI<>.w_COD_AZI
            .w_COD_AZI = i_codazi
          .link_1_25('Full')
        endif
        .DoRTCalc(15,15,.t.)
        if .o_TIPARC<>.w_TIPARC
          .link_2_2('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_2_11.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_11.Calculate()
    endwith
  return

  proc Calculate_FLRPBMVPMR()
    with this
          * --- Carica valori combo
          GSUT_BGS(this;
             )
    endwith
  endproc
  proc Calculate_KYCTVAZQVD()
    with this
          * --- Calcolo il nome corretto del file nel caso di moduli
          GSUT_BFS(this;
             )
    endwith
  endproc
  proc Calculate_VTZQGUFNKU()
    with this
          * --- Seleziona il file da importare
          .w_PATH = FILE_TO_BE_LOADED( .w_TIPARC, .w_TIPMDF, .w_FILENAME )
    endwith
  endproc
  proc Calculate_LMZUISUYVI()
    with this
          * --- Controllo path
          .w_LPATH = chknfile(.w_PATH,'F')
    endwith
  endproc
  proc Calculate_JCOUBHWRNJ()
    with this
          * --- Seleziona il file da importare
          .w_PATH = FILE_TO_BE_LOADED( .w_TIPARC, .w_TIPMDF, .w_FILENAME )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_18.enabled = this.oPgFrm.Page2.oPag.oBtn_2_18.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPMDF_1_7.visible=!this.oPgFrm.Page1.oPag.oTIPMDF_1_7.mHide()
    this.oPgFrm.Page1.oPag.oTIPMDF_1_8.visible=!this.oPgFrm.Page1.oPag.oTIPMDF_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oABICABOBSO_1_14.visible=!this.oPgFrm.Page1.oPag.oABICABOBSO_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page2.oPag.oTIPPROP_2_3.visible=!this.oPgFrm.Page2.oPag.oTIPPROP_2_3.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_5.visible=!this.oPgFrm.Page2.oPag.oStr_2_5.mHide()
    this.oPgFrm.Page2.oPag.oTIPMDF_2_6.visible=!this.oPgFrm.Page2.oPag.oTIPMDF_2_6.mHide()
    this.oPgFrm.Page2.oPag.oTIPMDF_2_7.visible=!this.oPgFrm.Page2.oPag.oTIPMDF_2_7.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_12.visible=!this.oPgFrm.Page2.oPag.oStr_2_12.mHide()
    this.oPgFrm.Page2.oPag.oRESET_2_13.visible=!this.oPgFrm.Page2.oPag.oRESET_2_13.mHide()
    this.oPgFrm.Page2.oPag.oEKCHKFIL_2_14.visible=!this.oPgFrm.Page2.oPag.oEKCHKFIL_2_14.mHide()
    this.oPgFrm.Page2.oPag.oABICABOBSO_2_15.visible=!this.oPgFrm.Page2.oPag.oABICABOBSO_2_15.mHide()
    this.oPgFrm.Page2.oPag.oEKCHKLOG_2_16.visible=!this.oPgFrm.Page2.oPag.oEKCHKLOG_2_16.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_17.visible=!this.oPgFrm.Page2.oPag.oStr_2_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oGAUPDATE_1_28.visible=!this.oPgFrm.Page1.oPag.oGAUPDATE_1_28.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_FLRPBMVPMR()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("w_TIPARC Changed")
          .Calculate_KYCTVAZQVD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("w_FILENAME Changed") or lower(cEvent)==lower("w_TIPARC Changed") or lower(cEvent)==lower("w_TIPMDF Changed")
          .Calculate_VTZQGUFNKU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("w_FILENAME Changed") or lower(cEvent)==lower("w_TIPARC Changed") or lower(cEvent)==lower("w_TIPMDF Changed")
          .Calculate_JCOUBHWRNJ()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_sco
    if cevent = 'Done' and (ISAHR() or ISALT())  
        *Resetto le variabili relative ai moduli Remote Banking e Work FLow
         RELEASE g_REBA,g_WFLD,CODAPP
    endif
    if cevent = 'Done' and isahe()
        *Resetto le variabili relative ai moduli Remote Banking 
         RELEASE g_SOLL
     endif 
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COPATHDE";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COPATHDE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_COPATHDE = NVL(_Link_.COPATHDE,space(200))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_COPATHDE = space(200)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPARC
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_IMP_IDX,3]
    i_lTable = "COD_IMP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_IMP_IDX,2], .t., this.COD_IMP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_IMP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPARC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'COD_IMP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CICODIMP like "+cp_ToStrODBC(trim(this.w_TIPARC)+"%");

          i_ret=cp_SQL(i_nConn,"select CICODIMP,CIPROMOD";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CICODIMP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CICODIMP',trim(this.w_TIPARC))
          select CICODIMP,CIPROMOD;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CICODIMP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPARC)==trim(_Link_.CICODIMP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPARC) and !this.bDontReportError
            deferred_cp_zoom('COD_IMP','*','CICODIMP',cp_AbsName(oSource.parent,'oTIPARC_1_5'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CICODIMP,CIPROMOD";
                     +" from "+i_cTable+" "+i_lTable+" where CICODIMP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CICODIMP',oSource.xKey(1))
            select CICODIMP,CIPROMOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPARC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CICODIMP,CIPROMOD";
                   +" from "+i_cTable+" "+i_lTable+" where CICODIMP="+cp_ToStrODBC(this.w_TIPARC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CICODIMP',this.w_TIPARC)
            select CICODIMP,CIPROMOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPARC = NVL(_Link_.CICODIMP,space(2))
      this.w_PROMOD = NVL(_Link_.CIPROMOD,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_TIPARC = space(2)
      endif
      this.w_PROMOD = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_IMP_IDX,2])+'\'+cp_ToStr(_Link_.CICODIMP,1)
      cp_ShowWarn(i_cKey,this.COD_IMP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPARC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COD_AZI
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_AZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_AZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAFLDESP";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_COD_AZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_COD_AZI)
            select PACODAZI,PAFLDESP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_AZI = NVL(_Link_.PACODAZI,space(5))
      this.w_DesPreRagg = NVL(_Link_.PAFLDESP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COD_AZI = space(5)
      endif
      this.w_DesPreRagg = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_AZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPARC
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_IMP_IDX,3]
    i_lTable = "COD_IMP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_IMP_IDX,2], .t., this.COD_IMP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_IMP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPARC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'COD_IMP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CICODIMP like "+cp_ToStrODBC(trim(this.w_TIPARC)+"%");

          i_ret=cp_SQL(i_nConn,"select CICODIMP,CIPROMOD";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CICODIMP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CICODIMP',trim(this.w_TIPARC))
          select CICODIMP,CIPROMOD;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CICODIMP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPARC)==trim(_Link_.CICODIMP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPARC) and !this.bDontReportError
            deferred_cp_zoom('COD_IMP','*','CICODIMP',cp_AbsName(oSource.parent,'oTIPARC_2_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CICODIMP,CIPROMOD";
                     +" from "+i_cTable+" "+i_lTable+" where CICODIMP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CICODIMP',oSource.xKey(1))
            select CICODIMP,CIPROMOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPARC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CICODIMP,CIPROMOD";
                   +" from "+i_cTable+" "+i_lTable+" where CICODIMP="+cp_ToStrODBC(this.w_TIPARC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CICODIMP',this.w_TIPARC)
            select CICODIMP,CIPROMOD;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPARC = NVL(_Link_.CICODIMP,space(2))
      this.w_PROMOD = NVL(_Link_.CIPROMOD,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_TIPARC = space(2)
      endif
      this.w_PROMOD = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_IMP_IDX,2])+'\'+cp_ToStr(_Link_.CICODIMP,1)
      cp_ShowWarn(i_cKey,this.COD_IMP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPARC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPARC_1_5.RadioValue()==this.w_TIPARC)
      this.oPgFrm.Page1.oPag.oTIPARC_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPMDF_1_7.RadioValue()==this.w_TIPMDF)
      this.oPgFrm.Page1.oPag.oTIPMDF_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPMDF_1_8.RadioValue()==this.w_TIPMDF)
      this.oPgFrm.Page1.oPag.oTIPMDF_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_9.value==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_9.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oPATH_1_11.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_11.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oABICABOBSO_1_14.RadioValue()==this.w_ABICABOBSO)
      this.oPgFrm.Page1.oPag.oABICABOBSO_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPARC_2_2.RadioValue()==this.w_TIPARC)
      this.oPgFrm.Page2.oPag.oTIPARC_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPPROP_2_3.RadioValue()==this.w_TIPPROP)
      this.oPgFrm.Page2.oPag.oTIPPROP_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPMDF_2_6.RadioValue()==this.w_TIPMDF)
      this.oPgFrm.Page2.oPag.oTIPMDF_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPMDF_2_7.RadioValue()==this.w_TIPMDF)
      this.oPgFrm.Page2.oPag.oTIPMDF_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTE_2_8.value==this.w_NOTE)
      this.oPgFrm.Page2.oPag.oNOTE_2_8.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oPATH_2_9.value==this.w_PATH)
      this.oPgFrm.Page2.oPag.oPATH_2_9.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page2.oPag.oRESET_2_13.RadioValue()==this.w_RESET)
      this.oPgFrm.Page2.oPag.oRESET_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEKCHKFIL_2_14.RadioValue()==this.w_EKCHKFIL)
      this.oPgFrm.Page2.oPag.oEKCHKFIL_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oABICABOBSO_2_15.RadioValue()==this.w_ABICABOBSO)
      this.oPgFrm.Page2.oPag.oABICABOBSO_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEKCHKLOG_2_16.RadioValue()==this.w_EKCHKLOG)
      this.oPgFrm.Page2.oPag.oEKCHKLOG_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGAUPDATE_1_28.RadioValue()==this.w_GAUPDATE)
      this.oPgFrm.Page1.oPag.oGAUPDATE_1_28.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPARC = this.w_TIPARC
    this.o_PATH = this.w_PATH
    this.o_COD_AZI = this.w_COD_AZI
    return

enddefine

* --- Define pages as container
define class tgsut_scoPag1 as StdContainer
  Width  = 572
  height = 202
  stdWidth  = 572
  stdheight = 202
  resizeXpos=239
  resizeYpos=67
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPARC_1_5 as StdTableCombo with uid="YSNRRMIBZP",rtseq=3,rtrep=.f.,left=87,top=12,width=210,height=21;
    , ToolTipText = "Tipo archivio da elaborare";
    , HelpContextID = 218297546;
    , cFormVar="w_TIPARC",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="COD_IMP";
    , cTable='QUERY\GSUT3SCO.vqr',cKey='CICODIMP',cValue='CIDESCOD',cOrderBy='CIDESCOD',xDefault=space(2);
  , bGlobalFont=.t.


  func oTIPARC_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPARC_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oTIPMDF_1_7 as StdCombo with uid="DCOZLNLAIG",rtseq=5,rtrep=.f.,left=393,top=12,width=173,height=21;
    , ToolTipText = "Archivio F24 da elaborare";
    , HelpContextID = 181859530;
    , cFormVar="w_TIPMDF",RowSource=""+"Codici tributo,"+"Regioni e prov.auton,"+"Enti/comuni,"+"Causali cont.INPS,"+"Sedi INAIL,"+"Altri enti previdenza,"+"Causali altri enti,"+"Sedi INPS", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPMDF_1_7.RadioValue()
    return(iif(this.value =1,'TR',;
    iif(this.value =2,'RP',;
    iif(this.value =3,'EL',;
    iif(this.value =4,'CI',;
    iif(this.value =5,'SI',;
    iif(this.value =6,'AE',;
    iif(this.value =7,'CA',;
    iif(this.value =8,'SP',;
    space(2))))))))))
  endfunc
  func oTIPMDF_1_7.GetRadio()
    this.Parent.oContained.w_TIPMDF = this.RadioValue()
    return .t.
  endfunc

  func oTIPMDF_1_7.SetRadio()
    this.Parent.oContained.w_TIPMDF=trim(this.Parent.oContained.w_TIPMDF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPMDF=='TR',1,;
      iif(this.Parent.oContained.w_TIPMDF=='RP',2,;
      iif(this.Parent.oContained.w_TIPMDF=='EL',3,;
      iif(this.Parent.oContained.w_TIPMDF=='CI',4,;
      iif(this.Parent.oContained.w_TIPMDF=='SI',5,;
      iif(this.Parent.oContained.w_TIPMDF=='AE',6,;
      iif(this.Parent.oContained.w_TIPMDF=='CA',7,;
      iif(this.Parent.oContained.w_TIPMDF=='SP',8,;
      0))))))))
  endfunc

  func oTIPMDF_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'TF' OR IsAlt())
    endwith
  endfunc


  add object oTIPMDF_1_8 as StdCombo with uid="LOFTWVOYYV",rtseq=6,rtrep=.f.,left=393,top=12,width=173,height=21;
    , ToolTipText = "Archivio F24 da elaborare";
    , HelpContextID = 181859530;
    , cFormVar="w_TIPMDF",RowSource=""+"Codici tributo F24,"+"Regioni e prov.auton,"+"Enti/comuni,"+"Causali cont.INPS,"+"Sedi INAIL,"+"Altri enti previdenza,"+"Causali altri enti,"+"Sedi INPS,"+"Contributo unificato,"+"Codici tributo F23,"+"Codici contenzioso,"+"Causali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPMDF_1_8.RadioValue()
    return(iif(this.value =1,'TR',;
    iif(this.value =2,'RP',;
    iif(this.value =3,'EL',;
    iif(this.value =4,'CI',;
    iif(this.value =5,'SI',;
    iif(this.value =6,'AE',;
    iif(this.value =7,'CA',;
    iif(this.value =8,'SP',;
    iif(this.value =9,'CU',;
    iif(this.value =10,'CT',;
    iif(this.value =11,'CC',;
    iif(this.value =12,'CF',;
    space(2))))))))))))))
  endfunc
  func oTIPMDF_1_8.GetRadio()
    this.Parent.oContained.w_TIPMDF = this.RadioValue()
    return .t.
  endfunc

  func oTIPMDF_1_8.SetRadio()
    this.Parent.oContained.w_TIPMDF=trim(this.Parent.oContained.w_TIPMDF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPMDF=='TR',1,;
      iif(this.Parent.oContained.w_TIPMDF=='RP',2,;
      iif(this.Parent.oContained.w_TIPMDF=='EL',3,;
      iif(this.Parent.oContained.w_TIPMDF=='CI',4,;
      iif(this.Parent.oContained.w_TIPMDF=='SI',5,;
      iif(this.Parent.oContained.w_TIPMDF=='AE',6,;
      iif(this.Parent.oContained.w_TIPMDF=='CA',7,;
      iif(this.Parent.oContained.w_TIPMDF=='SP',8,;
      iif(this.Parent.oContained.w_TIPMDF=='CU',9,;
      iif(this.Parent.oContained.w_TIPMDF=='CT',10,;
      iif(this.Parent.oContained.w_TIPMDF=='CC',11,;
      iif(this.Parent.oContained.w_TIPMDF=='CF',12,;
      0))))))))))))
  endfunc

  func oTIPMDF_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'TF' OR !IsAlt())
    endwith
  endfunc

  add object oNOTE_1_9 as StdMemo with uid="LVCUDEDSWI",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 182538454,;
   bGlobalFont=.t.,;
    Height=75, Width=478, Left=87, Top=38, readonly=.t.

  add object oPATH_1_11 as StdField with uid="YJTBDAQENK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Inserisci il path+nome file",;
    HelpContextID = 182731510,;
   bGlobalFont=.t.,;
    Height=21, Width=455, Left=87, Top=117, InputMask=replicate('X',200)

  proc oPATH_1_11.mAfter
    with this.Parent.oContained
      .bdontreporterror=.T.
    endwith
  endproc


  add object oObj_1_13 as cp_askfile with uid="OBMZWDHYCM",left=547, top=119, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var='w_PATH',;
    nPag=1;
    , ToolTipText = "Premere per selezionare un file";
    , HelpContextID = 177851862

  add object oABICABOBSO_1_14 as StdCheck with uid="ZLGBVUANVX",rtseq=9,rtrep=.f.,left=87, top=147, caption="Obsolescenza",;
    ToolTipText = "Se attivo, in fase di importazione dati, rendere obsoleti i codici non presenti nel file di testo.",;
    HelpContextID = 252778632,;
    cFormVar="w_ABICABOBSO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oABICABOBSO_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oABICABOBSO_1_14.GetRadio()
    this.Parent.oContained.w_ABICABOBSO = this.RadioValue()
    return .t.
  endfunc

  func oABICABOBSO_1_14.SetRadio()
    this.Parent.oContained.w_ABICABOBSO=trim(this.Parent.oContained.w_ABICABOBSO)
    this.value = ;
      iif(this.Parent.oContained.w_ABICABOBSO=='S',1,;
      0)
  endfunc

  func oABICABOBSO_1_14.mHide()
    with this.Parent.oContained
      return (Not ( .w_TIPARC='AB' Or .w_TIPARC='CA' ))
    endwith
  endfunc


  add object oBtn_1_16 as StdButton with uid="FCTKAGOXAT",left=467, top=154, width=48,height=45,;
    CpPicture="BMP\carica.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare da un file di testo il contenuto della tabella selezionata";
    , HelpContextID = 231259610;
    , caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSUT_BCO(this.Parent.oContained,"CARICA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PATH) AND NOT EMPTY(.w_TIPARC))
      endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="ZSONRLTWLJ",left=518, top=154, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 184968262;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oGAUPDATE_1_28 as StdCheck with uid="KJIAWLFZBA",rtseq=26,rtrep=.f.,left=87, top=147, caption="Aggiorna esistenti",;
    ToolTipText = "Se attivo la procedura aggiunger� i nuovi gadget e modificher� gli esistenti a parit� di nome, modello, flag 'Aziendale' e campo 'Esclusivo per'",;
    HelpContextID = 265530709,;
    cFormVar="w_GAUPDATE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGAUPDATE_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGAUPDATE_1_28.GetRadio()
    this.Parent.oContained.w_GAUPDATE = this.RadioValue()
    return .t.
  endfunc

  func oGAUPDATE_1_28.SetRadio()
    this.Parent.oContained.w_GAUPDATE=trim(this.Parent.oContained.w_GAUPDATE)
    this.value = ;
      iif(this.Parent.oContained.w_GAUPDATE=='S',1,;
      0)
  endfunc

  func oGAUPDATE_1_28.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'GG' )
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="SHCDKROQHP",Visible=.t., Left=8, Top=119,;
    Alignment=1, Width=76, Height=18,;
    Caption="Percorso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="UOQLUQMRXZ",Visible=.t., Left=33, Top=15,;
    Alignment=1, Width=51, Height=15,;
    Caption="Archivio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="XUKXKOQXII",Visible=.t., Left=311, Top=12,;
    Alignment=1, Width=79, Height=15,;
    Caption="Tabelle F24:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'TF' OR IsAlt())
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="JOOMQJZPVQ",Visible=.t., Left=297, Top=12,;
    Alignment=1, Width=93, Height=18,;
    Caption="Tabelle F23-F24:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'TF' OR !IsAlt())
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="IQZNHJGHBX",Visible=.t., Left=-32, Top=522,;
    Alignment=0, Width=706, Height=18,;
    Caption="Se modificata la sequenza sul campo TIPARC in prima o seconda pagina ricordarsi di modificare la routine Gsut_Bgs"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc
enddefine
define class tgsut_scoPag2 as StdContainer
  Width  = 572
  height = 202
  stdWidth  = 572
  stdheight = 202
  resizeXpos=251
  resizeYpos=73
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPARC_2_2 as StdTableCombo with uid="EJGIISRFMX",rtseq=16,rtrep=.f.,left=87,top=12,width=210,height=21;
    , ToolTipText = "Tipo archivio da elaborare";
    , HelpContextID = 218297546;
    , cFormVar="w_TIPARC",tablefilter="", bObbl = .f. , nPag = 2;
    , cLinkFile="COD_IMP";
    , cTable='QUERY\GSUT3SCO.vqr',cKey='CICODIMP',cValue='CIDESCOD',cOrderBy='CIDESCOD',xDefault=space(2);
  , bGlobalFont=.t.


  func oTIPARC_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPARC_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oTIPPROP_2_3 as StdCombo with uid="RNGIHJDZKI",rtseq=17,rtrep=.f.,left=394,top=12,width=173,height=21;
    , ToolTipText = "Modalit� di esportazione delle propriet� del gadget";
    , HelpContextID = 15987914;
    , cFormVar="w_TIPPROP",RowSource=""+"Solo propriet� standard,"+"Standard e personalizzate", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPPROP_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oTIPPROP_2_3.GetRadio()
    this.Parent.oContained.w_TIPPROP = this.RadioValue()
    return .t.
  endfunc

  func oTIPPROP_2_3.SetRadio()
    this.Parent.oContained.w_TIPPROP=trim(this.Parent.oContained.w_TIPPROP)
    this.value = ;
      iif(this.Parent.oContained.w_TIPPROP=='S',1,;
      iif(this.Parent.oContained.w_TIPPROP=='T',2,;
      0))
  endfunc

  func oTIPPROP_2_3.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'GG' )
    endwith
  endfunc


  add object oTIPMDF_2_6 as StdCombo with uid="GBDFPMRUDP",rtseq=18,rtrep=.f.,left=394,top=12,width=173,height=21;
    , ToolTipText = "Archivio F24 da elaborare";
    , HelpContextID = 181859530;
    , cFormVar="w_TIPMDF",RowSource=""+"Codici tributo,"+"Regioni e prov.auton,"+"Enti/comuni,"+"Causali cont.INPS,"+"Sedi INAIL,"+"Altri enti previdenza,"+"Causali altri enti,"+"Sedi INPS", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPMDF_2_6.RadioValue()
    return(iif(this.value =1,'TR',;
    iif(this.value =2,'RP',;
    iif(this.value =3,'EL',;
    iif(this.value =4,'CI',;
    iif(this.value =5,'SI',;
    iif(this.value =6,'AE',;
    iif(this.value =7,'CA',;
    iif(this.value =8,'SP',;
    space(2))))))))))
  endfunc
  func oTIPMDF_2_6.GetRadio()
    this.Parent.oContained.w_TIPMDF = this.RadioValue()
    return .t.
  endfunc

  func oTIPMDF_2_6.SetRadio()
    this.Parent.oContained.w_TIPMDF=trim(this.Parent.oContained.w_TIPMDF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPMDF=='TR',1,;
      iif(this.Parent.oContained.w_TIPMDF=='RP',2,;
      iif(this.Parent.oContained.w_TIPMDF=='EL',3,;
      iif(this.Parent.oContained.w_TIPMDF=='CI',4,;
      iif(this.Parent.oContained.w_TIPMDF=='SI',5,;
      iif(this.Parent.oContained.w_TIPMDF=='AE',6,;
      iif(this.Parent.oContained.w_TIPMDF=='CA',7,;
      iif(this.Parent.oContained.w_TIPMDF=='SP',8,;
      0))))))))
  endfunc

  func oTIPMDF_2_6.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'TF' OR IsAlt())
    endwith
  endfunc


  add object oTIPMDF_2_7 as StdCombo with uid="BXKPXFVRSO",rtseq=19,rtrep=.f.,left=394,top=12,width=173,height=21;
    , ToolTipText = "Archivio F24 da elaborare";
    , HelpContextID = 181859530;
    , cFormVar="w_TIPMDF",RowSource=""+"Codici tributo F24,"+"Regioni e prov.auton,"+"Enti/comuni,"+"Causali cont.INPS,"+"Sedi INAIL,"+"Altri enti previdenza,"+"Causali altri enti,"+"Sedi INPS,"+"Contributo unificato,"+"Codici tributo F23,"+"Codici contenzioso,"+"Causali", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPMDF_2_7.RadioValue()
    return(iif(this.value =1,'TR',;
    iif(this.value =2,'RP',;
    iif(this.value =3,'EL',;
    iif(this.value =4,'CI',;
    iif(this.value =5,'SI',;
    iif(this.value =6,'AE',;
    iif(this.value =7,'CA',;
    iif(this.value =8,'SP',;
    iif(this.value =9,'CU',;
    iif(this.value =10,'CT',;
    iif(this.value =11,'CC',;
    iif(this.value =12,'CF',;
    space(2))))))))))))))
  endfunc
  func oTIPMDF_2_7.GetRadio()
    this.Parent.oContained.w_TIPMDF = this.RadioValue()
    return .t.
  endfunc

  func oTIPMDF_2_7.SetRadio()
    this.Parent.oContained.w_TIPMDF=trim(this.Parent.oContained.w_TIPMDF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPMDF=='TR',1,;
      iif(this.Parent.oContained.w_TIPMDF=='RP',2,;
      iif(this.Parent.oContained.w_TIPMDF=='EL',3,;
      iif(this.Parent.oContained.w_TIPMDF=='CI',4,;
      iif(this.Parent.oContained.w_TIPMDF=='SI',5,;
      iif(this.Parent.oContained.w_TIPMDF=='AE',6,;
      iif(this.Parent.oContained.w_TIPMDF=='CA',7,;
      iif(this.Parent.oContained.w_TIPMDF=='SP',8,;
      iif(this.Parent.oContained.w_TIPMDF=='CU',9,;
      iif(this.Parent.oContained.w_TIPMDF=='CT',10,;
      iif(this.Parent.oContained.w_TIPMDF=='CC',11,;
      iif(this.Parent.oContained.w_TIPMDF=='CF',12,;
      0))))))))))))
  endfunc

  func oTIPMDF_2_7.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'TF' OR !IsAlt())
    endwith
  endfunc

  add object oNOTE_2_8 as StdMemo with uid="BHWMJVPSDF",rtseq=20,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 182538454,;
   bGlobalFont=.t.,;
    Height=75, Width=478, Left=87, Top=38, readonly=.t.

  add object oPATH_2_9 as StdField with uid="VALJUDMYRA",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 2, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Inserisci il path+nome file",;
    HelpContextID = 182731510,;
   bGlobalFont=.t.,;
    Height=21, Width=455, Left=87, Top=117, InputMask=replicate('X',200)

  proc oPATH_2_9.mAfter
    with this.Parent.oContained
      .bdontreporterror=.T.
    endwith
  endproc


  add object oObj_2_11 as cp_askfile with uid="AMINSJBJQG",left=547, top=119, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var='w_PATH',;
    nPag=2;
    , ToolTipText = "Premere per selezionare un file";
    , HelpContextID = 177851862

  add object oRESET_2_13 as StdCheck with uid="ENXAWBSDHP",rtseq=22,rtrep=.f.,left=87, top=147, caption="Reset valori",;
    ToolTipText = "Se attivo, in fase di esportazione dati, sbianca i valori delle propriet� per cui � attivo il relativo check reset",;
    HelpContextID = 2176790,;
    cFormVar="w_RESET", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oRESET_2_13.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oRESET_2_13.GetRadio()
    this.Parent.oContained.w_RESET = this.RadioValue()
    return .t.
  endfunc

  func oRESET_2_13.SetRadio()
    this.Parent.oContained.w_RESET=trim(this.Parent.oContained.w_RESET)
    this.value = ;
      iif(this.Parent.oContained.w_RESET=='S',1,;
      0)
  endfunc

  func oRESET_2_13.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'GG' )
    endwith
  endfunc

  add object oEKCHKFIL_2_14 as StdCheck with uid="FYMTAOTPLP",rtseq=23,rtrep=.f.,left=87, top=147, caption="Esporta filtro visualizzazione risultati",;
    ToolTipText = "Se attivo riporta anche i dati relativi al filtro su gruppo/utente che potr� visualizzare i risultati dell'elaborazione",;
    HelpContextID = 93535378,;
    cFormVar="w_EKCHKFIL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oEKCHKFIL_2_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oEKCHKFIL_2_14.GetRadio()
    this.Parent.oContained.w_EKCHKFIL = this.RadioValue()
    return .t.
  endfunc

  func oEKCHKFIL_2_14.SetRadio()
    this.Parent.oContained.w_EKCHKFIL=trim(this.Parent.oContained.w_EKCHKFIL)
    this.value = ;
      iif(this.Parent.oContained.w_EKCHKFIL=='S',1,;
      0)
  endfunc

  func oEKCHKFIL_2_14.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'EK' )
    endwith
  endfunc

  add object oABICABOBSO_2_15 as StdCheck with uid="YNSMOVZTPZ",rtseq=24,rtrep=.f.,left=87, top=167, caption="Obsolescenza",;
    ToolTipText = "Se attivo, in fase di esportazione, esclude i codici obsoleti alla data odierna.",;
    HelpContextID = 252778632,;
    cFormVar="w_ABICABOBSO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oABICABOBSO_2_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oABICABOBSO_2_15.GetRadio()
    this.Parent.oContained.w_ABICABOBSO = this.RadioValue()
    return .t.
  endfunc

  func oABICABOBSO_2_15.SetRadio()
    this.Parent.oContained.w_ABICABOBSO=trim(this.Parent.oContained.w_ABICABOBSO)
    this.value = ;
      iif(this.Parent.oContained.w_ABICABOBSO=='S',1,;
      0)
  endfunc

  func oABICABOBSO_2_15.mHide()
    with this.Parent.oContained
      return (Not ( .w_TIPARC='AB' Or .w_TIPARC='CA' ))
    endwith
  endfunc

  add object oEKCHKLOG_2_16 as StdCheck with uid="NNJLXKSSOA",rtseq=25,rtrep=.f.,left=87, top=167, caption="Esporta attivazione Log",;
    ToolTipText = "Se attivo esporta anche il valore del check relativo al log (di default viene disattivato)",;
    HelpContextID = 74236787,;
    cFormVar="w_EKCHKLOG", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oEKCHKLOG_2_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oEKCHKLOG_2_16.GetRadio()
    this.Parent.oContained.w_EKCHKLOG = this.RadioValue()
    return .t.
  endfunc

  func oEKCHKLOG_2_16.SetRadio()
    this.Parent.oContained.w_EKCHKLOG=trim(this.Parent.oContained.w_EKCHKLOG)
    this.value = ;
      iif(this.Parent.oContained.w_EKCHKLOG=='S',1,;
      0)
  endfunc

  func oEKCHKLOG_2_16.mHide()
    with this.Parent.oContained
      return ( .w_TIPARC<>'EK')
    endwith
  endfunc


  add object oBtn_2_18 as StdButton with uid="SVOYBCFHDT",left=467, top=154, width=48,height=45,;
    CpPicture="BMP\save.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per salvare su un file di testo il contenuto della tabella selezionata";
    , HelpContextID = 19129126;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_18.Click()
      with this.Parent.oContained
        GSUT_BCO(this.Parent.oContained,"SALVA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_PATH) AND NOT EMPTY(.w_TIPARC) and .w_TIPARC<>'DP' )
      endwith
    endif
  endfunc


  add object oBtn_2_19 as StdButton with uid="RXLXIZEEUB",left=518, top=154, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 184968262;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_1 as StdString with uid="NHRFGMWFMU",Visible=.t., Left=8, Top=119,;
    Alignment=1, Width=76, Height=18,;
    Caption="Percorso:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="HFQOUTQWPS",Visible=.t., Left=33, Top=15,;
    Alignment=1, Width=51, Height=15,;
    Caption="Archivio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="TZVLJRCAEX",Visible=.t., Left=311, Top=12,;
    Alignment=1, Width=79, Height=18,;
    Caption="Tabelle F24:"  ;
  , bGlobalFont=.t.

  func oStr_2_5.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'TF' OR IsAlt())
    endwith
  endfunc

  add object oStr_2_12 as StdString with uid="KRYYBHKQAT",Visible=.t., Left=297, Top=12,;
    Alignment=1, Width=93, Height=18,;
    Caption="Tabelle F23-F24:"  ;
  , bGlobalFont=.t.

  func oStr_2_12.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'TF' OR !IsAlt())
    endwith
  endfunc

  add object oStr_2_17 as StdString with uid="RQCXHTOPGH",Visible=.t., Left=297, Top=12,;
    Alignment=1, Width=93, Height=18,;
    Caption="Modalit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_17.mHide()
    with this.Parent.oContained
      return (.w_TIPARC<>'GG' )
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_sco','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_sco
FUNCTION FILE_TO_BE_LOADED( pARCHIVE,pTIPMDF,pFILE)
  local L_DIRECTORY,L_FILE,L_ESTENSIONE
  *Calcolo del percorso di ricerca del file
  L_DIRECTORY=IIF(ISAHE(),ADDBS(SYS(5)+strtran(SYS(2003),'EXE',''))+"DOC\DATI_ESTERNI\",ADDBS(SYS(5)+SYS(2003))+"FILES_X_IMPORT\")
  L_FILE=pFILE  
  do case
    case pARCHIVE = 'TF'
        * Tabelle F24/F23
        do case
          case pTIPMDF = 'TR'
           * Codici tributo
           L_FILE = "CodTributo.TXT"
          case pTIPMDF = 'RP'
           * Regioni e prov.auton
           L_FILE = "Regioni.TXT"
          case pTIPMDF = 'EL'
           * Enti/comuni
           L_FILE = "Enticomuni.txt" 
          case pTIPMDF = 'CI'
           * Causali cont.INPS
           L_FILE ="Causinps.txt" 
          case pTIPMDF = 'SI'
           * Sedi INAIL
           L_FILE = "sedi_inail.txt" 
          case pTIPMDF = 'AE'
           * Altri enti previdenziali
           L_FILE = "AltriEnti.txt" 
          case pTIPMDF = 'CA'
           * Causali altri enti
           L_FILE = "CausAltriEnti.TXT" 
          case pTIPMDF = 'SP'
           * Sedi INPS
           L_FILE = "sedi_INPS.txt" 
          case pTIPMDF = 'CU'
           *Contributo unificato
           L_FILE = 'CONTRIBUTI_UNIFICATI.txt'
          case pTIPMDF = 'CT'
           *Codici trbuto F23
           L_FILE = 'CODICI_TRIBUTO_F23.txt'
          case pTIPMDF = 'CC'
           *Codici contenzioso
           L_FILE = 'CODICI_CONTENZIOSO.txt'
          case pTIPMDF = 'CF'
           *Causali
           L_FILE = 'CAUSALIF23.txt'
        endcase
    case pARCHIVE = 'WF'
         L_DIRECTORY = ADDBS(SYS(5)+strtran(SYS(2003),'EXE',''))+ "WFLD\DOC\"
    case ISAHR() and g_REVI='S' and (pARCHIVE = 'CD' or pARCHIVE = 'PD') 
         L_DIRECTORY = ADDBS(SYS(5)+strtran(SYS(2003),'EXE',''))+ "REVI\EXE\FILES_X_IMPORT\"
    case ISAHE() and (pARCHIVE = 'CD' or pARCHIVE = 'PD') 
         L_DIRECTORY = ADDBS(SYS(5)+strtran(SYS(2003),'EXE',''))+ "DOCM\DOC\"
    case ISAHE() and (pARCHIVE = 'GR' or pARCHIVE = 'RE')
         L_DIRECTORY = ADDBS(SYS(5)+strtran(SYS(2003),'EXE',''))+ "INFO\DOC\"
    case pARCHIVE = 'CR' or pARCHIVE = 'AI'
         L_DIRECTORY = IIF(ISAHE(),ADDBS(SYS(5)+strtran(SYS(2003),'EXE',''))+ "REBA\DOC\",ADDBS(SYS(5)+(SYS(2003)))+ "FILES_X_IMPORT\")
    case ISAHE() and (pARCHIVE = 'IR' or pARCHIVE = 'GI')
         L_DIRECTORY = ADDBS(SYS(5)+strtran(SYS(2003),'EXE',''))+ "IRDR\DOC\"
    case ISAHE() and pARCHIVE = 'GS'
         L_DIRECTORY = ADDBS(SYS(5)+strtran(SYS(2003),'EXE',''))+ "JBSH\DOC\"
    case ISAHE() and pARCHIVE = 'RB'
         L_DIRECTORY = ADDBS(SYS(5)+strtran(SYS(2003),'EXE',''))+ "BILC\DOC\"
    case pARCHIVE = 'PR' or pARCHIVE = 'PO'
         L_DIRECTORY = ADDBS(SYS(5)+strtran(SYS(2003),'EXE',''))+ "LESP\DOC\"     
    case ISAHE() and pARCHIVE = 'DP'
         L_DIRECTORY = ADDBS(SYS(5)+strtran(SYS(2003),'EXE',''))+ "DOC\PROGRESSIVI\"
    case ISAHE() and pARCHIVE = 'MO'
         L_DIRECTORY = ADDBS(SYS(5)+strtran(SYS(2003),'EXE',''))+ "OFFE\DOC\"    
  Endcase  
  RETURN L_DIRECTORY + alltrim(L_FILE)
ENDFUNC
* --- Fine Area Manuale
