* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_adi                                                        *
*              Manutenzione distinte                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_512]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-31                                                      *
* Last revis.: 2015-10-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_adi"))

* --- Class definition
define class tgste_adi as StdForm
  Top    = 2
  Left   = 9

  * --- Standard Properties
  Width  = 859
  Height = 418+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-26"
  HelpContextID=92943465
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=99

  * --- Constant Properties
  DIS_TINT_IDX = 0
  CAU_DIST_IDX = 0
  VALUTE_IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  ESERCIZI_IDX = 0
  COC_MAST_IDX = 0
  BAN_CHE_IDX = 0
  BAN_CONTI_IDX = 0
  TIP_DIST_IDX = 0
  MASTRI_IDX = 0
  ZONE_IDX = 0
  CATECOMM_IDX = 0
  CPUSERS_IDX = 0
  cFile = "DIS_TINT"
  cKeySelect = "DINUMDIS"
  cKeyWhere  = "DINUMDIS=this.w_DINUMDIS"
  cKeyWhereODBC = '"DINUMDIS="+cp_ToStrODBC(this.w_DINUMDIS)';

  cKeyWhereODBCqualified = '"DIS_TINT.DINUMDIS="+cp_ToStrODBC(this.w_DINUMDIS)';

  cPrg = "gste_adi"
  cComment = "Manutenzione distinte"
  icon = "anag.ico"
  cAutoZoom = 'GSTE0ADI'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_DINUMDIS = space(10)
  w_VALNAZ = space(3)
  w_TIPCC = space(1)
  w_DIRIFCON = space(10)
  w_DATOBSO = ctod('  /  /  ')
  w_DI__ANNO = space(4)
  w_DINUMERO = 0
  w_DIDATDIS = ctod('  /  /  ')
  o_DIDATDIS = ctod('  /  /  ')
  w_CODISO = space(3)
  w_DICODESE = space(4)
  w_OBTEST = ctod('  /  /  ')
  w_DITIPDIS = space(2)
  o_DITIPDIS = space(2)
  w_CATIPDIS = space(2)
  w_DICAURIF = space(5)
  o_DICAURIF = space(5)
  w_CATIPMAN = space(1)
  w_DICAUBON = space(15)
  w_DITIPMAN = space(1)
  w_FLCONT = space(1)
  w_DESCAR = space(35)
  w_IMPCOM = 0
  w_DICOSEFF = 0
  w_FLAVVI = space(1)
  w_CONSBF = space(1)
  w_DITIPSCA = space(1)
  o_DITIPSCA = space(1)
  w_DIBANRIF = space(15)
  o_DIBANRIF = space(15)
  w_CONSBF1 = space(1)
  w_FLIBAN = space(1)
  w_FLCABI = space(1)
  w_DICOSCOM = 0
  w_DIFLDEFI = space(1)
  w_FLRD = space(2)
  w_FLRI = space(2)
  w_FLMA = space(2)
  w_FLRB = space(2)
  w_FLBO = space(2)
  w_FLCA = space(2)
  w_DISCAINI = ctod('  /  /  ')
  w_DISCAFIN = ctod('  /  /  ')
  w_DIDATVAL = ctod('  /  /  ')
  o_DIDATVAL = ctod('  /  /  ')
  w_DIDATCON = ctod('  /  /  ')
  w_DICODVAL = space(3)
  o_DICODVAL = space(3)
  w_SIMVAL = space(5)
  w_TESVAL = 0
  w_DECTOT = 0
  o_DECTOT = 0
  w_DESAPP = space(35)
  w_CALCPICT = 0
  w_DICAOVAL = 0
  w_CONTAB = space(1)
  w_TIPREG = space(1)
  w_DESBAN = space(42)
  w_TIPSOT = space(1)
  w_NOCVS = space(1)
  w_FASE1 = 0
  w_SIMNAZ = space(5)
  w_TIPCON = space(1)
  w_FLBANC = space(1)
  w_FLSELE = 0
  w_DICODCAU = space(5)
  o_DICODCAU = space(5)
  w_DESCAU = space(35)
  w_DIDESCRI = space(35)
  w_DISCOTRA = 0
  w_DISCACES = 0
  w_DIFLAVVI = space(1)
  w_DIDATAVV = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_CODABI = space(5)
  w_FASE = 0
  w_IBAN = space(35)
  w_BBAN = space(30)
  w_BATIPO = space(1)
  w_FLORI = .F.
  w_COMMIS = 0
  w_SIMNAZ1 = space(5)
  w_BACKCOLOR = 0
  w_TEXTCOLOR = 0
  w_CAPTION = space(10)
  w_MESS = space(10)
  w_DATDOCINI = ctod('  /  /  ')
  w_DATDOCFIN = ctod('  /  /  ')
  w_TEST1 = .F.
  w_NUMDOCINI = 0
  w_NUMDOCFIN = 0
  w_ALFDOCINI = space(10)
  w_ALFDOCFIN = space(10)
  w_CATCOM = space(3)
  w_CODZON = space(3)
  w_CONSUP = space(15)
  w_CDESCRI = space(35)
  w_ZDESCRI = space(35)
  w_MDESCRI = space(40)
  w_DINSCFRB = space(20)
  w_DI__PATH = space(254)
  w_DATEFF = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_HTML_URL = space(10)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_DINUMDIS = this.W_DINUMDIS
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_DI__ANNO = this.W_DI__ANNO
  op_DINUMERO = this.W_DINUMERO

  * --- Children pointers
  GSTE_MPD = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gste_adi
  * Utilizzato come chiave per legare il dettaglio
  * dei record di PAR_TITE legate alla distinta
  w_ROWORDP=-2
  
  Proc ecpDelete()
  this.gste_mpd.opGFRM.page1.opAG.obody.oFIRSTCONTROL.SetFocus()
   DoDefault()
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DIS_TINT','gste_adi')
    stdPageFrame::Init()
    *set procedure to GSTE_MPD additive
    with this
      .Pages(1).addobject("oPag","tgste_adiPag1","gste_adi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Distinta")
      .Pages(1).HelpContextID = 8296041
      .Pages(2).addobject("oPag","tgste_adiPag2","gste_adi",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati aggiuntivi")
      .Pages(2).HelpContextID = 84816595
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSTE_MPD
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='CAU_DIST'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='COC_MAST'
    this.cWorkTables[7]='BAN_CHE'
    this.cWorkTables[8]='BAN_CONTI'
    this.cWorkTables[9]='TIP_DIST'
    this.cWorkTables[10]='MASTRI'
    this.cWorkTables[11]='ZONE'
    this.cWorkTables[12]='CATECOMM'
    this.cWorkTables[13]='CPUSERS'
    this.cWorkTables[14]='DIS_TINT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(14))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIS_TINT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIS_TINT_IDX,3]
  return

  function CreateChildren()
    this.GSTE_MPD = CREATEOBJECT('stdDynamicChild',this,'GSTE_MPD',this.oPgFrm.Page1.oPag.oLinkPC_1_67)
    this.GSTE_MPD.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSTE_MPD)
      this.GSTE_MPD.DestroyChildrenChain()
      this.GSTE_MPD=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_67')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSTE_MPD.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSTE_MPD.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSTE_MPD.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSTE_MPD.SetKey(;
            .w_DINUMDIS,"PTSERIAL";
            ,.w_ROWORDP,"PTROWORD";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSTE_MPD.ChangeRow(this.cRowID+'      1',1;
             ,.w_DINUMDIS,"PTSERIAL";
             ,.w_ROWORDP,"PTROWORD";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSTE_MPD)
        i_f=.GSTE_MPD.BuildFilter()
        if !(i_f==.GSTE_MPD.cQueryFilter)
          i_fnidx=.GSTE_MPD.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSTE_MPD.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSTE_MPD.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSTE_MPD.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSTE_MPD.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DINUMDIS = NVL(DINUMDIS,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_27_joined
    link_1_27_joined=.f.
    local link_1_43_joined
    link_1_43_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DIS_TINT where DINUMDIS=KeySet.DINUMDIS
    *
    i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIS_TINT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIS_TINT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIS_TINT '
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_27_joined=this.AddJoinedLink_1_27(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_43_joined=this.AddJoinedLink_1_43(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DINUMDIS',this.w_DINUMDIS  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_VALNAZ = g_PERVAL
        .w_DATOBSO = ctod("  /  /  ")
        .w_CODISO = g_ISONAZ
        .w_CATIPMAN = space(1)
        .w_FLCONT = space(1)
        .w_DESCAR = space(35)
        .w_IMPCOM = 0
        .w_FLAVVI = space(1)
        .w_CONSBF = space(1)
        .w_CONSBF1 = space(1)
        .w_FLIBAN = space(1)
        .w_FLRD = space(2)
        .w_FLRI = space(2)
        .w_FLMA = space(2)
        .w_FLRB = space(2)
        .w_FLBO = space(2)
        .w_FLCA = space(2)
        .w_SIMVAL = space(5)
        .w_TESVAL = 0
        .w_DECTOT = 0
        .w_DESAPP = space(35)
        .w_CONTAB = space(1)
        .w_TIPREG = space(1)
        .w_DESBAN = space(42)
        .w_TIPSOT = space(1)
        .w_NOCVS = space(1)
        .w_FASE1 = 0
        .w_TIPCON = space(1)
        .w_FLBANC = space(1)
        .w_FLSELE = 0
        .w_DESCAU = space(35)
        .w_DTOBSO = ctod("  /  /  ")
        .w_CODABI = space(5)
        .w_FASE = 0
        .w_IBAN = space(35)
        .w_BBAN = space(30)
        .w_BATIPO = space(1)
        .w_MESS = space(10)
        .w_DATDOCINI = ctod("  /  /  ")
        .w_DATDOCFIN = ctod("  /  /  ")
        .w_TEST1 = .F.
        .w_NUMDOCINI = 0
        .w_NUMDOCFIN = 0
        .w_ALFDOCINI = space(10)
        .w_ALFDOCFIN = space(10)
        .w_CATCOM = space(3)
        .w_CODZON = space(3)
        .w_CONSUP = space(15)
        .w_CDESCRI = space(35)
        .w_ZDESCRI = space(35)
        .w_MDESCRI = space(40)
        .w_DATEFF = space(1)
        .w_CODAZI = i_CODAZI
        .w_DINUMDIS = NVL(DINUMDIS,space(10))
        .op_DINUMDIS = .w_DINUMDIS
        .w_TIPCC = 'G'
        .w_DIRIFCON = NVL(DIRIFCON,space(10))
        .w_DI__ANNO = NVL(DI__ANNO,space(4))
        .op_DI__ANNO = .w_DI__ANNO
        .w_DINUMERO = NVL(DINUMERO,0)
        .op_DINUMERO = .w_DINUMERO
        .w_DIDATDIS = NVL(cp_ToDate(DIDATDIS),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .w_DICODESE = NVL(DICODESE,space(4))
          * evitabile
          *.link_1_12('Load')
        .w_OBTEST = .w_DIDATDIS
        .w_DITIPDIS = NVL(DITIPDIS,space(2))
        .w_CATIPDIS = .w_DITIPDIS
        .w_DICAURIF = NVL(DICAURIF,space(5))
          if link_1_16_joined
            this.w_DICAURIF = NVL(CACODICE116,NVL(this.w_DICAURIF,space(5)))
            this.w_DESCAR = NVL(CADESCAU116,space(35))
            this.w_DITIPSCA = NVL(CATIPSCD116,space(1))
            this.w_DITIPDIS = NVL(CATIPDIS116,space(2))
            this.w_DICODVAL = NVL(CACODVAD116,space(3))
            this.w_DICODCAU = NVL(CACODCAU116,space(5))
            this.w_FLCA = NVL(CADIS_CA116,space(2))
            this.w_FLRD = NVL(CADIS_RD116,space(2))
            this.w_FLRI = NVL(CADIS_RI116,space(2))
            this.w_FLBO = NVL(CADIS_BO116,space(2))
            this.w_FLMA = NVL(CADIS_MA116,space(2))
            this.w_FLRB = NVL(CADIS_RB116,space(2))
            this.w_IMPCOM = NVL(CAIMPCOM116,0)
            this.w_FLAVVI = NVL(CAFLAVVI116,space(1))
            this.w_TIPCON = NVL(CATIPCON116,space(1))
            this.w_FLBANC = NVL(CAFLBANC116,space(1))
            this.w_FLIBAN = NVL(CAFLIBAN116,space(1))
            this.w_CONSBF = NVL(CACONSBF116,space(1))
            this.w_FLCONT = NVL(CAFLCONT116,space(1))
            this.w_DICAUBON = NVL(CACAUBON116,space(15))
            this.w_DATEFF = NVL(CADATEFF116,space(1))
            this.w_NOCVS = NVL(CA_NOCVS116,space(1))
            this.w_CATIPMAN = NVL(CATIPMAN116,space(1))
          else
          .link_1_16('Load')
          endif
        .w_DICAUBON = NVL(DICAUBON,space(15))
          * evitabile
          *.link_1_18('Load')
        .w_DITIPMAN = NVL(DITIPMAN,space(1))
        .w_DICOSEFF = NVL(DICOSEFF,0)
        .w_DITIPSCA = NVL(DITIPSCA,space(1))
        .w_DIBANRIF = NVL(DIBANRIF,space(15))
          if link_1_27_joined
            this.w_DIBANRIF = NVL(BACODBAN127,NVL(this.w_DIBANRIF,space(15)))
            this.w_DESBAN = NVL(BADESCRI127,space(42))
            this.w_DATOBSO = NVL(cp_ToDate(BADTOBSO127),ctod("  /  /  "))
            this.w_CODABI = NVL(BACODABI127,space(5))
            this.w_IBAN = NVL(BA__IBAN127,space(35))
            this.w_BBAN = NVL(BA__BBAN127,space(30))
            this.w_BATIPO = NVL(BATIPCON127,space(1))
            this.w_CONSBF1 = NVL(BACONSBF127,space(1))
          else
          .link_1_27('Load')
          endif
        .w_FLCABI = ' '
        .w_DICOSCOM = NVL(DICOSCOM,0)
        .w_DIFLDEFI = NVL(DIFLDEFI,space(1))
        .w_DISCAINI = NVL(cp_ToDate(DISCAINI),ctod("  /  /  "))
        .w_DISCAFIN = NVL(cp_ToDate(DISCAFIN),ctod("  /  /  "))
        .w_DIDATVAL = NVL(cp_ToDate(DIDATVAL),ctod("  /  /  "))
        .w_DIDATCON = NVL(cp_ToDate(DIDATCON),ctod("  /  /  "))
        .w_DICODVAL = NVL(DICODVAL,space(3))
          if link_1_43_joined
            this.w_DICODVAL = NVL(VACODVAL143,NVL(this.w_DICODVAL,space(3)))
            this.w_DESAPP = NVL(VADESVAL143,space(35))
            this.w_TESVAL = NVL(VACAOVAL143,0)
            this.w_SIMVAL = NVL(VASIMVAL143,space(5))
            this.w_DECTOT = NVL(VADECTOT143,0)
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO143),ctod("  /  /  "))
          else
          .link_1_43('Load')
          endif
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_DICAOVAL = NVL(DICAOVAL,0)
        .w_SIMNAZ = g_VALSIM
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate(IIF(.w_DITIPSCA='C', ah_MsgFormat("Attive"), IIF(.w_DITIPSCA='F', ah_MsgFormat("Passive"),'')),'','')
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate(FUNDIS(.w_DITIPDIS),'','')
        .w_DICODCAU = NVL(DICODCAU,space(5))
          if link_2_2_joined
            this.w_DICODCAU = NVL(CCCODICE202,NVL(this.w_DICODCAU,space(5)))
            this.w_DESCAU = NVL(CCDESCRI202,space(35))
            this.w_TIPREG = NVL(CCTIPREG202,space(1))
          else
          .link_2_2('Load')
          endif
        .w_DIDESCRI = NVL(DIDESCRI,space(35))
        .w_DISCOTRA = NVL(DISCOTRA,0)
        .w_DISCACES = NVL(DISCACES,0)
        .w_DIFLAVVI = NVL(DIFLAVVI,space(1))
        .w_DIDATAVV = NVL(cp_ToDate(DIDATAVV),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .w_FLORI = .T.
        .w_COMMIS = IIF(g_COGE='S',.w_DICOSCOM+.w_DICOSEFF,0)
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate(IIF(.w_FLRB='RB', 'R.B./RIBA','')+' ' +IIF(.w_FLRD='RD', 'Rim.Dir.','')+ ' '+IIF(.w_FLMA='MA', 'M.AV.','')+ ' '+ iif(.w_FLRI $ 'RI-SD', 'R.I.D.', '')+ ' '+ iif(.w_FLBO $ 'BO-SC-SE', 'Bonifico','')+ ' ' +IIF(.w_FLCA='CA', 'Cambiali', ''))
        .w_SIMNAZ1 = g_VALSIM
        .w_BACKCOLOR = RGB(252,199,186)
        .w_TEXTCOLOR = RGB(252,199,186)
        .w_CAPTION = ''
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate(.w_CAPTION,.w_BACKCOLOR,.w_TEXTCOLOR)
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
          .link_2_23('Load')
          .link_2_24('Load')
          .link_2_25('Load')
        .w_DINSCFRB = NVL(DINSCFRB,space(20))
        .w_DI__PATH = NVL(DI__PATH,space(254))
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_109.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_110.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_HTML_URL = .w_DI__PATH
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DIS_TINT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_41.enabled = this.oPgFrm.Page2.oPag.oBtn_2_41.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gste_adi
    **Aggiorna la propriet� di editabilit� dei bottoni del figlio
    this.GSTE_MPD.mEnableControlsFixed()
    * --- Se sono in prima pagina forzo il refresh, altrimenti
    * --- no perch� causerei lo spostamento indebito di pagina
    * --- se passo da elenco a seconda pagina con le frecce della
    * --- tastiera
    IF this.opgfrm.activepage=1
    * --- Sotto certe condizioni � possibile che il dettaglio
    * ----delle partite non sia refreshato (es. f12 cambio=1 f10 e scansione
    * ----dei record, le distinte i dettagli con + di una partita non sono
    * ---- visualizzati se l'utente non vi clicca sopra)
    this.GSTE_MPD.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_1.KeyPress(5,0)
    * ---- La set Focus su DINUMERO serve poich� la KeyPress qui sopra lascia
    * ---- il focus settato sulla Banca di presentazione. Questo campo � obbligatorio
    * ---- e non editabile appena si va in caricamento. Andrebbe in loop continuando
    * ---- ad emettere il messaggio: campo Obbligatorio
    LOCAL controllo
    controllo = this.GetCtrl('w_DINUMERO')
    controllo.SetFocus()
    
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_DICAUBON")
    CTRL_CONCON.Popola()
    * --- Utilizzo la mHideControls() per rendere visibile la combo
    * --- anche in interroga.
    this.mHideControls()
    Endif
    * --- Fine Area Manuale
    this.Calculate_WMZSPYMSYG()
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_DINUMDIS = space(10)
      .w_VALNAZ = space(3)
      .w_TIPCC = space(1)
      .w_DIRIFCON = space(10)
      .w_DATOBSO = ctod("  /  /  ")
      .w_DI__ANNO = space(4)
      .w_DINUMERO = 0
      .w_DIDATDIS = ctod("  /  /  ")
      .w_CODISO = space(3)
      .w_DICODESE = space(4)
      .w_OBTEST = ctod("  /  /  ")
      .w_DITIPDIS = space(2)
      .w_CATIPDIS = space(2)
      .w_DICAURIF = space(5)
      .w_CATIPMAN = space(1)
      .w_DICAUBON = space(15)
      .w_DITIPMAN = space(1)
      .w_FLCONT = space(1)
      .w_DESCAR = space(35)
      .w_IMPCOM = 0
      .w_DICOSEFF = 0
      .w_FLAVVI = space(1)
      .w_CONSBF = space(1)
      .w_DITIPSCA = space(1)
      .w_DIBANRIF = space(15)
      .w_CONSBF1 = space(1)
      .w_FLIBAN = space(1)
      .w_FLCABI = space(1)
      .w_DICOSCOM = 0
      .w_DIFLDEFI = space(1)
      .w_FLRD = space(2)
      .w_FLRI = space(2)
      .w_FLMA = space(2)
      .w_FLRB = space(2)
      .w_FLBO = space(2)
      .w_FLCA = space(2)
      .w_DISCAINI = ctod("  /  /  ")
      .w_DISCAFIN = ctod("  /  /  ")
      .w_DIDATVAL = ctod("  /  /  ")
      .w_DIDATCON = ctod("  /  /  ")
      .w_DICODVAL = space(3)
      .w_SIMVAL = space(5)
      .w_TESVAL = 0
      .w_DECTOT = 0
      .w_DESAPP = space(35)
      .w_CALCPICT = 0
      .w_DICAOVAL = 0
      .w_CONTAB = space(1)
      .w_TIPREG = space(1)
      .w_DESBAN = space(42)
      .w_TIPSOT = space(1)
      .w_NOCVS = space(1)
      .w_FASE1 = 0
      .w_SIMNAZ = space(5)
      .w_TIPCON = space(1)
      .w_FLBANC = space(1)
      .w_FLSELE = 0
      .w_DICODCAU = space(5)
      .w_DESCAU = space(35)
      .w_DIDESCRI = space(35)
      .w_DISCOTRA = 0
      .w_DISCACES = 0
      .w_DIFLAVVI = space(1)
      .w_DIDATAVV = ctod("  /  /  ")
      .w_DTOBSO = ctod("  /  /  ")
      .w_CODABI = space(5)
      .w_FASE = 0
      .w_IBAN = space(35)
      .w_BBAN = space(30)
      .w_BATIPO = space(1)
      .w_FLORI = .f.
      .w_COMMIS = 0
      .w_SIMNAZ1 = space(5)
      .w_BACKCOLOR = 0
      .w_TEXTCOLOR = 0
      .w_CAPTION = space(10)
      .w_MESS = space(10)
      .w_DATDOCINI = ctod("  /  /  ")
      .w_DATDOCFIN = ctod("  /  /  ")
      .w_TEST1 = .f.
      .w_NUMDOCINI = 0
      .w_NUMDOCFIN = 0
      .w_ALFDOCINI = space(10)
      .w_ALFDOCFIN = space(10)
      .w_CATCOM = space(3)
      .w_CODZON = space(3)
      .w_CONSUP = space(15)
      .w_CDESCRI = space(35)
      .w_ZDESCRI = space(35)
      .w_MDESCRI = space(40)
      .w_DINSCFRB = space(20)
      .w_DI__PATH = space(254)
      .w_DATEFF = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_HTML_URL = space(10)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
          .DoRTCalc(2,2,.f.)
        .w_VALNAZ = g_PERVAL
        .w_TIPCC = 'G'
          .DoRTCalc(5,6,.f.)
        .w_DI__ANNO = STR(YEAR(IIF(EMPTY(.w_DIDATDIS), i_datsys, .w_DIDATDIS)),4,0)
          .DoRTCalc(8,8,.f.)
        .w_DIDATDIS = i_datsys
        .w_CODISO = g_ISONAZ
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .w_DICODESE = CALCESER(.w_DIDATVAL,g_CODESE)
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_DICODESE))
          .link_1_12('Full')
          endif
        .w_OBTEST = .w_DIDATDIS
          .DoRTCalc(13,13,.f.)
        .w_CATIPDIS = .w_DITIPDIS
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_DICAURIF))
          .link_1_16('Full')
          endif
        .DoRTCalc(16,17,.f.)
          if not(empty(.w_DICAUBON))
          .link_1_18('Full')
          endif
        .w_DITIPMAN = iif(.w_FLRI='RI',.w_CATIPMAN,'T')
        .DoRTCalc(19,26,.f.)
          if not(empty(.w_DIBANRIF))
          .link_1_27('Full')
          endif
          .DoRTCalc(27,28,.f.)
        .w_FLCABI = ' '
        .w_DICOSCOM = IIF(g_COGE='S',.w_IMPCOM,0)
        .w_DIFLDEFI = 'N'
          .DoRTCalc(32,37,.f.)
        .w_DISCAINI = IIF(.cFunction='Load',.w_DIDATDIS,.w_DISCAFIN)
        .w_DISCAFIN = .w_DISCAINI+120
        .w_DIDATVAL = .w_DIDATDIS
        .w_DIDATCON = .w_DIDATDIS
        .DoRTCalc(42,42,.f.)
          if not(empty(.w_DICODVAL))
          .link_1_43('Full')
          endif
          .DoRTCalc(43,46,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_DICAOVAL = IIF(EMPTY(.w_DICODVAL) OR EMPTY(.w_DIDATVAL), 0, GETCAM(.w_DICODVAL, .w_DIDATVAL, 7))
          .DoRTCalc(49,53,.f.)
        .w_FASE1 = 0
        .w_SIMNAZ = g_VALSIM
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate(IIF(.w_DITIPSCA='C', ah_MsgFormat("Attive"), IIF(.w_DITIPSCA='F', ah_MsgFormat("Passive"),'')),'','')
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate(FUNDIS(.w_DITIPDIS),'','')
          .DoRTCalc(56,57,.f.)
        .w_FLSELE = 0
        .DoRTCalc(59,59,.f.)
          if not(empty(.w_DICODCAU))
          .link_2_2('Full')
          endif
          .DoRTCalc(60,61,.f.)
        .w_DISCOTRA = 0
          .DoRTCalc(63,63,.f.)
        .w_DIFLAVVI = .w_FLAVVI
          .DoRTCalc(65,67,.f.)
        .w_FASE = 0
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
          .DoRTCalc(69,71,.f.)
        .w_FLORI = .T.
        .w_COMMIS = IIF(g_COGE='S',.w_DICOSCOM+.w_DICOSEFF,0)
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate(IIF(.w_FLRB='RB', 'R.B./RIBA','')+' ' +IIF(.w_FLRD='RD', 'Rim.Dir.','')+ ' '+IIF(.w_FLMA='MA', 'M.AV.','')+ ' '+ iif(.w_FLRI $ 'RI-SD', 'R.I.D.', '')+ ' '+ iif(.w_FLBO $ 'BO-SC-SE', 'Bonifico','')+ ' ' +IIF(.w_FLCA='CA', 'Cambiali', ''))
        .w_SIMNAZ1 = g_VALSIM
        .w_BACKCOLOR = RGB(252,199,186)
        .w_TEXTCOLOR = RGB(252,199,186)
        .w_CAPTION = ''
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate(.w_CAPTION,.w_BACKCOLOR,.w_TEXTCOLOR)
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
          .DoRTCalc(78,80,.f.)
        .w_TEST1 = .F.
        .DoRTCalc(82,86,.f.)
          if not(empty(.w_CATCOM))
          .link_2_23('Full')
          endif
        .DoRTCalc(87,87,.f.)
          if not(empty(.w_CODZON))
          .link_2_24('Full')
          endif
        .DoRTCalc(88,88,.f.)
          if not(empty(.w_CONSUP))
          .link_2_25('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_109.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_110.Calculate()
          .DoRTCalc(89,98,.f.)
        .w_HTML_URL = .w_DI__PATH
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIS_TINT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_41.enabled = this.oPgFrm.Page2.oPag.oBtn_2_41.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gste_adi
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_DICAUBON")
    CTRL_CONCON.Popola()
    * --- Utilizzo la mHideControls() per rendere visibile la combo
    * --- anche in interroga.
    this.mHideControls()
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"PRDIS","i_codazi,w_DINUMDIS")
      cp_AskTableProg(this,i_nConn,"PNDIS","i_codazi,w_DI__ANNO,w_DINUMERO")
      .op_codazi = .w_codazi
      .op_DINUMDIS = .w_DINUMDIS
      .op_codazi = .w_codazi
      .op_DI__ANNO = .w_DI__ANNO
      .op_DINUMERO = .w_DINUMERO
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDINUMERO_1_8.enabled = i_bVal
      .Page1.oPag.oDIDATDIS_1_9.enabled = i_bVal
      .Page1.oPag.oDICODESE_1_12.enabled = i_bVal
      .Page1.oPag.oDICAURIF_1_16.enabled = i_bVal
      .Page1.oPag.oDICAUBON_1_18.enabled = i_bVal
      .Page1.oPag.oDITIPMAN_1_19.enabled = i_bVal
      .Page1.oPag.oDIBANRIF_1_27.enabled = i_bVal
      .Page1.oPag.oFLCABI_1_30.enabled = i_bVal
      .Page1.oPag.oDICOSCOM_1_31.enabled = i_bVal
      .Page1.oPag.oDISCAINI_1_39.enabled = i_bVal
      .Page1.oPag.oDISCAFIN_1_40.enabled = i_bVal
      .Page1.oPag.oDIDATVAL_1_41.enabled = i_bVal
      .Page1.oPag.oDIDATCON_1_42.enabled = i_bVal
      .Page1.oPag.oDICODVAL_1_43.enabled = i_bVal
      .Page1.oPag.oDICAOVAL_1_49.enabled = i_bVal
      .Page2.oPag.oDICODCAU_2_2.enabled = i_bVal
      .Page2.oPag.oDIDESCRI_2_4.enabled = i_bVal
      .Page2.oPag.oDISCOTRA_2_5.enabled = i_bVal
      .Page2.oPag.oDISCACES_2_6.enabled = i_bVal
      .Page2.oPag.oDIFLAVVI_2_7.enabled = i_bVal
      .Page2.oPag.oDIDATAVV_2_8.enabled = i_bVal
      .Page2.oPag.oDATDOCINI_2_15.enabled = i_bVal
      .Page2.oPag.oDATDOCFIN_2_16.enabled = i_bVal
      .Page2.oPag.oNUMDOCINI_2_19.enabled = i_bVal
      .Page2.oPag.oNUMDOCFIN_2_20.enabled = i_bVal
      .Page2.oPag.oALFDOCINI_2_21.enabled = i_bVal
      .Page2.oPag.oALFDOCFIN_2_22.enabled = i_bVal
      .Page2.oPag.oCATCOM_2_23.enabled = i_bVal
      .Page2.oPag.oCODZON_2_24.enabled = i_bVal
      .Page2.oPag.oCONSUP_2_25.enabled = i_bVal
      .Page1.oPag.oBtn_1_52.enabled = .Page1.oPag.oBtn_1_52.mCond()
      .Page1.oPag.oBtn_1_69.enabled = i_bVal
      .Page2.oPag.oBtn_2_41.enabled = .Page2.oPag.oBtn_2_41.mCond()
      .Page1.oPag.oObj_1_11.enabled = i_bVal
      .Page1.oPag.oObj_1_85.enabled = i_bVal
      .Page1.oPag.oObj_1_95.enabled = i_bVal
      .Page1.oPag.oObj_1_99.enabled = i_bVal
      .Page1.oPag.oObj_1_102.enabled = i_bVal
      .Page1.oPag.oObj_1_103.enabled = i_bVal
      .Page1.oPag.oObj_1_107.enabled = i_bVal
      .Page1.oPag.oObj_1_109.enabled = i_bVal
      .Page1.oPag.oObj_1_110.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oDINUMERO_1_8.enabled = .t.
        .Page1.oPag.oDIBANRIF_1_27.enabled = .t.
      endif
    endwith
    this.GSTE_MPD.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DIS_TINT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSTE_MPD.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DINUMDIS,"DINUMDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIRIFCON,"DIRIFCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DI__ANNO,"DI__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DINUMERO,"DINUMERO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATDIS,"DIDATDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODESE,"DICODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPDIS,"DITIPDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICAURIF,"DICAURIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICAUBON,"DICAUBON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPMAN,"DITIPMAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICOSEFF,"DICOSEFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPSCA,"DITIPSCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIBANRIF,"DIBANRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICOSCOM,"DICOSCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIFLDEFI,"DIFLDEFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DISCAINI,"DISCAINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DISCAFIN,"DISCAFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATVAL,"DIDATVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATCON,"DIDATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODVAL,"DICODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICAOVAL,"DICAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODCAU,"DICODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDESCRI,"DIDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DISCOTRA,"DISCOTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DISCACES,"DISCACES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIFLAVVI,"DIFLAVVI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIDATAVV,"DIDATAVV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DINSCFRB,"DINSCFRB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DI__PATH,"DI__PATH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
    i_lTable = "DIS_TINT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DIS_TINT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSTE_SDI with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DIS_TINT_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"PRDIS","i_codazi,w_DINUMDIS")
          cp_NextTableProg(this,i_nConn,"PNDIS","i_codazi,w_DI__ANNO,w_DINUMERO")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DIS_TINT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIS_TINT')
        i_extval=cp_InsertValODBCExtFlds(this,'DIS_TINT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DINUMDIS,DIRIFCON,DI__ANNO,DINUMERO,DIDATDIS"+;
                  ",DICODESE,DITIPDIS,DICAURIF,DICAUBON,DITIPMAN"+;
                  ",DICOSEFF,DITIPSCA,DIBANRIF,DICOSCOM,DIFLDEFI"+;
                  ",DISCAINI,DISCAFIN,DIDATVAL,DIDATCON,DICODVAL"+;
                  ",DICAOVAL,DICODCAU,DIDESCRI,DISCOTRA,DISCACES"+;
                  ",DIFLAVVI,DIDATAVV,DINSCFRB,DI__PATH,UTCC"+;
                  ",UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DINUMDIS)+;
                  ","+cp_ToStrODBC(this.w_DIRIFCON)+;
                  ","+cp_ToStrODBC(this.w_DI__ANNO)+;
                  ","+cp_ToStrODBC(this.w_DINUMERO)+;
                  ","+cp_ToStrODBC(this.w_DIDATDIS)+;
                  ","+cp_ToStrODBCNull(this.w_DICODESE)+;
                  ","+cp_ToStrODBC(this.w_DITIPDIS)+;
                  ","+cp_ToStrODBCNull(this.w_DICAURIF)+;
                  ","+cp_ToStrODBCNull(this.w_DICAUBON)+;
                  ","+cp_ToStrODBC(this.w_DITIPMAN)+;
                  ","+cp_ToStrODBC(this.w_DICOSEFF)+;
                  ","+cp_ToStrODBC(this.w_DITIPSCA)+;
                  ","+cp_ToStrODBCNull(this.w_DIBANRIF)+;
                  ","+cp_ToStrODBC(this.w_DICOSCOM)+;
                  ","+cp_ToStrODBC(this.w_DIFLDEFI)+;
                  ","+cp_ToStrODBC(this.w_DISCAINI)+;
                  ","+cp_ToStrODBC(this.w_DISCAFIN)+;
                  ","+cp_ToStrODBC(this.w_DIDATVAL)+;
                  ","+cp_ToStrODBC(this.w_DIDATCON)+;
                  ","+cp_ToStrODBCNull(this.w_DICODVAL)+;
                  ","+cp_ToStrODBC(this.w_DICAOVAL)+;
                  ","+cp_ToStrODBCNull(this.w_DICODCAU)+;
                  ","+cp_ToStrODBC(this.w_DIDESCRI)+;
                  ","+cp_ToStrODBC(this.w_DISCOTRA)+;
                  ","+cp_ToStrODBC(this.w_DISCACES)+;
                  ","+cp_ToStrODBC(this.w_DIFLAVVI)+;
                  ","+cp_ToStrODBC(this.w_DIDATAVV)+;
                  ","+cp_ToStrODBC(this.w_DINSCFRB)+;
                  ","+cp_ToStrODBC(this.w_DI__PATH)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIS_TINT')
        i_extval=cp_InsertValVFPExtFlds(this,'DIS_TINT')
        cp_CheckDeletedKey(i_cTable,0,'DINUMDIS',this.w_DINUMDIS)
        INSERT INTO (i_cTable);
              (DINUMDIS,DIRIFCON,DI__ANNO,DINUMERO,DIDATDIS,DICODESE,DITIPDIS,DICAURIF,DICAUBON,DITIPMAN,DICOSEFF,DITIPSCA,DIBANRIF,DICOSCOM,DIFLDEFI,DISCAINI,DISCAFIN,DIDATVAL,DIDATCON,DICODVAL,DICAOVAL,DICODCAU,DIDESCRI,DISCOTRA,DISCACES,DIFLAVVI,DIDATAVV,DINSCFRB,DI__PATH,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DINUMDIS;
                  ,this.w_DIRIFCON;
                  ,this.w_DI__ANNO;
                  ,this.w_DINUMERO;
                  ,this.w_DIDATDIS;
                  ,this.w_DICODESE;
                  ,this.w_DITIPDIS;
                  ,this.w_DICAURIF;
                  ,this.w_DICAUBON;
                  ,this.w_DITIPMAN;
                  ,this.w_DICOSEFF;
                  ,this.w_DITIPSCA;
                  ,this.w_DIBANRIF;
                  ,this.w_DICOSCOM;
                  ,this.w_DIFLDEFI;
                  ,this.w_DISCAINI;
                  ,this.w_DISCAFIN;
                  ,this.w_DIDATVAL;
                  ,this.w_DIDATCON;
                  ,this.w_DICODVAL;
                  ,this.w_DICAOVAL;
                  ,this.w_DICODCAU;
                  ,this.w_DIDESCRI;
                  ,this.w_DISCOTRA;
                  ,this.w_DISCACES;
                  ,this.w_DIFLAVVI;
                  ,this.w_DIDATAVV;
                  ,this.w_DINSCFRB;
                  ,this.w_DI__PATH;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DIS_TINT_IDX,i_nConn)
      *
      * update DIS_TINT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DIS_TINT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DIRIFCON="+cp_ToStrODBC(this.w_DIRIFCON)+;
             ",DI__ANNO="+cp_ToStrODBC(this.w_DI__ANNO)+;
             ",DINUMERO="+cp_ToStrODBC(this.w_DINUMERO)+;
             ",DIDATDIS="+cp_ToStrODBC(this.w_DIDATDIS)+;
             ",DICODESE="+cp_ToStrODBCNull(this.w_DICODESE)+;
             ",DITIPDIS="+cp_ToStrODBC(this.w_DITIPDIS)+;
             ",DICAURIF="+cp_ToStrODBCNull(this.w_DICAURIF)+;
             ",DICAUBON="+cp_ToStrODBCNull(this.w_DICAUBON)+;
             ",DITIPMAN="+cp_ToStrODBC(this.w_DITIPMAN)+;
             ",DICOSEFF="+cp_ToStrODBC(this.w_DICOSEFF)+;
             ",DITIPSCA="+cp_ToStrODBC(this.w_DITIPSCA)+;
             ",DIBANRIF="+cp_ToStrODBCNull(this.w_DIBANRIF)+;
             ",DICOSCOM="+cp_ToStrODBC(this.w_DICOSCOM)+;
             ",DIFLDEFI="+cp_ToStrODBC(this.w_DIFLDEFI)+;
             ",DISCAINI="+cp_ToStrODBC(this.w_DISCAINI)+;
             ",DISCAFIN="+cp_ToStrODBC(this.w_DISCAFIN)+;
             ",DIDATVAL="+cp_ToStrODBC(this.w_DIDATVAL)+;
             ",DIDATCON="+cp_ToStrODBC(this.w_DIDATCON)+;
             ",DICODVAL="+cp_ToStrODBCNull(this.w_DICODVAL)+;
             ",DICAOVAL="+cp_ToStrODBC(this.w_DICAOVAL)+;
             ",DICODCAU="+cp_ToStrODBCNull(this.w_DICODCAU)+;
             ",DIDESCRI="+cp_ToStrODBC(this.w_DIDESCRI)+;
             ",DISCOTRA="+cp_ToStrODBC(this.w_DISCOTRA)+;
             ",DISCACES="+cp_ToStrODBC(this.w_DISCACES)+;
             ",DIFLAVVI="+cp_ToStrODBC(this.w_DIFLAVVI)+;
             ",DIDATAVV="+cp_ToStrODBC(this.w_DIDATAVV)+;
             ",DINSCFRB="+cp_ToStrODBC(this.w_DINSCFRB)+;
             ",DI__PATH="+cp_ToStrODBC(this.w_DI__PATH)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DIS_TINT')
        i_cWhere = cp_PKFox(i_cTable  ,'DINUMDIS',this.w_DINUMDIS  )
        UPDATE (i_cTable) SET;
              DIRIFCON=this.w_DIRIFCON;
             ,DI__ANNO=this.w_DI__ANNO;
             ,DINUMERO=this.w_DINUMERO;
             ,DIDATDIS=this.w_DIDATDIS;
             ,DICODESE=this.w_DICODESE;
             ,DITIPDIS=this.w_DITIPDIS;
             ,DICAURIF=this.w_DICAURIF;
             ,DICAUBON=this.w_DICAUBON;
             ,DITIPMAN=this.w_DITIPMAN;
             ,DICOSEFF=this.w_DICOSEFF;
             ,DITIPSCA=this.w_DITIPSCA;
             ,DIBANRIF=this.w_DIBANRIF;
             ,DICOSCOM=this.w_DICOSCOM;
             ,DIFLDEFI=this.w_DIFLDEFI;
             ,DISCAINI=this.w_DISCAINI;
             ,DISCAFIN=this.w_DISCAFIN;
             ,DIDATVAL=this.w_DIDATVAL;
             ,DIDATCON=this.w_DIDATCON;
             ,DICODVAL=this.w_DICODVAL;
             ,DICAOVAL=this.w_DICAOVAL;
             ,DICODCAU=this.w_DICODCAU;
             ,DIDESCRI=this.w_DIDESCRI;
             ,DISCOTRA=this.w_DISCOTRA;
             ,DISCACES=this.w_DISCACES;
             ,DIFLAVVI=this.w_DIFLAVVI;
             ,DIDATAVV=this.w_DIDATAVV;
             ,DINSCFRB=this.w_DINSCFRB;
             ,DI__PATH=this.w_DI__PATH;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSTE_MPD : Saving
      this.GSTE_MPD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DINUMDIS,"PTSERIAL";
             ,this.w_ROWORDP,"PTROWORD";
             )
      this.GSTE_MPD.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSTE_MPD : Deleting
    this.GSTE_MPD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DINUMDIS,"PTSERIAL";
           ,this.w_ROWORDP,"PTROWORD";
           )
    this.GSTE_MPD.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DIS_TINT_IDX,i_nConn)
      *
      * delete DIS_TINT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DINUMDIS',this.w_DINUMDIS  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
    if i_bUpd
      with this
            .w_CODAZI = i_CODAZI
        .DoRTCalc(2,3,.t.)
            .w_TIPCC = 'G'
        .DoRTCalc(5,6,.t.)
        if .o_DIDATDIS<>.w_DIDATDIS
            .w_DI__ANNO = STR(YEAR(IIF(EMPTY(.w_DIDATDIS), i_datsys, .w_DIDATDIS)),4,0)
        endif
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .DoRTCalc(8,10,.t.)
        if .o_DIDATVAL<>.w_DIDATVAL
            .w_DICODESE = CALCESER(.w_DIDATVAL,g_CODESE)
          .link_1_12('Full')
        endif
            .w_OBTEST = .w_DIDATDIS
        .DoRTCalc(13,13,.t.)
            .w_CATIPDIS = .w_DITIPDIS
        .DoRTCalc(15,16,.t.)
        if .o_DICAURIF<>.w_DICAURIF
          .link_1_18('Full')
        endif
        if .o_DICAURIF<>.w_DICAURIF
            .w_DITIPMAN = iif(.w_FLRI='RI',.w_CATIPMAN,'T')
        endif
        .DoRTCalc(19,28,.t.)
        if .o_DICODCAU<>.w_DICODCAU
            .w_FLCABI = ' '
        endif
        if .o_DICAURIF<>.w_DICAURIF
            .w_DICOSCOM = IIF(g_COGE='S',.w_IMPCOM,0)
        endif
        .DoRTCalc(31,37,.t.)
        if .o_DIDATDIS<>.w_DIDATDIS
            .w_DISCAINI = IIF(.cFunction='Load',.w_DIDATDIS,.w_DISCAFIN)
        endif
        .DoRTCalc(39,39,.t.)
        if .o_DIDATDIS<>.w_DIDATDIS
            .w_DIDATVAL = .w_DIDATDIS
        endif
        if .o_DIDATDIS<>.w_DIDATDIS.or. .o_DIDATVAL<>.w_DIDATVAL
            .w_DIDATCON = .w_DIDATDIS
        endif
        if .o_DICAURIF<>.w_DICAURIF
          .link_1_43('Full')
        endif
        .DoRTCalc(43,46,.t.)
        if .o_DECTOT<>.w_DECTOT
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_DICODVAL<>.w_DICODVAL.or. .o_DIDATVAL<>.w_DIDATVAL.or. .o_DICAURIF<>.w_DICAURIF
            .w_DICAOVAL = IIF(EMPTY(.w_DICODVAL) OR EMPTY(.w_DIDATVAL), 0, GETCAM(.w_DICODVAL, .w_DIDATVAL, 7))
        endif
        .DoRTCalc(49,54,.t.)
            .w_SIMNAZ = g_VALSIM
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate(IIF(.w_DITIPSCA='C', ah_MsgFormat("Attive"), IIF(.w_DITIPSCA='F', ah_MsgFormat("Passive"),'')),'','')
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate(FUNDIS(.w_DITIPDIS),'','')
        .DoRTCalc(56,58,.t.)
        if .o_DICAURIF<>.w_DICAURIF
          .link_2_2('Full')
        endif
        .DoRTCalc(60,61,.t.)
        if .o_DITIPDIS<>.w_DITIPDIS.or. .o_DITIPSCA<>.w_DITIPSCA
            .w_DISCOTRA = 0
        endif
        .DoRTCalc(63,63,.t.)
        if .o_DICAURIF<>.w_DICAURIF
            .w_DIFLAVVI = .w_FLAVVI
        endif
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .DoRTCalc(65,71,.t.)
            .w_FLORI = .T.
            .w_COMMIS = IIF(g_COGE='S',.w_DICOSCOM+.w_DICOSEFF,0)
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate(IIF(.w_FLRB='RB', 'R.B./RIBA','')+' ' +IIF(.w_FLRD='RD', 'Rim.Dir.','')+ ' '+IIF(.w_FLMA='MA', 'M.AV.','')+ ' '+ iif(.w_FLRI $ 'RI-SD', 'R.I.D.', '')+ ' '+ iif(.w_FLBO $ 'BO-SC-SE', 'Bonifico','')+ ' ' +IIF(.w_FLCA='CA', 'Cambiali', ''))
            .w_SIMNAZ1 = g_VALSIM
            .w_BACKCOLOR = RGB(252,199,186)
            .w_TEXTCOLOR = RGB(252,199,186)
            .w_CAPTION = ''
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate(.w_CAPTION,.w_BACKCOLOR,.w_TEXTCOLOR)
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        if .o_DIDATVAL<>.w_DIDATVAL
          .Calculate_BNILYYWITU()
        endif
        .oPgFrm.Page1.oPag.oObj_1_109.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_110.Calculate()
        .DoRTCalc(78,98,.t.)
            .w_HTML_URL = .w_DI__PATH
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"PRDIS","i_codazi,w_DINUMDIS")
          .op_DINUMDIS = .w_DINUMDIS
        endif
        if .op_codazi<>.w_codazi .or. .op_DI__ANNO<>.w_DI__ANNO
           cp_AskTableProg(this,i_nConn,"PNDIS","i_codazi,w_DI__ANNO,w_DINUMERO")
          .op_DINUMERO = .w_DINUMERO
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_DI__ANNO = .w_DI__ANNO
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_73.Calculate(IIF(.w_DITIPSCA='C', ah_MsgFormat("Attive"), IIF(.w_DITIPSCA='F', ah_MsgFormat("Passive"),'')),'','')
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate(FUNDIS(.w_DITIPDIS),'','')
        .oPgFrm.Page1.oPag.oObj_1_85.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_88.Calculate(IIF(.w_FLRB='RB', 'R.B./RIBA','')+' ' +IIF(.w_FLRD='RD', 'Rim.Dir.','')+ ' '+IIF(.w_FLMA='MA', 'M.AV.','')+ ' '+ iif(.w_FLRI $ 'RI-SD', 'R.I.D.', '')+ ' '+ iif(.w_FLBO $ 'BO-SC-SE', 'Bonifico','')+ ' ' +IIF(.w_FLCA='CA', 'Cambiali', ''))
        .oPgFrm.Page1.oPag.oObj_1_95.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate(.w_CAPTION,.w_BACKCOLOR,.w_TEXTCOLOR)
        .oPgFrm.Page1.oPag.oObj_1_99.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_102.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_109.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_110.Calculate()
    endwith
  return

  proc Calculate_WMZSPYMSYG()
    with this
          * --- Gste_bav (Z)
          GSTE_BAV(this;
              ,'Z';
             )
    endwith
  endproc
  proc Calculate_BNILYYWITU()
    with this
          * --- Agg data valuta
          .w_DIDATCON = .w_DIDATVAL
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDICAURIF_1_16.enabled = this.oPgFrm.Page1.oPag.oDICAURIF_1_16.mCond()
    this.oPgFrm.Page1.oPag.oDITIPMAN_1_19.enabled = this.oPgFrm.Page1.oPag.oDITIPMAN_1_19.mCond()
    this.oPgFrm.Page1.oPag.oDIBANRIF_1_27.enabled = this.oPgFrm.Page1.oPag.oDIBANRIF_1_27.mCond()
    this.oPgFrm.Page1.oPag.oDICOSCOM_1_31.enabled = this.oPgFrm.Page1.oPag.oDICOSCOM_1_31.mCond()
    this.oPgFrm.Page1.oPag.oDICODVAL_1_43.enabled = this.oPgFrm.Page1.oPag.oDICODVAL_1_43.mCond()
    this.oPgFrm.Page1.oPag.oDICAOVAL_1_49.enabled = this.oPgFrm.Page1.oPag.oDICAOVAL_1_49.mCond()
    this.oPgFrm.Page2.oPag.oDICODCAU_2_2.enabled = this.oPgFrm.Page2.oPag.oDICODCAU_2_2.mCond()
    this.oPgFrm.Page2.oPag.oDISCOTRA_2_5.enabled = this.oPgFrm.Page2.oPag.oDISCOTRA_2_5.mCond()
    this.oPgFrm.Page2.oPag.oDISCACES_2_6.enabled = this.oPgFrm.Page2.oPag.oDISCACES_2_6.mCond()
    this.oPgFrm.Page2.oPag.oDIFLAVVI_2_7.enabled = this.oPgFrm.Page2.oPag.oDIFLAVVI_2_7.mCond()
    this.oPgFrm.Page2.oPag.oDIDATAVV_2_8.enabled = this.oPgFrm.Page2.oPag.oDIDATAVV_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_69.enabled = this.oPgFrm.Page1.oPag.oBtn_1_69.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDICAUBON_1_18.visible=!this.oPgFrm.Page1.oPag.oDICAUBON_1_18.mHide()
    this.oPgFrm.Page1.oPag.oDITIPMAN_1_19.visible=!this.oPgFrm.Page1.oPag.oDITIPMAN_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDICOSCOM_1_31.visible=!this.oPgFrm.Page1.oPag.oDICOSCOM_1_31.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_52.visible=!this.oPgFrm.Page1.oPag.oBtn_1_52.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_69.visible=!this.oPgFrm.Page1.oPag.oBtn_1_69.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oSIMNAZ_1_71.visible=!this.oPgFrm.Page1.oPag.oSIMNAZ_1_71.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_75.visible=!this.oPgFrm.Page1.oPag.oStr_1_75.mHide()
    this.oPgFrm.Page2.oPag.oDICODCAU_2_2.visible=!this.oPgFrm.Page2.oPag.oDICODCAU_2_2.mHide()
    this.oPgFrm.Page2.oPag.oDESCAU_2_3.visible=!this.oPgFrm.Page2.oPag.oDESCAU_2_3.mHide()
    this.oPgFrm.Page2.oPag.oDIFLAVVI_2_7.visible=!this.oPgFrm.Page2.oPag.oDIFLAVVI_2_7.mHide()
    this.oPgFrm.Page2.oPag.oDIDATAVV_2_8.visible=!this.oPgFrm.Page2.oPag.oDIDATAVV_2_8.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_10.visible=!this.oPgFrm.Page2.oPag.oStr_2_10.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_12.visible=!this.oPgFrm.Page2.oPag.oStr_2_12.mHide()
    this.oPgFrm.Page1.oPag.oCOMMIS_1_87.visible=!this.oPgFrm.Page1.oPag.oCOMMIS_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_90.visible=!this.oPgFrm.Page1.oPag.oStr_1_90.mHide()
    this.oPgFrm.Page1.oPag.oSIMNAZ1_1_91.visible=!this.oPgFrm.Page1.oPag.oSIMNAZ1_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_96.visible=!this.oPgFrm.Page1.oPag.oStr_1_96.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_100.visible=!this.oPgFrm.Page1.oPag.oStr_1_100.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_112.visible=!this.oPgFrm.Page1.oPag.oStr_1_112.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_41.visible=!this.oPgFrm.Page2.oPag.oBtn_2_41.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_73.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_74.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_85.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_88.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_95.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_98.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_99.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_102.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_103.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_107.Event(cEvent)
        if lower(cEvent)==lower("w_DIDATVAL Changed")
          .Calculate_BNILYYWITU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_109.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_110.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DICODESE
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_DICODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_DICODESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oDICODESE_1_12'),i_cWhere,'GSAR_KES',"Elenco esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_DICODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_DICODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_DICODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICAURIF
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_DIST_IDX,3]
    i_lTable = "CAU_DIST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2], .t., this.CAU_DIST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICAURIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACD',True,'CAU_DIST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DICAURIF)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCAU,CATIPSCD,CATIPDIS,CACODVAD,CACODCAU,CADIS_CA,CADIS_RD,CADIS_RI,CADIS_BO,CADIS_MA,CADIS_RB,CAIMPCOM,CAFLAVVI,CATIPCON,CAFLBANC,CAFLIBAN,CACONSBF,CAFLCONT,CACAUBON,CADATEFF,CA_NOCVS,CATIPMAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DICAURIF))
          select CACODICE,CADESCAU,CATIPSCD,CATIPDIS,CACODVAD,CACODCAU,CADIS_CA,CADIS_RD,CADIS_RI,CADIS_BO,CADIS_MA,CADIS_RB,CAIMPCOM,CAFLAVVI,CATIPCON,CAFLBANC,CAFLIBAN,CACONSBF,CAFLCONT,CACAUBON,CADATEFF,CA_NOCVS,CATIPMAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICAURIF)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICAURIF) and !this.bDontReportError
            deferred_cp_zoom('CAU_DIST','*','CACODICE',cp_AbsName(oSource.parent,'oDICAURIF_1_16'),i_cWhere,'GSTE_ACD',"Causali distinte",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCAU,CATIPSCD,CATIPDIS,CACODVAD,CACODCAU,CADIS_CA,CADIS_RD,CADIS_RI,CADIS_BO,CADIS_MA,CADIS_RB,CAIMPCOM,CAFLAVVI,CATIPCON,CAFLBANC,CAFLIBAN,CACONSBF,CAFLCONT,CACAUBON,CADATEFF,CA_NOCVS,CATIPMAN";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCAU,CATIPSCD,CATIPDIS,CACODVAD,CACODCAU,CADIS_CA,CADIS_RD,CADIS_RI,CADIS_BO,CADIS_MA,CADIS_RB,CAIMPCOM,CAFLAVVI,CATIPCON,CAFLBANC,CAFLIBAN,CACONSBF,CAFLCONT,CACAUBON,CADATEFF,CA_NOCVS,CATIPMAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICAURIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCAU,CATIPSCD,CATIPDIS,CACODVAD,CACODCAU,CADIS_CA,CADIS_RD,CADIS_RI,CADIS_BO,CADIS_MA,CADIS_RB,CAIMPCOM,CAFLAVVI,CATIPCON,CAFLBANC,CAFLIBAN,CACONSBF,CAFLCONT,CACAUBON,CADATEFF,CA_NOCVS,CATIPMAN";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DICAURIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DICAURIF)
            select CACODICE,CADESCAU,CATIPSCD,CATIPDIS,CACODVAD,CACODCAU,CADIS_CA,CADIS_RD,CADIS_RI,CADIS_BO,CADIS_MA,CADIS_RB,CAIMPCOM,CAFLAVVI,CATIPCON,CAFLBANC,CAFLIBAN,CACONSBF,CAFLCONT,CACAUBON,CADATEFF,CA_NOCVS,CATIPMAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICAURIF = NVL(_Link_.CACODICE,space(5))
      this.w_DESCAR = NVL(_Link_.CADESCAU,space(35))
      this.w_DITIPSCA = NVL(_Link_.CATIPSCD,space(1))
      this.w_DITIPDIS = NVL(_Link_.CATIPDIS,space(2))
      this.w_DICODVAL = NVL(_Link_.CACODVAD,space(3))
      this.w_DICODCAU = NVL(_Link_.CACODCAU,space(5))
      this.w_FLCA = NVL(_Link_.CADIS_CA,space(2))
      this.w_FLRD = NVL(_Link_.CADIS_RD,space(2))
      this.w_FLRI = NVL(_Link_.CADIS_RI,space(2))
      this.w_FLBO = NVL(_Link_.CADIS_BO,space(2))
      this.w_FLMA = NVL(_Link_.CADIS_MA,space(2))
      this.w_FLRB = NVL(_Link_.CADIS_RB,space(2))
      this.w_IMPCOM = NVL(_Link_.CAIMPCOM,0)
      this.w_FLAVVI = NVL(_Link_.CAFLAVVI,space(1))
      this.w_TIPCON = NVL(_Link_.CATIPCON,space(1))
      this.w_FLBANC = NVL(_Link_.CAFLBANC,space(1))
      this.w_FLIBAN = NVL(_Link_.CAFLIBAN,space(1))
      this.w_CONSBF = NVL(_Link_.CACONSBF,space(1))
      this.w_FLCONT = NVL(_Link_.CAFLCONT,space(1))
      this.w_DICAUBON = NVL(_Link_.CACAUBON,space(15))
      this.w_DATEFF = NVL(_Link_.CADATEFF,space(1))
      this.w_NOCVS = NVL(_Link_.CA_NOCVS,space(1))
      this.w_CATIPMAN = NVL(_Link_.CATIPMAN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DICAURIF = space(5)
      endif
      this.w_DESCAR = space(35)
      this.w_DITIPSCA = space(1)
      this.w_DITIPDIS = space(2)
      this.w_DICODVAL = space(3)
      this.w_DICODCAU = space(5)
      this.w_FLCA = space(2)
      this.w_FLRD = space(2)
      this.w_FLRI = space(2)
      this.w_FLBO = space(2)
      this.w_FLMA = space(2)
      this.w_FLRB = space(2)
      this.w_IMPCOM = 0
      this.w_FLAVVI = space(1)
      this.w_TIPCON = space(1)
      this.w_FLBANC = space(1)
      this.w_FLIBAN = space(1)
      this.w_CONSBF = space(1)
      this.w_FLCONT = space(1)
      this.w_DICAUBON = space(15)
      this.w_DATEFF = space(1)
      this.w_NOCVS = space(1)
      this.w_CATIPMAN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_DIST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICAURIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 23 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_DIST_IDX,3] and i_nFlds+23<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.CACODICE as CACODICE116"+ ",link_1_16.CADESCAU as CADESCAU116"+ ",link_1_16.CATIPSCD as CATIPSCD116"+ ",link_1_16.CATIPDIS as CATIPDIS116"+ ",link_1_16.CACODVAD as CACODVAD116"+ ",link_1_16.CACODCAU as CACODCAU116"+ ",link_1_16.CADIS_CA as CADIS_CA116"+ ",link_1_16.CADIS_RD as CADIS_RD116"+ ",link_1_16.CADIS_RI as CADIS_RI116"+ ",link_1_16.CADIS_BO as CADIS_BO116"+ ",link_1_16.CADIS_MA as CADIS_MA116"+ ",link_1_16.CADIS_RB as CADIS_RB116"+ ",link_1_16.CAIMPCOM as CAIMPCOM116"+ ",link_1_16.CAFLAVVI as CAFLAVVI116"+ ",link_1_16.CATIPCON as CATIPCON116"+ ",link_1_16.CAFLBANC as CAFLBANC116"+ ",link_1_16.CAFLIBAN as CAFLIBAN116"+ ",link_1_16.CACONSBF as CACONSBF116"+ ",link_1_16.CAFLCONT as CAFLCONT116"+ ",link_1_16.CACAUBON as CACAUBON116"+ ",link_1_16.CADATEFF as CADATEFF116"+ ",link_1_16.CA_NOCVS as CA_NOCVS116"+ ",link_1_16.CATIPMAN as CATIPMAN116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on DIS_TINT.DICAURIF=link_1_16.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+23
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and DIS_TINT.DICAURIF=link_1_16.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+23
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DICAUBON
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DIST_IDX,3]
    i_lTable = "TIP_DIST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIST_IDX,2], .t., this.TIP_DIST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICAUBON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DIST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DICAUDIS like "+cp_ToStrODBC(trim(this.w_DICAUBON)+"%");
                   +" and DICODISO="+cp_ToStrODBC(this.w_CODISO);
                   +" and DITIPDIS="+cp_ToStrODBC(this.w_DITIPDIS);

          i_ret=cp_SQL(i_nConn,"select DICODISO,DITIPDIS,DICAUDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DICODISO,DITIPDIS,DICAUDIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DICODISO',this.w_CODISO;
                     ,'DITIPDIS',this.w_DITIPDIS;
                     ,'DICAUDIS',trim(this.w_DICAUBON))
          select DICODISO,DITIPDIS,DICAUDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DICODISO,DITIPDIS,DICAUDIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICAUBON)==trim(_Link_.DICAUDIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICAUBON) and !this.bDontReportError
            deferred_cp_zoom('TIP_DIST','*','DICODISO,DITIPDIS,DICAUDIS',cp_AbsName(oSource.parent,'oDICAUBON_1_18'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODISO<>oSource.xKey(1);
           .or. this.w_DITIPDIS<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DICODISO,DITIPDIS,DICAUDIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DICODISO,DITIPDIS,DICAUDIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DICODISO,DITIPDIS,DICAUDIS";
                     +" from "+i_cTable+" "+i_lTable+" where DICAUDIS="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DICODISO="+cp_ToStrODBC(this.w_CODISO);
                     +" and DITIPDIS="+cp_ToStrODBC(this.w_DITIPDIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DICODISO',oSource.xKey(1);
                       ,'DITIPDIS',oSource.xKey(2);
                       ,'DICAUDIS',oSource.xKey(3))
            select DICODISO,DITIPDIS,DICAUDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICAUBON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DICODISO,DITIPDIS,DICAUDIS";
                   +" from "+i_cTable+" "+i_lTable+" where DICAUDIS="+cp_ToStrODBC(this.w_DICAUBON);
                   +" and DICODISO="+cp_ToStrODBC(this.w_CODISO);
                   +" and DITIPDIS="+cp_ToStrODBC(this.w_DITIPDIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DICODISO',this.w_CODISO;
                       ,'DITIPDIS',this.w_DITIPDIS;
                       ,'DICAUDIS',this.w_DICAUBON)
            select DICODISO,DITIPDIS,DICAUDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICAUBON = NVL(_Link_.DICAUDIS,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_DICAUBON = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DIST_IDX,2])+'\'+cp_ToStr(_Link_.DICODISO,1)+'\'+cp_ToStr(_Link_.DITIPDIS,1)+'\'+cp_ToStr(_Link_.DICAUDIS,1)
      cp_ShowWarn(i_cKey,this.TIP_DIST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICAUBON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIBANRIF
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIBANRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_DIBANRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACODABI,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_DIBANRIF))
          select BACODBAN,BADESCRI,BADTOBSO,BACODABI,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIBANRIF)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_DIBANRIF)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACODABI,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_DIBANRIF)+"%");

            select BACODBAN,BADESCRI,BADTOBSO,BACODABI,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DIBANRIF) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oDIBANRIF_1_27'),i_cWhere,'GSTE_ACB',"Elenco conti banche",'GSTE_MCB.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACODABI,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO,BACODABI,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIBANRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACODABI,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_DIBANRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_DIBANRIF)
            select BACODBAN,BADESCRI,BADTOBSO,BACODABI,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIBANRIF = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBAN = NVL(_Link_.BADESCRI,space(42))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_CODABI = NVL(_Link_.BACODABI,space(5))
      this.w_IBAN = NVL(_Link_.BA__IBAN,space(35))
      this.w_BBAN = NVL(_Link_.BA__BBAN,space(30))
      this.w_BATIPO = NVL(_Link_.BATIPCON,space(1))
      this.w_CONSBF1 = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DIBANRIF = space(15)
      endif
      this.w_DESBAN = space(42)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CODABI = space(5)
      this.w_IBAN = space(35)
      this.w_BBAN = space(30)
      this.w_BATIPO = space(1)
      this.w_CONSBF1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (Not(((.w_DITIPSCA='F' AND .w_DITIPDIS $ 'BO-BE') OR (.w_DITIPSCA='C' AND .w_DITIPDIS $ 'RI-MA')) AND .w_FLIBAN='S' And ( ( EMPTY(.w_IBAN) Or EMPTY(.w_BBAN) ) ) Or  NOT EMPTY(.w_BATIPO) ) ) ) AND (ALLTRIM(.w_CONSBF)=ALLTRIM(.w_CONSBF1) OR .w_CONSBF='E')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto o incongruente o codice IBAN/BBAN mancanti")
        endif
        this.w_DIBANRIF = space(15)
        this.w_DESBAN = space(42)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CODABI = space(5)
        this.w_IBAN = space(35)
        this.w_BBAN = space(30)
        this.w_BATIPO = space(1)
        this.w_CONSBF1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIBANRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_27(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_27.BACODBAN as BACODBAN127"+ ",link_1_27.BADESCRI as BADESCRI127"+ ",link_1_27.BADTOBSO as BADTOBSO127"+ ",link_1_27.BACODABI as BACODABI127"+ ",link_1_27.BA__IBAN as BA__IBAN127"+ ",link_1_27.BA__BBAN as BA__BBAN127"+ ",link_1_27.BATIPCON as BATIPCON127"+ ",link_1_27.BACONSBF as BACONSBF127"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_27 on DIS_TINT.DIBANRIF=link_1_27.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_27"
          i_cKey=i_cKey+'+" and DIS_TINT.DIBANRIF=link_1_27.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DICODVAL
  func Link_1_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_DICODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_DICODVAL))
          select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_DICODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_DICODVAL)+"%");

            select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DICODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oDICODVAL_1_43'),i_cWhere,'GSAR_AVL',"Elenco valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_DICODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_DICODVAL)
            select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
      this.w_TESVAL = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DICODVAL = space(3)
      endif
      this.w_DESAPP = space(35)
      this.w_TESVAL = 0
      this.w_SIMVAL = space(5)
      this.w_DECTOT = 0
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta distinte inesistente o incongruente o obsoleta")
        endif
        this.w_DICODVAL = space(3)
        this.w_DESAPP = space(35)
        this.w_TESVAL = 0
        this.w_SIMVAL = space(5)
        this.w_DECTOT = 0
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_43(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_43.VACODVAL as VACODVAL143"+ ",link_1_43.VADESVAL as VADESVAL143"+ ",link_1_43.VACAOVAL as VACAOVAL143"+ ",link_1_43.VASIMVAL as VASIMVAL143"+ ",link_1_43.VADECTOT as VADECTOT143"+ ",link_1_43.VADTOBSO as VADTOBSO143"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_43 on DIS_TINT.DICODVAL=link_1_43.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_43"
          i_cKey=i_cKey+'+" and DIS_TINT.DICODVAL=link_1_43.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DICODCAU
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_DICODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_DICODCAU))
          select CCCODICE,CCDESCRI,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oDICODCAU_2_2'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSTE_ACD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_DICODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_DICODCAU)
            select CCCODICE,CCDESCRI,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DICODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_TIPREG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DICODCAU) OR .w_TIPREG $ 'NE'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente o incongruente o obsoleta")
        endif
        this.w_DICODCAU = space(5)
        this.w_DESCAU = space(35)
        this.w_TIPREG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CCCODICE as CCCODICE202"+ ",link_2_2.CCDESCRI as CCDESCRI202"+ ",link_2_2.CCTIPREG as CCTIPREG202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on DIS_TINT.DICODCAU=link_2_2.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and DIS_TINT.DICODCAU=link_2_2.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CATCOM
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStrODBC(trim(this.w_CATCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStr(trim(this.w_CATCOM)+"%");

            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATCOM_2_23'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_CDESCRI = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCOM = space(3)
      endif
      this.w_CDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODZON
  func Link_2_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_CODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_CODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ZODESZON like "+cp_ToStrODBC(trim(this.w_CODZON)+"%");

            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ZODESZON like "+cp_ToStr(trim(this.w_CODZON)+"%");

            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oCODZON_2_24'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_CODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_CODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_ZDESCRI = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODZON = space(3)
      endif
      this.w_ZDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONSUP
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CONSUP))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_CONSUP)+"%");

            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCONSUP_2_25'),i_cWhere,'GSAR_AMC',"Mastri",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CONSUP)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_MDESCRI = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CONSUP = space(15)
      endif
      this.w_MDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDI__ANNO_1_7.value==this.w_DI__ANNO)
      this.oPgFrm.Page1.oPag.oDI__ANNO_1_7.value=this.w_DI__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDINUMERO_1_8.value==this.w_DINUMERO)
      this.oPgFrm.Page1.oPag.oDINUMERO_1_8.value=this.w_DINUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATDIS_1_9.value==this.w_DIDATDIS)
      this.oPgFrm.Page1.oPag.oDIDATDIS_1_9.value=this.w_DIDATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODESE_1_12.value==this.w_DICODESE)
      this.oPgFrm.Page1.oPag.oDICODESE_1_12.value=this.w_DICODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDICAURIF_1_16.value==this.w_DICAURIF)
      this.oPgFrm.Page1.oPag.oDICAURIF_1_16.value=this.w_DICAURIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDICAUBON_1_18.RadioValue()==this.w_DICAUBON)
      this.oPgFrm.Page1.oPag.oDICAUBON_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDITIPMAN_1_19.RadioValue()==this.w_DITIPMAN)
      this.oPgFrm.Page1.oPag.oDITIPMAN_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAR_1_21.value==this.w_DESCAR)
      this.oPgFrm.Page1.oPag.oDESCAR_1_21.value=this.w_DESCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDIBANRIF_1_27.value==this.w_DIBANRIF)
      this.oPgFrm.Page1.oPag.oDIBANRIF_1_27.value=this.w_DIBANRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCABI_1_30.RadioValue()==this.w_FLCABI)
      this.oPgFrm.Page1.oPag.oFLCABI_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDICOSCOM_1_31.value==this.w_DICOSCOM)
      this.oPgFrm.Page1.oPag.oDICOSCOM_1_31.value=this.w_DICOSCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDIFLDEFI_1_32.RadioValue()==this.w_DIFLDEFI)
      this.oPgFrm.Page1.oPag.oDIFLDEFI_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDISCAINI_1_39.value==this.w_DISCAINI)
      this.oPgFrm.Page1.oPag.oDISCAINI_1_39.value=this.w_DISCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDISCAFIN_1_40.value==this.w_DISCAFIN)
      this.oPgFrm.Page1.oPag.oDISCAFIN_1_40.value=this.w_DISCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATVAL_1_41.value==this.w_DIDATVAL)
      this.oPgFrm.Page1.oPag.oDIDATVAL_1_41.value=this.w_DIDATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDIDATCON_1_42.value==this.w_DIDATCON)
      this.oPgFrm.Page1.oPag.oDIDATCON_1_42.value=this.w_DIDATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDICODVAL_1_43.value==this.w_DICODVAL)
      this.oPgFrm.Page1.oPag.oDICODVAL_1_43.value=this.w_DICODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_44.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_44.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDICAOVAL_1_49.value==this.w_DICAOVAL)
      this.oPgFrm.Page1.oPag.oDICAOVAL_1_49.value=this.w_DICAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBAN_1_53.value==this.w_DESBAN)
      this.oPgFrm.Page1.oPag.oDESBAN_1_53.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMNAZ_1_71.value==this.w_SIMNAZ)
      this.oPgFrm.Page1.oPag.oSIMNAZ_1_71.value=this.w_SIMNAZ
    endif
    if not(this.oPgFrm.Page2.oPag.oDICODCAU_2_2.value==this.w_DICODCAU)
      this.oPgFrm.Page2.oPag.oDICODCAU_2_2.value=this.w_DICODCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_2_3.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_2_3.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oDIDESCRI_2_4.value==this.w_DIDESCRI)
      this.oPgFrm.Page2.oPag.oDIDESCRI_2_4.value=this.w_DIDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDISCOTRA_2_5.value==this.w_DISCOTRA)
      this.oPgFrm.Page2.oPag.oDISCOTRA_2_5.value=this.w_DISCOTRA
    endif
    if not(this.oPgFrm.Page2.oPag.oDISCACES_2_6.value==this.w_DISCACES)
      this.oPgFrm.Page2.oPag.oDISCACES_2_6.value=this.w_DISCACES
    endif
    if not(this.oPgFrm.Page2.oPag.oDIFLAVVI_2_7.RadioValue()==this.w_DIFLAVVI)
      this.oPgFrm.Page2.oPag.oDIFLAVVI_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDIDATAVV_2_8.value==this.w_DIDATAVV)
      this.oPgFrm.Page2.oPag.oDIDATAVV_2_8.value=this.w_DIDATAVV
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMMIS_1_87.value==this.w_COMMIS)
      this.oPgFrm.Page1.oPag.oCOMMIS_1_87.value=this.w_COMMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMNAZ1_1_91.value==this.w_SIMNAZ1)
      this.oPgFrm.Page1.oPag.oSIMNAZ1_1_91.value=this.w_SIMNAZ1
    endif
    if not(this.oPgFrm.Page2.oPag.oDATDOCINI_2_15.value==this.w_DATDOCINI)
      this.oPgFrm.Page2.oPag.oDATDOCINI_2_15.value=this.w_DATDOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATDOCFIN_2_16.value==this.w_DATDOCFIN)
      this.oPgFrm.Page2.oPag.oDATDOCFIN_2_16.value=this.w_DATDOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMDOCINI_2_19.value==this.w_NUMDOCINI)
      this.oPgFrm.Page2.oPag.oNUMDOCINI_2_19.value=this.w_NUMDOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMDOCFIN_2_20.value==this.w_NUMDOCFIN)
      this.oPgFrm.Page2.oPag.oNUMDOCFIN_2_20.value=this.w_NUMDOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oALFDOCINI_2_21.value==this.w_ALFDOCINI)
      this.oPgFrm.Page2.oPag.oALFDOCINI_2_21.value=this.w_ALFDOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oALFDOCFIN_2_22.value==this.w_ALFDOCFIN)
      this.oPgFrm.Page2.oPag.oALFDOCFIN_2_22.value=this.w_ALFDOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCATCOM_2_23.value==this.w_CATCOM)
      this.oPgFrm.Page2.oPag.oCATCOM_2_23.value=this.w_CATCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODZON_2_24.value==this.w_CODZON)
      this.oPgFrm.Page2.oPag.oCODZON_2_24.value=this.w_CODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oCONSUP_2_25.value==this.w_CONSUP)
      this.oPgFrm.Page2.oPag.oCONSUP_2_25.value=this.w_CONSUP
    endif
    if not(this.oPgFrm.Page2.oPag.oCDESCRI_2_26.value==this.w_CDESCRI)
      this.oPgFrm.Page2.oPag.oCDESCRI_2_26.value=this.w_CDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oZDESCRI_2_27.value==this.w_ZDESCRI)
      this.oPgFrm.Page2.oPag.oZDESCRI_2_27.value=this.w_ZDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oMDESCRI_2_28.value==this.w_MDESCRI)
      this.oPgFrm.Page2.oPag.oMDESCRI_2_28.value=this.w_MDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDINSCFRB_2_29.value==this.w_DINSCFRB)
      this.oPgFrm.Page2.oPag.oDINSCFRB_2_29.value=this.w_DINSCFRB
    endif
    if not(this.oPgFrm.Page2.oPag.oDI__PATH_2_31.value==this.w_DI__PATH)
      this.oPgFrm.Page2.oPag.oDI__PATH_2_31.value=this.w_DI__PATH
    endif
    cp_SetControlsValueExtFlds(this,'DIS_TINT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(GSTE_BDK( This , 'Load'  ))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt(""+This.w_mess+"")
          case   (empty(.w_DINUMERO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDINUMERO_1_8.SetFocus()
            i_bnoObbl = !empty(.w_DINUMERO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DIDATDIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIDATDIS_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DIDATDIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DICODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICODESE_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DICODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DICAURIF))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICAURIF_1_16.SetFocus()
            i_bnoObbl = !empty(.w_DICAURIF)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale distinta inesistente")
          case   ((empty(.w_DIBANRIF)) or not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND (Not(((.w_DITIPSCA='F' AND .w_DITIPDIS $ 'BO-BE') OR (.w_DITIPSCA='C' AND .w_DITIPDIS $ 'RI-MA')) AND .w_FLIBAN='S' And ( ( EMPTY(.w_IBAN) Or EMPTY(.w_BBAN) ) ) Or  NOT EMPTY(.w_BATIPO) ) ) ) AND (ALLTRIM(.w_CONSBF)=ALLTRIM(.w_CONSBF1) OR .w_CONSBF='E')))  and (Not Empty( .w_DICAURIF ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIBANRIF_1_27.SetFocus()
            i_bnoObbl = !empty(.w_DIBANRIF)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto o incongruente o codice IBAN/BBAN mancanti")
          case   not(.w_DISCAFIN>=.w_DISCAINI or empty(.w_DISCAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDISCAINI_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DISCAFIN)) or not(.w_DISCAFIN>=.w_DISCAINI or empty(.w_DISCAINI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDISCAFIN_1_40.SetFocus()
            i_bnoObbl = !empty(.w_DISCAFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DIDATVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIDATVAL_1_41.SetFocus()
            i_bnoObbl = !empty(.w_DIDATVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DICODVAL)) or not(CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICODVAL_1_43.SetFocus()
            i_bnoObbl = !empty(.w_DICODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta distinte inesistente o incongruente o obsoleta")
          case   (empty(.w_DICAOVAL))  and (.w_DIDATVAL<GETVALUT(g_PERVAL, 'VADATEUR') OR .w_TESVAL=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICAOVAL_1_49.SetFocus()
            i_bnoObbl = !empty(.w_DICAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DICODCAU) OR .w_TIPREG $ 'NE')  and not(g_COGE<>'S')  and (g_COGE='S' AND EMPTY(.w_DIRIFCON) AND .cFunction<>'Edit')  and not(empty(.w_DICODCAU))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDICODCAU_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente o incongruente o obsoleta")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSTE_MPD.CheckForm()
      if i_bres
        i_bres=  .GSTE_MPD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gste_adi
      * Se l'utente tenta di confermare senza aver premuto abbina
      * si da l'avviso
      if i_bRes And .w_FASE=0 AND .cFunction='Load'
           i_bnoChk = .f.
           i_bRes = .f.
           i_cErrorMsg = ah_MsgFormat("Per registrare la prima volta premere il bottone 'Abbina'")
      endif
      
      * Verifico se l'utente dividendo delle scadenze ha creato effetti negativi
      * Eseguo il controllo solo in modifica (in caricamento lo svolge la conferma
      * della maschera di abbina)
      if i_bRes And .cFunction='Edit'
           i_cErrorMsg=GSTE_BCR(This , 'C' , This.GSTE_MPD.cTrsName ,  This.w_FLBANC , This.w_DITIPDIS , This.w_DITIPSCA  )
           If not Empty( i_cErrorMsg )
            i_bnoChk = .f.
            i_bRes = .f.
           Endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DIDATDIS = this.w_DIDATDIS
    this.o_DITIPDIS = this.w_DITIPDIS
    this.o_DICAURIF = this.w_DICAURIF
    this.o_DITIPSCA = this.w_DITIPSCA
    this.o_DIBANRIF = this.w_DIBANRIF
    this.o_DIDATVAL = this.w_DIDATVAL
    this.o_DICODVAL = this.w_DICODVAL
    this.o_DECTOT = this.w_DECTOT
    this.o_DICODCAU = this.w_DICODCAU
    * --- GSTE_MPD : Depends On
    this.GSTE_MPD.SaveDependsOn()
    return

  func CanEdit()
    local i_res
    i_res=GSTE_BDK( This , 'Edit'  )
    if !i_res
      cp_ErrorMsg(thisform.msgFmt(""+This.w_mess+""))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=GSTE_BDK( This , 'Query' )
    if !i_res
      cp_ErrorMsg(thisform.msgFmt(""+This.w_mess+""))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgste_adiPag1 as StdContainer
  Width  = 855
  height = 419
  stdWidth  = 855
  stdheight = 419
  resizeXpos=502
  resizeYpos=289
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDI__ANNO_1_7 as StdField with uid="KPAZCTTUJU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DI__ANNO", cQueryName = "DI__ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 216729989,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=178, Top=9, InputMask=replicate('X',4)

  add object oDINUMERO_1_8 as StdField with uid="RAGNCYNOOE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DINUMERO", cQueryName = "DI__ANNO,DINUMERO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo distinta",;
    HelpContextID = 77592965,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=104, Top=9, cSayPict='"999999"', cGetPict='"999999"'

  add object oDIDATDIS_1_9 as StdField with uid="XGKVGYKSGF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DIDATDIS", cQueryName = "DIDATDIS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della distinta",;
    HelpContextID = 201631351,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=258, Top=9


  add object oObj_1_11 as cp_runprogram with uid="AMSEXDLGQN",left=-2, top=573, width=267,height=19,;
    caption='GSTE_BAV(P)',;
   bGlobalFont=.t.,;
    prg="GSTE_BAV('P')",;
    cEvent = "New record, w_DICAURIF Changed,Load",;
    nPag=1;
    , HelpContextID = 45303356

  add object oDICODESE_1_12 as StdField with uid="MWOWHCARAG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DICODESE", cQueryName = "DICODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza (per calcolo progressivi effetti)",;
    HelpContextID = 67717499,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=413, Top=9, InputMask=replicate('X',4), bHasZoom = .t. , tabstop=.f., cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_DICODESE"

  func oDICODESE_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODESE_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODESE_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oDICODESE_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"Elenco esercizi",'',this.parent.oContained
  endproc
  proc oDICODESE_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_CODAZI
     i_obj.w_ESCODESE=this.parent.oContained.w_DICODESE
     i_obj.ecpSave()
  endproc

  add object oDICAURIF_1_16 as StdField with uid="IVMSUIHXCU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DICAURIF", cQueryName = "DICAURIF",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale distinta inesistente",;
    ToolTipText = "Causale distinta",;
    HelpContextID = 234141316,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=104, Top=35, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_DIST", cZoomOnZoom="GSTE_ACD", oKey_1_1="CACODICE", oKey_1_2="this.w_DICAURIF"

  func oDICAURIF_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oDICAURIF_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICAURIF_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICAURIF_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_DIST','*','CACODICE',cp_AbsName(this.parent,'oDICAURIF_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACD',"Causali distinte",'',this.parent.oContained
  endproc
  proc oDICAURIF_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DICAURIF
     i_obj.ecpSave()
  endproc


  add object oDICAUBON_1_18 as StdZTamTableCombo with uid="GGKCWJRNCX",rtseq=17,rtrep=.f.,left=104,top=63,width=215,height=21;
    , ToolTipText = "Causale di pagamento";
    , HelpContextID = 34294148;
    , cFormVar="w_DICAUBON",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="TIP_DIST";
    , cTable='QUERY\GSTECACD.VQR',cKey='DICAUDIS',cValue='DIDESCRI',cOrderBy='DIDESCRI',xDefault=space(15);
  , bGlobalFont=.t.


  func oDICAUBON_1_18.mHide()
    with this.Parent.oContained
      return (.w_TEST1=.F.)
    endwith
  endfunc

  func oDICAUBON_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICAUBON_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oDITIPMAN_1_19 as StdCombo with uid="OZAQRRZGGB",rtseq=18,rtrep=.f.,left=456,top=61,width=128,height=21;
    , ToolTipText = "Tipo SDD a cui devono appartenere le partite da inserire in distinta";
    , HelpContextID = 214194564;
    , cFormVar="w_DITIPMAN",RowSource=""+"CORE,"+"B2B,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDITIPMAN_1_19.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'B',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oDITIPMAN_1_19.GetRadio()
    this.Parent.oContained.w_DITIPMAN = this.RadioValue()
    return .t.
  endfunc

  func oDITIPMAN_1_19.SetRadio()
    this.Parent.oContained.w_DITIPMAN=trim(this.Parent.oContained.w_DITIPMAN)
    this.value = ;
      iif(this.Parent.oContained.w_DITIPMAN=='C',1,;
      iif(this.Parent.oContained.w_DITIPMAN=='B',2,;
      iif(this.Parent.oContained.w_DITIPMAN=='T',3,;
      0)))
  endfunc

  func oDITIPMAN_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' OR .gste_mpd.NumRow()=0)
    endwith
   endif
  endfunc

  func oDITIPMAN_1_19.mHide()
    with this.Parent.oContained
      return (.w_FLRI<>'RI' )
    endwith
  endfunc

  add object oDESCAR_1_21 as StdField with uid="VQYYZQUVSM",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCAR", cQueryName = "DESCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 13518134,;
   bGlobalFont=.t.,;
    Height=21, Width=271, Left=178, Top=35, InputMask=replicate('X',35)

  add object oDIBANRIF_1_27 as StdField with uid="CVGCFVPCON",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DIBANRIF", cQueryName = "DIBANRIF",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto o incongruente o codice IBAN/BBAN mancanti",;
    ToolTipText = "Conto banca di presentazione distinta",;
    HelpContextID = 241485444,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=104, Top=90, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_DIBANRIF"

  func oDIBANRIF_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty( .w_DICAURIF ))
    endwith
   endif
  endfunc

  func oDIBANRIF_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIBANRIF_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIBANRIF_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oDIBANRIF_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenco conti banche",'GSTE_MCB.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oDIBANRIF_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_DIBANRIF
     i_obj.ecpSave()
  endproc

  add object oFLCABI_1_30 as StdCheck with uid="NDGYXQOOHM",rtseq=29,rtrep=.f.,left=527, top=86, caption="ABI",;
    ToolTipText = "Se attivo: filtra le partite con codice ABI della banca di appoggio uguale a quello della banca di presentazione",;
    HelpContextID = 131812438,;
    cFormVar="w_FLCABI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCABI_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLCABI_1_30.GetRadio()
    this.Parent.oContained.w_FLCABI = this.RadioValue()
    return .t.
  endfunc

  func oFLCABI_1_30.SetRadio()
    this.Parent.oContained.w_FLCABI=trim(this.Parent.oContained.w_FLCABI)
    this.value = ;
      iif(this.Parent.oContained.w_FLCABI=='S',1,;
      0)
  endfunc

  add object oDICOSCOM_1_31 as StdField with uid="IBHMNOZURZ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DICOSCOM", cQueryName = "DICOSCOM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale commissioni fisse",;
    HelpContextID = 49891715,;
   bGlobalFont=.t.,;
    Height=21, Width=81, Left=758, Top=118, cSayPict="v_PV(32+VVP)", cGetPict="v_GV(32+VVP)"

  func oDICOSCOM_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE='S')
    endwith
   endif
  endfunc

  func oDICOSCOM_1_31.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc


  add object oDIFLDEFI_1_32 as StdCombo with uid="XOUJVIKMLL",rtseq=31,rtrep=.f.,left=711,top=61,width=128,height=21, enabled=.f.;
    , ToolTipText = "Status distinta: confermata (gi� stampata) o provvisoria";
    , HelpContextID = 200902273;
    , cFormVar="w_DIFLDEFI",RowSource=""+"Provvisoria,"+"Confermata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDIFLDEFI_1_32.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oDIFLDEFI_1_32.GetRadio()
    this.Parent.oContained.w_DIFLDEFI = this.RadioValue()
    return .t.
  endfunc

  func oDIFLDEFI_1_32.SetRadio()
    this.Parent.oContained.w_DIFLDEFI=trim(this.Parent.oContained.w_DIFLDEFI)
    this.value = ;
      iif(this.Parent.oContained.w_DIFLDEFI=='N',1,;
      iif(this.Parent.oContained.w_DIFLDEFI=='S',2,;
      0))
  endfunc

  add object oDISCAINI_1_39 as StdField with uid="TCIWJFNRQK",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DISCAINI", cQueryName = "DISCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione scadenze",;
    HelpContextID = 130959743,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=104, Top=118

  func oDISCAINI_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DISCAFIN>=.w_DISCAINI or empty(.w_DISCAFIN))
    endwith
    return bRes
  endfunc

  add object oDISCAFIN_1_40 as StdField with uid="VOZNAJKNQJ",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DISCAFIN", cQueryName = "DISCAFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine selezione scadenze",;
    HelpContextID = 187807356,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=209, Top=118

  func oDISCAFIN_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DISCAFIN>=.w_DISCAINI or empty(.w_DISCAINI))
    endwith
    return bRes
  endfunc

  add object oDIDATVAL_1_41 as StdField with uid="BHAIKCIBBB",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DIDATVAL", cQueryName = "DIDATVAL",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di presentazione in banca della distinta, che sar� utilizzata per la registrazione in primanota",;
    HelpContextID = 100358530,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=401, Top=118

  add object oDIDATCON_1_42 as StdField with uid="NWILSSZYFV",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DIDATCON", cQueryName = "DIDATCON",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data valuta da utilizzare per la generazione di movimenti di conto corrente",;
    HelpContextID = 50026884,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=585, Top=118

  add object oDICODVAL_1_43 as StdField with uid="LAYJZPVLTV",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DICODVAL", cQueryName = "DICODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta distinte inesistente o incongruente o obsoleta",;
    ToolTipText = "Codice valuta delle scadenze selezionate",;
    HelpContextID = 84494722,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=104, Top=145, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_DICODVAL"

  func oDICODVAL_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oDICODVAL_1_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODVAL_1_43.ecpDrop(oSource)
    this.Parent.oContained.link_1_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODVAL_1_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oDICODVAL_1_43'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Elenco valute",'',this.parent.oContained
  endproc
  proc oDICODVAL_1_43.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_DICODVAL
     i_obj.ecpSave()
  endproc

  add object oSIMVAL_1_44 as StdField with uid="GFITFAWTGV",rtseq=43,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 182512166,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=157, Top=145, InputMask=replicate('X',5)

  add object oDICAOVAL_1_49 as StdField with uid="BIPRTUJGDH",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DICAOVAL", cQueryName = "DICAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio di contabilizzazione delle partite",;
    HelpContextID = 95111554,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=401, Top=143, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oDICAOVAL_1_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DIDATVAL<GETVALUT(g_PERVAL, 'VADATEUR') OR .w_TESVAL=0)
    endwith
   endif
  endfunc


  add object oBtn_1_52 as StdButton with uid="QBJIAEIARX",left=791, top=141, width=48,height=45,;
    CpPicture="..\exe\bmp\GENERA.ico", caption="", nPag=1;
    , ToolTipText = "Contabilizza distinte parziali";
    , HelpContextID = 211140574;
    , Caption='C\<ontab.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_52.Click()
      do GSCG_KCD with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_52.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLCONT<>'S' Or .cFunction <> 'Query' Or .w_DIFLDEFI <>'S' OR .w_CONTAB='S')
     endwith
    endif
  endfunc

  add object oDESBAN_1_53 as StdField with uid="ZXIGRFEVBZ",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(42), bMultilanguage =  .f.,;
    HelpContextID = 214779190,;
   bGlobalFont=.t.,;
    Height=21, Width=264, Left=244, Top=90, InputMask=replicate('X',42)


  add object oLinkPC_1_67 as stdDynamicChildContainer with uid="LGOXTNYHLJ",left=1, top=187, width=853, height=232, bOnScreen=.t.;



  add object oBtn_1_69 as StdButton with uid="NVBXBYHAQH",left=206, top=369, width=48,height=45,;
    CpPicture="bmp\ABBINA.bmp", caption="", nPag=1;
    , ToolTipText = "Abbina partite / scadenze";
    , HelpContextID = 46486022;
    , Caption='A\<bbina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_69.Click()
      with this.Parent.oContained
        GSTE_BDC(this.Parent.oContained,.w_DINUMDIS,this.name)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_69.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_DIFLDEFI<>'S' AND .cFunction<>'Query')
      endwith
    endif
  endfunc

  func oBtn_1_69.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_DIFLDEFI='S')
     endwith
    endif
  endfunc

  add object oSIMNAZ_1_71 as StdField with uid="RTWYXJEBQK",rtseq=55,rtrep=.f.,;
    cFormVar = "w_SIMNAZ", cQueryName = "SIMNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 148433446,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=711, Top=90, InputMask=replicate('X',5)

  func oSIMNAZ_1_71.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc


  add object oObj_1_73 as cp_calclbl with uid="OEZJLVWPKG",left=711, top=9, width=100,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold =.t.,Alignment=0,;
    nPag=1;
    , HelpContextID = 85054182


  add object oObj_1_74 as cp_calclbl with uid="IWBOHLACAW",left=566, top=9, width=140,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold =.t.,Alignment=0,;
    nPag=1;
    , HelpContextID = 85054182


  add object oObj_1_85 as cp_runprogram with uid="XPWIJUNFIW",left=240, top=483, width=304,height=18,;
    caption='GSTE_BDC(w_DINUMDIS,name)',;
   bGlobalFont=.t.,;
    prg="GSTE_BDC(w_DINUMDIS,this.name)",;
    cEvent = "LanciaAbbina",;
    nPag=1;
    , HelpContextID = 228347307

  add object oCOMMIS_1_87 as StdField with uid="SYKOHMFHFB",rtseq=73,rtrep=.f.,;
    cFormVar = "w_COMMIS", cQueryName = "COMMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Se distinta in definitiva, commissioni fisse+commissioni per numero effetto",;
    HelpContextID = 39317286,;
   bGlobalFont=.t.,;
    Height=21, Width=81, Left=758, Top=90, cSayPict="v_PV(32+VVP)", cGetPict="v_GV(32+VVP)"

  func oCOMMIS_1_87.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc


  add object oObj_1_88 as cp_calclbl with uid="LIFKXIQPKL",left=566, top=35, width=271,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold =.t.,Alignment=0,bGlobalFont=.t.,alignment=1,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,;
    nPag=1;
    , HelpContextID = 85054182

  add object oSIMNAZ1_1_91 as StdField with uid="RWVDZKHCEA",rtseq=74,rtrep=.f.,;
    cFormVar = "w_SIMNAZ1", cQueryName = "SIMNAZ1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 148433446,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=711, Top=118, InputMask=replicate('X',5)

  func oSIMNAZ1_1_91.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc


  add object oObj_1_95 as cp_runprogram with uid="JYXKVJCFCD",left=-2, top=482, width=231,height=19,;
    caption='GSTE_BAV(T)',;
   bGlobalFont=.t.,;
    prg="GSTE_BAV('T')",;
    cEvent = "AggTotali",;
    nPag=1;
    , HelpContextID = 45304380


  add object oObj_1_98 as cp_calclbl with uid="EVDAFXRTAP",left=585, top=143, width=33,height=17,;
    caption='',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 92943370


  add object oObj_1_99 as cp_runprogram with uid="FAAAQQCRLM",left=-2, top=503, width=231,height=19,;
    caption='GSTE_BAV(D)',;
   bGlobalFont=.t.,;
    prg="GSTE_BAV('D')",;
    cEvent = "w_DIDATCON Changed",;
    nPag=1;
    , HelpContextID = 45300284


  add object oObj_1_102 as cp_runprogram with uid="QLQKORLUDY",left=-2, top=529, width=231,height=19,;
    caption='GSTE_BAV(E)',;
   bGlobalFont=.t.,;
    prg="GSTE_BAV('E')",;
    cEvent = "AggDat",;
    nPag=1;
    , ToolTipText = "Aggiorna data valuta su partite dello stesso raggruppamento";
    , HelpContextID = 45300540


  add object oObj_1_103 as cp_runprogram with uid="TARUWCGGKQ",left=-2, top=552, width=231,height=19,;
    caption='GSTE_BAV(M)',;
   bGlobalFont=.t.,;
    prg="GSTE_BAV('M')",;
    cEvent = "w_DIDATDIS Changed",;
    nPag=1;
    , HelpContextID = 45302588


  add object oObj_1_107 as cp_runprogram with uid="GGSPCZWPOY",left=-2, top=595, width=231,height=19,;
    caption='GSTE_BAV(A)',;
   bGlobalFont=.t.,;
    prg="GSTE_BAV('A')",;
    cEvent = "w_DIDATVAL Changed",;
    nPag=1;
    , HelpContextID = 45299516


  add object oObj_1_109 as cp_runprogram with uid="VXPJZXYEBO",left=-2, top=618, width=231,height=19,;
    caption='GSTE_BAV(B)',;
   bGlobalFont=.t.,;
    prg="GSTE_BAV('B')",;
    cEvent = "w_DICAUBON Changed",;
    nPag=1;
    , HelpContextID = 45299772


  add object oObj_1_110 as cp_runprogram with uid="CWXJDLCJDD",left=-4, top=641, width=218,height=20,;
    caption='GSTE_BPC',;
   bGlobalFont=.t.,;
    prg="GSTE_BPC",;
    cEvent = "Delete end",;
    nPag=1;
    , HelpContextID = 45114281

  add object oStr_1_55 as StdString with uid="CJHNGURSUO",Visible=.t., Left=229, Top=9,;
    Alignment=1, Width=26, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_56 as StdString with uid="LGXSJZUXED",Visible=.t., Left=7, Top=118,;
    Alignment=1, Width=94, Height=15,;
    Caption="Scadenze da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="VFUBJDONTN",Visible=.t., Left=188, Top=118,;
    Alignment=1, Width=19, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="JSLMDGTQZS",Visible=.t., Left=22, Top=35,;
    Alignment=1, Width=79, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="DKYKKZLJCV",Visible=.t., Left=344, Top=9,;
    Alignment=1, Width=67, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="WMHRJKKMWV",Visible=.t., Left=334, Top=143,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="UIRAFPKNWW",Visible=.t., Left=44, Top=9,;
    Alignment=1, Width=57, Height=15,;
    Caption="Numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_63 as StdString with uid="DAFHILRDBL",Visible=.t., Left=172, Top=12,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="BPDGXCXEIS",Visible=.t., Left=661, Top=61,;
    Alignment=1, Width=46, Height=15,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="JZXUAQZZBR",Visible=.t., Left=17, Top=90,;
    Alignment=1, Width=84, Height=18,;
    Caption="Banca pres.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="CSKSODILXB",Visible=.t., Left=41, Top=145,;
    Alignment=1, Width=60, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="OGZHPGOSMG",Visible=.t., Left=617, Top=90,;
    Alignment=1, Width=92, Height=15,;
    Caption="Commissioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_1_75 as StdString with uid="TOWPUDLNVO",Visible=.t., Left=502, Top=10,;
    Alignment=1, Width=60, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_1_75.mHide()
    with this.Parent.oContained
      return (EMPTY (FUNDIS(.w_DITIPDIS)))
    endwith
  endfunc

  add object oStr_1_78 as StdString with uid="XSUKUHJUOZ",Visible=.t., Left=295, Top=118,;
    Alignment=1, Width=103, Height=18,;
    Caption="Data pres.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="DYABGBOIWK",Visible=.t., Left=486, Top=118,;
    Alignment=1, Width=97, Height=18,;
    Caption="Data valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="IUZZQQMGCF",Visible=.t., Left=502, Top=35,;
    Alignment=1, Width=60, Height=18,;
    Caption="Tipo pag:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="HHXMJLADHB",Visible=.t., Left=675, Top=118,;
    Alignment=1, Width=34, Height=18,;
    Caption="Fisse:"  ;
  , bGlobalFont=.t.

  func oStr_1_90.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_1_96 as StdString with uid="YJVARRQKFO",Visible=.t., Left=243, Top=508,;
    Alignment=0, Width=311, Height=18,;
    Caption="il bottone abbina e primanota sono sovrapposti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_96.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_97 as StdString with uid="WNSXPHYWIY",Visible=.t., Left=621, Top=143,;
    Alignment=0, Width=77, Height=17,;
    Caption="Riga separata"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_100 as StdString with uid="FRHQUHRIDK",Visible=.t., Left=4, Top=64,;
    Alignment=1, Width=97, Height=18,;
    Caption="Causale pagam.:"  ;
  , bGlobalFont=.t.

  func oStr_1_100.mHide()
    with this.Parent.oContained
      return (.w_TEST1=.F.)
    endwith
  endfunc

  add object oStr_1_112 as StdString with uid="ZXHCYOQMRO",Visible=.t., Left=391, Top=64,;
    Alignment=1, Width=61, Height=18,;
    Caption="Tipo SDD:"  ;
  , bGlobalFont=.t.

  func oStr_1_112.mHide()
    with this.Parent.oContained
      return (.w_FLRI<>'RI')
    endwith
  endfunc
enddefine
define class tgste_adiPag2 as StdContainer
  Width  = 855
  height = 419
  stdWidth  = 855
  stdheight = 419
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDICODCAU_2_2 as StdField with uid="LECTKEKATY",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DICODCAU", cQueryName = "DICODCAU",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente o incongruente o obsoleta",;
    ToolTipText = "Causale per la contabilizzazione in primanota",;
    HelpContextID = 34163083,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=154, Top=17, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_DICODCAU"

  func oDICODCAU_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE='S' AND EMPTY(.w_DIRIFCON) AND .cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oDICODCAU_2_2.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  func oDICODCAU_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODCAU_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODCAU_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oDICODCAU_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSTE_ACD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oDICODCAU_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_DICODCAU
     i_obj.ecpSave()
  endproc

  add object oDESCAU_2_3 as StdField with uid="RDKAULVPFO",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 63849782,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=223, Top=17, InputMask=replicate('X',35)

  func oDESCAU_2_3.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oDIDESCRI_2_4 as StdField with uid="CDWAYGWBRM",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DIDESCRI", cQueryName = "DIDESCRI",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione della distinta",;
    HelpContextID = 49240447,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=154, Top=42, InputMask=replicate('X',35)

  add object oDISCOTRA_2_5 as StdField with uid="DOBKOZIMQH",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DISCOTRA", cQueryName = "DISCOTRA",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di sconto legata alle tratte",;
    HelpContextID = 61753719,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=154, Top=67, cSayPict='"99.99"', cGetPict='"99.99"'

  func oDISCOTRA_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPDIS='CA' AND .w_DITIPSCA='C')
    endwith
   endif
  endfunc

  add object oDISCACES_2_6 as StdField with uid="KVUFJBBTGQ",rtseq=63,rtrep=.f.,;
    cFormVar = "w_DISCACES", cQueryName = "DISCACES",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Eventuale scarto da cessione",;
    HelpContextID = 238138999,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=154, Top=93, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oDISCACES_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DITIPDIS = 'CE')
    endwith
   endif
  endfunc

  add object oDIFLAVVI_2_7 as StdCheck with uid="UFNFQHKINX",rtseq=64,rtrep=.f.,left=154, top=120, caption="Gestione avvisi",;
    ToolTipText = "Se attivo: abilita l'emissione degli avvisi sulla distinta",;
    HelpContextID = 187270785,;
    cFormVar="w_DIFLAVVI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDIFLAVVI_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oDIFLAVVI_2_7.GetRadio()
    this.Parent.oContained.w_DIFLAVVI = this.RadioValue()
    return .t.
  endfunc

  func oDIFLAVVI_2_7.SetRadio()
    this.Parent.oContained.w_DIFLAVVI=trim(this.Parent.oContained.w_DIFLAVVI)
    this.value = ;
      iif(this.Parent.oContained.w_DIFLAVVI=='S',1,;
      0)
  endfunc

  func oDIFLAVVI_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE='S')
    endwith
   endif
  endfunc

  func oDIFLAVVI_2_7.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oDIDATAVV_2_8 as StdField with uid="IKXVZBVWRP",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DIDATAVV", cQueryName = "DIDATAVV",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di emissione dell'avviso",;
    HelpContextID = 251962996,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=445, Top=117

  func oDIDATAVV_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE='S')
    endwith
   endif
  endfunc

  func oDIDATAVV_2_8.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oDATDOCINI_2_15 as StdField with uid="YGOSHBYFSC",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DATDOCINI", cQueryName = "DATDOCINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Filtro per data documento presente sulla partita",;
    HelpContextID = 223390188,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=154, Top=146

  add object oDATDOCFIN_2_16 as StdField with uid="VLYBHOOKUN",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DATDOCFIN", cQueryName = "DATDOCFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Filtro per data documento presente sulla partita",;
    HelpContextID = 223390113,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=328, Top=146

  add object oNUMDOCINI_2_19 as StdField with uid="KGWWVHMLPE",rtseq=82,rtrep=.f.,;
    cFormVar = "w_NUMDOCINI", cQueryName = "NUMDOCINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 223413580,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=154, Top=174, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oNUMDOCFIN_2_20 as StdField with uid="MURWGCRMYK",rtseq=83,rtrep=.f.,;
    cFormVar = "w_NUMDOCFIN", cQueryName = "NUMDOCFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 223413505,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=328, Top=174, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oALFDOCINI_2_21 as StdField with uid="CHJUPBBDKL",rtseq=84,rtrep=.f.,;
    cFormVar = "w_ALFDOCINI", cQueryName = "ALFDOCINI",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di inizio selezione",;
    HelpContextID = 223444764,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=154, Top=202, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oALFDOCFIN_2_22 as StdField with uid="UDFMAGZZLL",rtseq=85,rtrep=.f.,;
    cFormVar = "w_ALFDOCFIN", cQueryName = "ALFDOCFIN",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di fine selezione",;
    HelpContextID = 223444689,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=328, Top=202, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oCATCOM_2_23 as StdField with uid="BDGMMYEBSH",rtseq=86,rtrep=.f.,;
    cFormVar = "w_CATCOM", cQueryName = "CATCOM",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categ. commerciale",;
    HelpContextID = 212750630,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=154, Top=230, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATCOM"

  func oCATCOM_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCOM_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCOM_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATCOM_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCATCOM_2_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATCOM
     i_obj.ecpSave()
  endproc

  add object oCODZON_2_24 as StdField with uid="GVZZHXJTUL",rtseq=87,rtrep=.f.,;
    cFormVar = "w_CODZON", cQueryName = "CODZON",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice zona",;
    HelpContextID = 230973222,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=154, Top=258, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_CODZON"

  func oCODZON_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODZON_2_24.ecpDrop(oSource)
    this.Parent.oContained.link_2_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODZON_2_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oCODZON_2_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oCODZON_2_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_CODZON
     i_obj.ecpSave()
  endproc

  add object oCONSUP_2_25 as StdField with uid="PBQRVSKHCV",rtseq=88,rtrep=.f.,;
    cFormVar = "w_CONSUP", cQueryName = "CONSUP",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggrupp.",;
    HelpContextID = 1965862,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=154, Top=286, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CONSUP"

  func oCONSUP_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONSUP_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONSUP_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCONSUP_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'',this.parent.oContained
  endproc
  proc oCONSUP_2_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CONSUP
     i_obj.ecpSave()
  endproc

  add object oCDESCRI_2_26 as StdField with uid="HXAGQHGXLI",rtseq=89,rtrep=.f.,;
    cFormVar = "w_CDESCRI", cQueryName = "CDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 251829210,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=328, Top=230, InputMask=replicate('X',35)

  add object oZDESCRI_2_27 as StdField with uid="EZCYTRAVNP",rtseq=90,rtrep=.f.,;
    cFormVar = "w_ZDESCRI", cQueryName = "ZDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 251828842,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=328, Top=258, InputMask=replicate('X',35)

  add object oMDESCRI_2_28 as StdField with uid="GPYMOYJYIM",rtseq=91,rtrep=.f.,;
    cFormVar = "w_MDESCRI", cQueryName = "MDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 251829050,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=328, Top=286, InputMask=replicate('X',40)

  add object oDINSCFRB_2_29 as StdField with uid="RQKAJYCEEH",rtseq=92,rtrep=.f.,;
    cFormVar = "w_DINSCFRB", cQueryName = "DINSCFRB",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome supporto Remote Banking",;
    HelpContextID = 83753336,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=154, Top=337, InputMask=replicate('X',20)

  add object oDI__PATH_2_31 as StdField with uid="VSZKSLECNZ",rtseq=93,rtrep=.f.,;
    cFormVar = "w_DI__PATH", cQueryName = "DI__PATH",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di memorizzazione del file CBI",;
    HelpContextID = 254080642,;
   bGlobalFont=.t.,;
    Height=21, Width=627, Left=154, Top=364, InputMask=replicate('X',254)


  add object oBtn_2_41 as StdButton with uid="ZQIEYZWFVL",left=795, top=319, width=48,height=45,;
    CpPicture="..\exe\bmp\GENERA.ico", caption="", nPag=2;
    , ToolTipText = "Apri file CBI/SEPA";
    , HelpContextID = 85565434;
    , Caption='A\<pri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_41.Click()
      do GSTE_KSX with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_41.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_DI__PATH))
     endwith
    endif
  endfunc

  add object oStr_2_9 as StdString with uid="BFCIDEFRJF",Visible=.t., Left=10, Top=67,;
    Alignment=1, Width=142, Height=15,;
    Caption="Sconto tratte:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="IZCNBCYGWW",Visible=.t., Left=332, Top=120,;
    Alignment=1, Width=110, Height=17,;
    Caption="Data avviso:"  ;
  , bGlobalFont=.t.

  func oStr_2_10.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_2_11 as StdString with uid="XSNTLPDNWT",Visible=.t., Left=10, Top=42,;
    Alignment=1, Width=142, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="XYISLYMRTV",Visible=.t., Left=10, Top=17,;
    Alignment=1, Width=142, Height=15,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  func oStr_2_12.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_2_13 as StdString with uid="WUMHRGOHEU",Visible=.t., Left=10, Top=93,;
    Alignment=1, Width=142, Height=15,;
    Caption="Scarto da cessione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="QZQBNMMKYT",Visible=.t., Left=53, Top=364,;
    Alignment=1, Width=101, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="ZQRHQPFMCR",Visible=.t., Left=10, Top=146,;
    Alignment=1, Width=142, Height=18,;
    Caption="Data doc. da:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="SMCKSHDWJW",Visible=.t., Left=306, Top=146,;
    Alignment=1, Width=18, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="QXFCXSPDDF",Visible=.t., Left=306, Top=175,;
    Alignment=1, Width=18, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="FGJNGOTUZM",Visible=.t., Left=10, Top=174,;
    Alignment=1, Width=142, Height=18,;
    Caption="Numero documento da:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="PYGVZLENXL",Visible=.t., Left=306, Top=203,;
    Alignment=1, Width=18, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="YHYXORPVVE",Visible=.t., Left=10, Top=202,;
    Alignment=1, Width=142, Height=18,;
    Caption="Serie documento da:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="GBBCDVHBRQ",Visible=.t., Left=10, Top=230,;
    Alignment=1, Width=142, Height=18,;
    Caption="Categoria commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_36 as StdString with uid="YDDJXRHOVZ",Visible=.t., Left=10, Top=258,;
    Alignment=1, Width=142, Height=18,;
    Caption="Zone:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="RWRWCTBOZV",Visible=.t., Left=10, Top=286,;
    Alignment=1, Width=142, Height=18,;
    Caption="Mastro contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="GLYDKDTSZQ",Visible=.t., Left=43, Top=319,;
    Alignment=0, Width=133, Height=17,;
    Caption="Remote Banking"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_40 as StdString with uid="NAUEIRIIMU",Visible=.t., Left=47, Top=337,;
    Alignment=1, Width=105, Height=18,;
    Caption="Nome supporto:"  ;
  , bGlobalFont=.t.

  add object oBox_2_38 as StdBox with uid="UTTSARQRFQ",left=40, top=318, width=753,height=76
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_adi','DIS_TINT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DINUMDIS=DIS_TINT.DINUMDIS";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gste_adi
define class StdZTamTableCombo as StdTableCombo

proc Init()
  IF VARTYPE(this.bNoBackColor)='U'
		This.backcolor=i_EBackColor
	ENDIF

endproc

  proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear
      i_bCharKey=type(i_fk)='C'
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()]=trim(&i_fk)
        else
          this.combovalues[recno()]=&i_fk
        endif
        skip
      enddo
      use
    endif

enddefine
* --- Fine Area Manuale
