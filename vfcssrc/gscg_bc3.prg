* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bc3                                                        *
*              Seleziona conto utile/perdita                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-04                                                      *
* Last revis.: 2009-10-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bc3",oParentObject)
return(i_retval)

define class tgscg_bc3 as StdBatch
  * --- Local variables
  w_ANCODICE = space(15)
  w_ANDESCRI = space(40)
  w_SALDO = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizza il Conto utile/perdita nella maschera GSCG_KCE
    this.oParentObject.w_CONUTI = SPACE(15)
    this.oParentObject.w_DESCON1 = SPACE(40)
    this.w_ANCODICE = SPACE(15)
    this.w_ANDESCRI = SPACE(40)
    vx_exec("QUERY\GSCG_KCE.VZM",this)
    if this.w_SALDO<>0
      AH_ERRORMSG("Il conto selezionato ha un saldo diverso da zero",48,"ATTENZIONE")
      this.w_ANCODICE = SPACE(15)
    endif
    if NOT EMPTY(this.w_ANCODICE)
      * --- Carica Codici
      this.oParentObject.w_CONUTI = this.w_ANCODICE
      this.oParentObject.w_DESCON1 = this.w_ANDESCRI
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
