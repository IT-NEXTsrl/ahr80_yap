* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bdi                                                        *
*              Cancellazione dettaglio indici                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-08-28                                                      *
* Last revis.: 2006-08-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bdi",oParentObject)
return(i_retval)

define class tgsut_bdi as StdBatch
  * --- Local variables
  * --- WorkFile variables
  PRODINDI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine chiamata da GSUT_AIN all'atto della cancellazioone di un record
    *     Presente solo se DOCM non � attivo
    * --- Cancello il dettaglio associato alla registrazione di cui si vuole cancellare la testata (Indici DOCM)
    * --- Try
    local bErr_038C5E50
    bErr_038C5E50=bTrsErr
    this.Try_038C5E50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      i_retcode = 'stop'
      i_retval = .F.
      return
    endif
    bTrsErr=bTrsErr or bErr_038C5E50
    * --- End
  endproc
  proc Try_038C5E50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from PRODINDI
    i_nConn=i_TableProp[this.PRODINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"IDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_IDSERIAL);
             )
    else
      delete from (i_cTable) where;
            IDSERIAL = this.oParentObject.w_IDSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    i_retcode = 'stop'
    i_retval = .T.
    return
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PRODINDI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
