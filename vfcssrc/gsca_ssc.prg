* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_ssc                                                        *
*              Stampe di controllo                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [116] [VRS_111]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-22                                                      *
* Last revis.: 2007-07-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsca_ssc",oParentObject))

* --- Class definition
define class tgsca_ssc as StdForm
  Top    = 36
  Left   = 40

  * --- Standard Properties
  Width  = 517
  Height = 342
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-11"
  HelpContextID=90867049
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  AZIENDA_IDX = 0
  CAN_TIER_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  cPrg = "gsca_ssc"
  cComment = "Stampe di controllo"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(5)
  w_tipo1 = space(1)
  w_SUCOMMES = space(1)
  w_COMMES = space(15)
  w_DESCOM = space(40)
  w_CENCER = space(15)
  w_DESCEN = space(35)
  w_VOCE = space(15)
  w_DESVOC = space(35)
  w_CONCON = space(15)
  w_DESCON = space(40)
  w_DATA1 = ctod('  /  /  ')
  o_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_TIPO = space(1)
  w_PROVECOM = space(1)
  w_PROVE = space(1)
  w_PROVECCR = space(1)
  w_FLANAL = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ONUME = 0
  w_LIVE1 = 0
  w_LIVE2 = 0
  w_TIPSTA = 0
  w_COMPET = space(1)
  w_PERCOMPE = space(1)
  o_PERCOMPE = space(1)
  w_DATARAGG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsca_sscPag1","gsca_ssc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOMMES_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='CENCOST'
    this.cWorkTables[5]='VOC_COST'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCA_BSC with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(5)
      .w_tipo1=space(1)
      .w_SUCOMMES=space(1)
      .w_COMMES=space(15)
      .w_DESCOM=space(40)
      .w_CENCER=space(15)
      .w_DESCEN=space(35)
      .w_VOCE=space(15)
      .w_DESVOC=space(35)
      .w_CONCON=space(15)
      .w_DESCON=space(40)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_TIPO=space(1)
      .w_PROVECOM=space(1)
      .w_PROVE=space(1)
      .w_PROVECCR=space(1)
      .w_FLANAL=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_ONUME=0
      .w_LIVE1=0
      .w_LIVE2=0
      .w_TIPSTA=0
      .w_COMPET=space(1)
      .w_PERCOMPE=space(1)
      .w_DATARAGG=space(1)
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_1('Full')
        endif
        .w_tipo1 = 'G'
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_COMMES))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_CENCER))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_VOCE))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_CONCON))
          .link_1_10('Full')
        endif
          .DoRTCalc(11,11,.f.)
        .w_DATA1 = g_iniese
        .w_DATA2 = g_finese
        .w_TIPO = 'E'
        .w_PROVECOM = 'E'
        .w_PROVE = 'O'
        .w_PROVECCR = 'T'
          .DoRTCalc(18,18,.f.)
        .w_OBTEST = .w_DATA1
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
          .DoRTCalc(20,21,.f.)
        .w_LIVE1 = 1
        .w_LIVE2 = 99
        .w_TIPSTA = .w_ONUME
        .w_COMPET = IIF(.w_PERCOMPE='S',.w_PERCOMPE,'N')
    endwith
    this.DoRTCalc(26,27,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,18,.t.)
        if .o_DATA1<>.w_DATA1
            .w_OBTEST = .w_DATA1
        endif
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .DoRTCalc(20,23,.t.)
            .w_TIPSTA = .w_ONUME
        if .o_PERCOMPE<>.w_PERCOMPE
            .w_COMPET = IIF(.w_PERCOMPE='S',.w_PERCOMPE,'N')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(26,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPROVECOM_1_15.enabled = this.oPgFrm.Page1.oPag.oPROVECOM_1_15.mCond()
    this.oPgFrm.Page1.oPag.oPROVE_1_16.enabled = this.oPgFrm.Page1.oPag.oPROVE_1_16.mCond()
    this.oPgFrm.Page1.oPag.oPROVECCR_1_17.enabled = this.oPgFrm.Page1.oPag.oPROVECCR_1_17.mCond()
    this.oPgFrm.Page1.oPag.oLIVE1_1_33.enabled = this.oPgFrm.Page1.oPag.oLIVE1_1_33.mCond()
    this.oPgFrm.Page1.oPag.oLIVE2_1_34.enabled = this.oPgFrm.Page1.oPag.oLIVE2_1_34.mCond()
    this.oPgFrm.Page1.oPag.oCOMPET_1_38.enabled = this.oPgFrm.Page1.oPag.oCOMPET_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPROVECOM_1_15.visible=!this.oPgFrm.Page1.oPag.oPROVECOM_1_15.mHide()
    this.oPgFrm.Page1.oPag.oPROVE_1_16.visible=!this.oPgFrm.Page1.oPag.oPROVE_1_16.mHide()
    this.oPgFrm.Page1.oPag.oPROVECCR_1_17.visible=!this.oPgFrm.Page1.oPag.oPROVECCR_1_17.mHide()
    this.oPgFrm.Page1.oPag.oLIVE1_1_33.visible=!this.oPgFrm.Page1.oPag.oLIVE1_1_33.mHide()
    this.oPgFrm.Page1.oPag.oLIVE2_1_34.visible=!this.oPgFrm.Page1.oPag.oLIVE2_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oCOMPET_1_38.visible=!this.oPgFrm.Page1.oPag.oCOMPET_1_38.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRIPCOM,AZFLRIPC";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZRIPCOM,AZFLRIPC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_PERCOMPE = NVL(_Link_.AZRIPCOM,space(1))
      this.w_SUCOMMES = NVL(_Link_.AZFLRIPC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_PERCOMPE = space(1)
      this.w_SUCOMMES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMMES
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMMES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_COMMES)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_COMMES))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMMES)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_COMMES)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_COMMES)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COMMES) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCOMMES_1_4'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMMES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_COMMES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_COMMES)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMMES = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COMMES = space(15)
      endif
      this.w_DESCOM = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice commessa � obsoleto")
        endif
        this.w_COMMES = space(15)
        this.w_DESCOM = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMMES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CENCER
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CENCER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_CENCER)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_CENCER))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CENCER)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_CENCER)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_CENCER)+"%");

            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CENCER) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oCENCER_1_6'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CENCER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CENCER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CENCER)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CENCER = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CENCER = space(15)
      endif
      this.w_DESCEN = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice c. C/R � obsoleto")
        endif
        this.w_CENCER = space(15)
        this.w_DESCEN = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CENCER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VOCE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VOCE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_VOCE)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_VOCE))
          select VCCODICE,VCDESCRI,VCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VOCE)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VOCE) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oVOCE_1_8'),i_cWhere,'GSCA_AVC',"Voci centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VOCE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_VOCE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_VOCE)
            select VCCODICE,VCDESCRI,VCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VOCE = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_VOCE = space(15)
      endif
      this.w_DESVOC = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice voce � obsoleto")
        endif
        this.w_VOCE = space(15)
        this.w_DESVOC = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VOCE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONCON
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CONCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_tipo1);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_tipo1;
                     ,'ANCODICE',trim(this.w_CONCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CONCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_tipo1);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CONCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_tipo1);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCONCON_1_10'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_tipo1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice conto � obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_tipo1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CONCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_tipo1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_tipo1;
                       ,'ANCODICE',this.w_CONCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CONCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice conto � obsoleto")
        endif
        this.w_CONCON = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOMMES_1_4.value==this.w_COMMES)
      this.oPgFrm.Page1.oPag.oCOMMES_1_4.value=this.w_COMMES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_5.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_5.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCENCER_1_6.value==this.w_CENCER)
      this.oPgFrm.Page1.oPag.oCENCER_1_6.value=this.w_CENCER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCEN_1_7.value==this.w_DESCEN)
      this.oPgFrm.Page1.oPag.oDESCEN_1_7.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oVOCE_1_8.value==this.w_VOCE)
      this.oPgFrm.Page1.oPag.oVOCE_1_8.value=this.w_VOCE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVOC_1_9.value==this.w_DESVOC)
      this.oPgFrm.Page1.oPag.oDESVOC_1_9.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCONCON_1_10.value==this.w_CONCON)
      this.oPgFrm.Page1.oPag.oCONCON_1_10.value=this.w_CONCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_11.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_11.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_12.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_12.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_13.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_13.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_14.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVECOM_1_15.RadioValue()==this.w_PROVECOM)
      this.oPgFrm.Page1.oPag.oPROVECOM_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVE_1_16.RadioValue()==this.w_PROVE)
      this.oPgFrm.Page1.oPag.oPROVE_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVECCR_1_17.RadioValue()==this.w_PROVECCR)
      this.oPgFrm.Page1.oPag.oPROVECCR_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLIVE1_1_33.value==this.w_LIVE1)
      this.oPgFrm.Page1.oPag.oLIVE1_1_33.value=this.w_LIVE1
    endif
    if not(this.oPgFrm.Page1.oPag.oLIVE2_1_34.value==this.w_LIVE2)
      this.oPgFrm.Page1.oPag.oLIVE2_1_34.value=this.w_LIVE2
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMPET_1_38.RadioValue()==this.w_COMPET)
      this.oPgFrm.Page1.oPag.oCOMPET_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATARAGG_1_40.RadioValue()==this.w_DATARAGG)
      this.oPgFrm.Page1.oPag.oDATARAGG_1_40.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COMMES))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMMES_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice commessa � obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CENCER))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCENCER_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice c. C/R � obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_VOCE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVOCE_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice voce � obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CONCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONCON_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice conto � obsoleto")
          case   ((empty(.w_DATA1)) or not(.w_data1<=.w_data2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DATA1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DATA2)) or not(.w_data1<=.w_data2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DATA2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_LIVE2=0 OR .w_LIVE2>=.w_LIVE1)  and not(.w_TIPSTA<>2)  and (.w_TIPSTA=2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLIVE1_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di livelli vuoto")
          case   not(.w_LIVE1=0 OR .w_LIVE2>=.w_LIVE1)  and not(.w_TIPSTA<>2)  and (.w_TIPSTA=2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLIVE2_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di livelli vuoto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATA1 = this.w_DATA1
    this.o_PERCOMPE = this.w_PERCOMPE
    return

enddefine

* --- Define pages as container
define class tgsca_sscPag1 as StdContainer
  Width  = 513
  height = 342
  stdWidth  = 513
  stdheight = 342
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOMMES_1_4 as StdField with uid="WETJBYSNTL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_COMMES", cQueryName = "COMMES",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice commessa � obsoleto",;
    ToolTipText = "Codice commessa selezionata",;
    HelpContextID = 37199398,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=107, Top=16, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_COMMES"

  func oCOMMES_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMMES_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMMES_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCOMMES_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oCOMMES_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_COMMES
     i_obj.ecpSave()
  endproc

  add object oDESCOM_1_5 as StdField with uid="NWPRGOZUYV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 53611466,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=246, Top=16, InputMask=replicate('X',40)

  add object oCENCER_1_6 as StdField with uid="LQRZCLZZLC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CENCER", cQueryName = "CENCER",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice c. C/R � obsoleto",;
    ToolTipText = "Codice centro di costo/ricavo selezionato",;
    HelpContextID = 19768358,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=107, Top=45, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CENCER"

  func oCENCER_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCENCER_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCENCER_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oCENCER_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oCENCER_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_CENCER
     i_obj.ecpSave()
  endproc

  add object oDESCEN_1_7 as StdField with uid="QRQUDHILEP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 47320010,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=246, Top=45, InputMask=replicate('X',35)

  add object oVOCE_1_8 as StdField with uid="IAXKTYHJHJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_VOCE", cQueryName = "VOCE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice voce � obsoleto",;
    ToolTipText = "Codice voce di costo/ricavo selezionata",;
    HelpContextID = 86048938,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=107, Top=74, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_VOCE"

  func oVOCE_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oVOCE_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVOCE_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oVOCE_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oVOCE_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_VOCE
     i_obj.ecpSave()
  endproc

  add object oDESVOC_1_9 as StdField with uid="PNSJPUHPFG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 220138442,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=246, Top=74, InputMask=replicate('X',35)

  add object oCONCON_1_10 as StdField with uid="COHOQNHMCW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CONCON", cQueryName = "CONCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice conto � obsoleto",;
    ToolTipText = "Codice conto selezionato (editabile solo se il tipo di stampa � (conto e c. C/R) o (voci c. C/R e conto))",;
    HelpContextID = 36852186,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=107, Top=103, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_tipo1", oKey_2_1="ANCODICE", oKey_2_2="this.w_CONCON"

  func oCONCON_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONCON_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONCON_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_tipo1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_tipo1)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCONCON_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCONCON_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_tipo1
     i_obj.w_ANCODICE=this.parent.oContained.w_CONCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_11 as StdField with uid="TNAFSIBJRC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 36834250,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=246, Top=103, InputMask=replicate('X',40)

  add object oDATA1_1_12 as StdField with uid="OJKWDBTMPG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data movimento di inizio stampa",;
    HelpContextID = 34865098,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=107, Top=132

  func oDATA1_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_data1<=.w_data2)
    endwith
    return bRes
  endfunc

  add object oDATA2_1_13 as StdField with uid="RODGIGQWNS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data movimento di fine stampa",;
    HelpContextID = 33816522,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=107, Top=161

  func oDATA2_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_data1<=.w_data2)
    endwith
    return bRes
  endfunc


  add object oTIPO_1_14 as StdCombo with uid="YEVVXXFAGW",rtseq=14,rtrep=.f.,left=108,top=190,width=128,height=21;
    , ToolTipText = "Tipo di movimento selezionato";
    , HelpContextID = 85341898;
    , cFormVar="w_TIPO",RowSource=""+"Effettivo,"+"Previsionale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_1_14.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oTIPO_1_14.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_14.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='E',1,;
      iif(this.Parent.oContained.w_TIPO=='P',2,;
      0))
  endfunc


  add object oPROVECOM_1_15 as StdCombo with uid="GBHEPRGOZY",rtseq=15,rtrep=.f.,left=253,top=190,width=128,height=21;
    , ToolTipText = "Natura dei movimenti selezionati";
    , HelpContextID = 230636989;
    , cFormVar="w_PROVECOM",RowSource=""+"Originari,"+"Escluso ripartiti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVECOM_1_15.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oPROVECOM_1_15.GetRadio()
    this.Parent.oContained.w_PROVECOM = this.RadioValue()
    return .t.
  endfunc

  func oPROVECOM_1_15.SetRadio()
    this.Parent.oContained.w_PROVECOM=trim(this.Parent.oContained.w_PROVECOM)
    this.value = ;
      iif(this.Parent.oContained.w_PROVECOM=='O',1,;
      iif(this.Parent.oContained.w_PROVECOM=='E',2,;
      0))
  endfunc

  func oPROVECOM_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA=5)
    endwith
   endif
  endfunc

  func oPROVECOM_1_15.mHide()
    with this.Parent.oContained
      return (.w_TIPSTA<>5)
    endwith
  endfunc

  proc oPROVECOM_1_15.mAfter
    with this.Parent.oContained
      IIF(.w_SUCOMMES='S',Ah_ErrorMsg("La ripartizione su commessa � attiva: l'opzione originari/escluso ripartiti influenza i totali per commessa"),.w_SUCOMMES=.w_SUCOMMES)
    endwith
  endproc


  add object oPROVE_1_16 as StdCombo with uid="IUTAEYCRDT",rtseq=16,rtrep=.f.,left=253,top=190,width=128,height=21;
    , ToolTipText = "Natura dei movimenti selezionati";
    , HelpContextID = 12533258;
    , cFormVar="w_PROVE",RowSource=""+"Originari,"+"Escluso ripartiti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVE_1_16.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oPROVE_1_16.GetRadio()
    this.Parent.oContained.w_PROVE = this.RadioValue()
    return .t.
  endfunc

  func oPROVE_1_16.SetRadio()
    this.Parent.oContained.w_PROVE=trim(this.Parent.oContained.w_PROVE)
    this.value = ;
      iif(this.Parent.oContained.w_PROVE=='O',1,;
      iif(this.Parent.oContained.w_PROVE=='E',2,;
      0))
  endfunc

  func oPROVE_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA<>2 AND .w_TIPSTA<>5)
    endwith
   endif
  endfunc

  func oPROVE_1_16.mHide()
    with this.Parent.oContained
      return (.w_TIPSTA=2 OR .w_TIPSTA=5)
    endwith
  endfunc


  add object oPROVECCR_1_17 as StdCombo with uid="QDNPVQZXCR",rtseq=17,rtrep=.f.,left=253,top=190,width=128,height=21;
    , ToolTipText = "Natura dei movimenti selezionati";
    , HelpContextID = 37798472;
    , cFormVar="w_PROVECCR",RowSource=""+"Tutti,"+"Originari,"+"Escluso ripartiti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVECCR_1_17.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'O',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oPROVECCR_1_17.GetRadio()
    this.Parent.oContained.w_PROVECCR = this.RadioValue()
    return .t.
  endfunc

  func oPROVECCR_1_17.SetRadio()
    this.Parent.oContained.w_PROVECCR=trim(this.Parent.oContained.w_PROVECCR)
    this.value = ;
      iif(this.Parent.oContained.w_PROVECCR=='T',1,;
      iif(this.Parent.oContained.w_PROVECCR=='O',2,;
      iif(this.Parent.oContained.w_PROVECCR=='E',3,;
      0)))
  endfunc

  func oPROVECCR_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA=2)
    endwith
   endif
  endfunc

  func oPROVECCR_1_17.mHide()
    with this.Parent.oContained
      return (.w_TIPSTA<>2)
    endwith
  endfunc


  add object oObj_1_30 as cp_outputCombo with uid="ZLERXIVPWT",left=105, top=253, width=402,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87130598


  add object oBtn_1_31 as StdButton with uid="INJTDMCWND",left=401, top=288, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 50922534;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        do GSCA_BSC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_data1) and not empty(.w_data2))
      endwith
    endif
  endfunc


  add object oBtn_1_32 as StdButton with uid="CBSQPZYKXR",left=458, top=288, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 83549626;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oLIVE1_1_33 as StdField with uid="DVWPQWHEZS",rtseq=22,rtrep=.f.,;
    cFormVar = "w_LIVE1", cQueryName = "LIVE1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di livelli vuoto",;
    ToolTipText = "Livello di inizio selezione",;
    HelpContextID = 34592586,;
   bGlobalFont=.t.,;
    Height=21, Width=17, Left=108, Top=222, cSayPict='"99"', cGetPict='"99"'

  func oLIVE1_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA=2)
    endwith
   endif
  endfunc

  func oLIVE1_1_33.mHide()
    with this.Parent.oContained
      return (.w_TIPSTA<>2)
    endwith
  endfunc

  func oLIVE1_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LIVE2=0 OR .w_LIVE2>=.w_LIVE1)
    endwith
    return bRes
  endfunc

  add object oLIVE2_1_34 as StdField with uid="DKRWMESLHQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_LIVE2", cQueryName = "LIVE2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di livelli vuoto",;
    ToolTipText = "Livello di fine selezione",;
    HelpContextID = 33544010,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=206, Top=222, cSayPict='"99"', cGetPict='"99"'

  func oLIVE2_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSTA=2)
    endwith
   endif
  endfunc

  func oLIVE2_1_34.mHide()
    with this.Parent.oContained
      return (.w_TIPSTA<>2)
    endwith
  endfunc

  func oLIVE2_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LIVE1=0 OR .w_LIVE2>=.w_LIVE1)
    endwith
    return bRes
  endfunc

  add object oCOMPET_1_38 as StdCheck with uid="KOOXIGRZGQ",rtseq=25,rtrep=.f.,left=246, top=222, caption="Competenza",;
    ToolTipText = "Considera solo la parte di competenza del periodo impostato",;
    HelpContextID = 54173222,;
    cFormVar="w_COMPET", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oCOMPET_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCOMPET_1_38.GetRadio()
    this.Parent.oContained.w_COMPET = this.RadioValue()
    return .t.
  endfunc

  func oCOMPET_1_38.SetRadio()
    this.Parent.oContained.w_COMPET=trim(this.Parent.oContained.w_COMPET)
    this.value = ;
      iif(this.Parent.oContained.w_COMPET=='S',1,;
      0)
  endfunc

  func oCOMPET_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERCOMPE<>'S')
    endwith
   endif
  endfunc

  func oCOMPET_1_38.mHide()
    with this.Parent.oContained
      return (.w_PERCOMPE='S')
    endwith
  endfunc

  add object oDATARAGG_1_40 as StdCheck with uid="QFLCKMDHQO",rtseq=27,rtrep=.f.,left=246, top=161, caption="Raggruppamento per data",;
    ToolTipText = "Raggruppamento per data registrazione",;
    HelpContextID = 16515197,;
    cFormVar="w_DATARAGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDATARAGG_1_40.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDATARAGG_1_40.GetRadio()
    this.Parent.oContained.w_DATARAGG = this.RadioValue()
    return .t.
  endfunc

  func oDATARAGG_1_40.SetRadio()
    this.Parent.oContained.w_DATARAGG=trim(this.Parent.oContained.w_DATARAGG)
    this.value = ;
      iif(this.Parent.oContained.w_DATARAGG=='S',1,;
      0)
  endfunc

  add object oStr_1_18 as StdString with uid="VDHUIHLJAF",Visible=.t., Left=11, Top=103,;
    Alignment=1, Width=94, Height=15,;
    Caption="Conto contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="OZIEUBHTEL",Visible=.t., Left=11, Top=132,;
    Alignment=1, Width=94, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="WYASBUKUQF",Visible=.t., Left=11, Top=161,;
    Alignment=1, Width=94, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="FIZDALSUFR",Visible=.t., Left=11, Top=253,;
    Alignment=1, Width=94, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="JNYPQGJPIH",Visible=.t., Left=11, Top=190,;
    Alignment=1, Width=94, Height=15,;
    Caption="Tipo movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="GCLJJGSTPS",Visible=.t., Left=3, Top=74,;
    Alignment=1, Width=102, Height=15,;
    Caption="Voce costo/ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="QTGAGMSWFC",Visible=.t., Left=11, Top=45,;
    Alignment=1, Width=94, Height=15,;
    Caption="C. costo/ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="UDKMFVYHGJ",Visible=.t., Left=11, Top=16,;
    Alignment=1, Width=94, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="WCEIQJQHJG",Visible=.t., Left=11, Top=222,;
    Alignment=1, Width=94, Height=15,;
    Caption="Da livello:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_TIPSTA<>2)
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="IWTPZVKMCV",Visible=.t., Left=121, Top=222,;
    Alignment=1, Width=83, Height=15,;
    Caption="A livello:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_TIPSTA<>2)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsca_ssc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
