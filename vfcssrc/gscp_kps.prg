* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_kps                                                        *
*              Parametri attribuzione regole acquisizione clienti              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_41]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-07-08                                                      *
* Last revis.: 2010-06-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscp_kps",oParentObject))

* --- Class definition
define class tgscp_kps as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 451
  Height = 124
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-06-21"
  HelpContextID=149525865
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  PES_RACC_IDX = 0
  cPrg = "gscp_kps"
  cComment = "Parametri attribuzione regole acquisizione clienti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_PSCATCOM = 0
  w_PSCODVAL = 0
  w_PSCODLIN = 0
  w_PSCODNAZ = 0
  w_PSCODPAG = 0
  w_PSCODVAL = 0
  w_PSCODNAZ = 0
  w_PSCODPAG = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscp_kpsPag1","gscp_kps",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPSCATCOM_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PES_RACC'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_PSCATCOM=0
      .w_PSCODVAL=0
      .w_PSCODLIN=0
      .w_PSCODNAZ=0
      .w_PSCODPAG=0
      .w_PSCODVAL=0
      .w_PSCODNAZ=0
      .w_PSCODPAG=0
        .w_CODAZI = i_CODAZI
        .w_PSCATCOM = 0
        .w_PSCODVAL = 0
        .w_PSCODLIN = 0
        .w_PSCODNAZ = 0
        .w_PSCODPAG = 0
        .w_PSCODVAL = 0
        .w_PSCODNAZ = 0
        .w_PSCODPAG = 0
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_VPPKINHRLY()
    with this
          * --- Carico i valori delle combo o le inizializzo con i valori standard
          gscp_bps(this;
              ,'L';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPSCATCOM_1_2.visible=!this.oPgFrm.Page1.oPag.oPSCATCOM_1_2.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oPSCODVAL_1_5.visible=!this.oPgFrm.Page1.oPag.oPSCODVAL_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oPSCODNAZ_1_9.visible=!this.oPgFrm.Page1.oPag.oPSCODNAZ_1_9.mHide()
    this.oPgFrm.Page1.oPag.oPSCODPAG_1_11.visible=!this.oPgFrm.Page1.oPag.oPSCODPAG_1_11.mHide()
    this.oPgFrm.Page1.oPag.oPSCODVAL_1_15.visible=!this.oPgFrm.Page1.oPag.oPSCODVAL_1_15.mHide()
    this.oPgFrm.Page1.oPag.oPSCODNAZ_1_16.visible=!this.oPgFrm.Page1.oPag.oPSCODNAZ_1_16.mHide()
    this.oPgFrm.Page1.oPag.oPSCODPAG_1_17.visible=!this.oPgFrm.Page1.oPag.oPSCODPAG_1_17.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_VPPKINHRLY()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPSCATCOM_1_2.RadioValue()==this.w_PSCATCOM)
      this.oPgFrm.Page1.oPag.oPSCATCOM_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODVAL_1_5.RadioValue()==this.w_PSCODVAL)
      this.oPgFrm.Page1.oPag.oPSCODVAL_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODNAZ_1_9.RadioValue()==this.w_PSCODNAZ)
      this.oPgFrm.Page1.oPag.oPSCODNAZ_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODPAG_1_11.RadioValue()==this.w_PSCODPAG)
      this.oPgFrm.Page1.oPag.oPSCODPAG_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODVAL_1_15.RadioValue()==this.w_PSCODVAL)
      this.oPgFrm.Page1.oPag.oPSCODVAL_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODNAZ_1_16.RadioValue()==this.w_PSCODNAZ)
      this.oPgFrm.Page1.oPag.oPSCODNAZ_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODPAG_1_17.RadioValue()==this.w_PSCODPAG)
      this.oPgFrm.Page1.oPag.oPSCODPAG_1_17.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gscp_kps
      i_bRes=GSCP_BPS(this,'S')
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscp_kpsPag1 as StdContainer
  Width  = 447
  height = 124
  stdWidth  = 447
  stdheight = 124
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oPSCATCOM_1_2 as StdCombo with uid="WNRFHROWHO",rtseq=2,rtrep=.f.,left=175,top=9,width=44,height=22;
    , HelpContextID = 6556861;
    , cFormVar="w_PSCATCOM",RowSource=""+"4,"+"3,"+"2,"+"1", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oPSCATCOM_1_2.RadioValue()
    return(iif(this.value =1,100,;
    iif(this.value =2,70,;
    iif(this.value =3,50,;
    iif(this.value =4,25,;
    0)))))
  endfunc
  func oPSCATCOM_1_2.GetRadio()
    this.Parent.oContained.w_PSCATCOM = this.RadioValue()
    return .t.
  endfunc

  func oPSCATCOM_1_2.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PSCATCOM==100,1,;
      iif(this.Parent.oContained.w_PSCATCOM==70,2,;
      iif(this.Parent.oContained.w_PSCATCOM==50,3,;
      iif(this.Parent.oContained.w_PSCATCOM==25,4,;
      0))))
  endfunc

  func oPSCATCOM_1_2.mHide()
    with this.Parent.oContained
      return (g_CPIN='S')
    endwith
  endfunc


  add object oPSCODVAL_1_5 as StdCombo with uid="KIPWMQHLMB",rtseq=3,rtrep=.f.,left=175,top=41,width=44,height=22;
    , HelpContextID = 27915074;
    , cFormVar="w_PSCODVAL",RowSource=""+"4,"+"3,"+"2,"+"1", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oPSCODVAL_1_5.RadioValue()
    return(iif(this.value =1,100,;
    iif(this.value =2,70,;
    iif(this.value =3,50,;
    iif(this.value =4,25,;
    0)))))
  endfunc
  func oPSCODVAL_1_5.GetRadio()
    this.Parent.oContained.w_PSCODVAL = this.RadioValue()
    return .t.
  endfunc

  func oPSCODVAL_1_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PSCODVAL==100,1,;
      iif(this.Parent.oContained.w_PSCODVAL==70,2,;
      iif(this.Parent.oContained.w_PSCODVAL==50,3,;
      iif(this.Parent.oContained.w_PSCODVAL==25,4,;
      0))))
  endfunc

  func oPSCODVAL_1_5.mHide()
    with this.Parent.oContained
      return (g_CPIN='S')
    endwith
  endfunc


  add object oPSCODNAZ_1_9 as StdCombo with uid="KOJVHETRPR",rtseq=5,rtrep=.f.,left=394,top=9,width=44,height=22;
    , HelpContextID = 162132816;
    , cFormVar="w_PSCODNAZ",RowSource=""+"4,"+"3,"+"2,"+"1", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oPSCODNAZ_1_9.RadioValue()
    return(iif(this.value =1,100,;
    iif(this.value =2,70,;
    iif(this.value =3,50,;
    iif(this.value =4,25,;
    0)))))
  endfunc
  func oPSCODNAZ_1_9.GetRadio()
    this.Parent.oContained.w_PSCODNAZ = this.RadioValue()
    return .t.
  endfunc

  func oPSCODNAZ_1_9.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PSCODNAZ==100,1,;
      iif(this.Parent.oContained.w_PSCODNAZ==70,2,;
      iif(this.Parent.oContained.w_PSCODNAZ==50,3,;
      iif(this.Parent.oContained.w_PSCODNAZ==25,4,;
      0))))
  endfunc

  func oPSCODNAZ_1_9.mHide()
    with this.Parent.oContained
      return (g_CPIN='S')
    endwith
  endfunc


  add object oPSCODPAG_1_11 as StdCombo with uid="ROOMXFSYXV",rtseq=6,rtrep=.f.,left=394,top=41,width=44,height=22;
    , HelpContextID = 195687229;
    , cFormVar="w_PSCODPAG",RowSource=""+"4,"+"3,"+"2,"+"1", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oPSCODPAG_1_11.RadioValue()
    return(iif(this.value =1,100,;
    iif(this.value =2,70,;
    iif(this.value =3,50,;
    iif(this.value =4,25,;
    0)))))
  endfunc
  func oPSCODPAG_1_11.GetRadio()
    this.Parent.oContained.w_PSCODPAG = this.RadioValue()
    return .t.
  endfunc

  func oPSCODPAG_1_11.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PSCODPAG==100,1,;
      iif(this.Parent.oContained.w_PSCODPAG==70,2,;
      iif(this.Parent.oContained.w_PSCODPAG==50,3,;
      iif(this.Parent.oContained.w_PSCODPAG==25,4,;
      0))))
  endfunc

  func oPSCODPAG_1_11.mHide()
    with this.Parent.oContained
      return (g_CPIN='S')
    endwith
  endfunc


  add object oBtn_1_12 as StdButton with uid="ZMIZZNTUDJ",left=339, top=75, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 149497114;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="JWIBHYOYEX",left=390, top=75, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 142208442;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oPSCODVAL_1_15 as StdCombo with uid="UBGDFQUALE",rtseq=7,rtrep=.f.,left=175,top=41,width=44,height=22;
    , ToolTipText = "Codice valuta";
    , HelpContextID = 27915074;
    , cFormVar="w_PSCODVAL",RowSource=""+"3,"+"2,"+"1", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oPSCODVAL_1_15.RadioValue()
    return(iif(this.value =1,35,;
    iif(this.value =2,30,;
    iif(this.value =3,25,;
    0))))
  endfunc
  func oPSCODVAL_1_15.GetRadio()
    this.Parent.oContained.w_PSCODVAL = this.RadioValue()
    return .t.
  endfunc

  func oPSCODVAL_1_15.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PSCODVAL==35,1,;
      iif(this.Parent.oContained.w_PSCODVAL==30,2,;
      iif(this.Parent.oContained.w_PSCODVAL==25,3,;
      0)))
  endfunc

  func oPSCODVAL_1_15.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S')
    endwith
  endfunc


  add object oPSCODNAZ_1_16 as StdCombo with uid="JEOJLUVECR",rtseq=8,rtrep=.f.,left=394,top=9,width=44,height=22;
    , ToolTipText = "Nazione";
    , HelpContextID = 162132816;
    , cFormVar="w_PSCODNAZ",RowSource=""+"3,"+"2,"+"1", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oPSCODNAZ_1_16.RadioValue()
    return(iif(this.value =1,35,;
    iif(this.value =2,30,;
    iif(this.value =3,25,;
    0))))
  endfunc
  func oPSCODNAZ_1_16.GetRadio()
    this.Parent.oContained.w_PSCODNAZ = this.RadioValue()
    return .t.
  endfunc

  func oPSCODNAZ_1_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PSCODNAZ==35,1,;
      iif(this.Parent.oContained.w_PSCODNAZ==30,2,;
      iif(this.Parent.oContained.w_PSCODNAZ==25,3,;
      0)))
  endfunc

  func oPSCODNAZ_1_16.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S')
    endwith
  endfunc


  add object oPSCODPAG_1_17 as StdCombo with uid="ANSDMZFALZ",rtseq=9,rtrep=.f.,left=394,top=41,width=44,height=22;
    , ToolTipText = "Codice pagamento";
    , HelpContextID = 195687229;
    , cFormVar="w_PSCODPAG",RowSource=""+"3,"+"2,"+"1", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oPSCODPAG_1_17.RadioValue()
    return(iif(this.value =1,35,;
    iif(this.value =2,30,;
    iif(this.value =3,25,;
    0))))
  endfunc
  func oPSCODPAG_1_17.GetRadio()
    this.Parent.oContained.w_PSCODPAG = this.RadioValue()
    return .t.
  endfunc

  func oPSCODPAG_1_17.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PSCODPAG==35,1,;
      iif(this.Parent.oContained.w_PSCODPAG==30,2,;
      iif(this.Parent.oContained.w_PSCODPAG==25,3,;
      0)))
  endfunc

  func oPSCODPAG_1_17.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S')
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="IZJSMAASRB",Visible=.t., Left=23, Top=12,;
    Alignment=1, Width=142, Height=18,;
    Caption="Categoria commerciale:"  ;
  , bGlobalFont=.t.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (g_CPIN='S')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="GQKPVBQTBF",Visible=.t., Left=23, Top=44,;
    Alignment=1, Width=142, Height=18,;
    Caption="Codice valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="UGETCEKUBW",Visible=.t., Left=23, Top=76,;
    Alignment=1, Width=142, Height=18,;
    Caption="Codice lingua:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="YBBWPKEKLF",Visible=.t., Left=245, Top=12,;
    Alignment=1, Width=142, Height=18,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="FFMWUHSFEE",Visible=.t., Left=245, Top=44,;
    Alignment=1, Width=142, Height=18,;
    Caption="Codice pagamento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_kps','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
