* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bgn                                                        *
*              Genera nominativi                                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_152]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-10                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bgn",oParentObject)
return(i_retval)

define class tgsar_bgn as StdBatch
  * --- Local variables
  w_CODCLI = space(15)
  w_CODICE = space(15)
  w_NOCODICE = space(20)
  w_NEWCOD = space(15)
  w_NEWDES = space(40)
  w_DESCLI = space(40)
  w_MSG = space(200)
  w_OK = .f.
  w_FLGNUM = space(1)
  w_NUM = 0
  w_NUM2 = 0
  w_APPO = space(50)
  w_FLGNCL = space(1)
  w_NCAR = 0
  w_oERRORLOG = .NULL.
  w_PERFIS = space(1)
  w_NOSOGGET = space(2)
  w_CHKIVA = space(1)
  w_DTOBSO = ctod("  /  /  ")
  * --- WorkFile variables
  OFF_NOMI_idx=0
  CONTI_idx=0
  PAR_OFFE_idx=0
  AZIENDA_idx=0
  NOM_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da Generazione Nominativi (GSOF_KGN)
    this.w_FLGNCL = " "
    this.w_FLGNUM = " "
    this.w_CODCLI = " "
    * --- Caricamento nuovo cliente offerte
    * --- Lettura tipo codifica cliente numerica/alfanumerica
    * --- Read from PAR_OFFE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "POFLCOOF,POCHKIVA"+;
        " from "+i_cTable+" PAR_OFFE where ";
            +"POCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        POFLCOOF,POCHKIVA;
        from (i_cTable) where;
            POCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLGNUM = NVL(cp_ToDate(_read_.POFLCOOF),cp_NullValue(_read_.POFLCOOF))
      this.w_CHKIVA = NVL(cp_ToDate(_read_.POCHKIVA),cp_NullValue(_read_.POCHKIVA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura flag numerico su cliente
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCFNUME"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCFNUME;
        from (i_cTable) where;
            AZCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLGNCL = NVL(cp_ToDate(_read_.AZCFNUME),cp_NullValue(_read_.AZCFNUME))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura valori del cliente in azienda da copiare nell'archivio clienti delle offerte
    vq_exec("QUERY\GSAR_KGN.VQR",this,"CLIENTI")
    if reccount("CLIENTI")=0 
      AH_ErrorMsg("Non ci sono nominativi da generare",48)
      i_retcode = 'stop'
      return
    endif
    * --- Crea il File delle Messaggistiche di Errore
    this.w_oERRORLOG=createobject("AH_ERRORLOG")
    SELECT CLIENTI
    GO TOP
    * --- Ciclo su tutti i clienti che non sono nei nominativi
    SCAN 
    this.w_OK = .T.
    this.w_CODICE = ALLTRIM(CLIENTI.ANCODICE)
    this.w_CODCLI = this.w_CODICE
    this.w_PERFIS = CLIENTI.ANPERFIS
    if this.w_PERFIS="S"
      this.w_NOSOGGET = "PF"
    else
      this.w_NOSOGGET = "EN"
    endif
    * --- Controllo sulla Partita IVA
    if this.w_CHKIVA="S" AND NOT EMPTY(ALLTRIM(CLIENTI.ANPARIVA)) 
      * --- Controllo su  partita iva
      * --- Read from OFF_NOMI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NODTOBSO"+;
          " from "+i_cTable+" OFF_NOMI where ";
              +"NOPARIVA = "+cp_ToStrODBC(ALLTRIM(CLIENTI.ANPARIVA));
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NODTOBSO;
          from (i_cTable) where;
              NOPARIVA = ALLTRIM(CLIENTI.ANPARIVA);
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DTOBSO = NVL(cp_ToDate(_read_.NODTOBSO),cp_NullValue(_read_.NODTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0 And (Empty(this.w_DTOBSO) Or this.w_DTOBSO>i_DATSYS)
        this.w_OK = .F.
        if this.oParentObject.w_Parametro="C"
          this.w_oERRORLOG.AddMsgLog("Verificare cliente: %1", this.w_CODICE)     
        else
          this.w_oERRORLOG.AddMsgLog("Verificare fornitore: %1", this.w_CODICE)     
        endif
        this.w_oERRORLOG.AddMsgLog("%1Esiste un nominativo con la stessa partita IVA", space(5))     
      endif
    endif
    * --- Se esiste gi� un nominativo con quella partita iva non viene considerato.
    if this.w_OK
      * --- Controllo se impostata codifica numerica nei parametri offerta
      if this.w_FLGNUM="S"
        * --- Nel caso di codifica numerica ricalcolo il progressivo
        this.w_NOCODICE = SPACE(20)
        i_Conn=i_TableProp[this.OFF_NOMI_IDX, 3]
        cp_NextTableProg(this, i_Conn, "PRNUNMOF", "i_codazi,w_NOCODICE")
        p_LL=LEN(ALLTRIM(p_NOM))
        * --- Ridimensiono il codice in base alla 'maschera'  impostata sui parametri offerte
        this.w_CODCLI = RIGHT(ALLTRIM(this.w_NOCODICE),p_LL)
      else
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_CODCLI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                NOCODICE = this.w_CODCLI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Se il codice � gi� presente
        * --- Codifica cliente maggiore o solita codifica
        if (LEN(this.w_CODCLI)>LEN(p_NOM) AND this.w_OK) OR i_Rows<>0
          this.w_NUM = 1
          this.w_NUM2 = 1
          this.w_OK = .F.
          this.w_NCAR = LEN(p_NOM)-1
          if this.w_FLGNCL="S"
            this.w_CODCLI = RIGHT(this.w_CODCLI,this.w_NCAR)+ALLTRIM(STR(this.w_NUM))
          else
            this.w_CODCLI = LEFT(this.w_CODCLI,this.w_NCAR)+ALLTRIM(STR(this.w_NUM))
          endif
          * --- Controllo sulla lunghezza del codice da creare
          do while !this.w_OK AND this.w_NCAR>=0
            * --- Read from OFF_NOMI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" OFF_NOMI where ";
                    +"NOCODICE = "+cp_ToStrODBC(this.w_CODCLI);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    NOCODICE = this.w_CODCLI;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Nel caso esista gi� la codifica ne viene costruita una
            if i_Rows<>0 AND this.w_NUM <= 999999
              this.w_NUM = this.w_NUM+1
              this.w_NUM2 = this.w_NUM2+1
              if this.w_NUM2=10
                this.w_NCAR = this.w_NCAR-1
                this.w_NUM2 = 0
              endif
              if this.w_FLGNCL="S"
                this.w_CODCLI = RIGHT(this.w_CODCLI,this.w_NCAR)+ALLTRIM(STR(this.w_NUM))
              else
                this.w_CODCLI = LEFT(this.w_CODCLI,this.w_NCAR)+ALLTRIM(STR(this.w_NUM))
              endif
            else
              this.w_OK = .T.
            endif
          enddo
        endif
        if !this.w_OK
          if this.oParentObject.w_Parametro="C"
            this.w_oERRORLOG.AddMsgLog("Verificare cliente: %1", this.w_CODICE)     
          else
            this.w_oERRORLOG.AddMsgLog("Verificare fornitore: %1", this.w_CODICE)     
          endif
          this.w_oERRORLOG.AddMsgLog("%1Non � stato possibile generare un nuovo codice", space(5))     
        endif
      endif
      if this.w_OK
        * --- Inserimento nuovo cliente offerte
        ah_Msg("Inserimento nominativo: %1",.T.,.F.,.F., this.w_CODCLI )
        * --- Insert into OFF_NOMI
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_NOMI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"NOCODCLI"+",NOTIPCLI"+",NODESCRI"+",NOCODICE"+",NOTIPNOM"+",NOINDIRI"+",NOINDI_2"+",NO___CAP"+",NOLOCALI"+",NOPROVIN"+",NONAZION"+",NOPARIVA"+",NOCODFIS"+",NOTELEFO"+",NOTELFAX"+",NOINDWEB"+",NO_EMAIL"+",NO_EMPEC"+",NODTINVA"+",NODTOBSO"+",NOCODZON"+",NOCODLIN"+",NOCODAGE"+",NOCODVAL"+",NODESCR2"+",NOSOGGET"+",NO_SESSO"+",NODATNAS"+",NOLOCNAS"+",NOPRONAS"+",NOCOGNOM"+",NO__NOME"+",NONUMCAR"+",NONUMCEL"+",NOCHKSTA"+",NOCHKMAI"+",NOCHKPEC"+",NOCHKFAX"+",NOCHKCPZ"+",NOCHKWWP"+",NOCHKPTL"+",NOCODSAL"+",NOCODBAN"+",NOCODBA2"+",NO_SKYPE"+",NO__NOTE"+",NOCODPAG"+",NONUMLIS"+",NOCATCOM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CODICE),'OFF_NOMI','NOCODCLI');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_Parametro),'OFF_NOMI','NOTIPCLI');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANDESCRI)),'OFF_NOMI','NODESCRI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODCLI),'OFF_NOMI','NOCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_Parametro),'OFF_NOMI','NOTIPNOM');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANINDIRI)),'OFF_NOMI','NOINDIRI');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANINDIR2)),'OFF_NOMI','NOINDI_2');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.AN___CAP)),'OFF_NOMI','NO___CAP');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANLOCALI)),'OFF_NOMI','NOLOCALI');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANPROVIN)),'OFF_NOMI','NOPROVIN');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANNAZION)),'OFF_NOMI','NONAZION');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANPARIVA)),'OFF_NOMI','NOPARIVA');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCODFIS)),'OFF_NOMI','NOCODFIS');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANTELEFO)),'OFF_NOMI','NOTELEFO');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANTELFAX)),'OFF_NOMI','NOTELFAX');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANINDWEB)),'OFF_NOMI','NOINDWEB');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.AN_EMAIL)),'OFF_NOMI','NO_EMAIL');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.AN_EMPEC)),'OFF_NOMI','NO_EMPEC');
          +","+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDTINVA),'OFF_NOMI','NODTINVA');
          +","+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDTOBSO),'OFF_NOMI','NODTOBSO');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCODZON)),'OFF_NOMI','NOCODZON');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCODLIN)),'OFF_NOMI','NOCODLIN');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCODAGE)),'OFF_NOMI','NOCODAGE');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCODVAL)),'OFF_NOMI','NOCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANDESCR2)),'OFF_NOMI','NODESCR2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NOSOGGET),'OFF_NOMI','NOSOGGET');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.AN_SESSO)),'OFF_NOMI','NO_SESSO');
          +","+cp_NullLink(cp_ToStrODBC(CLIENTI.ANDATNAS),'OFF_NOMI','NODATNAS');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANLOCNAS)),'OFF_NOMI','NOLOCNAS');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANPRONAS)),'OFF_NOMI','NOPRONAS');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCOGNOM)),'OFF_NOMI','NOCOGNOM');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.AN__NOME )),'OFF_NOMI','NO__NOME ');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANNUMCAR )),'OFF_NOMI','NONUMCAR');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANNUMCEL)),'OFF_NOMI','NONUMCEL');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCHKSTA)),'OFF_NOMI','NOCHKSTA');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCHKMAI)),'OFF_NOMI','NOCHKMAI');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCHKPEC)),'OFF_NOMI','NOCHKPEC');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCHKFAX )),'OFF_NOMI','NOCHKFAX  ');
          +","+cp_NullLink(cp_ToStrODBC("N"),'OFF_NOMI','NOCHKCPZ');
          +","+cp_NullLink(cp_ToStrODBC("N"),'OFF_NOMI','NOCHKWWP');
          +","+cp_NullLink(cp_ToStrODBC("N"),'OFF_NOMI','NOCHKPTL');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCODSAL)),'OFF_NOMI','NOCODSAL');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCODBAN)),'OFF_NOMI','NOCODBAN');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCODBA2)),'OFF_NOMI','NOCODBA2');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.AN_SKYPE)),'OFF_NOMI','NO_SKYPE');
          +","+cp_NullLink(cp_ToStrODBC(AN__NOTE),'OFF_NOMI','NO__NOTE');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCODPAG)),'OFF_NOMI','NOCODPAG');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANNUMLIS)),'OFF_NOMI','NONUMLIS');
          +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(CLIENTI.ANCATCOM)),'OFF_NOMI','NOCATCOM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'NOCODCLI',this.w_CODICE,'NOTIPCLI',this.oParentObject.w_Parametro,'NODESCRI',ALLTRIM(CLIENTI.ANDESCRI),'NOCODICE',this.w_CODCLI,'NOTIPNOM',this.oParentObject.w_Parametro,'NOINDIRI',ALLTRIM(CLIENTI.ANINDIRI),'NOINDI_2',ALLTRIM(CLIENTI.ANINDIR2),'NO___CAP',ALLTRIM(CLIENTI.AN___CAP),'NOLOCALI',ALLTRIM(CLIENTI.ANLOCALI),'NOPROVIN',ALLTRIM(CLIENTI.ANPROVIN),'NONAZION',ALLTRIM(CLIENTI.ANNAZION),'NOPARIVA',ALLTRIM(CLIENTI.ANPARIVA))
          insert into (i_cTable) (NOCODCLI,NOTIPCLI,NODESCRI,NOCODICE,NOTIPNOM,NOINDIRI,NOINDI_2,NO___CAP,NOLOCALI,NOPROVIN,NONAZION,NOPARIVA,NOCODFIS,NOTELEFO,NOTELFAX,NOINDWEB,NO_EMAIL,NO_EMPEC,NODTINVA,NODTOBSO,NOCODZON,NOCODLIN,NOCODAGE,NOCODVAL,NODESCR2,NOSOGGET,NO_SESSO,NODATNAS,NOLOCNAS,NOPRONAS,NOCOGNOM,NO__NOME ,NONUMCAR,NONUMCEL,NOCHKSTA,NOCHKMAI,NOCHKPEC,NOCHKFAX  ,NOCHKCPZ,NOCHKWWP,NOCHKPTL,NOCODSAL,NOCODBAN,NOCODBA2,NO_SKYPE,NO__NOTE,NOCODPAG,NONUMLIS,NOCATCOM &i_ccchkf. );
             values (;
               this.w_CODICE;
               ,this.oParentObject.w_Parametro;
               ,ALLTRIM(CLIENTI.ANDESCRI);
               ,this.w_CODCLI;
               ,this.oParentObject.w_Parametro;
               ,ALLTRIM(CLIENTI.ANINDIRI);
               ,ALLTRIM(CLIENTI.ANINDIR2);
               ,ALLTRIM(CLIENTI.AN___CAP);
               ,ALLTRIM(CLIENTI.ANLOCALI);
               ,ALLTRIM(CLIENTI.ANPROVIN);
               ,ALLTRIM(CLIENTI.ANNAZION);
               ,ALLTRIM(CLIENTI.ANPARIVA);
               ,ALLTRIM(CLIENTI.ANCODFIS);
               ,ALLTRIM(CLIENTI.ANTELEFO);
               ,ALLTRIM(CLIENTI.ANTELFAX);
               ,ALLTRIM(CLIENTI.ANINDWEB);
               ,ALLTRIM(CLIENTI.AN_EMAIL);
               ,ALLTRIM(CLIENTI.AN_EMPEC);
               ,CLIENTI.ANDTINVA;
               ,CLIENTI.ANDTOBSO;
               ,ALLTRIM(CLIENTI.ANCODZON);
               ,ALLTRIM(CLIENTI.ANCODLIN);
               ,ALLTRIM(CLIENTI.ANCODAGE);
               ,ALLTRIM(CLIENTI.ANCODVAL);
               ,ALLTRIM(CLIENTI.ANDESCR2);
               ,this.w_NOSOGGET;
               ,ALLTRIM(CLIENTI.AN_SESSO);
               ,CLIENTI.ANDATNAS;
               ,ALLTRIM(CLIENTI.ANLOCNAS);
               ,ALLTRIM(CLIENTI.ANPRONAS);
               ,ALLTRIM(CLIENTI.ANCOGNOM);
               ,ALLTRIM(CLIENTI.AN__NOME );
               ,ALLTRIM(CLIENTI.ANNUMCAR );
               ,ALLTRIM(CLIENTI.ANNUMCEL);
               ,ALLTRIM(CLIENTI.ANCHKSTA);
               ,ALLTRIM(CLIENTI.ANCHKMAI);
               ,ALLTRIM(CLIENTI.ANCHKPEC);
               ,ALLTRIM(CLIENTI.ANCHKFAX );
               ,"N";
               ,"N";
               ,"N";
               ,ALLTRIM(CLIENTI.ANCODSAL);
               ,ALLTRIM(CLIENTI.ANCODBAN);
               ,ALLTRIM(CLIENTI.ANCODBA2);
               ,ALLTRIM(CLIENTI.AN_SKYPE);
               ,AN__NOTE;
               ,ALLTRIM(CLIENTI.ANCODPAG);
               ,ALLTRIM(CLIENTI.ANNUMLIS);
               ,ALLTRIM(CLIENTI.ANCATCOM);
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        if UPPER( g_APPLICATION ) = "ADHOC REVOLUTION"
          * --- Insert into NOM_CONT
          i_nConn=i_TableProp[this.NOM_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsar2kgn",this.NOM_CONT_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
    endif
    ENDSCAN
    * --- Messaggio di chiusura
    AH_ErrorMsg("Generazione nominativi completata",64)
    * --- Lancia report
    this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
    * --- Chiusura cursori
    USE IN SELECT("__TMP__")
    USE IN SELECT("CLIENTI")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='PAR_OFFE'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='NOM_CONT'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
