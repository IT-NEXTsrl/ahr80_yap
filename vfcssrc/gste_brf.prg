* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_brf                                                        *
*              Emissione ri.BA / bonifici                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_163]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-03                                                      *
* Last revis.: 2016-11-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_brf",oParentObject)
return(i_retval)

define class tgste_brf as StdBatch
  * --- Local variables
  w_NUMDIS = space(10)
  w_Valuta = space(3)
  w_TIPCON = space(1)
  w_ValDec = 0
  w_DESCRI = space(35)
  w_ValSim = space(5)
  w_TIPCONTO = space(1)
  w_CODCON = space(15)
  w_TIPODIS = space(2)
  w_CONTORIF = space(15)
  w_CODCON = space(15)
  w_CodSia = space(5)
  w_INDIRI = space(35)
  w_PROVIN = space(2)
  w_CAP = space(8)
  w_CODDES = space(5)
  w_LOCALI = space(30)
  w_CODABI = space(5)
  w_NOMDES = space(40)
  w_CODCAB = space(5)
  w_CONCOR = space(15)
  w_APPO = 0
  w_CAMRIF = 0
  w_PTDATSCA = ctod("  /  /  ")
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PT_SEGNO = space(1)
  w_PTNUMPAR = space(31)
  w_PTCODVAL = space(3)
  w_PTSERIAL = space(10)
  w_PTNUMDOC = 0
  w_PTALFDOC = space(10)
  w_PTDATDOC = ctod("  /  /  ")
  w_DTOBSO = ctod("  /  /  ")
  w_DATVAL = ctod("  /  /  ")
  w_DTOBS1 = ctod("  /  /  ")
  w_DISTINT = space(10)
  w_NUMERO = 0
  w_ANNO = space(4)
  w_COBANC = space(15)
  w_TIPOBAN = space(1)
  w_TIPOPAG = space(1)
  w_PATHFILE = space(254)
  w_PATHFILE1 = space(254)
  w_PATHFILE2 = space(254)
  w_BADESC = space(35)
  w_POSIZ = 0
  w_FILE = space(254)
  w_CINABI = space(1)
  w_IBAN = space(35)
  w_CODISO = space(3)
  w_CODCAU = space(5)
  w_CAUDIS = space(5)
  w_CINEUR = space(2)
  w_DESCRIZ = space(150)
  w_DESCRIZI = space(254)
  w_CAFLDSPR = space(1)
  w_CADESPAR = space(254)
  w_DATSTA = ctod("  /  /  ")
  w_PNAGG_01 = space(15)
  w_PNAGG_02 = space(15)
  w_PNAGG_03 = space(15)
  w_PNAGG_04 = space(15)
  w_PNAGG_05 = ctod("  /  /  ")
  w_PNAGG_06 = ctod("  /  /  ")
  w_PNNUMFAT = space(20)
  w_SERIALE = space(10)
  w_BLOCCO = .f.
  w_BAIDCRED = space(23)
  w_GENERA = .f.
  w_INSCFRB = space(20)
  w_DESPAR = space(254)
  w_CASTRUTT = space(10)
  w_FILDIS = space(20)
  w_OCCURS1 = 0
  w_OCCURS2 = 0
  w_OCCURS3 = 0
  w_OCCURS4 = 0
  w_OCCURS5 = 0
  w_OCCURS6 = 0
  w_OCCURS7 = 0
  w_MAXAGG = space(100)
  w_MINAGG = space(100)
  w_nFindVar = 0
  w_nFindVarFin = 0
  * --- WorkFile variables
  DIS_TINT_idx=0
  DES_DIVE_idx=0
  CONTI_idx=0
  VALUTE_idx=0
  COC_MAST_idx=0
  PAR_TITE_idx=0
  SCA_VARI_idx=0
  DIS_TINT_idx=0
  TIP_DIST_idx=0
  CAU_DIST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione file Ri.Ba/Bonifici/R.I.D./M.AV. (da GSTE_KRF)
    * --- Questo batch si occupa di creare un cursore con i dati necessari per la generazione
    * --- del file ascii delle Ri.Ba./Bonifici/R.I.D./M.AV.
    * --- L'effettiva scrittura del file ascii viene poi effettuata dal batch GSTE_BRW per le Ri.Ba..
    * --- L'effettiva scrittura del file ascii viene poi effettuata dal batch GSTE_BRX per i Bonifici.
    * --- L'effettiva scrittura del file ascii viene poi effettuata dal batch GSTE_BRR per le R.I.D..
    * --- L'effettiva scrittura del file ascii viene poi effettuata dal batch GSTE_BRM per le M.AV..
    * --- L'effettiva scrittura del file ascii viene poi effettuata dal batch GSTE_BRE per il Bonifico Estero..
    * --- Variabili della maschera
    * --- Variabili locali
    this.w_CODISO = g_ISONAZ
    this.w_POSIZ = AT("\", IIF(NOT EMPTY(this.oParentObject.w_NOMEFILE5) , this.oParentObject.w_NOMEFILE5, this.oParentObject.w_NOMEFILE2))
    if (EMPTY(this.oParentObject.w_NOMEFILE2) AND EMPTY(this.oParentObject.w_NOMEFILE6)) OR this.w_POSIZ<>3
      ah_ErrorMsg("Inserire un path valido",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Lettura dati distinta
    this.w_PATHFILE = IIF(NOT EMPTY(this.oParentObject.w_NOMEFILE5) , this.oParentObject.w_NOMEFILE5, this.oParentObject.w_NOMEFILE2)
    this.w_PATHFILE1 = this.oParentObject.w_NOMEFILE4
    this.w_PATHFILE2 = this.oParentObject.w_NOMEFILE7
    this.w_NUMDIS = this.oParentObject.w_DISERIAL
    this.w_SERIALE = this.oParentObject.w_DISERIAL
    this.w_NUMERO = this.oParentObject.w_NUMERO1
    this.w_ANNO = this.oParentObject.w_ANNO1
    this.w_COBANC = this.oParentObject.w_BANCA
    this.w_TIPOBAN = this.oParentObject.w_TIPBAN
    this.w_TIPOPAG = this.oParentObject.w_TIPPAG
    this.w_BADESC = this.oParentObject.w_DESBAN
    this.w_FILE = ALLTRIM(this.oParentObject.w_NOMEFILE) + IIF(EMPTY(ALLTRIM(this.oParentObject.w_NOMEFILE5)), ALLTRIM(this.oParentObject.w_NOMEFILE4), ALLTRIM(this.oParentObject.w_NOMEFILE7))
    if this.oParentObject.w_TIPDIS $ "SD-SC-SE"
      this.w_FILE = FORCEEXT(ADDBS(JUSTPATH(Alltrim(this.w_FILE)))+Juststem(Alltrim(this.w_FILE)),"xml")
    endif
    this.w_INSCFRB = ""
    * --- Read from DIS_TINT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIS_TINT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DIBANRIF,DICODVAL,DITIPDIS,DINUMERO,DI__ANNO,DICAUBON,DINSCFRB"+;
        " from "+i_cTable+" DIS_TINT where ";
            +"DINUMDIS = "+cp_ToStrODBC(this.w_NUMDIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DIBANRIF,DICODVAL,DITIPDIS,DINUMERO,DI__ANNO,DICAUBON,DINSCFRB;
        from (i_cTable) where;
            DINUMDIS = this.w_NUMDIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CONTORIF = NVL(cp_ToDate(_read_.DIBANRIF),cp_NullValue(_read_.DIBANRIF))
      this.w_Valuta = NVL(cp_ToDate(_read_.DICODVAL),cp_NullValue(_read_.DICODVAL))
      this.w_TIPODIS = NVL(cp_ToDate(_read_.DITIPDIS),cp_NullValue(_read_.DITIPDIS))
      this.w_NUMERO = NVL(cp_ToDate(_read_.DINUMERO),cp_NullValue(_read_.DINUMERO))
      this.w_ANNO = NVL(cp_ToDate(_read_.DI__ANNO),cp_NullValue(_read_.DI__ANNO))
      this.w_CAUDIS = NVL(cp_ToDate(_read_.DICAUBON),cp_NullValue(_read_.DICAUBON))
      this.w_INSCFRB = NVL(cp_ToDate(_read_.DINSCFRB),cp_NullValue(_read_.DINSCFRB))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from TIP_DIST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DIST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DIST_idx,2],.t.,this.TIP_DIST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DIDESCRI"+;
        " from "+i_cTable+" TIP_DIST where ";
            +"DICODISO = "+cp_ToStrODBC(this.w_CODISO);
            +" and DITIPDIS = "+cp_ToStrODBC(this.w_TIPODIS);
            +" and DICAUDIS = "+cp_ToStrODBC(this.w_CAUDIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DIDESCRI;
        from (i_cTable) where;
            DICODISO = this.w_CODISO;
            and DITIPDIS = this.w_TIPODIS;
            and DICAUDIS = this.w_CAUDIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESCRIZ = NVL(cp_ToDate(_read_.DIDESCRI),cp_NullValue(_read_.DIDESCRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CAU_DIST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_DIST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_DIST_idx,2],.t.,this.CAU_DIST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CADESPAR,CASTRUTT"+;
        " from "+i_cTable+" CAU_DIST where ";
            +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_DICAURIF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CADESPAR,CASTRUTT;
        from (i_cTable) where;
            CACODICE = this.oParentObject.w_DICAURIF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DESPAR = NVL(cp_ToDate(_read_.CADESPAR),cp_NullValue(_read_.CADESPAR))
      this.w_CASTRUTT = NVL(cp_ToDate(_read_.CASTRUTT),cp_NullValue(_read_.CASTRUTT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Parametri da passare alla query per determinare la descrizione parametrica
    this.w_OCCURS1 = OCCURS("%1", this.w_DESPAR)
    this.w_OCCURS2 = OCCURS("%2", this.w_DESPAR)
    this.w_OCCURS3 = OCCURS("%3", this.w_DESPAR)
    this.w_OCCURS4 = OCCURS("%4", this.w_DESPAR)
    this.w_OCCURS5 = OCCURS("%5", this.w_DESPAR)
    this.w_OCCURS6 = OCCURS("%6", this.w_DESPAR)
    this.w_OCCURS7 = OCCURS("%7", this.w_DESPAR)
    * --- Richiesta azzeramento file esistente
    if file(ALLTRIM(this.w_File))
      if NOT ah_YesNo("Il file %1 � gi� presente, si desidera sovrascriverlo?","", alltrim(this.w_File) )
        i_retcode = 'stop'
        return
      else
        if this.oParentObject.w_TIPDIS $ "SC-SE-SD"
          * --- Nel caso di XML elimino il file
          Delete File alltrim(this.w_File) 
        endif
      endif
    endif
    * --- Verifica congruenza tipo distinta
    if this.w_TIPODIS<>"RB" AND this.w_TIPODIS<>"BO" AND this.w_TIPODIS<>"RI" AND this.w_TIPODIS<>"MA" AND this.w_TIPODIS<>"BE" AND ! this.w_TIPODIS $ "SC-SD-SE"
      ah_ErrorMsg("Distinta di tipo non congruente",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Lettura conto collegato
    if EMPTY(this.w_CONTORIF)
      ah_ErrorMsg("Manca il conto banca collegato",,"")
      i_retcode = 'stop'
      return
    else
      * --- Read from COC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "BADESCRI,BACODSIA,BACODABI,BACODCAB,BACONCOR,BACINABI,BA__IBAN,BACINEUR,BAIDCRED"+;
          " from "+i_cTable+" COC_MAST where ";
              +"BACODBAN = "+cp_ToStrODBC(this.w_CONTORIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          BADESCRI,BACODSIA,BACODABI,BACODCAB,BACONCOR,BACINABI,BA__IBAN,BACINEUR,BAIDCRED;
          from (i_cTable) where;
              BACODBAN = this.w_CONTORIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DESCRI = NVL(cp_ToDate(_read_.BADESCRI),cp_NullValue(_read_.BADESCRI))
        this.w_CodSia = NVL(cp_ToDate(_read_.BACODSIA),cp_NullValue(_read_.BACODSIA))
        this.w_CODABI = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
        this.w_CODCAB = NVL(cp_ToDate(_read_.BACODCAB),cp_NullValue(_read_.BACODCAB))
        this.w_CONCOR = NVL(cp_ToDate(_read_.BACONCOR),cp_NullValue(_read_.BACONCOR))
        this.w_CINABI = NVL(cp_ToDate(_read_.BACINABI),cp_NullValue(_read_.BACINABI))
        this.w_IBAN = NVL(cp_ToDate(_read_.BA__IBAN),cp_NullValue(_read_.BA__IBAN))
        this.w_CINEUR = NVL(cp_ToDate(_read_.BACINEUR),cp_NullValue(_read_.BACINEUR))
        this.w_BAIDCRED = NVL(cp_ToDate(_read_.BAIDCRED),cp_NullValue(_read_.BAIDCRED))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_TIPODIS="RB"
        * --- Controllo su codice banca a cui devono essere inviate le disposizioni
        if EMPTY(this.w_CODABI)
          ah_ErrorMsg("Codice ABI della banca di presentazione non inserito",,"")
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.w_CODCAB)
          ah_ErrorMsg("Codice CAB della banca di presentazione non inserito",,"")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    * --- Lettura simbolo valuta
    this.w_APPO = 0
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL,VADECTOT,VACAOVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_Valuta);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL,VADECTOT,VACAOVAL;
        from (i_cTable) where;
            VACODVAL = this.w_Valuta;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ValSim = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      this.w_ValDec = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_APPO = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_APPO = 0
      if this.w_TIPODIS<>"BE" AND this.w_TIPODIS<>"SE" 
        ah_ErrorMsg("Distinta in valuta extra EMU impossibile generare il file",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Se sulla Maschera imposto il Flag Importi in euro ma la Distinta e' gia' in EURO o e' extra EMU non deve fare nulla
    this.oParentObject.w_FLEURO = IIF(this.w_Valuta=g_PERVAL, " ", this.oParentObject.w_FLEURO)
    this.w_CAMRIF = 1
    if this.oParentObject.w_FLEURO="S"
      * --- Coverto I dati Valuta in Euro
      this.w_Valuta = g_PERVAL
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VASIMVAL,VADECTOT"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_Valuta);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VASIMVAL,VADECTOT;
          from (i_cTable) where;
              VACODVAL = this.w_Valuta;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ValSim = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
        this.w_ValDec = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      if this.w_VALUTA<>g_PERVAL
        * --- Se Valuta EMU ma non Lire o EURO Converte in Valuta di Conto 
        this.w_Valuta = g_PERVAL
        this.w_CAMRIF = g_CAOVAL
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VASIMVAL,VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_Valuta);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VASIMVAL,VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_Valuta;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ValSim = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
          this.w_ValDec = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_FLEURO = "S"
      endif
    endif
    ah_Msg("Elaborazione dati distinta...",.T.)
    * --- Creazione cursore Effetti (scadenze)
    VQ_EXEC("QUERY\GSTE_BRF.VQR",this,"GENERA")
    select GENERA
    go top
    scan
    REPLACE BACODABI with this.w_CODABI
    REPLACE BACODCAB with this.w_CODCAB
    REPLACE BACONCOR with this.w_CONCOR
    REPLACE BACINABI with this.w_CINABI
    REPLACE BA__IBAN with this.w_IBAN
    REPLACE BACINEUR with this.w_CINEUR
    REPLACE BAIDCRED with this.w_BAIDCRED
    * --- Se Fatture raggruppate, Verifica esistenza numero Documento 
    if this.w_TIPODIS="RB" AND NVL(PTFLRAGG, " ")="S" AND NVL(PNNUMDOC, 0)=0
      this.w_PTDATSCA = CP_TODATE(PTDATSCA)
      this.w_PTTIPCON = NVL(PNTIPCON, " ")
      this.w_PTCODCON = NVL(CODCLIEN, SPACE(15))
      this.w_PT_SEGNO = IIF(NVL(PT_SEGNO, " ")="D", "A", "D")
      this.w_PTNUMPAR = NVL(PTNUMPAR, SPACE(31))
      this.w_PTCODVAL = NVL(PTCODVAL, "   ")
      this.w_PTNUMDOC = NVL(PNNUMDOC, 0)
      this.w_PTALFDOC = NVL(PNALFDOC, Space(10))
      this.w_PTDATDOC = CP_TODATE(PNDATDOC)
      this.w_PTSERIAL = SPACE(10)
      * --- Select from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
            +" where PTROWORD=-1 AND PTNUMPAR="+cp_ToStrODBC(this.w_PTNUMPAR)+" AND PTTIPCON="+cp_ToStrODBC(this.w_PTTIPCON)+" AND PTCODCON="+cp_ToStrODBC(this.w_PTCODCON)+" AND PT_SEGNO="+cp_ToStrODBC(this.w_PT_SEGNO)+" AND PTFLRAGG='S'";
             ,"_Curs_PAR_TITE")
      else
        select * from (i_cTable);
         where PTROWORD=-1 AND PTNUMPAR=this.w_PTNUMPAR AND PTTIPCON=this.w_PTTIPCON AND PTCODCON=this.w_PTCODCON AND PT_SEGNO=this.w_PT_SEGNO AND PTFLRAGG="S";
          into cursor _Curs_PAR_TITE
      endif
      if used('_Curs_PAR_TITE')
        select _Curs_PAR_TITE
        locate for 1=1
        do while not(eof())
        if CP_TODATE(_Curs_PAR_TITE.PTDATSCA)=this.w_PTDATSCA AND NVL(_Curs_PAR_TITE.PTCODVAL, "   ")=this.w_PTCODVAL
          this.w_PTSERIAL = NVL(PTSERIAL, SPACE(10))
        endif
          select _Curs_PAR_TITE
          continue
        enddo
        use
      endif
      if NOT EMPTY(this.w_PTSERIAL)
        * --- Read from SCA_VARI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SCA_VARI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2],.t.,this.SCA_VARI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SCNUMDOC,SCALFDOC,SCDATDOC"+;
            " from "+i_cTable+" SCA_VARI where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_PTSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SCNUMDOC,SCALFDOC,SCDATDOC;
            from (i_cTable) where;
                SCCODICE = this.w_PTSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PTNUMDOC = NVL(cp_ToDate(_read_.SCNUMDOC),cp_NullValue(_read_.SCNUMDOC))
          this.w_PTALFDOC = NVL(cp_ToDate(_read_.SCALFDOC),cp_NullValue(_read_.SCALFDOC))
          this.w_PTDATDOC = NVL(cp_ToDate(_read_.SCDATDOC),cp_NullValue(_read_.SCDATDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        select GENERA
        REPLACE PNNUMDOC with this.w_PTNUMDOC, PNALFDOC with this.w_PTALFDOC, PNDATDOC with this.w_PTDATDOC
      endif
      select GENERA
    endif
    this.w_CAFLDSPR = NVL(CAFLDSPR,"")
    this.w_CADESPAR = NVL(CADESPAR,254)
    if NVL(this.w_CAFLDSPR, " ")="S"
      this.w_DESCRIZI = this.w_CADESPAR
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if NOT EMPTY(this.w_DESCRIZI)
        Replace DESCRIZ with this.w_DESCRIZI
      endif
    endif
    this.w_CODCAU = NVL(CACODCAU, SPACE(5))
    * --- Controllo se il cliente/fornitore ha una sede di tipo 'PAGAMENTO'
    this.w_DTOBSO = NVL(CP_TODATE(DDDTOBSO),cp_CharToDate("  -  -  "))
    this.w_DATVAL = NVL(CP_TODATE(DIDATVAL),cp_CharToDate("  -  -  "))
    do case
      case NVL(DDTIPRIF,"")="PA" AND (this.w_DTOBSO>this.w_DATVAL OR EMPTY(this.w_DTOBSO))
        * --- Sostituisco al valore dei campi dell'anagrafica del cliente il valore presente nelle sedi.
        REPLACE ANDESCRI with DDNOMDES
        REPLACE ANINDIRI with DDINDIRI
        REPLACE AN___CAP with DD___CAP
        REPLACE ANLOCALI with DDLOCALI
        REPLACE ANPROVIN with DDPROVIN
      case NVL(DDTIPRIF,"")<>"PA" OR (this.w_DTOBSO<=this.w_DATVAL AND NOT EMPTY(this.w_DTOBSO))
        this.w_TIPCONTO = PNTIPCON
        this.w_CODCON = CODCLIEN
        * --- Effettuo una SELECT all'interno delle Destinazioni Diverse per poter vedere se ci sono delle sedi di tipo Fatturazione
        this.w_CODDES = SPACE(5)
        * --- Select from DES_DIVE
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DES_DIVE ";
              +" where DDTIPCON="+cp_ToStrODBC(this.w_TIPCONTO)+" AND DDCODICE="+cp_ToStrODBC(this.w_CODCON)+" AND DDTIPRIF='FA'";
               ,"_Curs_DES_DIVE")
        else
          select * from (i_cTable);
           where DDTIPCON=this.w_TIPCONTO AND DDCODICE=this.w_CODCON AND DDTIPRIF="FA";
            into cursor _Curs_DES_DIVE
        endif
        if used('_Curs_DES_DIVE')
          select _Curs_DES_DIVE
          locate for 1=1
          do while not(eof())
          this.w_NOMDES = NVL(DDNOMDES,SPACE(40))
          this.w_INDIRI = NVL(DDINDIRI,SPACE(35))
          this.w_CAP = NVL(DD___CAP,SPACE(8))
          this.w_LOCALI = NVL(DDLOCALI, SPACE(30))
          this.w_PROVIN = NVL(DDPROVIN,SPACE(2))
          this.w_CODDES = NVL(DDCODDES,SPACE(5))
          this.w_DTOBS1 = NVL(CP_TODATE(DDDTOBSO),cp_CharToDate("  -  -  "))
            select _Curs_DES_DIVE
            continue
          enddo
          use
        endif
        * --- Se esiste almeno un record effettuo delle replace sui dati anagrafici del cliente
        SELECT GENERA
        if NOT EMPTY(NVL(this.w_CODDES,"")) AND (this.w_DTOBS1 > this.w_DATVAL OR EMPTY(this.w_DTOBS1)) 
          REPLACE ANDESCRI with this.w_NOMDES
          REPLACE ANINDIRI with this.w_INDIRI
          REPLACE AN___CAP with this.w_CAP
          REPLACE ANLOCALI with this.w_LOCALI
          REPLACE ANPROVIN with this.w_PROVIN
        endif
    endcase
    * --- Inverte Segno di PTTOTIMP se Bonifico a Fornitore
    if this.w_TIPODIS $ "BO-BE-SC-SE"
      REPLACE PTTOTIMP WITH NVL(PTTOTIMP,0) * -1
    endif
    * --- Converte gli Importi EMU in EURO
    if this.oParentObject.w_FLEURO="S" AND NOT EMPTY(NVL(PTCODVAL, " ")) AND NVL(VACAOVAL, 0)<>0
      this.w_APPO = IIF(PTCODVAL=g_PERVAL, 1, VACAOVAL)
      REPLACE PTTOTIMP WITH VAL2MON(PTTOTIMP, this.w_APPO,1, GETVALUT(PTCODVAL,"VADATEUR"),g_PERVAL)
      REPLACE PNTOTDOC WITH VAL2MON(PNTOTDOC, this.w_APPO,1, GETVALUT(PTCODVAL,"VADATEUR"),g_PERVAL)
    endif
    endscan
    SELECT * FROM GENERA INTO CURSOR __TMP__ ORDER BY 1
    if used("GENERA")
      select Genera
      use
    endif
    if reccount("__tmp__") > 0
      do case
        case this.w_TIPODIS="RB"
          if this.w_CODISO="ITA"
            * --- Scrittura file ascii RI.BA.
            do GSTE_BRW with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            if this.w_CAUDIS="00004"
              * --- Anticipos de efectos
              do GLES_BRA with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Remesas de efectos
              do GLES_BRD with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          if !this.w_BLOCCO AND !this.w_GENERA
            * --- Write into DIS_TINT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DIS_TINT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_TINT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DI__PATH ="+cp_NullLink(cp_ToStrODBC(this.w_FILE),'DIS_TINT','DI__PATH');
              +",DINSCFRB ="+cp_NullLink(cp_ToStrODBC(this.w_FILDIS),'DIS_TINT','DINSCFRB');
                  +i_ccchkf ;
              +" where ";
                  +"DINUMDIS = "+cp_ToStrODBC(this.w_NUMDIS);
                  +" and DINUMERO = "+cp_ToStrODBC(this.w_NUMERO);
                     )
            else
              update (i_cTable) set;
                  DI__PATH = this.w_FILE;
                  ,DINSCFRB = this.w_FILDIS;
                  &i_ccchkf. ;
               where;
                  DINUMDIS = this.w_NUMDIS;
                  and DINUMERO = this.w_NUMERO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        case this.w_TIPODIS $ "BO-SC"
          if this.w_CODISO="ITA" or ( this.w_CODISO="ESP" AND this.w_TIPODIS="SC")
            * --- Scrittura file ascii Bonifici
            do GSTE_BRX with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Transferencias, n�minas, cheques y pagar�s (nacionales)
            do GLES_BRX with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if !this.w_BLOCCO AND !this.w_GENERA
            * --- Write into DIS_TINT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DIS_TINT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_TINT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DI__PATH ="+cp_NullLink(cp_ToStrODBC(this.w_FILE),'DIS_TINT','DI__PATH');
              +",DINSCFRB ="+cp_NullLink(cp_ToStrODBC(this.w_FILDIS),'DIS_TINT','DINSCFRB');
                  +i_ccchkf ;
              +" where ";
                  +"DINUMDIS = "+cp_ToStrODBC(this.w_NUMDIS);
                  +" and DINUMERO = "+cp_ToStrODBC(this.w_NUMERO);
                     )
            else
              update (i_cTable) set;
                  DI__PATH = this.w_FILE;
                  ,DINSCFRB = this.w_FILDIS;
                  &i_ccchkf. ;
               where;
                  DINUMDIS = this.w_NUMDIS;
                  and DINUMERO = this.w_NUMERO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.oParentObject.w_DI__PATH = this.w_FILE
          endif
        case this.w_TIPODIS $ "RI-SD"
          if this.w_CODISO="ITA" or ( this.w_CODISO="ESP" AND this.w_TIPODIS="SD")
            * --- Scrittura file ascii R.I.D.
            do GSTE_BRR with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Recibos domiciliados a la vista
            do GLES_BRZ with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if !this.w_BLOCCO AND !this.w_GENERA
            * --- Write into DIS_TINT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DIS_TINT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_TINT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DI__PATH ="+cp_NullLink(cp_ToStrODBC(this.w_FILE),'DIS_TINT','DI__PATH');
              +",DINSCFRB ="+cp_NullLink(cp_ToStrODBC(this.w_FILDIS),'DIS_TINT','DINSCFRB');
                  +i_ccchkf ;
              +" where ";
                  +"DINUMDIS = "+cp_ToStrODBC(this.w_NUMDIS);
                  +" and DINUMERO = "+cp_ToStrODBC(this.w_NUMERO);
                     )
            else
              update (i_cTable) set;
                  DI__PATH = this.w_FILE;
                  ,DINSCFRB = this.w_FILDIS;
                  &i_ccchkf. ;
               where;
                  DINUMDIS = this.w_NUMDIS;
                  and DINUMERO = this.w_NUMERO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.oParentObject.w_DI__PATH = this.w_FILE
          endif
        case this.w_TIPODIS="MA"
          * --- Scrittura file ascii M.AV.
          do GSTE_BRM with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.w_TIPODIS $ "BE-SE"
          * --- Bonifico Estero
          do GSTE_BRE with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if !this.w_BLOCCO AND !this.w_GENERA
            * --- Write into DIS_TINT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DIS_TINT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_TINT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DI__PATH ="+cp_NullLink(cp_ToStrODBC(this.w_FILE),'DIS_TINT','DI__PATH');
              +",DINSCFRB ="+cp_NullLink(cp_ToStrODBC(this.w_FILDIS),'DIS_TINT','DINSCFRB');
                  +i_ccchkf ;
              +" where ";
                  +"DINUMDIS = "+cp_ToStrODBC(this.w_NUMDIS);
                  +" and DINUMERO = "+cp_ToStrODBC(this.w_NUMERO);
                     )
            else
              update (i_cTable) set;
                  DI__PATH = this.w_FILE;
                  ,DINSCFRB = this.w_FILDIS;
                  &i_ccchkf. ;
               where;
                  DINUMDIS = this.w_NUMDIS;
                  and DINUMERO = this.w_NUMERO;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.oParentObject.w_DI__PATH = this.w_FILE
          endif
      endcase
      this.oParentObject.w_HTML_URL = this.w_FILE
    else
      ah_ErrorMsg("Per la selezione effettuata non esistono effetti da elaborare",,"")
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruisco descrizione parametrica
    this.w_nFindVar = AT("<", this.w_DESCRIZI)
    do while this.w_nFindVar>0
      this.w_nFindVarFin = AT(">", this.w_DESCRIZI)
      local w_VarCurs 
 w_VarCurs = "GENERA."+SUBSTR(this.w_DESCRIZI, this.w_nFindVar+1, this.w_nFindVarFin-this.w_nFindVar-1)
      do case
        case TYPE(w_VarCurs)="C"
          w_VarCurs = ALLTRIM( &w_VarCurs )
        case TYPE(w_VarCurs)="N"
          w_VarCurs = ALLTRIM( STR( &w_VarCurs ) )
        case TYPE(w_VarCurs)="D" OR TYPE(w_VarCurs)="T"
          w_VarCurs = ALLTRIM( DTOC( CP_TODATE( &w_VarCurs ) ) )
        otherwise
          w_VarCurs = ""
      endcase
      this.w_DESCRIZI = LEFT(this.w_DESCRIZI, this.w_nFindVar-1)+w_VarCurs+SUBSTR(this.w_DESCRIZI,this.w_nFindVarFin+1)
      this.w_nFindVar = AT("<", this.w_DESCRIZI)
    enddo
    this.w_PNAGG_01 = NVL(GENERA.PNAGG_01, space(15))
    this.w_PNAGG_02 = NVL(GENERA.PNAGG_02, space(15))
    this.w_PNAGG_03 = NVL(GENERA.PNAGG_03, space(15))
    this.w_PNAGG_04 = NVL(GENERA.PNAGG_04, space(15))
    this.w_PNAGG_05 = NVL(GENERA.PNAGG_05, cp_CharToDate("  -  -    "))
    this.w_PNAGG_06 = NVL(GENERA.PNAGG_06, cp_CharToDate("  -  -    "))
    this.w_PNNUMFAT = NVL(GENERA.PNNUMFAT,space(20))
    this.w_MAXAGG = ALLTRIM(NVL(GENERA.MAXAGG,""))
    this.w_MINAGG = ALLTRIM(NVL(GENERA.MINAGG,""))
    this.w_DESCRIZI = ah_msgformat(ALLTRIM(this.w_DESCRIZI),alltrim(this.w_PNAGG_01),alltrim(this.w_PNAGG_02),alltrim(this.w_PNAGG_03),alltrim(this.w_PNAGG_04),alltrim(dtoc(CP_TODATE(this.w_PNAGG_05))),alltrim(dtoc(CP_TODATE(this.w_PNAGG_06))),alltrim(this.w_PNNUMFAT))
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='DIS_TINT'
    this.cWorkTables[2]='DES_DIVE'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='COC_MAST'
    this.cWorkTables[6]='PAR_TITE'
    this.cWorkTables[7]='SCA_VARI'
    this.cWorkTables[8]='DIS_TINT'
    this.cWorkTables[9]='TIP_DIST'
    this.cWorkTables[10]='CAU_DIST'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
