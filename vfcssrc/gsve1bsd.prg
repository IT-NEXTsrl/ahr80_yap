* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve1bsd                                                        *
*              Elab. cursore stampa documenti                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRS_161][VRS_9]                                                *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-06                                                      *
* Last revis.: 2012-10-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR,w_MultiReportVarEnv,w_OTES,w_TIPDOC,w_CARMEM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve1bsd",oParentObject,m.w_CURSOR,m.w_MultiReportVarEnv,m.w_OTES,m.w_TIPDOC,m.w_CARMEM)
return(i_retval)

define class tgsve1bsd as StdBatch
  * --- Local variables
  w_CURSOR = space(254)
  w_MultiReportVarEnv = .NULL.
  w_OTES = .f.
  w_TIPDOC = space(5)
  w_CARMEM = 0
  w_MVFLVEAC = space(1)
  w_MVCLADOC = space(2)
  w_MVDATDOC = ctod("  /  /  ")
  w_CODCLA = space(3)
  w_CODSTA = space(3)
  w_OLDMAGA = space(5)
  w_OLDCAUSA = space(5)
  w_BIANCO = .f.
  w_OLDSER = space(10)
  w_LINGUA = space(3)
  w_CODICE = space(15)
  w_CODCON = space(15)
  w_DT = space(35)
  w_CODART = space(20)
  w_DESART = space(40)
  w_TIPCON = space(1)
  w_CODICE1 = space(20)
  w_DESSUP = space(0)
  w_PRIMAG = space(5)
  w_KEYOBS = ctod("  /  /  ")
  w_TIPCON1 = space(1)
  w_CODCON1 = space(15)
  w_DTOBS1 = ctod("  /  /  ")
  w_codic = space(15)
  w_nmdes = space(40)
  w_ind = space(40)
  w_cap = space(8)
  w_loca = space(30)
  w_prov = space(2)
  w_rif = space(2)
  w_PRIMAT = space(5)
  w_DESART1 = space(40)
  w_DESSUP1 = space(0)
  w_MVCODICE = space(20)
  w_LENSCF = 0
  w_MVDESART = space(40)
  w_KEYDESART = space(40)
  w_MVSERIAL = space(10)
  w_OLDMVSER = space(10)
  w_OLDREC = 0
  w_OKMAG = .f.
  w_CODMAG = space(5)
  w_CAUMAG = space(5)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  DES_DIVE_idx=0
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione cursore di stampa documenti
    * --- Try
    local bErr_03D4B950
    bErr_03D4B950=bTrsErr
    this.Try_03D4B950()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      i_retcode = 'stop'
      i_retval = ah_MsgFormat("La query in output utente non contiene uno o pi� campi obbligatori%0%1%0La stampa non pu� essere portata a termine", message() )
      return
    endif
    bTrsErr=bTrsErr or bErr_03D4B950
    * --- End
    * --- Passo queste variabili al report
    this.w_MultiReportVarEnv.AddVariable("L_ARTESE", "")     
    this.w_MultiReportVarEnv.AddVariable("L_DESOPE", "")     
    this.w_MultiReportVarEnv.AddVariable("L_DATINI", cp_CharToDate("  -  -  "))     
    this.w_MultiReportVarEnv.AddVariable("L_DATFIN", cp_CharToDate("  -  -  "))     
    this.w_MultiReportVarEnv.AddVariable("L_NDIC", 0)     
    this.w_MultiReportVarEnv.AddVariable("L_ADIC", "")     
    this.w_MultiReportVarEnv.AddVariable("L_DDIC", cp_CharToDate("  -  -  "))     
    this.w_MultiReportVarEnv.AddVariable("L_TIPOPER", "")     
    this.w_MultiReportVarEnv.AddVariable("L_IMPONI", 0)     
    this.w_MultiReportVarEnv.AddVariable("L_FLINCA", IIF(vartype(L_FLINCA)="C",L_FLINCA,iif(this.w_MVCLADOC="RF","S","")))     
    * --- Controllo se il cliente/fornitore imputato in testata del documento
    * --- ha il flag di codifica settato.Il controllo viene effettuato su un campo
    * --- che si trova nell'anagrafica Clienti/Fornitori (ANFLCODI)
    SELECT (this.w_CURSOR)
    GO TOP
    SCAN FOR NVL(ANFLCODI," ")="S" AND NVL(MVTIPRIG," ")<>"D"
    this.w_CODART = NVL(MVCODART,"")
    this.w_MVCODICE = NVL(MVCODICE," ")
    this.w_CODCON = MVCODCON
    this.w_MVDESART = NVL(MVDESART, SPACE(40) ) 
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CADESART"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_MVCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CADESART;
        from (i_cTable) where;
            CACODICE = this.w_MVCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_KEYDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TIPCON = MVTIPCON
    this.w_KEYOBS = CP_TODATE(MVDATREG)
    this.w_CODICE1 = SPACE(20)
    this.w_LENSCF = 0
    * --- Select from KEY_ARTI
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CACODICE,CADTOBSO,CALENSCF,CADESART  from "+i_cTable+" KEY_ARTI ";
          +" where CACODART= "+cp_ToStrODBC(this.w_CODART)+" AND CACODCON= "+cp_ToStrODBC(this.w_CODCON)+" AND CATIPCON= "+cp_ToStrODBC(this.w_TIPCON)+" AND CACODICE<> "+cp_ToStrODBC(this.w_CODART)+" ";
          +" order by CATIPBAR DESC,CALENSCF, CACODICE";
           ,"_Curs_KEY_ARTI")
    else
      select CACODICE,CADTOBSO,CALENSCF,CADESART from (i_cTable);
       where CACODART= this.w_CODART AND CACODCON= this.w_CODCON AND CATIPCON= this.w_TIPCON AND CACODICE<> this.w_CODART ;
       order by CATIPBAR DESC,CALENSCF, CACODICE;
        into cursor _Curs_KEY_ARTI
    endif
    if used('_Curs_KEY_ARTI')
      select _Curs_KEY_ARTI
      locate for 1=1
      do while not(eof())
      if (CP_TODATE(_Curs_KEY_ARTI.CADTOBSO) > this.w_KEYOBS) OR EMPTY(NVL(CP_TODATE(_Curs_KEY_ARTI.CADTOBSO)," "))
        this.w_CODICE1 = _Curs_KEY_ARTI.CACODICE 
        this.w_LENSCF = Nvl(_Curs_KEY_ARTI.CALENSCF,0)
        if this.w_MVDESART=this.w_KEYDESART
          this.w_DESART1 = NVL(_Curs_KEY_ARTI.CADESART,space(40))
        else
          * --- privilegia la descrizione modificata manualmente 
          this.w_DESART1 = this.w_MVDESART
        endif
        if this.w_CODICE1= this.w_MVCODICE
          * --- Nel caso sia stato trovato come codice di ricerca valido lo stesso usato sul documento
          *     esce dal ciclo e mantiene questo.
          EXIT
        endif
      endif
        select _Curs_KEY_ARTI
        continue
      enddo
      use
    endif
    SELECT (this.w_CURSOR)
    this.w_LINGUA = NVL(ANCODLIN,"   ")
    if NOT EMPTY(NVL(this.w_CODICE1,""))
       
 DECLARE ARRRIS (2) 
 Tradlin(this.w_CODICE1,this.w_LINGUA,"TRADARTI",@ARRRIS,"B") 
 this.w_DESART1=IIF(NOT EMPTY(ARRRIS(1)), ARRRIS(1), this.w_DESART1) 
 this.w_DESSUP1=ARRRIS(2)
      SELECT (this.w_CURSOR)
      if NOT EMPTY(NVL(this.w_DESART1," "))
        REPLACE MVDESART WITH this.w_DESART1
      endif
      if NOT EMPTY(NVL(this.w_DESSUP1," "))
        REPLACE MVDESSUP WITH this.w_DESSUP1
      endif
      * --- Se Attivo Flag Codifica Cliente/Fornitore Esclusiva in Azienda
      *     Prendo solo la prima parte del codice di ricerca (elimino il suffisso da destra)
      if g_FLCESC = "S" And this.w_LENSCF <> 0
        this.w_CODICE1 = Left(Alltrim(this.w_CODICE1),Len(Alltrim(this.w_CODICE1))-this.w_LENSCF)
      endif
      REPLACE mvcodice WITH this.w_CODICE1
    endif
    ENDSCAN
    * --- Marco ultima riga e nello stesso ciclo controllo obsolescenza della sede di
    *     fatturazione
    this.w_OLDSER = "ZXZXZXZXZX"
    SELECT (this.w_CURSOR)
    GO BOTTOM
    do while NOT BOF()
      if NOT DELETED()
        this.w_TIPCON1 = NVL(MVTIPCON," ")
        this.w_CODCON1 = NVL(MVCODCON,SPACE(15))
        this.w_DTOBS1 = cp_CharToDate("  -  -  ")
        this.w_codic = NVL(ANCODICE,SPACE(15))
        this.w_nmdes = NVL(ANDESCRI,SPACE(40))
        this.w_ind = NVL(ANINDIRI,SPACE(40))
        this.w_cap = NVL(AN___CAP,SPACE(8))
        this.w_loca = NVL(ANLOCALI,SPACE(30))
        this.w_prov = NVL(ANPROVIN,SPACE(2))
        this.w_rif = NVL(DDTIPRIF,SPACE(2))
        * --- Marca l'ultima riga del documento
        if MVSERIAL<>this.w_OLDSER
          REPLACE ULTRIG WITH "S"
          if this.w_RIF="FA"
            * --- Read from DES_DIVE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DES_DIVE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "DDDTOBSO"+;
                " from "+i_cTable+" DES_DIVE where ";
                    +"DDTIPCON = "+cp_ToStrODBC(this.w_TIPCON1);
                    +" and DDCODICE = "+cp_ToStrODBC(this.w_CODCON1);
                    +" and DDTIPRIF = "+cp_ToStrODBC("FA");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                DDDTOBSO;
                from (i_cTable) where;
                    DDTIPCON = this.w_TIPCON1;
                    and DDCODICE = this.w_CODCON1;
                    and DDTIPRIF = "FA";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DTOBS1 = NVL(cp_ToDate(_read_.DDDTOBSO),cp_NullValue(_read_.DDDTOBSO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.w_OLDSER = MVSERIAL
        endif
        * --- Controlla l'obsolescenza della sede di fatturazione 
        if this.w_RIF="FA"
          if this.w_DTOBS1<=this.w_MVDATDOC AND NOT EMPTY(this.w_DTOBS1)
            REPLACE DDCODDES WITH this.w_CODIC, DDNOMDES WITH this.w_NMDES, DDINDIRI WITH this.w_IND
            REPLACE DD___CAP WITH this.w_CAP, DDLOCALI WITH this.w_LOCA, DDPROVIN WITH this.w_PROV
          endif
        endif
      endif
      SKIP -1
    enddo
    * --- Cerca il bmp del logo con il nome del codice dell'azienda se non lo trova mette quello di default
    this.w_MultiReportVarEnv.AddVariable("L_LOGO", GETBMP(I_CODAZI))     
    if this.w_OTES
      * --- Lancio la funzione per divisione su pi� righe del campo memo della
      *     descrizione supplementare articolo solo se � stato definito il numero
      *     di caratteri per riga nell'output utente
      if this.w_CARMEM<>0
        * --- Lancio la funzione per la divisione del campo memo su pi� righe
        DIVIMEMO(this.w_CARMEM, this.w_CURSOR)
      endif
    endif
    * --- Tutto OK
    i_retcode = 'stop'
    i_retval = ""
    return
  endproc
  proc Try_03D4B950()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    on error return 
 i_Error="La stampa scelta non � valida"
    * --- Se lanciato da ristampa devo filtrare anche per serie
    if TYPE("This.oParentObject.w_SERIE1")<>"U" AND TYPE("This.oParentObject.w_SERIE2")<>"U"
      * --- Per non modificare le visual query, si filtra in questo modo
      if NOT EMPTY(NVL(This.oParentObject.w_SERIE1,""))
        DELETE FROM (this.w_CURSOR) WHERE MVALFDOC<This.oParentObject.w_SERIE1
      endif
      if NOT EMPTY(NVL(This.oParentObject.w_SERIE2,""))
        DELETE FROM (this.w_CURSOR) WHERE MVALFDOC>This.oParentObject.w_SERIE2
      endif
    endif
    if Isalt() and This.oParentObject.w_PRTIPCAU="N"
      this.w_MultiReportVarEnv.AddVariable("L_RifPrat", This.oParentObject.w_RifPra)     
      this.w_MultiReportVarEnv.AddVariable("L_RifOgg", This.oParentObject.w_RifOgg)     
      this.w_MultiReportVarEnv.AddVariable("L_RifVal", This.oParentObject.w_Rifval)     
      this.w_MultiReportVarEnv.AddVariable("L_RifDat", This.oParentObject.w_RifDat)     
      this.w_MultiReportVarEnv.AddVariable("L_RifSup", This.oParentObject.w_RifSup)     
      this.w_MultiReportVarEnv.AddVariable("L_RifRighe", This.oParentObject.w_RifRighe)     
      this.w_MultiReportVarEnv.AddVariable("L_Rifgu", This.oParentObject.w_Rifgu)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFMINMAX", This.oParentObject.w_RIFMINMAX)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFSCOFIN", This.oParentObject.w_RIFSCOFIN)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFDATDIR", This.oParentObject.w_RIFDATDIR)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFDATONO", This.oParentObject.w_RIFDATONO)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFDATTEM", This.oParentObject.w_RIFDATTEM)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFDATSPA", This.oParentObject.w_RIFDATSPA)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFQTA", This.oParentObject.w_RIFQTA)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFNOIMPO", This.oParentObject.w_RIFNOIMPO)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFCOEDIF", This.oParentObject.w_RIFCOEDIF)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFIMP", This.oParentObject.w_RIFIMP)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFDATGEN", This.oParentObject.w_RIFDATGEN)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFPARTI", This.oParentObject.w_RIFPARTI)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFRESTIT", This.oParentObject.w_RIFRESTIT)     
      this.w_MultiReportVarEnv.AddVariable("L_RIFRAGFAS", This.oParentObject.w_RIFRAGFAS)     
    endif
    this.w_TIPDOC = SubStr(this.w_TIPDOC,3,5)
    * --- Leggo MVCLADOC e MVFLVEAC
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDCATDOC,TDFLVEAC"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDCATDOC,TDFLVEAC;
        from (i_cTable) where;
            TDTIPDOC = this.w_TIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
      this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT (this.w_CURSOR)
    GO TOP
    if this.w_MVCLADOC="DT" OR this.w_MVCLADOC="DI" 
      this.w_OLDMVSER = "XXXYYYWWWK"
      this.w_OLDREC = 0
      this.w_OKMAG = .T.
      SELECT (this.w_CURSOR)
      GO TOP
      * --- Ciclo sul cursore
      do while NOT EOF()
        * --- Controllo per eseguire LOCATE una sola volta per ogni documento
        this.w_MVSERIAL = MVSERIAL
        if this.w_OLDMVSER<>this.w_MVSERIAL
          this.w_OKMAG = .T.
        endif
        * --- Segno seriale e posizione sul cursore
        this.w_OLDMVSER = this.w_MVSERIAL
        this.w_OLDREC = RECNO()
        this.w_PRIMAG = SPACE(5)
        this.w_PRIMAT = SPACE(5)
        this.w_CODCLA = NVL(MVCODCLA,"   ")
        this.w_CODSTA = NVL(TDSTACL1,"   ")+"|"+NVL(TDSTACL2,"   ")+"|"+NVL(TDSTACL3,"   ")
        this.w_CODSTA = this.w_CODSTA+"|"+NVL(TDSTACL3,"   ")+"|"+NVL(TDSTACL4,"   ")+"|"+NVL(TDSTACL5,"   ")
        this.w_BIANCO = .F.
        * --- Una volta per ogni documento
        if this.w_OKMAG
          LOCATE FOR NOT EMPTY(NVL(MVCODMAG," ")) AND MVSERIAL = this.w_OLDMVSER
          if FOUND()
            this.w_PRIMAG = NVL(MVTCOMAG, SPACE(5))
            * --- Scrive il primo codice magazzino valido
            SELECT (this.w_CURSOR)
            GO TOP
            REPLACE MVTCOMAG WITH this.w_PRIMAG FOR MVSERIAL=this.w_OLDMVSER
            * --- Metto OKMAG a .F. altrimenti nel ciclo vendite eseguirebbe la LOCATE per ogni record del cursore
            this.w_OKMAG = .F.
          endif
        endif
        * --- Solo per gli acquisti o documenti interni; nelle vendite non occorre modificare i magazzini sulla riga.
        if this.w_MVFLVEAC="A" OR this.w_MVCLADOC="DI" 
          * --- Mi riposiziono sul record giusto dopo aver eseguito la LOCATE
          * --- Metto il codice magazzino collegato uguale a quello principale cos� se � diverso viene riportato gi� dalla prima riga
          this.w_PRIMAT = this.w_PRIMAG
          SELECT (this.w_CURSOR)
          GOTO this.w_OLDREC
          do while this.w_OLDMVSER=MVSERIAL
            if MVTIPRIG="R" AND (EMPTY(this.w_CODCLA) OR NOT this.w_CODCLA $ this.w_CODSTA)
              this.w_CODMAG = MVCODMAG
              this.w_CAUMAG = MVCAUMAG
              * --- Se il magazzino della riga precedente � uguale all'attuale o l'attuale � vuoto e la causale non cambia
              if (this.w_OLDMAGA=this.w_CODMAG OR EMPTY(this.w_CODMAG)) AND this.w_OLDCAUSA=this.w_CAUMAG
                * --- Non devo stampare il magazzino e la causale di riga
                this.w_BIANCO = .T.
              else
                if this.w_OLDCAUSA<>this.w_CAUMAG
                  * --- Cambia la Causale devo stampare - non sbianco niente
                  this.w_OLDCAUSA = this.w_CAUMAG
                  this.w_BIANCO = .F.
                else
                  * --- La causale � uguale alla precedente, cambia il magazzino
                  * --- Se il magazzino cambia oppure l'attuale � vuoto
                  if this.w_OLDMAGA=this.w_CODMAG OR EMPTY(this.w_CODMAG)
                    this.w_BIANCO = .F.
                  else
                    * --- Se il magazzino non cambia e non � vuoto allora devo sbiancarlo
                    this.w_BIANCO = this.w_OLDMAGA=this.w_CODMAG
                  endif
                endif
              endif
              * --- Se il magazzino attuale non � vuoto lo assegno come magazzino precedente
              if NOT EMPTY(this.w_CODMAG)
                this.w_OLDMAGA = this.w_CODMAG
              endif
              this.w_OLDCAUSA = this.w_CAUMAG
              * --- Aggiorno i valori riga precedente e svuoto i dati da non stampare
              if this.w_BIANCO=.T.
                REPLACE mvcodmag WITH ""
                REPLACE mvcaumag WITH ""
              endif
            else
              * --- Se la riga non devo stamparla azzerro il magazzino e la causale
              REPLACE mvcodmag WITH ""
              REPLACE mvcaumag WITH ""
            endif
            * --- Se cambia il codice magazzino principale o collegato memorizzo i cambiamenti
            *     ma non vario i campi : comunque nella stampa devono essere memorizzati entrambi
            * --- Record successivo
            SELECT (this.w_CURSOR)
            SKIP
          enddo
        else
          * --- Nel ciclo vendite mi riposiziono sul record giusto dopo aver eseguito la LOCATE
          SELECT (this.w_CURSOR)
          GOTO this.w_OLDREC
          SKIP
        endif
      enddo
    endif
    i_Error="" 
 on error
    return


  proc Init(oParentObject,w_CURSOR,w_MultiReportVarEnv,w_OTES,w_TIPDOC,w_CARMEM)
    this.w_CURSOR=w_CURSOR
    this.w_MultiReportVarEnv=w_MultiReportVarEnv
    this.w_OTES=w_OTES
    this.w_TIPDOC=w_TIPDOC
    this.w_CARMEM=w_CARMEM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='DES_DIVE'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='TIP_DOCU'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR,w_MultiReportVarEnv,w_OTES,w_TIPDOC,w_CARMEM"
endproc
