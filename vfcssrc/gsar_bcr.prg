* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bcr                                                        *
*              Visualizza/modifica codici di ricerca                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-12                                                      *
* Last revis.: 2014-04-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bcr",oParentObject,m.pOPER)
return(i_retval)

define class tgsar_bcr as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_OBJECT = .NULL.
  w_OBJ = .NULL.
  w_CODART = space(20)
  * --- WorkFile variables
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Visualizza/modifica i codici di ricerca (KEY_ARTI)
    * --- Parametri pOPER
    *     V :  visualizza codice di ricerca
    *     N:  nuova tariffa
    *     T:  visualizza tariffa
    *     A: aggiornamento data obsolescenza
    do case
      case this.pOPER $ "VNT"
        * --- Istanzio l'anagrafica
        if this.pOper$"TN"
          * --- Interroga la tariffa
          this.w_OBJECT = GSMA_AAS()
        else
          * --- Interroga il codice di ricerca
          this.w_OBJECT = GSMA_ACA()
        endif
        * --- Controllo se ha passato il test di accesso
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        if this.pOper="N"
          * --- Vado in caricamento
          this.w_OBJECT.ECPLOAD()     
        else
          this.w_OBJECT.ECPFILTER()     
          if this.pOper="T"
            * --- Interroga la tariffa
            this.w_OBJECT.w_ARCODART = this.oParentObject.w_CACODART
          else
            * --- Interroga il codice di ricerca
            this.w_OBJECT.w_CACODICE = this.oParentObject.w_CACODICE
          endif
          this.w_OBJECT.ECPSAVE()     
          if this.pOper="T"
            do while Empty(this.w_OBJECT.w_ARCODART) or !(this.w_OBJECT.w_ARCODART == this.oParentObject.w_CACODART)
              this.w_OBJECT.ecpnext()     
            enddo
          endif
        endif
      case this.pOPER = "A"
        SELECT * FROM ( this.oParentObject.w_AGKCR_ZOOM.cCursor ) WHERE XCHK = 1 INTO CURSOR TARIFFE
        if RECCOUNT("TARIFFE") = 0
          ah_ErrorMsg("Attenzione: selezionare almeno una tariffa",,"")
          * --- Chiusura cursore
          if USED("TARIFFE")
            SELECT TARIFFE
            USE
          endif
          i_retcode = 'stop'
          return
        else
          SELECT TARIFFE
          GO TOP
          SCAN
          this.w_CODART = TARIFFE.CACODART
          * --- Try
          local bErr_03F29260
          bErr_03F29260=bTrsErr
          this.Try_03F29260()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03F29260
          * --- End
          ENDSCAN
          This.oparentobject.Notifyevent("Ricerca")
        endif
        * --- Chiusura cursore
        if USED("TARIFFE")
          SELECT TARIFFE
          USE
        endif
        ah_ErrorMsg("Aggiornamento ultimato",,"")
    endcase
  endproc
  proc Try_03F29260()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiorna la data di obsolescenza della tariffa
    * --- Write into ART_ICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ARDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATAOBSO),'ART_ICOL','ARDTOBSO');
          +i_ccchkf ;
      +" where ";
          +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
             )
    else
      update (i_cTable) set;
          ARDTOBSO = this.oParentObject.w_DATAOBSO;
          &i_ccchkf. ;
       where;
          ARCODART = this.w_CODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiorna la data di obsolescenza dei codici di ricerca associati alla tariffa
    * --- Write into KEY_ARTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CADTOBSO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATAOBSO),'KEY_ARTI','CADTOBSO');
          +i_ccchkf ;
      +" where ";
          +"CACODART = "+cp_ToStrODBC(this.w_CODART);
             )
    else
      update (i_cTable) set;
          CADTOBSO = this.oParentObject.w_DATAOBSO;
          &i_ccchkf. ;
       where;
          CACODART = this.w_CODART;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
