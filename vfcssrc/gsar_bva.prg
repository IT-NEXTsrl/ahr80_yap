* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bva                                                        *
*              Ultimi articoli venduti/acquistati                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_80]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-24                                                      *
* Last revis.: 2013-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bva",oParentObject,m.pOPER)
return(i_retval)

define class tgsar_bva as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_VALFISC = 0
  w_FLVEAC = space(1)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_FLULPV = space(1)
  w_FLULCA = space(1)
  w_ZOOM = space(10)
  w_OLDART = space(20)
  w_DESART = space(40)
  w_DATREG = ctod("  /  /  ")
  w_CODVAL = space(3)
  w_VALULT = 0
  w_UNIMIS = space(3)
  w_QTAMOV = 0
  w_SERIAL = space(10)
  w_CPROWNUM = 0
  w_NUMRIF = 0
  w_PREZZO = 0
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_FLOMAG = space(20)
  w_PREZZOSM = 0
  w_CODICE = space(20)
  w_VALUNI = 0
  w_DECUNI = 0
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora la situazione degli Ultimi Articoli Venduti a Cliente o Acquistati da Fornitore (da GSAR_KUV, GSAR_KUA)
    * --- Al termine della Elaborazione Popola il Cursore dello Zoom della Maschera chiamante
    * --- Parametro Chiamato da: C = Venduto a Cliente ; F = Acquistato da Fornitore
    * --- Parametri in Funzione del Cli/For
    if this.pOPER="C"
      this.w_FLVEAC = "V"
      this.w_TIPCON = "C"
      this.w_CODCON = this.oParentObject.w_ANCODICE
    else
      this.w_FLVEAC = "A"
      this.w_TIPCON = "F"
      this.w_CODCON = this.oParentObject.w_ANCODICE
    endif
    this.w_ZOOM = this.oParentObject.w_ZoomART
    ND = this.oParentObject.w_ZoomART.cCursor
    This.OparentObject.NotifyEvent("Ricerca")
    ah_Msg("Lettura documenti...",.T.)
    vq_exec("query\GSAR_QVA",this, "TOTDOC")
    if USED("TOTDOC")
      this.w_OLDART = "@@@###XXX"
      SELECT TOTDOC
      GO TOP
      SCAN FOR NOT EMPTY(NVL(MVCODART,""))
      if MVCODART<>this.w_OLDART
        * --- Nuovo Articolo
        this.w_OLDART = MVCODART
        this.w_DESART = MVDESART
        this.w_DATREG = CP_TODATE(MVDATREG)
        this.w_CODVAL = MVCODVAL
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECUNI"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECUNI;
            from (i_cTable) where;
                VACODVAL = this.w_CODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_UNIMIS = MVUNIMIS
        this.w_QTAMOV = MVQTAMOV
        this.w_PREZZO = MVPREZZO
        this.w_SCONT1 = MVSCONT1
        this.w_SCONT2 = MVSCONT2
        this.w_SCONT3 = MVSCONT3
        this.w_SCONT4 = MVSCONT4
        this.w_FLOMAG = MVFLOMAG
        this.w_VALUNI = cp_ROUND((this.w_PREZZO * (1+this.w_SCONT1/100)*(1+this.w_SCONT2/100)*(1+this.w_SCONT3/100)*(1+this.w_SCONT4/100)),this.w_DECUNI)
        this.w_SERIAL = MVSERIAL
        this.w_CPROWNUM = CPROWNUM
        this.w_NUMRIF = MVNUMRIF
        this.w_VALULT = MVVALULT
        this.w_CODICE = IIF(this.w_FLOMAG="X"," ",ah_Msgformat("Omaggio"))
        this.w_VALFISC = cp_ROUND(VALFISC,this.w_DECUNI)
        INSERT INTO (ND) (MVCODART,MVDESART,MVDATREG,MVCODVAL,MVPREZZO,MVUNIMIS,MVQTAMOV,OMAGGIO,MVSCONT1,; 
 MVSCONT2,MVSCONT3,MVSCONT4, MVPREZZOSM,MVSERIAL,MVNUMRIF,CPROWNUM, VALFISC) VALUES ; 
 (this.w_OLDART,this.w_DESART,CP_TODATE(this.w_DATREG),this.w_CODVAL,this.w_PREZZO,this.w_UNIMIS,this.w_QTAMOV,this.w_CODICE,; 
 this.w_SCONT1,this.w_SCONT2,this.w_SCONT3;
        ,this.w_SCONT4,this.w_VALUNI,this.w_SERIAL,this.w_NUMRIF,this.w_CPROWNUM,this.w_VALFISC)
      endif
      ENDSCAN
      * --- chiude il cursore
      if USED("TOTDOC")
        SELECT TOTDOC
        USE
      endif
      select (ND)
      go top
      this.w_ZOOM = this.oParentObject.w_ZoomART.refresh()
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
