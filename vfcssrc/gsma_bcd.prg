* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bcd                                                        *
*              Calcola prezzo listino da M.M.                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_178]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-07-04                                                      *
* Last revis.: 2012-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bcd",oParentObject,m.pExec)
return(i_retval)

define class tgsma_bcd as StdBatch
  * --- Local variables
  pExec = space(1)
  w_QUAN = 0
  w_RN0 = 0
  w_S11 = 0
  w_APPO = space(12)
  w_DT0 = ctod("  /  /  ")
  w_S21 = 0
  w_QTAUM1 = 0
  w_PZ0 = 0
  w_S31 = 0
  w_CODART = space(20)
  w_OK0 = .f.
  w_S41 = 0
  w_DATREG = ctod("  /  /  ")
  w_OK1 = .f.
  w_CODLIS = space(5)
  w_PZ1 = 0
  w_CATCLI = space(5)
  w_DT1 = ctod("  /  /  ")
  w_CATART = space(5)
  w_S10 = 0
  w_ARRSUP = 0
  w_S20 = 0
  w_APPO1 = 0
  w_S30 = 0
  w_CODVAL = space(3)
  w_S40 = 0
  w_OK = .f.
  w_NETTO = 0
  w_DTOBSO = ctod("  /  /  ")
  w_FLFRAZ = space(1)
  w_MODUM2 = space(1)
  w_QTATEST = 0
  w_MLCODICE = space(20)
  w_CODMAG = space(5)
  w_CALPRZ = 0
  w_RICTOT = .f.
  w_CODMAT = space(40)
  w_LENSCF = 0
  w_MESS = space(1)
  w_ARTCLI = space(1)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_cKey = space(20)
  w_FLLMAG = space(1)
  w_OKSCA = space(1)
  w_FLAVAL = space(1)
  w_FLCAUM = space(1)
  w_F2RISE = space(1)
  w_F2CASC = space(1)
  w_QTAUM3 = 0
  w_QTALIS = 0
  w_RICSCO = .f.
  * --- WorkFile variables
  ART_ICOL_idx=0
  LOTTIART_idx=0
  UNIMIS_idx=0
  MOVIMATR_idx=0
  KEY_ARTI_idx=0
  CAM_AGAZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Listino o esegue lo Scorporo (da GSMA_MVM)
    * --- Lanciato da:
    * --- A = Cambio Articolo/Cambio Quantita'; R = Automatismi (ev. Ricalcola); S = Zoom su Prezzo (Scorporo); P = Qta in 1^ U.M.
    * --- Variabili Controllo Lotti
    * --- Calcolo prezzo
    msg=""
    if this.pExec="A" And this.oParentObject.w_MMQTAMOV<>0 And this.oParentObject.w_MMUNIMIS<>this.oParentObject.w_UNMIS1
      * --- Lettura dei flag U.M. frazionabile e Forza 2^ U.M.
      * --- Read from UNIMIS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.UNIMIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "UMFLFRAZ,UMMODUM2"+;
          " from "+i_cTable+" UNIMIS where ";
              +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_UNMIS1);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          UMFLFRAZ,UMMODUM2;
          from (i_cTable) where;
              UMCODICE = this.oParentObject.w_UNMIS1;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
        this.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Ricalcolo della Qt� nella Prima U.M. poich� al lancio del batch non � ancora stata calcolata
      this.w_QTATEST = CALQTA(this.oParentObject.w_MMQTAMOV,this.oParentObject.w_MMUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
      * --- Se la prima U.M. non � frazionabile ed � attivo il flag Forza seconda U.M. e la Qt� nella Prima U.M. ha decimali
      *     ricalcolo la qt� nella Prima U.M. arrotondata alla unit� superiore
      if this.w_FLFRAZ="S" And this.w_QTATEST<>INT(this.w_QTATEST)
        if this.w_MODUM2="S" 
          this.oParentObject.w_MMQTAUM1 = INT(this.w_QTATEST)+1
          * --- Ricalcolo la quantit� su riga in base alla nuova Qt� nella prima U.M.
          this.oParentObject.w_MMQTAMOV = cp_Round((this.oParentObject.w_MMQTAMOV*this.oParentObject.w_MMQTAUM1)/this.w_QTATEST,g_PERPQT)
          * --- Per evitare di nuovo il ricalcolo della Qt� nella prima U.M. dovuta al calculate sul campo nei documenti
          this.oParentObject.o_MMQTAMOV = this.oParentObject.w_MMQTAMOV
          this.w_QTAUM1 = this.oParentObject.w_MMQTAUM1
        else
          if this.oParentObject.w_FLUSEP<>" "
            * --- Se UM separate arrotondo la qt� della 1UM
            this.oParentObject.w_MMQTAUM1 = INT(this.w_QTATEST)+1
          else
            * --- segnalo errore
            msg="Attenzione: articolo privo di unit� di misura separate e prima unit� di misura non frazionabile%0La quantit� nella 1a UM non pu� essere arrotondata all'intero"
            this.oParentObject.w_MMQTAUM1 = 0
          endif
          this.w_QTAUM1 = this.oParentObject.w_MMQTAUM1
          if NOT EMPTY(msg)
            * --- Errore relativo alla quantit� della 1a UM
            Ah_ErrorMsg(msg)
          endif
        endif
      endif
    endif
    if this.pExec= "P" And this.oParentObject.w_FLUSEP = "Q"
      * --- Cambio della quantit� della prima unit� di misura con U.M. Separate su Per Quantit�
      *     Non devo effettuare nessun ricalcolo.
      Ah_ErrorMsg("Ricalcolo non necessario!",48,"")
      i_retcode = 'stop'
      return
    endif
    this.w_CODART = this.oParentObject.w_MMCODART
    this.w_CODLIS = this.oParentObject.w_MMCODLIS
    this.w_DATREG = IIF(EMPTY(this.oParentObject.w_MMDATDOC), this.oParentObject.w_MMDATREG, this.oParentObject.w_MMDATDOC)
    this.w_CODVAL = this.oParentObject.w_MMCODVAL
    this.w_CATCLI = this.oParentObject.w_CATSCC
    this.w_CATART = this.oParentObject.w_CATSCA
    this.w_ARRSUP = iif(this.oParentObject.w_DECTOT=0, .499, .00499)
    if this.pExec="S" AND this.oParentObject.w_ACTSCOR=.F. And this.oParentObject.w_MMPREZZO<>0
      if ah_YesNo("Vuoi scorporare il prezzo di riga?")
        * --- Scorpora
        this.oParentObject.w_ACTSCOR = .T.
        this.w_NETTO = cp_ROUND(this.oParentObject.w_MMPREZZO / (1 + (this.oParentObject.w_PERIVA / 100)), this.oParentObject.w_DECUNI)
        this.oParentObject.w_IVASCOR = this.oParentObject.w_MMPREZZO - this.w_NETTO
        this.oParentObject.w_MMPREZZO = this.w_NETTO
        * --- Notifica che la Riga e' Stata Variata
        SELECT (this.oParentObject.cTrsName)
        if EMPTY(i_SRV) AND NOT DELETED()
          REPLACE i_SRV WITH "U"
        endif
      endif
      i_retcode = 'stop'
      return
    endif
    if this.pExec="S" AND this.oParentObject.w_ACTSCOR=.T.And Not(ah_YesNo("Vuoi ricalcolare il prezzo di riga?"))
      * --- F9 sul prezzo per la seconda volta (ACTSCOR=.T.) riporto il prezzo nella situazione originaria (lo ricalcolo)
      i_retcode = 'stop'
      return
    endif
    if this.pEXEC="A" AND this.oParentObject.w_MMCODICE<>this.oParentObject.o_MMCODICE AND g_MATR="S" AND this.oParentObject.w_GESMAT="S" 
      * --- Eseguo controllo riga movimentata a matricole
      if this.oParentObject.w_TOTMAT<0
        * --- Read from MOVIMATR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MOVIMATR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2],.t.,this.MOVIMATR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MTCODMAT"+;
            " from "+i_cTable+" MOVIMATR where ";
                +"MTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MMSERIAL);
                +" and MTROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                +" and MTNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MMNUMRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MTCODMAT;
            from (i_cTable) where;
                MTSERIAL = this.oParentObject.w_MMSERIAL;
                and MTROWNUM = this.oParentObject.w_CPROWNUM;
                and MTNUMRIF = this.oParentObject.w_MMNUMRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODMAT = NVL(cp_ToDate(_read_.MTCODMAT),cp_NullValue(_read_.MTCODMAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if NOT EMPTY(this.w_CODMAT) OR this.oParentObject.w_TOTMAT >0 
        this.oParentObject.w_MMCODICE = this.oParentObject.o_MMCODICE
        ah_ErrorMsg("Attenzione: articolo associato a movimenti matricole impossibile modificare","i")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Controllo obsolescenza Articolo
    if this.pExec="A" AND NOT EMPTY(this.oParentObject.w_MMCODICE)
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CADTOBSO,CALENSCF,CATIPCON"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_MMCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CADTOBSO,CALENSCF,CATIPCON;
          from (i_cTable) where;
              CACODICE = this.oParentObject.w_MMCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DTOBSO = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
        this.w_LENSCF = NVL(cp_ToDate(_read_.CALENSCF),cp_NullValue(_read_.CALENSCF))
        this.w_ARTCLI = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.w_DTOBSO) AND this.w_DTOBSO<=this.w_DATREG
        this.oParentObject.w_MMCODICE = " "
        this.oParentObject.w_DESART = " "
        ah_ErrorMsg("Codice articolo obsoleto dal %1",,"",dtoc(this.w_DTOBSO))
        i_retcode = 'stop'
        return
      endif
      if g_FLCESC = "S" And ((this.w_LENSCF<>0 And Right(Alltrim(this.oParentObject.w_MMCODICE), this.w_LENSCF) <> IIF(Empty(Alltrim(this.oParentObject.w_CODESC)),"######",Alltrim(this.oParentObject.w_CODESC))) Or (this.w_LENSCF=0 And Not Empty(this.oParentObject.w_CODESC) And this.w_ARTCLI$"C-F"))
        * --- oggetto per mess. incrementali
        this.w_oMESS=createobject("ah_message")
        this.w_oMESS.AddMsgPartNL("Codice articolo incongruente con intestatario")     
        if Empty(this.oParentObject.w_CODESC)
          this.w_oPART = this.w_oMESS.addmsgpartNL("Nessun suffisso codice di ricerca legato a %1")
          this.w_oPART.addParam(Alltrim(this.oParentObject.w_MMCODCON))     
        else
          this.w_oPART = this.w_oMESS.addmsgpartNL("Suffisso codice di ricerca legato a %1 = %2")
          this.w_oPART.addParam(Alltrim(this.oParentObject.w_MMCODCON))     
          this.w_oPART.addParam(this.oParentObject.w_CODESC)     
        endif
        if this.w_LENSCF = 0
          this.w_oMESS.AddMsgPartNL("Nessun suffisso assegnato al codice di ricerca di tipo cliente")     
        else
          this.w_oPART = this.w_oMESS.addmsgpartNL("Suffisso assegnato al codice di ricerca = %1")
          this.w_oPART.addParam(Right(Alltrim(this.oParentObject.w_MMCODICE),this.w_LENSCF))     
        endif
        this.w_oMESS.Ah_ErrorMsg()     
        this.oParentObject.w_MMCODICE = Space(20)
        this.oParentObject.w_DESART = Space(40)
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Attiva il Post-In relativo al Codice Articolo associato al Codice di Ricerca
    if this.pEXEC="A" AND this.oParentObject.w_MMCODICE<>this.oParentObject.o_MMCODICE AND NOT EMPTY(this.oParentObject.w_MMCODART)
      this.w_cKey = cp_setAzi(i_TableProp[this.oParentObject.ART_ICOL_IDX, 2]) + "\" + cp_ToStr(this.oParentObject.w_MMCODART, 1)
      cp_ShowWarn(this.w_cKey, this.oParentObject.ART_ICOL_IDX)
    endif
    * --- Lotti a Consumo Automatico
    if this.pEXEC="A" AND this.oParentObject.w_MMCODICE<>this.oParentObject.o_MMCODICE AND g_PERLOT="S"
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARFLLOTT,ARFLLMAG,ARMAGPRE"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MMCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARFLLOTT,ARFLLMAG,ARMAGPRE;
          from (i_cTable) where;
              ARCODART = this.oParentObject.w_MMCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_FLLOTT = NVL(cp_ToDate(_read_.ARFLLOTT),cp_NullValue(_read_.ARFLLOTT))
        this.w_FLLMAG = NVL(cp_ToDate(_read_.ARFLLMAG),cp_NullValue(_read_.ARFLLMAG))
        this.oParentObject.w_MAGPRE = NVL(cp_ToDate(_read_.ARMAGPRE),cp_NullValue(_read_.ARMAGPRE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.oParentObject.w_MMCODMAG = SPACE(5)
      this.w_OKSCA = "F"
      if Not Empty(this.oParentObject.w_MMCAUMAG)
        this.w_FLCAUM = Left(Alltrim(this.oParentObject.w_MMFLCASC)+IIF(this.oParentObject.w_MMFLRISE="+", "-", IIF(this.oParentObject.w_MMFLRISE="-", "+", " ")),1)
        this.w_OKSCA = IIF((this.oParentObject.w_FLAVA1="A" and this.w_FLCAUM="-") or (this.oParentObject.w_FLAVA1="V" and this.w_FLCAUM="+") ,"T","F")
      endif
      if Not Empty(this.oParentObject.w_MMCAUCOL) AND this.w_OKSCA="F"
        * --- Read from CAM_AGAZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CMFLAVAL,CMFLCASC,CMFLRISE"+;
            " from "+i_cTable+" CAM_AGAZ where ";
                +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MMCAUCOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CMFLAVAL,CMFLCASC,CMFLRISE;
            from (i_cTable) where;
                CMCODICE = this.oParentObject.w_MMCAUCOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
          this.w_F2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
          this.w_F2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_FLCAUM = Left(Alltrim(this.w_F2CASC)+IIF(this.w_F2RISE="+", "-", IIF(this.w_F2RISE="-", "+", " ")),1)
        this.w_OKSCA = IIF((this.w_FLAVAL="A" and this.w_FLCAUM="-") or (this.w_FLAVAL="V" and this.w_FLCAUM="+") ,"T","F")
      endif
      * --- Leggo Flag gestione lotti
      if g_PERLOT="S" AND this.oParentObject.w_FLLOTT="C" AND (this.oParentObject.w_MMFLCASC="-" OR this.oParentObject.w_MMFLRISE="+") 
        * --- Lotti a Consumo Automatico
        this.w_CODMAG = IIF(this.w_FLLMAG="S",Space(5),IIF(NOT EMPTY(this.oParentObject.w_MAGPRE) , this.oParentObject.w_MAGPRE, this.oParentObject.w_OMAG))
        vq_exec("..\MADV\EXE\QUERY\GSMD_QCA.VQR",this,"LOTTI")
        * --- Cerco il Lotto con data scadenza pi� prossima alla data registrazione e 
        *     a parit� di  data scadenza con data creazione pi� vecchia.
        this.oParentObject.w_MMCODLOT = CERCLOT(this.oParentObject.w_MMDATREG,this.w_CODART,"LOTTI")
        if NOT EMPTY(this.oParentObject.w_MMCODLOT)
          * --- Assegno Magazzino e Ubicazione congruenti al lotto se consumo automatico
          if RECCOUNT("LOTTI")>0
            SELECT LOTTI
            LOCATE FOR LOTTI.LOCODICE = this.oParentObject.w_MMCODLOT AND LOTTI.CODART=this.oParentObject.w_MMCODART
            if FOUND()
              * --- Assegno Magazzino e Ubicazione congruenti al lotto se consumo automatico
              this.oParentObject.w_MMCODMAG = NVL(LOTTI.CODMAG,SPACE(5))
              this.oParentObject.w_MMCODUBI = NVL(LOTTI.LOCODUBI,SPACE(20))
              this.oParentObject.w_DATLOT = CP_TODATE(LOTTI.LODATSCA)
              this.oParentObject.w_FLSTAT = "D"
            endif
            SELECT LOTTI
            USE
          endif
        endif
      endif
      if this.oParentObject.w_FLLOTT <>"C"
        this.oParentObject.w_MMCODLOT = SPACE(20)
      endif
    endif
    if this.oParentObject.w_MMQTAMOV=0 OR EMPTY(this.w_CODART)
      * --- Se non Specifico la Quantita o l'articolo esce
      this.oParentObject.w_MMPREZZO = 0
      i_retcode = 'stop'
      return
    endif
    this.w_APPO = this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3+this.oParentObject.w_MMUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+" "+"Q"
    * --- Ricalcolo Quantit� nella Prima unit� di misura
    *     Solo se non � gi� stata calcolata in precedenza per attivazione flag:
    *     Forza U.M. secondaria 
    if this.w_QTAUM1=0
      if this.pExec $ "PRS" AND this.oParentObject.w_MMQTAUM1<>0
        * --- Modificata 1^UM
        this.w_QTAUM1 = this.oParentObject.w_MMQTAUM1
      else
        this.w_QTAUM1 = CALQTA(this.oParentObject.w_MMQTAMOV,this.oParentObject.w_MMUNIMIS,this.oParentObject.w_UNMIS2,this.oParentObject.w_OPERAT, this.oParentObject.w_MOLTIP, this.oParentObject.w_FLUSEP, this.w_FLFRAZ, this.w_MODUM2, @msg, this.oParentObject.w_UNMIS3, this.oParentObject.w_OPERA3, this.oParentObject.w_MOLTI3)
      endif
    endif
    DECLARE ARRCALC (16,1)
    * --- Azzero l'Array che verr� riempito dalla Funzione
    ARRCALC(1)=0
    this.w_QTAUM3 = CALQTA(this.w_QTAUM1,this.oParentObject.w_UNMIS3, Space(3),IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
    this.oParentObject.w_QTAUM2 = CALQTA(this.w_QTAUM1,this.oParentObject.w_UNMIS2, this.oParentObject.w_UNMIS2,IIF(this.oParentObject.w_OPERAT="/","*","/"), this.oParentObject.w_MOLTIP, "", "", "", , this.oParentObject.w_UNMIS3, IIF(this.oParentObject.w_OPERA3="/","*","/"), this.oParentObject.w_MOLTI3)
    DIMENSION pArrUm[9]
    pArrUm [1] = this.oParentObject.w_PREZUM 
 pArrUm [2] = this.oParentObject.w_MMUNIMIS 
 pArrUm [3] = this.oParentObject.w_MMQTAMOV 
 pArrUm [4] = this.oParentObject.w_UNMIS1 
 pArrUm [5] = this.w_QTAUM1 
 pArrUm [6] = this.oParentObject.w_UNMIS2 
 pArrUm [7] = this.oParentObject.w_QTAUM2 
 pArrUm [8] = this.oParentObject.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
    this.w_CALPRZ = CalPrzli( REPL("X",16) , " " , this.oParentObject.w_MMCODLIS , this.oParentObject.w_MMCODART , " " , this.w_QTAUM1 , this.oParentObject.w_MMCODVAL , this.oParentObject.w_MMCAOVAL , this.oParentObject.w_MMDATREG , this.w_CATCLI , this.w_CATART, this.oParentObject.w_MMCODVAL, " ", " ", " ", this.oParentObject.w_SCOLIS, 0,"M", @ARRCALC, " ", " ", "N", @pArrUm )
    this.oParentObject.w_CLUNIMIS = ARRCALC(16)
    this.oParentObject.w_LIPREZZO = ARRCALC(5)
    this.w_OK = ARRCALC(7)=2 OR ARRCALC(8)=1
    if this.oParentObject.w_MMUNIMIS<>this.oParentObject.w_CLUNIMIS AND NOT EMPTY(this.oParentObject.w_CLUNIMIS)
      this.w_QTALIS = IIF(this.oParentObject.w_CLUNIMIS=this.oParentObject.w_UNMIS1,this.w_QTAUM1, IIF(this.oParentObject.w_CLUNIMIS=this.oParentObject.w_UNMIS2, this.oParentObject.w_QTAUM2, this.w_QTAUM3))
      this.oParentObject.w_LIPREZZO = cp_Round(CALMMPZ(this.oParentObject.w_LIPREZZO, this.oParentObject.w_MMQTAMOV, this.w_QTALIS, this.oParentObject.w_IVALIS,this.oParentObject.w_PERIVA, this.oParentObject.w_DECUNI),this.oParentObject.w_DECUNI)
    endif
    this.oParentObject.w_MMSCONT1 = ARRCALC(1)
    this.oParentObject.w_MMSCONT2 = ARRCALC(2)
    this.oParentObject.w_MMSCONT3 = ARRCALC(3)
    this.oParentObject.w_MMSCONT4 = ARRCALC(4)
    this.oParentObject.w_ACTSCOR = ARRCALC(10)
    * --- Impedisco il ricalcolo degli sconti
    this.oParentObject.o_MMSCONT1 = this.oParentObject.w_MMSCONT1
    this.oParentObject.o_MMSCONT2 = this.oParentObject.w_MMSCONT2
    this.oParentObject.o_MMSCONT3 = this.oParentObject.w_MMSCONT3
    if this.oParentObject.w_LIPREZZO = 0 AND NOT EMPTY(this.oParentObject.w_MMCODLIS)
      * --- Quando w_LIPREZZO vene variato il ricalcolo � forzato dall'assegnamento
      *     o_LIPREZZO = 0
      *     nella routine GSMA_BES.
      *     Solo nel caso in cui w_LIPREZZO = 0, per eseguire il ricalcolo, occorre che
      *     o_LIPREZZO <> 0
      this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO + 1
    endif
    if ((this.pExec="A" AND this.oParentObject.w_MMQTAMOV<>0 AND Not Empty(this.w_CODLIS)) OR this.pExec="P") AND this.oParentObject.o_LIPREZZO=this.oParentObject.w_LIPREZZO
      * --- Forza il Ricalcolo se variate QTA
      this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO + 1
    endif
    this.w_RICTOT = .F.
    if this.pExec="S" AND this.w_OK=.F. AND this.oParentObject.w_ACTSCOR=.F. And this.oParentObject.w_MMPREZZO<>0
      this.w_RICTOT = .T.
      * --- Se non presente Listino Ripristina situazione pre Scorporo
      this.oParentObject.w_MMPREZZO = this.oParentObject.w_MMPREZZO+this.oParentObject.w_IVASCOR
      this.oParentObject.w_IVASCOR = 0
      * --- Notifica che la Riga e' Stata Variata
      SELECT (this.oParentObject.cTrsName)
      if EMPTY(i_SRV) AND NOT DELETED()
        REPLACE i_SRV WITH "U"
      endif
    endif
    * --- Num. Max. Sconti Consentiti
    this.oParentObject.w_MMSCONT1 = IIF(g_NUMSCO>0, this.oParentObject.w_MMSCONT1, 0)
    this.oParentObject.w_MMSCONT2 = IIF(g_NUMSCO>1, this.oParentObject.w_MMSCONT2, 0)
    this.oParentObject.w_MMSCONT3 = IIF(g_NUMSCO>2, this.oParentObject.w_MMSCONT3, 0)
    this.oParentObject.w_MMSCONT4 = IIF(g_NUMSCO>3, this.oParentObject.w_MMSCONT4, 0)
    this.w_RICSCO = .F.
    this.w_RICSCO = IIF(g_NUMSCO>0, this.oParentObject.w_MMSCONT1, 0)<>this.oParentObject.o_MMSCONT1 And this.oParentObject.o_MMSCONT1<>0
    this.w_RICSCO = this.w_RICSCO OR (IIF(g_NUMSCO>1, this.oParentObject.w_MMSCONT2, 0)<>this.oParentObject.o_MMSCONT2 And this.oParentObject.o_MMSCONT2<>0)
    this.w_RICSCO = this.w_RICSCO OR (IIF(g_NUMSCO>2, this.oParentObject.w_MMSCONT3, 0)<>this.oParentObject.o_MMSCONT3 And this.oParentObject.o_MMSCONT3<>0)
    this.w_RICSCO = this.w_RICSCO OR (IIF(g_NUMSCO>3, this.oParentObject.w_MMSCONT4, 0)<>this.oParentObject.o_MMSCONT4 And this.oParentObject.o_MMSCONT4<>0)
    * --- Impedisco il ricalcolo degli sconti
    this.oParentObject.o_MMSCONT1 = this.oParentObject.w_MMSCONT1
    this.oParentObject.o_MMSCONT2 = this.oParentObject.w_MMSCONT2
    this.oParentObject.o_MMSCONT3 = this.oParentObject.w_MMSCONT3
    if this.pExec $ "SPR" And (this.w_OK Or this.w_RICTOT or this.w_RICSCO)
      if Not this.w_RICTOT
        * --- Ricalcolo il prezzo solo se non deriva da Reincorporo dell'Iva 
        if this.pExec="P"
          * --- Ricalcolo prezzo se cambio quantit� nella prima U.M con CALMMPZ
          *     poich� ricalcola in base qlle quantit� effettivamente digitate (per U.M separate)
          this.oParentObject.w_MMPREZZO = cp_Round(CALMMPZ(this.oParentObject.w_LIPREZZO, this.oParentObject.w_MMQTAMOV, this.oParentObject.w_MMQTAUM1, this.oParentObject.w_IVALIS, this.oParentObject.w_PERIVA, this.oParentObject.w_DECUNI),this.oParentObject.w_DECUNI)
          this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
        else
          * --- Altrimenti tramite CALMMLIS
          if empty(this.oParentObject.w_CLUNIMIS)
            this.oParentObject.w_MMPREZZO = cp_Round(CALMMLIS(this.oParentObject.w_LIPREZZO, this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3+this.oParentObject.w_MMUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+this.oParentObject.w_IVALIS+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), this.oParentObject.w_MOLTIP, this.oParentObject.w_MOLTI3, this.oParentObject.w_PERIVA), this.oParentObject.w_DECUNI)
          else
            if this.oParentObject.w_MMUNIMIS=this.oParentObject.w_CLUNIMIS
              * --- mi serve solo calcolare l'iva pertanto inganno il ricalcolo delle quantit� mettendo i moltiplicatori a 1
              this.oParentObject.w_MMPREZZO = cp_Round(CALMMLIS(this.oParentObject.w_LIPREZZO, this.oParentObject.w_UNMIS1+this.oParentObject.w_UNMIS2+this.oParentObject.w_UNMIS3+this.oParentObject.w_MMUNIMIS+this.oParentObject.w_OPERAT+this.oParentObject.w_OPERA3+this.oParentObject.w_IVALIS+"P"+ALLTRIM(STR(this.oParentObject.w_DECUNI)), 1, 1, this.oParentObject.w_PERIVA), this.oParentObject.w_DECUNI)
            else
              this.oParentObject.w_MMPREZZO = this.oParentObject.w_LIPREZZO
            endif
          endif
        endif
      endif
      this.oParentObject.w_PREUM1 = IIF(this.oParentObject.w_MMQTAUM1=0, 0, cp_ROUND((this.oParentObject.w_MMPREZZO * this.oParentObject.w_MMQTAMOV) / this.oParentObject.w_MMQTAUM1, this.oParentObject.w_DECUNI))
      this.oParentObject.w_VALUNI = cp_ROUND(this.oParentObject.w_MMPREZZO * (1+this.oParentObject.w_MMSCONT1/100)*(1+this.oParentObject.w_MMSCONT2/100)*(1+this.oParentObject.w_MMSCONT3/100)*(1+this.oParentObject.w_MMSCONT4/100),5)
      this.oParentObject.w_VALUNI2 = cp_ROUND (this.oParentObject.w_VALUNI * (1+this.oParentObject.w_MMSCOCL1/100)*(1+this.oParentObject.w_MMSCOCL2/100)*(1+this.oParentObject.w_MMSCOPAG/100),5)
      this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE - this.oParentObject.w_MMVALMAG
      this.oParentObject.w_MMVALMAG = cp_ROUND(this.oParentObject.w_MMQTAMOV*this.oParentObject.w_VALUNI2, this.oParentObject.w_DECTOT)
      this.oParentObject.w_MMIMPNAZ = CALCNAZ(this.oParentObject.w_MMVALMAG,this.oParentObject.w_MMCAOVAL,this.oParentObject.w_CAONAZ,IIF(EMPTY(this.oParentObject.w_MMDATDOC), this.oParentObject.w_MMDATREG, this.oParentObject.w_MMDATDOC),this.oParentObject.w_MMVALNAZ,this.oParentObject.w_MMCODVAL,IIF(this.oParentObject.w_FLAVA1="A",(this.oParentObject.w_PERIVA*this.oParentObject.w_INDIVA),0))
      this.oParentObject.w_VISNAZ = cp_ROUND(this.oParentObject.w_MMIMPNAZ, this.oParentObject.w_DECTOP)
      this.oParentObject.w_TOTALE = this.oParentObject.w_TOTALE + this.oParentObject.w_MMVALMAG
      * --- Notifica che la Riga e' Stata Variata
      SELECT (this.oParentObject.cTrsName)
      if EMPTY(i_SRV) AND NOT DELETED()
        REPLACE i_SRV WITH "U"
      endif
    endif
    if this.pExec="S"
      * --- Valorizzo o_LIPREZZO con w_LIPREZZO per evitare il ricalcolo
      *     del prezzo nel caso in cui l'utente modifica il prezzo a mano
      *     a seguito di un F9 sul prezzo.
      this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
    endif
  endproc


  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='LOTTIART'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='MOVIMATR'
    this.cWorkTables[5]='KEY_ARTI'
    this.cWorkTables[6]='CAM_AGAZ'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
