* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_sca                                                        *
*              Analisi scaduto per partita                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_199]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-02                                                      *
* Last revis.: 2008-08-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_sca",oParentObject))

* --- Class definition
define class tgste_sca as StdForm
  Top    = 15
  Left   = 11

  * --- Standard Properties
  Width  = 554
  Height = 276+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-08-08"
  HelpContextID=177589143
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  BAN_CHE_IDX = 0
  COC_MAST_IDX = 0
  AGENTI_IDX = 0
  cPrg = "gste_sca"
  cComment = "Analisi scaduto per partita"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_SCAINI = ctod('  /  /  ')
  o_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_DATSTA = ctod('  /  /  ')
  w_FLSOSP = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_FLGSOS = space(1)
  w_FLGNSO = space(1)
  w_DESCON = space(40)
  w_NUMPAR = space(31)
  w_BANAPP = space(10)
  w_DESBAN = space(50)
  w_BANNOS = space(15)
  w_DESNOS = space(35)
  w_NDOINI = 0
  w_ADOINI = space(10)
  w_DDOINI = ctod('  /  /  ')
  w_NDOFIN = 0
  w_ADOFIN = space(10)
  w_DDOFIN = ctod('  /  /  ')
  w_VALUTA = space(3)
  w_DESVAL = space(35)
  w_AGENTE = space(5)
  w_DESAGE = space(35)
  w_CODPAG = space(5)
  w_FLPART = space(1)
  w_CODCON = space(15)
  w_CODFIN = space(15)
  w_FLORIG = space(1)
  w_FLDESC = space(1)
  w_DESFIN = space(40)
  w_PARTSN = space(1)
  w_PARTSN2 = space(1)
  w_FLANSCA = space(1)
  w_ONUME = 0
  w_Output = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_scaPag1","gste_sca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgste_scaPag2","gste_sca",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCAINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Output = this.oPgFrm.Pages(1).oPag.Output
    DoDefault()
    proc Destroy()
      this.w_Output = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='BAN_CHE'
    this.cWorkTables[4]='COC_MAST'
    this.cWorkTables[5]='AGENTI'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSTE_BCA with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_sca
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_TIPCON=space(1)
      .w_DATSTA=ctod("  /  /  ")
      .w_FLSOSP=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_FLGSOS=space(1)
      .w_FLGNSO=space(1)
      .w_DESCON=space(40)
      .w_NUMPAR=space(31)
      .w_BANAPP=space(10)
      .w_DESBAN=space(50)
      .w_BANNOS=space(15)
      .w_DESNOS=space(35)
      .w_NDOINI=0
      .w_ADOINI=space(10)
      .w_DDOINI=ctod("  /  /  ")
      .w_NDOFIN=0
      .w_ADOFIN=space(10)
      .w_DDOFIN=ctod("  /  /  ")
      .w_VALUTA=space(3)
      .w_DESVAL=space(35)
      .w_AGENTE=space(5)
      .w_DESAGE=space(35)
      .w_CODPAG=space(5)
      .w_FLPART=space(1)
      .w_CODCON=space(15)
      .w_CODFIN=space(15)
      .w_FLORIG=space(1)
      .w_FLDESC=space(1)
      .w_DESFIN=space(40)
      .w_PARTSN=space(1)
      .w_PARTSN2=space(1)
      .w_FLANSCA=space(1)
      .w_ONUME=0
        .w_CODAZI = i_CODAZI
        .w_SCAINI = i_datsys
        .w_SCAFIN = i_datsys+365
        .w_TIPCON = 'C'
        .w_DATSTA = i_datsys
        .w_FLSOSP = 'T'
        .w_OBTEST = iif(empty(.w_SCAINI),i_INIDAT,.w_SCAINI)
          .DoRTCalc(8,8,.f.)
        .w_FLGSOS = IIF(.w_FLSOSP $ 'TN', ' ', 'S')
        .w_FLGNSO = IIF(.w_FLSOSP $ 'TS', ' ', 'S')
        .DoRTCalc(11,13,.f.)
        if not(empty(.w_BANAPP))
          .link_2_2('Full')
        endif
        .DoRTCalc(14,15,.f.)
        if not(empty(.w_BANNOS))
          .link_2_4('Full')
        endif
        .DoRTCalc(16,23,.f.)
        if not(empty(.w_VALUTA))
          .link_2_12('Full')
        endif
          .DoRTCalc(24,24,.f.)
        .w_AGENTE = SPACE(5)
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_AGENTE))
          .link_2_25('Full')
        endif
        .DoRTCalc(26,29,.f.)
        if not(empty(.w_CODCON))
          .link_1_21('Full')
        endif
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_CODFIN))
          .link_1_22('Full')
        endif
        .w_FLORIG = ' '
        .w_FLDESC = ' '
      .oPgFrm.Page1.oPag.Output.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
    endwith
    this.DoRTCalc(33,37,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_SCAINI<>.w_SCAINI
            .w_OBTEST = iif(empty(.w_SCAINI),i_INIDAT,.w_SCAINI)
        endif
        .DoRTCalc(8,8,.t.)
            .w_FLGSOS = IIF(.w_FLSOSP $ 'TN', ' ', 'S')
            .w_FLGNSO = IIF(.w_FLSOSP $ 'TS', ' ', 'S')
        .DoRTCalc(11,24,.t.)
        if .o_TIPCON<>.w_TIPCON
            .w_AGENTE = SPACE(5)
          .link_2_25('Full')
        endif
        .oPgFrm.Page1.oPag.Output.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(26,37,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Output.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oAGENTE_2_25.enabled = this.oPgFrm.Page2.oPag.oAGENTE_2_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Output.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=BANAPP
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANAPP)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANAPP))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANAPP)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESBAN like "+cp_ToStrODBC(trim(this.w_BANAPP)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESBAN like "+cp_ToStr(trim(this.w_BANAPP)+"%");

            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_BANAPP) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oBANAPP_2_2'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANAPP)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANAPP = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_BANAPP = space(10)
      endif
      this.w_DESBAN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANNOS
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANNOS))
          select BACODBAN,BADESCRI,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANNOS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_BANNOS)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_BANNOS)+"%");

            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_BANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oBANNOS_2_4'),i_cWhere,'GSTE_ACB',"Conti banca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANNOS)
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANNOS = NVL(_Link_.BACODBAN,space(15))
      this.w_DESNOS = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_BANNOS = space(15)
      endif
      this.w_DESNOS = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente oppure obsoleto")
        endif
        this.w_BANNOS = space(15)
        this.w_DESNOS = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VALUTA))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VALUTA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_VALUTA)+"%");

            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_VALUTA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVALUTA_2_12'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DESVAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AGENTE
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_AGENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_AGENTE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGENTE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_AGENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_AGENTE)+"%");

            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AGENTE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oAGENTE_2_25'),i_cWhere,'GSAR_AGE',"Elenco agenti",'AGE1ZOOM.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_AGENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_AGENTE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGENTE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_AGENTE = space(5)
      endif
      this.w_DESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_21'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'GSTE_KMS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice non � gestito a partite, � inesistente o obsoleto oppure il codice iniziale � maggiore di quello finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PARTSN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_CODFIN)) OR  (.w_CODCON<=.w_CODFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) And .w_PARTSN='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice non � gestito a partite, � inesistente o obsoleto oppure il codice iniziale � maggiore di quello finale")
        endif
        this.w_CODCON = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PARTSN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODFIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_22'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'GSTE_KMS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice non � gestito a partite, � inesistente o obsoleto oppure il codice iniziale � maggiore di quello finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PARTSN2 = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(15)
      endif
      this.w_DESFIN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PARTSN2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_CODFIN>=.w_CODCON) or (empty(.w_CODCON))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) And .w_PARTSN2='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice non � gestito a partite, � inesistente o obsoleto oppure il codice iniziale � maggiore di quello finale")
        endif
        this.w_CODFIN = space(15)
        this.w_DESFIN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PARTSN2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCAINI_1_2.value==this.w_SCAINI)
      this.oPgFrm.Page1.oPag.oSCAINI_1_2.value=this.w_SCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAFIN_1_3.value==this.w_SCAFIN)
      this.oPgFrm.Page1.oPag.oSCAFIN_1_3.value=this.w_SCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_4.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_5.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_5.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSOSP_1_6.RadioValue()==this.w_FLSOSP)
      this.oPgFrm.Page1.oPag.oFLSOSP_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_14.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_14.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMPAR_2_1.value==this.w_NUMPAR)
      this.oPgFrm.Page2.oPag.oNUMPAR_2_1.value=this.w_NUMPAR
    endif
    if not(this.oPgFrm.Page2.oPag.oBANAPP_2_2.value==this.w_BANAPP)
      this.oPgFrm.Page2.oPag.oBANAPP_2_2.value=this.w_BANAPP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESBAN_2_3.value==this.w_DESBAN)
      this.oPgFrm.Page2.oPag.oDESBAN_2_3.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page2.oPag.oBANNOS_2_4.value==this.w_BANNOS)
      this.oPgFrm.Page2.oPag.oBANNOS_2_4.value=this.w_BANNOS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOS_2_5.value==this.w_DESNOS)
      this.oPgFrm.Page2.oPag.oDESNOS_2_5.value=this.w_DESNOS
    endif
    if not(this.oPgFrm.Page2.oPag.oNDOINI_2_6.value==this.w_NDOINI)
      this.oPgFrm.Page2.oPag.oNDOINI_2_6.value=this.w_NDOINI
    endif
    if not(this.oPgFrm.Page2.oPag.oADOINI_2_7.value==this.w_ADOINI)
      this.oPgFrm.Page2.oPag.oADOINI_2_7.value=this.w_ADOINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDDOINI_2_8.value==this.w_DDOINI)
      this.oPgFrm.Page2.oPag.oDDOINI_2_8.value=this.w_DDOINI
    endif
    if not(this.oPgFrm.Page2.oPag.oNDOFIN_2_9.value==this.w_NDOFIN)
      this.oPgFrm.Page2.oPag.oNDOFIN_2_9.value=this.w_NDOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oADOFIN_2_10.value==this.w_ADOFIN)
      this.oPgFrm.Page2.oPag.oADOFIN_2_10.value=this.w_ADOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDDOFIN_2_11.value==this.w_DDOFIN)
      this.oPgFrm.Page2.oPag.oDDOFIN_2_11.value=this.w_DDOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oVALUTA_2_12.value==this.w_VALUTA)
      this.oPgFrm.Page2.oPag.oVALUTA_2_12.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVAL_2_13.value==this.w_DESVAL)
      this.oPgFrm.Page2.oPag.oDESVAL_2_13.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oAGENTE_2_25.value==this.w_AGENTE)
      this.oPgFrm.Page2.oPag.oAGENTE_2_25.value=this.w_AGENTE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE_2_26.value==this.w_DESAGE)
      this.oPgFrm.Page2.oPag.oDESAGE_2_26.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_21.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_21.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_22.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_22.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLORIG_1_23.RadioValue()==this.w_FLORIG)
      this.oPgFrm.Page1.oPag.oFLORIG_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDESC_1_24.RadioValue()==this.w_FLDESC)
      this.oPgFrm.Page1.oPag.oFLDESC_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_25.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_25.value=this.w_DESFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_scaini<=.w_scafin) or (empty(.w_scafin)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La scadenza iniziale � maggiore della scadenza finale")
          case   not((.w_scaini<=.w_scafin) or (empty(.w_scaini)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La scadenza iniziale � maggiore della scadenza finale")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(empty(.w_BANNOS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oBANNOS_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente oppure obsoleto")
          case   not(((empty(.w_CODFIN)) OR  (.w_CODCON<=.w_CODFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) And .w_PARTSN='S')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice non � gestito a partite, � inesistente o obsoleto oppure il codice iniziale � maggiore di quello finale")
          case   not(((.w_CODFIN>=.w_CODCON) or (empty(.w_CODCON))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) And .w_PARTSN2='S')  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice non � gestito a partite, � inesistente o obsoleto oppure il codice iniziale � maggiore di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SCAINI = this.w_SCAINI
    this.o_TIPCON = this.w_TIPCON
    return

enddefine

* --- Define pages as container
define class tgste_scaPag1 as StdContainer
  Width  = 550
  height = 276
  stdWidth  = 550
  stdheight = 276
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCAINI_1_2 as StdField with uid="BUACHACMJF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SCAINI", cQueryName = "SCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La scadenza iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data iniziale scadenza da ricercare",;
    HelpContextID = 121428954,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=104, Top=16

  func oSCAINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_scaini<=.w_scafin) or (empty(.w_scafin)))
    endwith
    return bRes
  endfunc

  add object oSCAFIN_1_3 as StdField with uid="AVZCQICAXG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SCAFIN", cQueryName = "SCAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La scadenza iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data finale scadenza da ricercare",;
    HelpContextID = 42982362,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=104, Top=50

  func oSCAFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_scaini<=.w_scafin) or (empty(.w_scaini)))
    endwith
    return bRes
  endfunc


  add object oTIPCON_1_4 as StdCombo with uid="WDXDHXNHSV",rtseq=4,rtrep=.f.,left=104,top=83,width=92,height=21;
    , ToolTipText = "Tipo partite/scadenze da selezionare, clienti, fornitori";
    , HelpContextID = 36824522;
    , cFormVar="w_TIPCON",RowSource=""+"Clienti,"+"Fornitori", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_4.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPCON_1_4.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_4.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      0))
  endfunc

  func oTIPCON_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_1_21('Full')
      endif
      if .not. empty(.w_CODFIN)
        bRes2=.link_1_22('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oDATSTA_1_5 as StdField with uid="ESGOUQUDUQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 248622794,;
   bGlobalFont=.t.,;
    Height=21, Width=92, Left=453, Top=16


  add object oFLSOSP_1_6 as StdCombo with uid="FMCTCNPNEF",rtseq=6,rtrep=.f.,left=453,top=52,width=92,height=21;
    , ToolTipText = "Ricerca le scadenze per il flag sospeso";
    , HelpContextID = 266711978;
    , cFormVar="w_FLSOSP",RowSource=""+"Tutte,"+"Non sospese,"+"Sospese", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSOSP_1_6.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oFLSOSP_1_6.GetRadio()
    this.Parent.oContained.w_FLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oFLSOSP_1_6.SetRadio()
    this.Parent.oContained.w_FLSOSP=trim(this.Parent.oContained.w_FLSOSP)
    this.value = ;
      iif(this.Parent.oContained.w_FLSOSP=='T',1,;
      iif(this.Parent.oContained.w_FLSOSP=='N',2,;
      iif(this.Parent.oContained.w_FLSOSP=='S',3,;
      0)))
  endfunc

  add object oDESCON_1_14 as StdField with uid="XZILIWPJAW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 36813514,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=263, Top=114, InputMask=replicate('X',40)

  add object oCODCON_1_21 as StdField with uid="ICXFDOKTCK",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice non � gestito a partite, � inesistente o obsoleto oppure il codice iniziale � maggiore di quello finale",;
    ToolTipText = "Cliente/fornitore/di inizio selezione scadenze",;
    HelpContextID = 36872410,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=104, Top=114, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'GSTE_KMS.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oCODFIN_1_22 as StdField with uid="OIKJTJLXZT",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice non � gestito a partite, � inesistente o obsoleto oppure il codice iniziale � maggiore di quello finale",;
    ToolTipText = "Cliente/fornitore/di fine selezione scadenze",;
    HelpContextID = 42967258,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=104, Top=147, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFIN"

  func oCODFIN_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFIN_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'GSTE_KMS.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODFIN_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oFLORIG_1_23 as StdCheck with uid="SXTARWMLKD",rtseq=31,rtrep=.f.,left=106, top=178, caption="Dettaglio partite",;
    ToolTipText = "Se attivo: stampa dettaglio partite di origine",;
    HelpContextID = 159577002,;
    cFormVar="w_FLORIG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLORIG_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLORIG_1_23.GetRadio()
    this.Parent.oContained.w_FLORIG = this.RadioValue()
    return .t.
  endfunc

  func oFLORIG_1_23.SetRadio()
    this.Parent.oContained.w_FLORIG=trim(this.Parent.oContained.w_FLORIG)
    this.value = ;
      iif(this.Parent.oContained.w_FLORIG=='S',1,;
      0)
  endfunc

  add object oFLDESC_1_24 as StdCheck with uid="UEKFBGEDWL",rtseq=32,rtrep=.f.,left=263, top=178, caption="Descrizione partite",;
    ToolTipText = "Se attivo: stampa descrizione partite.",;
    HelpContextID = 217097130,;
    cFormVar="w_FLDESC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLDESC_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLDESC_1_24.GetRadio()
    this.Parent.oContained.w_FLDESC = this.RadioValue()
    return .t.
  endfunc

  func oFLDESC_1_24.SetRadio()
    this.Parent.oContained.w_FLDESC=trim(this.Parent.oContained.w_FLDESC)
    this.value = ;
      iif(this.Parent.oContained.w_FLDESC=='S',1,;
      0)
  endfunc

  add object oDESFIN_1_25 as StdField with uid="HVNQNBDJGA",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 42908362,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=263, Top=147, InputMask=replicate('X',40)


  add object oBtn_1_26 as StdButton with uid="SQNLIXYMPH",left=447, top=231, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 217492186;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        do GSTE_BCA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object Output as cp_outputCombo with uid="ZLERXIVPWT",left=106, top=205, width=378,height=22,;
    caption='Output',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 161643034


  add object oBtn_1_29 as StdButton with uid="VJEYGWVSZN",left=497, top=231, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 184906566;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_35 as cp_runprogram with uid="RKLIARGCXL",left=601, top=263, width=89,height=21,;
    caption='GSTE_BC1',;
   bGlobalFont=.t.,;
    prg="GSTE_BC1",;
    cEvent = "Output",;
    nPag=1;
    , HelpContextID = 221224041

  add object oStr_1_7 as StdString with uid="HRHDNGMHAE",Visible=.t., Left=5, Top=16,;
    Alignment=1, Width=99, Height=15,;
    Caption="Scadenze da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="PMYSREYHZS",Visible=.t., Left=4, Top=50,;
    Alignment=1, Width=99, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="MJOOVSZMAE",Visible=.t., Left=331, Top=52,;
    Alignment=1, Width=119, Height=15,;
    Caption="Test sospese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="DKKXSABCFP",Visible=.t., Left=68, Top=83,;
    Alignment=1, Width=34, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="VNGFGUEZQD",Visible=.t., Left=4, Top=114,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="SOQNUXLCBD",Visible=.t., Left=4, Top=114,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'F')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="OWNJEUYTGK",Visible=.t., Left=345, Top=16,;
    Alignment=1, Width=105, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="ZUKMDNZAHR",Visible=.t., Left=4, Top=147,;
    Alignment=1, Width=99, Height=18,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'F')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="BQVWQOTDBI",Visible=.t., Left=4, Top=147,;
    Alignment=1, Width=99, Height=18,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="LXYZRMJLIY",Visible=.t., Left=13, Top=205,;
    Alignment=1, Width=89, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine
define class tgste_scaPag2 as StdContainer
  Width  = 550
  height = 276
  stdWidth  = 550
  stdheight = 276
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMPAR_2_1 as StdField with uid="JUQOMHYDXS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NUMPAR", cQueryName = "NUMPAR",;
    bObbl = .f. , nPag = 2, value=space(31), bMultilanguage =  .f.,;
    ToolTipText = "Numero partita di selezione",;
    HelpContextID = 251988522,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=124, Top=15, InputMask=replicate('X',31)

  add object oBANAPP_2_2 as StdField with uid="PLRTHMFISS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_BANAPP", cQueryName = "BANAPP",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca di appoggio del cliente/fornitore",;
    HelpContextID = 2363114,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=124, Top=46, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANAPP"

  func oBANAPP_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANAPP_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANAPP_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oBANAPP_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oBANAPP_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANAPP
     i_obj.ecpSave()
  endproc

  add object oDESBAN_2_3 as StdField with uid="XLWKXOVCNH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 51559114,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=262, Top=46, InputMask=replicate('X',50)

  add object oBANNOS_2_4 as StdField with uid="BFMKOPRZXR",rtseq=15,rtrep=.f.,;
    cFormVar = "w_BANNOS", cQueryName = "BANNOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente oppure obsoleto",;
    ToolTipText = "Ns C/C comunicato a fornitore per RB o a cliente per bonifico",;
    HelpContextID = 220663530,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=124, Top=77, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANNOS"

  func oBANNOS_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANNOS_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANNOS_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oBANNOS_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banca",'',this.parent.oContained
  endproc
  proc oBANNOS_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANNOS
     i_obj.ecpSave()
  endproc

  add object oDESNOS_2_5 as StdField with uid="OSCDGUJGEM",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESNOS", cQueryName = "DESNOS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 220641994,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=262, Top=77, InputMask=replicate('X',35)

  add object oNDOINI_2_6 as StdField with uid="MFFRYELHLU",rtseq=17,rtrep=.f.,;
    cFormVar = "w_NDOINI", cQueryName = "NDOINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 121371434,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=124, Top=108, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOINI_2_7 as StdField with uid="LFYMWAETPI",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ADOINI", cQueryName = "ADOINI",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di inizio selezione",;
    HelpContextID = 121371642,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=271, Top=108, InputMask=replicate('X',10)

  add object oDDOINI_2_8 as StdField with uid="GVHSMJJJQT",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DDOINI", cQueryName = "DDOINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento inizio selezione",;
    HelpContextID = 121371594,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=465, Top=108

  add object oNDOFIN_2_9 as StdField with uid="YVXOAZLBNP",rtseq=20,rtrep=.f.,;
    cFormVar = "w_NDOFIN", cQueryName = "NDOFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 42924842,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=124, Top=139, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOFIN_2_10 as StdField with uid="WVYMVOQZXQ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_ADOFIN", cQueryName = "ADOFIN",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di fine selezione",;
    HelpContextID = 42925050,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=271, Top=139, InputMask=replicate('X',10)

  add object oDDOFIN_2_11 as StdField with uid="YKXKJINOJS",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DDOFIN", cQueryName = "DDOFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento fine selezione",;
    HelpContextID = 42925002,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=465, Top=139

  add object oVALUTA_2_12 as StdField with uid="KITUVABRTZ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta, se vuota tutte",;
    HelpContextID = 248524202,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=124, Top=170, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oVALUTA_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVALUTA_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oVALUTA_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oVALUTA_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_VALUTA
     i_obj.ecpSave()
  endproc

  add object oDESVAL_2_13 as StdField with uid="IPQSGATYTZ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 83802826,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=192, Top=170, InputMask=replicate('X',35)

  add object oAGENTE_2_25 as StdField with uid="LPZDJMOTNX",rtseq=25,rtrep=.f.,;
    cFormVar = "w_AGENTE", cQueryName = "AGENTE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente di selezione (se vuoto=no selezione)",;
    HelpContextID = 181901562,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=124, Top=201, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_AGENTE"

  func oAGENTE_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCON='C')
    endwith
   endif
  endfunc

  func oAGENTE_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGENTE_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGENTE_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oAGENTE_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Elenco agenti",'AGE1ZOOM.AGENTI_VZM',this.parent.oContained
  endproc
  proc oAGENTE_2_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_AGENTE
     i_obj.ecpSave()
  endproc

  add object oDESAGE_2_26 as StdField with uid="LKEGMTSGUL",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 196328138,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=192, Top=201, InputMask=replicate('X',35)

  add object oStr_2_14 as StdString with uid="OJNPKZHYTH",Visible=.t., Left=261, Top=108,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="LLRZQJNAKN",Visible=.t., Left=407, Top=108,;
    Alignment=1, Width=55, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="JTWYBEKTDW",Visible=.t., Left=261, Top=137,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="HHLBYQEEYA",Visible=.t., Left=407, Top=137,;
    Alignment=1, Width=55, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="BCXAOTEFTP",Visible=.t., Left=4, Top=170,;
    Alignment=1, Width=118, Height=15,;
    Caption="Codice valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="ELXUJQFUPN",Visible=.t., Left=4, Top=15,;
    Alignment=1, Width=118, Height=15,;
    Caption="Numero partita:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="SFGBWPEBBO",Visible=.t., Left=4, Top=77,;
    Alignment=1, Width=118, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="EZXYCTGLBA",Visible=.t., Left=4, Top=46,;
    Alignment=1, Width=118, Height=15,;
    Caption="Banca di appoggio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="HPKOJQQYXJ",Visible=.t., Left=4, Top=108,;
    Alignment=1, Width=118, Height=15,;
    Caption="Da n. documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="XNNPVHYWCF",Visible=.t., Left=4, Top=137,;
    Alignment=1, Width=118, Height=15,;
    Caption="A n. documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="JBHFNNXGLO",Visible=.t., Left=5, Top=201,;
    Alignment=1, Width=117, Height=18,;
    Caption="Codice agente:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_sca','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
