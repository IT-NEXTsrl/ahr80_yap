* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gssr_kad                                                        *
*              Analisi pre-storicizzazione documenti                           *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_98]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-04                                                      *
* Last revis.: 2017-02-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgssr_kad",oParentObject))

* --- Class definition
define class tgssr_kad as StdForm
  Top    = -1
  Left   = 6

  * --- Standard Properties
  Width  = 692
  Height = 453+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-02-07"
  HelpContextID=132736361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  DOC_DETT_IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  cPrg = "gssr_kad"
  cComment = "Analisi pre-storicizzazione documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(4)
  w_CODESE = space(4)
  w_STORICO = space(1)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_TIPANA = space(1)
  w_VALNAZ = space(3)
  w_SELALL = 0
  w_SERIALE = space(10)
  w_PARAME = space(3)
  w_SERPNT = space(10)
  w_ROWNUM = 0
  w_NUMRIF = 0
  w_STO_OK = space(1)
  w_Msg = space(0)
  w_TIPO = space(1)
  w_TIPDOC = space(5)
  w_DESDOC = space(35)
  w_FLINTE = space(1)
  o_FLINTE = space(1)
  w_CODCON = space(15)
  w_ANDESCRI = space(40)
  w_STATO = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_ZoomDoc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgssr_kadPag1","gssr_kad",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Analisi")
      .Pages(2).addobject("oPag","tgssr_kadPag2","gssr_kad",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni risultato")
      .Pages(3).addobject("oPag","tgssr_kadPag3","gssr_kad",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Messaggi elaborazione")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODESE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDoc = this.oPgFrm.Pages(1).oPag.ZoomDoc
    DoDefault()
    proc Destroy()
      this.w_ZoomDoc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='CONTI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(4)
      .w_CODESE=space(4)
      .w_STORICO=space(1)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_TIPANA=space(1)
      .w_VALNAZ=space(3)
      .w_SELALL=0
      .w_SERIALE=space(10)
      .w_PARAME=space(3)
      .w_SERPNT=space(10)
      .w_ROWNUM=0
      .w_NUMRIF=0
      .w_STO_OK=space(1)
      .w_Msg=space(0)
      .w_TIPO=space(1)
      .w_TIPDOC=space(5)
      .w_DESDOC=space(35)
      .w_FLINTE=space(1)
      .w_CODCON=space(15)
      .w_ANDESCRI=space(40)
      .w_STATO=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODESE))
          .link_1_2('Full')
        endif
        .w_STORICO = 'N'
          .DoRTCalc(4,5,.f.)
        .w_TIPANA = 'T'
      .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
          .DoRTCalc(7,7,.f.)
        .w_SELALL = 0
        .w_SERIALE = Nvl( .w_ZoomDoc.getVar('ANSERIAL') ,'' )
        .w_PARAME = Nvl( .w_ZoomDoc.getVar('FLVEAC')+.w_ZoomDoc.getVar('CLADOC') , '' )
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .w_SERPNT = Nvl( .w_ZoomDoc.getVar('MVRIFCON') , '')
        .w_ROWNUM = Nvl ( .w_ZoomDoc.getVar('ANROWNUM')  , 0 )
        .w_NUMRIF = Nvl ( .w_ZoomDoc.getVar('ANNUMRIF') , 0 )
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_NUMRIF))
          .link_1_28('Full')
        endif
        .w_STO_OK = Nvl ( .w_ZoomDoc.getVar('ANSTO_OK') , '' )
          .DoRTCalc(15,15,.f.)
        .w_TIPO = 'T'
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_TIPDOC))
          .link_2_4('Full')
        endif
          .DoRTCalc(18,19,.f.)
        .w_CODCON = Space(15)
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CODCON))
          .link_2_8('Full')
        endif
          .DoRTCalc(21,21,.f.)
        .w_STATO = 'T'
          .DoRTCalc(23,25,.f.)
        .w_OBTEST = i_INIDAT
      .oPgFrm.Page2.oPag.oObj_2_18.Calculate(AH_MSGFORMAT("Impostare i filtri per visualizzare una parte del risultato dell'analisi%0I filtri non hanno effetto sui documenti antenati",0))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .DoRTCalc(1,8,.t.)
            .w_SERIALE = Nvl( .w_ZoomDoc.getVar('ANSERIAL') ,'' )
            .w_PARAME = Nvl( .w_ZoomDoc.getVar('FLVEAC')+.w_ZoomDoc.getVar('CLADOC') , '' )
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
            .w_SERPNT = Nvl( .w_ZoomDoc.getVar('MVRIFCON') , '')
            .w_ROWNUM = Nvl ( .w_ZoomDoc.getVar('ANROWNUM')  , 0 )
            .w_NUMRIF = Nvl ( .w_ZoomDoc.getVar('ANNUMRIF') , 0 )
          .link_1_28('Full')
            .w_STO_OK = Nvl ( .w_ZoomDoc.getVar('ANSTO_OK') , '' )
        .DoRTCalc(15,19,.t.)
        if .o_FLINTE<>.w_FLINTE
            .w_CODCON = Space(15)
          .link_2_8('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_2_18.Calculate(AH_MSGFORMAT("Impostare i filtri per visualizzare una parte del risultato dell'analisi%0I filtri non hanno effetto sui documenti antenati",0))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_18.Calculate(AH_MSGFORMAT("Impostare i filtri per visualizzare una parte del risultato dell'analisi%0I filtri non hanno effetto sui documenti antenati",0))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSELALL_1_14.enabled_(this.oPgFrm.Page1.oPag.oSELALL_1_14.mCond())
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oStr_2_7.visible=!this.oPgFrm.Page2.oPag.oStr_2_7.mHide()
    this.oPgFrm.Page2.oPag.oCODCON_2_8.visible=!this.oPgFrm.Page2.oPag.oCODCON_2_8.mHide()
    this.oPgFrm.Page2.oPag.oANDESCRI_2_9.visible=!this.oPgFrm.Page2.oPag.oANDESCRI_2_9.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomDoc.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_18.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_2'),i_cWhere,'',"Elenco esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMRIF
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_DETT_IDX,3]
    i_lTable = "DOC_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2], .t., this.DOC_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,CPROWNUM,MVNUMRIF";
                   +" from "+i_cTable+" "+i_lTable+" where MVNUMRIF="+cp_ToStrODBC(this.w_NUMRIF);
                   +" and MVSERIAL="+cp_ToStrODBC(this.w_SERIALE);
                   +" and CPROWNUM="+cp_ToStrODBC(this.w_ROWNUM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_SERIALE;
                       ,'CPROWNUM',this.w_ROWNUM;
                       ,'MVNUMRIF',this.w_NUMRIF)
            select MVSERIAL,CPROWNUM,MVNUMRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMRIF = NVL(_Link_.MVNUMRIF,0)
    else
      if i_cCtrl<>'Load'
        this.w_NUMRIF = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_DETT_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)+'\'+cp_ToStr(_Link_.MVNUMRIF,1)
      cp_ShowWarn(i_cKey,this.DOC_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPDOC
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC,TDFLINTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_2_4'),i_cWhere,'GSVE_ATD',"Elenco tipi documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_FLINTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLINTE);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_FLINTE;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_2_8'),i_cWhere,'GSAR_BZC',"Intestatari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLINTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_FLINTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLINTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_FLINTE;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_ANDESCRI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCON = space(15)
        this.w_ANDESCRI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_2.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_2.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oSTORICO_1_4.RadioValue()==this.w_STORICO)
      this.oPgFrm.Page1.oPag.oSTORICO_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINIESE_1_5.value==this.w_INIESE)
      this.oPgFrm.Page1.oPag.oINIESE_1_5.value=this.w_INIESE
    endif
    if not(this.oPgFrm.Page1.oPag.oFINESE_1_8.value==this.w_FINESE)
      this.oPgFrm.Page1.oPag.oFINESE_1_8.value=this.w_FINESE
    endif
    if not(this.oPgFrm.Page1.oPag.oVALNAZ_1_11.value==this.w_VALNAZ)
      this.oPgFrm.Page1.oPag.oVALNAZ_1_11.value=this.w_VALNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oSELALL_1_14.RadioValue()==this.w_SELALL)
      this.oPgFrm.Page1.oPag.oSELALL_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oMsg_3_1.value==this.w_Msg)
      this.oPgFrm.Page3.oPag.oMsg_3_1.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPO_2_2.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page2.oPag.oTIPO_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDOC_2_4.value==this.w_TIPDOC)
      this.oPgFrm.Page2.oPag.oTIPDOC_2_4.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDOC_2_5.value==this.w_DESDOC)
      this.oPgFrm.Page2.oPag.oDESDOC_2_5.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON_2_8.value==this.w_CODCON)
      this.oPgFrm.Page2.oPag.oCODCON_2_8.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page2.oPag.oANDESCRI_2_9.value==this.w_ANDESCRI)
      this.oPgFrm.Page2.oPag.oANDESCRI_2_9.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oSTATO_2_11.RadioValue()==this.w_STATO)
      this.oPgFrm.Page2.oPag.oSTATO_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINI_2_12.value==this.w_DATINI)
      this.oPgFrm.Page2.oPag.oDATINI_2_12.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFIN_2_13.value==this.w_DATFIN)
      this.oPgFrm.Page2.oPag.oDATFIN_2_13.value=this.w_DATFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODESE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(Not .w_FLINTE$'C-F')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCON_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLINTE = this.w_FLINTE
    return

enddefine

* --- Define pages as container
define class tgssr_kadPag1 as StdContainer
  Width  = 688
  height = 453
  stdWidth  = 688
  stdheight = 453
  resizeXpos=626
  resizeYpos=346
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODESE_1_2 as StdField with uid="QBGAPNSEQE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esericzio da storicizzare",;
    HelpContextID = 225432026,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=66, Top=11, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco esercizi",'',this.parent.oContained
  endproc


  add object oSTORICO_1_4 as StdCombo with uid="PZJJEWZENM",rtseq=3,rtrep=.f.,left=485,top=11,width=117,height=21;
    , Tabstop=.f.;
    , ToolTipText = "Filtra i dati in base all'attività svolta sul risultato di analisi precedenti";
    , HelpContextID = 138202;
    , cFormVar="w_STORICO",RowSource=""+"Da controllare,"+"Controllati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTORICO_1_4.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oSTORICO_1_4.GetRadio()
    this.Parent.oContained.w_STORICO = this.RadioValue()
    return .t.
  endfunc

  func oSTORICO_1_4.SetRadio()
    this.Parent.oContained.w_STORICO=trim(this.Parent.oContained.w_STORICO)
    this.value = ;
      iif(this.Parent.oContained.w_STORICO=='N',1,;
      iif(this.Parent.oContained.w_STORICO=='A',2,;
      0))
  endfunc

  add object oINIESE_1_5 as StdField with uid="FMJFPRTCIL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_INIESE", cQueryName = "INIESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio esercizio",;
    HelpContextID = 225411706,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=165, Top=11

  add object oFINESE_1_8 as StdField with uid="VGLKCZJIOO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FINESE", cQueryName = "FINESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine esercizio",;
    HelpContextID = 225392554,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=271, Top=11

  add object oVALNAZ_1_11 as StdField with uid="JTQAJEARDT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_VALNAZ", cQueryName = "VALNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta nazionale esercizio",;
    HelpContextID = 108634454,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=374, Top=11, InputMask=replicate('X',3)


  add object oBtn_1_12 as StdButton with uid="QYBPKEREZO",left=631, top=4, width=48,height=42,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare risultati analisi";
    , HelpContextID = 148136518;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSSR_BAD(this.Parent.oContained,"VEDI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_CODESE ))
      endwith
    endif
  endfunc


  add object ZoomDoc as cp_szoombox with uid="LWJQGUGBQG",left=1, top=48, width=684,height=352,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="DOC_MAST",cZoomFile="GSSR_KAD",bOptions=.f.,bQueryOnDblClick=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , ToolTipText = "La visual query dello zoom viene re-impostata in gssr_bad a gssr_kad.vqr";
    , HelpContextID = 45261286

  add object oSELALL_1_14 as StdRadio with uid="ULZHLIAPFO",rtseq=8,rtrep=.f.,left=4, top=406, width=136,height=32;
    , TabStop=.f.;
    , ToolTipText = "Seleziona deleseziona elementi";
    , cFormVar="w_SELALL", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELALL_1_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 115563226
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 115563226
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona deleseziona elementi")
      StdRadio::init()
    endproc

  func oSELALL_1_14.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,0,;
    0)))
  endfunc
  func oSELALL_1_14.GetRadio()
    this.Parent.oContained.w_SELALL = this.RadioValue()
    return .t.
  endfunc

  func oSELALL_1_14.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SELALL==1,1,;
      iif(this.Parent.oContained.w_SELALL==0,2,;
      0))
  endfunc

  func oSELALL_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STO_OK<>'T')
    endwith
   endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="IVUYXSPMRW",left=140, top=406, width=48,height=45,;
    CpPicture="bmp\doc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il documento associato alla riga selezionata";
    , HelpContextID = 1139258;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_SERIALE, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE) And .w_STO_OK<>'T')
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="IVDEPLUZUC",left=191, top=406, width=48,height=45,;
    CpPicture="BMP\tracciabilita.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la tracciabilità del documento selezionato";
    , HelpContextID = 56784054;
    , Caption='\<Tracciab.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSSR_BAD(this.Parent.oContained,"Tracciabilita")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_SERIALE ))
      endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="XEDXHWOFBI",left=242, top=406, width=48,height=45,;
    CpPicture="bmp\VERIFICA.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza la registrazione di prima nota generata dal documento";
    , HelpContextID = 13282806;
    , Caption='\<Reg.Cont.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_SERPNT)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_COGE='S' AND NOT EMPTY(.w_SERPNT)  And .w_STO_OK<>'T')
      endwith
    endif
  endfunc


  add object oBtn_1_18 as StdButton with uid="IUBVGKCRZN",left=384, top=406, width=48,height=45,;
    CpPicture="BMP\LEGENDA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la legenda";
    , HelpContextID = 56784054;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      do GSSR_KLE with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="KSXKBPWACS",left=579, top=406, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare i documenti selezionati dai documenti non storicizzabili";
    , HelpContextID = 226645014;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSSR_BAD(this.Parent.oContained,"MARCA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_CODESE ) And .w_STO_OK=' ')
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="PXBGFOADEM",left=631, top=406, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 226645014;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_23 as cp_runprogram with uid="EOADVHPTSG",left=2, top=490, width=227,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSSR_BAD('SELEZIONA')",;
    cEvent = "w_SELALL Changed",;
    nPag=1;
    , ToolTipText = "Seleziona / deseleziona";
    , HelpContextID = 45261286


  add object oObj_1_24 as cp_runprogram with uid="GGATGSJACH",left=2, top=518, width=227,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSSR_BAD('CHECK')",;
    cEvent = "ZOOMDOC row checked",;
    nPag=1;
    , ToolTipText = "Check riga";
    , HelpContextID = 45261286


  add object oObj_1_25 as cp_runprogram with uid="ZFWEWOFZFZ",left=1, top=546, width=227,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSSR_BAD('UNCHECK')",;
    cEvent = "ZOOMDOC row unchecked",;
    nPag=1;
    , ToolTipText = "Uncheck riga";
    , HelpContextID = 45261286

  add object oStr_1_3 as StdString with uid="XUJQWVEMDJ",Visible=.t., Left=1, Top=15,;
    Alignment=1, Width=62, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="YVUHVTDVBP",Visible=.t., Left=119, Top=15,;
    Alignment=1, Width=42, Height=18,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="FWOFMLPYSY",Visible=.t., Left=243, Top=15,;
    Alignment=1, Width=26, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="STWYAAMLAQ",Visible=.t., Left=349, Top=15,;
    Alignment=1, Width=22, Height=18,;
    Caption="in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="QUIHQXYZKN",Visible=.t., Left=414, Top=13,;
    Alignment=1, Width=68, Height=18,;
    Caption="Elenco:"  ;
  , bGlobalFont=.t.
enddefine
define class tgssr_kadPag2 as StdContainer
  Width  = 688
  height = 453
  stdWidth  = 688
  stdheight = 453
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPO_2_2 as StdCombo with uid="DBMGXGBKCM",rtseq=16,rtrep=.f.,left=137,top=79,width=140,height=21;
    , Tabstop=.f.;
    , HelpContextID = 127211210;
    , cFormVar="w_TIPO",RowSource=""+"Da verificare (tutti),"+"Da contabilizzare,"+"Ordini aperti,"+"Impegni aperti,"+"Doc.esterni che evadono,"+"Doc.aperti esercizio", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPO_2_2.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'C',;
    iif(this.value =3,'O',;
    iif(this.value =4,'I',;
    iif(this.value =5,'F',;
    iif(this.value =6,'S',;
    space(1))))))))
  endfunc
  func oTIPO_2_2.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_2_2.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='T',1,;
      iif(this.Parent.oContained.w_TIPO=='C',2,;
      iif(this.Parent.oContained.w_TIPO=='O',3,;
      iif(this.Parent.oContained.w_TIPO=='I',4,;
      iif(this.Parent.oContained.w_TIPO=='F',5,;
      iif(this.Parent.oContained.w_TIPO=='S',6,;
      0))))))
  endfunc

  add object oTIPDOC_2_4 as StdField with uid="JMTBTEECVG",rtseq=17,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipo documento",;
    HelpContextID = 263198410,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=137, Top=123, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Elenco tipi documenti",'',this.parent.oContained
  endproc
  proc oTIPDOC_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPDOC
     i_obj.ecpSave()
  endproc

  add object oDESDOC_2_5 as StdField with uid="XMDEBGWAPK",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 263187402,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=202, Top=123, InputMask=replicate('X',35)

  add object oCODCON_2_8 as StdField with uid="BIANSQMZQN",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice intestatario",;
    HelpContextID = 78762458,;
   bGlobalFont=.t.,;
    Height=21, Width=129, Left=137, Top=159, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_FLINTE", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_2_8.mHide()
    with this.Parent.oContained
      return (Not .w_FLINTE$'C-F')
    endwith
  endfunc

  func oCODCON_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_FLINTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_FLINTE)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Intestatari",'',this.parent.oContained
  endproc
  proc oCODCON_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_FLINTE
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oANDESCRI_2_9 as StdField with uid="AZWPETXYMV",rtseq=21,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 9448783,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=269, Top=159, InputMask=replicate('X',40)

  func oANDESCRI_2_9.mHide()
    with this.Parent.oContained
      return (Not .w_FLINTE$'C-F')
    endwith
  endfunc


  add object oSTATO_2_11 as StdCombo with uid="KLDWGYENNV",rtseq=22,rtrep=.f.,left=137,top=204,width=135,height=21;
    , Tabstop=.f.;
    , ToolTipText = "Stato del documento";
    , HelpContextID = 44104666;
    , cFormVar="w_STATO",RowSource=""+"Tutti,"+"Confermato,"+"Provvisorio", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oSTATO_2_11.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oSTATO_2_11.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_2_11.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='T',1,;
      iif(this.Parent.oContained.w_STATO=='N',2,;
      iif(this.Parent.oContained.w_STATO=='S',3,;
      0)))
  endfunc

  add object oDATINI_2_12 as StdField with uid="HTBCCMIQFI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 163241930,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=137, Top=250

  add object oDATFIN_2_13 as StdField with uid="YNDOCVMZZB",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 84795338,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=241, Top=250


  add object oObj_2_18 as cp_calclbl with uid="PSSKDOBUQN",left=10, top=11, width=668,height=56,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=2;
    , HelpContextID = 45261286

  add object oStr_2_1 as StdString with uid="ARCVTYJEBX",Visible=.t., Left=8, Top=82,;
    Alignment=1, Width=123, Height=18,;
    Caption="Risultati:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="ZLSLJTLPWO",Visible=.t., Left=6, Top=123,;
    Alignment=1, Width=125, Height=18,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="FVKPSLQVUG",Visible=.t., Left=8, Top=159,;
    Alignment=1, Width=123, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  func oStr_2_7.mHide()
    with this.Parent.oContained
      return (Not .w_FLINTE$'C-F')
    endwith
  endfunc

  add object oStr_2_10 as StdString with uid="FWGBNZKFFL",Visible=.t., Left=9, Top=204,;
    Alignment=1, Width=122, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="HOEVHHMKNU",Visible=.t., Left=27, Top=250,;
    Alignment=1, Width=104, Height=17,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="GXQGGCQDWL",Visible=.t., Left=213, Top=250,;
    Alignment=1, Width=25, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.
enddefine
define class tgssr_kadPag3 as StdContainer
  Width  = 688
  height = 453
  stdWidth  = 688
  stdheight = 453
  resizeXpos=558
  resizeYpos=389
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMsg_3_1 as StdMemo with uid="ATBBCHAJSX",rtseq=15,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 3, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Messaggi durante l'elaborazione",;
    HelpContextID = 132283706,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=436, Width=672, Left=6, Top=5, tabstop = .f., readonly = .t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gssr_kad','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
