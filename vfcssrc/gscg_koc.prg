* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_koc                                                        *
*              Riferimenti contratto                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-07-15                                                      *
* Last revis.: 2011-08-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_koc",oParentObject))

* --- Class definition
define class tgscg_koc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 530
  Height = 280
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-08-22"
  HelpContextID=166339945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_koc"
  cComment = "Riferimenti contratto"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_DESCLF = space(40)
  w_PNCODCLF = space(15)
  w_DESCLF = space(40)
  w_RIFCON = space(30)
  w_CONF_ELAB = space(1)
  w_TIPOCONTRATTO = space(1)
  w_ragru = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kocPag1","gscg_koc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ragru = this.oPgFrm.Pages(1).oPag.ragru
    DoDefault()
    proc Destroy()
      this.w_ragru = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PNTIPCLF=space(1)
      .w_PNCODCLF=space(15)
      .w_DESCLF=space(40)
      .w_PNCODCLF=space(15)
      .w_DESCLF=space(40)
      .w_RIFCON=space(30)
      .w_CONF_ELAB=space(1)
      .w_TIPOCONTRATTO=space(1)
      .w_PNTIPCLF=oParentObject.w_PNTIPCLF
      .w_PNCODCLF=oParentObject.w_PNCODCLF
      .w_DESCLF=oParentObject.w_DESCLF
      .w_PNCODCLF=oParentObject.w_PNCODCLF
      .w_DESCLF=oParentObject.w_DESCLF
      .w_RIFCON=oParentObject.w_RIFCON
      .w_CONF_ELAB=oParentObject.w_CONF_ELAB
      .w_TIPOCONTRATTO=oParentObject.w_TIPOCONTRATTO
      .oPgFrm.Page1.oPag.ragru.Calculate()
          .DoRTCalc(1,5,.f.)
        .w_RIFCON = .w_ragru.getVar('OSRIFCON')
      .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .w_CONF_ELAB = 'S'
        .w_TIPOCONTRATTO = .w_ragru.getVar('OSTIPOPE')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PNTIPCLF=.w_PNTIPCLF
      .oParentObject.w_PNCODCLF=.w_PNCODCLF
      .oParentObject.w_DESCLF=.w_DESCLF
      .oParentObject.w_PNCODCLF=.w_PNCODCLF
      .oParentObject.w_DESCLF=.w_DESCLF
      .oParentObject.w_RIFCON=.w_RIFCON
      .oParentObject.w_CONF_ELAB=.w_CONF_ELAB
      .oParentObject.w_TIPOCONTRATTO=.w_TIPOCONTRATTO
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ragru.Calculate()
        .DoRTCalc(1,5,.t.)
            .w_RIFCON = .w_ragru.getVar('OSRIFCON')
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
            .w_CONF_ELAB = 'S'
            .w_TIPOCONTRATTO = .w_ragru.getVar('OSTIPOPE')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ragru.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ragru.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPNCODCLF_1_2.value==this.w_PNCODCLF)
      this.oPgFrm.Page1.oPag.oPNCODCLF_1_2.value=this.w_PNCODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_5.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_5.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCODCLF_1_6.value==this.w_PNCODCLF)
      this.oPgFrm.Page1.oPag.oPNCODCLF_1_6.value=this.w_PNCODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_9.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_9.value=this.w_DESCLF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kocPag1 as StdContainer
  Width  = 526
  height = 280
  stdWidth  = 526
  stdheight = 280
  resizeXpos=217
  resizeYpos=146
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPNCODCLF_1_2 as StdField with uid="TNOYPOOZHJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PNCODCLF", cQueryName = "PNCODCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 39231940,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=89, Top=-271, InputMask=replicate('X',15)

  add object oDESCLF_1_5 as StdField with uid="GDSKXGBWMZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 249670602,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=220, Top=-271, InputMask=replicate('X',40)

  add object oPNCODCLF_1_6 as StdField with uid="CINRTCNUBC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PNCODCLF", cQueryName = "PNCODCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 39231940,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=89, Top=9, InputMask=replicate('X',15)

  add object oDESCLF_1_9 as StdField with uid="DJVMBHSDFR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 249670602,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=220, Top=9, InputMask=replicate('X',40)


  add object ragru as cp_zoombox with uid="IMYJOIBLJT",left=3, top=42, width=517,height=232,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OPERSUPE",cZoomFile="GSCG_KOC",bOptions=.f.,bAdvOptions=.f.,bRetriveAllRows=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 11657702


  add object oObj_1_12 as cp_runprogram with uid="JQNMJKTDTH",left=100, top=287, width=159,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BOS('A')",;
    cEvent = "w_ragru selected",;
    nPag=1;
    , HelpContextID = 11657702

  add object oStr_1_3 as StdString with uid="UJKYBVMJAQ",Visible=.t., Left=7, Top=-268,;
    Alignment=1, Width=77, Height=18,;
    Caption="Cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (.w_PNTIPCLF<>'C')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="KRDLCJCXTN",Visible=.t., Left=7, Top=-268,;
    Alignment=1, Width=77, Height=18,;
    Caption="Fornitore:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (.w_PNTIPCLF<>'F')
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="DKZICCHMVQ",Visible=.t., Left=7, Top=12,;
    Alignment=1, Width=77, Height=18,;
    Caption="Cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_PNTIPCLF<>'C')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="UHJZENLZGN",Visible=.t., Left=7, Top=12,;
    Alignment=1, Width=77, Height=18,;
    Caption="Fornitore:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_PNTIPCLF<>'F')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_koc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
