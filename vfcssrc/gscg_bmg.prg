* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bmg                                                        *
*              STAMPA MODELLO F24 - 2007                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-26                                                      *
* Last revis.: 2013-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bmg",oParentObject)
return(i_retval)

define class tgscg_bmg as StdBatch
  * --- Local variables
  w_VFDTPRES = ctod("  /  /  ")
  w_OKIMP = .f.
  w_MESS = space(100)
  w_MFENTRAT = space(1)
  w_CFSECCOD = space(16)
  w_cfcodide = space(16)
  w_CICLO = 0
  w_TOTASC = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Preleva i dati relativi al modello F24 individuato dal seriale, genera il file .FDF 
    * --- ed esegue  l'associazione al documento PDF (da GSCG_AMF, GSCG_SMF)
    * --- Cursore Curs1
    VQ_EXEC("QUERY\GSCG_SMF.VQR", this, "Curs1")
    * --- Memorizzo la data di Presentazione per distinguere il tipo di stampa
    this.w_VFDTPRES = Curs1.VFDTPRES
    * --- Controllo segno Saldo Finale
    if mfsalfin < 0
      this.w_MESS = "Attenzione: importi compensati maggiori degli importi dovuti%0"
      AH_ERRORMSG(this.w_MESS,48,"")
    endif
    * --- Cursore Curs2
    VQ_EXEC("QUERY\GSCG1SMF.VQR", this, "Curs2")
    SELECT * FROM Curs1 LEFT OUTER JOIN Curs2 ON Curs1.MFSERIAL=Curs2.EFSERIAL into Cursor Model
    if this.w_VFDTPRES >= cp_CharToDate("01-01-2003")
      * --- Creo un nuovo cursore contenente i dati relativi alla sezione ICI ed altri tributi locali
      VQ_EXEC("QUERY\GSCG2SMF.VQR", this, "Curs3")
      Select Curs3
      replace IFCODEL1 with right(space(4)+alltrim(nvl(IFCODEL1," ")),4) all
      replace IFCODEL2 with right(space(4)+alltrim(nvl(IFCODEL2," ")),4) all
      replace IFCODEL3 with right(space(4)+alltrim(nvl(IFCODEL3," ")),4) all
      replace IFCODEL4 with right(space(4)+alltrim(nvl(IFCODEL4," ")),4) all
    endif
    Select Model
    this.w_MFENTRAT = NVL (MFENTRAT," ")
    pun=fcreate (tempadhoc()+"\MODEF24.fdf")
    fputs(pun,"%FDF-1.2")
    fputs(pun,"%����")
    fputs(pun,"1 0 obj")
    fputs(pun,"<< ")
    * --- Sezione Contribuente
    this.w_CFSECCOD = IIF (EMPTY (NVL (MFCARFIR ," ")) OR NVL (MFCARFIR ," ")="1" OR this.w_MFENTRAT<>"E", CFSECCOD , NVL (MFCODFIR," ")) 
    this.w_cfcodide = IIF (EMPTY (NVL (MFCARFIR ," ")) OR NVL (MFCARFIR ," ")="1" OR this.w_MFENTRAT<>"E" , cfcodide , right ( "00"+alltrim (NVL (MFCARFIR," ")) ,2)) 
    fputs(pun,"/FDF << /Fields [ << /V ("+ IIF ( this.w_MFENTRAT="E" ," ", iif(g_Teso="S",alltrim(nvl(cfdelegt," ")),alltrim(nvl(cfdelegb," "))))+")/T (Banca)>>")
    fputs(pun,"<< /V ("+iif(mf_coinc="S"," ","X")+")/T (Coinc)>>")
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",alltrim(nvl(cfcodage," ")))+")/T (Agenzia)>>")
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",alltrim(nvl(cfagprov," ")))+")/T (Prov1)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,1,1)+")/T (cf1)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,2,1)+")/T (cf2)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,3,1)+")/T (cf3)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,4,1)+")/T (cf4)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,5,1)+")/T (cf5)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,6,1)+")/T (cf6)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,7,1)+")/T (cf7)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,8,1)+")/T (cf8)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,9,1)+")/T (cf9)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,10,1)+")/T (cf10)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,11,1)+")/T (cf11)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,12,1)+")/T (cf12)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,13,1)+")/T (cf13)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,14,1)+")/T (cf14)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,15,1)+")/T (cf15)>>")
    fputs(pun,"<< /V ("+substr(cfcodfis,16,1)+")/T (cf16)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(cfragsoc," "))+")/T (Ragsoc)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(cf__nome," "))+")/T (Nome)>>")
    fputs(pun,"<< /V ("+iif(empty(nvl(cfdatnas," "))," ",alltrim(nvl(cf_sesso," ")))+")/T (Sesso)>>")
    fputs(pun,"<< /V ("+substr(nvl(dtoc(cfdatnas,1),"        "),7,1)+")/T (Dn1)>>")
    fputs(pun,"<< /V ("+substr(nvl(dtoc(cfdatnas,1),"        "),8,1)+")/T (Dn2)>>")
    fputs(pun,"<< /V ("+substr(nvl(dtoc(cfdatnas,1),"        "),5,1)+")/T (Dn3)>>")
    fputs(pun,"<< /V ("+substr(nvl(dtoc(cfdatnas,1),"        "),6,1)+")/T (Dn4)>>")
    fputs(pun,"<< /V ("+substr(nvl(dtoc(cfdatnas,1),"        "),1,1)+")/T (Dn5)>>")
    fputs(pun,"<< /V ("+substr(nvl(dtoc(cfdatnas,1),"        "),2,1)+")/T (Dn6)>>")
    fputs(pun,"<< /V ("+substr(nvl(dtoc(cfdatnas,1),"        "),3,1)+")/T (Dn7)>>")
    fputs(pun,"<< /V ("+substr(nvl(dtoc(cfdatnas,1),"        "),4,1)+")/T (Dn8)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(cflocnas," "))+")/T (Luogon)>>")
    fputs(pun,"<< /V ("+substr(nvl(cfpronas," "),1,1)+")/T (P2a)>>")
    fputs(pun,"<< /V ("+substr(nvl(cfpronas," "),2,1)+")/T (P2b)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(cfindiri," "))+")/T (Indiri)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(cflocali," "))+")/T (Comune)>>")
    fputs(pun,"<< /V ("+substr(cfprovin,1,1)+")/T (P3a)>>")
    fputs(pun,"<< /V ("+substr(cfprovin,2,1)+")/T (P3b)>>")
    * --- Sezione dati altri soggetti
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,1,1)+")/T (cfs1)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,2,1)+")/T (cfs2)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,3,1)+")/T (cfs3)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,4,1)+")/T (cfs4)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,5,1)+")/T (cfs5)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,6,1)+")/T (cfs6)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,7,1)+")/T (cfs7)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,8,1)+")/T (cfs8)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,9,1)+")/T (cfs9)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,10,1)+")/T (cfs10)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,11,1)+")/T (cfs11)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,12,1)+")/T (cfs12)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,13,1)+")/T (cfs13)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,14,1)+")/T (cfs14)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,15,1)+")/T (cfs15)>>")
    fputs(pun,"<< /V ("+substr(this.w_cfseccod,16,1)+")/T (cfs16)>>")
    fputs(pun,"<< /V ("+substr(nvl(this.w_cfcodide," "),1,1)+")/T (CodIden1)>>")
    fputs(pun,"<< /V ("+substr(nvl(this.w_cfcodide," "),2,1)+")/T (CodIden2)>>")
    * --- Sezione Erario
    fputs(pun,"<< /V ("+alltrim(nvl(eftrier1," "))+")/T (Tri1)>>")
    if not empty(nvl(efmeser1,""))
      fputs(pun,"<< /V ("+alltrim(nvl(efmeser1," "))+")/T (Rate1)>>")
    else
      fputs(pun,"<< /V ("+alltrim(nvl(efrater1," "))+")/T (Rate1)>>")
    endif
    fputs(pun,"<< /V ("+iif(val(efanner1)=0," ",alltrim(nvl(efanner1," ")))+")/T (Anno1)>>")
    fputs(pun,"<< /V ("+trans(efimder1,"@Z "+v_pv[36])+")/T (Impd1)>>")
    fputs(pun,"<< /V ("+subs(trans(efimder1,"@Z "),at(",",trans(efimder1,"@Z "))+1,1)+")/T (d11)>>")
    fputs(pun,"<< /V ("+subs(trans(efimder1,"@Z "),at(",",trans(efimder1,"@Z "))+2,1)+")/T (d12)>>")
    fputs(pun,"<< /V ("+trans(efimcer1,"@Z "+v_pv[36])+")/T (Impc1)>>")
    fputs(pun,"<< /V ("+subs(trans(efimcer1,"@Z "),at(",",trans(efimcer1,"@Z "))+1,1)+")/T (d13)>>")
    fputs(pun,"<< /V ("+subs(trans(efimcer1,"@Z "),at(",",trans(efimcer1,"@Z "))+2,1)+")/T (d14)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(eftrier2," "))+")/T (Tri2)>>")
    if not empty(nvl(efmeser2,""))
      fputs(pun,"<< /V ("+alltrim(nvl(efmeser2," "))+")/T (Rate2)>>")
    else
      fputs(pun,"<< /V ("+alltrim(nvl(efrater2," "))+")/T (Rate2)>>")
    endif
    fputs(pun,"<< /V ("+iif(val(efanner2)=0," ",alltrim(nvl(efanner2," ")))+")/T (Anno2)>>")
    fputs(pun,"<< /V ("+trans(efimder2,"@Z "+v_pv[36])+")/T (Impd2)>>")
    fputs(pun,"<< /V ("+subs(trans(efimder2,"@Z "),at(",",trans(efimder2,"@Z "))+1,1)+")/T (d21)>>")
    fputs(pun,"<< /V ("+subs(trans(efimder2,"@Z "),at(",",trans(efimder2,"@Z "))+2,1)+")/T (d22)>>")
    fputs(pun,"<< /V ("+trans(efimcer2,"@Z "+v_pv[36])+")/T (Impc2)>>")
    fputs(pun,"<< /V ("+subs(trans(efimcer2,"@Z "),at(",",trans(efimcer2,"@Z "))+1,1)+")/T (d23)>>")
    fputs(pun,"<< /V ("+subs(trans(efimcer2,"@Z "),at(",",trans(efimcer2,"@Z "))+2,1)+")/T (d24)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(eftrier3," "))+")/T (Tri3)>>")
    if not empty(nvl(efmeser3,""))
      fputs(pun,"<< /V ("+alltrim(nvl(efmeser3," "))+")/T (Rate3)>>")
    else
      fputs(pun,"<< /V ("+alltrim(nvl(efrater3," "))+")/T (Rate3)>>")
    endif
    fputs(pun,"<< /V ("+iif(val(efanner3)=0," ",alltrim(nvl(efanner3," ")))+")/T (Anno3)>>")
    fputs(pun,"<< /V ("+trans(efimder3,"@Z "+v_pv[36])+")/T (Impd3)>>")
    fputs(pun,"<< /V ("+subs(trans(efimder3,"@Z "),at(",",trans(efimder3,"@Z "))+1,1)+")/T (d31)>>")
    fputs(pun,"<< /V ("+subs(trans(efimder3,"@Z "),at(",",trans(efimder3,"@Z "))+2,1)+")/T (d32)>>")
    fputs(pun,"<< /V ("+trans(efimcer3,"@Z "+v_pv[36])+")/T (Impc3)>> ")
    fputs(pun,"<< /V ("+subs(trans(efimcer3,"@Z "),at(",",trans(efimcer3,"@Z "))+1,1)+")/T (d33)>>")
    fputs(pun,"<< /V ("+subs(trans(efimcer3,"@Z "),at(",",trans(efimcer3,"@Z "))+2,1)+")/T (d34)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(eftrier4," "))+")/T (Tri4)>>")
    if not empty(nvl(efmeser4,""))
      fputs(pun,"<< /V ("+alltrim(nvl(efmeser4," "))+")/T (Rate4)>>")
    else
      fputs(pun,"<< /V ("+alltrim(nvl(efrater4," "))+")/T (Rate4)>>")
    endif
    fputs(pun,"<< /V ("+iif(val(efanner4)=0," ",alltrim(nvl(efanner4," ")))+")/T (Anno4)>>")
    fputs(pun,"<< /V ("+trans(efimder4,"@Z "+v_pv[36])+")/T (Impd4)>>")
    fputs(pun,"<< /V ("+subs(trans(efimder4,"@Z "),at(",",trans(efimder4,"@Z "))+1,1)+")/T (d41)>>")
    fputs(pun,"<< /V ("+subs(trans(efimder4,"@Z "),at(",",trans(efimder4,"@Z "))+2,1)+")/T (d42)>>")
    fputs(pun,"<< /V ("+trans(efimcer4,"@Z "+v_pv[36])+")/T (Impc4)>>")
    fputs(pun,"<< /V ("+subs(trans(efimcer4,"@Z "),at(",",trans(efimcer4,"@Z "))+1,1)+")/T (d43)>>")
    fputs(pun,"<< /V ("+subs(trans(efimcer4,"@Z "),at(",",trans(efimcer4,"@Z "))+2,1)+")/T (d44)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(eftrier5," "))+")/T (Tri5)>>")
    if not empty(nvl(efmeser5,""))
      fputs(pun,"<< /V ("+alltrim(nvl(efmeser5," "))+")/T (Rate5)>>")
    else
      fputs(pun,"<< /V ("+alltrim(nvl(efrater5," "))+")/T (Rate5)>>")
    endif
    fputs(pun,"<< /V ("+iif(val(efanner5)=0," ",alltrim(nvl(efanner5," ")))+")/T (Anno5)>>")
    fputs(pun,"<< /V ("+trans(efimder5,"@Z "+v_pv[36])+")/T (Impd5)>>")
    fputs(pun,"<< /V ("+subs(trans(efimder5,"@Z "),at(",",trans(efimder5,"@Z "))+1,1)+")/T (d51)>>")
    fputs(pun,"<< /V ("+subs(trans(efimder5,"@Z "),at(",",trans(efimder5,"@Z "))+2,1)+")/T (d52)>>")
    fputs(pun,"<< /V ("+trans(efimcer5,"@Z "+v_pv[36])+")/T (Impc5)>>")
    fputs(pun,"<< /V ("+subs(trans(efimcer5,"@Z "),at(",",trans(efimcer5,"@Z "))+1,1)+")/T (d53)>>")
    fputs(pun,"<< /V ("+subs(trans(efimcer5,"@Z "),at(",",trans(efimcer5,"@Z "))+2,1)+")/T (d54)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(eftrier6," "))+")/T (Tri6)>>")
    if not empty(nvl(efmeser6,""))
      fputs(pun,"<< /V ("+alltrim(nvl(efmeser6," "))+")/T (Rate6)>>")
    else
      fputs(pun,"<< /V ("+alltrim(nvl(efrater6," "))+")/T (Rate6)>>")
    endif
    fputs(pun,"<< /V ("+iif(val(efanner6)=0," ",alltrim(nvl(efanner6," ")))+")/T (Anno6)>>")
    fputs(pun,"<< /V ("+trans(efimder6,"@Z "+v_pv[36])+")/T (Impd6)>>")
    fputs(pun,"<< /V ("+subs(trans(efimder6,"@Z "),at(",",trans(efimder6,"@Z "))+1,1)+")/T (d61)>>")
    fputs(pun,"<< /V ("+subs(trans(efimder6,"@Z "),at(",",trans(efimder6,"@Z "))+2,1)+")/T (d62)>>")
    fputs(pun,"<< /V ("+trans(efimcer6,"@Z "+v_pv[36])+")/T (Impc6)>>")
    fputs(pun,"<< /V ("+subs(trans(efimcer6,"@Z "),at(",",trans(efimcer6,"@Z "))+1,1)+")/T (d63)>>")
    fputs(pun,"<< /V ("+subs(trans(efimcer6,"@Z "),at(",",trans(efimcer6,"@Z "))+2,1)+")/T (d64)>>")
    * --- Modifica per il modello F24ICI 2003
    *     -
    *     Non sono pi� presenti le righe numero 7 - 8 - 9 della sezion erario
    if this.w_VFDTPRES < cp_CharToDate("01-01-2003")
      fputs(pun,"<< /V ("+alltrim(nvl(eftrier7," "))+")/T (Tri7)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(efrater7," "))+")/T (Rate7)>>")
      fputs(pun,"<< /V ("+iif(val(efanner7)=0," ",alltrim(nvl(efanner7," ")))+")/T (Anno7)>>")
      fputs(pun,"<< /V ("+trans(efimder7,"@Z "+v_pv[36])+")/T (Impd7)>>")
      fputs(pun,"<< /V ("+subs(trans(efimder7,"@Z "),at(",",trans(efimder7,"@Z "))+1,1)+")/T (d71)>>")
      fputs(pun,"<< /V ("+subs(trans(efimder7,"@Z "),at(",",trans(efimder7,"@Z "))+2,1)+")/T (d72)>>")
      fputs(pun,"<< /V ("+trans(efimcer7,"@Z "+v_pv[36])+")/T (Impc7)>>")
      fputs(pun,"<< /V ("+subs(trans(efimcer7,"@Z "),at(",",trans(efimcer7,"@Z "))+1,1)+")/T (d73)>>")
      fputs(pun,"<< /V ("+subs(trans(efimcer7,"@Z "),at(",",trans(efimcer7,"@Z "))+2,1)+")/T (d74)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(eftrier8," "))+")/T (Tri8)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(efrater8," "))+")/T (Rate8)>>")
      fputs(pun,"<< /V ("+iif(val(efanner8)=0," ",alltrim(nvl(efanner8," ")))+")/T (Anno8)>>")
      fputs(pun,"<< /V ("+trans(efimder8,"@Z "+v_pv[36])+")/T (Impd8)>>")
      fputs(pun,"<< /V ("+subs(trans(efimder8,"@Z "),at(",",trans(efimder8,"@Z "))+1,1)+")/T (d81)>>")
      fputs(pun,"<< /V ("+subs(trans(efimder8,"@Z "),at(",",trans(efimder8,"@Z "))+2,1)+")/T (d82)>>")
      fputs(pun,"<< /V ("+trans(efimcer8,"@Z "+v_pv[36])+")/T (Impc8)>>")
      fputs(pun,"<< /V ("+subs(trans(efimcer8,"@Z "),at(",",trans(efimcer8,"@Z "))+1,1)+")/T (d83)>>")
      fputs(pun,"<< /V ("+subs(trans(efimcer8,"@Z "),at(",",trans(efimcer8,"@Z "))+2,1)+")/T (d84)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(eftrier9," "))+")/T (Tri9)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(efrater9," "))+")/T (Rate9)>>")
      fputs(pun,"<< /V ("+iif(val(efanner9)=0," ",alltrim(nvl(efanner9," ")))+")/T (Anno9)>>")
      fputs(pun,"<< /V ("+trans(efimder9,"@Z "+v_pv[36])+")/T (Impd9)>>")
      fputs(pun,"<< /V ("+subs(trans(efimder9,"@Z "),at(",",trans(efimder9,"@Z "))+1,1)+")/T (d91)>>")
      fputs(pun,"<< /V ("+subs(trans(efimder9,"@Z "),at(",",trans(efimder9,"@Z "))+2,1)+")/T (d92)>>")
      fputs(pun,"<< /V ("+trans(efimcer9,"@Z "+v_pv[36])+")/T (Impc9)>>")
      fputs(pun,"<< /V ("+subs(trans(efimcer9,"@Z "),at(",",trans(efimcer9,"@Z "))+1,1)+")/T (d93)>>")
      fputs(pun,"<< /V ("+subs(trans(efimcer9,"@Z "),at(",",trans(efimcer9,"@Z "))+2,1)+")/T (d94)>>")
    endif
    fputs(pun,"<< /V ("+trans(eftotder,"@Z "+v_pv[36])+")/T (Totd1)>>")
    fputs(pun,"<< /V ("+subs(trans(eftotder,"@Z "),at(",",trans(eftotder,"@Z "))+1,1)+")/T (d101)>>")
    fputs(pun,"<< /V ("+subs(trans(eftotder,"@Z "),at(",",trans(eftotder,"@Z "))+2,1)+")/T (d102)>>")
    fputs(pun,"<< /V ("+trans(eftotcer,"@Z "+v_pv[36])+")/T (Totc1)>>")
    fputs(pun,"<< /V ("+subs(trans(eftotcer,"@Z "),at(",",trans(eftotcer,"@Z "))+1,1)+")/T (d103)>>")
    fputs(pun,"<< /V ("+subs(trans(eftotcer,"@Z "),at(",",trans(eftotcer,"@Z "))+2,1)+")/T (d104)>>")
    fputs(pun,"<< /V ("+ iif(efsalder < 0,"-","+") +")/T (Segno1)>>")
    this.w_OKIMP = eftotcer <> 0 
    if efsalder = 0 and this.w_OKIMP
      fputs(pun,"<< /V (0)/T (Saldo1)>>")
      fputs(pun,"<< /V (0)/T (ds1)>>")
      fputs(pun,"<< /V (0)/T (ds2)>>")
    else
      fputs(pun,"<< /V ("+trans(abs(efsalder),"@Z "+v_pv[36])+")/T (Saldo1)>>")
      fputs(pun,"<< /V ("+subs(trans(efsalder,"@Z "),at(",",trans(efsalder,"@Z "))+1,1)+")/T (ds1)>>")
      fputs(pun,"<< /V ("+subs(trans(efsalder,"@Z "),at(",",trans(efsalder,"@Z "))+2,1)+")/T (ds2)>>")
    endif
    if TRFLCODA ="S"
      fputs(pun,"<< /V ("+substr(nvl(mfcoduff,"   "),1,1)+")/T (Cu1)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcoduff,"   "),2,1)+")/T (Cu2)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcoduff,"   "),3,1)+")/T (Cu3)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodatt,"           "),1,1)+")/T (Ca1)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodatt,"           "),2,1)+")/T (Ca2)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodatt,"           "),3,1)+")/T (Ca3)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodatt,"           "),4,1)+")/T (Ca4)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodatt,"           "),5,1)+")/T (Ca5)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodatt,"           "),6,1)+")/T (Ca6)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodatt,"           "),7,1)+")/T (Ca7)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodatt,"           "),8,1)+")/T (Ca8)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodatt,"           "),9,1)+")/T (Ca9)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodatt,"           "),10,1)+")/T (Ca10)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodatt,"           "),11,1)+")/T (Ca11)>>")
    endif
    * --- Sezione INPS
    fputs(pun,"<< /V ("+alltrim(nvl(mfcdsed1," "))+")/T (Sede1)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mfccont1," "))+")/T (Caus1)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mfminps1," "))+")/T (Matr1)>>")
    fputs(pun,"<< /V ("+iif(val(mfdames1)=0," ",alltrim(nvl(mfdames1," ")))+")/T (Dame1)>>")
    fputs(pun,"<< /V ("+iif(val(mfdaann1)=0," ",alltrim(nvl(mfdaann1," ")))+")/T (Daan1)>>")
    fputs(pun,"<< /V ("+iif(val(mf_ames1)=0," ",alltrim(nvl(mf_ames1," ")))+")/T (Ame1)>>")
    fputs(pun,"<< /V ("+iif(val(mfan1)=0," ",alltrim(nvl(mfan1," ")))+")/T (Aan1)>>")
    fputs(pun,"<< /V ("+trans(mfimpsd1,"@Z "+v_pv[36])+")/T (Impd10)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsd1,"@Z "),at(",",trans(mfimpsd1,"@Z "))+1,1)+")/T (n11)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsd1,"@Z "),at(",",trans(mfimpsd1,"@Z "))+2,1)+")/T (n12)>>")
    fputs(pun,"<< /V ("+trans(mfimpsc1,"@Z "+v_pv[36])+")/T (Impc10)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsc1,"@Z "),at(",",trans(mfimpsc1,"@Z "))+1,1)+")/T (n13)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsc1,"@Z "),at(",",trans(mfimpsc1,"@Z "))+2,1)+")/T (n14)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mfcdsed2," "))+")/T (Sede2)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mfccont2," "))+")/T (Caus2)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mfminps2," "))+")/T (Matr2)>>")
    fputs(pun,"<< /V ("+iif(val(mfdames2)=0," ",alltrim(nvl(mfdames2," ")))+")/T (Dame2)>>")
    fputs(pun,"<< /V ("+iif(val(mfdaann2)=0," ",alltrim(nvl(mfdaann2," ")))+")/T (Daan2)>>")
    fputs(pun,"<< /V ("+iif(val(mf_ames2)=0," ",alltrim(nvl(mf_ames2," ")))+")/T (Ame2)>>")
    fputs(pun,"<< /V ("+iif(val(mfan2)=0," ",alltrim(nvl(mfan2," ")))+")/T (Aan2)>>")
    fputs(pun,"<< /V ("+trans(mfimpsd2,"@Z "+v_pv[36])+")/T (Impd11)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsd2,"@Z "),at(",",trans(mfimpsd2,"@Z "))+1,1)+")/T (n21)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsd2,"@Z "),at(",",trans(mfimpsd2,"@Z "))+2,1)+")/T (n22)>>")
    fputs(pun,"<< /V ("+trans(mfimpsc2,"@Z "+v_pv[36])+")/T (Impc11)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsc2,"@Z "),at(",",trans(mfimpsc2,"@Z "))+1,1)+")/T (n23)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsc2,"@Z "),at(",",trans(mfimpsc2,"@Z "))+2,1)+")/T (n24)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mfcdsed3," "))+")/T (Sede3)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mfccont3," "))+")/T (Caus3)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mfminps3," "))+")/T (Matr3)>>")
    fputs(pun,"<< /V ("+iif(val(mfdames3)=0," ",alltrim(nvl(mfdames3," ")))+")/T (Dame3)>>")
    fputs(pun,"<< /V ("+iif(val(mfdaann3)=0," ",alltrim(nvl(mfdaann3," ")))+")/T (Daan3)>>")
    fputs(pun,"<< /V ("+iif(val(mf_ames3)=0," ",alltrim(nvl(mf_ames3," ")))+")/T (Ame3)>>")
    fputs(pun,"<< /V ("+iif(val(mfan3)=0," ",alltrim(nvl(mfan3," ")))+")/T (Aan3)>>")
    fputs(pun,"<< /V ("+trans(mfimpsd3,"@Z "+v_pv[36])+")/T (Impd12)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsd3,"@Z "),at(",",trans(mfimpsd3,"@Z "))+1,1)+")/T (n31)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsd3,"@Z "),at(",",trans(mfimpsd3,"@Z "))+2,1)+")/T (n32)>>")
    fputs(pun,"<< /V ("+trans(mfimpsc3,"@Z "+v_pv[36])+")/T (Impc12)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsc3,"@Z "),at(",",trans(mfimpsc3,"@Z "))+1,1)+")/T (n33)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsc3,"@Z "),at(",",trans(mfimpsc3,"@Z "))+2,1)+")/T (n34)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mfcdsed4," "))+")/T (Sede4)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mfccont4," "))+")/T (Caus4)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mfminps4," "))+")/T (Matr4)>>")
    fputs(pun,"<< /V ("+iif(val(mfdames4)=0," ",alltrim(nvl(mfdames4," ")))+")/T (Dame4)>>")
    fputs(pun,"<< /V ("+iif(val(mfdaann4)=0," ",alltrim(nvl(mfdaann4," ")))+")/T (Daan4)>>")
    fputs(pun,"<< /V ("+iif(val(mf_ames4)=0," ",alltrim(nvl(mf_ames4," ")))+")/T (Ame4)>>")
    fputs(pun,"<< /V ("+iif(val(mfan4)=0," ",alltrim(nvl(mfan4," ")))+")/T (Aan4)>>")
    fputs(pun,"<< /V ("+trans(mfimpsd4,"@Z "+v_pv[36])+")/T (Impd13)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsd4,"@Z "),at(",",trans(mfimpsd4,"@Z "))+1,1)+")/T (n41)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsd4,"@Z "),at(",",trans(mfimpsd4,"@Z "))+2,1)+")/T (n42)>>")
    fputs(pun,"<< /V ("+trans(mfimpsc4,"@Z "+v_pv[36])+")/T (Impc13)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsc4,"@Z "),at(",",trans(mfimpsc4,"@Z "))+1,1)+")/T (n43)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimpsc4,"@Z "),at(",",trans(mfimpsc4,"@Z "))+2,1)+")/T (n44)>>")
    fputs(pun,"<< /V ("+trans(mftotdps,"@Z "+v_pv[36])+")/T (Totd2)>>")
    fputs(pun,"<< /V ("+subs(trans(mftotdps,"@Z "),at(",",trans(mftotdps,"@Z "))+1,1)+")/T (n51)>>")
    fputs(pun,"<< /V ("+subs(trans(mftotdps,"@Z "),at(",",trans(mftotdps,"@Z "))+2,1)+")/T (n52)>>")
    fputs(pun,"<< /V ("+trans(mftotcps,"@Z "+v_pv[36])+")/T (Totc2)>>")
    fputs(pun,"<< /V ("+subs(trans(mftotcps,"@Z "),at(",",trans(mftotcps,"@Z "))+1,1)+")/T (n53)>>")
    fputs(pun,"<< /V ("+subs(trans(mftotcps,"@Z "),at(",",trans(mftotcps,"@Z "))+2,1)+")/T (n54)>>")
    fputs(pun,"<< /V ("+ iif(mfsaldps < 0,"-","+") +")/T (Segno2)>>")
    this.w_OKIMP = mftotcps <> 0 
    if mfsaldps = 0 and this.w_OKIMP
      fputs(pun,"<< /V (0)/T (Saldo2)>>")
      fputs(pun,"<< /V (0)/T (ds3)>>")
      fputs(pun,"<< /V (0)/T (ds4)>>")
    else
      fputs(pun,"<< /V ("+trans(abs(mfsaldps),"@Z "+v_pv[36])+")/T (Saldo2)>>")
      fputs(pun,"<< /V ("+subs(trans(mfsaldps,"@Z "),at(",",trans(mfsaldps,"@Z "))+1,1)+")/T (ds3)>>")
      fputs(pun,"<< /V ("+subs(trans(mfsaldps,"@Z "),at(",",trans(mfsaldps,"@Z "))+2,1)+")/T (ds4)>>")
    endif
    * --- Sezione Regioni
    fputs(pun,"<< /V ("+substr(nvl(mfcodre1,"  "),1,1)+")/T (Cr1a)>>")
    fputs(pun,"<< /V ("+substr(nvl(mfcodre1,"  "),2,1)+")/T (Cr2a)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mftrire1," "))+")/T (Tri10)>>")
    if not empty(nvl(mfmeser1,""))
      fputs(pun,"<< /V ("+alltrim(nvl(mfmeser1," "))+")/T (Rate10)>>")
    else
      fputs(pun,"<< /V ("+alltrim(nvl(mfratre1," "))+")/T (Rate10)>>")
    endif
    fputs(pun,"<< /V ("+iif(val(mfannre1)=0," ",alltrim(nvl(mfannre1," ")))+")/T (Anno10)>>")
    fputs(pun,"<< /V ("+trans(mfimdre1,"@Z "+v_pv[36])+")/T (Impd14)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimdre1,"@Z "),at(",",trans(mfimdre1,"@Z "))+1,1)+")/T (r11)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimdre1,"@Z "),at(",",trans(mfimdre1,"@Z "))+2,1)+")/T (r12)>>")
    fputs(pun,"<< /V ("+trans(mfimcre1,"@Z "+v_pv[36])+")/T (Impc14)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimcre1,"@Z "),at(",",trans(mfimcre1,"@Z "))+1,1)+")/T (r13)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimcre1,"@Z "),at(",",trans(mfimcre1,"@Z "))+2,1)+")/T (r14)>>")
    fputs(pun,"<< /V ("+substr(nvl(mfcodre2,"  "),1,1)+")/T (Cr1b)>>")
    fputs(pun,"<< /V ("+substr(nvl(mfcodre2,"  "),2,1)+")/T (Cr2b)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mftrire2," "))+")/T (Tri11)>>")
    if not empty(nvl(mfmeser2,""))
      fputs(pun,"<< /V ("+alltrim(nvl(mfmeser2," "))+")/T (Rate11)>>")
    else
      fputs(pun,"<< /V ("+alltrim(nvl(mfratre2," "))+")/T (Rate11)>>")
    endif
    fputs(pun,"<< /V ("+iif(val(mfannre2)=0," ",alltrim(nvl(mfannre2," ")))+")/T (Anno11)>>")
    fputs(pun,"<< /V ("+trans(mfimdre2,"@Z "+v_pv[36])+")/T (Impd15)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimdre2,"@Z "),at(",",trans(mfimdre2,"@Z "))+1,1)+")/T (r21)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimdre2,"@Z "),at(",",trans(mfimdre2,"@Z "))+2,1)+")/T (r22)>>")
    fputs(pun,"<< /V ("+trans(mfimcre2,"@Z "+v_pv[36])+")/T (Impc15)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimcre2,"@Z "),at(",",trans(mfimcre2,"@Z "))+1,1)+")/T (r23)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimcre2,"@Z "),at(",",trans(mfimcre2,"@Z "))+2,1)+")/T (r24)>>")
    fputs(pun,"<< /V ("+substr(nvl(mfcodre3,"  "),1,1)+")/T (Cr1c)>>")
    fputs(pun,"<< /V ("+substr(nvl(mfcodre3,"  "),2,1)+")/T (Cr2c)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mftrire3," "))+")/T (Tri12)>>")
    if not empty(nvl(mfmeser3,""))
      fputs(pun,"<< /V ("+alltrim(nvl(mfmeser3," "))+")/T (Rate12)>>")
    else
      fputs(pun,"<< /V ("+alltrim(nvl(mfratre3," "))+")/T (Rate12)>>")
    endif
    fputs(pun,"<< /V ("+iif(val(mfannre3)=0," ",alltrim(nvl(mfannre3," ")))+")/T (Anno12)>>")
    fputs(pun,"<< /V ("+trans(mfimdre3,"@Z "+v_pv[36])+")/T (Impd16)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimdre3,"@Z "),at(",",trans(mfimdre3,"@Z "))+1,1)+")/T (r31)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimdre3,"@Z "),at(",",trans(mfimdre3,"@Z "))+2,1)+")/T (r32)>>")
    fputs(pun,"<< /V ("+trans(mfimcre3,"@Z "+v_pv[36])+")/T (Impc16)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimcre3,"@Z "),at(",",trans(mfimcre3,"@Z "))+1,1)+")/T (r33)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimcre3,"@Z "),at(",",trans(mfimcre3,"@Z "))+2,1)+")/T (r34)>>")
    fputs(pun,"<< /V ("+substr(nvl(mfcodre4,"  "),1,1)+")/T (Cr1d)>>")
    fputs(pun,"<< /V ("+substr(nvl(mfcodre4,"  "),2,1)+")/T (Cr2d)>>")
    fputs(pun,"<< /V ("+alltrim(nvl(mftrire4," "))+")/T (Tri13)>>")
    if not empty(nvl(mfmeser4,""))
      fputs(pun,"<< /V ("+alltrim(nvl(mfmeser4," "))+")/T (Rate13)>>")
    else
      fputs(pun,"<< /V ("+alltrim(nvl(mfratre4," "))+")/T (Rate13)>>")
    endif
    fputs(pun,"<< /V ("+iif(val(mfannre4)=0," ",alltrim(nvl(mfannre4," ")))+")/T (Anno13)>>")
    fputs(pun,"<< /V ("+trans(mfimdre4,"@Z "+v_pv[36])+")/T (Impd17)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimdre4,"@Z "),at(",",trans(mfimdre4,"@Z "))+1,1)+")/T (r41)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimdre4,"@Z "),at(",",trans(mfimdre4,"@Z "))+2,1)+")/T (r42)>>")
    fputs(pun,"<< /V ("+trans(mfimcre4,"@Z "+v_pv[36])+")/T (Impc17)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimcre4,"@Z "),at(",",trans(mfimcre4,"@Z "))+1,1)+")/T (r43)>>")
    fputs(pun,"<< /V ("+subs(trans(mfimcre4,"@Z "),at(",",trans(mfimcre4,"@Z "))+2,1)+")/T (r44)>>")
    fputs(pun,"<< /V ("+trans(mftotdre,"@Z "+v_pv[36])+")/T (Totd3)>>")
    fputs(pun,"<< /V ("+subs(trans(mftotdre,"@Z "),at(",",trans(mftotdre,"@Z "))+1,1)+")/T (r51)>>")
    fputs(pun,"<< /V ("+subs(trans(mftotdre,"@Z "),at(",",trans(mftotdre,"@Z "))+2,1)+")/T (r52)>>")
    fputs(pun,"<< /V ("+trans(mftotcre,"@Z "+v_pv[36])+")/T (Totc3)>>")
    fputs(pun,"<< /V ("+subs(trans(mftotcre,"@Z "),at(",",trans(mftotcre,"@Z "))+1,1)+")/T (r53)>>")
    fputs(pun,"<< /V ("+subs(trans(mftotcre,"@Z "),at(",",trans(mftotcre,"@Z "))+2,1)+")/T (r54)>>")
    fputs(pun,"<< /V ("+ iif(mfsaldre < 0,"-","+") +")/T (Segno3)>>")
    this.w_OKIMP = mftotcre <> 0 
    if mfsaldre = 0 and this.w_OKIMP
      fputs(pun,"<< /V (0)/T (Saldo3)>>")
      fputs(pun,"<< /V (0)/T (ds5)>>")
      fputs(pun,"<< /V (0)/T (ds6)>>")
    else
      fputs(pun,"<< /V ("+trans(abs(mfsaldre),"@Z "+v_pv[36])+")/T (Saldo3)>>")
      fputs(pun,"<< /V ("+subs(trans(mfsaldre,"@Z "),at(",",trans(mfsaldre,"@Z "))+1,1)+")/T (ds5)>>")
      fputs(pun,"<< /V ("+subs(trans(mfsaldre,"@Z "),at(",",trans(mfsaldre,"@Z "))+2,1)+")/T (ds6)>>")
    endif
    * --- Sezione Enti Locali
    if this.w_VFDTPRES< cp_CharToDate("01-01-2003")
      fputs(pun,"<< /V ("+substr(nvl(mfcodel1,"  "),1,1)+")/T (Cl1a)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodel1,"  "),2,1)+")/T (Cl2a)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mftriel1," "))+")/T (Tri14)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfratel1," "))+")/T (Rate14)>>")
      fputs(pun,"<< /V ("+iif(val(mfannel1)=0," ",alltrim(nvl(mfannel1," ")))+")/T (Anno14)>>")
      fputs(pun,"<< /V ("+trans(mfimdel1,"@Z "+v_pv[36])+")/T (Impd18)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdel1,"@Z "),at(",",trans(mfimdel1,"@Z "))+1,1)+")/T (e11)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdel1,"@Z "),at(",",trans(mfimdel1,"@Z "))+2,1)+")/T (e12)>>")
      fputs(pun,"<< /V ("+trans(mfimcel1,"@Z "+v_pv[36])+")/T (Impc18)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcel1,"@Z "),at(",",trans(mfimcel1,"@Z "))+1,1)+")/T (e13)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcel1,"@Z "),at(",",trans(mfimcel1,"@Z "))+2,1)+")/T (e14)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodel2,"  "),1,1)+")/T (Cl1b)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodel2,"  "),2,1)+")/T (Cl2b)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mftriel2," "))+")/T (Tri15)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfratel2," "))+")/T (Rate15)>>")
      fputs(pun,"<< /V ("+iif(val(mfannel2)=0," ",alltrim(nvl(mfannel2," ")))+")/T (Anno15)>>")
      fputs(pun,"<< /V ("+trans(mfimdel2,"@Z "+v_pv[36])+")/T (Impd19)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdel2,"@Z "),at(",",trans(mfimdel2,"@Z "))+1,1)+")/T (e21)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdel2,"@Z "),at(",",trans(mfimdel2,"@Z "))+2,1)+")/T (e22)>>")
      fputs(pun,"<< /V ("+trans(mfimcel2,"@Z "+v_pv[36])+")/T (Impc19)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcel2,"@Z "),at(",",trans(mfimcel2,"@Z "))+1,1)+")/T (e23)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcel2,"@Z "),at(",",trans(mfimcel2,"@Z "))+2,1)+")/T (e24)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodel3,"  "),1,1)+")/T (Cl1c)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcodel3,"  "),2,1)+")/T (Cl2c)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mftriel3," "))+")/T (Tri16)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfratel3," "))+")/T (Rate16)>>")
      fputs(pun,"<< /V ("+iif(val(mfannel3)=0," ",alltrim(nvl(mfannel3," ")))+")/T (Anno16)>>")
      fputs(pun,"<< /V ("+trans(mfimdel3,"@Z "+v_pv[36])+")/T (Impd20)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdel3,"@Z "),at(",",trans(mfimdel3,"@Z "))+1,1)+")/T (e31)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdel3,"@Z "),at(",",trans(mfimdel3,"@Z "))+2,1)+")/T (e32)>>")
      fputs(pun,"<< /V ("+trans(mfimcel3,"@Z "+v_pv[36])+")/T (Impc20)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcel3,"@Z "),at(",",trans(mfimcel3,"@Z "))+1,1)+")/T (e33)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcel3,"@Z "),at(",",trans(mfimcel3,"@Z "))+2,1)+")/T (e34)>>")
      fputs(pun,"<< /V ("+trans(mftotdel,"@Z "+v_pv[36])+")/T (Totd4)>>")
      fputs(pun,"<< /V ("+subs(trans(mftotdel,"@Z "),at(",",trans(mftotdel,"@Z "))+1,1)+")/T (e41)>>")
      fputs(pun,"<< /V ("+subs(trans(mftotdel,"@Z "),at(",",trans(mftotdel,"@Z "))+2,1)+")/T (e42)>>")
      fputs(pun,"<< /V ("+trans(mftotcel,"@Z "+v_pv[36])+")/T (Totc4)>>")
      fputs(pun,"<< /V ("+subs(trans(mftotcel,"@Z "),at(",",trans(mftotcel,"@Z "))+1,1)+")/T (e43)>>")
      fputs(pun,"<< /V ("+subs(trans(mftotcel,"@Z "),at(",",trans(mftotcel,"@Z "))+2,1)+")/T (e44)>>")
      fputs(pun,"<< /V ("+ iif(mfsaldel < 0,"-","+") +")/T (Segno4)>>")
      this.w_OKIMP = mftotcel <> 0 
      if mfsaldel = 0 and this.w_OKIMP
        fputs(pun,"<< /V (0)/T (Saldo4)>>")
        fputs(pun,"<< /V (0)/T (ds7)>>")
        fputs(pun,"<< /V (0)/T (ds8)>>")
      else
        fputs(pun,"<< /V ("+trans(abs(mfsaldel),"@Z "+v_pv[36])+")/T (Saldo4)>>")
        fputs(pun,"<< /V ("+subs(trans(mfsaldel,"@Z "),at(",",trans(mfsaldel,"@Z "))+1,1)+")/T (ds7)>>")
        fputs(pun,"<< /V ("+subs(trans(mfsaldel,"@Z "),at(",",trans(mfsaldel,"@Z "))+2,1)+")/T (ds8)>>")
      endif
    else
      Select Curs3 
 Go Top
      * --- Modifica per il modello F24ICI 2003
      *     -
      *     Riga 1
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel1,"  ")),1,1)+")/T (Cl1a)>>")
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel1,"  ")),2,1)+")/T (Cl2a)>>")
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel1,"  ")),3,1)+")/T (Cc1a)>>")
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel1,"  ")),4,1)+")/T (Cc2a)>>")
      fputs(pun,"<< /V ("+iif(ifravve1=1,"X","")+")/T (Rav1)>>")
      fputs(pun,"<< /V ("+iif(ifimmov1=1,"X","")+")/T (Imm1)>>")
      fputs(pun,"<< /V ("+iif(ifaccon1=1,"X","")+")/T (Acc1)>>")
      fputs(pun,"<< /V ("+iif(iftsald1=1,"X","")+")/T (Sal1)>>")
      fputs(pun,"<< /V ("+trans(iif(ifnumfa1=0,"",ifnumfa1),"999")+")/T (NumIm1)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(iftriel1," "))+")/T (Tri14)>>")
      if not empty(nvl(ifmeser1,""))
        fputs(pun,"<< /V ("+alltrim(nvl(ifmeser1," "))+")/T (Rate14)>>")
      else
        fputs(pun,"<< /V ("+alltrim(nvl(ifratel1," "))+")/T (Rate14)>>")
      endif
      fputs(pun,"<< /V ("+iif(val(ifannel1)=0," ",alltrim(nvl(ifannel1," ")))+")/T (Anno14)>>")
      fputs(pun,"<< /V ("+trans(ifimdel1,"@Z "+v_pv[36])+")/T (Impd18)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimdel1,"@Z "),at(",",trans(ifimdel1,"@Z "))+1,1)+")/T (e11)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimdel1,"@Z "),at(",",trans(ifimdel1,"@Z "))+2,1)+")/T (e12)>>")
      fputs(pun,"<< /V ("+trans(ifimcel1,"@Z "+v_pv[36])+")/T (Impc18)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimcel1,"@Z "),at(",",trans(ifimcel1,"@Z "))+1,1)+")/T (e13)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimcel1,"@Z "),at(",",trans(ifimcel1,"@Z "))+2,1)+")/T (e14)>>")
      * --- Riga 2
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel2,"  ")),1,1)+")/T (Cl1b)>>")
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel2,"  ")),2,1)+")/T (Cl2b)>>")
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel2,"  ")),3,1)+")/T (Cc1b)>>")
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel2,"  ")),4,1)+")/T (Cc2b)>>")
      fputs(pun,"<< /V ("+iif(ifravve2=1,"X","")+")/T (Rav2)>>")
      fputs(pun,"<< /V ("+iif(ifimmov2=1,"X","")+")/T (Imm2)>>")
      fputs(pun,"<< /V ("+iif(ifaccon2=1,"X","")+")/T (Acc2)>>")
      fputs(pun,"<< /V ("+iif(iftsald2=1,"X","")+")/T (Sal2)>>")
      fputs(pun,"<< /V ("+trans(iif(ifnumfa2=0," ",ifnumfa2),"999")+")/T (NumIm2)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(iftriel2," "))+")/T (Tri15)>>")
      if not empty(nvl(ifmeser2,""))
        fputs(pun,"<< /V ("+alltrim(nvl(ifmeser2," "))+")/T (Rate15)>>")
      else
        fputs(pun,"<< /V ("+alltrim(nvl(ifratel2," "))+")/T (Rate15)>>")
      endif
      fputs(pun,"<< /V ("+iif(val(ifannel2)=0," ",alltrim(nvl(ifannel2," ")))+")/T (Anno15)>>")
      fputs(pun,"<< /V ("+trans(ifimdel2,"@Z "+v_pv[36])+")/T (Impd19)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimdel2,"@Z "),at(",",trans(ifimdel2,"@Z "))+1,1)+")/T (e21)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimdel2,"@Z "),at(",",trans(ifimdel2,"@Z "))+2,1)+")/T (e22)>>")
      fputs(pun,"<< /V ("+trans(ifimcel2,"@Z "+v_pv[36])+")/T (Impc19)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimcel2,"@Z "),at(",",trans(ifimcel2,"@Z "))+1,1)+")/T (e23)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimcel2,"@Z "),at(",",trans(ifimcel2,"@Z "))+2,1)+")/T (e24)>>")
      * --- Riga 3
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel3,"  ")),1,1)+")/T (Cl1c)>>")
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel3,"  ")),2,1)+")/T (Cl2c)>>")
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel3,"  ")),3,1)+")/T (Cc1c)>>")
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel3,"  ")),4,1)+")/T (Cc2c)>>")
      fputs(pun,"<< /V ("+iif(ifravve3=1,"X","")+")/T (Rav3)>>")
      fputs(pun,"<< /V ("+iif(ifimmov3=1,"X","")+")/T (Imm3)>>")
      fputs(pun,"<< /V ("+iif(ifaccon3=1,"X","")+")/T (Acc3)>>")
      fputs(pun,"<< /V ("+iif(iftsald3=1,"X","")+")/T (Sal3)>>")
      fputs(pun,"<< /V ("+trans(iif(ifnumfa3=0," ",ifnumfa3),"999")+")/T (NumIm3)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(iftriel3," "))+")/T (Tri16)>>")
      if not empty(nvl(ifmeser3,""))
        fputs(pun,"<< /V ("+alltrim(nvl(ifmeser3," "))+")/T (Rate16)>>")
      else
        fputs(pun,"<< /V ("+alltrim(nvl(ifratel3," "))+")/T (Rate16)>>")
      endif
      fputs(pun,"<< /V ("+iif(val(ifannel3)=0," ",alltrim(nvl(ifannel3," ")))+")/T (Anno16)>>")
      fputs(pun,"<< /V ("+trans(ifimdel3,"@Z "+v_pv[36])+")/T (Impd20)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimdel3,"@Z "),at(",",trans(ifimdel3,"@Z "))+1,1)+")/T (e31)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimdel3,"@Z "),at(",",trans(ifimdel3,"@Z "))+2,1)+")/T (e32)>>")
      fputs(pun,"<< /V ("+trans(ifimcel3,"@Z "+v_pv[36])+")/T (Impc20)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimcel3,"@Z "),at(",",trans(ifimcel3,"@Z "))+1,1)+")/T (e33)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimcel3,"@Z "),at(",",trans(ifimcel3,"@Z "))+2,1)+")/T (e34)>>")
      * --- Riga 4
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel4,"  ")),1,1)+")/T (Cl1d)>>")
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel4,"  ")),2,1)+")/T (Cl2d)>>")
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel4,"  ")),3,1)+")/T (Cc1d)>>")
      fputs(pun,"<< /V ("+substr(alltrim(nvl(ifcodel4,"  ")),4,1)+")/T (Cc2d)>>")
      fputs(pun,"<< /V ("+iif(ifravve4=1,"X","")+")/T (Rav4)>>")
      fputs(pun,"<< /V ("+iif(ifimmov4=1,"X","")+")/T (Imm4)>>")
      fputs(pun,"<< /V ("+iif(ifaccon4=1,"X","")+")/T (Acc4)>>")
      fputs(pun,"<< /V ("+iif(iftsald4=1,"X","")+")/T (Sal4)>>")
      fputs(pun,"<< /V ("+trans(iif(ifnumfa4=0," ",ifnumfa4),"999")+")/T (NumIm4)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(iftriel4," "))+")/T (Tri17)>>")
      if not empty(nvl(ifmeser4,""))
        fputs(pun,"<< /V ("+alltrim(nvl(ifmeser4," "))+")/T (Rate17)>>")
      else
        fputs(pun,"<< /V ("+alltrim(nvl(ifratel4," "))+")/T (Rate17)>>")
      endif
      fputs(pun,"<< /V ("+iif(val(ifannel4)=0," ",alltrim(nvl(ifannel4," ")))+")/T (Anno17)>>")
      fputs(pun,"<< /V ("+trans(ifimdel4,"@Z "+v_pv[36])+")/T (Impd27)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimdel4,"@Z "),at(",",trans(ifimdel4,"@Z "))+1,1)+")/T (e51)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimdel4,"@Z "),at(",",trans(ifimdel4,"@Z "))+2,1)+")/T (e52)>>")
      fputs(pun,"<< /V ("+trans(ifimcel4,"@Z "+v_pv[36])+")/T (Impc27)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimcel4,"@Z "),at(",",trans(ifimcel4,"@Z "))+1,1)+")/T (e53)>>")
      fputs(pun,"<< /V ("+subs(trans(ifimcel4,"@Z "),at(",",trans(ifimcel4,"@Z "))+2,1)+")/T (e54)>>")
      fputs(pun,"<< /V ("+trans(iftotdel,"@Z "+v_pv[36])+")/T (Totd4)>>")
      fputs(pun,"<< /V ("+subs(trans(iftotdel,"@Z "),at(",",trans(iftotdel,"@Z "))+1,1)+")/T (e41)>>")
      fputs(pun,"<< /V ("+subs(trans(iftotdel,"@Z "),at(",",trans(iftotdel,"@Z "))+2,1)+")/T (e42)>>")
      fputs(pun,"<< /V ("+trans(iftotcel,"@Z "+v_pv[36])+")/T (Totc4)>>")
      fputs(pun,"<< /V ("+subs(trans(iftotcel,"@Z "),at(",",trans(iftotcel,"@Z "))+1,1)+")/T (e43)>>")
      fputs(pun,"<< /V ("+subs(trans(iftotcel,"@Z "),at(",",trans(iftotcel,"@Z "))+2,1)+")/T (e44)>>")
      fputs(pun,"<< /V ("+ iif(ifsaldel < 0,"-","+") +")/T (Segno4)>>")
      this.w_OKIMP = iftotcel <> 0 
      if ifsaldel = 0 and this.w_OKIMP
        fputs(pun,"<< /V (0)/T (Saldo4)>>")
        fputs(pun,"<< /V (0)/T (ds7)>>")
        fputs(pun,"<< /V (0)/T (ds8)>>")
      else
        fputs(pun,"<< /V ("+trans(abs(ifsaldel),"@Z "+v_pv[36])+")/T (Saldo4)>>")
        fputs(pun,"<< /V ("+subs(trans(ifsaldel,"@Z "),at(",",trans(ifsaldel,"@Z "))+1,1)+")/T (ds7)>>")
        fputs(pun,"<< /V ("+subs(trans(ifsaldel,"@Z "),at(",",trans(ifsaldel,"@Z "))+2,1)+")/T (ds8)>>")
      endif
      * --- Detrazione ICI abitaz. Princ.
      fputs(pun,"<< /V ("+trans(ifdetici,"@Z "+v_pv[36])+")/T (DetIci)>>")
      fputs(pun,"<< /V ("+subs(trans(ifdetici,"@Z "),at(",",trans(ifdetici,"@Z "))+1,1)+")/T (e61)>>")
      fputs(pun,"<< /V ("+subs(trans(ifdetici,"@Z "),at(",",trans(ifdetici,"@Z "))+2,1)+")/T (e62)>>")
      if TRIDOPER="S"
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),1,1))+")/T (ID0P1)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),2,1))+")/T (ID0P2)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),3,1))+")/T (ID0P3)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),4,1))+")/T (ID0P4)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),5,1))+")/T (ID0P5)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),6,1))+")/T (ID0P6)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),7,1))+")/T (ID0P7)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),8,1))+")/T (ID0P8)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),9,1))+")/T (ID0P9)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),10,1))+")/T (ID0P10)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),11,1))+")/T (ID0P11)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),12,1))+")/T (ID0P12)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),13,1))+")/T (ID0P13)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),14,1))+")/T (ID0P14)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),15,1))+")/T (ID0P15)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),16,1))+")/T (ID0P16)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),17,1))+")/T (ID0P17)>>")
        fputs(pun,"<< /V ("+UPPER(substr(nvl(IFIDOPER,"  "),18,1))+")/T (ID0P18)>>")
      endif
      Select Model
    endif
    * --- Sezione INAIL
    * --- La stampa assise non contiene i dati relativi alla sezione INAIL
    if type ("this.oparentobject.w_MFTIPMOD")="C" AND this.oparentobject.w_MFTIPMOD="ASSISE06"
      this.w_TOTASC = 0
      this.w_CICLO = 0
      VQ_EXEC("QUERY\GSCGASMF.VQR", this, "Ass") 
 Select Ass 
 Go Top
      Scan
      fputs(pun,"<< /V ("+alltrim(nvl(AFCODENT," "))+")/T (CE2"+CHR(asc("a")+this.w_CICLO) +")>>")
      fputs(pun,"<< /V ("+subs(trans(AFCODPRO,"@Z "),1,1)+")/T (CP1"+CHR(asc("a")+this.w_CICLO) + ")>>")
      fputs(pun,"<< /V ("+subs(trans(AFCODPRO,"@Z "),2,1)+")/T (CP2"+CHR(asc("a")+this.w_CICLO) + ")>>")
      fputs(pun,"<< /V ("+alltrim(nvl(AFTRIBUT," "))+")/T (Tri"+str (18+this.w_CICLO) +")>>")
      fputs(pun,"<< /V ("+alltrim(nvl(AFCODICE," "))+")/T (CiD"+chr (asc("a")+this.w_CICLO) +")>>")
      fputs(pun,"<< /V ("+left (right(alltrim(trans("0"+AF__MESE,"@Z ")),2),1)+")/T (me1"+CHR(asc("a")+this.w_CICLO) + ")>>")
      fputs(pun,"<< /V ("+right(alltrim(trans("0"+AF__MESE,"@Z ")),1)+")/T (me2"+CHR(asc("a")+this.w_CICLO) + ")>>")
      fputs(pun,"<< /V ("+alltrim(nvl(AF__ANNO," "))+")/T (Anno"+alltrim(str (18+this.w_CICLO)) +")>>")
      fputs(pun,"<< /V ("+trans(AFIMPORT,"@Z "+v_pv[36])+")/T (Impd"+alltrim(str (28+this.w_CICLO)) +")>>")
      fputs(pun,"<< /V ("+subs(trans(AFIMPORT,"@Z "),at(",",trans(AFIMPORT,"@Z "))+1,1)+")/T (a" + alltrim(str (1+this.w_CICLO)) +"1)>>")
      fputs(pun,"<< /V ("+subs(trans(AFIMPORT,"@Z "),at(",",trans(AFIMPORT,"@Z "))+2,1)+")/T (a"+ alltrim(str (1+this.w_CICLO)) +"2)>>")
      this.w_TOTASC = this.w_TOTASC + AFIMPORT
      this.w_CICLO = this.w_CICLO + 1
      ENDSCAN
      fputs(pun,"<< /V ("+trans(this.w_TOTASC,"@Z "+v_pv[36])+")/T (Totd5)>>")
      fputs(pun,"<< /V ("+subs(trans(this.w_TOTASC,"@Z "),at(",",trans(this.w_TOTASC,"@Z "))+1,1)+")/T (a81)>>")
      fputs(pun,"<< /V ("+subs(trans(this.w_TOTASC,"@Z "),at(",",trans(this.w_TOTASC,"@Z "))+2,1)+")/T (a82)>>")
      fputs(pun,"<< /V ("+trans(this.w_TOTASC,"@Z "+v_pv[36])+")/T (Saldo5)>>")
      fputs(pun,"<< /V ("+subs(trans(this.w_TOTASC,"@Z "),at(",",trans(this.w_TOTASC,"@Z "))+1,1)+")/T (ds9)>>")
      fputs(pun,"<< /V ("+subs(trans(this.w_TOTASC,"@Z "),at(",",trans(this.w_TOTASC,"@Z "))+2,1)+")/T (ds10)>>")
      if USED ("Ass")
        SELECT ASS 
 USE
      endif
      Select Model
    else
      fputs(pun,"<< /V ("+alltrim(nvl(mfsinai1," "))+")/T (Sede5)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mf_npos1," "))+")/T (Nass1)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mf_pacc1," "))+")/T (CC1)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mf_nrif1," "))+")/T (Nrif1)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfcausa1," "))+")/T (Cau1)>>")
      fputs(pun,"<< /V ("+trans(mfimdil1,"@Z "+v_pv[36])+")/T (Impd21)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdil1,"@Z "),at(",",trans(mfimdil1,"@Z "))+1,1)+")/T (i11)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdil1,"@Z "),at(",",trans(mfimdil1,"@Z "))+2,1)+")/T (i12)>>")
      fputs(pun,"<< /V ("+trans(mfimcil1,"@Z "+v_pv[36])+")/T (Impc21)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcil1,"@Z "),at(",",trans(mfimcil1,"@Z "))+1,1)+")/T (i13)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcil1,"@Z "),at(",",trans(mfimcil1,"@Z "))+2,1)+")/T (i14)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfsinai2," "))+")/T (Sede6)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mf_npos2," "))+")/T (Nass2)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mf_pacc2," "))+")/T (CC2)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mf_nrif2," "))+")/T (Nrif2)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfcausa2," "))+")/T (Cau2)>>")
      fputs(pun,"<< /V ("+trans(mfimdil2,"@Z "+v_pv[36])+")/T (Impd22)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdil2,"@Z "),at(",",trans(mfimdil2,"@Z "))+1,1)+")/T (i21)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdil2,"@Z "),at(",",trans(mfimdil2,"@Z "))+2,1)+")/T (i22)>>")
      fputs(pun,"<< /V ("+trans(mfimcil2,"@Z "+v_pv[36])+")/T (Impc22)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcil2,"@Z "),at(",",trans(mfimcil2,"@Z "))+1,1)+")/T (i23)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcil2,"@Z "),at(",",trans(mfimcil2,"@Z "))+2,1)+")/T (i24)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfsinai3," "))+")/T (Sede7)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mf_npos3," "))+")/T (Nass3)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mf_pacc3," "))+")/T (CC3)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mf_nrif3," "))+")/T (Nrif3)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfcausa3," "))+")/T (Cau3)>>")
      fputs(pun,"<< /V ("+trans(mfimdil3,"@Z "+v_pv[36])+")/T (Impd23)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdil3,"@Z "),at(",",trans(mfimdil3,"@Z "))+1,1)+")/T (i31)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdil3,"@Z "),at(",",trans(mfimdil3,"@Z "))+2,1)+")/T (i32)>>")
      fputs(pun,"<< /V ("+trans(mfimcil3,"@Z "+v_pv[36])+")/T (Impc23)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcil3,"@Z "),at(",",trans(mfimcil3,"@Z "))+1,1)+")/T (i33)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcil3,"@Z "),at(",",trans(mfimcil3,"@Z "))+2,1)+")/T (i34)>>")
      fputs(pun,"<< /V ("+trans(mftdinai,"@Z "+v_pv[36])+")/T (Totd5)>>")
      fputs(pun,"<< /V ("+subs(trans(mftdinai,"@Z "),at(",",trans(mftdinai,"@Z "))+1,1)+")/T (i41)>>")
      fputs(pun,"<< /V ("+subs(trans(mftdinai,"@Z "),at(",",trans(mftdinai,"@Z "))+2,1)+")/T (i42)>>")
      fputs(pun,"<< /V ("+trans(mftcinai,"@Z "+v_pv[36])+")/T (Totc5)>>")
      fputs(pun,"<< /V ("+subs(trans(mftcinai,"@Z "),at(",",trans(mftcinai,"@Z "))+1,1)+")/T (i43)>>")
      fputs(pun,"<< /V ("+subs(trans(mftcinai,"@Z "),at(",",trans(mftcinai,"@Z "))+2,1)+")/T (i44)>>")
      fputs(pun,"<< /V ("+ iif(mfsalina < 0,"-","+") +")/T (Segno5)>>")
      this.w_OKIMP = mftcinai <> 0
      if mfsalina = 0 and this.w_OKIMP
        fputs(pun,"<< /V (0)/T (Saldo5)>>")
        fputs(pun,"<< /V (0)/T (ds9)>>")
        fputs(pun,"<< /V (0)/T (ds10)>>")
      else
        fputs(pun,"<< /V ("+trans(abs(mfsalina),"@Z "+v_pv[36])+")/T (Saldo5)>>")
        fputs(pun,"<< /V ("+subs(trans(mfsalina,"@Z "),at(",",trans(mfsalina,"@Z "))+1,1)+")/T (ds9)>>")
        fputs(pun,"<< /V ("+subs(trans(mfsalina,"@Z "),at(",",trans(mfsalina,"@Z "))+2,1)+")/T (ds10)>>")
      endif
    endif
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Sezione Altri Enti
    if type ("this.oparentobject.w_MFTIPMOD")="C" AND this.oparentobject.w_MFTIPMOD="ASSISE06"
    else
      fputs(pun,"<< /V ("+substr(nvl(mfcdente,"  "),1,1)+")/T (Cde1)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcdente,"  "),2,1)+")/T (Cde2)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcdente,"  "),3,1)+")/T (Cde3)>>")
      fputs(pun,"<< /V ("+substr(nvl(mfcdente,"  "),4,1)+")/T (Cde4)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfsdent1," "))+")/T (Sede8)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfccoae1," "))+")/T (Caus5)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfcdpos1," "))+")/T (Codpo1)>>")
      fputs(pun,"<< /V ("+iif(val(mfmsine1)=0," ",alltrim(nvl(mfmsine1," ")))+")/T (Dame5)>>")
      fputs(pun,"<< /V ("+iif(val(mfanine1)=0," ",alltrim(nvl(mfanine1," ")))+")/T (Daan5)>>")
      fputs(pun,"<< /V ("+iif(val(mfmsfie1)=0," ",alltrim(nvl(mfmsfie1," ")))+")/T (Ame5)>>")
      fputs(pun,"<< /V ("+iif(val(mfanf1)=0," ",alltrim(nvl(mfanf1," ")))+")/T (Aan5)>>")
      fputs(pun,"<< /V ("+trans(mfimdae1,"@Z "+v_pv[36])+")/T (Impd24)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdae1,"@Z "),at(",",trans(mfimdae1,"@Z "))+1,1)+")/T (a11)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdae1,"@Z "),at(",",trans(mfimdae1,"@Z "))+2,1)+")/T (a12)>>")
      fputs(pun,"<< /V ("+trans(mfimcae1,"@Z "+v_pv[36])+")/T (Impc24)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcae1,"@Z "),at(",",trans(mfimcae1,"@Z "))+1,1)+")/T (a13)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcae1,"@Z "),at(",",trans(mfimcae1,"@Z "))+2,1)+")/T (a14)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfsdent2," "))+")/T (Sede9)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfccoae2," "))+")/T (Caus6)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfcdpos2," "))+")/T (Codpo2)>>")
      fputs(pun,"<< /V ("+iif(val(mfmsine2)=0," ",alltrim(nvl(mfmsine2," ")))+")/T (Dame6)>>")
      fputs(pun,"<< /V ("+iif(val(mfanine2)=0," ",alltrim(nvl(mfanine2," ")))+")/T (Daan6)>>")
      fputs(pun,"<< /V ("+iif(val(mfmsfie2)=0," ",alltrim(nvl(mfmsfie2," ")))+")/T (Ame6)>>")
      fputs(pun,"<< /V ("+iif(val(mfanf2)=0," ",alltrim(nvl(mfanf2," ")))+")/T (Aan6)>>")
      fputs(pun,"<< /V ("+trans(mfimdae2,"@Z "+v_pv[36])+")/T (Impd25)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdae2,"@Z "),at(",",trans(mfimdae2,"@Z "))+1,1)+")/T (a21)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdae2,"@Z "),at(",",trans(mfimdae2,"@Z "))+2,1)+")/T (a22)>>")
      fputs(pun,"<< /V ("+trans(mfimcae2,"@Z "+v_pv[36])+")/T (Impc25)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcae2,"@Z "),at(",",trans(mfimcae2,"@Z "))+1,1)+")/T (a23)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcae2,"@Z "),at(",",trans(mfimcae2,"@Z "))+2,1)+")/T (a24)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfsdent3," "))+")/T (Sede10)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfccoae3," "))+")/T (Caus7)>>")
      fputs(pun,"<< /V ("+alltrim(nvl(mfcdpos3," "))+")/T (Codpo3)>>")
      fputs(pun,"<< /V ("+iif(val(mfmsine3)=0," ",alltrim(nvl(mfmsine3," ")))+")/T (Dame7)>>")
      fputs(pun,"<< /V ("+iif(val(mfanine3)=0," ",alltrim(nvl(mfanine3," ")))+")/T (Daan7)>>")
      fputs(pun,"<< /V ("+iif(val(mfmsfie3)=0," ",alltrim(nvl(mfmsfie3," ")))+")/T (Ame7)>>")
      fputs(pun,"<< /V ("+iif(val(mfanf3)=0," ",alltrim(nvl(mfanf3," ")))+")/T (Aan7)>>")
      fputs(pun,"<< /V ("+trans(mfimdae3,"@Z "+v_pv[36])+")/T (Impd26)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdae3,"@Z "),at(",",trans(mfimdae3,"@Z "))+1,1)+")/T (a31)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimdae3,"@Z "),at(",",trans(mfimdae3,"@Z "))+2,1)+")/T (a32)>>")
      fputs(pun,"<< /V ("+trans(mfimcae3,"@Z "+v_pv[36])+")/T (Impc26)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcae3,"@Z "),at(",",trans(mfimcae3,"@Z "))+1,1)+")/T (a33)>>")
      fputs(pun,"<< /V ("+subs(trans(mfimcae3,"@Z "),at(",",trans(mfimcae3,"@Z "))+2,1)+")/T (a34)>>")
      fputs(pun,"<< /V ("+trans(mftdaent,"@Z "+v_pv[36])+")/T (Totd6)>>")
      fputs(pun,"<< /V ("+subs(trans(mftdaent,"@Z "),at(",",trans(mftdaent,"@Z "))+1,1)+")/T (a41)>>")
      fputs(pun,"<< /V ("+subs(trans(mftdaent,"@Z "),at(",",trans(mftdaent,"@Z "))+2,1)+")/T (a42)>>")
      fputs(pun,"<< /V ("+trans(mftcaent,"@Z "+v_pv[36])+")/T (Totc6)>>")
      fputs(pun,"<< /V ("+subs(trans(mftcaent,"@Z "),at(",",trans(mftcaent,"@Z "))+1,1)+")/T (a43)>>")
      fputs(pun,"<< /V ("+subs(trans(mftcaent,"@Z "),at(",",trans(mftcaent,"@Z "))+2,1)+")/T (a44)>>")
      fputs(pun,"<< /V ("+ iif(mfsalaen < 0,"-","+") +")/T (Segno6)>>")
      this.w_OKIMP = mftcaent <> 0 
      if mfsalaen = 0 and this.w_OKIMP
        fputs(pun,"<< /V (0)/T (Saldo6)>>")
        fputs(pun,"<< /V (0)/T (ds11)>>")
        fputs(pun,"<< /V (0)/T (ds12)>>")
      else
        fputs(pun,"<< /V ("+trans(abs(mfsalaen),"@Z "+v_pv[36])+")/T (Saldo6)>>")
        fputs(pun,"<< /V ("+subs(trans(mfsalaen,"@Z "),at(",",trans(mfsalaen,"@Z "))+1,1)+")/T (ds11)>>")
        fputs(pun,"<< /V ("+subs(trans(mfsalaen,"@Z "),at(",",trans(mfsalaen,"@Z "))+2,1)+")/T (ds12)>>")
      endif
    endif
    * --- Se il Saldo finale � minore di zero devo impostare 0
    if mfsalfin > 0
      fputs(pun,"<< /V ("+trans(mfsalfin,"@Z "+v_pv[36])+")/T (Saldtot)>>")
      fputs(pun,"<< /V ("+subs(trans(mfsalfin,"@Z "),at(",",trans(mfsalfin,"@Z "))+1,1)+")/T (ds13)>>")
      fputs(pun,"<< /V ("+subs(trans(mfsalfin,"@Z "),at(",",trans(mfsalfin,"@Z "))+2,1)+")/T (ds14)>>")
    else
      fputs(pun,"<< /V (0)/T (Saldtot)>>")
      fputs(pun,"<< /V (0)/T (ds13)>>")
      fputs(pun,"<< /V (0)/T (ds14)>>")
    endif
    * --- Estremi Versamento
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",substr(dtoc(vfdtpres,1),7,1))+")/T (Dver1)>>")
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",substr(dtoc(vfdtpres,1),8,1))+")/T (Dver2)>>")
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",substr(dtoc(vfdtpres,1),5,1))+")/T (Dver3)>>")
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",substr(dtoc(vfdtpres,1),6,1))+")/T (Dver4)>>")
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",substr(dtoc(vfdtpres,1),1,1))+")/T (Dver5)>>")
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",substr(dtoc(vfdtpres,1),2,1))+")/T (Dver6)>>")
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",substr(dtoc(vfdtpres,1),3,1))+")/T (Dver7)>>")
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",substr(dtoc(vfdtpres,1),4,1))+")/T (Dver8)>>")
    if g_APPLICATION = "ad hoc ENTERPRISE"
      fputs(pun,"<< /V ("+ IIF ( this.w_MFENTRAT="E" ," ", iif(g_Teso="S",alltrim(nvl(vfcodtes," ")),alltrim(nvl(vfcodban," "))))+")/T (Azienda)>>")
      fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",iif(g_Teso="S",alltrim(nvl(vfcabtes," ")),alltrim(nvl(vfcabban," "))))+")/T (CAB)>>")
    else
      fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",alltrim(nvl(vfcodtes," ")))+")/T (Azienda)>>")
      fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",alltrim(nvl(vfcabtes," ")))+")/T (CAB)>>")
    endif
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",alltrim(nvl(vfnumass," ")))+")/T (Nasse)>>")
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ", iif(empty(vfnumass)," ",iif(vfbancic="B","X"," ")))+")/T (Assb)>>")
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ", iif(empty(vfnumass)," ",iif(vfbancic="C","X"," ")))+")/T (Assc)>>")
    fputs(pun,"<< /V ("+IIF ( this.w_MFENTRAT="E" ," ",alltrim(nvl(vfcodabi," "))+ space(22) +alltrim(nvl(vfcodcab," ")))+ ")/T (abicab)>>]")
    do case
      case this.w_VFDTPRES >= cp_CharToDate("01-01-2002") and this.w_VFDTPRES<=cp_CharToDate("31-12-2002")
        fputs(pun,"/F ("+STRTRAN(SYS(5)+SYS(2003),"\","/")+"/QUERY/MOD02F24.pdf)>>")
      case this.w_VFDTPRES >= cp_CharToDate("01-01-2003")
        do case
          case type ("this.oparentobject.w_MFTIPMOD")="C" AND this.oparentobject.w_MFTIPMOD="ASSISE06"
            fputs(pun,"/F ("+STRTRAN(SYS(5)+SYS(2003),"\","/")+"/QUERY/MODF24accise.pdf)>>")
          case this.w_VFDTPRES < cp_CharToDate("01-01-2007")
            fputs(pun,"/F ("+STRTRAN(SYS(5)+SYS(2003),"\","/")+"/QUERY/MODF24ICI.pdf)>>")
          case this.w_VFDTPRES < cp_CharToDate("01-01-2013")
            * --- Valido dal 29/10/2007
            if G_ADHOCONE
              if g_APPLICATION = "ad hoc ENTERPRISE"
                fputs(pun,"/F ("+STRTRAN(SYS(5)+STRTRAN(SYS(2003),"AHE\",""),"\","/")+"/QUERY/MODF24ICI2.pdf)>>")
              else
                fputs(pun,"/F ("+STRTRAN(SYS(5)+STRTRAN(SYS(2003),"AHR\",""),"\","/")+"/QUERY/MODF24ICI2.pdf)>>")
              endif
            else
              fputs(pun,"/F ("+STRTRAN(SYS(5)+SYS(2003),"\","/")+"/QUERY/MODF24ICI2.pdf)>>")
            endif
          otherwise
            * --- Valido dal 01/01/2013
            if G_ADHOCONE
              if g_APPLICATION = "ad hoc ENTERPRISE"
                fputs(pun,"/F ("+STRTRAN(SYS(5)+STRTRAN(SYS(2003),"AHE\",""),"\","/")+"/QUERY/MODF24ICI3.pdf)>>")
              else
                fputs(pun,"/F ("+STRTRAN(SYS(5)+STRTRAN(SYS(2003),"AHR\",""),"\","/")+"/QUERY/MODF24ICI3.pdf)>>")
              endif
            else
              fputs(pun,"/F ("+STRTRAN(SYS(5)+SYS(2003),"\","/")+"/QUERY/MODF24ICI3.pdf)>>")
            endif
        endcase
      otherwise
        if this.oParentObject.w_MFVALUTA=g_CODLIR
          fputs(pun,"/F ("+STRTRAN(SYS(5)+SYS(2003),"\","/")+"/QUERY/MODEF24.pdf)>>")
        else
          fputs(pun,"/F ("+STRTRAN(SYS(5)+SYS(2003),"\","/")+"/QUERY/MODEF24E.pdf)>>")
        endif
    endcase
    fputs(pun,">>")
    fputs(pun,"endobj")
    fputs(pun,"trailer")
    fputs(pun,"<</Root 1 0 R>> ")
    fputs(pun,"%%EOF")
    fclose(pun)
    if g_APPLICATION = "ADHOC REVOLUTION"
      ris=gsar_bsf(tempadhoc()+"\MODEF24.fdf","OPEN","")
    else
      CP_CHPRN(tempadhoc()+"\MODEF24.fdf","",this.oParentObject)
    endif
    * --- Chiusura Cursori
    if used("Curs1")
      select Curs1
      use
    endif
    if used("Curs2")
      select Curs2
      use
    endif
    if used("Curs3")
      select Curs3
      use
    endif
    if used("Model")
      select Model
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
