* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bi4                                                        *
*              Elimina/varia la distinta                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_65]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-01                                                      *
* Last revis.: 2013-12-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bi4",oParentObject,m.pTipo)
return(i_retval)

define class tgscg_bi4 as StdBatch
  * --- Local variables
  pTipo = 0
  w_MESS = space(250)
  * --- WorkFile variables
  PNT_MAST_idx=0
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione Distinta di Contabilizzazione Indiretta: Controlli Preliminari (Lanciato da GSCG_ACI)
    *     Elimina temporaneo lato server alla chiusura della maschera
    *     pTipo
    *     Se vale 0 lanciato alla Delete Start, se 1 lanciato alla Done
    do case
      case this.pTipo=0
        SELECT (this.oParentObject.w_CalcZoom.cCursor)
        GO TOP
        * --- Se lo zoom contiene partite non permetto la cancellazione della contabilizzazione indiretta
        WAIT CLEAR
        if RECCOUNT() >0
          this.w_MESS = Ah_MsgFormat("Distinta contabilizzata; impossibile eliminare.%0Cancellare la corrispondente registrazione di primanota.")
          * --- Abbandona la Transazione
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pTipo=1
        * --- Alla chiusura della maschera Droppo il temporano...
        * --- Drop temporary table TMP_PART_IND
        i_nIdx=cp_GetTableDefIdx('TMP_PART_IND')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_PART_IND')
        endif
    endcase
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='PAR_TITE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
