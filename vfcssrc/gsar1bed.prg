* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1bed                                                        *
*              EXP/IMP TABELLE IMPORT DBF                                      *
*                                                                              *
*      Author: TAM SOFTWARE                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_414]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-12                                                      *
* Last revis.: 2016-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1bed",oParentObject,m.pOper)
return(i_retval)

define class tgsar1bed as StdBatch
  * --- Local variables
  pOper = space(5)
  Messaggio = space(254)
  w_SCELTA = 0
  w_OK = .f.
  w_nt = 0
  w_nConn = 0
  w_cTable = space(50)
  w_cTableName = space(50)
  w_cWhere = space(50)
  w_READ_RES = space(10)
  w_FLDFILTER = space(10)
  w_NUMFILTER = 0
  w_IMPERROR = .f.
  w_TROVATO = 0
  w_VALOREDBF = 0
  w_STRUFILTER = space(0)
  w_StructWhere = space(10)
  w_IMPORTA = .f.
  w_STRTRS = space(10)
  * --- WorkFile variables
  VASTRUTT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export/Import Dati Tracciati, Destinazioni Valori Predefiniti e Trascodifiche DBF
    * --- Variabili maschera
    * --- Tipo operazione da effettuare (I=Import; E=Export)
    * --- Nomi e path dei files DBF
    * --- Variabili per le connessioni al database
    * --- Numero area di lavoro tabella
    * --- Numero connessione
    * --- Nome fisico tabella
    * --- Nome Logico Tabella
    * --- Filtro nella select in Export
    this.w_nt = 0
    this.w_nConn = 0
    this.w_cTable = ""
    this.w_cTableName = ""
    this.w_FLDFILTER = ""
    this.w_NUMFILTER = 0
    this.w_IMPERROR = .F.
    do case
      case this.pOPER=="SELEZ" Or this.pOPER=="DESEL" Or this.pOPER=="INVSE"
        this.oParentObject.w_SELENT = ICASE(this.pOPER=="SELEZ","EN", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELENT="EN", " ", "EN"))
        this.oParentObject.w_SELCAR = ICASE(this.pOPER=="SELEZ","CS", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELCAR="CS", " ", "CS"))
        this.oParentObject.w_SELSTR = ICASE(this.pOPER=="SELEZ","ST", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELSTR="ST", " ", "ST"))
        this.oParentObject.w_SELTRS = ICASE(this.pOPER=="SELEZ","TS", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELTRS="TS", " ", "TS"))
        this.oParentObject.w_SELELE = ICASE(this.pOPER=="SELEZ","EL", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELELE="EL", " ", "EL"))
        this.oParentObject.w_SELVAP = ICASE(this.pOPER=="SELEZ","VP", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELVAP="VP", " ", "VP"))
        this.oParentObject.w_SELFAS = ICASE(this.pOPER=="SELEZ","FA", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELVAP="FA", " ", "FA"))
        this.oParentObject.w_SELFOR = ICASE(this.pOPER=="SELEZ","FO", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELFOR="FO", " ", "FO"))
        this.oParentObject.w_SELTRR = ICASE(this.pOPER=="SELEZ","TE", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELTRR="TE", " ", "TE"))
        this.oParentObject.w_SELVAR = ICASE(this.pOPER=="SELEZ","VF", this.pOPER=="DESEL", " ", IIF(this.oParentObject.w_SELVAR="VF", " ", "VF"))
      case this.pOPER=="ELABO"
        this.oParentObject.w_MSG = ""
        this.oParentObject.oPGFRM.ActivePage=3
        if this.oParentObject.w_RADSELIE1 = "E"
          addMsgNL("Inizio esportazione tabelle %1" ,This,TTOC(DATETIME()))
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- begin transaction
          cp_BeginTrs()
          addMsgNL("Inizio importazione tabelle %1" ,This,TTOC(DATETIME()))
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_IMPERROR or bTrsErr
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          else
            * --- commit
            cp_EndTrs(.t.)
          endif
        endif
        if this.oParentObject.w_RADSELIE1="I"
          addMsg("Fine importazione tabelle %1" ,This,TTOC(DATETIME()))
          if this.w_IMPERROR or bTrsErr
            ah_ErrorMsg("Procedura di importazione terminata con errori",48,"")
          else
            ah_ErrorMsg("Procedura di importazione terminata con successo",64,"")
          endif
        else
          addMsg("Fine esportazione tabelle %1" ,This,TTOC(DATETIME()))
          ah_ErrorMsg("Procedura di esportazione terminata con successo",64,"")
        endif
    endcase
    * --- Chiusura cursore generico
    if used("Cursdbf")
      use in "Cursdbf"
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export Tabelle
    * --- Scrittura files dbf
    this.w_SCELTA = 1
    this.w_FLDFILTER = ""
    this.w_cWhere = ""
    if this.oParentObject.w_SELENT = "EN"
      Filedbf = alltrim(this.oParentObject.w_DBF1)
      addmsgNL("Export tracciati record...", This)
      this.w_cTableName = "ENT_MAST"
      this.w_cWhere = "ENTIPENT = 'V' "
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Fine esportazione di IMPORTAZ
      Filedbf = alltrim(this.oParentObject.w_DBF11) 
      addmsgNL("Export tracciati record...", This)
      this.w_cTableName = "ENT_DETT"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELCAR = "CS"
      Filedbf = alltrim(this.oParentObject.w_DBF5)
      addmsgNL("Caratteri speciali...", This)
      this.w_cTableName = "CAR_SPEC"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELSTR = "ST"
      Filedbf = alltrim(this.oParentObject.w_DBF2)
      addmsgNL("Export strutture...", This)
      this.w_FLDFILTER = "STCODICE"
      this.w_cTableName = "VASTRUTT"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELVAP = "VP"
      Filedbf = alltrim(this.oParentObject.w_DBF3)
      addmsgNL("Export valori predefiniti...", This)
      this.w_FLDFILTER = "PRCODSTR"
      this.w_cTableName = "VAPREDEF"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELVAR = "VF"
      Filedbf = alltrim(this.oParentObject.w_DBF10)
      addmsgNL("Export variabili nome file...", This)
      this.w_FLDFILTER = "VNCODICE"
      this.w_cTableName = "VAVARNOM"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELFAS = "FA"
      Filedbf = alltrim(this.oParentObject.w_DBF9)
      addmsgNL("Export file associati...", This)
      this.w_FLDFILTER = "PFCODSTR"
      this.w_cTableName = "PAT_FILE"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELFOR = "FO"
      Filedbf = alltrim(this.oParentObject.w_DBF7)
      addmsgNL("Export formati...", This)
      this.w_FLDFILTER = "FOCODSTR"
      this.w_cTableName = "VAFORMAT"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      Filedbf = alltrim(this.oParentObject.w_DBF77)
      addmsgNL("Export dettaglio formati...", This)
      this.w_FLDFILTER = "DFCODSTR"
      this.w_cTableName = "VADETTFO"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELELE = "EL"
      Filedbf = alltrim(this.oParentObject.w_DBF8)
      addmsgNL("Export elementi...", This)
      this.w_FLDFILTER = "ELCODSTR"
      this.w_cTableName = "VAELEMEN"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELTRS = "TS"
      Filedbf = alltrim(this.oParentObject.w_DBF4)
      addmsgNL("Export trascodifiche...", This)
      this.w_cTableName = "TRS_MAST"
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      Filedbf = alltrim(this.oParentObject.w_DBF44)
      addmsgNL("Export trascodifiche...", This)
      this.w_cTableName = "TRS_DETT"
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Fine esportazione di TRASCODI
    endif
    if this.oParentObject.w_SELTRR = "TE"
      Filedbf = alltrim(this.oParentObject.w_DBF6)
      if this.oParentObject.w_SOLSTR="S"
        this.w_cWhere = "TRTIPCON = 'N' "
      endif
      addmsgNL("Export trascodifiche...", This)
      this.w_FLDFILTER = "TRCODSTR"
      this.w_cTableName = "VATRASCO"
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Import Tabelle
    * --- Lettura files DBF
    this.w_cTableName = ""
    this.w_FLDFILTER = ""
    this.w_IMPERROR = .F.
    this.w_STRUFILTER = ""
    if this.oParentObject.w_SELENT = "EN"
      addmsgNL("Import entit�" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF1)
      this.w_cTableName = "ENT_MAST"
      this.w_FLDFILTER = "ENCODICE"
      this.w_STRUFILTER = ""
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if !this.w_IMPERROR
        addmsgNL("Import dettaglio entit�" ,This)
        Filedbf = alltrim(this.oParentObject.w_DBF11)
        this.w_STRUFILTER = ""
        this.w_cTableName = "ENT_DETT"
        this.w_FLDFILTER = "ENCODICE,EN_TABLE"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.oParentObject.w_SELCAR = "CS" and !this.w_IMPERROR
      addmsgNL("Import caratteri speciali" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF5)
      this.w_cTableName = "CAR_SPEC"
      this.w_STRUFILTER = ""
      this.w_FLDFILTER = "CACODICE,CACODORI"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELSTR = "ST" and !this.w_IMPERROR
      addmsgNL("Import strutture" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF2)
      this.w_cTableName = "VASTRUTT"
      this.w_FLDFILTER = "STCODICE"
      this.w_STRUFILTER = "STCODICE"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELVAP = "VP" and !this.w_IMPERROR
      addmsgNL("Import valori predefiniti" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF3)
      this.w_cTableName = "VAPREDEF"
      this.w_STRUFILTER = "PRCODSTR"
      this.w_FLDFILTER = "PRCODSTR,PRCODTAB,PR_CAMPO"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELVAR = "VF" and !this.w_IMPERROR
      addmsgNL("Import Variabili nome file" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF10)
      this.w_cTableName = "VAVARNOM"
      this.w_STRUFILTER = "VNCODICE"
      this.w_FLDFILTER = "VNCODICE,VNCODVAR"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELFAS = "FA" and !this.w_IMPERROR
      addmsgNL("Import file associati" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF9)
      this.w_cTableName = "PAT_FILE"
      this.w_STRUFILTER = "PFCODSTR"
      this.w_FLDFILTER = "PFCODSTR,PFPATTER"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELFOR = "FO" and !this.w_IMPERROR
      addmsgNL("Import formati" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF7)
      this.w_cTableName = "VAFORMAT"
      this.w_STRUFILTER = "FOCODSTR"
      this.w_FLDFILTER = "FOCODSTR,FOCODICE"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if !this.w_IMPERROR
        addmsgNL("Import dettaglio formati" ,This)
        Filedbf = alltrim(this.oParentObject.w_DBF77)
        this.w_cTableName = "VADETTFO"
        this.w_STRUFILTER = "DFCODSTR"
        this.w_FLDFILTER = "DFCODSTR,DFCODFOR,CPROWNUM"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.oParentObject.w_SELELE = "EL" and !this.w_IMPERROR
      addmsgNL("Import elementi" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF8)
      this.w_cTableName = "VAELEMEN"
      this.w_STRUFILTER = "ELCODSTR"
      this.w_FLDFILTER = "ELCODSTR,ELCODICE"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_SELTRS = "TS" and !this.w_IMPERROR
      addmsgNL("Import trascodifiche" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF4)
      this.w_cTableName = "TRS_MAST"
      this.w_STRUFILTER = ""
      this.w_FLDFILTER = "TRCODICE"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if !this.w_IMPERROR
        addmsgNL("Import dettaglio trascodifiche" ,This)
        Filedbf = alltrim(this.oParentObject.w_DBF44)
        this.w_cTableName = "TRS_DETT"
        this.w_STRUFILTER = ""
        this.w_FLDFILTER = "TRCODICE,CPROWNUM"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.oParentObject.w_SELTRR = "TE" and !this.w_IMPERROR
      addmsgNL("Import associazione trascodifiche" ,This)
      Filedbf = alltrim(this.oParentObject.w_DBF6)
      this.w_STRUFILTER = "TRCODSTR"
      this.w_cTableName = "VATRASCO"
      this.w_FLDFILTER = "TRCODSTR,TRCODENT,TRCODELE,TRCODCON,TRTIPCON,TRCODRAG"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica esistenza file da esportare
    this.w_OK = .T.
    this.w_cTableName = ALLTRIM(this.w_cTableName)
    if file( Filedbf )
      this.w_OK = ah_YesNo("E' gi� presente il file:%1. Si desidera sovrascrivere?","",FileDBF)
    endif
    if this.w_OK
      if !EMPTY(NVL(this.w_FLDFILTER, " "))
        this.w_StructWhere = ""
        L_OldArea=SELECT()
        SELECT (this.oParentObject.w_STRUTTURE.cCursor)
        GO TOP
        SCAN FOR XCHK=1
        this.w_StructWhere = this.w_StructWhere+IIF(EMPTY(this.w_StructWhere), "", " OR ")+this.w_FLDFILTER+"='"+STCODICE+"'"
        ENDSCAN
        this.w_cWhere = this.w_cWhere+IIF(EMPTY(this.w_cWhere), " (", " AND (")
        if EMPTY(this.w_StructWhere)
          this.w_cWhere = this.w_cWhere+"1=0"+")"
        else
          this.w_cWhere = this.w_cWhere+ ALLTRIM(this.w_StructWhere)+")"
        endif
        SELECT(L_OldArea)
      endif
      this.w_cWhere = IIF(EMPTY(NVL(this.w_cWhere, " ")), "1=1", this.w_cWhere)
      this.w_READ_RES = READTABLE(this.w_cTableName,"*","","",.F.,this.w_cWhere)
      if EMPTY(this.w_READ_RES)
        ah_ErrorMsg("Errore durante la lettura dei dati della tabella %1", "" , "", this.w_cTableName)
      else
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_cWhere = ""
      this.w_FLDFILTER = ""
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if file(Filedbf)
      use (Filedbf) alias CursDBF in 0
      if USED("CursDBF") and reccount("CursDBF")<>0
        Select "CursDBF" 
        go top
        do while !EOF()
          * --- Strutture ed elementi non vengono importati se l utente li ha deselezionati sul database
          this.w_IMPORTA = .T.
          if NOT EMPTY (this.w_STRUFILTER)
            this.w_TROVATO = 0
            this.w_VALOREDBF = UPPER(ALLTRIM(eval(this.w_STRUFILTER)))
            * --- Controlla se la struttura � selezionata per la scrittura
            SELECT (this.oParentObject.w_STRUTTURE.cCursor) 
 Go Top 
 COUNT FOR UPPER(alltrim(STCODICE)) == this.w_VALOREDBF AND XCHK=0 TO this.w_trovato
            * --- L utente ha deselezionato la struttua e non vuole importarla
            if this.w_TROVATO>0
              this.w_IMPORTA = .F.
            endif
            Select "CursDBF"
          endif
          if this.w_IMPORTA
            * --- Try
            local bErr_03E12AF0
            bErr_03E12AF0=bTrsErr
            this.Try_03E12AF0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              * --- Divido la stringa dei filtri divisa da virgole
              this.w_NUMFILTER = ALINES(ARRSPLIT, this.w_FLDFILTER, 5, ",")
              if this.w_NUMFILTER>0
                * --- Preparo l'array dei filtri
                PUBLIC ARRAY ARRFILTER(this.w_NUMFILTER ,2)
                do while this.w_NUMFILTER>0
                  ARRFILTER(this.w_NUMFILTER,1)= ARRSPLIT(this.w_NUMFILTER)
                  L_MAC = "IIF(VARTYPE(CursDBF."+ALLTRIM(ARRSPLIT(this.w_NUMFILTER))+")='C',ALLTRIM(CursDBF."+ALLTRIM(ARRSPLIT(this.w_numFILTER))+"),CursDBF."+ALLTRIM(ARRSPLIT(this.w_numFILTER))+")"
                  ARRFILTER(this.w_NUMFILTER,2)= &L_MAC
                  this.w_NUMFILTER = this.w_NUMFILTER - 1
                enddo
                if !EMPTY(writetable(this.w_cTableName, @ARRINS, @ARRFILTER, this, this.oParentObject.w_FLVERBOS))
                  this.w_IMPERROR = .T.
                  addmsgNL("Errore scrittura archivio:%0%1" ,This, MESSAGE())
                  * --- transaction error
                  bTrsErr=.t.
                  i_TrsMsg=ah_MsgFormat("Errore aggiornamento tabella %1", this.w_cTableName)
                endif
              else
                this.w_IMPERROR = .T.
                * --- transaction error
                bTrsErr=.t.
                i_TrsMsg=ah_MsgFormat("Filtri aggiornamento tabelle %1 non specificati", this.w_cTableName)
              endif
            endif
            bTrsErr=bTrsErr or bErr_03E12AF0
            * --- End
          endif
          SELECT "CursDBF"
          SKIP
        enddo
      endif
      RELEASE ARRINS
      RELEASE ARRFILTER
      RELEASE ARRSPLIT
      this.w_NUMFILTER = 0
      if USED("CursDBF")
        USE IN "CursDBF"
      endif
    else
      ah_ErrorMsg("File %1 non trovato o vuoto",48,"", alltrim (Filedbf))
    endif
  endproc
  proc Try_03E12AF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    PUBLIC ARRAY ARRINS(1,1)
    if curtoarr("CursDBF",0,@ARRINS)<0
      * --- Raise
      i_Error="CreaArrErr"
      return
    endif
    if !EMPTY(inserttable(this.w_cTableName, @ARRINS, iif(this.oParentObject.w_FLVERBOS,this," "), this.oParentObject.w_FLVERBOS))
      * --- Raise
      i_Error="InsErr"
      return
    endif
    return


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OK = .T.
    this.w_cTableName = ALLTRIM(this.w_cTableName)
    if file( Filedbf )
      this.w_OK = ah_YesNo("E' gi� presente il file:%1. Si desidera sovrascrivere?","",FileDBF)
    endif
    * --- Filtro per esportazione trs_mast trs_dett
    this.w_STRTRS = Sys(2015)
    SELECT * FROM (this.oParentObject.w_STRUTTURE.cCursor) WHERE XCHK=1 INTO CURSOR (this.w_STRTRS)
    if reccount((this.w_STRTRS))=0
      this.w_STRTRS = " "
    endif
    this.w_READ_RES = Sys(2015)
    if this.w_cTableName="TRS_MAST"
      VQ_EXEC("..\VEFA\EXE\QUERY\GSVA_BED.VQR",THIS,(this.w_READ_RES))
    else
      VQ_EXEC("..\VEFA\EXE\QUERY\GSVA2BED.VQR",THIS,(this.w_READ_RES))
    endif
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    USE IN (this.w_STRTRS)
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea file dbf
    select (this.w_READ_RES)
    go top
    L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t.
    if vartype(g_USE_FOX2X)="C" AND g_USE_FOX2X="S"
      * --- Compatibilit� versione fox 2.x
      copy to (Filedbf) TYPE FOX2X
    else
      copy to (Filedbf)
    endif
    on error &L_OldError 
    if l_err
      ah_ErrorMsg("Impossibile creare il file %1 nella cartella indicata", "" , "", Alltrim((Filedbf)))
    endif
    USE IN (this.w_READ_RES)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VASTRUTT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
