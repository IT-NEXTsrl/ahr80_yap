* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_mda                                                        *
*              Distinte alternative                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-24                                                      *
* Last revis.: 2015-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsma_mda")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsma_mda")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsma_mda")
  return

* --- Class definition
define class tgsma_mda as StdPCForm
  Width  = 727
  Height = 269
  Top    = 12
  Left   = 0
  cComment = "Distinte alternative"
  cPrg = "gsma_mda"
  HelpContextID=188056727
  add object cnt as tcgsma_mda
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsma_mda as PCContext
  w_DICODART = space(20)
  w_CPROWORD = 0
  w_DICODDIS = space(20)
  w_DIFLCOMP = space(1)
  w_DIFLPREF = space(1)
  w_DESDIS = space(40)
  w_DITIPDIS = space(4)
  proc Save(i_oFrom)
    this.w_DICODART = i_oFrom.w_DICODART
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_DICODDIS = i_oFrom.w_DICODDIS
    this.w_DIFLCOMP = i_oFrom.w_DIFLCOMP
    this.w_DIFLPREF = i_oFrom.w_DIFLPREF
    this.w_DESDIS = i_oFrom.w_DESDIS
    this.w_DITIPDIS = i_oFrom.w_DITIPDIS
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DICODART = this.w_DICODART
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_DICODDIS = this.w_DICODDIS
    i_oTo.w_DIFLCOMP = this.w_DIFLCOMP
    i_oTo.w_DIFLPREF = this.w_DIFLPREF
    i_oTo.w_DESDIS = this.w_DESDIS
    i_oTo.w_DITIPDIS = this.w_DITIPDIS
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsma_mda as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 727
  Height = 269
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-14"
  HelpContextID=188056727
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ART_DIST_IDX = 0
  DISMBASE_IDX = 0
  TIP_DIBA_IDX = 0
  ART_ICOL_IDX = 0
  cFile = "ART_DIST"
  cKeySelect = "DICODART"
  cKeyWhere  = "DICODART=this.w_DICODART"
  cKeyDetail  = "DICODART=this.w_DICODART and DICODDIS=this.w_DICODDIS"
  cKeyWhereODBC = '"DICODART="+cp_ToStrODBC(this.w_DICODART)';

  cKeyDetailWhereODBC = '"DICODART="+cp_ToStrODBC(this.w_DICODART)';
      +'+" and DICODDIS="+cp_ToStrODBC(this.w_DICODDIS)';

  cKeyWhereODBCqualified = '"ART_DIST.DICODART="+cp_ToStrODBC(this.w_DICODART)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ART_DIST.CPROWORD '
  cPrg = "gsma_mda"
  cComment = "Distinte alternative"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DICODART = space(20)
  w_CPROWORD = 0
  w_DICODDIS = space(20)
  w_DIFLCOMP = space(1)
  w_DIFLPREF = space(1)
  w_DESDIS = space(40)
  w_DITIPDIS = space(4)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_mdaPag1","gsma_mda",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='DISMBASE'
    this.cWorkTables[2]='TIP_DIBA'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='ART_DIST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ART_DIST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ART_DIST_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsma_mda'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ART_DIST where DICODART=KeySet.DICODART
    *                            and DICODDIS=KeySet.DICODDIS
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ART_DIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_DIST_IDX,2],this.bLoadRecFilter,this.ART_DIST_IDX,"gsma_mda")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ART_DIST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ART_DIST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ART_DIST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DICODART',this.w_DICODART  )
      select * from (i_cTable) ART_DIST where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DICODART = NVL(DICODART,space(20))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ART_DIST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESDIS = space(40)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DICODDIS = NVL(DICODDIS,space(20))
          .link_2_2('Load')
          .w_DIFLCOMP = NVL(DIFLCOMP,space(1))
          .w_DIFLPREF = NVL(DIFLPREF,space(1))
          .w_DITIPDIS = NVL(DITIPDIS,space(4))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace DICODDIS with .w_DICODDIS
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_7.enabled = .oPgFrm.Page1.oPag.oBtn_2_7.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_DICODART=space(20)
      .w_CPROWORD=10
      .w_DICODDIS=space(20)
      .w_DIFLCOMP=space(1)
      .w_DIFLPREF=space(1)
      .w_DESDIS=space(40)
      .w_DITIPDIS=space(4)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_DICODDIS))
         .link_2_2('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ART_DIST')
    this.DoRTCalc(4,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_7.enabled = this.oPgFrm.Page1.oPag.oBtn_2_7.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_2_7.enabled = .Page1.oPag.oBtn_2_7.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ART_DIST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ART_DIST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODART,"DICODART",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_DICODDIS C(20);
      ,t_DIFLCOMP N(3);
      ,t_DIFLPREF N(3);
      ,t_DESDIS C(40);
      ,t_DITIPDIS N(3);
      ,DICODDIS C(20);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsma_mdabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDICODDIS_2_2.controlsource=this.cTrsName+'.t_DICODDIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIFLCOMP_2_3.controlsource=this.cTrsName+'.t_DIFLCOMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIFLPREF_2_4.controlsource=this.cTrsName+'.t_DIFLPREF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESDIS_2_5.controlsource=this.cTrsName+'.t_DESDIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPDIS_2_6.controlsource=this.cTrsName+'.t_DITIPDIS'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(65)
    this.AddVLine(221)
    this.AddVLine(518)
    this.AddVLine(647)
    this.AddVLine(675)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ART_DIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_DIST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ART_DIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ART_DIST_IDX,2])
      *
      * insert into ART_DIST
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ART_DIST')
        i_extval=cp_InsertValODBCExtFlds(this,'ART_DIST')
        i_cFldBody=" "+;
                  "(DICODART,CPROWORD,DICODDIS,DIFLCOMP,DIFLPREF"+;
                  ",DITIPDIS,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DICODART)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_DICODDIS)+","+cp_ToStrODBC(this.w_DIFLCOMP)+","+cp_ToStrODBC(this.w_DIFLPREF)+;
             ","+cp_ToStrODBC(this.w_DITIPDIS)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ART_DIST')
        i_extval=cp_InsertValVFPExtFlds(this,'ART_DIST')
        cp_CheckDeletedKey(i_cTable,0,'DICODART',this.w_DICODART,'DICODDIS',this.w_DICODDIS)
        INSERT INTO (i_cTable) (;
                   DICODART;
                  ,CPROWORD;
                  ,DICODDIS;
                  ,DIFLCOMP;
                  ,DIFLPREF;
                  ,DITIPDIS;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DICODART;
                  ,this.w_CPROWORD;
                  ,this.w_DICODDIS;
                  ,this.w_DIFLCOMP;
                  ,this.w_DIFLPREF;
                  ,this.w_DITIPDIS;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ART_DIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ART_DIST_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD)) and !Empty(t_DICODDIS)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ART_DIST')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and DICODDIS="+cp_ToStrODBC(&i_TN.->DICODDIS)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ART_DIST')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and DICODDIS=&i_TN.->DICODDIS;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) and !Empty(t_DICODDIS)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and DICODDIS="+cp_ToStrODBC(&i_TN.->DICODDIS)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and DICODDIS=&i_TN.->DICODDIS;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace DICODDIS with this.w_DICODDIS
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ART_DIST
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ART_DIST')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DIFLCOMP="+cp_ToStrODBC(this.w_DIFLCOMP)+;
                     ",DIFLPREF="+cp_ToStrODBC(this.w_DIFLPREF)+;
                     ",DITIPDIS="+cp_ToStrODBC(this.w_DITIPDIS)+;
                     ",DICODDIS="+cp_ToStrODBC(this.w_DICODDIS)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and DICODDIS="+cp_ToStrODBC(DICODDIS)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ART_DIST')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,DIFLCOMP=this.w_DIFLCOMP;
                     ,DIFLPREF=this.w_DIFLPREF;
                     ,DITIPDIS=this.w_DITIPDIS;
                     ,DICODDIS=this.w_DICODDIS;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and DICODDIS=&i_TN.->DICODDIS;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsma_mda
    GSMA_BDA(This, "SETDEFDIBA")
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ART_DIST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ART_DIST_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) and !Empty(t_DICODDIS)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ART_DIST
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and DICODDIS="+cp_ToStrODBC(&i_TN.->DICODDIS)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and DICODDIS=&i_TN.->DICODDIS;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) and !Empty(t_DICODDIS)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ART_DIST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_DIST_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBtn_2_7.enabled =this.oPgFrm.Page1.oPag.oBtn_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsma_mda
    IF Upper(CEVENT)='W_DIFLPREF CHANGED'
       NC = this.ctrsName
       Select (NC)
       this.markpos()
       SUM t_diflpref TO righsel
       this.repos()
       if righsel>1
         ah_ErrorMsg("E' necessario inserire una sola distinta base preferenziale",,'')
         this.w_diflpref='N'
         replace t_diflpref with 0
       endif   
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DICODDIS
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DISMBASE_IDX,3]
    i_lTable = "DISMBASE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2], .t., this.DISMBASE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODDIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DISMBASE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DBCODICE like "+cp_ToStrODBC(trim(this.w_DICODDIS)+"%");

          i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBTIPDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DBCODICE',trim(this.w_DICODDIS))
          select DBCODICE,DBDESCRI,DBTIPDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODDIS)==trim(_Link_.DBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICODDIS) and !this.bDontReportError
            deferred_cp_zoom('DISMBASE','*','DBCODICE',cp_AbsName(oSource.parent,'oDICODDIS_2_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBTIPDIS";
                     +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',oSource.xKey(1))
            select DBCODICE,DBDESCRI,DBTIPDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODDIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DBCODICE,DBDESCRI,DBTIPDIS";
                   +" from "+i_cTable+" "+i_lTable+" where DBCODICE="+cp_ToStrODBC(this.w_DICODDIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DBCODICE',this.w_DICODDIS)
            select DBCODICE,DBDESCRI,DBTIPDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODDIS = NVL(_Link_.DBCODICE,space(20))
      this.w_DESDIS = NVL(_Link_.DBDESCRI,space(40))
      this.w_DITIPDIS = NVL(_Link_.DBTIPDIS,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_DICODDIS = space(20)
      endif
      this.w_DESDIS = space(40)
      this.w_DITIPDIS = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DISMBASE_IDX,2])+'\'+cp_ToStr(_Link_.DBCODICE,1)
      cp_ShowWarn(i_cKey,this.DISMBASE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODDIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICODDIS_2_2.value==this.w_DICODDIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICODDIS_2_2.value=this.w_DICODDIS
      replace t_DICODDIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICODDIS_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIFLCOMP_2_3.RadioValue()==this.w_DIFLCOMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIFLCOMP_2_3.SetRadio()
      replace t_DIFLCOMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIFLCOMP_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIFLPREF_2_4.RadioValue()==this.w_DIFLPREF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIFLPREF_2_4.SetRadio()
      replace t_DIFLPREF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIFLPREF_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESDIS_2_5.value==this.w_DESDIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESDIS_2_5.value=this.w_DESDIS
      replace t_DESDIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESDIS_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPDIS_2_6.RadioValue()==this.w_DITIPDIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPDIS_2_6.SetRadio()
      replace t_DITIPDIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPDIS_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'ART_DIST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsma_mda
         NC = this.ctrsName
         Select (NC)
         this.markpos()
         COUNT FOR not(Empty(t_CPROWORD)) and !Empty(t_DICODDIS) TO righsel
         this.repos()
         if righsel > 0
           righsel=0
           this.markpos()
           SUM t_diflpref FOR not(Empty(t_CPROWORD)) and !Empty(t_DICODDIS) TO righsel
           this.repos()
           If righsel<>1
             If righsel>1
               ah_ErrorMsg("E' necessario inserire una sola distinta base preferenziale",,'')
               this.w_diflpref='N'
               replace t_diflpref with 0
              else
                ah_ErrorMsg("E' necessario inserire una distinta base preferenziale",,'')
              endif   
              i_bres=.f.
           endif
         endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_CPROWORD)) and !Empty(.w_DICODDIS)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) and !Empty(t_DICODDIS))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_DICODDIS=space(20)
      .w_DIFLCOMP=space(1)
      .w_DIFLPREF=space(1)
      .w_DESDIS=space(40)
      .w_DITIPDIS=space(4)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_DICODDIS))
        .link_2_2('Full')
      endif
    endwith
    this.DoRTCalc(4,7,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_DICODDIS = t_DICODDIS
    this.w_DIFLCOMP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIFLCOMP_2_3.RadioValue(.t.)
    this.w_DIFLPREF = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIFLPREF_2_4.RadioValue(.t.)
    this.w_DESDIS = t_DESDIS
    this.w_DITIPDIS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPDIS_2_6.RadioValue(.t.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DICODDIS with this.w_DICODDIS
    replace t_DIFLCOMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIFLCOMP_2_3.ToRadio()
    replace t_DIFLPREF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIFLPREF_2_4.ToRadio()
    replace t_DESDIS with this.w_DESDIS
    replace t_DITIPDIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDITIPDIS_2_6.ToRadio()
    if i_srv='A'
      replace DICODDIS with this.w_DICODDIS
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsma_mdaPag1 as StdContainer
  Width  = 723
  height = 269
  stdWidth  = 723
  stdheight = 269
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="WKAAPUZSLU",left=7, top=5, width=708,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Riga",Field2="DICODDIS",Label2="Distinta/Kit",Field3="DESDIS",Label3="Descrizione",Field4="DITIPDIS",Label4="Tipologia",Field5="DIFLCOMP",Label5="Composto",Field6="DIFLPREF",Label6="Pref.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235712122

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=24,;
    width=703+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=25,width=702+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='DISMBASE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='DISMBASE'
        oDropInto=this.oBodyCol.oRow.oDICODDIS_2_2
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_7 as StdButton with uid="ISRNWLUDKR",width=48,height=45,;
   left=665, top=219,;
    CpPicture="BMP\verifica.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alla distinta associata";
    , HelpContextID = 4268695;
    , caption='\<Distinta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_7.Click()
      with this.Parent.oContained
        OpenGest('A', 'GSDS_MDB', 'DBCODICE' , .w_DICODDIS)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_7.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICODDIS))
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsma_mdaBodyRow as CPBodyRowCnt
  Width=693
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="CIWAHKOYPU",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Posizione",;
    HelpContextID = 394902,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=-2, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oDICODDIS_2_2 as StdTrsField with uid="FWSMVEDCHC",rtseq=3,rtrep=.t.,;
    cFormVar="w_DICODDIS",value=space(20),isprimarykey=.t.,;
    ToolTipText = "Distinta alternativa",;
    HelpContextID = 63505033,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=56, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="DISMBASE", oKey_1_1="DBCODICE", oKey_1_2="this.w_DICODDIS"

  func oDICODDIS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODDIS_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDICODDIS_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oDICODDIS_2_2.readonly and this.parent.oDICODDIS_2_2.isprimarykey)
    do cp_zoom with 'DISMBASE','*','DBCODICE',cp_AbsName(this.parent,'oDICODDIS_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
   endif
  endproc

  add object oDIFLCOMP_2_3 as StdTrsCheck with uid="WZIATMOSRT",rtrep=.t.,;
    cFormVar="w_DIFLCOMP",  caption="",;
    ToolTipText = "Articolo composto",;
    HelpContextID = 21613946,;
    Left=641, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oDIFLCOMP_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DIFLCOMP,&i_cF..t_DIFLCOMP),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDIFLCOMP_2_3.GetRadio()
    this.Parent.oContained.w_DIFLCOMP = this.RadioValue()
    return .t.
  endfunc

  func oDIFLCOMP_2_3.ToRadio()
    this.Parent.oContained.w_DIFLCOMP=trim(this.Parent.oContained.w_DIFLCOMP)
    return(;
      iif(this.Parent.oContained.w_DIFLCOMP=='S',1,;
      0))
  endfunc

  func oDIFLCOMP_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDIFLPREF_2_4 as StdTrsCheck with uid="KSSFKRPMZP",rtrep=.t.,;
    cFormVar="w_DIFLPREF",  caption="",;
    ToolTipText = "Distinta preferenziale",;
    HelpContextID = 42349180,;
    Left=668, Top=0, Width=20,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oDIFLPREF_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DIFLPREF,&i_cF..t_DIFLPREF),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDIFLPREF_2_4.GetRadio()
    this.Parent.oContained.w_DIFLPREF = this.RadioValue()
    return .t.
  endfunc

  func oDIFLPREF_2_4.ToRadio()
    this.Parent.oContained.w_DIFLPREF=trim(this.Parent.oContained.w_DIFLPREF)
    return(;
      iif(this.Parent.oContained.w_DIFLPREF=='S',1,;
      0))
  endfunc

  func oDIFLPREF_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDESDIS_2_5 as StdTrsField with uid="QXBJHQFJER",rtseq=6,rtrep=.t.,;
    cFormVar="w_DESDIS",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione distinta",;
    HelpContextID = 217121226,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=293, Left=212, Top=0, InputMask=replicate('X',40)

  add object oDITIPDIS_2_6 as stdTrsTableCombo with uid="ODFDLHRUSL",rtrep=.t.,;
    cFormVar="w_DITIPDIS", tablefilter="" , enabled=.f.,;
    HelpContextID = 75764361,;
    Height=22, Width=121, Left=510, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , cDescEmptyElement='Selezionare una chiave';
, bIsInHeader=.f.;
    , cTable='TIP_DIBA',cKey='TDCODICE',cValue='TDDESCRI',cOrderBy='',xDefault=space(4);
  , bGlobalFont=.t.


  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_mda','ART_DIST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DICODART=ART_DIST.DICODART";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
