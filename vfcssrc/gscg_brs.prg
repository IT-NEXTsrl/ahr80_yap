* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_brs                                                        *
*              Ricostruzione saldi contabili                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][147]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-06-25                                                      *
* Last revis.: 2008-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_brs",oParentObject)
return(i_retval)

define class tgscg_brs as StdBatch
  * --- Local variables
  w_DATBLO = ctod("  /  /  ")
  w_DARPER = 0
  w_AVEPER = 0
  w_MESS = space(90)
  w_DARINI = 0
  w_AVEINI = 0
  w_DARFIN = 0
  w_AVEFIN = 0
  * --- WorkFile variables
  PNT_DETT_idx=0
  ESERCIZI_idx=0
  SALDICON_idx=0
  PNT_MAST_idx=0
  AZIENDA_idx=0
  TMPSALDI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricostruzione Saldi Periodo dalle Registrazioni Contabili
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    * --- Controlli Preliminari
    * --- Qui deve Bloccare l'Archivio dei Saldi!!
    * --- Try
    local bErr_05238428
    bErr_05238428=bTrsErr
    this.Try_05238428()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      if empty(this.w_MESS)
        ah_errormsg("Errore in inserimento blocchi primanota.%0%1",48,,i_ErrMsg)
      else
        ah_errormsg(this.w_MESS,48)
      endif
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_05238428
    * --- End
    * --- Try
    local bErr_05238008
    bErr_05238008=bTrsErr
    this.Try_05238008()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_errormsg("Errore in aggiornamento saldi",48)
    endif
    bTrsErr=bTrsErr or bErr_05238008
    * --- End
    * --- Drop temporary table TMPSALDI
    i_nIdx=cp_GetTableDefIdx('TMPSALDI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSALDI')
    endif
    * --- Try
    local bErr_04FFE0E0
    bErr_04FFE0E0=bTrsErr
    this.Try_04FFE0E0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      AH_ERRORMSG("Impossibile rimuovere i blocchi. Rimuoverli dai dati azienda",48)
    endif
    bTrsErr=bTrsErr or bErr_04FFE0E0
    * --- End
  endproc
  proc Try_05238428()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT EMPTY(this.w_DATBLO)
      this.w_MESS = "Prima nota bloccata - verificare semaforo bollati in dati azienda. Operazione annullata"
      * --- Raise
      i_Error=this.w_MESS
      return
    endif
    * --- Blocca Primanota
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(i_findat),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = i_findat;
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_05238008()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorna Saldi del Periodo
    AddMsgNL("FASE 1: elaborazione saldi...",IIF(UPPER(this.oParentObject.class)="TGSSR_BSP",this.oParentObject,this))
    if g_APPLICATION <> "ADHOC REVOLUTION" 
      * --- Create temporary table TMPSALDI
      i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_CDPAOEFUDY[1]
      indexes_CDPAOEFUDY[1]='SLTIPCON,SLCODICE,SLCODESE'
      vq_exec('query\GSCG_BRS.vqr',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_CDPAOEFUDY,.f.)
      this.TMPSALDI_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table TMPSALDI
      i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_FXZSWFDRJM[1]
      indexes_FXZSWFDRJM[1]='SLTIPCON,SLCODICE,SLCODESE'
      vq_exec('query\GSCGRBRS.vqr',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_FXZSWFDRJM,.f.)
      this.TMPSALDI_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    AddMsgNL("FASE 2: fase azzeramento saldi periodo...",IIF(UPPER(this.oParentObject.class)="TGSSR_BSP",this.oParentObject,this))
    * --- Delete from SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex
      declare i_aIndex[1]
      i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPSALDI_idx,2])
      i_cWhere=i_cTable+".SLTIPCON = "+i_cQueryTable+".SLTIPCON";
            +" and "+i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
            +" and "+i_cTable+".SLCODESE = "+i_cQueryTable+".SLCODESE";
    
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where ";
            +""+i_cTable+".SLTIPCON = "+i_cQueryTable+".SLTIPCON";
            +" and "+i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
            +" and "+i_cTable+".SLCODESE = "+i_cQueryTable+".SLCODESE";
            +")")
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +",SLAVEPER ="+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +",SLDARINI ="+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +",SLAVEINI ="+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +",SLDARFIN ="+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +",SLAVEFIN ="+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
          +i_ccchkf ;
      +" where ";
          +"SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
             )
    else
      update (i_cTable) set;
          SLDARPER = 0;
          ,SLAVEPER = 0;
          ,SLDARINI = 0;
          ,SLAVEINI = 0;
          ,SLDARFIN = 0;
          ,SLAVEFIN = 0;
          &i_ccchkf. ;
       where;
          SLCODESE = this.oParentObject.w_CODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    AddMsgNL("FASE 3: inserimento saldi...",IIF(UPPER(this.oParentObject.class)="TGSSR_BSP",this.oParentObject,this))
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\gscg3brs",this.SALDICON_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if g_APPLICATION = "ADHOC REVOLUTION" 
      * --- Aggiorno saldi Iniziali\Finali con relativi saldi Finali\Iniziali Fuori linea
      * --- Write into SALDICON
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="SLTIPCON,SLCODICE,SLCODESE"
        do vq_exec with 'GSCG4BRS',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="SALDICON.SLTIPCON = _t2.SLTIPCON";
                +" and "+"SALDICON.SLCODICE = _t2.SLCODICE";
                +" and "+"SALDICON.SLCODESE = _t2.SLCODESE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLDARINI = SALDICON.SLDARINI+_t2.SLDARINF";
            +",SLAVEINI = SALDICON.SLAVEINI+_t2.SLAVEINF";
            +",SLDARFIN = SALDICON.SLDARFIN+_t2.SLDARFIF";
            +",SLAVEFIN = SALDICON.SLAVEFIN+_t2.SLAVEFIF";
            +i_ccchkf;
            +" from "+i_cTable+" SALDICON, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="SALDICON.SLTIPCON = _t2.SLTIPCON";
                +" and "+"SALDICON.SLCODICE = _t2.SLCODICE";
                +" and "+"SALDICON.SLCODESE = _t2.SLCODESE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICON, "+i_cQueryTable+" _t2 set ";
            +"SALDICON.SLDARINI = SALDICON.SLDARINI+_t2.SLDARINF";
            +",SALDICON.SLAVEINI = SALDICON.SLAVEINI+_t2.SLAVEINF";
            +",SALDICON.SLDARFIN = SALDICON.SLDARFIN+_t2.SLDARFIF";
            +",SALDICON.SLAVEFIN = SALDICON.SLAVEFIN+_t2.SLAVEFIF";
            +Iif(Empty(i_ccchkf),"",",SALDICON.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="SALDICON.SLTIPCON = t2.SLTIPCON";
                +" and "+"SALDICON.SLCODICE = t2.SLCODICE";
                +" and "+"SALDICON.SLCODESE = t2.SLCODESE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICON set (";
            +"SLDARINI,";
            +"SLAVEINI,";
            +"SLDARFIN,";
            +"SLAVEFIN";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"SLDARINI+t2.SLDARINF,";
            +"SLAVEINI+t2.SLAVEINF,";
            +"SLDARFIN+t2.SLDARFIF,";
            +"SLAVEFIN+t2.SLAVEFIF";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="SALDICON.SLTIPCON = _t2.SLTIPCON";
                +" and "+"SALDICON.SLCODICE = _t2.SLCODICE";
                +" and "+"SALDICON.SLCODESE = _t2.SLCODESE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICON set ";
            +"SLDARINI = SALDICON.SLDARINI+_t2.SLDARINF";
            +",SLAVEINI = SALDICON.SLAVEINI+_t2.SLAVEINF";
            +",SLDARFIN = SALDICON.SLDARFIN+_t2.SLDARFIF";
            +",SLAVEFIN = SALDICON.SLAVEFIN+_t2.SLAVEFIF";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".SLTIPCON = "+i_cQueryTable+".SLTIPCON";
                +" and "+i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
                +" and "+i_cTable+".SLCODESE = "+i_cQueryTable+".SLCODESE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLDARINI = (select "+i_cTable+".SLDARINI+SLDARINF from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SLAVEINI = (select "+i_cTable+".SLAVEINI+SLAVEINF from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SLDARFIN = (select "+i_cTable+".SLDARFIN+SLDARFIF from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SLAVEFIN = (select "+i_cTable+".SLAVEFIN+SLAVEFIF from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Errore aggiunta giacenze fuori linea'
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    if UPPER(this.oParentObject.class)<>"TGSSR_BSP" AND UPPER(this.oParentObject.class)<>"TGSCV_BEM" 
      ah_errormsg("Ricostruzione saldi terminata",48)
    endif
    return
  proc Try_04FFE0E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Sblocca Primanota
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='PNT_DETT'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='SALDICON'
    this.cWorkTables[4]='PNT_MAST'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='*TMPSALDI'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
