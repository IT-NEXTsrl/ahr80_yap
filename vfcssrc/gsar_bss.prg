* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bss                                                        *
*              Attivazione codifica automatica archivi                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-14                                                      *
* Last revis.: 2009-06-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bss",oParentObject)
return(i_retval)

define class tgsar_bss as StdBatch
  * --- Local variables
  w_PADRE = space(20)
  w_AUTOCODE = space(15)
  w_TIPGES = space(2)
  w_CODAZI = space(5)
  w_ACTIVE = space(1)
  w_OBJCTRL = .NULL.
  w_ARCHIVIO = space(2)
  w_OBJCTRL = .NULL.
  * --- WorkFile variables
  NUMAUT_M_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Attivazione codifica automatica archivi
    * --- Nel caso di codifica automatica+sincronizzazione devo evitare l' mcalc 
    *     sul padre per evitare l'assegnamento alla chiave primaria 'AUTO'
    this.bUpdateParentObject=.f.
    if this.oParentObject.w_AUTO="S"
      * --- Calcolo codice automatico in caricamento
      this.w_PADRE = UPPER(this.oParentObject.class)
      do case
        case this.w_PADRE="TGSAR_API"
          this.w_ARCHIVIO = "CG"
          STAT="this.oParentObject.w_ANCODICE"
        case this.w_PADRE="TGSAR_ACL"
          this.w_ARCHIVIO = "CL"
          STAT="this.oParentObject.w_ANCODICE"
        case this.w_PADRE="TGSAR_AFR"
          this.w_ARCHIVIO = "FO"
          STAT="this.oParentObject.w_ANCODICE"
        case this.w_PADRE="TGSAR_AMC"
          this.w_ARCHIVIO = "MG"
          STAT="this.oParentObject.w_MCCODICE"
        case this.w_PADRE="TGSCA_AVC"
          this.w_ARCHIVIO = "CA"
          STAT="this.oParentObject.w_VCCODICE"
        case this.w_PADRE="TGSCA_AMV"
          this.w_ARCHIVIO = "MA"
          STAT="this.oParentObject.w_MCCODICE"
        case this.w_PADRE="TGSAR_ACM"
          this.w_ARCHIVIO = "CM"
          STAT="this.oParentObject.w_ANCODICE"
        case this.w_PADRE="TGSAR_AMM"
          this.w_ARCHIVIO = "MM"
          STAT="this.oParentObject.w_MCCODICE"
        case this.w_PADRE="TGSCA_AVM"
          this.w_ARCHIVIO = "VM"
          STAT="this.oParentObject.w_VCCODICE"
        case this.w_PADRE="TGSCA_AMM"
          this.w_ARCHIVIO = "AM"
          STAT="this.oParentObject.w_MCCODICE"
        case this.w_PADRE="TGSAR_AGE"
          this.w_ARCHIVIO = "AG"
          STAT="this.oParentObject.w_AGCODAGE"
        case this.w_PADRE="TGSPR_ACN"
          this.w_ARCHIVIO = "PR"
          STAT="this.oParentObject.w_CNCODCAN"
        case this.w_PADRE="TGSMA_AAR"
          this.w_ARCHIVIO = "AR"
          STAT="this.oParentObject.w_ARCODART"
        case this.w_PADRE="TGSMA_AAS"
          this.w_ARCHIVIO = "AS"
          STAT="this.oParentObject.w_ARCODART"
      endcase
      this.w_AUTOCODE = calnumaut(i_CODAZI,this.w_ARCHIVIO,"S")
      if EMPTY(this.w_AUTOCODE)
        this.oParentObject.w_AUTO = "N"
        this.oParentObject.mCalc(.T.)
      else
        ah_errormsg("Assegnato il codice automatico %1", 48, "", this.w_AUTOCODE)
        this.w_OBJCTRL = this.oParentObject.getCtrl(RIGHT(STAT,10))
        this.w_OBJCTRL.value = this.w_AUTOCODE
        STAT=STAT+"=this.w_AUTOCODE"
        &STAT
        * --- Aggiorno le chivi dei figli integrati.....
        do case
          case this.w_PADRE="TGSAR_API"
            this.w_OBJCTRL = this.oParentObject.GSAR_MRB.getCtrl("w_MRCODICE")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSAR_MRB.w_MRCODICE=this.w_AUTOCODE
            this.w_OBJCTRL = this.oParentObject.GSAR_MRB.GSAR_MRC.getCtrl("w_MRCODICE")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSAR_MRB.GSAR_MRC.w_MRCODICE=this.w_AUTOCODE
          case this.w_PADRE="TGSAR_ACL" or this.w_PADRE="TGSAR_AFR"
            this.oParentObject.o_CODI=this.w_AUTOCODE
            this.oParentObject.w_CODI=this.w_AUTOCODE
            this.w_OBJCTRL = this.oParentObject.GSAR_MCF.getCtrl("w_COCODCON")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSAR_MCF.w_COCODCON=this.w_AUTOCODE
            this.w_OBJCTRL = this.oParentObject.GSAR_MMC.getCtrl("w_CCCODCON")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSAR_MMC.w_CCCODCON=this.w_AUTOCODE
            this.w_OBJCTRL = this.oParentObject.GSAR_MDD.getCtrl("w_DDCODICE")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSAR_MDD.w_DDCODICE=this.w_AUTOCODE
          case this.w_PADRE="TGSPR_ACN"
            this.w_OBJCTRL = this.oParentObject.GSPR_MSE.getCtrl("w_SECODPRA")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSPR_MSE.w_SECODPRA=this.w_AUTOCODE
            this.w_OBJCTRL = this.oParentObject.GSPR_MRI.getCtrl("w_RICODPRA")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSPR_MRI.w_RICODPRA=this.w_AUTOCODE
          case this.w_PADRE="TGSMA_AAR"
            this.w_OBJCTRL = this.oParentObject.GSMA_ADP.getCtrl("w_DPCODART")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSPR_MSE.w_DPCODART=this.w_AUTOCODE
            this.w_OBJCTRL = this.oParentObject.GSMA_ANO.getCtrl("w_ARCODART")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSMA_ANO.w_ARCODART=this.w_AUTOCODE
            this.w_OBJCTRL = this.oParentObject.GSMA_MAC.getCtrl("w_CA__GUID")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSMA_MAC.w_CA__GUID=this.w_AUTOCODE
            this.w_OBJCTRL = this.oParentObject.GSMA_MAR.getCtrl("w_MCCODART")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSMA_MAR.w_MCCODART=this.w_AUTOCODE
            this.w_OBJCTRL = this.oParentObject.GSMA_MSC.getCtrl("w_SCCODART")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSMA_MSC.w_SCCODART=this.w_AUTOCODE
            this.w_OBJCTRL = this.oParentObject.GSMA_MSS.getCtrl("w_SCCODART")
            this.w_OBJCTRL.value = this.w_AUTOCODE
            this.oParentObject.GSMA_MSS.w_SCCODART=this.w_AUTOCODE
        endcase
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='NUMAUT_M'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
