* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bed                                                        *
*              Elimina documenti                                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_7]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-11                                                      *
* Last revis.: 2008-09-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SEDOC,w_NUMRIF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bed",oParentObject,m.w_SEDOC,m.w_NUMRIF)
return(i_retval)

define class tgsar_bed as StdBatch
  * --- Local variables
  w_SEDOC = space(10)
  w_NUMRIF = 0
  * --- WorkFile variables
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  CON_PAGA_idx=0
  MOVIMATR_idx=0
  ALT_DETT_idx=0
  VDATRITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Per eliminazione massiva di un documento
    * --- Elimina Documento
    * --- Delete from CON_PAGA
    i_nConn=i_TableProp[this.CON_PAGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_PAGA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CPSERIAL = "+cp_ToStrODBC(this.w_SEDOC);
             )
    else
      delete from (i_cTable) where;
            CPSERIAL = this.w_SEDOC;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from DOC_RATE
    i_nConn=i_TableProp[this.DOC_RATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RSSERIAL = "+cp_ToStrODBC(this.w_SEDOC);
             )
    else
      delete from (i_cTable) where;
            RSSERIAL = this.w_SEDOC;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from VDATRITE
    i_nConn=i_TableProp[this.VDATRITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VDATRITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DRSERIAL = "+cp_ToStrODBC(this.w_SEDOC);
             )
    else
      delete from (i_cTable) where;
            DRSERIAL = this.w_SEDOC;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SEDOC);
             )
    else
      delete from (i_cTable) where;
            MVSERIAL = this.w_SEDOC;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if IsAlt()
      * --- Delete from ALT_DETT
      i_nConn=i_TableProp[this.ALT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ALT_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"DESERIAL = "+cp_ToStrODBC(this.w_SEDOC);
               )
      else
        delete from (i_cTable) where;
              DESERIAL = this.w_SEDOC;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Delete from DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SEDOC);
             )
    else
      delete from (i_cTable) where;
            MVSERIAL = this.w_SEDOC;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Reimposto MT_SALDO a 0 nelle Matricole di carico di quelle che sto cancellando
    * --- Write into MOVIMATR
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOVIMATR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MTSERIAL,MTROWNUM,MTKEYSAL,MTCODMAT"
      do vq_exec with 'gsar_bed',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
              +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
              +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
              +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
          +i_ccchkf;
          +" from "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
              +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
              +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
              +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 set ";
      +"MOVIMATR.MT_SALDO ="+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
          +Iif(Empty(i_ccchkf),"",",MOVIMATR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="MOVIMATR.MTSERIAL = t2.MTSERIAL";
              +" and "+"MOVIMATR.MTROWNUM = t2.MTROWNUM";
              +" and "+"MOVIMATR.MTKEYSAL = t2.MTKEYSAL";
              +" and "+"MOVIMATR.MTCODMAT = t2.MTCODMAT";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set (";
          +"MT_SALDO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
              +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
              +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
              +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set ";
      +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MTSERIAL = "+i_cQueryTable+".MTSERIAL";
              +" and "+i_cTable+".MTROWNUM = "+i_cQueryTable+".MTROWNUM";
              +" and "+i_cTable+".MTKEYSAL = "+i_cQueryTable+".MTKEYSAL";
              +" and "+i_cTable+".MTCODMAT = "+i_cQueryTable+".MTCODMAT";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Delete from MOVIMATR
    i_nConn=i_TableProp[this.MOVIMATR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MTSERIAL = "+cp_ToStrODBC(this.w_SEDOC);
            +" and MTNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
             )
    else
      delete from (i_cTable) where;
            MTSERIAL = this.w_SEDOC;
            and MTNUMRIF = this.w_NUMRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
  endproc


  proc Init(oParentObject,w_SEDOC,w_NUMRIF)
    this.w_SEDOC=w_SEDOC
    this.w_NUMRIF=w_NUMRIF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='DOC_RATE'
    this.cWorkTables[4]='CON_PAGA'
    this.cWorkTables[5]='MOVIMATR'
    this.cWorkTables[6]='ALT_DETT'
    this.cWorkTables[7]='VDATRITE'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SEDOC,w_NUMRIF"
endproc
