* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kca                                                        *
*              Crea azienda                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_76]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2014-05-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kca",oParentObject))

* --- Class definition
define class tgsut_kca as StdForm
  Top    = 33
  Left   = 77

  * --- Standard Properties
  Width  = 544
  Height = 334+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-05-16"
  HelpContextID=169262231
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=80

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsut_kca"
  cComment = "Crea azienda"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_RAGAZI = space(30)
  w_CODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_VALESE = space(3)
  w_CONVE = space(1)
  w_CONTA = space(10)
  o_CONTA = space(10)
  w_MAGA = space(10)
  w_VENDI = space(10)
  o_VENDI = space(10)
  w_SPEDI = space(10)
  w_FIMPO = space(1)
  w_ANAL = space(10)
  w_CESPI = space(10)
  w_IMPO = space(1)
  w_STATIS = space(10)
  w_ANABIL = space(10)
  w_DISB = space(10)
  o_DISB = space(10)
  w_COMM = space(10)
  w_VEFA = space(1)
  o_VEFA = space(1)
  w_SELEZI = space(1)
  w_ATTSER = space(1)
  o_ATTSER = space(1)
  w_FANAL = space(10)
  w_FLABUT = space(1)
  w_locazi = space(5)
  w_newazi = space(5)
  w_TESO = space(10)
  w_pagame = space(10)
  w_mastri = space(10)
  w_conti = space(10)
  w_agenti = space(10)
  w_Listini = space(10)
  w_CATELIST = space(10)
  w_lib_imma = space(10)
  w_TBLSCO = space(10)
  w_framode = space(10)
  w_CLIENTI = space(10)
  w_forni = space(10)
  w_NOMECLA = space(10)
  w_cambi = space(10)
  w_SBIL = space(10)
  w_PARALIST = space(10)
  w_PROVVI = space(10)
  w_CONTRA1 = space(10)
  w_contro = space(10)
  w_cauconta = space(10)
  o_cauconta = space(10)
  w_modconta = space(1)
  w_caumaga = space(10)
  w_caudocu = space(10)
  w_ARTICOLI = space(10)
  w_ARTMAG = space(10)
  w_MATRICOLE = space(10)
  w_MESSAGGI = space(1)
  w_caumovcc = space(10)
  w_FLVEFA = space(10)
  w_FLDISB = space(10)
  w_STLEGAL = space(1)
  o_STLEGAL = space(1)
  w_MANS = space(1)
  w_TIAT = space(1)
  w_TPAT = space(1)
  w_GRCP = space(1)
  w_RAAT = space(1)
  w_DIPE = space(1)
  w_RAPR = space(1)
  w_MOEL = space(1)
  w_locazi = space(5)
  w_newazi = space(5)
  w_tariffa = space(10)
  o_tariffa = space(10)
  w_tipiatt = space(10)
  o_tipiatt = space(10)
  w_raggatt = space(10)
  w_raggpre = space(10)
  w_raggtipi = space(10)
  w_oggepra = space(10)
  w_statpra = space(10)
  w_ubicpra = space(10)
  w_tipipra = space(10)
  w_matepra = space(10)
  w_entipra = space(10)
  w_uffipra = space(10)
  w_classpra = space(10)
  w_LblAzie = .NULL.
  w_LblCopiaDa = .NULL.
  w_LblCopiaIn = .NULL.
  w_LblCopiaDa = .NULL.
  w_LblCopiaIn = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kcaPag1","gsut_kca",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Azienda")
      .Pages(2).addobject("oPag","tgsut_kcaPag2","gsut_kca",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Archivi superiori")
      .Pages(3).addobject("oPag","tgsut_kcaPag3","gsut_kca",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Archivi alterego")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODAZI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsut_kca
    this.Pages(1).Caption = IIF(IsAlt(),AH_MSGFORMAT("Studio"),this.Pages(1).Caption) 
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_LblAzie = this.oPgFrm.Pages(1).oPag.LblAzie
    this.w_LblCopiaDa = this.oPgFrm.Pages(2).oPag.LblCopiaDa
    this.w_LblCopiaIn = this.oPgFrm.Pages(2).oPag.LblCopiaIn
    this.w_LblCopiaDa = this.oPgFrm.Pages(3).oPag.LblCopiaDa
    this.w_LblCopiaIn = this.oPgFrm.Pages(3).oPag.LblCopiaIn
    DoDefault()
    proc Destroy()
      this.w_LblAzie = .NULL.
      this.w_LblCopiaDa = .NULL.
      this.w_LblCopiaIn = .NULL.
      this.w_LblCopiaDa = .NULL.
      this.w_LblCopiaIn = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSUT_BCA with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_RAGAZI=space(30)
      .w_CODESE=space(4)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_VALESE=space(3)
      .w_CONVE=space(1)
      .w_CONTA=space(10)
      .w_MAGA=space(10)
      .w_VENDI=space(10)
      .w_SPEDI=space(10)
      .w_FIMPO=space(1)
      .w_ANAL=space(10)
      .w_CESPI=space(10)
      .w_IMPO=space(1)
      .w_STATIS=space(10)
      .w_ANABIL=space(10)
      .w_DISB=space(10)
      .w_COMM=space(10)
      .w_VEFA=space(1)
      .w_SELEZI=space(1)
      .w_ATTSER=space(1)
      .w_FANAL=space(10)
      .w_FLABUT=space(1)
      .w_locazi=space(5)
      .w_newazi=space(5)
      .w_TESO=space(10)
      .w_pagame=space(10)
      .w_mastri=space(10)
      .w_conti=space(10)
      .w_agenti=space(10)
      .w_Listini=space(10)
      .w_CATELIST=space(10)
      .w_lib_imma=space(10)
      .w_TBLSCO=space(10)
      .w_framode=space(10)
      .w_CLIENTI=space(10)
      .w_forni=space(10)
      .w_NOMECLA=space(10)
      .w_cambi=space(10)
      .w_SBIL=space(10)
      .w_PARALIST=space(10)
      .w_PROVVI=space(10)
      .w_CONTRA1=space(10)
      .w_contro=space(10)
      .w_cauconta=space(10)
      .w_modconta=space(1)
      .w_caumaga=space(10)
      .w_caudocu=space(10)
      .w_ARTICOLI=space(10)
      .w_ARTMAG=space(10)
      .w_MATRICOLE=space(10)
      .w_MESSAGGI=space(1)
      .w_caumovcc=space(10)
      .w_FLVEFA=space(10)
      .w_FLDISB=space(10)
      .w_STLEGAL=space(1)
      .w_MANS=space(1)
      .w_TIAT=space(1)
      .w_TPAT=space(1)
      .w_GRCP=space(1)
      .w_RAAT=space(1)
      .w_DIPE=space(1)
      .w_RAPR=space(1)
      .w_MOEL=space(1)
      .w_locazi=space(5)
      .w_newazi=space(5)
      .w_tariffa=space(10)
      .w_tipiatt=space(10)
      .w_raggatt=space(10)
      .w_raggpre=space(10)
      .w_raggtipi=space(10)
      .w_oggepra=space(10)
      .w_statpra=space(10)
      .w_ubicpra=space(10)
      .w_tipipra=space(10)
      .w_matepra=space(10)
      .w_entipra=space(10)
      .w_uffipra=space(10)
      .w_classpra=space(10)
          .DoRTCalc(1,2,.f.)
        .w_CODESE = ALLTRIM(STR(YEAR(i_datsys)))
        .w_INIESE = cp_CharToDate( '01-01-'+ALLTR(STR(YEAR(i_datsys))), 'dd-mm-yyyy' )
        .w_FINESE = cp_CharToDate( '31-12-'+ALLTR(STR(YEAR(i_datsys))), 'dd-mm-yyyy' )
        .w_VALESE = IIF(.w_VENDI='S' Or .w_STLEGAL='S', g_PERVAL, "")
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_VALESE))
          .link_1_6('Full')
        endif
        .w_CONVE = 'S'
        .w_CONTA = 'S'
        .w_MAGA = 'S'
        .w_VENDI = 'S'
        .w_SPEDI = 'S'
        .w_FIMPO = g_IMPO
        .w_ANAL = ' '
        .w_CESPI = ' '
        .w_IMPO = iif(.w_FIMPO='S','S',' ')
        .w_STATIS = ' '
        .w_ANABIL = ' '
        .w_DISB = ' '
        .w_COMM = ' '
        .w_VEFA = ' '
        .w_SELEZI = 'D'
        .w_ATTSER = ' '
        .w_FANAL = IIF(g_PERCCR='S' OR g_PERCCM='S', 'S', 'N')
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .w_FLABUT = 'T'
        .w_locazi = i_codazi
        .w_newazi = .w_codazi
        .w_TESO = ' '
        .w_pagame = IIF(ISALT(), 'S', ' ')
        .w_mastri = IIF(ISALT(), 'S', ' ')
        .w_conti = IIF(ISALT(), 'S', ' ')
        .w_agenti = ' '
        .w_Listini = IIF(ISALT(), 'S', ' ')
        .w_CATELIST = ' '
        .w_lib_imma = IIF(ISALT(), 'S', ' ')
        .w_TBLSCO = ' '
        .w_framode = IIF(ISALT(), 'S', ' ')
        .w_CLIENTI = ' '
        .w_forni = ' '
        .w_NOMECLA = ' '
        .w_cambi = ' '
        .w_SBIL = ' '
        .w_PARALIST = ' '
        .w_PROVVI = ' '
        .w_CONTRA1 = ' '
        .w_contro = IIF(ISALT(), 'S', ' ')
        .w_cauconta = IIF(ISALT(), 'S', ' ')
        .w_modconta = ' '
        .w_caumaga = ' '
        .w_caudocu = IIF(ISALT(), 'S', ' ')
        .w_ARTICOLI = ' '
        .w_ARTMAG = ' '
        .w_MATRICOLE = ' '
        .w_MESSAGGI = ' '
        .w_caumovcc = ' '
        .w_FLVEFA = IIF((g_VEFA='S' AND .w_VEFA='S') OR g_VEFA='N','S','N')
        .w_FLDISB = IIF((g_DISB='S' AND .w_DISB='S') OR g_DISB='N','S','N')
        .w_STLEGAL = iif(IsAlt(), 'S', ' ')
      .oPgFrm.Page1.oPag.LblAzie.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .w_MANS = ' '
        .w_TIAT = ' '
        .w_TPAT = ' '
        .w_GRCP = ' '
        .w_RAAT = ' '
        .w_DIPE = ' '
        .w_RAPR = ' '
      .oPgFrm.Page2.oPag.LblCopiaDa.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Copia archivi dallo studio:"),AH_MSGFORMAT("Copia archivi dall'azienda:")))
        .w_MOEL = ' '
      .oPgFrm.Page2.oPag.LblCopiaIn.Calculate(IIF(IsAlt(),AH_MSGFORMAT("nello studio:"),AH_MSGFORMAT("nell'azienda:")))
        .w_locazi = i_codazi
        .w_newazi = .w_codazi
      .oPgFrm.Page3.oPag.LblCopiaDa.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Copia archivi dallo studio:"),AH_MSGFORMAT("Copia archivi dall'azienda:")))
      .oPgFrm.Page3.oPag.LblCopiaIn.Calculate(IIF(IsAlt(),AH_MSGFORMAT("nello studio:"),AH_MSGFORMAT("nell'azienda:")))
        .w_tariffa = iif(IsAlt(), 'S', ' ')
        .w_tipiatt = iif(IsAlt(), 'S', ' ')
        .w_raggatt = iif(IsAlt(), 'S', ' ')
        .w_raggpre = iif(IsAlt(), 'S', ' ')
        .w_raggtipi = iif(IsAlt(), 'S', ' ')
        .w_oggepra = iif(IsAlt(), 'S', ' ')
        .w_statpra = iif(IsAlt(), 'S', ' ')
        .w_ubicpra = iif(IsAlt(), 'S', ' ')
        .w_tipipra = iif(IsAlt(), 'S', ' ')
        .w_matepra = iif(IsAlt(), 'S', ' ')
        .w_entipra = iif(IsAlt(), 'S', ' ')
        .w_uffipra = iif(IsAlt(), 'S', ' ')
        .w_classpra = iif(IsAlt(), 'S', ' ')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_kca
    *--- Valorizzo la valuto dell'esercizio, di default le Vedite sono attive
    This.w_VALESE = g_PERVAL
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_VENDI<>.w_VENDI.or. .o_STLEGAL<>.w_STLEGAL
            .w_VALESE = IIF(.w_VENDI='S' Or .w_STLEGAL='S', g_PERVAL, "")
          .link_1_6('Full')
        endif
        .DoRTCalc(7,11,.t.)
            .w_FIMPO = g_IMPO
        .DoRTCalc(13,22,.t.)
            .w_FANAL = IIF(g_PERCCR='S' OR g_PERCCM='S', 'S', 'N')
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .DoRTCalc(24,25,.t.)
        if .o_CODAZI<>.w_CODAZI
            .w_newazi = .w_codazi
        endif
        .DoRTCalc(27,54,.t.)
        if .o_VEFA<>.w_VEFA
            .w_FLVEFA = IIF((g_VEFA='S' AND .w_VEFA='S') OR g_VEFA='N','S','N')
        endif
        if .o_DISB<>.w_DISB
            .w_FLDISB = IIF((g_DISB='S' AND .w_DISB='S') OR g_DISB='N','S','N')
        endif
        .oPgFrm.Page1.oPag.LblAzie.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page2.oPag.LblCopiaDa.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Copia archivi dallo studio:"),AH_MSGFORMAT("Copia archivi dall'azienda:")))
        .oPgFrm.Page2.oPag.LblCopiaIn.Calculate(IIF(IsAlt(),AH_MSGFORMAT("nello studio:"),AH_MSGFORMAT("nell'azienda:")))
        if .o_STLEGAL<>.w_STLEGAL
          .Calculate_DRTGLUYDLP()
        endif
        .DoRTCalc(57,66,.t.)
        if .o_CODAZI<>.w_CODAZI
            .w_newazi = .w_codazi
        endif
        .oPgFrm.Page3.oPag.LblCopiaDa.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Copia archivi dallo studio:"),AH_MSGFORMAT("Copia archivi dall'azienda:")))
        .oPgFrm.Page3.oPag.LblCopiaIn.Calculate(IIF(IsAlt(),AH_MSGFORMAT("nello studio:"),AH_MSGFORMAT("nell'azienda:")))
        if .o_tariffa<>.w_tariffa
          .Calculate_MPHXCJJTMK()
        endif
        if .o_tipiatt<>.w_tipiatt
          .Calculate_AKSTKKRMZY()
        endif
        if .o_cauconta<>.w_cauconta
          .Calculate_PHMAVKVEIY()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(68,80,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.LblAzie.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Studio:"),AH_MSGFORMAT("Azienda:")))
        .oPgFrm.Page2.oPag.LblCopiaDa.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Copia archivi dallo studio:"),AH_MSGFORMAT("Copia archivi dall'azienda:")))
        .oPgFrm.Page2.oPag.LblCopiaIn.Calculate(IIF(IsAlt(),AH_MSGFORMAT("nello studio:"),AH_MSGFORMAT("nell'azienda:")))
        .oPgFrm.Page3.oPag.LblCopiaDa.Calculate(IIF(IsAlt(),AH_MSGFORMAT("Copia archivi dallo studio:"),AH_MSGFORMAT("Copia archivi dall'azienda:")))
        .oPgFrm.Page3.oPag.LblCopiaIn.Calculate(IIF(IsAlt(),AH_MSGFORMAT("nello studio:"),AH_MSGFORMAT("nell'azienda:")))
    endwith
  return

  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .Caption = IIF(IsAlt(),AH_MSGFORMAT("Creazione studio"),.Caption)
    endwith
  endproc
  proc Calculate_DRTGLUYDLP()
    with this
          * --- Disabilita check
          .w_CONTA = IIF(.w_STLEGAL='S','S','N')
          .w_MAGA = IIF(.w_STLEGAL='S','S','N')
          .w_VENDI = IIF(.w_STLEGAL='S','S','N')
          .w_SPEDI = IIF(.w_STLEGAL='S','S','N')
          .w_tariffa = IIF(.w_STLEGAL='S','S','N')
          .w_tipiatt = IIF(.w_STLEGAL='S','S','N')
          .w_raggatt = IIF(.w_STLEGAL='S','S','N')
          .w_raggpre = IIF(.w_STLEGAL='S','S','N')
          .w_oggepra = IIF(.w_STLEGAL='S','S','N')
          .w_statpra = IIF(.w_STLEGAL='S','S','N')
          .w_ubicpra = IIF(.w_STLEGAL='S','S','N')
          .w_tipipra = IIF(.w_STLEGAL='S','S','N')
          .w_matepra = IIF(.w_STLEGAL='S','S','N')
          .w_entipra = IIF(.w_STLEGAL='S','S','N')
          .w_uffipra = IIF(.w_STLEGAL='S','S','N')
          .w_CESPI = IIF(.w_STLEGAL<>'S', ' ', .w_CESPI)
          .w_pagame = IIF(.w_STLEGAL='S', 'S', ' ')
          .w_mastri = IIF(.w_STLEGAL='S', 'S', ' ')
          .w_conti = IIF(.w_STLEGAL='S', 'S', ' ')
          .w_Listini = IIF(.w_STLEGAL='S', 'S', ' ')
          .w_cauconta = IIF(.w_STLEGAL='S', 'S', ' ')
          .w_framode = IIF(.w_STLEGAL='S', 'S', ' ')
          .w_contro = IIF(.w_STLEGAL='S', 'S', ' ')
          .w_caudocu = IIF(.w_STLEGAL='S', 'S', ' ')
          .w_lib_imma = IIF(.w_STLEGAL='S', 'S', ' ')
          .w_Classpra = IIF(.w_STLEGAL='S', 'S', ' ')
          .w_raggtipi = IIF(.w_STLEGAL='S', 'S', ' ')
          .w_modconta = IIF(.w_STLEGAL<>'S', ' ', .w_modconta)
    endwith
  endproc
  proc Calculate_MPHXCJJTMK()
    with this
          * --- Dipendenze da Tariffario
          .w_tipiatt = IIF(.w_tariffa='S','S','N')
          .w_raggatt = IIF(.w_tariffa='S','S','N')
          .w_raggpre = IIF(.w_tariffa='S','S','N')
    endwith
  endproc
  proc Calculate_AKSTKKRMZY()
    with this
          * --- Dipendenze da Tipi attivit�
          .w_raggatt = IIF(.w_tipiatt='S','S','N')
    endwith
  endproc
  proc Calculate_PWWRHVJSQQ()
    with this
          * --- Dipendenze da Enti pratica
          .w_tariffa = IIF(.w_entipra<>'S',' ',.w_tariffa)
          .w_tipiatt = IIF(.w_entipra<>'S',' ',.w_tipiatt)
          .w_raggatt = IIF(.w_entipra<>'S',' ',.w_raggatt)
          .w_raggpre = IIF(.w_entipra<>'S',' ',.w_raggpre)
    endwith
  endproc
  proc Calculate_PHMAVKVEIY()
    with this
          * --- Valorizza caudocu
          .w_caudocu = IIF(ISALT(), .w_cauconta, .w_caudocu)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oVALESE_1_6.enabled = this.oPgFrm.Page1.oPag.oVALESE_1_6.mCond()
    this.oPgFrm.Page1.oPag.oANAL_1_13.enabled = this.oPgFrm.Page1.oPag.oANAL_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCESPI_1_14.enabled = this.oPgFrm.Page1.oPag.oCESPI_1_14.mCond()
    this.oPgFrm.Page1.oPag.oSTATIS_1_16.enabled = this.oPgFrm.Page1.oPag.oSTATIS_1_16.mCond()
    this.oPgFrm.Page1.oPag.oANABIL_1_17.enabled = this.oPgFrm.Page1.oPag.oANABIL_1_17.mCond()
    this.oPgFrm.Page1.oPag.oDISB_1_18.enabled = this.oPgFrm.Page1.oPag.oDISB_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCOMM_1_19.enabled = this.oPgFrm.Page1.oPag.oCOMM_1_19.mCond()
    this.oPgFrm.Page1.oPag.oVEFA_1_20.enabled = this.oPgFrm.Page1.oPag.oVEFA_1_20.mCond()
    this.oPgFrm.Page1.oPag.oSELEZI_1_21.enabled_(this.oPgFrm.Page1.oPag.oSELEZI_1_21.mCond())
    this.oPgFrm.Page1.oPag.oATTSER_1_22.enabled = this.oPgFrm.Page1.oPag.oATTSER_1_22.mCond()
    this.oPgFrm.Page2.oPag.oMATRICOLE_2_29.enabled = this.oPgFrm.Page2.oPag.oMATRICOLE_2_29.mCond()
    this.oPgFrm.Page2.oPag.oTIAT_2_33.enabled = this.oPgFrm.Page2.oPag.oTIAT_2_33.mCond()
    this.oPgFrm.Page2.oPag.oTPAT_2_34.enabled = this.oPgFrm.Page2.oPag.oTPAT_2_34.mCond()
    this.oPgFrm.Page2.oPag.oGRCP_2_35.enabled = this.oPgFrm.Page2.oPag.oGRCP_2_35.mCond()
    this.oPgFrm.Page2.oPag.oRAAT_2_36.enabled = this.oPgFrm.Page2.oPag.oRAAT_2_36.mCond()
    this.oPgFrm.Page2.oPag.oDIPE_2_37.enabled = this.oPgFrm.Page2.oPag.oDIPE_2_37.mCond()
    this.oPgFrm.Page2.oPag.oRAPR_2_38.enabled = this.oPgFrm.Page2.oPag.oRAPR_2_38.mCond()
    this.oPgFrm.Page2.oPag.oMOEL_2_40.enabled = this.oPgFrm.Page2.oPag.oMOEL_2_40.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(2).enabled=not(this.w_STLEGAL<>'S' and Isalt())
    this.oPgFrm.Pages(3).enabled=not(NOT IsAlt() or this.w_STLEGAL<>'S')
    local i_show1
    i_show1=not(this.w_STLEGAL<>'S' and Isalt())
    this.oPgFrm.Pages(2).enabled=i_show1 and not(this.w_STLEGAL<>'S' and Isalt())
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Archivi superiori"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    local i_show2
    i_show2=not(NOT IsAlt() or this.w_STLEGAL<>'S')
    this.oPgFrm.Pages(3).enabled=i_show2 and not(NOT IsAlt() or this.w_STLEGAL<>'S')
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Archivi alterego"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page1.oPag.oCODESE_1_3.visible=!this.oPgFrm.Page1.oPag.oCODESE_1_3.mHide()
    this.oPgFrm.Page1.oPag.oINIESE_1_4.visible=!this.oPgFrm.Page1.oPag.oINIESE_1_4.mHide()
    this.oPgFrm.Page1.oPag.oFINESE_1_5.visible=!this.oPgFrm.Page1.oPag.oFINESE_1_5.mHide()
    this.oPgFrm.Page1.oPag.oVALESE_1_6.visible=!this.oPgFrm.Page1.oPag.oVALESE_1_6.mHide()
    this.oPgFrm.Page1.oPag.oCONTA_1_8.visible=!this.oPgFrm.Page1.oPag.oCONTA_1_8.mHide()
    this.oPgFrm.Page1.oPag.oMAGA_1_9.visible=!this.oPgFrm.Page1.oPag.oMAGA_1_9.mHide()
    this.oPgFrm.Page1.oPag.oVENDI_1_10.visible=!this.oPgFrm.Page1.oPag.oVENDI_1_10.mHide()
    this.oPgFrm.Page1.oPag.oSPEDI_1_11.visible=!this.oPgFrm.Page1.oPag.oSPEDI_1_11.mHide()
    this.oPgFrm.Page1.oPag.oANAL_1_13.visible=!this.oPgFrm.Page1.oPag.oANAL_1_13.mHide()
    this.oPgFrm.Page1.oPag.oCESPI_1_14.visible=!this.oPgFrm.Page1.oPag.oCESPI_1_14.mHide()
    this.oPgFrm.Page1.oPag.oSTATIS_1_16.visible=!this.oPgFrm.Page1.oPag.oSTATIS_1_16.mHide()
    this.oPgFrm.Page1.oPag.oANABIL_1_17.visible=!this.oPgFrm.Page1.oPag.oANABIL_1_17.mHide()
    this.oPgFrm.Page1.oPag.oDISB_1_18.visible=!this.oPgFrm.Page1.oPag.oDISB_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCOMM_1_19.visible=!this.oPgFrm.Page1.oPag.oCOMM_1_19.mHide()
    this.oPgFrm.Page1.oPag.oVEFA_1_20.visible=!this.oPgFrm.Page1.oPag.oVEFA_1_20.mHide()
    this.oPgFrm.Page1.oPag.oSELEZI_1_21.visible=!this.oPgFrm.Page1.oPag.oSELEZI_1_21.mHide()
    this.oPgFrm.Page1.oPag.oATTSER_1_22.visible=!this.oPgFrm.Page1.oPag.oATTSER_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page2.oPag.oagenti_2_7.visible=!this.oPgFrm.Page2.oPag.oagenti_2_7.mHide()
    this.oPgFrm.Page2.oPag.oListini_2_8.visible=!this.oPgFrm.Page2.oPag.oListini_2_8.mHide()
    this.oPgFrm.Page2.oPag.oCATELIST_2_9.visible=!this.oPgFrm.Page2.oPag.oCATELIST_2_9.mHide()
    this.oPgFrm.Page2.oPag.olib_imma_2_10.visible=!this.oPgFrm.Page2.oPag.olib_imma_2_10.mHide()
    this.oPgFrm.Page2.oPag.oTBLSCO_2_11.visible=!this.oPgFrm.Page2.oPag.oTBLSCO_2_11.mHide()
    this.oPgFrm.Page2.oPag.oNOMECLA_2_16.visible=!this.oPgFrm.Page2.oPag.oNOMECLA_2_16.mHide()
    this.oPgFrm.Page2.oPag.ocambi_2_17.visible=!this.oPgFrm.Page2.oPag.ocambi_2_17.mHide()
    this.oPgFrm.Page2.oPag.oSBIL_2_18.visible=!this.oPgFrm.Page2.oPag.oSBIL_2_18.mHide()
    this.oPgFrm.Page2.oPag.oPARALIST_2_19.visible=!this.oPgFrm.Page2.oPag.oPARALIST_2_19.mHide()
    this.oPgFrm.Page2.oPag.oPROVVI_2_20.visible=!this.oPgFrm.Page2.oPag.oPROVVI_2_20.mHide()
    this.oPgFrm.Page2.oPag.oCONTRA1_2_21.visible=!this.oPgFrm.Page2.oPag.oCONTRA1_2_21.mHide()
    this.oPgFrm.Page2.oPag.ocaumaga_2_25.visible=!this.oPgFrm.Page2.oPag.ocaumaga_2_25.mHide()
    this.oPgFrm.Page2.oPag.ocaudocu_2_26.visible=!this.oPgFrm.Page2.oPag.ocaudocu_2_26.mHide()
    this.oPgFrm.Page2.oPag.oARTICOLI_2_27.visible=!this.oPgFrm.Page2.oPag.oARTICOLI_2_27.mHide()
    this.oPgFrm.Page2.oPag.oARTMAG_2_28.visible=!this.oPgFrm.Page2.oPag.oARTMAG_2_28.mHide()
    this.oPgFrm.Page2.oPag.oMESSAGGI_2_30.visible=!this.oPgFrm.Page2.oPag.oMESSAGGI_2_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page2.oPag.ocaumovcc_2_31.visible=!this.oPgFrm.Page2.oPag.ocaumovcc_2_31.mHide()
    this.oPgFrm.Page1.oPag.oSTLEGAL_1_39.visible=!this.oPgFrm.Page1.oPag.oSTLEGAL_1_39.mHide()
    this.oPgFrm.Page2.oPag.oTIAT_2_33.visible=!this.oPgFrm.Page2.oPag.oTIAT_2_33.mHide()
    this.oPgFrm.Page2.oPag.oTPAT_2_34.visible=!this.oPgFrm.Page2.oPag.oTPAT_2_34.mHide()
    this.oPgFrm.Page2.oPag.oGRCP_2_35.visible=!this.oPgFrm.Page2.oPag.oGRCP_2_35.mHide()
    this.oPgFrm.Page2.oPag.oRAAT_2_36.visible=!this.oPgFrm.Page2.oPag.oRAAT_2_36.mHide()
    this.oPgFrm.Page2.oPag.oDIPE_2_37.visible=!this.oPgFrm.Page2.oPag.oDIPE_2_37.mHide()
    this.oPgFrm.Page2.oPag.oRAPR_2_38.visible=!this.oPgFrm.Page2.oPag.oRAPR_2_38.mHide()
    this.oPgFrm.Page2.oPag.oMOEL_2_40.visible=!this.oPgFrm.Page2.oPag.oMOEL_2_40.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.LblAzie.Event(cEvent)
      .oPgFrm.Page2.oPag.LblCopiaDa.Event(cEvent)
      .oPgFrm.Page2.oPag.LblCopiaIn.Event(cEvent)
      .oPgFrm.Page3.oPag.LblCopiaDa.Event(cEvent)
      .oPgFrm.Page3.oPag.LblCopiaIn.Event(cEvent)
        if lower(cEvent)==lower("w_entipra Changed")
          .Calculate_PWWRHVJSQQ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=VALESE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VALESE)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VALESE))
          select VACODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VALESE)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VALESE) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVALESE_1_6'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALESE)
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALESE = NVL(_Link_.VACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_VALESE = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODAZI_1_1.value==this.w_CODAZI)
      this.oPgFrm.Page1.oPag.oCODAZI_1_1.value=this.w_CODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_2.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_2.value=this.w_RAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_3.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_3.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oINIESE_1_4.value==this.w_INIESE)
      this.oPgFrm.Page1.oPag.oINIESE_1_4.value=this.w_INIESE
    endif
    if not(this.oPgFrm.Page1.oPag.oFINESE_1_5.value==this.w_FINESE)
      this.oPgFrm.Page1.oPag.oFINESE_1_5.value=this.w_FINESE
    endif
    if not(this.oPgFrm.Page1.oPag.oVALESE_1_6.value==this.w_VALESE)
      this.oPgFrm.Page1.oPag.oVALESE_1_6.value=this.w_VALESE
    endif
    if not(this.oPgFrm.Page1.oPag.oCONVE_1_7.RadioValue()==this.w_CONVE)
      this.oPgFrm.Page1.oPag.oCONVE_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONTA_1_8.RadioValue()==this.w_CONTA)
      this.oPgFrm.Page1.oPag.oCONTA_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGA_1_9.RadioValue()==this.w_MAGA)
      this.oPgFrm.Page1.oPag.oMAGA_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVENDI_1_10.RadioValue()==this.w_VENDI)
      this.oPgFrm.Page1.oPag.oVENDI_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSPEDI_1_11.RadioValue()==this.w_SPEDI)
      this.oPgFrm.Page1.oPag.oSPEDI_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANAL_1_13.RadioValue()==this.w_ANAL)
      this.oPgFrm.Page1.oPag.oANAL_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCESPI_1_14.RadioValue()==this.w_CESPI)
      this.oPgFrm.Page1.oPag.oCESPI_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTATIS_1_16.RadioValue()==this.w_STATIS)
      this.oPgFrm.Page1.oPag.oSTATIS_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANABIL_1_17.RadioValue()==this.w_ANABIL)
      this.oPgFrm.Page1.oPag.oANABIL_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDISB_1_18.RadioValue()==this.w_DISB)
      this.oPgFrm.Page1.oPag.oDISB_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMM_1_19.RadioValue()==this.w_COMM)
      this.oPgFrm.Page1.oPag.oCOMM_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVEFA_1_20.RadioValue()==this.w_VEFA)
      this.oPgFrm.Page1.oPag.oVEFA_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_21.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oATTSER_1_22.RadioValue()==this.w_ATTSER)
      this.oPgFrm.Page1.oPag.oATTSER_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLABUT_1_30.RadioValue()==this.w_FLABUT)
      this.oPgFrm.Page1.oPag.oFLABUT_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.olocazi_2_1.value==this.w_locazi)
      this.oPgFrm.Page2.oPag.olocazi_2_1.value=this.w_locazi
    endif
    if not(this.oPgFrm.Page2.oPag.onewazi_2_2.value==this.w_newazi)
      this.oPgFrm.Page2.oPag.onewazi_2_2.value=this.w_newazi
    endif
    if not(this.oPgFrm.Page2.oPag.opagame_2_4.RadioValue()==this.w_pagame)
      this.oPgFrm.Page2.oPag.opagame_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.omastri_2_5.RadioValue()==this.w_mastri)
      this.oPgFrm.Page2.oPag.omastri_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oconti_2_6.RadioValue()==this.w_conti)
      this.oPgFrm.Page2.oPag.oconti_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oagenti_2_7.RadioValue()==this.w_agenti)
      this.oPgFrm.Page2.oPag.oagenti_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oListini_2_8.RadioValue()==this.w_Listini)
      this.oPgFrm.Page2.oPag.oListini_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCATELIST_2_9.RadioValue()==this.w_CATELIST)
      this.oPgFrm.Page2.oPag.oCATELIST_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.olib_imma_2_10.RadioValue()==this.w_lib_imma)
      this.oPgFrm.Page2.oPag.olib_imma_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTBLSCO_2_11.RadioValue()==this.w_TBLSCO)
      this.oPgFrm.Page2.oPag.oTBLSCO_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oframode_2_12.RadioValue()==this.w_framode)
      this.oPgFrm.Page2.oPag.oframode_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCLIENTI_2_14.RadioValue()==this.w_CLIENTI)
      this.oPgFrm.Page2.oPag.oCLIENTI_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oforni_2_15.RadioValue()==this.w_forni)
      this.oPgFrm.Page2.oPag.oforni_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNOMECLA_2_16.RadioValue()==this.w_NOMECLA)
      this.oPgFrm.Page2.oPag.oNOMECLA_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.ocambi_2_17.RadioValue()==this.w_cambi)
      this.oPgFrm.Page2.oPag.ocambi_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSBIL_2_18.RadioValue()==this.w_SBIL)
      this.oPgFrm.Page2.oPag.oSBIL_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPARALIST_2_19.RadioValue()==this.w_PARALIST)
      this.oPgFrm.Page2.oPag.oPARALIST_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPROVVI_2_20.RadioValue()==this.w_PROVVI)
      this.oPgFrm.Page2.oPag.oPROVVI_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCONTRA1_2_21.RadioValue()==this.w_CONTRA1)
      this.oPgFrm.Page2.oPag.oCONTRA1_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.ocontro_2_22.RadioValue()==this.w_contro)
      this.oPgFrm.Page2.oPag.ocontro_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.ocauconta_2_23.RadioValue()==this.w_cauconta)
      this.oPgFrm.Page2.oPag.ocauconta_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.omodconta_2_24.RadioValue()==this.w_modconta)
      this.oPgFrm.Page2.oPag.omodconta_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.ocaumaga_2_25.RadioValue()==this.w_caumaga)
      this.oPgFrm.Page2.oPag.ocaumaga_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.ocaudocu_2_26.RadioValue()==this.w_caudocu)
      this.oPgFrm.Page2.oPag.ocaudocu_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARTICOLI_2_27.RadioValue()==this.w_ARTICOLI)
      this.oPgFrm.Page2.oPag.oARTICOLI_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oARTMAG_2_28.RadioValue()==this.w_ARTMAG)
      this.oPgFrm.Page2.oPag.oARTMAG_2_28.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMATRICOLE_2_29.RadioValue()==this.w_MATRICOLE)
      this.oPgFrm.Page2.oPag.oMATRICOLE_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMESSAGGI_2_30.RadioValue()==this.w_MESSAGGI)
      this.oPgFrm.Page2.oPag.oMESSAGGI_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.ocaumovcc_2_31.RadioValue()==this.w_caumovcc)
      this.oPgFrm.Page2.oPag.ocaumovcc_2_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTLEGAL_1_39.RadioValue()==this.w_STLEGAL)
      this.oPgFrm.Page1.oPag.oSTLEGAL_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMANS_2_32.RadioValue()==this.w_MANS)
      this.oPgFrm.Page2.oPag.oMANS_2_32.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIAT_2_33.RadioValue()==this.w_TIAT)
      this.oPgFrm.Page2.oPag.oTIAT_2_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTPAT_2_34.RadioValue()==this.w_TPAT)
      this.oPgFrm.Page2.oPag.oTPAT_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGRCP_2_35.RadioValue()==this.w_GRCP)
      this.oPgFrm.Page2.oPag.oGRCP_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRAAT_2_36.RadioValue()==this.w_RAAT)
      this.oPgFrm.Page2.oPag.oRAAT_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDIPE_2_37.RadioValue()==this.w_DIPE)
      this.oPgFrm.Page2.oPag.oDIPE_2_37.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRAPR_2_38.RadioValue()==this.w_RAPR)
      this.oPgFrm.Page2.oPag.oRAPR_2_38.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMOEL_2_40.RadioValue()==this.w_MOEL)
      this.oPgFrm.Page2.oPag.oMOEL_2_40.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.olocazi_3_1.value==this.w_locazi)
      this.oPgFrm.Page3.oPag.olocazi_3_1.value=this.w_locazi
    endif
    if not(this.oPgFrm.Page3.oPag.onewazi_3_2.value==this.w_newazi)
      this.oPgFrm.Page3.oPag.onewazi_3_2.value=this.w_newazi
    endif
    if not(this.oPgFrm.Page3.oPag.otariffa_3_6.RadioValue()==this.w_tariffa)
      this.oPgFrm.Page3.oPag.otariffa_3_6.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.otipiatt_3_7.RadioValue()==this.w_tipiatt)
      this.oPgFrm.Page3.oPag.otipiatt_3_7.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oraggatt_3_8.RadioValue()==this.w_raggatt)
      this.oPgFrm.Page3.oPag.oraggatt_3_8.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oraggpre_3_9.RadioValue()==this.w_raggpre)
      this.oPgFrm.Page3.oPag.oraggpre_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oraggtipi_3_10.RadioValue()==this.w_raggtipi)
      this.oPgFrm.Page3.oPag.oraggtipi_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ooggepra_3_11.RadioValue()==this.w_oggepra)
      this.oPgFrm.Page3.oPag.ooggepra_3_11.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ostatpra_3_12.RadioValue()==this.w_statpra)
      this.oPgFrm.Page3.oPag.ostatpra_3_12.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oubicpra_3_13.RadioValue()==this.w_ubicpra)
      this.oPgFrm.Page3.oPag.oubicpra_3_13.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.otipipra_3_14.RadioValue()==this.w_tipipra)
      this.oPgFrm.Page3.oPag.otipipra_3_14.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.omatepra_3_15.RadioValue()==this.w_matepra)
      this.oPgFrm.Page3.oPag.omatepra_3_15.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oentipra_3_16.RadioValue()==this.w_entipra)
      this.oPgFrm.Page3.oPag.oentipra_3_16.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.ouffipra_3_17.RadioValue()==this.w_uffipra)
      this.oPgFrm.Page3.oPag.ouffipra_3_17.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oclasspra_3_18.RadioValue()==this.w_classpra)
      this.oPgFrm.Page3.oPag.oclasspra_3_18.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(looktab('AZIENDA','AZCODAZI','AZCODAZI',.w_CODAZI)) And Len(TRIM(.w_CODAZI))=5 And Upper(Trim(.w_CODAZI))<>'XXXXX')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODAZI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice azienda gi� presente non lungo 5 caratteri oppure invalido")
          case   not(.w_CESPI<>'S' or (.w_CAUCONTA='S' AND (.w_ANAL='S' OR .w_FANAL='N' OR ISALT())))  and not(g_CESP<>'S' OR (.w_STLEGAL<>'S' AND isAlt() ))  and (g_CESP='S' )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCESPI_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare l'archivio contab. analitica e causali contabili")
          case   not(.w_DISB<>'S' or (.w_CAUMAGA='S'))  and not(g_DISB<>'S')  and (g_DISB='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDISB_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare l'archivio articoli -servizi e causali magazzino")
          case   not(.w_pagame<>'S' OR .w_vendi='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.opagame_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche vendite / acquisti")
          case   not(.w_conti<>'S' OR (.w_mastri='S' AND .w_pagame='S'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oconti_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche i mastri e i pagamenti")
          case   not(.w_agenti<>'S' or (.w_vendi='S' and .w_maga='S'))  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oagenti_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche il magazzino e le vendite / acquisti")
          case   not(.w_listini<>'S' OR .w_vendi='S')  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oListini_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche vendite / acquisti")
          case   not(.w_CATELIST<>'S' OR .w_MAGA='S')  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCATELIST_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche magazzino")
          case   not(.w_TBLSCO<>'S' or (.w_AGENTI='S' and .w_VENDI='S'))  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oTBLSCO_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche gli agenti e le vendite")
          case   not(.w_clienti<>'S' OR (.w_conta='S' and .w_vendi='S' and .w_mastri='S' and .w_listini='S' and .w_pagame='S' and (.w_agenti='S' or Isalt()) and iif(g_VEFA='S', (.w_vefa='S' or Isalt()), .t.)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLIENTI_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mancano vendite/acq o contabili o mastri, listini, pagamenti, agenti o funzioni avanzate")
          case   not(.w_forni<>'S' or (.w_conta='S' and .w_vendi='S' and .w_mastri='S' and .w_listini='S' and .w_pagame='S' and (.w_agenti='S' or Isalt())))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oforni_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche i gruppi vendite/acq e contabili pi� gli archivi mastri")
          case   not(.w_nomecla<>'S' or .w_maga='S')  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNOMECLA_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche il gruppo magazzino")
          case   not(.w_cambi<>'S' or .w_vendi='S')  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.ocambi_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare il gruppo vendite")
          case   not(.w_PARALIST<>'S' or (.w_MAGA+.w_CONTA+.w_NOMECLA+.w_VENDI+.w_CLIENTI+.w_FORNI='SSSSSS' AND (.w_ANAL='S' OR .w_FANAL<>'S') AND .w_CATELIST='S'))  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPARALIST_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorrono magazzini, vend./acq., nomenclature, clienti/forn., contabili analitica, categorie listini")
          case   not(.w_PROVVI<>'S' or (.w_AGENTI='S'))  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPROVVI_2_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche gli agenti")
          case   not(.w_CONTRA1<>'S' or (.w_MAGA+.w_VENDI+.w_CLIENTI+.w_FORNI+.w_ARTICOLI='SSSSS'))  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCONTRA1_2_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare magazzino, vendite/acquisti, clienti e fornitori o articoli")
          case   not(.w_contro<>'S' or (.w_conti='S' and .w_conta='S'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.ocontro_2_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare il gruppo contabili e l'archivio conti")
          case   not(.w_cauconta<>'S' or (.w_conti='S' and .w_conta='S'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.ocauconta_2_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare il gruppo contabili e l'archivio dei conti")
          case   not(.w_modconta<>'S' OR (.w_cauconta='S' AND .w_clienti='S' AND .w_forni='S'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.omodconta_2_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare gli archivi clienti, fornitori e causali contabili")
          case   not(.w_caumaga<>'S' or (.w_vendi='S' and .w_spedi='S'))  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.ocaumaga_2_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare i gruppi vendite / acquisti e spedizioni")
          case   not(.w_caudocu<>'S' or (.w_cauconta='S' and .w_caumaga='S' and .w_maga='S'))  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.ocaudocu_2_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare gli archivi causali contabili, causali magazzino e magazzini")
          case   not(.w_ARTICOLI<>'S' or ((.w_MAGA+.w_CONTA+.w_NOMECLA+.w_VENDI+.w_CLIENTI+.w_FORNI)='SSSSSS' AND (.w_ANAL='S' OR .w_FANAL<>'S')))  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARTICOLI_2_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorrono magazzini, vend./acq., nomenclature, clienti/forrn., contabili analitica, distinta base, funzioni avanzate")
          case   not(.w_ARTMAG<>'S' or (.w_MAGA='S' AND .w_ARTICOLI='S'))  and not(ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARTMAG_2_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare magazzini e articoli")
          case   not(.w_MATRICOLE<>'S' or (.w_ARTICOLI='S'))  and (g_MATR='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMATRICOLE_2_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorrono gli articoli")
          case   not(.w_caumovcc<>'S' or (.w_conti='S' and .w_clienti='S' and .w_forni='S' and .w_conta='S' and .w_cauconta='S'))  and not(g_BANC<>'S' OR ISALT())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.ocaumovcc_2_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare il gruppo contabili e gli archivi conti, clienti, fornitori e causali contabili")
          case   not(.w_TIAT='S' and .w_ARTICOLI='S')  and not(.w_ATTSER<>'S')  and (.w_ATTSER='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oTPAT_2_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorrono le tipologie attivit�,articoli/servizi")
          case   not(.w_TPAT='S')  and not(.w_ATTSER<>'S')  and (.w_ATTSER='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRAAT_2_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorrono i tipi attivit�")
          case   not(.w_MANS='S'  and .w_CONTI='S' and  (.w_ANAL='S' OR .w_FANAL='N') AND .w_ARTICOLI='S')  and not(.w_ATTSER<>'S')  and (.w_ATTSER='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDIPE_2_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorrono: articoli/servizi,archivi di analitica,reparti,mansioni,conti")
          case   not(.w_ARTICOLI='S')  and not(.w_ATTSER<>'S')  and (.w_ATTSER='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRAPR_2_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorrono articoli/servizi")
          case   not(.w_listini='S' and  .w_caudocu ='S' and .w_TPAT='S' and .w_DIPE='S')  and not(.w_ATTSER<>'S')  and (.w_ATTSER='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMOEL_2_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorrono  i listini,le causali documento,i tipi attivit� e la struttura aziendale")
          case   not(.w_tariffa<>'S' OR (.w_entipra='S' and .w_CONTA='S'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.otariffa_3_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche Enti pratica e gli archivi contabili")
          case   not(.w_tipiatt<>'S' OR .w_tariffa='S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.otipiatt_3_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche Tariffario")
          case   not(.w_raggatt<>'S' OR (.w_tariffa='S' and .w_tipiatt='S'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oraggatt_3_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche Tariffario e Tipi attivit�")
          case   not(.w_raggpre<>'S' OR .w_tariffa='S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oraggpre_3_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Occorre copiare anche Tariffario")
          case   not(.w_entipra='S' OR .w_tariffa<>'S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oentipra_3_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Per copiare il Tariffario occorre Enti pratica")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_kca
      * Controllo articoli/funzioni avanzate
      If  i_bRes And ((.w_ARTICOLI='S' and Empty(.w_VEFA)) Or (Empty(.w_ARTICOLI)and .w_VEFA='S' )) and g_VEFA='S'
         i_bRes = .f.
         i_bnoChk = .f.
      	 i_cErrorMsg = Ah_MsgFormat("Articoli/Servizi e funzioni avanzate sono dipendenti l'uno dall'altro. Attivare entrambi")
      endif
      
      If i_bRes And ((.w_ARTICOLI='S' and Empty(.w_DISB)) Or (Empty(.w_ARTICOLI)and .w_DISB='S')) and g_DISB='S'
         i_bRes = .f.
         i_bnoChk = .f.
      	 i_cErrorMsg = Ah_MsgFormat("Articoli/Servizi e distinta base sono dipendenti l'uno dall'altro. Attivare entrambi")
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_CONTA = this.w_CONTA
    this.o_VENDI = this.w_VENDI
    this.o_DISB = this.w_DISB
    this.o_VEFA = this.w_VEFA
    this.o_ATTSER = this.w_ATTSER
    this.o_cauconta = this.w_cauconta
    this.o_STLEGAL = this.w_STLEGAL
    this.o_tariffa = this.w_tariffa
    this.o_tipiatt = this.w_tipiatt
    return

enddefine

* --- Define pages as container
define class tgsut_kcaPag1 as StdContainer
  Width  = 540
  height = 334
  stdWidth  = 540
  stdheight = 334
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODAZI_1_1 as StdField with uid="OORKIUURCQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODAZI", cQueryName = "CODAZI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice azienda gi� presente non lungo 5 caratteri oppure invalido",;
    ToolTipText = "Codice azienda da generare",;
    HelpContextID = 117682138,;
   bGlobalFont=.t.,;
    Height=21, Width=66, Left=76, Top=11, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5)

  func oCODAZI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(looktab('AZIENDA','AZCODAZI','AZCODAZI',.w_CODAZI)) And Len(TRIM(.w_CODAZI))=5 And Upper(Trim(.w_CODAZI))<>'XXXXX')
    endwith
    return bRes
  endfunc

  add object oRAGAZI_1_2 as StdField with uid="HVULULTXST",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione azienda",;
    HelpContextID = 117673194,;
   bGlobalFont=.t.,;
    Height=21, Width=274, Left=229, Top=11, InputMask=replicate('X',30)

  add object oCODESE_1_3 as StdField with uid="TDJGTTKRGJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio da generare",;
    HelpContextID = 191868890,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=76, Top=42, InputMask=replicate('X',4)

  func oCODESE_1_3.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oINIESE_1_4 as StdField with uid="CSCLQIRXVW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_INIESE", cQueryName = "INIESE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio esercizio",;
    HelpContextID = 191848570,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=181, Top=42

  func oINIESE_1_4.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFINESE_1_5 as StdField with uid="MAWHLKOHOX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FINESE", cQueryName = "FINESE",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine esercizio",;
    HelpContextID = 191829418,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=305, Top=42

  func oFINESE_1_5.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oVALESE_1_6 as StdField with uid="APBYUPPTTF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_VALESE", cQueryName = "VALESE",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta di conto dell'esercizio da generare",;
    HelpContextID = 191839402,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=455, Top=42, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALESE"

  func oVALESE_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VENDI='S' )
    endwith
   endif
  endfunc

  func oVALESE_1_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oVALESE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oVALESE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVALESE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oVALESE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oVALESE_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_VALESE
     i_obj.ecpSave()
  endproc

  add object oCONVE_1_7 as StdCheck with uid="HZKFBCWGEO",rtseq=7,rtrep=.f.,left=76, top=73, caption="Tabella conversioni",;
    ToolTipText = "Se attivo: importa anche la tabelle delle conversioni eseguite per l'azienda origine",;
    HelpContextID = 247590950,;
    cFormVar="w_CONVE", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCONVE_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCONVE_1_7.GetRadio()
    this.Parent.oContained.w_CONVE = this.RadioValue()
    return .t.
  endfunc

  func oCONVE_1_7.SetRadio()
    this.Parent.oContained.w_CONVE=trim(this.Parent.oContained.w_CONVE)
    this.value = ;
      iif(this.Parent.oContained.w_CONVE=='S',1,;
      0)
  endfunc

  add object oCONTA_1_8 as StdCheck with uid="JEMUSVHQJS",rtseq=8,rtrep=.f.,left=27, top=118, caption="Contabili",;
    ToolTipText = "Se attivo, vengono copiate le tabelle dei Codici Iva, delle Categorie contabili cli/for, delle Categorie contabili articoli",;
    HelpContextID = 243265574,;
    cFormVar="w_CONTA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONTA_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oCONTA_1_8.GetRadio()
    this.Parent.oContained.w_CONTA = this.RadioValue()
    return .t.
  endfunc

  func oCONTA_1_8.SetRadio()
    this.Parent.oContained.w_CONTA=trim(this.Parent.oContained.w_CONTA)
    this.value = ;
      iif(this.Parent.oContained.w_CONTA=='S',1,;
      0)
  endfunc

  func oCONTA_1_8.mHide()
    with this.Parent.oContained
      return (.w_STLEGAL<>'S' AND isAlt())
    endwith
  endfunc

  add object oMAGA_1_9 as StdCheck with uid="PECWKVMNXX",rtseq=9,rtrep=.f.,left=27, top=136, caption="Magazzino",;
    HelpContextID = 173830854,;
    cFormVar="w_MAGA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMAGA_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oMAGA_1_9.GetRadio()
    this.Parent.oContained.w_MAGA = this.RadioValue()
    return .t.
  endfunc

  func oMAGA_1_9.SetRadio()
    this.Parent.oContained.w_MAGA=trim(this.Parent.oContained.w_MAGA)
    this.value = ;
      iif(this.Parent.oContained.w_MAGA=='S',1,;
      0)
  endfunc

  func oMAGA_1_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oVENDI_1_10 as StdCheck with uid="IBTBVDHXRS",rtseq=10,rtrep=.f.,left=27, top=154, caption="Vendite / acquisti",;
    HelpContextID = 250603350,;
    cFormVar="w_VENDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVENDI_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oVENDI_1_10.GetRadio()
    this.Parent.oContained.w_VENDI = this.RadioValue()
    return .t.
  endfunc

  func oVENDI_1_10.SetRadio()
    this.Parent.oContained.w_VENDI=trim(this.Parent.oContained.w_VENDI)
    this.value = ;
      iif(this.Parent.oContained.w_VENDI=='S',1,;
      0)
  endfunc

  func oVENDI_1_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oSPEDI_1_11 as StdCheck with uid="MKCNATJKMN",rtseq=11,rtrep=.f.,left=27, top=172, caption="Spedizioni / INTRA",;
    HelpContextID = 250569254,;
    cFormVar="w_SPEDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSPEDI_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oSPEDI_1_11.GetRadio()
    this.Parent.oContained.w_SPEDI = this.RadioValue()
    return .t.
  endfunc

  func oSPEDI_1_11.SetRadio()
    this.Parent.oContained.w_SPEDI=trim(this.Parent.oContained.w_SPEDI)
    this.value = ;
      iif(this.Parent.oContained.w_SPEDI=='S',1,;
      0)
  endfunc

  func oSPEDI_1_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oANAL_1_13 as StdCheck with uid="AQLVYSFYHM",rtseq=13,rtrep=.f.,left=27, top=190, caption="Contab. analitica",;
    HelpContextID = 174530310,;
    cFormVar="w_ANAL", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Mancano l'archivio spedizioni e i clienti";
   , bGlobalFont=.t.


  func oANAL_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oANAL_1_13.GetRadio()
    this.Parent.oContained.w_ANAL = this.RadioValue()
    return .t.
  endfunc

  func oANAL_1_13.SetRadio()
    this.Parent.oContained.w_ANAL=trim(this.Parent.oContained.w_ANAL)
    this.value = ;
      iif(this.Parent.oContained.w_ANAL=='S',1,;
      0)
  endfunc

  func oANAL_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FANAL='S')
    endwith
   endif
  endfunc

  func oANAL_1_13.mHide()
    with this.Parent.oContained
      return (.w_FANAL<>'S')
    endwith
  endfunc

  add object oCESPI_1_14 as StdCheck with uid="BVNXEYUNSK",rtseq=14,rtrep=.f.,left=27, top=208, caption="Cespiti",;
    HelpContextID = 251409958,;
    cFormVar="w_CESPI", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Occorre copiare l'archivio contab. analitica e causali contabili";
   , bGlobalFont=.t.


  func oCESPI_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oCESPI_1_14.GetRadio()
    this.Parent.oContained.w_CESPI = this.RadioValue()
    return .t.
  endfunc

  func oCESPI_1_14.SetRadio()
    this.Parent.oContained.w_CESPI=trim(this.Parent.oContained.w_CESPI)
    this.value = ;
      iif(this.Parent.oContained.w_CESPI=='S',1,;
      0)
  endfunc

  func oCESPI_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_CESP='S' )
    endwith
   endif
  endfunc

  func oCESPI_1_14.mHide()
    with this.Parent.oContained
      return (g_CESP<>'S' OR (.w_STLEGAL<>'S' AND isAlt() ))
    endwith
  endfunc

  func oCESPI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CESPI<>'S' or (.w_CAUCONTA='S' AND (.w_ANAL='S' OR .w_FANAL='N' OR ISALT())))
    endwith
    return bRes
  endfunc

  add object oSTATIS_1_16 as StdCheck with uid="RQUXHWHNEU",rtseq=16,rtrep=.f.,left=185, top=118, caption="Statistiche",;
    HelpContextID = 234936794,;
    cFormVar="w_STATIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTATIS_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oSTATIS_1_16.GetRadio()
    this.Parent.oContained.w_STATIS = this.RadioValue()
    return .t.
  endfunc

  func oSTATIS_1_16.SetRadio()
    this.Parent.oContained.w_STATIS=trim(this.Parent.oContained.w_STATIS)
    this.value = ;
      iif(this.Parent.oContained.w_STATIS=='S',1,;
      0)
  endfunc

  func oSTATIS_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_STAT='S')
    endwith
   endif
  endfunc

  func oSTATIS_1_16.mHide()
    with this.Parent.oContained
      return (g_STAT<>'S')
    endwith
  endfunc

  add object oANABIL_1_17 as StdCheck with uid="CSVHBNMTGA",rtseq=17,rtrep=.f.,left=185, top=136, caption="Analisi bilancio",;
    HelpContextID = 85123322,;
    cFormVar="w_ANABIL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANABIL_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oANABIL_1_17.GetRadio()
    this.Parent.oContained.w_ANABIL = this.RadioValue()
    return .t.
  endfunc

  func oANABIL_1_17.SetRadio()
    this.Parent.oContained.w_ANABIL=trim(this.Parent.oContained.w_ANABIL)
    this.value = ;
      iif(this.Parent.oContained.w_ANABIL=='S',1,;
      0)
  endfunc

  func oANABIL_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_BILC='S')
    endwith
   endif
  endfunc

  func oANABIL_1_17.mHide()
    with this.Parent.oContained
      return (g_BILC<>'S')
    endwith
  endfunc

  add object oDISB_1_18 as StdCheck with uid="GQJPWXNBSJ",rtseq=18,rtrep=.f.,left=185, top=154, caption="Distinta base",;
    HelpContextID = 173947446,;
    cFormVar="w_DISB", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Occorre copiare l'archivio articoli -servizi e causali magazzino";
   , bGlobalFont=.t.


  func oDISB_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oDISB_1_18.GetRadio()
    this.Parent.oContained.w_DISB = this.RadioValue()
    return .t.
  endfunc

  func oDISB_1_18.SetRadio()
    this.Parent.oContained.w_DISB=trim(this.Parent.oContained.w_DISB)
    this.value = ;
      iif(this.Parent.oContained.w_DISB=='S',1,;
      0)
  endfunc

  func oDISB_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DISB='S')
    endwith
   endif
  endfunc

  func oDISB_1_18.mHide()
    with this.Parent.oContained
      return (g_DISB<>'S')
    endwith
  endfunc

  func oDISB_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DISB<>'S' or (.w_CAUMAGA='S'))
    endwith
    return bRes
  endfunc

  add object oCOMM_1_19 as StdCheck with uid="QYRIGUSSQL",rtseq=19,rtrep=.f.,left=185, top=172, caption="Gestione progetti",;
    HelpContextID = 174645286,;
    cFormVar="w_COMM", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Occorre copiare l'archivio causali contabili";
   , bGlobalFont=.t.


  func oCOMM_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oCOMM_1_19.GetRadio()
    this.Parent.oContained.w_COMM = this.RadioValue()
    return .t.
  endfunc

  func oCOMM_1_19.SetRadio()
    this.Parent.oContained.w_COMM=trim(this.Parent.oContained.w_COMM)
    this.value = ;
      iif(this.Parent.oContained.w_COMM=='S',1,;
      0)
  endfunc

  func oCOMM_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S')
    endwith
   endif
  endfunc

  func oCOMM_1_19.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oVEFA_1_20 as StdCheck with uid="IOOFJHQCNW",rtseq=20,rtrep=.f.,left=185, top=190, caption="Funzioni avanzate",;
    HelpContextID = 173827926,;
    cFormVar="w_VEFA", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Occorre copiare l'archivio articoli/servizi";
   , bGlobalFont=.t.


  func oVEFA_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVEFA_1_20.GetRadio()
    this.Parent.oContained.w_VEFA = this.RadioValue()
    return .t.
  endfunc

  func oVEFA_1_20.SetRadio()
    this.Parent.oContained.w_VEFA=trim(this.Parent.oContained.w_VEFA)
    this.value = ;
      iif(this.Parent.oContained.w_VEFA=='S',1,;
      0)
  endfunc

  func oVEFA_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_VEFA='S')
    endwith
   endif
  endfunc

  func oVEFA_1_20.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oSELEZI_1_21 as StdRadio with uid="YFCKLYJWVG",rtseq=21,rtrep=.f.,left=7, top=257, width=143,height=32;
    , ToolTipText = "Seleziona/deseleziona tutti i gruppi e tutti gli archivi";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_21.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 117389530
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 117389530
      this.Buttons(2).Top=15
      this.SetAll("Width",141)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutti i gruppi e tutti gli archivi")
      StdRadio::init()
    endproc

  func oSELEZI_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_21.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_21.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc

  func oSELEZI_1_21.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oATTSER_1_22 as StdCheck with uid="BSWXDNHFBZ",rtseq=22,rtrep=.f.,left=185, top=208, caption="Attivit� e servizi",;
    HelpContextID = 255896314,;
    cFormVar="w_ATTSER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oATTSER_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oATTSER_1_22.GetRadio()
    this.Parent.oContained.w_ATTSER = this.RadioValue()
    return .t.
  endfunc

  func oATTSER_1_22.SetRadio()
    this.Parent.oContained.w_ATTSER=trim(this.Parent.oContained.w_ATTSER)
    this.value = ;
      iif(this.Parent.oContained.w_ATTSER=='S',1,;
      0)
  endfunc

  func oATTSER_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ('AGEN' $ UPPER(i_cModules))
    endwith
   endif
  endfunc

  func oATTSER_1_22.mHide()
    with this.Parent.oContained
      return (NOT ('AGEN' $ UPPER(i_cModules)) or isalt())
    endwith
  endfunc


  add object oBtn_1_23 as StdButton with uid="FJPWVKCLIK",left=421, top=255, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Procedi con la creazione di una nuova azienda";
    , HelpContextID = 169290982;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT (EMPTY(.w_CODAZI) OR EMPTY(.w_CODESE)))
      endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="HSUFYMENFE",left=473, top=255, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176579654;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_29 as cp_runprogram with uid="PTKDLMHYJL",left=-2, top=332, width=213,height=19,;
    caption='GSUT_BCS',;
   bGlobalFont=.t.,;
    prg="GSUT_BCS",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 228563783

  add object oFLABUT_1_30 as StdRadio with uid="HBAVIIIVQC",rtseq=24,rtrep=.f.,left=352, top=122, width=129,height=32;
    , ToolTipText = "Abilita l'accesso a tutti gli utente dell'azienda origine o al solo supervisore";
    , cFormVar="w_FLABUT", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oFLABUT_1_30.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Tutti gli utenti"
      this.Buttons(1).HelpContextID = 206758570
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Solo supervisore"
      this.Buttons(2).HelpContextID = 206758570
      this.Buttons(2).Top=15
      this.SetAll("Width",127)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Abilita l'accesso a tutti gli utente dell'azienda origine o al solo supervisore")
      StdRadio::init()
    endproc

  func oFLABUT_1_30.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oFLABUT_1_30.GetRadio()
    this.Parent.oContained.w_FLABUT = this.RadioValue()
    return .t.
  endfunc

  func oFLABUT_1_30.SetRadio()
    this.Parent.oContained.w_FLABUT=trim(this.Parent.oContained.w_FLABUT)
    this.value = ;
      iif(this.Parent.oContained.w_FLABUT=='T',1,;
      iif(this.Parent.oContained.w_FLABUT=='S',2,;
      0))
  endfunc

  add object oSTLEGAL_1_39 as StdCheck with uid="VTNLTRSBZJ",rtseq=57,rtrep=.f.,left=27, top=226, caption="Gestione studio legale",;
    HelpContextID = 265344550,;
    cFormVar="w_STLEGAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTLEGAL_1_39.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSTLEGAL_1_39.GetRadio()
    this.Parent.oContained.w_STLEGAL = this.RadioValue()
    return .t.
  endfunc

  func oSTLEGAL_1_39.SetRadio()
    this.Parent.oContained.w_STLEGAL=trim(this.Parent.oContained.w_STLEGAL)
    this.value = ;
      iif(this.Parent.oContained.w_STLEGAL=='S',1,;
      0)
  endfunc

  func oSTLEGAL_1_39.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc


  add object LblAzie as cp_calclbl with uid="KZLEWMBSIL",left=3, top=12, width=70,height=25,;
    caption='LblAzie',;
   bGlobalFont=.t.,;
    caption="label text",fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,Alignment=1,;
    nPag=1;
    , HelpContextID = 184476598

  add object oStr_1_24 as StdString with uid="YONFECPRTS",Visible=.t., Left=3, Top=43,;
    Alignment=1, Width=70, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="OTIJLBRUGP",Visible=.t., Left=148, Top=12,;
    Alignment=1, Width=79, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="EMMAWCFBWY",Visible=.t., Left=6, Top=95,;
    Alignment=0, Width=298, Height=15,;
    Caption="Gruppo di archivi da copiare"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="AVYOBOYAFW",Visible=.t., Left=354, Top=95,;
    Alignment=0, Width=130, Height=15,;
    Caption="Abilitazione utenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="HFEWTHZIRQ",Visible=.t., Left=134, Top=43,;
    Alignment=1, Width=45, Height=18,;
    Caption="Inizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="NYHPYJJZJN",Visible=.t., Left=262, Top=43,;
    Alignment=1, Width=40, Height=18,;
    Caption="Fine:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="CEMSWMQCHO",Visible=.t., Left=406, Top=43,;
    Alignment=1, Width=47, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oBox_1_28 as StdBox with uid="BBEPDMUEGZ",left=7, top=113, width=497,height=137

  add object oBox_1_33 as StdBox with uid="GKPRUDJJQQ",left=341, top=113, width=1,height=137
enddefine
define class tgsut_kcaPag2 as StdContainer
  Width  = 540
  height = 334
  stdWidth  = 540
  stdheight = 334
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object olocazi_2_1 as StdField with uid="KRHNMQMSMP",rtseq=25,rtrep=.f.,;
    cFormVar = "w_locazi", cQueryName = "locazi",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 81894730,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=164, Top=5, InputMask=replicate('X',5)

  add object onewazi_2_2 as StdField with uid="ILBRHGFZKP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_newazi", cQueryName = "newazi",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 81815338,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=317, Top=5, InputMask=replicate('X',5)

  add object opagame_2_4 as StdCheck with uid="YRMVGRPCLH",rtseq=28,rtrep=.f.,left=9, top=67, caption="Pagamenti",;
    HelpContextID = 162622218,;
    cFormVar="w_pagame", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare anche vendite / acquisti";
   , bGlobalFont=.t.


  func opagame_2_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func opagame_2_4.GetRadio()
    this.Parent.oContained.w_pagame = this.RadioValue()
    return .t.
  endfunc

  func opagame_2_4.SetRadio()
    this.Parent.oContained.w_pagame=trim(this.Parent.oContained.w_pagame)
    this.value = ;
      iif(this.Parent.oContained.w_pagame=='S',1,;
      0)
  endfunc

  func opagame_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_pagame<>'S' OR .w_vendi='S')
    endwith
    return bRes
  endfunc

  add object omastri_2_5 as StdCheck with uid="NXOACOGNYJ",rtseq=29,rtrep=.f.,left=9, top=88, caption="Mastri",;
    HelpContextID = 88976186,;
    cFormVar="w_mastri", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func omastri_2_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func omastri_2_5.GetRadio()
    this.Parent.oContained.w_mastri = this.RadioValue()
    return .t.
  endfunc

  func omastri_2_5.SetRadio()
    this.Parent.oContained.w_mastri=trim(this.Parent.oContained.w_mastri)
    this.value = ;
      iif(this.Parent.oContained.w_mastri=='S',1,;
      0)
  endfunc

  add object oconti_2_6 as StdCheck with uid="OVQYJVTHUS",rtseq=30,rtrep=.f.,left=9, top=109, caption="Conti",;
    HelpContextID = 249425370,;
    cFormVar="w_conti", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare anche i mastri e i pagamenti";
   , bGlobalFont=.t.


  func oconti_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oconti_2_6.GetRadio()
    this.Parent.oContained.w_conti = this.RadioValue()
    return .t.
  endfunc

  func oconti_2_6.SetRadio()
    this.Parent.oContained.w_conti=trim(this.Parent.oContained.w_conti)
    this.value = ;
      iif(this.Parent.oContained.w_conti=='S',1,;
      0)
  endfunc

  func oconti_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_conti<>'S' OR (.w_mastri='S' AND .w_pagame='S'))
    endwith
    return bRes
  endfunc

  add object oagenti_2_7 as StdCheck with uid="MZVZPTLWNL",rtseq=31,rtrep=.f.,left=9, top=130, caption="Agenti",;
    HelpContextID = 87328250,;
    cFormVar="w_agenti", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare anche il magazzino e le vendite / acquisti";
   , bGlobalFont=.t.


  func oagenti_2_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oagenti_2_7.GetRadio()
    this.Parent.oContained.w_agenti = this.RadioValue()
    return .t.
  endfunc

  func oagenti_2_7.SetRadio()
    this.Parent.oContained.w_agenti=trim(this.Parent.oContained.w_agenti)
    this.value = ;
      iif(this.Parent.oContained.w_agenti=='S',1,;
      0)
  endfunc

  func oagenti_2_7.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oagenti_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_agenti<>'S' or (.w_vendi='S' and .w_maga='S'))
    endwith
    return bRes
  endfunc

  add object oListini_2_8 as StdCheck with uid="UEBDJBDKSX",rtseq=32,rtrep=.f.,left=9, top=151, caption="Listini",;
    HelpContextID = 253909686,;
    cFormVar="w_Listini", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare anche vendite / acquisti";
   , bGlobalFont=.t.


  func oListini_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oListini_2_8.GetRadio()
    this.Parent.oContained.w_Listini = this.RadioValue()
    return .t.
  endfunc

  func oListini_2_8.SetRadio()
    this.Parent.oContained.w_Listini=trim(this.Parent.oContained.w_Listini)
    this.value = ;
      iif(this.Parent.oContained.w_Listini=='S',1,;
      0)
  endfunc

  func oListini_2_8.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oListini_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_listini<>'S' OR .w_vendi='S')
    endwith
    return bRes
  endfunc

  add object oCATELIST_2_9 as StdCheck with uid="LEGPYBLABW",rtseq=33,rtrep=.f.,left=9, top=172, caption="Categorie listini",;
    HelpContextID = 132038022,;
    cFormVar="w_CATELIST", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare anche magazzino";
   , bGlobalFont=.t.


  func oCATELIST_2_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oCATELIST_2_9.GetRadio()
    this.Parent.oContained.w_CATELIST = this.RadioValue()
    return .t.
  endfunc

  func oCATELIST_2_9.SetRadio()
    this.Parent.oContained.w_CATELIST=trim(this.Parent.oContained.w_CATELIST)
    this.value = ;
      iif(this.Parent.oContained.w_CATELIST=='S',1,;
      0)
  endfunc

  func oCATELIST_2_9.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oCATELIST_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CATELIST<>'S' OR .w_MAGA='S')
    endwith
    return bRes
  endfunc

  add object olib_imma_2_10 as StdCheck with uid="DWKHMCWBRH",rtseq=34,rtrep=.f.,left=9, top=193, caption="Librerie immagini",;
    HelpContextID = 32748265,;
    cFormVar="w_lib_imma", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func olib_imma_2_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func olib_imma_2_10.GetRadio()
    this.Parent.oContained.w_lib_imma = this.RadioValue()
    return .t.
  endfunc

  func olib_imma_2_10.SetRadio()
    this.Parent.oContained.w_lib_imma=trim(this.Parent.oContained.w_lib_imma)
    this.value = ;
      iif(this.Parent.oContained.w_lib_imma=='S',1,;
      0)
  endfunc

  func olib_imma_2_10.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oTBLSCO_2_11 as StdCheck with uid="XKNEYPYEJC",rtseq=35,rtrep=.f.,left=9, top=214, caption="Tabella sconti/magg.",;
    HelpContextID = 39926730,;
    cFormVar="w_TBLSCO", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare anche gli agenti e le vendite";
   , bGlobalFont=.t.


  func oTBLSCO_2_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oTBLSCO_2_11.GetRadio()
    this.Parent.oContained.w_TBLSCO = this.RadioValue()
    return .t.
  endfunc

  func oTBLSCO_2_11.SetRadio()
    this.Parent.oContained.w_TBLSCO=trim(this.Parent.oContained.w_TBLSCO)
    this.value = ;
      iif(this.Parent.oContained.w_TBLSCO=='S',1,;
      0)
  endfunc

  func oTBLSCO_2_11.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oTBLSCO_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TBLSCO<>'S' or (.w_AGENTI='S' and .w_VENDI='S'))
    endwith
    return bRes
  endfunc

  add object oframode_2_12 as StdCheck with uid="CXQGLLRGMA",rtseq=36,rtrep=.f.,left=9, top=236, caption="Frasi modello",;
    HelpContextID = 91899222,;
    cFormVar="w_framode", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oframode_2_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oframode_2_12.GetRadio()
    this.Parent.oContained.w_framode = this.RadioValue()
    return .t.
  endfunc

  func oframode_2_12.SetRadio()
    this.Parent.oContained.w_framode=trim(this.Parent.oContained.w_framode)
    this.value = ;
      iif(this.Parent.oContained.w_framode=='S',1,;
      0)
  endfunc

  add object oCLIENTI_2_14 as StdCheck with uid="JWTAHBLZDV",rtseq=37,rtrep=.f.,left=9, top=257, caption="Clienti",;
    HelpContextID = 54566182,;
    cFormVar="w_CLIENTI", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Mancano vendite/acq o contabili o mastri, listini, pagamenti, agenti o funzioni avanzate";
   , bGlobalFont=.t.


  func oCLIENTI_2_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oCLIENTI_2_14.GetRadio()
    this.Parent.oContained.w_CLIENTI = this.RadioValue()
    return .t.
  endfunc

  func oCLIENTI_2_14.SetRadio()
    this.Parent.oContained.w_CLIENTI=trim(this.Parent.oContained.w_CLIENTI)
    this.value = ;
      iif(this.Parent.oContained.w_CLIENTI=='S',1,;
      0)
  endfunc

  func oCLIENTI_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_clienti<>'S' OR (.w_conta='S' and .w_vendi='S' and .w_mastri='S' and .w_listini='S' and .w_pagame='S' and (.w_agenti='S' or Isalt()) and iif(g_VEFA='S', (.w_vefa='S' or Isalt()), .t.)))
    endwith
    return bRes
  endfunc

  add object oforni_2_15 as StdCheck with uid="HYAKXUVLLX",rtseq=38,rtrep=.f.,left=9, top=278, caption="Fornitori",;
    HelpContextID = 249802154,;
    cFormVar="w_forni", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare anche i gruppi vendite/acq e contabili pi� gli archivi mastri";
   , bGlobalFont=.t.


  func oforni_2_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oforni_2_15.GetRadio()
    this.Parent.oContained.w_forni = this.RadioValue()
    return .t.
  endfunc

  func oforni_2_15.SetRadio()
    this.Parent.oContained.w_forni=trim(this.Parent.oContained.w_forni)
    this.value = ;
      iif(this.Parent.oContained.w_forni=='S',1,;
      0)
  endfunc

  func oforni_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_forni<>'S' or (.w_conta='S' and .w_vendi='S' and .w_mastri='S' and .w_listini='S' and .w_pagame='S' and (.w_agenti='S' or Isalt())))
    endwith
    return bRes
  endfunc

  add object oNOMECLA_2_16 as StdCheck with uid="XLKGFZOWXD",rtseq=39,rtrep=.f.,left=9, top=299, caption="Nomenclature",;
    HelpContextID = 91168554,;
    cFormVar="w_NOMECLA", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare anche il gruppo magazzino";
   , bGlobalFont=.t.


  func oNOMECLA_2_16.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oNOMECLA_2_16.GetRadio()
    this.Parent.oContained.w_NOMECLA = this.RadioValue()
    return .t.
  endfunc

  func oNOMECLA_2_16.SetRadio()
    this.Parent.oContained.w_NOMECLA=trim(this.Parent.oContained.w_NOMECLA)
    this.value = ;
      iif(this.Parent.oContained.w_NOMECLA=='S',1,;
      0)
  endfunc

  func oNOMECLA_2_16.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oNOMECLA_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_nomecla<>'S' or .w_maga='S')
    endwith
    return bRes
  endfunc

  add object ocambi_2_17 as StdCheck with uid="JFLXUTPNAN",rtseq=40,rtrep=.f.,left=179, top=69, caption="Cambi giornalieri",;
    HelpContextID = 250612698,;
    cFormVar="w_cambi", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare il gruppo vendite";
   , bGlobalFont=.t.


  func ocambi_2_17.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func ocambi_2_17.GetRadio()
    this.Parent.oContained.w_cambi = this.RadioValue()
    return .t.
  endfunc

  func ocambi_2_17.SetRadio()
    this.Parent.oContained.w_cambi=trim(this.Parent.oContained.w_cambi)
    this.value = ;
      iif(this.Parent.oContained.w_cambi=='S',1,;
      0)
  endfunc

  func ocambi_2_17.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func ocambi_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_cambi<>'S' or .w_vendi='S')
    endwith
    return bRes
  endfunc

  add object oSBIL_2_18 as StdCheck with uid="HMUNVRTYFI",rtseq=41,rtrep=.f.,left=179, top=90, caption="Struttura di bilancio UE",;
    HelpContextID = 174560294,;
    cFormVar="w_SBIL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSBIL_2_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oSBIL_2_18.GetRadio()
    this.Parent.oContained.w_SBIL = this.RadioValue()
    return .t.
  endfunc

  func oSBIL_2_18.SetRadio()
    this.Parent.oContained.w_SBIL=trim(this.Parent.oContained.w_SBIL)
    this.value = ;
      iif(this.Parent.oContained.w_SBIL=='S',1,;
      0)
  endfunc

  func oSBIL_2_18.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object oPARALIST_2_19 as StdCheck with uid="ROAFOPLLHJ",rtseq=42,rtrep=.f.,left=179, top=111, caption="Parametri listini",;
    HelpContextID = 132308150,;
    cFormVar="w_PARALIST", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorrono magazzini, vend./acq., nomenclature, clienti/forn., contabili analitica, categorie listini";
   , bGlobalFont=.t.


  func oPARALIST_2_19.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oPARALIST_2_19.GetRadio()
    this.Parent.oContained.w_PARALIST = this.RadioValue()
    return .t.
  endfunc

  func oPARALIST_2_19.SetRadio()
    this.Parent.oContained.w_PARALIST=trim(this.Parent.oContained.w_PARALIST)
    this.value = ;
      iif(this.Parent.oContained.w_PARALIST=='S',1,;
      0)
  endfunc

  func oPARALIST_2_19.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oPARALIST_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PARALIST<>'S' or (.w_MAGA+.w_CONTA+.w_NOMECLA+.w_VENDI+.w_CLIENTI+.w_FORNI='SSSSSS' AND (.w_ANAL='S' OR .w_FANAL<>'S') AND .w_CATELIST='S'))
    endwith
    return bRes
  endfunc

  add object oPROVVI_2_20 as StdCheck with uid="ZDLZYIQMOU",rtseq=43,rtrep=.f.,left=179, top=132, caption="Provvigioni",;
    HelpContextID = 120454154,;
    cFormVar="w_PROVVI", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare anche gli agenti";
   , bGlobalFont=.t.


  func oPROVVI_2_20.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oPROVVI_2_20.GetRadio()
    this.Parent.oContained.w_PROVVI = this.RadioValue()
    return .t.
  endfunc

  func oPROVVI_2_20.SetRadio()
    this.Parent.oContained.w_PROVVI=trim(this.Parent.oContained.w_PROVVI)
    this.value = ;
      iif(this.Parent.oContained.w_PROVVI=='S',1,;
      0)
  endfunc

  func oPROVVI_2_20.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oPROVVI_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PROVVI<>'S' or (.w_AGENTI='S'))
    endwith
    return bRes
  endfunc

  add object oCONTRA1_2_21 as StdCheck with uid="JDAMPECQHJ",rtseq=44,rtrep=.f.,left=179, top=153, caption="Contratti",;
    HelpContextID = 259002330,;
    cFormVar="w_CONTRA1", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare magazzino, vendite/acquisti, clienti e fornitori o articoli";
   , bGlobalFont=.t.


  func oCONTRA1_2_21.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oCONTRA1_2_21.GetRadio()
    this.Parent.oContained.w_CONTRA1 = this.RadioValue()
    return .t.
  endfunc

  func oCONTRA1_2_21.SetRadio()
    this.Parent.oContained.w_CONTRA1=trim(this.Parent.oContained.w_CONTRA1)
    this.value = ;
      iif(this.Parent.oContained.w_CONTRA1=='S',1,;
      0)
  endfunc

  func oCONTRA1_2_21.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oCONTRA1_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CONTRA1<>'S' or (.w_MAGA+.w_VENDI+.w_CLIENTI+.w_FORNI+.w_ARTICOLI='SSSSS'))
    endwith
    return bRes
  endfunc

  add object ocontro_2_22 as StdCheck with uid="CDKCJTWXZB",rtseq=45,rtrep=.f.,left=179, top=175, caption="Contropartite V/A",;
    HelpContextID = 256765402,;
    cFormVar="w_contro", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare il gruppo contabili e l'archivio conti";
   , bGlobalFont=.t.


  func ocontro_2_22.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func ocontro_2_22.GetRadio()
    this.Parent.oContained.w_contro = this.RadioValue()
    return .t.
  endfunc

  func ocontro_2_22.SetRadio()
    this.Parent.oContained.w_contro=trim(this.Parent.oContained.w_contro)
    this.value = ;
      iif(this.Parent.oContained.w_contro=='S',1,;
      0)
  endfunc

  func ocontro_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_contro<>'S' or (.w_conti='S' and .w_conta='S'))
    endwith
    return bRes
  endfunc

  add object ocauconta_2_23 as StdCheck with uid="MMOYMZOZXO",rtseq=46,rtrep=.f.,left=179, top=195, caption="Causali contabili",;
    HelpContextID = 259093639,;
    cFormVar="w_cauconta", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare il gruppo contabili e l'archivio dei conti";
   , bGlobalFont=.t.


  func ocauconta_2_23.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func ocauconta_2_23.GetRadio()
    this.Parent.oContained.w_cauconta = this.RadioValue()
    return .t.
  endfunc

  func ocauconta_2_23.SetRadio()
    this.Parent.oContained.w_cauconta=trim(this.Parent.oContained.w_cauconta)
    this.value = ;
      iif(this.Parent.oContained.w_cauconta=='S',1,;
      0)
  endfunc

  func ocauconta_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_cauconta<>'S' or (.w_conti='S' and .w_conta='S'))
    endwith
    return bRes
  endfunc

  add object omodconta_2_24 as StdCheck with uid="RBQVBQKTNI",rtseq=47,rtrep=.f.,left=179, top=216, caption="Modelli contabili",;
    HelpContextID = 259027751,;
    cFormVar="w_modconta", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare gli archivi clienti, fornitori e causali contabili";
   , bGlobalFont=.t.


  func omodconta_2_24.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func omodconta_2_24.GetRadio()
    this.Parent.oContained.w_modconta = this.RadioValue()
    return .t.
  endfunc

  func omodconta_2_24.SetRadio()
    this.Parent.oContained.w_modconta=trim(this.Parent.oContained.w_modconta)
    this.value = ;
      iif(this.Parent.oContained.w_modconta=='S',1,;
      0)
  endfunc

  func omodconta_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_modconta<>'S' OR (.w_cauconta='S' AND .w_clienti='S' AND .w_forni='S'))
    endwith
    return bRes
  endfunc

  add object ocaumaga_2_25 as StdCheck with uid="SXONCTHHTQ",rtseq=48,rtrep=.f.,left=179, top=237, caption="Causali magazzino",;
    HelpContextID = 140807130,;
    cFormVar="w_caumaga", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare i gruppi vendite / acquisti e spedizioni";
   , bGlobalFont=.t.


  func ocaumaga_2_25.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func ocaumaga_2_25.GetRadio()
    this.Parent.oContained.w_caumaga = this.RadioValue()
    return .t.
  endfunc

  func ocaumaga_2_25.SetRadio()
    this.Parent.oContained.w_caumaga=trim(this.Parent.oContained.w_caumaga)
    this.value = ;
      iif(this.Parent.oContained.w_caumaga=='S',1,;
      0)
  endfunc

  func ocaumaga_2_25.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func ocaumaga_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_caumaga<>'S' or (.w_vendi='S' and .w_spedi='S'))
    endwith
    return bRes
  endfunc

  add object ocaudocu_2_26 as StdCheck with uid="BKIAQLAWBE",rtseq=49,rtrep=.f.,left=179, top=258, caption="Causali documenti",;
    HelpContextID = 74609702,;
    cFormVar="w_caudocu", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare gli archivi causali contabili, causali magazzino e magazzini";
   , bGlobalFont=.t.


  func ocaudocu_2_26.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func ocaudocu_2_26.GetRadio()
    this.Parent.oContained.w_caudocu = this.RadioValue()
    return .t.
  endfunc

  func ocaudocu_2_26.SetRadio()
    this.Parent.oContained.w_caudocu=trim(this.Parent.oContained.w_caudocu)
    this.value = ;
      iif(this.Parent.oContained.w_caudocu=='S',1,;
      0)
  endfunc

  func ocaudocu_2_26.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func ocaudocu_2_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_caudocu<>'S' or (.w_cauconta='S' and .w_caumaga='S' and .w_maga='S'))
    endwith
    return bRes
  endfunc

  add object oARTICOLI_2_27 as StdCheck with uid="HYIMJZOGSW",rtseq=50,rtrep=.f.,left=179, top=279, caption="Articoli - servizi",;
    ToolTipText = "Articoli servizi e codici di ricerca",;
    HelpContextID = 40545457,;
    cFormVar="w_ARTICOLI", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorrono magazzini, vend./acq., nomenclature, clienti/forrn., contabili analitica, distinta base, funzioni avanzate";
   , bGlobalFont=.t.


  func oARTICOLI_2_27.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oARTICOLI_2_27.GetRadio()
    this.Parent.oContained.w_ARTICOLI = this.RadioValue()
    return .t.
  endfunc

  func oARTICOLI_2_27.SetRadio()
    this.Parent.oContained.w_ARTICOLI=trim(this.Parent.oContained.w_ARTICOLI)
    this.value = ;
      iif(this.Parent.oContained.w_ARTICOLI=='S',1,;
      0)
  endfunc

  func oARTICOLI_2_27.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oARTICOLI_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ARTICOLI<>'S' or ((.w_MAGA+.w_CONTA+.w_NOMECLA+.w_VENDI+.w_CLIENTI+.w_FORNI)='SSSSSS' AND (.w_ANAL='S' OR .w_FANAL<>'S')))
    endwith
    return bRes
  endfunc

  add object oARTMAG_2_28 as StdCheck with uid="WBDYMRCIDV",rtseq=51,rtrep=.f.,left=179, top=300, caption="Dati articoli-mag.",;
    HelpContextID = 176598266,;
    cFormVar="w_ARTMAG", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare magazzini e articoli";
   , bGlobalFont=.t.


  func oARTMAG_2_28.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oARTMAG_2_28.GetRadio()
    this.Parent.oContained.w_ARTMAG = this.RadioValue()
    return .t.
  endfunc

  func oARTMAG_2_28.SetRadio()
    this.Parent.oContained.w_ARTMAG=trim(this.Parent.oContained.w_ARTMAG)
    this.value = ;
      iif(this.Parent.oContained.w_ARTMAG=='S',1,;
      0)
  endfunc

  func oARTMAG_2_28.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  func oARTMAG_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ARTMAG<>'S' or (.w_MAGA='S' AND .w_ARTICOLI='S'))
    endwith
    return bRes
  endfunc

  add object oMATRICOLE_2_29 as StdCheck with uid="KOCUHVYWOE",rtseq=52,rtrep=.f.,left=366, top=67, caption="Codici matricole",;
    ToolTipText = "Codici matricole",;
    HelpContextID = 234993822,;
    cFormVar="w_MATRICOLE", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorrono gli articoli";
   , bGlobalFont=.t.


  func oMATRICOLE_2_29.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oMATRICOLE_2_29.GetRadio()
    this.Parent.oContained.w_MATRICOLE = this.RadioValue()
    return .t.
  endfunc

  func oMATRICOLE_2_29.SetRadio()
    this.Parent.oContained.w_MATRICOLE=trim(this.Parent.oContained.w_MATRICOLE)
    this.value = ;
      iif(this.Parent.oContained.w_MATRICOLE=='S',1,;
      0)
  endfunc

  func oMATRICOLE_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MATR='S')
    endwith
   endif
  endfunc

  func oMATRICOLE_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MATRICOLE<>'S' or (.w_ARTICOLI='S'))
    endwith
    return bRes
  endfunc

  add object oMESSAGGI_2_30 as StdCheck with uid="XXWXRQZUAI",rtseq=53,rtrep=.f.,left=366, top=90, caption="Messaggi",;
    HelpContextID = 92223247,;
    cFormVar="w_MESSAGGI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMESSAGGI_2_30.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMESSAGGI_2_30.GetRadio()
    this.Parent.oContained.w_MESSAGGI = this.RadioValue()
    return .t.
  endfunc

  func oMESSAGGI_2_30.SetRadio()
    this.Parent.oContained.w_MESSAGGI=trim(this.Parent.oContained.w_MESSAGGI)
    this.value = ;
      iif(this.Parent.oContained.w_MESSAGGI=='S',1,;
      0)
  endfunc

  func oMESSAGGI_2_30.mHide()
    with this.Parent.oContained
      return (ISALT())
    endwith
  endfunc

  add object ocaumovcc_2_31 as StdCheck with uid="NCBQFPWJUW",rtseq=54,rtrep=.f.,left=366, top=111, caption="Causali movimenti C\C",;
    HelpContextID = 125531273,;
    cFormVar="w_caumovcc", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorre copiare il gruppo contabili e gli archivi conti, clienti, fornitori e causali contabili";
   , bGlobalFont=.t.


  func ocaumovcc_2_31.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func ocaumovcc_2_31.GetRadio()
    this.Parent.oContained.w_caumovcc = this.RadioValue()
    return .t.
  endfunc

  func ocaumovcc_2_31.SetRadio()
    this.Parent.oContained.w_caumovcc=trim(this.Parent.oContained.w_caumovcc)
    this.value = ;
      iif(this.Parent.oContained.w_caumovcc=='S',1,;
      0)
  endfunc

  func ocaumovcc_2_31.mHide()
    with this.Parent.oContained
      return (g_BANC<>'S' OR ISALT())
    endwith
  endfunc

  func ocaumovcc_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_caumovcc<>'S' or (.w_conti='S' and .w_clienti='S' and .w_forni='S' and .w_conta='S' and .w_cauconta='S'))
    endwith
    return bRes
  endfunc

  add object oMANS_2_32 as StdCheck with uid="ZVEJSQYFLI",rtseq=58,rtrep=.f.,left=366, top=132, caption="Mansioni",;
    ToolTipText = "Mansioni",;
    HelpContextID = 175039174,;
    cFormVar="w_MANS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMANS_2_32.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMANS_2_32.GetRadio()
    this.Parent.oContained.w_MANS = this.RadioValue()
    return .t.
  endfunc

  func oMANS_2_32.SetRadio()
    this.Parent.oContained.w_MANS=trim(this.Parent.oContained.w_MANS)
    this.value = ;
      iif(this.Parent.oContained.w_MANS=='S',1,;
      0)
  endfunc

  add object oTIAT_2_33 as StdCheck with uid="EMWQFXKOAF",rtseq=59,rtrep=.f.,left=366, top=153, caption="Tipologia attivit�",;
    ToolTipText = "Tipologia attivit�",;
    HelpContextID = 175053622,;
    cFormVar="w_TIAT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTIAT_2_33.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTIAT_2_33.GetRadio()
    this.Parent.oContained.w_TIAT = this.RadioValue()
    return .t.
  endfunc

  func oTIAT_2_33.SetRadio()
    this.Parent.oContained.w_TIAT=trim(this.Parent.oContained.w_TIAT)
    this.value = ;
      iif(this.Parent.oContained.w_TIAT=='S',1,;
      0)
  endfunc

  func oTIAT_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTSER='S')
    endwith
   endif
  endfunc

  func oTIAT_2_33.mHide()
    with this.Parent.oContained
      return (.w_ATTSER<>'S')
    endwith
  endfunc

  add object oTPAT_2_34 as StdCheck with uid="UJEKVUPEQM",rtseq=60,rtrep=.f.,left=366, top=175, caption="Tipi attivit�",;
    ToolTipText = "Tipi attivit�",;
    HelpContextID = 175055414,;
    cFormVar="w_TPAT", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorrono le tipologie attivit�,articoli/servizi";
   , bGlobalFont=.t.


  func oTPAT_2_34.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTPAT_2_34.GetRadio()
    this.Parent.oContained.w_TPAT = this.RadioValue()
    return .t.
  endfunc

  func oTPAT_2_34.SetRadio()
    this.Parent.oContained.w_TPAT=trim(this.Parent.oContained.w_TPAT)
    this.value = ;
      iif(this.Parent.oContained.w_TPAT=='S',1,;
      0)
  endfunc

  func oTPAT_2_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTSER='S')
    endwith
   endif
  endfunc

  func oTPAT_2_34.mHide()
    with this.Parent.oContained
      return (.w_ATTSER<>'S')
    endwith
  endfunc

  func oTPAT_2_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TIAT='S' and .w_ARTICOLI='S')
    endwith
    return bRes
  endfunc

  add object oGRCP_2_35 as StdCheck with uid="UBASTDGNHY",rtseq=61,rtrep=.f.,left=366, top=195, caption="Gruppi componenti",;
    ToolTipText = "Gruppi componenti",;
    HelpContextID = 174801766,;
    cFormVar="w_GRCP", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorrono i modelli attributo";
   , bGlobalFont=.t.


  func oGRCP_2_35.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oGRCP_2_35.GetRadio()
    this.Parent.oContained.w_GRCP = this.RadioValue()
    return .t.
  endfunc

  func oGRCP_2_35.SetRadio()
    this.Parent.oContained.w_GRCP=trim(this.Parent.oContained.w_GRCP)
    this.value = ;
      iif(this.Parent.oContained.w_GRCP=='S',1,;
      0)
  endfunc

  func oGRCP_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTSER='S')
    endwith
   endif
  endfunc

  func oGRCP_2_35.mHide()
    with this.Parent.oContained
      return (.w_ATTSER<>'S')
    endwith
  endfunc

  add object oRAAT_2_36 as StdCheck with uid="OZANMZULID",rtseq=62,rtrep=.f.,left=366, top=216, caption="Raggruppamenti attivit�",;
    ToolTipText = "Raggruppamenti attivit�",;
    HelpContextID = 175051542,;
    cFormVar="w_RAAT", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorrono i tipi attivit�";
   , bGlobalFont=.t.


  func oRAAT_2_36.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oRAAT_2_36.GetRadio()
    this.Parent.oContained.w_RAAT = this.RadioValue()
    return .t.
  endfunc

  func oRAAT_2_36.SetRadio()
    this.Parent.oContained.w_RAAT=trim(this.Parent.oContained.w_RAAT)
    this.value = ;
      iif(this.Parent.oContained.w_RAAT=='S',1,;
      0)
  endfunc

  func oRAAT_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTSER='S')
    endwith
   endif
  endfunc

  func oRAAT_2_36.mHide()
    with this.Parent.oContained
      return (.w_ATTSER<>'S')
    endwith
  endfunc

  func oRAAT_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TPAT='S')
    endwith
    return bRes
  endfunc

  add object oDIPE_2_37 as StdCheck with uid="FQDHLXFIRX",rtseq=63,rtrep=.f.,left=366, top=237, caption="Struttura aziendale",;
    ToolTipText = "Struttura aziendale",;
    HelpContextID = 174131766,;
    cFormVar="w_DIPE", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorrono: articoli/servizi,archivi di analitica,reparti,mansioni,conti";
   , bGlobalFont=.t.


  func oDIPE_2_37.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDIPE_2_37.GetRadio()
    this.Parent.oContained.w_DIPE = this.RadioValue()
    return .t.
  endfunc

  func oDIPE_2_37.SetRadio()
    this.Parent.oContained.w_DIPE=trim(this.Parent.oContained.w_DIPE)
    this.value = ;
      iif(this.Parent.oContained.w_DIPE=='S',1,;
      0)
  endfunc

  func oDIPE_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTSER='S')
    endwith
   endif
  endfunc

  func oDIPE_2_37.mHide()
    with this.Parent.oContained
      return (.w_ATTSER<>'S')
    endwith
  endfunc

  func oDIPE_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MANS='S'  and .w_CONTI='S' and  (.w_ANAL='S' OR .w_FANAL='N') AND .w_ARTICOLI='S')
    endwith
    return bRes
  endfunc

  add object oRAPR_2_38 as StdCheck with uid="NWUUZXDVKK",rtseq=64,rtrep=.f.,left=366, top=258, caption="Raggrupp. prestazioni",;
    ToolTipText = "Raggruppamento prestazioni",;
    HelpContextID = 174981910,;
    cFormVar="w_RAPR", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorrono articoli/servizi";
   , bGlobalFont=.t.


  func oRAPR_2_38.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oRAPR_2_38.GetRadio()
    this.Parent.oContained.w_RAPR = this.RadioValue()
    return .t.
  endfunc

  func oRAPR_2_38.SetRadio()
    this.Parent.oContained.w_RAPR=trim(this.Parent.oContained.w_RAPR)
    this.value = ;
      iif(this.Parent.oContained.w_RAPR=='S',1,;
      0)
  endfunc

  func oRAPR_2_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTSER='S')
    endwith
   endif
  endfunc

  func oRAPR_2_38.mHide()
    with this.Parent.oContained
      return (.w_ATTSER<>'S')
    endwith
  endfunc

  func oRAPR_2_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ARTICOLI='S')
    endwith
    return bRes
  endfunc


  add object LblCopiaDa as cp_calclbl with uid="DPGAEQOQAT",left=6, top=5, width=156,height=25,;
    caption='LblCopiaDa',;
   bGlobalFont=.t.,;
    caption="label text",fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,Alignment=1,;
    nPag=2;
    , HelpContextID = 246331049

  add object oMOEL_2_40 as StdCheck with uid="MBIXPEHGHN",rtseq=65,rtrep=.f.,left=366, top=279, caption="Modelli elementi",;
    ToolTipText = "Modelli elementi",;
    HelpContextID = 174547142,;
    cFormVar="w_MOEL", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Occorrono  i listini,le causali documento,i tipi attivit� e la struttura aziendale";
   , bGlobalFont=.t.


  func oMOEL_2_40.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oMOEL_2_40.GetRadio()
    this.Parent.oContained.w_MOEL = this.RadioValue()
    return .t.
  endfunc

  func oMOEL_2_40.SetRadio()
    this.Parent.oContained.w_MOEL=trim(this.Parent.oContained.w_MOEL)
    this.value = ;
      iif(this.Parent.oContained.w_MOEL=='S',1,;
      0)
  endfunc

  func oMOEL_2_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ATTSER='S')
    endwith
   endif
  endfunc

  func oMOEL_2_40.mHide()
    with this.Parent.oContained
      return (.w_ATTSER<>'S')
    endwith
  endfunc

  func oMOEL_2_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_listini='S' and  .w_caudocu ='S' and .w_TPAT='S' and .w_DIPE='S')
    endwith
    return bRes
  endfunc


  add object LblCopiaIn as cp_calclbl with uid="ZXNXBIUTKU",left=226, top=7, width=88,height=25,;
    caption='LblCopiaIn',;
   bGlobalFont=.t.,;
    caption="label text",fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,Alignment=1,;
    nPag=2;
    , HelpContextID = 246327641

  add object oStr_2_13 as StdString with uid="BTIWQZHMWB",Visible=.t., Left=6, Top=47,;
    Alignment=0, Width=172, Height=15,;
    Caption="Seleziona gli archivi"  ;
  , bGlobalFont=.t.
enddefine
define class tgsut_kcaPag3 as StdContainer
  Width  = 540
  height = 334
  stdWidth  = 540
  stdheight = 334
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object olocazi_3_1 as StdField with uid="KQYGISRYCO",rtseq=66,rtrep=.f.,;
    cFormVar = "w_locazi", cQueryName = "locazi",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 81894730,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=164, Top=5, InputMask=replicate('X',5)

  add object onewazi_3_2 as StdField with uid="QEYFUHRPZY",rtseq=67,rtrep=.f.,;
    cFormVar = "w_newazi", cQueryName = "newazi",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 81815338,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=317, Top=5, InputMask=replicate('X',5)


  add object LblCopiaDa as cp_calclbl with uid="WJKNMUNLFC",left=6, top=5, width=156,height=25,;
    caption='LblCopiaDa',;
   bGlobalFont=.t.,;
    caption="label text",fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,Alignment=1,;
    nPag=3;
    , HelpContextID = 246331049


  add object LblCopiaIn as cp_calclbl with uid="VIXRQPGOCD",left=226, top=7, width=88,height=25,;
    caption='LblCopiaIn',;
   bGlobalFont=.t.,;
    caption="label text",fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,Alignment=1,;
    nPag=3;
    , HelpContextID = 246327641

  add object otariffa_3_6 as StdCheck with uid="VWQHPOJJRN",rtseq=68,rtrep=.f.,left=9, top=67, caption="Tariffario",;
    HelpContextID = 152615626,;
    cFormVar="w_tariffa", bObbl = .f. , nPag = 3;
    ,sErrorMsg = "Occorre copiare anche Enti pratica e gli archivi contabili";
   , bGlobalFont=.t.


  func otariffa_3_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func otariffa_3_6.GetRadio()
    this.Parent.oContained.w_tariffa = this.RadioValue()
    return .t.
  endfunc

  func otariffa_3_6.SetRadio()
    this.Parent.oContained.w_tariffa=trim(this.Parent.oContained.w_tariffa)
    this.value = ;
      iif(this.Parent.oContained.w_tariffa=='S',1,;
      0)
  endfunc

  func otariffa_3_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_tariffa<>'S' OR (.w_entipra='S' and .w_CONTA='S'))
    endwith
    return bRes
  endfunc

  add object otipiatt_3_7 as StdCheck with uid="NRRVRJCLPU",rtseq=69,rtrep=.f.,left=9, top=88, caption="Tipi attivit�",;
    HelpContextID = 77016374,;
    cFormVar="w_tipiatt", bObbl = .f. , nPag = 3;
    ,sErrorMsg = "Occorre copiare anche Tariffario";
   , bGlobalFont=.t.


  func otipiatt_3_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func otipiatt_3_7.GetRadio()
    this.Parent.oContained.w_tipiatt = this.RadioValue()
    return .t.
  endfunc

  func otipiatt_3_7.SetRadio()
    this.Parent.oContained.w_tipiatt=trim(this.Parent.oContained.w_tipiatt)
    this.value = ;
      iif(this.Parent.oContained.w_tipiatt=='S',1,;
      0)
  endfunc

  func otipiatt_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_tipiatt<>'S' OR .w_tariffa='S')
    endwith
    return bRes
  endfunc

  add object oraggatt_3_8 as StdCheck with uid="QEURJJWUBF",rtseq=70,rtrep=.f.,left=9, top=109, caption="Raggruppamenti di attivit�",;
    HelpContextID = 76846358,;
    cFormVar="w_raggatt", bObbl = .f. , nPag = 3;
    ,sErrorMsg = "Occorre copiare anche Tariffario e Tipi attivit�";
   , bGlobalFont=.t.


  func oraggatt_3_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oraggatt_3_8.GetRadio()
    this.Parent.oContained.w_raggatt = this.RadioValue()
    return .t.
  endfunc

  func oraggatt_3_8.SetRadio()
    this.Parent.oContained.w_raggatt=trim(this.Parent.oContained.w_raggatt)
    this.value = ;
      iif(this.Parent.oContained.w_raggatt=='S',1,;
      0)
  endfunc

  func oraggatt_3_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_raggatt<>'S' OR (.w_tariffa='S' and .w_tipiatt='S'))
    endwith
    return bRes
  endfunc

  add object oraggpre_3_9 as StdCheck with uid="CUXEQDNSAZ",rtseq=71,rtrep=.f.,left=9, top=130, caption="Raggruppamenti di prestazioni",;
    HelpContextID = 59020566,;
    cFormVar="w_raggpre", bObbl = .f. , nPag = 3;
    ,sErrorMsg = "Occorre copiare anche Tariffario";
   , bGlobalFont=.t.


  func oraggpre_3_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oraggpre_3_9.GetRadio()
    this.Parent.oContained.w_raggpre = this.RadioValue()
    return .t.
  endfunc

  func oraggpre_3_9.SetRadio()
    this.Parent.oContained.w_raggpre=trim(this.Parent.oContained.w_raggpre)
    this.value = ;
      iif(this.Parent.oContained.w_raggpre=='S',1,;
      0)
  endfunc

  func oraggpre_3_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_raggpre<>'S' OR .w_tariffa='S')
    endwith
    return bRes
  endfunc

  add object oraggtipi_3_10 as StdCheck with uid="JMZXHWQHGW",rtseq=72,rtrep=.f.,left=9, top=152, caption="Raggruppamenti tipi file",;
    HelpContextID = 87779969,;
    cFormVar="w_raggtipi", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oraggtipi_3_10.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oraggtipi_3_10.GetRadio()
    this.Parent.oContained.w_raggtipi = this.RadioValue()
    return .t.
  endfunc

  func oraggtipi_3_10.SetRadio()
    this.Parent.oContained.w_raggtipi=trim(this.Parent.oContained.w_raggtipi)
    this.value = ;
      iif(this.Parent.oContained.w_raggtipi=='S',1,;
      0)
  endfunc

  add object ooggepra_3_11 as StdCheck with uid="XHMNDBYYPV",rtseq=73,rtrep=.f.,left=254, top=67, caption="Oggetti pratica",;
    HelpContextID = 58890982,;
    cFormVar="w_oggepra", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ooggepra_3_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func ooggepra_3_11.GetRadio()
    this.Parent.oContained.w_oggepra = this.RadioValue()
    return .t.
  endfunc

  func ooggepra_3_11.SetRadio()
    this.Parent.oContained.w_oggepra=trim(this.Parent.oContained.w_oggepra)
    this.value = ;
      iif(this.Parent.oContained.w_oggepra=='S',1,;
      0)
  endfunc

  add object ostatpra_3_12 as StdCheck with uid="HRBGAVFJGH",rtseq=74,rtrep=.f.,left=254, top=88, caption="Stati pratica",;
    HelpContextID = 59852838,;
    cFormVar="w_statpra", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ostatpra_3_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func ostatpra_3_12.GetRadio()
    this.Parent.oContained.w_statpra = this.RadioValue()
    return .t.
  endfunc

  func ostatpra_3_12.SetRadio()
    this.Parent.oContained.w_statpra=trim(this.Parent.oContained.w_statpra)
    this.value = ;
      iif(this.Parent.oContained.w_statpra=='S',1,;
      0)
  endfunc

  add object oubicpra_3_13 as StdCheck with uid="QXUJEQIDYE",rtseq=75,rtrep=.f.,left=254, top=109, caption="Ubicazioni pratica",;
    HelpContextID = 58766918,;
    cFormVar="w_ubicpra", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oubicpra_3_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oubicpra_3_13.GetRadio()
    this.Parent.oContained.w_ubicpra = this.RadioValue()
    return .t.
  endfunc

  func oubicpra_3_13.SetRadio()
    this.Parent.oContained.w_ubicpra=trim(this.Parent.oContained.w_ubicpra)
    this.value = ;
      iif(this.Parent.oContained.w_ubicpra=='S',1,;
      0)
  endfunc

  add object otipipra_3_14 as StdCheck with uid="ZCJPNWIGIW",rtseq=76,rtrep=.f.,left=254, top=130, caption="Tipi pratica",;
    HelpContextID = 59190582,;
    cFormVar="w_tipipra", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func otipipra_3_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func otipipra_3_14.GetRadio()
    this.Parent.oContained.w_tipipra = this.RadioValue()
    return .t.
  endfunc

  func otipipra_3_14.SetRadio()
    this.Parent.oContained.w_tipipra=trim(this.Parent.oContained.w_tipipra)
    this.value = ;
      iif(this.Parent.oContained.w_tipipra=='S',1,;
      0)
  endfunc

  add object omatepra_3_15 as StdCheck with uid="EPGWZVCWYY",rtseq=77,rtrep=.f.,left=254, top=151, caption="Materie pratica",;
    HelpContextID = 58942662,;
    cFormVar="w_matepra", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func omatepra_3_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func omatepra_3_15.GetRadio()
    this.Parent.oContained.w_matepra = this.RadioValue()
    return .t.
  endfunc

  func omatepra_3_15.SetRadio()
    this.Parent.oContained.w_matepra=trim(this.Parent.oContained.w_matepra)
    this.value = ;
      iif(this.Parent.oContained.w_matepra=='S',1,;
      0)
  endfunc

  add object oentipra_3_16 as StdCheck with uid="BDTIGLWULA",rtseq=78,rtrep=.f.,left=254, top=172, caption="Enti pratica",;
    HelpContextID = 59208006,;
    cFormVar="w_entipra", bObbl = .f. , nPag = 3;
    ,sErrorMsg = "Per copiare il Tariffario occorre Enti pratica";
   , bGlobalFont=.t.


  func oentipra_3_16.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oentipra_3_16.GetRadio()
    this.Parent.oContained.w_entipra = this.RadioValue()
    return .t.
  endfunc

  func oentipra_3_16.SetRadio()
    this.Parent.oContained.w_entipra=trim(this.Parent.oContained.w_entipra)
    this.value = ;
      iif(this.Parent.oContained.w_entipra=='S',1,;
      0)
  endfunc

  func oentipra_3_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_entipra='S' OR .w_tariffa<>'S')
    endwith
    return bRes
  endfunc

  add object ouffipra_3_17 as StdCheck with uid="FJXBIZCVWN",rtseq=79,rtrep=.f.,left=254, top=193, caption="Uffici pratica",;
    HelpContextID = 59148870,;
    cFormVar="w_uffipra", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func ouffipra_3_17.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func ouffipra_3_17.GetRadio()
    this.Parent.oContained.w_uffipra = this.RadioValue()
    return .t.
  endfunc

  func ouffipra_3_17.SetRadio()
    this.Parent.oContained.w_uffipra=trim(this.Parent.oContained.w_uffipra)
    this.value = ;
      iif(this.Parent.oContained.w_uffipra=='S',1,;
      0)
  endfunc

  add object oclasspra_3_18 as StdCheck with uid="RNZFNZSRMH",rtseq=80,rtrep=.f.,left=254, top=214, caption="Classificazioni pratica",;
    HelpContextID = 29376391,;
    cFormVar="w_classpra", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oclasspra_3_18.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oclasspra_3_18.GetRadio()
    this.Parent.oContained.w_classpra = this.RadioValue()
    return .t.
  endfunc

  func oclasspra_3_18.SetRadio()
    this.Parent.oContained.w_classpra=trim(this.Parent.oContained.w_classpra)
    this.value = ;
      iif(this.Parent.oContained.w_classpra=='S',1,;
      0)
  endfunc

  add object oStr_3_5 as StdString with uid="JWGSHRWZSN",Visible=.t., Left=6, Top=47,;
    Alignment=0, Width=172, Height=15,;
    Caption="Seleziona gli archivi"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kca','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
