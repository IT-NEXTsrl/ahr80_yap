* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bvb                                                        *
*              Controlli versione blackbox                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-11                                                      *
* Last revis.: 2016-06-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bvb",oParentObject)
return(i_retval)

define class tgsut_bvb as StdBatch
  * --- Local variables
  w_VERSIONE = 0
  w_oMESS = .NULL.
  w_oPART = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato nel cp3start esegue controlli congruenza blackbox
    this.w_VERSIONE = 190
     
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 Getblackbversion() 
 on error &L_OldError 
    if L_Err
      AH_ERRORMSG( "Attenzione, livello patch incongruente" ,64)
      i_retcode = 'stop'
      i_retval = .F.
      return
    endif
    if Type("BBAPPLICA") <>"C" OR g_APPLICATION<>BBAPPLICA
      AH_ERRORMSG("Attenzione, applicativo livello patch incongruente" ,64)
      i_retcode = 'stop'
      i_retval = .F.
      return
    endif
    if Type("BBRELEASE") ="U" OR g_VERSION<>BBRELEASE
      ah_ERRORMSG("Attenzione, release livello patch incongruente" ,64)
      i_retcode = 'stop'
      i_retval = .F.
      return
    endif
    if this.w_VERSIONE>BBVERSION
      this.w_oMESS=createobject("AH_Message")
      this.w_oMESS.AddMsgPartNL("Attenzione%0La sequenza di installazione delle patch ha provocato un disallineamento della versione del programma di controllo licenza.")     
      this.w_oPART = this.w_oMESS.AddMsgPartNL("Per un completo e corretto funzionamento della procedura, � richiesta l'installazione della patch n.%1, installabile senza prerequisiti.%0Uscire ora dalla procedura ?")
      this.w_oPART.AddParam(Alltrim(str(this.w_VERSIONE)))     
      if !this.w_oMESS.AH_yesno()
        i_retcode = 'stop'
        i_retval = .T.
        return
      else
        i_retcode = 'stop'
        i_retval = .F.
        return
      endif
    else
      i_retcode = 'stop'
      i_retval = .T.
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
