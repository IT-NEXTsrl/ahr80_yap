* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bcv                                                        *
*              Valorizzo totale righe CVS be                                   *
*                                                                              *
*      Author: Zucchetti                                                       *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-28                                                      *
* Last revis.: 2008-06-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPar
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bcv",oParentObject,m.pPar)
return(i_retval)

define class tgste_bcv as StdBatch
  * --- Local variables
  pPar = space(3)
  w_PADRE = .NULL.
  w_TROVATO = .f.
  COMBO = .NULL.
  w_RECO = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- verifico inizializzazione totale righe
    this.w_TROVATO = .F.
    do case
      case this.pPar="IMP"
        Select (this.oParentObject.cTrsName)
        this.w_RECO = recno()
        Select (this.oParentObject.cTrsName) 
 CALCULATE SUM(t_DIIMPORT) TO this.oParentObject.w_TOTRIG 
 this.oParentObject.SetControlsValue()
        Select (this.oParentObject.cTrsName)
        GOTO this.w_RECO
      case this.pPar="TIP"
        * --- Verifica se esistono i record di tipo CVS='REG'
        *     Se esistono record di tale tipo allora si bisogna aggiungere una voce alla combo DITIPCVS poich� � un tipo vecchio e non pi� gestito ma che si vuole ncora vedere per i vecchi record
        this.w_PADRE = this.oparentobject
        this.w_PADRE.MARKPOS()     
        this.w_PADRE.FirstRow()     
        do while NOT this.w_PADRE.EOF_TRS()
          this.w_PADRE.SetRow()     
          if this.w_PADRE.FullRow()
            if this.oParentObject.w_DITIPCVS ="REG"
              * --- Trovato un record con tipo CVS='REG'  
              this.oParentObject.w_COMBOVECCHIA = .T.
              exit
            endif
          endif
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.REPOS()     
    endcase
  endproc


  proc Init(oParentObject,pPar)
    this.pPar=pPar
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPar"
endproc
