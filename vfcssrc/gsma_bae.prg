* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bae                                                        *
*              Cancella allegati collegati                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-03-06                                                      *
* Last revis.: 2012-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_DELGI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bae",oParentObject,m.w_DELGI)
return(i_retval)

define class tgsma_bae as StdBatch
  * --- Local variables
  w_DELGI = space(1)
  w_IDVALATT = space(20)
  * --- WorkFile variables
  PRODINDI_idx=0
  PROMINDI_idx=0
  TMPPROV_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancella allegati collegati
    *     w_DELGI=G Cerca allegati collegati nella gestione da cancellare
    *     w_DELGI=I Cancella allegati collegati se � stato richiesto
    do case
      case this.w_DELGI="G"
        * --- Read from PRODINDI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IDVALATT"+;
            " from "+i_cTable+" PRODINDI where ";
                +"IDTABKEY = "+cp_ToStrODBC(this.oParentObject.w_TABKEY);
                +" and IDVALATT = "+cp_ToStrODBC(this.oParentObject.w_VALATT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IDVALATT;
            from (i_cTable) where;
                IDTABKEY = this.oParentObject.w_TABKEY;
                and IDVALATT = this.oParentObject.w_VALATT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_IDVALATT = NVL(cp_ToDate(_read_.IDVALATT),cp_NullValue(_read_.IDVALATT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(NVL(this.w_IDVALATT,""))
          i_retcode = 'stop'
          i_retval = .T.
          return
        else
          if Ah_yesno("Esiste almeno un indice allegato collegato, si vuole procedere con la cancellazione?")
            this.oParentObject.w_DELIMM = Ah_yesno("Si vogliono eliminare anche gli indici allegati non bloccati?")
            i_retcode = 'stop'
            i_retval = .T.
            return
          else
            i_retcode = 'stop'
            i_retval = .F.
            return
          endif
        endif
      case this.w_DELGI="I"
        if this.oParentObject.w_DELIMM
          * --- Create temporary table TMPPROV
          i_nIdx=cp_AddTableDef('TMPPROV') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('gsma_bae',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPPROV_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Delete from PRODINDI
          i_nConn=i_TableProp[this.PRODINDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex
            declare i_aIndex[1]
            i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPPROV_idx,2])
            i_cTempTable=cp_GetTempTableName(i_nConn)
            i_aIndex[1]='IDSERIAL'
            cp_CreateTempTable(i_nConn,i_cTempTable,"IDSERIAL "," from "+i_cQueryTable+" where 1=1",.f.,@i_aIndex)
            i_cQueryTable=i_cTempTable
            i_cWhere=i_cTable+".IDSERIAL = "+i_cQueryTable+".IDSERIAL";
          
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where ";
                  +""+i_cTable+".IDSERIAL = "+i_cQueryTable+".IDSERIAL";
                  +")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Delete from PROMINDI
          i_nConn=i_TableProp[this.PROMINDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex
            declare i_aIndex[1]
            i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPPROV_idx,2])
            i_cTempTable=cp_GetTempTableName(i_nConn)
            i_aIndex[1]='IDSERIAL'
            cp_CreateTempTable(i_nConn,i_cTempTable,"IDSERIAL "," from "+i_cQueryTable+" where 1=1",.f.,@i_aIndex)
            i_cQueryTable=i_cTempTable
            i_cWhere=i_cTable+".IDSERIAL = "+i_cQueryTable+".IDSERIAL";
          
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where ";
                  +""+i_cTable+".IDSERIAL = "+i_cQueryTable+".IDSERIAL";
                  +")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Drop temporary table TMPPROV
          i_nIdx=cp_GetTableDefIdx('TMPPROV')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPPROV')
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,w_DELGI)
    this.w_DELGI=w_DELGI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PRODINDI'
    this.cWorkTables[2]='PROMINDI'
    this.cWorkTables[3]='*TMPPROV'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_DELGI"
endproc
