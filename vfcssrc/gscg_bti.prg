* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bti                                                        *
*              Legge dati per trasm. IVA                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_703]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-08-10                                                      *
* Last revis.: 2000-08-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bti",oParentObject)
return(i_retval)

define class tgscg_bti as StdBatch
  * --- Local variables
  w_CODAZI = space(5)
  w_AZCOFFOR = space(16)
  w_AZFISFOR = space(1)
  w_AZCOGFOR = space(24)
  w_AZNOMFOR = space(20)
  w_AZSEXFOR = space(1)
  w_AZDATFOR = ctod("  /  /  ")
  w_AZCONFOR = space(40)
  w_AZPRNFOR = space(2)
  w_AZCOMFOR = space(40)
  w_AZPROFOR = space(2)
  w_AZINDFOR = space(35)
  w_AZCAPFOR = space(5)
  w_AZDENFOR = space(60)
  w_AZCOLFOR = space(40)
  w_AZPRLFOR = space(2)
  w_AZINLFOR = space(35)
  w_AZCALFOR = space(5)
  w_AZTIPDEN = space(1)
  w_AZNUMCAF = space(5)
  w_AZRAGAZI = space(40)
  w_AZPERAZI = space(1)
  w_AZCOFAZI = space(16)
  w_AZPIVAZI = space(12)
  w_TTCOGTIT = space(24)
  w_TTNOMTIT = space(20)
  w_VPVARIMP = space(1)
  w_VPCORTER = space(1)
  w_VPACQBEA = space(1)
  w_VPDICGRU = space(1)
  w_VPDICSOC = space(1)
  w_VPCODVAL = space(1)
  w_VPIMPOR1 = 0
  w_VPCESINT = 0
  w_VPIMPOR2 = 0
  w_VPACQINT = 0
  w_VPIMPON3 = 0
  w_VPIMPOS3 = 0
  w_VPIMPOR5 = 0
  w_VPIMPOR6 = 0
  w_VPIMPOR7 = 0
  w_VPIMPOR8 = 0
  w_VPIMPOR9 = 0
  w_VPIMPO10 = 0
  w_VPIMPO11 = 0
  w_VPIMPO12 = 0
  w_VPIMPO121 = 0
  w_VPIMPO13 = 0
  w_VPIMPO14 = 0
  w_VPIMPO15 = 0
  w_VPIMPO16 = 0
  w_VPIMPO17 = 0
  w_VPCODAZI = space(5)
  w_VPCODCAB = space(5)
  w_VPVERNEF = space(1)
  w_VPAR74C5 = space(1)
  w_VPAR74C4 = space(1)
  w_VPOPMEDI = space(1)
  w_VPOPNOIM = space(1)
  w_VPCRERIM = 0
  w_VPEURO19 = space(1)
  w_VPCREUTI = 0
  w_CODFISDI = space(16)
  w_CODCARIC = space(2)
  w_VERCAS17 = space(1)
  w_VPDATVER = ctod("  /  /  ")
  w_IDPRODSW = space(16)
  w_CDATE = space(8)
  w_LTmp = .f.
  w_DATINO = ctod("  /  /  ")
  w_CONTRO = 0
  w_nErrFile = 0
  w_MESS = space(100)
  w_MEMOREC = space(0)
  w_OK = .f.
  * --- WorkFile variables
  AZIENDA_idx=0
  IVA_PERI_idx=0
  TITOLARI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch legge i dati relativi all'iva per poi poter scrivere il file ascii. (da GSCG_KTI)
    * --- Variabili Dati del Fornitore
    * --- Dati del Contribuente
    * --- variabili locali per la lettura dei dati dell'IVA periodica IVA_PERI per il rigo VP17
    * --- Variabili Locali
    * --- Controllo se nella dichiarazione periodica � presente la data di inoltro telematico
    if  empty(this.oParentObject.w_TIPFORN) 
      ah_ErrorMsg("Tipo fornitore non definito","!","")
      i_retcode = 'stop'
      return
    endif
    if  empty(this.oParentObject.w_DATASTAM) 
      ah_ErrorMsg("Data di inoltro non definita","!","")
      i_retcode = 'stop'
      return
    endif
    if  empty(this.oParentObject.w_PERIOD) 
      ah_ErrorMsg("Periodo non valorizzato","!","")
      i_retcode = 'stop'
      return
    endif
    if  empty(this.oParentObject.w_ANNO) 
      ah_ErrorMsg("Anno non valorizzato","!","")
      i_retcode = 'stop'
      return
    endif
    if  empty(this.oParentObject.w_NOMEFILE)
      ah_ErrorMsg("Nome file non indicato","!","")
      i_retcode = 'stop'
      return
    endif
    * --- leggo il codice dell'azienda
    this.w_CODAZI = i_CODAZI
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCOFFOR,AZFISFOR,AZCOGFOR,AZNOMFOR,AZSEXFOR,AZDATFOR,AZCONFOR,AZPRNFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR,AZDENFOR,AZCOLFOR,AZPRLFOR,AZINLFOR,AZCALFOR,AZTIPDEN,AZNUMCAF"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCOFFOR,AZFISFOR,AZCOGFOR,AZNOMFOR,AZSEXFOR,AZDATFOR,AZCONFOR,AZPRNFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR,AZDENFOR,AZCOLFOR,AZPRLFOR,AZINLFOR,AZCALFOR,AZTIPDEN,AZNUMCAF;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZCOFFOR = NVL(cp_ToDate(_read_.AZCOFFOR),cp_NullValue(_read_.AZCOFFOR))
      this.w_AZFISFOR = NVL(cp_ToDate(_read_.AZFISFOR),cp_NullValue(_read_.AZFISFOR))
      this.w_AZCOGFOR = NVL(cp_ToDate(_read_.AZCOGFOR),cp_NullValue(_read_.AZCOGFOR))
      this.w_AZNOMFOR = NVL(cp_ToDate(_read_.AZNOMFOR),cp_NullValue(_read_.AZNOMFOR))
      this.w_AZSEXFOR = NVL(cp_ToDate(_read_.AZSEXFOR),cp_NullValue(_read_.AZSEXFOR))
      this.w_AZDATFOR = NVL(cp_ToDate(_read_.AZDATFOR),cp_NullValue(_read_.AZDATFOR))
      this.w_AZCONFOR = NVL(cp_ToDate(_read_.AZCONFOR),cp_NullValue(_read_.AZCONFOR))
      this.w_AZPRNFOR = NVL(cp_ToDate(_read_.AZPRNFOR),cp_NullValue(_read_.AZPRNFOR))
      this.w_AZCOMFOR = NVL(cp_ToDate(_read_.AZCOMFOR),cp_NullValue(_read_.AZCOMFOR))
      this.w_AZPROFOR = NVL(cp_ToDate(_read_.AZPROFOR),cp_NullValue(_read_.AZPROFOR))
      this.w_AZINDFOR = NVL(cp_ToDate(_read_.AZINDFOR),cp_NullValue(_read_.AZINDFOR))
      this.w_AZCAPFOR = NVL(cp_ToDate(_read_.AZCAPFOR),cp_NullValue(_read_.AZCAPFOR))
      this.w_AZDENFOR = NVL(cp_ToDate(_read_.AZDENFOR),cp_NullValue(_read_.AZDENFOR))
      this.w_AZCOLFOR = NVL(cp_ToDate(_read_.AZCOLFOR),cp_NullValue(_read_.AZCOLFOR))
      this.w_AZPRLFOR = NVL(cp_ToDate(_read_.AZPRLFOR),cp_NullValue(_read_.AZPRLFOR))
      this.w_AZINLFOR = NVL(cp_ToDate(_read_.AZINLFOR),cp_NullValue(_read_.AZINLFOR))
      this.w_AZCALFOR = NVL(cp_ToDate(_read_.AZCALFOR),cp_NullValue(_read_.AZCALFOR))
      this.w_AZTIPDEN = NVL(cp_ToDate(_read_.AZTIPDEN),cp_NullValue(_read_.AZTIPDEN))
      this.w_AZNUMCAF = NVL(cp_ToDate(_read_.AZNUMCAF),cp_NullValue(_read_.AZNUMCAF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZRAGAZI,AZPERAZI,AZCOFAZI,AZPIVAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZRAGAZI,AZPERAZI,AZCOFAZI,AZPIVAZI;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZRAGAZI = NVL(cp_ToDate(_read_.AZRAGAZI),cp_NullValue(_read_.AZRAGAZI))
      this.w_AZPERAZI = NVL(cp_ToDate(_read_.AZPERAZI),cp_NullValue(_read_.AZPERAZI))
      this.w_AZCOFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      this.w_AZPIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(this.w_AZCOFFOR)
      ah_ErrorMsg("Codice fiscale fornitore non definito","!","")
      i_retcode = 'stop'
      return
    endif
    if empty(this.w_AZCOFAZI)
      ah_ErrorMsg("Codice fiscale contribuente non definito","!","")
      i_retcode = 'stop'
      return
    endif
    if empty(this.w_AZPIVAZI)
      ah_ErrorMsg("Partita IVA contribuente non definita","!","")
      i_retcode = 'stop'
      return
    endif
    this.w_TTCOGTIT = SPACE(24)
    this.w_TTNOMTIT = SPACE(20)
    if this.w_AZPERAZI="S"
      * --- Se persona Fisica
      this.w_AZRAGAZI = SPACE(60)
      * --- Read from TITOLARI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TITOLARI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TITOLARI_idx,2],.t.,this.TITOLARI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TTCOGTIT,TTNOMTIT"+;
          " from "+i_cTable+" TITOLARI where ";
              +"TTCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TTCOGTIT,TTNOMTIT;
          from (i_cTable) where;
              TTCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TTCOGTIT = NVL(cp_ToDate(_read_.TTCOGTIT),cp_NullValue(_read_.TTCOGTIT))
        this.w_TTNOMTIT = NVL(cp_ToDate(_read_.TTNOMTIT),cp_NullValue(_read_.TTNOMTIT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Read from IVA_PERI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.IVA_PERI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2],.t.,this.IVA_PERI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" IVA_PERI where ";
            +"VP__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
            +" and VPPERIOD = "+cp_ToStrODBC(this.oParentObject.w_PERIOD);
            +" and VPKEYATT = "+cp_ToStrODBC(this.oParentObject.w_KEYATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            VP__ANNO = this.oParentObject.w_ANNO;
            and VPPERIOD = this.oParentObject.w_PERIOD;
            and VPKEYATT = this.oParentObject.w_KEYATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_VPACQINT = NVL(cp_ToDate(_read_.VPACQINT),cp_NullValue(_read_.VPACQINT))
      this.w_VPAR74C4 = NVL(cp_ToDate(_read_.VPAR74C4),cp_NullValue(_read_.VPAR74C4))
      this.w_VPAR74C5 = NVL(cp_ToDate(_read_.VPAR74C5),cp_NullValue(_read_.VPAR74C5))
      this.w_VPCESINT = NVL(cp_ToDate(_read_.VPCESINT),cp_NullValue(_read_.VPCESINT))
      this.w_VPCODAZI = NVL(cp_ToDate(_read_.VPCODAZI),cp_NullValue(_read_.VPCODAZI))
      this.w_VPCODCAB = NVL(cp_ToDate(_read_.VPCODCAB),cp_NullValue(_read_.VPCODCAB))
      this.w_CODCARIC = NVL(cp_ToDate(_read_.VPCODCAR),cp_NullValue(_read_.VPCODCAR))
      this.w_CODFISDI = NVL(cp_ToDate(_read_.VPCODFIS),cp_NullValue(_read_.VPCODFIS))
      this.w_VPCODVAL = NVL(cp_ToDate(_read_.VPCODVAL),cp_NullValue(_read_.VPCODVAL))
      this.w_VPCRERIM = NVL(cp_ToDate(_read_.VPCRERIM),cp_NullValue(_read_.VPCRERIM))
      this.w_VPCREUTI = NVL(cp_ToDate(_read_.VPCREUTI),cp_NullValue(_read_.VPCREUTI))
      this.w_VPDATVER = NVL(cp_ToDate(_read_.VPDATVER),cp_NullValue(_read_.VPDATVER))
      this.w_VPDICGRU = NVL(cp_ToDate(_read_.VPDICGRU),cp_NullValue(_read_.VPDICGRU))
      this.w_VPDICSOC = NVL(cp_ToDate(_read_.VPDICSOC),cp_NullValue(_read_.VPDICSOC))
      this.w_VPEURO19 = NVL(cp_ToDate(_read_.VPEURO19),cp_NullValue(_read_.VPEURO19))
      this.w_VPIMPO10 = NVL(cp_ToDate(_read_.VPIMPO10),cp_NullValue(_read_.VPIMPO10))
      this.w_VPIMPO11 = NVL(cp_ToDate(_read_.VPIMPO11),cp_NullValue(_read_.VPIMPO11))
      this.w_VPIMPO12 = NVL(cp_ToDate(_read_.VPIMPO12),cp_NullValue(_read_.VPIMPO12))
      this.w_VPIMPO13 = NVL(cp_ToDate(_read_.VPIMPO13),cp_NullValue(_read_.VPIMPO13))
      this.w_VPIMPO14 = NVL(cp_ToDate(_read_.VPIMPO14),cp_NullValue(_read_.VPIMPO14))
      this.w_VPIMPO15 = NVL(cp_ToDate(_read_.VPIMPO15),cp_NullValue(_read_.VPIMPO15))
      this.w_VPIMPO16 = NVL(cp_ToDate(_read_.VPIMPO16),cp_NullValue(_read_.VPIMPO16))
      this.w_VPIMPON3 = NVL(cp_ToDate(_read_.VPIMPON3),cp_NullValue(_read_.VPIMPON3))
      this.w_VPIMPOR1 = NVL(cp_ToDate(_read_.VPIMPOR1),cp_NullValue(_read_.VPIMPOR1))
      this.w_VPIMPOR2 = NVL(cp_ToDate(_read_.VPIMPOR2),cp_NullValue(_read_.VPIMPOR2))
      this.w_VPIMPOR5 = NVL(cp_ToDate(_read_.VPIMPOR5),cp_NullValue(_read_.VPIMPOR5))
      this.w_VPIMPOR6 = NVL(cp_ToDate(_read_.VPIMPOR6),cp_NullValue(_read_.VPIMPOR6))
      this.w_VPIMPOR7 = NVL(cp_ToDate(_read_.VPIMPOR7),cp_NullValue(_read_.VPIMPOR7))
      this.w_VPIMPOR8 = NVL(cp_ToDate(_read_.VPIMPOR8),cp_NullValue(_read_.VPIMPOR8))
      this.w_VPIMPOR9 = NVL(cp_ToDate(_read_.VPIMPOR9),cp_NullValue(_read_.VPIMPOR9))
      this.w_VPIMPOS3 = NVL(cp_ToDate(_read_.VPIMPOS3),cp_NullValue(_read_.VPIMPOS3))
      this.w_VPIMPO17 = NVL(cp_ToDate(_read_.VPIMPVER),cp_NullValue(_read_.VPIMPVER))
      this.w_VPOPMEDI = NVL(cp_ToDate(_read_.VPOPMEDI),cp_NullValue(_read_.VPOPMEDI))
      this.w_VPOPNOIM = NVL(cp_ToDate(_read_.VPOPNOIM),cp_NullValue(_read_.VPOPNOIM))
      this.w_VPVARIMP = NVL(cp_ToDate(_read_.VPVARIMP),cp_NullValue(_read_.VPVARIMP))
      this.w_VERCAS17 = NVL(cp_ToDate(_read_.VPVEREUR),cp_NullValue(_read_.VPVEREUR))
      this.w_VPVERNEF = NVL(cp_ToDate(_read_.VPVERNEF),cp_NullValue(_read_.VPVERNEF))
      this.w_DATINO = NVL(cp_ToDate(_read_.VPDATINO),cp_NullValue(_read_.VPDATINO))
      this.w_VPCORTER = NVL(cp_ToDate(_read_.VPCORTER),cp_NullValue(_read_.VPCORTER))
      this.w_VPACQBEA = NVL(cp_ToDate(_read_.VPACQBEA),cp_NullValue(_read_.VPACQBEA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_VPIMPO17 > 0 AND (EMPTY(this.w_VPDATVER) OR EMPTY(this.w_VPCODAZI) OR EMPTY(this.w_VPCODCAB))
      ah_ErrorMsg("Estremi del versamento incompleti","!","")
      i_retcode = 'stop'
      return
    endif
    if NOT EMPTY(CP_TODATE(this.w_DATINO))
      this.w_OK = ah_YesNo("Dichiarazione periodica gi� inoltrata%0Confermi ugualmente l'elaborazione?")
      if NOT this.w_OK
        i_retcode = 'stop'
        return
      endif
    endif
    Create Cursor tip_rec_a ;
    ( TIPREC C(1), PRIMOFILL C(14), CODFORN C(5), TIPFORN C(2), CODFISF C(16), ;
    TTCOGTIT C(24), TTNOMTIT C(20), TT_SESSO C(1), TTDATNAS C(8), TTLUONAS C(40), TTPRONAS C(2), TTLOCTIT C(40), ;
    TTPROTIT C(2), TTINDIRI C(35), TTCAPTIT C(5), AZRAGAZI C(60), AZLOCAZI C(40), AZPROAZI C(2), AZINDAZI C(35), AZCAPAZI C(8), ;
    SELOCALI C(40), SEPROVIN C(2), SEINDIRI C(35), SE__CAP C(8), CAFLOCAL C(40), CAFPROV C(2), CAFINDIR C(35), CAF__CAP C(8), ;
    CAFFILL C(6), CAMPOUT C(100), FILLER301 C(254), FILLER302 C(254), FILLER303 C(254), FILLER304 C(254), FILLER305 C(54),;
    SPAZRIS C(200), CONTR32 C(1), CONTR33 C(2) )
    * --- scrittura record di tipo A
    SELECT tip_rec_a
    GO TOP
    APPEND BLANK
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    CREATE CURSOR tip_rec_b ;
    ( TIPREC C(1), AZCOFAZI C(16), FILLER3 C(8), SPAZIO4 C(3), FILLER25 C(25), SPAZIO8 C(20), ;
    IDPRODSW C(16), FLAGCONF C(1), IDSERTEL C(17), PROGRDIC C(6), AZRAGAZI C(60), TTCOGTIT C(24), TTNOMTIT C(20), AZPIVAZI C(11), ;
    DI_ANNO C(4), DIPERIOD C(2), DITRIM C(1), IMPEURO C(1), IMPLIRE C(1), ;
    VPVARIMP C(1), VPCORTER C(1), DICINTEG C(1), VPDICGRU C(1), VPDICSOC C(1), ;
    VPIMPOR1 C(13), VPCESINT C(13), VPIMPOR2 C(13), VPACQINT C(13), VPIMPON3 C(13), VPIMPOS3 C(13), VPIMPOR5 C(13), ;
    VPIMPOR6 C(13), VPIMPOR7 C(13), VPIMPOR71 C(13), VPIMPOR8 C(13), VPIMPOR81 C(13), VPIMPOR9 C(13), VPIMPOR91 C(13), ;
    VPIMPO10 C(13), VPIMPO101 C(13),VPIMPO11 C(13),VPIMPO12 C(13), VPIMPO121 C(13), VPIMPO13 C(13), ;
    VPIMPO14 C(13), VPIMPO15 C(13),VPIMPO16 C(13), ;
    VPIMPO17 C(13), VERCAS17 C(1), VPDATVER C(8), VPCODAZI C(5), VPCODCAB C(5), VPVERNEF C(1), VPAR74C5 C(1), VPAR74C4 C(1), ;
    VPOPMEDI C(1), VPOPNOIM C(1), VPACQBEA C(1), VPCRERIM C(13), VPEURO19 C(1), VPCREUTI C(13), ;
    FIRMA C(1), VPCODCAR C(2), VPCODFIS C(16), ;
    CAMPO65 C(16), CAMPO66 C(5), CAMPO67 C(8), CAMPO68 C(1), CAMPO69 C(1), CAMPO70 C(1), ;
    CAMPO71 C(16), CAMPO72 C(1), CAMPO73 C(1), ;
    FILLER74A C(250), FILLER74B C(250), FILLER74C C(250), FILLER74D C(250), FILLER74E C(168), ;
    CAMPO75 C(20), CAMPO76 C(8), CAMPO77 C(8), CAMPO78 C(1), CAMPO79 C(17), CAMPO80 C(1), CAMPO81 C(2))
    * --- scrittura record di tipo B campi 1.. 81
    SELECT tip_rec_b
    GO TOP
    APPEND BLANK
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    CREATE CURSOR tip_rec_z ;
    (TIPREC C(1), FILLER2 C(14), NUMRECB C(9), FILLER41 C(254), FILLER42 C(254), FILLER43 C(254), FILLER44 C(254), FILLER45 C(254),;
    FILLER46 C(254), FILLER47 C(254), FILLER48 C(95), FILLER5 C(1), FILLER6 C(2))
    * --- scrittura record di tipo Z
    SELECT tip_rec_z
    GO TOP
    APPEND BLANK
    Replace TIPREC with "Z"
    Replace FILLER2 with SPACE(14)
    Replace NUMRECB with "000000001"
    Replace FILLER41 with SPACE(254)
    Replace FILLER42 with SPACE(254)
    Replace FILLER43 with SPACE(254)
    Replace FILLER44 with SPACE(254)
    Replace FILLER45 with SPACE(254)
    Replace FILLER46 with SPACE(254)
    Replace FILLER47 with SPACE(254)
    Replace FILLER48 with SPACE(95)
    Replace FILLER5 with "A"
    Replace FILLER6 with CHR(13)+CHR(10)
    this.Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura Record Tipo A
    Replace TIPREC with "A"
    Replace PRIMOFILL with Space(14)
    Replace CODFORN with "IVP01"
    Replace TIPFORN with RIGHT("00"+ALLTRIM(this.oParentObject.w_TIPFORN), 2)
    Replace CODFISF with LEFT(ALLTRIM(UPPER(this.w_AZCOFFOR))+SPACE(16), 16)
    Replace TTCOGTIT with Space(24)
    Replace TTNOMTIT with Space(20)
    Replace TT_SESSO with Space(1)
    Replace TTDATNAS with "00000000"
    Replace TTLUONAS with Space(40)
    Replace TTPRONAS with Space(2)
    Replace TTLOCTIT with Space(40)
    Replace TTPROTIT with Space(2)
    Replace TTINDIRI with Space(35)
    Replace TTCAPTIT with "00000000"
    Replace AZRAGAZI with SPACE(60)
    Replace AZLOCAZI with SPACE(40)
    Replace AZPROAZI with SPACE(2)
    Replace AZINDAZI with SPACE(35)
    Replace AZCAPAZI with "00000000"
    Replace SELOCALI with SPACE(40)
    Replace SEPROVIN with SPACE(2)
    Replace SEINDIRI with SPACE(35)
    Replace SE__CAP with "00000000"
    Replace CAFLOCAL with Space(40)
    Replace CAFPROV with Space(2)
    Replace CAFINDIR with Space(35)
    Replace CAF__CAP with "00000000"
    Replace CAFFILL with "000000"
    if this.w_AZFISFOR="S"
      * --- Dati Riservati al Fornitore Persona Fisica
      Replace TTCOGTIT with LEFT(ALLTRIM(UPPER(this.w_AZCOGFOR))+SPACE(24), 24)
      Replace TTNOMTIT with LEFT(ALLTRIM(UPPER(this.w_AZNOMFOR))+SPACE(20), 20)
      Replace TT_SESSO with IIF(EMPTY(this.w_AZSEXFOR), "M", this.w_AZSEXFOR)
      if NOT EMPTY(this.w_AZDATFOR)
        this.w_cDate = DTOS(this.w_AZDATFOR)
        Replace TTDATNAS with RIGHT(this.w_CDATE, 2)+SUBSTR(this.w_CDATE, 5, 2)+LEFT(this.w_CDATE, 4)
      endif
      Replace TTLUONAS with LEFT(ALLTRIM(UPPER(this.w_AZCONFOR))+SPACE(40), 40)
      Replace TTPRONAS with LEFT(ALLTRIM(UPPER(this.w_AZPRNFOR))+SPACE(2), 2)
      Replace TTLOCTIT with LEFT(ALLTRIM(UPPER(this.w_AZCOMFOR))+SPACE(40), 40)
      Replace TTPROTIT with LEFT(ALLTRIM(UPPER(this.w_AZPROFOR))+SPACE(2), 2)
      Replace TTINDIRI with LEFT(ALLTRIM(UPPER(this.w_AZINDFOR))+SPACE(35), 35)
      Replace TTCAPTIT with RIGHT("00000000"+ALLTRIM(UPPER(this.w_AZCAPFOR)), 8)
    else
      * --- Dati Riservati al Fornitore Persona non Fisica
      Replace AZRAGAZI with LEFT(ALLTRIM(UPPER(this.w_AZDENFOR))+SPACE(60), 60)
      Replace AZLOCAZI with LEFT(ALLTRIM(UPPER(this.w_AZCOLFOR))+SPACE(40), 40)
      Replace AZPROAZI with LEFT(ALLTRIM(UPPER(this.w_AZPRLFOR))+SPACE(2), 2)
      Replace AZINDAZI with LEFT(ALLTRIM(UPPER(this.w_AZINLFOR))+SPACE(35), 35)
      Replace AZCAPAZI with RIGHT("00000000"+ALLTRIM(UPPER(this.w_AZCALFOR)), 8)
      if this.oParentObject.w_TIPFORN="03" OR this.oParentObject.w_TIPFORN="05" 
        * --- Il fornitore e' un C.A.F.
        Replace CAFLOCAL with LEFT(ALLTRIM(UPPER(this.w_AZCOMFOR))+SPACE(40), 40)
        Replace CAFPROV with LEFT(ALLTRIM(UPPER(this.w_AZPROFOR))+SPACE(2), 2)
        Replace CAFINDIR with LEFT(ALLTRIM(UPPER(this.w_AZINDFOR))+SPACE(35), 35)
        Replace CAF__CAP with RIGHT("00000000"+ALLTRIM(UPPER(this.w_AZCAPFOR)), 8)
      else
        Replace SELOCALI with LEFT(ALLTRIM(UPPER(this.w_AZCOMFOR))+SPACE(40), 40)
        Replace SEPROVIN with LEFT(ALLTRIM(UPPER(this.w_AZPROFOR))+SPACE(2), 2)
        Replace SEINDIRI with LEFT(ALLTRIM(UPPER(this.w_AZINDFOR))+SPACE(35), 35)
        Replace SE__CAP with RIGHT("00000000"+ALLTRIM(UPPER(this.w_AZCAPFOR)), 8)
      endif
    endif
    * --- Spazio a disposizione dell'utente / non utilizzato / non disponibile
    Replace CAMPOUT with Space(100)
    Replace FILLER301 with Space(254)
    Replace FILLER302 with Space(254)
    Replace FILLER303 with Space(254)
    Replace FILLER304 with Space(254)
    Replace FILLER305 with Space(54)
    Replace SPAZRIS with Space(200)
    * --- Caratteri di controllo del record
    Replace CONTR32 with "A"
    Replace CONTR33 with CHR(13)+CHR(10)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura Record B ; Campi 1 .. 24
    Replace TIPREC with "B"
    Replace AZCOFAZI with LEFT(ALLTRIM(UPPER(this.w_AZCOFAZI))+SPACE(16), 16)
    Replace FILLER3 with SPACE(8)
    Replace SPAZIO4 with SPACE(3)
    Replace FILLER25 with SPACE(25)
    Replace SPAZIO8 with SPACE(20)
    * --- IDENTIFICATIVO PRODUTTORE DEL SOFTWARE - PARTITA IVA DI Z_TAM (7)
    this.w_IDPRODSW = "06005190159     "
    Replace IDPRODSW with this.w_IDPRODSW 
 
    * --- COMUNICAZIONE DI MANCATA CORRISPONDENZA DEI DATI DA TRASMETTERE (8)
    REPLACE FLAGCONF WITH IIF(this.oParentObject.w_DICHCONF="1", "1", "0")
    * --- PROTOCOLLO DELLA DICHIARAZIONE DA SOSTITUIRE (9..10)
    Replace IDSERTEL with RIGHT("00000000000000000" + ALLTRIM(this.oParentObject.w_IDSERTEL), 17)
    Replace PROGRDIC with RIGHT("000000" + ALLTRIM(this.oParentObject.w_PROGRDIC), 6)
    * --- Dati del contribuente (11..14)
    Replace AZRAGAZI with LEFT(ALLTRIM(UPPER(this.w_AZRAGAZI))+SPACE(60), 60)
    Replace TTCOGTIT with LEFT(ALLTRIM(UPPER(this.w_TTCOGTIT))+SPACE(24), 24)
    Replace TTNOMTIT with LEFT(ALLTRIM(UPPER(this.w_TTNOMTIT))+SPACE(20), 20)
    Replace AZPIVAZI with RIGHT("00000000000"+ALLTRIM(UPPER(this.w_AZPIVAZI)), 11)
    * --- PERIODO (15..22)
    Replace DI_ANNO with RIGHT("0000"+ALLTRIM(UPPER(this.oParentObject.w_ANNO)), 4)
    REPLACE DIPERIOD WITH IIF(this.w_AZTIPDEN="M", RIGHT("00"+ALLTRIM(STR(this.oParentObject.w_PERIOD)), 2), "00")
    REPLACE DITRIM WITH IIF(this.w_AZTIPDEN="M", "0", RIGHT("0"+ALLTRIM(STR(this.oParentObject.w_PERIOD)), 1))
    REPLACE IMPEURO WITH IIF(this.w_VPCODVAL="S", "1", "0")
    REPLACE IMPLIRE WITH IIF(this.w_VPCODVAL="S", "0", "1")
    REPLACE VPVARIMP WITH IIF(this.w_VPVARIMP="S", "1", "0")
    REPLACE VPCORTER WITH IIF(this.w_VPCORTER="S", "1", "0")
    REPLACE DICINTEG WITH IIF(this.oParentObject.w_DICINTEG="1", "1", "0")
    * --- SOCIETA ED ENTI CHE PARTECIPANO ALLA LIQUIDAZIONE DELL'IVA DI GRUPPO (23..24)
    REPLACE VPDICGRU WITH IIF(this.w_VPDICGRU="S", "1", "0")
    REPLACE VPDICSOC WITH IIF(this.w_VPDICSOC="S" AND this.w_VPDICGRU<>"S", "1", "0")
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura Record B Campi 25 .. 55
    * --- Dati Contabili : Campi 25, 26, 27, 28, 29, 30
    Replace VPIMPOR1 WITH REPLICATE("0", 13)
    Replace VPCESINT WITH REPLICATE("0", 13)
    Replace VPIMPOR2 WITH REPLICATE("0", 13)
    Replace VPACQINT WITH REPLICATE("0", 13)
    Replace VPIMPON3 WITH REPLICATE("0", 13)
    Replace VPIMPOS3 WITH REPLICATE("0", 13)
    if this.w_VPIMPOR1 > 0
      Replace VPIMPOR1 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPOR1)), 13)
    endif
    if this.w_VPCESINT > 0
      Replace VPCESINT with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPCESINT)), 13)
    endif
    if this.w_VPIMPOR2 > 0
      Replace VPIMPOR2 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPOR2)), 13)
    endif
    if this.w_VPACQINT > 0
      Replace VPACQINT with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPACQINT)), 13)
    endif
    if this.w_VPIMPON3 > 0
      Replace VPIMPON3 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPON3)), 13)
    endif
    if this.w_VPIMPOS3 > 0
      Replace VPIMPOS3 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPOS3)), 13)
    endif
    * --- Campo 31 IVA Esigibile
    Replace VPIMPOR5 WITH REPLICATE("0", 13)
    if this.w_VPIMPOR5 > 0
      Replace VPIMPOR5 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPOR5)), 13)
    endif
    * --- Campo 32 IVA Detratta
    Replace VPIMPOR6 WITH REPLICATE("0", 13)
    if this.w_VPIMPOR6 > 0
      Replace VPIMPOR6 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPOR6)), 13)
    endif
    * --- Campi 33 IVA a Debito / 34 IVA a Credito
    Replace VPIMPOR7 WITH REPLICATE("0", 13)
    Replace VPIMPOR71 WITH REPLICATE("0", 13)
    do case
      case this.w_VPIMPOR7 > 0
        * --- IVA a Debito
        Replace VPIMPOR7 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPOR7)), 13)
      case this.w_VPIMPOR7 < 0
        * --- IVA a Credito
        this.w_VPIMPOR7 = ABS(this.w_VPIMPOR7)
        Replace VPIMPOR71 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPOR7)), 13)
    endcase
    * --- Campi 35 Variazioni di Imposta a Debito / 36 Variazioni di Imposta a Credito
    Replace VPIMPOR8 WITH REPLICATE("0", 13)
    Replace VPIMPOR81 WITH REPLICATE("0", 13)
    do case
      case this.w_VPIMPOR8 > 0
        Replace VPIMPOR8 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPOR8)), 13)
      case this.w_VPIMPOR8 < 0
        this.w_VPIMPOR8 = ABS(this.w_VPIMPOR8)
        Replace VPIMPOR81 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPOR8)), 13)
    endcase
    * --- Campi 37 IVA non Versata / 38 IVA Versata in Eccesso
    Replace VPIMPOR9 WITH REPLICATE("0", 13)
    Replace VPIMPOR91 WITH REPLICATE("0", 13)
    do case
      case this.w_VPIMPOR9 > 0
        Replace VPIMPOR9 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPOR9)), 13)
      case this.w_VPIMPOR9 < 0
        this.w_VPIMPOR9 = ABS(this.w_VPIMPOR9)
        Replace VPIMPOR91 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPOR9)), 13)
    endcase
    * --- Campi 39 Debito Riportato / 40 Credito Riportato
    Replace VPIMPO10 WITH REPLICATE("0", 13)
    Replace VPIMPO101 WITH REPLICATE("0", 13)
    if this.w_VPDICGRU <> "S" AND this.oParentObject.w_PERIOD<>1
      do case
        case this.w_VPIMPO10 > 0
          Replace VPIMPO10 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPO10)), 13)
        case this.w_VPIMPO10 < 0
          this.w_VPIMPO10 = ABS(this.w_VPIMPO10)
          Replace VPIMPO101 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPO10)), 13)
      endcase
    endif
    * --- Campo 41 Credito IVA Compensabile
    Replace VPIMPO11 WITH REPLICATE("0", 13)
    if this.w_VPIMPO11 > 0
      Replace VPIMPO11 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPO11)), 13)
    endif
    * --- Campi 42 IVA Dovuta / 43 IVA a Credito
    Replace VPIMPO12 WITH REPLICATE("0", 13)
    Replace VPIMPO121 WITH REPLICATE("0", 13)
    this.w_VPIMPO121 = 0
    do case
      case this.w_VPIMPO12 > 0
        Replace VPIMPO12 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPO12)), 13)
      case this.w_VPIMPO12 < 0
        this.w_VPIMPO121 = ABS(this.w_VPIMPO12)
        Replace VPIMPO121 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPO121)), 13)
    endcase
    * --- Campo 44 Crediti Speciali
    Replace VPIMPO13 WITH REPLICATE("0", 13)
    if this.w_VPIMPO13 > 0
      Replace VPIMPO13 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPO13)), 13)
    endif
    * --- Campo 45 Interessi Dovuti
    Replace VPIMPO14 WITH REPLICATE("0", 13)
    if this.w_VPIMPO14 > 0
      Replace VPIMPO14 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPO14)), 13)
    endif
    * --- Campo 46 Acconto Versato
    Replace VPIMPO15 WITH REPLICATE("0", 13)
    if this.w_VPIMPO15 > 0
      if this.w_VPDICSOC <> "S"
        if (this.w_AZTIPDEN = "M" AND this.oParentObject.w_PERIOD = 12) OR (this.w_AZTIPDEN = "T" AND this.oParentObject.w_PERIOD = 4)
          Replace VPIMPO15 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPO15)), 13)
        endif
      endif
    endif
    * --- Campo 47 Importo da Versare
    Replace VPIMPO16 WITH REPLICATE("0", 13)
    if this.w_VPIMPO16 > 0
      Replace VPIMPO16 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPO16)), 13)
    endif
    * --- Campi 48 Importo Versato / 49 Versamento in EURO / 50 Data Versamento / 51 Codice Azienda / 52 Codice CAB
    Replace VPIMPO17 WITH REPLICATE("0", 13)
    Replace VERCAS17 with "0"
    Replace VPDATVER with "00000000"
    Replace VPCODAZI with "00000"
    Replace VPCODCAB with "00000"
    * --- Estremi Versamento 48..52
    if this.w_VPDICSOC <> "S" AND this.w_VPIMPO17 > 0
      Replace VPIMPO17 with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPIMPO17)), 13)
      REPLACE VERCAS17 WITH IIF(this.w_VERCAS17 = "S", "1", "0")
      if NOT EMPTY(this.w_VPDATVER)
        this.w_cDate = DTOS(this.w_VPDATVER)
        Replace VPDATVER with RIGHT(this.w_CDATE, 2)+SUBSTR(this.w_CDATE, 5, 2)+LEFT(this.w_CDATE, 4)
      endif
      Replace VPCODAZI with RIGHT("00000"+ALLTRIM(this.w_VPCODAZI), 5)
      Replace VPCODCAB with RIGHT("00000"+ALLTRIM(this.w_VPCODCAB), 5)
    endif
    * --- Campi 53 Versamenti non Effettuati / 54 Subfornitori art 74 comma 5 / 55 Contribuenti art 74 comma 4 
     REPLACE VPVERNEF WITH IIF(this.w_VPVERNEF = "S", "1", "0")
    REPLACE VPAR74C5 WITH IIF(this.w_VPAR74C5 = "S", "1", "0")
    REPLACE VPAR74C4 WITH IIF(this.w_VPAR74C4 = "S", "1", "0")
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura Record B Campi 56 .. 81
    * --- Campi 56 Aliquota Media / 57 Op. non Imponibili / 58 Acq. beni Ammortizzabili
    REPLACE VPOPMEDI WITH IIF(this.w_VPDICSOC<>"S" AND this.w_VPOPMEDI = "S", "1", "0")
    REPLACE VPOPNOIM WITH IIF(this.w_VPDICSOC<>"S" AND this.w_VPOPNOIM = "S", "1", "0")
    REPLACE VPACQBEA WITH IIF(this.w_VPDICSOC<>"S" AND this.w_VPACQBEA = "S", "1", "0")
    * --- Campi 59 Credito Chiesto a Rimborso / 60 Importi in EURO / 61 Credito da utilizzare in compensazione con F24
    Replace VPCRERIM WITH REPLICATE("0", 13)
    Replace VPEURO19 with "0"
    Replace VPCREUTI WITH REPLICATE("0", 13)
    if this.w_VPDICSOC <> "S" 
      if this.w_VPCRERIM > 0 AND this.w_VPCRERIM <= this.w_VPIMPO121 AND (this.w_VPOPMEDI = "S" OR this.w_VPOPNOIM = "S")
        Replace VPCRERIM with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPCRERIM)), 13)
        Replace VPEURO19 with IIF(this.w_VPEURO19 = "S", "1", "0")
      endif
      if this.w_VPCREUTI > 0 AND this.w_VPCREUTI <= this.w_VPIMPO121 AND (this.w_VPOPMEDI = "S" OR this.w_VPOPNOIM = "S")
        Replace VPCREUTI with RIGHT(SPACE(13)+ALLTRIM(STR(this.w_VPCREUTI)), 13)
      endif
    endif
    * --- Dati del Dichiarante
    * --- Campi 62 Firma / 63 Codice carica / 64 Codice fiscale
    REPLACE FIRMA with IIF(this.oParentObject.w_FIRMAPRE="1", "1", "0")
    REPLACE VPCODCAR with "00"
    REPLACE VPCODFIS with Space(16)
    if NOT EMPTY(this.w_CODCARIC) AND NOT EMPTY(this.w_CODFISDI)
      REPLACE VPCODCAR with RIGHT("00"+ALLTRIM(this.w_CODCARIC), 2)
      Replace VPCODFIS with LEFT(ALLTRIM(UPPER(this.w_CODFISDI))+SPACE(16), 16)
    endif
    * --- SPAZIO RISERVATO ALL'UFFICIO - DATA DI PRESENTAZIONE
    * --- Campi 65 ... 70 Ricevuta di Presentazione agli Intermediari
    Replace CAMPO65 with SPACE(16)
    Replace CAMPO66 with "00000"
    REPLACE CAMPO67 WITH "00000000"
    REPLACE CAMPO68 WITH "0"
    REPLACE CAMPO69 WITH "0"
    REPLACE CAMPO70 WITH "0"
    if NOT (this.oParentObject.w_TIPFORN="01" OR this.oParentObject.w_TIPFORN="02")
      Replace CAMPO65 with LEFT(ALLTRIM(UPPER(this.w_AZCOFFOR))+SPACE(16), 16)
      Replace CAMPO66 with RIGHT("00000" + ALLTRIM(this.w_AZNUMCAF), 5)
      this.w_cDate = DTOS(this.oParentObject.w_DATASTAM)
      REPLACE CAMPO67 WITH RIGHT(this.w_CDATE, 2)+SUBSTR(this.w_CDATE, 5, 2)+LEFT(this.w_CDATE, 4)
      REPLACE CAMPO68 WITH IIF(this.oParentObject.w_CAMPO68="1", "1", "0")
      REPLACE CAMPO69 WITH IIF(this.oParentObject.w_CAMPO69="1", "1", "0")
      REPLACE CAMPO70 WITH IIF(this.oParentObject.w_CAMPO70="1", "1", "0")
    endif
    * --- Campi 71 ... 73 Visto di Conformita'
    Replace CAMPO71 with LEFT(ALLTRIM(UPPER(this.oParentObject.w_CODFISCA))+SPACE(16), 16)
    REPLACE CAMPO72 WITH IIF(this.oParentObject.w_CAMPO72="1", "1", "0")
    REPLACE CAMPO73 WITH IIF(this.oParentObject.w_CAMPO73="1", "1", "0")
    * --- Campi 74..79 Spazio non Utilizzato / non Disponibile / Riservato all'Ufficio (lasciati Blank)
    * --- Campi 80..81 Caratteri di Controllo
    REPLACE CAMPO80 WITH "A"
    REPLACE CAMPO81 WITH CHR(13)+CHR(10)
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali e Aggiornamento Data di Presentazione
    this.w_LTmp = .T.
    if FILE(this.oParentObject.w_NOMEFILE)
      this.w_LTmp = ah_YesNo("Il file %1 � gi� presente: sovrascrivo?","", alltrim(this.oParentObject.w_NOMEFILE))
    endif
    if this.w_LTmp
      this.w_nErrFile = FCREATE(this.oParentObject.w_NOMEFILE)
      * --- SCRITTURA DEL FILE
      if this.w_nErrFile = -1
        ah_ErrorMsg("Elaborazione abortita: impossibile creare il file%0%1","!","", ALLTRIM(this.oParentObject.w_NOMEFILE) )
        i_retcode = 'stop'
        return
      endif
      Select tip_rec_a
      this.w_MEMOREC = TIPREC+PRIMOFILL+CODFORN+TIPFORN+CODFISF
      this.w_MEMOREC = this.w_MEMOREC+TTCOGTIT+TTNOMTIT+TT_SESSO+TTDATNAS+TTLUONAS+TTPRONAS+TTLOCTIT+TTPROTIT+TTINDIRI+TTCAPTIT
      this.w_MEMOREC = this.w_MEMOREC+AZRAGAZI+AZLOCAZI+AZPROAZI+AZINDAZI+AZCAPAZI+SELOCALI+SEPROVIN+SEINDIRI+SE__CAP
      this.w_MEMOREC = this.w_MEMOREC+CAFLOCAL+CAFPROV+CAFINDIR+CAF__CAP+CAFFILL
      this.w_MEMOREC = this.w_MEMOREC+CAMPOUT+FILLER301+FILLER302+FILLER303+FILLER304+FILLER305+SPAZRIS+CONTR32+CONTR33
      this.w_CONTRO = FWRITE(this.w_nErrFile,this.w_MEMOREC)
      * --- Controllo se la scrittura � avvenuta in modo corretto.
      if this.w_CONTRO=0
        ah_ErrorMsg("Elaborazione abortita: sezione a non corretta%0Impossibile creare il file","!","")
        i_retcode = 'stop'
        return
      endif
      Select tip_rec_b
      this.w_MEMOREC = TIPREC+AZCOFAZI+FILLER3+SPAZIO4+FILLER25+SPAZIO8+IDPRODSW+FLAGCONF+IDSERTEL+PROGRDIC
      this.w_MEMOREC = this.w_MEMOREC+AZRAGAZI+TTCOGTIT+TTNOMTIT+AZPIVAZI+DI_ANNO+DIPERIOD+DITRIM
      this.w_MEMOREC = this.w_MEMOREC+IMPEURO+IMPLIRE+VPVARIMP+VPCORTER+DICINTEG+VPDICGRU+VPDICSOC
      this.w_MEMOREC = this.w_MEMOREC+VPIMPOR1+VPCESINT+VPIMPOR2+VPACQINT+VPIMPON3+VPIMPOS3+VPIMPOR5+VPIMPOR6
      this.w_MEMOREC = this.w_MEMOREC+VPIMPOR7+VPIMPOR71+VPIMPOR8+VPIMPOR81+VPIMPOR9+VPIMPOR91
      this.w_MEMOREC = this.w_MEMOREC+VPIMPO10+VPIMPO101+VPIMPO11+VPIMPO12+VPIMPO121+VPIMPO13+VPIMPO14+VPIMPO15
      this.w_MEMOREC = this.w_MEMOREC+VPIMPO16+VPIMPO17+VERCAS17+VPDATVER+VPCODAZI+VPCODCAB+VPVERNEF+VPAR74C5+VPAR74C4
      this.w_MEMOREC = this.w_MEMOREC+VPOPMEDI+VPOPNOIM+VPACQBEA+VPCRERIM+VPEURO19+VPCREUTI
      this.w_MEMOREC = this.w_MEMOREC+FIRMA+VPCODCAR+VPCODFIS
      this.w_MEMOREC = this.w_MEMOREC+CAMPO65+CAMPO66+CAMPO67+CAMPO68+CAMPO69+CAMPO70
      this.w_MEMOREC = this.w_MEMOREC+CAMPO71+CAMPO72+CAMPO73
      this.w_MEMOREC = this.w_MEMOREC+FILLER74A+FILLER74B+FILLER74C+FILLER74D+FILLER74E
      this.w_MEMOREC = this.w_MEMOREC+CAMPO75+CAMPO76+CAMPO77+CAMPO78+CAMPO79+CAMPO80+CAMPO81
      this.w_CONTRO = FWRITE(this.w_nErrFile,this.w_MEMOREC)
      * --- Controllo se la scrittura � avvenuta in modo corretto.
      if this.w_CONTRO=0
        ah_ErrorMsg("Elaborazione abortita: sezione B non corretta%0Impossibile creare il file","!","")
        i_retcode = 'stop'
        return
      endif
      Select tip_rec_z
      this.w_MEMOREC = TIPREC+FILLER2+NUMRECB+FILLER41+FILLER42+FILLER43+FILLER44+FILLER45+FILLER46+FILLER47+FILLER48+FILLER5+FILLER6
      this.w_CONTRO = FWRITE(this.w_nErrFile,this.w_MEMOREC)
      * --- Controllo se la scrittura � avvenuta in modo corretto.
      if this.w_CONTRO=0
        ah_ErrorMsg("Elaborazione abortita: sezione Z non corretta%0Impossibile creare il file","!","")
        i_retcode = 'stop'
        return
      endif
      FCLOSE(this.w_nErrFile)
      this.w_OK = ah_YesNo("Scrittura del file %1 terminata con successo%0Si vuole aggiornare la data di invio telematico%0presente nella dichiarazione periodica?","", ALLTRIM(this.oParentObject.w_NOMEFILE) )
      if this.w_OK
        * --- Aggiorno la data presente nell'anagrafica della dichiarazione periodica.
        * --- Write into IVA_PERI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.IVA_PERI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.IVA_PERI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"VPDATINO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATASTAM),'IVA_PERI','VPDATINO');
              +i_ccchkf ;
          +" where ";
              +"VP__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
              +" and VPPERIOD = "+cp_ToStrODBC(this.oParentObject.w_PERIOD);
              +" and VPKEYATT = "+cp_ToStrODBC(this.oParentObject.w_KEYATT);
                 )
        else
          update (i_cTable) set;
              VPDATINO = this.oParentObject.w_DATASTAM;
              &i_ccchkf. ;
           where;
              VP__ANNO = this.oParentObject.w_ANNO;
              and VPPERIOD = this.oParentObject.w_PERIOD;
              and VPKEYATT = this.oParentObject.w_KEYATT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='IVA_PERI'
    this.cWorkTables[3]='TITOLARI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
