* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ano                                                        *
*              Nominativi                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1992-03-23                                                      *
* Last revis.: 2018-07-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsar_ano
* Puo' assumere i valori
* - B Lanciato da men� beneficiari
* - N Lanciato da men� Nominativi Offerte
PARAMETERS pParam

**quando la maschera viene lanciata dagli zoom dei debitori/creditori diversi
IF vartype (pParam) <>'C'
  IF ( Type ('application.activeform.caption')='C' AND ('red. o deb. diversi'$ application.activeform.caption OR 'scadenze diverse'$ application.activeform.caption))
    pParam='B'
  ENDIF
ENDIF
* --- Fine Area Manuale
return(createobject("tgsar_ano"))

* --- Class definition
define class tgsar_ano as StdForm
  Top    = 0
  Left   = 6

  * --- Standard Properties
  Width  = 787
  Height = 544+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-10"
  HelpContextID=74877079
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=325

  * --- Constant Properties
  OFF_NOMI_IDX = 0
  BAN_CHE_IDX = 0
  AGENTI_IDX = 0
  PAG_AMEN_IDX = 0
  LISTINI_IDX = 0
  VALUTE_IDX = 0
  GRUPRO_IDX = 0
  CATECOMM_IDX = 0
  LINGUE_IDX = 0
  CAT_SCMA_IDX = 0
  NAZIONI_IDX = 0
  CACOCLFO_IDX = 0
  CONTI_IDX = 0
  ZONE_IDX = 0
  VOCIIVA_IDX = 0
  DES_DIVE_IDX = 0
  MASTRI_IDX = 0
  COC_MAST_IDX = 0
  SIT_FIDI_IDX = 0
  COD_ABI_IDX = 0
  COD_CAB_IDX = 0
  BAN_CONTI_IDX = 0
  DIPENDEN_IDX = 0
  GRU_NOMI_IDX = 0
  ORI_NOMI_IDX = 0
  GRU_PRIO_IDX = 0
  cpusers_IDX = 0
  CPUSERS_IDX = 0
  SAL_NOMI_IDX = 0
  COD_CATA_IDX = 0
  NOM_CONT_IDX = 0
  NAT_GIUR_IDX = 0
  CAANCLFO_IDX = 0
  VOC_FINZ_IDX = 0
  DORATING_IDX = 0
  CAT_FINA_IDX = 0
  cFile = "OFF_NOMI"
  cKeySelect = "NOCODICE"
  cKeyWhere  = "NOCODICE=this.w_NOCODICE"
  cKeyWhereODBC = '"NOCODICE="+cp_ToStrODBC(this.w_NOCODICE)';

  cKeyWhereODBCqualified = '"OFF_NOMI.NOCODICE="+cp_ToStrODBC(this.w_NOCODICE)';

  cPrg = "gsar_ano"
  cComment = "Nominativi"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0ACL'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PARCALLER = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_NOCODICE = space(15)
  o_NOCODICE = space(15)
  w_NOCODCLI = space(15)
  w_VALATT = space(20)
  w_TABKEY = space(8)
  w_DELIMM = .F.
  w_NODESCRI = space(60)
  w_NODESCR2 = space(60)
  w_NOSOGGET = space(2)
  o_NOSOGGET = space(2)
  w_TIPRIS = space(1)
  w_NOCOD_RU = space(5)
  o_NOCOD_RU = space(5)
  w_NOBADGE = space(20)
  w_NOCOGNOM = space(50)
  o_NOCOGNOM = space(50)
  w_NOCOGNOM = space(50)
  w_NO__NOME = space(50)
  o_NO__NOME = space(50)
  w_NO__NOME = space(50)
  w_NOLOCNAS = space(30)
  w_NOLOCNAS = space(30)
  w_NOPRONAS = space(2)
  w_NOPRONAS = space(2)
  w_NODATNAS = ctod('  /  /  ')
  w_NO_SESSO = space(1)
  w_NONAZNAS = space(3)
  w_NONUMCAR = space(18)
  o_NONUMCAR = space(18)
  w_NOCODFIS = space(16)
  o_NOCODFIS = space(16)
  w_NOPARIVA = space(12)
  w_NONATGIU = space(3)
  w_NOCODSAL = space(5)
  w_NOINDIRI = space(35)
  o_NOINDIRI = space(35)
  w_NOINDI_2 = space(35)
  o_NOINDI_2 = space(35)
  w_CAP = space(9)
  w_NO___CAP = space(8)
  w_NO___CAP = space(9)
  w_NOLOCALI = space(30)
  w_NOLOCALI = space(30)
  w_NOPROVIN = space(2)
  w_NOPROVIN = space(2)
  w_NONAZION = space(3)
  w_NOINDIR2 = space(35)
  o_NOINDIR2 = space(35)
  w_NO___CAD = space(9)
  w_NO___CAD = space(9)
  w_NOLOCALD = space(30)
  w_OCOGNOM = space(50)
  w_ONOME = space(50)
  w_LOCALD = space(40)
  o_LOCALD = space(40)
  w_NOLOCALD = space(30)
  w_NOPROVID = space(2)
  w_NOPROVID = space(2)
  w_NONAZIOD = space(3)
  w_NOTELEFO = space(18)
  w_NONUMCEL = space(18)
  w_NO_SKYPE = space(50)
  w_NOTELFAX = space(18)
  w_NO_EMAIL = space(254)
  w_NOINDWEB = space(254)
  o_NOINDWEB = space(254)
  w_NO_EMPEC = space(254)
  w_NOCHKSTA = space(1)
  w_NOCHKMAI = space(1)
  w_NOCHKPEC = space(1)
  w_NOCHKFAX = space(1)
  w_NOCHKCPZ = space(1)
  w_NOCHKCPZ = space(1)
  w_NOCHKWWP = space(1)
  w_NOCHKPTL = space(1)
  w_NODTINVA = ctod('  /  /  ')
  w_NODTOBSO = ctod('  /  /  ')
  w_NOSOGGET = space(2)
  w_NOTIPPER = space(1)
  o_NOTIPPER = space(1)
  w_NOTIPNOM = space(1)
  w_NOTIPNOM = space(1)
  w_NOTIPNOM = space(1)
  w_NOFLGBEN = space(1)
  o_NOFLGBEN = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_TIPOPE = space(4)
  w_NOTIPCLI = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_NOTIPO = space(1)
  w_DESNAZ1 = space(35)
  w_CODISO1 = space(3)
  w_FL_SOG_RIT = space(1)
  w_ERR = space(10)
  w_DATOBSO = ctod('  /  /  ')
  w_CONSBF = space(1)
  w_VALLIS = space(3)
  w_IVALIS = space(1)
  w_TIPO = space(1)
  w_INIVAL = ctod('  /  /  ')
  w_FINVAL = ctod('  /  /  ')
  w_DATOBS = ctod('  /  /  ')
  w_COMODO = space(10)
  w_LOCNAS = space(40)
  o_LOCNAS = space(40)
  w_DPFLINEX = space(1)
  w_CODCOM = space(4)
  w_CONTA = 0
  o_CONTA = 0
  w_NOTIPBAN = space(1)
  w_NOTIPCON = space(1)
  w_NOCODCON = space(15)
  o_NOCODCON = space(15)
  w_NOCODPAG = space(5)
  w_DESPAG = space(30)
  w_NOGIOFIS = 0
  w_NO1MESCL = 0
  w_DESMES1 = space(12)
  w_NOGIOSC1 = 0
  w_NO2MESCL = 0
  o_NO2MESCL = 0
  w_NOGIOSC2 = 0
  w_DESMES2 = space(12)
  w_NOCODBAN = space(10)
  w_NOCODBA2 = space(15)
  o_NOCODBA2 = space(15)
  w_NOFLRAGG = space(1)
  w_NOFLGAVV = space(1)
  w_NODATAVV = ctod('  /  /  ')
  w_NOINTMOR = space(1)
  o_NOINTMOR = space(1)
  w_NOMORTAS = 0
  w_DESBA2 = space(42)
  w_DESBAN = space(42)
  w_NOCODICE = space(15)
  w_ANDESCRI = space(40)
  w_NOTIPPER = space(1)
  w_DATOBSOPAG = ctod('  /  /  ')
  w_DATOBSOBA2 = ctod('  /  /  ')
  w_DATOBSN = ctod('  /  /  ')
  w_BANCAAHE = space(10)
  o_BANCAAHE = space(10)
  w_NOCATFIN = space(5)
  w_NOCATDER = space(5)
  w_NOIDRIDY = space(1)
  o_NOIDRIDY = space(1)
  w_NOTIIDRI = 0
  o_NOTIIDRI = 0
  w_NOCOIDRI = space(16)
  o_NOCOIDRI = space(16)
  w_OLDANTIIDRI = space(1)
  w_OLDANCOIDRI = space(16)
  w_ANRITENU = space(10)
  w_ANFLRITE = space(1)
  w_ANTIPCLF = space(1)
  w_NOCOGNOM = space(50)
  w_NO__NOME = space(50)
  w_NODESCRI = space(40)
  w_NODESCR2 = space(40)
  w_NONUMCOR = space(25)
  w_ANPARTSN = space(1)
  w_CFDESCRIC = space(30)
  w_CFDESCRID = space(30)
  w_TIPOFIN = space(1)
  w_TIPOFIND = space(1)
  w_NONOMFIL = space(30)
  w_NOOGGE = space(30)
  w_NONOTE = space(30)
  w_SERALL = space(10)
  w_PATALL = space(254)
  w_CODI = space(15)
  w_OF__NOTE = space(0)
  w_NOTIPPER = space(1)
  w_NOCOGNOM = space(50)
  w_NO__NOME = space(50)
  w_NODESCRI = space(40)
  w_NODESCR2 = space(40)
  w_CODI = space(15)
  w_NOCODPRI = 0
  w_NOPRIVCY = space(1)
  w_NOCODGRU = space(5)
  w_NODESGRU = space(35)
  w_NOCODORI = space(5)
  w_NODESORI = space(35)
  w_NORIFINT = space(5)
  o_NORIFINT = space(5)
  w_NOCODOPE = 0
  w_NODESOPE = space(35)
  w_NOASSOPE = ctod('  /  /  ')
  w_NOCODZON = space(3)
  w_NODESZON = space(35)
  w_NOCODLIN = space(3)
  w_NODESLIN = space(35)
  w_NOCODVAL = space(3)
  w_NOCODAGE = space(5)
  w_NODESVAL = space(35)
  w_DTOBZON = ctod('  /  /  ')
  w_DTOBVAL = ctod('  /  /  ')
  w_DTOBAGE = ctod('  /  /  ')
  w_NODESAGE = space(35)
  w_NOTIPPER = space(1)
  w_NOCOGNOM = space(50)
  w_NO__NOME = space(50)
  w_NODESCRI = space(40)
  w_NODESCR2 = space(40)
  w_NONUMLIS = space(5)
  w_NONUMLIS = space(5)
  w_NOCODPAG = space(5)
  w_NOCODBAN = space(10)
  w_NOCATCOM = space(3)
  w_NOCATCON = space(5)
  w_NOCACLFO = space(5)
  w_BANCAAHR = space(10)
  o_BANCAAHR = space(10)
  w_BANCAAHE = space(10)
  w_DESBA3 = space(42)
  w_DESBA2 = space(42)
  w_DESBAN = space(42)
  w_DESPAG = space(30)
  w_DESCAC = space(35)
  w_DESLIS = space(40)
  w_COGNOME = space(40)
  w_NOME = space(40)
  w_NODENOMRIF = space(37)
  w_DESCON = space(35)
  w_DESSCATCLFO = space(35)
  w_CodiceFittizio = space(20)
  w_ForzaNom = space(1)
  w_SAL_Dest = space(5)
  w_RAGSOC_Dest = space(100)
  w_INDIRI_Dest = space(100)
  w_LOCALI_Dest = space(100)
  w_CAP_Dest = space(9)
  w_PROV_Dest = space(2)
  w_NAZ_Dest = space(3)
  w_CortAtt_Dest = space(100)
  w_CODI = space(15)
  w_NOTIPPER = space(1)
  w_NOCOGNOM = space(50)
  w_NO__NOME = space(50)
  w_NODESCRI = space(40)
  w_NODESCR2 = space(40)
  w_OFSTATUS = space(10)
  w_SEROFF = space(10)
  w_NFDTOF1 = ctod('  /  /  ')
  w_NFDTOF2 = ctod('  /  /  ')
  w_NFDTRC1 = ctod('  /  /  ')
  w_NFDTRC2 = ctod('  /  /  ')
  w_NFGRPRIO = space(5)
  w_NFRIFDE = space(45)
  w_NUMINI = space(10)
  w_DTOBGR = ctod('  /  /  ')
  w_NUMFIN = space(10)
  w_CODI = space(15)
  w_OFDATDOC = ctod('  /  /  ')
  w_OFPATFWP = space(254)
  w_OFPATPDF = space(254)
  w_PATHARC = space(254)
  w_PERIODO = space(1)
  w_OFCODNOM = space(15)
  w_NOTIPPER = space(1)
  w_NOCOGNOM = space(50)
  w_NO__NOME = space(50)
  w_NODESCRI = space(40)
  w_NODESCR2 = space(40)
  w_OPERAT = 0
  w_STATO = space(1)
  w_NAME = space(20)
  w_STATO = space(1)
  w_DATAINI = ctod('  /  /  ')
  o_DATAINI = ctod('  /  /  ')
  w_ORAINI = space(2)
  o_ORAINI = space(2)
  w_MININI = space(2)
  o_MININI = space(2)
  w_DATAFIN = ctod('  /  /  ')
  o_DATAFIN = ctod('  /  /  ')
  w_ORAFIN = space(2)
  o_ORAFIN = space(2)
  w_MINFIN = space(2)
  o_MINFIN = space(2)
  w_SUBJECT = space(254)
  o_SUBJECT = space(254)
  w_CONTINI = space(5)
  o_CONTINI = space(5)
  w_PRIOINI = 0
  o_PRIOINI = 0
  w_PRIOINI = 0
  w_TIPOATINI = space(5)
  w_TIPOATFIN = space(5)
  w_ATSERIAL = space(20)
  w_OGGETTO = space(254)
  w_ATDATINI = ctot('')
  w_ATDATFIN = ctot('')
  w_ATDATINI1 = ctot('')
  w_ATDATFIN1 = ctot('')
  w_NOMINI = space(20)
  o_NOMINI = space(20)
  w_NOMFIN = space(20)
  o_NOMFIN = space(20)
  w_ESITOATI = space(5)
  w_ESITOATF = space(5)
  w_PRIOFIN = 0
  w_DESCPER = space(40)
  w_OFF_SER = space(10)
  w_NODATINS = ctod('  /  /  ')
  w_NO__RESP = space(5)
  o_NO__RESP = space(5)
  w_CODUTE = 0
  o_CODUTE = 0
  w_DATI = space(40)
  w_NOTIPOID = space(2)
  w_NOTIPDOC = space(2)
  o_NOTIPDOC = space(2)
  w_NONUMDOC = space(15)
  o_NONUMDOC = space(15)
  w_NODATRIL = ctod('  /  /  ')
  w_NOAUTLOC = space(30)
  w_NOATTLAV = space(50)
  w_NONAGIUR = space(4)
  w_NGDESCRI = space(50)
  w_NODATCOS = ctod('  /  /  ')
  w_NO_SIGLA = space(30)
  w_NO_PAESE = space(30)
  w_NOESTNAS = space(30)
  w_OLD_NOFLANTI = space(1)
  w_NOFLANTI = space(1)
  w_NOFLANTI = space(1)
  w_NOCODICE = space(15)
  w_NOCOGNOM = space(50)
  w_NODESCRI = space(40)
  w_NO__NOME = space(50)
  w_NODESCR2 = space(40)
  w_TipoRisorsa = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_NO__NOTE = space(0)
  w_CODI = space(15)
  w_NOTIPPER = space(1)
  w_NOCOGNOM = space(50)
  w_NO__NOME = space(50)
  w_NODESCRI = space(40)
  w_NODESCR2 = space(40)
  w_EMAIL = space(254)
  w_NORATING = space(2)
  w_NOGIORIT = 0
  w_NOVOCFIN = space(6)
  w_DFDESCRI = space(60)
  w_NOESCDOF = space(1)
  w_NODESPAR = space(1)
  w_RADESCRI = space(50)
  w_NOCODICE = space(15)
  w_NOTIPPER = space(1)
  w_NOCOGNOM = space(50)
  w_NO__NOME = space(50)
  w_NODESCRI = space(40)
  w_NODESCR2 = space(40)
  w_EMPEC = space(254)
  w_OFCODMOD = space(5)
  w_TELFAX = space(18)
  w_APEMAIL = space(254)
  w_APEMPEC = space(254)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_NOCODICE = this.W_NOCODICE

  * --- Children pointers
  GSAR_MCC = .NULL.
  GSAR_MAN = .NULL.
  GSAR_MCN = .NULL.
  w_ZoomAll = .NULL.
  w_ZoomOff = .NULL.
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar_ano
  w_PaginaNote = 0
  w_BckpForClr = 0
  pTipMask=' '
  p_aut=0
  w_RESCHK = 0 && Controlli alla checkform
  
  PROCEDURE CodiceRid(obj)
  	IF (this.cfunction='Load')
  		return
  	ENDIF
  	IF ((NVL (this.w_NOTIIDRI,0)<>NVL(this.w_OLDANTIIDRI,0) AND NVL(this.w_OLDANTIIDRI,0)<>0  ) OR ( NVL(this.w_NOCOIDRI,"")<>NVL (this.w_OLDANCOIDRI,"") AND NVL (this.w_OLDANCOIDRI," ")<>" " ))
  		IF (AH_YESNO ("Qualora si dovessero ricevere, dopo la modifica, esiti relativi a effetti presentati con il precedente ID bancario, la procedura non riuscir� a ricondurli al debitore/creditore diverso. Confermare la modifica?") )
  			this.w_OLDANTIIDRI=this.w_NOTIIDRI
  			this.w_OLDANCOIDRI=this.w_NOCOIDRI
  		ELSE
  			this.w_NOTIIDRI=this.w_OLDANTIIDRI
  			this.w_NOCOIDRI=this.w_OLDANCOIDRI
  		ENDIF
  	ENDIF
  ENDPROC
  
  PROCEDURE IdentificaNom
   GSAT_BIN(this, this.w_NOCODICE, 'N')
   this.LoadRecWarn()
   this.Refresh()
  ENDPROC
  
  PROCEDURE IdentificaNom2(pParent)
   IF g_ANTI='S' AND pParent.w_OLD_NOFLANTI#'S' AND pParent.w_NOFLANTI='S'
      GSAT_BIN(pParent, pParent.w_NOCODICE, 'S')
   ENDIF
  ENDPROC
  * --- ZeroFill, restituisce '00' se il parametro � vuoto o di
  * --- lunghezza inferiore a 2
  Func ZeroFill(pNum)
    return IIF(EMPTY(pNum) Or Len(Alltrim(pNum)) < 2, '00', pNum)
  EndFunc
  
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
     return(lower('GSAR_ANO,'+iif(Type('this.ptipmask')='C',this.ptipmask,'N')))
  EndFunc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=11, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'OFF_NOMI','gsar_ano')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_anoPag1","gsar_ano",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Anagrafici")
      .Pages(1).HelpContextID = 218397343
      .Pages(2).addobject("oPag","tgsar_anoPag2","gsar_ano",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati pagamenti")
      .Pages(2).HelpContextID = 127660461
      .Pages(3).addobject("oPag","tgsar_anoPag3","gsar_ano",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Allegati")
      .Pages(3).HelpContextID = 206748015
      .Pages(4).addobject("oPag","tgsar_anoPag4","gsar_ano",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Attributi")
      .Pages(4).HelpContextID = 226510858
      .Pages(5).addobject("oPag","tgsar_anoPag5","gsar_ano",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Persone")
      .Pages(5).HelpContextID = 165745398
      .Pages(6).addobject("oPag","tgsar_anoPag6","gsar_ano",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Offerte")
      .Pages(6).HelpContextID = 152550
      .Pages(7).addobject("oPag","tgsar_anoPag7","gsar_ano",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Agenda attivit�")
      .Pages(7).HelpContextID = 121130919
      .Pages(8).addobject("oPag","tgsar_anoPag8","gsar_ano",8)
      .Pages(8).oPag.Visible=.t.
      .Pages(8).Caption=cp_Translate("Antiriciclaggio")
      .Pages(8).HelpContextID = 82099615
      .Pages(9).addobject("oPag","tgsar_anoPag9","gsar_ano",9)
      .Pages(9).oPag.Visible=.t.
      .Pages(9).Caption=cp_Translate("Note")
      .Pages(9).HelpContextID = 82001110
      .Pages(10).addobject("oPag","tgsar_anoPag10","gsar_ano",10)
      .Pages(10).oPag.Visible=.t.
      .Pages(10).Caption=cp_Translate("Dati DocFinance")
      .Pages(10).HelpContextID = 86998009
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNOCODICE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsar_ano
    * Se parametro � vuoto viene assegnato a N come per le offerte
    this.parent.pTipMask=iif(type("pParam")='C',pParam,'N')
    with THIS.PARENT
       if .pTipMask='B'
         .cComment = cp_Translate('Debitori / creditori diversi')
         .cqueryfilter=iif(empty(nvl(.cQueryFilter,'')),"NOFLGBEN='B'","("+.cQueryFilter+ ") AND NOFLGBEN='B'")
         .cAutoZoom = 'GSAR10ACN'
       else
         .cQueryFilter =iif(empty(nvl(.cQueryFilter,'')),"(NOFLGBEN=' ' or NOFLGBEN is null)","("+.cQueryFilter+ ") AND (NOFLGBEN=' ' or NOFLGBEN is null)")
       endif
    endwith
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_ZoomAll = this.oPgFrm.Pages(3).oPag.ZoomAll
      this.w_ZoomOff = this.oPgFrm.Pages(6).oPag.ZoomOff
      this.w_ZOOM = this.oPgFrm.Pages(7).oPag.ZOOM
      DoDefault()
    proc Destroy()
      this.w_ZoomAll = .NULL.
      this.w_ZoomOff = .NULL.
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[36]
    this.cWorkTables[1]='BAN_CHE'
    this.cWorkTables[2]='AGENTI'
    this.cWorkTables[3]='PAG_AMEN'
    this.cWorkTables[4]='LISTINI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='GRUPRO'
    this.cWorkTables[7]='CATECOMM'
    this.cWorkTables[8]='LINGUE'
    this.cWorkTables[9]='CAT_SCMA'
    this.cWorkTables[10]='NAZIONI'
    this.cWorkTables[11]='CACOCLFO'
    this.cWorkTables[12]='CONTI'
    this.cWorkTables[13]='ZONE'
    this.cWorkTables[14]='VOCIIVA'
    this.cWorkTables[15]='DES_DIVE'
    this.cWorkTables[16]='MASTRI'
    this.cWorkTables[17]='COC_MAST'
    this.cWorkTables[18]='SIT_FIDI'
    this.cWorkTables[19]='COD_ABI'
    this.cWorkTables[20]='COD_CAB'
    this.cWorkTables[21]='BAN_CONTI'
    this.cWorkTables[22]='DIPENDEN'
    this.cWorkTables[23]='GRU_NOMI'
    this.cWorkTables[24]='ORI_NOMI'
    this.cWorkTables[25]='GRU_PRIO'
    this.cWorkTables[26]='cpusers'
    this.cWorkTables[27]='CPUSERS'
    this.cWorkTables[28]='SAL_NOMI'
    this.cWorkTables[29]='COD_CATA'
    this.cWorkTables[30]='NOM_CONT'
    this.cWorkTables[31]='NAT_GIUR'
    this.cWorkTables[32]='CAANCLFO'
    this.cWorkTables[33]='VOC_FINZ'
    this.cWorkTables[34]='DORATING'
    this.cWorkTables[35]='CAT_FINA'
    this.cWorkTables[36]='OFF_NOMI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(36))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.OFF_NOMI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.OFF_NOMI_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MCC = CREATEOBJECT('stdDynamicChild',this,'GSAR_MCC',this.oPgFrm.Page2.oPag.oLinkPC_2_30)
    this.GSAR_MCC.createrealchild()
    this.GSAR_MAN = CREATEOBJECT('stdDynamicChild',this,'GSAR_MAN',this.oPgFrm.Page4.oPag.oLinkPC_4_28)
    this.GSAR_MCN = CREATEOBJECT('stdDynamicChild',this,'GSAR_MCN',this.oPgFrm.Page5.oPag.oLinkPC_5_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MCC)
      this.GSAR_MCC.DestroyChildrenChain()
      this.GSAR_MCC=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_30')
    if !ISNULL(this.GSAR_MAN)
      this.GSAR_MAN.DestroyChildrenChain()
      this.GSAR_MAN=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_28')
    if !ISNULL(this.GSAR_MCN)
      this.GSAR_MCN.DestroyChildrenChain()
      this.GSAR_MCN=.NULL.
    endif
    this.oPgFrm.Page5.oPag.RemoveObject('oLinkPC_5_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MCC.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MAN.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MCN.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MCC.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MAN.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MCN.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MCC.NewDocument()
    this.GSAR_MAN.NewDocument()
    this.GSAR_MCN.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAR_MCC.SetKey(;
            .w_NOTIPBAN,"CCTIPCON";
            ,.w_NOCODICE,"CCCODCON";
            )
      this.GSAR_MAN.SetKey(;
            .w_NOCODICE,"ANCODICE";
            )
      this.GSAR_MCN.SetKey(;
            .w_NOCODICE,"NCCODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAR_MCC.ChangeRow(this.cRowID+'      1',1;
             ,.w_NOTIPBAN,"CCTIPCON";
             ,.w_NOCODICE,"CCCODCON";
             )
      .GSAR_MAN.ChangeRow(this.cRowID+'      1',1;
             ,.w_NOCODICE,"ANCODICE";
             )
      .GSAR_MCN.ChangeRow(this.cRowID+'      1',1;
             ,.w_NOCODICE,"NCCODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAR_MCC)
        i_f=.GSAR_MCC.BuildFilter()
        if !(i_f==.GSAR_MCC.cQueryFilter)
          i_fnidx=.GSAR_MCC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MCC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MCC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MCC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MCC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MAN)
        i_f=.GSAR_MAN.BuildFilter()
        if !(i_f==.GSAR_MAN.cQueryFilter)
          i_fnidx=.GSAR_MAN.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MAN.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MAN.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MAN.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MAN.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MCN)
        i_f=.GSAR_MCN.BuildFilter()
        if !(i_f==.GSAR_MCN.cQueryFilter)
          i_fnidx=.GSAR_MCN.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MCN.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MCN.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MCN.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MCN.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_NOCODICE = NVL(NOCODICE,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_24_joined
    link_1_24_joined=.f.
    local link_1_39_joined
    link_1_39_joined=.f.
    local link_1_50_joined
    link_1_50_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_13_joined
    link_2_13_joined=.f.
    local link_4_6_joined
    link_4_6_joined=.f.
    local link_4_9_joined
    link_4_9_joined=.f.
    local link_4_17_joined
    link_4_17_joined=.f.
    local link_4_20_joined
    link_4_20_joined=.f.
    local link_4_23_joined
    link_4_23_joined=.f.
    local link_4_24_joined
    link_4_24_joined=.f.
    local link_4_44_joined
    link_4_44_joined=.f.
    local link_4_45_joined
    link_4_45_joined=.f.
    local link_4_46_joined
    link_4_46_joined=.f.
    local link_4_47_joined
    link_4_47_joined=.f.
    local link_4_48_joined
    link_4_48_joined=.f.
    local link_4_49_joined
    link_4_49_joined=.f.
    local link_4_50_joined
    link_4_50_joined=.f.
    local link_8_11_joined
    link_8_11_joined=.f.
    local link_10_4_joined
    link_10_4_joined=.f.
    local link_10_6_joined
    link_10_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from OFF_NOMI where NOCODICE=KeySet.NOCODICE
    *
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('OFF_NOMI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "OFF_NOMI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' OFF_NOMI '
      link_1_24_joined=this.AddJoinedLink_1_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_39_joined=this.AddJoinedLink_1_39(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_50_joined=this.AddJoinedLink_1_50(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_6_joined=this.AddJoinedLink_4_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_9_joined=this.AddJoinedLink_4_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_17_joined=this.AddJoinedLink_4_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_20_joined=this.AddJoinedLink_4_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_23_joined=this.AddJoinedLink_4_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_24_joined=this.AddJoinedLink_4_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_44_joined=this.AddJoinedLink_4_44(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_45_joined=this.AddJoinedLink_4_45(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_46_joined=this.AddJoinedLink_4_46(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_47_joined=this.AddJoinedLink_4_47(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_48_joined=this.AddJoinedLink_4_48(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_49_joined=this.AddJoinedLink_4_49(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_50_joined=this.AddJoinedLink_4_50(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_8_11_joined=this.AddJoinedLink_8_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_10_4_joined=this.AddJoinedLink_10_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_10_6_joined=this.AddJoinedLink_10_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'NOCODICE',this.w_NOCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_datsys
        .w_VALATT = .w_NOCODICE
        .w_TABKEY = 'OFF_NOMI'
        .w_DELIMM = .f.
        .w_TIPRIS = 'P'
        .w_CAP = IIF(upper(g_APPLICATION) = "ADHOC REVOLUTION", SPACE(8), SPACE(9) )
        .w_LOCALD = space(40)
        .w_DESNAZ1 = space(35)
        .w_CODISO1 = space(3)
        .w_FL_SOG_RIT = 'N'
        .w_ERR = space(10)
        .w_DATOBSO = ctod("  /  /  ")
        .w_CONSBF = space(1)
        .w_VALLIS = space(3)
        .w_IVALIS = space(1)
        .w_TIPO = 'C'
        .w_INIVAL = ctod("  /  /  ")
        .w_FINVAL = ctod("  /  /  ")
        .w_DATOBS = ctod("  /  /  ")
        .w_COMODO = space(10)
        .w_LOCNAS = space(40)
        .w_DPFLINEX = space(1)
        .w_CODCOM = space(4)
        .w_CONTA = .w_CONTA
        .w_NOTIPBAN = 'B'
        .w_DESPAG = space(30)
        .w_DESBA2 = space(42)
        .w_DESBAN = space(42)
        .w_ANDESCRI = space(40)
        .w_DATOBSOPAG = ctod("  /  /  ")
        .w_DATOBSOBA2 = ctod("  /  /  ")
        .w_DATOBSN = ctod("  /  /  ")
        .w_BANCAAHE = space(10)
        .w_OLDANTIIDRI = 1
        .w_OLDANCOIDRI = ''
        .w_ANFLRITE = 'N'
        .w_ANTIPCLF = space(1)
        .w_ANPARTSN = space(1)
        .w_CFDESCRIC = space(30)
        .w_CFDESCRID = space(30)
        .w_TIPOFIN = 'C'
        .w_TIPOFIND = 'D'
        .w_NONOMFIL = space(30)
        .w_NOOGGE = space(30)
        .w_NONOTE = space(30)
        .w_OF__NOTE = space(0)
        .w_NODESGRU = space(35)
        .w_NODESORI = space(35)
        .w_NODESOPE = space(35)
        .w_NODESZON = space(35)
        .w_NODESLIN = space(35)
        .w_NODESVAL = space(35)
        .w_DTOBZON = ctod("  /  /  ")
        .w_DTOBVAL = ctod("  /  /  ")
        .w_DTOBAGE = ctod("  /  /  ")
        .w_NODESAGE = space(35)
        .w_DESBA3 = space(42)
        .w_DESBA2 = space(42)
        .w_DESBAN = space(42)
        .w_DESPAG = space(30)
        .w_DESCAC = space(35)
        .w_DESLIS = space(40)
        .w_COGNOME = space(40)
        .w_NOME = space(40)
        .w_DESCON = space(35)
        .w_DESSCATCLFO = space(35)
        .w_CodiceFittizio = 'X*?-X*?-X*?-X*?-X*?-'
        .w_ForzaNom = space(1)
        .w_SAL_Dest = space(5)
        .w_RAGSOC_Dest = space(100)
        .w_INDIRI_Dest = space(100)
        .w_LOCALI_Dest = space(100)
        .w_CAP_Dest = space(9)
        .w_PROV_Dest = space(2)
        .w_NAZ_Dest = space(3)
        .w_CortAtt_Dest = space(100)
        .w_OFSTATUS = 'I'
        .w_NFDTOF1 = ctod("  /  /  ")
        .w_NFDTOF2 = ctod("  /  /  ")
        .w_NFDTRC1 = ctod("  /  /  ")
        .w_NFDTRC2 = ctod("  /  /  ")
        .w_NFGRPRIO = space(5)
        .w_NFRIFDE = space(45)
        .w_NUMINI = space(10)
        .w_DTOBGR = ctod("  /  /  ")
        .w_NUMFIN = space(10)
        .w_OPERAT = 0
        .w_STATO = iif(g_AGEN='S','N','A')
        .w_NAME = space(20)
        .w_STATO = iif(g_AGEN='S','N','A')
        .w_DATAINI = ctod("  /  /  ")
        .w_ORAINI = '00'
        .w_MININI = '00'
        .w_DATAFIN = ctod("  /  /  ")
        .w_ORAFIN = '23'
        .w_MINFIN = '59'
        .w_SUBJECT = space(254)
        .w_CONTINI = space(5)
        .w_PRIOINI = 0
        .w_PRIOINI = 0
        .w_TIPOATINI = space(5)
        .w_TIPOATFIN = space(5)
        .w_ATDATINI = ctot("")
        .w_ATDATFIN = ctot("")
        .w_ATDATINI1 = ctot("")
        .w_ATDATFIN1 = ctot("")
        .w_ESITOATI = space(5)
        .w_ESITOATF = space(5)
        .w_DESCPER = space(40)
        .w_CODUTE = 0
        .w_NGDESCRI = space(50)
        .w_OLD_NOFLANTI = space(1)
        .w_TipoRisorsa = 'P'
        .w_OB_TEST = i_INIDAT
        .w_DFDESCRI = space(60)
        .w_RADESCRI = space(50)
        .w_TELFAX = space(18)
        .w_APEMAIL = space(254)
        .w_APEMPEC = space(254)
        .w_PARCALLER = this.pTipMask
        .w_NOCODICE = NVL(NOCODICE,space(15))
        .op_NOCODICE = .w_NOCODICE
        .w_NOCODCLI = NVL(NOCODCLI,space(15))
          * evitabile
          *.link_1_4('Load')
        .w_NODESCRI = NVL(NODESCRI,space(60))
        .w_NODESCR2 = NVL(NODESCR2,space(60))
        .w_NOSOGGET = NVL(NOSOGGET,space(2))
        .w_NOCOD_RU = NVL(NOCOD_RU,space(5))
          .link_1_12('Load')
        .w_NOBADGE = NVL(NOBADGE,space(20))
        .w_NOCOGNOM = NVL(NOCOGNOM,space(50))
        .w_NOCOGNOM = NVL(NOCOGNOM,space(50))
        .w_NO__NOME = NVL(NO__NOME,space(50))
        .w_NO__NOME = NVL(NO__NOME,space(50))
        .w_NOLOCNAS = NVL(NOLOCNAS,space(30))
        .w_NOLOCNAS = NVL(NOLOCNAS,space(30))
        .w_NOPRONAS = NVL(NOPRONAS,space(2))
        .w_NOPRONAS = NVL(NOPRONAS,space(2))
        .w_NODATNAS = NVL(cp_ToDate(NODATNAS),ctod("  /  /  "))
        .w_NO_SESSO = NVL(NO_SESSO,space(1))
        .w_NONAZNAS = NVL(NONAZNAS,space(3))
          if link_1_24_joined
            this.w_NONAZNAS = NVL(NACODNAZ124,NVL(this.w_NONAZNAS,space(3)))
            this.w_DESNAZ1 = NVL(NADESNAZ124,space(35))
            this.w_CODISO1 = NVL(NACODISO124,space(3))
          else
          .link_1_24('Load')
          endif
        .w_NONUMCAR = NVL(NONUMCAR,space(18))
        .w_NOCODFIS = NVL(NOCODFIS,space(16))
        .w_NOPARIVA = NVL(NOPARIVA,space(12))
        .w_NONATGIU = NVL(NONATGIU,space(3))
        .w_NOCODSAL = NVL(NOCODSAL,space(5))
          * evitabile
          *.link_1_29('Load')
        .w_NOINDIRI = NVL(NOINDIRI,space(35))
        .w_NOINDI_2 = NVL(NOINDI_2,space(35))
        .w_NO___CAP = NVL(NO___CAP,space(8))
        .w_NO___CAP = NVL(NO___CAP,space(9))
        .w_NOLOCALI = NVL(NOLOCALI,space(30))
        .w_NOLOCALI = NVL(NOLOCALI,space(30))
        .w_NOPROVIN = NVL(NOPROVIN,space(2))
        .w_NOPROVIN = NVL(NOPROVIN,space(2))
        .w_NONAZION = NVL(NONAZION,space(3))
          if link_1_39_joined
            this.w_NONAZION = NVL(NACODNAZ139,NVL(this.w_NONAZION,space(3)))
            this.w_COMODO = NVL(NADESNAZ139,space(10))
          else
          .link_1_39('Load')
          endif
        .w_NOINDIR2 = NVL(NOINDIR2,space(35))
        .w_NO___CAD = NVL(NO___CAD,space(9))
        .w_NO___CAD = NVL(NO___CAD,space(9))
        .w_NOLOCALD = NVL(NOLOCALD,space(30))
        .w_OCOGNOM = .w_NOCOGNOM
        .w_ONOME = .w_NO__NOME
        .w_NOLOCALD = NVL(NOLOCALD,space(30))
        .w_NOPROVID = NVL(NOPROVID,space(2))
        .w_NOPROVID = NVL(NOPROVID,space(2))
        .w_NONAZIOD = NVL(NONAZIOD,space(3))
          if link_1_50_joined
            this.w_NONAZIOD = NVL(NACODNAZ150,NVL(this.w_NONAZIOD,space(3)))
            this.w_COMODO = NVL(NADESNAZ150,space(10))
          else
          .link_1_50('Load')
          endif
        .w_NOTELEFO = NVL(NOTELEFO,space(18))
        .w_NONUMCEL = NVL(NONUMCEL,space(18))
        .w_NO_SKYPE = NVL(NO_SKYPE,space(50))
        .w_NOTELFAX = NVL(NOTELFAX,space(18))
        .w_NO_EMAIL = NVL(NO_EMAIL,space(254))
        .w_NOINDWEB = NVL(NOINDWEB,space(254))
        .w_NO_EMPEC = NVL(NO_EMPEC,space(254))
        .w_NOCHKSTA = NVL(NOCHKSTA,space(1))
        .w_NOCHKMAI = NVL(NOCHKMAI,space(1))
        .w_NOCHKPEC = NVL(NOCHKPEC,space(1))
        .w_NOCHKFAX = NVL(NOCHKFAX,space(1))
        .w_NOCHKCPZ = NVL(NOCHKCPZ,space(1))
        .w_NOCHKCPZ = NVL(NOCHKCPZ,space(1))
        .w_NOCHKWWP = NVL(NOCHKWWP,space(1))
        .w_NOCHKPTL = NVL(NOCHKPTL,space(1))
        .w_NODTINVA = NVL(cp_ToDate(NODTINVA),ctod("  /  /  "))
        .w_NODTOBSO = NVL(cp_ToDate(NODTOBSO),ctod("  /  /  "))
        .w_NOSOGGET = NVL(NOSOGGET,space(2))
        .w_NOTIPPER = NVL(NOTIPPER,space(1))
        .w_NOTIPNOM = NVL(NOTIPNOM,space(1))
        .w_NOTIPNOM = NVL(NOTIPNOM,space(1))
        .w_NOTIPNOM = NVL(NOTIPNOM,space(1))
        .w_NOFLGBEN = NVL(NOFLGBEN,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_TIPOPE = UPPER(this.cFunction)
        .w_NOTIPCLI = NVL(NOTIPCLI,space(1))
        .w_OBTEST = i_datsys
        .w_NOTIPO = .w_NOTIPNOM
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_108.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_128.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_129.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_147.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_162.Calculate()
        .w_NOTIPCON = NVL(NOTIPCON,space(1))
        .w_NOCODCON = NVL(NOCODCON,space(15))
          if link_2_3_joined
            this.w_NOCODCON = NVL(ANCODICE203,NVL(this.w_NOCODCON,space(15)))
            this.w_ANDESCRI = NVL(ANDESCRI203,space(40))
            this.w_NOCODPAG = NVL(ANCODPAG203,space(5))
            this.w_NOGIOFIS = NVL(ANGIOFIS203,0)
            this.w_NO1MESCL = NVL(AN1MESCL203,0)
            this.w_NO2MESCL = NVL(AN2MESCL203,0)
            this.w_NOGIOSC1 = NVL(ANGIOSC1203,0)
            this.w_NOGIOSC2 = NVL(ANGIOSC2203,0)
            this.w_NOCODBAN = NVL(ANCODBAN203,space(10))
            this.w_DATOBSN = NVL(cp_ToDate(ANDTOBSO203),ctod("  /  /  "))
            this.w_ANRITENU = NVL(ANRITENU203,space(10))
            this.w_ANFLRITE = NVL(ANFLRITE203,space(1))
            this.w_ANTIPCLF = NVL(ANTIPCLF203,space(1))
            this.w_ANPARTSN = NVL(ANPARTSN203,space(1))
          else
          .link_2_3('Load')
          endif
        .w_NOCODPAG = NVL(NOCODPAG,space(5))
          if link_2_4_joined
            this.w_NOCODPAG = NVL(PACODICE204,NVL(this.w_NOCODPAG,space(5)))
            this.w_DESPAG = NVL(PADESCRI204,space(30))
            this.w_DATOBSOPAG = NVL(cp_ToDate(PADTOBSO204),ctod("  /  /  "))
          else
          .link_2_4('Load')
          endif
        .w_NOGIOFIS = NVL(NOGIOFIS,0)
        .w_NO1MESCL = NVL(NO1MESCL,0)
        .w_DESMES1 = LEFT(IIF(.w_NO1MESCL>0 AND .w_NO1MESCL<13, g_MESE[.w_NO1MESCL], "")+SPACE(12),12)
        .w_NOGIOSC1 = NVL(NOGIOSC1,0)
        .w_NO2MESCL = NVL(NO2MESCL,0)
        .w_NOGIOSC2 = NVL(NOGIOSC2,0)
        .w_DESMES2 = LEFT(IIF(.w_NO2MESCL>0 AND .w_NO2MESCL<13, g_MESE[.w_NO2MESCL], "")+SPACE(12),12)
        .w_NOCODBAN = NVL(NOCODBAN,space(10))
          if link_2_13_joined
            this.w_NOCODBAN = NVL(BACODBAN213,NVL(this.w_NOCODBAN,space(10)))
            this.w_DESBAN = NVL(BADESBAN213,space(42))
          else
          .link_2_13('Load')
          endif
        .w_NOCODBA2 = NVL(NOCODBA2,space(15))
        .w_NOFLRAGG = NVL(NOFLRAGG,space(1))
        .w_NOFLGAVV = NVL(NOFLGAVV,space(1))
        .w_NODATAVV = NVL(cp_ToDate(NODATAVV),ctod("  /  /  "))
        .w_NOINTMOR = NVL(NOINTMOR,space(1))
        .w_NOMORTAS = NVL(NOMORTAS,0)
        .oPgFrm.Page2.oPag.oObj_2_27.Calculate()
        .w_NOCODICE = NVL(NOCODICE,space(15))
        .op_NOCODICE = .w_NOCODICE
        .w_NOTIPPER = NVL(NOTIPPER,space(1))
          .link_2_43('Load')
        .w_NOCATFIN = NVL(NOCATFIN,space(5))
          .link_2_44('Load')
        .w_NOCATDER = NVL(NOCATDER,space(5))
          .link_2_45('Load')
        .w_NOIDRIDY = NVL(NOIDRIDY,space(1))
        .w_NOTIIDRI = NVL(NOTIIDRI,0)
        .w_NOCOIDRI = NVL(NOCOIDRI,space(16))
        .w_ANRITENU = 'N'
        .w_NOCOGNOM = NVL(NOCOGNOM,space(50))
        .w_NO__NOME = NVL(NO__NOME,space(50))
        .w_NODESCRI = NVL(NODESCRI,space(40))
        .w_NODESCR2 = NVL(NODESCR2,space(40))
        .w_NONUMCOR = NVL(NONUMCOR,space(25))
        .oPgFrm.Page3.oPag.ZoomAll.Calculate()
        .w_SERALL = NVL(.w_ZoomAll.getVar('ALSERIAL'), SPACE(10))
        .w_PATALL = NVL(.w_ZoomAll.getVar('ALPATFIL'), SPACE(254))
        .w_CODI = .w_NOCODICE
        .w_NOTIPPER = NVL(NOTIPPER,space(1))
        .w_NOCOGNOM = NVL(NOCOGNOM,space(50))
        .w_NO__NOME = NVL(NO__NOME,space(50))
        .w_NODESCRI = NVL(NODESCRI,space(40))
        .w_NODESCR2 = NVL(NODESCR2,space(40))
        .w_CODI = .w_NOCODICE
        .w_NOCODPRI = NVL(NOCODPRI,0)
        .w_NOPRIVCY = NVL(NOPRIVCY,space(1))
        .w_NOCODGRU = NVL(NOCODGRU,space(5))
          if link_4_6_joined
            this.w_NOCODGRU = NVL(GNCODICE406,NVL(this.w_NOCODGRU,space(5)))
            this.w_NODESGRU = NVL(GNDESCRI406,space(35))
          else
          .link_4_6('Load')
          endif
        .w_NOCODORI = NVL(NOCODORI,space(5))
          if link_4_9_joined
            this.w_NOCODORI = NVL(ONCODICE409,NVL(this.w_NOCODORI,space(5)))
            this.w_NODESORI = NVL(ONDESCRI409,space(35))
          else
          .link_4_9('Load')
          endif
        .w_NORIFINT = NVL(NORIFINT,space(5))
          .link_4_12('Load')
        .w_NOCODOPE = NVL(NOCODOPE,0)
          .link_4_13('Load')
        .w_NOASSOPE = NVL(cp_ToDate(NOASSOPE),ctod("  /  /  "))
        .w_NOCODZON = NVL(NOCODZON,space(3))
          if link_4_17_joined
            this.w_NOCODZON = NVL(ZOCODZON417,NVL(this.w_NOCODZON,space(3)))
            this.w_NODESZON = NVL(ZODESZON417,space(35))
            this.w_DTOBZON = NVL(cp_ToDate(ZODTOBSO417),ctod("  /  /  "))
          else
          .link_4_17('Load')
          endif
        .w_NOCODLIN = NVL(NOCODLIN,space(3))
          if link_4_20_joined
            this.w_NOCODLIN = NVL(LUCODICE420,NVL(this.w_NOCODLIN,space(3)))
            this.w_NODESLIN = NVL(LUDESCRI420,space(35))
          else
          .link_4_20('Load')
          endif
        .w_NOCODVAL = NVL(NOCODVAL,space(3))
          if link_4_23_joined
            this.w_NOCODVAL = NVL(VACODVAL423,NVL(this.w_NOCODVAL,space(3)))
            this.w_NODESVAL = NVL(VADESVAL423,space(35))
            this.w_DTOBVAL = NVL(cp_ToDate(VADTOBSO423),ctod("  /  /  "))
          else
          .link_4_23('Load')
          endif
        .w_NOCODAGE = NVL(NOCODAGE,space(5))
          if link_4_24_joined
            this.w_NOCODAGE = NVL(AGCODAGE424,NVL(this.w_NOCODAGE,space(5)))
            this.w_NODESAGE = NVL(AGDESAGE424,space(35))
            this.w_DTOBAGE = NVL(cp_ToDate(AGDTOBSO424),ctod("  /  /  "))
          else
          .link_4_24('Load')
          endif
        .w_NOTIPPER = NVL(NOTIPPER,space(1))
        .w_NOCOGNOM = NVL(NOCOGNOM,space(50))
        .w_NO__NOME = NVL(NO__NOME,space(50))
        .w_NODESCRI = NVL(NODESCRI,space(40))
        .w_NODESCR2 = NVL(NODESCR2,space(40))
        .w_NONUMLIS = NVL(NONUMLIS,space(5))
          if link_4_44_joined
            this.w_NONUMLIS = NVL(LSCODLIS444,NVL(this.w_NONUMLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS444,space(40))
            this.w_VALLIS = NVL(LSVALLIS444,space(3))
            this.w_IVALIS = NVL(LSIVALIS444,space(1))
          else
          .link_4_44('Load')
          endif
        .w_NONUMLIS = NVL(NONUMLIS,space(5))
          if link_4_45_joined
            this.w_NONUMLIS = NVL(LSCODLIS445,NVL(this.w_NONUMLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS445,space(40))
            this.w_VALLIS = NVL(LSVALLIS445,space(3))
            this.w_IVALIS = NVL(LSIVALIS445,space(1))
            this.w_INIVAL = NVL(cp_ToDate(LSDTINVA445),ctod("  /  /  "))
            this.w_FINVAL = NVL(cp_ToDate(LSDTOBSO445),ctod("  /  /  "))
          else
          .link_4_45('Load')
          endif
        .w_NOCODPAG = NVL(NOCODPAG,space(5))
          if link_4_46_joined
            this.w_NOCODPAG = NVL(PACODICE446,NVL(this.w_NOCODPAG,space(5)))
            this.w_DESPAG = NVL(PADESCRI446,space(30))
            this.w_DATOBSOPAG = NVL(cp_ToDate(PADTOBSO446),ctod("  /  /  "))
          else
          .link_4_46('Load')
          endif
        .w_NOCODBAN = NVL(NOCODBAN,space(10))
          if link_4_47_joined
            this.w_NOCODBAN = NVL(BACODBAN447,NVL(this.w_NOCODBAN,space(10)))
            this.w_DESBAN = NVL(BADESBAN447,space(42))
          else
          .link_4_47('Load')
          endif
        .w_NOCATCOM = NVL(NOCATCOM,space(3))
          if link_4_48_joined
            this.w_NOCATCOM = NVL(CTCODICE448,NVL(this.w_NOCATCOM,space(3)))
            this.w_DESCAC = NVL(CTDESCRI448,space(35))
          else
          .link_4_48('Load')
          endif
        .w_NOCATCON = NVL(NOCATCON,space(5))
          if link_4_49_joined
            this.w_NOCATCON = NVL(C2CODICE449,NVL(this.w_NOCATCON,space(5)))
            this.w_DESCON = NVL(C2DESCRI449,space(35))
          else
          .link_4_49('Load')
          endif
        .w_NOCACLFO = NVL(NOCACLFO,space(5))
          if link_4_50_joined
            this.w_NOCACLFO = NVL(C1CODICE450,NVL(this.w_NOCACLFO,space(5)))
            this.w_DESSCATCLFO = NVL(C1DESCRI450,space(35))
          else
          .link_4_50('Load')
          endif
        .w_BANCAAHR = IIF (g_APPLICATION <> "ADHOC REVOLUTION" ,.w_BANCAAHR,.w_NOCODBA2)
          .link_4_51('Load')
        .w_BANCAAHE = IIF (g_APPLICATION = "ADHOC REVOLUTION" ,.w_BANCAAHE , .w_NOCODBA2)
          .link_4_52('Load')
        .w_NODENOMRIF = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        .w_CODI = .w_NOCODICE
        .w_NOTIPPER = NVL(NOTIPPER,space(1))
        .w_NOCOGNOM = NVL(NOCOGNOM,space(50))
        .w_NO__NOME = NVL(NO__NOME,space(50))
        .w_NODESCRI = NVL(NODESCRI,space(40))
        .w_NODESCR2 = NVL(NODESCR2,space(40))
        .oPgFrm.Page6.oPag.ZoomOff.Calculate()
        .w_SEROFF = NVL(.w_ZoomOff.getVar('OFSERIAL'), SPACE(10))
          .link_6_17('Load')
        .w_CODI = .w_NOCODICE
        .w_OFDATDOC = NVL(.w_ZoomOff.getVar('OFDATDOC'), cp_CharToDate('  /  /  '))
        .w_OFPATFWP = NVL(.w_ZoomOff.getVar('OFPATFWP'), SPACE(254))
        .w_OFPATPDF = NVL(.w_ZoomOff.getVar('OFPATPDF'), SPACE(254))
        .w_PATHARC = NVL(.w_ZoomOff.getVar('MOPATARC'), SPACE(254))
        .w_PERIODO = NVL(.w_ZoomOff.getVar('MONUMPER'), SPACE(1))
        .w_OFCODNOM = .w_NOCODICE
        .w_NOTIPPER = NVL(NOTIPPER,space(1))
        .w_NOCOGNOM = NVL(NOCOGNOM,space(50))
        .w_NO__NOME = NVL(NO__NOME,space(50))
        .w_NODESCRI = NVL(NODESCRI,space(40))
        .w_NODESCR2 = NVL(NODESCR2,space(40))
        .oPgFrm.Page7.oPag.ZOOM.Calculate()
          .link_7_2('Load')
          .link_7_15('Load')
        .w_ATSERIAL = .w_ZOOM.GetVar('ATSERIAL')
        .w_OGGETTO = '%'+ALLTRIM(.w_SUBJECT)
        .w_NOMINI = .w_NOCODICE
        .w_NOMFIN = .w_NOCODICE
        .w_PRIOFIN = .w_PRIOINI
        .w_OFF_SER = ''
        .w_NODATINS = NVL(cp_ToDate(NODATINS),ctod("  /  /  "))
        .w_NO__RESP = NVL(NO__RESP,space(5))
          .link_8_2('Load')
        .w_DATI = iif(!empty(.w_NO__RESP), ReadDipend(.w_CODUTE,"D"), space(40) )
        .w_NOTIPOID = NVL(NOTIPOID,space(2))
        .w_NOTIPDOC = NVL(NOTIPDOC,space(2))
        .w_NONUMDOC = NVL(NONUMDOC,space(15))
        .w_NODATRIL = NVL(cp_ToDate(NODATRIL),ctod("  /  /  "))
        .w_NOAUTLOC = NVL(NOAUTLOC,space(30))
        .w_NOATTLAV = NVL(NOATTLAV,space(50))
        .w_NONAGIUR = NVL(NONAGIUR,space(4))
          if link_8_11_joined
            this.w_NONAGIUR = NVL(NGCODICE811,NVL(this.w_NONAGIUR,space(4)))
            this.w_NGDESCRI = NVL(NGDESCRI811,space(50))
          else
          .link_8_11('Load')
          endif
        .w_NODATCOS = NVL(cp_ToDate(NODATCOS),ctod("  /  /  "))
        .w_NO_SIGLA = NVL(NO_SIGLA,space(30))
        .w_NO_PAESE = NVL(NO_PAESE,space(30))
        .w_NOESTNAS = NVL(NOESTNAS,space(30))
        .w_NOFLANTI = NVL(NOFLANTI,space(1))
        .w_NOFLANTI = NVL(NOFLANTI,space(1))
        .oPgFrm.Page8.oPag.oObj_8_44.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_45.Calculate()
        .w_NOCODICE = NVL(NOCODICE,space(15))
        .op_NOCODICE = .w_NOCODICE
        .w_NOCOGNOM = NVL(NOCOGNOM,space(50))
        .w_NODESCRI = NVL(NODESCRI,space(40))
        .w_NO__NOME = NVL(NO__NOME,space(50))
        .w_NODESCR2 = NVL(NODESCR2,space(40))
        .w_NO__NOTE = NVL(NO__NOTE,space(0))
        .w_CODI = .w_NOCODICE
        .w_NOTIPPER = NVL(NOTIPPER,space(1))
        .w_NOCOGNOM = NVL(NOCOGNOM,space(50))
        .w_NO__NOME = NVL(NO__NOME,space(50))
        .w_NODESCRI = NVL(NODESCRI,space(40))
        .w_NODESCR2 = NVL(NODESCR2,space(40))
        .w_EMAIL = evl(.w_NO_EMAIL,.w_APEMAIL)
        .w_NORATING = NVL(NORATING,space(2))
          if link_10_4_joined
            this.w_NORATING = NVL(RACODICE1004,NVL(this.w_NORATING,space(2)))
            this.w_RADESCRI = NVL(RADESCRI1004,space(50))
          else
          .link_10_4('Load')
          endif
        .w_NOGIORIT = NVL(NOGIORIT,0)
        .w_NOVOCFIN = NVL(NOVOCFIN,space(6))
          if link_10_6_joined
            this.w_NOVOCFIN = NVL(DFVOCFIN1006,NVL(this.w_NOVOCFIN,space(6)))
            this.w_DFDESCRI = NVL(DFDESCRI1006,space(60))
          else
          .link_10_6('Load')
          endif
        .w_NOESCDOF = NVL(NOESCDOF,space(1))
        .w_NODESPAR = NVL(NODESPAR,space(1))
        .w_NOCODICE = NVL(NOCODICE,space(15))
        .op_NOCODICE = .w_NOCODICE
        .w_NOTIPPER = NVL(NOTIPPER,space(1))
        .w_NOCOGNOM = NVL(NOCOGNOM,space(50))
        .w_NO__NOME = NVL(NO__NOME,space(50))
        .w_NODESCRI = NVL(NODESCRI,space(40))
        .w_NODESCR2 = NVL(NODESCR2,space(40))
        .w_EMPEC = evl(.w_NO_EMPEC,.w_APEMPEC)
        .w_OFCODMOD = NVL(.w_ZoomOff.getVar('OFCODMOD'), SPACE(5))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'OFF_NOMI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_130.enabled = this.oPgFrm.Page1.oPag.oBtn_1_130.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_132.enabled = this.oPgFrm.Page1.oPag.oBtn_1_132.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_133.enabled = this.oPgFrm.Page1.oPag.oBtn_1_133.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_134.enabled = this.oPgFrm.Page1.oPag.oBtn_1_134.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_135.enabled = this.oPgFrm.Page1.oPag.oBtn_1_135.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_136.enabled = this.oPgFrm.Page1.oPag.oBtn_1_136.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_137.enabled = this.oPgFrm.Page1.oPag.oBtn_1_137.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_138.enabled = this.oPgFrm.Page1.oPag.oBtn_1_138.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_139.enabled = this.oPgFrm.Page1.oPag.oBtn_1_139.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_140.enabled = this.oPgFrm.Page1.oPag.oBtn_1_140.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_141.enabled = this.oPgFrm.Page1.oPag.oBtn_1_141.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_168.enabled = this.oPgFrm.Page1.oPag.oBtn_1_168.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_169.enabled = this.oPgFrm.Page1.oPag.oBtn_1_169.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_170.enabled = this.oPgFrm.Page1.oPag.oBtn_1_170.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_5.enabled = this.oPgFrm.Page3.oPag.oBtn_3_5.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_6.enabled = this.oPgFrm.Page3.oPag.oBtn_3_6.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_7.enabled = this.oPgFrm.Page3.oPag.oBtn_3_7.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_9.enabled = this.oPgFrm.Page3.oPag.oBtn_3_9.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_13.enabled = this.oPgFrm.Page3.oPag.oBtn_3_13.mCond()
      this.oPgFrm.Page4.oPag.oBtn_4_4.enabled = this.oPgFrm.Page4.oPag.oBtn_4_4.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_2.enabled = this.oPgFrm.Page6.oPag.oBtn_6_2.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_3.enabled = this.oPgFrm.Page6.oPag.oBtn_6_3.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_6.enabled = this.oPgFrm.Page6.oPag.oBtn_6_6.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_7.enabled = this.oPgFrm.Page6.oPag.oBtn_6_7.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_8.enabled = this.oPgFrm.Page6.oPag.oBtn_6_8.mCond()
      this.oPgFrm.Page6.oPag.oBtn_6_20.enabled = this.oPgFrm.Page6.oPag.oBtn_6_20.mCond()
      this.oPgFrm.Page7.oPag.oBtn_7_40.enabled = this.oPgFrm.Page7.oPag.oBtn_7_40.mCond()
      this.oPgFrm.Page7.oPag.oBtn_7_42.enabled = this.oPgFrm.Page7.oPag.oBtn_7_42.mCond()
      this.oPgFrm.Page7.oPag.oBtn_7_43.enabled = this.oPgFrm.Page7.oPag.oBtn_7_43.mCond()
      this.oPgFrm.Page7.oPag.oBtn_7_45.enabled = this.oPgFrm.Page7.oPag.oBtn_7_45.mCond()
      this.oPgFrm.Page8.oPag.oBtn_8_42.enabled = this.oPgFrm.Page8.oPag.oBtn_8_42.mCond()
      this.oPgFrm.Page9.oPag.oBtn_9_2.enabled = this.oPgFrm.Page9.oPag.oBtn_9_2.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_180.enabled = this.oPgFrm.Page1.oPag.oBtn_1_180.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_ano
    this.notifyevent ("W_NOSOGGET CHANGED")
    
    * -- Vengono rieseguite la mHide e la mEnable dopo la valorizzazione
    * -- di w_OLD_NOFLANTI avvenuta all'evento Load. E' infatti presente
    * -- una condizione di hide sul campo NOFLANTI la quale dipende da OLD_NOFLANTI.
    this.mHideControls()
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARCALLER = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_NOCODICE = space(15)
      .w_NOCODCLI = space(15)
      .w_VALATT = space(20)
      .w_TABKEY = space(8)
      .w_DELIMM = .f.
      .w_NODESCRI = space(60)
      .w_NODESCR2 = space(60)
      .w_NOSOGGET = space(2)
      .w_TIPRIS = space(1)
      .w_NOCOD_RU = space(5)
      .w_NOBADGE = space(20)
      .w_NOCOGNOM = space(50)
      .w_NOCOGNOM = space(50)
      .w_NO__NOME = space(50)
      .w_NO__NOME = space(50)
      .w_NOLOCNAS = space(30)
      .w_NOLOCNAS = space(30)
      .w_NOPRONAS = space(2)
      .w_NOPRONAS = space(2)
      .w_NODATNAS = ctod("  /  /  ")
      .w_NO_SESSO = space(1)
      .w_NONAZNAS = space(3)
      .w_NONUMCAR = space(18)
      .w_NOCODFIS = space(16)
      .w_NOPARIVA = space(12)
      .w_NONATGIU = space(3)
      .w_NOCODSAL = space(5)
      .w_NOINDIRI = space(35)
      .w_NOINDI_2 = space(35)
      .w_CAP = space(9)
      .w_NO___CAP = space(8)
      .w_NO___CAP = space(9)
      .w_NOLOCALI = space(30)
      .w_NOLOCALI = space(30)
      .w_NOPROVIN = space(2)
      .w_NOPROVIN = space(2)
      .w_NONAZION = space(3)
      .w_NOINDIR2 = space(35)
      .w_NO___CAD = space(9)
      .w_NO___CAD = space(9)
      .w_NOLOCALD = space(30)
      .w_OCOGNOM = space(50)
      .w_ONOME = space(50)
      .w_LOCALD = space(40)
      .w_NOLOCALD = space(30)
      .w_NOPROVID = space(2)
      .w_NOPROVID = space(2)
      .w_NONAZIOD = space(3)
      .w_NOTELEFO = space(18)
      .w_NONUMCEL = space(18)
      .w_NO_SKYPE = space(50)
      .w_NOTELFAX = space(18)
      .w_NO_EMAIL = space(254)
      .w_NOINDWEB = space(254)
      .w_NO_EMPEC = space(254)
      .w_NOCHKSTA = space(1)
      .w_NOCHKMAI = space(1)
      .w_NOCHKPEC = space(1)
      .w_NOCHKFAX = space(1)
      .w_NOCHKCPZ = space(1)
      .w_NOCHKCPZ = space(1)
      .w_NOCHKWWP = space(1)
      .w_NOCHKPTL = space(1)
      .w_NODTINVA = ctod("  /  /  ")
      .w_NODTOBSO = ctod("  /  /  ")
      .w_NOSOGGET = space(2)
      .w_NOTIPPER = space(1)
      .w_NOTIPNOM = space(1)
      .w_NOTIPNOM = space(1)
      .w_NOTIPNOM = space(1)
      .w_NOFLGBEN = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_TIPOPE = space(4)
      .w_NOTIPCLI = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_NOTIPO = space(1)
      .w_DESNAZ1 = space(35)
      .w_CODISO1 = space(3)
      .w_FL_SOG_RIT = space(1)
      .w_ERR = space(10)
      .w_DATOBSO = ctod("  /  /  ")
      .w_CONSBF = space(1)
      .w_VALLIS = space(3)
      .w_IVALIS = space(1)
      .w_TIPO = space(1)
      .w_INIVAL = ctod("  /  /  ")
      .w_FINVAL = ctod("  /  /  ")
      .w_DATOBS = ctod("  /  /  ")
      .w_COMODO = space(10)
      .w_LOCNAS = space(40)
      .w_DPFLINEX = space(1)
      .w_CODCOM = space(4)
      .w_CONTA = 0
      .w_NOTIPBAN = space(1)
      .w_NOTIPCON = space(1)
      .w_NOCODCON = space(15)
      .w_NOCODPAG = space(5)
      .w_DESPAG = space(30)
      .w_NOGIOFIS = 0
      .w_NO1MESCL = 0
      .w_DESMES1 = space(12)
      .w_NOGIOSC1 = 0
      .w_NO2MESCL = 0
      .w_NOGIOSC2 = 0
      .w_DESMES2 = space(12)
      .w_NOCODBAN = space(10)
      .w_NOCODBA2 = space(15)
      .w_NOFLRAGG = space(1)
      .w_NOFLGAVV = space(1)
      .w_NODATAVV = ctod("  /  /  ")
      .w_NOINTMOR = space(1)
      .w_NOMORTAS = 0
      .w_DESBA2 = space(42)
      .w_DESBAN = space(42)
      .w_NOCODICE = space(15)
      .w_ANDESCRI = space(40)
      .w_NOTIPPER = space(1)
      .w_DATOBSOPAG = ctod("  /  /  ")
      .w_DATOBSOBA2 = ctod("  /  /  ")
      .w_DATOBSN = ctod("  /  /  ")
      .w_BANCAAHE = space(10)
      .w_NOCATFIN = space(5)
      .w_NOCATDER = space(5)
      .w_NOIDRIDY = space(1)
      .w_NOTIIDRI = 0
      .w_NOCOIDRI = space(16)
      .w_OLDANTIIDRI = space(1)
      .w_OLDANCOIDRI = space(16)
      .w_ANRITENU = space(10)
      .w_ANFLRITE = space(1)
      .w_ANTIPCLF = space(1)
      .w_NOCOGNOM = space(50)
      .w_NO__NOME = space(50)
      .w_NODESCRI = space(40)
      .w_NODESCR2 = space(40)
      .w_NONUMCOR = space(25)
      .w_ANPARTSN = space(1)
      .w_CFDESCRIC = space(30)
      .w_CFDESCRID = space(30)
      .w_TIPOFIN = space(1)
      .w_TIPOFIND = space(1)
      .w_NONOMFIL = space(30)
      .w_NOOGGE = space(30)
      .w_NONOTE = space(30)
      .w_SERALL = space(10)
      .w_PATALL = space(254)
      .w_CODI = space(15)
      .w_OF__NOTE = space(0)
      .w_NOTIPPER = space(1)
      .w_NOCOGNOM = space(50)
      .w_NO__NOME = space(50)
      .w_NODESCRI = space(40)
      .w_NODESCR2 = space(40)
      .w_CODI = space(15)
      .w_NOCODPRI = 0
      .w_NOPRIVCY = space(1)
      .w_NOCODGRU = space(5)
      .w_NODESGRU = space(35)
      .w_NOCODORI = space(5)
      .w_NODESORI = space(35)
      .w_NORIFINT = space(5)
      .w_NOCODOPE = 0
      .w_NODESOPE = space(35)
      .w_NOASSOPE = ctod("  /  /  ")
      .w_NOCODZON = space(3)
      .w_NODESZON = space(35)
      .w_NOCODLIN = space(3)
      .w_NODESLIN = space(35)
      .w_NOCODVAL = space(3)
      .w_NOCODAGE = space(5)
      .w_NODESVAL = space(35)
      .w_DTOBZON = ctod("  /  /  ")
      .w_DTOBVAL = ctod("  /  /  ")
      .w_DTOBAGE = ctod("  /  /  ")
      .w_NODESAGE = space(35)
      .w_NOTIPPER = space(1)
      .w_NOCOGNOM = space(50)
      .w_NO__NOME = space(50)
      .w_NODESCRI = space(40)
      .w_NODESCR2 = space(40)
      .w_NONUMLIS = space(5)
      .w_NONUMLIS = space(5)
      .w_NOCODPAG = space(5)
      .w_NOCODBAN = space(10)
      .w_NOCATCOM = space(3)
      .w_NOCATCON = space(5)
      .w_NOCACLFO = space(5)
      .w_BANCAAHR = space(10)
      .w_BANCAAHE = space(10)
      .w_DESBA3 = space(42)
      .w_DESBA2 = space(42)
      .w_DESBAN = space(42)
      .w_DESPAG = space(30)
      .w_DESCAC = space(35)
      .w_DESLIS = space(40)
      .w_COGNOME = space(40)
      .w_NOME = space(40)
      .w_NODENOMRIF = space(37)
      .w_DESCON = space(35)
      .w_DESSCATCLFO = space(35)
      .w_CodiceFittizio = space(20)
      .w_ForzaNom = space(1)
      .w_SAL_Dest = space(5)
      .w_RAGSOC_Dest = space(100)
      .w_INDIRI_Dest = space(100)
      .w_LOCALI_Dest = space(100)
      .w_CAP_Dest = space(9)
      .w_PROV_Dest = space(2)
      .w_NAZ_Dest = space(3)
      .w_CortAtt_Dest = space(100)
      .w_CODI = space(15)
      .w_NOTIPPER = space(1)
      .w_NOCOGNOM = space(50)
      .w_NO__NOME = space(50)
      .w_NODESCRI = space(40)
      .w_NODESCR2 = space(40)
      .w_OFSTATUS = space(10)
      .w_SEROFF = space(10)
      .w_NFDTOF1 = ctod("  /  /  ")
      .w_NFDTOF2 = ctod("  /  /  ")
      .w_NFDTRC1 = ctod("  /  /  ")
      .w_NFDTRC2 = ctod("  /  /  ")
      .w_NFGRPRIO = space(5)
      .w_NFRIFDE = space(45)
      .w_NUMINI = space(10)
      .w_DTOBGR = ctod("  /  /  ")
      .w_NUMFIN = space(10)
      .w_CODI = space(15)
      .w_OFDATDOC = ctod("  /  /  ")
      .w_OFPATFWP = space(254)
      .w_OFPATPDF = space(254)
      .w_PATHARC = space(254)
      .w_PERIODO = space(1)
      .w_OFCODNOM = space(15)
      .w_NOTIPPER = space(1)
      .w_NOCOGNOM = space(50)
      .w_NO__NOME = space(50)
      .w_NODESCRI = space(40)
      .w_NODESCR2 = space(40)
      .w_OPERAT = 0
      .w_STATO = space(1)
      .w_NAME = space(20)
      .w_STATO = space(1)
      .w_DATAINI = ctod("  /  /  ")
      .w_ORAINI = space(2)
      .w_MININI = space(2)
      .w_DATAFIN = ctod("  /  /  ")
      .w_ORAFIN = space(2)
      .w_MINFIN = space(2)
      .w_SUBJECT = space(254)
      .w_CONTINI = space(5)
      .w_PRIOINI = 0
      .w_PRIOINI = 0
      .w_TIPOATINI = space(5)
      .w_TIPOATFIN = space(5)
      .w_ATSERIAL = space(20)
      .w_OGGETTO = space(254)
      .w_ATDATINI = ctot("")
      .w_ATDATFIN = ctot("")
      .w_ATDATINI1 = ctot("")
      .w_ATDATFIN1 = ctot("")
      .w_NOMINI = space(20)
      .w_NOMFIN = space(20)
      .w_ESITOATI = space(5)
      .w_ESITOATF = space(5)
      .w_PRIOFIN = 0
      .w_DESCPER = space(40)
      .w_OFF_SER = space(10)
      .w_NODATINS = ctod("  /  /  ")
      .w_NO__RESP = space(5)
      .w_CODUTE = 0
      .w_DATI = space(40)
      .w_NOTIPOID = space(2)
      .w_NOTIPDOC = space(2)
      .w_NONUMDOC = space(15)
      .w_NODATRIL = ctod("  /  /  ")
      .w_NOAUTLOC = space(30)
      .w_NOATTLAV = space(50)
      .w_NONAGIUR = space(4)
      .w_NGDESCRI = space(50)
      .w_NODATCOS = ctod("  /  /  ")
      .w_NO_SIGLA = space(30)
      .w_NO_PAESE = space(30)
      .w_NOESTNAS = space(30)
      .w_OLD_NOFLANTI = space(1)
      .w_NOFLANTI = space(1)
      .w_NOFLANTI = space(1)
      .w_NOCODICE = space(15)
      .w_NOCOGNOM = space(50)
      .w_NODESCRI = space(40)
      .w_NO__NOME = space(50)
      .w_NODESCR2 = space(40)
      .w_TipoRisorsa = space(1)
      .w_OB_TEST = ctod("  /  /  ")
      .w_NO__NOTE = space(0)
      .w_CODI = space(15)
      .w_NOTIPPER = space(1)
      .w_NOCOGNOM = space(50)
      .w_NO__NOME = space(50)
      .w_NODESCRI = space(40)
      .w_NODESCR2 = space(40)
      .w_EMAIL = space(254)
      .w_NORATING = space(2)
      .w_NOGIORIT = 0
      .w_NOVOCFIN = space(6)
      .w_DFDESCRI = space(60)
      .w_NOESCDOF = space(1)
      .w_NODESPAR = space(1)
      .w_RADESCRI = space(50)
      .w_NOCODICE = space(15)
      .w_NOTIPPER = space(1)
      .w_NOCOGNOM = space(50)
      .w_NO__NOME = space(50)
      .w_NODESCRI = space(40)
      .w_NODESCR2 = space(40)
      .w_EMPEC = space(254)
      .w_OFCODMOD = space(5)
      .w_TELFAX = space(18)
      .w_APEMAIL = space(254)
      .w_APEMPEC = space(254)
      if .cFunction<>"Filter"
        .w_PARCALLER = this.pTipMask
        .w_OBTEST = i_datsys
        .DoRTCalc(3,4,.f.)
          if not(empty(.w_NOCODCLI))
          .link_1_4('Full')
          endif
        .w_VALATT = .w_NOCODICE
        .w_TABKEY = 'OFF_NOMI'
        .w_DELIMM = .f.
        .w_NODESCRI = iif(.w_NOFLGBEN='B',IIF(.w_NOSOGGET='PF',LEFT(ALLTRIM(.w_NOCOGNOM)+' '+ALLTRIM(.w_NO__NOME),60),''),.w_NODESCRI)
        .w_NODESCR2 = iif(.w_NOFLGBEN='B' AND .w_NOSOGGET='PF',' ',.w_NODESCR2)
        .w_NOSOGGET = 'EN'
        .w_TIPRIS = 'P'
        .w_NOCOD_RU = iif(.w_NOTIPPER='A',' ',.w_NOCOD_RU)
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_NOCOD_RU))
          .link_1_12('Full')
          endif
        .w_NOBADGE = iif(.w_NOTIPPER='A',' ',.w_NOBADGE)
        .w_NOCOGNOM = iif(.w_NOTIPPER='A',' ',.w_NOCOGNOM)
        .w_NOCOGNOM = iif(.w_NOTIPPER='A',' ',.w_NOCOGNOM)
        .w_NO__NOME = iif(.w_NOTIPPER='A',' ',.w_NO__NOME)
        .w_NO__NOME = iif(.w_NOTIPPER='A',' ',.w_NO__NOME)
        .w_NOLOCNAS = iif(.w_NOTIPPER='A',' ',LEFT(.w_LOCNAS,30 ))
        .w_NOLOCNAS = iif(.w_NOTIPPER='A',' ',LEFT(.w_LOCNAS,30))
        .w_NOPRONAS = iif(.w_NOTIPPER='A',' ',.w_NOPRONAS)
        .w_NOPRONAS = iif(.w_NOTIPPER='A',' ',.w_NOPRONAS)
        .w_NODATNAS = iif(.w_NOTIPPER<>'A',.w_NODATNAS,CTOD('  -  -    '))
        .w_NO_SESSO = 'M'
        .DoRTCalc(24,24,.f.)
          if not(empty(.w_NONAZNAS))
          .link_1_24('Full')
          endif
        .w_NONUMCAR = iif(.w_NOTIPPER='A',' ',.w_NONUMCAR)
          .DoRTCalc(26,27,.f.)
        .w_NONATGIU = IIF(.w_NOSOGGET='PF', 'PFI', '')
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_NOCODSAL))
          .link_1_29('Full')
          endif
        .w_NOINDIRI = LEFT(.w_NOINDIRI,35)
        .w_NOINDI_2 = left(.w_NOINDI_2,35)
        .w_CAP = IIF(upper(g_APPLICATION) = "ADHOC REVOLUTION", SPACE(8), SPACE(9) )
        .w_NO___CAP = .w_CAP
        .w_NO___CAP = .w_CAP
        .DoRTCalc(35,39,.f.)
          if not(empty(.w_NONAZION))
          .link_1_39('Full')
          endif
        .w_NOINDIR2 = LEFT(.w_NOINDIR2,35)
          .DoRTCalc(41,42,.f.)
        .w_NOLOCALD = LEFT(.w_LOCALD,30)
        .w_OCOGNOM = .w_NOCOGNOM
        .w_ONOME = .w_NO__NOME
          .DoRTCalc(46,46,.f.)
        .w_NOLOCALD = LEFT(.w_LOCALD,30)
        .DoRTCalc(48,50,.f.)
          if not(empty(.w_NONAZIOD))
          .link_1_50('Full')
          endif
          .DoRTCalc(51,55,.f.)
        .w_NOINDWEB = LEFT(.w_NOINDWEB,50)
          .DoRTCalc(57,62,.f.)
        .w_NOCHKCPZ = 'N'
        .w_NOCHKWWP = 'N'
        .w_NOCHKPTL = 'N'
          .DoRTCalc(66,67,.f.)
        .w_NOSOGGET = 'EN'
        .w_NOTIPPER = 'A'
        .w_NOTIPNOM = 'T'
          .DoRTCalc(71,71,.f.)
        .w_NOTIPNOM = 'T'
        .w_NOFLGBEN = iif( .w_PARCALLER='B' ,'B' , ' ')
          .DoRTCalc(74,77,.f.)
        .w_TIPOPE = UPPER(this.cFunction)
        .w_NOTIPCLI = IIF(IsAlt() AND .w_NOTIPNOM='F','F','C')
        .w_OBTEST = i_datsys
        .w_NOTIPO = .w_NOTIPNOM
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_108.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_128.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_129.Calculate()
          .DoRTCalc(82,83,.f.)
        .w_FL_SOG_RIT = 'N'
        .oPgFrm.Page1.oPag.oObj_1_147.Calculate()
          .DoRTCalc(85,89,.f.)
        .w_TIPO = 'C'
        .oPgFrm.Page1.oPag.oObj_1_162.Calculate()
          .DoRTCalc(91,97,.f.)
        .w_CONTA = .w_CONTA
        .w_NOTIPBAN = 'B'
        .w_NOTIPCON = "F"
        .DoRTCalc(101,101,.f.)
          if not(empty(.w_NOCODCON))
          .link_2_3('Full')
          endif
        .w_NOCODPAG = .w_NOCODPAG
        .DoRTCalc(102,102,.f.)
          if not(empty(.w_NOCODPAG))
          .link_2_4('Full')
          endif
          .DoRTCalc(103,105,.f.)
        .w_DESMES1 = LEFT(IIF(.w_NO1MESCL>0 AND .w_NO1MESCL<13, g_MESE[.w_NO1MESCL], "")+SPACE(12),12)
          .DoRTCalc(107,109,.f.)
        .w_DESMES2 = LEFT(IIF(.w_NO2MESCL>0 AND .w_NO2MESCL<13, g_MESE[.w_NO2MESCL], "")+SPACE(12),12)
        .w_NOCODBAN = .w_NOCODBAN
        .DoRTCalc(111,111,.f.)
          if not(empty(.w_NOCODBAN))
          .link_2_13('Full')
          endif
          .DoRTCalc(112,112,.f.)
        .w_NOFLRAGG = ' '
        .w_NOFLGAVV = 'N'
          .DoRTCalc(115,115,.f.)
        .w_NOINTMOR = 'N'
        .w_NOMORTAS = 0
        .oPgFrm.Page2.oPag.oObj_2_27.Calculate()
        .DoRTCalc(118,126,.f.)
          if not(empty(.w_BANCAAHE))
          .link_2_43('Full')
          endif
        .DoRTCalc(127,127,.f.)
          if not(empty(.w_NOCATFIN))
          .link_2_44('Full')
          endif
        .DoRTCalc(128,128,.f.)
          if not(empty(.w_NOCATDER))
          .link_2_45('Full')
          endif
        .w_NOIDRIDY = 'N'
        .w_NOTIIDRI = 1
        .w_NOCOIDRI = ''
        .w_OLDANTIIDRI = 1
        .w_OLDANCOIDRI = ''
        .w_ANRITENU = 'N'
        .w_ANFLRITE = 'N'
          .DoRTCalc(136,144,.f.)
        .w_TIPOFIN = 'C'
        .w_TIPOFIND = 'D'
        .oPgFrm.Page3.oPag.ZoomAll.Calculate()
          .DoRTCalc(147,149,.f.)
        .w_SERALL = NVL(.w_ZoomAll.getVar('ALSERIAL'), SPACE(10))
        .w_PATALL = NVL(.w_ZoomAll.getVar('ALPATFIL'), SPACE(254))
        .w_CODI = .w_NOCODICE
          .DoRTCalc(153,158,.f.)
        .w_CODI = .w_NOCODICE
          .DoRTCalc(160,160,.f.)
        .w_NOPRIVCY = 'N'
        .DoRTCalc(162,162,.f.)
          if not(empty(.w_NOCODGRU))
          .link_4_6('Full')
          endif
        .DoRTCalc(163,164,.f.)
          if not(empty(.w_NOCODORI))
          .link_4_9('Full')
          endif
        .DoRTCalc(165,166,.f.)
          if not(empty(.w_NORIFINT))
          .link_4_12('Full')
          endif
        .w_NOCODOPE = i_CODUTE
        .DoRTCalc(167,167,.f.)
          if not(empty(.w_NOCODOPE))
          .link_4_13('Full')
          endif
        .DoRTCalc(168,170,.f.)
          if not(empty(.w_NOCODZON))
          .link_4_17('Full')
          endif
          .DoRTCalc(171,171,.f.)
        .w_NOCODLIN = g_CODLIN
        .DoRTCalc(172,172,.f.)
          if not(empty(.w_NOCODLIN))
          .link_4_20('Full')
          endif
        .DoRTCalc(173,174,.f.)
          if not(empty(.w_NOCODVAL))
          .link_4_23('Full')
          endif
        .DoRTCalc(175,175,.f.)
          if not(empty(.w_NOCODAGE))
          .link_4_24('Full')
          endif
        .DoRTCalc(176,186,.f.)
          if not(empty(.w_NONUMLIS))
          .link_4_44('Full')
          endif
        .DoRTCalc(187,187,.f.)
          if not(empty(.w_NONUMLIS))
          .link_4_45('Full')
          endif
        .DoRTCalc(188,188,.f.)
          if not(empty(.w_NOCODPAG))
          .link_4_46('Full')
          endif
        .DoRTCalc(189,189,.f.)
          if not(empty(.w_NOCODBAN))
          .link_4_47('Full')
          endif
        .DoRTCalc(190,190,.f.)
          if not(empty(.w_NOCATCOM))
          .link_4_48('Full')
          endif
        .DoRTCalc(191,191,.f.)
          if not(empty(.w_NOCATCON))
          .link_4_49('Full')
          endif
        .DoRTCalc(192,192,.f.)
          if not(empty(.w_NOCACLFO))
          .link_4_50('Full')
          endif
        .w_BANCAAHR = IIF (g_APPLICATION <> "ADHOC REVOLUTION" ,.w_BANCAAHR,.w_NOCODBA2)
        .DoRTCalc(193,193,.f.)
          if not(empty(.w_BANCAAHR))
          .link_4_51('Full')
          endif
        .w_BANCAAHE = IIF (g_APPLICATION = "ADHOC REVOLUTION" ,.w_BANCAAHE , .w_NOCODBA2)
        .DoRTCalc(194,194,.f.)
          if not(empty(.w_BANCAAHE))
          .link_4_52('Full')
          endif
          .DoRTCalc(195,202,.f.)
        .w_NODENOMRIF = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
          .DoRTCalc(204,205,.f.)
        .w_CodiceFittizio = 'X*?-X*?-X*?-X*?-X*?-'
          .DoRTCalc(207,215,.f.)
        .w_CODI = .w_NOCODICE
          .DoRTCalc(217,221,.f.)
        .w_OFSTATUS = 'I'
        .oPgFrm.Page6.oPag.ZoomOff.Calculate()
        .w_SEROFF = NVL(.w_ZoomOff.getVar('OFSERIAL'), SPACE(10))
        .DoRTCalc(224,228,.f.)
          if not(empty(.w_NFGRPRIO))
          .link_6_17('Full')
          endif
          .DoRTCalc(229,232,.f.)
        .w_CODI = .w_NOCODICE
        .w_OFDATDOC = NVL(.w_ZoomOff.getVar('OFDATDOC'), cp_CharToDate('  /  /  '))
        .w_OFPATFWP = NVL(.w_ZoomOff.getVar('OFPATFWP'), SPACE(254))
        .w_OFPATPDF = NVL(.w_ZoomOff.getVar('OFPATPDF'), SPACE(254))
        .w_PATHARC = NVL(.w_ZoomOff.getVar('MOPATARC'), SPACE(254))
        .w_PERIODO = NVL(.w_ZoomOff.getVar('MONUMPER'), SPACE(1))
        .w_OFCODNOM = .w_NOCODICE
        .oPgFrm.Page7.oPag.ZOOM.Calculate()
          .DoRTCalc(240,244,.f.)
        .w_OPERAT = 0
        .DoRTCalc(245,245,.f.)
          if not(empty(.w_OPERAT))
          .link_7_2('Full')
          endif
        .w_STATO = iif(g_AGEN='S','N','A')
          .DoRTCalc(247,247,.f.)
        .w_STATO = iif(g_AGEN='S','N','A')
          .DoRTCalc(249,249,.f.)
        .w_ORAINI = '00'
        .w_MININI = '00'
          .DoRTCalc(252,252,.f.)
        .w_ORAFIN = '23'
        .w_MINFIN = '59'
        .DoRTCalc(255,256,.f.)
          if not(empty(.w_CONTINI))
          .link_7_15('Full')
          endif
          .DoRTCalc(257,257,.f.)
        .w_PRIOINI = 0
          .DoRTCalc(259,260,.f.)
        .w_ATSERIAL = .w_ZOOM.GetVar('ATSERIAL')
        .w_OGGETTO = '%'+ALLTRIM(.w_SUBJECT)
          .DoRTCalc(263,266,.f.)
        .w_NOMINI = .w_NOCODICE
        .w_NOMFIN = .w_NOCODICE
          .DoRTCalc(269,270,.f.)
        .w_PRIOFIN = .w_PRIOINI
          .DoRTCalc(272,272,.f.)
        .w_OFF_SER = ''
        .DoRTCalc(274,275,.f.)
          if not(empty(.w_NO__RESP))
          .link_8_2('Full')
          endif
          .DoRTCalc(276,276,.f.)
        .w_DATI = iif(!empty(.w_NO__RESP), ReadDipend(.w_CODUTE,"D"), space(40) )
        .w_NOTIPOID = 'A'
          .DoRTCalc(279,279,.f.)
        .w_NONUMDOC = iif(.w_NOTIPDOC='01'and !empty(.w_NONUMCAR),.w_NONUMCAR,.w_NONUMDOC)
        .DoRTCalc(281,284,.f.)
          if not(empty(.w_NONAGIUR))
          .link_8_11('Full')
          endif
        .oPgFrm.Page8.oPag.oObj_8_44.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_45.Calculate()
          .DoRTCalc(285,297,.f.)
        .w_TipoRisorsa = 'P'
        .w_OB_TEST = i_INIDAT
          .DoRTCalc(300,300,.f.)
        .w_CODI = .w_NOCODICE
          .DoRTCalc(302,306,.f.)
        .w_EMAIL = evl(.w_NO_EMAIL,.w_APEMAIL)
        .DoRTCalc(308,308,.f.)
          if not(empty(.w_NORATING))
          .link_10_4('Full')
          endif
        .DoRTCalc(309,310,.f.)
          if not(empty(.w_NOVOCFIN))
          .link_10_6('Full')
          endif
          .DoRTCalc(311,311,.f.)
        .w_NOESCDOF = 'N'
        .w_NODESPAR = 'N'
          .DoRTCalc(314,320,.f.)
        .w_EMPEC = evl(.w_NO_EMPEC,.w_APEMPEC)
        .w_OFCODMOD = NVL(.w_ZoomOff.getVar('OFCODMOD'), SPACE(5))
      endif
    endwith
    cp_BlankRecExtFlds(this,'OFF_NOMI')
    this.DoRTCalc(323,325,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_130.enabled = this.oPgFrm.Page1.oPag.oBtn_1_130.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_132.enabled = this.oPgFrm.Page1.oPag.oBtn_1_132.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_133.enabled = this.oPgFrm.Page1.oPag.oBtn_1_133.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_134.enabled = this.oPgFrm.Page1.oPag.oBtn_1_134.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_135.enabled = this.oPgFrm.Page1.oPag.oBtn_1_135.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_136.enabled = this.oPgFrm.Page1.oPag.oBtn_1_136.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_137.enabled = this.oPgFrm.Page1.oPag.oBtn_1_137.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_138.enabled = this.oPgFrm.Page1.oPag.oBtn_1_138.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_139.enabled = this.oPgFrm.Page1.oPag.oBtn_1_139.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_140.enabled = this.oPgFrm.Page1.oPag.oBtn_1_140.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_141.enabled = this.oPgFrm.Page1.oPag.oBtn_1_141.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_168.enabled = this.oPgFrm.Page1.oPag.oBtn_1_168.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_169.enabled = this.oPgFrm.Page1.oPag.oBtn_1_169.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_170.enabled = this.oPgFrm.Page1.oPag.oBtn_1_170.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_5.enabled = this.oPgFrm.Page3.oPag.oBtn_3_5.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_6.enabled = this.oPgFrm.Page3.oPag.oBtn_3_6.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_7.enabled = this.oPgFrm.Page3.oPag.oBtn_3_7.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_9.enabled = this.oPgFrm.Page3.oPag.oBtn_3_9.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_13.enabled = this.oPgFrm.Page3.oPag.oBtn_3_13.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_4.enabled = this.oPgFrm.Page4.oPag.oBtn_4_4.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_2.enabled = this.oPgFrm.Page6.oPag.oBtn_6_2.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_3.enabled = this.oPgFrm.Page6.oPag.oBtn_6_3.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_6.enabled = this.oPgFrm.Page6.oPag.oBtn_6_6.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_7.enabled = this.oPgFrm.Page6.oPag.oBtn_6_7.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_8.enabled = this.oPgFrm.Page6.oPag.oBtn_6_8.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_20.enabled = this.oPgFrm.Page6.oPag.oBtn_6_20.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_40.enabled = this.oPgFrm.Page7.oPag.oBtn_7_40.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_42.enabled = this.oPgFrm.Page7.oPag.oBtn_7_42.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_43.enabled = this.oPgFrm.Page7.oPag.oBtn_7_43.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_45.enabled = this.oPgFrm.Page7.oPag.oBtn_7_45.mCond()
    this.oPgFrm.Page8.oPag.oBtn_8_42.enabled = this.oPgFrm.Page8.oPag.oBtn_8_42.mCond()
    this.oPgFrm.Page9.oPag.oBtn_9_2.enabled = this.oPgFrm.Page9.oPag.oBtn_9_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_180.enabled = this.oPgFrm.Page1.oPag.oBtn_1_180.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_ano
    * --- Refresh dello Zoom
    if this.cFunction='Load'
       this.NotifyEvent('CalcolaOfferte')
       this.NotifyEvent('CalcolaAllegati')
    endif
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    with this	
      if g_OFNUME='S' AND .p_AUT=0
        cp_AskTableProg(this,i_nConn,"PRNUNMOF","i_CODAZI,w_NOCODICE")
      endif
      .op_CODAZI = .w_CODAZI
      .op_NOCODICE = .w_NOCODICE
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oNOCODICE_1_3.enabled = i_bVal
      .Page1.oPag.oNODESCRI_1_8.enabled = i_bVal
      .Page1.oPag.oNODESCR2_1_9.enabled = i_bVal
      .Page1.oPag.oNOSOGGET_1_10.enabled = i_bVal
      .Page1.oPag.oNOCOD_RU_1_12.enabled = i_bVal
      .Page1.oPag.oNOBADGE_1_13.enabled = i_bVal
      .Page1.oPag.oNOCOGNOM_1_14.enabled = i_bVal
      .Page1.oPag.oNOCOGNOM_1_15.enabled = i_bVal
      .Page1.oPag.oNO__NOME_1_16.enabled = i_bVal
      .Page1.oPag.oNO__NOME_1_17.enabled = i_bVal
      .Page1.oPag.oNOLOCNAS_1_18.enabled = i_bVal
      .Page1.oPag.oNOLOCNAS_1_19.enabled = i_bVal
      .Page1.oPag.oNOPRONAS_1_20.enabled = i_bVal
      .Page1.oPag.oNOPRONAS_1_21.enabled = i_bVal
      .Page1.oPag.oNODATNAS_1_22.enabled = i_bVal
      .Page1.oPag.oNO_SESSO_1_23.enabled = i_bVal
      .Page1.oPag.oNONAZNAS_1_24.enabled = i_bVal
      .Page1.oPag.oNONUMCAR_1_25.enabled = i_bVal
      .Page1.oPag.oNOCODFIS_1_26.enabled = i_bVal
      .Page1.oPag.oNOPARIVA_1_27.enabled = i_bVal
      .Page1.oPag.oNONATGIU_1_28.enabled = i_bVal
      .Page1.oPag.oNOCODSAL_1_29.enabled = i_bVal
      .Page1.oPag.oNOINDIRI_1_30.enabled = i_bVal
      .Page1.oPag.oNOINDI_2_1_31.enabled = i_bVal
      .Page1.oPag.oNO___CAP_1_33.enabled = i_bVal
      .Page1.oPag.oNO___CAP_1_34.enabled = i_bVal
      .Page1.oPag.oNOLOCALI_1_35.enabled = i_bVal
      .Page1.oPag.oNOLOCALI_1_36.enabled = i_bVal
      .Page1.oPag.oNOPROVIN_1_37.enabled = i_bVal
      .Page1.oPag.oNOPROVIN_1_38.enabled = i_bVal
      .Page1.oPag.oNONAZION_1_39.enabled = i_bVal
      .Page1.oPag.oNOINDIR2_1_40.enabled = i_bVal
      .Page1.oPag.oNO___CAD_1_41.enabled = i_bVal
      .Page1.oPag.oNO___CAD_1_42.enabled = i_bVal
      .Page1.oPag.oNOLOCALD_1_43.enabled = i_bVal
      .Page1.oPag.oNOLOCALD_1_47.enabled = i_bVal
      .Page1.oPag.oNOPROVID_1_48.enabled = i_bVal
      .Page1.oPag.oNOPROVID_1_49.enabled = i_bVal
      .Page1.oPag.oNONAZIOD_1_50.enabled = i_bVal
      .Page1.oPag.oNOTELEFO_1_51.enabled = i_bVal
      .Page1.oPag.oNONUMCEL_1_52.enabled = i_bVal
      .Page1.oPag.oNO_SKYPE_1_53.enabled = i_bVal
      .Page1.oPag.oNOTELFAX_1_54.enabled = i_bVal
      .Page1.oPag.oNO_EMAIL_1_55.enabled = i_bVal
      .Page1.oPag.oNOINDWEB_1_56.enabled = i_bVal
      .Page1.oPag.oNO_EMPEC_1_57.enabled = i_bVal
      .Page1.oPag.oNOCHKSTA_1_58.enabled = i_bVal
      .Page1.oPag.oNOCHKMAI_1_59.enabled = i_bVal
      .Page1.oPag.oNOCHKPEC_1_60.enabled = i_bVal
      .Page1.oPag.oNOCHKFAX_1_61.enabled = i_bVal
      .Page1.oPag.oNOCHKCPZ_1_62.enabled = i_bVal
      .Page1.oPag.oNODTINVA_1_66.enabled = i_bVal
      .Page1.oPag.oNODTOBSO_1_67.enabled = i_bVal
      .Page1.oPag.oNOSOGGET_1_69.enabled = i_bVal
      .Page1.oPag.oNOTIPPER_1_72.enabled = i_bVal
      .Page1.oPag.oNOTIPNOM_1_74.enabled = i_bVal
      .Page1.oPag.oNOTIPNOM_1_75.enabled = i_bVal
      .Page1.oPag.oNOTIPNOM_1_76.enabled = i_bVal
      .Page2.oPag.oNOTIPCON_2_2.enabled = i_bVal
      .Page2.oPag.oNOCODCON_2_3.enabled = i_bVal
      .Page2.oPag.oNOCODPAG_2_4.enabled = i_bVal
      .Page2.oPag.oNOGIOFIS_2_6.enabled = i_bVal
      .Page2.oPag.oNO1MESCL_2_7.enabled = i_bVal
      .Page2.oPag.oNOGIOSC1_2_9.enabled = i_bVal
      .Page2.oPag.oNO2MESCL_2_10.enabled = i_bVal
      .Page2.oPag.oNOGIOSC2_2_11.enabled = i_bVal
      .Page2.oPag.oNOCODBAN_2_13.enabled = i_bVal
      .Page2.oPag.oBANCAAHE_2_43.enabled = i_bVal
      .Page2.oPag.oNOCATFIN_2_44.enabled = i_bVal
      .Page2.oPag.oNOCATDER_2_45.enabled = i_bVal
      .Page2.oPag.oNOIDRIDY_2_46.enabled = i_bVal
      .Page2.oPag.oNOTIIDRI_2_47.enabled = i_bVal
      .Page2.oPag.oNOCOIDRI_2_48.enabled = i_bVal
      .Page3.oPag.oNONOMFIL_3_1.enabled = i_bVal
      .Page3.oPag.oNOOGGE_3_2.enabled = i_bVal
      .Page3.oPag.oNONOTE_3_3.enabled = i_bVal
      .Page4.oPag.oNOCODPRI_4_2.enabled = i_bVal
      .Page4.oPag.oNOPRIVCY_4_3.enabled = i_bVal
      .Page4.oPag.oNOCODGRU_4_6.enabled = i_bVal
      .Page4.oPag.oNOCODORI_4_9.enabled = i_bVal
      .Page4.oPag.oNORIFINT_4_12.enabled = i_bVal
      .Page4.oPag.oNOCODOPE_4_13.enabled = i_bVal
      .Page4.oPag.oNOASSOPE_4_15.enabled = i_bVal
      .Page4.oPag.oNOCODZON_4_17.enabled = i_bVal
      .Page4.oPag.oNOCODLIN_4_20.enabled = i_bVal
      .Page4.oPag.oNOCODVAL_4_23.enabled = i_bVal
      .Page4.oPag.oNOCODAGE_4_24.enabled = i_bVal
      .Page4.oPag.oNONUMLIS_4_44.enabled = i_bVal
      .Page4.oPag.oNONUMLIS_4_45.enabled = i_bVal
      .Page4.oPag.oNOCODPAG_4_46.enabled = i_bVal
      .Page4.oPag.oNOCODBAN_4_47.enabled = i_bVal
      .Page4.oPag.oNOCATCOM_4_48.enabled = i_bVal
      .Page4.oPag.oNOCATCON_4_49.enabled = i_bVal
      .Page4.oPag.oNOCACLFO_4_50.enabled = i_bVal
      .Page4.oPag.oBANCAAHR_4_51.enabled = i_bVal
      .Page4.oPag.oBANCAAHE_4_52.enabled = i_bVal
      .Page6.oPag.oOFSTATUS_6_1.enabled = i_bVal
      .Page6.oPag.oNFDTOF1_6_9.enabled = i_bVal
      .Page6.oPag.oNFDTOF2_6_10.enabled = i_bVal
      .Page6.oPag.oNFDTRC1_6_11.enabled = i_bVal
      .Page6.oPag.oNFDTRC2_6_12.enabled = i_bVal
      .Page6.oPag.oNFGRPRIO_6_17.enabled = i_bVal
      .Page6.oPag.oNFRIFDE_6_18.enabled = i_bVal
      .Page7.oPag.oOPERAT_7_2.enabled = i_bVal
      .Page7.oPag.oSTATO_7_3.enabled = i_bVal
      .Page7.oPag.oSTATO_7_6.enabled = i_bVal
      .Page7.oPag.oDATAINI_7_7.enabled = i_bVal
      .Page7.oPag.oORAINI_7_8.enabled = i_bVal
      .Page7.oPag.oMININI_7_9.enabled = i_bVal
      .Page7.oPag.oDATAFIN_7_10.enabled = i_bVal
      .Page7.oPag.oORAFIN_7_11.enabled = i_bVal
      .Page7.oPag.oMINFIN_7_12.enabled = i_bVal
      .Page7.oPag.oSUBJECT_7_14.enabled = i_bVal
      .Page7.oPag.oCONTINI_7_15.enabled = i_bVal
      .Page7.oPag.oPRIOINI_7_16.enabled = i_bVal
      .Page7.oPag.oPRIOINI_7_17.enabled = i_bVal
      .Page8.oPag.oNODATINS_8_1.enabled = i_bVal
      .Page8.oPag.oNO__RESP_8_2.enabled = i_bVal
      .Page8.oPag.oNOTIPOID_8_5.enabled = i_bVal
      .Page8.oPag.oNOTIPDOC_8_6.enabled = i_bVal
      .Page8.oPag.oNONUMDOC_8_7.enabled = i_bVal
      .Page8.oPag.oNODATRIL_8_8.enabled = i_bVal
      .Page8.oPag.oNOAUTLOC_8_9.enabled = i_bVal
      .Page8.oPag.oNOATTLAV_8_10.enabled = i_bVal
      .Page8.oPag.oNONAGIUR_8_11.enabled = i_bVal
      .Page8.oPag.oNODATCOS_8_13.enabled = i_bVal
      .Page8.oPag.oNO_SIGLA_8_14.enabled = i_bVal
      .Page8.oPag.oNO_PAESE_8_15.enabled = i_bVal
      .Page8.oPag.oNOESTNAS_8_16.enabled = i_bVal
      .Page8.oPag.oNOFLANTI_8_20.enabled = i_bVal
      .Page9.oPag.oNO__NOTE_9_1.enabled = i_bVal
      .Page10.oPag.oNORATING_10_4.enabled = i_bVal
      .Page10.oPag.oNOGIORIT_10_5.enabled = i_bVal
      .Page10.oPag.oNOVOCFIN_10_6.enabled = i_bVal
      .Page10.oPag.oNOESCDOF_10_8.enabled = i_bVal
      .Page10.oPag.oNODESPAR_10_9.enabled = i_bVal
      .Page1.oPag.oBtn_1_130.enabled = .Page1.oPag.oBtn_1_130.mCond()
      .Page1.oPag.oBtn_1_132.enabled = .Page1.oPag.oBtn_1_132.mCond()
      .Page1.oPag.oBtn_1_133.enabled = .Page1.oPag.oBtn_1_133.mCond()
      .Page1.oPag.oBtn_1_134.enabled = .Page1.oPag.oBtn_1_134.mCond()
      .Page1.oPag.oBtn_1_135.enabled = .Page1.oPag.oBtn_1_135.mCond()
      .Page1.oPag.oBtn_1_136.enabled = .Page1.oPag.oBtn_1_136.mCond()
      .Page1.oPag.oBtn_1_137.enabled = .Page1.oPag.oBtn_1_137.mCond()
      .Page1.oPag.oBtn_1_138.enabled = .Page1.oPag.oBtn_1_138.mCond()
      .Page1.oPag.oBtn_1_139.enabled = .Page1.oPag.oBtn_1_139.mCond()
      .Page1.oPag.oBtn_1_140.enabled = .Page1.oPag.oBtn_1_140.mCond()
      .Page1.oPag.oBtn_1_141.enabled = .Page1.oPag.oBtn_1_141.mCond()
      .Page1.oPag.oBtn_1_163.enabled = i_bVal
      .Page1.oPag.oBtn_1_168.enabled = .Page1.oPag.oBtn_1_168.mCond()
      .Page1.oPag.oBtn_1_169.enabled = .Page1.oPag.oBtn_1_169.mCond()
      .Page1.oPag.oBtn_1_170.enabled = .Page1.oPag.oBtn_1_170.mCond()
      .Page3.oPag.oBtn_3_5.enabled = .Page3.oPag.oBtn_3_5.mCond()
      .Page3.oPag.oBtn_3_6.enabled = .Page3.oPag.oBtn_3_6.mCond()
      .Page3.oPag.oBtn_3_7.enabled = .Page3.oPag.oBtn_3_7.mCond()
      .Page3.oPag.oBtn_3_9.enabled = .Page3.oPag.oBtn_3_9.mCond()
      .Page3.oPag.oBtn_3_13.enabled = .Page3.oPag.oBtn_3_13.mCond()
      .Page4.oPag.oBtn_4_4.enabled = .Page4.oPag.oBtn_4_4.mCond()
      .Page6.oPag.oBtn_6_2.enabled = .Page6.oPag.oBtn_6_2.mCond()
      .Page6.oPag.oBtn_6_3.enabled = .Page6.oPag.oBtn_6_3.mCond()
      .Page6.oPag.oBtn_6_6.enabled = .Page6.oPag.oBtn_6_6.mCond()
      .Page6.oPag.oBtn_6_7.enabled = .Page6.oPag.oBtn_6_7.mCond()
      .Page6.oPag.oBtn_6_8.enabled = .Page6.oPag.oBtn_6_8.mCond()
      .Page6.oPag.oBtn_6_20.enabled = .Page6.oPag.oBtn_6_20.mCond()
      .Page7.oPag.oBtn_7_40.enabled = .Page7.oPag.oBtn_7_40.mCond()
      .Page7.oPag.oBtn_7_42.enabled = .Page7.oPag.oBtn_7_42.mCond()
      .Page7.oPag.oBtn_7_43.enabled = .Page7.oPag.oBtn_7_43.mCond()
      .Page7.oPag.oBtn_7_45.enabled = .Page7.oPag.oBtn_7_45.mCond()
      .Page8.oPag.oBtn_8_42.enabled = .Page8.oPag.oBtn_8_42.mCond()
      .Page9.oPag.oBtn_9_2.enabled = .Page9.oPag.oBtn_9_2.mCond()
      .Page1.oPag.oBtn_1_180.enabled = .Page1.oPag.oBtn_1_180.mCond()
      .Page1.oPag.oObj_1_107.enabled = i_bVal
      .Page1.oPag.oObj_1_162.enabled = i_bVal
      .Page2.oPag.oObj_2_27.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oNOCODICE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oNOCODICE_1_3.enabled = .t.
        .Page1.oPag.oNODESCRI_1_8.enabled = .t.
        .Page3.oPag.oNONOMFIL_3_1.enabled = .t.
        .Page3.oPag.oNOOGGE_3_2.enabled = .t.
        .Page3.oPag.oNONOTE_3_3.enabled = .t.
        .Page6.oPag.oOFSTATUS_6_1.enabled = .t.
        .Page6.oPag.oNFDTOF1_6_9.enabled = .t.
        .Page6.oPag.oNFDTOF2_6_10.enabled = .t.
        .Page6.oPag.oNFDTRC1_6_11.enabled = .t.
        .Page6.oPag.oNFDTRC2_6_12.enabled = .t.
        .Page6.oPag.oNFGRPRIO_6_17.enabled = .t.
        .Page6.oPag.oNFRIFDE_6_18.enabled = .t.
        .Page7.oPag.oOPERAT_7_2.enabled = .t.
        .Page7.oPag.oSTATO_7_3.enabled = .t.
        .Page7.oPag.oSTATO_7_6.enabled = .t.
        .Page7.oPag.oDATAINI_7_7.enabled = .t.
        .Page7.oPag.oORAINI_7_8.enabled = .t.
        .Page7.oPag.oMININI_7_9.enabled = .t.
        .Page7.oPag.oDATAFIN_7_10.enabled = .t.
        .Page7.oPag.oORAFIN_7_11.enabled = .t.
        .Page7.oPag.oMINFIN_7_12.enabled = .t.
        .Page7.oPag.oSUBJECT_7_14.enabled = .t.
        .Page7.oPag.oCONTINI_7_15.enabled = .t.
        .Page7.oPag.oPRIOINI_7_16.enabled = .t.
        .Page7.oPag.oPRIOINI_7_17.enabled = .t.
      endif
    endwith
    this.GSAR_MCC.SetStatus(i_cOp)
    this.GSAR_MAN.SetStatus(i_cOp)
    this.GSAR_MCN.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'OFF_NOMI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- gsar_ano
    EnableBtnStampa(This)
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MCC.SetChildrenStatus(i_cOp)
  *  this.GSAR_MAN.SetChildrenStatus(i_cOp)
  *  this.GSAR_MCN.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODICE,"NOCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODCLI,"NOCODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCRI,"NODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCR2,"NODESCR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOSOGGET,"NOSOGGET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCOD_RU,"NOCOD_RU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOBADGE,"NOBADGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCOGNOM,"NOCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCOGNOM,"NOCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO__NOME,"NO__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO__NOME,"NO__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOLOCNAS,"NOLOCNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOLOCNAS,"NOLOCNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOPRONAS,"NOPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOPRONAS,"NOPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODATNAS,"NODATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO_SESSO,"NO_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NONAZNAS,"NONAZNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NONUMCAR,"NONUMCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODFIS,"NOCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOPARIVA,"NOPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NONATGIU,"NONATGIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODSAL,"NOCODSAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOINDIRI,"NOINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOINDI_2,"NOINDI_2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO___CAP,"NO___CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO___CAP,"NO___CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOLOCALI,"NOLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOLOCALI,"NOLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOPROVIN,"NOPROVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOPROVIN,"NOPROVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NONAZION,"NONAZION",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOINDIR2,"NOINDIR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO___CAD,"NO___CAD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO___CAD,"NO___CAD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOLOCALD,"NOLOCALD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOLOCALD,"NOLOCALD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOPROVID,"NOPROVID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOPROVID,"NOPROVID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NONAZIOD,"NONAZIOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTELEFO,"NOTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NONUMCEL,"NONUMCEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO_SKYPE,"NO_SKYPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTELFAX,"NOTELFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO_EMAIL,"NO_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOINDWEB,"NOINDWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO_EMPEC,"NO_EMPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCHKSTA,"NOCHKSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCHKMAI,"NOCHKMAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCHKPEC,"NOCHKPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCHKFAX,"NOCHKFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCHKCPZ,"NOCHKCPZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCHKCPZ,"NOCHKCPZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCHKWWP,"NOCHKWWP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCHKPTL,"NOCHKPTL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODTINVA,"NODTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODTOBSO,"NODTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOSOGGET,"NOSOGGET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPPER,"NOTIPPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPNOM,"NOTIPNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPNOM,"NOTIPNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPNOM,"NOTIPNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOFLGBEN,"NOFLGBEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPCLI,"NOTIPCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPCON,"NOTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODCON,"NOCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODPAG,"NOCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOGIOFIS,"NOGIOFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO1MESCL,"NO1MESCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOGIOSC1,"NOGIOSC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO2MESCL,"NO2MESCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOGIOSC2,"NOGIOSC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODBAN,"NOCODBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODBA2,"NOCODBA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOFLRAGG,"NOFLRAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOFLGAVV,"NOFLGAVV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODATAVV,"NODATAVV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOINTMOR,"NOINTMOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOMORTAS,"NOMORTAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODICE,"NOCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPPER,"NOTIPPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCATFIN,"NOCATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCATDER,"NOCATDER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOIDRIDY,"NOIDRIDY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIIDRI,"NOTIIDRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCOIDRI,"NOCOIDRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCOGNOM,"NOCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO__NOME,"NO__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCRI,"NODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCR2,"NODESCR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NONUMCOR,"NONUMCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPPER,"NOTIPPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCOGNOM,"NOCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO__NOME,"NO__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCRI,"NODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCR2,"NODESCR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODPRI,"NOCODPRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOPRIVCY,"NOPRIVCY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODGRU,"NOCODGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODORI,"NOCODORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NORIFINT,"NORIFINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODOPE,"NOCODOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOASSOPE,"NOASSOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODZON,"NOCODZON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODLIN,"NOCODLIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODVAL,"NOCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODAGE,"NOCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPPER,"NOTIPPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCOGNOM,"NOCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO__NOME,"NO__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCRI,"NODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCR2,"NODESCR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NONUMLIS,"NONUMLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NONUMLIS,"NONUMLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODPAG,"NOCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODBAN,"NOCODBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCATCOM,"NOCATCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCATCON,"NOCATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCACLFO,"NOCACLFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPPER,"NOTIPPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCOGNOM,"NOCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO__NOME,"NO__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCRI,"NODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCR2,"NODESCR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPPER,"NOTIPPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCOGNOM,"NOCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO__NOME,"NO__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCRI,"NODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCR2,"NODESCR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODATINS,"NODATINS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO__RESP,"NO__RESP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPOID,"NOTIPOID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPDOC,"NOTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NONUMDOC,"NONUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODATRIL,"NODATRIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOAUTLOC,"NOAUTLOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOATTLAV,"NOATTLAV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NONAGIUR,"NONAGIUR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODATCOS,"NODATCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO_SIGLA,"NO_SIGLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO_PAESE,"NO_PAESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOESTNAS,"NOESTNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOFLANTI,"NOFLANTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOFLANTI,"NOFLANTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODICE,"NOCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCOGNOM,"NOCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCRI,"NODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO__NOME,"NO__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCR2,"NODESCR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO__NOTE,"NO__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPPER,"NOTIPPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCOGNOM,"NOCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO__NOME,"NO__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCRI,"NODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCR2,"NODESCR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NORATING,"NORATING",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOGIORIT,"NOGIORIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOVOCFIN,"NOVOCFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOESCDOF,"NOESCDOF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESPAR,"NODESPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCODICE,"NOCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOTIPPER,"NOTIPPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NOCOGNOM,"NOCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NO__NOME,"NO__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCRI,"NODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NODESCR2,"NODESCR2",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    i_lTable = "OFF_NOMI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.OFF_NOMI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        do LanciaRep with this
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gsar_ano
    ** Previene eventuale inserimento di Codici non numerici dal calcolo Autonumber
    local p_POS, p_COD
    p_COD = ALLTRIM(this.w_NOCODICE)
    FOR p_POS=1 TO LEN(p_COD)
         this.p_AUT = IIF(SUBSTR(p_COD, p_POS, 1)$'0123456789', this.p_AUT, 1)
    ENDFOR
    
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.OFF_NOMI_IDX,i_nConn)
      with this
        if g_OFNUME='S' AND .p_AUT=0
          cp_NextTableProg(this,i_nConn,"PRNUNMOF","i_CODAZI,w_NOCODICE")
        endif
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into OFF_NOMI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'OFF_NOMI')
        i_extval=cp_InsertValODBCExtFlds(this,'OFF_NOMI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(NOCODICE,NOCODCLI,NODESCRI,NODESCR2,NOSOGGET"+;
                  ",NOCOD_RU,NOBADGE,NOCOGNOM,NO__NOME,NOLOCNAS"+;
                  ",NOPRONAS,NODATNAS,NO_SESSO,NONAZNAS,NONUMCAR"+;
                  ",NOCODFIS,NOPARIVA,NONATGIU,NOCODSAL,NOINDIRI"+;
                  ",NOINDI_2,NO___CAP,NOLOCALI,NOPROVIN,NONAZION"+;
                  ",NOINDIR2,NO___CAD,NOLOCALD,NOPROVID,NONAZIOD"+;
                  ",NOTELEFO,NONUMCEL,NO_SKYPE,NOTELFAX,NO_EMAIL"+;
                  ",NOINDWEB,NO_EMPEC,NOCHKSTA,NOCHKMAI,NOCHKPEC"+;
                  ",NOCHKFAX,NOCHKCPZ,NOCHKWWP,NOCHKPTL,NODTINVA"+;
                  ",NODTOBSO,NOTIPPER,NOTIPNOM,NOFLGBEN,UTCC"+;
                  ",UTCV,UTDC,UTDV,NOTIPCLI,NOTIPCON"+;
                  ",NOCODCON,NOCODPAG,NOGIOFIS,NO1MESCL,NOGIOSC1"+;
                  ",NO2MESCL,NOGIOSC2,NOCODBAN,NOCODBA2,NOFLRAGG"+;
                  ",NOFLGAVV,NODATAVV,NOINTMOR,NOMORTAS,NOCATFIN"+;
                  ",NOCATDER,NOIDRIDY,NOTIIDRI,NOCOIDRI,NONUMCOR"+;
                  ",NOCODPRI,NOPRIVCY,NOCODGRU,NOCODORI,NORIFINT"+;
                  ",NOCODOPE,NOASSOPE,NOCODZON,NOCODLIN,NOCODVAL"+;
                  ",NOCODAGE,NONUMLIS,NOCATCOM,NOCATCON,NOCACLFO"+;
                  ",NODATINS,NO__RESP,NOTIPOID,NOTIPDOC,NONUMDOC"+;
                  ",NODATRIL,NOAUTLOC,NOATTLAV,NONAGIUR,NODATCOS"+;
                  ",NO_SIGLA,NO_PAESE,NOESTNAS,NOFLANTI,NO__NOTE"+;
                  ",NORATING,NOGIORIT,NOVOCFIN,NOESCDOF,NODESPAR "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_NOCODICE)+;
                  ","+cp_ToStrODBCNull(this.w_NOCODCLI)+;
                  ","+cp_ToStrODBC(this.w_NODESCRI)+;
                  ","+cp_ToStrODBC(this.w_NODESCR2)+;
                  ","+cp_ToStrODBC(this.w_NOSOGGET)+;
                  ","+cp_ToStrODBCNull(this.w_NOCOD_RU)+;
                  ","+cp_ToStrODBC(this.w_NOBADGE)+;
                  ","+cp_ToStrODBC(this.w_NOCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_NO__NOME)+;
                  ","+cp_ToStrODBC(this.w_NOLOCNAS)+;
                  ","+cp_ToStrODBC(this.w_NOPRONAS)+;
                  ","+cp_ToStrODBC(this.w_NODATNAS)+;
                  ","+cp_ToStrODBC(this.w_NO_SESSO)+;
                  ","+cp_ToStrODBCNull(this.w_NONAZNAS)+;
                  ","+cp_ToStrODBC(this.w_NONUMCAR)+;
                  ","+cp_ToStrODBC(this.w_NOCODFIS)+;
                  ","+cp_ToStrODBC(this.w_NOPARIVA)+;
                  ","+cp_ToStrODBC(this.w_NONATGIU)+;
                  ","+cp_ToStrODBCNull(this.w_NOCODSAL)+;
                  ","+cp_ToStrODBC(this.w_NOINDIRI)+;
                  ","+cp_ToStrODBC(this.w_NOINDI_2)+;
                  ","+cp_ToStrODBC(this.w_NO___CAP)+;
                  ","+cp_ToStrODBC(this.w_NOLOCALI)+;
                  ","+cp_ToStrODBC(this.w_NOPROVIN)+;
                  ","+cp_ToStrODBCNull(this.w_NONAZION)+;
                  ","+cp_ToStrODBC(this.w_NOINDIR2)+;
                  ","+cp_ToStrODBC(this.w_NO___CAD)+;
                  ","+cp_ToStrODBC(this.w_NOLOCALD)+;
                  ","+cp_ToStrODBC(this.w_NOPROVID)+;
                  ","+cp_ToStrODBCNull(this.w_NONAZIOD)+;
                  ","+cp_ToStrODBC(this.w_NOTELEFO)+;
                  ","+cp_ToStrODBC(this.w_NONUMCEL)+;
                  ","+cp_ToStrODBC(this.w_NO_SKYPE)+;
                  ","+cp_ToStrODBC(this.w_NOTELFAX)+;
                  ","+cp_ToStrODBC(this.w_NO_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_NOINDWEB)+;
                  ","+cp_ToStrODBC(this.w_NO_EMPEC)+;
                  ","+cp_ToStrODBC(this.w_NOCHKSTA)+;
                  ","+cp_ToStrODBC(this.w_NOCHKMAI)+;
                  ","+cp_ToStrODBC(this.w_NOCHKPEC)+;
                  ","+cp_ToStrODBC(this.w_NOCHKFAX)+;
                  ","+cp_ToStrODBC(this.w_NOCHKCPZ)+;
                  ","+cp_ToStrODBC(this.w_NOCHKWWP)+;
                  ","+cp_ToStrODBC(this.w_NOCHKPTL)+;
                  ","+cp_ToStrODBC(this.w_NODTINVA)+;
                  ","+cp_ToStrODBC(this.w_NODTOBSO)+;
                  ","+cp_ToStrODBC(this.w_NOTIPPER)+;
                  ","+cp_ToStrODBC(this.w_NOTIPNOM)+;
                  ","+cp_ToStrODBC(this.w_NOFLGBEN)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_NOTIPCLI)+;
                  ","+cp_ToStrODBC(this.w_NOTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_NOCODCON)+;
                  ","+cp_ToStrODBCNull(this.w_NOCODPAG)+;
                  ","+cp_ToStrODBC(this.w_NOGIOFIS)+;
                  ","+cp_ToStrODBC(this.w_NO1MESCL)+;
                  ","+cp_ToStrODBC(this.w_NOGIOSC1)+;
                  ","+cp_ToStrODBC(this.w_NO2MESCL)+;
                  ","+cp_ToStrODBC(this.w_NOGIOSC2)+;
                  ","+cp_ToStrODBCNull(this.w_NOCODBAN)+;
                  ","+cp_ToStrODBC(this.w_NOCODBA2)+;
                  ","+cp_ToStrODBC(this.w_NOFLRAGG)+;
                  ","+cp_ToStrODBC(this.w_NOFLGAVV)+;
                  ","+cp_ToStrODBC(this.w_NODATAVV)+;
                  ","+cp_ToStrODBC(this.w_NOINTMOR)+;
                  ","+cp_ToStrODBC(this.w_NOMORTAS)+;
                  ","+cp_ToStrODBCNull(this.w_NOCATFIN)+;
                  ","+cp_ToStrODBCNull(this.w_NOCATDER)+;
                  ","+cp_ToStrODBC(this.w_NOIDRIDY)+;
                  ","+cp_ToStrODBC(this.w_NOTIIDRI)+;
                  ","+cp_ToStrODBC(this.w_NOCOIDRI)+;
                  ","+cp_ToStrODBC(this.w_NONUMCOR)+;
                  ","+cp_ToStrODBC(this.w_NOCODPRI)+;
                  ","+cp_ToStrODBC(this.w_NOPRIVCY)+;
                  ","+cp_ToStrODBCNull(this.w_NOCODGRU)+;
                  ","+cp_ToStrODBCNull(this.w_NOCODORI)+;
                  ","+cp_ToStrODBCNull(this.w_NORIFINT)+;
                  ","+cp_ToStrODBCNull(this.w_NOCODOPE)+;
                  ","+cp_ToStrODBC(this.w_NOASSOPE)+;
                  ","+cp_ToStrODBCNull(this.w_NOCODZON)+;
                  ","+cp_ToStrODBCNull(this.w_NOCODLIN)+;
                  ","+cp_ToStrODBCNull(this.w_NOCODVAL)+;
                  ","+cp_ToStrODBCNull(this.w_NOCODAGE)+;
                  ","+cp_ToStrODBCNull(this.w_NONUMLIS)+;
                  ","+cp_ToStrODBCNull(this.w_NOCATCOM)+;
                  ","+cp_ToStrODBCNull(this.w_NOCATCON)+;
                  ","+cp_ToStrODBCNull(this.w_NOCACLFO)+;
                  ","+cp_ToStrODBC(this.w_NODATINS)+;
                  ","+cp_ToStrODBCNull(this.w_NO__RESP)+;
                  ","+cp_ToStrODBC(this.w_NOTIPOID)+;
                  ","+cp_ToStrODBC(this.w_NOTIPDOC)+;
                  ","+cp_ToStrODBC(this.w_NONUMDOC)+;
                  ","+cp_ToStrODBC(this.w_NODATRIL)+;
                  ","+cp_ToStrODBC(this.w_NOAUTLOC)+;
                  ","+cp_ToStrODBC(this.w_NOATTLAV)+;
                  ","+cp_ToStrODBCNull(this.w_NONAGIUR)+;
                  ","+cp_ToStrODBC(this.w_NODATCOS)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBC(this.w_NO_SIGLA)+;
                  ","+cp_ToStrODBC(this.w_NO_PAESE)+;
                  ","+cp_ToStrODBC(this.w_NOESTNAS)+;
                  ","+cp_ToStrODBC(this.w_NOFLANTI)+;
                  ","+cp_ToStrODBC(this.w_NO__NOTE)+;
                  ","+cp_ToStrODBCNull(this.w_NORATING)+;
                  ","+cp_ToStrODBC(this.w_NOGIORIT)+;
                  ","+cp_ToStrODBCNull(this.w_NOVOCFIN)+;
                  ","+cp_ToStrODBC(this.w_NOESCDOF)+;
                  ","+cp_ToStrODBC(this.w_NODESPAR)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'OFF_NOMI')
        i_extval=cp_InsertValVFPExtFlds(this,'OFF_NOMI')
        cp_CheckDeletedKey(i_cTable,0,'NOCODICE',this.w_NOCODICE)
        INSERT INTO (i_cTable);
              (NOCODICE,NOCODCLI,NODESCRI,NODESCR2,NOSOGGET,NOCOD_RU,NOBADGE,NOCOGNOM,NO__NOME,NOLOCNAS,NOPRONAS,NODATNAS,NO_SESSO,NONAZNAS,NONUMCAR,NOCODFIS,NOPARIVA,NONATGIU,NOCODSAL,NOINDIRI,NOINDI_2,NO___CAP,NOLOCALI,NOPROVIN,NONAZION,NOINDIR2,NO___CAD,NOLOCALD,NOPROVID,NONAZIOD,NOTELEFO,NONUMCEL,NO_SKYPE,NOTELFAX,NO_EMAIL,NOINDWEB,NO_EMPEC,NOCHKSTA,NOCHKMAI,NOCHKPEC,NOCHKFAX,NOCHKCPZ,NOCHKWWP,NOCHKPTL,NODTINVA,NODTOBSO,NOTIPPER,NOTIPNOM,NOFLGBEN,UTCC,UTCV,UTDC,UTDV,NOTIPCLI,NOTIPCON,NOCODCON,NOCODPAG,NOGIOFIS,NO1MESCL,NOGIOSC1,NO2MESCL,NOGIOSC2,NOCODBAN,NOCODBA2,NOFLRAGG,NOFLGAVV,NODATAVV,NOINTMOR,NOMORTAS,NOCATFIN,NOCATDER,NOIDRIDY,NOTIIDRI,NOCOIDRI,NONUMCOR,NOCODPRI,NOPRIVCY,NOCODGRU,NOCODORI,NORIFINT,NOCODOPE,NOASSOPE,NOCODZON,NOCODLIN,NOCODVAL,NOCODAGE,NONUMLIS,NOCATCOM,NOCATCON,NOCACLFO,NODATINS,NO__RESP,NOTIPOID,NOTIPDOC,NONUMDOC,NODATRIL,NOAUTLOC,NOATTLAV,NONAGIUR,NODATCOS,NO_SIGLA,NO_PAESE,NOESTNAS,NOFLANTI,NO__NOTE,NORATING,NOGIORIT,NOVOCFIN,NOESCDOF,NODESPAR  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_NOCODICE;
                  ,this.w_NOCODCLI;
                  ,this.w_NODESCRI;
                  ,this.w_NODESCR2;
                  ,this.w_NOSOGGET;
                  ,this.w_NOCOD_RU;
                  ,this.w_NOBADGE;
                  ,this.w_NOCOGNOM;
                  ,this.w_NO__NOME;
                  ,this.w_NOLOCNAS;
                  ,this.w_NOPRONAS;
                  ,this.w_NODATNAS;
                  ,this.w_NO_SESSO;
                  ,this.w_NONAZNAS;
                  ,this.w_NONUMCAR;
                  ,this.w_NOCODFIS;
                  ,this.w_NOPARIVA;
                  ,this.w_NONATGIU;
                  ,this.w_NOCODSAL;
                  ,this.w_NOINDIRI;
                  ,this.w_NOINDI_2;
                  ,this.w_NO___CAP;
                  ,this.w_NOLOCALI;
                  ,this.w_NOPROVIN;
                  ,this.w_NONAZION;
                  ,this.w_NOINDIR2;
                  ,this.w_NO___CAD;
                  ,this.w_NOLOCALD;
                  ,this.w_NOPROVID;
                  ,this.w_NONAZIOD;
                  ,this.w_NOTELEFO;
                  ,this.w_NONUMCEL;
                  ,this.w_NO_SKYPE;
                  ,this.w_NOTELFAX;
                  ,this.w_NO_EMAIL;
                  ,this.w_NOINDWEB;
                  ,this.w_NO_EMPEC;
                  ,this.w_NOCHKSTA;
                  ,this.w_NOCHKMAI;
                  ,this.w_NOCHKPEC;
                  ,this.w_NOCHKFAX;
                  ,this.w_NOCHKCPZ;
                  ,this.w_NOCHKWWP;
                  ,this.w_NOCHKPTL;
                  ,this.w_NODTINVA;
                  ,this.w_NODTOBSO;
                  ,this.w_NOTIPPER;
                  ,this.w_NOTIPNOM;
                  ,this.w_NOFLGBEN;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_NOTIPCLI;
                  ,this.w_NOTIPCON;
                  ,this.w_NOCODCON;
                  ,this.w_NOCODPAG;
                  ,this.w_NOGIOFIS;
                  ,this.w_NO1MESCL;
                  ,this.w_NOGIOSC1;
                  ,this.w_NO2MESCL;
                  ,this.w_NOGIOSC2;
                  ,this.w_NOCODBAN;
                  ,this.w_NOCODBA2;
                  ,this.w_NOFLRAGG;
                  ,this.w_NOFLGAVV;
                  ,this.w_NODATAVV;
                  ,this.w_NOINTMOR;
                  ,this.w_NOMORTAS;
                  ,this.w_NOCATFIN;
                  ,this.w_NOCATDER;
                  ,this.w_NOIDRIDY;
                  ,this.w_NOTIIDRI;
                  ,this.w_NOCOIDRI;
                  ,this.w_NONUMCOR;
                  ,this.w_NOCODPRI;
                  ,this.w_NOPRIVCY;
                  ,this.w_NOCODGRU;
                  ,this.w_NOCODORI;
                  ,this.w_NORIFINT;
                  ,this.w_NOCODOPE;
                  ,this.w_NOASSOPE;
                  ,this.w_NOCODZON;
                  ,this.w_NOCODLIN;
                  ,this.w_NOCODVAL;
                  ,this.w_NOCODAGE;
                  ,this.w_NONUMLIS;
                  ,this.w_NOCATCOM;
                  ,this.w_NOCATCON;
                  ,this.w_NOCACLFO;
                  ,this.w_NODATINS;
                  ,this.w_NO__RESP;
                  ,this.w_NOTIPOID;
                  ,this.w_NOTIPDOC;
                  ,this.w_NONUMDOC;
                  ,this.w_NODATRIL;
                  ,this.w_NOAUTLOC;
                  ,this.w_NOATTLAV;
                  ,this.w_NONAGIUR;
                  ,this.w_NODATCOS;
                  ,this.w_NO_SIGLA;
                  ,this.w_NO_PAESE;
                  ,this.w_NOESTNAS;
                  ,this.w_NOFLANTI;
                  ,this.w_NO__NOTE;
                  ,this.w_NORATING;
                  ,this.w_NOGIORIT;
                  ,this.w_NOVOCFIN;
                  ,this.w_NOESCDOF;
                  ,this.w_NODESPAR;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.OFF_NOMI_IDX,i_nConn)
      *
      * update OFF_NOMI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'OFF_NOMI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " NOCODCLI="+cp_ToStrODBCNull(this.w_NOCODCLI)+;
             ",NODESCRI="+cp_ToStrODBC(this.w_NODESCRI)+;
             ",NODESCR2="+cp_ToStrODBC(this.w_NODESCR2)+;
             ",NOSOGGET="+cp_ToStrODBC(this.w_NOSOGGET)+;
             ",NOCOD_RU="+cp_ToStrODBCNull(this.w_NOCOD_RU)+;
             ",NOBADGE="+cp_ToStrODBC(this.w_NOBADGE)+;
             ",NOCOGNOM="+cp_ToStrODBC(this.w_NOCOGNOM)+;
             ",NO__NOME="+cp_ToStrODBC(this.w_NO__NOME)+;
             ",NOLOCNAS="+cp_ToStrODBC(this.w_NOLOCNAS)+;
             ",NOPRONAS="+cp_ToStrODBC(this.w_NOPRONAS)+;
             ",NODATNAS="+cp_ToStrODBC(this.w_NODATNAS)+;
             ",NO_SESSO="+cp_ToStrODBC(this.w_NO_SESSO)+;
             ",NONAZNAS="+cp_ToStrODBCNull(this.w_NONAZNAS)+;
             ",NONUMCAR="+cp_ToStrODBC(this.w_NONUMCAR)+;
             ",NOCODFIS="+cp_ToStrODBC(this.w_NOCODFIS)+;
             ",NOPARIVA="+cp_ToStrODBC(this.w_NOPARIVA)+;
             ",NONATGIU="+cp_ToStrODBC(this.w_NONATGIU)+;
             ",NOCODSAL="+cp_ToStrODBCNull(this.w_NOCODSAL)+;
             ",NOINDIRI="+cp_ToStrODBC(this.w_NOINDIRI)+;
             ",NOINDI_2="+cp_ToStrODBC(this.w_NOINDI_2)+;
             ",NO___CAP="+cp_ToStrODBC(this.w_NO___CAP)+;
             ",NOLOCALI="+cp_ToStrODBC(this.w_NOLOCALI)+;
             ",NOPROVIN="+cp_ToStrODBC(this.w_NOPROVIN)+;
             ",NONAZION="+cp_ToStrODBCNull(this.w_NONAZION)+;
             ",NOINDIR2="+cp_ToStrODBC(this.w_NOINDIR2)+;
             ",NO___CAD="+cp_ToStrODBC(this.w_NO___CAD)+;
             ",NOLOCALD="+cp_ToStrODBC(this.w_NOLOCALD)+;
             ",NOPROVID="+cp_ToStrODBC(this.w_NOPROVID)+;
             ",NONAZIOD="+cp_ToStrODBCNull(this.w_NONAZIOD)+;
             ",NOTELEFO="+cp_ToStrODBC(this.w_NOTELEFO)+;
             ",NONUMCEL="+cp_ToStrODBC(this.w_NONUMCEL)+;
             ",NO_SKYPE="+cp_ToStrODBC(this.w_NO_SKYPE)+;
             ",NOTELFAX="+cp_ToStrODBC(this.w_NOTELFAX)+;
             ",NO_EMAIL="+cp_ToStrODBC(this.w_NO_EMAIL)+;
             ",NOINDWEB="+cp_ToStrODBC(this.w_NOINDWEB)+;
             ",NO_EMPEC="+cp_ToStrODBC(this.w_NO_EMPEC)+;
             ",NOCHKSTA="+cp_ToStrODBC(this.w_NOCHKSTA)+;
             ",NOCHKMAI="+cp_ToStrODBC(this.w_NOCHKMAI)+;
             ",NOCHKPEC="+cp_ToStrODBC(this.w_NOCHKPEC)+;
             ",NOCHKFAX="+cp_ToStrODBC(this.w_NOCHKFAX)+;
             ",NOCHKCPZ="+cp_ToStrODBC(this.w_NOCHKCPZ)+;
             ",NOCHKWWP="+cp_ToStrODBC(this.w_NOCHKWWP)+;
             ",NOCHKPTL="+cp_ToStrODBC(this.w_NOCHKPTL)+;
             ",NODTINVA="+cp_ToStrODBC(this.w_NODTINVA)+;
             ",NODTOBSO="+cp_ToStrODBC(this.w_NODTOBSO)+;
             ",NOTIPPER="+cp_ToStrODBC(this.w_NOTIPPER)+;
             ",NOTIPNOM="+cp_ToStrODBC(this.w_NOTIPNOM)+;
             ",NOFLGBEN="+cp_ToStrODBC(this.w_NOFLGBEN)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",NOTIPCLI="+cp_ToStrODBC(this.w_NOTIPCLI)+;
             ",NOTIPCON="+cp_ToStrODBC(this.w_NOTIPCON)+;
             ",NOCODCON="+cp_ToStrODBCNull(this.w_NOCODCON)+;
             ",NOCODPAG="+cp_ToStrODBCNull(this.w_NOCODPAG)+;
             ",NOGIOFIS="+cp_ToStrODBC(this.w_NOGIOFIS)+;
             ",NO1MESCL="+cp_ToStrODBC(this.w_NO1MESCL)+;
             ",NOGIOSC1="+cp_ToStrODBC(this.w_NOGIOSC1)+;
             ",NO2MESCL="+cp_ToStrODBC(this.w_NO2MESCL)+;
             ",NOGIOSC2="+cp_ToStrODBC(this.w_NOGIOSC2)+;
             ",NOCODBAN="+cp_ToStrODBCNull(this.w_NOCODBAN)+;
             ",NOCODBA2="+cp_ToStrODBC(this.w_NOCODBA2)+;
             ",NOFLRAGG="+cp_ToStrODBC(this.w_NOFLRAGG)+;
             ",NOFLGAVV="+cp_ToStrODBC(this.w_NOFLGAVV)+;
             ",NODATAVV="+cp_ToStrODBC(this.w_NODATAVV)+;
             ",NOINTMOR="+cp_ToStrODBC(this.w_NOINTMOR)+;
             ",NOMORTAS="+cp_ToStrODBC(this.w_NOMORTAS)+;
             ",NOCATFIN="+cp_ToStrODBCNull(this.w_NOCATFIN)+;
             ",NOCATDER="+cp_ToStrODBCNull(this.w_NOCATDER)+;
             ",NOIDRIDY="+cp_ToStrODBC(this.w_NOIDRIDY)+;
             ",NOTIIDRI="+cp_ToStrODBC(this.w_NOTIIDRI)+;
             ",NOCOIDRI="+cp_ToStrODBC(this.w_NOCOIDRI)+;
             ",NONUMCOR="+cp_ToStrODBC(this.w_NONUMCOR)+;
             ",NOCODPRI="+cp_ToStrODBC(this.w_NOCODPRI)+;
             ",NOPRIVCY="+cp_ToStrODBC(this.w_NOPRIVCY)+;
             ",NOCODGRU="+cp_ToStrODBCNull(this.w_NOCODGRU)+;
             ",NOCODORI="+cp_ToStrODBCNull(this.w_NOCODORI)+;
             ",NORIFINT="+cp_ToStrODBCNull(this.w_NORIFINT)+;
             ",NOCODOPE="+cp_ToStrODBCNull(this.w_NOCODOPE)+;
             ",NOASSOPE="+cp_ToStrODBC(this.w_NOASSOPE)+;
             ",NOCODZON="+cp_ToStrODBCNull(this.w_NOCODZON)+;
             ",NOCODLIN="+cp_ToStrODBCNull(this.w_NOCODLIN)+;
             ",NOCODVAL="+cp_ToStrODBCNull(this.w_NOCODVAL)+;
             ",NOCODAGE="+cp_ToStrODBCNull(this.w_NOCODAGE)+;
             ",NONUMLIS="+cp_ToStrODBCNull(this.w_NONUMLIS)+;
             ",NOCATCOM="+cp_ToStrODBCNull(this.w_NOCATCOM)+;
             ",NOCATCON="+cp_ToStrODBCNull(this.w_NOCATCON)+;
             ",NOCACLFO="+cp_ToStrODBCNull(this.w_NOCACLFO)+;
             ",NODATINS="+cp_ToStrODBC(this.w_NODATINS)+;
             ",NO__RESP="+cp_ToStrODBCNull(this.w_NO__RESP)+;
             ",NOTIPOID="+cp_ToStrODBC(this.w_NOTIPOID)+;
             ",NOTIPDOC="+cp_ToStrODBC(this.w_NOTIPDOC)+;
             ",NONUMDOC="+cp_ToStrODBC(this.w_NONUMDOC)+;
             ",NODATRIL="+cp_ToStrODBC(this.w_NODATRIL)+;
             ",NOAUTLOC="+cp_ToStrODBC(this.w_NOAUTLOC)+;
             ",NOATTLAV="+cp_ToStrODBC(this.w_NOATTLAV)+;
             ",NONAGIUR="+cp_ToStrODBCNull(this.w_NONAGIUR)+;
             ",NODATCOS="+cp_ToStrODBC(this.w_NODATCOS)+;
             ",NO_SIGLA="+cp_ToStrODBC(this.w_NO_SIGLA)+;
             ""
             i_nnn=i_nnn+;
             ",NO_PAESE="+cp_ToStrODBC(this.w_NO_PAESE)+;
             ",NOESTNAS="+cp_ToStrODBC(this.w_NOESTNAS)+;
             ",NOFLANTI="+cp_ToStrODBC(this.w_NOFLANTI)+;
             ",NO__NOTE="+cp_ToStrODBC(this.w_NO__NOTE)+;
             ",NORATING="+cp_ToStrODBCNull(this.w_NORATING)+;
             ",NOGIORIT="+cp_ToStrODBC(this.w_NOGIORIT)+;
             ",NOVOCFIN="+cp_ToStrODBCNull(this.w_NOVOCFIN)+;
             ",NOESCDOF="+cp_ToStrODBC(this.w_NOESCDOF)+;
             ",NODESPAR="+cp_ToStrODBC(this.w_NODESPAR)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'OFF_NOMI')
        i_cWhere = cp_PKFox(i_cTable  ,'NOCODICE',this.w_NOCODICE  )
        UPDATE (i_cTable) SET;
              NOCODCLI=this.w_NOCODCLI;
             ,NODESCRI=this.w_NODESCRI;
             ,NODESCR2=this.w_NODESCR2;
             ,NOSOGGET=this.w_NOSOGGET;
             ,NOCOD_RU=this.w_NOCOD_RU;
             ,NOBADGE=this.w_NOBADGE;
             ,NOCOGNOM=this.w_NOCOGNOM;
             ,NO__NOME=this.w_NO__NOME;
             ,NOLOCNAS=this.w_NOLOCNAS;
             ,NOPRONAS=this.w_NOPRONAS;
             ,NODATNAS=this.w_NODATNAS;
             ,NO_SESSO=this.w_NO_SESSO;
             ,NONAZNAS=this.w_NONAZNAS;
             ,NONUMCAR=this.w_NONUMCAR;
             ,NOCODFIS=this.w_NOCODFIS;
             ,NOPARIVA=this.w_NOPARIVA;
             ,NONATGIU=this.w_NONATGIU;
             ,NOCODSAL=this.w_NOCODSAL;
             ,NOINDIRI=this.w_NOINDIRI;
             ,NOINDI_2=this.w_NOINDI_2;
             ,NO___CAP=this.w_NO___CAP;
             ,NOLOCALI=this.w_NOLOCALI;
             ,NOPROVIN=this.w_NOPROVIN;
             ,NONAZION=this.w_NONAZION;
             ,NOINDIR2=this.w_NOINDIR2;
             ,NO___CAD=this.w_NO___CAD;
             ,NOLOCALD=this.w_NOLOCALD;
             ,NOPROVID=this.w_NOPROVID;
             ,NONAZIOD=this.w_NONAZIOD;
             ,NOTELEFO=this.w_NOTELEFO;
             ,NONUMCEL=this.w_NONUMCEL;
             ,NO_SKYPE=this.w_NO_SKYPE;
             ,NOTELFAX=this.w_NOTELFAX;
             ,NO_EMAIL=this.w_NO_EMAIL;
             ,NOINDWEB=this.w_NOINDWEB;
             ,NO_EMPEC=this.w_NO_EMPEC;
             ,NOCHKSTA=this.w_NOCHKSTA;
             ,NOCHKMAI=this.w_NOCHKMAI;
             ,NOCHKPEC=this.w_NOCHKPEC;
             ,NOCHKFAX=this.w_NOCHKFAX;
             ,NOCHKCPZ=this.w_NOCHKCPZ;
             ,NOCHKWWP=this.w_NOCHKWWP;
             ,NOCHKPTL=this.w_NOCHKPTL;
             ,NODTINVA=this.w_NODTINVA;
             ,NODTOBSO=this.w_NODTOBSO;
             ,NOTIPPER=this.w_NOTIPPER;
             ,NOTIPNOM=this.w_NOTIPNOM;
             ,NOFLGBEN=this.w_NOFLGBEN;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,NOTIPCLI=this.w_NOTIPCLI;
             ,NOTIPCON=this.w_NOTIPCON;
             ,NOCODCON=this.w_NOCODCON;
             ,NOCODPAG=this.w_NOCODPAG;
             ,NOGIOFIS=this.w_NOGIOFIS;
             ,NO1MESCL=this.w_NO1MESCL;
             ,NOGIOSC1=this.w_NOGIOSC1;
             ,NO2MESCL=this.w_NO2MESCL;
             ,NOGIOSC2=this.w_NOGIOSC2;
             ,NOCODBAN=this.w_NOCODBAN;
             ,NOCODBA2=this.w_NOCODBA2;
             ,NOFLRAGG=this.w_NOFLRAGG;
             ,NOFLGAVV=this.w_NOFLGAVV;
             ,NODATAVV=this.w_NODATAVV;
             ,NOINTMOR=this.w_NOINTMOR;
             ,NOMORTAS=this.w_NOMORTAS;
             ,NOCATFIN=this.w_NOCATFIN;
             ,NOCATDER=this.w_NOCATDER;
             ,NOIDRIDY=this.w_NOIDRIDY;
             ,NOTIIDRI=this.w_NOTIIDRI;
             ,NOCOIDRI=this.w_NOCOIDRI;
             ,NONUMCOR=this.w_NONUMCOR;
             ,NOCODPRI=this.w_NOCODPRI;
             ,NOPRIVCY=this.w_NOPRIVCY;
             ,NOCODGRU=this.w_NOCODGRU;
             ,NOCODORI=this.w_NOCODORI;
             ,NORIFINT=this.w_NORIFINT;
             ,NOCODOPE=this.w_NOCODOPE;
             ,NOASSOPE=this.w_NOASSOPE;
             ,NOCODZON=this.w_NOCODZON;
             ,NOCODLIN=this.w_NOCODLIN;
             ,NOCODVAL=this.w_NOCODVAL;
             ,NOCODAGE=this.w_NOCODAGE;
             ,NONUMLIS=this.w_NONUMLIS;
             ,NOCATCOM=this.w_NOCATCOM;
             ,NOCATCON=this.w_NOCATCON;
             ,NOCACLFO=this.w_NOCACLFO;
             ,NODATINS=this.w_NODATINS;
             ,NO__RESP=this.w_NO__RESP;
             ,NOTIPOID=this.w_NOTIPOID;
             ,NOTIPDOC=this.w_NOTIPDOC;
             ,NONUMDOC=this.w_NONUMDOC;
             ,NODATRIL=this.w_NODATRIL;
             ,NOAUTLOC=this.w_NOAUTLOC;
             ,NOATTLAV=this.w_NOATTLAV;
             ,NONAGIUR=this.w_NONAGIUR;
             ,NODATCOS=this.w_NODATCOS;
             ,NO_SIGLA=this.w_NO_SIGLA;
             ,NO_PAESE=this.w_NO_PAESE;
             ,NOESTNAS=this.w_NOESTNAS;
             ,NOFLANTI=this.w_NOFLANTI;
             ,NO__NOTE=this.w_NO__NOTE;
             ,NORATING=this.w_NORATING;
             ,NOGIORIT=this.w_NOGIORIT;
             ,NOVOCFIN=this.w_NOVOCFIN;
             ,NOESCDOF=this.w_NOESCDOF;
             ,NODESPAR=this.w_NODESPAR;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAR_MCC : Saving
      this.GSAR_MCC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_NOTIPBAN,"CCTIPCON";
             ,this.w_NOCODICE,"CCCODCON";
             )
      this.GSAR_MCC.mReplace()
      * --- GSAR_MAN : Saving
      this.GSAR_MAN.ChangeRow(this.cRowID+'      1',0;
             ,this.w_NOCODICE,"ANCODICE";
             )
      this.GSAR_MAN.mReplace()
      * --- GSAR_MCN : Saving
      this.GSAR_MCN.ChangeRow(this.cRowID+'      1',0;
             ,this.w_NOCODICE,"NCCODICE";
             )
      this.GSAR_MCN.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAR_MCC : Deleting
    this.GSAR_MCC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_NOTIPBAN,"CCTIPCON";
           ,this.w_NOCODICE,"CCCODCON";
           )
    this.GSAR_MCC.mDelete()
    * --- GSAR_MAN : Deleting
    this.GSAR_MAN.ChangeRow(this.cRowID+'      1',0;
           ,this.w_NOCODICE,"ANCODICE";
           )
    this.GSAR_MAN.mDelete()
    * --- GSAR_MCN : Deleting
    this.GSAR_MCN.ChangeRow(this.cRowID+'      1',0;
           ,this.w_NOCODICE,"NCCODICE";
           )
    this.GSAR_MCN.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.OFF_NOMI_IDX,i_nConn)
      *
      * delete OFF_NOMI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'NOCODICE',this.w_NOCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsar_ano
    IF (g_OFFE='S') and not(bTrsErr)
       GSAR_BDN(this, this.w_NOCODICE,this.w_NODESCRI)
    ENDIF
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    if i_bUpd
      with this
            .w_PARCALLER = this.pTipMask
        .DoRTCalc(2,3,.t.)
          .link_1_4('Full')
        .DoRTCalc(5,7,.t.)
        if .o_NOFLGBEN<>.w_NOFLGBEN.or. .o_NOSOGGET<>.w_NOSOGGET.or. .o_NOCOGNOM<>.w_NOCOGNOM.or. .o_NO__NOME<>.w_NO__NOME
            .w_NODESCRI = iif(.w_NOFLGBEN='B',IIF(.w_NOSOGGET='PF',LEFT(ALLTRIM(.w_NOCOGNOM)+' '+ALLTRIM(.w_NO__NOME),60),''),.w_NODESCRI)
        endif
        if .o_NOFLGBEN<>.w_NOFLGBEN.or. .o_NOSOGGET<>.w_NOSOGGET
            .w_NODESCR2 = iif(.w_NOFLGBEN='B' AND .w_NOSOGGET='PF',' ',.w_NODESCR2)
        endif
        .DoRTCalc(10,11,.t.)
        if .o_NOTIPPER<>.w_NOTIPPER
            .w_NOCOD_RU = iif(.w_NOTIPPER='A',' ',.w_NOCOD_RU)
          .link_1_12('Full')
        endif
        if .o_NOTIPPER<>.w_NOTIPPER
            .w_NOBADGE = iif(.w_NOTIPPER='A',' ',.w_NOBADGE)
        endif
        if .o_NOTIPPER<>.w_NOTIPPER
            .w_NOCOGNOM = iif(.w_NOTIPPER='A',' ',.w_NOCOGNOM)
        endif
        if .o_NOTIPPER<>.w_NOTIPPER
            .w_NOCOGNOM = iif(.w_NOTIPPER='A',' ',.w_NOCOGNOM)
        endif
        if .o_NOTIPPER<>.w_NOTIPPER
            .w_NO__NOME = iif(.w_NOTIPPER='A',' ',.w_NO__NOME)
        endif
        if .o_NOTIPPER<>.w_NOTIPPER
            .w_NO__NOME = iif(.w_NOTIPPER='A',' ',.w_NO__NOME)
        endif
        if .o_NOTIPPER<>.w_NOTIPPER.or. .o_LOCNAS<>.w_LOCNAS
            .w_NOLOCNAS = iif(.w_NOTIPPER='A',' ',LEFT(.w_LOCNAS,30 ))
        endif
        if .o_NOTIPPER<>.w_NOTIPPER.or. .o_LOCNAS<>.w_LOCNAS
            .w_NOLOCNAS = iif(.w_NOTIPPER='A',' ',LEFT(.w_LOCNAS,30))
        endif
        if .o_NOTIPPER<>.w_NOTIPPER
            .w_NOPRONAS = iif(.w_NOTIPPER='A',' ',.w_NOPRONAS)
        endif
        if .o_NOTIPPER<>.w_NOTIPPER
            .w_NOPRONAS = iif(.w_NOTIPPER='A',' ',.w_NOPRONAS)
        endif
        if .o_NOTIPPER<>.w_NOTIPPER
            .w_NODATNAS = iif(.w_NOTIPPER<>'A',.w_NODATNAS,CTOD('  -  -    '))
        endif
        .DoRTCalc(23,23,.t.)
        if .o_NOCOD_RU<>.w_NOCOD_RU
          .link_1_24('Full')
        endif
        if .o_NOTIPPER<>.w_NOTIPPER
            .w_NONUMCAR = iif(.w_NOTIPPER='A',' ',.w_NONUMCAR)
        endif
        .DoRTCalc(26,27,.t.)
        if .o_NOSOGGET<>.w_NOSOGGET
            .w_NONATGIU = IIF(.w_NOSOGGET='PF', 'PFI', '')
        endif
        .DoRTCalc(29,29,.t.)
        if .o_NOINDIRI<>.w_NOINDIRI
            .w_NOINDIRI = LEFT(.w_NOINDIRI,35)
        endif
        if .o_NOINDI_2<>.w_NOINDI_2
            .w_NOINDI_2 = left(.w_NOINDI_2,35)
        endif
        .DoRTCalc(32,39,.t.)
        if .o_NOINDIR2<>.w_NOINDIR2
            .w_NOINDIR2 = LEFT(.w_NOINDIR2,35)
        endif
        .DoRTCalc(41,42,.t.)
        if .o_LOCALD<>.w_LOCALD
            .w_NOLOCALD = LEFT(.w_LOCALD,30)
        endif
            .w_OCOGNOM = .w_NOCOGNOM
            .w_ONOME = .w_NO__NOME
        .DoRTCalc(46,46,.t.)
        if .o_LOCALD<>.w_LOCALD
            .w_NOLOCALD = LEFT(.w_LOCALD,30)
        endif
        .DoRTCalc(48,55,.t.)
        if .o_NOINDWEB<>.w_NOINDWEB
            .w_NOINDWEB = LEFT(.w_NOINDWEB,50)
        endif
        .DoRTCalc(57,77,.t.)
            .w_TIPOPE = UPPER(this.cFunction)
            .w_NOTIPCLI = IIF(IsAlt() AND .w_NOTIPNOM='F','F','C')
            .w_OBTEST = i_datsys
        if .o_NOCODICE<>.w_NOCODICE
            .w_NOTIPO = .w_NOTIPNOM
        endif
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_108.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_128.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_129.Calculate()
        if .o_NOSOGGET<>.w_NOSOGGET
          .Calculate_FATZINRGJB()
        endif
        if .o_NOSOGGET<>.w_NOSOGGET
          .Calculate_WWPJDQWIRN()
        endif
        if .o_NOSOGGET<>.w_NOSOGGET
          .Calculate_QWKRQHGBHI()
        endif
        .oPgFrm.Page1.oPag.oObj_1_147.Calculate()
        if .o_NOCODFIS<>.w_NOCODFIS
          .Calculate_XNLBETFNTC()
        endif
        if .o_BANCAAHR<>.w_BANCAAHR
          .Calculate_GOBYLZEOPZ()
        endif
        if .o_BANCAAHE<>.w_BANCAAHE
          .Calculate_PQHBNYDLBB()
        endif
        .oPgFrm.Page1.oPag.oObj_1_162.Calculate()
        .DoRTCalc(82,99,.t.)
        if .o_NOFLGBEN<>.w_NOFLGBEN
            .w_NOTIPCON = "F"
        endif
        .DoRTCalc(101,101,.t.)
        if .o_NOCODCON<>.w_NOCODCON
            .w_NOCODPAG = .w_NOCODPAG
          .link_2_4('Full')
        endif
        .DoRTCalc(103,105,.t.)
            .w_DESMES1 = LEFT(IIF(.w_NO1MESCL>0 AND .w_NO1MESCL<13, g_MESE[.w_NO1MESCL], "")+SPACE(12),12)
        .DoRTCalc(107,109,.t.)
            .w_DESMES2 = LEFT(IIF(.w_NO2MESCL>0 AND .w_NO2MESCL<13, g_MESE[.w_NO2MESCL], "")+SPACE(12),12)
        if .o_NOCODCON<>.w_NOCODCON
            .w_NOCODBAN = .w_NOCODBAN
          .link_2_13('Full')
        endif
        .DoRTCalc(112,116,.t.)
        if .o_NOINTMOR<>.w_NOINTMOR
            .w_NOMORTAS = 0
        endif
        .oPgFrm.Page2.oPag.oObj_2_27.Calculate()
        .DoRTCalc(118,125,.t.)
        if .o_NOCODBA2<>.w_NOCODBA2
          .link_2_43('Full')
        endif
        if .o_NOTIIDRI<>.w_NOTIIDRI.or. .o_NOCOIDRI<>.w_NOCOIDRI
          .Calculate_DLGMYOICKZ()
        endif
        if .o_NOIDRIDY<>.w_NOIDRIDY
          .Calculate_IRPKOAKLYU()
        endif
        .DoRTCalc(127,133,.t.)
            .w_ANRITENU = 'N'
        .oPgFrm.Page3.oPag.ZoomAll.Calculate()
        .DoRTCalc(135,149,.t.)
            .w_SERALL = NVL(.w_ZoomAll.getVar('ALSERIAL'), SPACE(10))
            .w_PATALL = NVL(.w_ZoomAll.getVar('ALPATFIL'), SPACE(254))
            .w_CODI = .w_NOCODICE
        .DoRTCalc(153,158,.t.)
            .w_CODI = .w_NOCODICE
        .DoRTCalc(160,192,.t.)
        if .o_NOCODBA2<>.w_NOCODBA2
            .w_BANCAAHR = IIF (g_APPLICATION <> "ADHOC REVOLUTION" ,.w_BANCAAHR,.w_NOCODBA2)
          .link_4_51('Full')
        endif
        if .o_NOCODBA2<>.w_NOCODBA2
            .w_BANCAAHE = IIF (g_APPLICATION = "ADHOC REVOLUTION" ,.w_BANCAAHE , .w_NOCODBA2)
          .link_4_52('Full')
        endif
        .DoRTCalc(195,202,.t.)
        if .o_NORIFINT<>.w_NORIFINT
            .w_NODENOMRIF = alltrim(.w_COGNOME)+space(1)+alltrim(.w_NOME)
        endif
        .DoRTCalc(204,215,.t.)
            .w_CODI = .w_NOCODICE
        .oPgFrm.Page6.oPag.ZoomOff.Calculate()
        .DoRTCalc(217,222,.t.)
            .w_SEROFF = NVL(.w_ZoomOff.getVar('OFSERIAL'), SPACE(10))
        .DoRTCalc(224,232,.t.)
            .w_CODI = .w_NOCODICE
            .w_OFDATDOC = NVL(.w_ZoomOff.getVar('OFDATDOC'), cp_CharToDate('  /  /  '))
            .w_OFPATFWP = NVL(.w_ZoomOff.getVar('OFPATFWP'), SPACE(254))
            .w_OFPATPDF = NVL(.w_ZoomOff.getVar('OFPATPDF'), SPACE(254))
            .w_PATHARC = NVL(.w_ZoomOff.getVar('MOPATARC'), SPACE(254))
            .w_PERIODO = NVL(.w_ZoomOff.getVar('MONUMPER'), SPACE(1))
        if .o_NOCODICE<>.w_NOCODICE
            .w_OFCODNOM = .w_NOCODICE
        endif
        .oPgFrm.Page7.oPag.ZOOM.Calculate()
        .DoRTCalc(240,255,.t.)
        if .o_NOMINI<>.w_NOMINI.or. .o_NOMFIN<>.w_NOMFIN
          .link_7_15('Full')
        endif
        .DoRTCalc(257,260,.t.)
            .w_ATSERIAL = .w_ZOOM.GetVar('ATSERIAL')
        if .o_SUBJECT<>.w_SUBJECT
            .w_OGGETTO = '%'+ALLTRIM(.w_SUBJECT)
        endif
        if .o_DATAINI<>.w_DATAINI.or. .o_ORAINI<>.w_ORAINI.or. .o_MININI<>.w_MININI
          .Calculate_HEZCDUNPFW()
        endif
        if .o_DATAFIN<>.w_DATAFIN.or. .o_ORAFIN<>.w_ORAFIN.or. .o_MINFIN<>.w_MINFIN
          .Calculate_ZQLIBXAFGH()
        endif
        .DoRTCalc(263,266,.t.)
        if .o_NOCODICE<>.w_NOCODICE
            .w_NOMINI = .w_NOCODICE
        endif
        if .o_NOCODICE<>.w_NOCODICE
            .w_NOMFIN = .w_NOCODICE
        endif
        .DoRTCalc(269,270,.t.)
        if .o_PRIOINI<>.w_PRIOINI
            .w_PRIOFIN = .w_PRIOINI
        endif
        .DoRTCalc(272,272,.t.)
            .w_OFF_SER = ''
        .DoRTCalc(274,276,.t.)
        if .o_NO__RESP<>.w_NO__RESP.or. .o_CODUTE<>.w_CODUTE
            .w_DATI = iif(!empty(.w_NO__RESP), ReadDipend(.w_CODUTE,"D"), space(40) )
        endif
        .DoRTCalc(278,279,.t.)
        if .o_NONUMDOC<>.w_NONUMDOC.or. .o_NONUMCAR<>.w_NONUMCAR
            .w_NONUMDOC = iif(.w_NOTIPDOC='01'and !empty(.w_NONUMCAR),.w_NONUMCAR,.w_NONUMDOC)
        endif
        if .o_NOTIPDOC<>.w_NOTIPDOC
          .Calculate_EVFCJUGOFG()
        endif
        .oPgFrm.Page8.oPag.oObj_8_44.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_45.Calculate()
        .DoRTCalc(281,300,.t.)
            .w_CODI = .w_NOCODICE
        .DoRTCalc(302,306,.t.)
        if .o_NOCODICE<>.w_NOCODICE.or. .o_CONTINI<>.w_CONTINI
            .w_EMAIL = evl(.w_NO_EMAIL,.w_APEMAIL)
        endif
        .DoRTCalc(308,320,.t.)
        if .o_NOCODICE<>.w_NOCODICE.or. .o_CONTINI<>.w_CONTINI
            .w_EMPEC = evl(.w_NO_EMPEC,.w_APEMPEC)
        endif
            .w_OFCODMOD = NVL(.w_ZoomOff.getVar('OFCODMOD'), SPACE(5))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
          if g_OFNUME='S' AND .p_AUT=0
             cp_AskTableProg(this,i_nConn,"PRNUNMOF","i_CODAZI,w_NOCODICE")
          endif
          .op_NOCODICE = .w_NOCODICE
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(323,325,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_107.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_108.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_128.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_129.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_147.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_162.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_27.Calculate()
        .oPgFrm.Page3.oPag.ZoomAll.Calculate()
        .oPgFrm.Page6.oPag.ZoomOff.Calculate()
        .oPgFrm.Page7.oPag.ZOOM.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_44.Calculate()
        .oPgFrm.Page8.oPag.oObj_8_45.Calculate()
    endwith
  return

  proc Calculate_ZQDJUELRPO()
    with this
          * --- Sbiancamento persona,denominazione,partita iva al cambio del tipo
          .w_NOCOD_RU = space(5)
          .w_NOPARIVA = space(12)
          .w_NODESCRI = space(444)
    endwith
  endproc
  proc Calculate_FATZINRGJB()
    with this
          * --- Valorizzo il tpo
          .w_NOTIPPER = iif(.w_NOSOGGET='EN' or IsAlt(),'A','D')
    endwith
  endproc
  proc Calculate_WWPJDQWIRN()
    with this
          * --- Divide ragione sociale
          GSAR_BDR(this;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_QWKRQHGBHI()
    with this
          * --- Sbianco inf. persona fisica
          .w_NOCOGNOM = iif(.w_NOSOGGET='PF', .w_NOCOGNOM, '')
          .w_NO__NOME = iif(.w_NOSOGGET='PF', .w_NO__NOME, '')
          .w_NOLOCNAS = iif(.w_NOSOGGET='PF', .w_NOLOCNAS, '')
          .w_NOPRONAS = iif(.w_NOSOGGET='PF', .w_NOPRONAS, '')
          .w_NODATNAS = iif(.w_NOSOGGET='PF', .w_NODATNAS, Cp_CharToDate('  -  -    '))
          .w_NO_SESSO = iif(.w_NOSOGGET='PF', .w_NO_SESSO, '')
          .w_NONUMCAR = iif(.w_NOSOGGET='PF', .w_NONUMCAR, '')
          .w_NONAZNAS = iif(.w_NOSOGGET='PF', .w_NONAZNAS, '')
          .w_DESNAZ1 = iif(.w_NOSOGGET='PF', .w_DESNAZ1, '')
          .w_CODISO1 = iif(.w_NOSOGGET='PF', .w_CODISO1, '')
    endwith
  endproc
  proc Calculate_XNLBETFNTC()
    with this
          * --- w_NOCODFIS changed
          .w_ERR = cfanag(.w_NOCODFIS,'V')
          .w_NOSOGGET = iif(.w_ERR, .w_NOSOGGET , 'PF' )
          .w_NODATNAS = iif(.w_ERR, .w_NODATNAS, IIF (.w_CONTA<>.o_CONTA, .w_NODATNAS ,cfanag(.w_NOCODFIS,'D') ))
          .w_NO_SESSO = iif(.w_ERR,  .w_NO_SESSO , cfanag(.w_NOCODFIS,'S'))
          .w_NOLOCNAS = iif(.w_ERR, .w_NOLOCNAS , cfanag(.w_NOCODFIS,'L'))
          .w_NOPRONAS = iif(.w_ERR,  .w_NOPRONAS , cfanag(.w_NOCODFIS,'P'))
          .w_CODCOM = iif(.w_ERR, .w_CODCOM , SUBSTR(.w_NOCODFIS,12,4))
    endwith
  endproc
  proc Calculate_GOBYLZEOPZ()
    with this
          * --- Legge nostra banca in AHR
          .w_NOCODBA2 = .w_BANCAAHR
    endwith
  endproc
  proc Calculate_PQHBNYDLBB()
    with this
          * --- Legge nostra banca in AHE
          .w_NOCODBA2 = .w_BANCAAHE
    endwith
  endproc
  proc Calculate_SOXWATTGMZ()
    with this
          * --- Resetta w_PROGR
          GSAR_BTT(this;
              ,'RESETTA';
             )
    endwith
  endproc
  proc Calculate_PGQNPFGCXZ()
    with this
          * --- Colora pagina note
          GSUT_BCN(this;
              ,"COLORA";
              ,.w_NO__NOTE;
             )
    endwith
  endproc
  proc Calculate_GRJFLMWOLU()
    with this
          * --- Numero pagina note
          GSUT_BCN(this;
              ,"PAGINA";
              ,"Note";
             )
    endwith
  endproc
  proc Calculate_CZGHMRBITS()
    with this
          * --- Cancellazione allegati collegati
          gsma_bae(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_DLGMYOICKZ()
    with this
          * --- Conferma modifica ai campi del codice RID
          this.CodiceRid(this;
             )
    endwith
  endproc
  proc Calculate_IRPKOAKLYU()
    with this
          * --- Inizializza codice RID
          .w_NOTIIDRI = IIF (NVL(.w_NOTIIDRI,0)=0 ,1,.w_NOTIIDRI)
    endwith
  endproc
  proc Calculate_SIGJGJWAYX()
    with this
          * --- Memorizza codici i vecchi codicei RID
          .w_OLDANCOIDRI = .w_NOCOIDRI
          .w_OLDANTIIDRI = .w_NOTIIDRI
    endwith
  endproc
  proc Calculate_GYGSSTAZGU()
    with this
          * --- Azzerro dati al cambio 1 mese escluso
          .w_NOGIOSC1 = 0
          .w_NO2MESCL = 0
    endwith
  endproc
  proc Calculate_PGRKRUGTXB()
    with this
          * --- Azzerro dati al cambio2 mese escluso
          .w_NOGIOSC2 = 0
    endwith
  endproc
  proc Calculate_KHHVUGHDUO()
    with this
          * --- Enable bottone di stampa
          EnableBtnStampa(this;
             )
    endwith
  endproc
  proc Calculate_HEZCDUNPFW()
    with this
          * --- Setta w_ATDATINI
          .w_ATDATINI = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATAINI),  DTOC(.w_DATAINI)+' '+.w_ORAINI+':'+.w_MININI+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_ZQLIBXAFGH()
    with this
          * --- Setta w_ATDATFIN1
          .w_ATDATFIN1 = cp_CharToDatetime( IIF(NOT EMPTY(.w_DATAFIN),  DTOC(.w_DATAFIN)+' '+.w_ORAFIN+':'+.w_MINFIN+':00', '  -  -       :  :  '))
    endwith
  endproc
  proc Calculate_VXJYEYMBXV()
    with this
          * --- Inizializza OLD_NOFLANTI
          .w_OLD_NOFLANTI = .w_NOFLANTI
    endwith
  endproc
  proc Calculate_EVFCJUGOFG()
    with this
          * --- Ricalcolo numero ducumento
          .w_NONUMDOC = iif(.w_NOTIPDOC='01'and !empty(.w_NONUMCAR),.w_NONUMCAR,space(15))
    endwith
  endproc
  proc Calculate_KQAWRAXMWG()
    with this
          * --- Controlli antiriciclaggio
          gsar_bdr(this;
              ,'A';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oNODESCRI_1_8.enabled = this.oPgFrm.Page1.oPag.oNODESCRI_1_8.mCond()
    this.oPgFrm.Page1.oPag.oNODESCR2_1_9.enabled = this.oPgFrm.Page1.oPag.oNODESCR2_1_9.mCond()
    this.oPgFrm.Page1.oPag.oNOSOGGET_1_10.enabled = this.oPgFrm.Page1.oPag.oNOSOGGET_1_10.mCond()
    this.oPgFrm.Page1.oPag.oNOCOGNOM_1_14.enabled = this.oPgFrm.Page1.oPag.oNOCOGNOM_1_14.mCond()
    this.oPgFrm.Page1.oPag.oNOCOGNOM_1_15.enabled = this.oPgFrm.Page1.oPag.oNOCOGNOM_1_15.mCond()
    this.oPgFrm.Page1.oPag.oNO__NOME_1_16.enabled = this.oPgFrm.Page1.oPag.oNO__NOME_1_16.mCond()
    this.oPgFrm.Page1.oPag.oNO__NOME_1_17.enabled = this.oPgFrm.Page1.oPag.oNO__NOME_1_17.mCond()
    this.oPgFrm.Page1.oPag.oNOLOCNAS_1_18.enabled = this.oPgFrm.Page1.oPag.oNOLOCNAS_1_18.mCond()
    this.oPgFrm.Page1.oPag.oNOLOCNAS_1_19.enabled = this.oPgFrm.Page1.oPag.oNOLOCNAS_1_19.mCond()
    this.oPgFrm.Page1.oPag.oNOPRONAS_1_20.enabled = this.oPgFrm.Page1.oPag.oNOPRONAS_1_20.mCond()
    this.oPgFrm.Page1.oPag.oNOPRONAS_1_21.enabled = this.oPgFrm.Page1.oPag.oNOPRONAS_1_21.mCond()
    this.oPgFrm.Page1.oPag.oNODATNAS_1_22.enabled = this.oPgFrm.Page1.oPag.oNODATNAS_1_22.mCond()
    this.oPgFrm.Page1.oPag.oNO_SESSO_1_23.enabled = this.oPgFrm.Page1.oPag.oNO_SESSO_1_23.mCond()
    this.oPgFrm.Page1.oPag.oNONAZNAS_1_24.enabled = this.oPgFrm.Page1.oPag.oNONAZNAS_1_24.mCond()
    this.oPgFrm.Page1.oPag.oNONUMCAR_1_25.enabled = this.oPgFrm.Page1.oPag.oNONUMCAR_1_25.mCond()
    this.oPgFrm.Page1.oPag.oNOCODFIS_1_26.enabled = this.oPgFrm.Page1.oPag.oNOCODFIS_1_26.mCond()
    this.oPgFrm.Page1.oPag.oNOPARIVA_1_27.enabled = this.oPgFrm.Page1.oPag.oNOPARIVA_1_27.mCond()
    this.oPgFrm.Page1.oPag.oNOCODSAL_1_29.enabled = this.oPgFrm.Page1.oPag.oNOCODSAL_1_29.mCond()
    this.oPgFrm.Page1.oPag.oNOINDIRI_1_30.enabled = this.oPgFrm.Page1.oPag.oNOINDIRI_1_30.mCond()
    this.oPgFrm.Page1.oPag.oNOINDI_2_1_31.enabled = this.oPgFrm.Page1.oPag.oNOINDI_2_1_31.mCond()
    this.oPgFrm.Page1.oPag.oNO___CAP_1_33.enabled = this.oPgFrm.Page1.oPag.oNO___CAP_1_33.mCond()
    this.oPgFrm.Page1.oPag.oNO___CAP_1_34.enabled = this.oPgFrm.Page1.oPag.oNO___CAP_1_34.mCond()
    this.oPgFrm.Page1.oPag.oNOLOCALI_1_35.enabled = this.oPgFrm.Page1.oPag.oNOLOCALI_1_35.mCond()
    this.oPgFrm.Page1.oPag.oNOLOCALI_1_36.enabled = this.oPgFrm.Page1.oPag.oNOLOCALI_1_36.mCond()
    this.oPgFrm.Page1.oPag.oNOPROVIN_1_37.enabled = this.oPgFrm.Page1.oPag.oNOPROVIN_1_37.mCond()
    this.oPgFrm.Page1.oPag.oNOPROVIN_1_38.enabled = this.oPgFrm.Page1.oPag.oNOPROVIN_1_38.mCond()
    this.oPgFrm.Page1.oPag.oNONAZION_1_39.enabled = this.oPgFrm.Page1.oPag.oNONAZION_1_39.mCond()
    this.oPgFrm.Page1.oPag.oNOTELEFO_1_51.enabled = this.oPgFrm.Page1.oPag.oNOTELEFO_1_51.mCond()
    this.oPgFrm.Page1.oPag.oNONUMCEL_1_52.enabled = this.oPgFrm.Page1.oPag.oNONUMCEL_1_52.mCond()
    this.oPgFrm.Page1.oPag.oNO_SKYPE_1_53.enabled = this.oPgFrm.Page1.oPag.oNO_SKYPE_1_53.mCond()
    this.oPgFrm.Page1.oPag.oNOTELFAX_1_54.enabled = this.oPgFrm.Page1.oPag.oNOTELFAX_1_54.mCond()
    this.oPgFrm.Page1.oPag.oNO_EMAIL_1_55.enabled = this.oPgFrm.Page1.oPag.oNO_EMAIL_1_55.mCond()
    this.oPgFrm.Page1.oPag.oNOINDWEB_1_56.enabled = this.oPgFrm.Page1.oPag.oNOINDWEB_1_56.mCond()
    this.oPgFrm.Page1.oPag.oNO_EMPEC_1_57.enabled = this.oPgFrm.Page1.oPag.oNO_EMPEC_1_57.mCond()
    this.oPgFrm.Page1.oPag.oNOCHKSTA_1_58.enabled = this.oPgFrm.Page1.oPag.oNOCHKSTA_1_58.mCond()
    this.oPgFrm.Page1.oPag.oNOCHKMAI_1_59.enabled = this.oPgFrm.Page1.oPag.oNOCHKMAI_1_59.mCond()
    this.oPgFrm.Page1.oPag.oNOCHKPEC_1_60.enabled = this.oPgFrm.Page1.oPag.oNOCHKPEC_1_60.mCond()
    this.oPgFrm.Page1.oPag.oNOCHKFAX_1_61.enabled = this.oPgFrm.Page1.oPag.oNOCHKFAX_1_61.mCond()
    this.oPgFrm.Page1.oPag.oNOCHKCPZ_1_62.enabled = this.oPgFrm.Page1.oPag.oNOCHKCPZ_1_62.mCond()
    this.oPgFrm.Page1.oPag.oNOSOGGET_1_69.enabled = this.oPgFrm.Page1.oPag.oNOSOGGET_1_69.mCond()
    this.oPgFrm.Page1.oPag.oNOTIPPER_1_72.enabled = this.oPgFrm.Page1.oPag.oNOTIPPER_1_72.mCond()
    this.oPgFrm.Page1.oPag.oNOTIPNOM_1_74.enabled = this.oPgFrm.Page1.oPag.oNOTIPNOM_1_74.mCond()
    this.oPgFrm.Page1.oPag.oNOTIPNOM_1_75.enabled = this.oPgFrm.Page1.oPag.oNOTIPNOM_1_75.mCond()
    this.oPgFrm.Page1.oPag.oNOTIPNOM_1_76.enabled = this.oPgFrm.Page1.oPag.oNOTIPNOM_1_76.mCond()
    this.oPgFrm.Page2.oPag.oNOGIOSC1_2_9.enabled = this.oPgFrm.Page2.oPag.oNOGIOSC1_2_9.mCond()
    this.oPgFrm.Page2.oPag.oNO2MESCL_2_10.enabled = this.oPgFrm.Page2.oPag.oNO2MESCL_2_10.mCond()
    this.oPgFrm.Page2.oPag.oNOGIOSC2_2_11.enabled = this.oPgFrm.Page2.oPag.oNOGIOSC2_2_11.mCond()
    this.oPgFrm.Page2.oPag.oBANCAAHE_2_43.enabled = this.oPgFrm.Page2.oPag.oBANCAAHE_2_43.mCond()
    this.oPgFrm.Page2.oPag.oNOTIIDRI_2_47.enabled = this.oPgFrm.Page2.oPag.oNOTIIDRI_2_47.mCond()
    this.oPgFrm.Page2.oPag.oNOCOIDRI_2_48.enabled = this.oPgFrm.Page2.oPag.oNOCOIDRI_2_48.mCond()
    this.oPgFrm.Page3.oPag.oNONOMFIL_3_1.enabled = this.oPgFrm.Page3.oPag.oNONOMFIL_3_1.mCond()
    this.oPgFrm.Page3.oPag.oNOOGGE_3_2.enabled = this.oPgFrm.Page3.oPag.oNOOGGE_3_2.mCond()
    this.oPgFrm.Page3.oPag.oNONOTE_3_3.enabled = this.oPgFrm.Page3.oPag.oNONOTE_3_3.mCond()
    this.oPgFrm.Page4.oPag.oNOASSOPE_4_15.enabled = this.oPgFrm.Page4.oPag.oNOASSOPE_4_15.mCond()
    this.oPgFrm.Page4.oPag.oNOCODZON_4_17.enabled = this.oPgFrm.Page4.oPag.oNOCODZON_4_17.mCond()
    this.oPgFrm.Page4.oPag.oNOCODLIN_4_20.enabled = this.oPgFrm.Page4.oPag.oNOCODLIN_4_20.mCond()
    this.oPgFrm.Page4.oPag.oNOCODVAL_4_23.enabled = this.oPgFrm.Page4.oPag.oNOCODVAL_4_23.mCond()
    this.oPgFrm.Page4.oPag.oNOCODAGE_4_24.enabled = this.oPgFrm.Page4.oPag.oNOCODAGE_4_24.mCond()
    this.oPgFrm.Page4.oPag.oNONUMLIS_4_44.enabled = this.oPgFrm.Page4.oPag.oNONUMLIS_4_44.mCond()
    this.oPgFrm.Page4.oPag.oNONUMLIS_4_45.enabled = this.oPgFrm.Page4.oPag.oNONUMLIS_4_45.mCond()
    this.oPgFrm.Page4.oPag.oNOCODPAG_4_46.enabled = this.oPgFrm.Page4.oPag.oNOCODPAG_4_46.mCond()
    this.oPgFrm.Page4.oPag.oNOCODBAN_4_47.enabled = this.oPgFrm.Page4.oPag.oNOCODBAN_4_47.mCond()
    this.oPgFrm.Page4.oPag.oNOCATCOM_4_48.enabled = this.oPgFrm.Page4.oPag.oNOCATCOM_4_48.mCond()
    this.oPgFrm.Page4.oPag.oBANCAAHR_4_51.enabled = this.oPgFrm.Page4.oPag.oBANCAAHR_4_51.mCond()
    this.oPgFrm.Page4.oPag.oBANCAAHE_4_52.enabled = this.oPgFrm.Page4.oPag.oBANCAAHE_4_52.mCond()
    this.oPgFrm.Page6.oPag.oOFSTATUS_6_1.enabled = this.oPgFrm.Page6.oPag.oOFSTATUS_6_1.mCond()
    this.oPgFrm.Page6.oPag.oNFDTOF1_6_9.enabled = this.oPgFrm.Page6.oPag.oNFDTOF1_6_9.mCond()
    this.oPgFrm.Page6.oPag.oNFDTOF2_6_10.enabled = this.oPgFrm.Page6.oPag.oNFDTOF2_6_10.mCond()
    this.oPgFrm.Page6.oPag.oNFDTRC1_6_11.enabled = this.oPgFrm.Page6.oPag.oNFDTRC1_6_11.mCond()
    this.oPgFrm.Page6.oPag.oNFDTRC2_6_12.enabled = this.oPgFrm.Page6.oPag.oNFDTRC2_6_12.mCond()
    this.oPgFrm.Page6.oPag.oNFGRPRIO_6_17.enabled = this.oPgFrm.Page6.oPag.oNFGRPRIO_6_17.mCond()
    this.oPgFrm.Page6.oPag.oNFRIFDE_6_18.enabled = this.oPgFrm.Page6.oPag.oNFRIFDE_6_18.mCond()
    this.oPgFrm.Page7.oPag.oOPERAT_7_2.enabled = this.oPgFrm.Page7.oPag.oOPERAT_7_2.mCond()
    this.oPgFrm.Page7.oPag.oSTATO_7_3.enabled = this.oPgFrm.Page7.oPag.oSTATO_7_3.mCond()
    this.oPgFrm.Page7.oPag.oDATAINI_7_7.enabled = this.oPgFrm.Page7.oPag.oDATAINI_7_7.mCond()
    this.oPgFrm.Page7.oPag.oORAINI_7_8.enabled = this.oPgFrm.Page7.oPag.oORAINI_7_8.mCond()
    this.oPgFrm.Page7.oPag.oMININI_7_9.enabled = this.oPgFrm.Page7.oPag.oMININI_7_9.mCond()
    this.oPgFrm.Page7.oPag.oDATAFIN_7_10.enabled = this.oPgFrm.Page7.oPag.oDATAFIN_7_10.mCond()
    this.oPgFrm.Page7.oPag.oORAFIN_7_11.enabled = this.oPgFrm.Page7.oPag.oORAFIN_7_11.mCond()
    this.oPgFrm.Page7.oPag.oMINFIN_7_12.enabled = this.oPgFrm.Page7.oPag.oMINFIN_7_12.mCond()
    this.oPgFrm.Page7.oPag.oSUBJECT_7_14.enabled = this.oPgFrm.Page7.oPag.oSUBJECT_7_14.mCond()
    this.oPgFrm.Page7.oPag.oPRIOINI_7_16.enabled = this.oPgFrm.Page7.oPag.oPRIOINI_7_16.mCond()
    this.oPgFrm.Page7.oPag.oPRIOINI_7_17.enabled = this.oPgFrm.Page7.oPag.oPRIOINI_7_17.mCond()
    this.oPgFrm.Page8.oPag.oNODATINS_8_1.enabled = this.oPgFrm.Page8.oPag.oNODATINS_8_1.mCond()
    this.oPgFrm.Page8.oPag.oNO__RESP_8_2.enabled = this.oPgFrm.Page8.oPag.oNO__RESP_8_2.mCond()
    this.oPgFrm.Page8.oPag.oNOTIPOID_8_5.enabled = this.oPgFrm.Page8.oPag.oNOTIPOID_8_5.mCond()
    this.oPgFrm.Page8.oPag.oNOTIPDOC_8_6.enabled = this.oPgFrm.Page8.oPag.oNOTIPDOC_8_6.mCond()
    this.oPgFrm.Page8.oPag.oNONUMDOC_8_7.enabled = this.oPgFrm.Page8.oPag.oNONUMDOC_8_7.mCond()
    this.oPgFrm.Page8.oPag.oNODATRIL_8_8.enabled = this.oPgFrm.Page8.oPag.oNODATRIL_8_8.mCond()
    this.oPgFrm.Page8.oPag.oNOAUTLOC_8_9.enabled = this.oPgFrm.Page8.oPag.oNOAUTLOC_8_9.mCond()
    this.oPgFrm.Page8.oPag.oNOATTLAV_8_10.enabled = this.oPgFrm.Page8.oPag.oNOATTLAV_8_10.mCond()
    this.oPgFrm.Page8.oPag.oNONAGIUR_8_11.enabled = this.oPgFrm.Page8.oPag.oNONAGIUR_8_11.mCond()
    this.oPgFrm.Page8.oPag.oNODATCOS_8_13.enabled = this.oPgFrm.Page8.oPag.oNODATCOS_8_13.mCond()
    this.oPgFrm.Page8.oPag.oNO_SIGLA_8_14.enabled = this.oPgFrm.Page8.oPag.oNO_SIGLA_8_14.mCond()
    this.oPgFrm.Page8.oPag.oNO_PAESE_8_15.enabled = this.oPgFrm.Page8.oPag.oNO_PAESE_8_15.mCond()
    this.oPgFrm.Page8.oPag.oNOESTNAS_8_16.enabled = this.oPgFrm.Page8.oPag.oNOESTNAS_8_16.mCond()
    this.oPgFrm.Page8.oPag.oNOFLANTI_8_20.enabled = this.oPgFrm.Page8.oPag.oNOFLANTI_8_20.mCond()
    this.oPgFrm.Page9.oPag.oNO__NOTE_9_1.enabled = this.oPgFrm.Page9.oPag.oNO__NOTE_9_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_130.enabled = this.oPgFrm.Page1.oPag.oBtn_1_130.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_132.enabled = this.oPgFrm.Page1.oPag.oBtn_1_132.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_133.enabled = this.oPgFrm.Page1.oPag.oBtn_1_133.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_134.enabled = this.oPgFrm.Page1.oPag.oBtn_1_134.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_135.enabled = this.oPgFrm.Page1.oPag.oBtn_1_135.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_136.enabled = this.oPgFrm.Page1.oPag.oBtn_1_136.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_137.enabled = this.oPgFrm.Page1.oPag.oBtn_1_137.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_138.enabled = this.oPgFrm.Page1.oPag.oBtn_1_138.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_139.enabled = this.oPgFrm.Page1.oPag.oBtn_1_139.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_140.enabled = this.oPgFrm.Page1.oPag.oBtn_1_140.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_141.enabled = this.oPgFrm.Page1.oPag.oBtn_1_141.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_163.enabled = this.oPgFrm.Page1.oPag.oBtn_1_163.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_168.enabled = this.oPgFrm.Page1.oPag.oBtn_1_168.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_169.enabled = this.oPgFrm.Page1.oPag.oBtn_1_169.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_170.enabled = this.oPgFrm.Page1.oPag.oBtn_1_170.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_5.enabled = this.oPgFrm.Page3.oPag.oBtn_3_5.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_6.enabled = this.oPgFrm.Page3.oPag.oBtn_3_6.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_7.enabled = this.oPgFrm.Page3.oPag.oBtn_3_7.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_9.enabled = this.oPgFrm.Page3.oPag.oBtn_3_9.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_13.enabled = this.oPgFrm.Page3.oPag.oBtn_3_13.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_4.enabled = this.oPgFrm.Page4.oPag.oBtn_4_4.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_2.enabled = this.oPgFrm.Page6.oPag.oBtn_6_2.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_3.enabled = this.oPgFrm.Page6.oPag.oBtn_6_3.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_6.enabled = this.oPgFrm.Page6.oPag.oBtn_6_6.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_7.enabled = this.oPgFrm.Page6.oPag.oBtn_6_7.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_8.enabled = this.oPgFrm.Page6.oPag.oBtn_6_8.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_20.enabled = this.oPgFrm.Page6.oPag.oBtn_6_20.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_40.enabled = this.oPgFrm.Page7.oPag.oBtn_7_40.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_42.enabled = this.oPgFrm.Page7.oPag.oBtn_7_42.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_43.enabled = this.oPgFrm.Page7.oPag.oBtn_7_43.mCond()
    this.oPgFrm.Page7.oPag.oBtn_7_45.enabled = this.oPgFrm.Page7.oPag.oBtn_7_45.mCond()
    this.oPgFrm.Page8.oPag.oBtn_8_42.enabled = this.oPgFrm.Page8.oPag.oBtn_8_42.mCond()
    this.oPgFrm.Page9.oPag.oBtn_9_2.enabled = this.oPgFrm.Page9.oPag.oBtn_9_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_180.enabled = this.oPgFrm.Page1.oPag.oBtn_1_180.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(3).enabled=not(!Empty(this.w_NOFLGBEN) or g_OFFE<>'S')
    this.oPgFrm.Pages(4).enabled=not(g_OFFE<>'S' and Not IsAlt() and Empty(this.w_NOFLGBEN) and g_AGEN <>'S')
    this.oPgFrm.Pages(5).enabled=not(!Empty(this.w_NOFLGBEN) )
    this.oPgFrm.Pages(6).enabled=not(!Empty(this.w_NOFLGBEN) or g_OFFE<>'S')
    this.oPgFrm.Pages(7).enabled=not(!Empty(this.w_NOFLGBEN) or g_OFFE<>'S')
    this.oPgFrm.Pages(8).enabled=not(g_ANTI<>'S')
    local i_show1
    i_show1=not(this.w_NOFLGBEN<>'B')
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Dati pagamenti"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    local i_show2
    i_show2=not(IsAlt() or !Empty(this.w_NOFLGBEN))
    this.oPgFrm.Pages(3).enabled=i_show2 and not(!Empty(this.w_NOFLGBEN) or g_OFFE<>'S')
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Allegati"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    local i_show4
    i_show4=not(!Empty(this.w_NOFLGBEN) )
    this.oPgFrm.Pages(5).enabled=i_show4 and not(!Empty(this.w_NOFLGBEN) )
    this.oPgFrm.Pages(5).caption=iif(i_show4,cp_translate("Persone"),"")
    this.oPgFrm.Pages(5).oPag.visible=this.oPgFrm.Pages(5).enabled
    local i_show5
    i_show5=not(IsAlt() or !Empty(this.w_NOFLGBEN))
    this.oPgFrm.Pages(6).enabled=i_show5 and not(!Empty(this.w_NOFLGBEN) or g_OFFE<>'S')
    this.oPgFrm.Pages(6).caption=iif(i_show5,cp_translate("Offerte"),"")
    this.oPgFrm.Pages(6).oPag.visible=this.oPgFrm.Pages(6).enabled
    local i_show6
    i_show6=not(IsAlt() or !Empty(this.w_NOFLGBEN))
    this.oPgFrm.Pages(7).enabled=i_show6 and not(!Empty(this.w_NOFLGBEN) or g_OFFE<>'S')
    this.oPgFrm.Pages(7).caption=iif(i_show6,cp_translate("Agenda attivit�"),"")
    this.oPgFrm.Pages(7).oPag.visible=this.oPgFrm.Pages(7).enabled
    local i_show7
    i_show7=not(g_ANTI<>'S')
    this.oPgFrm.Pages(8).enabled=i_show7 and not(g_ANTI<>'S')
    this.oPgFrm.Pages(8).caption=iif(i_show7,cp_translate("Antiriciclaggio"),"")
    this.oPgFrm.Pages(8).oPag.visible=this.oPgFrm.Pages(8).enabled
    local i_show9
    i_show9=not(this.w_NOFLGBEN<>'B' Or g_ISDF<>'S')
    this.oPgFrm.Pages(10).enabled=i_show9
    this.oPgFrm.Pages(10).caption=iif(i_show9,cp_translate("Dati DocFinance"),"")
    this.oPgFrm.Pages(10).oPag.visible=this.oPgFrm.Pages(10).enabled
    this.oPgFrm.Page1.oPag.oNOCODCLI_1_4.visible=!this.oPgFrm.Page1.oPag.oNOCODCLI_1_4.mHide()
    this.oPgFrm.Page1.oPag.oNOSOGGET_1_10.visible=!this.oPgFrm.Page1.oPag.oNOSOGGET_1_10.mHide()
    this.oPgFrm.Page1.oPag.oNOCOD_RU_1_12.visible=!this.oPgFrm.Page1.oPag.oNOCOD_RU_1_12.mHide()
    this.oPgFrm.Page1.oPag.oNOBADGE_1_13.visible=!this.oPgFrm.Page1.oPag.oNOBADGE_1_13.mHide()
    this.oPgFrm.Page1.oPag.oNOCOGNOM_1_14.visible=!this.oPgFrm.Page1.oPag.oNOCOGNOM_1_14.mHide()
    this.oPgFrm.Page1.oPag.oNOCOGNOM_1_15.visible=!this.oPgFrm.Page1.oPag.oNOCOGNOM_1_15.mHide()
    this.oPgFrm.Page1.oPag.oNO__NOME_1_16.visible=!this.oPgFrm.Page1.oPag.oNO__NOME_1_16.mHide()
    this.oPgFrm.Page1.oPag.oNO__NOME_1_17.visible=!this.oPgFrm.Page1.oPag.oNO__NOME_1_17.mHide()
    this.oPgFrm.Page1.oPag.oNOLOCNAS_1_18.visible=!this.oPgFrm.Page1.oPag.oNOLOCNAS_1_18.mHide()
    this.oPgFrm.Page1.oPag.oNOLOCNAS_1_19.visible=!this.oPgFrm.Page1.oPag.oNOLOCNAS_1_19.mHide()
    this.oPgFrm.Page1.oPag.oNOPRONAS_1_20.visible=!this.oPgFrm.Page1.oPag.oNOPRONAS_1_20.mHide()
    this.oPgFrm.Page1.oPag.oNOPRONAS_1_21.visible=!this.oPgFrm.Page1.oPag.oNOPRONAS_1_21.mHide()
    this.oPgFrm.Page1.oPag.oNONATGIU_1_28.visible=!this.oPgFrm.Page1.oPag.oNONATGIU_1_28.mHide()
    this.oPgFrm.Page1.oPag.oNO___CAP_1_33.visible=!this.oPgFrm.Page1.oPag.oNO___CAP_1_33.mHide()
    this.oPgFrm.Page1.oPag.oNO___CAP_1_34.visible=!this.oPgFrm.Page1.oPag.oNO___CAP_1_34.mHide()
    this.oPgFrm.Page1.oPag.oNOLOCALI_1_35.visible=!this.oPgFrm.Page1.oPag.oNOLOCALI_1_35.mHide()
    this.oPgFrm.Page1.oPag.oNOLOCALI_1_36.visible=!this.oPgFrm.Page1.oPag.oNOLOCALI_1_36.mHide()
    this.oPgFrm.Page1.oPag.oNOPROVIN_1_37.visible=!this.oPgFrm.Page1.oPag.oNOPROVIN_1_37.mHide()
    this.oPgFrm.Page1.oPag.oNOPROVIN_1_38.visible=!this.oPgFrm.Page1.oPag.oNOPROVIN_1_38.mHide()
    this.oPgFrm.Page1.oPag.oNO___CAD_1_41.visible=!this.oPgFrm.Page1.oPag.oNO___CAD_1_41.mHide()
    this.oPgFrm.Page1.oPag.oNO___CAD_1_42.visible=!this.oPgFrm.Page1.oPag.oNO___CAD_1_42.mHide()
    this.oPgFrm.Page1.oPag.oNOLOCALD_1_43.visible=!this.oPgFrm.Page1.oPag.oNOLOCALD_1_43.mHide()
    this.oPgFrm.Page1.oPag.oNOLOCALD_1_47.visible=!this.oPgFrm.Page1.oPag.oNOLOCALD_1_47.mHide()
    this.oPgFrm.Page1.oPag.oNOPROVID_1_48.visible=!this.oPgFrm.Page1.oPag.oNOPROVID_1_48.mHide()
    this.oPgFrm.Page1.oPag.oNOPROVID_1_49.visible=!this.oPgFrm.Page1.oPag.oNOPROVID_1_49.mHide()
    this.oPgFrm.Page1.oPag.oNOCHKSTA_1_58.visible=!this.oPgFrm.Page1.oPag.oNOCHKSTA_1_58.mHide()
    this.oPgFrm.Page1.oPag.oNOCHKMAI_1_59.visible=!this.oPgFrm.Page1.oPag.oNOCHKMAI_1_59.mHide()
    this.oPgFrm.Page1.oPag.oNOCHKPEC_1_60.visible=!this.oPgFrm.Page1.oPag.oNOCHKPEC_1_60.mHide()
    this.oPgFrm.Page1.oPag.oNOCHKFAX_1_61.visible=!this.oPgFrm.Page1.oPag.oNOCHKFAX_1_61.mHide()
    this.oPgFrm.Page1.oPag.oNOCHKCPZ_1_62.visible=!this.oPgFrm.Page1.oPag.oNOCHKCPZ_1_62.mHide()
    this.oPgFrm.Page1.oPag.oNODTINVA_1_66.visible=!this.oPgFrm.Page1.oPag.oNODTINVA_1_66.mHide()
    this.oPgFrm.Page1.oPag.oNOSOGGET_1_69.visible=!this.oPgFrm.Page1.oPag.oNOSOGGET_1_69.mHide()
    this.oPgFrm.Page1.oPag.oNOTIPPER_1_72.visible=!this.oPgFrm.Page1.oPag.oNOTIPPER_1_72.mHide()
    this.oPgFrm.Page1.oPag.oNOTIPNOM_1_74.visible=!this.oPgFrm.Page1.oPag.oNOTIPNOM_1_74.mHide()
    this.oPgFrm.Page1.oPag.oNOTIPNOM_1_75.visible=!this.oPgFrm.Page1.oPag.oNOTIPNOM_1_75.mHide()
    this.oPgFrm.Page1.oPag.oNOTIPNOM_1_76.visible=!this.oPgFrm.Page1.oPag.oNOTIPNOM_1_76.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_94.visible=!this.oPgFrm.Page1.oPag.oStr_1_94.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_101.visible=!this.oPgFrm.Page1.oPag.oStr_1_101.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_117.visible=!this.oPgFrm.Page1.oPag.oStr_1_117.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_118.visible=!this.oPgFrm.Page1.oPag.oStr_1_118.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_119.visible=!this.oPgFrm.Page1.oPag.oStr_1_119.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_120.visible=!this.oPgFrm.Page1.oPag.oStr_1_120.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_122.visible=!this.oPgFrm.Page1.oPag.oStr_1_122.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_124.visible=!this.oPgFrm.Page1.oPag.oStr_1_124.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_130.visible=!this.oPgFrm.Page1.oPag.oBtn_1_130.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_132.visible=!this.oPgFrm.Page1.oPag.oBtn_1_132.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_133.visible=!this.oPgFrm.Page1.oPag.oBtn_1_133.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_134.visible=!this.oPgFrm.Page1.oPag.oBtn_1_134.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_135.visible=!this.oPgFrm.Page1.oPag.oBtn_1_135.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_136.visible=!this.oPgFrm.Page1.oPag.oBtn_1_136.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_137.visible=!this.oPgFrm.Page1.oPag.oBtn_1_137.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_138.visible=!this.oPgFrm.Page1.oPag.oBtn_1_138.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_139.visible=!this.oPgFrm.Page1.oPag.oBtn_1_139.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_140.visible=!this.oPgFrm.Page1.oPag.oBtn_1_140.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_141.visible=!this.oPgFrm.Page1.oPag.oBtn_1_141.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_144.visible=!this.oPgFrm.Page1.oPag.oStr_1_144.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_163.visible=!this.oPgFrm.Page1.oPag.oBtn_1_163.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_168.visible=!this.oPgFrm.Page1.oPag.oBtn_1_168.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_169.visible=!this.oPgFrm.Page1.oPag.oBtn_1_169.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_170.visible=!this.oPgFrm.Page1.oPag.oBtn_1_170.mHide()
    this.oPgFrm.Page2.oPag.oNOTIPCON_2_2.visible=!this.oPgFrm.Page2.oPag.oNOTIPCON_2_2.mHide()
    this.oPgFrm.Page2.oPag.oNOCODCON_2_3.visible=!this.oPgFrm.Page2.oPag.oNOCODCON_2_3.mHide()
    this.oPgFrm.Page2.oPag.oANDESCRI_2_36.visible=!this.oPgFrm.Page2.oPag.oANDESCRI_2_36.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_37.visible=!this.oPgFrm.Page2.oPag.oStr_2_37.mHide()
    this.oPgFrm.Page2.oPag.oNOTIPPER_2_38.visible=!this.oPgFrm.Page2.oPag.oNOTIPPER_2_38.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_39.visible=!this.oPgFrm.Page2.oPag.oStr_2_39.mHide()
    this.oPgFrm.Page2.oPag.oNOCOGNOM_2_59.visible=!this.oPgFrm.Page2.oPag.oNOCOGNOM_2_59.mHide()
    this.oPgFrm.Page2.oPag.oNO__NOME_2_60.visible=!this.oPgFrm.Page2.oPag.oNO__NOME_2_60.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_61.visible=!this.oPgFrm.Page2.oPag.oStr_2_61.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_62.visible=!this.oPgFrm.Page2.oPag.oStr_2_62.mHide()
    this.oPgFrm.Page2.oPag.oNODESCRI_2_63.visible=!this.oPgFrm.Page2.oPag.oNODESCRI_2_63.mHide()
    this.oPgFrm.Page2.oPag.oNODESCR2_2_64.visible=!this.oPgFrm.Page2.oPag.oNODESCR2_2_64.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_65.visible=!this.oPgFrm.Page2.oPag.oStr_2_65.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_5.visible=!this.oPgFrm.Page3.oPag.oBtn_3_5.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_6.visible=!this.oPgFrm.Page3.oPag.oBtn_3_6.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_7.visible=!this.oPgFrm.Page3.oPag.oBtn_3_7.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_9.visible=!this.oPgFrm.Page3.oPag.oBtn_3_9.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_13.visible=!this.oPgFrm.Page3.oPag.oBtn_3_13.mHide()
    this.oPgFrm.Page3.oPag.oNOTIPPER_3_18.visible=!this.oPgFrm.Page3.oPag.oNOTIPPER_3_18.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_19.visible=!this.oPgFrm.Page3.oPag.oStr_3_19.mHide()
    this.oPgFrm.Page3.oPag.oNOCOGNOM_3_20.visible=!this.oPgFrm.Page3.oPag.oNOCOGNOM_3_20.mHide()
    this.oPgFrm.Page3.oPag.oNO__NOME_3_21.visible=!this.oPgFrm.Page3.oPag.oNO__NOME_3_21.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_22.visible=!this.oPgFrm.Page3.oPag.oStr_3_22.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_23.visible=!this.oPgFrm.Page3.oPag.oStr_3_23.mHide()
    this.oPgFrm.Page3.oPag.oNODESCRI_3_24.visible=!this.oPgFrm.Page3.oPag.oNODESCRI_3_24.mHide()
    this.oPgFrm.Page3.oPag.oNODESCR2_3_25.visible=!this.oPgFrm.Page3.oPag.oNODESCR2_3_25.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_26.visible=!this.oPgFrm.Page3.oPag.oStr_3_26.mHide()
    this.oPgFrm.Page4.oPag.oNOCODPRI_4_2.visible=!this.oPgFrm.Page4.oPag.oNOCODPRI_4_2.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_4.visible=!this.oPgFrm.Page4.oPag.oBtn_4_4.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_5.visible=!this.oPgFrm.Page4.oPag.oStr_4_5.mHide()
    this.oPgFrm.Page4.oPag.oNOCODGRU_4_6.visible=!this.oPgFrm.Page4.oPag.oNOCODGRU_4_6.mHide()
    this.oPgFrm.Page4.oPag.oNODESGRU_4_7.visible=!this.oPgFrm.Page4.oPag.oNODESGRU_4_7.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_8.visible=!this.oPgFrm.Page4.oPag.oStr_4_8.mHide()
    this.oPgFrm.Page4.oPag.oNOCODORI_4_9.visible=!this.oPgFrm.Page4.oPag.oNOCODORI_4_9.mHide()
    this.oPgFrm.Page4.oPag.oNODESORI_4_10.visible=!this.oPgFrm.Page4.oPag.oNODESORI_4_10.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_11.visible=!this.oPgFrm.Page4.oPag.oStr_4_11.mHide()
    this.oPgFrm.Page4.oPag.oNORIFINT_4_12.visible=!this.oPgFrm.Page4.oPag.oNORIFINT_4_12.mHide()
    this.oPgFrm.Page4.oPag.oNOCODOPE_4_13.visible=!this.oPgFrm.Page4.oPag.oNOCODOPE_4_13.mHide()
    this.oPgFrm.Page4.oPag.oNODESOPE_4_14.visible=!this.oPgFrm.Page4.oPag.oNODESOPE_4_14.mHide()
    this.oPgFrm.Page4.oPag.oNOASSOPE_4_15.visible=!this.oPgFrm.Page4.oPag.oNOASSOPE_4_15.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_16.visible=!this.oPgFrm.Page4.oPag.oStr_4_16.mHide()
    this.oPgFrm.Page4.oPag.oNOCODZON_4_17.visible=!this.oPgFrm.Page4.oPag.oNOCODZON_4_17.mHide()
    this.oPgFrm.Page4.oPag.oNODESZON_4_18.visible=!this.oPgFrm.Page4.oPag.oNODESZON_4_18.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_19.visible=!this.oPgFrm.Page4.oPag.oStr_4_19.mHide()
    this.oPgFrm.Page4.oPag.oNOCODVAL_4_23.visible=!this.oPgFrm.Page4.oPag.oNOCODVAL_4_23.mHide()
    this.oPgFrm.Page4.oPag.oNOCODAGE_4_24.visible=!this.oPgFrm.Page4.oPag.oNOCODAGE_4_24.mHide()
    this.oPgFrm.Page4.oPag.oNODESVAL_4_25.visible=!this.oPgFrm.Page4.oPag.oNODESVAL_4_25.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_26.visible=!this.oPgFrm.Page4.oPag.oStr_4_26.mHide()
    this.oPgFrm.Page4.oPag.oLinkPC_4_28.visible=!this.oPgFrm.Page4.oPag.oLinkPC_4_28.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_32.visible=!this.oPgFrm.Page4.oPag.oStr_4_32.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_33.visible=!this.oPgFrm.Page4.oPag.oStr_4_33.mHide()
    this.oPgFrm.Page4.oPag.oNODESAGE_4_34.visible=!this.oPgFrm.Page4.oPag.oNODESAGE_4_34.mHide()
    this.oPgFrm.Page4.oPag.oNOTIPPER_4_35.visible=!this.oPgFrm.Page4.oPag.oNOTIPPER_4_35.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_36.visible=!this.oPgFrm.Page4.oPag.oStr_4_36.mHide()
    this.oPgFrm.Page4.oPag.oNOCOGNOM_4_37.visible=!this.oPgFrm.Page4.oPag.oNOCOGNOM_4_37.mHide()
    this.oPgFrm.Page4.oPag.oNO__NOME_4_38.visible=!this.oPgFrm.Page4.oPag.oNO__NOME_4_38.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_39.visible=!this.oPgFrm.Page4.oPag.oStr_4_39.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_40.visible=!this.oPgFrm.Page4.oPag.oStr_4_40.mHide()
    this.oPgFrm.Page4.oPag.oNODESCRI_4_41.visible=!this.oPgFrm.Page4.oPag.oNODESCRI_4_41.mHide()
    this.oPgFrm.Page4.oPag.oNODESCR2_4_42.visible=!this.oPgFrm.Page4.oPag.oNODESCR2_4_42.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_43.visible=!this.oPgFrm.Page4.oPag.oStr_4_43.mHide()
    this.oPgFrm.Page4.oPag.oNONUMLIS_4_44.visible=!this.oPgFrm.Page4.oPag.oNONUMLIS_4_44.mHide()
    this.oPgFrm.Page4.oPag.oNONUMLIS_4_45.visible=!this.oPgFrm.Page4.oPag.oNONUMLIS_4_45.mHide()
    this.oPgFrm.Page4.oPag.oNOCODPAG_4_46.visible=!this.oPgFrm.Page4.oPag.oNOCODPAG_4_46.mHide()
    this.oPgFrm.Page4.oPag.oNOCODBAN_4_47.visible=!this.oPgFrm.Page4.oPag.oNOCODBAN_4_47.mHide()
    this.oPgFrm.Page4.oPag.oNOCATCOM_4_48.visible=!this.oPgFrm.Page4.oPag.oNOCATCOM_4_48.mHide()
    this.oPgFrm.Page4.oPag.oNOCATCON_4_49.visible=!this.oPgFrm.Page4.oPag.oNOCATCON_4_49.mHide()
    this.oPgFrm.Page4.oPag.oNOCACLFO_4_50.visible=!this.oPgFrm.Page4.oPag.oNOCACLFO_4_50.mHide()
    this.oPgFrm.Page4.oPag.oBANCAAHR_4_51.visible=!this.oPgFrm.Page4.oPag.oBANCAAHR_4_51.mHide()
    this.oPgFrm.Page4.oPag.oBANCAAHE_4_52.visible=!this.oPgFrm.Page4.oPag.oBANCAAHE_4_52.mHide()
    this.oPgFrm.Page4.oPag.oDESBA3_4_53.visible=!this.oPgFrm.Page4.oPag.oDESBA3_4_53.mHide()
    this.oPgFrm.Page4.oPag.oDESBA2_4_54.visible=!this.oPgFrm.Page4.oPag.oDESBA2_4_54.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_55.visible=!this.oPgFrm.Page4.oPag.oStr_4_55.mHide()
    this.oPgFrm.Page4.oPag.oDESBAN_4_56.visible=!this.oPgFrm.Page4.oPag.oDESBAN_4_56.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_57.visible=!this.oPgFrm.Page4.oPag.oStr_4_57.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_58.visible=!this.oPgFrm.Page4.oPag.oStr_4_58.mHide()
    this.oPgFrm.Page4.oPag.oDESPAG_4_59.visible=!this.oPgFrm.Page4.oPag.oDESPAG_4_59.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_60.visible=!this.oPgFrm.Page4.oPag.oStr_4_60.mHide()
    this.oPgFrm.Page4.oPag.oDESCAC_4_61.visible=!this.oPgFrm.Page4.oPag.oDESCAC_4_61.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_62.visible=!this.oPgFrm.Page4.oPag.oStr_4_62.mHide()
    this.oPgFrm.Page4.oPag.oDESLIS_4_63.visible=!this.oPgFrm.Page4.oPag.oDESLIS_4_63.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_64.visible=!this.oPgFrm.Page4.oPag.oStr_4_64.mHide()
    this.oPgFrm.Page4.oPag.oNODENOMRIF_4_67.visible=!this.oPgFrm.Page4.oPag.oNODENOMRIF_4_67.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_68.visible=!this.oPgFrm.Page4.oPag.oStr_4_68.mHide()
    this.oPgFrm.Page4.oPag.oDESCON_4_69.visible=!this.oPgFrm.Page4.oPag.oDESCON_4_69.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_70.visible=!this.oPgFrm.Page4.oPag.oStr_4_70.mHide()
    this.oPgFrm.Page4.oPag.oDESSCATCLFO_4_71.visible=!this.oPgFrm.Page4.oPag.oDESSCATCLFO_4_71.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_72.visible=!this.oPgFrm.Page4.oPag.oStr_4_72.mHide()
    this.oPgFrm.Page5.oPag.oNOTIPPER_5_4.visible=!this.oPgFrm.Page5.oPag.oNOTIPPER_5_4.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_5.visible=!this.oPgFrm.Page5.oPag.oStr_5_5.mHide()
    this.oPgFrm.Page5.oPag.oNOCOGNOM_5_6.visible=!this.oPgFrm.Page5.oPag.oNOCOGNOM_5_6.mHide()
    this.oPgFrm.Page5.oPag.oNO__NOME_5_7.visible=!this.oPgFrm.Page5.oPag.oNO__NOME_5_7.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_8.visible=!this.oPgFrm.Page5.oPag.oStr_5_8.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_9.visible=!this.oPgFrm.Page5.oPag.oStr_5_9.mHide()
    this.oPgFrm.Page5.oPag.oNODESCRI_5_10.visible=!this.oPgFrm.Page5.oPag.oNODESCRI_5_10.mHide()
    this.oPgFrm.Page5.oPag.oNODESCR2_5_11.visible=!this.oPgFrm.Page5.oPag.oNODESCR2_5_11.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_12.visible=!this.oPgFrm.Page5.oPag.oStr_5_12.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_2.visible=!this.oPgFrm.Page6.oPag.oBtn_6_2.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_3.visible=!this.oPgFrm.Page6.oPag.oBtn_6_3.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_6.visible=!this.oPgFrm.Page6.oPag.oBtn_6_6.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_7.visible=!this.oPgFrm.Page6.oPag.oBtn_6_7.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_8.visible=!this.oPgFrm.Page6.oPag.oBtn_6_8.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_20.visible=!this.oPgFrm.Page6.oPag.oBtn_6_20.mHide()
    this.oPgFrm.Page6.oPag.oNOTIPPER_6_34.visible=!this.oPgFrm.Page6.oPag.oNOTIPPER_6_34.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_35.visible=!this.oPgFrm.Page6.oPag.oStr_6_35.mHide()
    this.oPgFrm.Page6.oPag.oNOCOGNOM_6_36.visible=!this.oPgFrm.Page6.oPag.oNOCOGNOM_6_36.mHide()
    this.oPgFrm.Page6.oPag.oNO__NOME_6_37.visible=!this.oPgFrm.Page6.oPag.oNO__NOME_6_37.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_38.visible=!this.oPgFrm.Page6.oPag.oStr_6_38.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_39.visible=!this.oPgFrm.Page6.oPag.oStr_6_39.mHide()
    this.oPgFrm.Page6.oPag.oNODESCRI_6_40.visible=!this.oPgFrm.Page6.oPag.oNODESCRI_6_40.mHide()
    this.oPgFrm.Page6.oPag.oNODESCR2_6_41.visible=!this.oPgFrm.Page6.oPag.oNODESCR2_6_41.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_42.visible=!this.oPgFrm.Page6.oPag.oStr_6_42.mHide()
    this.oPgFrm.Page7.oPag.oSTATO_7_3.visible=!this.oPgFrm.Page7.oPag.oSTATO_7_3.mHide()
    this.oPgFrm.Page7.oPag.oNAME_7_4.visible=!this.oPgFrm.Page7.oPag.oNAME_7_4.mHide()
    this.oPgFrm.Page7.oPag.oSTATO_7_6.visible=!this.oPgFrm.Page7.oPag.oSTATO_7_6.mHide()
    this.oPgFrm.Page7.oPag.oPRIOINI_7_16.visible=!this.oPgFrm.Page7.oPag.oPRIOINI_7_16.mHide()
    this.oPgFrm.Page7.oPag.oPRIOINI_7_17.visible=!this.oPgFrm.Page7.oPag.oPRIOINI_7_17.mHide()
    this.oPgFrm.Page7.oPag.oBtn_7_40.visible=!this.oPgFrm.Page7.oPag.oBtn_7_40.mHide()
    this.oPgFrm.Page7.oPag.oBtn_7_42.visible=!this.oPgFrm.Page7.oPag.oBtn_7_42.mHide()
    this.oPgFrm.Page7.oPag.oBtn_7_43.visible=!this.oPgFrm.Page7.oPag.oBtn_7_43.mHide()
    this.oPgFrm.Page7.oPag.oDESCPER_7_44.visible=!this.oPgFrm.Page7.oPag.oDESCPER_7_44.mHide()
    this.oPgFrm.Page7.oPag.oBtn_7_45.visible=!this.oPgFrm.Page7.oPag.oBtn_7_45.mHide()
    this.oPgFrm.Page8.oPag.oNOFLANTI_8_19.visible=!this.oPgFrm.Page8.oPag.oNOFLANTI_8_19.mHide()
    this.oPgFrm.Page8.oPag.oNOFLANTI_8_20.visible=!this.oPgFrm.Page8.oPag.oNOFLANTI_8_20.mHide()
    this.oPgFrm.Page8.oPag.oBtn_8_42.visible=!this.oPgFrm.Page8.oPag.oBtn_8_42.mHide()
    this.oPgFrm.Page8.oPag.oNOCOGNOM_8_48.visible=!this.oPgFrm.Page8.oPag.oNOCOGNOM_8_48.mHide()
    this.oPgFrm.Page8.oPag.oStr_8_49.visible=!this.oPgFrm.Page8.oPag.oStr_8_49.mHide()
    this.oPgFrm.Page8.oPag.oStr_8_50.visible=!this.oPgFrm.Page8.oPag.oStr_8_50.mHide()
    this.oPgFrm.Page8.oPag.oNODESCRI_8_51.visible=!this.oPgFrm.Page8.oPag.oNODESCRI_8_51.mHide()
    this.oPgFrm.Page8.oPag.oNO__NOME_8_52.visible=!this.oPgFrm.Page8.oPag.oNO__NOME_8_52.mHide()
    this.oPgFrm.Page8.oPag.oNODESCR2_8_53.visible=!this.oPgFrm.Page8.oPag.oNODESCR2_8_53.mHide()
    this.oPgFrm.Page8.oPag.oStr_8_54.visible=!this.oPgFrm.Page8.oPag.oStr_8_54.mHide()
    this.oPgFrm.Page9.oPag.oNOTIPPER_9_5.visible=!this.oPgFrm.Page9.oPag.oNOTIPPER_9_5.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_6.visible=!this.oPgFrm.Page9.oPag.oStr_9_6.mHide()
    this.oPgFrm.Page9.oPag.oNOCOGNOM_9_7.visible=!this.oPgFrm.Page9.oPag.oNOCOGNOM_9_7.mHide()
    this.oPgFrm.Page9.oPag.oNO__NOME_9_8.visible=!this.oPgFrm.Page9.oPag.oNO__NOME_9_8.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_9.visible=!this.oPgFrm.Page9.oPag.oStr_9_9.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_10.visible=!this.oPgFrm.Page9.oPag.oStr_9_10.mHide()
    this.oPgFrm.Page9.oPag.oNODESCRI_9_11.visible=!this.oPgFrm.Page9.oPag.oNODESCRI_9_11.mHide()
    this.oPgFrm.Page9.oPag.oNODESCR2_9_12.visible=!this.oPgFrm.Page9.oPag.oNODESCR2_9_12.mHide()
    this.oPgFrm.Page9.oPag.oStr_9_13.visible=!this.oPgFrm.Page9.oPag.oStr_9_13.mHide()
    this.oPgFrm.Page10.oPag.oNOTIPPER_10_13.visible=!this.oPgFrm.Page10.oPag.oNOTIPPER_10_13.mHide()
    this.oPgFrm.Page10.oPag.oStr_10_14.visible=!this.oPgFrm.Page10.oPag.oStr_10_14.mHide()
    this.oPgFrm.Page10.oPag.oNOCOGNOM_10_15.visible=!this.oPgFrm.Page10.oPag.oNOCOGNOM_10_15.mHide()
    this.oPgFrm.Page10.oPag.oNO__NOME_10_16.visible=!this.oPgFrm.Page10.oPag.oNO__NOME_10_16.mHide()
    this.oPgFrm.Page10.oPag.oStr_10_17.visible=!this.oPgFrm.Page10.oPag.oStr_10_17.mHide()
    this.oPgFrm.Page10.oPag.oStr_10_18.visible=!this.oPgFrm.Page10.oPag.oStr_10_18.mHide()
    this.oPgFrm.Page10.oPag.oNODESCRI_10_19.visible=!this.oPgFrm.Page10.oPag.oNODESCRI_10_19.mHide()
    this.oPgFrm.Page10.oPag.oNODESCR2_10_20.visible=!this.oPgFrm.Page10.oPag.oNODESCR2_10_20.mHide()
    this.oPgFrm.Page10.oPag.oStr_10_21.visible=!this.oPgFrm.Page10.oPag.oStr_10_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_177.visible=!this.oPgFrm.Page1.oPag.oStr_1_177.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_178.visible=!this.oPgFrm.Page1.oPag.oStr_1_178.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_180.visible=!this.oPgFrm.Page1.oPag.oBtn_1_180.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsar_ano
    *Se non c � il modulo offerte cambia gli zoom che estraggono i dati dalle tabelle delle offerte poich� non sono sul database
    IF  g_OFFE<>'S'  and cEvent='FormLoad'
    this.oPgFrm.Page6.oPag.ZoomOff.ctable="CONTI"
    this.oPgFrm.Page6.oPag.ZoomOff.czoomfile="VUOTO"
    this.oPgFrm.Page3.oPag.ZoomAll.ctable="CONTI"
    this.oPgFrm.Page3.oPag.ZoomAll.czoomfile="VUOTO"
    ENDIF
    
    
    IF Upper(CEVENT)='W_NOSOGGET CHANGED'
    local oggtip
    WITH THIS
      IF (.w_NOSOGGET='EN' )
        oggtip=.getctrl("w_NOINDIRI")
        oggtip.tooltiptext=AH_Msgformat("Indirizzo della sede legale")
        oggtip=.getctrl("w_NOLOCALI")
        oggtip.tooltiptext=AH_Msgformat("Localit� della sede legale")
        oggtip=.getctrl("w_NOPROVIN")
        oggtip.tooltiptext=AH_Msgformat("Sigla provincia della sede legale")
      ENDIF
      IF (.w_NOSOGGET='PF' )
        oggtip=.getctrl("w_NOINDIRI")
        oggtip.tooltiptext=AH_Msgformat("Indirizzo di residenza")
        oggtip=.getctrl("w_NOLOCALI")
        oggtip.tooltiptext=AH_Msgformat("Localit� di residenza")
        oggtip=.getctrl("w_NOPROVIN")
        oggtip.tooltiptext=AH_Msgformat("Sigla della provincia di residenza")
      ENDIF
    ENDWITH
    Endif
    
    
    if g_OFFE<>'S' and cEvent='FormLoad'
      this.cAutoZoom = 'GSAR1ACL'
    endif
    
    IF cevent ='w_NOCODBA2 Changed' AND g_APPLICATION <> "ADHOC REVOLUTION"
    this.notifyevent('controlladata')
    ENDIF
    
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_NOTIPPER Changed")
          .Calculate_ZQDJUELRPO()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_107.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_108.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_128.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_129.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_147.Event(cEvent)
        if lower(cEvent)==lower("AggiornaCF") or lower(cEvent)==lower("w_NOCODFIS LostFocus")
          .Calculate_XNLBETFNTC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Edit Aborted") or lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_SOXWATTGMZ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_162.Event(cEvent)
        if lower(cEvent)==lower("Load") or lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_PGQNPFGCXZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_GRJFLMWOLU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete start")
          .Calculate_CZGHMRBITS()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_27.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_SIGJGJWAYX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_NO1MESCL Changed")
          .Calculate_GYGSSTAZGU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_NO2MESCL Changed")
          .Calculate_PGRKRUGTXB()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.ZoomAll.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_KHHVUGHDUO()
          bRefresh=.t.
        endif
      .oPgFrm.Page6.oPag.ZoomOff.Event(cEvent)
      .oPgFrm.Page7.oPag.ZOOM.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_VXJYEYMBXV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CheckForm")
          .Calculate_KQAWRAXMWG()
          bRefresh=.t.
        endif
      .oPgFrm.Page8.oPag.oObj_8_44.Event(cEvent)
      .oPgFrm.Page8.oPag.oObj_8_45.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsar_ano
    IF (UPPER (cevent)= 'EDIT STARTED')
       IF NOT (  EMPTY(this.w_DATOBSN) OR this.w_DATOBSN>this.w_OBTEST )
         this.notifyEvent ("controlli")
       ENDIF
    ENDIF
    if ((cevent='w_NOCOGNOM Changed' or cevent='w_NO__NOME Changed') and isalt())
       IF AH_YESNO('Sono state apportate modifiche al cognome e/o al nome: si vuole ricalcolare la denominazione?')
         this.w_NODESCRI=ALLTRIM(THIS.w_NOCOGNOM)+' ' +ALLTRIM(THIS.w_NO__NOME)
       endif
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NOCODCLI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_NOCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_NOTIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_NOTIPCLI;
                       ,'ANCODICE',this.w_NOCODCLI)
            select ANTIPCON,ANCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODCLI = NVL(_Link_.ANCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODCLI = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOCOD_RU
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCOD_RU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_NOCOD_RU)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPRIS);

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TIPRIS;
                     ,'DPCODICE',trim(this.w_NOCOD_RU))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCOD_RU)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOCOD_RU) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oNOCOD_RU_1_12'),i_cWhere,'GSAR_BDZ',"Persone",'GSAR_ABD.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select *;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCOD_RU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_NOCOD_RU);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TIPRIS;
                       ,'DPCODICE',this.w_NOCOD_RU)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCOD_RU = NVL(_Link_.DPCODICE,space(5))
      this.w_NOBADGE = NVL(_Link_.DPBADGE,space(20))
      this.w_NOCOGNOM = NVL(_Link_.DPCOGNOM,space(50))
      this.w_NO__NOME = NVL(_Link_.DPNOME,space(50))
      this.w_LOCNAS = NVL(_Link_.DPNASLOC,space(40))
      this.w_NOPRONAS = NVL(_Link_.DPNASPRO,space(2))
      this.w_NODATNAS = NVL(cp_ToDate(_Link_.DPDATNAS),ctod("  /  /  "))
      this.w_NO_SESSO = NVL(_Link_.DP_SESSO,space(1))
      this.w_NOCODFIS = NVL(_Link_.DPCODFIS,space(16))
      this.w_NOINDIRI = NVL(_Link_.DPRESVIA,space(35))
      this.w_NO___CAP = NVL(_Link_.DPRESCAP,space(9))
      this.w_NOLOCALI = NVL(_Link_.DPRESLOC,space(30))
      this.w_NOPROVIN = NVL(_Link_.DPRESPRO,space(2))
      this.w_NOINDIR2 = NVL(_Link_.DPDOMVIA,space(35))
      this.w_NO___CAD = NVL(_Link_.DPDOMCAP,space(9))
      this.w_LOCALD = NVL(_Link_.DPDOMLOC,space(40))
      this.w_NOPROVID = NVL(_Link_.DPDOMPRO,space(2))
      this.w_NONAZNAS = NVL(_Link_.DPNASNAZ,space(3))
      this.w_NOTELEFO = NVL(_Link_.DPTELEF1,space(18))
      this.w_NO_EMAIL = NVL(_Link_.DPINDMAI,space(254))
      this.w_NO_EMPEC = NVL(_Link_.DPINDPEC,space(254))
      this.w_DPFLINEX = NVL(_Link_.DPFLINEX,space(1))
      this.w_NOPARIVA = NVL(_Link_.DPPARIVA,space(12))
      this.w_NONUMCAR = NVL(_Link_.DPNUMCAR,space(18))
      this.w_NOINDWEB = NVL(_Link_.DPINDWEB,space(254))
      this.w_NONUMCEL = NVL(_Link_.DPNUMCEL,space(18))
      this.w_NOINDI_2 = NVL(_Link_.DPINDIR2,space(35))
      this.w_NONAZION = NVL(_Link_.DPNAZION,space(3))
      this.w_NONAZIOD = NVL(_Link_.DPNAZION,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_NOCOD_RU = space(5)
      endif
      this.w_NOBADGE = space(20)
      this.w_NOCOGNOM = space(50)
      this.w_NO__NOME = space(50)
      this.w_LOCNAS = space(40)
      this.w_NOPRONAS = space(2)
      this.w_NODATNAS = ctod("  /  /  ")
      this.w_NO_SESSO = space(1)
      this.w_NOCODFIS = space(16)
      this.w_NOINDIRI = space(35)
      this.w_NO___CAP = space(9)
      this.w_NOLOCALI = space(30)
      this.w_NOPROVIN = space(2)
      this.w_NOINDIR2 = space(35)
      this.w_NO___CAD = space(9)
      this.w_LOCALD = space(40)
      this.w_NOPROVID = space(2)
      this.w_NONAZNAS = space(3)
      this.w_NOTELEFO = space(18)
      this.w_NO_EMAIL = space(254)
      this.w_NO_EMPEC = space(254)
      this.w_DPFLINEX = space(1)
      this.w_NOPARIVA = space(12)
      this.w_NONUMCAR = space(18)
      this.w_NOINDWEB = space(254)
      this.w_NONUMCEL = space(18)
      this.w_NOINDI_2 = space(35)
      this.w_NONAZION = space(3)
      this.w_NONAZIOD = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DPFLINEX='I' AND .w_NOTIPPER='D') OR (.w_DPFLINEX='E' AND .w_NOTIPPER='C')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NOCOD_RU = space(5)
        this.w_NOBADGE = space(20)
        this.w_NOCOGNOM = space(50)
        this.w_NO__NOME = space(50)
        this.w_LOCNAS = space(40)
        this.w_NOPRONAS = space(2)
        this.w_NODATNAS = ctod("  /  /  ")
        this.w_NO_SESSO = space(1)
        this.w_NOCODFIS = space(16)
        this.w_NOINDIRI = space(35)
        this.w_NO___CAP = space(9)
        this.w_NOLOCALI = space(30)
        this.w_NOPROVIN = space(2)
        this.w_NOINDIR2 = space(35)
        this.w_NO___CAD = space(9)
        this.w_LOCALD = space(40)
        this.w_NOPROVID = space(2)
        this.w_NONAZNAS = space(3)
        this.w_NOTELEFO = space(18)
        this.w_NO_EMAIL = space(254)
        this.w_NO_EMPEC = space(254)
        this.w_DPFLINEX = space(1)
        this.w_NOPARIVA = space(12)
        this.w_NONUMCAR = space(18)
        this.w_NOINDWEB = space(254)
        this.w_NONUMCEL = space(18)
        this.w_NOINDI_2 = space(35)
        this.w_NONAZION = space(3)
        this.w_NONAZIOD = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCOD_RU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NONAZNAS
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NONAZNAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_NONAZNAS)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_NONAZNAS))
          select NACODNAZ,NADESNAZ,NACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NONAZNAS)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NADESNAZ like "+cp_ToStrODBC(trim(this.w_NONAZNAS)+"%");

            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NADESNAZ like "+cp_ToStr(trim(this.w_NONAZNAS)+"%");

            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NONAZNAS) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oNONAZNAS_1_24'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NONAZNAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_NONAZNAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_NONAZNAS)
            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NONAZNAS = NVL(_Link_.NACODNAZ,space(3))
      this.w_DESNAZ1 = NVL(_Link_.NADESNAZ,space(35))
      this.w_CODISO1 = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_NONAZNAS = space(3)
      endif
      this.w_DESNAZ1 = space(35)
      this.w_CODISO1 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NONAZNAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_24.NACODNAZ as NACODNAZ124"+ ",link_1_24.NADESNAZ as NADESNAZ124"+ ",link_1_24.NACODISO as NACODISO124"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_24 on OFF_NOMI.NONAZNAS=link_1_24.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_24"
          i_cKey=i_cKey+'+" and OFF_NOMI.NONAZNAS=link_1_24.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCODSAL
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SAL_NOMI_IDX,3]
    i_lTable = "SAL_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SAL_NOMI_IDX,2], .t., this.SAL_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODSAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SAL_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SACODICE like "+cp_ToStrODBC(trim(this.w_NOCODSAL)+"%");

          i_ret=cp_SQL(i_nConn,"select SACODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SACODICE',trim(this.w_NOCODSAL))
          select SACODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODSAL)==trim(_Link_.SACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOCODSAL) and !this.bDontReportError
            deferred_cp_zoom('SAL_NOMI','*','SACODICE',cp_AbsName(oSource.parent,'oNOCODSAL_1_29'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SACODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SACODICE',oSource.xKey(1))
            select SACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODSAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SACODICE="+cp_ToStrODBC(this.w_NOCODSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SACODICE',this.w_NOCODSAL)
            select SACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODSAL = NVL(_Link_.SACODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODSAL = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SAL_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.SACODICE,1)
      cp_ShowWarn(i_cKey,this.SAL_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODSAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NONAZION
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NONAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_NONAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_NONAZION))
          select NACODNAZ,NADESNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NONAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NADESNAZ like "+cp_ToStrODBC(trim(this.w_NONAZION)+"%");

            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NADESNAZ like "+cp_ToStr(trim(this.w_NONAZION)+"%");

            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NONAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oNONAZION_1_39'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NONAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_NONAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_NONAZION)
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NONAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_COMODO = NVL(_Link_.NADESNAZ,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_NONAZION = space(3)
      endif
      this.w_COMODO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NONAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_39(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_39.NACODNAZ as NACODNAZ139"+ ",link_1_39.NADESNAZ as NADESNAZ139"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_39 on OFF_NOMI.NONAZION=link_1_39.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_39"
          i_cKey=i_cKey+'+" and OFF_NOMI.NONAZION=link_1_39.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NONAZIOD
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NONAZIOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_NONAZIOD)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_NONAZIOD))
          select NACODNAZ,NADESNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NONAZIOD)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NADESNAZ like "+cp_ToStrODBC(trim(this.w_NONAZIOD)+"%");

            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NADESNAZ like "+cp_ToStr(trim(this.w_NONAZIOD)+"%");

            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NONAZIOD) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oNONAZIOD_1_50'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NONAZIOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_NONAZIOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_NONAZIOD)
            select NACODNAZ,NADESNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NONAZIOD = NVL(_Link_.NACODNAZ,space(3))
      this.w_COMODO = NVL(_Link_.NADESNAZ,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_NONAZIOD = space(3)
      endif
      this.w_COMODO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NONAZIOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_50(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_50.NACODNAZ as NACODNAZ150"+ ",link_1_50.NADESNAZ as NADESNAZ150"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_50 on OFF_NOMI.NONAZIOD=link_1_50.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_50"
          i_cKey=i_cKey+'+" and OFF_NOMI.NONAZIOD=link_1_50.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCODCON
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_NOCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_NOTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODPAG,ANGIOFIS,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANCODBAN,ANDTOBSO,ANRITENU,ANFLRITE,ANTIPCLF,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_NOTIPCON;
                     ,'ANCODICE',trim(this.w_NOCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCODPAG,ANGIOFIS,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANCODBAN,ANDTOBSO,ANRITENU,ANFLRITE,ANTIPCLF,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_NOCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_NOTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODPAG,ANGIOFIS,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANCODBAN,ANDTOBSO,ANRITENU,ANFLRITE,ANTIPCLF,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_NOCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_NOTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCODPAG,ANGIOFIS,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANCODBAN,ANDTOBSO,ANRITENU,ANFLRITE,ANTIPCLF,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oNOCODCON_2_3'),i_cWhere,'GSAR_BZC',"Clienti/fornitori/conti (partite)",'GSAR_DCD.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_NOTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODPAG,ANGIOFIS,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANCODBAN,ANDTOBSO,ANRITENU,ANFLRITE,ANTIPCLF,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODPAG,ANGIOFIS,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANCODBAN,ANDTOBSO,ANRITENU,ANFLRITE,ANTIPCLF,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente,obsoleto,con gestione ritenute attivata oppure non gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODPAG,ANGIOFIS,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANCODBAN,ANDTOBSO,ANRITENU,ANFLRITE,ANTIPCLF,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_NOTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODPAG,ANGIOFIS,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANCODBAN,ANDTOBSO,ANRITENU,ANFLRITE,ANTIPCLF,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODPAG,ANGIOFIS,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANCODBAN,ANDTOBSO,ANRITENU,ANFLRITE,ANTIPCLF,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_NOCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_NOTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_NOTIPCON;
                       ,'ANCODICE',this.w_NOCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODPAG,ANGIOFIS,AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANCODBAN,ANDTOBSO,ANRITENU,ANFLRITE,ANTIPCLF,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_NOCODPAG = NVL(_Link_.ANCODPAG,space(5))
      this.w_NOGIOFIS = NVL(_Link_.ANGIOFIS,0)
      this.w_NO1MESCL = NVL(_Link_.AN1MESCL,0)
      this.w_NO2MESCL = NVL(_Link_.AN2MESCL,0)
      this.w_NOGIOSC1 = NVL(_Link_.ANGIOSC1,0)
      this.w_NOGIOSC2 = NVL(_Link_.ANGIOSC2,0)
      this.w_NOCODBAN = NVL(_Link_.ANCODBAN,space(10))
      this.w_DATOBSN = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_ANRITENU = NVL(_Link_.ANRITENU,space(10))
      this.w_ANFLRITE = NVL(_Link_.ANFLRITE,space(1))
      this.w_ANTIPCLF = NVL(_Link_.ANTIPCLF,space(1))
      this.w_ANPARTSN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODCON = space(15)
      endif
      this.w_ANDESCRI = space(40)
      this.w_NOCODPAG = space(5)
      this.w_NOGIOFIS = 0
      this.w_NO1MESCL = 0
      this.w_NO2MESCL = 0
      this.w_NOGIOSC1 = 0
      this.w_NOGIOSC2 = 0
      this.w_NOCODBAN = space(10)
      this.w_DATOBSN = ctod("  /  /  ")
      this.w_ANRITENU = space(10)
      this.w_ANFLRITE = space(1)
      this.w_ANTIPCLF = space(1)
      this.w_ANPARTSN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSN) OR .w_DATOBSN>.w_OBTEST) AND (.w_NOTIPCON='G' OR (.w_NOTIPCON='C' AND .w_ANFLRITE<>'S') OR (.w_NOTIPCON='F' AND .w_ANTIPCLF='G' AND .w_ANRITENU='N') ) AND .w_ANPARTSN='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente,obsoleto,con gestione ritenute attivata oppure non gestito a partite")
        endif
        this.w_NOCODCON = space(15)
        this.w_ANDESCRI = space(40)
        this.w_NOCODPAG = space(5)
        this.w_NOGIOFIS = 0
        this.w_NO1MESCL = 0
        this.w_NO2MESCL = 0
        this.w_NOGIOSC1 = 0
        this.w_NOGIOSC2 = 0
        this.w_NOCODBAN = space(10)
        this.w_DATOBSN = ctod("  /  /  ")
        this.w_ANRITENU = space(10)
        this.w_ANFLRITE = space(1)
        this.w_ANTIPCLF = space(1)
        this.w_ANPARTSN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 14 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+14<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ANCODICE as ANCODICE203"+ ",link_2_3.ANDESCRI as ANDESCRI203"+ ",link_2_3.ANCODPAG as ANCODPAG203"+ ",link_2_3.ANGIOFIS as ANGIOFIS203"+ ",link_2_3.AN1MESCL as AN1MESCL203"+ ",link_2_3.AN2MESCL as AN2MESCL203"+ ",link_2_3.ANGIOSC1 as ANGIOSC1203"+ ",link_2_3.ANGIOSC2 as ANGIOSC2203"+ ",link_2_3.ANCODBAN as ANCODBAN203"+ ",link_2_3.ANDTOBSO as ANDTOBSO203"+ ",link_2_3.ANRITENU as ANRITENU203"+ ",link_2_3.ANFLRITE as ANFLRITE203"+ ",link_2_3.ANTIPCLF as ANTIPCLF203"+ ",link_2_3.ANPARTSN as ANPARTSN203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on OFF_NOMI.NOCODCON=link_2_3.ANCODICE"+" and OFF_NOMI.NOTIPCON=link_2_3.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCODCON=link_2_3.ANCODICE(+)"'+'+" and OFF_NOMI.NOTIPCON=link_2_3.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCODPAG
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_NOCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_NOCODPAG))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_NOCODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_NOCODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_NOCODPAG)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oNOCODPAG_2_4'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_NOCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_NOCODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSOPAG = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DATOBSOPAG = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSOPAG) OR .w_DATOBSOPAG>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_NOCODPAG = space(5)
        this.w_DESPAG = space(30)
        this.w_DATOBSOPAG = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.PACODICE as PACODICE204"+ ","+cp_TransLinkFldName('link_2_4.PADESCRI')+" as PADESCRI204"+ ",link_2_4.PADTOBSO as PADTOBSO204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on OFF_NOMI.NOCODPAG=link_2_4.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCODPAG=link_2_4.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCODBAN
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_NOCODBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_NOCODBAN))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOCODBAN) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oNOCODBAN_2_13'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_NOCODBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_NOCODBAN)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODBAN = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(42))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODBAN = space(10)
      endif
      this.w_DESBAN = space(42)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.BAN_CHE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.BACODBAN as BACODBAN213"+ ",link_2_13.BADESBAN as BADESBAN213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on OFF_NOMI.NOCODBAN=link_2_13.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCODBAN=link_2_13.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=BANCAAHE
  func Link_2_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANCAAHE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACC',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BANUMCOR like "+cp_ToStrODBC(trim(this.w_BANCAAHE)+"%");

          i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BANUMCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BANUMCOR',trim(this.w_BANCAAHE))
          select BANUMCOR,BADESCRI,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BANUMCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANCAAHE)==trim(_Link_.BANUMCOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BANCAAHE) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BANUMCOR',cp_AbsName(oSource.parent,'oBANCAAHE_2_43'),i_cWhere,'GSTE_ACC',"Elenco conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BANUMCOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BANUMCOR',oSource.xKey(1))
            select BANUMCOR,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANCAAHE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BANUMCOR="+cp_ToStrODBC(this.w_BANCAAHE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BANUMCOR',this.w_BANCAAHE)
            select BANUMCOR,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANCAAHE = NVL(_Link_.BANUMCOR,space(10))
      this.w_DESBA2 = NVL(_Link_.BADESCRI,space(42))
      this.w_DATOBSOBA2 = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_BANCAAHE = space(10)
      endif
      this.w_DESBA2 = space(42)
      this.w_DATOBSOBA2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BANUMCOR,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANCAAHE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOCATFIN
  func Link_2_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_FINA_IDX,3]
    i_lTable = "CAT_FINA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2], .t., this.CAT_FINA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gste_Acf',True,'CAT_FINA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CFCODICE like "+cp_ToStrODBC(trim(this.w_NOCATFIN)+"%");
                   +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFIN);

          i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CFTIPCAT,CFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CFTIPCAT',this.w_TIPOFIN;
                     ,'CFCODICE',trim(this.w_NOCATFIN))
          select CFTIPCAT,CFCODICE,CFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CFTIPCAT,CFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCATFIN)==trim(_Link_.CFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOCATFIN) and !this.bDontReportError
            deferred_cp_zoom('CAT_FINA','*','CFTIPCAT,CFCODICE',cp_AbsName(oSource.parent,'oNOCATFIN_2_44'),i_cWhere,'Gste_Acf',"Categorie finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOFIN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("E' possibile selezionare solo categorie di tipo conti")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFIN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCAT',oSource.xKey(1);
                       ,'CFCODICE',oSource.xKey(2))
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(this.w_NOCATFIN);
                   +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCAT',this.w_TIPOFIN;
                       ,'CFCODICE',this.w_NOCATFIN)
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCATFIN = NVL(_Link_.CFCODICE,space(5))
      this.w_CFDESCRIC = NVL(_Link_.CFDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_NOCATFIN = space(5)
      endif
      this.w_CFDESCRIC = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2])+'\'+cp_ToStr(_Link_.CFTIPCAT,1)+'\'+cp_ToStr(_Link_.CFCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_FINA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOCATDER
  func Link_2_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_FINA_IDX,3]
    i_lTable = "CAT_FINA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2], .t., this.CAT_FINA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCATDER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gste_Acf',True,'CAT_FINA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CFCODICE like "+cp_ToStrODBC(trim(this.w_NOCATDER)+"%");
                   +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFIND);

          i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CFTIPCAT,CFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CFTIPCAT',this.w_TIPOFIND;
                     ,'CFCODICE',trim(this.w_NOCATDER))
          select CFTIPCAT,CFCODICE,CFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CFTIPCAT,CFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCATDER)==trim(_Link_.CFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOCATDER) and !this.bDontReportError
            deferred_cp_zoom('CAT_FINA','*','CFTIPCAT,CFCODICE',cp_AbsName(oSource.parent,'oNOCATDER_2_45'),i_cWhere,'Gste_Acf',"Categorie finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOFIND<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("E' possibile selezionare solo categorie di tipo deroghe")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFIND);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCAT',oSource.xKey(1);
                       ,'CFCODICE',oSource.xKey(2))
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCATDER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CFTIPCAT,CFCODICE,CFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CFCODICE="+cp_ToStrODBC(this.w_NOCATDER);
                   +" and CFTIPCAT="+cp_ToStrODBC(this.w_TIPOFIND);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CFTIPCAT',this.w_TIPOFIND;
                       ,'CFCODICE',this.w_NOCATDER)
            select CFTIPCAT,CFCODICE,CFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCATDER = NVL(_Link_.CFCODICE,space(5))
      this.w_CFDESCRID = NVL(_Link_.CFDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_NOCATDER = space(5)
      endif
      this.w_CFDESCRID = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_FINA_IDX,2])+'\'+cp_ToStr(_Link_.CFTIPCAT,1)+'\'+cp_ToStr(_Link_.CFCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_FINA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCATDER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOCODGRU
  func Link_4_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_NOMI_IDX,3]
    i_lTable = "GRU_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2], .t., this.GRU_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGN',True,'GRU_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GNCODICE like "+cp_ToStrODBC(trim(this.w_NOCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GNCODICE',trim(this.w_NOCODGRU))
          select GNCODICE,GNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODGRU)==trim(_Link_.GNCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" GNDESCRI like "+cp_ToStrODBC(trim(this.w_NOCODGRU)+"%");

            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GNDESCRI like "+cp_ToStr(trim(this.w_NOCODGRU)+"%");

            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRU_NOMI','*','GNCODICE',cp_AbsName(oSource.parent,'oNOCODGRU_4_6'),i_cWhere,'GSAR_AGN',"Gruppi nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',oSource.xKey(1))
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(this.w_NOCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',this.w_NOCODGRU)
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODGRU = NVL(_Link_.GNCODICE,space(5))
      this.w_NODESGRU = NVL(_Link_.GNDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODGRU = space(5)
      endif
      this.w_NODESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.GNCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRU_NOMI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_6.GNCODICE as GNCODICE406"+ ",link_4_6.GNDESCRI as GNDESCRI406"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_6 on OFF_NOMI.NOCODGRU=link_4_6.GNCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_6"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCODGRU=link_4_6.GNCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCODORI
  func Link_4_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ORI_NOMI_IDX,3]
    i_lTable = "ORI_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2], .t., this.ORI_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AON',True,'ORI_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ONCODICE like "+cp_ToStrODBC(trim(this.w_NOCODORI)+"%");

          i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ONCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ONCODICE',trim(this.w_NOCODORI))
          select ONCODICE,ONDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ONCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODORI)==trim(_Link_.ONCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ONDESCRI like "+cp_ToStrODBC(trim(this.w_NOCODORI)+"%");

            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ONDESCRI like "+cp_ToStr(trim(this.w_NOCODORI)+"%");

            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOCODORI) and !this.bDontReportError
            deferred_cp_zoom('ORI_NOMI','*','ONCODICE',cp_AbsName(oSource.parent,'oNOCODORI_4_9'),i_cWhere,'GSAR_AON',"Origine",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',oSource.xKey(1))
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(this.w_NOCODORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',this.w_NOCODORI)
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODORI = NVL(_Link_.ONCODICE,space(5))
      this.w_NODESORI = NVL(_Link_.ONDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODORI = space(5)
      endif
      this.w_NODESORI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.ONCODICE,1)
      cp_ShowWarn(i_cKey,this.ORI_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ORI_NOMI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_9.ONCODICE as ONCODICE409"+ ",link_4_9.ONDESCRI as ONDESCRI409"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_9 on OFF_NOMI.NOCODORI=link_4_9.ONCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_9"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCODORI=link_4_9.ONCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NORIFINT
  func Link_4_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NORIFINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_NORIFINT))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NORIFINT)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_NORIFINT)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NORIFINT) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oNORIFINT_4_12'),i_cWhere,'GSAR_BDZ',"Riferimenti interni",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NORIFINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_NORIFINT);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_NORIFINT)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NORIFINT = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNOME = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOME = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_NORIFINT = space(5)
      endif
      this.w_COGNOME = space(40)
      this.w_NOME = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NORIFINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOCODOPE
  func Link_4_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_NOCODOPE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_NOCODOPE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_NOCODOPE) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oNOCODOPE_4_13'),i_cWhere,'',"Elenco utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_NOCODOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_NOCODOPE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODOPE = NVL(_Link_.CODE,0)
      this.w_NODESOPE = NVL(_Link_.NAME,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODOPE = 0
      endif
      this.w_NODESOPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOCODZON
  func Link_4_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_NOCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_NOCODZON))
          select ZOCODZON,ZODESZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oNOCODZON_4_17'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_NOCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_NOCODZON)
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_NODESZON = NVL(_Link_.ZODESZON,space(35))
      this.w_DTOBZON = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODZON = space(3)
      endif
      this.w_NODESZON = space(35)
      this.w_DTOBZON = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DTOBZON, .w_OBTEST, "Zona obsoleta alla data Attuale!"), CHKDTOBS(.w_DTOBZON,.w_OBTEST,"Zona obsoleta alla data Attuale!", .T.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NOCODZON = space(3)
        this.w_NODESZON = space(35)
        this.w_DTOBZON = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ZONE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_17.ZOCODZON as ZOCODZON417"+ ",link_4_17.ZODESZON as ZODESZON417"+ ",link_4_17.ZODTOBSO as ZODTOBSO417"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_17 on OFF_NOMI.NOCODZON=link_4_17.ZOCODZON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_17"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCODZON=link_4_17.ZOCODZON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCODLIN
  func Link_4_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_NOCODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_NOCODLIN))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LUDESCRI like "+cp_ToStrODBC(trim(this.w_NOCODLIN)+"%");

            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LUDESCRI like "+cp_ToStr(trim(this.w_NOCODLIN)+"%");

            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOCODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oNOCODLIN_4_20'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_NOCODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_NOCODLIN)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODLIN = NVL(_Link_.LUCODICE,space(3))
      this.w_NODESLIN = NVL(_Link_.LUDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODLIN = space(3)
      endif
      this.w_NODESLIN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LINGUE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_20.LUCODICE as LUCODICE420"+ ",link_4_20.LUDESCRI as LUDESCRI420"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_20 on OFF_NOMI.NOCODLIN=link_4_20.LUCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_20"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCODLIN=link_4_20.LUCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCODVAL
  func Link_4_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_NOCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_NOCODVAL))
          select VACODVAL,VADESVAL,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_NOCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_NOCODVAL)+"%");

            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oNOCODVAL_4_23'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_NOCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_NOCODVAL)
            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_NODESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DTOBVAL = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODVAL = space(3)
      endif
      this.w_NODESVAL = space(35)
      this.w_DTOBVAL = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF(UPPER(g_APPLICATION)="AD HOC ENTERPRISE",CHKDTOBS(this,.w_DTOBVAL,.w_OBTEST,"Valuta obsoleta alla data Attuale!") AND CHKVLCF(.w_NOCODVAL,.w_VALLIS,"NOMINATIVO",'N',.w_IVALIS,.w_INIVAL,.w_FINVAL),CHKDTOBS(.w_DTOBVAL,.w_OBTEST,"Valuta obsoleta alla data Attuale!") AND CHKVLCF(.w_NOCODVAL, .w_VALLIS, "NOMINATIVO",'N',' '))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NOCODVAL = space(3)
        this.w_NODESVAL = space(35)
        this.w_DTOBVAL = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_23.VACODVAL as VACODVAL423"+ ",link_4_23.VADESVAL as VADESVAL423"+ ",link_4_23.VADTOBSO as VADTOBSO423"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_23 on OFF_NOMI.NOCODVAL=link_4_23.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_23"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCODVAL=link_4_23.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCODAGE
  func Link_4_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_NOCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_NOCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_NOCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_NOCODAGE)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oNOCODAGE_4_24'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_NOCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_NOCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_NODESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DTOBAGE = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODAGE = space(5)
      endif
      this.w_NODESAGE = space(35)
      this.w_DTOBAGE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DTOBAGE,.w_OBTEST,"Agente obsoleto alla data Attuale!"), CHKDTOBS(.w_DTOBAGE,.w_OBTEST,"Agente obsoleto alla data Attuale!", .T.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NOCODAGE = space(5)
        this.w_NODESAGE = space(35)
        this.w_DTOBAGE = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_24.AGCODAGE as AGCODAGE424"+ ",link_4_24.AGDESAGE as AGDESAGE424"+ ",link_4_24.AGDTOBSO as AGDTOBSO424"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_24 on OFF_NOMI.NOCODAGE=link_4_24.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_24"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCODAGE=link_4_24.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NONUMLIS
  func Link_4_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NONUMLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_NONUMLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_NONUMLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NONUMLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_NONUMLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_NONUMLIS)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NONUMLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oNONUMLIS_4_44'),i_cWhere,'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NONUMLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_NONUMLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_NONUMLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NONUMLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NONUMLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_IVALIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes= IIF (g_APPLICATION <> "ADHOC REVOLUTION"  , .T. ,CHKVLCF(.w_NOCODVAL, .w_VALLIS, "NOMINATIVO", 'N',.w_IVALIS))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NONUMLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_VALLIS = space(3)
        this.w_IVALIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NONUMLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_44(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_44.LSCODLIS as LSCODLIS444"+ ",link_4_44.LSDESLIS as LSDESLIS444"+ ",link_4_44.LSVALLIS as LSVALLIS444"+ ",link_4_44.LSIVALIS as LSIVALIS444"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_44 on OFF_NOMI.NONUMLIS=link_4_44.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_44"
          i_cKey=i_cKey+'+" and OFF_NOMI.NONUMLIS=link_4_44.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NONUMLIS
  func Link_4_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NONUMLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_NONUMLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_NONUMLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NONUMLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_NONUMLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_NONUMLIS)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NONUMLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oNONUMLIS_4_45'),i_cWhere,'',"Listini",'QUERY\ASSCLIFOR.VZM.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NONUMLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_NONUMLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_NONUMLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTINVA,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NONUMLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
      this.w_INIVAL = NVL(cp_ToDate(_Link_.LSDTINVA),ctod("  /  /  "))
      this.w_FINVAL = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NONUMLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_IVALIS = space(1)
      this.w_INIVAL = ctod("  /  /  ")
      this.w_FINVAL = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes= IIF (g_APPLICATION = "ADHOC REVOLUTION" ,.T.,CHKVLCF(.w_NOCODVAL, .w_VALLIS, "NOMINATIVO", 'N',.w_IVALIS,.w_INIVAL,.w_FINVAL)   )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NONUMLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_VALLIS = space(3)
        this.w_IVALIS = space(1)
        this.w_INIVAL = ctod("  /  /  ")
        this.w_FINVAL = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NONUMLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_45(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_45.LSCODLIS as LSCODLIS445"+ ",link_4_45.LSDESLIS as LSDESLIS445"+ ",link_4_45.LSVALLIS as LSVALLIS445"+ ",link_4_45.LSIVALIS as LSIVALIS445"+ ",link_4_45.LSDTINVA as LSDTINVA445"+ ",link_4_45.LSDTOBSO as LSDTOBSO445"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_45 on OFF_NOMI.NONUMLIS=link_4_45.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_45"
          i_cKey=i_cKey+'+" and OFF_NOMI.NONUMLIS=link_4_45.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCODPAG
  func Link_4_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_NOCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_NOCODPAG))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_NOCODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_NOCODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_NOCODPAG)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oNOCODPAG_4_46'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_NOCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_NOCODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSOPAG = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DATOBSOPAG = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSOPAG) OR .w_DATOBSOPAG>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_NOCODPAG = space(5)
        this.w_DESPAG = space(30)
        this.w_DATOBSOPAG = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_46(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_46.PACODICE as PACODICE446"+ ","+cp_TransLinkFldName('link_4_46.PADESCRI')+" as PADESCRI446"+ ",link_4_46.PADTOBSO as PADTOBSO446"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_46 on OFF_NOMI.NOCODPAG=link_4_46.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_46"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCODPAG=link_4_46.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCODBAN
  func Link_4_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCODBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_NOCODBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_NOCODBAN))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCODBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOCODBAN) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oNOCODBAN_4_47'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCODBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_NOCODBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_NOCODBAN)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCODBAN = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(42))
    else
      if i_cCtrl<>'Load'
        this.w_NOCODBAN = space(10)
      endif
      this.w_DESBAN = space(42)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCODBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_47(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.BAN_CHE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_47.BACODBAN as BACODBAN447"+ ",link_4_47.BADESBAN as BADESBAN447"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_47 on OFF_NOMI.NOCODBAN=link_4_47.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_47"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCODBAN=link_4_47.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCATCOM
  func Link_4_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_NOCATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_NOCATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOCATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oNOCATCOM_4_48'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_NOCATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_NOCATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCAC = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NOCATCOM = space(3)
      endif
      this.w_DESCAC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_48(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATECOMM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_48.CTCODICE as CTCODICE448"+ ",link_4_48.CTDESCRI as CTDESCRI448"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_48 on OFF_NOMI.NOCATCOM=link_4_48.CTCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_48"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCATCOM=link_4_48.CTCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCATCON
  func Link_4_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_NOCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_NOCATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOCATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oNOCATCON_4_49'),i_cWhere,'GSAR_AC2',"Categorie contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_NOCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_NOCATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCON = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NOCATCON = space(5)
      endif
      this.w_DESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_49(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOCLFO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_49.C2CODICE as C2CODICE449"+ ",link_4_49.C2DESCRI as C2DESCRI449"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_49 on OFF_NOMI.NOCATCON=link_4_49.C2CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_49"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCATCON=link_4_49.C2CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOCACLFO
  func Link_4_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAANCLFO_IDX,3]
    i_lTable = "CAANCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAANCLFO_IDX,2], .t., this.CAANCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAANCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOCACLFO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACF',True,'CAANCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_NOCACLFO)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_NOCACLFO))
          select C1CODICE,C1DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOCACLFO)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NOCACLFO) and !this.bDontReportError
            deferred_cp_zoom('CAANCLFO','*','C1CODICE',cp_AbsName(oSource.parent,'oNOCACLFO_4_50'),i_cWhere,'GSAR_ACF',"Elenco cat. analitica intestatario",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOCACLFO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_NOCACLFO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_NOCACLFO)
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOCACLFO = NVL(_Link_.C1CODICE,space(5))
      this.w_DESSCATCLFO = NVL(_Link_.C1DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NOCACLFO = space(5)
      endif
      this.w_DESSCATCLFO = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAANCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CAANCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOCACLFO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_50(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAANCLFO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAANCLFO_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_50.C1CODICE as C1CODICE450"+ ",link_4_50.C1DESCRI as C1DESCRI450"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_50 on OFF_NOMI.NOCACLFO=link_4_50.C1CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_50"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOCACLFO=link_4_50.C1CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=BANCAAHR
  func Link_4_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANCAAHR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANCAAHR)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANCAAHR))
          select BACODBAN,BADESCRI,BADTOBSO,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANCAAHR)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BANCAAHR) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oBANCAAHR_4_51'),i_cWhere,'GSTE_ACB',"Elenco conti banche",'GSAR_ACL.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANCAAHR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANCAAHR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANCAAHR)
            select BACODBAN,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANCAAHR = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBA3 = NVL(_Link_.BADESCRI,space(42))
      this.w_DATOBSOBA2 = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_CONSBF = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_BANCAAHR = space(10)
      endif
      this.w_DESBA3 = space(42)
      this.w_DATOBSOBA2 = ctod("  /  /  ")
      this.w_CONSBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CONSBF='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto o di tipo salvo buon fine")
        endif
        this.w_BANCAAHR = space(10)
        this.w_DESBA3 = space(42)
        this.w_DATOBSOBA2 = ctod("  /  /  ")
        this.w_CONSBF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANCAAHR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANCAAHE
  func Link_4_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANCAAHE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACC',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BANUMCOR like "+cp_ToStrODBC(trim(this.w_BANCAAHE)+"%");

          i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BANUMCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BANUMCOR',trim(this.w_BANCAAHE))
          select BANUMCOR,BADESCRI,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BANUMCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANCAAHE)==trim(_Link_.BANUMCOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BANCAAHE) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BANUMCOR',cp_AbsName(oSource.parent,'oBANCAAHE_4_52'),i_cWhere,'GSTE_ACC',"Elenco conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BANUMCOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BANUMCOR',oSource.xKey(1))
            select BANUMCOR,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANCAAHE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BANUMCOR="+cp_ToStrODBC(this.w_BANCAAHE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BANUMCOR',this.w_BANCAAHE)
            select BANUMCOR,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANCAAHE = NVL(_Link_.BANUMCOR,space(10))
      this.w_DESBA2 = NVL(_Link_.BADESCRI,space(42))
      this.w_DATOBSOBA2 = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_BANCAAHE = space(10)
      endif
      this.w_DESBA2 = space(42)
      this.w_DATOBSOBA2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBS>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto o di tipo salvo buon fine")
        endif
        this.w_BANCAAHE = space(10)
        this.w_DESBA2 = space(42)
        this.w_DATOBSOBA2 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BANUMCOR,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANCAAHE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NFGRPRIO
  func Link_6_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_PRIO_IDX,3]
    i_lTable = "GRU_PRIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2], .t., this.GRU_PRIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NFGRPRIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOF_AGP',True,'GRU_PRIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_NFGRPRIO)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPNUMINI,GPNUMFIN,GPDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_NFGRPRIO))
          select GPCODICE,GPNUMINI,GPNUMFIN,GPDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NFGRPRIO)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NFGRPRIO) and !this.bDontReportError
            deferred_cp_zoom('GRU_PRIO','*','GPCODICE',cp_AbsName(oSource.parent,'oNFGRPRIO_6_17'),i_cWhere,'GSOF_AGP',"Gruppi priorit�",'GSOF_ZGP.GRU_PRIO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPNUMINI,GPNUMFIN,GPDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPNUMINI,GPNUMFIN,GPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NFGRPRIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPNUMINI,GPNUMFIN,GPDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_NFGRPRIO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_NFGRPRIO)
            select GPCODICE,GPNUMINI,GPNUMFIN,GPDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NFGRPRIO = NVL(_Link_.GPCODICE,space(5))
      this.w_NUMINI = NVL(_Link_.GPNUMINI,space(10))
      this.w_NUMFIN = NVL(_Link_.GPNUMFIN,space(10))
      this.w_DTOBGR = NVL(cp_ToDate(_Link_.GPDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NFGRPRIO = space(5)
      endif
      this.w_NUMINI = space(10)
      this.w_NUMFIN = space(10)
      this.w_DTOBGR = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DTOBGR,.w_OBTEST,"Gruppo Priorit� obsoleto alla data Attuale!"),CHKDTOBS(.w_DTOBGR,.w_OBTEST,"Gruppo Priorit� obsoleto alla data Attuale!", .T.))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_NFGRPRIO = space(5)
        this.w_NUMINI = space(10)
        this.w_NUMFIN = space(10)
        this.w_DTOBGR = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_PRIO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_PRIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NFGRPRIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OPERAT
  func Link_7_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OPERAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_OPERAT);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_OPERAT)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_OPERAT) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oOPERAT_7_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OPERAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_OPERAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_OPERAT)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OPERAT = NVL(_Link_.CODE,0)
      this.w_NAME = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OPERAT = 0
      endif
      this.w_NAME = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OPERAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTINI
  func Link_7_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_lTable = "NOM_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2], .t., this.NOM_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NOM_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NCCODCON like "+cp_ToStrODBC(trim(this.w_CONTINI)+"%");
                   +" and NCCODICE="+cp_ToStrODBC(this.w_NOCODICE);

          i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NC_EMAIL,NC_EMPEC,NCTELFAX";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NCCODICE,NCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NCCODICE',this.w_NOCODICE;
                     ,'NCCODCON',trim(this.w_CONTINI))
          select NCCODICE,NCCODCON,NCPERSON,NC_EMAIL,NC_EMPEC,NCTELFAX;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NCCODICE,NCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONTINI)==trim(_Link_.NCCODCON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONTINI) and !this.bDontReportError
            deferred_cp_zoom('NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(oSource.parent,'oCONTINI_7_15'),i_cWhere,'',"Contatti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_NOCODICE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NC_EMAIL,NC_EMPEC,NCTELFAX";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select NCCODICE,NCCODCON,NCPERSON,NC_EMAIL,NC_EMPEC,NCTELFAX;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NC_EMAIL,NC_EMPEC,NCTELFAX";
                     +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and NCCODICE="+cp_ToStrODBC(this.w_NOCODICE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',oSource.xKey(1);
                       ,'NCCODCON',oSource.xKey(2))
            select NCCODICE,NCCODCON,NCPERSON,NC_EMAIL,NC_EMPEC,NCTELFAX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCCODICE,NCCODCON,NCPERSON,NC_EMAIL,NC_EMPEC,NCTELFAX";
                   +" from "+i_cTable+" "+i_lTable+" where NCCODCON="+cp_ToStrODBC(this.w_CONTINI);
                   +" and NCCODICE="+cp_ToStrODBC(this.w_NOCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCCODICE',this.w_NOCODICE;
                       ,'NCCODCON',this.w_CONTINI)
            select NCCODICE,NCCODCON,NCPERSON,NC_EMAIL,NC_EMPEC,NCTELFAX;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTINI = NVL(_Link_.NCCODCON,space(5))
      this.w_DESCPER = NVL(_Link_.NCPERSON,space(40))
      this.w_APEMAIL = NVL(_Link_.NC_EMAIL,space(254))
      this.w_APEMPEC = NVL(_Link_.NC_EMPEC,space(254))
      this.w_TELFAX = NVL(_Link_.NCTELFAX,space(18))
    else
      if i_cCtrl<>'Load'
        this.w_CONTINI = space(5)
      endif
      this.w_DESCPER = space(40)
      this.w_APEMAIL = space(254)
      this.w_APEMPEC = space(254)
      this.w_TELFAX = space(18)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])+'\'+cp_ToStr(_Link_.NCCODICE,1)+'\'+cp_ToStr(_Link_.NCCODCON,1)
      cp_ShowWarn(i_cKey,this.NOM_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NO__RESP
  func Link_8_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NO__RESP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_NO__RESP)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODUTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_NO__RESP))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODUTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NO__RESP)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_NO__RESP)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODUTE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_NO__RESP)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_NO__RESP)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODUTE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_NO__RESP)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NO__RESP) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oNO__RESP_8_2'),i_cWhere,'GSAR_BDZ',"Responsabili",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODUTE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODUTE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODUTE";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NO__RESP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODUTE";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_NO__RESP);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_NO__RESP)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NO__RESP = NVL(_Link_.DPCODICE,space(5))
      this.w_COMODO = NVL(_Link_.DPCOGNOM,space(10))
      this.w_COMODO = NVL(_Link_.DPNOME,space(10))
      this.w_CODUTE = NVL(_Link_.DPCODUTE,0)
    else
      if i_cCtrl<>'Load'
        this.w_NO__RESP = space(5)
      endif
      this.w_COMODO = space(10)
      this.w_COMODO = space(10)
      this.w_CODUTE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NO__RESP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NONAGIUR
  func Link_8_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAT_GIUR_IDX,3]
    i_lTable = "NAT_GIUR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAT_GIUR_IDX,2], .t., this.NAT_GIUR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAT_GIUR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NONAGIUR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAL_ANG',True,'NAT_GIUR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NGCODICE like "+cp_ToStrODBC(trim(this.w_NONAGIUR)+"%");

          i_ret=cp_SQL(i_nConn,"select NGCODICE,NGDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NGCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NGCODICE',trim(this.w_NONAGIUR))
          select NGCODICE,NGDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NGCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NONAGIUR)==trim(_Link_.NGCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" NGDESCRI like "+cp_ToStrODBC(trim(this.w_NONAGIUR)+"%");

            i_ret=cp_SQL(i_nConn,"select NGCODICE,NGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" NGDESCRI like "+cp_ToStr(trim(this.w_NONAGIUR)+"%");

            select NGCODICE,NGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NONAGIUR) and !this.bDontReportError
            deferred_cp_zoom('NAT_GIUR','*','NGCODICE',cp_AbsName(oSource.parent,'oNONAGIUR_8_11'),i_cWhere,'GSAL_ANG',"Natura giuridica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NGCODICE,NGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NGCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NGCODICE',oSource.xKey(1))
            select NGCODICE,NGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NONAGIUR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NGCODICE,NGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NGCODICE="+cp_ToStrODBC(this.w_NONAGIUR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NGCODICE',this.w_NONAGIUR)
            select NGCODICE,NGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NONAGIUR = NVL(_Link_.NGCODICE,space(4))
      this.w_NGDESCRI = NVL(_Link_.NGDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_NONAGIUR = space(4)
      endif
      this.w_NGDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAT_GIUR_IDX,2])+'\'+cp_ToStr(_Link_.NGCODICE,1)
      cp_ShowWarn(i_cKey,this.NAT_GIUR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NONAGIUR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_8_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAT_GIUR_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAT_GIUR_IDX,2])
      i_cNewSel = i_cSel+ ",link_8_11.NGCODICE as NGCODICE811"+ ",link_8_11.NGDESCRI as NGDESCRI811"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_8_11 on OFF_NOMI.NONAGIUR=link_8_11.NGCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_8_11"
          i_cKey=i_cKey+'+" and OFF_NOMI.NONAGIUR=link_8_11.NGCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NORATING
  func Link_10_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DORATING_IDX,3]
    i_lTable = "DORATING"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2], .t., this.DORATING_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NORATING) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gsdf_Ara',True,'DORATING')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RACODICE like "+cp_ToStrODBC(trim(this.w_NORATING)+"%");

          i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RACODICE',trim(this.w_NORATING))
          select RACODICE,RADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NORATING)==trim(_Link_.RACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NORATING) and !this.bDontReportError
            deferred_cp_zoom('DORATING','*','RACODICE',cp_AbsName(oSource.parent,'oNORATING_10_4'),i_cWhere,'Gsdf_Ara',"Rating",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',oSource.xKey(1))
            select RACODICE,RADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NORATING)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(this.w_NORATING);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',this.w_NORATING)
            select RACODICE,RADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NORATING = NVL(_Link_.RACODICE,space(2))
      this.w_RADESCRI = NVL(_Link_.RADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_NORATING = space(2)
      endif
      this.w_RADESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])+'\'+cp_ToStr(_Link_.RACODICE,1)
      cp_ShowWarn(i_cKey,this.DORATING_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NORATING Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_10_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DORATING_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
      i_cNewSel = i_cSel+ ",link_10_4.RACODICE as RACODICE1004"+ ",link_10_4.RADESCRI as RADESCRI1004"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_10_4 on OFF_NOMI.NORATING=link_10_4.RACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_10_4"
          i_cKey=i_cKey+'+" and OFF_NOMI.NORATING=link_10_4.RACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=NOVOCFIN
  func Link_10_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_FINZ_IDX,3]
    i_lTable = "VOC_FINZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2], .t., this.VOC_FINZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOVOCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDF_AVF',True,'VOC_FINZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DFVOCFIN like "+cp_ToStrODBC(trim(this.w_NOVOCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DFVOCFIN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DFVOCFIN',trim(this.w_NOVOCFIN))
          select DFVOCFIN,DFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DFVOCFIN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOVOCFIN)==trim(_Link_.DFVOCFIN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DFDESCRI like "+cp_ToStrODBC(trim(this.w_NOVOCFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DFDESCRI like "+cp_ToStr(trim(this.w_NOVOCFIN)+"%");

            select DFVOCFIN,DFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOVOCFIN) and !this.bDontReportError
            deferred_cp_zoom('VOC_FINZ','*','DFVOCFIN',cp_AbsName(oSource.parent,'oNOVOCFIN_10_6'),i_cWhere,'GSDF_AVF',"Voci finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',oSource.xKey(1))
            select DFVOCFIN,DFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOVOCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(this.w_NOVOCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',this.w_NOVOCFIN)
            select DFVOCFIN,DFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOVOCFIN = NVL(_Link_.DFVOCFIN,space(6))
      this.w_DFDESCRI = NVL(_Link_.DFDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_NOVOCFIN = space(6)
      endif
      this.w_DFDESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])+'\'+cp_ToStr(_Link_.DFVOCFIN,1)
      cp_ShowWarn(i_cKey,this.VOC_FINZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOVOCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_10_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_FINZ_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_10_6.DFVOCFIN as DFVOCFIN1006"+ ",link_10_6.DFDESCRI as DFDESCRI1006"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_10_6 on OFF_NOMI.NOVOCFIN=link_10_6.DFVOCFIN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_10_6"
          i_cKey=i_cKey+'+" and OFF_NOMI.NOVOCFIN=link_10_6.DFVOCFIN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNOCODICE_1_3.value==this.w_NOCODICE)
      this.oPgFrm.Page1.oPag.oNOCODICE_1_3.value=this.w_NOCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCODCLI_1_4.value==this.w_NOCODCLI)
      this.oPgFrm.Page1.oPag.oNOCODCLI_1_4.value=this.w_NOCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oNODESCRI_1_8.value==this.w_NODESCRI)
      this.oPgFrm.Page1.oPag.oNODESCRI_1_8.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oNODESCR2_1_9.value==this.w_NODESCR2)
      this.oPgFrm.Page1.oPag.oNODESCR2_1_9.value=this.w_NODESCR2
    endif
    if not(this.oPgFrm.Page1.oPag.oNOSOGGET_1_10.RadioValue()==this.w_NOSOGGET)
      this.oPgFrm.Page1.oPag.oNOSOGGET_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCOD_RU_1_12.value==this.w_NOCOD_RU)
      this.oPgFrm.Page1.oPag.oNOCOD_RU_1_12.value=this.w_NOCOD_RU
    endif
    if not(this.oPgFrm.Page1.oPag.oNOBADGE_1_13.value==this.w_NOBADGE)
      this.oPgFrm.Page1.oPag.oNOBADGE_1_13.value=this.w_NOBADGE
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCOGNOM_1_14.value==this.w_NOCOGNOM)
      this.oPgFrm.Page1.oPag.oNOCOGNOM_1_14.value=this.w_NOCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCOGNOM_1_15.value==this.w_NOCOGNOM)
      this.oPgFrm.Page1.oPag.oNOCOGNOM_1_15.value=this.w_NOCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oNO__NOME_1_16.value==this.w_NO__NOME)
      this.oPgFrm.Page1.oPag.oNO__NOME_1_16.value=this.w_NO__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oNO__NOME_1_17.value==this.w_NO__NOME)
      this.oPgFrm.Page1.oPag.oNO__NOME_1_17.value=this.w_NO__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oNOLOCNAS_1_18.value==this.w_NOLOCNAS)
      this.oPgFrm.Page1.oPag.oNOLOCNAS_1_18.value=this.w_NOLOCNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oNOLOCNAS_1_19.value==this.w_NOLOCNAS)
      this.oPgFrm.Page1.oPag.oNOLOCNAS_1_19.value=this.w_NOLOCNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oNOPRONAS_1_20.value==this.w_NOPRONAS)
      this.oPgFrm.Page1.oPag.oNOPRONAS_1_20.value=this.w_NOPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oNOPRONAS_1_21.value==this.w_NOPRONAS)
      this.oPgFrm.Page1.oPag.oNOPRONAS_1_21.value=this.w_NOPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oNODATNAS_1_22.value==this.w_NODATNAS)
      this.oPgFrm.Page1.oPag.oNODATNAS_1_22.value=this.w_NODATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oNO_SESSO_1_23.RadioValue()==this.w_NO_SESSO)
      this.oPgFrm.Page1.oPag.oNO_SESSO_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNONAZNAS_1_24.value==this.w_NONAZNAS)
      this.oPgFrm.Page1.oPag.oNONAZNAS_1_24.value=this.w_NONAZNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oNONUMCAR_1_25.value==this.w_NONUMCAR)
      this.oPgFrm.Page1.oPag.oNONUMCAR_1_25.value=this.w_NONUMCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCODFIS_1_26.value==this.w_NOCODFIS)
      this.oPgFrm.Page1.oPag.oNOCODFIS_1_26.value=this.w_NOCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oNOPARIVA_1_27.value==this.w_NOPARIVA)
      this.oPgFrm.Page1.oPag.oNOPARIVA_1_27.value=this.w_NOPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oNONATGIU_1_28.RadioValue()==this.w_NONATGIU)
      this.oPgFrm.Page1.oPag.oNONATGIU_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCODSAL_1_29.RadioValue()==this.w_NOCODSAL)
      this.oPgFrm.Page1.oPag.oNOCODSAL_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOINDIRI_1_30.value==this.w_NOINDIRI)
      this.oPgFrm.Page1.oPag.oNOINDIRI_1_30.value=this.w_NOINDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oNOINDI_2_1_31.value==this.w_NOINDI_2)
      this.oPgFrm.Page1.oPag.oNOINDI_2_1_31.value=this.w_NOINDI_2
    endif
    if not(this.oPgFrm.Page1.oPag.oNO___CAP_1_33.value==this.w_NO___CAP)
      this.oPgFrm.Page1.oPag.oNO___CAP_1_33.value=this.w_NO___CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oNO___CAP_1_34.value==this.w_NO___CAP)
      this.oPgFrm.Page1.oPag.oNO___CAP_1_34.value=this.w_NO___CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oNOLOCALI_1_35.value==this.w_NOLOCALI)
      this.oPgFrm.Page1.oPag.oNOLOCALI_1_35.value=this.w_NOLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oNOLOCALI_1_36.value==this.w_NOLOCALI)
      this.oPgFrm.Page1.oPag.oNOLOCALI_1_36.value=this.w_NOLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oNOPROVIN_1_37.value==this.w_NOPROVIN)
      this.oPgFrm.Page1.oPag.oNOPROVIN_1_37.value=this.w_NOPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNOPROVIN_1_38.value==this.w_NOPROVIN)
      this.oPgFrm.Page1.oPag.oNOPROVIN_1_38.value=this.w_NOPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNONAZION_1_39.value==this.w_NONAZION)
      this.oPgFrm.Page1.oPag.oNONAZION_1_39.value=this.w_NONAZION
    endif
    if not(this.oPgFrm.Page1.oPag.oNOINDIR2_1_40.value==this.w_NOINDIR2)
      this.oPgFrm.Page1.oPag.oNOINDIR2_1_40.value=this.w_NOINDIR2
    endif
    if not(this.oPgFrm.Page1.oPag.oNO___CAD_1_41.value==this.w_NO___CAD)
      this.oPgFrm.Page1.oPag.oNO___CAD_1_41.value=this.w_NO___CAD
    endif
    if not(this.oPgFrm.Page1.oPag.oNO___CAD_1_42.value==this.w_NO___CAD)
      this.oPgFrm.Page1.oPag.oNO___CAD_1_42.value=this.w_NO___CAD
    endif
    if not(this.oPgFrm.Page1.oPag.oNOLOCALD_1_43.value==this.w_NOLOCALD)
      this.oPgFrm.Page1.oPag.oNOLOCALD_1_43.value=this.w_NOLOCALD
    endif
    if not(this.oPgFrm.Page1.oPag.oNOLOCALD_1_47.value==this.w_NOLOCALD)
      this.oPgFrm.Page1.oPag.oNOLOCALD_1_47.value=this.w_NOLOCALD
    endif
    if not(this.oPgFrm.Page1.oPag.oNOPROVID_1_48.value==this.w_NOPROVID)
      this.oPgFrm.Page1.oPag.oNOPROVID_1_48.value=this.w_NOPROVID
    endif
    if not(this.oPgFrm.Page1.oPag.oNOPROVID_1_49.value==this.w_NOPROVID)
      this.oPgFrm.Page1.oPag.oNOPROVID_1_49.value=this.w_NOPROVID
    endif
    if not(this.oPgFrm.Page1.oPag.oNONAZIOD_1_50.value==this.w_NONAZIOD)
      this.oPgFrm.Page1.oPag.oNONAZIOD_1_50.value=this.w_NONAZIOD
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTELEFO_1_51.value==this.w_NOTELEFO)
      this.oPgFrm.Page1.oPag.oNOTELEFO_1_51.value=this.w_NOTELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oNONUMCEL_1_52.value==this.w_NONUMCEL)
      this.oPgFrm.Page1.oPag.oNONUMCEL_1_52.value=this.w_NONUMCEL
    endif
    if not(this.oPgFrm.Page1.oPag.oNO_SKYPE_1_53.value==this.w_NO_SKYPE)
      this.oPgFrm.Page1.oPag.oNO_SKYPE_1_53.value=this.w_NO_SKYPE
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTELFAX_1_54.value==this.w_NOTELFAX)
      this.oPgFrm.Page1.oPag.oNOTELFAX_1_54.value=this.w_NOTELFAX
    endif
    if not(this.oPgFrm.Page1.oPag.oNO_EMAIL_1_55.value==this.w_NO_EMAIL)
      this.oPgFrm.Page1.oPag.oNO_EMAIL_1_55.value=this.w_NO_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oNOINDWEB_1_56.value==this.w_NOINDWEB)
      this.oPgFrm.Page1.oPag.oNOINDWEB_1_56.value=this.w_NOINDWEB
    endif
    if not(this.oPgFrm.Page1.oPag.oNO_EMPEC_1_57.value==this.w_NO_EMPEC)
      this.oPgFrm.Page1.oPag.oNO_EMPEC_1_57.value=this.w_NO_EMPEC
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCHKSTA_1_58.RadioValue()==this.w_NOCHKSTA)
      this.oPgFrm.Page1.oPag.oNOCHKSTA_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCHKMAI_1_59.RadioValue()==this.w_NOCHKMAI)
      this.oPgFrm.Page1.oPag.oNOCHKMAI_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCHKPEC_1_60.RadioValue()==this.w_NOCHKPEC)
      this.oPgFrm.Page1.oPag.oNOCHKPEC_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCHKFAX_1_61.RadioValue()==this.w_NOCHKFAX)
      this.oPgFrm.Page1.oPag.oNOCHKFAX_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCHKCPZ_1_62.RadioValue()==this.w_NOCHKCPZ)
      this.oPgFrm.Page1.oPag.oNOCHKCPZ_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNODTINVA_1_66.value==this.w_NODTINVA)
      this.oPgFrm.Page1.oPag.oNODTINVA_1_66.value=this.w_NODTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oNODTOBSO_1_67.value==this.w_NODTOBSO)
      this.oPgFrm.Page1.oPag.oNODTOBSO_1_67.value=this.w_NODTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oNOSOGGET_1_69.RadioValue()==this.w_NOSOGGET)
      this.oPgFrm.Page1.oPag.oNOSOGGET_1_69.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTIPPER_1_72.RadioValue()==this.w_NOTIPPER)
      this.oPgFrm.Page1.oPag.oNOTIPPER_1_72.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTIPNOM_1_74.RadioValue()==this.w_NOTIPNOM)
      this.oPgFrm.Page1.oPag.oNOTIPNOM_1_74.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTIPNOM_1_75.RadioValue()==this.w_NOTIPNOM)
      this.oPgFrm.Page1.oPag.oNOTIPNOM_1_75.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTIPNOM_1_76.RadioValue()==this.w_NOTIPNOM)
      this.oPgFrm.Page1.oPag.oNOTIPNOM_1_76.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNAZ1_1_126.value==this.w_DESNAZ1)
      this.oPgFrm.Page1.oPag.oDESNAZ1_1_126.value=this.w_DESNAZ1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODISO1_1_127.value==this.w_CODISO1)
      this.oPgFrm.Page1.oPag.oCODISO1_1_127.value=this.w_CODISO1
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTIPCON_2_2.RadioValue()==this.w_NOTIPCON)
      this.oPgFrm.Page2.oPag.oNOTIPCON_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCODCON_2_3.value==this.w_NOCODCON)
      this.oPgFrm.Page2.oPag.oNOCODCON_2_3.value=this.w_NOCODCON
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCODPAG_2_4.value==this.w_NOCODPAG)
      this.oPgFrm.Page2.oPag.oNOCODPAG_2_4.value=this.w_NOCODPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPAG_2_5.value==this.w_DESPAG)
      this.oPgFrm.Page2.oPag.oDESPAG_2_5.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oNOGIOFIS_2_6.value==this.w_NOGIOFIS)
      this.oPgFrm.Page2.oPag.oNOGIOFIS_2_6.value=this.w_NOGIOFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oNO1MESCL_2_7.value==this.w_NO1MESCL)
      this.oPgFrm.Page2.oPag.oNO1MESCL_2_7.value=this.w_NO1MESCL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMES1_2_8.value==this.w_DESMES1)
      this.oPgFrm.Page2.oPag.oDESMES1_2_8.value=this.w_DESMES1
    endif
    if not(this.oPgFrm.Page2.oPag.oNOGIOSC1_2_9.value==this.w_NOGIOSC1)
      this.oPgFrm.Page2.oPag.oNOGIOSC1_2_9.value=this.w_NOGIOSC1
    endif
    if not(this.oPgFrm.Page2.oPag.oNO2MESCL_2_10.value==this.w_NO2MESCL)
      this.oPgFrm.Page2.oPag.oNO2MESCL_2_10.value=this.w_NO2MESCL
    endif
    if not(this.oPgFrm.Page2.oPag.oNOGIOSC2_2_11.value==this.w_NOGIOSC2)
      this.oPgFrm.Page2.oPag.oNOGIOSC2_2_11.value=this.w_NOGIOSC2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMES2_2_12.value==this.w_DESMES2)
      this.oPgFrm.Page2.oPag.oDESMES2_2_12.value=this.w_DESMES2
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCODBAN_2_13.value==this.w_NOCODBAN)
      this.oPgFrm.Page2.oPag.oNOCODBAN_2_13.value=this.w_NOCODBAN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESBA2_2_29.value==this.w_DESBA2)
      this.oPgFrm.Page2.oPag.oDESBA2_2_29.value=this.w_DESBA2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESBAN_2_34.value==this.w_DESBAN)
      this.oPgFrm.Page2.oPag.oDESBAN_2_34.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCODICE_2_35.value==this.w_NOCODICE)
      this.oPgFrm.Page2.oPag.oNOCODICE_2_35.value=this.w_NOCODICE
    endif
    if not(this.oPgFrm.Page2.oPag.oANDESCRI_2_36.value==this.w_ANDESCRI)
      this.oPgFrm.Page2.oPag.oANDESCRI_2_36.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTIPPER_2_38.RadioValue()==this.w_NOTIPPER)
      this.oPgFrm.Page2.oPag.oNOTIPPER_2_38.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oBANCAAHE_2_43.value==this.w_BANCAAHE)
      this.oPgFrm.Page2.oPag.oBANCAAHE_2_43.value=this.w_BANCAAHE
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCATFIN_2_44.value==this.w_NOCATFIN)
      this.oPgFrm.Page2.oPag.oNOCATFIN_2_44.value=this.w_NOCATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCATDER_2_45.value==this.w_NOCATDER)
      this.oPgFrm.Page2.oPag.oNOCATDER_2_45.value=this.w_NOCATDER
    endif
    if not(this.oPgFrm.Page2.oPag.oNOIDRIDY_2_46.RadioValue()==this.w_NOIDRIDY)
      this.oPgFrm.Page2.oPag.oNOIDRIDY_2_46.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNOTIIDRI_2_47.RadioValue()==this.w_NOTIIDRI)
      this.oPgFrm.Page2.oPag.oNOTIIDRI_2_47.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCOIDRI_2_48.value==this.w_NOCOIDRI)
      this.oPgFrm.Page2.oPag.oNOCOIDRI_2_48.value=this.w_NOCOIDRI
    endif
    if not(this.oPgFrm.Page2.oPag.oNOCOGNOM_2_59.value==this.w_NOCOGNOM)
      this.oPgFrm.Page2.oPag.oNOCOGNOM_2_59.value=this.w_NOCOGNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oNO__NOME_2_60.value==this.w_NO__NOME)
      this.oPgFrm.Page2.oPag.oNO__NOME_2_60.value=this.w_NO__NOME
    endif
    if not(this.oPgFrm.Page2.oPag.oNODESCRI_2_63.value==this.w_NODESCRI)
      this.oPgFrm.Page2.oPag.oNODESCRI_2_63.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oNODESCR2_2_64.value==this.w_NODESCR2)
      this.oPgFrm.Page2.oPag.oNODESCR2_2_64.value=this.w_NODESCR2
    endif
    if not(this.oPgFrm.Page2.oPag.oCFDESCRIC_2_72.value==this.w_CFDESCRIC)
      this.oPgFrm.Page2.oPag.oCFDESCRIC_2_72.value=this.w_CFDESCRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oCFDESCRID_2_73.value==this.w_CFDESCRID)
      this.oPgFrm.Page2.oPag.oCFDESCRID_2_73.value=this.w_CFDESCRID
    endif
    if not(this.oPgFrm.Page3.oPag.oNONOMFIL_3_1.value==this.w_NONOMFIL)
      this.oPgFrm.Page3.oPag.oNONOMFIL_3_1.value=this.w_NONOMFIL
    endif
    if not(this.oPgFrm.Page3.oPag.oNOOGGE_3_2.value==this.w_NOOGGE)
      this.oPgFrm.Page3.oPag.oNOOGGE_3_2.value=this.w_NOOGGE
    endif
    if not(this.oPgFrm.Page3.oPag.oNONOTE_3_3.value==this.w_NONOTE)
      this.oPgFrm.Page3.oPag.oNONOTE_3_3.value=this.w_NONOTE
    endif
    if not(this.oPgFrm.Page3.oPag.oCODI_3_15.value==this.w_CODI)
      this.oPgFrm.Page3.oPag.oCODI_3_15.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page3.oPag.oNOTIPPER_3_18.RadioValue()==this.w_NOTIPPER)
      this.oPgFrm.Page3.oPag.oNOTIPPER_3_18.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oNOCOGNOM_3_20.value==this.w_NOCOGNOM)
      this.oPgFrm.Page3.oPag.oNOCOGNOM_3_20.value=this.w_NOCOGNOM
    endif
    if not(this.oPgFrm.Page3.oPag.oNO__NOME_3_21.value==this.w_NO__NOME)
      this.oPgFrm.Page3.oPag.oNO__NOME_3_21.value=this.w_NO__NOME
    endif
    if not(this.oPgFrm.Page3.oPag.oNODESCRI_3_24.value==this.w_NODESCRI)
      this.oPgFrm.Page3.oPag.oNODESCRI_3_24.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oNODESCR2_3_25.value==this.w_NODESCR2)
      this.oPgFrm.Page3.oPag.oNODESCR2_3_25.value=this.w_NODESCR2
    endif
    if not(this.oPgFrm.Page4.oPag.oCODI_4_1.value==this.w_CODI)
      this.oPgFrm.Page4.oPag.oCODI_4_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCODPRI_4_2.value==this.w_NOCODPRI)
      this.oPgFrm.Page4.oPag.oNOCODPRI_4_2.value=this.w_NOCODPRI
    endif
    if not(this.oPgFrm.Page4.oPag.oNOPRIVCY_4_3.RadioValue()==this.w_NOPRIVCY)
      this.oPgFrm.Page4.oPag.oNOPRIVCY_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCODGRU_4_6.value==this.w_NOCODGRU)
      this.oPgFrm.Page4.oPag.oNOCODGRU_4_6.value=this.w_NOCODGRU
    endif
    if not(this.oPgFrm.Page4.oPag.oNODESGRU_4_7.value==this.w_NODESGRU)
      this.oPgFrm.Page4.oPag.oNODESGRU_4_7.value=this.w_NODESGRU
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCODORI_4_9.value==this.w_NOCODORI)
      this.oPgFrm.Page4.oPag.oNOCODORI_4_9.value=this.w_NOCODORI
    endif
    if not(this.oPgFrm.Page4.oPag.oNODESORI_4_10.value==this.w_NODESORI)
      this.oPgFrm.Page4.oPag.oNODESORI_4_10.value=this.w_NODESORI
    endif
    if not(this.oPgFrm.Page4.oPag.oNORIFINT_4_12.value==this.w_NORIFINT)
      this.oPgFrm.Page4.oPag.oNORIFINT_4_12.value=this.w_NORIFINT
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCODOPE_4_13.value==this.w_NOCODOPE)
      this.oPgFrm.Page4.oPag.oNOCODOPE_4_13.value=this.w_NOCODOPE
    endif
    if not(this.oPgFrm.Page4.oPag.oNODESOPE_4_14.value==this.w_NODESOPE)
      this.oPgFrm.Page4.oPag.oNODESOPE_4_14.value=this.w_NODESOPE
    endif
    if not(this.oPgFrm.Page4.oPag.oNOASSOPE_4_15.value==this.w_NOASSOPE)
      this.oPgFrm.Page4.oPag.oNOASSOPE_4_15.value=this.w_NOASSOPE
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCODZON_4_17.value==this.w_NOCODZON)
      this.oPgFrm.Page4.oPag.oNOCODZON_4_17.value=this.w_NOCODZON
    endif
    if not(this.oPgFrm.Page4.oPag.oNODESZON_4_18.value==this.w_NODESZON)
      this.oPgFrm.Page4.oPag.oNODESZON_4_18.value=this.w_NODESZON
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCODLIN_4_20.value==this.w_NOCODLIN)
      this.oPgFrm.Page4.oPag.oNOCODLIN_4_20.value=this.w_NOCODLIN
    endif
    if not(this.oPgFrm.Page4.oPag.oNODESLIN_4_21.value==this.w_NODESLIN)
      this.oPgFrm.Page4.oPag.oNODESLIN_4_21.value=this.w_NODESLIN
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCODVAL_4_23.value==this.w_NOCODVAL)
      this.oPgFrm.Page4.oPag.oNOCODVAL_4_23.value=this.w_NOCODVAL
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCODAGE_4_24.value==this.w_NOCODAGE)
      this.oPgFrm.Page4.oPag.oNOCODAGE_4_24.value=this.w_NOCODAGE
    endif
    if not(this.oPgFrm.Page4.oPag.oNODESVAL_4_25.value==this.w_NODESVAL)
      this.oPgFrm.Page4.oPag.oNODESVAL_4_25.value=this.w_NODESVAL
    endif
    if not(this.oPgFrm.Page4.oPag.oNODESAGE_4_34.value==this.w_NODESAGE)
      this.oPgFrm.Page4.oPag.oNODESAGE_4_34.value=this.w_NODESAGE
    endif
    if not(this.oPgFrm.Page4.oPag.oNOTIPPER_4_35.RadioValue()==this.w_NOTIPPER)
      this.oPgFrm.Page4.oPag.oNOTIPPER_4_35.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCOGNOM_4_37.value==this.w_NOCOGNOM)
      this.oPgFrm.Page4.oPag.oNOCOGNOM_4_37.value=this.w_NOCOGNOM
    endif
    if not(this.oPgFrm.Page4.oPag.oNO__NOME_4_38.value==this.w_NO__NOME)
      this.oPgFrm.Page4.oPag.oNO__NOME_4_38.value=this.w_NO__NOME
    endif
    if not(this.oPgFrm.Page4.oPag.oNODESCRI_4_41.value==this.w_NODESCRI)
      this.oPgFrm.Page4.oPag.oNODESCRI_4_41.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page4.oPag.oNODESCR2_4_42.value==this.w_NODESCR2)
      this.oPgFrm.Page4.oPag.oNODESCR2_4_42.value=this.w_NODESCR2
    endif
    if not(this.oPgFrm.Page4.oPag.oNONUMLIS_4_44.value==this.w_NONUMLIS)
      this.oPgFrm.Page4.oPag.oNONUMLIS_4_44.value=this.w_NONUMLIS
    endif
    if not(this.oPgFrm.Page4.oPag.oNONUMLIS_4_45.value==this.w_NONUMLIS)
      this.oPgFrm.Page4.oPag.oNONUMLIS_4_45.value=this.w_NONUMLIS
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCODPAG_4_46.value==this.w_NOCODPAG)
      this.oPgFrm.Page4.oPag.oNOCODPAG_4_46.value=this.w_NOCODPAG
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCODBAN_4_47.value==this.w_NOCODBAN)
      this.oPgFrm.Page4.oPag.oNOCODBAN_4_47.value=this.w_NOCODBAN
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCATCOM_4_48.value==this.w_NOCATCOM)
      this.oPgFrm.Page4.oPag.oNOCATCOM_4_48.value=this.w_NOCATCOM
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCATCON_4_49.value==this.w_NOCATCON)
      this.oPgFrm.Page4.oPag.oNOCATCON_4_49.value=this.w_NOCATCON
    endif
    if not(this.oPgFrm.Page4.oPag.oNOCACLFO_4_50.value==this.w_NOCACLFO)
      this.oPgFrm.Page4.oPag.oNOCACLFO_4_50.value=this.w_NOCACLFO
    endif
    if not(this.oPgFrm.Page4.oPag.oBANCAAHR_4_51.value==this.w_BANCAAHR)
      this.oPgFrm.Page4.oPag.oBANCAAHR_4_51.value=this.w_BANCAAHR
    endif
    if not(this.oPgFrm.Page4.oPag.oBANCAAHE_4_52.value==this.w_BANCAAHE)
      this.oPgFrm.Page4.oPag.oBANCAAHE_4_52.value=this.w_BANCAAHE
    endif
    if not(this.oPgFrm.Page4.oPag.oDESBA3_4_53.value==this.w_DESBA3)
      this.oPgFrm.Page4.oPag.oDESBA3_4_53.value=this.w_DESBA3
    endif
    if not(this.oPgFrm.Page4.oPag.oDESBA2_4_54.value==this.w_DESBA2)
      this.oPgFrm.Page4.oPag.oDESBA2_4_54.value=this.w_DESBA2
    endif
    if not(this.oPgFrm.Page4.oPag.oDESBAN_4_56.value==this.w_DESBAN)
      this.oPgFrm.Page4.oPag.oDESBAN_4_56.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page4.oPag.oDESPAG_4_59.value==this.w_DESPAG)
      this.oPgFrm.Page4.oPag.oDESPAG_4_59.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCAC_4_61.value==this.w_DESCAC)
      this.oPgFrm.Page4.oPag.oDESCAC_4_61.value=this.w_DESCAC
    endif
    if not(this.oPgFrm.Page4.oPag.oDESLIS_4_63.value==this.w_DESLIS)
      this.oPgFrm.Page4.oPag.oDESLIS_4_63.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page4.oPag.oNODENOMRIF_4_67.value==this.w_NODENOMRIF)
      this.oPgFrm.Page4.oPag.oNODENOMRIF_4_67.value=this.w_NODENOMRIF
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCON_4_69.value==this.w_DESCON)
      this.oPgFrm.Page4.oPag.oDESCON_4_69.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page4.oPag.oDESSCATCLFO_4_71.value==this.w_DESSCATCLFO)
      this.oPgFrm.Page4.oPag.oDESSCATCLFO_4_71.value=this.w_DESSCATCLFO
    endif
    if not(this.oPgFrm.Page5.oPag.oCODI_5_2.value==this.w_CODI)
      this.oPgFrm.Page5.oPag.oCODI_5_2.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page5.oPag.oNOTIPPER_5_4.RadioValue()==this.w_NOTIPPER)
      this.oPgFrm.Page5.oPag.oNOTIPPER_5_4.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oNOCOGNOM_5_6.value==this.w_NOCOGNOM)
      this.oPgFrm.Page5.oPag.oNOCOGNOM_5_6.value=this.w_NOCOGNOM
    endif
    if not(this.oPgFrm.Page5.oPag.oNO__NOME_5_7.value==this.w_NO__NOME)
      this.oPgFrm.Page5.oPag.oNO__NOME_5_7.value=this.w_NO__NOME
    endif
    if not(this.oPgFrm.Page5.oPag.oNODESCRI_5_10.value==this.w_NODESCRI)
      this.oPgFrm.Page5.oPag.oNODESCRI_5_10.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page5.oPag.oNODESCR2_5_11.value==this.w_NODESCR2)
      this.oPgFrm.Page5.oPag.oNODESCR2_5_11.value=this.w_NODESCR2
    endif
    if not(this.oPgFrm.Page6.oPag.oOFSTATUS_6_1.RadioValue()==this.w_OFSTATUS)
      this.oPgFrm.Page6.oPag.oOFSTATUS_6_1.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oNFDTOF1_6_9.value==this.w_NFDTOF1)
      this.oPgFrm.Page6.oPag.oNFDTOF1_6_9.value=this.w_NFDTOF1
    endif
    if not(this.oPgFrm.Page6.oPag.oNFDTOF2_6_10.value==this.w_NFDTOF2)
      this.oPgFrm.Page6.oPag.oNFDTOF2_6_10.value=this.w_NFDTOF2
    endif
    if not(this.oPgFrm.Page6.oPag.oNFDTRC1_6_11.value==this.w_NFDTRC1)
      this.oPgFrm.Page6.oPag.oNFDTRC1_6_11.value=this.w_NFDTRC1
    endif
    if not(this.oPgFrm.Page6.oPag.oNFDTRC2_6_12.value==this.w_NFDTRC2)
      this.oPgFrm.Page6.oPag.oNFDTRC2_6_12.value=this.w_NFDTRC2
    endif
    if not(this.oPgFrm.Page6.oPag.oNFGRPRIO_6_17.value==this.w_NFGRPRIO)
      this.oPgFrm.Page6.oPag.oNFGRPRIO_6_17.value=this.w_NFGRPRIO
    endif
    if not(this.oPgFrm.Page6.oPag.oNFRIFDE_6_18.value==this.w_NFRIFDE)
      this.oPgFrm.Page6.oPag.oNFRIFDE_6_18.value=this.w_NFRIFDE
    endif
    if not(this.oPgFrm.Page6.oPag.oCODI_6_26.value==this.w_CODI)
      this.oPgFrm.Page6.oPag.oCODI_6_26.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page6.oPag.oNOTIPPER_6_34.RadioValue()==this.w_NOTIPPER)
      this.oPgFrm.Page6.oPag.oNOTIPPER_6_34.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oNOCOGNOM_6_36.value==this.w_NOCOGNOM)
      this.oPgFrm.Page6.oPag.oNOCOGNOM_6_36.value=this.w_NOCOGNOM
    endif
    if not(this.oPgFrm.Page6.oPag.oNO__NOME_6_37.value==this.w_NO__NOME)
      this.oPgFrm.Page6.oPag.oNO__NOME_6_37.value=this.w_NO__NOME
    endif
    if not(this.oPgFrm.Page6.oPag.oNODESCRI_6_40.value==this.w_NODESCRI)
      this.oPgFrm.Page6.oPag.oNODESCRI_6_40.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page6.oPag.oNODESCR2_6_41.value==this.w_NODESCR2)
      this.oPgFrm.Page6.oPag.oNODESCR2_6_41.value=this.w_NODESCR2
    endif
    if not(this.oPgFrm.Page7.oPag.oOPERAT_7_2.value==this.w_OPERAT)
      this.oPgFrm.Page7.oPag.oOPERAT_7_2.value=this.w_OPERAT
    endif
    if not(this.oPgFrm.Page7.oPag.oSTATO_7_3.RadioValue()==this.w_STATO)
      this.oPgFrm.Page7.oPag.oSTATO_7_3.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oNAME_7_4.value==this.w_NAME)
      this.oPgFrm.Page7.oPag.oNAME_7_4.value=this.w_NAME
    endif
    if not(this.oPgFrm.Page7.oPag.oSTATO_7_6.RadioValue()==this.w_STATO)
      this.oPgFrm.Page7.oPag.oSTATO_7_6.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oDATAINI_7_7.value==this.w_DATAINI)
      this.oPgFrm.Page7.oPag.oDATAINI_7_7.value=this.w_DATAINI
    endif
    if not(this.oPgFrm.Page7.oPag.oORAINI_7_8.value==this.w_ORAINI)
      this.oPgFrm.Page7.oPag.oORAINI_7_8.value=this.w_ORAINI
    endif
    if not(this.oPgFrm.Page7.oPag.oMININI_7_9.value==this.w_MININI)
      this.oPgFrm.Page7.oPag.oMININI_7_9.value=this.w_MININI
    endif
    if not(this.oPgFrm.Page7.oPag.oDATAFIN_7_10.value==this.w_DATAFIN)
      this.oPgFrm.Page7.oPag.oDATAFIN_7_10.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page7.oPag.oORAFIN_7_11.value==this.w_ORAFIN)
      this.oPgFrm.Page7.oPag.oORAFIN_7_11.value=this.w_ORAFIN
    endif
    if not(this.oPgFrm.Page7.oPag.oMINFIN_7_12.value==this.w_MINFIN)
      this.oPgFrm.Page7.oPag.oMINFIN_7_12.value=this.w_MINFIN
    endif
    if not(this.oPgFrm.Page7.oPag.oSUBJECT_7_14.value==this.w_SUBJECT)
      this.oPgFrm.Page7.oPag.oSUBJECT_7_14.value=this.w_SUBJECT
    endif
    if not(this.oPgFrm.Page7.oPag.oCONTINI_7_15.value==this.w_CONTINI)
      this.oPgFrm.Page7.oPag.oCONTINI_7_15.value=this.w_CONTINI
    endif
    if not(this.oPgFrm.Page7.oPag.oPRIOINI_7_16.value==this.w_PRIOINI)
      this.oPgFrm.Page7.oPag.oPRIOINI_7_16.value=this.w_PRIOINI
    endif
    if not(this.oPgFrm.Page7.oPag.oPRIOINI_7_17.RadioValue()==this.w_PRIOINI)
      this.oPgFrm.Page7.oPag.oPRIOINI_7_17.SetRadio()
    endif
    if not(this.oPgFrm.Page7.oPag.oDESCPER_7_44.value==this.w_DESCPER)
      this.oPgFrm.Page7.oPag.oDESCPER_7_44.value=this.w_DESCPER
    endif
    if not(this.oPgFrm.Page8.oPag.oNODATINS_8_1.value==this.w_NODATINS)
      this.oPgFrm.Page8.oPag.oNODATINS_8_1.value=this.w_NODATINS
    endif
    if not(this.oPgFrm.Page8.oPag.oNO__RESP_8_2.value==this.w_NO__RESP)
      this.oPgFrm.Page8.oPag.oNO__RESP_8_2.value=this.w_NO__RESP
    endif
    if not(this.oPgFrm.Page8.oPag.oDATI_8_4.value==this.w_DATI)
      this.oPgFrm.Page8.oPag.oDATI_8_4.value=this.w_DATI
    endif
    if not(this.oPgFrm.Page8.oPag.oNOTIPOID_8_5.RadioValue()==this.w_NOTIPOID)
      this.oPgFrm.Page8.oPag.oNOTIPOID_8_5.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oNOTIPDOC_8_6.RadioValue()==this.w_NOTIPDOC)
      this.oPgFrm.Page8.oPag.oNOTIPDOC_8_6.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oNONUMDOC_8_7.value==this.w_NONUMDOC)
      this.oPgFrm.Page8.oPag.oNONUMDOC_8_7.value=this.w_NONUMDOC
    endif
    if not(this.oPgFrm.Page8.oPag.oNODATRIL_8_8.value==this.w_NODATRIL)
      this.oPgFrm.Page8.oPag.oNODATRIL_8_8.value=this.w_NODATRIL
    endif
    if not(this.oPgFrm.Page8.oPag.oNOAUTLOC_8_9.value==this.w_NOAUTLOC)
      this.oPgFrm.Page8.oPag.oNOAUTLOC_8_9.value=this.w_NOAUTLOC
    endif
    if not(this.oPgFrm.Page8.oPag.oNOATTLAV_8_10.value==this.w_NOATTLAV)
      this.oPgFrm.Page8.oPag.oNOATTLAV_8_10.value=this.w_NOATTLAV
    endif
    if not(this.oPgFrm.Page8.oPag.oNONAGIUR_8_11.value==this.w_NONAGIUR)
      this.oPgFrm.Page8.oPag.oNONAGIUR_8_11.value=this.w_NONAGIUR
    endif
    if not(this.oPgFrm.Page8.oPag.oNGDESCRI_8_12.value==this.w_NGDESCRI)
      this.oPgFrm.Page8.oPag.oNGDESCRI_8_12.value=this.w_NGDESCRI
    endif
    if not(this.oPgFrm.Page8.oPag.oNODATCOS_8_13.value==this.w_NODATCOS)
      this.oPgFrm.Page8.oPag.oNODATCOS_8_13.value=this.w_NODATCOS
    endif
    if not(this.oPgFrm.Page8.oPag.oNO_SIGLA_8_14.value==this.w_NO_SIGLA)
      this.oPgFrm.Page8.oPag.oNO_SIGLA_8_14.value=this.w_NO_SIGLA
    endif
    if not(this.oPgFrm.Page8.oPag.oNO_PAESE_8_15.value==this.w_NO_PAESE)
      this.oPgFrm.Page8.oPag.oNO_PAESE_8_15.value=this.w_NO_PAESE
    endif
    if not(this.oPgFrm.Page8.oPag.oNOESTNAS_8_16.value==this.w_NOESTNAS)
      this.oPgFrm.Page8.oPag.oNOESTNAS_8_16.value=this.w_NOESTNAS
    endif
    if not(this.oPgFrm.Page8.oPag.oNOFLANTI_8_19.RadioValue()==this.w_NOFLANTI)
      this.oPgFrm.Page8.oPag.oNOFLANTI_8_19.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oNOFLANTI_8_20.RadioValue()==this.w_NOFLANTI)
      this.oPgFrm.Page8.oPag.oNOFLANTI_8_20.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oNOCODICE_8_47.value==this.w_NOCODICE)
      this.oPgFrm.Page8.oPag.oNOCODICE_8_47.value=this.w_NOCODICE
    endif
    if not(this.oPgFrm.Page8.oPag.oNOCOGNOM_8_48.value==this.w_NOCOGNOM)
      this.oPgFrm.Page8.oPag.oNOCOGNOM_8_48.value=this.w_NOCOGNOM
    endif
    if not(this.oPgFrm.Page8.oPag.oNODESCRI_8_51.value==this.w_NODESCRI)
      this.oPgFrm.Page8.oPag.oNODESCRI_8_51.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page8.oPag.oNO__NOME_8_52.value==this.w_NO__NOME)
      this.oPgFrm.Page8.oPag.oNO__NOME_8_52.value=this.w_NO__NOME
    endif
    if not(this.oPgFrm.Page8.oPag.oNODESCR2_8_53.value==this.w_NODESCR2)
      this.oPgFrm.Page8.oPag.oNODESCR2_8_53.value=this.w_NODESCR2
    endif
    if not(this.oPgFrm.Page9.oPag.oNO__NOTE_9_1.value==this.w_NO__NOTE)
      this.oPgFrm.Page9.oPag.oNO__NOTE_9_1.value=this.w_NO__NOTE
    endif
    if not(this.oPgFrm.Page9.oPag.oCODI_9_3.value==this.w_CODI)
      this.oPgFrm.Page9.oPag.oCODI_9_3.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page9.oPag.oNOTIPPER_9_5.RadioValue()==this.w_NOTIPPER)
      this.oPgFrm.Page9.oPag.oNOTIPPER_9_5.SetRadio()
    endif
    if not(this.oPgFrm.Page9.oPag.oNOCOGNOM_9_7.value==this.w_NOCOGNOM)
      this.oPgFrm.Page9.oPag.oNOCOGNOM_9_7.value=this.w_NOCOGNOM
    endif
    if not(this.oPgFrm.Page9.oPag.oNO__NOME_9_8.value==this.w_NO__NOME)
      this.oPgFrm.Page9.oPag.oNO__NOME_9_8.value=this.w_NO__NOME
    endif
    if not(this.oPgFrm.Page9.oPag.oNODESCRI_9_11.value==this.w_NODESCRI)
      this.oPgFrm.Page9.oPag.oNODESCRI_9_11.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page9.oPag.oNODESCR2_9_12.value==this.w_NODESCR2)
      this.oPgFrm.Page9.oPag.oNODESCR2_9_12.value=this.w_NODESCR2
    endif
    if not(this.oPgFrm.Page10.oPag.oNORATING_10_4.value==this.w_NORATING)
      this.oPgFrm.Page10.oPag.oNORATING_10_4.value=this.w_NORATING
    endif
    if not(this.oPgFrm.Page10.oPag.oNOGIORIT_10_5.value==this.w_NOGIORIT)
      this.oPgFrm.Page10.oPag.oNOGIORIT_10_5.value=this.w_NOGIORIT
    endif
    if not(this.oPgFrm.Page10.oPag.oNOVOCFIN_10_6.value==this.w_NOVOCFIN)
      this.oPgFrm.Page10.oPag.oNOVOCFIN_10_6.value=this.w_NOVOCFIN
    endif
    if not(this.oPgFrm.Page10.oPag.oDFDESCRI_10_7.value==this.w_DFDESCRI)
      this.oPgFrm.Page10.oPag.oDFDESCRI_10_7.value=this.w_DFDESCRI
    endif
    if not(this.oPgFrm.Page10.oPag.oNOESCDOF_10_8.RadioValue()==this.w_NOESCDOF)
      this.oPgFrm.Page10.oPag.oNOESCDOF_10_8.SetRadio()
    endif
    if not(this.oPgFrm.Page10.oPag.oNODESPAR_10_9.RadioValue()==this.w_NODESPAR)
      this.oPgFrm.Page10.oPag.oNODESPAR_10_9.SetRadio()
    endif
    if not(this.oPgFrm.Page10.oPag.oRADESCRI_10_10.value==this.w_RADESCRI)
      this.oPgFrm.Page10.oPag.oRADESCRI_10_10.value=this.w_RADESCRI
    endif
    if not(this.oPgFrm.Page10.oPag.oNOCODICE_10_12.value==this.w_NOCODICE)
      this.oPgFrm.Page10.oPag.oNOCODICE_10_12.value=this.w_NOCODICE
    endif
    if not(this.oPgFrm.Page10.oPag.oNOTIPPER_10_13.RadioValue()==this.w_NOTIPPER)
      this.oPgFrm.Page10.oPag.oNOTIPPER_10_13.SetRadio()
    endif
    if not(this.oPgFrm.Page10.oPag.oNOCOGNOM_10_15.value==this.w_NOCOGNOM)
      this.oPgFrm.Page10.oPag.oNOCOGNOM_10_15.value=this.w_NOCOGNOM
    endif
    if not(this.oPgFrm.Page10.oPag.oNO__NOME_10_16.value==this.w_NO__NOME)
      this.oPgFrm.Page10.oPag.oNO__NOME_10_16.value=this.w_NO__NOME
    endif
    if not(this.oPgFrm.Page10.oPag.oNODESCRI_10_19.value==this.w_NODESCRI)
      this.oPgFrm.Page10.oPag.oNODESCRI_10_19.value=this.w_NODESCRI
    endif
    if not(this.oPgFrm.Page10.oPag.oNODESCR2_10_20.value==this.w_NODESCR2)
      this.oPgFrm.Page10.oPag.oNODESCR2_10_20.value=this.w_NODESCR2
    endif
    cp_SetControlsValueExtFlds(this,'OFF_NOMI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_NOCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOCODICE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_NOCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NODESCRI)) or not(IIF(.w_NOSOGGET<>'PF' OR .w_NOFLGBEN<>'B',CHKCFP(.w_NODESCRI, "RA", "N",1, .w_NOCODICE),.T.)))  and (( (.w_NOFLGBEN<>'B' AND .w_NOTIPNOM<>'C') OR (.w_NOFLGBEN='B' AND .w_NOSOGGET='EN') ) And Empty(.w_NOCODCLI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNODESCRI_1_8.SetFocus()
            i_bnoObbl = !empty(.w_NODESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_DPFLINEX='I' AND .w_NOTIPPER='D') OR (.w_DPFLINEX='E' AND .w_NOTIPPER='C'))  and not( .w_NOFLGBEN<>'B'  OR  .w_NOSOGGET='EN' or .w_NOTIPPER='A')  and not(empty(.w_NOCOD_RU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOCOD_RU_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NOCOGNOM))  and not(IsAlt())  and (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI) And NOT IsAlt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOCOGNOM_1_15.SetFocus()
            i_bnoObbl = !empty(.w_NOCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NO__NOME))  and not(IsAlt())  and (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI) And NOT IsAlt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNO__NOME_1_16.SetFocus()
            i_bnoObbl = !empty(.w_NO__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_NOCODFIS, 'CF', 'N',0,.w_NOCODICE, .w_NONAZION))  and (Empty(.w_NOCODCLI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOCODFIS_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_NOPARIVA, "PI",'N',0,.w_NOCODICE, .w_NONAZION))  and ((.w_NOTIPNOM<>'C' OR .w_NOTIPPER='A') And Empty(.w_NOCODCLI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOPARIVA_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NOTIPPER))  and not(.w_NOFLGBEN<>'B')  and (Empty( .w_NOCODCLI ) And .w_NOSOGGET='PF')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOTIPPER_1_72.SetFocus()
            i_bnoObbl = !empty(.w_NOTIPPER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NOCODCON)) or not((EMPTY(.w_DATOBSN) OR .w_DATOBSN>.w_OBTEST) AND (.w_NOTIPCON='G' OR (.w_NOTIPCON='C' AND .w_ANFLRITE<>'S') OR (.w_NOTIPCON='F' AND .w_ANTIPCLF='G' AND .w_ANRITENU='N') ) AND .w_ANPARTSN='S'))  and not(.w_NOFLGBEN<>'B')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNOCODCON_2_3.SetFocus()
            i_bnoObbl = !empty(.w_NOCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente,obsoleto,con gestione ritenute attivata oppure non gestito a partite")
          case   not(EMPTY(.w_DATOBSOPAG) OR .w_DATOBSOPAG>.w_OBTEST)  and not(empty(.w_NOCODPAG))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNOCODPAG_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   not(.w_NOGIOFIS <= 31)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNOGIOFIS_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NO1MESCL<13 AND (.w_NO1MESCL<.w_NO2MESCL OR .w_NO2MESCL=0))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNO1MESCL_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NOGIOSC1)) or not(.w_NOGIOSC1 <= 31))  and (.w_NO1MESCL>0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNOGIOSC1_2_9.SetFocus()
            i_bnoObbl = !empty(.w_NOGIOSC1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NO2MESCL<13 AND (.w_NO1MESCL<.w_NO2MESCL OR .w_NO2MESCL=0))  and (.w_NO1MESCL > 0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNO2MESCL_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NOGIOSC2)) or not(.w_NOGIOSC2 <= 31))  and (.w_NO2MESCL>0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNOGIOSC2_2_11.SetFocus()
            i_bnoObbl = !empty(.w_NOGIOSC2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NOCODICE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNOCODICE_2_35.SetFocus()
            i_bnoObbl = !empty(.w_NOCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DTOBZON, .w_OBTEST, "Zona obsoleta alla data Attuale!"), CHKDTOBS(.w_DTOBZON,.w_OBTEST,"Zona obsoleta alla data Attuale!", .T.)))  and not(IsAlt())  and (.w_NOTIPNOM<>'C')  and not(empty(.w_NOCODZON))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oNOCODZON_4_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF(UPPER(g_APPLICATION)="AD HOC ENTERPRISE",CHKDTOBS(this,.w_DTOBVAL,.w_OBTEST,"Valuta obsoleta alla data Attuale!") AND CHKVLCF(.w_NOCODVAL,.w_VALLIS,"NOMINATIVO",'N',.w_IVALIS,.w_INIVAL,.w_FINVAL),CHKDTOBS(.w_DTOBVAL,.w_OBTEST,"Valuta obsoleta alla data Attuale!") AND CHKVLCF(.w_NOCODVAL, .w_VALLIS, "NOMINATIVO",'N',' ')))  and not(IsAlt())  and (.w_NOTIPNOM<>'C')  and not(empty(.w_NOCODVAL))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oNOCODVAL_4_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DTOBAGE,.w_OBTEST,"Agente obsoleto alla data Attuale!"), CHKDTOBS(.w_DTOBAGE,.w_OBTEST,"Agente obsoleto alla data Attuale!", .T.)))  and not(IsAlt() or !Empty(.w_NOFLGBEN))  and (.w_NOTIPNOM<>'C')  and not(empty(.w_NOCODAGE))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oNOCODAGE_4_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not( IIF (g_APPLICATION <> "ADHOC REVOLUTION"  , .T. ,CHKVLCF(.w_NOCODVAL, .w_VALLIS, "NOMINATIVO", 'N',.w_IVALIS)))  and not(g_APPLICATION <> "ADHOC REVOLUTION" OR IsAlt() or !Empty(.w_NOFLGBEN))  and (EMPTY (.w_NOCODCLI))  and not(empty(.w_NONUMLIS))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oNONUMLIS_4_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not( IIF (g_APPLICATION = "ADHOC REVOLUTION" ,.T.,CHKVLCF(.w_NOCODVAL, .w_VALLIS, "NOMINATIVO", 'N',.w_IVALIS,.w_INIVAL,.w_FINVAL)   ))  and not(g_APPLICATION = "ADHOC REVOLUTION" or !Empty(.w_NOFLGBEN))  and (EMPTY (.w_NOCODCLI))  and not(empty(.w_NONUMLIS))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oNONUMLIS_4_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSOPAG) OR .w_DATOBSOPAG>.w_OBTEST)  and not(.w_NOFLGBEN='B' OR IsAlt())  and (EMPTY (.w_NOCODCLI))  and not(empty(.w_NOCODPAG))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oNOCODPAG_4_46.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CONSBF='C')  and not(g_APPLICATION <> "ADHOC REVOLUTION"  OR .w_NOFLGBEN='B' OR IsAlt())  and (EMPTY (.w_NOCODCLI))  and not(empty(.w_BANCAAHR))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oBANCAAHR_4_51.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto o di tipo salvo buon fine")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBS>.w_OBTEST))  and not(g_APPLICATION= "ADHOC REVOLUTION" OR .w_NOFLGBEN='B')  and (EMPTY (.w_NOCODCLI) AND g_TESO='S')  and not(empty(.w_BANCAAHE))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oBANCAAHE_4_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto o di tipo salvo buon fine")
          case   not(IIF( UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", CHKDTOBS(this, .w_DTOBGR,.w_OBTEST,"Gruppo Priorit� obsoleto alla data Attuale!"),CHKDTOBS(.w_DTOBGR,.w_OBTEST,"Gruppo Priorit� obsoleto alla data Attuale!", .T.)))  and (.cFunction<>'Load')  and not(empty(.w_NFGRPRIO))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oNFGRPRIO_6_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(VAL(.w_ORAINI) < 24)  and (.cFunction<>'Load')
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oORAINI_7_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MININI) < 60)  and (.cFunction<>'Load')
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oMININI_7_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   not(VAL(.w_ORAFIN) < 24)  and (.cFunction<>'Load')
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oORAFIN_7_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 23")
          case   not(VAL(.w_MINFIN) < 60)  and (.cFunction<>'Load')
            .oPgFrm.ActivePage = 7
            .oPgFrm.Page7.oPag.oMINFIN_7_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore compreso tra 00 e 59")
          case   (empty(.w_NOCODICE))
            .oPgFrm.ActivePage = 8
            .oPgFrm.Page8.oPag.oNOCODICE_8_47.SetFocus()
            i_bnoObbl = !empty(.w_NOCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NOCODICE))
            .oPgFrm.ActivePage = 10
            .oPgFrm.Page10.oPag.oNOCODICE_10_12.SetFocus()
            i_bnoObbl = !empty(.w_NOCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAR_MCC.CheckForm()
      if i_bres
        i_bres=  .GSAR_MCC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MAN.CheckForm()
      if i_bres
        i_bres=  .GSAR_MAN.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MCN.CheckForm()
      if i_bres
        i_bres=  .GSAR_MCN.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=5
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsar_ano
      local oggfocus
      IF (.w_NOFLGBEN='B')
        IF EMPTY (.w_NOCODPAG)
          AH_ERRORMSG ("Codice pagamento non inserito")
        ENDIF
        IF EMPTY (.w_NOCODBAN)
          AH_ERRORMSG ("Banca di appoggio non inserita")
        ENDIF
      ENDIF
      
      IF (this.w_NOIDRIDY='S' AND EMPTY (this.w_NOCOIDRI))
      AH_ERRORMSG ("Codice identificativo RID non inserito")
      ENDIF
      
      if i_bRes And g_ANACOM<>'S'
         IF not(g_Anacom='S'  Or IIF (.w_CODISO1='IT' OR EMPTY (.w_CODISO1) , CHKCFP(.w_NOCODFIS, 'CFA', 'N',1,.w_NOCODICE,' ',.w_NOCOGNOM,.w_NO__NOME,.w_NOLOCNAS,.w_NOPRONAS,.w_NODATNAS,.w_NO_SESSO,IIF(.w_NOSOGGET='PF', 'S', 'N'),,.w_CODCOM),.T.))
            i_bRes = .f.
          	i_bnoChk = .t.
         ENDIF
      ENDIF
      
      * --- Controlli alla conferma...
      This.NotifyEvent('CheckForm')
      if .w_RESCHK<>0
      	i_bRes=.f.
       endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NOCODICE = this.w_NOCODICE
    this.o_NOSOGGET = this.w_NOSOGGET
    this.o_NOCOD_RU = this.w_NOCOD_RU
    this.o_NOCOGNOM = this.w_NOCOGNOM
    this.o_NO__NOME = this.w_NO__NOME
    this.o_NONUMCAR = this.w_NONUMCAR
    this.o_NOCODFIS = this.w_NOCODFIS
    this.o_NOINDIRI = this.w_NOINDIRI
    this.o_NOINDI_2 = this.w_NOINDI_2
    this.o_NOINDIR2 = this.w_NOINDIR2
    this.o_LOCALD = this.w_LOCALD
    this.o_NOINDWEB = this.w_NOINDWEB
    this.o_NOTIPPER = this.w_NOTIPPER
    this.o_NOFLGBEN = this.w_NOFLGBEN
    this.o_LOCNAS = this.w_LOCNAS
    this.o_CONTA = this.w_CONTA
    this.o_NOCODCON = this.w_NOCODCON
    this.o_NO2MESCL = this.w_NO2MESCL
    this.o_NOCODBA2 = this.w_NOCODBA2
    this.o_NOINTMOR = this.w_NOINTMOR
    this.o_BANCAAHE = this.w_BANCAAHE
    this.o_NOIDRIDY = this.w_NOIDRIDY
    this.o_NOTIIDRI = this.w_NOTIIDRI
    this.o_NOCOIDRI = this.w_NOCOIDRI
    this.o_NORIFINT = this.w_NORIFINT
    this.o_BANCAAHR = this.w_BANCAAHR
    this.o_DATAINI = this.w_DATAINI
    this.o_ORAINI = this.w_ORAINI
    this.o_MININI = this.w_MININI
    this.o_DATAFIN = this.w_DATAFIN
    this.o_ORAFIN = this.w_ORAFIN
    this.o_MINFIN = this.w_MINFIN
    this.o_SUBJECT = this.w_SUBJECT
    this.o_CONTINI = this.w_CONTINI
    this.o_PRIOINI = this.w_PRIOINI
    this.o_NOMINI = this.w_NOMINI
    this.o_NOMFIN = this.w_NOMFIN
    this.o_NO__RESP = this.w_NO__RESP
    this.o_CODUTE = this.w_CODUTE
    this.o_NOTIPDOC = this.w_NOTIPDOC
    this.o_NONUMDOC = this.w_NONUMDOC
    * --- GSAR_MCC : Depends On
    this.GSAR_MCC.SaveDependsOn()
    * --- GSAR_MAN : Depends On
    this.GSAR_MAN.SaveDependsOn()
    * --- GSAR_MCN : Depends On
    this.GSAR_MCN.SaveDependsOn()
    return

  func CanDelete()
    local i_res
    i_res=GSMA_BAE(this,'G')
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione annullata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsar_anoPag1 as StdContainer
  Width  = 783
  height = 544
  stdWidth  = 783
  stdheight = 544
  resizeXpos=284
  resizeYpos=472
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNOCODICE_1_3 as StdField with uid="HFNNITEZYS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NOCODICE", cQueryName = "NOCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice",;
    HelpContextID = 234222309,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=126, Left=125, Top=7, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15)

  proc oNOCODICE_1_3.mAfter
    with this.Parent.oContained
      .w_NOCODICE=CALCZER(.w_NOCODICE, 'OFF_NOMI')
    endwith
  endproc

  func oNOCODICE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CONTINI)
        bRes2=.link_7_15('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oNOCODCLI_1_4 as StdField with uid="PEUUCHNKLL",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NOCODCLI", cQueryName = "NOCODCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente",;
    HelpContextID = 201985311,;
   bGlobalFont=.t.,;
    Height=21, Width=126, Left=125, Top=35, InputMask=replicate('X',15), cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_NOTIPCLI", oKey_2_1="ANCODICE", oKey_2_2="this.w_NOCODCLI"

  func oNOCODCLI_1_4.mHide()
    with this.Parent.oContained
      return (.w_PARCALLER='B' Or .w_NOFLGBEN='B')
    endwith
  endfunc

  func oNOCODCLI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oNODESCRI_1_8 as StdField with uid="OEFMLTQKHX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione sociale",;
    HelpContextID = 51372769,;
   bGlobalFont=.t.,;
    Height=21, Width=306, Left=126, Top=84, InputMask=replicate('X',60)

  func oNODESCRI_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (( (.w_NOFLGBEN<>'B' AND .w_NOTIPNOM<>'C') OR (.w_NOFLGBEN='B' AND .w_NOSOGGET='EN') ) And Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  proc oNODESCRI_1_8.mAfter
    with this.Parent.oContained
      .w_NODESCRI=IIF(.cFunction<>'Query', Normragsoc(.w_NODESCRI), .w_NODESCRI)
    endwith
  endproc

  func oNODESCRI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(.w_NOSOGGET<>'PF' OR .w_NOFLGBEN<>'B',CHKCFP(.w_NODESCRI, "RA", "N",1, .w_NOCODICE),.T.))
    endwith
    return bRes
  endfunc

  add object oNODESCR2_1_9 as StdField with uid="UBVBCHRBKG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_NODESCR2", cQueryName = "NODESCR2",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi denominazione sociale",;
    HelpContextID = 51372792,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=460, Top=84, InputMask=replicate('X',60)

  func oNODESCR2_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (( (.w_NOFLGBEN<>'B' AND .w_NOTIPNOM<>'C') OR (.w_NOFLGBEN='B' AND .w_NOSOGGET='EN') ) And Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  proc oNODESCR2_1_9.mAfter
    with this.Parent.oContained
      .w_NODESCR2=IIF(.cFunction<>'Query', Normragsoc(.w_NODESCR2), .w_NODESCR2)
    endwith
  endproc

  add object oNOSOGGET_1_10 as StdCheck with uid="ZXUFLWVQON",rtseq=10,rtrep=.f.,left=310, top=9, caption="Persona fisica",;
    ToolTipText = "Se attivo: il nominativo � una persona fisica",;
    HelpContextID = 3869994,;
    cFormVar="w_NOSOGGET", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOSOGGET_1_10.RadioValue()
    return(iif(this.value =1,'PF',;
    'EN'))
  endfunc
  func oNOSOGGET_1_10.GetRadio()
    this.Parent.oContained.w_NOSOGGET = this.RadioValue()
    return .t.
  endfunc

  func oNOSOGGET_1_10.SetRadio()
    this.Parent.oContained.w_NOSOGGET=trim(this.Parent.oContained.w_NOSOGGET)
    this.value = ;
      iif(this.Parent.oContained.w_NOSOGGET=='PF',1,;
      0)
  endfunc

  func oNOSOGGET_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_NOCODCLI ))
    endwith
   endif
  endfunc

  func oNOSOGGET_1_10.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oNOCOD_RU_1_12 as StdField with uid="UTKKFDMPLR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NOCOD_RU", cQueryName = "NOCOD_RU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona",;
    HelpContextID = 133558997,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=126, Top=108, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TIPRIS", oKey_2_1="DPCODICE", oKey_2_2="this.w_NOCOD_RU"

  func oNOCOD_RU_1_12.mHide()
    with this.Parent.oContained
      return ( .w_NOFLGBEN<>'B'  OR  .w_NOSOGGET='EN' or .w_NOTIPPER='A')
    endwith
  endfunc

  func oNOCOD_RU_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCOD_RU_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCOD_RU_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TIPRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TIPRIS)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oNOCOD_RU_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Persone",'GSAR_ABD.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oNOCOD_RU_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TIPRIS
     i_obj.w_DPCODICE=this.parent.oContained.w_NOCOD_RU
     i_obj.ecpSave()
  endproc

  add object oNOBADGE_1_13 as StdField with uid="YFWWLONHHR",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NOBADGE", cQueryName = "NOBADGE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice badge",;
    HelpContextID = 262954,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=460, Top=108, InputMask=replicate('X',20)

  func oNOBADGE_1_13.mHide()
    with this.Parent.oContained
      return ( .w_NOFLGBEN<>'B'  OR  .w_NOSOGGET='EN' or .w_NOTIPPER='A')
    endwith
  endfunc

  add object oNOCOGNOM_1_14 as StdField with uid="WXSROJQYFL",rtseq=14,rtrep=.f.,;
    cFormVar = "w_NOCOGNOM", cQueryName = "NOCOGNOM",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 147190493,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=126, Top=133, InputMask=replicate('X',50)

  func oNOCOGNOM_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI) And IsAlt())
    endwith
   endif
  endfunc

  func oNOCOGNOM_1_14.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oNOCOGNOM_1_15 as StdField with uid="ZZNZUIPJAK",rtseq=15,rtrep=.f.,;
    cFormVar = "w_NOCOGNOM", cQueryName = "NOCOGNOM",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 147190493,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=126, Top=132, InputMask=replicate('X',50)

  func oNOCOGNOM_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI) And NOT IsAlt())
    endwith
   endif
  endfunc

  func oNOCOGNOM_1_15.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oNO__NOME_1_16 as StdField with uid="OBJSQJOBOP",rtseq=16,rtrep=.f.,;
    cFormVar = "w_NO__NOME", cQueryName = "NO__NOME",;
    bObbl = .t. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 121909989,;
   bGlobalFont=.t.,;
    Height=21, Width=178, Left=602, Top=132, InputMask=replicate('X',50)

  func oNO__NOME_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI) And NOT IsAlt())
    endwith
   endif
  endfunc

  func oNO__NOME_1_16.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oNO__NOME_1_17 as StdField with uid="LVKVPMZSYD",rtseq=17,rtrep=.f.,;
    cFormVar = "w_NO__NOME", cQueryName = "NO__NOME",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 121909989,;
   bGlobalFont=.t.,;
    Height=21, Width=178, Left=602, Top=132, InputMask=replicate('X',50)

  func oNO__NOME_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI) And IsAlt())
    endwith
   endif
  endfunc

  func oNO__NOME_1_17.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oNOLOCNAS_1_18 as StdField with uid="DQIMPFTXUM",rtseq=18,rtrep=.f.,;
    cFormVar = "w_NOLOCNAS", cQueryName = "NOLOCNAS",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Luogo di nascita",;
    HelpContextID = 151347927,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=125, Top=158, InputMask=replicate('X',30), bHasZoom = .t. 

  func oNOLOCNAS_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOLOCNAS_1_18.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOLOCNAS_1_18.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_NOLOCNAS",".w_NOPRONAS",,,".w_CODCOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOLOCNAS_1_19 as StdField with uid="NJZWPOCKBB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_NOLOCNAS", cQueryName = "NOLOCNAS",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Luogo di nascita",;
    HelpContextID = 151347927,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=126, Top=158, InputMask=replicate('X',30), bHasZoom = .t. 

  func oNOLOCNAS_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOLOCNAS_1_19.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOLOCNAS_1_19.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_NOLOCNAS",".w_NOPRONAS",,,".w_CODCOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOPRONAS_1_20 as StdField with uid="HNIMRBBKVY",rtseq=20,rtrep=.f.,;
    cFormVar = "w_NOPRONAS", cQueryName = "NOPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia del luogo di nascita",;
    HelpContextID = 138552023,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=415, Top=159, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oNOPRONAS_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOPRONAS_1_20.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOPRONAS_1_20.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_NOPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOPRONAS_1_21 as StdField with uid="JNYFVCYDEL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_NOPRONAS", cQueryName = "NOPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia del luogo di nascita",;
    HelpContextID = 138552023,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=415, Top=158, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oNOPRONAS_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOPRONAS_1_21.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOPRONAS_1_21.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_NOPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNODATNAS_1_22 as StdField with uid="BSWJZPFIMW",rtseq=22,rtrep=.f.,;
    cFormVar = "w_NODATNAS", cQueryName = "NODATNAS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 134472407,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=602, Top=158

  func oNODATNAS_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc


  add object oNO_SESSO_1_23 as StdCombo with uid="JIOWUUYHNU",rtseq=23,rtrep=.f.,left=687,top=158,width=93,height=21;
    , ToolTipText = "Sesso";
    , HelpContextID = 65024731;
    , cFormVar="w_NO_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oNO_SESSO_1_23.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oNO_SESSO_1_23.GetRadio()
    this.Parent.oContained.w_NO_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oNO_SESSO_1_23.SetRadio()
    this.Parent.oContained.w_NO_SESSO=trim(this.Parent.oContained.w_NO_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_NO_SESSO=='M',1,;
      iif(this.Parent.oContained.w_NO_SESSO=='F',2,;
      0))
  endfunc

  func oNO_SESSO_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  add object oNONAZNAS_1_24 as StdField with uid="IXPIPJBYNX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_NONAZNAS", cQueryName = "NONAZNAS",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice nazione di nascita",;
    HelpContextID = 128139991,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=126, Top=183, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_NONAZNAS"

  func oNONAZNAS_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
   endif
  endfunc

  func oNONAZNAS_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oNONAZNAS_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNONAZNAS_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oNONAZNAS_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oNONAZNAS_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_NONAZNAS
     i_obj.ecpSave()
  endproc

  add object oNONUMCAR_1_25 as StdField with uid="KUMIMWIJSD",rtseq=25,rtrep=.f.,;
    cFormVar = "w_NONUMCAR", cQueryName = "NONUMCAR",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero carta di identit�",;
    HelpContextID = 56574680,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=602, Top=183, InputMask=replicate('X',18)

  func oNONUMCAR_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN' And Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  add object oNOCODFIS_1_26 as StdField with uid="FPRVORIMBL",rtseq=26,rtrep=.f.,;
    cFormVar = "w_NOCODFIS", cQueryName = "NOCODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale - Doppio click per eseguire il calcolo del codice fiscale di una persona fisica",;
    HelpContextID = 252316969,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=126, Top=208, cSayPict='REPL("!",16)', cGetPict='REPL("!",16)', InputMask=replicate('X',16), bHasZoom = .t. 

  func oNOCODFIS_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOCODFIS_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_NOCODFIS, 'CF', 'N',0,.w_NOCODICE, .w_NONAZION))
    endwith
    return bRes
  endfunc

  proc oNOCODFIS_1_26.mZoom
    ZoomCF (  this.parent.oContained )
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOPARIVA_1_27 as StdField with uid="HRGTVADCOM",rtseq=27,rtrep=.f.,;
    cFormVar = "w_NOPARIVA", cQueryName = "NOPARIVA",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 48028951,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=126, Top=233, InputMask=replicate('X',12)

  func oNOPARIVA_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_NOTIPNOM<>'C' OR .w_NOTIPPER='A') And Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOPARIVA_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_NOPARIVA, "PI",'N',0,.w_NOCODICE, .w_NONAZION))
    endwith
    return bRes
  endfunc


  add object oNONATGIU_1_28 as StdCombo with uid="MHZGWJNXXW",rtseq=28,rtrep=.f.,left=461,top=208,width=251,height=22;
    , ToolTipText = "Natura giuridica del nominativo. Richiesto al momento del deposito telematico se questo nominativo sar� inserito fra le parti di un procedimento";
    , HelpContextID = 16563499;
    , cFormVar="w_NONATGIU",RowSource=""+"Persona fisica,"+"Societ�,"+"Societ� di persone,"+"Cooperative,"+"Consorzio,"+"Condominio,"+"Associazione,"+"Comitato,"+"Enti di gestione,"+"Ente pubblico,"+"Ente religioso,"+"Partito o Sindacato,"+"Stato/Organizzazione estera,"+"Ditta Individuale,"+"Persona giuridica,"+"Pubblica amminstrazione,"+"Istituto di credito,"+"Non specificata,"+"Pubblico Ministero", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oNONATGIU_1_28.RadioValue()
    return(iif(this.value =1,'PFI',;
    iif(this.value =2,'SOC',;
    iif(this.value =3,'SOP',;
    iif(this.value =4,'COP',;
    iif(this.value =5,'CON',;
    iif(this.value =6,'CND',;
    iif(this.value =7,'ASS',;
    iif(this.value =8,'COM',;
    iif(this.value =9,'EDG',;
    iif(this.value =10,'ENP',;
    iif(this.value =11,'EIS',;
    iif(this.value =12,'PAS',;
    iif(this.value =13,'OSE',;
    iif(this.value =14,'DIM',;
    iif(this.value =15,'PGI',;
    iif(this.value =16,'PAM',;
    iif(this.value =17,'ISC',;
    iif(this.value =18,'NA',;
    iif(this.value =19,'PUM',;
    space(3)))))))))))))))))))))
  endfunc
  func oNONATGIU_1_28.GetRadio()
    this.Parent.oContained.w_NONATGIU = this.RadioValue()
    return .t.
  endfunc

  func oNONATGIU_1_28.SetRadio()
    this.Parent.oContained.w_NONATGIU=trim(this.Parent.oContained.w_NONATGIU)
    this.value = ;
      iif(this.Parent.oContained.w_NONATGIU=='PFI',1,;
      iif(this.Parent.oContained.w_NONATGIU=='SOC',2,;
      iif(this.Parent.oContained.w_NONATGIU=='SOP',3,;
      iif(this.Parent.oContained.w_NONATGIU=='COP',4,;
      iif(this.Parent.oContained.w_NONATGIU=='CON',5,;
      iif(this.Parent.oContained.w_NONATGIU=='CND',6,;
      iif(this.Parent.oContained.w_NONATGIU=='ASS',7,;
      iif(this.Parent.oContained.w_NONATGIU=='COM',8,;
      iif(this.Parent.oContained.w_NONATGIU=='EDG',9,;
      iif(this.Parent.oContained.w_NONATGIU=='ENP',10,;
      iif(this.Parent.oContained.w_NONATGIU=='EIS',11,;
      iif(this.Parent.oContained.w_NONATGIU=='PAS',12,;
      iif(this.Parent.oContained.w_NONATGIU=='OSE',13,;
      iif(this.Parent.oContained.w_NONATGIU=='DIM',14,;
      iif(this.Parent.oContained.w_NONATGIU=='PGI',15,;
      iif(this.Parent.oContained.w_NONATGIU=='PAM',16,;
      iif(this.Parent.oContained.w_NONATGIU=='ISC',17,;
      iif(this.Parent.oContained.w_NONATGIU=='NA',18,;
      iif(this.Parent.oContained.w_NONATGIU=='PUM',19,;
      0)))))))))))))))))))
  endfunc

  func oNONATGIU_1_28.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oNOCODSAL_1_29 as StdTableCombo with uid="PZAGGDGVEP",rtseq=29,rtrep=.f.,left=461,top=233,width=251,height=21;
    , ToolTipText = "Formula di cortesia";
    , HelpContextID = 66450142;
    , cFormVar="w_NOCODSAL",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="SAL_NOMI";
    , cTable='SAL_NOMI',cKey='SACODICE',cValue='SADESCRI',cOrderBy='SADESCRI',xDefault=space(5);
  , bGlobalFont=.t.


  func oNOCODSAL_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOCODSAL_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODSAL_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oNOINDIRI_1_30 as StdField with uid="DFQOEYUXBZ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_NOINDIRI", cQueryName = "NOINDIRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di residenza",;
    HelpContextID = 234263265,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=124, Top=270, InputMask=replicate('X',35)

  func oNOINDIRI_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  add object oNOINDI_2_1_31 as StdField with uid="SDRBXYIKZB",rtseq=31,rtrep=.f.,;
    cFormVar = "w_NOINDI_2", cQueryName = "NOINDI_2",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi all'indirizzo del nominativo",;
    HelpContextID = 234263288,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=517, Top=270, InputMask=replicate('X',35)

  func oNOINDI_2_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  add object oNO___CAP_1_33 as StdField with uid="AKYTINVTJU",rtseq=33,rtrep=.f.,;
    cFormVar = "w_NO___CAP", cQueryName = "NO___CAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice di avviamento postale",;
    HelpContextID = 36975322,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=124, Top=295, InputMask=replicate('X',8), bHasZoom = .t. 

  func oNO___CAP_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOTIPNOM<>'C')
    endwith
   endif
  endfunc

  func oNO___CAP_1_33.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "AD HOC ENTERPRISE")
    endwith
  endfunc

  proc oNO___CAP_1_33.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_NO___CAP",".w_NOLOCALI",".w_NOPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNO___CAP_1_34 as StdField with uid="SBJJJJVARO",rtseq=34,rtrep=.f.,;
    cFormVar = "w_NO___CAP", cQueryName = "NO___CAP",;
    bObbl = .f. , nPag = 1, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "Codice di avviamento postale",;
    HelpContextID = 36975322,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=124, Top=295, InputMask=replicate('X',9), bHasZoom = .t. 

  func oNO___CAP_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNO___CAP_1_34.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNO___CAP_1_34.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_NO___CAP",".w_NOLOCALI",".w_NOPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOLOCALI_1_35 as StdField with uid="VYMPWVJAME",rtseq=35,rtrep=.f.,;
    cFormVar = "w_NOLOCALI", cQueryName = "NOLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 167419167,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=231, Top=295, InputMask=replicate('X',30), bHasZoom = .t. 

  func oNOLOCALI_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOLOCALI_1_35.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOLOCALI_1_35.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_NO___CAP",".w_NOLOCALI",".w_NOPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOLOCALI_1_36 as StdField with uid="IFTNFSXAKM",rtseq=36,rtrep=.f.,;
    cFormVar = "w_NOLOCALI", cQueryName = "NOLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 167419167,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=231, Top=295, InputMask=replicate('X',30), bHasZoom = .t. 

  func oNOLOCALI_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOLOCALI_1_36.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOLOCALI_1_36.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_NO___CAP",".w_NOLOCALI",".w_NOPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOPROVIN_1_37 as StdField with uid="GKGABNMOQL",rtseq=37,rtrep=.f.,;
    cFormVar = "w_NOPROVIN", cQueryName = "NOPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza",;
    HelpContextID = 264101156,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=554, Top=295, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oNOPROVIN_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOPROVIN_1_37.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOPROVIN_1_37.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_NOPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOPROVIN_1_38 as StdField with uid="RCCUFUNZAG",rtseq=38,rtrep=.f.,;
    cFormVar = "w_NOPROVIN", cQueryName = "NOPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza",;
    HelpContextID = 264101156,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=554, Top=295, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oNOPROVIN_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOPROVIN_1_38.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOPROVIN_1_38.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_NOPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNONAZION_1_39 as StdField with uid="KBSTCDEWAB",rtseq=39,rtrep=.f.,;
    cFormVar = "w_NONAZION", cQueryName = "NONAZION",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della nazione di appartenenza",;
    HelpContextID = 212026076,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=692, Top=296, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_NONAZION"

  func oNONAZION_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNONAZION_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oNONAZION_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNONAZION_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oNONAZION_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oNONAZION_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_NONAZION
     i_obj.ecpSave()
  endproc

  add object oNOINDIR2_1_40 as StdField with uid="UIONGOSCIR",rtseq=40,rtrep=.f.,;
    cFormVar = "w_NOINDIR2", cQueryName = "NOINDIR2",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo domicilio",;
    HelpContextID = 234263288,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=123, Top=330, InputMask=replicate('X',35)

  add object oNO___CAD_1_41 as StdField with uid="CSKYVRCLOM",rtseq=41,rtrep=.f.,;
    cFormVar = "w_NO___CAD", cQueryName = "NO___CAD",;
    bObbl = .f. , nPag = 1, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "Codice di avviamento postale domicilio",;
    HelpContextID = 36975334,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=123, Top=355, InputMask=replicate('X',9), bHasZoom = .t. 

  func oNO___CAD_1_41.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNO___CAD_1_41.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_NO___CAD",".w_NOLOCALD",".w_NOPROVID")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNO___CAD_1_42 as StdField with uid="FQPBUERUSK",rtseq=42,rtrep=.f.,;
    cFormVar = "w_NO___CAD", cQueryName = "NO___CAD",;
    bObbl = .f. , nPag = 1, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "Codice di avviamento postale domicilio",;
    HelpContextID = 36975334,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=123, Top=355, InputMask=replicate('X',9), bHasZoom = .t. 

  func oNO___CAD_1_42.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNO___CAD_1_42.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_NO___CAD",".w_NOLOCALD",".w_NOPROVID")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOLOCALD_1_43 as StdField with uid="ERHEALQJIV",rtseq=43,rtrep=.f.,;
    cFormVar = "w_NOLOCALD", cQueryName = "NOLOCALD",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� domicilio",;
    HelpContextID = 167419162,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=232, Top=355, InputMask=replicate('X',30), bHasZoom = .t. 

  func oNOLOCALD_1_43.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOLOCALD_1_43.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_NO___CAD",".w_NOLOCALD",".w_NOPROVID")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOLOCALD_1_47 as StdField with uid="SLTBCOJCPD",rtseq=47,rtrep=.f.,;
    cFormVar = "w_NOLOCALD", cQueryName = "NOLOCALD",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� domicilio",;
    HelpContextID = 167419162,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=232, Top=355, InputMask=replicate('X',30), bHasZoom = .t. 

  func oNOLOCALD_1_47.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOLOCALD_1_47.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_NO___CAD",".w_NOLOCALD",".w_NOPROVID")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOPROVID_1_48 as StdField with uid="VXWYLBGLFE",rtseq=48,rtrep=.f.,;
    cFormVar = "w_NOPROVID", cQueryName = "NOPROVID",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia domicilio",;
    HelpContextID = 264101146,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=553, Top=355, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oNOPROVID_1_48.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOPROVID_1_48.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_NOPROVID")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOPROVID_1_49 as StdField with uid="PANEIDXKJZ",rtseq=49,rtrep=.f.,;
    cFormVar = "w_NOPROVID", cQueryName = "NOPROVID",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia domicilio",;
    HelpContextID = 264101146,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=553, Top=355, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oNOPROVID_1_49.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  proc oNOPROVID_1_49.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_NOPROVID")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNONAZIOD_1_50 as StdField with uid="XMEZRFGSFU",rtseq=50,rtrep=.f.,;
    cFormVar = "w_NONAZIOD", cQueryName = "NONAZIOD",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della nazione domicilio",;
    HelpContextID = 212026086,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=692, Top=355, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_NONAZIOD"

  func oNONAZIOD_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_50('Part',this)
    endwith
    return bRes
  endfunc

  proc oNONAZIOD_1_50.ecpDrop(oSource)
    this.Parent.oContained.link_1_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNONAZIOD_1_50.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oNONAZIOD_1_50'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oNONAZIOD_1_50.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_NONAZIOD
     i_obj.ecpSave()
  endproc

  add object oNOTELEFO_1_51 as StdField with uid="DHVKXMJMAW",rtseq=51,rtrep=.f.,;
    cFormVar = "w_NOTELEFO", cQueryName = "NOTELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero telefonico principale",;
    HelpContextID = 243342629,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=124, Top=398, InputMask=replicate('X',18)

  func oNOTELEFO_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  add object oNONUMCEL_1_52 as StdField with uid="YYSDODZUJY",rtseq=52,rtrep=.f.,;
    cFormVar = "w_NONUMCEL", cQueryName = "NONUMCEL",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero del telefono cellulare",;
    HelpContextID = 56574686,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=384, Top=398, InputMask=replicate('X',18)

  func oNONUMCEL_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  add object oNO_SKYPE_1_53 as StdField with uid="ZHYLYWTOOS",rtseq=53,rtrep=.f.,;
    cFormVar = "w_NO_SKYPE", cQueryName = "NO_SKYPE",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Codice utente Skype",;
    HelpContextID = 226505445,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=605, Top=397, InputMask=replicate('X',50)

  func oNO_SKYPE_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  add object oNOTELFAX_1_54 as StdField with uid="BZKYFXBYJU",rtseq=54,rtrep=.f.,;
    cFormVar = "w_NOTELFAX", cQueryName = "NOTELFAX",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di TELEX o FAX",;
    HelpContextID = 8315602,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=124, Top=421, InputMask=replicate('X',18)

  func oNOTELFAX_1_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  add object oNO_EMAIL_1_55 as StdField with uid="DWSEHYJFNK",rtseq=55,rtrep=.f.,;
    cFormVar = "w_NO_EMAIL", cQueryName = "NO_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di posta elettronica",;
    HelpContextID = 177327394,;
   bGlobalFont=.t.,;
    Height=21, Width=369, Left=384, Top=421, InputMask=replicate('X',254)

  func oNO_EMAIL_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  add object oNOINDWEB_1_56 as StdField with uid="ILQXJSWQWT",rtseq=56,rtrep=.f.,;
    cFormVar = "w_NOINDWEB", cQueryName = "NOINDWEB",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo sito Internet",;
    HelpContextID = 617752,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=124, Top=444, InputMask=replicate('X',254)

  func oNOINDWEB_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  add object oNO_EMPEC_1_57 as StdField with uid="FHEUYCTJAM",rtseq=57,rtrep=.f.,;
    cFormVar = "w_NO_EMPEC", cQueryName = "NO_EMPEC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di posta elettronica certificata (PEC)",;
    HelpContextID = 160550169,;
   bGlobalFont=.t.,;
    Height=21, Width=369, Left=384, Top=444, InputMask=replicate('X',254)

  func oNO_EMPEC_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  add object oNOCHKSTA_1_58 as StdCheck with uid="HDBTXBPKLN",rtseq=58,rtrep=.f.,left=124, top=492, caption="Stampa",;
    HelpContextID = 59568873,;
    cFormVar="w_NOCHKSTA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOCHKSTA_1_58.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOCHKSTA_1_58.GetRadio()
    this.Parent.oContained.w_NOCHKSTA = this.RadioValue()
    return .t.
  endfunc

  func oNOCHKSTA_1_58.SetRadio()
    this.Parent.oContained.w_NOCHKSTA=trim(this.Parent.oContained.w_NOCHKSTA)
    this.value = ;
      iif(this.Parent.oContained.w_NOCHKSTA=='S',1,;
      0)
  endfunc

  func oNOCHKSTA_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOCHKSTA_1_58.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oNOCHKMAI_1_59 as StdCheck with uid="VNSLKUJWHZ",rtseq=59,rtrep=.f.,left=216, top=492, caption="E-mail",;
    HelpContextID = 160232161,;
    cFormVar="w_NOCHKMAI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOCHKMAI_1_59.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOCHKMAI_1_59.GetRadio()
    this.Parent.oContained.w_NOCHKMAI = this.RadioValue()
    return .t.
  endfunc

  func oNOCHKMAI_1_59.SetRadio()
    this.Parent.oContained.w_NOCHKMAI=trim(this.Parent.oContained.w_NOCHKMAI)
    this.value = ;
      iif(this.Parent.oContained.w_NOCHKMAI=='S',1,;
      0)
  endfunc

  func oNOCHKMAI_1_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOCHKMAI_1_59.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oNOCHKPEC_1_60 as StdCheck with uid="RRNWWQHUTY",rtseq=60,rtrep=.f.,left=291, top=492, caption="PEC",;
    HelpContextID = 158534937,;
    cFormVar="w_NOCHKPEC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOCHKPEC_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOCHKPEC_1_60.GetRadio()
    this.Parent.oContained.w_NOCHKPEC = this.RadioValue()
    return .t.
  endfunc

  func oNOCHKPEC_1_60.SetRadio()
    this.Parent.oContained.w_NOCHKPEC=trim(this.Parent.oContained.w_NOCHKPEC)
    this.value = ;
      iif(this.Parent.oContained.w_NOCHKPEC=='S',1,;
      0)
  endfunc

  func oNOCHKPEC_1_60.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOCHKPEC_1_60.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oNOCHKFAX_1_61 as StdCheck with uid="ZXITRVALRT",rtseq=61,rtrep=.f.,left=368, top=492, caption="FAX",;
    HelpContextID = 9237202,;
    cFormVar="w_NOCHKFAX", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOCHKFAX_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOCHKFAX_1_61.GetRadio()
    this.Parent.oContained.w_NOCHKFAX = this.RadioValue()
    return .t.
  endfunc

  func oNOCHKFAX_1_61.SetRadio()
    this.Parent.oContained.w_NOCHKFAX=trim(this.Parent.oContained.w_NOCHKFAX)
    this.value = ;
      iif(this.Parent.oContained.w_NOCHKFAX=='S',1,;
      0)
  endfunc

  func oNOCHKFAX_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOCHKFAX_1_61.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oNOCHKCPZ_1_62 as StdCheck with uid="PEKTCZDVIU",rtseq=62,rtrep=.f.,left=438, top=492, caption="Web Application",;
    ToolTipText = "Se attivo: consente invio documenti a cliente su Web Application",;
    HelpContextID = 59568848,;
    cFormVar="w_NOCHKCPZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOCHKCPZ_1_62.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOCHKCPZ_1_62.GetRadio()
    this.Parent.oContained.w_NOCHKCPZ = this.RadioValue()
    return .t.
  endfunc

  func oNOCHKCPZ_1_62.SetRadio()
    this.Parent.oContained.w_NOCHKCPZ=trim(this.Parent.oContained.w_NOCHKCPZ)
    this.value = ;
      iif(this.Parent.oContained.w_NOCHKCPZ=='S',1,;
      0)
  endfunc

  func oNOCHKCPZ_1_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_IZCP$'SA'  or g_CPIN='S') and g_DMIP<>'S')
    endwith
   endif
  endfunc

  func oNOCHKCPZ_1_62.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' )
    endwith
  endfunc

  add object oNODTINVA_1_66 as StdField with uid="SAGPOQNJEK",rtseq=66,rtrep=.f.,;
    cFormVar = "w_NODTINVA", cQueryName = "NODTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 123673879,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=124, Top=518, tabstop=.f.

  func oNODTINVA_1_66.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oNODTOBSO_1_67 as StdField with uid="QFRFCERXAV",rtseq=67,rtrep=.f.,;
    cFormVar = "w_NODTOBSO", cQueryName = "NODTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 71361243,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=354, Top=518, tabstop=.f.


  add object oNOSOGGET_1_69 as StdCombo with uid="ETMYYXTHVB",rtseq=68,rtrep=.f.,left=326,top=7,width=107,height=21;
    , ToolTipText = "Soggetto: ente\persona fisica";
    , HelpContextID = 3869994;
    , cFormVar="w_NOSOGGET",RowSource=""+"Persona fisica,"+"Ente", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oNOSOGGET_1_69.RadioValue()
    return(iif(this.value =1,'PF',;
    iif(this.value =2,'EN',;
    space(2))))
  endfunc
  func oNOSOGGET_1_69.GetRadio()
    this.Parent.oContained.w_NOSOGGET = this.RadioValue()
    return .t.
  endfunc

  func oNOSOGGET_1_69.SetRadio()
    this.Parent.oContained.w_NOSOGGET=trim(this.Parent.oContained.w_NOSOGGET)
    this.value = ;
      iif(this.Parent.oContained.w_NOSOGGET=='PF',1,;
      iif(this.Parent.oContained.w_NOSOGGET=='EN',2,;
      0))
  endfunc

  func oNOSOGGET_1_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_NOCODCLI ))
    endwith
   endif
  endfunc

  func oNOSOGGET_1_69.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oNOTIPPER_1_72 as StdCombo with uid="GJIGXLZNVU",rtseq=69,rtrep=.f.,left=474,top=7,width=107,height=21;
    , ToolTipText = "Tipo persona (dipendente, collaboratore, altro)";
    , HelpContextID = 163913000;
    , cFormVar="w_NOTIPPER",RowSource=""+"Dipendente,"+"Collaboratore,"+"Altro", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oNOTIPPER_1_72.RadioValue()
    return(iif(this.value =1,"D",;
    iif(this.value =2,"C",;
    iif(this.value =3,"A",;
    space(1)))))
  endfunc
  func oNOTIPPER_1_72.GetRadio()
    this.Parent.oContained.w_NOTIPPER = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPPER_1_72.SetRadio()
    this.Parent.oContained.w_NOTIPPER=trim(this.Parent.oContained.w_NOTIPPER)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPPER=="D",1,;
      iif(this.Parent.oContained.w_NOTIPPER=="C",2,;
      iif(this.Parent.oContained.w_NOTIPPER=="A",3,;
      0)))
  endfunc

  func oNOTIPPER_1_72.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_NOCODCLI ) And .w_NOSOGGET='PF')
    endwith
   endif
  endfunc

  func oNOTIPPER_1_72.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc


  add object oNOTIPNOM_1_74 as StdCombo with uid="LKTEOERMIJ",rtseq=70,rtrep=.f.,left=660,top=7,width=93,height=21;
    , ToolTipText = "Tipo nominativo";
    , HelpContextID = 138076893;
    , cFormVar="w_NOTIPNOM",RowSource=""+"Da valutare,"+"Potenziale,"+"Lead,"+"Prospect", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oNOTIPNOM_1_74.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'Z',;
    iif(this.value =3,'L',;
    iif(this.value =4,'P',;
    space(1))))))
  endfunc
  func oNOTIPNOM_1_74.GetRadio()
    this.Parent.oContained.w_NOTIPNOM = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPNOM_1_74.SetRadio()
    this.Parent.oContained.w_NOTIPNOM=trim(this.Parent.oContained.w_NOTIPNOM)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPNOM=='T',1,;
      iif(this.Parent.oContained.w_NOTIPNOM=='Z',2,;
      iif(this.Parent.oContained.w_NOTIPNOM=='L',3,;
      iif(this.Parent.oContained.w_NOTIPNOM=='P',4,;
      0))))
  endfunc

  func oNOTIPNOM_1_74.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOPE='LOAD')
    endwith
   endif
  endfunc

  func oNOTIPNOM_1_74.mHide()
    with this.Parent.oContained
      return (.w_PARCALLER='B' or (.w_PARCALLER='N' and .w_NOFLGBEN='B') OR .w_TIPOPE<>'LOAD' OR IsAlt())
    endwith
  endfunc


  add object oNOTIPNOM_1_75 as StdCombo with uid="KMJEZXZTGH",rtseq=71,rtrep=.f.,left=660,top=7,width=93,height=21;
    , ToolTipText = "Tipo nominativo";
    , HelpContextID = 138076893;
    , cFormVar="w_NOTIPNOM",RowSource=""+"Nominativo,"+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oNOTIPNOM_1_75.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oNOTIPNOM_1_75.GetRadio()
    this.Parent.oContained.w_NOTIPNOM = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPNOM_1_75.SetRadio()
    this.Parent.oContained.w_NOTIPNOM=trim(this.Parent.oContained.w_NOTIPNOM)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPNOM=='T',1,;
      iif(this.Parent.oContained.w_NOTIPNOM=='C',2,;
      iif(this.Parent.oContained.w_NOTIPNOM=='F',3,;
      0)))
  endfunc

  func oNOTIPNOM_1_75.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOTIPNOM_1_75.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc


  add object oNOTIPNOM_1_76 as StdCombo with uid="IQRQSNNXBG",rtseq=72,rtrep=.f.,left=660,top=7,width=93,height=21;
    , ToolTipText = "Tipo nominativo";
    , HelpContextID = 138076893;
    , cFormVar="w_NOTIPNOM",RowSource=""+"Da valutare,"+"Potenziale,"+"Lead,"+"Prospect,"+"Cliente,"+"Congelato,"+"Non interessante", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oNOTIPNOM_1_76.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'Z',;
    iif(this.value =3,'L',;
    iif(this.value =4,'P',;
    iif(this.value =5,'C',;
    iif(this.value =6,'G',;
    iif(this.value =7,'N',;
    space(1)))))))))
  endfunc
  func oNOTIPNOM_1_76.GetRadio()
    this.Parent.oContained.w_NOTIPNOM = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPNOM_1_76.SetRadio()
    this.Parent.oContained.w_NOTIPNOM=trim(this.Parent.oContained.w_NOTIPNOM)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPNOM=='T',1,;
      iif(this.Parent.oContained.w_NOTIPNOM=='Z',2,;
      iif(this.Parent.oContained.w_NOTIPNOM=='L',3,;
      iif(this.Parent.oContained.w_NOTIPNOM=='P',4,;
      iif(this.Parent.oContained.w_NOTIPNOM=='C',5,;
      iif(this.Parent.oContained.w_NOTIPNOM=='G',6,;
      iif(this.Parent.oContained.w_NOTIPNOM=='N',7,;
      0)))))))
  endfunc

  func oNOTIPNOM_1_76.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOPE<>'LOAD'   AND EMPTY(.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOTIPNOM_1_76.mHide()
    with this.Parent.oContained
      return (.w_PARCALLER='B' or (.w_PARCALLER='N' and .w_NOFLGBEN='B') OR .w_TIPOPE='LOAD' OR IsAlt())
    endwith
  endfunc


  add object oObj_1_107 as cp_runprogram with uid="XRJBLACLJR",left=0, top=598, width=172,height=22,;
    caption='GSAR_BOU',;
   bGlobalFont=.t.,;
    prg="GSAR_BOU",;
    cEvent = "New record",;
    nPag=1;
    , HelpContextID = 54726469


  add object oObj_1_108 as cp_runprogram with uid="KNCKMLMSHW",left=0, top=622, width=172,height=22,;
    caption='GSAR_BO1',;
   bGlobalFont=.t.,;
    prg="GSAR_BO1( w_NOCODICE, w_NOTIPO, w_FL_SOG_RIT, w_NOTIPNOM )",;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 54726505

  add object oDESNAZ1_1_126 as StdField with uid="KHAVALXEFI",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DESNAZ1", cQueryName = "DESNAZ1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 220593610,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=194, Top=183, InputMask=replicate('X',35)

  add object oCODISO1_1_127 as StdField with uid="TRNSXVVLVI",rtseq=83,rtrep=.f.,;
    cFormVar = "w_CODISO1", cQueryName = "CODISO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice ISO nazione",;
    HelpContextID = 118219738,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=415, Top=183, InputMask=replicate('X',3)


  add object oObj_1_128 as cp_runprogram with uid="NXTEGUHZST",left=177, top=599, width=172,height=22,;
    caption='GSAR_BBP',;
   bGlobalFont=.t.,;
    prg="GSAR_BBP",;
    cEvent = "controlli",;
    nPag=1;
    , HelpContextID = 54726474


  add object oObj_1_129 as cp_runprogram with uid="ZHFBUODMKZ",left=176, top=624, width=172,height=22,;
    caption='GSAR_Bb2',;
   bGlobalFont=.t.,;
    prg="GSAR_BB2",;
    cEvent = "CheckForm",;
    nPag=1;
    , HelpContextID = 54726504


  add object oBtn_1_130 as StdButton with uid="AWBAWYVKFB",left=632, top=33, width=48,height=45,;
    CpPicture="bmp\visualicat.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare gli allegati";
    , HelpContextID = 206748015;
    , tabstop=.f., caption='\<Allegati';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_130.Click()
      with this.Parent.oContained
        gsut_bcv(this.Parent.oContained,"OFF_NOMI",.w_NOCODICE,"GSAR_ANO","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_130.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_NOCODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_130.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (! IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_132 as StdButton with uid="KGTKXLZPSX",left=532, top=33, width=48,height=45,;
    CpPicture="BMP\VISUALI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare le immagini per il catalogo prodotti";
    , HelpContextID = 184756054;
    , tabstop=.f., caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_132.Click()
      with this.Parent.oContained
        gsut_bcv(this.Parent.oContained,"OFF_NOMI",.w_NOCODICE,"GSAR_ANO","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_132.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_NOCODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_132.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_133 as StdButton with uid="XWNHVNGPCB",left=582, top=33, width=48,height=45,;
    CpPicture="BMP\CATTURA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per associare una nuova immagine per il catalogo prodotti";
    , HelpContextID = 29217242;
    , tabstop=.f., caption='C\<attura',UID = 'CATTURA';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_133.Click()
      with this.Parent.oContained
        gsut_bcv(this.Parent.oContained,"OFF_NOMI",.w_NOCODICE,"GSAR_ANO","C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_133.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_NOCODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_133.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_134 as StdButton with uid="BEPDWBBHMD",left=632, top=33, width=48,height=45,;
    CpPicture="BMP\SCANNER.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per associare una nuova immagine per il catalogo prodotti";
    , HelpContextID = 255131354;
    , tabstop=.f., caption='\<Scanner',UID = 'SCANNER';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_134.Click()
      with this.Parent.oContained
        gsut_bcv(this.Parent.oContained,"OFF_NOMI",.w_NOCODICE,"GSAR_ANO","S",MROW(),MCOL(),.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_134.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_NOCODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_134.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DOCM<>'S' or IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_135 as StdButton with uid="NXCNXGUTLZ",left=630, top=495, width=48,height=45,;
    CpPicture="bmp\fattura.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai preventivi";
    , HelpContextID = 155435199;
    , Caption='\<Preventivi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_135.Click()
      with this.Parent.oContained
        GSAR_BO2(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_135.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NOCODICE))
      endwith
    endif
  endfunc

  func oBtn_1_135.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_NOCODICE) OR .cFunction='Load' OR !Isalt() OR .w_NOTIPNOM='F')
     endwith
    endif
  endfunc


  add object oBtn_1_136 as StdButton with uid="SALQXUYEGN",left=682, top=33, width=48,height=45,;
    CpPicture="bmp\log_pwd.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere all'anagrafica del fornitore";
    , HelpContextID = 8641768;
    , Caption='\<Fornitore';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_136.Click()
      with this.Parent.oContained
        GSAR_BO2(this.Parent.oContained,"N",,"F")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_136.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_NOCODCLI) AND .w_NOTIPCLI='F')
      endwith
    endif
  endfunc

  func oBtn_1_136.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_NOCODCLI)  OR .w_NOFLGBEN='B' OR .w_NOTIPCLI<>'F')
     endwith
    endif
  endfunc


  add object oBtn_1_137 as StdButton with uid="PVRNPREXQE",left=682, top=33, width=48,height=45,;
    CpPicture="bmp\log_pwd.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere all'anagrafica del cliente";
    , HelpContextID = 264407334;
    , Caption='\<Cliente';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_137.Click()
      with this.Parent.oContained
        GSAR_BO2(this.Parent.oContained,"N")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_137.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_NOCODCLI) AND .w_NOTIPCLI='C')
      endwith
    endif
  endfunc

  func oBtn_1_137.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_NOCODCLI)  OR .w_NOFLGBEN='B' OR .w_NOTIPCLI<>'C')
     endwith
    endif
  endfunc


  add object oBtn_1_138 as StdButton with uid="CKNQGJBKPP",left=681, top=495, width=48,height=45,;
    CpPicture="bmp\gestpc.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alle pratiche del nominativo";
    , HelpContextID = 243339355;
    , Caption='\<Pratiche';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_138.Click()
      with this.Parent.oContained
        do GSPR_BPR with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_138.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NOCODICE) AND g_PRAT='S')
      endwith
    endif
  endfunc

  func oBtn_1_138.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_PRAT<>'S' OR EMPTY(.w_NOCODICE) OR .cFunction='Load')
     endwith
    endif
  endfunc


  add object oBtn_1_139 as StdButton with uid="XYZLCDBLNU",left=731, top=495, width=48,height=45,;
    CpPicture="bmp\DM_agenda.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alle attivit� del nominativo";
    , HelpContextID = 88556006;
    , Caption='\<Attivit�';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_139.Click()
      with this.Parent.oContained
        GSAR_BO2(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_139.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NOCODICE) AND (g_AGEN='S' Or g_OFFE = 'S'))
      endwith
    endif
  endfunc

  func oBtn_1_139.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return ((g_AGEN<>'S' And g_OFFE <> 'S')  OR EMPTY(.w_NOCODICE) OR .cFunction='Load' OR !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc


  add object oBtn_1_140 as StdButton with uid="XEUWBAPBCE",left=756, top=421, width=22,height=22,;
    CpPicture="BMP\BTSEND.bmp", caption="", nPag=1;
    , ToolTipText = "Manda una e-mail all'indirizzo specificato";
    , HelpContextID = 195430982;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_140.Click()
      with this.Parent.oContained
        MAILTO(.w_NO_EMAIL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_140.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_NO_EMAIL))
      endwith
    endif
  endfunc

  func oBtn_1_140.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_NO_EMAIL))
     endwith
    endif
  endfunc


  add object oBtn_1_141 as StdButton with uid="PZKLYNNGQZ",left=317, top=444, width=22,height=22,;
    CpPicture="BMP\ROOT.BMP", caption="", nPag=1;
    , ToolTipText = "Apre il browser all'indirizzo web specificato";
    , HelpContextID = 75396966;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_141.Click()
      with this.Parent.oContained
        viewFile(.w_NOINDWEB, .T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_141.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_NOINDWEB))
      endwith
    endif
  endfunc

  func oBtn_1_141.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_NOINDWEB))
     endwith
    endif
  endfunc


  add object oObj_1_147 as cp_runprogram with uid="URCTFRVZCJ",left=0, top=643, width=172,height=22,;
    caption='GSAR_BO1',;
   bGlobalFont=.t.,;
    prg="GSAR_BO1( w_NOCODICE, w_NOTIPO, w_FL_SOG_RIT, w_NOTIPNOM )",;
    cEvent = "Record Inserted",;
    nPag=1;
    , HelpContextID = 54726505


  add object oObj_1_162 as cp_runprogram with uid="YTXCYIWILY",left=0, top=664, width=172,height=22,;
    caption='GSAR_BKC',;
   bGlobalFont=.t.,;
    prg="GSAR_BKC",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 213708969


  add object oBtn_1_163 as StdButton with uid="YXOXMAGUJB",left=732, top=33, width=48,height=45,;
    CpPicture="bmp\ImportCF.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare i dati anagrafici dal codice fiscale";
    , HelpContextID = 102612458;
    , tabstop=.f., caption='Cod.\<Fisc';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_163.Click()
      with this.Parent.oContained
        iif(AH_yesno("Si vuole aggiornare sesso, luogo, provincia e data di nascita in base al codice fiscale?"), .notifyevent("AggiornaCF"),'')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_163.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_NOCODFIS) AND .w_NOSOGGET<>'EN')
      endwith
    endif
  endfunc

  func oBtn_1_163.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (ISALT())
     endwith
    endif
  endfunc


  add object oBtn_1_168 as StdButton with uid="GZBWLHZOEO",left=268, top=398, width=22,height=22,;
    CpPicture="BMP\PHONE.bmp", caption="", nPag=1;
    , ToolTipText = "Permette di effettuare chiamate al numero indicato";
    , HelpContextID = 188648230;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_168.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_NOTELEFO, "MN", .w_NOCODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_168.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_NOTELEFO) And (g_SkypeService='C' Or !Empty(g_PhoneService)))
      endwith
    endif
  endfunc

  func oBtn_1_168.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_NOTELEFO) Or (g_SkypeService<>'C' And Empty(g_PhoneService)))
     endwith
    endif
  endfunc


  add object oBtn_1_169 as StdButton with uid="XHPGOLSVZY",left=526, top=398, width=22,height=22,;
    CpPicture="BMP\PHONE.bmp", caption="", nPag=1;
    , ToolTipText = "Permette di effettuare chiamate al numero indicato";
    , HelpContextID = 188648230;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_169.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_NONUMCEL, "MM", .w_NOCODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_169.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_NONUMCEL) And (g_SkypeService='C' Or !Empty(g_PhoneService)))
      endwith
    endif
  endfunc

  func oBtn_1_169.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_NONUMCEL) Or (g_SkypeService<>'C' And Empty(g_PhoneService)))
     endwith
    endif
  endfunc


  add object oBtn_1_170 as StdButton with uid="OUBHQIRZJT",left=756, top=397, width=22,height=22,;
    CpPicture="BMP\SKYPE.bmp", caption="", nPag=1;
    , ToolTipText = "Permette di effettuare chiamate via Skype al numero indicato";
    , HelpContextID = 188648230;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_170.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_NO_SKYPE, "CC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_170.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_NO_SKYPE) AND g_SkypeService<>'N')
      endwith
    endif
  endfunc

  func oBtn_1_170.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_NO_SKYPE) OR g_SkypeService='N')
     endwith
    endif
  endfunc


  add object oBtn_1_180 as StdButton with uid="PROFXLUGBZ",left=756, top=446, width=22,height=22,;
    CpPicture="BMP\fxmail_pec.bmp", caption="", nPag=1;
    , ToolTipText = "Manda una PEC all'indirizzo specificato";
    , HelpContextID = 75170550;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_180.Click()
      with this.Parent.oContained
        MAILTO(.w_NO_EMPEC,,,.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_180.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_NO_EMPEC))
      endwith
    endif
  endfunc

  func oBtn_1_180.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_NO_EMPEC))
     endwith
    endif
  endfunc

  add object oStr_1_68 as StdString with uid="OOCVUAVGLQ",Visible=.t., Left=44, Top=270,;
    Alignment=1, Width=77, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="PYPSGNKPTO",Visible=.t., Left=40, Top=398,;
    Alignment=1, Width=81, Height=15,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="RMVPWBZJFX",Visible=.t., Left=45, Top=421,;
    Alignment=1, Width=76, Height=15,;
    Caption="Telefax:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="NNCFPFBBKL",Visible=.t., Left=611, Top=296,;
    Alignment=1, Width=78, Height=15,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="ZVMLVFVZGC",Visible=.t., Left=311, Top=398,;
    Alignment=1, Width=70, Height=15,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="TIFSTQZFZS",Visible=.t., Left=17, Top=445,;
    Alignment=1, Width=104, Height=15,;
    Caption="Internet web:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="CPMEHQSUCC",Visible=.t., Left=289, Top=421,;
    Alignment=1, Width=92, Height=18,;
    Caption="E-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="CDRHFGWYSP",Visible=.t., Left=9, Top=295,;
    Alignment=1, Width=112, Height=15,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="LEOBLCIIEJ",Visible=.t., Left=493, Top=295,;
    Alignment=1, Width=56, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="EKKEGXNWKT",Visible=.t., Left=25, Top=520,;
    Alignment=1, Width=96, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_88 as StdString with uid="CVMTTHKVWA",Visible=.t., Left=228, Top=520,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="ZJGCAZEGYZ",Visible=.t., Left=4, Top=378,;
    Alignment=0, Width=97, Height=18,;
    Caption="Comunicazioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="RCGOPLUALE",Visible=.t., Left=4, Top=473,;
    Alignment=0, Width=157, Height=18,;
    Caption="Processi documentali"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oStr_1_94 as StdString with uid="DQUQSNPAOU",Visible=.t., Left=4, Top=249,;
    Alignment=0, Width=97, Height=18,;
    Caption="Residenza"  ;
  , bGlobalFont=.t.

  func oStr_1_94.mHide()
    with this.Parent.oContained
      return ( .w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_1_95 as StdString with uid="ZDXIDBBOMT",Visible=.t., Left=43, Top=330,;
    Alignment=1, Width=77, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_96 as StdString with uid="ZVQLUXFEWH",Visible=.t., Left=611, Top=355,;
    Alignment=1, Width=78, Height=15,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_97 as StdString with uid="CRLFGTAYTJ",Visible=.t., Left=8, Top=355,;
    Alignment=1, Width=112, Height=15,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="CPAHSRNOVE",Visible=.t., Left=492, Top=355,;
    Alignment=1, Width=56, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_100 as StdString with uid="XBIOGMYILZ",Visible=.t., Left=4, Top=309,;
    Alignment=0, Width=97, Height=18,;
    Caption="Domicilio"  ;
  , bGlobalFont=.t.

  add object oStr_1_101 as StdString with uid="HZAPHSORFT",Visible=.t., Left=4, Top=249,;
    Alignment=0, Width=97, Height=18,;
    Caption="Sede legale"  ;
  , bGlobalFont=.t.

  func oStr_1_101.mHide()
    with this.Parent.oContained
      return ( .w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oStr_1_109 as StdString with uid="PQEFGKMSRW",Visible=.t., Left=50, Top=7,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_110 as StdString with uid="EWPMRFVONT",Visible=.t., Left=21, Top=208,;
    Alignment=1, Width=102, Height=15,;
    Caption="Cod. fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_111 as StdString with uid="ENMIZNDLXT",Visible=.t., Left=53, Top=158,;
    Alignment=1, Width=69, Height=15,;
    Caption="Nato a:"  ;
  , bGlobalFont=.t.

  add object oStr_1_112 as StdString with uid="SCJLHGANON",Visible=.t., Left=569, Top=158,;
    Alignment=1, Width=31, Height=15,;
    Caption="Il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_113 as StdString with uid="KUUSXZBUKT",Visible=.t., Left=366, Top=158,;
    Alignment=1, Width=42, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_114 as StdString with uid="NXZSESAJZM",Visible=.t., Left=59, Top=134,;
    Alignment=1, Width=64, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_115 as StdString with uid="SPJAMZFENN",Visible=.t., Left=556, Top=135,;
    Alignment=1, Width=44, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_116 as StdString with uid="CWGTGSXYAJ",Visible=.t., Left=480, Top=183,;
    Alignment=1, Width=120, Height=15,;
    Caption="Carta di identit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_117 as StdString with uid="EQOEIMKRTQ",Visible=.t., Left=439, Top=7,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_1_117.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oStr_1_118 as StdString with uid="RGQBIMXKTF",Visible=.t., Left=54, Top=108,;
    Alignment=1, Width=69, Height=18,;
    Caption="Persona:"  ;
  , bGlobalFont=.t.

  func oStr_1_118.mHide()
    with this.Parent.oContained
      return ( .w_NOFLGBEN<>'B' OR  .w_NOSOGGET='EN' or .w_NOTIPPER='A')
    endwith
  endfunc

  add object oStr_1_119 as StdString with uid="XYNUBRPNRR",Visible=.t., Left=384, Top=108,;
    Alignment=1, Width=69, Height=18,;
    Caption="Badge:"  ;
  , bGlobalFont=.t.

  func oStr_1_119.mHide()
    with this.Parent.oContained
      return ( .w_NOFLGBEN<>'B'  OR  .w_NOSOGGET='EN' or .w_NOTIPPER='A')
    endwith
  endfunc

  add object oStr_1_120 as StdString with uid="VRSBHYCUEF",Visible=.t., Left=259, Top=7,;
    Alignment=1, Width=63, Height=18,;
    Caption="Soggetto:"  ;
  , bGlobalFont=.t.

  func oStr_1_120.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_121 as StdString with uid="RUOBRCYSZE",Visible=.t., Left=33, Top=84,;
    Alignment=1, Width=90, Height=18,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_122 as StdString with uid="TOXWUYSPAL",Visible=.t., Left=591, Top=7,;
    Alignment=1, Width=66, Height=18,;
    Caption="Valutazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_122.mHide()
    with this.Parent.oContained
      return (.w_PARCALLER='B' or (.w_PARCALLER='N' and .w_NOFLGBEN='B') OR IsAlt())
    endwith
  endfunc

  add object oStr_1_123 as StdString with uid="BORASHHCMV",Visible=.t., Left=49, Top=233,;
    Alignment=1, Width=74, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_124 as StdString with uid="STORSESFCQ",Visible=.t., Left=63, Top=37,;
    Alignment=1, Width=59, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_124.mHide()
    with this.Parent.oContained
      return (.w_PARCALLER='B' Or .w_NOFLGBEN='B' OR .w_NOTIPNOM='F')
    endwith
  endfunc

  add object oStr_1_125 as StdString with uid="ZXYHMGRYNB",Visible=.t., Left=0, Top=183,;
    Alignment=1, Width=123, Height=18,;
    Caption="Nazione di nascita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_142 as StdString with uid="KGWBAABAUN",Visible=.t., Left=334, Top=233,;
    Alignment=1, Width=122, Height=18,;
    Caption="Formula di cortesia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_144 as StdString with uid="TYMWSPMMOZ",Visible=.t., Left=626, Top=7,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_1_144.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_1_167 as StdString with uid="ACUELWKBTT",Visible=.t., Left=550, Top=399,;
    Alignment=1, Width=51, Height=18,;
    Caption="Skype:"  ;
  , bGlobalFont=.t.

  add object oStr_1_177 as StdString with uid="LIVJITKWXK",Visible=.t., Left=310, Top=208,;
    Alignment=1, Width=146, Height=18,;
    Caption="Natura giuridica Pct:"  ;
  , bGlobalFont=.t.

  func oStr_1_177.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_1_178 as StdString with uid="MEGDEQAMOG",Visible=.t., Left=63, Top=37,;
    Alignment=1, Width=59, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_178.mHide()
    with this.Parent.oContained
      return (.w_PARCALLER='B' Or .w_NOFLGBEN='B' Or .w_NOTIPNOM<>'F')
    endwith
  endfunc

  add object oStr_1_179 as StdString with uid="PBKTNAFZNR",Visible=.t., Left=342, Top=446,;
    Alignment=1, Width=39, Height=18,;
    Caption="PEC:"  ;
  , bGlobalFont=.t.

  add object oBox_1_89 as StdBox with uid="KXLDJCWPHP",left=3, top=395, width=768,height=1

  add object oBox_1_92 as StdBox with uid="SSYEZXKUZO",left=3, top=488, width=768,height=1

  add object oBox_1_93 as StdBox with uid="JCAHILQLQY",left=3, top=264, width=768,height=1

  add object oBox_1_99 as StdBox with uid="NFJGNVXXBW",left=3, top=324, width=768,height=1
enddefine
define class tgsar_anoPag2 as StdContainer
  Width  = 783
  height = 544
  stdWidth  = 783
  stdheight = 544
  resizeXpos=7
  resizeYpos=304
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oNOTIPCON_2_2 as StdCombo with uid="PCPLAVVMFM",rtseq=100,rtrep=.f.,left=148,top=53,width=107,height=21;
    , ToolTipText = "Tipo conto";
    , HelpContextID = 54190812;
    , cFormVar="w_NOTIPCON",RowSource=""+"Cliente,"+"Fornitore,"+"Generico", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNOTIPCON_2_2.RadioValue()
    return(iif(this.value =1,"C",;
    iif(this.value =2,"F",;
    iif(this.value =3,"G",;
    space(1)))))
  endfunc
  func oNOTIPCON_2_2.GetRadio()
    this.Parent.oContained.w_NOTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPCON_2_2.SetRadio()
    this.Parent.oContained.w_NOTIPCON=trim(this.Parent.oContained.w_NOTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPCON=="C",1,;
      iif(this.Parent.oContained.w_NOTIPCON=="F",2,;
      iif(this.Parent.oContained.w_NOTIPCON=="G",3,;
      0)))
  endfunc

  func oNOTIPCON_2_2.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  func oNOTIPCON_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_NOCODCON)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oNOCODCON_2_3 as StdField with uid="HIJSXONBAZ",rtseq=101,rtrep=.f.,;
    cFormVar = "w_NOCODCON", cQueryName = "NOCODCON",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente,obsoleto,con gestione ritenute attivata oppure non gestito a partite",;
    ToolTipText = "Codice del conto",;
    HelpContextID = 66450140,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=260, Top=53, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_NOTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_NOCODCON"

  func oNOCODCON_2_3.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  func oNOCODCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODCON_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODCON_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_NOTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_NOTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oNOCODCON_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori/conti (partite)",'GSAR_DCD.CONTI_VZM',this.parent.oContained
  endproc
  proc oNOCODCON_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_NOTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_NOCODCON
     i_obj.ecpSave()
  endproc

  add object oNOCODPAG_2_4 as StdField with uid="FZQGLGXRBK",rtseq=102,rtrep=.f.,;
    cFormVar = "w_NOCODPAG", cQueryName = "NOCODPAG",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice del pagamento praticato normalmente al cliente",;
    HelpContextID = 116781795,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=148, Top=78, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_NOCODPAG"

  func oNOCODPAG_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODPAG_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODPAG_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oNOCODPAG_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oNOCODPAG_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_NOCODPAG
     i_obj.ecpSave()
  endproc

  add object oDESPAG_2_5 as StdField with uid="VARKZBHVFI",rtseq=103,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 2358730,;
   bGlobalFont=.t.,;
    Height=21, Width=226, Left=223, Top=78, InputMask=replicate('X',30)

  add object oNOGIOFIS_2_6 as StdField with uid="KGXVNFHXGD",rtseq=104,rtrep=.f.,;
    cFormVar = "w_NOGIOFIS", cQueryName = "NOGIOFIS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Eventuale giorno fisso di scadenza",;
    HelpContextID = 263474473,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=580, Top=78, cSayPict='"99"', cGetPict='"99"'

  func oNOGIOFIS_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NOGIOFIS <= 31)
    endwith
    return bRes
  endfunc

  add object oNO1MESCL_2_7 as StdField with uid="AYFCYOFUAO",rtseq=105,rtrep=.f.,;
    cFormVar = "w_NO1MESCL", cQueryName = "NO1MESCL",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Primo mese in cui le scadenze sono rinviate al mese successivo",;
    HelpContextID = 65606366,;
   bGlobalFont=.t.,;
    Height=21, Width=29, Left=148, Top=101, cSayPict='"99"', cGetPict='"99"'

  func oNO1MESCL_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NO1MESCL<13 AND (.w_NO1MESCL<.w_NO2MESCL OR .w_NO2MESCL=0))
    endwith
    return bRes
  endfunc

  add object oDESMES1_2_8 as StdField with uid="NRPKACWDWA",rtseq=106,rtrep=.f.,;
    cFormVar = "w_DESMES1", cQueryName = "DESMES1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 65469898,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=185, Top=101, InputMask=replicate('X',12)

  add object oNOGIOSC1_2_9 as StdField with uid="PXMVSCXOOT",rtseq=107,rtrep=.f.,;
    cFormVar = "w_NOGIOSC1", cQueryName = "NOGIOSC1",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorno del mese successivo al 1^mese escluso a cui viene rinviata la scadenza",;
    HelpContextID = 55292665,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=371, Top=101, cSayPict='"99"', cGetPict='"99"'

  func oNOGIOSC1_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NO1MESCL>0)
    endwith
   endif
  endfunc

  func oNOGIOSC1_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NOGIOSC1 <= 31)
    endwith
    return bRes
  endfunc

  add object oNO2MESCL_2_10 as StdField with uid="TWFTESSEQW",rtseq=108,rtrep=.f.,;
    cFormVar = "w_NO2MESCL", cQueryName = "NO2MESCL",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Secondo mese in cui le scadenze sono rinviate al mese successivo",;
    HelpContextID = 65602270,;
   bGlobalFont=.t.,;
    Height=21, Width=29, Left=514, Top=101, cSayPict='"99"', cGetPict='"99"'

  func oNO2MESCL_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NO1MESCL > 0)
    endwith
   endif
  endfunc

  func oNO2MESCL_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NO2MESCL<13 AND (.w_NO1MESCL<.w_NO2MESCL OR .w_NO2MESCL=0))
    endwith
    return bRes
  endfunc

  add object oNOGIOSC2_2_11 as StdField with uid="UJRGGPFKCG",rtseq=109,rtrep=.f.,;
    cFormVar = "w_NOGIOSC2", cQueryName = "NOGIOSC2",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorno del mese successivo al 2^mese escluso a cui viene rinviata la scadenza",;
    HelpContextID = 55292664,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=731, Top=101, cSayPict='"99"', cGetPict='"99"'

  func oNOGIOSC2_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NO2MESCL>0)
    endwith
   endif
  endfunc

  func oNOGIOSC2_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NOGIOSC2 <= 31)
    endwith
    return bRes
  endfunc

  add object oDESMES2_2_12 as StdField with uid="ECHYROVYMP",rtseq=110,rtrep=.f.,;
    cFormVar = "w_DESMES2", cQueryName = "DESMES2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 65469898,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=545, Top=101, InputMask=replicate('X',12)

  add object oNOCODBAN_2_13 as StdField with uid="HOHLBIKLSN",rtseq=111,rtrep=.f.,;
    cFormVar = "w_NOCODBAN", cQueryName = "NOCODBAN",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca di appoggio di deb/cred. diversi",;
    HelpContextID = 83227356,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=148, Top=126, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_NOCODBAN"

  func oNOCODBAN_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODBAN_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODBAN_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oNOCODBAN_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oNOCODBAN_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_NOCODBAN
     i_obj.ecpSave()
  endproc


  add object oObj_2_27 as cp_runprogram with uid="QQLMSUMQRR",left=1, top=567, width=236,height=19,;
    caption='CHKDTOBSO',;
   bGlobalFont=.t.,;
    prg="CHKDTOBS(w_DATOBSOBA2,w_OBTEST,'Conto obsoleto !')",;
    cEvent = "controlladata",;
    nPag=2;
    , HelpContextID = 117470615

  add object oDESBA2_2_29 as StdField with uid="ITHXVBLSSK",rtseq=118,rtrep=.f.,;
    cFormVar = "w_DESBA2", cQueryName = "DESBA2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(42), bMultilanguage =  .f.,;
    HelpContextID = 87162314,;
   bGlobalFont=.t.,;
    Height=21, Width=264, Left=321, Top=436, InputMask=replicate('X',42)


  add object oLinkPC_2_30 as stdDynamicChildContainer with uid="ULXRVQYODV",left=4, top=154, width=770, height=275, bOnScreen=.t.;


  add object oDESBAN_2_34 as StdField with uid="WHLEQMTXIK",rtseq=119,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(42), bMultilanguage =  .f.,;
    HelpContextID = 154271178,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=264, Top=126, InputMask=replicate('X',42)

  add object oNOCODICE_2_35 as StdField with uid="HZLRFPVVAJ",rtseq=120,rtrep=.f.,;
    cFormVar = "w_NOCODICE", cQueryName = "NOCODICE",enabled=.f.,;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice deb/cred. diversi",;
    HelpContextID = 234222309,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=150, Left=148, Top=4, InputMask=replicate('X',15)

  func oNOCODICE_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CONTINI)
        bRes2=.link_7_15('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oANDESCRI_2_36 as StdField with uid="LWTPSFVVIU",rtseq=121,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del conto",;
    HelpContextID = 51373233,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=355, Left=405, Top=53, InputMask=replicate('X',40)

  func oANDESCRI_2_36.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc


  add object oNOTIPPER_2_38 as StdCombo with uid="LKACRUPNUN",rtseq=122,rtrep=.f.,left=485,top=4,width=107,height=21, enabled=.f.;
    , ToolTipText = "Tipo persona (dipendente, collaboratore, altro)";
    , HelpContextID = 163913000;
    , cFormVar="w_NOTIPPER",RowSource=""+"Dipendente,"+"Collaboratore,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNOTIPPER_2_38.RadioValue()
    return(iif(this.value =1,"D",;
    iif(this.value =2,"C",;
    iif(this.value =3,"A",;
    space(1)))))
  endfunc
  func oNOTIPPER_2_38.GetRadio()
    this.Parent.oContained.w_NOTIPPER = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPPER_2_38.SetRadio()
    this.Parent.oContained.w_NOTIPPER=trim(this.Parent.oContained.w_NOTIPPER)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPPER=="D",1,;
      iif(this.Parent.oContained.w_NOTIPPER=="C",2,;
      iif(this.Parent.oContained.w_NOTIPPER=="A",3,;
      0)))
  endfunc

  func oNOTIPPER_2_38.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oBANCAAHE_2_43 as StdField with uid="DZZQDXAMPH",rtseq=126,rtrep=.f.,;
    cFormVar = "w_BANCAAHE", cQueryName = "BANCAAHE",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Ns. C/Corrente, comunicato al cliente per incasso bonifici",;
    HelpContextID = 164539995,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=185, Top=436, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACC", oKey_1_1="BANUMCOR", oKey_1_2="this.w_BANCAAHE"

  func oBANCAAHE_2_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TESO='S')
    endwith
   endif
  endfunc

  func oBANCAAHE_2_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_43('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANCAAHE_2_43.ecpDrop(oSource)
    this.Parent.oContained.link_2_43('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANCAAHE_2_43.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BANUMCOR',cp_AbsName(this.parent,'oBANCAAHE_2_43'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACC',"Elenco conti banche",'',this.parent.oContained
  endproc
  proc oBANCAAHE_2_43.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BANUMCOR=this.parent.oContained.w_BANCAAHE
     i_obj.ecpSave()
  endproc

  add object oNOCATFIN_2_44 as StdField with uid="YUXDGCAVIX",rtseq=127,rtrep=.f.,;
    cFormVar = "w_NOCATFIN", cQueryName = "NOCATFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "E' possibile selezionare solo categorie di tipo conti",;
    ToolTipText = "Categoria finanziaria",;
    HelpContextID = 268176676,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=185, Top=461, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_FINA", cZoomOnZoom="Gste_Acf", oKey_1_1="CFTIPCAT", oKey_1_2="this.w_TIPOFIN", oKey_2_1="CFCODICE", oKey_2_2="this.w_NOCATFIN"

  func oNOCATFIN_2_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCATFIN_2_44.ecpDrop(oSource)
    this.Parent.oContained.link_2_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCATFIN_2_44.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAT_FINA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCAT="+cp_ToStrODBC(this.Parent.oContained.w_TIPOFIN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCAT="+cp_ToStr(this.Parent.oContained.w_TIPOFIN)
    endif
    do cp_zoom with 'CAT_FINA','*','CFTIPCAT,CFCODICE',cp_AbsName(this.parent,'oNOCATFIN_2_44'),iif(empty(i_cWhere),.f.,i_cWhere),'Gste_Acf',"Categorie finanziarie",'',this.parent.oContained
  endproc
  proc oNOCATFIN_2_44.mZoomOnZoom
    local i_obj
    i_obj=Gste_Acf()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CFTIPCAT=w_TIPOFIN
     i_obj.w_CFCODICE=this.parent.oContained.w_NOCATFIN
     i_obj.ecpSave()
  endproc

  add object oNOCATDER_2_45 as StdField with uid="EXSNNIFYMV",rtseq=128,rtrep=.f.,;
    cFormVar = "w_NOCATDER", cQueryName = "NOCATDER",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "E' possibile selezionare solo categorie di tipo deroghe",;
    ToolTipText = "Categoria calcolo giorni",;
    HelpContextID = 33813208,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=185, Top=488, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_FINA", cZoomOnZoom="Gste_Acf", oKey_1_1="CFTIPCAT", oKey_1_2="this.w_TIPOFIND", oKey_2_1="CFCODICE", oKey_2_2="this.w_NOCATDER"

  func oNOCATDER_2_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCATDER_2_45.ecpDrop(oSource)
    this.Parent.oContained.link_2_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCATDER_2_45.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAT_FINA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCAT="+cp_ToStrODBC(this.Parent.oContained.w_TIPOFIND)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CFTIPCAT="+cp_ToStr(this.Parent.oContained.w_TIPOFIND)
    endif
    do cp_zoom with 'CAT_FINA','*','CFTIPCAT,CFCODICE',cp_AbsName(this.parent,'oNOCATDER_2_45'),iif(empty(i_cWhere),.f.,i_cWhere),'Gste_Acf',"Categorie finanziarie",'',this.parent.oContained
  endproc
  proc oNOCATDER_2_45.mZoomOnZoom
    local i_obj
    i_obj=Gste_Acf()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CFTIPCAT=w_TIPOFIND
     i_obj.w_CFCODICE=this.parent.oContained.w_NOCATDER
     i_obj.ecpSave()
  endproc

  add object oNOIDRIDY_2_46 as StdCheck with uid="BDWUZIRLUH",rtseq=129,rtrep=.f.,left=16, top=521, caption="Id. bancario RID",;
    ToolTipText = "Check id RID alternativo",;
    HelpContextID = 220238545,;
    cFormVar="w_NOIDRIDY", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oNOIDRIDY_2_46.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOIDRIDY_2_46.GetRadio()
    this.Parent.oContained.w_NOIDRIDY = this.RadioValue()
    return .t.
  endfunc

  func oNOIDRIDY_2_46.SetRadio()
    this.Parent.oContained.w_NOIDRIDY=trim(this.Parent.oContained.w_NOIDRIDY)
    this.value = ;
      iif(this.Parent.oContained.w_NOIDRIDY=='S',1,;
      0)
  endfunc


  add object oNOTIIDRI_2_47 as StdCombo with uid="YNSIVZVOGR",rtseq=130,rtrep=.f.,left=186,top=521,width=154,height=21;
    , ToolTipText = "Tipo identificativo RID";
    , HelpContextID = 44753633;
    , cFormVar="w_NOTIIDRI",RowSource=""+"Utenza,"+"Matricola,"+"Codice fiscale,"+"Portafoglio commerciale,"+"Altro", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNOTIIDRI_2_47.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,6,;
    iif(this.value =5,9,;
    0))))))
  endfunc
  func oNOTIIDRI_2_47.GetRadio()
    this.Parent.oContained.w_NOTIIDRI = this.RadioValue()
    return .t.
  endfunc

  func oNOTIIDRI_2_47.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_NOTIIDRI==1,1,;
      iif(this.Parent.oContained.w_NOTIIDRI==2,2,;
      iif(this.Parent.oContained.w_NOTIIDRI==3,3,;
      iif(this.Parent.oContained.w_NOTIIDRI==6,4,;
      iif(this.Parent.oContained.w_NOTIIDRI==9,5,;
      0)))))
  endfunc

  func oNOTIIDRI_2_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOIDRIDY='S')
    endwith
   endif
  endfunc

  add object oNOCOIDRI_2_48 as StdField with uid="QFPSQSEFKX",rtseq=131,rtrep=.f.,;
    cFormVar = "w_NOCOIDRI", cQueryName = "NOCOIDRI",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Identificativo RID",;
    HelpContextID = 44430049,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=411, Top=520, InputMask=replicate('X',16)

  func oNOCOIDRI_2_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOIDRIDY='S')
    endwith
   endif
  endfunc

  add object oNOCOGNOM_2_59 as StdField with uid="YMGDXCFPOS",rtseq=137,rtrep=.f.,;
    cFormVar = "w_NOCOGNOM", cQueryName = "NOCOGNOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 147190493,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=148, Top=28, InputMask=replicate('X',50)

  func oNOCOGNOM_2_59.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNO__NOME_2_60 as StdField with uid="LGVMNJKXED",rtseq=138,rtrep=.f.,;
    cFormVar = "w_NO__NOME", cQueryName = "NO__NOME",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 121909989,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=465, Top=28, InputMask=replicate('X',50)

  func oNO__NOME_2_60.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNODESCRI_2_63 as StdField with uid="ORNJQCBSLN",rtseq=139,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI,NODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Esatta ragione sociale",;
    HelpContextID = 51372769,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=148, Top=28, InputMask=replicate('X',40)

  func oNODESCRI_2_63.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oNODESCR2_2_64 as StdField with uid="EMKTBKVXYK",rtseq=140,rtrep=.f.,;
    cFormVar = "w_NODESCR2", cQueryName = "NODESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 51372792,;
   bGlobalFont=.t.,;
    Height=21, Width=355, Left=405, Top=28, InputMask=replicate('X',40)

  func oNODESCR2_2_64.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oCFDESCRIC_2_72 as StdField with uid="FIXCNHTYDL",rtseq=143,rtrep=.f.,;
    cFormVar = "w_CFDESCRIC", cQueryName = "CFDESCRIC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 51374177,;
   bGlobalFont=.t.,;
    Height=21, Width=264, Left=321, Top=461, InputMask=replicate('X',30)

  add object oCFDESCRID_2_73 as StdField with uid="VKHGZLQZOT",rtseq=144,rtrep=.f.,;
    cFormVar = "w_CFDESCRID", cQueryName = "CFDESCRID",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 51374161,;
   bGlobalFont=.t.,;
    Height=21, Width=264, Left=321, Top=488, InputMask=replicate('X',30)

  add object oStr_2_17 as StdString with uid="QQNPEHZDAB",Visible=.t., Left=76, Top=4,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_19 as StdString with uid="WSEVDQLBZA",Visible=.t., Left=26, Top=78,;
    Alignment=1, Width=122, Height=15,;
    Caption="Cod. pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="LTXHPBKTTH",Visible=.t., Left=40, Top=103,;
    Alignment=1, Width=108, Height=15,;
    Caption="1^ Mese escluso:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="WCKCCYHKTK",Visible=.t., Left=404, Top=103,;
    Alignment=1, Width=108, Height=15,;
    Caption="2^ Mese escluso:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="OIZLHODXUW",Visible=.t., Left=287, Top=103,;
    Alignment=1, Width=82, Height=15,;
    Caption="Rinvio al:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="REQZJLMEVU",Visible=.t., Left=646, Top=103,;
    Alignment=1, Width=82, Height=15,;
    Caption="Rinvio al:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="DYOQYGVRWR",Visible=.t., Left=474, Top=78,;
    Alignment=1, Width=101, Height=15,;
    Caption="Giorno fisso:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="QZQXFXFLEN",Visible=.t., Left=77, Top=438,;
    Alignment=1, Width=105, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="AGEYZVGWYP",Visible=.t., Left=2, Top=126,;
    Alignment=1, Width=146, Height=18,;
    Caption="Banca deb/cred. diversi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="MTNMUWGWVB",Visible=.t., Left=99, Top=53,;
    Alignment=1, Width=49, Height=15,;
    Caption="Conto:"    , backcolor=rgb(200,0,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_37.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oStr_2_39 as StdString with uid="RAKREJXYQS",Visible=.t., Left=448, Top=4,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_2_39.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oStr_2_49 as StdString with uid="WTTPBZPEQP",Visible=.t., Left=150, Top=524,;
    Alignment=1, Width=30, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="HJAAOXERKN",Visible=.t., Left=359, Top=524,;
    Alignment=1, Width=45, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_2_61 as StdString with uid="UCGKCALGIB",Visible=.t., Left=65, Top=28,;
    Alignment=1, Width=83, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_2_61.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_2_62 as StdString with uid="OKQZNLSDJY",Visible=.t., Left=417, Top=28,;
    Alignment=1, Width=44, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_2_62.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_2_65 as StdString with uid="CXCZKJIDTN",Visible=.t., Left=58, Top=28,;
    Alignment=1, Width=90, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  func oStr_2_65.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oStr_2_70 as StdString with uid="WJPMAFNBXG",Visible=.t., Left=22, Top=463,;
    Alignment=1, Width=160, Height=18,;
    Caption="Categoria finanziaria:"  ;
  , bGlobalFont=.t.

  add object oStr_2_71 as StdString with uid="OFDQMGMPLS",Visible=.t., Left=22, Top=491,;
    Alignment=1, Width=160, Height=18,;
    Caption="Categoria calcolo giorni:"  ;
  , bGlobalFont=.t.

  add object oBox_2_31 as StdBox with uid="VITZMVGGIZ",left=68, top=151, width=698,height=2

  add object oBox_2_32 as StdBox with uid="GTVZKKHSFT",left=0, top=431, width=766,height=2
enddefine
define class tgsar_anoPag3 as StdContainer
  Width  = 783
  height = 544
  stdWidth  = 783
  stdheight = 544
  resizeXpos=425
  resizeYpos=356
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNONOMFIL_3_1 as StdField with uid="YISHMHSPJX",rtseq=147,rtrep=.f.,;
    cFormVar = "w_NONOMFIL", cQueryName = "",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di ricerca nome o link del file",;
    HelpContextID = 261799202,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=141, Top=56, InputMask=replicate('X',30)

  func oNONOMFIL_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oNOOGGE_3_2 as StdField with uid="EAWGGJCHXT",rtseq=148,rtrep=.f.,;
    cFormVar = "w_NOOGGE", cQueryName = "",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di ricerca oggetto allegato",;
    HelpContextID = 30225194,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=141, Top=81, InputMask=replicate('X',30)

  func oNOOGGE_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oNONOTE_3_3 as StdField with uid="UPJCWTTCTQ",rtseq=149,rtrep=.f.,;
    cFormVar = "w_NONOTE", cQueryName = "",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stringa di ricerca note allegato",;
    HelpContextID = 16073514,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=449, Top=81, InputMask=replicate('X',30)

  func oNONOTE_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc


  add object ZoomAll as cp_zoombox with uid="UVDGIFWGIJ",left=5, top=110, width=768,height=367,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSOF_ANM",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="ALL_EGAT",bRetriveAllRows=.f.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.t.,;
    cEvent = "CalcolaAllegati,Load",;
    nPag=3;
    , HelpContextID = 15560730


  add object oBtn_3_5 as StdButton with uid="OEXSBFSQXC",left=17, top=480, width=48,height=45,;
    CpPicture="bmp\VISUALI.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per aprire il documento allegato";
    , HelpContextID = 82255110;
    , Caption='\<Apri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_5.Click()
      with this.Parent.oContained
        GSOF_BGN(this.Parent.oContained,"APRI_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY( .w_PATALL ))
      endwith
    endif
  endfunc

  func oBtn_3_5.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc


  add object oBtn_3_6 as StdButton with uid="SQPKZTQSOU",left=68, top=480, width=48,height=45,;
    CpPicture="bmp\Modifica.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per modificare i riferimenti del documento allegato";
    , HelpContextID = 71712039;
    , Caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_6.Click()
      with this.Parent.oContained
        GSOF_BGN(this.Parent.oContained,"VARIA_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_SERALL))
      endwith
    endif
  endfunc

  func oBtn_3_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc


  add object oBtn_3_7 as StdButton with uid="KMDXFIKYVW",left=119, top=480, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per cancellare i riferimenti del documento allegato";
    , HelpContextID = 109409978;
    , Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_7.Click()
      with this.Parent.oContained
        GSOF_BGN(this.Parent.oContained,"ELIMINA_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_SERALL))
      endwith
    endif
  endfunc

  func oBtn_3_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc


  add object oBtn_3_9 as StdButton with uid="NFTYKEFVSA",left=719, top=480, width=48,height=45,;
    CpPicture="bmp\CARICA.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per associare un nuovo allegato";
    , HelpContextID = 199488214;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_9.Click()
      with this.Parent.oContained
        GSOF_BGN(this.Parent.oContained,"CARICA_ALLEGATO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_3_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc


  add object oBtn_3_13 as StdButton with uid="WWPCQQOGTJ",left=719, top=58, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per ricercare i documenti allegati";
    , HelpContextID = 16636138;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_13.Click()
      with this.Parent.oContained
        .Notifyevent('CalcolaAllegati')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' and NOT EMPTY(.w_NOCODICE))
      endwith
    endif
  endfunc

  func oBtn_3_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc

  add object oCODI_3_15 as StdField with uid="KNYXEELOZK",rtseq=152,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 79961126,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=150, Left=141, Top=7, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15)


  add object oNOTIPPER_3_18 as StdCombo with uid="XIJULZOBYQ",rtseq=154,rtrep=.f.,left=477,top=7,width=107,height=21, enabled=.f.;
    , ToolTipText = "Tipo persona (dipendente, collaboratore, altro)";
    , HelpContextID = 163913000;
    , cFormVar="w_NOTIPPER",RowSource=""+"Dipendente,"+"Collaboratore,"+"Altro", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oNOTIPPER_3_18.RadioValue()
    return(iif(this.value =1,"D",;
    iif(this.value =2,"C",;
    iif(this.value =3,"A",;
    space(1)))))
  endfunc
  func oNOTIPPER_3_18.GetRadio()
    this.Parent.oContained.w_NOTIPPER = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPPER_3_18.SetRadio()
    this.Parent.oContained.w_NOTIPPER=trim(this.Parent.oContained.w_NOTIPPER)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPPER=="D",1,;
      iif(this.Parent.oContained.w_NOTIPPER=="C",2,;
      iif(this.Parent.oContained.w_NOTIPPER=="A",3,;
      0)))
  endfunc

  func oNOTIPPER_3_18.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oNOCOGNOM_3_20 as StdField with uid="MQGALFZJOD",rtseq=155,rtrep=.f.,;
    cFormVar = "w_NOCOGNOM", cQueryName = "NOCOGNOM",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 147190493,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=141, Top=31, InputMask=replicate('X',50)

  func oNOCOGNOM_3_20.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNO__NOME_3_21 as StdField with uid="LPNQLBFOGO",rtseq=156,rtrep=.f.,;
    cFormVar = "w_NO__NOME", cQueryName = "NO__NOME",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 121909989,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=458, Top=31, InputMask=replicate('X',50)

  func oNO__NOME_3_21.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNODESCRI_3_24 as StdField with uid="KUFPRTYFLJ",rtseq=157,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI,NODESCRI,NODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Esatta ragione sociale",;
    HelpContextID = 51372769,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=141, Top=31, InputMask=replicate('X',40)

  func oNODESCRI_3_24.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oNODESCR2_3_25 as StdField with uid="GQKNPSAEXO",rtseq=158,rtrep=.f.,;
    cFormVar = "w_NODESCR2", cQueryName = "NODESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 51372792,;
   bGlobalFont=.t.,;
    Height=21, Width=369, Left=398, Top=31, InputMask=replicate('X',40)

  func oNODESCR2_3_25.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oStr_3_10 as StdString with uid="FXVGAXWYFQ",Visible=.t., Left=56, Top=56,;
    Alignment=1, Width=81, Height=18,;
    Caption="File:"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="XRKKMGDKHL",Visible=.t., Left=49, Top=81,;
    Alignment=1, Width=88, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="HNPBTANUPR",Visible=.t., Left=372, Top=81,;
    Alignment=1, Width=74, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="MOCDQBMGWN",Visible=.t., Left=30, Top=7,;
    Alignment=1, Width=107, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_19 as StdString with uid="GHNSNNHRJK",Visible=.t., Left=440, Top=7,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_3_19.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oStr_3_22 as StdString with uid="TJVDORCSBB",Visible=.t., Left=54, Top=31,;
    Alignment=1, Width=83, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_3_22.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_3_23 as StdString with uid="HUYBTAHRVM",Visible=.t., Left=410, Top=31,;
    Alignment=1, Width=44, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_3_23.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_3_26 as StdString with uid="SLNVSRCPAT",Visible=.t., Left=47, Top=31,;
    Alignment=1, Width=90, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  func oStr_3_26.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc
enddefine
define class tgsar_anoPag4 as StdContainer
  Width  = 783
  height = 544
  stdWidth  = 783
  stdheight = 544
  resizeXpos=379
  resizeYpos=421
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_4_1 as StdField with uid="JNCHVLOARA",rtseq=159,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 79961126,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=150, Left=152, Top=7, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15)

  add object oNOCODPRI_4_2 as StdField with uid="JSFDINYOJF",rtseq=160,rtrep=.f.,;
    cFormVar = "w_NOCODPRI", cQueryName = "NOCODPRI",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Priorit� da attribuire al nominativo",;
    HelpContextID = 116781793,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=54, cSayPict='"999999"', cGetPict='"999999"'

  func oNOCODPRI_4_2.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oNOPRIVCY_4_3 as StdCheck with uid="MOACLKVGMY",rtseq=161,rtrep=.f.,left=458, top=56, caption="Ricevuto consenso trattamento dati personali",;
    ToolTipText = "Se attivo: � stato ricevuto il consenso per il trattamento dei dati personali del nominativo",;
    HelpContextID = 10625745,;
    cFormVar="w_NOPRIVCY", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oNOPRIVCY_4_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOPRIVCY_4_3.GetRadio()
    this.Parent.oContained.w_NOPRIVCY = this.RadioValue()
    return .t.
  endfunc

  func oNOPRIVCY_4_3.SetRadio()
    this.Parent.oContained.w_NOPRIVCY=trim(this.Parent.oContained.w_NOPRIVCY)
    this.value = ;
      iif(this.Parent.oContained.w_NOPRIVCY=='S',1,;
      0)
  endfunc


  add object oBtn_4_4 as StdButton with uid="ODLLTVKJGX",left=728, top=56, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per stampare l'informativa trattamento dati personali";
    , HelpContextID = 51768794;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_4.Click()
      with this.Parent.oContained
        GSAG_BSB(this.Parent.oContained,"STAMPA_DA_NOM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' AND NOT EMPTY(.w_NOCODICE) AND .w_NOPRIVCY<>'S')
      endwith
    endif
  endfunc

  func oBtn_4_4.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT ISALT())
     endwith
    endif
  endfunc

  add object oNOCODGRU_4_6 as StdField with uid="POUTKTHGQZ",rtseq=162,rtrep=.f.,;
    cFormVar = "w_NOCODGRU", cQueryName = "NOCODGRU",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo di appartenenza",;
    HelpContextID = 267776725,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=77, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_NOMI", cZoomOnZoom="GSAR_AGN", oKey_1_1="GNCODICE", oKey_1_2="this.w_NOCODGRU"

  func oNOCODGRU_4_6.mHide()
    with this.Parent.oContained
      return (!Empty(.w_NOFLGBEN))
    endwith
  endfunc

  func oNOCODGRU_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODGRU_4_6.ecpDrop(oSource)
    this.Parent.oContained.link_4_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODGRU_4_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_NOMI','*','GNCODICE',cp_AbsName(this.parent,'oNOCODGRU_4_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGN',"Gruppi nominativi",'',this.parent.oContained
  endproc
  proc oNOCODGRU_4_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GNCODICE=this.parent.oContained.w_NOCODGRU
     i_obj.ecpSave()
  endproc

  add object oNODESGRU_4_7 as StdField with uid="RVWCTSCNEV",rtseq=163,rtrep=.f.,;
    cFormVar = "w_NODESGRU", cQueryName = "NODESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 252699349,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=217, Top=77, InputMask=replicate('X',35)

  func oNODESGRU_4_7.mHide()
    with this.Parent.oContained
      return (!Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oNOCODORI_4_9 as StdField with uid="DRTMTMFUHF",rtseq=164,rtrep=.f.,;
    cFormVar = "w_NOCODORI", cQueryName = "NOCODORI",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Origine reperimento nominativo",;
    HelpContextID = 133559009,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=100, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ORI_NOMI", cZoomOnZoom="GSAR_AON", oKey_1_1="ONCODICE", oKey_1_2="this.w_NOCODORI"

  func oNOCODORI_4_9.mHide()
    with this.Parent.oContained
      return (!Empty(.w_NOFLGBEN))
    endwith
  endfunc

  func oNOCODORI_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODORI_4_9.ecpDrop(oSource)
    this.Parent.oContained.link_4_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODORI_4_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ORI_NOMI','*','ONCODICE',cp_AbsName(this.parent,'oNOCODORI_4_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AON',"Origine",'',this.parent.oContained
  endproc
  proc oNOCODORI_4_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AON()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ONCODICE=this.parent.oContained.w_NOCODORI
     i_obj.ecpSave()
  endproc

  add object oNODESORI_4_10 as StdField with uid="ODRVEDQFCX",rtseq=165,rtrep=.f.,;
    cFormVar = "w_NODESORI", cQueryName = "NODESORI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 118481633,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=217, Top=100, InputMask=replicate('X',35)

  func oNODESORI_4_10.mHide()
    with this.Parent.oContained
      return (!Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oNORIFINT_4_12 as StdField with uid="VOTHCMOGPS",rtseq=166,rtrep=.f.,;
    cFormVar = "w_NORIFINT", cQueryName = "NORIFINT",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il riferimento interno che ha portato il contatto e che � responsabile del cliente.",;
    HelpContextID = 232456918,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=123, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_NORIFINT"

  func oNORIFINT_4_12.mHide()
    with this.Parent.oContained
      return (!Empty(.w_NOFLGBEN))
    endwith
  endfunc

  func oNORIFINT_4_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oNORIFINT_4_12.ecpDrop(oSource)
    this.Parent.oContained.link_4_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNORIFINT_4_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oNORIFINT_4_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Riferimenti interni",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oNORIFINT_4_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TipoRisorsa
     i_obj.w_DPCODICE=this.parent.oContained.w_NORIFINT
     i_obj.ecpSave()
  endproc

  add object oNOCODOPE_4_13 as StdField with uid="TEMYSDFCLO",rtseq=167,rtrep=.f.,;
    cFormVar = "w_NOCODOPE", cQueryName = "NOCODOPE",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 133559013,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=146, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_NOCODOPE"

  func oNOCODOPE_4_13.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  func oNOCODOPE_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODOPE_4_13.ecpDrop(oSource)
    this.Parent.oContained.link_4_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODOPE_4_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oNOCODOPE_4_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco utenti",'',this.parent.oContained
  endproc

  add object oNODESOPE_4_14 as StdField with uid="KQMXCKNOVS",rtseq=168,rtrep=.f.,;
    cFormVar = "w_NODESOPE", cQueryName = "NODESOPE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 118481637,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=217, Top=146, InputMask=replicate('X',35)

  func oNODESOPE_4_14.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oNOASSOPE_4_15 as StdField with uid="CUATRNAODD",rtseq=169,rtrep=.f.,;
    cFormVar = "w_NOASSOPE", cQueryName = "NOASSOPE",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di assegnazione dell'attributo operatore",;
    HelpContextID = 117576421,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=578, Top=146

  func oNOASSOPE_4_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_NOCODOPE))
    endwith
   endif
  endfunc

  func oNOASSOPE_4_15.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oNOCODZON_4_17 as StdField with uid="JNGKLZBZIT",rtseq=170,rtrep=.f.,;
    cFormVar = "w_NOCODZON", cQueryName = "NOCODZON",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della zona di appartenenza",;
    HelpContextID = 217445084,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=169, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_NOCODZON"

  func oNOCODZON_4_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOTIPNOM<>'C')
    endwith
   endif
  endfunc

  func oNOCODZON_4_17.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oNOCODZON_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODZON_4_17.ecpDrop(oSource)
    this.Parent.oContained.link_4_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODZON_4_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oNOCODZON_4_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oNOCODZON_4_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_NOCODZON
     i_obj.ecpSave()
  endproc

  add object oNODESZON_4_18 as StdField with uid="TMPXHVUPSY",rtseq=171,rtrep=.f.,;
    cFormVar = "w_NODESZON", cQueryName = "NODESZON",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 202367708,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=217, Top=169, InputMask=replicate('X',35)

  func oNODESZON_4_18.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oNOCODLIN_4_20 as StdField with uid="JMMIHZEUWE",rtseq=172,rtrep=.f.,;
    cFormVar = "w_NOCODLIN", cQueryName = "NOCODLIN",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Lingua in cui stampare i documenti",;
    HelpContextID = 84544804,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=192, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_NOCODLIN"

  func oNOCODLIN_4_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOTIPNOM<>'C')
    endwith
   endif
  endfunc

  func oNOCODLIN_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODLIN_4_20.ecpDrop(oSource)
    this.Parent.oContained.link_4_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODLIN_4_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oNOCODLIN_4_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oNOCODLIN_4_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_NOCODLIN
     i_obj.ecpSave()
  endproc

  add object oNODESLIN_4_21 as StdField with uid="KZWVFYRGOD",rtseq=173,rtrep=.f.,;
    cFormVar = "w_NODESLIN", cQueryName = "NODESLIN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 99622180,;
   bGlobalFont=.t.,;
    Height=21, Width=206, Left=217, Top=192, InputMask=replicate('X',35)

  add object oNOCODVAL_4_23 as StdField with uid="HQKIVTVUFJ",rtseq=174,rtrep=.f.,;
    cFormVar = "w_NOCODVAL", cQueryName = "NOCODVAL",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della valuta utilizzata in fatturazione",;
    HelpContextID = 16118494,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=512, Top=192, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_NOCODVAL"

  func oNOCODVAL_4_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOTIPNOM<>'C')
    endwith
   endif
  endfunc

  func oNOCODVAL_4_23.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oNOCODVAL_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODVAL_4_23.ecpDrop(oSource)
    this.Parent.oContained.link_4_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODVAL_4_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oNOCODVAL_4_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oNOCODVAL_4_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_NOCODVAL
     i_obj.ecpSave()
  endproc

  add object oNOCODAGE_4_24 as StdField with uid="VFVHOJTMPS",rtseq=175,rtrep=.f.,;
    cFormVar = "w_NOCODAGE", cQueryName = "NOCODAGE",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente che cura i rapporti",;
    HelpContextID = 168430875,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=215, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_NOCODAGE"

  func oNOCODAGE_4_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOTIPNOM<>'C')
    endwith
   endif
  endfunc

  func oNOCODAGE_4_24.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  func oNOCODAGE_4_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODAGE_4_24.ecpDrop(oSource)
    this.Parent.oContained.link_4_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODAGE_4_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oNOCODAGE_4_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oNOCODAGE_4_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_NOCODAGE
     i_obj.ecpSave()
  endproc

  add object oNODESVAL_4_25 as StdField with uid="BTAORANDSA",rtseq=176,rtrep=.f.,;
    cFormVar = "w_NODESVAL", cQueryName = "NODESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1041118,;
   bGlobalFont=.t.,;
    Height=21, Width=206, Left=575, Top=192, InputMask=replicate('X',35)

  func oNODESVAL_4_25.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oLinkPC_4_28 as stdDynamicChildContainer with uid="GCNHYVTNAY",left=10, top=412, width=746, height=130, bOnScreen=.t.;


  func oLinkPC_4_28.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc

  add object oNODESAGE_4_34 as StdField with uid="QHWWKCHFKZ",rtseq=180,rtrep=.f.,;
    cFormVar = "w_NODESAGE", cQueryName = "NODESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 183508251,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=220, Top=215, InputMask=replicate('X',35)

  func oNODESAGE_4_34.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc


  add object oNOTIPPER_4_35 as StdCombo with uid="TRYUPUOSGP",rtseq=181,rtrep=.f.,left=457,top=7,width=107,height=21, enabled=.f.;
    , ToolTipText = "Tipo persona (dipendente, collaboratore, altro)";
    , HelpContextID = 163913000;
    , cFormVar="w_NOTIPPER",RowSource=""+"Dipendente,"+"Collaboratore,"+"Altro", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oNOTIPPER_4_35.RadioValue()
    return(iif(this.value =1,"D",;
    iif(this.value =2,"C",;
    iif(this.value =3,"A",;
    space(1)))))
  endfunc
  func oNOTIPPER_4_35.GetRadio()
    this.Parent.oContained.w_NOTIPPER = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPPER_4_35.SetRadio()
    this.Parent.oContained.w_NOTIPPER=trim(this.Parent.oContained.w_NOTIPPER)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPPER=="D",1,;
      iif(this.Parent.oContained.w_NOTIPPER=="C",2,;
      iif(this.Parent.oContained.w_NOTIPPER=="A",3,;
      0)))
  endfunc

  func oNOTIPPER_4_35.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oNOCOGNOM_4_37 as StdField with uid="JPFFIKBOPK",rtseq=182,rtrep=.f.,;
    cFormVar = "w_NOCOGNOM", cQueryName = "NOCOGNOM",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 147190493,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=152, Top=31, InputMask=replicate('X',50)

  func oNOCOGNOM_4_37.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNO__NOME_4_38 as StdField with uid="HRLVZLXGJM",rtseq=183,rtrep=.f.,;
    cFormVar = "w_NO__NOME", cQueryName = "NO__NOME",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 121909989,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=457, Top=31, InputMask=replicate('X',50)

  func oNO__NOME_4_38.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNODESCRI_4_41 as StdField with uid="VTVPDOKFYS",rtseq=184,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI,NODESCRI,NODESCRI,NODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Esatta ragione sociale",;
    HelpContextID = 51372769,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=152, Top=31, InputMask=replicate('X',40)

  func oNODESCRI_4_41.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oNODESCR2_4_42 as StdField with uid="KVEHDPKYZS",rtseq=185,rtrep=.f.,;
    cFormVar = "w_NODESCR2", cQueryName = "NODESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 51372792,;
   bGlobalFont=.t.,;
    Height=21, Width=368, Left=408, Top=31, InputMask=replicate('X',40)

  func oNODESCR2_4_42.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oNONUMLIS_4_44 as StdField with uid="NMKFSTGHND",rtseq=186,rtrep=.f.,;
    cFormVar = "w_NONUMLIS", cQueryName = "NONUMLIS",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del listino a cui fa riferimento al nominativo",;
    HelpContextID = 94420265,;
   bGlobalFont=.t.,;
    Height=21, Width=80, Left=151, Top=238, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_NONUMLIS"

  func oNONUMLIS_4_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY (.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNONUMLIS_4_44.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION" OR IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  func oNONUMLIS_4_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oNONUMLIS_4_44.ecpDrop(oSource)
    this.Parent.oContained.link_4_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNONUMLIS_4_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oNONUMLIS_4_44'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oNONUMLIS_4_44.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_NONUMLIS
     i_obj.ecpSave()
  endproc

  add object oNONUMLIS_4_45 as StdField with uid="EXMZJNISYA",rtseq=187,rtrep=.f.,;
    cFormVar = "w_NONUMLIS", cQueryName = "NONUMLIS",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del listino a cui fa riferimento al nominativo",;
    HelpContextID = 94420265,;
   bGlobalFont=.t.,;
    Height=21, Width=80, Left=151, Top=238, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_NONUMLIS"

  func oNONUMLIS_4_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY (.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNONUMLIS_4_45.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ADHOC REVOLUTION" or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  func oNONUMLIS_4_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oNONUMLIS_4_45.ecpDrop(oSource)
    this.Parent.oContained.link_4_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNONUMLIS_4_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oNONUMLIS_4_45'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Listini",'QUERY\ASSCLIFOR.VZM.LISTINI_VZM',this.parent.oContained
  endproc

  add object oNOCODPAG_4_46 as StdField with uid="XVENVAWDXP",rtseq=188,rtrep=.f.,;
    cFormVar = "w_NOCODPAG", cQueryName = "NOCODPAG",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice del pagamento praticato normalmente al cliente",;
    HelpContextID = 116781795,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=151, Top=261, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_NOCODPAG"

  func oNOCODPAG_4_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY (.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOCODPAG_4_46.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN='B' OR IsAlt())
    endwith
  endfunc

  func oNOCODPAG_4_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_46('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODPAG_4_46.ecpDrop(oSource)
    this.Parent.oContained.link_4_46('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODPAG_4_46.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oNOCODPAG_4_46'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oNOCODPAG_4_46.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_NOCODPAG
     i_obj.ecpSave()
  endproc

  add object oNOCODBAN_4_47 as StdField with uid="LZPQQQZITD",rtseq=189,rtrep=.f.,;
    cFormVar = "w_NOCODBAN", cQueryName = "NOCODBAN",;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca di appoggio di deb/cred. diversi",;
    HelpContextID = 83227356,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=152, Top=284, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_NOCODBAN"

  func oNOCODBAN_4_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY (.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOCODBAN_4_47.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN='B' OR IsAlt())
    endwith
  endfunc

  func oNOCODBAN_4_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_47('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCODBAN_4_47.ecpDrop(oSource)
    this.Parent.oContained.link_4_47('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCODBAN_4_47.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oNOCODBAN_4_47'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oNOCODBAN_4_47.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_NOCODBAN
     i_obj.ecpSave()
  endproc

  add object oNOCATCOM_4_48 as StdField with uid="EABMHGYNOV",rtseq=190,rtrep=.f.,;
    cFormVar = "w_NOCATCOM", cQueryName = "NOCATCOM",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale associata al nominativo",;
    HelpContextID = 50590429,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=330, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_NOCATCOM"

  func oNOCATCOM_4_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY (.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oNOCATCOM_4_48.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  func oNOCATCOM_4_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_48('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCATCOM_4_48.ecpDrop(oSource)
    this.Parent.oContained.link_4_48('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCATCOM_4_48.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oNOCATCOM_4_48'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oNOCATCOM_4_48.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_NOCATCOM
     i_obj.ecpSave()
  endproc

  add object oNOCATCON_4_49 as StdField with uid="ZNWGWWPOUG",rtseq=191,rtrep=.f.,;
    cFormVar = "w_NOCATCON", cQueryName = "NOCATCON",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria contabile",;
    HelpContextID = 50590428,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=353, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_NOCATCON"

  func oNOCATCON_4_49.mHide()
    with this.Parent.oContained
      return (not(isapa() or isahe()) or .w_NOFLGBEN<>'B')
    endwith
  endfunc

  func oNOCATCON_4_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_49('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCATCON_4_49.ecpDrop(oSource)
    this.Parent.oContained.link_4_49('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCATCON_4_49.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oNOCATCON_4_49'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili",'',this.parent.oContained
  endproc
  proc oNOCATCON_4_49.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_NOCATCON
     i_obj.ecpSave()
  endproc

  add object oNOCACLFO_4_50 as StdField with uid="TDCBMZYFNA",rtseq=192,rtrep=.f.,;
    cFormVar = "w_NOCACLFO", cQueryName = "NOCACLFO",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria analitica",;
    HelpContextID = 82578725,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=152, Top=376, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAANCLFO", cZoomOnZoom="GSAR_ACF", oKey_1_1="C1CODICE", oKey_1_2="this.w_NOCACLFO"

  func oNOCACLFO_4_50.mHide()
    with this.Parent.oContained
      return (not(isapa() or isahe()) or .w_NOFLGBEN<>'B')
    endwith
  endfunc

  func oNOCACLFO_4_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_50('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOCACLFO_4_50.ecpDrop(oSource)
    this.Parent.oContained.link_4_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOCACLFO_4_50.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAANCLFO','*','C1CODICE',cp_AbsName(this.parent,'oNOCACLFO_4_50'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACF',"Elenco cat. analitica intestatario",'',this.parent.oContained
  endproc
  proc oNOCACLFO_4_50.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_NOCACLFO
     i_obj.ecpSave()
  endproc

  add object oBANCAAHR_4_51 as StdField with uid="RXHQBSYLOF",rtseq=193,rtrep=.f.,;
    cFormVar = "w_BANCAAHR", cQueryName = "BANCAAHR",;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto o di tipo salvo buon fine",;
    HelpContextID = 164540008,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=152, Top=307, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANCAAHR"

  func oBANCAAHR_4_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY (.w_NOCODCLI))
    endwith
   endif
  endfunc

  func oBANCAAHR_4_51.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION"  OR .w_NOFLGBEN='B' OR IsAlt())
    endwith
  endfunc

  func oBANCAAHR_4_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_51('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANCAAHR_4_51.ecpDrop(oSource)
    this.Parent.oContained.link_4_51('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANCAAHR_4_51.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oBANCAAHR_4_51'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenco conti banche",'GSAR_ACL.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oBANCAAHR_4_51.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANCAAHR
     i_obj.ecpSave()
  endproc

  add object oBANCAAHE_4_52 as StdField with uid="OVVWKGFLJC",rtseq=194,rtrep=.f.,;
    cFormVar = "w_BANCAAHE", cQueryName = "BANCAAHE",;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto o di tipo salvo buon fine",;
    HelpContextID = 164539995,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=152, Top=307, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACC", oKey_1_1="BANUMCOR", oKey_1_2="this.w_BANCAAHE"

  func oBANCAAHE_4_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY (.w_NOCODCLI) AND g_TESO='S')
    endwith
   endif
  endfunc

  func oBANCAAHE_4_52.mHide()
    with this.Parent.oContained
      return (g_APPLICATION= "ADHOC REVOLUTION" OR .w_NOFLGBEN='B')
    endwith
  endfunc

  func oBANCAAHE_4_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANCAAHE_4_52.ecpDrop(oSource)
    this.Parent.oContained.link_4_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANCAAHE_4_52.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BANUMCOR',cp_AbsName(this.parent,'oBANCAAHE_4_52'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACC',"Elenco conti banche",'',this.parent.oContained
  endproc
  proc oBANCAAHE_4_52.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BANUMCOR=this.parent.oContained.w_BANCAAHE
     i_obj.ecpSave()
  endproc

  add object oDESBA3_4_53 as StdField with uid="TJMOSEPKNW",rtseq=195,rtrep=.f.,;
    cFormVar = "w_DESBA3", cQueryName = "DESBA3",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(42), bMultilanguage =  .f.,;
    HelpContextID = 70385098,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=290, Top=307, InputMask=replicate('X',42)

  func oDESBA3_4_53.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION"  OR .w_NOFLGBEN='B' OR IsAlt())
    endwith
  endfunc

  add object oDESBA2_4_54 as StdField with uid="AMJCMKGYGT",rtseq=196,rtrep=.f.,;
    cFormVar = "w_DESBA2", cQueryName = "DESBA2",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(42), bMultilanguage =  .f.,;
    HelpContextID = 87162314,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=290, Top=307, InputMask=replicate('X',42)

  func oDESBA2_4_54.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ADHOC REVOLUTION"  OR .w_NOFLGBEN='B')
    endwith
  endfunc

  add object oDESBAN_4_56 as StdField with uid="ICVUROCYQA",rtseq=197,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(42), bMultilanguage =  .f.,;
    HelpContextID = 154271178,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=253, Top=284, InputMask=replicate('X',42)

  func oDESBAN_4_56.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN='B' OR IsAlt())
    endwith
  endfunc

  add object oDESPAG_4_59 as StdField with uid="PSCQRYSZLC",rtseq=198,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 2358730,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=216, Top=261, InputMask=replicate('X',30)

  func oDESPAG_4_59.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN='B' OR IsAlt())
    endwith
  endfunc

  add object oDESCAC_4_61 as StdField with uid="TFNKVTIPBP",rtseq=199,rtrep=.f.,;
    cFormVar = "w_DESCAC", cQueryName = "DESCAC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 70319562,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=217, Top=330, InputMask=replicate('X',35)

  func oDESCAC_4_61.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oDESLIS_4_63 as StdField with uid="NHQIECGKWQ",rtseq=200,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61341130,;
   bGlobalFont=.t.,;
    Height=21, Width=211, Left=234, Top=238, InputMask=replicate('X',40)

  func oDESLIS_4_63.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oNODENOMRIF_4_67 as StdField with uid="FHMZMXSXWN",rtseq=203,rtrep=.f.,;
    cFormVar = "w_NODENOMRIF", cQueryName = "NODENOMRIF",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(37), bMultilanguage =  .f.,;
    HelpContextID = 123705416,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=217, Top=123, InputMask=replicate('X',37)

  func oNODENOMRIF_4_67.mHide()
    with this.Parent.oContained
      return (!Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oDESCON_4_69 as StdField with uid="DOBENCOINA",rtseq=204,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 139525578,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=217, Top=353, InputMask=replicate('X',35)

  func oDESCON_4_69.mHide()
    with this.Parent.oContained
      return (not(isapa() or isahe()) or .w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oDESSCATCLFO_4_71 as StdField with uid="HLMRLJTIFM",rtseq=205,rtrep=.f.,;
    cFormVar = "w_DESSCATCLFO", cQueryName = "DESSCATCLFO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 100385479,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=217, Top=376, InputMask=replicate('X',35)

  func oDESSCATCLFO_4_71.mHide()
    with this.Parent.oContained
      return (not(isapa() or isahe()) or .w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oStr_4_5 as StdString with uid="VAHPFOOOIN",Visible=.t., Left=42, Top=54,;
    Alignment=1, Width=107, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  func oStr_4_5.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oStr_4_8 as StdString with uid="HXHUGBYSBE",Visible=.t., Left=42, Top=77,;
    Alignment=1, Width=107, Height=18,;
    Caption="Gruppo:"  ;
  , bGlobalFont=.t.

  func oStr_4_8.mHide()
    with this.Parent.oContained
      return (!Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oStr_4_11 as StdString with uid="LYCTHQJGGO",Visible=.t., Left=42, Top=100,;
    Alignment=1, Width=107, Height=18,;
    Caption="Origine:"  ;
  , bGlobalFont=.t.

  func oStr_4_11.mHide()
    with this.Parent.oContained
      return (!Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oStr_4_16 as StdString with uid="FOPBLQISDQ",Visible=.t., Left=42, Top=146,;
    Alignment=1, Width=107, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  func oStr_4_16.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oStr_4_19 as StdString with uid="IWPPITBRNJ",Visible=.t., Left=42, Top=169,;
    Alignment=1, Width=107, Height=18,;
    Caption="Codice zona:"  ;
  , bGlobalFont=.t.

  func oStr_4_19.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_22 as StdString with uid="QZQVJKGUNX",Visible=.t., Left=42, Top=192,;
    Alignment=1, Width=107, Height=18,;
    Caption="Codice lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_4_26 as StdString with uid="MSYRKXRZTG",Visible=.t., Left=425, Top=192,;
    Alignment=1, Width=83, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_4_26.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_4_27 as StdString with uid="QEZADKQSVJ",Visible=.t., Left=42, Top=7,;
    Alignment=1, Width=107, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_32 as StdString with uid="VFZASDCIVG",Visible=.t., Left=486, Top=146,;
    Alignment=1, Width=89, Height=18,;
    Caption="Assegnato il:"  ;
  , bGlobalFont=.t.

  func oStr_4_32.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oStr_4_33 as StdString with uid="AVSWODSNDF",Visible=.t., Left=42, Top=215,;
    Alignment=1, Width=107, Height=18,;
    Caption="Cod.agente:"  ;
  , bGlobalFont=.t.

  func oStr_4_33.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oStr_4_36 as StdString with uid="HAZAKRLIRF",Visible=.t., Left=420, Top=7,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_4_36.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oStr_4_39 as StdString with uid="VAIMABGBCE",Visible=.t., Left=66, Top=31,;
    Alignment=1, Width=83, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_4_39.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_4_40 as StdString with uid="RAWPURLEYJ",Visible=.t., Left=407, Top=31,;
    Alignment=1, Width=44, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_4_40.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_4_43 as StdString with uid="LGMERZWVPB",Visible=.t., Left=59, Top=31,;
    Alignment=1, Width=90, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  func oStr_4_43.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oStr_4_55 as StdString with uid="GANIXFARCK",Visible=.t., Left=38, Top=307,;
    Alignment=1, Width=111, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  func oStr_4_55.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN='B' OR IsAlt())
    endwith
  endfunc

  add object oStr_4_57 as StdString with uid="BSUNEOVLBE",Visible=.t., Left=100, Top=284,;
    Alignment=1, Width=49, Height=18,;
    Caption="Banca:"  ;
  , bGlobalFont=.t.

  func oStr_4_57.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN='B' OR IsAlt())
    endwith
  endfunc

  add object oStr_4_58 as StdString with uid="TDKBBYHHWD",Visible=.t., Left=79, Top=261,;
    Alignment=1, Width=69, Height=18,;
    Caption="Cod. pag:"  ;
  , bGlobalFont=.t.

  func oStr_4_58.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN='B' OR IsAlt())
    endwith
  endfunc

  add object oStr_4_60 as StdString with uid="RBLORRHYWX",Visible=.t., Left=39, Top=330,;
    Alignment=1, Width=110, Height=15,;
    Caption="Cat.commerciale:"  ;
  , bGlobalFont=.t.

  func oStr_4_60.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oStr_4_62 as StdString with uid="MXIVGMPMJX",Visible=.t., Left=35, Top=238,;
    Alignment=1, Width=113, Height=15,;
    Caption="Cod. listino:"  ;
  , bGlobalFont=.t.

  func oStr_4_62.mHide()
    with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oStr_4_64 as StdString with uid="PGTENDWNQZ",Visible=.t., Left=41, Top=124,;
    Alignment=1, Width=108, Height=18,;
    Caption="Riferimento interno:"  ;
  , bGlobalFont=.t.

  func oStr_4_64.mHide()
    with this.Parent.oContained
      return (!Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oStr_4_68 as StdString with uid="XCWMIXKXTY",Visible=.t., Left=26, Top=395,;
    Alignment=0, Width=107, Height=18,;
    Caption="Attributi:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_4_68.mHide()
    with this.Parent.oContained
      return (!Empty(.w_NOFLGBEN))
    endwith
  endfunc

  add object oStr_4_70 as StdString with uid="STUXQMQJUM",Visible=.t., Left=29, Top=353,;
    Alignment=1, Width=120, Height=15,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  func oStr_4_70.mHide()
    with this.Parent.oContained
      return (not(isapa() or isahe()) or .w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oStr_4_72 as StdString with uid="GLVAQNSUXJ",Visible=.t., Left=-4, Top=376,;
    Alignment=1, Width=153, Height=18,;
    Caption="Cat. analitica intestatario:"  ;
  , bGlobalFont=.t.

  func oStr_4_72.mHide()
    with this.Parent.oContained
      return (not(isapa() or isahe()) or .w_NOFLGBEN<>'B')
    endwith
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsar_man",lower(this.oContained.GSAR_MAN.class))=0
        this.oContained.GSAR_MAN.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsar_anoPag5 as StdContainer
  Width  = 783
  height = 544
  stdWidth  = 783
  stdheight = 544
  resizeXpos=470
  resizeYpos=301
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_5_1 as stdDynamicChildContainer with uid="ATXJQBJFIU",left=26, top=74, width=722, height=464, bOnScreen=.t.;


  add object oCODI_5_2 as StdField with uid="XXJHXKPBVU",rtseq=216,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 79961126,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=150, Left=139, Top=7, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15)


  add object oNOTIPPER_5_4 as StdCombo with uid="MQPZBJKVFG",rtseq=217,rtrep=.f.,left=475,top=7,width=107,height=21, enabled=.f.;
    , ToolTipText = "Tipo persona (dipendente, collaboratore, altro)";
    , HelpContextID = 163913000;
    , cFormVar="w_NOTIPPER",RowSource=""+"Dipendente,"+"Collaboratore,"+"Altro", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oNOTIPPER_5_4.RadioValue()
    return(iif(this.value =1,"D",;
    iif(this.value =2,"C",;
    iif(this.value =3,"A",;
    space(1)))))
  endfunc
  func oNOTIPPER_5_4.GetRadio()
    this.Parent.oContained.w_NOTIPPER = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPPER_5_4.SetRadio()
    this.Parent.oContained.w_NOTIPPER=trim(this.Parent.oContained.w_NOTIPPER)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPPER=="D",1,;
      iif(this.Parent.oContained.w_NOTIPPER=="C",2,;
      iif(this.Parent.oContained.w_NOTIPPER=="A",3,;
      0)))
  endfunc

  func oNOTIPPER_5_4.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oNOCOGNOM_5_6 as StdField with uid="PADSUSVRUL",rtseq=218,rtrep=.f.,;
    cFormVar = "w_NOCOGNOM", cQueryName = "NOCOGNOM",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 147190493,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=139, Top=31, InputMask=replicate('X',50)

  func oNOCOGNOM_5_6.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNO__NOME_5_7 as StdField with uid="DICCUYPZNV",rtseq=219,rtrep=.f.,;
    cFormVar = "w_NO__NOME", cQueryName = "NO__NOME",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 121909989,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=456, Top=31, InputMask=replicate('X',50)

  func oNO__NOME_5_7.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNODESCRI_5_10 as StdField with uid="DEXHRXXIMP",rtseq=220,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Esatta ragione sociale",;
    HelpContextID = 51372769,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=139, Top=31, InputMask=replicate('X',40)

  func oNODESCRI_5_10.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oNODESCR2_5_11 as StdField with uid="WWAVVUHBRE",rtseq=221,rtrep=.f.,;
    cFormVar = "w_NODESCR2", cQueryName = "NODESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 51372792,;
   bGlobalFont=.t.,;
    Height=21, Width=369, Left=398, Top=31, InputMask=replicate('X',40)

  func oNODESCR2_5_11.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oStr_5_3 as StdString with uid="ONDHEAHJHZ",Visible=.t., Left=28, Top=7,;
    Alignment=1, Width=107, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_5 as StdString with uid="RHDNYTGLPX",Visible=.t., Left=438, Top=7,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_5_5.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oStr_5_8 as StdString with uid="XABWAELNFL",Visible=.t., Left=52, Top=31,;
    Alignment=1, Width=83, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_5_8.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_5_9 as StdString with uid="AMCXKNRNFB",Visible=.t., Left=408, Top=31,;
    Alignment=1, Width=44, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_5_9.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_5_12 as StdString with uid="PNCDYDUFMH",Visible=.t., Left=45, Top=31,;
    Alignment=1, Width=90, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  func oStr_5_12.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsar_mcn",lower(this.oContained.GSAR_MCN.class))=0
        this.oContained.GSAR_MCN.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsar_anoPag6 as StdContainer
  Width  = 783
  height = 544
  stdWidth  = 783
  stdheight = 544
  resizeXpos=335
  resizeYpos=250
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oOFSTATUS_6_1 as StdCombo with uid="CLFJQZVEXS",rtseq=222,rtrep=.f.,left=574,top=59,width=115,height=21;
    , ToolTipText = "Stato: in corso; inviate; in corso+inviate; confermate; versione chiusa; sospese; rifiutata; tutte";
    , HelpContextID = 216007737;
    , cFormVar="w_OFSTATUS",RowSource=""+"In corso,"+"Inviate,"+"In corso+inviate,"+"Confermate,"+"Versione chiusa,"+"Rifiutate,"+"Sospese,"+"Tutte", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oOFSTATUS_6_1.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'A',;
    iif(this.value =3,'B',;
    iif(this.value =4,'C',;
    iif(this.value =5,'V',;
    iif(this.value =6,'R',;
    iif(this.value =7,'S',;
    iif(this.value =8,'T',;
    space(10))))))))))
  endfunc
  func oOFSTATUS_6_1.GetRadio()
    this.Parent.oContained.w_OFSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oOFSTATUS_6_1.SetRadio()
    this.Parent.oContained.w_OFSTATUS=trim(this.Parent.oContained.w_OFSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_OFSTATUS=='I',1,;
      iif(this.Parent.oContained.w_OFSTATUS=='A',2,;
      iif(this.Parent.oContained.w_OFSTATUS=='B',3,;
      iif(this.Parent.oContained.w_OFSTATUS=='C',4,;
      iif(this.Parent.oContained.w_OFSTATUS=='V',5,;
      iif(this.Parent.oContained.w_OFSTATUS=='R',6,;
      iif(this.Parent.oContained.w_OFSTATUS=='S',7,;
      iif(this.Parent.oContained.w_OFSTATUS=='T',8,;
      0))))))))
  endfunc

  func oOFSTATUS_6_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc


  add object oBtn_6_2 as StdButton with uid="IQLYXLWBIO",left=13, top=479, width=48,height=45,;
    CpPicture="bmp\Modifica.bmp", caption="", nPag=6;
    , ToolTipText = "Premere per modificare i riferimenti del documento di offerta";
    , HelpContextID = 71712039;
    , Caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_2.Click()
      with this.Parent.oContained
        GSOF_BGN(this.Parent.oContained,"VARIA_OFFERTA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_SEROFF))
      endwith
    endif
  endfunc

  func oBtn_6_2.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc


  add object oBtn_6_3 as StdButton with uid="USXHNAYITR",left=719, top=479, width=48,height=45,;
    CpPicture="bmp\NewOffe.bmp", caption="", nPag=6;
    , ToolTipText = "Premere per inserire una nuova offerta";
    , HelpContextID = 199488214;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_3.Click()
      with this.Parent.oContained
        GSOF_BGN(this.Parent.oContained,"CARICA_OFFERTA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_6_3.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc


  add object ZoomOff as cp_zoombox with uid="LFFQVTQVIV",left=0, top=134, width=773,height=340,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSOF_ANO",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="OFF_ERTE",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomOnZoom="",cMenuFile="",bRetriveAllRows=.f.,;
    cEvent = "CalcolaOfferte,Load",;
    nPag=6;
    , HelpContextID = 15560730


  add object oBtn_6_6 as StdButton with uid="KJDMVREAET",left=64, top=479, width=48,height=45,;
    CpPicture="bmp\stampa.bmp", caption="", nPag=6;
    , ToolTipText = "Premere per eseguire il report delle offerte";
    , HelpContextID = 267586794;
    , Caption='R\<eport';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_6.Click()
      with this.Parent.oContained
        GSOF_BGN(this.Parent.oContained,"ESEGUE_REPORT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_SEROFF))
      endwith
    endif
  endfunc

  func oBtn_6_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc


  add object oBtn_6_7 as StdButton with uid="IGTYYSERQH",left=115, top=479, width=48,height=45,;
    CpPicture="bmp\word.bmp", caption="", nPag=6;
    , ToolTipText = "Premere per aprire il file W.P. associato";
    , HelpContextID = 35473018;
    , Caption='File\<W.P.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_7.Click()
      with this.Parent.oContained
        GSOF_BWP(this.Parent.oContained,"W")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_SEROFF))
      endwith
    endif
  endfunc

  func oBtn_6_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc


  add object oBtn_6_8 as StdButton with uid="QSANCFVZLZ",left=166, top=479, width=48,height=45,;
    CpPicture="bmp\pdf.bmp", caption="", nPag=6;
    , ToolTipText = "Premere per aprire il file PDF associato";
    , HelpContextID = 152914276;
    , Caption='File\<PDF.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_8.Click()
      with this.Parent.oContained
        GSOF_BWP(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_SEROFF))
      endwith
    endif
  endfunc

  func oBtn_6_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc

  add object oNFDTOF1_6_9 as StdField with uid="AUCTAYPSJS",rtseq=224,rtrep=.f.,;
    cFormVar = "w_NFDTOF1", cQueryName = "",;
    bObbl = .f. , nPag = 6, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Filtro data iniziale offerta",;
    HelpContextID = 4254762,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=149, Top=59

  func oNFDTOF1_6_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oNFDTOF2_6_10 as StdField with uid="BQTURMDWOV",rtseq=225,rtrep=.f.,;
    cFormVar = "w_NFDTOF2", cQueryName = "",;
    bObbl = .f. , nPag = 6, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Filtro data finale offerta",;
    HelpContextID = 4254762,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=324, Top=59

  func oNFDTOF2_6_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oNFDTRC1_6_11 as StdField with uid="XGINAQPFUZ",rtseq=226,rtrep=.f.,;
    cFormVar = "w_NFDTRC1", cQueryName = "",;
    bObbl = .f. , nPag = 6, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 51440682,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=149, Top=85

  func oNFDTRC1_6_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oNFDTRC2_6_12 as StdField with uid="SALOVYXYNM",rtseq=227,rtrep=.f.,;
    cFormVar = "w_NFDTRC2", cQueryName = "",;
    bObbl = .f. , nPag = 6, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 51440682,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=324, Top=85

  func oNFDTRC2_6_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oNFGRPRIO_6_17 as StdField with uid="SPPXXPRJQW",rtseq=228,rtrep=.f.,;
    cFormVar = "w_NFGRPRIO", cQueryName = "",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    HelpContextID = 198001701,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=619, Top=85, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_PRIO", cZoomOnZoom="GSOF_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_NFGRPRIO"

  func oNFGRPRIO_6_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oNFGRPRIO_6_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oNFGRPRIO_6_17.ecpDrop(oSource)
    this.Parent.oContained.link_6_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNFGRPRIO_6_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_PRIO','*','GPCODICE',cp_AbsName(this.parent,'oNFGRPRIO_6_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOF_AGP',"Gruppi priorit�",'GSOF_ZGP.GRU_PRIO_VZM',this.parent.oContained
  endproc
  proc oNFGRPRIO_6_17.mZoomOnZoom
    local i_obj
    i_obj=GSOF_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_NFGRPRIO
     i_obj.ecpSave()
  endproc

  add object oNFRIFDE_6_18 as StdField with uid="MISNYXDLLN",rtseq=229,rtrep=.f.,;
    cFormVar = "w_NFRIFDE", cQueryName = "",;
    bObbl = .f. , nPag = 6, value=space(45), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento descrittivo",;
    HelpContextID = 47909930,;
   bGlobalFont=.t.,;
    Height=21, Width=532, Left=149, Top=110, InputMask=replicate('X',45)

  func oNFRIFDE_6_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc


  add object oBtn_6_20 as StdButton with uid="JPWTKGSNWJ",left=719, top=88, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per ricercare le offerte";
    , HelpContextID = 16636138;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_20.Click()
      with this.Parent.oContained
        GSOF_BGN(this.Parent.oContained,"CALCOLA_OFFERTE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_6_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc

  add object oCODI_6_26 as StdField with uid="KRQUAWUFQL",rtseq=233,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 79961126,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=150, Left=149, Top=7, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15)


  add object oNOTIPPER_6_34 as StdCombo with uid="BTENKDCCQF",rtseq=240,rtrep=.f.,left=485,top=7,width=107,height=21, enabled=.f.;
    , ToolTipText = "Tipo persona (dipendente, collaboratore, altro)";
    , HelpContextID = 163913000;
    , cFormVar="w_NOTIPPER",RowSource=""+"Dipendente,"+"Collaboratore,"+"Altro", bObbl = .f. , nPag = 6;
  , bGlobalFont=.t.


  func oNOTIPPER_6_34.RadioValue()
    return(iif(this.value =1,"D",;
    iif(this.value =2,"C",;
    iif(this.value =3,"A",;
    space(1)))))
  endfunc
  func oNOTIPPER_6_34.GetRadio()
    this.Parent.oContained.w_NOTIPPER = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPPER_6_34.SetRadio()
    this.Parent.oContained.w_NOTIPPER=trim(this.Parent.oContained.w_NOTIPPER)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPPER=="D",1,;
      iif(this.Parent.oContained.w_NOTIPPER=="C",2,;
      iif(this.Parent.oContained.w_NOTIPPER=="A",3,;
      0)))
  endfunc

  func oNOTIPPER_6_34.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oNOCOGNOM_6_36 as StdField with uid="DTBSSMOQFY",rtseq=241,rtrep=.f.,;
    cFormVar = "w_NOCOGNOM", cQueryName = "NOCOGNOM",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 147190493,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=149, Top=34, InputMask=replicate('X',50)

  func oNOCOGNOM_6_36.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNO__NOME_6_37 as StdField with uid="CKBKXQDDIT",rtseq=242,rtrep=.f.,;
    cFormVar = "w_NO__NOME", cQueryName = "NO__NOME",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 121909989,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=457, Top=34, InputMask=replicate('X',50)

  func oNO__NOME_6_37.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNODESCRI_6_40 as StdField with uid="EPXGNXXGVU",rtseq=243,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Esatta ragione sociale",;
    HelpContextID = 51372769,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=149, Top=34, InputMask=replicate('X',40)

  func oNODESCRI_6_40.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oNODESCR2_6_41 as StdField with uid="QFUYHRRHMA",rtseq=244,rtrep=.f.,;
    cFormVar = "w_NODESCR2", cQueryName = "NODESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 51372792,;
   bGlobalFont=.t.,;
    Height=21, Width=369, Left=403, Top=34, InputMask=replicate('X',40)

  func oNODESCR2_6_41.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oStr_6_13 as StdString with uid="OCVSTJCWPD",Visible=.t., Left=76, Top=59,;
    Alignment=1, Width=70, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_6_14 as StdString with uid="PBQDLUZJGV",Visible=.t., Left=257, Top=59,;
    Alignment=1, Width=63, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_6_15 as StdString with uid="FVQVIZBYZQ",Visible=.t., Left=1, Top=85,;
    Alignment=1, Width=145, Height=18,;
    Caption="Ricontattare dal:"  ;
  , bGlobalFont=.t.

  add object oStr_6_16 as StdString with uid="ZNUCIZWNXA",Visible=.t., Left=274, Top=85,;
    Alignment=1, Width=46, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_6_19 as StdString with uid="AUWSYBNXTX",Visible=.t., Left=521, Top=85,;
    Alignment=1, Width=91, Height=18,;
    Caption="Gruppo priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_6_21 as StdString with uid="TGAUIJNXUC",Visible=.t., Left=527, Top=59,;
    Alignment=1, Width=44, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_6_22 as StdString with uid="JYWDOWCSFF",Visible=.t., Left=44, Top=110,;
    Alignment=1, Width=102, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_6_27 as StdString with uid="PAFJBUEXHC",Visible=.t., Left=39, Top=7,;
    Alignment=1, Width=107, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_35 as StdString with uid="KWTJACMDRR",Visible=.t., Left=448, Top=7,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_6_35.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oStr_6_38 as StdString with uid="ZRZKHUSIMH",Visible=.t., Left=63, Top=34,;
    Alignment=1, Width=83, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_6_38.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_6_39 as StdString with uid="DRWRQJMYQW",Visible=.t., Left=409, Top=34,;
    Alignment=1, Width=44, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_6_39.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_6_42 as StdString with uid="CAMFGINMGM",Visible=.t., Left=56, Top=34,;
    Alignment=1, Width=90, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  func oStr_6_42.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc
enddefine
define class tgsar_anoPag7 as StdContainer
  Width  = 783
  height = 544
  stdWidth  = 783
  stdheight = 544
  resizeXpos=511
  resizeYpos=169
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOM as cp_zoombox with uid="XBTEELQVTF",left=54, top=147, width=625,height=286,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="OFF_ATTI",bQueryOnLoad=.f.,bReadOnly=.t.,cZoomFile="GSOF_KAT",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",bRetriveAllRows=.f.,cMenuFile="",;
    cEvent = "Ricerca,Load,New record",;
    nPag=7;
    , HelpContextID = 15560730

  add object oOPERAT_7_2 as StdField with uid="TVADFWZWKK",rtseq=245,rtrep=.f.,;
    cFormVar = "w_OPERAT", cQueryName = "",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice operatore",;
    HelpContextID = 52613658,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=132, Top=25, cSayPict='"999999"', cGetPict='"999999"', cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_OPERAT"

  func oOPERAT_7_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (CP_ISADMINISTRATOR() and .cFunction<>'Load')
    endwith
   endif
  endfunc

  func oOPERAT_7_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oOPERAT_7_2.ecpDrop(oSource)
    this.Parent.oContained.link_7_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oSTATO_7_3 as StdCombo with uid="EHZRBCEAJQ",rtseq=246,rtrep=.f.,left=581,top=27,width=93,height=21;
    , ToolTipText = "Stato attivit�";
    , HelpContextID = 163508774;
    , cFormVar="w_STATO",RowSource=""+"Non evasa,"+"Da svolgere,"+"In corso,"+"Provvisoria,"+"Evasa,"+"Tutti", bObbl = .f. , nPag = 7;
  , bGlobalFont=.t.


  func oSTATO_7_3.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'I',;
    iif(this.value =4,'T',;
    iif(this.value =5,'F',;
    iif(this.value =6,'K',;
    space(1))))))))
  endfunc
  func oSTATO_7_3.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_7_3.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='N',1,;
      iif(this.Parent.oContained.w_STATO=='D',2,;
      iif(this.Parent.oContained.w_STATO=='I',3,;
      iif(this.Parent.oContained.w_STATO=='T',4,;
      iif(this.Parent.oContained.w_STATO=='F',5,;
      iif(this.Parent.oContained.w_STATO=='K',6,;
      0))))))
  endfunc

  func oSTATO_7_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oSTATO_7_3.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S')
    endwith
  endfunc

  add object oNAME_7_4 as StdField with uid="WASGPNEQLK",rtseq=247,rtrep=.f.,;
    cFormVar = "w_NAME", cQueryName = "NAME",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione operatore",;
    HelpContextID = 79732438,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=183, Top=25, InputMask=replicate('X',20)

  func oNAME_7_4.mHide()
    with this.Parent.oContained
      return (.cFunction='Query')
    endwith
  endfunc


  add object oSTATO_7_6 as StdCombo with uid="UAVRKIKCDT",rtseq=248,rtrep=.f.,left=581,top=27,width=93,height=21;
    , ToolTipText = "Stato attivit�";
    , HelpContextID = 163508774;
    , cFormVar="w_STATO",RowSource=""+"Aperta,"+"Chiusa", bObbl = .f. , nPag = 7;
  , bGlobalFont=.t.


  func oSTATO_7_6.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oSTATO_7_6.GetRadio()
    this.Parent.oContained.w_STATO = this.RadioValue()
    return .t.
  endfunc

  func oSTATO_7_6.SetRadio()
    this.Parent.oContained.w_STATO=trim(this.Parent.oContained.w_STATO)
    this.value = ;
      iif(this.Parent.oContained.w_STATO=='A',1,;
      iif(this.Parent.oContained.w_STATO=='C',2,;
      0))
  endfunc

  func oSTATO_7_6.mHide()
    with this.Parent.oContained
      return (g_AGEN='S')
    endwith
  endfunc

  add object oDATAINI_7_7 as StdField with uid="QXSPXYXHLS",rtseq=249,rtrep=.f.,;
    cFormVar = "w_DATAINI", cQueryName = "",;
    bObbl = .f. , nPag = 7, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio attivit�",;
    HelpContextID = 122490422,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=132, Top=53

  func oDATAINI_7_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oORAINI_7_8 as StdField with uid="IFZETBJSDN",rtseq=250,rtrep=.f.,;
    cFormVar = "w_ORAINI", cQueryName = "",nZero=2,;
    bObbl = .f. , nPag = 7, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora inizio attivit�",;
    HelpContextID = 224137242,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=324, Top=53, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAINI_7_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oORAINI_7_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAINI) < 24)
    endwith
    return bRes
  endfunc

  add object oMININI_7_9 as StdField with uid="BMAZHAYBOV",rtseq=251,rtrep=.f.,;
    cFormVar = "w_MININI", cQueryName = "",nZero=2,;
    bObbl = .f. , nPag = 7, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti inizio attivit�",;
    HelpContextID = 224086330,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=358, Top=53, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMININI_7_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oMININI_7_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MININI) < 60)
    endwith
    return bRes
  endfunc

  add object oDATAFIN_7_10 as StdField with uid="IMMOZELIYO",rtseq=252,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "",;
    bObbl = .f. , nPag = 7, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine attivit�",;
    HelpContextID = 232976842,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=469, Top=53

  func oDATAFIN_7_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oORAFIN_7_11 as StdField with uid="TWVCQEVEXZ",rtseq=253,rtrep=.f.,;
    cFormVar = "w_ORAFIN", cQueryName = "",nZero=2,;
    bObbl = .f. , nPag = 7, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 23",;
    ToolTipText = "Ora fine attivit�",;
    HelpContextID = 145690650,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=613, Top=53, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oORAFIN_7_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oORAFIN_7_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORAFIN) < 24)
    endwith
    return bRes
  endfunc

  add object oMINFIN_7_12 as StdField with uid="VEYPJJGXRE",rtseq=254,rtrep=.f.,;
    cFormVar = "w_MINFIN", cQueryName = "",nZero=2,;
    bObbl = .f. , nPag = 7, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore compreso tra 00 e 59",;
    ToolTipText = "Minuti fine attivit�",;
    HelpContextID = 145639738,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=647, Top=53, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMINFIN_7_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oMINFIN_7_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINFIN) < 60)
    endwith
    return bRes
  endfunc

  add object oSUBJECT_7_14 as StdField with uid="MWYPXPNUGK",rtseq=255,rtrep=.f.,;
    cFormVar = "w_SUBJECT", cQueryName = "",;
    bObbl = .f. , nPag = 7, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto attivit�",;
    HelpContextID = 65731802,;
   bGlobalFont=.t.,;
    Height=21, Width=354, Left=132, Top=81, InputMask=replicate('X',254)

  func oSUBJECT_7_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  add object oCONTINI_7_15 as StdField with uid="TPGDOPMLNT",rtseq=256,rtrep=.f.,;
    cFormVar = "w_CONTINI", cQueryName = "",;
    bObbl = .f. , nPag = 7, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Contatto",;
    HelpContextID = 123714598,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=132, Top=109, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="NOM_CONT", oKey_1_1="NCCODICE", oKey_1_2="this.w_NOCODICE", oKey_2_1="NCCODCON", oKey_2_2="this.w_CONTINI"

  func oCONTINI_7_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONTINI_7_15.ecpDrop(oSource)
    this.Parent.oContained.link_7_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONTINI_7_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.NOM_CONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_NOCODICE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"NCCODICE="+cp_ToStr(this.Parent.oContained.w_NOCODICE)
    endif
    do cp_zoom with 'NOM_CONT','*','NCCODICE,NCCODCON',cp_AbsName(this.parent,'oCONTINI_7_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Contatti",'',this.parent.oContained
  endproc

  add object oPRIOINI_7_16 as StdField with uid="JXKZIUYBKI",rtseq=257,rtrep=.f.,;
    cFormVar = "w_PRIOINI", cQueryName = "",;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Priorit� attivit�",;
    HelpContextID = 123367414,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=469, Top=109, cSayPict='"999999"', cGetPict='"999999"'

  func oPRIOINI_7_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oPRIOINI_7_16.mHide()
    with this.Parent.oContained
      return (g_AGEN='S')
    endwith
  endfunc


  add object oPRIOINI_7_17 as StdCombo with uid="TGDRVMLJAL",value=1,rtseq=258,rtrep=.f.,left=469,top=109,width=130,height=21;
    , ToolTipText = "Priorit� attivit�";
    , HelpContextID = 123367414;
    , cFormVar="w_PRIOINI",RowSource=""+"Tutti,"+"Normale,"+"Urgente,"+"Scadenza termine", bObbl = .f. , nPag = 7;
  , bGlobalFont=.t.


  func oPRIOINI_7_17.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    0)))))
  endfunc
  func oPRIOINI_7_17.GetRadio()
    this.Parent.oContained.w_PRIOINI = this.RadioValue()
    return .t.
  endfunc

  func oPRIOINI_7_17.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_PRIOINI==0,1,;
      iif(this.Parent.oContained.w_PRIOINI==1,2,;
      iif(this.Parent.oContained.w_PRIOINI==3,3,;
      iif(this.Parent.oContained.w_PRIOINI==4,4,;
      0))))
  endfunc

  func oPRIOINI_7_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc

  func oPRIOINI_7_17.mHide()
    with this.Parent.oContained
      return (g_AGEN<>'S')
    endwith
  endfunc


  add object oBtn_7_40 as StdButton with uid="PHEWMSSLEL",left=626, top=94, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=7;
    , ToolTipText = "Ricerca attivit�";
    , HelpContextID = 16636138;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_40.Click()
      with this.Parent.oContained
        GSOF_BAT(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_7_40.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NOCODICE) and .cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_7_40.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc


  add object oBtn_7_42 as StdButton with uid="UUNSDLSMLD",left=624, top=438, width=48,height=45,;
    CpPicture="BMP\CARICA.BMP", caption="", nPag=7;
    , ToolTipText = "Nuova attivit�";
    , HelpContextID = 199488214;
    , Caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_42.Click()
      with this.Parent.oContained
        GSOF_BAT(this.Parent.oContained,"N")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_7_42.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load')
      endwith
    endif
  endfunc

  func oBtn_7_42.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc


  add object oBtn_7_43 as StdButton with uid="PKVNCOXTJT",left=70, top=438, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=7;
    , ToolTipText = "Visualizza attivit�";
    , HelpContextID = 117674096;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_43.Click()
      with this.Parent.oContained
        GSOF_BAT(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_7_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_ATSERIAL))
      endwith
    endif
  endfunc

  func oBtn_7_43.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc

  add object oDESCPER_7_44 as StdField with uid="WTNPLHLSCK",rtseq=272,rtrep=.f.,;
    cFormVar = "w_DESCPER", cQueryName = "DESCPER",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione contatto",;
    HelpContextID = 21036490,;
   bGlobalFont=.t.,;
    Height=21, Width=192, Left=210, Top=109, InputMask=replicate('X',40)

  func oDESCPER_7_44.mHide()
    with this.Parent.oContained
      return (.cFunction='Query')
    endwith
  endfunc


  add object oBtn_7_45 as StdButton with uid="NBCVTUMNVJ",left=122, top=438, width=48,height=45,;
    CpPicture="BMP\MODIFICA.BMP", caption="", nPag=7;
    , ToolTipText = "Modifica attivit�";
    , HelpContextID = 71712039;
    , Caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_45.Click()
      with this.Parent.oContained
        GSOF_BAT(this.Parent.oContained,"M")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_7_45.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' or NOT EMPTY(.w_ATSERIAL))
      endwith
    endif
  endfunc

  func oBtn_7_45.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt() or !Empty(.w_NOFLGBEN))
     endwith
    endif
  endfunc

  add object oStr_7_5 as StdString with uid="TDPGPHZSYZ",Visible=.t., Left=73, Top=25,;
    Alignment=1, Width=58, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_7_13 as StdString with uid="VQVHVEIIGN",Visible=.t., Left=532, Top=28,;
    Alignment=1, Width=44, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_7_18 as StdString with uid="NPICABCYRW",Visible=.t., Left=85, Top=85,;
    Alignment=1, Width=46, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_7_23 as StdString with uid="NORSJWWDPX",Visible=.t., Left=350, Top=53,;
    Alignment=1, Width=6, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_7_24 as StdString with uid="CXKRSVQCMU",Visible=.t., Left=235, Top=53,;
    Alignment=1, Width=86, Height=18,;
    Caption="Ora inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_7_25 as StdString with uid="APZVWNKQUA",Visible=.t., Left=639, Top=53,;
    Alignment=1, Width=6, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_7_26 as StdString with uid="YYAKHNGDYF",Visible=.t., Left=549, Top=53,;
    Alignment=1, Width=61, Height=18,;
    Caption="Ora fine:"  ;
  , bGlobalFont=.t.

  add object oStr_7_27 as StdString with uid="PGFKKSQZAP",Visible=.t., Left=46, Top=53,;
    Alignment=1, Width=85, Height=18,;
    Caption="Data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_7_28 as StdString with uid="UKKSUZUVPA",Visible=.t., Left=397, Top=53,;
    Alignment=1, Width=71, Height=18,;
    Caption="Data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_7_41 as StdString with uid="AFHPWIJAZL",Visible=.t., Left=407, Top=109,;
    Alignment=1, Width=61, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_7_46 as StdString with uid="EBFZHXXMTL",Visible=.t., Left=78, Top=109,;
    Alignment=1, Width=54, Height=18,;
    Caption="Contatto:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsar_anoPag8 as StdContainer
  Width  = 783
  height = 544
  stdWidth  = 783
  stdheight = 544
  resizeXpos=711
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNODATINS_8_1 as StdField with uid="CBSUOOLIUB",rtseq=274,rtrep=.f.,;
    cFormVar = "w_NODATINS", cQueryName = "NODATINS",;
    bObbl = .f. , nPag = 8, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di identificazione",;
    HelpContextID = 218358487,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=52, Top=101

  func oNODATINS_8_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  add object oNO__RESP_8_2 as StdField with uid="XXIPHQQRFK",rtseq=275,rtrep=.f.,;
    cFormVar = "w_NO__RESP", cQueryName = "NO__RESP",;
    bObbl = .f. , nPag = 8, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Responsabile identificazione",;
    HelpContextID = 17052378,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=257, Top=102, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_NO__RESP"

  func oNO__RESP_8_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  func oNO__RESP_8_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_8_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oNO__RESP_8_2.ecpDrop(oSource)
    this.Parent.oContained.link_8_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNO__RESP_8_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oNO__RESP_8_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Responsabili",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oNO__RESP_8_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_TipoRisorsa
     i_obj.w_DPCODICE=this.parent.oContained.w_NO__RESP
     i_obj.ecpSave()
  endproc

  add object oDATI_8_4 as StdField with uid="HNOUZFUWTN",rtseq=277,rtrep=.f.,;
    cFormVar = "w_DATI", cQueryName = "DATI",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Dipendente",;
    HelpContextID = 80023094,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=347, Top=102, InputMask=replicate('X',40)


  add object oNOTIPOID_8_5 as StdCombo with uid="EGVJCJLCJW",rtseq=278,rtrep=.f.,left=52,top=132,width=277,height=21;
    , ToolTipText = "Tipo identificazione";
    , HelpContextID = 147135770;
    , cFormVar="w_NOTIPOID",RowSource=""+"Identificazione non effettuata,"+"Dal professionista o collaboratore,"+"Atti pubblici o scrittura privata aut.,"+"Dichiarazione del console,"+"Attestazione di altro professionista,"+"Attestazione di intermediari abilitati,"+"Tramite ulteriori modalit�", bObbl = .f. , nPag = 8;
  , bGlobalFont=.t.


  func oNOTIPOID_8_5.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'A',;
    iif(this.value =3,'b',;
    iif(this.value =4,'c',;
    iif(this.value =5,'d',;
    iif(this.value =6,'e',;
    iif(this.value =7,'F',;
    space(2)))))))))
  endfunc
  func oNOTIPOID_8_5.GetRadio()
    this.Parent.oContained.w_NOTIPOID = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPOID_8_5.SetRadio()
    this.Parent.oContained.w_NOTIPOID=trim(this.Parent.oContained.w_NOTIPOID)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPOID=='O',1,;
      iif(this.Parent.oContained.w_NOTIPOID=='A',2,;
      iif(this.Parent.oContained.w_NOTIPOID=='b',3,;
      iif(this.Parent.oContained.w_NOTIPOID=='c',4,;
      iif(this.Parent.oContained.w_NOTIPOID=='d',5,;
      iif(this.Parent.oContained.w_NOTIPOID=='e',6,;
      iif(this.Parent.oContained.w_NOTIPOID=='F',7,;
      0)))))))
  endfunc

  func oNOTIPOID_8_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc


  add object oNOTIPDOC_8_6 as StdCombo with uid="HHGXGHGERT",rtseq=279,rtrep=.f.,left=52,top=217,width=152,height=21;
    , ToolTipText = "Tipo documento";
    , HelpContextID = 37413607;
    , cFormVar="w_NOTIPDOC",RowSource=""+"Carta di identit�,"+"Patente di guida,"+"Passaporto,"+"Porto d'armi,"+"Tessera postale,"+"Altro", bObbl = .f. , nPag = 8;
  , bGlobalFont=.t.


  func oNOTIPDOC_8_6.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'02',;
    iif(this.value =3,'03',;
    iif(this.value =4,'04',;
    iif(this.value =5,'05',;
    iif(this.value =6,'06',;
    space(2))))))))
  endfunc
  func oNOTIPDOC_8_6.GetRadio()
    this.Parent.oContained.w_NOTIPDOC = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPDOC_8_6.SetRadio()
    this.Parent.oContained.w_NOTIPDOC=trim(this.Parent.oContained.w_NOTIPDOC)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPDOC=='01',1,;
      iif(this.Parent.oContained.w_NOTIPDOC=='02',2,;
      iif(this.Parent.oContained.w_NOTIPDOC=='03',3,;
      iif(this.Parent.oContained.w_NOTIPDOC=='04',4,;
      iif(this.Parent.oContained.w_NOTIPDOC=='05',5,;
      iif(this.Parent.oContained.w_NOTIPDOC=='06',6,;
      0))))))
  endfunc

  func oNOTIPDOC_8_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  add object oNONUMDOC_8_7 as StdField with uid="JRTLMTEZVP",rtseq=280,rtrep=.f.,;
    cFormVar = "w_NONUMDOC", cQueryName = "NONUMDOC",;
    bObbl = .f. , nPag = 8, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 39797479,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=347, Top=217, InputMask=replicate('X',15)

  func oNONUMDOC_8_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  add object oNODATRIL_8_8 as StdField with uid="MWRMONELCQ",rtseq=281,rtrep=.f.,;
    cFormVar = "w_NODATRIL", cQueryName = "NODATRIL",;
    bObbl = .f. , nPag = 8, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di rilascio",;
    HelpContextID = 201071906,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=617, Top=217

  func oNODATRIL_8_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  add object oNOAUTLOC_8_9 as StdField with uid="TBKPBFBLFF",rtseq=282,rtrep=.f.,;
    cFormVar = "w_NOAUTLOC", cQueryName = "NOAUTLOC",;
    bObbl = .f. , nPag = 8, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Autorit� e localit� di rilascio",;
    HelpContextID = 166728423,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=347, Top=244, InputMask=replicate('X',30)

  func oNOAUTLOC_8_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  add object oNOATTLAV_8_10 as StdField with uid="KMTXZTBAFD",rtseq=283,rtrep=.f.,;
    cFormVar = "w_NOATTLAV", cQueryName = "NOATTLAV",;
    bObbl = .f. , nPag = 8, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Attivit� lavorativa cliente",;
    HelpContextID = 166793940,;
   bGlobalFont=.t.,;
    Height=21, Width=517, Left=197, Top=338, InputMask=replicate('X',50)

  func oNOATTLAV_8_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  add object oNONAGIUR_8_11 as StdField with uid="YNBZVTFIUC",rtseq=284,rtrep=.f.,;
    cFormVar = "w_NONAGIUR", cQueryName = "NONAGIUR",;
    bObbl = .f. , nPag = 8, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Natura giuridica",;
    HelpContextID = 36486440,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=197, Top=399, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="NAT_GIUR", cZoomOnZoom="GSAL_ANG", oKey_1_1="NGCODICE", oKey_1_2="this.w_NONAGIUR"

  func oNONAGIUR_8_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  func oNONAGIUR_8_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_8_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oNONAGIUR_8_11.ecpDrop(oSource)
    this.Parent.oContained.link_8_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNONAGIUR_8_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAT_GIUR','*','NGCODICE',cp_AbsName(this.parent,'oNONAGIUR_8_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAL_ANG',"Natura giuridica",'',this.parent.oContained
  endproc
  proc oNONAGIUR_8_11.mZoomOnZoom
    local i_obj
    i_obj=GSAL_ANG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NGCODICE=this.parent.oContained.w_NONAGIUR
     i_obj.ecpSave()
  endproc

  add object oNGDESCRI_8_12 as StdField with uid="OBNMFHLAKM",rtseq=285,rtrep=.f.,;
    cFormVar = "w_NGDESCRI", cQueryName = "NGDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione natura giuridica",;
    HelpContextID = 51374817,;
   bGlobalFont=.t.,;
    Height=21, Width=424, Left=290, Top=399, InputMask=replicate('X',50)

  add object oNODATCOS_8_13 as StdField with uid="WCUQXFJNCW",rtseq=286,rtrep=.f.,;
    cFormVar = "w_NODATCOS", cQueryName = "NODATCOS",;
    bObbl = .f. , nPag = 8, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di costituzione societ�",;
    HelpContextID = 50586327,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=197, Top=427

  func oNODATCOS_8_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  add object oNO_SIGLA_8_14 as StdField with uid="BQXSDNAETV",rtseq=287,rtrep=.f.,;
    cFormVar = "w_NO_SIGLA", cQueryName = "NO_SIGLA",;
    bObbl = .f. , nPag = 8, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Sigla (acronimo) della societ�",;
    HelpContextID = 6278423,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=197, Top=455, InputMask=replicate('X',30)

  func oNO_SIGLA_8_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  add object oNO_PAESE_8_15 as StdField with uid="GWUZBBPQKH",rtseq=288,rtrep=.f.,;
    cFormVar = "w_NO_PAESE", cQueryName = "NO_PAESE",;
    bObbl = .f. , nPag = 8, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Paese estero di residenza",;
    HelpContextID = 35861221,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=197, Top=483, InputMask=replicate('X',30)

  func oNO_PAESE_8_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  add object oNOESTNAS_8_16 as StdField with uid="MLQLXPTBII",rtseq=289,rtrep=.f.,;
    cFormVar = "w_NOESTNAS", cQueryName = "NOESTNAS",;
    bObbl = .f. , nPag = 8, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Stato estero di nascita del cliente",;
    HelpContextID = 133288663,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=197, Top=511, InputMask=replicate('X',30)

  func oNOESTNAS_8_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  add object oNOFLANTI_8_19 as StdCheck with uid="JRRUCABIBL",rtseq=291,rtrep=.f.,left=428, top=511, caption="Trasferito in anagrafe soggetti antiriciclaggio", enabled=.f.,;
    ToolTipText = "Flag trasferito in anagrafe soggetti antiriciclaggio",;
    HelpContextID = 153666273,;
    cFormVar="w_NOFLANTI", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oNOFLANTI_8_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOFLANTI_8_19.GetRadio()
    this.Parent.oContained.w_NOFLANTI = this.RadioValue()
    return .t.
  endfunc

  func oNOFLANTI_8_19.SetRadio()
    this.Parent.oContained.w_NOFLANTI=trim(this.Parent.oContained.w_NOFLANTI)
    this.value = ;
      iif(this.Parent.oContained.w_NOFLANTI=='S',1,;
      0)
  endfunc

  func oNOFLANTI_8_19.mHide()
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
  endfunc

  add object oNOFLANTI_8_20 as StdCheck with uid="ADCRCRNNCL",rtseq=292,rtrep=.f.,left=428, top=511, caption="Trasferisci in anagrafe soggetti antiriciclaggio",;
    ToolTipText = "Flag trasferito in anagrafe soggetti antiriciclaggio",;
    HelpContextID = 153666273,;
    cFormVar="w_NOFLANTI", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oNOFLANTI_8_20.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOFLANTI_8_20.GetRadio()
    this.Parent.oContained.w_NOFLANTI = this.RadioValue()
    return .t.
  endfunc

  func oNOFLANTI_8_20.SetRadio()
    this.Parent.oContained.w_NOFLANTI=trim(this.Parent.oContained.w_NOFLANTI)
    this.value = ;
      iif(this.Parent.oContained.w_NOFLANTI=='S',1,;
      0)
  endfunc

  func oNOFLANTI_8_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI<>'S')
    endwith
   endif
  endfunc

  func oNOFLANTI_8_20.mHide()
    with this.Parent.oContained
      return (.w_OLD_NOFLANTI='S')
    endwith
  endfunc


  add object oBtn_8_42 as StdButton with uid="VMAVLCWAFG",left=716, top=102, width=48,height=45,;
    CpPicture="BMP\Storicizza.bmp", caption="", nPag=8;
    , ToolTipText = "Premere per identificare il nominativo nell'anagrafe dei soggetti dell'antiriciclaggio";
    , HelpContextID = 86747423;
    , tabstop=.f., Caption='\<Identifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_8_42.Click()
      with this.Parent.oContained
        .IdentificaNom()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_8_42.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_NOCODICE))
      endwith
    endif
  endfunc

  func oBtn_8_42.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction#"Query" OR (.cFunction="Query" AND empty(.w_NOCODICE)) OR .w_OLD_NOFLANTI='S' OR g_ANTI<>'S')
     endwith
    endif
  endfunc


  add object oObj_8_44 as cp_runprogram with uid="CNNQVBAYNW",left=235, top=566, width=172,height=22,;
    caption='GSAT_BIN',;
   bGlobalFont=.t.,;
    prg=".IdentificaNom2",;
    cEvent = "Record Updated",;
    nPag=8;
    , HelpContextID = 213840052


  add object oObj_8_45 as cp_runprogram with uid="OZKMHROTRG",left=235, top=589, width=172,height=22,;
    caption='GSAT_BIN',;
   bGlobalFont=.t.,;
    prg=".IdentificaNom2",;
    cEvent = "Record Inserted",;
    nPag=8;
    , HelpContextID = 213840052

  add object oNOCODICE_8_47 as StdField with uid="EVVNJVXCUF",rtseq=293,rtrep=.f.,;
    cFormVar = "w_NOCODICE", cQueryName = "NOCODICE",enabled=.f.,;
    bObbl = .t. , nPag = 8, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice deb/cred. diversi",;
    HelpContextID = 234222309,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=150, Left=139, Top=7, InputMask=replicate('X',15)

  func oNOCODICE_8_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CONTINI)
        bRes2=.link_7_15('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oNOCOGNOM_8_48 as StdField with uid="LYLSGWMMLY",rtseq=294,rtrep=.f.,;
    cFormVar = "w_NOCOGNOM", cQueryName = "NOCOGNOM",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 147190493,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=139, Top=31, InputMask=replicate('X',50)

  func oNOCOGNOM_8_48.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNODESCRI_8_51 as StdField with uid="MKRAYYZSWS",rtseq=295,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Esatta ragione sociale",;
    HelpContextID = 51372769,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=139, Top=31, InputMask=replicate('X',40)

  func oNODESCRI_8_51.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oNO__NOME_8_52 as StdField with uid="VYKYZYQLZQ",rtseq=296,rtrep=.f.,;
    cFormVar = "w_NO__NOME", cQueryName = "NO__NOME",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 121909989,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=457, Top=31, InputMask=replicate('X',50)

  func oNO__NOME_8_52.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNODESCR2_8_53 as StdField with uid="JAMGCJXHHC",rtseq=297,rtrep=.f.,;
    cFormVar = "w_NODESCR2", cQueryName = "NODESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 51372792,;
   bGlobalFont=.t.,;
    Height=21, Width=355, Left=397, Top=31, InputMask=replicate('X',40)

  func oNODESCR2_8_53.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oStr_8_21 as StdString with uid="BSXEULCRZP",Visible=.t., Left=99, Top=399,;
    Alignment=1, Width=90, Height=18,;
    Caption="Natura giuridica:"  ;
  , bGlobalFont=.t.

  add object oStr_8_22 as StdString with uid="IDKXXNZMOJ",Visible=.t., Left=162, Top=102,;
    Alignment=1, Width=90, Height=18,;
    Caption="Responsabile:"  ;
  , bGlobalFont=.t.

  add object oStr_8_23 as StdString with uid="SKRATUIPTP",Visible=.t., Left=17, Top=132,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_8_24 as StdString with uid="OYJKMEWQSL",Visible=.t., Left=6, Top=101,;
    Alignment=1, Width=42, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_8_25 as StdString with uid="ZAMGGTKJCI",Visible=.t., Left=51, Top=455,;
    Alignment=1, Width=138, Height=18,;
    Caption="Sigla della societ�:"  ;
  , bGlobalFont=.t.

  add object oStr_8_26 as StdString with uid="NMNEIXCJSN",Visible=.t., Left=90, Top=339,;
    Alignment=1, Width=99, Height=18,;
    Caption="Attivit� svolta:"  ;
  , bGlobalFont=.t.

  add object oStr_8_27 as StdString with uid="IZZWPGVSDA",Visible=.t., Left=0, Top=483,;
    Alignment=1, Width=189, Height=18,;
    Caption="Stato estero di residenza:"  ;
  , bGlobalFont=.t.

  add object oStr_8_28 as StdString with uid="GUTEGMVCYK",Visible=.t., Left=1, Top=427,;
    Alignment=1, Width=188, Height=18,;
    Caption="Data di costituzione societ�:"  ;
  , bGlobalFont=.t.

  add object oStr_8_29 as StdString with uid="TJBDCQLPVY",Visible=.t., Left=33, Top=511,;
    Alignment=1, Width=156, Height=18,;
    Caption="Stato estero di nascita:"  ;
  , bGlobalFont=.t.

  add object oStr_8_30 as StdString with uid="KTKRAZZDSD",Visible=.t., Left=224, Top=218,;
    Alignment=1, Width=116, Height=18,;
    Caption="Numero documento:"  ;
  , bGlobalFont=.t.

  add object oStr_8_31 as StdString with uid="UNGVJHIYGQ",Visible=.t., Left=508, Top=217,;
    Alignment=1, Width=104, Height=18,;
    Caption="Data di rilascio:"  ;
  , bGlobalFont=.t.

  add object oStr_8_32 as StdString with uid="KJDHFVDVBO",Visible=.t., Left=173, Top=248,;
    Alignment=1, Width=167, Height=18,;
    Caption="Autorit� localit� di rilascio:"  ;
  , bGlobalFont=.t.

  add object oStr_8_34 as StdString with uid="VAPODQWBZA",Visible=.t., Left=11, Top=73,;
    Alignment=0, Width=116, Height=18,;
    Caption="Dati identificazione"  ;
  , bGlobalFont=.t.

  add object oStr_8_36 as StdString with uid="MTQLWBYMFO",Visible=.t., Left=11, Top=186,;
    Alignment=0, Width=232, Height=18,;
    Caption="Estremi del documento di identificazione"  ;
  , bGlobalFont=.t.

  add object oStr_8_38 as StdString with uid="ARKDIGWDVR",Visible=.t., Left=17, Top=216,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_8_40 as StdString with uid="WPTYZWPNKU",Visible=.t., Left=11, Top=300,;
    Alignment=0, Width=156, Height=18,;
    Caption="Dati identificativi attivit�"  ;
  , bGlobalFont=.t.

  add object oStr_8_46 as StdString with uid="HDEXSBECTX",Visible=.t., Left=63, Top=7,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_8_49 as StdString with uid="AWFSYDVWFF",Visible=.t., Left=52, Top=31,;
    Alignment=1, Width=83, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_8_49.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_8_50 as StdString with uid="BQTKCEAFUI",Visible=.t., Left=409, Top=31,;
    Alignment=1, Width=44, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_8_50.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_8_54 as StdString with uid="DJJENSPLVD",Visible=.t., Left=45, Top=31,;
    Alignment=1, Width=90, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  func oStr_8_54.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oBox_8_35 as StdBox with uid="AMAAILRSWL",left=12, top=90, width=752,height=1

  add object oBox_8_37 as StdBox with uid="UHFFGXBLUN",left=12, top=205, width=752,height=2

  add object oBox_8_39 as StdBox with uid="JKZGKQEUVW",left=12, top=320, width=752,height=2

  add object oBox_8_41 as StdBox with uid="CJQJECLXYX",left=10, top=388, width=752,height=2
enddefine
define class tgsar_anoPag9 as StdContainer
  Width  = 783
  height = 544
  stdWidth  = 783
  stdheight = 544
  resizeXpos=675
  resizeYpos=465
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNO__NOTE_9_1 as StdMemo with uid="FCJTRNOWVD",rtseq=300,rtrep=.f.,;
    cFormVar = "w_NO__NOTE", cQueryName = "NO__NOTE",;
    bObbl = .f. , nPag = 9, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali annotazioni sul deb. / cred. diverso",;
    HelpContextID = 121909989,;
   bGlobalFont=.t.,;
    Height=409, Width=755, Left=8, Top=71

  func oNO__NOTE_9_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NOTIPNOM<>'C')
    endwith
   endif
  endfunc


  add object oBtn_9_2 as StdButton with uid="AJZTXWKQCE",left=711, top=485, width=48,height=45,;
    CpPicture="bmp\note.bmp", caption="", nPag=9;
    , ToolTipText = "Premere per inserire riferimenti note";
    , HelpContextID = 184834949;
    , Caption='\<Nota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_9_2.Click()
      with this.Parent.oContained
        gsAR_bno (this.Parent.oContained,"N")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_9_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_NOTIPNOM<>'C')
      endwith
    endif
  endfunc

  add object oCODI_9_3 as StdField with uid="RULKTJLVJP",rtseq=301,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice nominativo",;
    HelpContextID = 79961126,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=150, Left=139, Top=7, cSayPict="p_NOM", cGetPict="p_NOM", InputMask=replicate('X',15)


  add object oNOTIPPER_9_5 as StdCombo with uid="NNFBLUZFCH",rtseq=302,rtrep=.f.,left=475,top=7,width=107,height=21, enabled=.f.;
    , ToolTipText = "Tipo persona (dipendente, collaboratore, altro)";
    , HelpContextID = 163913000;
    , cFormVar="w_NOTIPPER",RowSource=""+"Dipendente,"+"Collaboratore,"+"Altro", bObbl = .f. , nPag = 9;
  , bGlobalFont=.t.


  func oNOTIPPER_9_5.RadioValue()
    return(iif(this.value =1,"D",;
    iif(this.value =2,"C",;
    iif(this.value =3,"A",;
    space(1)))))
  endfunc
  func oNOTIPPER_9_5.GetRadio()
    this.Parent.oContained.w_NOTIPPER = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPPER_9_5.SetRadio()
    this.Parent.oContained.w_NOTIPPER=trim(this.Parent.oContained.w_NOTIPPER)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPPER=="D",1,;
      iif(this.Parent.oContained.w_NOTIPPER=="C",2,;
      iif(this.Parent.oContained.w_NOTIPPER=="A",3,;
      0)))
  endfunc

  func oNOTIPPER_9_5.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oNOCOGNOM_9_7 as StdField with uid="DBHRKOJYHM",rtseq=303,rtrep=.f.,;
    cFormVar = "w_NOCOGNOM", cQueryName = "NOCOGNOM",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 147190493,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=139, Top=31, InputMask=replicate('X',50)

  func oNOCOGNOM_9_7.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNO__NOME_9_8 as StdField with uid="DACXSTAKFJ",rtseq=304,rtrep=.f.,;
    cFormVar = "w_NO__NOME", cQueryName = "NO__NOME",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 121909989,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=456, Top=31, InputMask=replicate('X',50)

  func oNO__NOME_9_8.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNODESCRI_9_11 as StdField with uid="OYENUWHBEZ",rtseq=305,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Esatta ragione sociale",;
    HelpContextID = 51372769,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=139, Top=31, InputMask=replicate('X',40)

  func oNODESCRI_9_11.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oNODESCR2_9_12 as StdField with uid="SYUNYKCXSS",rtseq=306,rtrep=.f.,;
    cFormVar = "w_NODESCR2", cQueryName = "NODESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 51372792,;
   bGlobalFont=.t.,;
    Height=21, Width=369, Left=396, Top=31, InputMask=replicate('X',40)

  func oNODESCR2_9_12.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oStr_9_4 as StdString with uid="KAURESFLYS",Visible=.t., Left=28, Top=7,;
    Alignment=1, Width=107, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_9_6 as StdString with uid="DWUBDIMMLV",Visible=.t., Left=438, Top=7,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_9_6.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oStr_9_9 as StdString with uid="ICIFBMWXRT",Visible=.t., Left=52, Top=31,;
    Alignment=1, Width=83, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_9_9.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_9_10 as StdString with uid="LJPXHQEHCY",Visible=.t., Left=408, Top=31,;
    Alignment=1, Width=44, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_9_10.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_9_13 as StdString with uid="VXFNUHPAGQ",Visible=.t., Left=45, Top=31,;
    Alignment=1, Width=90, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  func oStr_9_13.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc
enddefine
define class tgsar_anoPag10 as StdContainer
  Width  = 783
  height = 544
  stdWidth  = 783
  stdheight = 544
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNORATING_10_4 as StdField with uid="LNGADKNCDM",rtseq=308,rtrep=.f.,;
    cFormVar = "w_NORATING", cQueryName = "NORATING",;
    bObbl = .f. , nPag = 10, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Livello di importanza per i pagamenti oppure livello di esigibilit� per gli incassi",;
    HelpContextID = 218301155,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=160, Top=83, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="DORATING", cZoomOnZoom="Gsdf_Ara", oKey_1_1="RACODICE", oKey_1_2="this.w_NORATING"

  func oNORATING_10_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_10_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oNORATING_10_4.ecpDrop(oSource)
    this.Parent.oContained.link_10_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNORATING_10_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DORATING','*','RACODICE',cp_AbsName(this.parent,'oNORATING_10_4'),iif(empty(i_cWhere),.f.,i_cWhere),'Gsdf_Ara',"Rating",'',this.parent.oContained
  endproc
  proc oNORATING_10_4.mZoomOnZoom
    local i_obj
    i_obj=Gsdf_Ara()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RACODICE=this.parent.oContained.w_NORATING
     i_obj.ecpSave()
  endproc

  add object oNOGIORIT_10_5 as StdField with uid="ACMTTAHCFQ",rtseq=309,rtrep=.f.,;
    cFormVar = "w_NOGIORIT", cQueryName = "NOGIORIT",;
    bObbl = .f. , nPag = 10, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni di ritardo su pagamenti e incassi",;
    HelpContextID = 196365610,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=160, Top=126, cSayPict='"999"', cGetPict='"999"'

  add object oNOVOCFIN_10_6 as StdField with uid="LSGWGQGNWC",rtseq=310,rtrep=.f.,;
    cFormVar = "w_NOVOCFIN", cQueryName = "NOVOCFIN",;
    bObbl = .f. , nPag = 10, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Voce finanziaria",;
    HelpContextID = 251346212,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=160, Top=169, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="VOC_FINZ", cZoomOnZoom="GSDF_AVF", oKey_1_1="DFVOCFIN", oKey_1_2="this.w_NOVOCFIN"

  func oNOVOCFIN_10_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_10_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOVOCFIN_10_6.ecpDrop(oSource)
    this.Parent.oContained.link_10_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOVOCFIN_10_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_FINZ','*','DFVOCFIN',cp_AbsName(this.parent,'oNOVOCFIN_10_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDF_AVF',"Voci finanziarie",'',this.parent.oContained
  endproc
  proc oNOVOCFIN_10_6.mZoomOnZoom
    local i_obj
    i_obj=GSDF_AVF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DFVOCFIN=this.parent.oContained.w_NOVOCFIN
     i_obj.ecpSave()
  endproc

  add object oDFDESCRI_10_7 as StdField with uid="LIZWSGVYPA",rtseq=311,rtrep=.f.,;
    cFormVar = "w_DFDESCRI", cQueryName = "DFDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 10, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 51375233,;
   bGlobalFont=.t.,;
    Height=21, Width=391, Left=232, Top=169, InputMask=replicate('X',60)

  add object oNOESCDOF_10_8 as StdCheck with uid="JFHGGHJQQS",rtseq=312,rtrep=.f.,left=160, top=212, caption="Escludi nell'esportazione per DocFinance",;
    ToolTipText = "Escludi nell'esportazione per DocFinance",;
    HelpContextID = 50451172,;
    cFormVar="w_NOESCDOF", bObbl = .f. , nPag = 10;
   , bGlobalFont=.t.


  func oNOESCDOF_10_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNOESCDOF_10_8.GetRadio()
    this.Parent.oContained.w_NOESCDOF = this.RadioValue()
    return .t.
  endfunc

  func oNOESCDOF_10_8.SetRadio()
    this.Parent.oContained.w_NOESCDOF=trim(this.Parent.oContained.w_NOESCDOF)
    this.value = ;
      iif(this.Parent.oContained.w_NOESCDOF=='S',1,;
      0)
  endfunc

  add object oNODESPAR_10_9 as StdCheck with uid="ECCTEFVCFP",rtseq=313,rtrep=.f.,left=160, top=254, caption="Riporta descrizione partita",;
    ToolTipText = "Riporta descrizione partita ",;
    HelpContextID = 101704408,;
    cFormVar="w_NODESPAR", bObbl = .f. , nPag = 10;
   , bGlobalFont=.t.


  func oNODESPAR_10_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNODESPAR_10_9.GetRadio()
    this.Parent.oContained.w_NODESPAR = this.RadioValue()
    return .t.
  endfunc

  func oNODESPAR_10_9.SetRadio()
    this.Parent.oContained.w_NODESPAR=trim(this.Parent.oContained.w_NODESPAR)
    this.value = ;
      iif(this.Parent.oContained.w_NODESPAR=='S',1,;
      0)
  endfunc

  add object oRADESCRI_10_10 as StdField with uid="LKXITUNVVR",rtseq=314,rtrep=.f.,;
    cFormVar = "w_RADESCRI", cQueryName = "RADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 10, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 51376289,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=232, Top=83, InputMask=replicate('X',50)

  add object oNOCODICE_10_12 as StdField with uid="NBVBHJKGCK",rtseq=315,rtrep=.f.,;
    cFormVar = "w_NOCODICE", cQueryName = "NOCODICE",enabled=.f.,;
    bObbl = .t. , nPag = 10, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice deb/cred. diversi",;
    HelpContextID = 234222309,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=150, Left=160, Top=9, InputMask=replicate('X',15)

  func oNOCODICE_10_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CONTINI)
        bRes2=.link_7_15('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oNOTIPPER_10_13 as StdCombo with uid="MRKMGZBMDB",rtseq=316,rtrep=.f.,left=495,top=9,width=107,height=21, enabled=.f.;
    , ToolTipText = "Tipo persona (dipendente, collaboratore, altro)";
    , HelpContextID = 163913000;
    , cFormVar="w_NOTIPPER",RowSource=""+"Dipendente,"+"Collaboratore,"+"Altro", bObbl = .f. , nPag = 10;
  , bGlobalFont=.t.


  func oNOTIPPER_10_13.RadioValue()
    return(iif(this.value =1,"D",;
    iif(this.value =2,"C",;
    iif(this.value =3,"A",;
    space(1)))))
  endfunc
  func oNOTIPPER_10_13.GetRadio()
    this.Parent.oContained.w_NOTIPPER = this.RadioValue()
    return .t.
  endfunc

  func oNOTIPPER_10_13.SetRadio()
    this.Parent.oContained.w_NOTIPPER=trim(this.Parent.oContained.w_NOTIPPER)
    this.value = ;
      iif(this.Parent.oContained.w_NOTIPPER=="D",1,;
      iif(this.Parent.oContained.w_NOTIPPER=="C",2,;
      iif(this.Parent.oContained.w_NOTIPPER=="A",3,;
      0)))
  endfunc

  func oNOTIPPER_10_13.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oNOCOGNOM_10_15 as StdField with uid="SNHMUWFOKT",rtseq=317,rtrep=.f.,;
    cFormVar = "w_NOCOGNOM", cQueryName = "NOCOGNOM",enabled=.f.,;
    bObbl = .f. , nPag = 10, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 147190493,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=160, Top=44, InputMask=replicate('X',50)

  func oNOCOGNOM_10_15.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNO__NOME_10_16 as StdField with uid="DEELKNAPZY",rtseq=318,rtrep=.f.,;
    cFormVar = "w_NO__NOME", cQueryName = "NO__NOME",enabled=.f.,;
    bObbl = .f. , nPag = 10, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 121909989,;
   bGlobalFont=.t.,;
    Height=21, Width=295, Left=475, Top=44, InputMask=replicate('X',50)

  func oNO__NOME_10_16.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oNODESCRI_10_19 as StdField with uid="NTOTDOWFXA",rtseq=319,rtrep=.f.,;
    cFormVar = "w_NODESCRI", cQueryName = "NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI,NODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 10, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Esatta ragione sociale",;
    HelpContextID = 51372769,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=160, Top=44, InputMask=replicate('X',40)

  func oNODESCRI_10_19.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oNODESCR2_10_20 as StdField with uid="MBEWWMGKDS",rtseq=320,rtrep=.f.,;
    cFormVar = "w_NODESCR2", cQueryName = "NODESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 10, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 51372792,;
   bGlobalFont=.t.,;
    Height=21, Width=355, Left=415, Top=44, InputMask=replicate('X',40)

  func oNODESCR2_10_20.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc

  add object oStr_10_1 as StdString with uid="ILIQAINTSU",Visible=.t., Left=80, Top=84,;
    Alignment=1, Width=77, Height=18,;
    Caption="Rating:"  ;
  , bGlobalFont=.t.

  add object oStr_10_2 as StdString with uid="FWWBRFHIQB",Visible=.t., Left=20, Top=128,;
    Alignment=1, Width=137, Height=18,;
    Caption="Giorni di ritardo:"  ;
  , bGlobalFont=.t.

  add object oStr_10_3 as StdString with uid="VPDDBFRBYL",Visible=.t., Left=32, Top=171,;
    Alignment=1, Width=125, Height=18,;
    Caption="Voce finanziaria:"  ;
  , bGlobalFont=.t.

  add object oStr_10_11 as StdString with uid="VEFPYTLRZL",Visible=.t., Left=85, Top=9,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_10_14 as StdString with uid="QJDGEBFHGM",Visible=.t., Left=458, Top=9,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_10_14.mHide()
    with this.Parent.oContained
      return (.w_NOFLGBEN<>'B')
    endwith
  endfunc

  add object oStr_10_17 as StdString with uid="FESJSGAVSP",Visible=.t., Left=74, Top=44,;
    Alignment=1, Width=83, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  func oStr_10_17.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_10_18 as StdString with uid="HIIEMRKRKF",Visible=.t., Left=427, Top=44,;
    Alignment=1, Width=44, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  func oStr_10_18.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET='EN')
    endwith
  endfunc

  add object oStr_10_21 as StdString with uid="KJMJTNIPTG",Visible=.t., Left=67, Top=44,;
    Alignment=1, Width=90, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  func oStr_10_21.mHide()
    with this.Parent.oContained
      return (.w_NOSOGGET<>'EN')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ano','OFF_NOMI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".NOCODICE=OFF_NOMI.NOCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_ano
Proc EnableBtnStampa(pParent)
  local oBtn
  oBtn = pParent.GetCtrl("S\<tampa")
  if !ISNULL(m.oBtn)
   with pParent
    oBtn.Enabled = .cFunction='Query' AND NOT EMPTY(.w_NOCODICE) AND .w_NOPRIVCY<>'S'
   EndWith
  endif
  oBtn = .null.
EndProc

func  ZoomCF
param maschera
*la variabile conta serve per disattivaer il ricalcolo della data di nascita 
maschera.w_conta = maschera.w_conta + 1 
return GSAR_BFC( maschera , 'w_NOCODFIS'  , maschera.w_NOCOGNOM,maschera.w_NO__NOME,maschera.w_NOLOCNAS,maschera.w_NOPRONAS,maschera.w_NODATNAS,maschera.w_NO_SESSO, , ,maschera.w_CODCOM )
endfunc


Proc LanciaRep(pParent)
  local obj
  If pParent.w_NOFLGBEN='B'      && Solo in AHE
    do gsar_sbn with pParent
  Else
    If g_Agen='S'
      * -- Viene lanciata la maschera di Stampa nominativi
      obj = GSAG_SBU(pParent)
      If ISALT()
        * -- Viene selezionata la stampa n. 6
        obj.w_AGSBU_OUTPUT.value = 6
        obj.w_AGSBU_OUTPUT.Fillprop(6)
      Endif
    Endif
  Endif
Endproc
* --- Fine Area Manuale
