* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_art                                                        *
*              Raggruppamenti tipi file                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-03-11                                                      *
* Last revis.: 2013-05-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_art"))

* --- Class definition
define class tgsut_art as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 598
  Height = 115+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-05-02"
  HelpContextID=126436201
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  RAG_TIPI_IDX = 0
  cFile = "RAG_TIPI"
  cKeySelect = "RASERIAL"
  cKeyWhere  = "RASERIAL=this.w_RASERIAL"
  cKeyWhereODBC = '"RASERIAL="+cp_ToStrODBC(this.w_RASERIAL)';

  cKeyWhereODBCqualified = '"RAG_TIPI.RASERIAL="+cp_ToStrODBC(this.w_RASERIAL)';

  cPrg = "gsut_art"
  cComment = "Raggruppamenti tipi file"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RASERIAL = space(10)
  w_RADESCRI = space(40)
  w_RAESTENS = space(254)
  w_RATIPBST = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_RASERIAL = this.W_RASERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RAG_TIPI','gsut_art')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_artPag1","gsut_art",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Raggruppamento")
      .Pages(1).HelpContextID = 71739754
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='RAG_TIPI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RAG_TIPI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RAG_TIPI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_RASERIAL = NVL(RASERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from RAG_TIPI where RASERIAL=KeySet.RASERIAL
    *
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RAG_TIPI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RAG_TIPI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RAG_TIPI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RASERIAL',this.w_RASERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RASERIAL = NVL(RASERIAL,space(10))
        .op_RASERIAL = .w_RASERIAL
        .w_RADESCRI = NVL(RADESCRI,space(40))
        .w_RAESTENS = NVL(RAESTENS,space(254))
        .w_RATIPBST = NVL(RATIPBST,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'RAG_TIPI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RASERIAL = space(10)
      .w_RADESCRI = space(40)
      .w_RAESTENS = space(254)
      .w_RATIPBST = space(1)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'RAG_TIPI')
    this.DoRTCalc(1,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SERAG","i_CODAZI,w_RASERIAL")
      .op_CODAZI = .w_CODAZI
      .op_RASERIAL = .w_RASERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oRADESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oRAESTENS_1_4.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'RAG_TIPI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RASERIAL,"RASERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RADESCRI,"RADESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RAESTENS,"RAESTENS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RATIPBST,"RATIPBST",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    i_lTable = "RAG_TIPI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RAG_TIPI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.RAG_TIPI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SERAG","i_CODAZI,w_RASERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RAG_TIPI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RAG_TIPI')
        i_extval=cp_InsertValODBCExtFlds(this,'RAG_TIPI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(RASERIAL,RADESCRI,RAESTENS,RATIPBST "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_RASERIAL)+;
                  ","+cp_ToStrODBC(this.w_RADESCRI)+;
                  ","+cp_ToStrODBC(this.w_RAESTENS)+;
                  ","+cp_ToStrODBC(this.w_RATIPBST)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RAG_TIPI')
        i_extval=cp_InsertValVFPExtFlds(this,'RAG_TIPI')
        cp_CheckDeletedKey(i_cTable,0,'RASERIAL',this.w_RASERIAL)
        INSERT INTO (i_cTable);
              (RASERIAL,RADESCRI,RAESTENS,RATIPBST  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_RASERIAL;
                  ,this.w_RADESCRI;
                  ,this.w_RAESTENS;
                  ,this.w_RATIPBST;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.RAG_TIPI_IDX,i_nConn)
      *
      * update RAG_TIPI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'RAG_TIPI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " RADESCRI="+cp_ToStrODBC(this.w_RADESCRI)+;
             ",RAESTENS="+cp_ToStrODBC(this.w_RAESTENS)+;
             ",RATIPBST="+cp_ToStrODBC(this.w_RATIPBST)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'RAG_TIPI')
        i_cWhere = cp_PKFox(i_cTable  ,'RASERIAL',this.w_RASERIAL  )
        UPDATE (i_cTable) SET;
              RADESCRI=this.w_RADESCRI;
             ,RAESTENS=this.w_RAESTENS;
             ,RATIPBST=this.w_RATIPBST;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.RAG_TIPI_IDX,i_nConn)
      *
      * delete RAG_TIPI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'RASERIAL',this.w_RASERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RAG_TIPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RAG_TIPI_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SERAG","i_CODAZI,w_RASERIAL")
          .op_RASERIAL = .w_RASERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oRATIPBST_1_6.visible=!this.oPgFrm.Page1.oPag.oRATIPBST_1_6.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRADESCRI_1_2.value==this.w_RADESCRI)
      this.oPgFrm.Page1.oPag.oRADESCRI_1_2.value=this.w_RADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAESTENS_1_4.value==this.w_RAESTENS)
      this.oPgFrm.Page1.oPag.oRAESTENS_1_4.value=this.w_RAESTENS
    endif
    if not(this.oPgFrm.Page1.oPag.oRATIPBST_1_6.RadioValue()==this.w_RATIPBST)
      this.oPgFrm.Page1.oPag.oRATIPBST_1_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'RAG_TIPI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_artPag1 as StdContainer
  Width  = 594
  height = 115
  stdWidth  = 594
  stdheight = 115
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRADESCRI_1_2 as StdField with uid="RTMMBNXVBW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RADESCRI", cQueryName = "RADESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione raggruppamento",;
    HelpContextID = 15745887,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=92, Top=23, InputMask=replicate('X',40)

  add object oRAESTENS_1_4 as StdField with uid="HIONGQUPKZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RAESTENS", cQueryName = "RAESTENS",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Inserire le estensioni del raggruppamento precedute dal  '.'",;
    HelpContextID = 217164951,;
   bGlobalFont=.t.,;
    Height=21, Width=496, Left=92, Top=55, InputMask=replicate('X',254)


  add object oRATIPBST_1_6 as StdCombo with uid="SNSCNRPTNF",value=4,rtseq=4,rtrep=.f.,left=92,top=87,width=293,height=22, enabled=.f.;
    , ToolTipText = "Tipologia per allegati busta";
    , HelpContextID = 264586090;
    , cFormVar="w_RATIPBST",RowSource=""+"Estensioni per files firmati,"+"Estensioni per allegati generici,"+"Estensioni per files compressi,"+"", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oRATIPBST_1_6.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'G',;
    iif(this.value =3,'Z',;
    iif(this.value =4,' ',;
    space(1))))))
  endfunc
  func oRATIPBST_1_6.GetRadio()
    this.Parent.oContained.w_RATIPBST = this.RadioValue()
    return .t.
  endfunc

  func oRATIPBST_1_6.SetRadio()
    this.Parent.oContained.w_RATIPBST=trim(this.Parent.oContained.w_RATIPBST)
    this.value = ;
      iif(this.Parent.oContained.w_RATIPBST=='F',1,;
      iif(this.Parent.oContained.w_RATIPBST=='G',2,;
      iif(this.Parent.oContained.w_RATIPBST=='Z',3,;
      iif(this.Parent.oContained.w_RATIPBST=='',4,;
      0))))
  endfunc

  func oRATIPBST_1_6.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR EMPTY(.w_RATIPBST))
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="OCOJGAZIOQ",Visible=.t., Left=7, Top=25,;
    Alignment=1, Width=81, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="EVMQSQLIFO",Visible=.t., Left=2, Top=57,;
    Alignment=1, Width=86, Height=18,;
    Caption="Estensioni file:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_art','RAG_TIPI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RASERIAL=RAG_TIPI.RASERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
