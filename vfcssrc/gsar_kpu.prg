* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kpu                                                        *
*              Impostazione pubblicazione agenti                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_125]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-11                                                      *
* Last revis.: 2016-05-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kpu",oParentObject))

* --- Class definition
define class tgsar_kpu as StdForm
  Top    = 3
  Left   = 4

  * --- Standard Properties
  Width  = 693
  Height = 439
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-12"
  HelpContextID=149518185
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  FAM_ARTI_IDX = 0
  MARCHI_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  cPrg = "gsar_kpu"
  cComment = "Impostazione pubblicazione agenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPANA = space(1)
  w_ANAGPUBL = space(1)
  w_OPZIONI = space(3)
  w_PUBBLICA = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DAAGENTE = space(5)
  w_AAGENTE = space(5)
  w_ADESAGE = space(35)
  w_DADESAGE = space(35)
  w_PIVAVUOTA = space(1)
  w_CFISVUOTA = space(1)
  w_ZOOMPRO = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kpuPag1","gsar_kpu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANAGPUBL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMPRO = this.oPgFrm.Pages(1).oPag.ZOOMPRO
    DoDefault()
    proc Destroy()
      this.w_ZOOMPRO = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='GRUMERC'
    this.cWorkTables[3]='CATEGOMO'
    this.cWorkTables[4]='FAM_ARTI'
    this.cWorkTables[5]='MARCHI'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='AGENTI'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        gsma_bpu(this,"MOD")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPANA=space(1)
      .w_ANAGPUBL=space(1)
      .w_OPZIONI=space(3)
      .w_PUBBLICA=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_DAAGENTE=space(5)
      .w_AAGENTE=space(5)
      .w_ADESAGE=space(35)
      .w_DADESAGE=space(35)
      .w_PIVAVUOTA=space(1)
      .w_CFISVUOTA=space(1)
        .w_TIPANA = 'A'
        .w_ANAGPUBL = 'T'
      .oPgFrm.Page1.oPag.ZOOMPRO.Calculate()
        .w_OPZIONI = 'DES'
        .w_PUBBLICA = 'S'
          .DoRTCalc(5,5,.f.)
        .w_OBTEST = i_datsys
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_DAAGENTE))
          .link_1_17('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_AAGENTE))
          .link_1_18('Full')
        endif
          .DoRTCalc(9,10,.f.)
        .w_PIVAVUOTA = 'N'
        .w_CFISVUOTA = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMPRO.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMPRO.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMPRO.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DAAGENTE
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DAAGENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_DAAGENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_DAAGENTE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DAAGENTE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_DAAGENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_DAAGENTE)+"%");

            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DAAGENTE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oDAAGENTE_1_17'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DAAGENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_DAAGENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_DAAGENTE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DAAGENTE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DADESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_DAAGENTE = space(5)
      endif
      this.w_DADESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DAAGENTE<=.w_AAGENTE or empty(.w_AAGENTE))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DAAGENTE = space(5)
        this.w_DADESAGE = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DAAGENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AAGENTE
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AAGENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_AAGENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_AAGENTE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AAGENTE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_AAGENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_AAGENTE)+"%");

            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AAGENTE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oAAGENTE_1_18'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AAGENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_AAGENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_AAGENTE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AAGENTE = NVL(_Link_.AGCODAGE,space(5))
      this.w_ADESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_AAGENTE = space(5)
      endif
      this.w_ADESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_AAGENTE>=.w_DAAGENTE or empty(.w_DAAGENTE))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_AAGENTE = space(5)
        this.w_ADESAGE = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AAGENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANAGPUBL_1_2.RadioValue()==this.w_ANAGPUBL)
      this.oPgFrm.Page1.oPag.oANAGPUBL_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOPZIONI_1_4.RadioValue()==this.w_OPZIONI)
      this.oPgFrm.Page1.oPag.oOPZIONI_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUBBLICA_1_5.RadioValue()==this.w_PUBBLICA)
      this.oPgFrm.Page1.oPag.oPUBBLICA_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDAAGENTE_1_17.value==this.w_DAAGENTE)
      this.oPgFrm.Page1.oPag.oDAAGENTE_1_17.value=this.w_DAAGENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oAAGENTE_1_18.value==this.w_AAGENTE)
      this.oPgFrm.Page1.oPag.oAAGENTE_1_18.value=this.w_AAGENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oADESAGE_1_19.value==this.w_ADESAGE)
      this.oPgFrm.Page1.oPag.oADESAGE_1_19.value=this.w_ADESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDADESAGE_1_20.value==this.w_DADESAGE)
      this.oPgFrm.Page1.oPag.oDADESAGE_1_20.value=this.w_DADESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCFISVUOTA_1_22.RadioValue()==this.w_CFISVUOTA)
      this.oPgFrm.Page1.oPag.oCFISVUOTA_1_22.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_DAAGENTE<=.w_AAGENTE or empty(.w_AAGENTE)))  and not(empty(.w_DAAGENTE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDAAGENTE_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_AAGENTE>=.w_DAAGENTE or empty(.w_DAAGENTE)))  and not(empty(.w_AAGENTE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAAGENTE_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_kpuPag1 as StdContainer
  Width  = 689
  height = 439
  stdWidth  = 689
  stdheight = 439
  resizeXpos=221
  resizeYpos=249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oANAGPUBL_1_2 as StdCombo with uid="MMQGNNFKPH",rtseq=2,rtrep=.f.,left=315,top=9,width=136,height=21;
    , ToolTipText = "Anagrafiche da visualizzare (solo quelle gi� pubblicate, solo quelle ancora da pubblicare oppure tutte)";
    , HelpContextID = 23194450;
    , cFormVar="w_ANAGPUBL",RowSource=""+"Solo pubblicate,"+"Solo non pubblicate,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANAGPUBL_1_2.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oANAGPUBL_1_2.GetRadio()
    this.Parent.oContained.w_ANAGPUBL = this.RadioValue()
    return .t.
  endfunc

  func oANAGPUBL_1_2.SetRadio()
    this.Parent.oContained.w_ANAGPUBL=trim(this.Parent.oContained.w_ANAGPUBL)
    this.value = ;
      iif(this.Parent.oContained.w_ANAGPUBL=='P',1,;
      iif(this.Parent.oContained.w_ANAGPUBL=='N',2,;
      iif(this.Parent.oContained.w_ANAGPUBL=='T',3,;
      0)))
  endfunc


  add object ZOOMPRO as cp_szoombox with uid="SOCIPGHJMX",left=3, top=86, width=688,height=260,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="gsar_kpu",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="AGENTI",cMenuFile="",cZoomOnZoom="",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 28479462

  add object oOPZIONI_1_4 as StdRadio with uid="BZYHCYCHXU",rtseq=3,rtrep=.f.,left=13, top=402, width=129,height=32;
    , ToolTipText = "Flag per selezionare o deselezionare tutto";
    , cFormVar="w_OPZIONI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oOPZIONI_1_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 95060506
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 95060506
      this.Buttons(2).Top=15
      this.SetAll("Width",127)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Flag per selezionare o deselezionare tutto")
      StdRadio::init()
    endproc

  func oOPZIONI_1_4.RadioValue()
    return(iif(this.value =1,'SEL',;
    iif(this.value =2,'DES',;
    space(3))))
  endfunc
  func oOPZIONI_1_4.GetRadio()
    this.Parent.oContained.w_OPZIONI = this.RadioValue()
    return .t.
  endfunc

  func oOPZIONI_1_4.SetRadio()
    this.Parent.oContained.w_OPZIONI=trim(this.Parent.oContained.w_OPZIONI)
    this.value = ;
      iif(this.Parent.oContained.w_OPZIONI=='SEL',1,;
      iif(this.Parent.oContained.w_OPZIONI=='DES',2,;
      0))
  endfunc


  add object oPUBBLICA_1_5 as StdCombo with uid="ZWKIGEDNGE",rtseq=4,rtrep=.f.,left=295,top=410,width=148,height=21;
    , ToolTipText = "Imposta flag di pubblicazione per le anagrafiche selezionate";
    , HelpContextID = 85787447;
    , cFormVar="w_PUBBLICA",RowSource=""+"Pubblica su web,"+"Non pubblicare su web", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUBBLICA_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oPUBBLICA_1_5.GetRadio()
    this.Parent.oContained.w_PUBBLICA = this.RadioValue()
    return .t.
  endfunc

  func oPUBBLICA_1_5.SetRadio()
    this.Parent.oContained.w_PUBBLICA=trim(this.Parent.oContained.w_PUBBLICA)
    this.value = ;
      iif(this.Parent.oContained.w_PUBBLICA=='S',1,;
      iif(this.Parent.oContained.w_PUBBLICA=='N',2,;
      0))
  endfunc


  add object oObj_1_10 as cp_runprogram with uid="AWLGTIXIOW",left=3, top=444, width=246,height=19,;
    caption='GSAR_BPU(OPT)',;
   bGlobalFont=.t.,;
    prg="gsar_bpu('OPT')",;
    cEvent = "w_OPZIONI Changed",;
    nPag=1;
    , HelpContextID = 38158907


  add object oBtn_1_12 as StdButton with uid="NQYKXBIKCI",left=531, top=388, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 260706854;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oObj_1_13 as cp_outputCombo with uid="ZLERXIVPWT",left=103, top=356, width=581,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 28479462


  add object oBtn_1_14 as StdButton with uid="SKBBJFTYSB",left=583, top=388, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'elaborazione";
    , HelpContextID = 149489434;
    , caption='\<OK';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        gsar_bpu(this.Parent.oContained,"MOD")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="KEVDUGZIVH",left=635, top=388, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142200762;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="VAFDASESPB",left=635, top=38, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue interrogazione";
    , HelpContextID = 27404054;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        .notifyevent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDAAGENTE_1_17 as StdField with uid="RMWXASSGXC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DAAGENTE", cQueryName = "DAAGENTE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Selezionare un codice per inizio intervallo",;
    HelpContextID = 162651771,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=121, Top=37, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_DAAGENTE"

  func oDAAGENTE_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oDAAGENTE_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDAAGENTE_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oDAAGENTE_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oAAGENTE_1_18 as StdField with uid="RLOLPLVGJG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_AAGENTE", cQueryName = "AAGENTE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Selezionare un codice per fine intervallo",;
    HelpContextID = 4210182,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=121, Top=61, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_AAGENTE"

  func oAAGENTE_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oAAGENTE_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAAGENTE_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oAAGENTE_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oADESAGE_1_19 as StdField with uid="SIQKBTCWFK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ADESAGE", cQueryName = "ADESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 41820422,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=186, Top=61, InputMask=replicate('X',35)

  add object oDADESAGE_1_20 as StdField with uid="JHXIVWLHWD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DADESAGE", cQueryName = "DADESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 227544699,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=186, Top=37, InputMask=replicate('X',35)

  add object oCFISVUOTA_1_22 as StdCheck with uid="LWMXCSWZYB",rtseq=12,rtrep=.f.,left=560, top=9, caption="Cod.fiscale vuoto",;
    ToolTipText = "Se attivo: visualizza anagrafiche con codice fiscale vuoto",;
    HelpContextID = 30304138,;
    cFormVar="w_CFISVUOTA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCFISVUOTA_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCFISVUOTA_1_22.GetRadio()
    this.Parent.oContained.w_CFISVUOTA = this.RadioValue()
    return .t.
  endfunc

  func oCFISVUOTA_1_22.SetRadio()
    this.Parent.oContained.w_CFISVUOTA=trim(this.Parent.oContained.w_CFISVUOTA)
    this.value = ;
      iif(this.Parent.oContained.w_CFISVUOTA=='S',1,;
      0)
  endfunc

  add object oStr_1_6 as StdString with uid="PIIGYOTNQY",Visible=.t., Left=37, Top=39,;
    Alignment=1, Width=80, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="PGRNPWRIEG",Visible=.t., Left=46, Top=64,;
    Alignment=1, Width=71, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="UJSDYRRSFK",Visible=.t., Left=156, Top=412,;
    Alignment=1, Width=134, Height=18,;
    Caption="Web application:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="WHLLZDGAZA",Visible=.t., Left=233, Top=9,;
    Alignment=1, Width=80, Height=18,;
    Caption="Stato attuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="NCVKEYXFTQ",Visible=.t., Left=11, Top=358,;
    Alignment=1, Width=86, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kpu','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
