* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mou                                                        *
*              Output utente                                                   *
*                                                                              *
*      Author: TAM SOFTWARE & CODELAB                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_56]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-10-24                                                      *
* Last revis.: 2012-10-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_mou"))

* --- Class definition
define class tgsut_mou as StdTrsForm
  Top    = 4
  Left   = 5

  * --- Standard Properties
  Width  = 758
  Height = 330+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-25"
  HelpContextID=164184937
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  OUT_PUTS_IDX = 0
  cpusers_IDX = 0
  AZIENDA_IDX = 0
  INF_AQM_IDX = 0
  cFile = "OUT_PUTS"
  cKeySelect = "OUNOMPRG"
  cKeyWhere  = "OUNOMPRG=this.w_OUNOMPRG"
  cKeyDetail  = "OUNOMPRG=this.w_OUNOMPRG and OUROWNUM=this.w_OUROWNUM"
  cKeyWhereODBC = '"OUNOMPRG="+cp_ToStrODBC(this.w_OUNOMPRG)';

  cKeyDetailWhereODBC = '"OUNOMPRG="+cp_ToStrODBC(this.w_OUNOMPRG)';
      +'+" and OUROWNUM="+cp_ToStrODBC(this.w_OUROWNUM)';

  cKeyWhereODBCqualified = '"OUT_PUTS.OUNOMPRG="+cp_ToStrODBC(this.w_OUNOMPRG)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsut_mou"
  cComment = "Output utente"
  i_nRowNum = 0
  i_nRowPerPage = 14
  icon = "movi.ico"
  cAutoZoom = 'GSUT0MOU'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_OUNOMPRG = space(30)
  w_OUDESPRG = space(35)
  w_OUROWNUM = 0
  w_OUDESCRI = space(100)
  w_OUPREDEF = space(1)
  w_OUFLGQUE = space(1)
  o_OUFLGQUE = space(1)
  w_OUNOMQUE = space(254)
  o_OUNOMQUE = space(254)
  w_OUNOMQUE = space(254)
  w_OUNOMBAT = space(254)
  w_OUNOMREP = space(254)
  w_OUSTESTO = space(1)
  o_OUSTESTO = space(1)
  w_OUCARMEM = 0
  w_OUCODAZI = space(5)
  w_OUCODUTE = 0
  w_NOMUTE = space(20)
  w_QUERY = space(254)
  o_QUERY = space(254)
  w_REPORT = space(254)
  o_REPORT = space(254)
  w_RAGAZI = space(40)
  w_BATCH = space(254)
  o_BATCH = space(254)
  w_OUTIMSTA = space(14)
  w_BTNQRY = .NULL.
  w_BTNBAT = .NULL.
  w_BTNREP = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'OUT_PUTS','gsut_mou')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_mouPag1","gsut_mou",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Output Utente")
      .Pages(1).HelpContextID = 147440507
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oOUNOMPRG_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_BTNQRY = this.oPgFrm.Pages(1).oPag.BTNQRY
    this.w_BTNBAT = this.oPgFrm.Pages(1).oPag.BTNBAT
    this.w_BTNREP = this.oPgFrm.Pages(1).oPag.BTNREP
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_BTNQRY = .NULL.
      this.w_BTNBAT = .NULL.
      this.w_BTNREP = .NULL.
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='cpusers'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='INF_AQM'
    this.cWorkTables[4]='OUT_PUTS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.OUT_PUTS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.OUT_PUTS_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_OUNOMPRG = NVL(OUNOMPRG,space(30))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_14_joined
    link_2_14_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from OUT_PUTS where OUNOMPRG=KeySet.OUNOMPRG
    *                            and OUROWNUM=KeySet.OUROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsut_mou
      * --- Setta Ordine per Data Scadenza
      i_cOrder = 'order by OUROWNUM '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2],this.bLoadRecFilter,this.OUT_PUTS_IDX,"gsut_mou")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('OUT_PUTS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "OUT_PUTS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' OUT_PUTS '
      link_2_14_joined=this.AddJoinedLink_2_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'OUNOMPRG',this.w_OUNOMPRG  )
      select * from (i_cTable) OUT_PUTS where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OUNOMPRG = NVL(OUNOMPRG,space(30))
        .w_OUDESPRG = NVL(OUDESPRG,space(35))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'OUT_PUTS')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_NOMUTE = space(20)
          .w_QUERY = space(254)
          .w_REPORT = space(254)
          .w_RAGAZI = space(40)
          .w_BATCH = space(254)
          .w_OUROWNUM = NVL(OUROWNUM,0)
          .w_OUDESCRI = NVL( cp_TransLoadField('OUDESCRI'),space(100))	    
          .w_OUPREDEF = NVL(OUPREDEF,space(1))
          .w_OUFLGQUE = NVL(OUFLGQUE,space(1))
          .w_OUNOMQUE = NVL(OUNOMQUE,space(254))
          * evitabile
          *.link_2_5('Load')
          .w_OUNOMQUE = NVL(OUNOMQUE,space(254))
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
          .w_OUNOMBAT = NVL(OUNOMBAT,space(254))
        .oPgFrm.Page1.oPag.BTNBAT.Calculate()
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
          .w_OUNOMREP = NVL(OUNOMREP,space(254))
          .w_OUSTESTO = NVL(OUSTESTO,space(1))
          .w_OUCARMEM = NVL(OUCARMEM,0)
          .w_OUCODAZI = NVL(OUCODAZI,space(5))
          if link_2_14_joined
            this.w_OUCODAZI = NVL(AZCODAZI214,NVL(this.w_OUCODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI214,space(40))
          else
          .link_2_14('Load')
          endif
          .w_OUCODUTE = NVL(OUCODUTE,0)
          .link_2_15('Load')
          .w_OUTIMSTA = NVL(OUTIMSTA,space(14))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace OUROWNUM with .w_OUROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_16.enabled = .oPgFrm.Page1.oPag.oBtn_1_16.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_17.enabled = .oPgFrm.Page1.oPag.oBtn_1_17.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_18.enabled = .oPgFrm.Page1.oPag.oBtn_1_18.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_OUNOMPRG=space(30)
      .w_OUDESPRG=space(35)
      .w_OUROWNUM=0
      .w_OUDESCRI=space(100)
      .w_OUPREDEF=space(1)
      .w_OUFLGQUE=space(1)
      .w_OUNOMQUE=space(254)
      .w_OUNOMQUE=space(254)
      .w_OUNOMBAT=space(254)
      .w_OUNOMREP=space(254)
      .w_OUSTESTO=space(1)
      .w_OUCARMEM=0
      .w_OUCODAZI=space(5)
      .w_OUCODUTE=0
      .w_NOMUTE=space(20)
      .w_QUERY=space(254)
      .w_REPORT=space(254)
      .w_RAGAZI=space(40)
      .w_BATCH=space(254)
      .w_OUTIMSTA=space(14)
      if .cFunction<>"Filter"
        .DoRTCalc(1,5,.f.)
        .w_OUFLGQUE = 'V'
        .w_OUNOMQUE = SYS(2014, .w_QUERY)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_OUNOMQUE))
         .link_2_5('Full')
        endif
        .w_OUNOMQUE = SYS(2014, .w_QUERY)
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .w_OUNOMBAT = JUSTSTEM(.w_BATCH)
        .oPgFrm.Page1.oPag.BTNBAT.Calculate()
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .w_OUNOMREP = IIF(.w_OUFLGQUE = 'I', .w_OUNOMQUE, IIF(EMPTY(.w_REPORT), .w_OUNOMREP, SYS(2014, .w_REPORT)))
        .w_OUSTESTO = ' '
        .w_OUCARMEM = IIF(.w_OUSTESTO<>'S',0,.w_OUCARMEM)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_OUCODAZI))
         .link_2_14('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_OUCODUTE))
         .link_2_15('Full')
        endif
        .DoRTCalc(15,19,.f.)
        .w_OUTIMSTA = alltrim(STR (year(i_datsys)))  +  right( '0'+alltrim(str(month(i_datsys))),2)  +  right( '0'+alltrim(str(day(i_datsys))),2) + right( '0'+alltrim(str(hour(DATETIME()))),2) + right( '0'+alltrim(str(minute(DATETIME()))),2) + right( '0'+alltrim (str(sec(DATETIME()))),2)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'OUT_PUTS')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oOUNOMPRG_1_1.enabled = i_bVal
      .Page1.oPag.oOUDESPRG_1_2.enabled = i_bVal
      .Page1.oPag.oOUFLGQUE_2_4.enabled = i_bVal
      .Page1.oPag.oOUNOMQUE_2_5.enabled = i_bVal
      .Page1.oPag.oOUNOMQUE_2_6.enabled = i_bVal
      .Page1.oPag.oOUNOMBAT_2_8.enabled = i_bVal
      .Page1.oPag.oOUNOMREP_2_11.enabled = i_bVal
      .Page1.oPag.oOUSTESTO_2_12.enabled = i_bVal
      .Page1.oPag.oOUCARMEM_2_13.enabled = i_bVal
      .Page1.oPag.oOUCODAZI_2_14.enabled = i_bVal
      .Page1.oPag.oOUCODUTE_2_15.enabled = i_bVal
      .Page1.oPag.oBtn_1_16.enabled = .Page1.oPag.oBtn_1_16.mCond()
      .Page1.oPag.oBtn_1_17.enabled = .Page1.oPag.oBtn_1_17.mCond()
      .Page1.oPag.oBtn_1_18.enabled = .Page1.oPag.oBtn_1_18.mCond()
      .Page1.oPag.BTNQRY.enabled = i_bVal
      .Page1.oPag.BTNBAT.enabled = i_bVal
      .Page1.oPag.BTNREP.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oOUNOMPRG_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oOUNOMPRG_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'OUT_PUTS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OUNOMPRG,"OUNOMPRG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_OUDESPRG,"OUDESPRG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
    i_lTable = "OUT_PUTS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.OUT_PUTS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SOU with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_OUROWNUM N(3);
      ,t_OUDESCRI C(100);
      ,t_OUPREDEF N(3);
      ,t_OUFLGQUE N(3);
      ,t_OUNOMQUE C(254);
      ,t_OUNOMBAT C(254);
      ,t_OUNOMREP C(254);
      ,t_OUSTESTO N(3);
      ,t_OUCARMEM N(3);
      ,t_OUCODAZI C(5);
      ,t_OUCODUTE N(4);
      ,t_NOMUTE C(20);
      ,t_RAGAZI C(40);
      ,t_OUTIMSTA C(14);
      ,OUROWNUM N(3);
      ,t_QUERY C(254);
      ,t_REPORT C(254);
      ,t_BATCH C(254);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_moubodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOUROWNUM_2_1.controlsource=this.cTrsName+'.t_OUROWNUM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOUDESCRI_2_2.controlsource=this.cTrsName+'.t_OUDESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oOUPREDEF_2_3.controlsource=this.cTrsName+'.t_OUPREDEF'
    this.oPgFRm.Page1.oPag.oOUFLGQUE_2_4.controlsource=this.cTrsName+'.t_OUFLGQUE'
    this.oPgFRm.Page1.oPag.oOUNOMQUE_2_5.controlsource=this.cTrsName+'.t_OUNOMQUE'
    this.oPgFRm.Page1.oPag.oOUNOMQUE_2_6.controlsource=this.cTrsName+'.t_OUNOMQUE'
    this.oPgFRm.Page1.oPag.oOUNOMBAT_2_8.controlsource=this.cTrsName+'.t_OUNOMBAT'
    this.oPgFRm.Page1.oPag.oOUNOMREP_2_11.controlsource=this.cTrsName+'.t_OUNOMREP'
    this.oPgFRm.Page1.oPag.oOUSTESTO_2_12.controlsource=this.cTrsName+'.t_OUSTESTO'
    this.oPgFRm.Page1.oPag.oOUCARMEM_2_13.controlsource=this.cTrsName+'.t_OUCARMEM'
    this.oPgFRm.Page1.oPag.oOUCODAZI_2_14.controlsource=this.cTrsName+'.t_OUCODAZI'
    this.oPgFRm.Page1.oPag.oOUCODUTE_2_15.controlsource=this.cTrsName+'.t_OUCODUTE'
    this.oPgFRm.Page1.oPag.oNOMUTE_2_16.controlsource=this.cTrsName+'.t_NOMUTE'
    this.oPgFRm.Page1.oPag.oRAGAZI_2_19.controlsource=this.cTrsName+'.t_RAGAZI'
    this.oPgFRm.Page1.oPag.oOUTIMSTA_2_23.controlsource=this.cTrsName+'.t_OUTIMSTA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(37)
    this.AddVLine(364)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOUROWNUM_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
      *
      * insert into OUT_PUTS
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'OUT_PUTS')
        i_extval=cp_InsertValODBCExtFlds(this,'OUT_PUTS')
        i_cFldBody=" "+;
                  "(OUNOMPRG,OUDESPRG,OUROWNUM,OUDESCRI,OUPREDEF"+;
                  ",OUFLGQUE,OUNOMQUE,OUNOMBAT,OUNOMREP,OUSTESTO"+;
                  ",OUCARMEM,OUCODAZI,OUCODUTE,OUTIMSTA,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_OUNOMPRG)+","+cp_ToStrODBC(this.w_OUDESPRG)+","+cp_ToStrODBC(this.w_OUROWNUM)+","+cp_ToStrODBC(this.w_OUDESCRI)+","+cp_ToStrODBC(this.w_OUPREDEF)+;
             ","+cp_ToStrODBC(this.w_OUFLGQUE)+","+cp_ToStrODBCNull(this.w_OUNOMQUE)+","+cp_ToStrODBC(this.w_OUNOMBAT)+","+cp_ToStrODBC(this.w_OUNOMREP)+","+cp_ToStrODBC(this.w_OUSTESTO)+;
             ","+cp_ToStrODBC(this.w_OUCARMEM)+","+cp_ToStrODBCNull(this.w_OUCODAZI)+","+cp_ToStrODBCNull(this.w_OUCODUTE)+","+cp_ToStrODBC(this.w_OUTIMSTA)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'OUT_PUTS')
        i_extval=cp_InsertValVFPExtFlds(this,'OUT_PUTS')
        cp_CheckDeletedKey(i_cTable,0,'OUNOMPRG',this.w_OUNOMPRG,'OUROWNUM',this.w_OUROWNUM)
        INSERT INTO (i_cTable) (;
                   OUNOMPRG;
                  ,OUDESPRG;
                  ,OUROWNUM;
                  ,OUDESCRI;
                  ,OUPREDEF;
                  ,OUFLGQUE;
                  ,OUNOMQUE;
                  ,OUNOMBAT;
                  ,OUNOMREP;
                  ,OUSTESTO;
                  ,OUCARMEM;
                  ,OUCODAZI;
                  ,OUCODUTE;
                  ,OUTIMSTA;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_OUNOMPRG;
                  ,this.w_OUDESPRG;
                  ,this.w_OUROWNUM;
                  ,this.w_OUDESCRI;
                  ,this.w_OUPREDEF;
                  ,this.w_OUFLGQUE;
                  ,this.w_OUNOMQUE;
                  ,this.w_OUNOMBAT;
                  ,this.w_OUNOMREP;
                  ,this.w_OUSTESTO;
                  ,this.w_OUCARMEM;
                  ,this.w_OUCODAZI;
                  ,this.w_OUCODUTE;
                  ,this.w_OUTIMSTA;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_OUROWNUM<>0 AND NOT EMPTY(t_OUDESCRI)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'OUT_PUTS')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " OUDESPRG="+cp_ToStrODBC(this.w_OUDESPRG)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and OUROWNUM="+cp_ToStrODBC(&i_TN.->OUROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'OUT_PUTS')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  OUDESPRG=this.w_OUDESPRG;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and OUROWNUM=&i_TN.->OUROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_OUROWNUM<>0 AND NOT EMPTY(t_OUDESCRI)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and OUROWNUM="+cp_ToStrODBC(&i_TN.->OUROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and OUROWNUM=&i_TN.->OUROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace OUROWNUM with this.w_OUROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update OUT_PUTS
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'OUT_PUTS')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " OUDESPRG="+cp_ToStrODBC(this.w_OUDESPRG)+;
                     ",OUDESCRI="+cp_TransUpdFldName('OUDESCRI',this.w_OUDESCRI)+;	
                     ",OUPREDEF="+cp_ToStrODBC(this.w_OUPREDEF)+;
                     ",OUFLGQUE="+cp_ToStrODBC(this.w_OUFLGQUE)+;
                     ",OUNOMQUE="+cp_ToStrODBCNull(this.w_OUNOMQUE)+;
                     ",OUNOMBAT="+cp_ToStrODBC(this.w_OUNOMBAT)+;
                     ",OUNOMREP="+cp_ToStrODBC(this.w_OUNOMREP)+;
                     ",OUSTESTO="+cp_ToStrODBC(this.w_OUSTESTO)+;
                     ",OUCARMEM="+cp_ToStrODBC(this.w_OUCARMEM)+;
                     ",OUCODAZI="+cp_ToStrODBCNull(this.w_OUCODAZI)+;
                     ",OUCODUTE="+cp_ToStrODBCNull(this.w_OUCODUTE)+;
                     ",OUTIMSTA="+cp_ToStrODBC(this.w_OUTIMSTA)+;
                     ",OUROWNUM="+cp_ToStrODBC(this.w_OUROWNUM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and OUROWNUM="+cp_ToStrODBC(OUROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'OUT_PUTS')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      OUDESPRG=this.w_OUDESPRG;
                     ,OUDESCRI=this.w_OUDESCRI;
                     ,OUPREDEF=this.w_OUPREDEF;
                     ,OUFLGQUE=this.w_OUFLGQUE;
                     ,OUNOMQUE=this.w_OUNOMQUE;
                     ,OUNOMBAT=this.w_OUNOMBAT;
                     ,OUNOMREP=this.w_OUNOMREP;
                     ,OUSTESTO=this.w_OUSTESTO;
                     ,OUCARMEM=this.w_OUCARMEM;
                     ,OUCODAZI=this.w_OUCODAZI;
                     ,OUCODUTE=this.w_OUCODUTE;
                     ,OUTIMSTA=this.w_OUTIMSTA;
                     ,OUROWNUM=this.w_OUROWNUM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and OUROWNUM=&i_TN.->OUROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_OUROWNUM<>0 AND NOT EMPTY(t_OUDESCRI)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete OUT_PUTS
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and OUROWNUM="+cp_ToStrODBC(&i_TN.->OUROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and OUROWNUM=&i_TN.->OUROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_OUROWNUM<>0 AND NOT EMPTY(t_OUDESCRI)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_QUERY<>.w_QUERY
          .w_OUNOMQUE = SYS(2014, .w_QUERY)
          .link_2_5('Full')
        endif
        if .o_QUERY<>.w_QUERY
          .w_OUNOMQUE = SYS(2014, .w_QUERY)
        endif
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        if .o_BATCH<>.w_BATCH
          .w_OUNOMBAT = JUSTSTEM(.w_BATCH)
        endif
        .oPgFrm.Page1.oPag.BTNBAT.Calculate()
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        if .o_REPORT<>.w_REPORT.or. .o_OUFLGQUE<>.w_OUFLGQUE.or. .o_OUNOMQUE<>.w_OUNOMQUE
          .w_OUNOMREP = IIF(.w_OUFLGQUE = 'I', .w_OUNOMQUE, IIF(EMPTY(.w_REPORT), .w_OUNOMREP, SYS(2014, .w_REPORT)))
        endif
        .DoRTCalc(11,11,.t.)
        if .o_OUSTESTO<>.w_OUSTESTO
          .w_OUCARMEM = IIF(.w_OUSTESTO<>'S',0,.w_OUCARMEM)
        endif
        if .o_OUFLGQUE<>.w_OUFLGQUE
          .Calculate_WKZMUPIAJK()
        endif
        if .o_OUNOMQUE<>.w_OUNOMQUE
          .Calculate_QYOKBBZQXN()
        endif
        .DoRTCalc(13,19,.t.)
          .w_OUTIMSTA = alltrim(STR (year(i_datsys)))  +  right( '0'+alltrim(str(month(i_datsys))),2)  +  right( '0'+alltrim(str(day(i_datsys))),2) + right( '0'+alltrim(str(hour(DATETIME()))),2) + right( '0'+alltrim(str(minute(DATETIME()))),2) + right( '0'+alltrim (str(sec(DATETIME()))),2)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_QUERY with this.w_QUERY
      replace t_REPORT with this.w_REPORT
      replace t_BATCH with this.w_BATCH
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page1.oPag.BTNBAT.Calculate()
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .oPgFrm.Page1.oPag.BTNBAT.Calculate()
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
    endwith
  return
  proc Calculate_WKZMUPIAJK()
    with this
          * --- Nasconde bottoni
          .w_BTNQRY.Enabled = .w_OUFLGQUE <> 'I' AND .cFunction<>"Query"
          .w_BTNREP.Enabled = .w_OUFLGQUE <> 'I' AND .cFunction<>"Query"
          .w_BTNBAT.Enabled = .w_OUFLGQUE <> 'I' AND .cFunction<>"Query" And Upper(Alltrim(.w_OUNOMPRG))$'GSAC_MDV-GSDM_SAU-GSDM_SCD-GSDM_SPD-GSDM_SPV-GSDM_SVA-GSDM_SVL-GSDS_SDP-GSVE_MDV-GSVEAMDV-GSACAMDV'
          .w_OUNOMQUE = SPACE(254)
    endwith
  endproc
  proc Calculate_QYOKBBZQXN()
    with this
          * --- Cambio estensione query
          .w_OUNOMQUE = IIF(.w_OUFLGQUE = 'I', MODIFEXT( .w_OUNOMQUE, 'IRP', .T. ), .w_OUNOMQUE)
          .w_OUNOMREP = IIF(.w_OUFLGQUE = 'I', .w_OUNOMQUE, .w_OUNOMREP)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oOUFLGQUE_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oOUFLGQUE_2_4.mCond()
    this.oPgFrm.Page1.oPag.oOUNOMQUE_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oOUNOMQUE_2_5.mCond()
    this.oPgFrm.Page1.oPag.oOUNOMQUE_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oOUNOMQUE_2_6.mCond()
    this.oPgFrm.Page1.oPag.oOUNOMBAT_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oOUNOMBAT_2_8.mCond()
    this.oPgFrm.Page1.oPag.oOUNOMREP_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oOUNOMREP_2_11.mCond()
    this.oPgFrm.Page1.oPag.oOUSTESTO_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oOUSTESTO_2_12.mCond()
    this.oPgFrm.Page1.oPag.oOUCARMEM_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oOUCARMEM_2_13.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oOUFLGQUE_2_4.visible=!this.oPgFrm.Page1.oPag.oOUFLGQUE_2_4.mHide()
    this.oPgFrm.Page1.oPag.oOUNOMQUE_2_5.visible=!this.oPgFrm.Page1.oPag.oOUNOMQUE_2_5.mHide()
    this.oPgFrm.Page1.oPag.oOUNOMQUE_2_6.visible=!this.oPgFrm.Page1.oPag.oOUNOMQUE_2_6.mHide()
    this.oPgFrm.Page1.oPag.oOUCARMEM_2_13.visible=!this.oPgFrm.Page1.oPag.oOUCARMEM_2_13.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.BTNQRY.Event(cEvent)
      .oPgFrm.Page1.oPag.BTNBAT.Event(cEvent)
      .oPgFrm.Page1.oPag.BTNREP.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_mou
    * --- Nasconde bottoni
    with this
      .w_BTNQRY.Enabled = .w_OUFLGQUE <> 'I' AND .cFunction<>"Query"
      .w_BTNREP.Enabled = .w_OUFLGQUE <> 'I' AND .cFunction<>"Query"
      .w_BTNBAT.Enabled = .w_OUFLGQUE <> 'I' AND .cFunction<>"Query" And Upper(Alltrim(.w_OUNOMPRG))$'GSAC_MDV-GSDM_SAU-GSDM_SCD-GSDM_SPD-GSDM_SPV-GSDM_SVA-GSDM_SVL-GSDS_SDP-GSVE_MDV-GSVEAMDV-GSACAMDV'
    endwith
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=OUNOMQUE
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INF_AQM_IDX,3]
    i_lTable = "INF_AQM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INF_AQM_IDX,2], .t., this.INF_AQM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INF_AQM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OUNOMQUE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsir_maq',True,'INF_AQM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AQCODICE like "+cp_ToStrODBC(trim(this.w_OUNOMQUE)+"%");

          i_ret=cp_SQL(i_nConn,"select AQCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AQCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AQCODICE',trim(this.w_OUNOMQUE))
          select AQCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AQCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OUNOMQUE)==trim(_Link_.AQCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OUNOMQUE) and !this.bDontReportError
            deferred_cp_zoom('INF_AQM','*','AQCODICE',cp_AbsName(oSource.parent,'oOUNOMQUE_2_5'),i_cWhere,'gsir_maq',"Query InfoPublisher",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AQCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AQCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AQCODICE',oSource.xKey(1))
            select AQCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OUNOMQUE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AQCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AQCODICE="+cp_ToStrODBC(this.w_OUNOMQUE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AQCODICE',this.w_OUNOMQUE)
            select AQCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OUNOMQUE = NVL(_Link_.AQCODICE,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_OUNOMQUE = space(254)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INF_AQM_IDX,2])+'\'+cp_ToStr(_Link_.AQCODICE,1)
      cp_ShowWarn(i_cKey,this.INF_AQM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OUNOMQUE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OUCODAZI
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OUCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AZIENDA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_OUCODAZI)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_OUCODAZI))
          select AZCODAZI,AZRAGAZI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OUCODAZI)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OUCODAZI) and !this.bDontReportError
            deferred_cp_zoom('AZIENDA','*','AZCODAZI',cp_AbsName(oSource.parent,'oOUCODAZI_2_14'),i_cWhere,'',"Elenco aziende",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OUCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_OUCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_OUCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OUCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_OUCODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OUCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_14.AZCODAZI as AZCODAZI214"+ ",link_2_14.AZRAGAZI as AZRAGAZI214"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_14 on OUT_PUTS.OUCODAZI=link_2_14.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_14"
          i_cKey=i_cKey+'+" and OUT_PUTS.OUCODAZI=link_2_14.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=OUCODUTE
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OUCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_OUCODUTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_OUCODUTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_OUCODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oOUCODUTE_2_15'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OUCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_OUCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_OUCODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OUCODUTE = NVL(_Link_.code,0)
      this.w_NOMUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OUCODUTE = 0
      endif
      this.w_NOMUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OUCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oOUNOMPRG_1_1.value==this.w_OUNOMPRG)
      this.oPgFrm.Page1.oPag.oOUNOMPRG_1_1.value=this.w_OUNOMPRG
    endif
    if not(this.oPgFrm.Page1.oPag.oOUDESPRG_1_2.value==this.w_OUDESPRG)
      this.oPgFrm.Page1.oPag.oOUDESPRG_1_2.value=this.w_OUDESPRG
    endif
    if not(this.oPgFrm.Page1.oPag.oOUFLGQUE_2_4.RadioValue()==this.w_OUFLGQUE)
      this.oPgFrm.Page1.oPag.oOUFLGQUE_2_4.SetRadio()
      replace t_OUFLGQUE with this.oPgFrm.Page1.oPag.oOUFLGQUE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOUNOMQUE_2_5.value==this.w_OUNOMQUE)
      this.oPgFrm.Page1.oPag.oOUNOMQUE_2_5.value=this.w_OUNOMQUE
      replace t_OUNOMQUE with this.oPgFrm.Page1.oPag.oOUNOMQUE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOUNOMQUE_2_6.value==this.w_OUNOMQUE)
      this.oPgFrm.Page1.oPag.oOUNOMQUE_2_6.value=this.w_OUNOMQUE
      replace t_OUNOMQUE with this.oPgFrm.Page1.oPag.oOUNOMQUE_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOUNOMBAT_2_8.value==this.w_OUNOMBAT)
      this.oPgFrm.Page1.oPag.oOUNOMBAT_2_8.value=this.w_OUNOMBAT
      replace t_OUNOMBAT with this.oPgFrm.Page1.oPag.oOUNOMBAT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOUNOMREP_2_11.value==this.w_OUNOMREP)
      this.oPgFrm.Page1.oPag.oOUNOMREP_2_11.value=this.w_OUNOMREP
      replace t_OUNOMREP with this.oPgFrm.Page1.oPag.oOUNOMREP_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOUSTESTO_2_12.RadioValue()==this.w_OUSTESTO)
      this.oPgFrm.Page1.oPag.oOUSTESTO_2_12.SetRadio()
      replace t_OUSTESTO with this.oPgFrm.Page1.oPag.oOUSTESTO_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOUCARMEM_2_13.value==this.w_OUCARMEM)
      this.oPgFrm.Page1.oPag.oOUCARMEM_2_13.value=this.w_OUCARMEM
      replace t_OUCARMEM with this.oPgFrm.Page1.oPag.oOUCARMEM_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOUCODAZI_2_14.value==this.w_OUCODAZI)
      this.oPgFrm.Page1.oPag.oOUCODAZI_2_14.value=this.w_OUCODAZI
      replace t_OUCODAZI with this.oPgFrm.Page1.oPag.oOUCODAZI_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOUCODUTE_2_15.value==this.w_OUCODUTE)
      this.oPgFrm.Page1.oPag.oOUCODUTE_2_15.value=this.w_OUCODUTE
      replace t_OUCODUTE with this.oPgFrm.Page1.oPag.oOUCODUTE_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMUTE_2_16.value==this.w_NOMUTE)
      this.oPgFrm.Page1.oPag.oNOMUTE_2_16.value=this.w_NOMUTE
      replace t_NOMUTE with this.oPgFrm.Page1.oPag.oNOMUTE_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_2_19.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_2_19.value=this.w_RAGAZI
      replace t_RAGAZI with this.oPgFrm.Page1.oPag.oRAGAZI_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oOUTIMSTA_2_23.value==this.w_OUTIMSTA)
      this.oPgFrm.Page1.oPag.oOUTIMSTA_2_23.value=this.w_OUTIMSTA
      replace t_OUTIMSTA with this.oPgFrm.Page1.oPag.oOUTIMSTA_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOUROWNUM_2_1.value==this.w_OUROWNUM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOUROWNUM_2_1.value=this.w_OUROWNUM
      replace t_OUROWNUM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOUROWNUM_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOUDESCRI_2_2.value==this.w_OUDESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOUDESCRI_2_2.value=this.w_OUDESCRI
      replace t_OUDESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOUDESCRI_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOUPREDEF_2_3.RadioValue()==this.w_OUPREDEF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOUPREDEF_2_3.SetRadio()
      replace t_OUPREDEF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOUPREDEF_2_3.value
    endif
    cp_SetControlsValueExtFlds(this,'OUT_PUTS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (t_OUROWNUM<>0 AND NOT EMPTY(t_OUDESCRI));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .w_OUROWNUM<>0 AND NOT EMPTY(.w_OUDESCRI)
        * --- Area Manuale = Check Row
        * --- gsut_mou
        * --- Impedisco l'utilizzo del check solo testo
        * --- per stampe visual fox pro (nel caso)
        * --- l'utente esegua questa combinazione
        * --- l'applicativo si blocca
        If Not Empty(.w_OUNOMREP) And (( .w_OUSTESTO='S' And Upper(Right(alltrim(.w_OUNOMREP),3))<>'FXP'))
         i_bRes = .f.
         i_bnoChk=.f.
         i_cErrorMsg = Ah_MsgFormat("Se attivato 'Solo testo', selezionare un file di tipo FXP")
        endif
        
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_OUFLGQUE = this.w_OUFLGQUE
    this.o_OUNOMQUE = this.w_OUNOMQUE
    this.o_OUSTESTO = this.w_OUSTESTO
    this.o_QUERY = this.w_QUERY
    this.o_REPORT = this.w_REPORT
    this.o_BATCH = this.w_BATCH
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_OUROWNUM<>0 AND NOT EMPTY(t_OUDESCRI))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_OUROWNUM=0
      .w_OUDESCRI=space(100)
      .w_OUPREDEF=space(1)
      .w_OUFLGQUE=space(1)
      .w_OUNOMQUE=space(254)
      .w_OUNOMQUE=space(254)
      .w_OUNOMBAT=space(254)
      .w_OUNOMREP=space(254)
      .w_OUSTESTO=space(1)
      .w_OUCARMEM=0
      .w_OUCODAZI=space(5)
      .w_OUCODUTE=0
      .w_NOMUTE=space(20)
      .w_QUERY=space(254)
      .w_REPORT=space(254)
      .w_RAGAZI=space(40)
      .w_BATCH=space(254)
      .w_OUTIMSTA=space(14)
      .DoRTCalc(1,5,.f.)
        .w_OUFLGQUE = 'V'
        .w_OUNOMQUE = SYS(2014, .w_QUERY)
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_OUNOMQUE))
        .link_2_5('Full')
      endif
        .w_OUNOMQUE = SYS(2014, .w_QUERY)
        .oPgFrm.Page1.oPag.BTNQRY.Calculate()
        .w_OUNOMBAT = JUSTSTEM(.w_BATCH)
        .oPgFrm.Page1.oPag.BTNBAT.Calculate()
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .w_OUNOMREP = IIF(.w_OUFLGQUE = 'I', .w_OUNOMQUE, IIF(EMPTY(.w_REPORT), .w_OUNOMREP, SYS(2014, .w_REPORT)))
        .w_OUSTESTO = ' '
        .w_OUCARMEM = IIF(.w_OUSTESTO<>'S',0,.w_OUCARMEM)
      .DoRTCalc(13,13,.f.)
      if not(empty(.w_OUCODAZI))
        .link_2_14('Full')
      endif
      .DoRTCalc(14,14,.f.)
      if not(empty(.w_OUCODUTE))
        .link_2_15('Full')
      endif
      .DoRTCalc(15,19,.f.)
        .w_OUTIMSTA = alltrim(STR (year(i_datsys)))  +  right( '0'+alltrim(str(month(i_datsys))),2)  +  right( '0'+alltrim(str(day(i_datsys))),2) + right( '0'+alltrim(str(hour(DATETIME()))),2) + right( '0'+alltrim(str(minute(DATETIME()))),2) + right( '0'+alltrim (str(sec(DATETIME()))),2)
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_OUROWNUM = t_OUROWNUM
    this.w_OUDESCRI = t_OUDESCRI
    this.w_OUPREDEF = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOUPREDEF_2_3.RadioValue(.t.)
    this.w_OUFLGQUE = this.oPgFrm.Page1.oPag.oOUFLGQUE_2_4.RadioValue(.t.)
    this.w_OUNOMQUE = t_OUNOMQUE
    this.w_OUNOMQUE = t_OUNOMQUE
    this.w_OUNOMBAT = t_OUNOMBAT
    this.w_OUNOMREP = t_OUNOMREP
    this.w_OUSTESTO = this.oPgFrm.Page1.oPag.oOUSTESTO_2_12.RadioValue(.t.)
    this.w_OUCARMEM = t_OUCARMEM
    this.w_OUCODAZI = t_OUCODAZI
    this.w_OUCODUTE = t_OUCODUTE
    this.w_NOMUTE = t_NOMUTE
    this.w_QUERY = t_QUERY
    this.w_REPORT = t_REPORT
    this.w_RAGAZI = t_RAGAZI
    this.w_BATCH = t_BATCH
    this.w_OUTIMSTA = t_OUTIMSTA
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_OUROWNUM with this.w_OUROWNUM
    replace t_OUDESCRI with this.w_OUDESCRI
    replace t_OUPREDEF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oOUPREDEF_2_3.ToRadio()
    replace t_OUFLGQUE with this.oPgFrm.Page1.oPag.oOUFLGQUE_2_4.ToRadio()
    replace t_OUNOMQUE with this.w_OUNOMQUE
    replace t_OUNOMQUE with this.w_OUNOMQUE
    replace t_OUNOMBAT with this.w_OUNOMBAT
    replace t_OUNOMREP with this.w_OUNOMREP
    replace t_OUSTESTO with this.oPgFrm.Page1.oPag.oOUSTESTO_2_12.ToRadio()
    replace t_OUCARMEM with this.w_OUCARMEM
    replace t_OUCODAZI with this.w_OUCODAZI
    replace t_OUCODUTE with this.w_OUCODUTE
    replace t_NOMUTE with this.w_NOMUTE
    replace t_QUERY with this.w_QUERY
    replace t_REPORT with this.w_REPORT
    replace t_RAGAZI with this.w_RAGAZI
    replace t_BATCH with this.w_BATCH
    replace t_OUTIMSTA with this.w_OUTIMSTA
    if i_srv='A'
      replace OUROWNUM with this.w_OUROWNUM
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_mouPag1 as StdContainer
  Width  = 754
  height = 331
  stdWidth  = 754
  stdheight = 331
  resizeXpos=314
  resizeYpos=171
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oOUNOMPRG_1_1 as StdField with uid="PUUKDYZQAC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_OUNOMPRG", cQueryName = "OUNOMPRG",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nome della procedura di gestione (es. maschera di stampa)",;
    HelpContextID = 190510893,;
   bGlobalFont=.t.,;
    Height=21, Width=239, Left=158, Top=9, cGetPict='REPL("!",30)', InputMask=replicate('X',30)

  add object oOUDESPRG_1_2 as StdField with uid="ZOCHAYYLBI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_OUDESPRG", cQueryName = "OUDESPRG",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della procedura di gestione",;
    HelpContextID = 196106029,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=399, Top=9, InputMask=replicate('X',35)


  add object oBtn_1_16 as StdButton with uid="RWDSSEOVZU",left=653, top=286, width=48,height=45,;
    CpPicture="BMP\save.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la gestione di salvataggio output utente su file";
    , HelpContextID = 195196438;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      do GSUT_KOU with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    with this.Parent.oContained
      return (.cfunction='Query')
    endwith
  endfunc


  add object oBtn_1_17 as StdButton with uid="FCTKAGOXAT",left=704, top=286, width=48,height=45,;
    CpPicture="BMP\carica.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la gestione di caricamento output utente da file";
    , HelpContextID = 195196438;
    , caption='\<Carica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      do GSUT_KCO with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_17.mCond()
    with this.Parent.oContained
      return (.cfunction='Query')
    endwith
  endfunc


  add object oBtn_1_18 as StdButton with uid="CPJUJZQMDN",left=704, top=236, width=48,height=45,;
    CpPicture="BMP\carica.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per caricare le traduzioni delle descrizioni nelle lingue abilitate";
    , HelpContextID = 195196438;
    , caption='Caric. \<Tr.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        do GSUT_BTR with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    with this.Parent.oContained
      return (.cfunction='Query')
    endwith
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=36, width=404,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="OUROWNUM",Label1="Num.",Field2="OUDESCRI",Label2="Descrizione",Field3="OUPREDEF",Label3="Predef.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 151905914

  add object oStr_1_3 as StdString with uid="QQDQTKLLXW",Visible=.t., Left=7, Top=9,;
    Alignment=1, Width=147, Height=15,;
    Caption="Programma di gestione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="YCAVLPKLIK",Visible=.t., Left=417, Top=54,;
    Alignment=0, Width=298, Height=15,;
    Caption="Query"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="IBQSVSKOUO",Visible=.t., Left=417, Top=142,;
    Alignment=0, Width=301, Height=15,;
    Caption="Report"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="EGPHCODMNJ",Visible=.t., Left=417, Top=283,;
    Alignment=0, Width=213, Height=15,;
    Caption="Filtro utente"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="EEZGPXGQIB",Visible=.t., Left=417, Top=234,;
    Alignment=0, Width=283, Height=15,;
    Caption="Filtro azienda"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="UJBVYLGNOP",Visible=.t., Left=528, Top=189,;
    Alignment=1, Width=130, Height=18,;
    Caption="Caratteri memo:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_OUFLGQUE <> 'V' OR UPPER (g_APPLICATION) <>'ADHOC REVOLUTION')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="YEHTGNGTYM",Visible=.t., Left=505, Top=34,;
    Alignment=1, Width=136, Height=15,;
    Caption="Tipo query:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (g_IRDR <> 'S')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="VCINEZEQHI",Visible=.t., Left=417, Top=98,;
    Alignment=0, Width=298, Height=15,;
    Caption="Program"  ;
  , bGlobalFont=.t.

  add object oBox_1_8 as StdBox with uid="XSIJFKTXZL",left=419, top=71, width=333,height=1

  add object oBox_1_9 as StdBox with uid="JZSRXEJVKS",left=417, top=299, width=220,height=1

  add object oBox_1_10 as StdBox with uid="NYBQOJPSPZ",left=419, top=159, width=333,height=1

  add object oBox_1_11 as StdBox with uid="YEPWZKOWEZ",left=417, top=253, width=286,height=1

  add object oBox_1_15 as StdBox with uid="NIUSIGRUMX",left=419, top=115, width=333,height=1

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=55,;
    width=400+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=56,width=399+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*14*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oOUFLGQUE_2_4.Refresh()
      this.Parent.oOUNOMQUE_2_5.Refresh()
      this.Parent.oOUNOMQUE_2_6.Refresh()
      this.Parent.oOUNOMBAT_2_8.Refresh()
      this.Parent.oOUNOMREP_2_11.Refresh()
      this.Parent.oOUSTESTO_2_12.Refresh()
      this.Parent.oOUCARMEM_2_13.Refresh()
      this.Parent.oOUCODAZI_2_14.Refresh()
      this.Parent.oOUCODUTE_2_15.Refresh()
      this.Parent.oNOMUTE_2_16.Refresh()
      this.Parent.oRAGAZI_2_19.Refresh()
      this.Parent.oOUTIMSTA_2_23.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oOUFLGQUE_2_4 as StdTrsCombo with uid="DCXASFMRMN",rtrep=.t.,;
    cFormVar="w_OUFLGQUE", RowSource=""+"Visual query,"+"InfoPublisher" , ;
    ToolTipText = "Tipo query output utente",;
    HelpContextID = 200767275,;
    Height=25, Width=104, Left=645, Top=34,;
    cTotal="", cQueryName = "OUFLGQUE",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oOUFLGQUE_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..OUFLGQUE,&i_cF..t_OUFLGQUE),this.value)
    return(iif(xVal =1,'V',;
    iif(xVal =2,'I',;
    space(1))))
  endfunc
  func oOUFLGQUE_2_4.GetRadio()
    this.Parent.oContained.w_OUFLGQUE = this.RadioValue()
    return .t.
  endfunc

  func oOUFLGQUE_2_4.ToRadio()
    this.Parent.oContained.w_OUFLGQUE=trim(this.Parent.oContained.w_OUFLGQUE)
    return(;
      iif(this.Parent.oContained.w_OUFLGQUE=='V',1,;
      iif(this.Parent.oContained.w_OUFLGQUE=='I',2,;
      0)))
  endfunc

  func oOUFLGQUE_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oOUFLGQUE_2_4.mCond()
    with this.Parent.oContained
      return (g_IRDR = 'S')
    endwith
  endfunc

  func oOUFLGQUE_2_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IRDR <> 'S')
    endwith
    endif
  endfunc

  add object oOUNOMQUE_2_5 as StdTrsField with uid="HHPFFUAINJ",rtseq=7,rtrep=.t.,;
    cFormVar="w_OUNOMQUE",value=space(254),;
    HelpContextID = 207288107,;
    cTotal="", bFixedPos=.t., cQueryName = "OUNOMQUE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=417, Top=74, cSayPict=[REPLICATE('X', 24)], cGetPict=[REPLICATE('X', 24)], InputMask=replicate('X',254), bHasZoom = .t. , cLinkFile="INF_AQM", cZoomOnZoom="gsir_maq", oKey_1_1="AQCODICE", oKey_1_2="this.w_OUNOMQUE"

  func oOUNOMQUE_2_5.mCond()
    with this.Parent.oContained
      return (.w_OUFLGQUE = 'I')
    endwith
  endfunc

  func oOUNOMQUE_2_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OUFLGQUE <> 'I')
    endwith
    endif
  endfunc

  func oOUNOMQUE_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oOUNOMQUE_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oOUNOMQUE_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'INF_AQM','*','AQCODICE',cp_AbsName(this.parent,'oOUNOMQUE_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'gsir_maq',"Query InfoPublisher",'',this.parent.oContained
  endproc
  proc oOUNOMQUE_2_5.mZoomOnZoom
    local i_obj
    i_obj=gsir_maq()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AQCODICE=this.parent.oContained.w_OUNOMQUE
    i_obj.ecpSave()
  endproc

  add object oOUNOMQUE_2_6 as StdTrsField with uid="KDCPKTEDDF",rtseq=8,rtrep=.t.,;
    cFormVar="w_OUNOMQUE",value=space(254),;
    ToolTipText = "Query per il recupero dei dati",;
    HelpContextID = 207288107,;
    cTotal="", bFixedPos=.t., cQueryName = "OUNOMQUE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=417, Top=74, InputMask=replicate('X',254)

  func oOUNOMQUE_2_6.mCond()
    with this.Parent.oContained
      return (.w_OUFLGQUE <> 'I')
    endwith
  endfunc

  func oOUNOMQUE_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OUFLGQUE = 'I')
    endwith
    endif
  endfunc

  add object BTNQRY as cp_askfile with uid="SVNANQOYOT",width=19,height=18,;
   left=732, top=75,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_QUERY",;
    nPag=2;
    , ToolTipText = "Premere per selezionare la query";
    , HelpContextID = 163983914

  add object oOUNOMBAT_2_8 as StdTrsField with uid="IGOPUILMFF",rtseq=9,rtrep=.t.,;
    cFormVar="w_OUNOMBAT",value=space(254),;
    ToolTipText = "Programma di elaborazione sui dati estratti dalla query",;
    HelpContextID = 224065338,;
    cTotal="", bFixedPos=.t., cQueryName = "OUNOMBAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=417, Top=118, InputMask=replicate('X',254)

  func oOUNOMBAT_2_8.mCond()
    with this.Parent.oContained
      return (.w_OUFLGQUE <> 'I' And Upper(Alltrim(.w_OUNOMPRG))$'GSAC_MDV-GSDM_SAU-GSDM_SCD-GSDM_SPD-GSDM_SPV-GSDM_SVA-GSDM_SVL-GSDS_SDP-GSVE_MDV-GSVEAMDV-GSACAMDV')
    endwith
  endfunc

  add object BTNBAT as cp_askfile with uid="MVDMEJLNTV",width=19,height=18,;
   left=732, top=120,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_BATCH",;
    nPag=2;
    , ToolTipText = "Premere per selezionare il programma";
    , HelpContextID = 163983914

  add object BTNREP as cp_askfile with uid="ISHGMEOMJK",width=19,height=18,;
   left=732, top=163,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_REPORT",;
    nPag=2;
    , ToolTipText = "Premere per selezionare il report";
    , HelpContextID = 163983914

  add object oOUNOMREP_2_11 as StdTrsField with uid="HYIZJBTFRW",rtseq=10,rtrep=.t.,;
    cFormVar="w_OUNOMREP",value=space(254),;
    ToolTipText = "Report di stampa",;
    HelpContextID = 224065334,;
    cTotal="", bFixedPos=.t., cQueryName = "OUNOMREP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=417, Top=162, InputMask=replicate('X',254)

  func oOUNOMREP_2_11.mCond()
    with this.Parent.oContained
      return (.w_OUFLGQUE <> 'I')
    endwith
  endfunc

  add object oOUSTESTO_2_12 as StdTrsCheck with uid="IFNHYHOWRV",rtrep=.t.,;
    cFormVar="w_OUSTESTO",  caption="Solo testo",;
    HelpContextID = 232802101,;
    Left=417, Top=185,;
    cTotal="", cQueryName = "OUSTESTO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oOUSTESTO_2_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..OUSTESTO,&i_cF..t_OUSTESTO),this.value)
    return(iif(xVal =1,"S",;
    ' '))
  endfunc
  func oOUSTESTO_2_12.GetRadio()
    this.Parent.oContained.w_OUSTESTO = this.RadioValue()
    return .t.
  endfunc

  func oOUSTESTO_2_12.ToRadio()
    this.Parent.oContained.w_OUSTESTO=trim(this.Parent.oContained.w_OUSTESTO)
    return(;
      iif(this.Parent.oContained.w_OUSTESTO=="S",1,;
      0))
  endfunc

  func oOUSTESTO_2_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oOUSTESTO_2_12.mCond()
    with this.Parent.oContained
      return (.w_OUFLGQUE <> 'I')
    endwith
  endfunc

  add object oOUCARMEM_2_13 as StdTrsField with uid="VEYALVXXFG",rtseq=12,rtrep=.t.,;
    cFormVar="w_OUCARMEM",value=0,;
    ToolTipText = "Numero caratteri x riga nel campo memo della descrizione supp. articolo",;
    HelpContextID = 144459571,;
    cTotal="", bFixedPos=.t., cQueryName = "OUCARMEM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=665, Top=187, cSayPict=["999"], cGetPict=["999"]

  func oOUCARMEM_2_13.mCond()
    with this.Parent.oContained
      return (.w_OUSTESTO='S' And .w_OUFLGQUE <> 'I')
    endwith
  endfunc

  func oOUCARMEM_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (UPPER (g_APPLICATION) <>'ADHOC REVOLUTION')
    endwith
    endif
  endfunc

  add object oOUCODAZI_2_14 as StdTrsField with uid="JBLCSIWBQA",rtseq=13,rtrep=.t.,;
    cFormVar="w_OUCODAZI",value=space(5),;
    ToolTipText = "Codice azienda a cui � riservata (spazio = tutte le aziende)",;
    HelpContextID = 70629585,;
    cTotal="", bFixedPos=.t., cQueryName = "OUCODAZI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=417, Top=259, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_OUCODAZI"

  func oOUCODAZI_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oOUCODAZI_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oOUCODAZI_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AZIENDA','*','AZCODAZI',cp_AbsName(this.parent,'oOUCODAZI_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco aziende",'',this.parent.oContained
  endproc

  add object oOUCODUTE_2_15 as StdTrsField with uid="JGITPBWUQA",rtseq=14,rtrep=.t.,;
    cFormVar="w_OUCODUTE",value=0,;
    ToolTipText = "Codice utente a cui � riservata (0 = tutti gli utenti)",;
    HelpContextID = 264914731,;
    cTotal="", bFixedPos=.t., cQueryName = "OUCODUTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=417, Top=302, cSayPict=["9999"], cGetPict=["9999"], bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_OUCODUTE"

  func oOUCODUTE_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oOUCODUTE_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oOUCODUTE_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oOUCODUTE_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oNOMUTE_2_16 as StdTrsField with uid="YYJXKDABYH",rtseq=15,rtrep=.t.,;
    cFormVar="w_NOMUTE",value=space(20),enabled=.f.,;
    HelpContextID = 13689046,;
    cTotal="", bFixedPos=.t., cQueryName = "NOMUTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=474, Top=302, InputMask=replicate('X',20)

  add object oRAGAZI_2_19 as StdTrsField with uid="BOUNILXTZM",rtseq=18,rtrep=.t.,;
    cFormVar="w_RAGAZI",value=space(40),enabled=.f.,;
    HelpContextID = 85750550,;
    cTotal="", bFixedPos=.t., cQueryName = "RAGAZI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=480, Top=259, InputMask=replicate('X',40)

  add object oOUTIMSTA_2_23 as StdTrsField with uid="BXAXKHWNMS",rtseq=20,rtrep=.t.,;
    cFormVar="w_OUTIMSTA",value=space(14),enabled=.f.,;
    ToolTipText = "Timestamp del momento in cui � stato creato/modificato il record (formato AAAAMMGGhhmmss)",;
    HelpContextID = 240473895,;
    cTotal="", bFixedPos=.t., cQueryName = "OUTIMSTA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=638, Top=210, InputMask=replicate('X',14)

  add object oStr_2_24 as StdString with uid="GQJQEFUCPX",Visible=.t., Left=412, Top=213,;
    Alignment=1, Width=222, Height=18,;
    Caption="Timestamp di creazione/modifica:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsut_mouBodyRow as CPBodyRowCnt
  Width=390
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oOUROWNUM_2_1 as StdTrsField with uid="HFKYNORTQM",rtseq=3,rtrep=.t.,;
    cFormVar="w_OUROWNUM",value=0,isprimarykey=.t.,;
    ToolTipText = "Progressivo numero documento",;
    HelpContextID = 167458611,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=33, Left=-2, Top=0, cSayPict=["999"], cGetPict=["999"]

  add object oOUDESCRI_2_2 as StdTrsField with uid="ZTSKLNUBQH",rtseq=4,rtrep=.t.,;
    cFormVar="w_OUDESCRI",value=space(100),;
    ToolTipText = "Descrizione dell'output",;
    HelpContextID = 246437679,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .t.,;
   bGlobalFont=.t.,;
    Height=17, Width=323, Left=35, Top=0, InputMask=replicate('X',100)

  add object oOUPREDEF_2_3 as StdTrsCheck with uid="JOOUUUPOHH",rtrep=.t.,;
    cFormVar="w_OUPREDEF",  caption="",;
    ToolTipText = "Se attivo propone la vista di default",;
    HelpContextID = 249435948,;
    Left=363, Top=0, Width=22,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oOUPREDEF_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..OUPREDEF,&i_cF..t_OUPREDEF),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oOUPREDEF_2_3.GetRadio()
    this.Parent.oContained.w_OUPREDEF = this.RadioValue()
    return .t.
  endfunc

  func oOUPREDEF_2_3.ToRadio()
    this.Parent.oContained.w_OUPREDEF=trim(this.Parent.oContained.w_OUPREDEF)
    return(;
      iif(this.Parent.oContained.w_OUPREDEF=='S',1,;
      0))
  endfunc

  func oOUPREDEF_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oOUROWNUM_2_1.When()
    return(.t.)
  proc oOUROWNUM_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oOUROWNUM_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=13
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mou','OUT_PUTS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".OUNOMPRG=OUT_PUTS.OUNOMPRG";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
