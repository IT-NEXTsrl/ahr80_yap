* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_aed                                                        *
*              Piano generazione movimenti esigibilit� differita               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [116] [VRS_259]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-08                                                      *
* Last revis.: 2016-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_aed"))

* --- Class definition
define class tgscg_aed as StdForm
  Top    = 8
  Left   = 11

  * --- Standard Properties
  Width  = 675
  Height = 534+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-12-06"
  HelpContextID=76162409
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=60

  * --- Constant Properties
  ESI_DIF_IDX = 0
  ESERCIZI_IDX = 0
  CAU_CONT_IDX = 0
  AZIENDA_IDX = 0
  cFile = "ESI_DIF"
  cKeySelect = "EDSERIAL"
  cKeyWhere  = "EDSERIAL=this.w_EDSERIAL"
  cKeyWhereODBC = '"EDSERIAL="+cp_ToStrODBC(this.w_EDSERIAL)';

  cKeyWhereODBCqualified = '"ESI_DIF.EDSERIAL="+cp_ToStrODBC(this.w_EDSERIAL)';

  cPrg = "gscg_aed"
  cComment = "Piano generazione movimenti esigibilit� differita"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_EDSERIAL = space(10)
  o_EDSERIAL = space(10)
  w_CODAZI = space(5)
  w_REGPAGAP = space(1)
  w_DIFPAGAP = space(1)
  w_TIPPAGAP = space(2)
  w_IVAPAGAP = space(15)
  w_NUMPAGAP = 0
  w_REGINCAT = space(1)
  w_DIFINCAT = space(1)
  w_TIPINCAT = space(2)
  w_IVAINCAT = space(15)
  w_NUMINCAT = 0
  w_TIPPAG = space(2)
  w_TIPINC = space(2)
  w_REGPAG = space(1)
  w_DIFPAG = space(1)
  w_REGINC = space(1)
  w_DIFINC = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_IVAPAG = space(15)
  w_IVAINC = space(15)
  w_NUMPAG = 0
  w_NUMINC = 0
  w_SERRIF = space(10)
  w_AZDATCON = space(1)
  w_ESEDATRIF = space(4)
  w_DPTIPREG = space(1)
  w_AZFLFIDO = space(1)
  w_AZGIORIS = 0
  w_EDTIPMOV = space(1)
  w_EDDATREG = ctod('  /  /  ')
  w_EDDATINI = ctod('  /  /  ')
  w_EDDATFIN = ctod('  /  /  ')
  w_EDTIPDIS = space(1)
  o_EDTIPDIS = space(1)
  w_EDSCADIS = space(1)
  o_EDSCADIS = space(1)
  w_EDDATESP = ctod('  /  /  ')
  w_EDGGTOLC = 0
  w_EDGGTOLF = 0
  w_EDDATSCI = ctod('  /  /  ')
  w_EDDATSCF = ctod('  /  /  ')
  w_EDRILTRI = space(1)
  w_EDMATEMP = space(1)
  o_EDMATEMP = space(1)
  w_EDGIORNO = 0
  w_EDDATRIF = ctod('  /  /  ')
  o_EDDATRIF = ctod('  /  /  ')
  w_EDNUMERO = 0
  w_EDCODESE = space(4)
  w_AZCAUINC = space(5)
  o_AZCAUINC = space(5)
  w_AZCAUPAG = space(5)
  o_AZCAUPAG = space(5)
  w_AZCAUMTA = space(5)
  o_AZCAUMTA = space(5)
  w_AZCAUMTP = space(5)
  w_EDDATGEN = ctod('  /  /  ')
  o_EDDATGEN = ctod('  /  /  ')
  w_CAUINC = space(5)
  w_CAUPAG = space(5)
  w_CAUMTA = space(5)
  w_CAUMTP = space(5)
  w_EDDESCRI = space(40)
  w_DESINC = space(35)
  w_DESPAG = space(35)
  w_DESINCAT = space(35)
  w_DESPAGAP = space(35)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_EDSERIAL = this.W_EDSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_EDCODESE = this.W_EDCODESE
  op_EDNUMERO = this.W_EDNUMERO
  w_DETT = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_aed
  proc ecpQuit()
      local i_oldfunc
      i_oldfunc=this.cFunction
      if inlist(this.cFunction,'Edit','Load')
        *** Modificato rispetto allo Standard INIZIO
        *** Eliminata richiesta di conferma
        *** Modificato rispetto allo Standard FINE
        this.bUpdated=.f.
        this.NotifyEvent('Edit Aborted')
      endif
      *--- Activity Logger traccio ESC
      If i_nACTIVATEPROFILER>0
      	cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UES", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
      Endif
      *--- Activity Logger traccio ESC
      * --- si sposta senza attivare i controlli
      this.cFunction='Filter'
      local i_err
      i_err=on('error')
      on error =.t.
      this.activecontrol.parent.oContained.bDontReportError=.t.
      on error &i_err
      this.__dummy__.enabled=.t.
      this.__dummy__.Setfocus()
      * ---
      do case
        case i_oldfunc="Query"
          this.NotifyEvent("Done")
          this.Hide()
          this.Release()
        case i_oldfunc="Load"
          *this.QueryKeySet(this.cLastWhere,this.cLastOrderBy)
          *this.ecpQuery()
          *this.nLoadTime=0
          This.cFunction="Query"
          This.BlankRec()
          This.bLoaded=.F.
          This.SetStatus()
          This.SetFocusOnFirstControl()
        case i_oldfunc="Edit"
          this.ecpQuery()
          this.nLoadTime=0
        otherwise
          this.ecpQuery()
      endcase
      return
  endproc
  
  * --- Salva
  Proc ecpSave()
  	If This.bSaving
  		Return
  	Endif
  	This.bSaving=.T.
  	*--- Activity Logger traccio la cancellazione di un record
  	If i_nACTIVATEPROFILER>0
  		cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "U10",This.cFunction , This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
  	Endif
  	*--- Activity Logger traccio la cancellazione di un record
  	Do Case
  		Case This.cFunction = "Filter"
  			If i_bSecurityRecord  And This.bSecDataFilter
  				Local i_SecFilter, w_ObjMask
  				This.bSecDataFilter=.F.
  				i_SecFilter=This.AddSonsFilter(This.BuildFilter(),This)
  				This.ecpQuery() && La Ecpquery deve andare prima dell'apertura della maschera con la sicurezza sul record...
  				w_ObjMask= Cp_recsec()
  				If Vartype(w_ObjMask)='O'
  					w_ObjMask.ecpLoad()
  					* valrizzo testata
  					setvaluelinked('ML',w_ObjMask,'w_TABLENAME', This.cFile)
  					setvaluelinked('DL',w_ObjMask,'w_DESCRI', This.cComment)
  					setvaluelinked('DL',w_ObjMask,'w_QUERYFILTER', i_SecFilter)
  				Endif
  			Else
  				This.QueryKeySet(This.AddSonsFilter(This.BuildFilter(),This),"")
  				This.LoadRecWarn()
  				This.ecpQuery()
  			Endif
  		Case This.cFunction = "Edit"
  			If This.TerminateEdit() And This.CheckForm()
  				cp_BeginTrs()
  				*  Zucchetti Aulla Inizio: modifica gestione padre anche se modificato solo un child
  
  				* --- Gestione dati caricato da, alla data..
  				If Vartype(This.w_UTDV)<>'U'
  					* --- Se presente nella gestione la data di caricamento
  					* --- allora forzo la scrittura, per contemplare anche
  					* --- eventuali modifiche ai figli
  					This.bUpdated=.T.
  					This.w_UTDV=SetInfoDate(This.cCalUtd)
  					If Vartype(This.w_UTCV)<>'U'
  						This.w_UTCV=i_CoduTe
  					Endif
  				Endif
  
  				*  Solo se attiva logistica remota, se la gestione non � gi� vista come modificata, se il figlio � modificato e se
  				*  la tabella relativa alla gestione � presente nelle entit� della logistica
  				If This.bUpdated=.F. And Vartype(g_LORE)='C' And g_LORE='S' And This.IsAChildUpdated()
  					* solo se � una gestione e quindi ha una tabella
  					If Type('this.cFile')='C'
  						If TISINENT(This.cFile)
  							* Forzo l'aggiornamento della gestione
  							This.bUpdated=.T.
  						Endif
  					Endif
  				Endif
  				**** Zucchetti Aulla inizio - Gestione modulo controllo flussi
  				This.cFlowStamp = Sys(2015)
  				This.bFlowNoRules = .F.
  				*  Zucchetti Aulla Fine - Gestione modulo controllo flussi
  				This.LockScreen=.T.
  				This.mReplace(.T.)
  				**** Zucchetti Aulla inizio - Sincronizzazione
  				*** Update
  				If g_APPLICATION = "ad hoc ENTERPRISE" And Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And Vartype(g_SINC)="C" And g_SINC='S'
  					Local i_cParam
  					i_cParam='UM'
  					If !Empty(i_cParam) And Type("g_SINC") = "C"
  						If g_SINC = "S"
  							l_sPrg="GSSI_BSD"
  							Do (l_sPrg) With This,i_cParam
  						Endif
  					Endif
  				Endif
  
  				**** Zucchetti Aulla fine - Sincronizzazione
  				This.LockScreen=.F.
  				cp_EndTrs(This.bTrsDontDisplayErr)
  				If .Not. bTrsErr
  					**** Zucchetti Aulla inizio - Gestione modulo controllo flussi
  					If Type("this.cFile")="C" And Not Empty(This.cFile) And IsFlus(This)
  						Local l_sPrg,i_cParam
  						l_sPrg="GSCF_BCF"
  						i_cParam='ET'
  						Do (l_sPrg) With This,i_cParam
  					Endif
  					*  Zucchetti Aulla Fine - Gestione modulo controllo flussi
  					This.NotifyEvent("Record Updated")
  					This.ecpQuery()
  					This.nLoadTime=0
  					This.bUpdated=.F.
  				Else
  					If !Empty(This.cTrsName)
  						Select (This.cTrsName)
  						Go Top
  						This.WorkFromTrs()
  						* ---Se ho problemi dopo la replace end raddoppia le righe
  						This.SetControlsValue()
  						This.ChildrenChangeRow()
  						This.oPgFrm.Page1.oPag.oBody.nAbsRow=1
  						This.oPgFrm.Page1.oPag.oBody.nRelRow=1
  					Endif
  					This.NotifyEvent("Edit Restarted")
  				Endif
  			Endif
  		Case This.cFunction = "Load"
  			If This.TerminateEdit() And This.CheckForm()
  				cp_BeginTrs()
  				**** Zucchetti Aulla inizio - controllo flussi
  				This.cFlowStamp = Sys(2015)
  				This.bFlowNoRules = .F.
  				**** Zucchetti Aulla fine - controllo flussi
  				This.LockScreen=.T.
  				If This.mInsert()
  					This.mReplace(.F.)
  				Endif
  				**** Zucchetti Aulla inizio - Sincronizzazione
  				*** Insert
  				If g_APPLICATION = "ad hoc ENTERPRISE" And Not bTrsErr And Type("this.cFile")="C" And Not Empty(This.cFile) And Vartype(g_SINC)="C" And g_SINC='S'
  					Local i_cParam
  					i_cParam='IM'
  					If !Empty(i_cParam) And Type("g_SINC") = "C"
  						If g_SINC = "S"
  							l_sPrg="GSSI_BSD"
  							Do (l_sPrg) With This,i_cParam
  						Endif
  					Endif
  				Endif
  
  				**** Zucchetti Aulla fine - Sincronizzazione
  				This.LockScreen=.F.
  				cp_EndTrs(This.bTrsDontDisplayErr)
  				If .Not. bTrsErr
  					**** Zucchetti Aulla inizio - Gestione modulo controllo flussi
  					If Type("this.cFile")="C" And Not Empty(This.cFile) And IsFlus(This)
  						Local l_sPrg,i_cParam
  						l_sPrg="GSCF_BCF"
  						i_cParam='ET'
  						Do (l_sPrg) With This,i_cParam
  					Endif
  					*  Zucchetti Aulla Fine - Gestione modulo controllo flussi
  					This.NotifyEvent("Record Inserted")
  		            **** Modificato rispetto allo standard INIZIO
  		            if not empty(this.w_EDSERIAL)
  		               * --- creo il curosre delle solo chiavi
  		               this.QueryKeySet("EDSERIAL='"+this.w_EDSERIAL+ "'","")
  		               * --- mi metto in interrogazione
  		               this.LoadRecWarn()
  		               this.ecpQuery()
  		               this.nLoadTime=0
  		               this.bUpdated=.f.
  		               KEYBOARD '{ESC}'
  					else
  						This.BlankRec()
  						This.InitAutonumber()
  						This.NotifyEvent('New record')
  		            endif
  		            **** Modificato rispetto allo standard FINE
  					This.bUpdated=.F.
  				Else
  					If !Empty(This.cTrsName)
  						Select (This.cTrsName)
  						Go Top
  						This.WorkFromTrs()
  						* ---Se ho problemi dopo la replace end raddoppia le righe
  						This.SetControlsValue()
  						This.ChildrenChangeRow()
  						This.oPgFrm.Page1.oPag.oBody.nAbsRow=1
  						This.oPgFrm.Page1.oPag.oBody.nRelRow=1
  					Endif
  				Endif
  				This.cFunction='Filter'  && disabilita when e valid
  				* !!! greg 26/6/96
  				Local b
  				b=This.oFirstControl.Enabled
  				This.oFirstControl.Enabled=.T.
  				This.SetFocusOnFirstControl()
  				This.__dummy__.Enabled=.F.
  				This.oPgFrm.ActivePage=1
  				This.SetFocusOnFirstControl()
  				If !Empty(This.cTrsName)
  					Select (This.cTrsName)
  					Go Top
  				Endif
  				* !!!
  				This.Refresh()
  				This.cFunction='Load'
  				This.oFirstControl.Enabled=m.b
  				This.mEnableControls()
  			Endif
  	Endcase
  	This.bTrsDontDisplayErr=.F.
  	This.bSaving=.F.
  	Return
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ESI_DIF','gscg_aed')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_aedPag1","gscg_aed",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Piano")
      .Pages(1).HelpContextID = 220571402
      .Pages(2).addobject("oPag","tgscg_aedPag2","gscg_aed",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).HelpContextID = 53736238
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_DETT = this.oPgFrm.Pages(1).oPag.DETT
      DoDefault()
    proc Destroy()
      this.w_DETT = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='ESI_DIF'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ESI_DIF_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ESI_DIF_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_EDSERIAL = NVL(EDSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ESI_DIF where EDSERIAL=KeySet.EDSERIAL
    *
    i_nConn = i_TableProp[this.ESI_DIF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ESI_DIF_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ESI_DIF')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ESI_DIF.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ESI_DIF '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'EDSERIAL',this.w_EDSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_REGPAGAP = space(1)
        .w_DIFPAGAP = space(1)
        .w_TIPPAGAP = space(2)
        .w_IVAPAGAP = space(15)
        .w_NUMPAGAP = 0
        .w_REGINCAT = space(1)
        .w_DIFINCAT = space(1)
        .w_TIPINCAT = space(2)
        .w_IVAINCAT = space(15)
        .w_NUMINCAT = 0
        .w_TIPPAG = space(2)
        .w_TIPINC = space(2)
        .w_REGPAG = space(1)
        .w_DIFPAG = space(1)
        .w_REGINC = space(1)
        .w_DIFINC = space(1)
        .w_OBTEST = i_INIDAT
        .w_IVAPAG = space(15)
        .w_IVAINC = space(15)
        .w_NUMPAG = 0
        .w_NUMINC = 0
        .w_AZDATCON = space(1)
        .w_AZFLFIDO = space(1)
        .w_AZGIORIS = 0
        .w_AZCAUINC = space(5)
        .w_AZCAUPAG = space(5)
        .w_AZCAUMTA = space(5)
        .w_AZCAUMTP = space(5)
        .w_DESINC = space(35)
        .w_DESPAG = space(35)
        .w_DESINCAT = space(35)
        .w_DESPAGAP = space(35)
        .w_EDSERIAL = NVL(EDSERIAL,space(10))
        .op_EDSERIAL = .w_EDSERIAL
          .link_1_2('Load')
        .oPgFrm.Page1.oPag.DETT.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .w_SERRIF = .w_DETT.getVar('DPSERINC')
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .w_ESEDATRIF = CALCESER(.w_EDDATRIF, g_CODESE)
        .w_DPTIPREG = NVL(.w_DETT.getVar('DPTIPREG'),'N')
        .w_EDTIPMOV = NVL(EDTIPMOV,space(1))
        .w_EDDATREG = NVL(cp_ToDate(EDDATREG),ctod("  /  /  "))
        .w_EDDATINI = NVL(cp_ToDate(EDDATINI),ctod("  /  /  "))
        .w_EDDATFIN = NVL(cp_ToDate(EDDATFIN),ctod("  /  /  "))
        .w_EDTIPDIS = NVL(EDTIPDIS,space(1))
        .w_EDSCADIS = NVL(EDSCADIS,space(1))
        .w_EDDATESP = NVL(cp_ToDate(EDDATESP),ctod("  /  /  "))
        .w_EDGGTOLC = NVL(EDGGTOLC,0)
        .w_EDGGTOLF = NVL(EDGGTOLF,0)
        .w_EDDATSCI = NVL(cp_ToDate(EDDATSCI),ctod("  /  /  "))
        .w_EDDATSCF = NVL(cp_ToDate(EDDATSCF),ctod("  /  /  "))
        .w_EDRILTRI = NVL(EDRILTRI,space(1))
        .w_EDMATEMP = NVL(EDMATEMP,space(1))
        .w_EDGIORNO = NVL(EDGIORNO,0)
        .w_EDDATRIF = NVL(cp_ToDate(EDDATRIF),ctod("  /  /  "))
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate(IIF(.w_Edmatemp='N' or .cFunction<>'Load','',IIF(.w_Azdatcon='I','su data competenza IVA',iif(.w_Azdatcon='R','su data registrazione','su data documento'))))
        .w_EDNUMERO = NVL(EDNUMERO,0)
        .op_EDNUMERO = .w_EDNUMERO
        .w_EDCODESE = NVL(EDCODESE,space(4))
        .op_EDCODESE = .w_EDCODESE
          * evitabile
          *.link_1_42('Load')
        .w_EDDATGEN = NVL(cp_ToDate(EDDATGEN),ctod("  /  /  "))
        .w_CAUINC = .w_AZCAUINC
          .link_1_48('Load')
        .w_CAUPAG = .w_AZCAUPAG
          .link_1_49('Load')
        .w_CAUMTA = .w_AZCAUMTA
          .link_1_50('Load')
        .w_CAUMTP = .w_AZCAUMTP
          .link_1_51('Load')
        .w_EDDESCRI = NVL(EDDESCRI,space(40))
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ESI_DIF')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_EDSERIAL = space(10)
      .w_CODAZI = space(5)
      .w_REGPAGAP = space(1)
      .w_DIFPAGAP = space(1)
      .w_TIPPAGAP = space(2)
      .w_IVAPAGAP = space(15)
      .w_NUMPAGAP = 0
      .w_REGINCAT = space(1)
      .w_DIFINCAT = space(1)
      .w_TIPINCAT = space(2)
      .w_IVAINCAT = space(15)
      .w_NUMINCAT = 0
      .w_TIPPAG = space(2)
      .w_TIPINC = space(2)
      .w_REGPAG = space(1)
      .w_DIFPAG = space(1)
      .w_REGINC = space(1)
      .w_DIFINC = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_IVAPAG = space(15)
      .w_IVAINC = space(15)
      .w_NUMPAG = 0
      .w_NUMINC = 0
      .w_SERRIF = space(10)
      .w_AZDATCON = space(1)
      .w_ESEDATRIF = space(4)
      .w_DPTIPREG = space(1)
      .w_AZFLFIDO = space(1)
      .w_AZGIORIS = 0
      .w_EDTIPMOV = space(1)
      .w_EDDATREG = ctod("  /  /  ")
      .w_EDDATINI = ctod("  /  /  ")
      .w_EDDATFIN = ctod("  /  /  ")
      .w_EDTIPDIS = space(1)
      .w_EDSCADIS = space(1)
      .w_EDDATESP = ctod("  /  /  ")
      .w_EDGGTOLC = 0
      .w_EDGGTOLF = 0
      .w_EDDATSCI = ctod("  /  /  ")
      .w_EDDATSCF = ctod("  /  /  ")
      .w_EDRILTRI = space(1)
      .w_EDMATEMP = space(1)
      .w_EDGIORNO = 0
      .w_EDDATRIF = ctod("  /  /  ")
      .w_EDNUMERO = 0
      .w_EDCODESE = space(4)
      .w_AZCAUINC = space(5)
      .w_AZCAUPAG = space(5)
      .w_AZCAUMTA = space(5)
      .w_AZCAUMTP = space(5)
      .w_EDDATGEN = ctod("  /  /  ")
      .w_CAUINC = space(5)
      .w_CAUPAG = space(5)
      .w_CAUMTA = space(5)
      .w_CAUMTP = space(5)
      .w_EDDESCRI = space(40)
      .w_DESINC = space(35)
      .w_DESPAG = space(35)
      .w_DESINCAT = space(35)
      .w_DESPAGAP = space(35)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_CODAZI))
          .link_1_2('Full')
          endif
        .oPgFrm.Page1.oPag.DETT.Calculate()
          .DoRTCalc(3,18,.f.)
        .w_OBTEST = i_INIDAT
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
          .DoRTCalc(20,23,.f.)
        .w_SERRIF = .w_DETT.getVar('DPSERINC')
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
          .DoRTCalc(25,25,.f.)
        .w_ESEDATRIF = CALCESER(.w_EDDATRIF, g_CODESE)
        .w_DPTIPREG = NVL(.w_DETT.getVar('DPTIPREG'),'N')
          .DoRTCalc(28,29,.f.)
        .w_EDTIPMOV = 'E'
          .DoRTCalc(31,33,.f.)
        .w_EDTIPDIS = 'E'
        .w_EDSCADIS = 'N'
        .w_EDDATESP = .w_EDDATGEN
        .w_EDGGTOLC = IIF(.w_AZFLFIDO='S',.w_AZGIORIS,0)
        .w_EDGGTOLF = IIF(.w_AZFLFIDO='S',.w_AZGIORIS,0)
        .w_EDDATSCI = Cp_CharToDate('  -  -    ')
        .w_EDDATSCF = Cp_CharToDate('  -  -    ')
        .w_EDRILTRI = 'N'
        .w_EDMATEMP = 'N'
        .w_EDGIORNO = IIF(.w_EDMATEMP='N',0,365)
        .w_EDDATRIF = IIF(.w_EDMATEMP='N',cp_CharToDate('  -  -  '),.w_EDDATGEN)
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate(IIF(.w_Edmatemp='N' or .cFunction<>'Load','',IIF(.w_Azdatcon='I','su data competenza IVA',iif(.w_Azdatcon='R','su data registrazione','su data documento'))))
          .DoRTCalc(45,45,.f.)
        .w_EDCODESE = g_CODESE
        .DoRTCalc(46,46,.f.)
          if not(empty(.w_EDCODESE))
          .link_1_42('Full')
          endif
          .DoRTCalc(47,50,.f.)
        .w_EDDATGEN = i_datsys
        .w_CAUINC = .w_AZCAUINC
        .DoRTCalc(52,52,.f.)
          if not(empty(.w_CAUINC))
          .link_1_48('Full')
          endif
        .w_CAUPAG = .w_AZCAUPAG
        .DoRTCalc(53,53,.f.)
          if not(empty(.w_CAUPAG))
          .link_1_49('Full')
          endif
        .w_CAUMTA = .w_AZCAUMTA
        .DoRTCalc(54,54,.f.)
          if not(empty(.w_CAUMTA))
          .link_1_50('Full')
          endif
        .w_CAUMTP = .w_AZCAUMTP
        .DoRTCalc(55,55,.f.)
          if not(empty(.w_CAUMTP))
          .link_1_51('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ESI_DIF')
    this.DoRTCalc(56,60,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_aed
    * --- Refresh dello Zoom
    if this.cFunction <> 'Edit'
       this.NotifyEvent('Carica')
    endif
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ESI_DIF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ESI_DIF_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEESIDIF","i_codazi,w_EDSERIAL")
      cp_AskTableProg(this,i_nConn,"PRESIDIF","i_codazi,w_EDCODESE,w_EDNUMERO")
      .op_codazi = .w_codazi
      .op_EDSERIAL = .w_EDSERIAL
      .op_codazi = .w_codazi
      .op_EDCODESE = .w_EDCODESE
      .op_EDNUMERO = .w_EDNUMERO
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page2.oPag.oEDTIPMOV_2_1.enabled = i_bVal
      .Page2.oPag.oEDDATREG_2_2.enabled = i_bVal
      .Page2.oPag.oEDDATINI_2_3.enabled = i_bVal
      .Page2.oPag.oEDDATFIN_2_4.enabled = i_bVal
      .Page2.oPag.oEDTIPDIS_2_5.enabled = i_bVal
      .Page2.oPag.oEDSCADIS_2_6.enabled = i_bVal
      .Page2.oPag.oEDDATESP_2_7.enabled = i_bVal
      .Page2.oPag.oEDGGTOLC_2_8.enabled = i_bVal
      .Page2.oPag.oEDGGTOLF_2_9.enabled = i_bVal
      .Page2.oPag.oEDDATSCI_2_10.enabled = i_bVal
      .Page2.oPag.oEDDATSCF_2_11.enabled = i_bVal
      .Page2.oPag.oEDRILTRI_2_12.enabled = i_bVal
      .Page2.oPag.oEDMATEMP_2_13.enabled = i_bVal
      .Page2.oPag.oEDGIORNO_2_14.enabled = i_bVal
      .Page2.oPag.oEDDATRIF_2_15.enabled = i_bVal
      .Page1.oPag.oEDNUMERO_1_41.enabled = i_bVal
      .Page1.oPag.oEDCODESE_1_42.enabled = i_bVal
      .Page1.oPag.oEDDATGEN_1_47.enabled = i_bVal
      .Page1.oPag.oCAUINC_1_48.enabled = i_bVal
      .Page1.oPag.oCAUPAG_1_49.enabled = i_bVal
      .Page1.oPag.oCAUMTA_1_50.enabled = i_bVal
      .Page1.oPag.oCAUMTP_1_51.enabled = i_bVal
      .Page1.oPag.oEDDESCRI_1_52.enabled = i_bVal
      .Page1.oPag.oBtn_1_13.enabled = i_bVal
      .Page1.oPag.oBtn_1_26.enabled = .Page1.oPag.oBtn_1_26.mCond()
      .Page1.oPag.oBtn_1_28.enabled = i_bVal
      .Page1.oPag.oBtn_1_29.enabled = i_bVal
      .Page1.oPag.oBtn_1_30.enabled = i_bVal
      .Page1.oPag.oBtn_1_31.enabled = i_bVal
      .Page1.oPag.oObj_1_27.enabled = i_bVal
      .Page1.oPag.oObj_1_32.enabled = i_bVal
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      .Page2.oPag.oObj_2_20.enabled = i_bVal
      .Page1.oPag.oObj_1_65.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oEDNUMERO_1_41.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ESI_DIF',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ESI_DIF_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDSERIAL,"EDSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDTIPMOV,"EDTIPMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDDATREG,"EDDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDDATINI,"EDDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDDATFIN,"EDDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDTIPDIS,"EDTIPDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDSCADIS,"EDSCADIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDDATESP,"EDDATESP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDGGTOLC,"EDGGTOLC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDGGTOLF,"EDGGTOLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDDATSCI,"EDDATSCI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDDATSCF,"EDDATSCF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDRILTRI,"EDRILTRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDMATEMP,"EDMATEMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDGIORNO,"EDGIORNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDDATRIF,"EDDATRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDNUMERO,"EDNUMERO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDCODESE,"EDCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDDATGEN,"EDDATGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EDDESCRI,"EDDESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ESI_DIF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ESI_DIF_IDX,2])
    i_lTable = "ESI_DIF"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ESI_DIF_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("QUERY\GSCG_AED.VQR,QUERY\GSCG_AED.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gscg_aed
    * --- per forzare sempre l'insermento anche se non si edita nulla
    this.bUpdated=.t.
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ESI_DIF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ESI_DIF_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ESI_DIF_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEESIDIF","i_codazi,w_EDSERIAL")
          cp_NextTableProg(this,i_nConn,"PRESIDIF","i_codazi,w_EDCODESE,w_EDNUMERO")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ESI_DIF
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ESI_DIF')
        i_extval=cp_InsertValODBCExtFlds(this,'ESI_DIF')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(EDSERIAL,EDTIPMOV,EDDATREG,EDDATINI,EDDATFIN"+;
                  ",EDTIPDIS,EDSCADIS,EDDATESP,EDGGTOLC,EDGGTOLF"+;
                  ",EDDATSCI,EDDATSCF,EDRILTRI,EDMATEMP,EDGIORNO"+;
                  ",EDDATRIF,EDNUMERO,EDCODESE,EDDATGEN,EDDESCRI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_EDSERIAL)+;
                  ","+cp_ToStrODBC(this.w_EDTIPMOV)+;
                  ","+cp_ToStrODBC(this.w_EDDATREG)+;
                  ","+cp_ToStrODBC(this.w_EDDATINI)+;
                  ","+cp_ToStrODBC(this.w_EDDATFIN)+;
                  ","+cp_ToStrODBC(this.w_EDTIPDIS)+;
                  ","+cp_ToStrODBC(this.w_EDSCADIS)+;
                  ","+cp_ToStrODBC(this.w_EDDATESP)+;
                  ","+cp_ToStrODBC(this.w_EDGGTOLC)+;
                  ","+cp_ToStrODBC(this.w_EDGGTOLF)+;
                  ","+cp_ToStrODBC(this.w_EDDATSCI)+;
                  ","+cp_ToStrODBC(this.w_EDDATSCF)+;
                  ","+cp_ToStrODBC(this.w_EDRILTRI)+;
                  ","+cp_ToStrODBC(this.w_EDMATEMP)+;
                  ","+cp_ToStrODBC(this.w_EDGIORNO)+;
                  ","+cp_ToStrODBC(this.w_EDDATRIF)+;
                  ","+cp_ToStrODBC(this.w_EDNUMERO)+;
                  ","+cp_ToStrODBCNull(this.w_EDCODESE)+;
                  ","+cp_ToStrODBC(this.w_EDDATGEN)+;
                  ","+cp_ToStrODBC(this.w_EDDESCRI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ESI_DIF')
        i_extval=cp_InsertValVFPExtFlds(this,'ESI_DIF')
        cp_CheckDeletedKey(i_cTable,0,'EDSERIAL',this.w_EDSERIAL)
        INSERT INTO (i_cTable);
              (EDSERIAL,EDTIPMOV,EDDATREG,EDDATINI,EDDATFIN,EDTIPDIS,EDSCADIS,EDDATESP,EDGGTOLC,EDGGTOLF,EDDATSCI,EDDATSCF,EDRILTRI,EDMATEMP,EDGIORNO,EDDATRIF,EDNUMERO,EDCODESE,EDDATGEN,EDDESCRI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_EDSERIAL;
                  ,this.w_EDTIPMOV;
                  ,this.w_EDDATREG;
                  ,this.w_EDDATINI;
                  ,this.w_EDDATFIN;
                  ,this.w_EDTIPDIS;
                  ,this.w_EDSCADIS;
                  ,this.w_EDDATESP;
                  ,this.w_EDGGTOLC;
                  ,this.w_EDGGTOLF;
                  ,this.w_EDDATSCI;
                  ,this.w_EDDATSCF;
                  ,this.w_EDRILTRI;
                  ,this.w_EDMATEMP;
                  ,this.w_EDGIORNO;
                  ,this.w_EDDATRIF;
                  ,this.w_EDNUMERO;
                  ,this.w_EDCODESE;
                  ,this.w_EDDATGEN;
                  ,this.w_EDDESCRI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ESI_DIF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ESI_DIF_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ESI_DIF_IDX,i_nConn)
      *
      * update ESI_DIF
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ESI_DIF')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " EDTIPMOV="+cp_ToStrODBC(this.w_EDTIPMOV)+;
             ",EDDATREG="+cp_ToStrODBC(this.w_EDDATREG)+;
             ",EDDATINI="+cp_ToStrODBC(this.w_EDDATINI)+;
             ",EDDATFIN="+cp_ToStrODBC(this.w_EDDATFIN)+;
             ",EDTIPDIS="+cp_ToStrODBC(this.w_EDTIPDIS)+;
             ",EDSCADIS="+cp_ToStrODBC(this.w_EDSCADIS)+;
             ",EDDATESP="+cp_ToStrODBC(this.w_EDDATESP)+;
             ",EDGGTOLC="+cp_ToStrODBC(this.w_EDGGTOLC)+;
             ",EDGGTOLF="+cp_ToStrODBC(this.w_EDGGTOLF)+;
             ",EDDATSCI="+cp_ToStrODBC(this.w_EDDATSCI)+;
             ",EDDATSCF="+cp_ToStrODBC(this.w_EDDATSCF)+;
             ",EDRILTRI="+cp_ToStrODBC(this.w_EDRILTRI)+;
             ",EDMATEMP="+cp_ToStrODBC(this.w_EDMATEMP)+;
             ",EDGIORNO="+cp_ToStrODBC(this.w_EDGIORNO)+;
             ",EDDATRIF="+cp_ToStrODBC(this.w_EDDATRIF)+;
             ",EDNUMERO="+cp_ToStrODBC(this.w_EDNUMERO)+;
             ",EDCODESE="+cp_ToStrODBCNull(this.w_EDCODESE)+;
             ",EDDATGEN="+cp_ToStrODBC(this.w_EDDATGEN)+;
             ",EDDESCRI="+cp_ToStrODBC(this.w_EDDESCRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ESI_DIF')
        i_cWhere = cp_PKFox(i_cTable  ,'EDSERIAL',this.w_EDSERIAL  )
        UPDATE (i_cTable) SET;
              EDTIPMOV=this.w_EDTIPMOV;
             ,EDDATREG=this.w_EDDATREG;
             ,EDDATINI=this.w_EDDATINI;
             ,EDDATFIN=this.w_EDDATFIN;
             ,EDTIPDIS=this.w_EDTIPDIS;
             ,EDSCADIS=this.w_EDSCADIS;
             ,EDDATESP=this.w_EDDATESP;
             ,EDGGTOLC=this.w_EDGGTOLC;
             ,EDGGTOLF=this.w_EDGGTOLF;
             ,EDDATSCI=this.w_EDDATSCI;
             ,EDDATSCF=this.w_EDDATSCF;
             ,EDRILTRI=this.w_EDRILTRI;
             ,EDMATEMP=this.w_EDMATEMP;
             ,EDGIORNO=this.w_EDGIORNO;
             ,EDDATRIF=this.w_EDDATRIF;
             ,EDNUMERO=this.w_EDNUMERO;
             ,EDCODESE=this.w_EDCODESE;
             ,EDDATGEN=this.w_EDDATGEN;
             ,EDDESCRI=this.w_EDDESCRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ESI_DIF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ESI_DIF_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ESI_DIF_IDX,i_nConn)
      *
      * delete ESI_DIF
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'EDSERIAL',this.w_EDSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ESI_DIF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ESI_DIF_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_EDSERIAL<>.w_EDSERIAL
          .link_1_2('Full')
        endif
        .oPgFrm.Page1.oPag.DETT.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .DoRTCalc(3,23,.t.)
            .w_SERRIF = .w_DETT.getVar('DPSERINC')
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(25,25,.t.)
        if .o_EDDATRIF<>.w_EDDATRIF
            .w_ESEDATRIF = CALCESER(.w_EDDATRIF, g_CODESE)
        endif
            .w_DPTIPREG = NVL(.w_DETT.getVar('DPTIPREG'),'N')
        if .o_EDTIPDIS<>.w_EDTIPDIS
          .Calculate_JCXNYJKMIN()
        endif
        .DoRTCalc(28,35,.t.)
        if .o_EDDATGEN<>.w_EDDATGEN
            .w_EDDATESP = .w_EDDATGEN
        endif
        .DoRTCalc(37,38,.t.)
        if .o_EDSCADIS<>.w_EDSCADIS
            .w_EDDATSCI = Cp_CharToDate('  -  -    ')
        endif
        if .o_EDSCADIS<>.w_EDSCADIS
            .w_EDDATSCF = Cp_CharToDate('  -  -    ')
        endif
        if .o_EDMATEMP<>.w_EDMATEMP
            .w_EDRILTRI = 'N'
        endif
        .DoRTCalc(42,42,.t.)
        if .o_EDMATEMP<>.w_EDMATEMP
            .w_EDGIORNO = IIF(.w_EDMATEMP='N',0,365)
        endif
        if .o_EDDATGEN<>.w_EDDATGEN.or. .o_EDMATEMP<>.w_EDMATEMP
            .w_EDDATRIF = IIF(.w_EDMATEMP='N',cp_CharToDate('  -  -  '),.w_EDDATGEN)
        endif
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate(IIF(.w_Edmatemp='N' or .cFunction<>'Load','',IIF(.w_Azdatcon='I','su data competenza IVA',iif(.w_Azdatcon='R','su data registrazione','su data documento'))))
        .DoRTCalc(45,51,.t.)
        if .o_AZCAUINC<>.w_AZCAUINC
            .w_CAUINC = .w_AZCAUINC
          .link_1_48('Full')
        endif
        if .o_AZCAUPAG<>.w_AZCAUPAG
            .w_CAUPAG = .w_AZCAUPAG
          .link_1_49('Full')
        endif
        if .o_AZCAUMTA<>.w_AZCAUMTA
            .w_CAUMTA = .w_AZCAUMTA
          .link_1_50('Full')
        endif
        if .o_AZCAUMTA<>.w_AZCAUMTA
            .w_CAUMTP = .w_AZCAUMTP
          .link_1_51('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEESIDIF","i_codazi,w_EDSERIAL")
          .op_EDSERIAL = .w_EDSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_EDCODESE<>.w_EDCODESE
           cp_AskTableProg(this,i_nConn,"PRESIDIF","i_codazi,w_EDCODESE,w_EDNUMERO")
          .op_EDNUMERO = .w_EDNUMERO
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_EDCODESE = .w_EDCODESE
      endwith
      this.DoRTCalc(56,60,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.DETT.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_20.Calculate(IIF(.w_Edmatemp='N' or .cFunction<>'Load','',IIF(.w_Azdatcon='I','su data competenza IVA',iif(.w_Azdatcon='R','su data registrazione','su data documento'))))
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
    endwith
  return

  proc Calculate_JCXNYJKMIN()
    with this
          * --- Valorizzazione Edscadis 
          .w_EDSCADIS = IIF(.w_EDTIPDIS='N','N',.w_EDSCADIS)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oEDSCADIS_2_6.enabled = this.oPgFrm.Page2.oPag.oEDSCADIS_2_6.mCond()
    this.oPgFrm.Page2.oPag.oEDRILTRI_2_12.enabled = this.oPgFrm.Page2.oPag.oEDRILTRI_2_12.mCond()
    this.oPgFrm.Page1.oPag.oCAUMTA_1_50.enabled = this.oPgFrm.Page1.oPag.oCAUMTA_1_50.mCond()
    this.oPgFrm.Page1.oPag.oCAUMTP_1_51.enabled = this.oPgFrm.Page1.oPag.oCAUMTP_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oEDDATESP_2_7.visible=!this.oPgFrm.Page2.oPag.oEDDATESP_2_7.mHide()
    this.oPgFrm.Page2.oPag.oEDGGTOLC_2_8.visible=!this.oPgFrm.Page2.oPag.oEDGGTOLC_2_8.mHide()
    this.oPgFrm.Page2.oPag.oEDGGTOLF_2_9.visible=!this.oPgFrm.Page2.oPag.oEDGGTOLF_2_9.mHide()
    this.oPgFrm.Page2.oPag.oEDDATSCI_2_10.visible=!this.oPgFrm.Page2.oPag.oEDDATSCI_2_10.mHide()
    this.oPgFrm.Page2.oPag.oEDDATSCF_2_11.visible=!this.oPgFrm.Page2.oPag.oEDDATSCF_2_11.mHide()
    this.oPgFrm.Page2.oPag.oEDGIORNO_2_14.visible=!this.oPgFrm.Page2.oPag.oEDGIORNO_2_14.mHide()
    this.oPgFrm.Page2.oPag.oEDDATRIF_2_15.visible=!this.oPgFrm.Page2.oPag.oEDDATRIF_2_15.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_18.visible=!this.oPgFrm.Page2.oPag.oStr_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_19.visible=!this.oPgFrm.Page2.oPag.oStr_2_19.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_21.visible=!this.oPgFrm.Page2.oPag.oStr_2_21.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_22.visible=!this.oPgFrm.Page2.oPag.oStr_2_22.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_23.visible=!this.oPgFrm.Page2.oPag.oStr_2_23.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_26.visible=!this.oPgFrm.Page2.oPag.oStr_2_26.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_27.visible=!this.oPgFrm.Page2.oPag.oStr_2_27.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.DETT.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCAUINC,AZCAUPAG,AZDATCON,AZCAUMTA,AZCAUMTP,AZFLFIDO,AZGIORIS";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZCAUINC,AZCAUPAG,AZDATCON,AZCAUMTA,AZCAUMTP,AZFLFIDO,AZGIORIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZCAUINC = NVL(_Link_.AZCAUINC,space(5))
      this.w_AZCAUPAG = NVL(_Link_.AZCAUPAG,space(5))
      this.w_AZDATCON = NVL(_Link_.AZDATCON,space(1))
      this.w_AZCAUMTA = NVL(_Link_.AZCAUMTA,space(5))
      this.w_AZCAUMTP = NVL(_Link_.AZCAUMTP,space(5))
      this.w_AZFLFIDO = NVL(_Link_.AZFLFIDO,space(1))
      this.w_AZGIORIS = NVL(_Link_.AZGIORIS,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZCAUINC = space(5)
      this.w_AZCAUPAG = space(5)
      this.w_AZDATCON = space(1)
      this.w_AZCAUMTA = space(5)
      this.w_AZCAUMTP = space(5)
      this.w_AZFLFIDO = space(1)
      this.w_AZGIORIS = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EDCODESE
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EDCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_EDCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_EDCODESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EDCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EDCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oEDCODESE_1_42'),i_cWhere,'',"Elenco esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EDCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_EDCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_EDCODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EDCODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_EDCODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EDCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUINC
  func Link_1_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUINC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CAUINC)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CAUINC))
          select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUINC)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUINC) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCAUINC_1_48'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCGIKGD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUINC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUINC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUINC)
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUINC = NVL(_Link_.CCCODICE,space(5))
      this.w_DESINC = NVL(_Link_.CCDESCRI,space(35))
      this.w_DIFINC = NVL(_Link_.CCFLPDIF,space(1))
      this.w_REGINC = NVL(_Link_.CCTIPREG,space(1))
      this.w_TIPINC = NVL(_Link_.CCTIPDOC,space(2))
      this.w_NUMINC = NVL(_Link_.CCNUMREG,0)
      this.w_IVAINC = NVL(_Link_.CCCONIVA,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CAUINC = space(5)
      endif
      this.w_DESINC = space(35)
      this.w_DIFINC = space(1)
      this.w_REGINC = space(1)
      this.w_TIPINC = space(2)
      this.w_NUMINC = 0
      this.w_IVAINC = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DIFINC='S' AND .w_REGINC='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale rilevazione incassi incongruente o inesistente")
        endif
        this.w_CAUINC = space(5)
        this.w_DESINC = space(35)
        this.w_DIFINC = space(1)
        this.w_REGINC = space(1)
        this.w_TIPINC = space(2)
        this.w_NUMINC = 0
        this.w_IVAINC = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUINC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUPAG
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CAUPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CAUPAG))
          select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUPAG)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUPAG) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCAUPAG_1_49'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCGPKGD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUPAG)
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUPAG = NVL(_Link_.CCCODICE,space(5))
      this.w_DESPAG = NVL(_Link_.CCDESCRI,space(35))
      this.w_DIFPAG = NVL(_Link_.CCFLPDIF,space(1))
      this.w_REGPAG = NVL(_Link_.CCTIPREG,space(1))
      this.w_TIPPAG = NVL(_Link_.CCTIPDOC,space(2))
      this.w_NUMPAG = NVL(_Link_.CCNUMREG,0)
      this.w_IVAPAG = NVL(_Link_.CCCONIVA,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CAUPAG = space(5)
      endif
      this.w_DESPAG = space(35)
      this.w_DIFPAG = space(1)
      this.w_REGPAG = space(1)
      this.w_TIPPAG = space(2)
      this.w_NUMPAG = 0
      this.w_IVAPAG = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DIFPAG='S' AND .w_REGPAG='A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale rilevazione pagamenti incongruente o inesistente")
        endif
        this.w_CAUPAG = space(5)
        this.w_DESPAG = space(35)
        this.w_DIFPAG = space(1)
        this.w_REGPAG = space(1)
        this.w_TIPPAG = space(2)
        this.w_NUMPAG = 0
        this.w_IVAPAG = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUMTA
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUMTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CAUMTA)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CAUMTA))
          select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUMTA)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CAUMTA)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CAUMTA)+"%");

            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAUMTA) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCAUMTA_1_50'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCGIKGD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUMTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUMTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUMTA)
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUMTA = NVL(_Link_.CCCODICE,space(5))
      this.w_DESINCAT = NVL(_Link_.CCDESCRI,space(35))
      this.w_DIFINCAT = NVL(_Link_.CCFLPDIF,space(1))
      this.w_REGINCAT = NVL(_Link_.CCTIPREG,space(1))
      this.w_TIPINCAT = NVL(_Link_.CCTIPDOC,space(2))
      this.w_NUMINCAT = NVL(_Link_.CCNUMREG,0)
      this.w_IVAINCAT = NVL(_Link_.CCCONIVA,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CAUMTA = space(5)
      endif
      this.w_DESINCAT = space(35)
      this.w_DIFINCAT = space(1)
      this.w_REGINCAT = space(1)
      this.w_TIPINCAT = space(2)
      this.w_NUMINCAT = 0
      this.w_IVAINCAT = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DIFINCAT='S' AND .w_REGINCAT='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale per maturazione temporale incongruente o inesistente")
        endif
        this.w_CAUMTA = space(5)
        this.w_DESINCAT = space(35)
        this.w_DIFINCAT = space(1)
        this.w_REGINCAT = space(1)
        this.w_TIPINCAT = space(2)
        this.w_NUMINCAT = 0
        this.w_IVAINCAT = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUMTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUMTP
  func Link_1_51(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUMTP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CAUMTP)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CAUMTP))
          select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUMTP)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CAUMTP)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CAUMTP)+"%");

            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAUMTP) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCAUMTP_1_51'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCGPKGD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUMTP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUMTP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUMTP)
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG,CCTIPDOC,CCNUMREG,CCCONIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUMTP = NVL(_Link_.CCCODICE,space(5))
      this.w_DESPAGAP = NVL(_Link_.CCDESCRI,space(35))
      this.w_DIFPAGAP = NVL(_Link_.CCFLPDIF,space(1))
      this.w_REGPAGAP = NVL(_Link_.CCTIPREG,space(1))
      this.w_TIPPAGAP = NVL(_Link_.CCTIPDOC,space(2))
      this.w_NUMPAGAP = NVL(_Link_.CCNUMREG,0)
      this.w_IVAPAGAP = NVL(_Link_.CCCONIVA,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CAUMTP = space(5)
      endif
      this.w_DESPAGAP = space(35)
      this.w_DIFPAGAP = space(1)
      this.w_REGPAGAP = space(1)
      this.w_TIPPAGAP = space(2)
      this.w_NUMPAGAP = 0
      this.w_IVAPAGAP = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DIFPAGAP='S' AND .w_REGPAGAP='A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale per maturazione temporale incongruente o inesistente")
        endif
        this.w_CAUMTP = space(5)
        this.w_DESPAGAP = space(35)
        this.w_DIFPAGAP = space(1)
        this.w_REGPAGAP = space(1)
        this.w_TIPPAGAP = space(2)
        this.w_NUMPAGAP = 0
        this.w_IVAPAGAP = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUMTP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page2.oPag.oEDTIPMOV_2_1.RadioValue()==this.w_EDTIPMOV)
      this.oPgFrm.Page2.oPag.oEDTIPMOV_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEDDATREG_2_2.value==this.w_EDDATREG)
      this.oPgFrm.Page2.oPag.oEDDATREG_2_2.value=this.w_EDDATREG
    endif
    if not(this.oPgFrm.Page2.oPag.oEDDATINI_2_3.value==this.w_EDDATINI)
      this.oPgFrm.Page2.oPag.oEDDATINI_2_3.value=this.w_EDDATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oEDDATFIN_2_4.value==this.w_EDDATFIN)
      this.oPgFrm.Page2.oPag.oEDDATFIN_2_4.value=this.w_EDDATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oEDTIPDIS_2_5.RadioValue()==this.w_EDTIPDIS)
      this.oPgFrm.Page2.oPag.oEDTIPDIS_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEDSCADIS_2_6.RadioValue()==this.w_EDSCADIS)
      this.oPgFrm.Page2.oPag.oEDSCADIS_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEDDATESP_2_7.value==this.w_EDDATESP)
      this.oPgFrm.Page2.oPag.oEDDATESP_2_7.value=this.w_EDDATESP
    endif
    if not(this.oPgFrm.Page2.oPag.oEDGGTOLC_2_8.value==this.w_EDGGTOLC)
      this.oPgFrm.Page2.oPag.oEDGGTOLC_2_8.value=this.w_EDGGTOLC
    endif
    if not(this.oPgFrm.Page2.oPag.oEDGGTOLF_2_9.value==this.w_EDGGTOLF)
      this.oPgFrm.Page2.oPag.oEDGGTOLF_2_9.value=this.w_EDGGTOLF
    endif
    if not(this.oPgFrm.Page2.oPag.oEDDATSCI_2_10.value==this.w_EDDATSCI)
      this.oPgFrm.Page2.oPag.oEDDATSCI_2_10.value=this.w_EDDATSCI
    endif
    if not(this.oPgFrm.Page2.oPag.oEDDATSCF_2_11.value==this.w_EDDATSCF)
      this.oPgFrm.Page2.oPag.oEDDATSCF_2_11.value=this.w_EDDATSCF
    endif
    if not(this.oPgFrm.Page2.oPag.oEDRILTRI_2_12.RadioValue()==this.w_EDRILTRI)
      this.oPgFrm.Page2.oPag.oEDRILTRI_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEDMATEMP_2_13.RadioValue()==this.w_EDMATEMP)
      this.oPgFrm.Page2.oPag.oEDMATEMP_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEDGIORNO_2_14.value==this.w_EDGIORNO)
      this.oPgFrm.Page2.oPag.oEDGIORNO_2_14.value=this.w_EDGIORNO
    endif
    if not(this.oPgFrm.Page2.oPag.oEDDATRIF_2_15.value==this.w_EDDATRIF)
      this.oPgFrm.Page2.oPag.oEDDATRIF_2_15.value=this.w_EDDATRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oEDNUMERO_1_41.value==this.w_EDNUMERO)
      this.oPgFrm.Page1.oPag.oEDNUMERO_1_41.value=this.w_EDNUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oEDCODESE_1_42.value==this.w_EDCODESE)
      this.oPgFrm.Page1.oPag.oEDCODESE_1_42.value=this.w_EDCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oEDDATGEN_1_47.value==this.w_EDDATGEN)
      this.oPgFrm.Page1.oPag.oEDDATGEN_1_47.value=this.w_EDDATGEN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUINC_1_48.value==this.w_CAUINC)
      this.oPgFrm.Page1.oPag.oCAUINC_1_48.value=this.w_CAUINC
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUPAG_1_49.value==this.w_CAUPAG)
      this.oPgFrm.Page1.oPag.oCAUPAG_1_49.value=this.w_CAUPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUMTA_1_50.value==this.w_CAUMTA)
      this.oPgFrm.Page1.oPag.oCAUMTA_1_50.value=this.w_CAUMTA
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUMTP_1_51.value==this.w_CAUMTP)
      this.oPgFrm.Page1.oPag.oCAUMTP_1_51.value=this.w_CAUMTP
    endif
    if not(this.oPgFrm.Page1.oPag.oEDDESCRI_1_52.value==this.w_EDDESCRI)
      this.oPgFrm.Page1.oPag.oEDDESCRI_1_52.value=this.w_EDDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINC_1_58.value==this.w_DESINC)
      this.oPgFrm.Page1.oPag.oDESINC_1_58.value=this.w_DESINC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_59.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_59.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINCAT_1_63.value==this.w_DESINCAT)
      this.oPgFrm.Page1.oPag.oDESINCAT_1_63.value=this.w_DESINCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAGAP_1_64.value==this.w_DESPAGAP)
      this.oPgFrm.Page1.oPag.oDESPAGAP_1_64.value=this.w_DESPAGAP
    endif
    cp_SetControlsValueExtFlds(this,'ESI_DIF')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_EDDATESP))  and not(.w_EDSCADIS='N')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oEDDATESP_2_7.SetFocus()
            i_bnoObbl = !empty(.w_EDDATESP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EDDATRIF))  and not(.w_EDMATEMP='N')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oEDDATRIF_2_15.SetFocus()
            i_bnoObbl = !empty(.w_EDDATRIF)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EDNUMERO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEDNUMERO_1_41.SetFocus()
            i_bnoObbl = !empty(.w_EDNUMERO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EDCODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEDCODESE_1_42.SetFocus()
            i_bnoObbl = !empty(.w_EDCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_EDDATGEN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oEDDATGEN_1_47.SetFocus()
            i_bnoObbl = !empty(.w_EDDATGEN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CAUINC)) or not(.w_DIFINC='S' AND .w_REGINC='V'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUINC_1_48.SetFocus()
            i_bnoObbl = !empty(.w_CAUINC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale rilevazione incassi incongruente o inesistente")
          case   ((empty(.w_CAUPAG)) or not(.w_DIFPAG='S' AND .w_REGPAG='A'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUPAG_1_49.SetFocus()
            i_bnoObbl = !empty(.w_CAUPAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale rilevazione pagamenti incongruente o inesistente")
          case   ((empty(.w_CAUMTA)) or not(.w_DIFINCAT='S' AND .w_REGINCAT='V'))  and (.w_EDMATEMP='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUMTA_1_50.SetFocus()
            i_bnoObbl = !empty(.w_CAUMTA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale per maturazione temporale incongruente o inesistente")
          case   ((empty(.w_CAUMTP)) or not(.w_DIFPAGAP='S' AND .w_REGPAGAP='A'))  and (.w_EDMATEMP='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUMTP_1_51.SetFocus()
            i_bnoObbl = !empty(.w_CAUMTP)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale per maturazione temporale incongruente o inesistente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_EDSERIAL = this.w_EDSERIAL
    this.o_EDTIPDIS = this.w_EDTIPDIS
    this.o_EDSCADIS = this.w_EDSCADIS
    this.o_EDMATEMP = this.w_EDMATEMP
    this.o_EDDATRIF = this.w_EDDATRIF
    this.o_AZCAUINC = this.w_AZCAUINC
    this.o_AZCAUPAG = this.w_AZCAUPAG
    this.o_AZCAUMTA = this.w_AZCAUMTA
    this.o_EDDATGEN = this.w_EDDATGEN
    return

enddefine

* --- Define pages as container
define class tgscg_aedPag1 as StdContainer
  Width  = 671
  height = 534
  stdWidth  = 671
  stdheight = 534
  resizeXpos=646
  resizeYpos=247
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_13 as StdButton with uid="ASBPFHFRCA",left=540, top=480, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare il piano movimenti";
    , HelpContextID = 76133658;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CAUINC) AND NOT EMPTY(.w_CAUPAG) AND (.w_EDMATEMP='N' OR (.w_EDMATEMP='S' AND  NOT EMPTY(.w_CAUMTA) AND NOT EMPTY(.w_CAUMTP))) AND .cFunction='Load')
      endwith
    endif
  endfunc


  add object DETT as cp_szoombox with uid="GEWSSFXCZX",left=14, top=234, width=642,height=241,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PNT_MAST",cZoomFile="GSCG1AED",bOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,bAdvOptions=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="GSCG_MPN",;
    cEvent = "Carica,Load",;
    nPag=1;
    , HelpContextID = 101835238


  add object oBtn_1_26 as StdButton with uid="KZSQCYUNAO",left=590, top=480, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare";
    , HelpContextID = 68844986;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_27 as cp_runprogram with uid="RRQDKDRONJ",left=-8, top=550, width=184,height=20,;
    caption='GSCG_BG1(E)',;
   bGlobalFont=.t.,;
    prg="GSCG_BG1('E')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 62142999


  add object oBtn_1_28 as StdButton with uid="PBZMZMHGFH",left=340, top=480, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare la registrazione di storno relativa all'incasso selezionato";
    , HelpContextID = 7985990;
    , Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        GSCG_BG1(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERRIF) AND .cFunction='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_29 as StdButton with uid="TMLNBOEYVZ",left=390, top=480, width=48,height=45,;
    CpPicture="BMP\SoldiT.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla registrazione di primanota di incasso";
    , HelpContextID = 101547398;
    , Caption='\<Incasso';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSCG_BG1(this.Parent.oContained,"P",.w_SERRIF)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERRIF) AND .w_DPTIPREG<>'S')
      endwith
    endif
  endfunc


  add object oBtn_1_30 as StdButton with uid="ATXSNCNTGS",left=440, top=480, width=48,height=45,;
    CpPicture="bmp\fattura.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al dettaglio emissioni fatture";
    , HelpContextID = 88178774;
    , Caption='\<Fatture';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      do GSCG_KEF with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERRIF))
      endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="EWSLGKXOHQ",left=490, top=480, width=48,height=45,;
    CpPicture="bmp\DETTAGLI.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare gli storni relativi all'incasso selezionato.";
    , HelpContextID = 117341025;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      do GSCG_KED with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERRIF))
      endwith
    endif
  endfunc


  add object oObj_1_32 as cp_runprogram with uid="DCULBWMAHV",left=180, top=572, width=184,height=20,;
    caption='GSCG_BGD',;
   bGlobalFont=.t.,;
    prg="GSCG_BGD",;
    cEvent = "Record Inserted",;
    nPag=1;
    , HelpContextID = 61956778


  add object oObj_1_34 as cp_runprogram with uid="ODPUEPOKEW",left=-8, top=572, width=184,height=20,;
    caption='GSCG_BG1',;
   bGlobalFont=.t.,;
    prg="GSCG_BG1('P',w_SERRIF)",;
    cEvent = "w_dett selected",;
    nPag=1;
    , HelpContextID = 61956759

  add object oEDNUMERO_1_41 as StdField with uid="QMFVWTHZRN",rtseq=45,rtrep=.f.,;
    cFormVar = "w_EDNUMERO", cQueryName = "EDNUMERO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo registrazione",;
    HelpContextID = 94372757,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=148, Top=8, cSayPict='"999999"', cGetPict='"999999"'

  add object oEDCODESE_1_42 as StdField with uid="DOLLSQMFIQ",rtseq=46,rtrep=.f.,;
    cFormVar = "w_EDCODESE", cQueryName = "EDCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza",;
    HelpContextID = 84497291,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=223, Top=8, InputMask=replicate('X',4), bHasZoom = .t. , tabstop=.f., cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_EDCODESE"

  func oEDCODESE_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oEDCODESE_1_42.ecpDrop(oSource)
    this.Parent.oContained.link_1_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEDCODESE_1_42.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oEDCODESE_1_42'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco esercizi",'',this.parent.oContained
  endproc

  add object oEDDATGEN_1_47 as StdField with uid="FNUHTQLIIV",rtseq=51,rtrep=.f.,;
    cFormVar = "w_EDDATGEN", cQueryName = "EDDATGEN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della registrazione",;
    HelpContextID = 133915540,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=314, Top=8

  add object oCAUINC_1_48 as StdField with uid="ISMKEUSGWK",rtseq=52,rtrep=.f.,;
    cFormVar = "w_CAUINC", cQueryName = "CAUINC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale rilevazione incassi incongruente o inesistente",;
    ToolTipText = "Causale contabile dei movimenti di rilevazione incassi ad esigib.differita",;
    HelpContextID = 207327194,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=224, Top=50, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CAUINC"

  func oCAUINC_1_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_48('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUINC_1_48.ecpDrop(oSource)
    this.Parent.oContained.link_1_48('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUINC_1_48.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCAUINC_1_48'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCGIKGD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCAUINC_1_48.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CAUINC
     i_obj.ecpSave()
  endproc

  add object oCAUPAG_1_49 as StdField with uid="KXXGAOXIEE",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CAUPAG", cQueryName = "CAUPAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale rilevazione pagamenti incongruente o inesistente",;
    ToolTipText = "Causale contabile dei movimenti di rilevazione pagamenti ad esigib.differita",;
    HelpContextID = 153391066,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=224, Top=89, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CAUPAG"

  func oCAUPAG_1_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_49('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUPAG_1_49.ecpDrop(oSource)
    this.Parent.oContained.link_1_49('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUPAG_1_49.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCAUPAG_1_49'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCGPKGD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCAUPAG_1_49.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CAUPAG
     i_obj.ecpSave()
  endproc

  add object oCAUMTA_1_50 as StdField with uid="THSMPYFHRG",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CAUMTA", cQueryName = "CAUMTA",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale per maturazione temporale incongruente o inesistente",;
    ToolTipText = "Causale contabile attiva per maturazione temporale IVA in sospensione",;
    HelpContextID = 234328026,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=224, Top=128, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CAUMTA"

  func oCAUMTA_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDMATEMP='S')
    endwith
   endif
  endfunc

  func oCAUMTA_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_50('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUMTA_1_50.ecpDrop(oSource)
    this.Parent.oContained.link_1_50('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUMTA_1_50.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCAUMTA_1_50'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCGIKGD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCAUMTA_1_50.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CAUMTA
     i_obj.ecpSave()
  endproc

  add object oCAUMTP_1_51 as StdField with uid="BSLJLKGXDP",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CAUMTP", cQueryName = "CAUMTP",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale per maturazione temporale incongruente o inesistente",;
    ToolTipText = "Causale contabile passiva per maturazione temporale IVA in sospensione",;
    HelpContextID = 17330214,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=224, Top=167, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CAUMTP"

  func oCAUMTP_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDMATEMP='S')
    endwith
   endif
  endfunc

  func oCAUMTP_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_51('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUMTP_1_51.ecpDrop(oSource)
    this.Parent.oContained.link_1_51('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUMTP_1_51.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCAUMTP_1_51'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCGPKGD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCAUMTP_1_51.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CAUMTP
     i_obj.ecpSave()
  endproc

  add object oEDDESCRI_1_52 as StdField with uid="NFKAUKIQHU",rtseq=56,rtrep=.f.,;
    cFormVar = "w_EDDESCRI", cQueryName = "EDDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione aggiuntiva",;
    HelpContextID = 66020239,;
   bGlobalFont=.t.,;
    Height=21, Width=325, Left=224, Top=206, InputMask=replicate('X',40)

  add object oDESINC_1_58 as StdField with uid="BZGOXPQHPQ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DESINC", cQueryName = "DESINC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 207334346,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=293, Top=50, InputMask=replicate('X',35)

  add object oDESPAG_1_59 as StdField with uid="RURLONOYAR",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 153398218,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=293, Top=89, InputMask=replicate('X',35)

  add object oDESINCAT_1_63 as StdField with uid="WHYPNUCKEU",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DESINCAT", cQueryName = "DESINCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 61101194,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=293, Top=128, InputMask=replicate('X',35)

  add object oDESPAGAP_1_64 as StdField with uid="WKGVBOVHWA",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESPAGAP", cQueryName = "DESPAGAP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 115037318,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=293, Top=167, InputMask=replicate('X',35)


  add object oObj_1_65 as cp_runprogram with uid="HZITPOHOEH",left=180, top=550, width=184,height=20,;
    caption='GSCG_BG1(L)',;
   bGlobalFont=.t.,;
    prg="GSCG_BG1('L')",;
    cEvent = "Load",;
    nPag=1;
    , HelpContextID = 62144791

  add object oStr_1_53 as StdString with uid="XCAEFGCYVD",Visible=.t., Left=285, Top=8,;
    Alignment=1, Width=27, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_54 as StdString with uid="QYKCIHIUFR",Visible=.t., Left=68, Top=8,;
    Alignment=1, Width=79, Height=15,;
    Caption="Numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_55 as StdString with uid="CKTWSMLJJH",Visible=.t., Left=135, Top=210,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="UXJPVHHUYU",Visible=.t., Left=217, Top=8,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="NEAHNJJWQE",Visible=.t., Left=92, Top=53,;
    Alignment=1, Width=125, Height=18,;
    Caption="Causale per incassi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="FBZJINFXAQ",Visible=.t., Left=72, Top=90,;
    Alignment=1, Width=145, Height=19,;
    Caption="Causale per pagamenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="PSKHBVNINI",Visible=.t., Left=5, Top=129,;
    Alignment=1, Width=212, Height=18,;
    Caption="Causale attiva per mat. temporale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="QAXWMJTLDZ",Visible=.t., Left=5, Top=169,;
    Alignment=1, Width=212, Height=18,;
    Caption="Causale passiva per mat. temporale:"  ;
  , bGlobalFont=.t.
enddefine
define class tgscg_aedPag2 as StdContainer
  Width  = 671
  height = 534
  stdWidth  = 671
  stdheight = 534
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oEDTIPMOV_2_1 as StdCombo with uid="NYUBJTQIXD",rtseq=30,rtrep=.f.,left=176,top=19,width=123,height=22;
    , HelpContextID = 37461092;
    , cFormVar="w_EDTIPMOV",RowSource=""+"Incassi,"+"Pagamenti,"+"Entrambi", bObbl = .f. , nPag = 2;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oEDTIPMOV_2_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oEDTIPMOV_2_1.GetRadio()
    this.Parent.oContained.w_EDTIPMOV = this.RadioValue()
    return .t.
  endfunc

  func oEDTIPMOV_2_1.SetRadio()
    this.Parent.oContained.w_EDTIPMOV=trim(this.Parent.oContained.w_EDTIPMOV)
    this.value = ;
      iif(this.Parent.oContained.w_EDTIPMOV=='C',1,;
      iif(this.Parent.oContained.w_EDTIPMOV=='F',2,;
      iif(this.Parent.oContained.w_EDTIPMOV=='E',3,;
      0)))
  endfunc

  add object oEDDATREG_2_2 as StdField with uid="UREFSVAPEW",rtseq=31,rtrep=.f.,;
    cFormVar = "w_EDDATREG", cQueryName = "EDDATREG",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Se valorizzata verr� usata per le scadenze incassate e maturate iva sospensione. Fa eccezione l'iva autotrasportatori perch� sar� mantenuto il posticipo a trimestre successivo alla fattura se l'incasso cade nel trimestre di registrazione della fattura",;
    HelpContextID = 50029453,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=553, Top=22

  add object oEDDATINI_2_3 as StdField with uid="NBCFIYIWPP",rtseq=32,rtrep=.f.,;
    cFormVar = "w_EDDATINI", cQueryName = "EDDATINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio selezione incassi/ pagamenti ",;
    HelpContextID = 100965489,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=176, Top=74

  add object oEDDATFIN_2_4 as StdField with uid="THQEEQOKDP",rtseq=33,rtrep=.f.,;
    cFormVar = "w_EDDATFIN", cQueryName = "EDDATFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine selezione incassi/ pagamenti ",;
    HelpContextID = 151297132,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=323, Top=74


  add object oEDTIPDIS_2_5 as StdCombo with uid="QPDEQWGZNZ",rtseq=34,rtrep=.f.,left=176,top=129,width=125,height=21;
    , HelpContextID = 188456039;
    , cFormVar="w_EDTIPDIS",RowSource=""+"Attive e passive,"+"Solo attive,"+"Solo passive,"+"Escluse", bObbl = .f. , nPag = 2;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oEDTIPDIS_2_5.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'A',;
    iif(this.value =3,'P',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oEDTIPDIS_2_5.GetRadio()
    this.Parent.oContained.w_EDTIPDIS = this.RadioValue()
    return .t.
  endfunc

  func oEDTIPDIS_2_5.SetRadio()
    this.Parent.oContained.w_EDTIPDIS=trim(this.Parent.oContained.w_EDTIPDIS)
    this.value = ;
      iif(this.Parent.oContained.w_EDTIPDIS=='E',1,;
      iif(this.Parent.oContained.w_EDTIPDIS=='A',2,;
      iif(this.Parent.oContained.w_EDTIPDIS=='P',3,;
      iif(this.Parent.oContained.w_EDTIPDIS=='N',4,;
      0))))
  endfunc

  add object oEDSCADIS_2_6 as StdCheck with uid="KKDUVYGSFR",rtseq=35,rtrep=.f.,left=176, top=174, caption="Considera scadenze in distinta come esposte",;
    ToolTipText = "Se attivato, vengono considerate le scadenze chiuse in distinta o in contabilizzazione indiretta effetti con data scadenza pi� giorni di tolleranza inferiore o uguale alla data specificata ",;
    HelpContextID = 204581991,;
    cFormVar="w_EDSCADIS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oEDSCADIS_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oEDSCADIS_2_6.GetRadio()
    this.Parent.oContained.w_EDSCADIS = this.RadioValue()
    return .t.
  endfunc

  func oEDSCADIS_2_6.SetRadio()
    this.Parent.oContained.w_EDSCADIS=trim(this.Parent.oContained.w_EDSCADIS)
    this.value = ;
      iif(this.Parent.oContained.w_EDSCADIS=='S',1,;
      0)
  endfunc

  func oEDSCADIS_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDTIPDIS $ 'EAP')
    endwith
   endif
  endfunc

  add object oEDDATESP_2_7 as StdField with uid="DEMXHCJVPF",rtseq=36,rtrep=.f.,;
    cFormVar = "w_EDDATESP", cQueryName = "EDDATESP",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 100361110,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=553, Top=175

  func oEDDATESP_2_7.mHide()
    with this.Parent.oContained
      return (.w_EDSCADIS='N')
    endwith
  endfunc

  add object oEDGGTOLC_2_8 as StdField with uid="SAZYNLZFHF",rtseq=37,rtrep=.f.,;
    cFormVar = "w_EDGGTOLC", cQueryName = "EDGGTOLC",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni tolleranza incassi",;
    HelpContextID = 268332151,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=333, Top=223, cSayPict='"999"', cGetPict='"999"'

  func oEDGGTOLC_2_8.mHide()
    with this.Parent.oContained
      return (.w_EDSCADIS='N')
    endwith
  endfunc

  add object oEDGGTOLF_2_9 as StdField with uid="USPIBPBMBX",rtseq=38,rtrep=.f.,;
    cFormVar = "w_EDGGTOLF", cQueryName = "EDGGTOLF",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni tolleranza pagamenti",;
    HelpContextID = 268332148,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=553, Top=223, cSayPict='"999"', cGetPict='"999"'

  func oEDGGTOLF_2_9.mHide()
    with this.Parent.oContained
      return (.w_EDSCADIS='N')
    endwith
  endfunc

  add object oEDDATSCI_2_10 as StdField with uid="CNFFUFZYYY",rtseq=39,rtrep=.f.,;
    cFormVar = "w_EDDATSCI", cQueryName = "EDDATSCI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scadenza di inizio selezione",;
    HelpContextID = 66806671,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=333, Top=270

  func oEDDATSCI_2_10.mHide()
    with this.Parent.oContained
      return (.w_EDSCADIS='N')
    endwith
  endfunc

  add object oEDDATSCF_2_11 as StdField with uid="YLRZFNUCYU",rtseq=40,rtrep=.f.,;
    cFormVar = "w_EDDATSCF", cQueryName = "EDDATSCF",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scadenza di fine selezione",;
    HelpContextID = 66806668,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=553, Top=270

  func oEDDATSCF_2_11.mHide()
    with this.Parent.oContained
      return (.w_EDSCADIS='N')
    endwith
  endfunc

  add object oEDRILTRI_2_12 as StdCheck with uid="ZJBGEDRZRN",rtseq=41,rtrep=.f.,left=176, top=325, caption="Rileva incasso a trimestre differito",;
    ToolTipText = "Se attivo, la data di registrazione dello storno delle fatture o note di credito autotrasportatori ad esigibilit� differita viene posticipata alla fine del trimestre successivo se la data dell'incasso � presente nello stesso trimestre della fattura",;
    HelpContextID = 75776911,;
    cFormVar="w_EDRILTRI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oEDRILTRI_2_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oEDRILTRI_2_12.GetRadio()
    this.Parent.oContained.w_EDRILTRI = this.RadioValue()
    return .t.
  endfunc

  func oEDRILTRI_2_12.SetRadio()
    this.Parent.oContained.w_EDRILTRI=trim(this.Parent.oContained.w_EDRILTRI)
    this.value = ;
      iif(this.Parent.oContained.w_EDRILTRI=='S',1,;
      0)
  endfunc

  func oEDRILTRI_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDMATEMP='N')
    endwith
   endif
  endfunc

  add object oEDMATEMP_2_13 as StdCheck with uid="XRBDEOLCBG",rtseq=42,rtrep=.f.,left=176, top=375, caption="Maturazione temporale IVA in sospensione",;
    ToolTipText = "Se attivo viene eseguita la maturazione temporale dell'IVA in sospensione in base ai giorni e alla data di riferimento",;
    HelpContextID = 168037482,;
    cFormVar="w_EDMATEMP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oEDMATEMP_2_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oEDMATEMP_2_13.GetRadio()
    this.Parent.oContained.w_EDMATEMP = this.RadioValue()
    return .t.
  endfunc

  func oEDMATEMP_2_13.SetRadio()
    this.Parent.oContained.w_EDMATEMP=trim(this.Parent.oContained.w_EDMATEMP)
    this.value = ;
      iif(this.Parent.oContained.w_EDMATEMP=='S',1,;
      0)
  endfunc

  add object oEDGIORNO_2_14 as StdField with uid="RRZCZODVVN",rtseq=43,rtrep=.f.,;
    cFormVar = "w_EDGIORNO", cQueryName = "EDGIORNO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni utilizzati per il calcolo della maturazione temporale IVA in sospensione",;
    HelpContextID = 223112299,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=333, Top=418, cSayPict='"999"', cGetPict='"999"'

  func oEDGIORNO_2_14.mHide()
    with this.Parent.oContained
      return (.w_EDMATEMP='N')
    endwith
  endfunc

  add object oEDDATRIF_2_15 as StdField with uid="PBRRXJQCDI",rtseq=44,rtrep=.f.,;
    cFormVar = "w_EDDATRIF", cQueryName = "EDDATRIF",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data a partire dalla quale vengono sottratti i giorni per il calcolo della maturazione temporale IVA in sospensione",;
    HelpContextID = 218406004,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=553, Top=418

  func oEDDATRIF_2_15.mHide()
    with this.Parent.oContained
      return (.w_EDMATEMP='N')
    endwith
  endfunc


  add object oObj_2_20 as cp_calclbl with uid="PRGPKDSJXC",left=451, top=377, width=149,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Data",;
    cEvent = "w_Azdatcon,w_Edmatemp",;
    nPag=2;
    , HelpContextID = 101835238

  add object oStr_2_16 as StdString with uid="AJBFAFMWAW",Visible=.t., Left=35, Top=75,;
    Alignment=1, Width=135, Height=18,;
    Caption="Incassi/pagamenti dal:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="ZMUGLLABBO",Visible=.t., Left=296, Top=75,;
    Alignment=1, Width=23, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="JIABMIDYTW",Visible=.t., Left=279, Top=420,;
    Alignment=1, Width=49, Height=18,;
    Caption="Giorni:"  ;
  , bGlobalFont=.t.

  func oStr_2_18.mHide()
    with this.Parent.oContained
      return (.w_EDMATEMP='N')
    endwith
  endfunc

  add object oStr_2_19 as StdString with uid="MXSDEVPKVV",Visible=.t., Left=424, Top=420,;
    Alignment=1, Width=127, Height=18,;
    Caption="Data di riferimento:"  ;
  , bGlobalFont=.t.

  func oStr_2_19.mHide()
    with this.Parent.oContained
      return (.w_EDMATEMP='N')
    endwith
  endfunc

  add object oStr_2_21 as StdString with uid="XLMYAMDEBH",Visible=.t., Left=175, Top=224,;
    Alignment=1, Width=153, Height=18,;
    Caption="Gg. tolleranza incassi:"  ;
  , bGlobalFont=.t.

  func oStr_2_21.mHide()
    with this.Parent.oContained
      return (.w_EDSCADIS='N')
    endwith
  endfunc

  add object oStr_2_22 as StdString with uid="LLNGLGYSHV",Visible=.t., Left=394, Top=224,;
    Alignment=1, Width=157, Height=18,;
    Caption="Gg. tolleranza pagamenti:"  ;
  , bGlobalFont=.t.

  func oStr_2_22.mHide()
    with this.Parent.oContained
      return (.w_EDSCADIS='N')
    endwith
  endfunc

  add object oStr_2_23 as StdString with uid="XLFMBCOWFB",Visible=.t., Left=483, Top=177,;
    Alignment=1, Width=68, Height=18,;
    Caption="alla data:"  ;
  , bGlobalFont=.t.

  func oStr_2_23.mHide()
    with this.Parent.oContained
      return (.w_EDSCADIS='N')
    endwith
  endfunc

  add object oStr_2_24 as StdString with uid="UEWHUFIHTC",Visible=.t., Left=77, Top=130,;
    Alignment=1, Width=93, Height=18,;
    Caption="Distinte:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="QSGEVQUUPQ",Visible=.t., Left=77, Top=23,;
    Alignment=1, Width=93, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="ZLRURFNOLY",Visible=.t., Left=200, Top=271,;
    Alignment=1, Width=128, Height=18,;
    Caption="Data data scadenza:"  ;
  , bGlobalFont=.t.

  func oStr_2_26.mHide()
    with this.Parent.oContained
      return (.w_EDSCADIS='N')
    endwith
  endfunc

  add object oStr_2_27 as StdString with uid="FHPJWIINLK",Visible=.t., Left=423, Top=271,;
    Alignment=1, Width=128, Height=18,;
    Caption="A data scadenza:"  ;
  , bGlobalFont=.t.

  func oStr_2_27.mHide()
    with this.Parent.oContained
      return (.w_EDSCADIS='N')
    endwith
  endfunc

  add object oStr_2_28 as StdString with uid="VPYTHVCVDL",Visible=.t., Left=408, Top=23,;
    Alignment=1, Width=143, Height=18,;
    Caption="Data reg. movimento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_aed','ESI_DIF','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".EDSERIAL=ESI_DIF.EDSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
