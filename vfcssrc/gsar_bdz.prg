* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bdz                                                        *
*              Gestione zoom Risorse                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-28                                                      *
* Last revis.: 2013-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bdz",oParentObject)
return(i_retval)

define class tgsar_bdz as StdBatch
  * --- Local variables
  w_DXBTN = .f.
  w_TIPCOL = space(1)
  w_CODCOL = space(5)
  w_PROG = .NULL.
  * --- WorkFile variables
  DIPENDEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      if Type("g_oMenu.oKey(2,3)")="C"
        this.w_TIPCOL = Nvl(g_oMenu.oKey(1,3)," ")
        this.w_CODCOL = Nvl(g_oMenu.oKey(2,3)," ")
      else
        this.w_CODCOL = Nvl(g_oMenu.oKey(1,3)," ")
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPTIPRIS"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(this.w_CODCOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPTIPRIS;
            from (i_cTable) where;
                DPCODICE = this.w_CODCOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPCOL = NVL(cp_ToDate(_read_.DPTIPRIS),cp_NullValue(_read_.DPTIPRIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    else
      * --- Se la variabile che riferisce all'ultimo form aperto � vuota esco
      if isnull(i_curform)
        i_retcode = 'stop'
        return
      endif
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor 
 Select (cCurs)
      if TYPE("DPTIPRIS")="C"
        * --- Tipo di Conto
        this.w_TIPCOL = &cCurs..DPTIPRIS
        * --- Codice conto
        this.w_CODCOL = &cCurs..DPCODICE
      endif
    endif
    this.w_TIPCOL = iif(Empty(this.w_TIPCOL),"P",this.w_TIPCOL)
    this.w_PROG = GSAR_ADP("N"+Alltrim(this.w_TIPCOL))
    if this.w_DXBTN
      * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
      *     presente nella variabile
      if g_oMenu.cBatchType$"AM"
        * --- Apri o Modifica
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_DPCODICE = this.w_CODCOL
        this.w_PROG.w_DPTIPRIS = this.w_TIPCOL
        this.w_PROG.ecpSave()     
        if g_oMenu.cBatchType="M"
          * --- Modifica
          this.w_PROG.ecpEdit()     
        endif
      endif
      if g_oMenu.cBatchType="L"
        * --- Carica
        this.w_PROG.ecpLoad()     
      endif
    else
      if this.w_PROG.bSEC1
        this.w_PROG.w_DPCODICE = this.w_CODCOL
        this.w_PROG.QueryKeySet("DPCODICE='"+this.w_CODCOL+"'")     
        this.w_PROG.LoadRecWarn()     
      else
        L_OLDERR=ON("ERROR")
        ON ERROR L_FErr=.T.
        L_T=1/"A"
        ON ERROR &L_OLDERR
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DIPENDEN'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
