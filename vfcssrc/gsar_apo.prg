* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_apo                                                        *
*              Porti                                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2008-09-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_apo"))

* --- Class definition
define class tgsar_apo as StdForm
  Top    = 63
  Left   = 44

  * --- Standard Properties
  Width  = 350
  Height = 54+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-01"
  HelpContextID=108431511
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  PORTI_IDX = 0
  cFile = "PORTI"
  cKeySelect = "POCODPOR"
  cKeyWhere  = "POCODPOR=this.w_POCODPOR"
  cKeyWhereODBC = '"POCODPOR="+cp_ToStrODBC(this.w_POCODPOR)';

  cKeyWhereODBCqualified = '"PORTI.POCODPOR="+cp_ToStrODBC(this.w_POCODPOR)';

  cPrg = "gsar_apo"
  cComment = "Porti"
  icon = "anag.ico"
  cAutoZoom = 'GSVE0APO'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_POCODPOR = space(1)
  w_PODESPOR = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PORTI','gsar_apo')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_apoPag1","gsar_apo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Porti")
      .Pages(1).HelpContextID = 226630902
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPOCODPOR_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PORTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PORTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PORTI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_POCODPOR = NVL(POCODPOR,space(1))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PORTI where POCODPOR=KeySet.POCODPOR
    *
    i_nConn = i_TableProp[this.PORTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PORTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PORTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PORTI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'POCODPOR',this.w_POCODPOR  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_POCODPOR = NVL(POCODPOR,space(1))
        .w_PODESPOR = NVL( cp_TransLoadField('PODESPOR') ,space(30))
        cp_LoadRecExtFlds(this,'PORTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_POCODPOR = space(1)
      .w_PODESPOR = space(30)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'PORTI')
    this.DoRTCalc(1,2,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPOCODPOR_1_1.enabled = i_bVal
      .Page1.oPag.oPODESPOR_1_2.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPOCODPOR_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPOCODPOR_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PORTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PORTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_POCODPOR,"POCODPOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PODESPOR,"PODESPOR",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PORTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
    i_lTable = "PORTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PORTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("QUERY\GSMA_QP1.VQR,QUERY\GSMA_SAS.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PORTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PORTI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PORTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PORTI')
        i_extval=cp_InsertValODBCExtFlds(this,'PORTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(POCODPOR,PODESPOR"+cp_TransInsFldName('PODESPOR')+" "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_POCODPOR)+;
                  ","+cp_ToStrODBC(this.w_PODESPOR)+cp_TransInsFldValue(this.w_PODESPOR)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PORTI')
        i_extval=cp_InsertValVFPExtFlds(this,'PORTI')
        cp_CheckDeletedKey(i_cTable,0,'POCODPOR',this.w_POCODPOR)
        INSERT INTO (i_cTable);
              (POCODPOR,PODESPOR  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_POCODPOR;
                  ,this.w_PODESPOR;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PORTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PORTI_IDX,i_nConn)
      *
      * update PORTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PORTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PODESPOR="+cp_TransUpdFldName('PODESPOR',this.w_PODESPOR)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PORTI')
        i_cWhere = cp_PKFox(i_cTable  ,'POCODPOR',this.w_POCODPOR  )
        UPDATE (i_cTable) SET;
              PODESPOR=this.w_PODESPOR;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PORTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PORTI_IDX,i_nConn)
      *
      * delete PORTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'POCODPOR',this.w_POCODPOR  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PORTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,2,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPOCODPOR_1_1.value==this.w_POCODPOR)
      this.oPgFrm.Page1.oPag.oPOCODPOR_1_1.value=this.w_POCODPOR
    endif
    if not(this.oPgFrm.Page1.oPag.oPODESPOR_1_2.value==this.w_PODESPOR)
      this.oPgFrm.Page1.oPag.oPODESPOR_1_2.value=this.w_PODESPOR
    endif
    cp_SetControlsValueExtFlds(this,'PORTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_POCODPOR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPOCODPOR_1_1.SetFocus()
            i_bnoObbl = !empty(.w_POCODPOR)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_apoPag1 as StdContainer
  Width  = 346
  height = 54
  stdWidth  = 346
  stdheight = 54
  resizeXpos=245
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPOCODPOR_1_1 as StdField with uid="ANWNLCMWNV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_POCODPOR", cQueryName = "POCODPOR",;
    bObbl = .t. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Codice del porto",;
    HelpContextID = 83227320,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=95, Top=6, InputMask=replicate('X',1)

  add object oPODESPOR_1_2 as StdField with uid="SIYPHZFNKB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PODESPOR", cQueryName = "PODESPOR",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .t.,;
    ToolTipText = "Descrizione del porto",;
    HelpContextID = 68149944,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=95, Top=30, InputMask=replicate('X',30)

  add object oStr_1_3 as StdString with uid="HOGYMYZNXH",Visible=.t., Left=16, Top=6,;
    Alignment=1, Width=77, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="QYSBGIZFQV",Visible=.t., Left=11, Top=30,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_apo','PORTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".POCODPOR=PORTI.POCODPOR";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
