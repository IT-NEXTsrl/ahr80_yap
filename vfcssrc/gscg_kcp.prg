* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kcp                                                        *
*              Conferma primanota                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-14                                                      *
* Last revis.: 2008-07-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kcp",oParentObject))

* --- Class definition
define class tgscg_kcp as StdForm
  Top    = 112
  Left   = 87

  * --- Standard Properties
  Width  = 388
  Height = 233
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-03"
  HelpContextID=169204375
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gscg_kcp"
  cComment = "Conferma primanota"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PNDATREG = ctod('  /  /  ')
  o_PNDATREG = ctod('  /  /  ')
  w_READAZI = space(5)
  w_PNDATDOC = ctod('  /  /  ')
  o_PNDATDOC = ctod('  /  /  ')
  w_PNVALNAZ = space(3)
  w_CODESE = space(4)
  w_VALNAZ = space(3)
  w_PNCOMIVA = ctod('  /  /  ')
  w_PNCOMPET = space(4)
  o_PNCOMPET = space(4)
  w_PNNUMDOC = 0
  w_PNALFDOC = space(10)
  w_PNNUMPRO = 0
  w_PNALFPRO = space(10)
  w_FLPPRO = space(1)
  w_PNANNDOC = space(4)
  w_PNPRD = space(2)
  w_PNANNPRO = space(4)
  w_FLPDOC = space(1)
  w_STAMPATO = space(1)
  w_PNSERIAL = space(10)
  o_PNSERIAL = space(10)
  w_PNPRP = space(2)
  w_PNTIPREG = space(1)
  w_FLCOMP = space(10)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kcpPag1","gscg_kcp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPNDATREG_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ESERCIZI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PNDATREG=ctod("  /  /  ")
      .w_READAZI=space(5)
      .w_PNDATDOC=ctod("  /  /  ")
      .w_PNVALNAZ=space(3)
      .w_CODESE=space(4)
      .w_VALNAZ=space(3)
      .w_PNCOMIVA=ctod("  /  /  ")
      .w_PNCOMPET=space(4)
      .w_PNNUMDOC=0
      .w_PNALFDOC=space(10)
      .w_PNNUMPRO=0
      .w_PNALFPRO=space(10)
      .w_FLPPRO=space(1)
      .w_PNANNDOC=space(4)
      .w_PNPRD=space(2)
      .w_PNANNPRO=space(4)
      .w_FLPDOC=space(1)
      .w_STAMPATO=space(1)
      .w_PNSERIAL=space(10)
      .w_PNPRP=space(2)
      .w_PNTIPREG=space(1)
      .w_FLCOMP=space(10)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_PNDATREG=oParentObject.w_PNDATREG
      .w_PNDATDOC=oParentObject.w_PNDATDOC
      .w_PNVALNAZ=oParentObject.w_PNVALNAZ
      .w_PNCOMIVA=oParentObject.w_PNCOMIVA
      .w_PNCOMPET=oParentObject.w_PNCOMPET
      .w_PNNUMDOC=oParentObject.w_PNNUMDOC
      .w_PNALFDOC=oParentObject.w_PNALFDOC
      .w_PNNUMPRO=oParentObject.w_PNNUMPRO
      .w_PNALFPRO=oParentObject.w_PNALFPRO
      .w_FLPPRO=oParentObject.w_FLPPRO
      .w_PNANNDOC=oParentObject.w_PNANNDOC
      .w_PNPRD=oParentObject.w_PNPRD
      .w_PNANNPRO=oParentObject.w_PNANNPRO
      .w_FLPDOC=oParentObject.w_FLPDOC
      .w_STAMPATO=oParentObject.w_STAMPATO
      .w_PNSERIAL=oParentObject.w_PNSERIAL
      .w_PNPRP=oParentObject.w_PNPRP
      .w_PNTIPREG=oParentObject.w_PNTIPREG
      .w_FLCOMP=oParentObject.w_FLCOMP
      .w_FINESE=oParentObject.w_FINESE
          .DoRTCalc(1,1,.f.)
        .w_READAZI = i_CODAZI
          .DoRTCalc(3,4,.f.)
        .w_CODESE = .w_PNCOMPET
        .DoRTCalc(6,8,.f.)
        if not(empty(.w_PNCOMPET))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,13,.f.)
        .w_PNANNDOC = CALPRO(IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC), .w_PNCOMPET, .w_FLPDOC)
          .DoRTCalc(15,15,.f.)
        .w_PNANNPRO = CALPRO(.w_PNDATREG, .w_PNCOMPET, .w_FLPPRO)
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
    endwith
    this.DoRTCalc(17,24,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PNDATREG=.w_PNDATREG
      .oParentObject.w_PNDATDOC=.w_PNDATDOC
      .oParentObject.w_PNVALNAZ=.w_PNVALNAZ
      .oParentObject.w_PNCOMIVA=.w_PNCOMIVA
      .oParentObject.w_PNCOMPET=.w_PNCOMPET
      .oParentObject.w_PNNUMDOC=.w_PNNUMDOC
      .oParentObject.w_PNALFDOC=.w_PNALFDOC
      .oParentObject.w_PNNUMPRO=.w_PNNUMPRO
      .oParentObject.w_PNALFPRO=.w_PNALFPRO
      .oParentObject.w_FLPPRO=.w_FLPPRO
      .oParentObject.w_PNANNDOC=.w_PNANNDOC
      .oParentObject.w_PNPRD=.w_PNPRD
      .oParentObject.w_PNANNPRO=.w_PNANNPRO
      .oParentObject.w_FLPDOC=.w_FLPDOC
      .oParentObject.w_STAMPATO=.w_STAMPATO
      .oParentObject.w_PNSERIAL=.w_PNSERIAL
      .oParentObject.w_PNPRP=.w_PNPRP
      .oParentObject.w_PNTIPREG=.w_PNTIPREG
      .oParentObject.w_FLCOMP=.w_FLCOMP
      .oParentObject.w_FINESE=.w_FINESE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_READAZI = i_CODAZI
        .DoRTCalc(3,4,.t.)
        if .o_PNSERIAL<>.w_PNSERIAL
            .w_CODESE = .w_PNCOMPET
        endif
        .DoRTCalc(6,6,.t.)
        if .o_PNDATREG<>.w_PNDATREG
            .w_PNCOMIVA = .w_PNDATREG
        endif
        .DoRTCalc(8,13,.t.)
        if .o_PNDATDOC<>.w_PNDATDOC.or. .o_PNDATREG<>.w_PNDATREG.or. .o_PNCOMPET<>.w_PNCOMPET
            .w_PNANNDOC = CALPRO(IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC), .w_PNCOMPET, .w_FLPDOC)
        endif
        .DoRTCalc(15,15,.t.)
        if .o_PNDATREG<>.w_PNDATREG.or. .o_PNCOMPET<>.w_PNCOMPET
            .w_PNANNPRO = CALPRO(.w_PNDATREG, .w_PNCOMPET, .w_FLPPRO)
        endif
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPNCOMPET_1_8.enabled = this.oPgFrm.Page1.oPag.oPNCOMPET_1_8.mCond()
    this.oPgFrm.Page1.oPag.oPNNUMDOC_1_9.enabled = this.oPgFrm.Page1.oPag.oPNNUMDOC_1_9.mCond()
    this.oPgFrm.Page1.oPag.oPNALFDOC_1_10.enabled = this.oPgFrm.Page1.oPag.oPNALFDOC_1_10.mCond()
    this.oPgFrm.Page1.oPag.oPNNUMPRO_1_11.enabled = this.oPgFrm.Page1.oPag.oPNNUMPRO_1_11.mCond()
    this.oPgFrm.Page1.oPag.oPNALFPRO_1_12.enabled = this.oPgFrm.Page1.oPag.oPNALFPRO_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPNNUMPRO_1_11.visible=!this.oPgFrm.Page1.oPag.oPNNUMPRO_1_11.mHide()
    this.oPgFrm.Page1.oPag.oPNALFPRO_1_12.visible=!this.oPgFrm.Page1.oPag.oPNALFPRO_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PNCOMPET
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCOMPET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_PNCOMPET)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_READAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_READAZI;
                     ,'ESCODESE',trim(this.w_PNCOMPET))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PNCOMPET)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PNCOMPET) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oPNCOMPET_1_8'),i_cWhere,'',"ELENCO ESERCIZI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_READAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Esercizio non ammesso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_READAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCOMPET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_PNCOMPET);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_READAZI;
                       ,'ESCODESE',this.w_PNCOMPET)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCOMPET = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PNCOMPET = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_VALNAZ =.w_PNVALNAZ
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Esercizio non ammesso")
        endif
        this.w_PNCOMPET = space(4)
        this.w_INIESE = ctod("  /  /  ")
        this.w_FINESE = ctod("  /  /  ")
        this.w_VALNAZ = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCOMPET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPNDATREG_1_1.value==this.w_PNDATREG)
      this.oPgFrm.Page1.oPag.oPNDATREG_1_1.value=this.w_PNDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPNDATDOC_1_3.value==this.w_PNDATDOC)
      this.oPgFrm.Page1.oPag.oPNDATDOC_1_3.value=this.w_PNDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCOMIVA_1_7.value==this.w_PNCOMIVA)
      this.oPgFrm.Page1.oPag.oPNCOMIVA_1_7.value=this.w_PNCOMIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCOMPET_1_8.value==this.w_PNCOMPET)
      this.oPgFrm.Page1.oPag.oPNCOMPET_1_8.value=this.w_PNCOMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oPNNUMDOC_1_9.value==this.w_PNNUMDOC)
      this.oPgFrm.Page1.oPag.oPNNUMDOC_1_9.value=this.w_PNNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPNALFDOC_1_10.value==this.w_PNALFDOC)
      this.oPgFrm.Page1.oPag.oPNALFDOC_1_10.value=this.w_PNALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPNNUMPRO_1_11.value==this.w_PNNUMPRO)
      this.oPgFrm.Page1.oPag.oPNNUMPRO_1_11.value=this.w_PNNUMPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPNALFPRO_1_12.value==this.w_PNALFPRO)
      this.oPgFrm.Page1.oPag.oPNALFPRO_1_12.value=this.w_PNALFPRO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_VALNAZ =.w_PNVALNAZ)  and (.w_FLCOMP='S' AND .w_STAMPATO<>'S')  and not(empty(.w_PNCOMPET))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPNCOMPET_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Esercizio non ammesso")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_kcp
      * --- Check incrociati sui Control
      * --- Impostati qui per agevolare la navigazione sulla maschera
      Do Case
        Case Not CHPNTDOC(.w_PNDATREG, .w_PNDATDOC)
      		i_bnoChk = .f.
      		i_bRes = .f.		
      	Case .w_PNDATREG<.w_PNDATDOC
      		i_bnoChk = .f.
      		i_bRes = .f.
      		i_cErrorMsg = ah_MsgFormat("Data documento superiore alla data registrazione")		
      	
        Case Not(.w_PNCOMIVA<=.w_PNDATREG OR .w_PNTIPREG $ " N")
          i_bnoChk = .f.
      		i_bRes = .f.
      		i_cErrorMsg = ah_MsgFormat("Data competenza IVA incongrutente o superiore alla data di registrazione")	
      endcase
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PNDATREG = this.w_PNDATREG
    this.o_PNDATDOC = this.w_PNDATDOC
    this.o_PNCOMPET = this.w_PNCOMPET
    this.o_PNSERIAL = this.w_PNSERIAL
    return

enddefine

* --- Define pages as container
define class tgscg_kcpPag1 as StdContainer
  Width  = 384
  height = 233
  stdWidth  = 384
  stdheight = 233
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPNDATREG_1_1 as StdField with uid="FOIGMXGVGJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PNDATREG", cQueryName = "PNDATREG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "data registrazione",;
    HelpContextID = 26963517,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=164, Top=16

  add object oPNDATDOC_1_3 as StdField with uid="JRCHELXLEX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PNDATDOC", cQueryName = "PNDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento",;
    HelpContextID = 207917511,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=164, Top=44

  add object oPNCOMIVA_1_7 as StdField with uid="RHEUOMFRWL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PNCOMIVA", cQueryName = "PNCOMIVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data competenza IVA",;
    HelpContextID = 137977399,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=164, Top=72

  add object oPNCOMPET_1_8 as StdField with uid="GZAMCPJLKC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PNCOMPET", cQueryName = "PNCOMPET",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Esercizio non ammesso",;
    ToolTipText = "Esercizio di competenza contabile della registrazione (imposta i saldi)",;
    HelpContextID = 255417930,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=164, Top=100, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_READAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_PNCOMPET"

  func oPNCOMPET_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLCOMP='S' AND .w_STAMPATO<>'S')
    endwith
   endif
  endfunc

  func oPNCOMPET_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPNCOMPET_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPNCOMPET_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_READAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_READAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oPNCOMPET_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"ELENCO ESERCIZI",'',this.parent.oContained
  endproc

  add object oPNNUMDOC_1_9 as StdField with uid="KSKQRDNYNC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PNNUMDOC", cQueryName = "PNNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento da confermare",;
    HelpContextID = 213905863,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=164, Top=127, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oPNNUMDOC_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAMPATO<>'S' AND .w_FLPDOC<>'N')
    endwith
   endif
  endfunc

  add object oPNALFDOC_1_10 as StdField with uid="PERYBOHTMB",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PNALFDOC", cQueryName = "PNALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documenti da confermare",;
    HelpContextID = 221888967,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=294, Top=129, InputMask=replicate('X',10)

  func oPNALFDOC_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAMPATO<>'S' AND .w_FLPDOC<>'N')
    endwith
   endif
  endfunc

  add object oPNNUMPRO_1_11 as StdField with uid="CVZYUGGARN",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PNNUMPRO", cQueryName = "PNNUMPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero protocollo da confermare",;
    HelpContextID = 12579259,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=164, Top=156, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oPNNUMPRO_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAMPATO<>'S' AND NOT .w_FLPPRO $ ' N')
    endwith
   endif
  endfunc

  func oPNNUMPRO_1_11.mHide()
    with this.Parent.oContained
      return (.w_FLPPRO $ ' N')
    endwith
  endfunc

  add object oPNALFPRO_1_12 as StdField with uid="QPBKWRMFJI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PNALFPRO", cQueryName = "PNALFPRO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie protocollo da confermare",;
    HelpContextID = 20562363,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=294, Top=156, InputMask=replicate('X',10)

  func oPNALFPRO_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STAMPATO<>'S' AND NOT .w_FLPPRO $ ' N')
    endwith
   endif
  endfunc

  func oPNALFPRO_1_12.mHide()
    with this.Parent.oContained
      return (.w_FLPPRO $ ' N')
    endwith
  endfunc


  add object oObj_1_24 as cp_runprogram with uid="CTSTFEMAJP",left=6, top=275, width=239,height=19,;
    caption='GSCG_BMS(C)',;
   bGlobalFont=.t.,;
    prg="GSCG_BMS('C')",;
    cEvent = "Update end",;
    nPag=1;
    , ToolTipText = "Conferma primanota";
    , HelpContextID = 229361607


  add object oObj_1_26 as cp_runprogram with uid="FDRBHBLJYW",left=6, top=301, width=241,height=19,;
    caption='GSCG_BMS(P)',;
   bGlobalFont=.t.,;
    prg="GSCG_BMS('P')",;
    cEvent = "w_PNALFPRO Changed,w_PNCOMPET Changed",;
    nPag=1;
    , ToolTipText = "aggiorna progressivo protocollo";
    , HelpContextID = 229358279


  add object oObj_1_27 as cp_runprogram with uid="FQWCNVUKLM",left=6, top=327, width=243,height=19,;
    caption='GSCG_BMS(D)',;
   bGlobalFont=.t.,;
    prg="GSCG_BMS('D')",;
    cEvent = "w_PNALFDOC Changed,w_PNCOMPET Changed",;
    nPag=1;
    , ToolTipText = "aggiorna progressivo protocollo";
    , HelpContextID = 229361351


  add object oBtn_1_36 as StdButton with uid="XNWYCGISAT",left=280, top=183, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 169233126;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_37 as StdButton with uid="JCFFQZETBZ",left=329, top=183, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176521798;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_13 as StdString with uid="EXKTVGEXZR",Visible=.t., Left=282, Top=131,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="OFJCSBIUDO",Visible=.t., Left=281, Top=159,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_FLPPRO $ ' N')
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="VJZYILQUBP",Visible=.t., Left=9, Top=129,;
    Alignment=1, Width=153, Height=18,;
    Caption="Primo numero documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="MWNUUBEACK",Visible=.t., Left=15, Top=157,;
    Alignment=1, Width=147, Height=18,;
    Caption="Primo numero protocollo:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_FLPPRO $ ' N')
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="NRDFGQHLOH",Visible=.t., Left=15, Top=16,;
    Alignment=1, Width=147, Height=18,;
    Caption="Data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="ZJLVTDSAZI",Visible=.t., Left=15, Top=45,;
    Alignment=1, Width=147, Height=18,;
    Caption="Data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="QEAUSDUDYP",Visible=.t., Left=15, Top=74,;
    Alignment=1, Width=147, Height=18,;
    Caption="Data competenza IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="CRICHDMPKP",Visible=.t., Left=95, Top=104,;
    Alignment=1, Width=67, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kcp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
