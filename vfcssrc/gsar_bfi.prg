* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bfi                                                        *
*              Puntamento tabelle collegate                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-06-21                                                      *
* Last revis.: 2016-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODSTR,pCODENT,pCODTAB,pLOG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bfi",oParentObject,m.pCODSTR,m.pCODENT,m.pCODTAB,m.pLOG)
return(i_retval)

define class tgsar_bfi as StdBatch
  * --- Local variables
  pCODSTR = space(10)
  pCODENT = space(15)
  pCODTAB = space(30)
  pLOG = .NULL.
  w_LINK = space(254)
  * --- WorkFile variables
  ENT_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Costruisce stringa elemento EDI in base al formato 
    * --- Codice struttura
    * --- Entit�
    * --- Tabella rieterata
    if Not Empty(this.pCODTAB) AND Used("ENTITA")
      Select ENTITA 
 GO top 
 Scan
      if Not EMpty(Nvl(ENTITA.ENFILTRO, " "))
        L_Err=.f. 
 L_OLDERROR=On("Error") 
 On Error L_Err=.t. 
 Select (Alltrim(ENTITA.EN_TABLE)) 
 Go top 
 Filtro="locate for " + Nvl(ENTITA.ENFILTRO, " ") 
 &Filtro 
 on error &L_OldError 
        if L_Err
          this.pLOG.AddMsgLogPartNoTrans(space(4),"Errore nell'applicazione della condizione di filtro %1 della tabella %2 con messaggio di errore: %3",Alltrim(ENTITA.ENFILTRO),Alltrim(ENTITA.EN_TABLE),Message())     
        endif
      endif
      endscan
    endif
  endproc


  proc Init(oParentObject,pCODSTR,pCODENT,pCODTAB,pLOG)
    this.pCODSTR=pCODSTR
    this.pCODENT=pCODENT
    this.pCODTAB=pCODTAB
    this.pLOG=pLOG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ENT_DETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODSTR,pCODENT,pCODTAB,pLOG"
endproc
