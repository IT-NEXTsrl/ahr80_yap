* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1bsk                                                        *
*              Recupera informazioni skype per menu                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-31                                                      *
* Last revis.: 2010-06-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFIELDNAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut1bsk",oParentObject,m.pFIELDNAM)
return(i_retval)

define class tgsut1bsk as StdBatch
  * --- Local variables
  pFIELDNAM = space(20)
  w_RETVAL = space(50)
  w_RET_CURS = space(50)
  w_OLD_AREA = space(50)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pFIELDNAM = Nome del campo da leggere
    *     Restituisce stringa vuota in caso d'errori oppure il contenuto del campo da leggere
    *     N.B. = Non � stato sfruttato un parametro contenente il nome della tabella da leggere
    *     in quanto questa routine viene utilizzata nel men� che ha un limite molto basso 
    *     nel numero di caratteri utilizzabili
    this.w_RETVAL = ""
    this.w_RET_CURS = ""
    do case
      case LEFT(ALLTRIM(this.pFIELDNAM),2)=="NO"
        DIMENSION L_ArrWhere(1,2)
        L_ArrWhere(1,1) = "NOCODICE"
        L_ArrWhere(1,2) = g_oMenu.GetByIndexKeyValue(1)
        this.w_RET_CURS = readtable("OFF_NOMI", this.pFIELDNAM, @L_ArrWhere)
        release L_ArrWhere
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case LEFT(ALLTRIM(this.pFIELDNAM),2)=="AN"
        DIMENSION L_ArrWhere(2,2)
        L_ArrWhere(1,1) = "ANTIPCON"
        L_ArrWhere(1,2) = g_oMenu.GetByIndexKeyValue(1)
        L_ArrWhere(2,1) = "ANCODICE"
        L_ArrWhere(2,2) = g_oMenu.GetByIndexKeyValue(2)
        this.w_RET_CURS = readtable("CONTI", this.pFIELDNAM, @L_ArrWhere)
        release L_ArrWhere
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case LEFT(ALLTRIM(this.pFIELDNAM),2)=="NC"
        DIMENSION L_ArrWhere(2,2)
        L_ArrWhere(1,1) = "NCCODICE"
        L_ArrWhere(1,2) = g_oMenu.GetByIndexKeyValue(1)
        L_ArrWhere(2,1) = "NCCODCON"
        L_ArrWhere(2,2) = g_oMenu.GetByIndexKeyValue(2)
        this.w_RET_CURS = readtable("NOM_CONT", this.pFIELDNAM, @L_ArrWhere)
        release L_ArrWhere
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    i_retcode = 'stop'
    i_retval = ALLTRIM(NVL(this.w_RETVAL,""))
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if !EMPTY(this.w_RET_CURS)
      this.w_OLD_AREA = SELECT()
      SELECT (this.w_RET_CURS)
      GO TOP
      private L_MACROFLD
      L_MACROFLD = (this.pFIELDNAM)
      this.w_RETVAL = &L_MACROFLD
      release L_MACROFLD
      USE IN SELECT(this.w_RET_CURS)
      SELECT (this.w_OLD_AREA)
    endif
  endproc


  proc Init(oParentObject,pFIELDNAM)
    this.pFIELDNAM=pFIELDNAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFIELDNAM"
endproc
