* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bil                                                        *
*              Controlli quantita/prezzo M.M.                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_27]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-12                                                      *
* Last revis.: 2002-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bil",oParentObject,m.pTipOpe)
return(i_retval)

define class tgsma_bil as StdBatch
  * --- Local variables
  pTipOpe = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli al Cambio Prezzo sulle Righe (da GSMA_MVM Evento: w_MMPREZZO)
    if this.pTipOpe="1"
      * --- Variato Prezzo su Prima U.M.
      this.oParentObject.w_MMPREZZO = IIF(this.oParentObject.w_MMQTAMOV=0, 0, cp_ROUND((this.oParentObject.w_PREUM1 * this.oParentObject.w_MMQTAUM1) / this.oParentObject.w_MMQTAMOV, this.oParentObject.w_DECUNI))
    else
      this.oParentObject.w_PREUM1 = IIF(this.oParentObject.w_MMQTAUM1=0, 0, cp_ROUND((this.oParentObject.w_MMPREZZO * this.oParentObject.w_MMQTAMOV) / this.oParentObject.w_MMQTAUM1, this.oParentObject.w_DECUNI))
    endif
    * --- Non esegue la mCalc
    this.bUpdateParentObject = .F.
  endproc


  proc Init(oParentObject,pTipOpe)
    this.pTipOpe=pTipOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOpe"
endproc
