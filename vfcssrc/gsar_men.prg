* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_men                                                        *
*              Entit�                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_145]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-13                                                      *
* Last revis.: 2017-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsar_men
PARAMETERS pTipGes
* --- Fine Area Manuale
return(createobject("tgsar_men"))

* --- Class definition
define class tgsar_men as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 745
  Height = 488+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-07-27"
  HelpContextID=204900503
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  ENT_MAST_IDX = 0
  ENT_DETT_IDX = 0
  XDC_TABLE_IDX = 0
  cFile = "ENT_MAST"
  cFileDetail = "ENT_DETT"
  cKeySelect = "ENCODICE"
  cKeyWhere  = "ENCODICE=this.w_ENCODICE"
  cKeyDetail  = "ENCODICE=this.w_ENCODICE and EN_TABLE=this.w_EN_TABLE"
  cKeyWhereODBC = '"ENCODICE="+cp_ToStrODBC(this.w_ENCODICE)';

  cKeyDetailWhereODBC = '"ENCODICE="+cp_ToStrODBC(this.w_ENCODICE)';
      +'+" and EN_TABLE="+cp_ToStrODBC(this.w_EN_TABLE)';

  cKeyWhereODBCqualified = '"ENT_DETT.ENCODICE="+cp_ToStrODBC(this.w_ENCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ENT_DETT.CPROWORD'
  cPrg = "gsar_men"
  cComment = "Entit�"
  i_nRowNum = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ENTIPENT = space(1)
  w_ENCODICE = space(15)
  w_ENDESCRI = space(50)
  w_ENVALINT = space(1)
  w_ENVALCAM = space(30)
  w_ENBATCHK = space(50)
  w_ENBATPRE = space(50)
  w_ENBATPOS = space(50)
  w_ENORDDEL = space(1)
  w_ENTIPORD = space(1)
  w_ENORDINE = space(254)
  w_EN_TABLE = space(30)
  o_EN_TABLE = space(30)
  w_CPROWORD = 0
  w_ENPRINCI = space(1)
  o_ENPRINCI = space(1)
  w_ENUPDATE = space(1)
  w_ENNEEDFL = space(1)
  w_EN__LINK = space(254)
  w_ENFILTRO = space(254)
  w_ENAUTONU = space(15)
  o_ENAUTONU = space(15)
  w_ENGENCLO = space(1)
  w_ENVALEXP = space(254)
  w_DESCOLL = space(60)
  w_ENTABCOL = space(30)
  w_destab = space(60)
  w_TABPRINC = space(30)
  o_TABPRINC = space(30)
  w_RET = .F.
  w_EN__NOTE = space(0)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar_men
  * ---- Parametro Gestione
   * ---- L: Entit� Logistica remota
   * ---- V: Entit� Vendite funzioni avanzate
  pTipGes=' '
  * Gestione controlli alla conferma fuori transazione
  w_RESCHK=0
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ENT_MAST','gsar_men')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_menPag1","gsar_men",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Entit�")
      .Pages(1).HelpContextID = 65485638
      .Pages(2).addobject("oPag","tgsar_menPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Note aggiuntive")
      .Pages(2).HelpContextID = 55666483
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oENCODICE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsar_men
    * --- Imposta il Titolo della Finestra della Gestione in Base al tipo
    WITH THIS.PARENT
       IF Type('pTipGes')='U' Or EMPTY(pTipGes)
          .pTipges = 'L'
       ELSE
          .pTipges = pTipges
       ENDIF
    
       DO CASE
             CASE .pTipGes = 'L'
             .cAutoZoom = 'GSARLMEN'
             CASE .pTipGes = 'V'
             .cAutoZoom = 'GSARVMEN'
       ENDCASE
    ENDWITH
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='XDC_TABLE'
    this.cWorkTables[2]='ENT_MAST'
    this.cWorkTables[3]='ENT_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ENT_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ENT_MAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_ENCODICE = NVL(ENCODICE,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_13_joined
    link_2_13_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from ENT_MAST where ENCODICE=KeySet.ENCODICE
    *
    i_nConn = i_TableProp[this.ENT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2],this.bLoadRecFilter,this.ENT_MAST_IDX,"gsar_men")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ENT_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ENT_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"ENT_DETT.","ENT_MAST.")
      i_cTable = i_cTable+' ENT_MAST '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ENCODICE',this.w_ENCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_TABPRINC = space(30)
        .w_RET = .f.
        .w_ENTIPENT = NVL(ENTIPENT,space(1))
        .w_ENCODICE = NVL(ENCODICE,space(15))
        .w_ENDESCRI = NVL(ENDESCRI,space(50))
        .w_ENVALINT = NVL(ENVALINT,space(1))
        .w_ENVALCAM = NVL(ENVALCAM,space(30))
        .w_ENBATCHK = NVL(ENBATCHK,space(50))
        .w_ENBATPRE = NVL(ENBATPRE,space(50))
        .w_ENBATPOS = NVL(ENBATPOS,space(50))
        .w_ENORDDEL = NVL(ENORDDEL,space(1))
        .w_ENTIPORD = NVL(ENTIPORD,space(1))
        .w_ENORDINE = NVL(ENORDINE,space(254))
        .w_EN__NOTE = NVL(EN__NOTE,space(0))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ENT_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from ENT_DETT where ENCODICE=KeySet.ENCODICE
      *                            and EN_TABLE=KeySet.EN_TABLE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.ENT_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('ENT_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "ENT_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" ENT_DETT"
        link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'ENCODICE',this.w_ENCODICE  )
        select * from (i_cTable) ENT_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCOLL = space(60)
          .w_destab = space(60)
          .w_EN_TABLE = NVL(EN_TABLE,space(30))
          if link_2_1_joined
            this.w_EN_TABLE = NVL(TBNAME201,NVL(this.w_EN_TABLE,space(30)))
            this.w_DESTAB = NVL(TBCOMMENT201,space(60))
          else
          .link_2_1('Load')
          endif
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_ENPRINCI = NVL(ENPRINCI,space(1))
          .w_ENUPDATE = NVL(ENUPDATE,space(1))
          .w_ENNEEDFL = NVL(ENNEEDFL,space(1))
          .w_EN__LINK = NVL(EN__LINK,space(254))
          .w_ENFILTRO = NVL(ENFILTRO,space(254))
          .w_ENAUTONU = NVL(ENAUTONU,space(15))
          .w_ENGENCLO = NVL(ENGENCLO,space(1))
          .w_ENVALEXP = NVL(ENVALEXP,space(254))
          .w_ENTABCOL = NVL(ENTABCOL,space(30))
          if link_2_13_joined
            this.w_ENTABCOL = NVL(TBNAME213,NVL(this.w_ENTABCOL,space(30)))
            this.w_DESCOLL = NVL(TBCOMMENT213,space(60))
          else
          .link_2_13('Load')
          endif
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace EN_TABLE with .w_EN_TABLE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_7.enabled = .oPgFrm.Page1.oPag.oBtn_2_7.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_26.enabled = .oPgFrm.Page1.oPag.oBtn_1_26.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_31.enabled = .oPgFrm.Page1.oPag.oBtn_1_31.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_ENTIPENT=space(1)
      .w_ENCODICE=space(15)
      .w_ENDESCRI=space(50)
      .w_ENVALINT=space(1)
      .w_ENVALCAM=space(30)
      .w_ENBATCHK=space(50)
      .w_ENBATPRE=space(50)
      .w_ENBATPOS=space(50)
      .w_ENORDDEL=space(1)
      .w_ENTIPORD=space(1)
      .w_ENORDINE=space(254)
      .w_EN_TABLE=space(30)
      .w_CPROWORD=10
      .w_ENPRINCI=space(1)
      .w_ENUPDATE=space(1)
      .w_ENNEEDFL=space(1)
      .w_EN__LINK=space(254)
      .w_ENFILTRO=space(254)
      .w_ENAUTONU=space(15)
      .w_ENGENCLO=space(1)
      .w_ENVALEXP=space(254)
      .w_DESCOLL=space(60)
      .w_ENTABCOL=space(30)
      .w_destab=space(60)
      .w_TABPRINC=space(30)
      .w_RET=.f.
      .w_EN__NOTE=space(0)
      if .cFunction<>"Filter"
        .w_ENTIPENT = this.pTipGes
        .DoRTCalc(2,3,.f.)
        .w_ENVALINT = 'N'
        .DoRTCalc(5,8,.f.)
        .w_ENORDDEL = 'N'
        .w_ENTIPORD = 'N'
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_EN_TABLE))
         .link_2_1('Full')
        endif
        .DoRTCalc(13,13,.f.)
        .w_ENPRINCI = 'N'
        .w_ENUPDATE = 'S'
        .w_ENNEEDFL = 'N'
        .DoRTCalc(17,19,.f.)
        .w_ENGENCLO = 'N'
        .w_ENVALEXP = IIF( .w_ENPRINCI='N' or .w_ENVALINT='S' ,Space(254),IIF(Not Empty(.w_ENAUTONU),'1=1',IIF(Empty(.w_ENVALEXP),'1=0',.w_ENVALEXP)))
        .DoRTCalc(22,22,.f.)
        .w_ENTABCOL = ''
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_ENTABCOL))
         .link_2_13('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ENT_MAST')
    this.DoRTCalc(24,27,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_7.enabled = this.oPgFrm.Page1.oPag.oBtn_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oENCODICE_1_2.enabled = i_bVal
      .Page1.oPag.oENDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oENVALINT_1_4.enabled = i_bVal
      .Page1.oPag.oENVALCAM_1_5.enabled = i_bVal
      .Page1.oPag.oENBATCHK_1_6.enabled = i_bVal
      .Page1.oPag.oENBATPRE_1_7.enabled = i_bVal
      .Page1.oPag.oENBATPOS_1_8.enabled = i_bVal
      .Page1.oPag.oENORDDEL_1_9.enabled = i_bVal
      .Page1.oPag.oENTIPORD_1_10.enabled = i_bVal
      .Page1.oPag.oENORDINE_1_11.enabled = i_bVal
      .Page1.oPag.oEN__LINK_2_6.enabled = i_bVal
      .Page1.oPag.oENFILTRO_2_8.enabled = i_bVal
      .Page1.oPag.oENAUTONU_2_9.enabled = i_bVal
      .Page1.oPag.oENGENCLO_2_10.enabled = i_bVal
      .Page1.oPag.oENVALEXP_2_11.enabled = i_bVal
      .Page1.oPag.oENTABCOL_2_13.enabled = i_bVal
      .Page2.oPag.oEN__NOTE_4_1.enabled = i_bVal
      .Page1.oPag.oBtn_2_7.enabled = .Page1.oPag.oBtn_2_7.mCond()
      .Page1.oPag.oBtn_1_26.enabled = .Page1.oPag.oBtn_1_26.mCond()
      .Page1.oPag.oBtn_1_31.enabled = .Page1.oPag.oBtn_1_31.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oENCODICE_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oENCODICE_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ENT_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ENT_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ENTIPENT,"ENTIPENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ENCODICE,"ENCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ENDESCRI,"ENDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ENVALINT,"ENVALINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ENVALCAM,"ENVALCAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ENBATCHK,"ENBATCHK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ENBATPRE,"ENBATPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ENBATPOS,"ENBATPOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ENORDDEL,"ENORDDEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ENTIPORD,"ENTIPORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ENORDINE,"ENORDINE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EN__NOTE,"EN__NOTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ENT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])
    i_lTable = "ENT_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ENT_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        GSAR_BF2(this,this.w_ENTIPENT,"GSAR_SEN")
      endwith
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_EN_TABLE C(30);
      ,t_CPROWORD N(5);
      ,t_ENPRINCI N(3);
      ,t_ENUPDATE N(3);
      ,t_ENNEEDFL N(3);
      ,t_EN__LINK C(254);
      ,t_ENFILTRO C(254);
      ,t_ENAUTONU C(15);
      ,t_ENGENCLO N(3);
      ,t_ENVALEXP C(254);
      ,t_DESCOLL C(60);
      ,t_ENTABCOL C(30);
      ,t_destab C(60);
      ,EN_TABLE C(30);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_menbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oEN_TABLE_2_1.controlsource=this.cTrsName+'.t_EN_TABLE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oENPRINCI_2_3.controlsource=this.cTrsName+'.t_ENPRINCI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oENUPDATE_2_4.controlsource=this.cTrsName+'.t_ENUPDATE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oENNEEDFL_2_5.controlsource=this.cTrsName+'.t_ENNEEDFL'
    this.oPgFRm.Page1.oPag.oEN__LINK_2_6.controlsource=this.cTrsName+'.t_EN__LINK'
    this.oPgFRm.Page1.oPag.oENFILTRO_2_8.controlsource=this.cTrsName+'.t_ENFILTRO'
    this.oPgFRm.Page1.oPag.oENAUTONU_2_9.controlsource=this.cTrsName+'.t_ENAUTONU'
    this.oPgFRm.Page1.oPag.oENGENCLO_2_10.controlsource=this.cTrsName+'.t_ENGENCLO'
    this.oPgFRm.Page1.oPag.oENVALEXP_2_11.controlsource=this.cTrsName+'.t_ENVALEXP'
    this.oPgFRm.Page1.oPag.oDESCOLL_2_12.controlsource=this.cTrsName+'.t_DESCOLL'
    this.oPgFRm.Page1.oPag.oENTABCOL_2_13.controlsource=this.cTrsName+'.t_ENTABCOL'
    this.oPgFRm.Page1.oPag.odestab_2_17.controlsource=this.cTrsName+'.t_destab'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(94)
    this.AddVLine(319)
    this.AddVLine(460)
    this.AddVLine(606)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEN_TABLE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ENT_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ENT_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ENT_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'ENT_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(ENTIPENT,ENCODICE,ENDESCRI,ENVALINT,ENVALCAM"+;
                  ",ENBATCHK,ENBATPRE,ENBATPOS,ENORDDEL,ENTIPORD"+;
                  ",ENORDINE,EN__NOTE"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_ENTIPENT)+;
                    ","+cp_ToStrODBC(this.w_ENCODICE)+;
                    ","+cp_ToStrODBC(this.w_ENDESCRI)+;
                    ","+cp_ToStrODBC(this.w_ENVALINT)+;
                    ","+cp_ToStrODBC(this.w_ENVALCAM)+;
                    ","+cp_ToStrODBC(this.w_ENBATCHK)+;
                    ","+cp_ToStrODBC(this.w_ENBATPRE)+;
                    ","+cp_ToStrODBC(this.w_ENBATPOS)+;
                    ","+cp_ToStrODBC(this.w_ENORDDEL)+;
                    ","+cp_ToStrODBC(this.w_ENTIPORD)+;
                    ","+cp_ToStrODBC(this.w_ENORDINE)+;
                    ","+cp_ToStrODBC(this.w_EN__NOTE)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ENT_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'ENT_MAST')
        cp_CheckDeletedKey(i_cTable,0,'ENCODICE',this.w_ENCODICE)
        INSERT INTO (i_cTable);
              (ENTIPENT,ENCODICE,ENDESCRI,ENVALINT,ENVALCAM,ENBATCHK,ENBATPRE,ENBATPOS,ENORDDEL,ENTIPORD,ENORDINE,EN__NOTE &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_ENTIPENT;
                  ,this.w_ENCODICE;
                  ,this.w_ENDESCRI;
                  ,this.w_ENVALINT;
                  ,this.w_ENVALCAM;
                  ,this.w_ENBATCHK;
                  ,this.w_ENBATPRE;
                  ,this.w_ENBATPOS;
                  ,this.w_ENORDDEL;
                  ,this.w_ENTIPORD;
                  ,this.w_ENORDINE;
                  ,this.w_EN__NOTE;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ENT_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])
      *
      * insert into ENT_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(ENCODICE,EN_TABLE,CPROWORD,ENPRINCI,ENUPDATE"+;
                  ",ENNEEDFL,EN__LINK,ENFILTRO,ENAUTONU,ENGENCLO"+;
                  ",ENVALEXP,ENTABCOL,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_ENCODICE)+","+cp_ToStrODBCNull(this.w_EN_TABLE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_ENPRINCI)+","+cp_ToStrODBC(this.w_ENUPDATE)+;
             ","+cp_ToStrODBC(this.w_ENNEEDFL)+","+cp_ToStrODBC(this.w_EN__LINK)+","+cp_ToStrODBC(this.w_ENFILTRO)+","+cp_ToStrODBC(this.w_ENAUTONU)+","+cp_ToStrODBC(this.w_ENGENCLO)+;
             ","+cp_ToStrODBC(this.w_ENVALEXP)+","+cp_ToStrODBCNull(this.w_ENTABCOL)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'ENCODICE',this.w_ENCODICE,'EN_TABLE',this.w_EN_TABLE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_ENCODICE,this.w_EN_TABLE,this.w_CPROWORD,this.w_ENPRINCI,this.w_ENUPDATE"+;
                ",this.w_ENNEEDFL,this.w_EN__LINK,this.w_ENFILTRO,this.w_ENAUTONU,this.w_ENGENCLO"+;
                ",this.w_ENVALEXP,this.w_ENTABCOL,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.ENT_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update ENT_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'ENT_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " ENTIPENT="+cp_ToStrODBC(this.w_ENTIPENT)+;
             ",ENDESCRI="+cp_ToStrODBC(this.w_ENDESCRI)+;
             ",ENVALINT="+cp_ToStrODBC(this.w_ENVALINT)+;
             ",ENVALCAM="+cp_ToStrODBC(this.w_ENVALCAM)+;
             ",ENBATCHK="+cp_ToStrODBC(this.w_ENBATCHK)+;
             ",ENBATPRE="+cp_ToStrODBC(this.w_ENBATPRE)+;
             ",ENBATPOS="+cp_ToStrODBC(this.w_ENBATPOS)+;
             ",ENORDDEL="+cp_ToStrODBC(this.w_ENORDDEL)+;
             ",ENTIPORD="+cp_ToStrODBC(this.w_ENTIPORD)+;
             ",ENORDINE="+cp_ToStrODBC(this.w_ENORDINE)+;
             ",EN__NOTE="+cp_ToStrODBC(this.w_EN__NOTE)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'ENT_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'ENCODICE',this.w_ENCODICE  )
          UPDATE (i_cTable) SET;
              ENTIPENT=this.w_ENTIPENT;
             ,ENDESCRI=this.w_ENDESCRI;
             ,ENVALINT=this.w_ENVALINT;
             ,ENVALCAM=this.w_ENVALCAM;
             ,ENBATCHK=this.w_ENBATCHK;
             ,ENBATPRE=this.w_ENBATPRE;
             ,ENBATPOS=this.w_ENBATPOS;
             ,ENORDDEL=this.w_ENORDDEL;
             ,ENTIPORD=this.w_ENTIPORD;
             ,ENORDINE=this.w_ENORDINE;
             ,EN__NOTE=this.w_EN__NOTE;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_EN_TABLE))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.ENT_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from ENT_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and EN_TABLE="+cp_ToStrODBC(&i_TN.->EN_TABLE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and EN_TABLE=&i_TN.->EN_TABLE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace EN_TABLE with this.w_EN_TABLE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ENT_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",ENPRINCI="+cp_ToStrODBC(this.w_ENPRINCI)+;
                     ",ENUPDATE="+cp_ToStrODBC(this.w_ENUPDATE)+;
                     ",ENNEEDFL="+cp_ToStrODBC(this.w_ENNEEDFL)+;
                     ",EN__LINK="+cp_ToStrODBC(this.w_EN__LINK)+;
                     ",ENFILTRO="+cp_ToStrODBC(this.w_ENFILTRO)+;
                     ",ENAUTONU="+cp_ToStrODBC(this.w_ENAUTONU)+;
                     ",ENGENCLO="+cp_ToStrODBC(this.w_ENGENCLO)+;
                     ",ENVALEXP="+cp_ToStrODBC(this.w_ENVALEXP)+;
                     ",ENTABCOL="+cp_ToStrODBCNull(this.w_ENTABCOL)+;
                     " ,EN_TABLE="+cp_ToStrODBC(this.w_EN_TABLE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and EN_TABLE="+cp_ToStrODBC(EN_TABLE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,ENPRINCI=this.w_ENPRINCI;
                     ,ENUPDATE=this.w_ENUPDATE;
                     ,ENNEEDFL=this.w_ENNEEDFL;
                     ,EN__LINK=this.w_EN__LINK;
                     ,ENFILTRO=this.w_ENFILTRO;
                     ,ENAUTONU=this.w_ENAUTONU;
                     ,ENGENCLO=this.w_ENGENCLO;
                     ,ENVALEXP=this.w_ENVALEXP;
                     ,ENTABCOL=this.w_ENTABCOL;
                     ,EN_TABLE=this.w_EN_TABLE;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and EN_TABLE=&i_TN.->EN_TABLE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_EN_TABLE))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.ENT_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])
        *
        * delete ENT_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and EN_TABLE="+cp_ToStrODBC(&i_TN.->EN_TABLE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and EN_TABLE=&i_TN.->EN_TABLE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.ENT_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])
        *
        * delete ENT_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_EN_TABLE))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ENT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_MAST_IDX,2])
    if i_bUpd
      with this
          .w_ENTIPENT = this.pTipGes
        .DoRTCalc(2,20,.t.)
        if .o_ENPRINCI<>.w_ENPRINCI.or. .o_ENAUTONU<>.w_ENAUTONU
          .w_ENVALEXP = IIF( .w_ENPRINCI='N' or .w_ENVALINT='S' ,Space(254),IIF(Not Empty(.w_ENAUTONU),'1=1',IIF(Empty(.w_ENVALEXP),'1=0',.w_ENVALEXP)))
        endif
        .DoRTCalc(22,22,.t.)
        if .o_EN_TABLE<>.w_EN_TABLE
          .w_ENTABCOL = ''
          .link_2_13('Full')
        endif
        if .o_ENPRINCI<>.w_ENPRINCI.or. .o_TABPRINC<>.w_TABPRINC
          .Calculate_QSMQLYELFA()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        this.Calculate_QSMQLYELFA()
      endwith
      this.DoRTCalc(24,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_AVAVRQZTTQ()
    with this
          * --- Cambio file principale
          .w_ENNEEDFL = iif (.w_ENPRINCI='S' , 'S' , .w_ENNEEDFL)
          .w_ENUPDATE = iif (.w_ENPRINCI='S' , 'S' , .w_ENUPDATE )
          .w_ENFILTRO = ''
          .w_EN__LINK = ''
    endwith
  endproc
  proc Calculate_CAVVHEVGMW()
    with this
          * --- Controllo tabella principale GSAR_BEN
          GSAR_BEN(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_VOUVMCIKJK()
    with this
          * --- Cambio tipo validazione
          .w_ENVALEXP = ''
    endwith
  endproc
  proc Calculate_PFFEOMYAEX()
    with this
          * --- Controlli alla checkform gslr_bce
          GSLR_BCE(this;
             )
    endwith
  endproc
  proc Calculate_LONQVFWPXN()
    with this
          * --- Calcola relazione
          .w_EN__LINK = FINDLINK(.w_EN_TABLE, .w_TABPRINC , 0,  .T.)
    endwith
  endproc
  proc Calculate_ZFBDOOQQJQ()
    with this
          * --- Calcola g_cprefiprog
          .w_Ret = iif(g_LORE='S',CALPROGSED(),.F.)
    endwith
  endproc
  proc Calculate_YKMUUJBBYT()
    with this
          * --- GSAR_BEN(L) - ControlliFinali
          gsar_ben(this;
              ,'L';
             )
    endwith
  endproc
  proc Calculate_QSMQLYELFA()
    with this
          * --- Calcola autonumber cablati
          .w_ENAUTONU = IIF( this.o_ENPRINCI<>.w_ENPRINCI or this.o_TABPRINC<>.w_TABPRINC ,  IIF(.w_ENPRINCI='N','',IIF(.w_TABPRINC='DOC_MAST','SEDOC',IIF(.w_TABPRINC='MVM_MAST','SEMVM',IIF(.w_TABPRINC='COR_RISP','SERPLU','')))),  .w_ENAUTONU )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oENUPDATE_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oENUPDATE_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oENNEEDFL_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oENNEEDFL_2_5.mCond()
    this.oPgFrm.Page1.oPag.oEN__LINK_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oEN__LINK_2_6.mCond()
    this.oPgFrm.Page1.oPag.oENVALEXP_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oENVALEXP_2_11.mCond()
    this.oPgFrm.Page1.oPag.oENTABCOL_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oENTABCOL_2_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_7.enabled =this.oPgFrm.Page1.oPag.oBtn_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oENVALINT_1_4.visible=!this.oPgFrm.Page1.oPag.oENVALINT_1_4.mHide()
    this.oPgFrm.Page1.oPag.oENVALCAM_1_5.visible=!this.oPgFrm.Page1.oPag.oENVALCAM_1_5.mHide()
    this.oPgFrm.Page1.oPag.oENBATCHK_1_6.visible=!this.oPgFrm.Page1.oPag.oENBATCHK_1_6.mHide()
    this.oPgFrm.Page1.oPag.oENBATPRE_1_7.visible=!this.oPgFrm.Page1.oPag.oENBATPRE_1_7.mHide()
    this.oPgFrm.Page1.oPag.oENBATPOS_1_8.visible=!this.oPgFrm.Page1.oPag.oENBATPOS_1_8.mHide()
    this.oPgFrm.Page1.oPag.oENORDDEL_1_9.visible=!this.oPgFrm.Page1.oPag.oENORDDEL_1_9.mHide()
    this.oPgFrm.Page1.oPag.oENTIPORD_1_10.visible=!this.oPgFrm.Page1.oPag.oENTIPORD_1_10.mHide()
    this.oPgFrm.Page1.oPag.oENORDINE_1_11.visible=!this.oPgFrm.Page1.oPag.oENORDINE_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_31.visible=!this.oPgFrm.Page1.oPag.oBtn_1_31.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oEN__LINK_2_6.visible=!this.oPgFrm.Page1.oPag.oEN__LINK_2_6.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_7.visible=!this.oPgFrm.Page1.oPag.oBtn_2_7.mHide()
    this.oPgFrm.Page1.oPag.oENFILTRO_2_8.visible=!this.oPgFrm.Page1.oPag.oENFILTRO_2_8.mHide()
    this.oPgFrm.Page1.oPag.oENAUTONU_2_9.visible=!this.oPgFrm.Page1.oPag.oENAUTONU_2_9.mHide()
    this.oPgFrm.Page1.oPag.oENGENCLO_2_10.visible=!this.oPgFrm.Page1.oPag.oENGENCLO_2_10.mHide()
    this.oPgFrm.Page1.oPag.oENVALEXP_2_11.visible=!this.oPgFrm.Page1.oPag.oENVALEXP_2_11.mHide()
    this.oPgFrm.Page1.oPag.oDESCOLL_2_12.visible=!this.oPgFrm.Page1.oPag.oDESCOLL_2_12.mHide()
    this.oPgFrm.Page1.oPag.oENTABCOL_2_13.visible=!this.oPgFrm.Page1.oPag.oENTABCOL_2_13.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_ENPRINCI Changed")
          .Calculate_AVAVRQZTTQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load") or lower(cEvent)==lower("w_CPROWORD Changed") or lower(cEvent)==lower("w_EN_TABLE Changed") or lower(cEvent)==lower("w_ENPRINCI Changed")
          .Calculate_CAVVHEVGMW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ENVALINT Changed")
          .Calculate_VOUVMCIKJK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CheckPrinc")
          .Calculate_PFFEOMYAEX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_EN_TABLE Changed") or lower(cEvent)==lower("w_ENPRINCI Changed")
          .Calculate_LONQVFWPXN()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Done")
          .Calculate_ZFBDOOQQJQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ControlliFinali")
          .Calculate_YKMUUJBBYT()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=EN_TABLE
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EN_TABLE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_EN_TABLE)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_EN_TABLE))
          select TBNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EN_TABLE)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EN_TABLE) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oEN_TABLE_2_1'),i_cWhere,'',"Archivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EN_TABLE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_EN_TABLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_EN_TABLE)
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EN_TABLE = NVL(_Link_.TBNAME,space(30))
      this.w_DESTAB = NVL(_Link_.TBCOMMENT,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_EN_TABLE = space(30)
      endif
      this.w_DESTAB = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKNFILE(.w_EN_TABLE,'C')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_EN_TABLE = space(30)
        this.w_DESTAB = space(60)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EN_TABLE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.XDC_TABLE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.TBNAME as TBNAME201"+ ",link_2_1.TBCOMMENT as TBCOMMENT201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on ENT_DETT.EN_TABLE=link_2_1.TBNAME"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and ENT_DETT.EN_TABLE=link_2_1.TBNAME(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ENTABCOL
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ENTABCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_ENTABCOL)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_ENTABCOL))
          select TBNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ENTABCOL)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ENTABCOL) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oENTABCOL_2_13'),i_cWhere,'',"Archivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ENTABCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_ENTABCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_ENTABCOL)
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ENTABCOL = NVL(_Link_.TBNAME,space(30))
      this.w_DESCOLL = NVL(_Link_.TBCOMMENT,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ENTABCOL = space(30)
      endif
      this.w_DESCOLL = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ENTABCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.XDC_TABLE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.TBNAME as TBNAME213"+ ",link_2_13.TBCOMMENT as TBCOMMENT213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on ENT_DETT.ENTABCOL=link_2_13.TBNAME"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and ENT_DETT.ENTABCOL=link_2_13.TBNAME(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oENCODICE_1_2.value==this.w_ENCODICE)
      this.oPgFrm.Page1.oPag.oENCODICE_1_2.value=this.w_ENCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oENDESCRI_1_3.value==this.w_ENDESCRI)
      this.oPgFrm.Page1.oPag.oENDESCRI_1_3.value=this.w_ENDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oENVALINT_1_4.RadioValue()==this.w_ENVALINT)
      this.oPgFrm.Page1.oPag.oENVALINT_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oENVALCAM_1_5.value==this.w_ENVALCAM)
      this.oPgFrm.Page1.oPag.oENVALCAM_1_5.value=this.w_ENVALCAM
    endif
    if not(this.oPgFrm.Page1.oPag.oENBATCHK_1_6.value==this.w_ENBATCHK)
      this.oPgFrm.Page1.oPag.oENBATCHK_1_6.value=this.w_ENBATCHK
    endif
    if not(this.oPgFrm.Page1.oPag.oENBATPRE_1_7.value==this.w_ENBATPRE)
      this.oPgFrm.Page1.oPag.oENBATPRE_1_7.value=this.w_ENBATPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oENBATPOS_1_8.value==this.w_ENBATPOS)
      this.oPgFrm.Page1.oPag.oENBATPOS_1_8.value=this.w_ENBATPOS
    endif
    if not(this.oPgFrm.Page1.oPag.oENORDDEL_1_9.RadioValue()==this.w_ENORDDEL)
      this.oPgFrm.Page1.oPag.oENORDDEL_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oENTIPORD_1_10.RadioValue()==this.w_ENTIPORD)
      this.oPgFrm.Page1.oPag.oENTIPORD_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oENORDINE_1_11.value==this.w_ENORDINE)
      this.oPgFrm.Page1.oPag.oENORDINE_1_11.value=this.w_ENORDINE
    endif
    if not(this.oPgFrm.Page1.oPag.oEN__LINK_2_6.value==this.w_EN__LINK)
      this.oPgFrm.Page1.oPag.oEN__LINK_2_6.value=this.w_EN__LINK
      replace t_EN__LINK with this.oPgFrm.Page1.oPag.oEN__LINK_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oENFILTRO_2_8.value==this.w_ENFILTRO)
      this.oPgFrm.Page1.oPag.oENFILTRO_2_8.value=this.w_ENFILTRO
      replace t_ENFILTRO with this.oPgFrm.Page1.oPag.oENFILTRO_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oENAUTONU_2_9.value==this.w_ENAUTONU)
      this.oPgFrm.Page1.oPag.oENAUTONU_2_9.value=this.w_ENAUTONU
      replace t_ENAUTONU with this.oPgFrm.Page1.oPag.oENAUTONU_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oENGENCLO_2_10.RadioValue()==this.w_ENGENCLO)
      this.oPgFrm.Page1.oPag.oENGENCLO_2_10.SetRadio()
      replace t_ENGENCLO with this.oPgFrm.Page1.oPag.oENGENCLO_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oENVALEXP_2_11.value==this.w_ENVALEXP)
      this.oPgFrm.Page1.oPag.oENVALEXP_2_11.value=this.w_ENVALEXP
      replace t_ENVALEXP with this.oPgFrm.Page1.oPag.oENVALEXP_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOLL_2_12.value==this.w_DESCOLL)
      this.oPgFrm.Page1.oPag.oDESCOLL_2_12.value=this.w_DESCOLL
      replace t_DESCOLL with this.oPgFrm.Page1.oPag.oDESCOLL_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oENTABCOL_2_13.value==this.w_ENTABCOL)
      this.oPgFrm.Page1.oPag.oENTABCOL_2_13.value=this.w_ENTABCOL
      replace t_ENTABCOL with this.oPgFrm.Page1.oPag.oENTABCOL_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.odestab_2_17.value==this.w_destab)
      this.oPgFrm.Page1.oPag.odestab_2_17.value=this.w_destab
      replace t_destab with this.oPgFrm.Page1.oPag.odestab_2_17.value
    endif
    if not(this.oPgFrm.Page2.oPag.oEN__NOTE_4_1.value==this.w_EN__NOTE)
      this.oPgFrm.Page2.oPag.oEN__NOTE_4_1.value=this.w_EN__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEN_TABLE_2_1.value==this.w_EN_TABLE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEN_TABLE_2_1.value=this.w_EN_TABLE
      replace t_EN_TABLE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEN_TABLE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENPRINCI_2_3.RadioValue()==this.w_ENPRINCI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENPRINCI_2_3.SetRadio()
      replace t_ENPRINCI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENPRINCI_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENUPDATE_2_4.RadioValue()==this.w_ENUPDATE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENUPDATE_2_4.SetRadio()
      replace t_ENUPDATE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENUPDATE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENNEEDFL_2_5.RadioValue()==this.w_ENNEEDFL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENNEEDFL_2_5.SetRadio()
      replace t_ENNEEDFL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENNEEDFL_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'ENT_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ENCODICE) or not(CHKNFILE(.w_ENCODICE,'C')))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oENCODICE_1_2.SetFocus()
            i_bnoObbl = !empty(.w_ENCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ENVALCAM))  and not(.w_ENVALINT='N' OR .w_ENTIPENT='V')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oENVALCAM_1_5.SetFocus()
            i_bnoObbl = !empty(.w_ENVALCAM)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_men
      * --- Con Logistica remota controllo che la tabella principale sia super entit�
      If g_LORE='S' AND .w_ENTIPENT='L'
         This.NotifyEvent('CheckPrinc')
      Endif
      If g_VEFA='S' AND .w_ENTIPENT='V'
         This.NotifyEvent('ControlliFinali')
      Endif
      if This.w_RESCHK<>0
      	i_bRes=.f.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (.cTrsName);
       where not(deleted()) and (not(Empty(t_EN_TABLE)));
        into cursor __chk__
    if not(1<=cnt)
      do cp_ErrorMsg with cp_MsgFormat(MSG_NEEDED_AT_LEAST__ROWS,"1"),"","",.F.
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not(IIF(.w_ENPRINCI='S' and g_LORE='S',CHECKEXP( .w_EN_TABLE , .w_ENFILTRO )  , .T. ) AND IIF(( .w_ENPRINCI='S' Or .w_ENVALINT='N' ) and g_LORE='S' , CHECKEXP( .w_EN_TABLE , .w_ENVALEXP ,  .T. ), .T. ))
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("L'espressione contiene campi non presenti ne nella chiave ne nell'eventuale super entit� definita sull'archivio.")
        case   not(CHKNFILE(.w_EN_TABLE,'C')) and not(empty(.w_EN_TABLE)) and (not(Empty(.w_EN_TABLE)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oEN_TABLE_2_1
          i_bRes = .f.
          i_bnoChk = .f.
        case   empty(.w_ENVALEXP) and (Not( .w_ENPRINCI='N' or .w_ENVALINT='S' ) And .w_ENTIPENT='L') and (not(Empty(.w_EN_TABLE)))
          .oNewFocus=.oPgFrm.Page1.oPag.oENVALEXP_2_11
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_EN_TABLE))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_EN_TABLE = this.w_EN_TABLE
    this.o_ENPRINCI = this.w_ENPRINCI
    this.o_ENAUTONU = this.w_ENAUTONU
    this.o_TABPRINC = this.w_TABPRINC
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_EN_TABLE)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_EN_TABLE=space(30)
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_ENPRINCI=space(1)
      .w_ENUPDATE=space(1)
      .w_ENNEEDFL=space(1)
      .w_EN__LINK=space(254)
      .w_ENFILTRO=space(254)
      .w_ENAUTONU=space(15)
      .w_ENGENCLO=space(1)
      .w_ENVALEXP=space(254)
      .w_DESCOLL=space(60)
      .w_ENTABCOL=space(30)
      .w_destab=space(60)
      .DoRTCalc(1,12,.f.)
      if not(empty(.w_EN_TABLE))
        .link_2_1('Full')
      endif
      .DoRTCalc(13,13,.f.)
        .w_ENPRINCI = 'N'
        .w_ENUPDATE = 'S'
        .w_ENNEEDFL = 'N'
      .DoRTCalc(17,19,.f.)
        .w_ENGENCLO = 'N'
        .w_ENVALEXP = IIF( .w_ENPRINCI='N' or .w_ENVALINT='S' ,Space(254),IIF(Not Empty(.w_ENAUTONU),'1=1',IIF(Empty(.w_ENVALEXP),'1=0',.w_ENVALEXP)))
      .DoRTCalc(22,22,.f.)
        .w_ENTABCOL = ''
      .DoRTCalc(23,23,.f.)
      if not(empty(.w_ENTABCOL))
        .link_2_13('Full')
      endif
    endwith
    this.DoRTCalc(24,27,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_EN_TABLE = t_EN_TABLE
    this.w_CPROWORD = t_CPROWORD
    this.w_ENPRINCI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENPRINCI_2_3.RadioValue(.t.)
    this.w_ENUPDATE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENUPDATE_2_4.RadioValue(.t.)
    this.w_ENNEEDFL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENNEEDFL_2_5.RadioValue(.t.)
    this.w_EN__LINK = t_EN__LINK
    this.w_ENFILTRO = t_ENFILTRO
    this.w_ENAUTONU = t_ENAUTONU
    this.w_ENGENCLO = this.oPgFrm.Page1.oPag.oENGENCLO_2_10.RadioValue(.t.)
    this.w_ENVALEXP = t_ENVALEXP
    this.w_DESCOLL = t_DESCOLL
    this.w_ENTABCOL = t_ENTABCOL
    this.w_destab = t_destab
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_EN_TABLE with this.w_EN_TABLE
    replace t_CPROWORD with this.w_CPROWORD
    replace t_ENPRINCI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENPRINCI_2_3.ToRadio()
    replace t_ENUPDATE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENUPDATE_2_4.ToRadio()
    replace t_ENNEEDFL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oENNEEDFL_2_5.ToRadio()
    replace t_EN__LINK with this.w_EN__LINK
    replace t_ENFILTRO with this.w_ENFILTRO
    replace t_ENAUTONU with this.w_ENAUTONU
    replace t_ENGENCLO with this.oPgFrm.Page1.oPag.oENGENCLO_2_10.ToRadio()
    replace t_ENVALEXP with this.w_ENVALEXP
    replace t_DESCOLL with this.w_DESCOLL
    replace t_ENTABCOL with this.w_ENTABCOL
    replace t_destab with this.w_destab
    if i_srv='A'
      replace EN_TABLE with this.w_EN_TABLE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_menPag1 as StdContainer
  Width  = 741
  height = 488
  stdWidth  = 741
  stdheight = 488
  resizeXpos=389
  resizeYpos=281
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oENCODICE_1_2 as StdField with uid="XNOHKQNSDK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ENCODICE", cQueryName = "ENCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 104199285,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=166, Top=5, InputMask=replicate('X',15)

  func oENCODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKNFILE(.w_ENCODICE,'C'))
    endwith
    return bRes
  endfunc

  add object oENDESCRI_1_3 as StdField with uid="SUWWKAZYKJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ENDESCRI", cQueryName = "ENDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione entit�",;
    HelpContextID = 189785201,;
   bGlobalFont=.t.,;
    Height=21, Width=407, Left=311, Top=5, InputMask=replicate('X',50)


  add object oENVALINT_1_4 as StdCombo with uid="OXWMONWXOR",rtseq=4,rtrep=.f.,left=166,top=33,width=139,height=21;
    , ToolTipText = "Se intrinseca chi crea il dato ne � automaticamente il validatore. (esempio. vendita negozio, movimenti di magazzino)";
    , HelpContextID = 171785114;
    , cFormVar="w_ENVALINT",RowSource=""+"con espressione,"+"intrinseca", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oENVALINT_1_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ENVALINT,&i_cF..t_ENVALINT),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oENVALINT_1_4.GetRadio()
    this.Parent.oContained.w_ENVALINT = this.RadioValue()
    return .t.
  endfunc

  func oENVALINT_1_4.ToRadio()
    this.Parent.oContained.w_ENVALINT=trim(this.Parent.oContained.w_ENVALINT)
    return(;
      iif(this.Parent.oContained.w_ENVALINT=='N',1,;
      iif(this.Parent.oContained.w_ENVALINT=='S',2,;
      0)))
  endfunc

  func oENVALINT_1_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oENVALINT_1_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENTIPENT='V')
    endwith
    endif
  endfunc

  add object oENVALCAM_1_5 as StdField with uid="TQLVTIGIOW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ENVALCAM", cQueryName = "ENVALCAM",;
    bObbl = .t. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Campo dell'archivio principale dal quale saranno estratti i primi due caratteri identificativi della sede di validazione (no alias)",;
    HelpContextID = 197313645,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=349, Top=33, InputMask=replicate('X',30)

  func oENVALCAM_1_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENVALINT='N' OR .w_ENTIPENT='V')
    endwith
    endif
  endfunc

  add object oENBATCHK_1_6 as StdField with uid="AWIDPYBRNE",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ENBATCHK", cQueryName = "ENBATCHK",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Routine eseguita in fase di sincronizzazione/importazione per effettuare i controlli prima della modifica/cancellazione e subito dopo l'inserimento/modifica",;
    HelpContextID = 79428497,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=166, Top=61, InputMask=replicate('X',50)

  func oENBATCHK_1_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_APPLICATION='ad hoc ENTERPRISE' and .w_ENTIPENT='V'))
    endwith
    endif
  endfunc

  add object oENBATPRE_1_7 as StdField with uid="XARUYIOCVB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ENBATPRE", cQueryName = "ENBATPRE",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Routine eseguita prima della sincronizzazione/importazione per aggiornamento tabelle collegate (Es.: aggiornamento saldi - storno vecchia qt� in modifica)",;
    HelpContextID = 239338613,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=166, Top=89, InputMask=replicate('X',50)

  func oENBATPRE_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_APPLICATION='ad hoc ENTERPRISE' and .w_ENTIPENT='V'))
    endwith
    endif
  endfunc

  add object oENBATPOS_1_8 as StdField with uid="STBRFZLGZS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ENBATPOS", cQueryName = "ENBATPOS",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Routine eseguita dopo la sincronizzazione/importazione per aggiornamento tabelle collegate (Es.: aggiornamento saldi - aggiornamento nuova qt� in modifica)",;
    HelpContextID = 239338599,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=166, Top=116, InputMask=replicate('X',50)

  func oENBATPOS_1_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_APPLICATION='ad hoc ENTERPRISE' and .w_ENTIPENT='V'))
    endwith
    endif
  endfunc


  add object oENORDDEL_1_9 as StdCombo with uid="TIBQEWRITH",rtseq=9,rtrep=.f.,left=166,top=146,width=139,height=21;
    , ToolTipText = "Ordine di elaborazione dei record relativi alla tabella principale in fase di sincronizzazione";
    , HelpContextID = 187839598;
    , cFormVar="w_ENORDDEL",RowSource=""+"Nessuno,"+"Cancellati in testa,"+"Cancellati in coda", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oENORDDEL_1_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ENORDDEL,&i_cF..t_ENORDDEL),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'T',;
    iif(xVal =3,'C',;
    space(1)))))
  endfunc
  func oENORDDEL_1_9.GetRadio()
    this.Parent.oContained.w_ENORDDEL = this.RadioValue()
    return .t.
  endfunc

  func oENORDDEL_1_9.ToRadio()
    this.Parent.oContained.w_ENORDDEL=trim(this.Parent.oContained.w_ENORDDEL)
    return(;
      iif(this.Parent.oContained.w_ENORDDEL=='N',1,;
      iif(this.Parent.oContained.w_ENORDDEL=='T',2,;
      iif(this.Parent.oContained.w_ENORDDEL=='C',3,;
      0))))
  endfunc

  func oENORDDEL_1_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oENORDDEL_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENTIPENT='V')
    endwith
    endif
  endfunc


  add object oENTIPORD_1_10 as StdCombo with uid="EVMNGBEGKL",rtseq=10,rtrep=.f.,left=580,top=146,width=139,height=21;
    , ToolTipText = "Indica se l'ordinamento libero del pacchetto deve essere considerato come una espressione o come un ordinamento diretto su un campo. Nel secondo caso si possono usare anche le indicazioni ASC o DESC";
    , HelpContextID = 259712118;
    , cFormVar="w_ENTIPORD",RowSource=""+"Nessuno,"+"Clausola order by,"+"Espressione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oENTIPORD_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ENTIPORD,&i_cF..t_ENTIPORD),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'O',;
    iif(xVal =3,'E',;
    space(1)))))
  endfunc
  func oENTIPORD_1_10.GetRadio()
    this.Parent.oContained.w_ENTIPORD = this.RadioValue()
    return .t.
  endfunc

  func oENTIPORD_1_10.ToRadio()
    this.Parent.oContained.w_ENTIPORD=trim(this.Parent.oContained.w_ENTIPORD)
    return(;
      iif(this.Parent.oContained.w_ENTIPORD=='N',1,;
      iif(this.Parent.oContained.w_ENTIPORD=='O',2,;
      iif(this.Parent.oContained.w_ENTIPORD=='E',3,;
      0))))
  endfunc

  func oENTIPORD_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oENTIPORD_1_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENTIPENT='V')
    endwith
    endif
  endfunc

  add object oENORDINE_1_11 as StdField with uid="MFROAYDMUR",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ENORDINE", cQueryName = "ENORDINE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Espressione di ordinamento che verr� accodata alla stessa dei cancellati in fase di sincronizzazione. Se espressione, il valore ritornato deve diventare stringa (usando funzioni del tipo: STR, DTOC)",;
    HelpContextID = 164481931,;
   bGlobalFont=.t.,;
    Height=21, Width=533, Left=166, Top=172, InputMask=replicate('X',254)

  func oENORDINE_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENTIPORD='N' or .w_ENTIPENT='V')
    endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="ORSWOUHUED",left=670, top=94, width=48,height=45,;
    CpPicture="BMP\TRACCIABILITA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la treeview delle tabelle che dipendono da quella principale";
    , HelpContextID = 46220780;
    , TabStop=.f.,Caption='\<Relazioni';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSUT1BCR(this.Parent.oContained,"Entita")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_TABPRINC) And Not Empty(.w_ENCODICE))
    endwith
  endfunc


  add object oBtn_1_31 as StdButton with uid="PZQVUJPZJF",left=702, top=172, width=20,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Verifica espressione, utilizzabile solo se definita la tabella principale dell'entit� e generate le tabelle di mirror";
    , HelpContextID = 205101526;
    , TabStop=.f.,Caption='...';
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        DO CHECKORD With .w_TABPRINC, .w_ENORDINE
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_TABPRINC))
    endwith
  endfunc

  func oBtn_1_31.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENTIPORD='N' or .w_ENTIPENT='V')
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=33, top=203, width=703,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1=" Ordine ",Field2="EN_TABLE",Label2=" Archivio ",Field3="ENPRINCI",Label3=" Principale ",Field4="ENUPDATE",Label4=" Tipo aggiornamento ",Field5="ENNEEDFL",Label5=" File ",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 15879558

  add object oStr_1_12 as StdString with uid="HNKHKKJTKU",Visible=.t., Left=7, Top=5,;
    Alignment=1, Width=156, Height=18,;
    Caption="Entit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="TYUZFIKQJI",Visible=.t., Left=3, Top=411,;
    Alignment=1, Width=102, Height=18,;
    Caption="Legame:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (.w_ENPRINCI='S')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="YDJAVJJEAG",Visible=.t., Left=7, Top=33,;
    Alignment=1, Width=156, Height=18,;
    Caption="Validazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_ENTIPENT='V')
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="UUAKWIPVEF",Visible=.t., Left=535, Top=441,;
    Alignment=1, Width=70, Height=18,;
    Caption="Autonumber:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_ENTIPENT='V')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="FHCVNHRFKI",Visible=.t., Left=3, Top=438,;
    Alignment=1, Width=102, Height=18,;
    Caption="Filtro:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_ENPRINCI='N' AND .w_ENTIPENT='L')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="QYCDOSFDBH",Visible=.t., Left=3, Top=466,;
    Alignment=1, Width=102, Height=18,;
    Caption="Validazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_ENPRINCI='N' or .w_ENVALINT='S' or .w_ENTIPENT='V')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="PISGWMSWHN",Visible=.t., Left=3, Top=384,;
    Alignment=1, Width=102, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="RWRWLUHEUV",Visible=.t., Left=310, Top=33,;
    Alignment=1, Width=37, Height=18,;
    Caption="su:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_ENVALINT='N'  OR .w_ENTIPENT='V')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="ONTNYFXFWC",Visible=.t., Left=7, Top=89,;
    Alignment=1, Width=156, Height=18,;
    Caption="Presincronizzazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_ENTIPENT='V')
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="JZPEVOTKUF",Visible=.t., Left=7, Top=116,;
    Alignment=1, Width=156, Height=18,;
    Caption="Postsincronizzazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (.w_ENTIPENT='V')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="TDWXUJNGXQ",Visible=.t., Left=7, Top=146,;
    Alignment=1, Width=156, Height=18,;
    Caption="Ordine dei cancellati:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_ENTIPENT='V')
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="KPCSYMPMOD",Visible=.t., Left=7, Top=173,;
    Alignment=1, Width=156, Height=18,;
    Caption="Ordinamento libero:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_ENTIPORD='N' or .w_ENTIPENT='V')
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="GIPDTQAETP",Visible=.t., Left=334, Top=146,;
    Alignment=1, Width=243, Height=17,;
    Caption="Tipo di ordinamento libero:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (.w_ENTIPENT='V')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="MGBFAPJPJA",Visible=.t., Left=7, Top=62,;
    Alignment=1, Width=156, Height=18,;
    Caption="Controlli:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return ((g_APPLICATION='ad hoc ENTERPRISE' and .w_ENTIPENT='V'))
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="SINAWMODBT",Visible=.t., Left=3, Top=466,;
    Alignment=1, Width=102, Height=18,;
    Caption="Collegata:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (.w_ENTIPENT='L')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="WXZTGKKLNS",Visible=.t., Left=7, Top=89,;
    Alignment=1, Width=156, Height=18,;
    Caption="Preimportazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (.w_ENTIPENT='L' or g_APPLICATION='ad hoc ENTERPRISE')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="HSFZHSTKNS",Visible=.t., Left=7, Top=116,;
    Alignment=1, Width=156, Height=18,;
    Caption="Postimportazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_ENTIPENT='L' or  g_APPLICATION='ad hoc ENTERPRISE')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=25,top=222,;
    width=697+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=26,top=223,width=696+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='XDC_TABLE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oEN__LINK_2_6.Refresh()
      this.Parent.oENFILTRO_2_8.Refresh()
      this.Parent.oENAUTONU_2_9.Refresh()
      this.Parent.oENGENCLO_2_10.Refresh()
      this.Parent.oENVALEXP_2_11.Refresh()
      this.Parent.oDESCOLL_2_12.Refresh()
      this.Parent.oENTABCOL_2_13.Refresh()
      this.Parent.odestab_2_17.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='XDC_TABLE'
        oDropInto=this.oBodyCol.oRow.oEN_TABLE_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oEN__LINK_2_6 as StdTrsField with uid="ISBUWLHAFC",rtseq=17,rtrep=.t.,;
    cFormVar="w_EN__LINK",value=space(254),;
    ToolTipText = "Espressione di link da utilizzare per identificare i dettagli dell'archivio principale",;
    HelpContextID = 173788049,;
    cTotal="", bFixedPos=.t., cQueryName = "EN__LINK",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=600, Left=107, Top=411, InputMask=replicate('X',254), bHasZoom = .t. 

  func oEN__LINK_2_6.mCond()
    with this.Parent.oContained
      return (.w_ENPRINCI<>'S')
    endwith
  endfunc

  func oEN__LINK_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENPRINCI='S')
    endwith
    endif
  endfunc

  proc oEN__LINK_2_6.mZoom
    this.parent.oContained.w_EN__LINK=FINDLINK(this.parent.oContained.w_EN_TABLE, this.parent.oContained.w_TABPRINC, 0)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oBtn_2_7 as StdButton with uid="VJAJKUQUTW",width=17,height=22,;
   left=710, top=411,;
    caption="...", nPag=2;
    , ToolTipText = "Verifica legame";
    , HelpContextID = 205101526;
    , TabStop=.f.,Caption='...';
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_7.Click()
      with this.Parent.oContained
        GSAR_BEN(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_7.mCond()
    with this.Parent.oContained
      return ((NOT EMPTY(.w_EN__LINK) or NOT EMPTY(.w_ENFILTRO)) and cp_ExistTableDef( .w_EN_TABLE))
    endwith
  endfunc

  func oBtn_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENPRINCI='S' OR .w_ENTIPENT='L')
    endwith
   endif
  endfunc

  add object oENFILTRO_2_8 as StdTrsField with uid="PTBMACJILT",rtseq=18,rtrep=.t.,;
    cFormVar="w_ENFILTRO",value=space(254),;
    HelpContextID = 180077675,;
    cTotal="", bFixedPos=.t., cQueryName = "ENFILTRO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=424, Left=107, Top=438, InputMask=replicate('X',254)

  func oENFILTRO_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENPRINCI='N' AND .w_ENTIPENT='L')
    endwith
    endif
  endfunc

  func oENFILTRO_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !( .w_ENTIPENT='V' OR CHECKEXP( .w_EN_TABLE , .w_ENFILTRO ,  .T. ))
         bRes=(cp_WarningMsg(thisform.msgFmt("L'espressione contiene campi non presenti ne nella chiave ne nell'eventuale super entit� definita sull'archivio.")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oENAUTONU_2_9 as StdTrsField with uid="VSUIHGSMSL",rtseq=19,rtrep=.t.,;
    cFormVar="w_ENAUTONU",value=space(15),;
    ToolTipText = "Codice autonumber per identificare la sede nel caso di movimentazioni",;
    HelpContextID = 254809189,;
    cTotal="", bFixedPos=.t., cQueryName = "ENAUTONU",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=608, Top=438, InputMask=replicate('X',15)

  func oENAUTONU_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENTIPENT='V')
    endwith
    endif
  endfunc

  add object oENGENCLO_2_10 as StdTrsCheck with uid="RVCGQZBFWF",rtrep=.t.,;
    cFormVar="w_ENGENCLO",  caption="Genera tabelle clone",;
    ToolTipText = "Se attivo genera tabella clone in vendite funzioni avanzate",;
    HelpContextID = 73419669,;
    Left=582, Top=438,;
    cTotal="", cQueryName = "ENGENCLO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oENGENCLO_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ENGENCLO,&i_cF..t_ENGENCLO),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oENGENCLO_2_10.GetRadio()
    this.Parent.oContained.w_ENGENCLO = this.RadioValue()
    return .t.
  endfunc

  func oENGENCLO_2_10.ToRadio()
    this.Parent.oContained.w_ENGENCLO=trim(this.Parent.oContained.w_ENGENCLO)
    return(;
      iif(this.Parent.oContained.w_ENGENCLO=='S',1,;
      0))
  endfunc

  func oENGENCLO_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oENGENCLO_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_VEFA<>'S' or .w_ENTIPENT='L' or UPPER(g_APPLICATION)<>'AD HOC REVOLUTION')
    endwith
    endif
  endfunc

  add object oENVALEXP_2_11 as StdTrsField with uid="LFFUGLWOCT",rtseq=21,rtrep=.t.,;
    cFormVar="w_ENVALEXP",value=space(254),;
    HelpContextID = 104676246,;
    cTotal="", bFixedPos=.t., cQueryName = "ENVALEXP",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=620, Left=107, Top=465, InputMask=replicate('X',254)

  func oENVALEXP_2_11.mCond()
    with this.Parent.oContained
      return (Not( .w_ENPRINCI='N' or .w_ENVALINT='S' ) And .w_ENTIPENT='L')
    endwith
  endfunc

  func oENVALEXP_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENPRINCI='N' or .w_ENVALINT='S' or .w_ENTIPENT='V')
    endwith
    endif
  endfunc

  func oENVALEXP_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if bRes and !(iif(g_lore='S',CHECKEXP( .w_EN_TABLE , .w_ENVALEXP ,  .T. ),.t.))
         bRes=(cp_WarningMsg(thisform.msgFmt("L'espressione contiene campi non presenti ne nella chiave ne nell'eventuale super entit� definita sull'archivio")))
         this.parent.oContained.bDontReportError=!bRes
      endif
    endwith
    return bRes
  endfunc

  add object oDESCOLL_2_12 as StdTrsField with uid="THJHBMVJJA",rtseq=22,rtrep=.t.,;
    cFormVar="w_DESCOLL",value=space(60),enabled=.f.,;
    HelpContextID = 225378870,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCOLL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=396, Left=331, Top=465, InputMask=replicate('X',60)

  func oDESCOLL_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENTIPENT='L')
    endwith
    endif
  endfunc

  add object oENTABCOL_2_13 as StdTrsField with uid="NSKFIHAZGT",rtseq=23,rtrep=.t.,;
    cFormVar="w_ENTABCOL",value=space(30),;
    ToolTipText = "Tabella obbligatoria nel caso di definizione del  legame di join",;
    HelpContextID = 207807598,;
    cTotal="", bFixedPos=.t., cQueryName = "ENTABCOL",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=107, Top=465, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_ENTABCOL"

  func oENTABCOL_2_13.mCond()
    with this.Parent.oContained
      return ( .w_ENTIPENT='V')
    endwith
  endfunc

  func oENTABCOL_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ENTIPENT='L')
    endwith
    endif
  endfunc

  func oENTABCOL_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oENTABCOL_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oENTABCOL_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oENTABCOL_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Archivi",'',this.parent.oContained
  endproc

  add object odestab_2_17 as StdTrsField with uid="OODNXSKZHG",rtseq=24,rtrep=.t.,;
    cFormVar="w_destab",value=space(60),enabled=.f.,;
    HelpContextID = 188603338,;
    cTotal="", bFixedPos=.t., cQueryName = "destab",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=620, Left=107, Top=384, InputMask=replicate('X',60)

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsar_menPag2 as StdContainer
    Width  = 741
    height = 488
    stdWidth  = 741
    stdheight = 488
  resizeXpos=371
  resizeYpos=292
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oEN__NOTE_4_1 as StdMemo with uid="UJEJHZPUXA",rtseq=27,rtrep=.f.,;
    cFormVar = "w_EN__NOTE", cQueryName = "EN__NOTE",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 260322421,;
   bGlobalFont=.t.,;
    Height=477, Width=690, Left=6, Top=9
enddefine

* --- Defining Body row
define class tgsar_menBodyRow as CPBodyRowCnt
  Width=687
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oEN_TABLE_2_1 as StdTrsField with uid="MSZKKUHDTT",rtseq=12,rtrep=.t.,;
    cFormVar="w_EN_TABLE",value=space(30),isprimarykey=.t.,;
    HelpContextID = 44092299,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=220, Left=59, Top=0, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_EN_TABLE"

  func oEN_TABLE_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oEN_TABLE_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oEN_TABLE_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oEN_TABLE_2_1.readonly and this.parent.oEN_TABLE_2_1.isprimarykey)
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oEN_TABLE_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Archivi",'',this.parent.oContained
   endif
  endproc

  add object oCPROWORD_2_2 as StdTrsField with uid="BMFCDMBJAB",rtseq=13,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 251986582,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=57, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], tabstop=.f.

  add object oENPRINCI_2_3 as StdTrsCombo with uid="FVIETWADXW",rtrep=.t.,;
    cFormVar="w_ENPRINCI", RowSource=""+"Collegata,"+"Principale" , ;
    ToolTipText = "Se attivo Identifica la tabella principale",;
    HelpContextID = 14820465,;
    Height=21, Width=134, Left=285, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oENPRINCI_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ENPRINCI,&i_cF..t_ENPRINCI),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oENPRINCI_2_3.GetRadio()
    this.Parent.oContained.w_ENPRINCI = this.RadioValue()
    return .t.
  endfunc

  func oENPRINCI_2_3.ToRadio()
    this.Parent.oContained.w_ENPRINCI=trim(this.Parent.oContained.w_ENPRINCI)
    return(;
      iif(this.Parent.oContained.w_ENPRINCI=='N',1,;
      iif(this.Parent.oContained.w_ENPRINCI=='S',2,;
      0)))
  endfunc

  func oENPRINCI_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oENUPDATE_2_4 as StdTrsCombo with uid="UTOYSURXAV",rtrep=.t.,;
    cFormVar="w_ENUPDATE", RowSource=""+"Modifica,"+"Cancella / Inserisce" , ;
    ToolTipText = "Se 'N'L'aggiornamento avviene tramite una cancellazione ed un inserimento",;
    HelpContextID = 238277749,;
    Height=21, Width=138, Left=427, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oENUPDATE_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ENUPDATE,&i_cF..t_ENUPDATE),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oENUPDATE_2_4.GetRadio()
    this.Parent.oContained.w_ENUPDATE = this.RadioValue()
    return .t.
  endfunc

  func oENUPDATE_2_4.ToRadio()
    this.Parent.oContained.w_ENUPDATE=trim(this.Parent.oContained.w_ENUPDATE)
    return(;
      iif(this.Parent.oContained.w_ENUPDATE=='S',1,;
      iif(this.Parent.oContained.w_ENUPDATE=='N',2,;
      0)))
  endfunc

  func oENUPDATE_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oENUPDATE_2_4.mCond()
    with this.Parent.oContained
      return (.w_ENPRINCI='N')
    endwith
  endfunc

  add object oENNEEDFL_2_5 as StdTrsCombo with uid="ZCGOVVRCWU",rtrep=.t.,;
    cFormVar="w_ENNEEDFL", RowSource=""+"Facoltativo,"+"Obbligatorio" , ;
    ToolTipText = "Se attivo allora per poter svolgere l'acquisizione deve esserci il file",;
    HelpContextID = 187647086,;
    Height=21, Width=108, Left=574, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oENNEEDFL_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ENNEEDFL,&i_cF..t_ENNEEDFL),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oENNEEDFL_2_5.GetRadio()
    this.Parent.oContained.w_ENNEEDFL = this.RadioValue()
    return .t.
  endfunc

  func oENNEEDFL_2_5.ToRadio()
    this.Parent.oContained.w_ENNEEDFL=trim(this.Parent.oContained.w_ENNEEDFL)
    return(;
      iif(this.Parent.oContained.w_ENNEEDFL=='N',1,;
      iif(this.Parent.oContained.w_ENNEEDFL=='S',2,;
      0)))
  endfunc

  func oENNEEDFL_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oENNEEDFL_2_5.mCond()
    with this.Parent.oContained
      return (.w_ENPRINCI='N')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oEN_TABLE_2_1.When()
    return(.t.)
  proc oEN_TABLE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oEN_TABLE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_men','ENT_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ENCODICE=ENT_MAST.ENCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
