* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bee                                                        *
*              generazione file CBI\SEPA                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_99]                                                    *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-01                                                      *
* Last revis.: 2015-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODSTR,pCODCON,pTIPCON,pCODRAG,pARRKEY,pHANDLE,pAZIONE,pLogErr
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bee",oParentObject,m.pCODSTR,m.pCODCON,m.pTIPCON,m.pCODRAG,m.pARRKEY,m.pHANDLE,m.pAZIONE,m.pLogErr)
return(i_retval)

define class tgsar_bee as StdBatch
  * --- Local variables
  pCODSTR = space(10)
  pCODCON = space(15)
  pTIPCON = space(1)
  pCODRAG = space(10)
  pARRKEY = space(10)
  pHANDLE = 0
  pAZIONE = space(1)
  pLogErr = .NULL.
  w_CODSTR = space(10)
  w_HANDLEFILE = 0
  w_CODELE = space(10)
  w_CODTAB = space(30)
  w_INDICE = 0
  w_PATHED = space(200)
  w_CODAZI = space(5)
  w_NOMFIL = space(200)
  w_CODENT = space(15)
  w_FLPRIN = space(1)
  w_TIPFIL = space(1)
  w_INDENT = 0
  w_LCODSTR = space(10)
  w_FLATAB = space(15)
  w_INDPROG = 0
  w_PROGRE = space(14)
  w_CODESE = space(5)
  w_oERRORLOG = .NULL.
  w_SERIAL = space(10)
  w_NUMDOC = 0
  w_NUMPRO = 0
  w_ALFDOC = space(15)
  w_ALFPRO = space(15)
  w_DATREG = ctod("  /  /  ")
  w_DATDOC = ctod("  /  /  ")
  w_POS = 0
  w_MASK = space(50)
  w_FILL = space(1)
  w_ERRODIR = .f.
  CODCUC = space(2)
  w_CREAIND = .f.
  * --- WorkFile variables
  CONTROPA_idx=0
  VASTRUTT_idx=0
  ENT_DETT_idx=0
  DOC_MAST_idx=0
  VAELEMEN_idx=0
  CONTI_idx=0
  VAVARNOM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguito da gestioni che creano file EDI
    *     1. GSVA_BGF
    * --- Codice struttura
    * --- Codice intestatario
    * --- Tipo intestatario
    * --- Codice raggruppamento
    * --- Array chiavi
    * --- riferimento file
    * --- Azione File
    *     U Creazione file unico multidato
    *     M Creazione multipla di file non multidato
    *     S Creazione singolo file non multidato
    * --- controllo per uscire da eventuli loop
    this.w_CODSTR = this.pCODSTR
    this.w_LCODSTR = this.pCODSTR
    this.w_CODAZI = i_CODAZI
    this.w_FLPRIN = "N"
    * --- Read from VASTRUTT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "STNOMFIL,STCODENT,STTIPFIL,STINDENT,STFLATAB"+;
        " from "+i_cTable+" VASTRUTT where ";
            +"STCODICE = "+cp_ToStrODBC(this.w_CODSTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        STNOMFIL,STCODENT,STTIPFIL,STINDENT,STFLATAB;
        from (i_cTable) where;
            STCODICE = this.w_CODSTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NOMFIL = NVL(cp_ToDate(_read_.STNOMFIL),cp_NullValue(_read_.STNOMFIL))
      this.w_CODENT = NVL(cp_ToDate(_read_.STCODENT),cp_NullValue(_read_.STCODENT))
      this.w_TIPFIL = NVL(cp_ToDate(_read_.STTIPFIL),cp_NullValue(_read_.STTIPFIL))
      this.w_INDENT = NVL(cp_ToDate(_read_.STINDENT),cp_NullValue(_read_.STINDENT))
      this.w_FLATAB = NVL(cp_ToDate(_read_.STFLATAB),cp_NullValue(_read_.STFLATAB))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.pAZIONE $ "S-M"
      * --- Read from CONTROPA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COPATHED"+;
          " from "+i_cTable+" CONTROPA where ";
              +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COPATHED;
          from (i_cTable) where;
              COCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PATHED = NVL(cp_ToDate(_read_.COPATHED),cp_NullValue(_read_.COPATHED))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Empty(Nvl(this.w_PATHED," "))
        ah_errormsg("Attenzione! Occorre indicare il percorso di generazione del file nei parametri!")
        i_retcode = 'stop'
        return
      endif
    endif
     
 righe="alen(parrkey,1)" 
 colon="alen(parrkey,2)" 
 dimension arrkey(&righe,&colon) 
 array="parrkey" 
 acopy(&array,arrkey) 
 CountSegm=0 
 CountRows=0 
 FileStream=" " 
    * --- Determino Tabella Principale nell'entit�
    * --- Select from ENT_DETT
    i_nConn=i_TableProp[this.ENT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ENT_DETT_idx,2],.t.,this.ENT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select EN_TABLE,ENPRINCI  from "+i_cTable+" ENT_DETT ";
          +" where ENPRINCI='S'";
           ,"_Curs_ENT_DETT")
    else
      select EN_TABLE,ENPRINCI from (i_cTable);
       where ENPRINCI="S";
        into cursor _Curs_ENT_DETT
    endif
    if used('_Curs_ENT_DETT')
      select _Curs_ENT_DETT
      locate for 1=1
      do while not(eof())
      this.w_CODTAB = _Curs_ENT_DETT.EN_TABLE
      Exit
        select _Curs_ENT_DETT
        continue
      enddo
      use
    endif
    this.w_INDPROG = 1
    * --- 1 Fase
    *     =======================================================
    * --- Creo tabelle temporanee in funzione dei campi inseriti nella flat table
     
 Dimension arrtab(1,1) 
 L_ret=MakeAllTempFT(alltrim(this.w_FLATAB), @arrtab)
    if L_ret=-1
      ah_errormsg("Attenzione, errore nella creazione tabelle EDI")
      i_retcode = 'stop'
      return
    endif
    * --- Eseguo valorizzazione tabelle TMP_EDI
    * --- Select from gsva1bge
    do vq_exec with 'gsva1bge',this,'_Curs_gsva1bge','',.f.,.t.
    if used('_Curs_gsva1bge')
      select _Curs_gsva1bge
      locate for 1=1
      do while not(eof())
      this.w_CODTAB = Alltrim(Nvl(_Curs_GSVA1BGE.ELCODTAB,Space(30)))
      ah_Msg("Creazione tabella: %1",.T.,.F.,.F., ALLTRIM(this.w_CODTAB))
       
 Dimension Arprog(this.w_INDPROG,3) 
 Arprog[this.w_INDPROG,1]=this.w_CODTAB 
 Arprog[this.w_INDPROG,2]=0 
 Arprog[this.w_INDPROG,3]=0
      a=creacuredi(this.w_CODSTR,Alltrim(_Curs_gsva1bge.ELCODTAB),@Arrkey)
      this.w_INDPROG = this.w_INDPROG + 1
      * --- a=creacuredi(w_CODSTR,Alltrim(gsva1bge->ELCODTAB),@Arrkey)
        select _Curs_gsva1bge
        continue
      enddo
      use
    endif
    if this.pAZIONE $ "S-M"
      this.w_HANDLEFILE = -1
      this.w_SERIAL = DOC_MAST.MVSERIAL
      if Not Empty(this.w_SERIAL)
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVNUMDOC,MVDATDOC,MVDATREG,MVALFEST,MVNUMEST,MVALFDOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVNUMDOC,MVDATDOC,MVDATREG,MVALFEST,MVNUMEST,MVALFDOC;
            from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          this.w_DATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
          this.w_ALFPRO = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
          this.w_NUMPRO = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
          this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
       
 CountSect=0 
 CountIndent=0 
 TestRiga=.t.
      * --- Try
      local bErr_03C099E0
      bErr_03C099E0=bTrsErr
      this.Try_03C099E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg(i_error,"",alltrim(this.w_NOMFIL))
      endif
      bTrsErr=bTrsErr or bErr_03C099E0
      * --- End
      if this.w_HANDLEFILE<0
        i_retcode = 'stop'
        return
      endif
      Predi=this.w_PROGRE
    endif
    if this.pAZIONE $ "S-M"
      * --- Eseguo scruttura elementi di apertura file
      this.w_oERRORLOG=createobject("AH_ErrorLog")
      * --- Select from GSVA0BVS
      do vq_exec with 'GSVA0BVS',this,'_Curs_GSVA0BVS','',.f.,.t.
      if used('_Curs_GSVA0BVS')
        select _Curs_GSVA0BVS
        locate for 1=1
        do while not(eof())
        this.w_CODELE = _Curs_GSVA0BVS.ELCODICE
         
 cursore=SYS(2015) 
 w_a=CREAELEEDI(this.w_CODSTR,this.w_CODENT,this.w_CODELE,this.pCODCON,this.pTIPCON,this.pCODRAG,this.w_HANDLEFILE,0,this.w_oerrorlog,"A","",this.w_INDENT,0,@Arprog)
          select _Curs_GSVA0BVS
          continue
        enddo
        use
      endif
    else
      this.w_oERRORLOG = this.pLogErr
      this.w_HANDLEFILE = this.pHANDLE
    endif
    * --- Cerco eventuale elemento stream per creare file
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCODCUC"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.pTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.pCODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCODCUC;
        from (i_cTable) where;
            ANTIPCON = this.pTIPCON;
            and ANCODICE = this.pCODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_CODCUC = NVL(cp_ToDate(_read_.ANCODCUC),cp_NullValue(_read_.ANCODCUC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_TIPFIL="X"
      * --- Prima era cos�  IIF(w_CODCUC='02',.T.,.F.)
      this.w_CREAIND = .T.
      * --- Select from VAELEMEN
      i_nConn=i_TableProp[this.VAELEMEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAELEMEN_idx,2],.t.,this.VAELEMEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ELCODSTR,EL__TIPO,ELCODCLA,ELCODTAB  from "+i_cTable+" VAELEMEN ";
            +" where ELCODSTR="+cp_ToStrODBC(this.pCODSTR)+" AND EL__TIPO='L'";
             ,"_Curs_VAELEMEN")
      else
        select ELCODSTR,EL__TIPO,ELCODCLA,ELCODTAB from (i_cTable);
         where ELCODSTR=this.pCODSTR AND EL__TIPO="L";
          into cursor _Curs_VAELEMEN
      endif
      if used('_Curs_VAELEMEN')
        select _Curs_VAELEMEN
        locate for 1=1
        do while not(eof())
         
 Filestream=gsar_bgi(null,_Curs_VAELEMEN.ELCODCLA,Alltrim(_Curs_VAELEMEN.ELCODTAB),ARRKEY[1,1],this.w_CREAIND)
        exit
          select _Curs_VAELEMEN
          continue
        enddo
        use
      endif
    endif
    * --- Select from GSVA0BVS
    do vq_exec with 'GSVA0BVS',this,'_Curs_GSVA0BVS','',.f.,.t.
    if used('_Curs_GSVA0BVS')
      select _Curs_GSVA0BVS
      locate for 1=1
      do while not(eof())
      this.w_CODELE = _Curs_GSVA0BVS.ELCODICE
       
 cursore=SYS(2015) 
 w_a=CREAELEEDI(this.w_CODSTR,this.w_CODENT,this.w_CODELE,this.pCODCON,this.pTIPCON,this.pCODRAG,this.w_HANDLEFILE,0,this.w_oerrorlog,"","",this.w_INDENT,0,@Arprog)
        select _Curs_GSVA0BVS
        continue
      enddo
      use
    endif
    if this.pAZIONE $ "S-M"
      * --- Eseguo scruttura elementi di chiusura file
      * --- Select from GSVA0BVS
      do vq_exec with 'GSVA0BVS',this,'_Curs_GSVA0BVS','',.f.,.t.
      if used('_Curs_GSVA0BVS')
        select _Curs_GSVA0BVS
        locate for 1=1
        do while not(eof())
        this.w_CODELE = _Curs_GSVA0BVS.ELCODICE
         
 cursore=SYS(2015) 
 w_a=CREAELEEDI(this.w_CODSTR,this.w_CODENT,this.w_CODELE,this.pCODCON,this.pTIPCON,this.pCODRAG,this.w_HANDLEFILE,0,this.w_oerrorlog,"C","",this.w_INDENT,0,@Arprog)
          select _Curs_GSVA0BVS
          continue
        enddo
        use
      endif
    endif
    if L_ret=-1
      ah_errormsg("Attenzione, errore nella eliminazione tabelle EDI")
      i_retcode = 'stop'
      return
    endif
    * --- Chiudo cursori tabelle utilizzate
    * --- Select from gsva1bge
    do vq_exec with 'gsva1bge',this,'_Curs_gsva1bge','',.f.,.t.
    if used('_Curs_gsva1bge')
      select _Curs_gsva1bge
      locate for 1=1
      do while not(eof())
      if USED(alltrim(_Curs_gsva1bge.ELCODTAB))
        ah_Msg("Eliminazione tabella: %1",.T.,.F.,.F., alltrim(_Curs_gsva1bge.ELCODTAB))
         
 select(alltrim(_Curs_gsva1bge.ELCODTAB)) 
 USE
      endif
        select _Curs_gsva1bge
        continue
      enddo
      use
    endif
    if this.pAZIONE $ "S-M"
      if this.pAZIONE = "S"
        this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
      endif
      if not (FCLOSE(this.w_HANDLEFILE))
        ah_ErrorMsg("Errore in chiusura del file %1.%0Operazione annullata","!","",this.w_NOMFIL)
      else
        if this.pAZIONE = "S"
          if this.w_oErrorLog.IsFullLog()
            ah_ErrorMsg("File generato","i","")
          else
            ah_ErrorMsg("File generato con successo","i","")
          endif
        endif
      endif
    endif
  endproc
  proc Try_03C099E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_PROGRE = SPACE(14)
    this.w_CODESE = g_CODESE
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "PREDI", "i_CODAZI,w_CODESE,w_PROGRE")
    if Not Empty(this.w_NOMFIL)
      * --- Array
       
 DIMENSION ARPARAM[11,2] 
 ARPARAM[1,1]="DATE" 
 ARPARAM[1,2]=ALLTRIM(STR(YEAR(i_DATSYS))+Right("00"+alltrim(STR(MONTH(i_DATSYS))),2)+Right("00"+alltrim(STR(DAY(i_DATSYS))),2)) 
 ARPARAM[2,1]="TIME" 
 ARPARAM[2,2]=ALLTRIM(STRTRAN(TIME(),":","")) 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=ALLTRIM(STR(YEAR(this.w_DATDOC))+Right("00"+alltrim(STR(MONTH(this.w_DATDOC))),2)+Right("00"+alltrim(STR(DAY(this.w_DATDOC))),2)) 
 ARPARAM[4,1]="NUMDOC" 
 ARPARAM[4,2]=ALLTRIM(STR(this.w_NUMDOC)) 
 ARPARAM[5,1]="PROGRE" 
 ARPARAM[5,2]=this.w_PROGRE 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=this.pCODCON 
 ARPARAM[7,1]="TIPCON" 
 ARPARAM[7,2]=this.pTIPCON 
 ARPARAM[8,1]="DATREG" 
 ARPARAM[8,2]=ALLTRIM(STR(YEAR(this.w_DATREG))+Right("00"+alltrim(STR(MONTH(this.w_DATREG))),2)+Right("00"+alltrim(STR(DAY(this.w_DATREG))),2)) 
 ARPARAM[9,1]="ALFDOC" 
 ARPARAM[9,2]=Alltrim(Nvl(this.w_ALFDOC,"")) 
 ARPARAM[10,1]="NUMPRO" 
 ARPARAM[10,2]=ALLTRIM(STR(this.w_NUMPRO)) 
 ARPARAM[11,1]="ALFPRO" 
 ARPARAM[11,2]=Alltrim(Nvl(this.w_ALFPRO,""))
       
 DIMENSION ARVAR[11,2] 
 ARVAR[1,1]="DATE" 
 ARVAR[1,2]=i_DATSYS 
 ARVAR[2,1]="TIME" 
 ARVAR[2,2]=TIME() 
 ARVAR[3,1]="DATDOC" 
 ARVAR[3,2]=this.w_DATDOC 
 ARVAR[4,1]="NUMDOC" 
 ARVAR[4,2]=this.w_NUMDOC 
 ARVAR[5,1]="PROGRE" 
 ARVAR[5,2]=this.w_PROGRE 
 ARVAR[6,1]="CODCON" 
 ARVAR[6,2]=this.pCODCON 
 ARVAR[7,1]="TIPCON" 
 ARVAR[7,2]=this.pTIPCON 
 ARVAR[8,1]="DATREG" 
 ARVAR[8,2]=this.w_DATREG 
 ARVAR[9,1]="ALFDOC" 
 ARVAR[9,2]=Nvl(this.w_ALFDOC,"") 
 ARVAR[10,1]="NUMPRO" 
 ARVAR[10,2]=this.w_NUMPRO 
 ARVAR[11,1]="ALFPRO" 
 ARVAR[11,2]=Nvl(this.w_ALFPRO,"")
      * --- Select from VAVARNOM
      i_nConn=i_TableProp[this.VAVARNOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAVARNOM_idx,2],.t.,this.VAVARNOM_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" VAVARNOM ";
            +" where VNCODICE="+cp_ToStrODBC(this.w_CODSTR)+"";
             ,"_Curs_VAVARNOM")
      else
        select * from (i_cTable);
         where VNCODICE=this.w_CODSTR;
          into cursor _Curs_VAVARNOM
      endif
      if used('_Curs_VAVARNOM')
        select _Curs_VAVARNOM
        locate for 1=1
        do while not(eof())
        this.w_MASK = _Curs_VAVARNOM.VN__MASK
        this.w_FILL = _Curs_VAVARNOM.VNZERFIL
        this.w_POS = INT((ASCAN("ARVAR",Alltrim(Nvl(_Curs_VAVARNOM.VNCODVAR," ")))+1)/2)
        if this.w_POS>0
          * --- Applico maschera
          if Not Empty(this.w_MASK)
            do case
              case VarType(ARVAR[this.w_POS,2])="D"
                 
 VALDAT=cp_todate(ARVAR[this.w_POS,2])
                if Not Empty(VALDAT)
                  * --- Anno
                  this.w_MASK=STRTRAN(this.w_MASK,REPLICATE("A",OCCURS("A",this.w_MASK)),right(ALLTRIM(STR(year(VALDAT))),oCCURS("A",this.w_MASK)))
                  * --- Mese
                  this.w_MASK=STRTRAN(this.w_MASK,REPLICATE("M",OCCURS("M",this.w_MASK)),right("00"+ALLTRIM(STR(month(VALDAT))),oCCURS("M",this.w_MASK)))
                  * --- giorno
                  this.w_MASK=STRTRAN(this.w_MASK,REPLICATE("G",OCCURS("G",this.w_MASK)),right("00"+ALLTRIM(STR(day(VALDAT))),oCCURS("G",this.w_MASK)))
                  * --- ora
                  this.w_MASK=STRTRAN(this.w_MASK,REPLICATE("O",OCCURS("O",this.w_MASK)),right(ALLTRIM(STR(HOUR(VALDAT))),oCCURS("O",this.w_MASK)))
                  * --- minuti
                  this.w_MASK=STRTRAN(this.w_MASK,REPLICATE("T",OCCURS("T",this.w_MASK)),right(ALLTRIM(STR(MINUTE(VALDAT))),oCCURS("T",this.w_MASK)))
                  * --- Secondi
                  this.w_MASK=STRTRAN(this.w_MASK,REPLICATE("S",OCCURS("S",this.w_MASK)),right(ALLTRIM(STR(SEC(VALDAT))),oCCURS("S",this.w_MASK)))
                   
 ARPARAM[this.w_POS,2]=strtran(Alltrim(this.w_MASK),SET ("MARK"),"")
                endif
              case VarType(ARVAR[this.w_POS,2])="N"
                ARPARAM[this.w_POS,2]=Tran(ARPARAM[this.w_POS,2],this.w_MASK)
              otherwise
                if this.w_FILL<>"S"
                   
 ARPARAM[this.w_POS,2]=Left(Alltrim(ARPARAM[this.w_POS,2]),Len(alltrim(this.w_MASK)))
                endif
            endcase
          endif
          * --- Applico zerofil in funzione della maschera inputata
          if this.w_FILL="S" and Not Empty(this.w_MASK) and Not Empty(ARVAR[this.w_POS,2])
            ARPARAM[this.w_POS,2]=Right(REPLICATE("0",len(Alltrim(this.w_MASK)))+ARPARAM[this.w_POS,2],len(Alltrim(this.w_MASK)))
          endif
        endif
          select _Curs_VAVARNOM
          continue
        enddo
        use
      endif
      this.w_NOMFIL = CALDESPA(this.w_NOMFIL,@ARPARAM,.T.)
    else
      * --- Raise
      i_Error="Errore nome file non definito"
      return
    endif
    if ! Directory(Alltrim(this.w_PATHED))
      this.w_ERRODIR = Not(this.AH_CreateFolder(this.w_PATHED))
    endif
    if !this.w_ERRODIR
      if Not File(Alltrim(this.w_PATHED)+Alltrim(this.w_NOMFIL)) Or ah_YesNo("Il file esiste gi�, si desidera sovrascriverlo?")
        this.w_HANDLEFILE = FCREATE(Alltrim(this.w_PATHED)+Alltrim(this.w_NOMFIL))
      endif
    endif
    if this.w_HANDLEFILE<0
      * --- Raise
      i_Error="Errore nella creazione file EDI"
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pCODSTR,pCODCON,pTIPCON,pCODRAG,pARRKEY,pHANDLE,pAZIONE,pLogErr)
    this.pCODSTR=pCODSTR
    this.pCODCON=pCODCON
    this.pTIPCON=pTIPCON
    this.pCODRAG=pCODRAG
    this.pARRKEY=pARRKEY
    this.pHANDLE=pHANDLE
    this.pAZIONE=pAZIONE
    this.pLogErr=pLogErr
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='VASTRUTT'
    this.cWorkTables[3]='ENT_DETT'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='VAELEMEN'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='VAVARNOM'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_ENT_DETT')
      use in _Curs_ENT_DETT
    endif
    if used('_Curs_gsva1bge')
      use in _Curs_gsva1bge
    endif
    if used('_Curs_VAVARNOM')
      use in _Curs_VAVARNOM
    endif
    if used('_Curs_GSVA0BVS')
      use in _Curs_GSVA0BVS
    endif
    if used('_Curs_VAELEMEN')
      use in _Curs_VAELEMEN
    endif
    if used('_Curs_GSVA0BVS')
      use in _Curs_GSVA0BVS
    endif
    if used('_Curs_GSVA0BVS')
      use in _Curs_GSVA0BVS
    endif
    if used('_Curs_gsva1bge')
      use in _Curs_gsva1bge
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsar_bee
  * ===============================================================================
    * AH_CreateFolder() - Crea la cartella passata come parametro
    *
    Function AH_CreateFolder(pDirectory)
    private lOldErr,lRet
    * On Error
    lOldErr=ON("Error")
    * Init Risultato
    lRet = True
    * Crea
    on error lRet=False
    *
    pDirectory=JUSTPATH(ADDBS(pDirectory))
    *
    MD (pDirectory)
    *
    if not(Empty(lOldErr))
      on error &lOldErr
    else
      on error
    endif   
    * 
    if not(lRet)
      = Ah_ErrorMsg("Impossibile creare la cartella %1",16,,pDirectory)
    endif
    * Risultato
    Return (lRet)
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODSTR,pCODCON,pTIPCON,pCODRAG,pARRKEY,pHANDLE,pAZIONE,pLogErr"
endproc
