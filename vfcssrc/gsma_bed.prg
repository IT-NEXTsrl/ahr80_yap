* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bed                                                        *
*              Cancellazione dati rilevati                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-15                                                      *
* Last revis.: 2009-08-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bed",oParentObject)
return(i_retval)

define class tgsma_bed as StdBatch
  * --- Local variables
  w_SCELTA = .f.
  * --- WorkFile variables
  RILEVAZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch di Cancellazione Dati Rilevati
    if EMPTY(this.oParentObject.w_CODRIL)
      ah_errormsg("Codice rilevazione obbligatorio",48,"")
      i_retcode = 'stop'
      return
    endif
    this.w_SCELTA = ah_yesno("Questa operazione eliminerÓ i dati rilevati selezionati che non hanno ancora generato un movimento di magazzino%0Si desidera proseguire?")
    if !(this.w_SCELTA)
      ah_errormsg("Eliminazione dati rilevati interrotta come richiesto",48,"")
      i_retcode = 'stop'
      return
    else
      ah_msg("Eliminazione dati rilevati...",.t.,.t.)
      * --- Delete from RILEVAZI
      i_nConn=i_TableProp[this.RILEVAZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".DRSERIAL = "+i_cQueryTable+".DRSERIAL";
      
        do vq_exec with 'QUERY\GSMA_EDR',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      WAIT CLEAR
      ah_errormsg("Eliminazione dati rilevati completata",64,"")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='RILEVAZI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
