* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_b08                                                        *
*              Elabora S.A.PN senza rat/ris                                    *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-02-12                                                      *
* Last revis.: 2008-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_b08",oParentObject)
return(i_retval)

define class tgscg_b08 as StdBatch
  * --- Local variables
  w_FILINI = ctod("  /  /  ")
  w_VALNAZ = space(3)
  w_GIOPER = 0
  w_COD001 = space(15)
  w_CODCEN = space(15)
  w_FILFIN = ctod("  /  /  ")
  w_CODBUN = space(3)
  w_GIOCOM = 0
  w_COD002 = space(15)
  w_VOCCEN = space(15)
  w_TIPCON = space(10)
  w_INICOM = ctod("  /  /  ")
  w_SCARTO = 0
  w_COD003 = space(15)
  w_CODCOM = space(15)
  w_CODICE = space(15)
  w_FINCOM = ctod("  /  /  ")
  w_RISCON = 0
  w_IMPDAR = 0
  w_DATCIN = ctod("  /  /  ")
  w_TOTIMP = 0
  w_CAOVAL = 0
  w_APPO = space(15)
  w_IMPAVE = 0
  w_DATCFI = ctod("  /  /  ")
  w_RISATT = space(15)
  w_RECODICE = space(15)
  w_DATREG = ctod("  /  /  ")
  w_RATATT = space(15)
  w_RISPAS = space(15)
  w_REDESCRI = space(45)
  w_RATPAS = space(15)
  w_PARAMETRO = 0
  w_TOTANA = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene usato in Elaborazione Scritture di Assestamento (da GSCG_BAT)
    *     Lanciato per la generazione delle scritture di assestamento per i movimenti
    *     cespiti.
    this.w_FILINI = this.oParentObject.oParentObject.w_ASDATINI
    this.w_FILFIN = this.oParentObject.oParentObject.w_ASDATFIN
    this.w_RISATT = this.oParentObject.oParentObject.w_CORISATT
    this.w_RISPAS = this.oParentObject.oParentObject.w_CORISPAS
    this.w_RATATT = this.oParentObject.oParentObject.w_CORATATT
    this.w_RATPAS = this.oParentObject.oParentObject.w_CORATPAS
    if USED("EXTCG001")
      * --- Crea il Temporaneo di Appoggio
      CREATE CURSOR TmpAgg1 (TIPCON C(1), CODICE C(15), CODBUN C(3), TOTIMP N(18,4), ;
      CODCEN C(15), VOCCEN C(15), CODCOM C(15), DATCIN D(8), DATCFI D(8),IMPORTO N(18,4))
      SELECT EXTCG001
      GO TOP
      SCAN FOR NVL(TIPCON," ") $ "MG" AND (NVL(IMPDAR,0)-NVL(IMPAVE,0))<>0 AND NOT EMPTY(NVL(CODICE," "))
      this.w_TIPCON = TIPCON
      this.w_CODICE = CODICE
      this.w_TOTIMP = NVL(IMPDAR,0) - NVL(IMPAVE,0)
      this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
      this.w_CODBUN = NVL(CODBUN, g_CODBUN)
      this.w_INICOM = CP_TODATE(INICOM)
      this.w_FINCOM = CP_TODATE(FINCOM)
      this.w_DATREG = CP_TODATE(DATREG)
      * --- Dati analitica
      this.w_CODCEN = NVL(CODCEN, SPACE(15))
      this.w_VOCCEN = NVL(VOCCEN, SPACE(15))
      this.w_CODCOM = NVL(CODCOM, SPACE(15))
      this.w_DATCIN = NVL(DATCIN, cp_CharToDate("  -  -    "))
      this.w_DATCFI = NVL(DATCFI, cp_CharToDate("  -  -    "))
      this.w_TOTIMP = NVL(IMPDAR,0) - NVL(IMPAVE,0)
      this.w_SCARTO = 0
      this.w_RISCON = 0
      * --- 1^OPERAZIONE: Riporta i Valori alla Valuta di Conto (si Presume Lira o EURO)
      if this.w_VALNAZ<>g_PERVAL
        this.w_CAOVAL = GETCAM(this.w_VALNAZ, this.w_DATREG)
        this.w_TOTIMP = VAL2MON(this.w_TOTIMP, this.w_CAOVAL, GETCAM(g_PERVAL,this.w_DATREG), this.w_DATREG, g_PERVAL, g_PERPVL)
      endif
      * --- 2^OPERAZIONE: Se presente una Competenza, calcola l'esatto Importo e eventuali Risconti
      if NOT EMPTY(this.w_INICOM) AND NOT EMPTY(this.w_FINCOM)
        if this.w_FINCOM<this.w_FILINI OR this.w_INICOM>this.w_FILFIN
          * --- Registrazione Totalmente fuori dal Periodo, Ignora e passa al record successivo
          LOOP
        else
          this.w_GIOCOM = (this.w_FINCOM+1) - this.w_INICOM
          * --- Registrazione con Competenza che inizia prima del Periodo, Scarta la Parte prima
          if this.w_INICOM<this.w_FILINI AND this.w_GIOCOM<>0
            this.w_GIOPER = this.w_FILINI - this.w_INICOM
            this.w_SCARTO = cp_ROUND((this.w_TOTIMP * this.w_GIOPER) / this.w_GIOCOM, g_PERPVL)
          endif
          * --- Registrazione con Competenza che Prosegue oltre il Periodo, Considera la parte successiva come Risconto
          if this.w_FINCOM>this.w_FILFIN AND this.w_GIOCOM<>0
            this.w_GIOPER = this.w_FINCOM - this.w_FILFIN
            this.w_RISCON = cp_ROUND((this.w_TOTIMP * this.w_GIOPER) / this.w_GIOCOM, g_PERPVL)
          endif
          * --- Ricalcola l'Importo di Competenza
          this.w_TOTIMP = this.w_TOTIMP - this.w_SCARTO - this.w_RISCON
        endif
      endif
      * --- Scrive il Temporaneo di Appoggio interno al Batch
      if this.w_TOTIMP<>0
        INSERT INTO TmpAgg1 (TIPCON, CODICE, CODBUN, TOTIMP, ;
        CODCEN, VOCCEN, CODCOM, DATCIN, DATCFI) ;
        VALUES (this.w_TIPCON, this.w_CODICE, this.w_CODBUN, this.w_TOTIMP, ;
        this.w_CODCEN, this.w_VOCCEN, this.w_CODCOM, this.w_DATCIN, this.w_DATCFI)
      endif
      SELECT EXTCG001
      ENDSCAN
      * --- Elimina il Cursore della Query
      Select Tipcon,Codice,Codbun,; 
 Sum(Totimp) as Totimp from Tmpagg1 into cursor Appoggio; 
 group by Tipcon,Codice,Codbun order by Tipcon,Codice,Codbun
      * --- Raggruppo i dati analitici per avere un unico cursore
      Select Tipcon,Codice,Codbun,Datcin,Datcfi,Codcen,Voccen,Codcom,Sum(Totimp) as Importo; 
 From TmpAgg1 into cursor Analitica group by Tipcon,Codice,Codbun,Codcen,Voccen,Codcom,Datcin,Datcfi order by Tipcon,Codice,Codbun,Codcen,Voccen,Codcom
      Select Analitica.Tipcon as Tipcon,Analitica.Codice as Codice,Analitica.Codbun as Codbun,Appoggio.Totimp as Totimp,; 
 Analitica.Datcin as Datcin,Analitica.Datcfi as Datcfi,Analitica.Codcen as Codcen,; 
 Analitica.Voccen as Voccen,Analitica.Codcom as Codcom,Analitica.Importo as Importo; 
 From Analitica Inner Join Appoggio On Appoggio.Tipcon=Analitica.Tipcon and Appoggio.Codice=Analitica.Codice; 
 and Appoggio.Codbun=Analitica.Codbun; 
 order by Analitica.Tipcon,Analitica.Codice,Analitica.Codbun into cursor TEMP 
      if used("EXTCG001")
        select EXTCG001
        use
      endif
      if USED("TmpAgg1")
        * --- Azzera temporaneo
        SELECT TmpAgg1
        USE
      endif
      if used("Appoggio")
        select Appoggio
        use
      endif
      if used("Analitica")
        select Analitica
        use
      endif
      if USED("Temp")
        SELECT TEMP
        if RECCOUNT()>0
          * --- A questo Punto aggiorno il Temporaneo Principale
          SELECT TEMP
          GO TOP
          SCAN FOR NVL(TIPCON," ") $ "GM" AND NVL(TOTIMP,0)<>0 AND NOT EMPTY(NVL(CODICE," "))
          this.w_CODBUN = NVL(CODBUN, g_CODBUN)
          this.w_TIPCON = TIPCON
          this.w_COD001 = IIF(this.w_TIPCON="M", CODICE, SPACE(15))
          this.w_COD002 = IIF(this.w_TIPCON="G", CODICE, SPACE(15))
          this.w_COD003 = SPACE(15)
          this.w_IMPDAR = IIF(TOTIMP>0, TOTIMP, 0)
          this.w_IMPAVE = IIF(TOTIMP<0, ABS(TOTIMP), 0)
          * --- Dati analitica
          this.w_CODCEN = NVL(CODCEN, SPACE(15))
          this.w_VOCCEN = NVL(VOCCEN, SPACE(15))
          this.w_CODCOM = NVL(CODCOM, SPACE(15))
          this.w_DATCIN = NVL(DATCIN, cp_CharToDate("  -  -    "))
          this.w_DATCFI = NVL(DATCFI, cp_CharToDate("  -  -    "))
          this.w_TOTANA = Importo
          this.w_PARAMETRO = IIF(this.w_TOTANA<>0,(ABS(this.w_TOTANA)/ABS(TOTIMP))*100,1)
          INSERT INTO TmpAgg (CODICE, CODBUN, DESCRI, ;
          TIPCON, COD001, COD002, COD003, IMPDAR, IMPAVE, ;
          CODCEN, VOCCEN, CODCOM, DATCIN, DATCFI,PARAMETRO,TOTIMP) ;
          VALUES (this.w_RECODICE, this.w_CODBUN, this.w_REDESCRI, ;
          this.w_TIPCON, this.w_COD001, this.w_COD002, this.w_COD003, this.w_IMPDAR, this.w_IMPAVE, ;
          this.w_CODCEN, this.w_VOCCEN, this.w_CODCOM, this.w_DATCIN, this.w_DATCFI,this.w_PARAMETRO,this.w_TOTANA)
          SELECT TEMP
          ENDSCAN
        endif
        SELECT TEMP
        USE
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
