* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_ber                                                        *
*              Elabora rischio cliente                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_687]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2008-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPRG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_ber",oParentObject,m.pPRG)
return(i_retval)

define class tgsve_ber as StdBatch
  * --- Local variables
  pPRG = space(1)
  w_CLIFRL = space(1)
  w_VENACQ = space(1)
  w_CATCOML = space(3)
  w_CATCONL = space(5)
  w_ZONAL = space(3)
  w_MASTCONTL = space(15)
  w_CODCON = space(15)
  w_CODIVA = space(5)
  w_TOTPAP = 0
  w_TOTEFF = 0
  w_TOTEFO = 0
  w_TOTORD = 0
  w_TOTDDT = 0
  w_TOTFAT = 0
  w_FLOMAG = space(1)
  w_CAOAPE = 0
  w_CAONAZ = 0
  w_DATVAL = ctod("  /  /  ")
  w_PERVAL = space(3)
  w_DECTOP = 0
  w_VALIVA = 0
  w_CLADOC = space(2)
  w_VALMAG = 0
  w_CODIVE = space(5)
  w_PERIVA = 0
  w_EVASA = 0
  w_TOTORD = 0
  w_TOTDDT = 0
  w_TOTFAT = 0
  w_FLSCOR = space(1)
  w_OVALMAG = 0
  w_IMPACC = 0
  w_DELROW = 0
  w_INSROW = 0
  w_PADRE = .NULL.
  * --- WorkFile variables
  AZIENDA_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  SIT_FIDI_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  TMPVEND1_idx=0
  TMPVEND2_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Rischio Cliente (da GSVE_KER , GSVE_ACF, GSUT_BGA (Euro-Kit)),GSTE_KFE (Elaborazione Esposizione Finanziaria)
    * --- Programma di lancio: K = Maschera ; A = Anagrafica ; E = EURO KIT ; F = RISCHIO FORNITORE ; P=P.O.S.
    if this.pPRG="F"
      if this.oParentObject.w_DAIMPO>this.oParentObject.w_ADIMPO
        ah_ErrorMsg("Importo finale minore di quello iniziale")
        i_retcode = 'stop'
        return
      endif
       
 L_CLIENTE = IIF(this.oParentObject.w_TIPO="C",this.oParentObject.w_CLIENTE,this.oParentObject.w_FORNITORE) 
 L_TIPO = this.oParentObject.w_TIPO 
 L_ORDI = this.oParentObject.w_ORDI 
 L_CATCOM = this.oParentObject.w_CATCOM 
 L_ZONA = this.oParentObject.w_ZONA 
 L_MASTCONT = this.oParentObject.w_MASTCONT 
 L_CATCON = this.oParentObject.w_CATCON 
 L_DAIMPO = this.oParentObject.w_DAIMPO 
 L_ADIMPO = this.oParentObject.w_ADIMPO
      this.w_CLIFRL = this.oParentObject.w_TIPO
      this.w_CATCOML = this.oParentObject.w_CATCOM
      this.w_CATCONL = this.oParentObject.w_CATCON
      this.w_ZONAL = this.oParentObject.w_ZONA
      this.w_MASTCONTL = this.oParentObject.w_MASTCONT
    else
      this.w_CLIFRL = "C"
    endif
    if this.w_CLIFRL="C"
      this.w_VENACQ = "V"
    else
      this.w_VENACQ = "A"
    endif
    * --- Struttura TMPVEND1:
    *     CODCLI - Cliente
    *     CODVAL  - Valuta
    *     CAOAPE - Cambio di Apertura / testata documento
    *     TOTPAP - Totale Partite Aperte
    *     TOTEFF - Totale effetti in scadenza
    *     TOTEFO - Totale effetti Scaduti
    *     TOTORD - Totale Ordini in essere - Prezzo per tipo='E' / 'M'
    *     TOTDDT - Totale DDT aperti - Impeva per Tipo='E' / 'M'
    *     TOTFAT - Totale Fatture da contabilizzare - Valmqg per Tipo='E' / 'M'
    *     DETT - (Origine partita ) gestito solo per partite / TipRig per Tipo='E' / 'M'
    *     DATDOC - Data Scadenza Partita / Data documento x calcoli cambi
    *     CAOORI - Cambio Origine (a 0 per i documenti)
    *     TIPO = 'P' da partite..
    *     CLADOC = Categoria documento (Solo Tipo='E' / 'M')
    *     CODIVE = Iva in testata (Solo Tipo='E' / 'M')
    *     CODIVA = Iva sul dettaglio (Solo Tipo='E' / 'M')
    *     FLOMAG = Tipo omaggio di riga (Solo Tipo='E' / 'M')
    *     FLSCOR = Scorporo (Solo per gestone righe documenti)
    ah_Msg("Ricerca partite aperte...")
    * --- Carica Gli Importi Partite sul temporaneo..
    if g_SOLL="S"
      * --- Per escludere contenziosi di tipo No effetto si � resa necessaria una Join con 
      *     la tabella CON_TENZ per tale motivo a seconda dell'abilitazione o meno del modulo 
      *     vengono lanciate due query diverse 
      *     Le query che si diversificano sono:
      *     GSVE10BE,GSVE11BE
      *     e  le rispettive 
      *     GSVES10BE,GSVES11BE
      * --- Create temporary table TMPVEND1
      i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSVE_BER.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      * --- Create temporary table TMPVEND1
      i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      vq_exec('QUERY\GSVESBER.VQR',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
      this.TMPVEND1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    endif
    * --- Evasa condiziona la GSVE_BER_4
    *     0=Documenti mai evasi (Sum(qtaeva)=0)
    *     Recupero il totale documenti come somma del cstelletto IVA
    *     1= documenti parzialmente evasi (Sum(qtaeva)<>0)
    *     Recupero le righe aperte e una per una le valuto
    *     2=Documenti mai evasi con righe evase a mano (Sum(qtaeva)=0 ed almeno una riga evasa)
    *     
    *     x le spese generali la logica � se il documento � effettivamente evaso (QTAEVA+IMPEVA<>0) non
    *     si considerano (sono confkuite sul documento che evade).
    *     Recupero le righe evase a mano e le decurto dal rischio.
    this.w_EVASA = 0
    ah_Msg("Ricerca documenti mai evasi o da contabilizzare...")
    * --- Insert into TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsve_ber_5",this.TMPVEND1_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_EVASA = 1
    ah_Msg("Ricerca righe ordini inevasi e DDT non fatturati...")
    * --- Insert into TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsve_ber_1",this.TMPVEND1_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_EVASA = 2
    ah_Msg("Ricerca righe ordini inevasi e DDT evasi a mano...")
    * --- Insert into TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsve_ber_1",this.TMPVEND1_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ricerca acconti da contabilizzare ciclo documentale...")
    * --- Insert into TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsve_ber_2",this.TMPVEND1_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_Msg("Ricerca DDT evasi da fatture provvisorie...")
    * --- Insert into TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsve_ber_8",this.TMPVEND1_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('GSVE_BER_9',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Delete from TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.TMPVEND2_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          )
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Elaboro i vari dati per convertire eventuali valori non in valuta...
    this.w_DECTOP = g_PERPVL
    this.w_CAONAZ = GETCAM(g_PERVAL, I_DATSYS)
    this.w_PERVAL = g_PERVAL
    * --- Recupero tutti i documenti/ partite in valuta diversa pi� le righe dei documenti
    *     parzialmente evasi o aperti con righe evase a mano
    * --- Select from TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMPVEND1 ";
          +" where CODVAL<>"+cp_ToStrODBC(this.w_PERVAL)+" Or TIPO='E' Or TIPO='M'";
           ,"_Curs_TMPVEND1")
    else
      select * from (i_cTable);
       where CODVAL<>this.w_PERVAL Or TIPO="E" Or TIPO="M";
        into cursor _Curs_TMPVEND1
    endif
    if used('_Curs_TMPVEND1')
      select _Curs_TMPVEND1
      locate for 1=1
      do while not(eof())
      * --- Converte gli Importi alla Moneta di Conto
      this.w_DATVAL = NVL(_Curs_TMPVEND1.DATDOC, GETVALUT(g_PERVAL, "VADATEUR")-1)
      do case
        case _Curs_TMPVEND1.TIPO="P"
          this.w_TOTPAP = NVL(_Curs_TMPVEND1.TOTPAP, 0)
          this.w_TOTEFF = NVL(_Curs_TMPVEND1.TOTEFF, 0)
          this.w_TOTEFO = NVL(_Curs_TMPVEND1.TOTEFO, 0)
          this.w_CAOAPE = IIF(EMPTY(NVL(_Curs_TMPVEND1.CAOAPE,0)), NVL(_Curs_TMPVEND1.CAOORI,0), _Curs_TMPVEND1.CAOAPE)
          this.w_CAOAPE = IIF(EMPTY(this.w_CAOAPE), GETCAM(g_PERVAL,I_DATSYS), this.w_CAOAPE)
          if this.w_TOTPAP<>0
            this.w_TOTPAP = VAL2MON(this.w_TOTPAP, this.w_CAOAPE, this.w_CAONAZ, this.w_DATVAL, g_PERVAL, this.w_DECTOP)
          endif
          if this.w_TOTEFF<>0
            this.w_TOTEFF = VAL2MON(this.w_TOTEFF, this.w_CAOAPE, this.w_CAONAZ, this.w_DATVAL, g_PERVAL, this.w_DECTOP)
          endif
          if this.w_TOTEFO<>0
            this.w_TOTEFO = VAL2MON(this.w_TOTEFO, this.w_CAOAPE, this.w_CAONAZ, this.w_DATVAL, g_PERVAL, this.w_DECTOP)
          endif
          * --- Esegua una WRITE per modificare i valori...
          * --- Write into TMPVEND1
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TOTPAP ="+cp_NullLink(cp_ToStrODBC(this.w_TOTPAP),'TMPVEND1','TOTPAP');
            +",TOTEFF ="+cp_NullLink(cp_ToStrODBC(this.w_TOTEFF),'TMPVEND1','TOTEFF');
            +",TOTEFO ="+cp_NullLink(cp_ToStrODBC(this.w_TOTEFO),'TMPVEND1','TOTEFO');
                +i_ccchkf ;
            +" where ";
                +"CODCLI = "+cp_ToStrODBC(_Curs_TMPVEND1.CODCLI);
                +" and CODVAL = "+cp_ToStrODBC(_Curs_TMPVEND1.CODVAL);
                +" and CAOAPE = "+cp_ToStrODBC(_Curs_TMPVEND1.CAOAPE);
                +" and DATDOC = "+cp_ToStrODBC(_Curs_TMPVEND1.DATDOC);
                +" and DETT = "+cp_ToStrODBC(_Curs_TMPVEND1.DETT);
                   )
          else
            update (i_cTable) set;
                TOTPAP = this.w_TOTPAP;
                ,TOTEFF = this.w_TOTEFF;
                ,TOTEFO = this.w_TOTEFO;
                &i_ccchkf. ;
             where;
                CODCLI = _Curs_TMPVEND1.CODCLI;
                and CODVAL = _Curs_TMPVEND1.CODVAL;
                and CAOAPE = _Curs_TMPVEND1.CAOAPE;
                and DATDOC = _Curs_TMPVEND1.DATDOC;
                and DETT = _Curs_TMPVEND1.DETT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case _Curs_TMPVEND1.TIPO="D"
          this.w_TOTORD = NVL(_Curs_TMPVEND1.TOTORD, 0)
          this.w_TOTDDT = NVL(_Curs_TMPVEND1.TOTDDT, 0)
          this.w_TOTFAT = NVL(_Curs_TMPVEND1.TOTFAT, 0)
          this.w_CAOAPE = IIF(EMPTY(NVL(_Curs_TMPVEND1.CAOAPE,0)), GETCAM(g_PERVAL,I_DATSYS), _Curs_TMPVEND1.CAOAPE)
          if this.w_TOTORD<>0
            this.w_TOTORD = VAL2MON(this.w_TOTORD, this.w_CAOAPE, this.w_CAONAZ, this.w_DATVAL, g_PERVAL, this.w_DECTOP)
          endif
          if this.w_TOTDDT<>0
            this.w_TOTDDT = VAL2MON(this.w_TOTDDT, this.w_CAOAPE, this.w_CAONAZ, this.w_DATVAL, g_PERVAL, this.w_DECTOP)
          endif
          if this.w_TOTFAT<>0
            this.w_TOTFAT = VAL2MON(this.w_TOTFAT, this.w_CAOAPE, this.w_CAONAZ, this.w_DATVAL, g_PERVAL, this.w_DECTOP)
          endif
          * --- Esegua una WRITE per modificare i valori...
          * --- Write into TMPVEND1
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TOTORD ="+cp_NullLink(cp_ToStrODBC(this.w_TOTORD),'TMPVEND1','TOTORD');
            +",TOTDDT ="+cp_NullLink(cp_ToStrODBC(this.w_TOTDDT),'TMPVEND1','TOTDDT');
            +",TOTFAT ="+cp_NullLink(cp_ToStrODBC(this.w_TOTFAT),'TMPVEND1','TOTFAT');
                +i_ccchkf ;
            +" where ";
                +"CODCLI = "+cp_ToStrODBC(_Curs_TMPVEND1.CODCLI);
                +" and CODVAL = "+cp_ToStrODBC(_Curs_TMPVEND1.CODVAL);
                +" and CAOAPE = "+cp_ToStrODBC(_Curs_TMPVEND1.CAOAPE);
                +" and DATDOC = "+cp_ToStrODBC(_Curs_TMPVEND1.DATDOC);
                   )
          else
            update (i_cTable) set;
                TOTORD = this.w_TOTORD;
                ,TOTDDT = this.w_TOTDDT;
                ,TOTFAT = this.w_TOTFAT;
                &i_ccchkf. ;
             where;
                CODCLI = _Curs_TMPVEND1.CODCLI;
                and CODVAL = _Curs_TMPVEND1.CODVAL;
                and CAOAPE = _Curs_TMPVEND1.CAOAPE;
                and DATDOC = _Curs_TMPVEND1.DATDOC;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case _Curs_TMPVEND1.TIPO="E" Or _Curs_TMPVEND1.TIPO="M"
          this.w_IMPACC = NVL(_Curs_TMPVEND1.TOTPAP, 0)
          this.w_OVALMAG = NVL(_Curs_TMPVEND1.TOTEFF, 0)
          this.w_FLSCOR = NVL(_Curs_TMPVEND1.FLSCOR, "  ")
          this.w_CLADOC = NVL(_Curs_TMPVEND1.CLADOC, "  ")
          this.w_CODIVA = IIF(NOT EMPTY(NVL(_Curs_TMPVEND1.CODIVE," ")), SPACE(5), NVL(_Curs_TMPVEND1.CODIVA, SPACE(5)))
          this.w_FLOMAG = NVL(_Curs_TMPVEND1.FLOMAG, " ")
          this.w_VALMAG = NVL(_Curs_TMPVEND1.TOTFAT, 0)
          this.w_VALIVA = 0
          this.w_PERIVA = 0
          if this.w_FLOMAG $ "XI" AND NOT EMPTY(this.w_CODIVA)
            * --- Calcola %IVA
            * --- Read from VOCIIVA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VOCIIVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "IVPERIVA"+;
                " from "+i_cTable+" VOCIIVA where ";
                    +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                IVPERIVA;
                from (i_cTable) where;
                    IVCODIVA = this.w_CODIVA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_PERIVA<>0
              if this.w_FLSCOR="S"
                * --- Se scorporo piede fattura eseguo l'antiscorporo, quindi determino
                *     l'iva e la sommo all'imponibile, dopo di che sottraggo le spese riparitite di
                *     riga.
                this.w_VALIVA = cp_ROUND(((this.w_OVALMAG * this.w_PERIVA) / 100), this.w_DECTOP)
                this.w_VALMAG = IIF(this.w_FLOMAG="X", this.w_VALMAG, 0) + this.w_VALIVA - this.w_IMPACC
              else
                this.w_VALIVA = cp_ROUND(((this.w_VALMAG * this.w_PERIVA) / 100), this.w_DECTOP)
                this.w_VALMAG = IIF(this.w_FLOMAG="X", this.w_VALMAG, 0) + this.w_VALIVA
              endif
            endif
          endif
          if _Curs_TMPVEND1.TIPO="M"
            * --- Nel caso di documento aperto con riga evasa a mano passo al rischio
            *     il totale documento - la riga evasa a mano, il cui valore (IVA INCLUSA)
            *     ho determinato in w_VALMAG. Il totale documento � gia stato inserito
            *     in quanto documento non evaso! (w_EVASA=0)
            this.w_VALMAG = - this.w_VALMAG
          endif
          * --- Alla fine di tutto converto nella valuta di conto se necessario...
          if _Curs_TMPVEND1.CODVAL<>g_PERVAL
            this.w_CAOAPE = IIF(EMPTY(NVL(_Curs_TMPVEND1.CAOAPE,0)), GETCAM(g_PERVAL,I_DATSYS), _Curs_TMPVEND1.CAOAPE)
            this.w_VALMAG = VAL2MON(this.w_VALMAG, this.w_CAOAPE, this.w_CAONAZ, this.w_DATVAL, g_PERVAL, this.w_DECTOP)
          endif
          if this.w_CLADOC="DT"
            this.w_TOTORD = 0
            this.w_TOTDDT = this.w_VALMAG
          else
            this.w_TOTDDT = 0
            this.w_TOTORD = this.w_VALMAG
          endif
          * --- Esegua una WRITE per modificare i valori...
          * --- Write into TMPVEND1
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TMPVEND1_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TOTORD ="+cp_NullLink(cp_ToStrODBC(this.w_TOTORD),'TMPVEND1','TOTORD');
            +",TOTDDT ="+cp_NullLink(cp_ToStrODBC(this.w_TOTDDT),'TMPVEND1','TOTDDT');
            +",TOTFAT ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','TOTFAT');
            +",TOTEFF ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','TOTEFF');
            +",TOTPAP ="+cp_NullLink(cp_ToStrODBC(0),'TMPVEND1','TOTPAP');
                +i_ccchkf ;
            +" where ";
                +"CODCLI = "+cp_ToStrODBC(_Curs_TMPVEND1.CODCLI);
                +" and CLADOC = "+cp_ToStrODBC(_Curs_TMPVEND1.CLADOC);
                +" and CODVAL = "+cp_ToStrODBC(_Curs_TMPVEND1.CODVAL);
                +" and CAOAPE = "+cp_ToStrODBC(_Curs_TMPVEND1.CAOAPE);
                +" and DATDOC = "+cp_ToStrODBC(_Curs_TMPVEND1.DATDOC);
                +" and CODIVE = "+cp_ToStrODBC(_Curs_TMPVEND1.CODIVE);
                +" and CODIVA = "+cp_ToStrODBC(_Curs_TMPVEND1.CODIVA);
                +" and FLOMAG = "+cp_ToStrODBC(_Curs_TMPVEND1.FLOMAG);
                +" and DETT = "+cp_ToStrODBC(_Curs_TMPVEND1.DETT);
                   )
          else
            update (i_cTable) set;
                TOTORD = this.w_TOTORD;
                ,TOTDDT = this.w_TOTDDT;
                ,TOTFAT = 0;
                ,TOTEFF = 0;
                ,TOTPAP = 0;
                &i_ccchkf. ;
             where;
                CODCLI = _Curs_TMPVEND1.CODCLI;
                and CLADOC = _Curs_TMPVEND1.CLADOC;
                and CODVAL = _Curs_TMPVEND1.CODVAL;
                and CAOAPE = _Curs_TMPVEND1.CAOAPE;
                and DATDOC = _Curs_TMPVEND1.DATDOC;
                and CODIVE = _Curs_TMPVEND1.CODIVE;
                and CODIVA = _Curs_TMPVEND1.CODIVA;
                and FLOMAG = _Curs_TMPVEND1.FLOMAG;
                and DETT = _Curs_TMPVEND1.DETT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
      endcase
        select _Curs_TMPVEND1
        continue
      enddo
      use
    endif
    if this.pPRG<>"F"
      ah_Msg("Azzeramento archivio rischio...")
      * --- Try
      local bErr_049D7DC0
      bErr_049D7DC0=bTrsErr
      this.Try_049D7DC0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Generazione archivio rischio abbandonata")
        * --- Se qualcosa va storto annullo la modifica dell'anagrafica...
        if this.pPRG="A" And VarType( this.w_PADRE )<>"O"
          this.w_PADRE.EcpQuit()     
        endif
      endif
      bTrsErr=bTrsErr or bErr_049D7DC0
      * --- End
    else
      * --- Lancio il report
      vx_exec(""+alltrim(this.oParentObject.w_OQRY)+", "+alltrim(this.oParentObject.w_OREP)+"",this)
    endif
    * --- Cmq al termine elimino il temporaneo..
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
  endproc
  proc Try_049D7DC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Azzera Tabella Fidi
    if this.pPRG<>"A"
      * --- Se lanciato dall'anagrafica non posso eseguire Frasi Sql su se stesso
      if EMPTY(this.oParentObject.w_CODCLI)
        * --- Delete from SIT_FIDI
        i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        * --- Delete from SIT_FIDI
        i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"FICODCLI = "+cp_ToStrODBC(this.oParentObject.w_CODCLI);
                 )
        else
          delete from (i_cTable) where;
                FICODCLI = this.oParentObject.w_CODCLI;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      this.w_DELROW = i_Rows
    endif
    this.w_INSROW = 0
    * --- Select from gsve_ber_7
    do vq_exec with 'gsve_ber_7',this,'_Curs_gsve_ber_7','',.f.,.t.
    if used('_Curs_gsve_ber_7')
      select _Curs_gsve_ber_7
      locate for 1=1
      do while not(eof())
      this.w_CODCON = _Curs_gsve_ber_7.CODCLI
      ah_Msg("Aggiorno nuovi dati; cliente: %1",.T.,.F.,.F., this.w_CODCON)
      this.w_INSROW = this.w_INSROW + 1
      this.w_TOTPAP = NVL(_Curs_gsve_ber_7.TOTPAP,0)
      this.w_TOTEFF = NVL(_Curs_gsve_ber_7.TOTEFF,0)
      this.w_TOTEFO = NVL(_Curs_gsve_ber_7.TOTEFO,0)
      this.w_TOTORD = NVL(_Curs_gsve_ber_7.TOTORD,0)
      this.w_TOTDDT = NVL(_Curs_gsve_ber_7.TOTDDT,0)
      this.w_TOTFAT = NVL(_Curs_gsve_ber_7.TOTFAT,0)
      if this.pPRG="A" 
        * --- Se lanciato dall'anagrafica non posso eseguire Frasi Sql su se stesso
        this.w_PADRE = This.oParentObject
        this.w_PADRE.EcpEdit()     
        this.oParentObject.w_FIDATELA = this.oParentObject.w_DATRIF
        this.oParentObject.w_FIIMPPAP = this.w_TOTPAP
        this.oParentObject.w_FIIMPESC = this.w_TOTEFF
        this.oParentObject.w_FIIMPESO = this.w_TOTEFO
        this.oParentObject.w_FIIMPORD = this.w_TOTORD
        this.oParentObject.w_FIIMPDDT = this.w_TOTDDT
        this.oParentObject.w_FIIMPFAT = this.w_TOTFAT
      else
        * --- Try
        local bErr_0498E700
        bErr_0498E700=bTrsErr
        this.Try_0498E700()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into SIT_FIDI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SIT_FIDI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"FIDATELA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATRIF),'SIT_FIDI','FIDATELA');
            +",FIIMPPAP ="+cp_NullLink(cp_ToStrODBC(this.w_TOTPAP),'SIT_FIDI','FIIMPPAP');
            +",FIIMPESC ="+cp_NullLink(cp_ToStrODBC(this.w_TOTEFF),'SIT_FIDI','FIIMPESC');
            +",FIIMPESO ="+cp_NullLink(cp_ToStrODBC(this.w_TOTEFO),'SIT_FIDI','FIIMPESO');
            +",FIIMPORD ="+cp_NullLink(cp_ToStrODBC(this.w_TOTORD),'SIT_FIDI','FIIMPORD');
            +",FIIMPDDT ="+cp_NullLink(cp_ToStrODBC(this.w_TOTDDT),'SIT_FIDI','FIIMPDDT');
            +",FIIMPFAT ="+cp_NullLink(cp_ToStrODBC(this.w_TOTFAT),'SIT_FIDI','FIIMPFAT');
                +i_ccchkf ;
            +" where ";
                +"FICODCLI = "+cp_ToStrODBC(this.w_CODCON);
                   )
          else
            update (i_cTable) set;
                FIDATELA = this.oParentObject.w_DATRIF;
                ,FIIMPPAP = this.w_TOTPAP;
                ,FIIMPESC = this.w_TOTEFF;
                ,FIIMPESO = this.w_TOTEFO;
                ,FIIMPORD = this.w_TOTORD;
                ,FIIMPDDT = this.w_TOTDDT;
                ,FIIMPFAT = this.w_TOTFAT;
                &i_ccchkf. ;
             where;
                FICODCLI = this.w_CODCON;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_0498E700
        * --- End
      endif
        select _Curs_gsve_ber_7
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.pPRG<>"E" AND this.pPRG<>"P" 
      * --- Il messaggio di chiusura non viene mostrato con l'EURO-KIT
      if this.w_DELROW<>0
        ah_ErrorMsg("Generazione archivio rischio completata%0Eliminati %1%0Inseriti %2",,"", Alltrim(Str(this.w_DELROW)), Alltrim(Str(this.w_INSROW)) )
      else
        ah_ErrorMsg("Generazione archivio rischio completata%0Modificati %1",,"", Alltrim(Str(this.w_INSROW)) )
      endif
    endif
    if this.pPRG="A" 
      if VarType( this.w_PADRE )<>"O"
        * --- Se il cliente non movimenta pi� il rischio metto tutto a 0
        this.w_PADRE = This.oParentObject
        this.w_PADRE.EcpEdit()     
        this.oParentObject.w_FIDATELA = this.oParentObject.w_DATRIF
        this.oParentObject.w_FIIMPPAP = 0
        this.oParentObject.w_FIIMPESC = 0
        this.oParentObject.w_FIIMPESO = 0
        this.oParentObject.w_FIIMPORD = 0
        this.oParentObject.w_FIIMPDDT = 0
        this.oParentObject.w_FIIMPFAT = 0
      endif
      this.w_PADRE.bUpdated = .T.
      this.w_PADRE.EcpSave()     
    endif
    return
  proc Try_0498E700()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SIT_FIDI
    i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SIT_FIDI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FICODCLI"+",FIDATELA"+",FIIMPPAP"+",FIIMPESC"+",FIIMPESO"+",FIIMPORD"+",FIIMPDDT"+",FIIMPFAT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCON),'SIT_FIDI','FICODCLI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATRIF),'SIT_FIDI','FIDATELA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTPAP),'SIT_FIDI','FIIMPPAP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTEFF),'SIT_FIDI','FIIMPESC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTEFO),'SIT_FIDI','FIIMPESO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTORD),'SIT_FIDI','FIIMPORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTDDT),'SIT_FIDI','FIIMPDDT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTFAT),'SIT_FIDI','FIIMPFAT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FICODCLI',this.w_CODCON,'FIDATELA',this.oParentObject.w_DATRIF,'FIIMPPAP',this.w_TOTPAP,'FIIMPESC',this.w_TOTEFF,'FIIMPESO',this.w_TOTEFO,'FIIMPORD',this.w_TOTORD,'FIIMPDDT',this.w_TOTDDT,'FIIMPFAT',this.w_TOTFAT)
      insert into (i_cTable) (FICODCLI,FIDATELA,FIIMPPAP,FIIMPESC,FIIMPESO,FIIMPORD,FIIMPDDT,FIIMPFAT &i_ccchkf. );
         values (;
           this.w_CODCON;
           ,this.oParentObject.w_DATRIF;
           ,this.w_TOTPAP;
           ,this.w_TOTEFF;
           ,this.w_TOTEFO;
           ,this.w_TOTORD;
           ,this.w_TOTDDT;
           ,this.w_TOTFAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pPRG)
    this.pPRG=pPRG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='SIT_FIDI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='VOCIIVA'
    this.cWorkTables[7]='*TMPVEND1'
    this.cWorkTables[8]='*TMPVEND2'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_TMPVEND1')
      use in _Curs_TMPVEND1
    endif
    if used('_Curs_gsve_ber_7')
      use in _Curs_gsve_ber_7
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsve_ber
  proc SetCPToolbar()
                doDefault()
                this.oParentObject.oCpToolBar.b4.enabled=.f.
  endproc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPRG"
endproc
