* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_sim                                                        *
*              Analisi scaduto con interessi di mora                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_164]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-02                                                      *
* Last revis.: 2012-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_sim",oParentObject))

* --- Class definition
define class tgste_sim as StdForm
  Top    = 15
  Left   = 11

  * --- Standard Properties
  Width  = 567
  Height = 314
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-05"
  HelpContextID=9816983
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gste_sim"
  cComment = "Analisi scaduto con interessi di mora"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_DATRIL = ctod('  /  /  ')
  o_DATRIL = ctod('  /  /  ')
  w_FLSALD = space(1)
  w_SCAINI = ctod('  /  /  ')
  o_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_NUMINI = space(31)
  w_NUMFIN = space(31)
  w_NDOINI = 0
  w_ADOINI = space(10)
  w_DDOINI = ctod('  /  /  ')
  w_NDOFIN = 0
  w_ADOFIN = space(10)
  w_DDOFIN = ctod('  /  /  ')
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_DESCON = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_FLESIM = space(1)
  w_PAGRD = space(2)
  w_PAGBO = space(2)
  w_PAGRB = space(2)
  w_PAGRI = space(2)
  w_PAGCA = space(2)
  w_PAGMA = space(2)
  w_PARTSN = space(1)
  w_INTMIN = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_simPag1","gste_sim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATRIL_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='AZIENDA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSTE_BIM with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_sim
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_DATRIL=ctod("  /  /  ")
      .w_FLSALD=space(1)
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_NUMINI=space(31)
      .w_NUMFIN=space(31)
      .w_NDOINI=0
      .w_ADOINI=space(10)
      .w_DDOINI=ctod("  /  /  ")
      .w_NDOFIN=0
      .w_ADOFIN=space(10)
      .w_DDOFIN=ctod("  /  /  ")
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_DESCON=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .w_FLESIM=space(1)
      .w_PAGRD=space(2)
      .w_PAGBO=space(2)
      .w_PAGRB=space(2)
      .w_PAGRI=space(2)
      .w_PAGCA=space(2)
      .w_PAGMA=space(2)
      .w_PARTSN=space(1)
      .w_INTMIN=0
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_DATRIL = i_datsys
        .w_FLSALD = 'R'
        .w_SCAINI = i_datsys-365
        .w_SCAFIN = .w_DATRIL
        .w_OBTEST = iif(empty(.w_SCAINI),i_INIDAT,.w_SCAINI)
          .DoRTCalc(7,14,.f.)
        .w_TIPCON = 'C'
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODCON))
          .link_1_32('Full')
        endif
          .DoRTCalc(17,19,.f.)
        .w_PAGRD = 'RD'
        .w_PAGBO = 'BO'
        .w_PAGRB = 'RB'
        .w_PAGRI = 'RI'
        .w_PAGCA = 'CA'
        .w_PAGMA = 'MA'
      .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
    endwith
    this.DoRTCalc(26,27,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,4,.t.)
        if .o_DATRIL<>.w_DATRIL
            .w_SCAFIN = .w_DATRIL
        endif
        if .o_SCAINI<>.w_SCAINI
            .w_OBTEST = iif(empty(.w_SCAINI),i_INIDAT,.w_SCAINI)
        endif
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZINTMIN";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZINTMIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_INTMIN = NVL(_Link_.AZINTMIN,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_INTMIN = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLESIM,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLESIM,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLESIM,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLESIM,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_32'),i_cWhere,'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_SIM.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLESIM,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLESIM,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso, non gestito a partite, obsoleto o escluso dall'applicazione di interessi di mora")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLESIM,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLESIM,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLESIM,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANFLESIM,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_FLESIM = NVL(_Link_.ANFLESIM,space(1))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLESIM = space(1)
      this.w_PARTSN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND .w_FLESIM<>'S' And .w_PARTSN='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso, non gestito a partite, obsoleto o escluso dall'applicazione di interessi di mora")
        endif
        this.w_CODCON = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLESIM = space(1)
        this.w_PARTSN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATRIL_1_3.value==this.w_DATRIL)
      this.oPgFrm.Page1.oPag.oDATRIL_1_3.value=this.w_DATRIL
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSALD_1_5.RadioValue()==this.w_FLSALD)
      this.oPgFrm.Page1.oPag.oFLSALD_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAINI_1_7.value==this.w_SCAINI)
      this.oPgFrm.Page1.oPag.oSCAINI_1_7.value=this.w_SCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAFIN_1_9.value==this.w_SCAFIN)
      this.oPgFrm.Page1.oPag.oSCAFIN_1_9.value=this.w_SCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_12.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_12.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_14.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_14.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNDOINI_1_16.value==this.w_NDOINI)
      this.oPgFrm.Page1.oPag.oNDOINI_1_16.value=this.w_NDOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oADOINI_1_18.value==this.w_ADOINI)
      this.oPgFrm.Page1.oPag.oADOINI_1_18.value=this.w_ADOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDDOINI_1_20.value==this.w_DDOINI)
      this.oPgFrm.Page1.oPag.oDDOINI_1_20.value=this.w_DDOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNDOFIN_1_22.value==this.w_NDOFIN)
      this.oPgFrm.Page1.oPag.oNDOFIN_1_22.value=this.w_NDOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oADOFIN_1_24.value==this.w_ADOFIN)
      this.oPgFrm.Page1.oPag.oADOFIN_1_24.value=this.w_ADOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDDOFIN_1_26.value==this.w_DDOFIN)
      this.oPgFrm.Page1.oPag.oDDOFIN_1_26.value=this.w_DDOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_28.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_32.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_32.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_33.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_33.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRD_1_37.RadioValue()==this.w_PAGRD)
      this.oPgFrm.Page1.oPag.oPAGRD_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGBO_1_38.RadioValue()==this.w_PAGBO)
      this.oPgFrm.Page1.oPag.oPAGBO_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRB_1_39.RadioValue()==this.w_PAGRB)
      this.oPgFrm.Page1.oPag.oPAGRB_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRI_1_40.RadioValue()==this.w_PAGRI)
      this.oPgFrm.Page1.oPag.oPAGRI_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGCA_1_41.RadioValue()==this.w_PAGCA)
      this.oPgFrm.Page1.oPag.oPAGCA_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGMA_1_42.RadioValue()==this.w_PAGMA)
      this.oPgFrm.Page1.oPag.oPAGMA_1_42.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DATRIL)) or not((.w_datril>=.w_scafin) or (empty(.w_scafin))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATRIL_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DATRIL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data rilevazione � minore della scadenza finale")
          case   not((.w_scaini<=.w_scafin) or (empty(.w_scafin)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAINI_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La scadenza iniziale � maggiore della scadenza finale")
          case   not(((.w_scaini<=.w_scafin) or (empty(.w_scaini))) and (.w_datril>=.w_scafin))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La scadenza finale � minore della scadenza inizale o maggiore della data rilevazione")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND .w_FLESIM<>'S' And .w_PARTSN='S')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso, non gestito a partite, obsoleto o escluso dall'applicazione di interessi di mora")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATRIL = this.w_DATRIL
    this.o_SCAINI = this.w_SCAINI
    return

enddefine

* --- Define pages as container
define class tgste_simPag1 as StdContainer
  Width  = 563
  height = 314
  stdWidth  = 563
  stdheight = 314
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATRIL_1_3 as StdField with uid="YRGPNABOOX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATRIL", cQueryName = "DATRIL",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data rilevazione � minore della scadenza finale",;
    ToolTipText = "Data rilevazione",;
    HelpContextID = 243445450,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=120, Top=8

  func oDATRIL_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_datril>=.w_scafin) or (empty(.w_scafin)))
    endwith
    return bRes
  endfunc


  add object oFLSALD_1_5 as StdCombo with uid="OWEHCVYMJW",rtseq=3,rtrep=.f.,left=319,top=8,width=113,height=21;
    , ToolTipText = "Ricerca le scadenze di chiusura, solo parte aperta o tutte";
    , HelpContextID = 107197354;
    , cFormVar="w_FLSALD",RowSource=""+"Tutte,"+"Chiusure,"+"Solo parte aperta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSALD_1_5.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'C',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oFLSALD_1_5.GetRadio()
    this.Parent.oContained.w_FLSALD = this.RadioValue()
    return .t.
  endfunc

  func oFLSALD_1_5.SetRadio()
    this.Parent.oContained.w_FLSALD=trim(this.Parent.oContained.w_FLSALD)
    this.value = ;
      iif(this.Parent.oContained.w_FLSALD=='T',1,;
      iif(this.Parent.oContained.w_FLSALD=='C',2,;
      iif(this.Parent.oContained.w_FLSALD=='R',3,;
      0)))
  endfunc

  add object oSCAINI_1_7 as StdField with uid="BUACHACMJF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCAINI", cQueryName = "SCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La scadenza iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data iniziale scadenza da ricercare",;
    HelpContextID = 20765658,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=120, Top=38

  func oSCAINI_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_scaini<=.w_scafin) or (empty(.w_scafin)))
    endwith
    return bRes
  endfunc

  add object oSCAFIN_1_9 as StdField with uid="AVZCQICAXG",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SCAFIN", cQueryName = "SCAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La scadenza finale � minore della scadenza inizale o maggiore della data rilevazione",;
    ToolTipText = "Data finale scadenza da ricercare",;
    HelpContextID = 210754522,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=120, Top=68

  func oSCAFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_scaini<=.w_scafin) or (empty(.w_scaini))) and (.w_datril>=.w_scafin))
    endwith
    return bRes
  endfunc

  add object oNUMINI_1_12 as StdField with uid="JPZWSXEPCG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 1, value=space(31), bMultilanguage =  .f.,;
    ToolTipText = "Numero iniziale partita da ricercare",;
    HelpContextID = 20711978,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=319, Top=38, InputMask=replicate('X',31)

  add object oNUMFIN_1_14 as StdField with uid="PDJGLOUCJU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 1, value=space(31), bMultilanguage =  .f.,;
    ToolTipText = "Numero finale partita da ricercare",;
    HelpContextID = 210700842,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=319, Top=68, InputMask=replicate('X',31)

  add object oNDOINI_1_16 as StdField with uid="MFFRYELHLU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_NDOINI", cQueryName = "NDOINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 20708138,;
   bGlobalFont=.t.,;
    Height=21, Width=129, Left=120, Top=98, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOINI_1_18 as StdField with uid="LFYMWAETPI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ADOINI", cQueryName = "ADOINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di inizio selezione",;
    HelpContextID = 20708346,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=265, Top=98, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDDOINI_1_20 as StdField with uid="GVHSMJJJQT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DDOINI", cQueryName = "DDOINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento inizio selezione",;
    HelpContextID = 20708298,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=470, Top=98

  add object oNDOFIN_1_22 as StdField with uid="YVXOAZLBNP",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NDOFIN", cQueryName = "NDOFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 210697002,;
   bGlobalFont=.t.,;
    Height=21, Width=129, Left=120, Top=128, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOFIN_1_24 as StdField with uid="WVYMVOQZXQ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ADOFIN", cQueryName = "ADOFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di fine selezione",;
    HelpContextID = 210697210,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=265, Top=128, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDDOFIN_1_26 as StdField with uid="YKXKJINOJS",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DDOFIN", cQueryName = "DDOFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento fine selezione",;
    HelpContextID = 210697162,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=470, Top=128


  add object oTIPCON_1_28 as StdCombo with uid="WDXDHXNHSV",rtseq=15,rtrep=.f.,left=120,top=158,width=92,height=21;
    , ToolTipText = "Tipo partite/scadenze da selezionare, clienti, fornitori, conti generici";
    , HelpContextID = 204596682;
    , cFormVar="w_TIPCON",RowSource=""+"Clienti,"+"Fornitori,"+"Conti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_28.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oTIPCON_1_28.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_28.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      iif(this.Parent.oContained.w_TIPCON=='G',3,;
      0)))
  endfunc

  func oTIPCON_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_1_32('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCON_1_32 as StdField with uid="ICXFDOKTCK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso, non gestito a partite, obsoleto o escluso dall'applicazione di interessi di mora",;
    ToolTipText = "Cliente/fornitore/conto di selezione scadenze",;
    HelpContextID = 204644570,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=120, Top=186, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_SIM.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_33 as StdField with uid="XZILIWPJAW",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 204585674,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=260, Top=186, InputMask=replicate('X',40)

  add object oPAGRD_1_37 as StdCheck with uid="WYJEWFDOLH",rtseq=20,rtrep=.f.,left=120, top=216, caption="Rimessa diretta",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti con rimessa diretta",;
    HelpContextID = 86802934,;
    cFormVar="w_PAGRD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRD_1_37.RadioValue()
    return(iif(this.value =1,'RD',;
    'XX'))
  endfunc
  func oPAGRD_1_37.GetRadio()
    this.Parent.oContained.w_PAGRD = this.RadioValue()
    return .t.
  endfunc

  func oPAGRD_1_37.SetRadio()
    this.Parent.oContained.w_PAGRD=trim(this.Parent.oContained.w_PAGRD)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRD=='RD',1,;
      0)
  endfunc

  add object oPAGBO_1_38 as StdCheck with uid="UHUFUMTJOS",rtseq=21,rtrep=.f.,left=120, top=236, caption="Bonifico",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite bonifico",;
    HelpContextID = 97288694,;
    cFormVar="w_PAGBO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGBO_1_38.RadioValue()
    return(iif(this.value =1,'BO',;
    'XX'))
  endfunc
  func oPAGBO_1_38.GetRadio()
    this.Parent.oContained.w_PAGBO = this.RadioValue()
    return .t.
  endfunc

  func oPAGBO_1_38.SetRadio()
    this.Parent.oContained.w_PAGBO=trim(this.Parent.oContained.w_PAGBO)
    this.value = ;
      iif(this.Parent.oContained.w_PAGBO=='BO',1,;
      0)
  endfunc

  add object oPAGRB_1_39 as StdCheck with uid="LJJBJQGWOI",rtseq=22,rtrep=.f.,left=120, top=256, caption="Ric.bancaria/Ri.Ba.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite ricevuta bancaria o RiBa",;
    HelpContextID = 84705782,;
    cFormVar="w_PAGRB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRB_1_39.RadioValue()
    return(iif(this.value =1,'RB',;
    'XX'))
  endfunc
  func oPAGRB_1_39.GetRadio()
    this.Parent.oContained.w_PAGRB = this.RadioValue()
    return .t.
  endfunc

  func oPAGRB_1_39.SetRadio()
    this.Parent.oContained.w_PAGRB=trim(this.Parent.oContained.w_PAGRB)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRB=='RB',1,;
      0)
  endfunc

  add object oPAGRI_1_40 as StdCheck with uid="CIJZFYBCCS",rtseq=23,rtrep=.f.,left=308, top=216, caption="R.I.D.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite R.I.D.",;
    HelpContextID = 92045814,;
    cFormVar="w_PAGRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRI_1_40.RadioValue()
    return(iif(this.value =1,'RI',;
    'XX'))
  endfunc
  func oPAGRI_1_40.GetRadio()
    this.Parent.oContained.w_PAGRI = this.RadioValue()
    return .t.
  endfunc

  func oPAGRI_1_40.SetRadio()
    this.Parent.oContained.w_PAGRI=trim(this.Parent.oContained.w_PAGRI)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRI=='RI',1,;
      0)
  endfunc

  add object oPAGCA_1_41 as StdCheck with uid="WMQWIJEKXN",rtseq=24,rtrep=.f.,left=308, top=236, caption="Cambiale",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite cambiale",;
    HelpContextID = 82674166,;
    cFormVar="w_PAGCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGCA_1_41.RadioValue()
    return(iif(this.value =1,'CA',;
    'XX'))
  endfunc
  func oPAGCA_1_41.GetRadio()
    this.Parent.oContained.w_PAGCA = this.RadioValue()
    return .t.
  endfunc

  func oPAGCA_1_41.SetRadio()
    this.Parent.oContained.w_PAGCA=trim(this.Parent.oContained.w_PAGCA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGCA=='CA',1,;
      0)
  endfunc

  add object oPAGMA_1_42 as StdCheck with uid="OXYWDXMXXW",rtseq=25,rtrep=.f.,left=308, top=256, caption="M.AV.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite M.AV. (mediante avviso)",;
    HelpContextID = 83329526,;
    cFormVar="w_PAGMA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGMA_1_42.RadioValue()
    return(iif(this.value =1,'MA',;
    'XX'))
  endfunc
  func oPAGMA_1_42.GetRadio()
    this.Parent.oContained.w_PAGMA = this.RadioValue()
    return .t.
  endfunc

  func oPAGMA_1_42.SetRadio()
    this.Parent.oContained.w_PAGMA=trim(this.Parent.oContained.w_PAGMA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGMA=='MA',1,;
      0)
  endfunc


  add object oBtn_1_43 as StdButton with uid="SQNLIXYMPH",left=457, top=263, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 9845734;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      with this.Parent.oContained
        do GSTE_BIM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_44 as StdButton with uid="VJEYGWVSZN",left=507, top=263, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 17134406;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_44.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_47 as cp_outputCombo with uid="ZLERXIVPWT",left=120, top=280, width=283,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cNoDefSep="",cDefSep="",;
    nPag=1;
    , HelpContextID = 80620826

  add object oStr_1_2 as StdString with uid="MUSXKNBEEC",Visible=.t., Left=19, Top=8,;
    Alignment=1, Width=99, Height=15,;
    Caption="Data rilevazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="VGFVDFYFIT",Visible=.t., Left=206, Top=8,;
    Alignment=1, Width=110, Height=15,;
    Caption="Test saldate:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="HRHDNGMHAE",Visible=.t., Left=20, Top=38,;
    Alignment=1, Width=99, Height=15,;
    Caption="Scadenze da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="PMYSREYHZS",Visible=.t., Left=20, Top=68,;
    Alignment=1, Width=99, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="NVBJZFZWIX",Visible=.t., Left=233, Top=38,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="BHEBGIAIYV",Visible=.t., Left=241, Top=68,;
    Alignment=1, Width=75, Height=18,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="HPKOJQQYXJ",Visible=.t., Left=6, Top=98,;
    Alignment=1, Width=112, Height=15,;
    Caption="Da n. documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="OJNPKZHYTH",Visible=.t., Left=253, Top=98,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="LLRZQJNAKN",Visible=.t., Left=440, Top=98,;
    Alignment=1, Width=28, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="XNNPVHYWCF",Visible=.t., Left=12, Top=128,;
    Alignment=1, Width=106, Height=15,;
    Caption="A n. documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="JTWYBEKTDW",Visible=.t., Left=253, Top=128,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="HHLBYQEEYA",Visible=.t., Left=440, Top=128,;
    Alignment=1, Width=28, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="DKKXSABCFP",Visible=.t., Left=20, Top=158,;
    Alignment=1, Width=99, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="MREIEMXUSI",Visible=.t., Left=20, Top=186,;
    Alignment=1, Width=99, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'G')
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="VNGFGUEZQD",Visible=.t., Left=20, Top=186,;
    Alignment=1, Width=99, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="SOQNUXLCBD",Visible=.t., Left=20, Top=186,;
    Alignment=1, Width=99, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'F')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="IBQAYEAHFT",Visible=.t., Left=20, Top=216,;
    Alignment=1, Width=99, Height=15,;
    Caption="Pagamenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="WCAEIDQCKM",Visible=.t., Left=20, Top=280,;
    Alignment=1, Width=99, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_sim','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
