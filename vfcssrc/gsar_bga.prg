* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bga                                                        *
*              GESTIONE CONTRIBUTI ACCESSORI CICLO DOCUMENTALE                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-23                                                      *
* Last revis.: 2016-07-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo,pPadre,pRowNum,pRowOrd,pStep,w_RESULT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bga",oParentObject,m.pTipo,m.pPadre,m.pRowNum,m.pRowOrd,m.pStep,m.w_RESULT)
return(i_retval)

define class tgsar_bga as StdBatch
  * --- Local variables
  pTipo = space(3)
  pPadre = .NULL.
  pRowNum = 0
  pRowOrd = 0
  pStep = 0
  w_RESULT = space(1)
  w_PADRE = .NULL.
  w_ROWINDEX = 0
  w_DATEVA = ctod("  /  /  ")
  w_CODART = space(20)
  w_CODIVA = space(5)
  w_UNIMIS = space(3)
  w_QTAMOV = 0
  w_PREZZO = 0
  w_PERIVA = 0
  w_FLFRAZ = space(1)
  w_OBJCTRL = .NULL.
  w_CHECK = .f.
  w_KCODICE = space(41)
  w_CATCON = space(0)
  w_ORECO = 0
  w_RIGHEGIAAGGIORNATE = space(10)
  * --- WorkFile variables
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione esplosione documentale contributi accessori
    *     =======================================
    *     PARAMETRI, la gestione, popolando la movimentazione dei documenti
    *     si aspetta che w_PADRE sia la gestione documentale.
    *     pTipo: Tipo esplosione (ROW / DTL )
    *     pPadre: Riferimento all'oggetto master/detail da aggiornare
    *     pRownum - Riga padre dell'esplosione (se pTipo=ROW)
    *     pRowOrd - Numero riga (utilizzato per determinare il numero di riga del primo contributo inserito
    *     pStep - Step da applicare al numero di riga, se non specificato o 0 allora 1
    *     w_RESULT - Se i contributi vanno inseriti o vanno aggiornati sul dettaglio (I o U)
    if Vartype( this.pStep )<>"N" Or this.pStep=0
      this.pStep = 10
    endif
    this.w_PADRE = this.pPadre
    * --- Logica di elaborazione:
    *     ==========================
    *     Se modifico quantit�, prezzo e codice IVA vado in agiornamento (pUpdate=.t.) se gia
    *     presente un'esplosione, altrimenti la routine come primo passo elimina le righe del dettaglio che
    *     fanno riferimento alla riga padre (pRowNum)
    *     
    *     Successivamente, la routine lancia la routine di esplosione dei contributi accessori
    *     
    *     Infine, dal cursore risultato, va a popolare il dettaglio aggiungendo righe in coda al transitorio
    if this.pTipo="ROW"
      if (this.w_RESULT="I" Or this.w_RESULT="U") And this.w_PADRE.w_MVQTAMOV<>0
        * --- La routine si deve riposizionare alla riga padre, per eviatare che la routine di controllo
        *     salti le righe appena create...
        this.w_ROWINDEX = this.w_PADRE.RowIndex()
        if VARTYPE( this.w_PADRE.w_MVDATEVA ) <> "U"
          this.w_DATEVA = NVL( this.w_PADRE.w_MVDATEVA , CP_CHARTODATE( "  -  -  " ) )
        endif
        GSAR_BEG(this,this.w_PADRE.w_MVCODICE, this.w_PADRE.w_MVTIPCON, iif( empty( nvl( this.w_PADRE.w_MVCODORN , Space(15) ) ) , this.w_PADRE.w_MVCODCON , nvl( this.w_PADRE.w_MVCODORN , Space(15) )), this.w_PADRE.w_MVDATDOC, this.w_PADRE.w_MVCODIVA,this.w_PADRE.w_MVUNIMIS ,this.w_PADRE.w_MVQTAMOV, this.w_PADRE.w_MVVALRIG / this.w_PADRE.w_MVQTAMOV, this.w_PADRE.w_MVCODVAL, iif(this.w_PADRE.w_MVFLSCOR="S",this.w_PADRE.w_PERIVA,0), this.w_PADRE.w_FLFRAZ, "ACC", "CURESPL")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_ROWINDEX<>this.w_PADRE.RowIndex()
          * --- Mi riposiziono nella riga di partenza...
          this.w_PADRE.SetRow(this.w_ROWINDEX)     
        endif
      endif
    else
      * --- Dal dettaglio estraggo le righe con le 3 S finali in w_TESTESPLACC
      *     (GSAR_BGA se lanciata significa che i controlli sulla avvenuta modifica del
      *     documento sono stati gia svolti)
      if this.pTipo="GEN"
        if g_APPLICATION = "ADHOC REVOLUTION"
          * --- Eseguo un raggruppamento sul cursore GENEAPP
           
 Select t_MVCODICE, t_MVCODIVA, t_MVUNIMIS, t_MVVALRIG / t_MVQTAMOV as t_MVPREZZO,Sum(t_MVQTAMOV) As t_MVQTAMOV,; 
 Min(t_PERIVA) as t_PERIVA, Min("X") AS t_FLRAZ From GeneApp ; 
 Where t_MVQTAMOV<>0 And Right(alltrim(t_TESTESPLACC),3)="SSS" Group By t_MVCODICE, t_MVCODIVA, t_MVUNIMIS, t_MVPREZZO ; 
 Into Cursor CurDett NoFilter
        else
          * --- Lanciata dal generatore...
          VQ_EXEC("QUERY\GSVE_BGE1.VQR", this.w_PADRE ,"CurDett")
        endif
      else
        this.w_PADRE.Exec_Select("CurDett", "t_MVCODICE, t_MVCODIVA, t_MVUNIMIS,  t_MVVALRIG / t_MVQTAMOV as t_MVPREZZO,Sum(t_MVQTAMOV) As t_MVQTAMOV,Min(t_PERIVA) as t_PERIVA, Min(t_FLFRAZ) AS t_FLRAZ " , " t_MVQTAMOV<>0 And Right(alltrim(t_TESTESPLACC),3)='SSS'" ,"","t_MVCODICE, t_MVCODIVA, t_MVUNIMIS,  t_MVPREZZO","")     
      endif
      Select CurDett 
 Go top
      do while (.not. eof( "CurDett" ))
        this.w_CODART = CurDett.t_MVCODICE
        this.w_CODIVA = CurDett.t_MVCODIVA
        this.w_UNIMIS = CurDett.t_MVUNIMIS
        this.w_QTAMOV = Nvl( CurDett.t_MVQTAMOV, 0 )
        this.w_PREZZO = Nvl( CurDett.t_MVPREZZO, 0 )
        this.w_PERIVA = Nvl( CurDett.t_PERIVA, 0 )
        this.w_FLFRAZ = Nvl( CurDett.t_FLRAZ, 0 )
        if this.w_FLFRAZ="X"
          * --- Se 'X' arriva da generazione fatture differite sul cui transitoio non ho
          *     se l'unit� di miusra � frazionabile
          * --- Read from UNIMIS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.UNIMIS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "UMFLFRAZ"+;
              " from "+i_cTable+" UNIMIS where ";
                  +"UMCODICE = "+cp_ToStrODBC(this.w_UNIMIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              UMFLFRAZ;
              from (i_cTable) where;
                  UMCODICE = this.w_UNIMIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        GSAR_BEG(this,this.w_CODART, iif( g_APPLICATION = "ADHOC REVOLUTION" and type("this.w_PADRE.oParentobject.w_MVTIPCON")="C" , this.w_PADRE.oParentobject.w_MVTIPCON , this.w_PADRE.w_MVTIPCON), iif( empty( nvl( this.w_PADRE.w_MVCODORN , Space(15) ) ) , this.w_PADRE.w_MVCODCON , nvl( this.w_PADRE.w_MVCODORN , Space(15) )), this.w_PADRE.w_MVDATDOC, this.w_CODIVA,this.w_UNIMIS ,this.w_QTAMOV, this.w_PREZZO, this.w_PADRE.w_MVCODVAL, IIF(this.w_PADRE.w_MVFLSCOR="S" ,this.w_PERIVA,0), this.w_FLFRAZ, "PES", "CURESPL_PESO")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Il risultato dell'esplosione lo accodo all'attuale responso..
        if Used( "CurEspl_1" )
           
 Insert into CurEspl_1 ; 
 Select * From CURESPL_PESO
        else
           
 Select * From CURESPL_PESO Into Cursor CurEspl_1 Nofilter 
 =WrCursor("CurEspl_1")
        endif
         
 Select( "CURESPL_PESO") 
 Use 
 Select CurDett
        if Not Eof("CurDett")
          Skip
        endif
      enddo
      * --- Rimuovo il cursore con il dettaglio...
       
 Select CurDett 
 Use
      if Used("CurEspl_1")
        * --- Raggruppo per articolo, categoria, unit� di misura, codice IVA e prezzo e sommo le quantit�
        Select TIPO, CODICE, Sum(QTAMOV) As QTAMOV, UNIMIS, PREZZO, CODIVA, CATCON From CURESPL_1 Group By TIPO,CODICE,UNIMIS,PREZZO,CODIVA,CATCON Into Cursor CurEspl Nofilter 
 =WrCursor("CurEspl")
        Select( "CurEspl_1" ) 
 Use
        if this.pTipo="DTL"
          * --- Popolo il dettaglio con le righe recuperate...
          *     a peso devo gestire le righe senza riferimento in caso di modifica...
          this.pRowNum = -1
          this.w_ROWINDEX = this.w_PADRE.RowIndex()
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_ROWINDEX<>this.w_PADRE.RowIndex()
            * --- Mi riposiziono nella riga di partenza...
            this.w_PADRE.SetRow(this.w_ROWINDEX)     
          endif
        endif
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Righe Documento
    if this.w_RESULT="I"
      * --- Se devo caricare i contributi ex novo...
      this.w_PADRE.AddRow()     
      this.w_PADRE.SaveDependsOn()     
      if this.pTipo="ROW"
        * --- Se esplosione di riga forzo il numero di riga al succesivo dell'articolo padre
        this.pRowOrd = this.pRowOrd + this.pStep
        * --- Riparte sempre dal numero riga del padre...
        this.w_PADRE.Set("w_CPROWORD" , this.pRowOrd, .T. , .T.)     
      endif
      * --- Contributo accessori ( MVCACONT pieno discrimina l'esecuzioen o meno
      *     di pezzi di codice
      this.w_PADRE.Set("w_MVRIFCAC" , this.pRowNum, .T. ,.T.)     
      this.w_PADRE.Set("w_MVCACONT" , CURESPL.CATCON, .T. ,.T.)     
      * --- Carica il Temporaneo dei Dati e skippa al record successivo
      this.w_PADRE.Set("w_MVCODICE" , CURESPL.CODICE, .T. , .T.)     
      this.w_OBJCTRL = this.w_PADRE.GetBodyCtrl( "w_MVCODICE" )
      L_Method_Name="this.w_PADRE.Link"+Right( this.w_OBJCTRL.Name , LEN( this.w_OBJCTRL.Name ) - rat("_" , this.w_OBJCTRL.Name , 2)+1)+"('Full',this)"
      this.w_CHECK = &L_Method_Name
      this.w_OBJCTRL = Null
      this.w_PADRE.NotifyEvent("w_MVCODICE Changed")     
      this.w_PADRE.mCalc(.T.)     
      this.w_PADRE.SetupdateRow()     
    else
      this.w_PADRE.SaveDependsOn()     
    endif
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    = setvaluelinked ( "M" , this.w_PADRE , "w_MVCODIVA" , CURESPL.CODIVA)
    this.w_PADRE.Set("w_MVUNIMIS" , CURESPL.UNIMIS, .T. , .T.)     
    this.w_OBJCTRL = this.w_PADRE.GetBodyCtrl( "w_MVUNIMIS" )
    L_Method_Name="this.w_PADRE.Link"+Right( this.w_OBJCTRL.Name , LEN( this.w_OBJCTRL.Name ) - rat("_" , this.w_OBJCTRL.Name , 2)+1)+"('Full',this)"
    this.w_CHECK = &L_Method_Name
    this.w_OBJCTRL = Null
    this.w_PADRE.NotifyEvent("w_MVUNIMIS Changed")     
    this.w_PADRE.mCalc(.T.)     
    this.w_PADRE.SetupdateRow()     
    * --- GSVE_BIA chiama la Repos che mi riporta alla prima riga...
    = setvaluelinked ( "D" , this.w_PADRE , "w_MVQTAMOV" , CURESPL.QTAMOV )
    = setvaluelinked ( "D" , this.w_PADRE , "w_MVPREZZO" , CURESPL.PREZZO )
    if this.pTipo="ROW" AND NOT EMPTY( this.w_DATEVA )
      * --- Assegna al contributo la data di prevista evasione del padre
      this.w_PADRE.Set("w_MVDATEVA" , this.w_DATEVA, .T. , .T.)     
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Select CurEspl 
 Go top
    this.w_RIGHEGIAAGGIORNATE = ""
    do while (.not. eof( "CurEspl" ))
      this.w_KCODICE = CurEspl.CODICE
      if this.w_RESULT="U"
        * --- l'articolo � gi� stato esploso
        * --- Contributo accessori, un servizio per il contributo pu� essere presente pi� volte
        this.w_CATCON = CurEspl.CATCON
        this.w_ORECO = 0
        * --- Per sapere se una riga � gi� stata aggiornata, si utilizza la variabile
        *     w_righegiaaggiornate
        *     Ogni volta che si aggiorna una riga, la stessa viene accodata nella variabile w_righegiaaggiornate (nell'istr. IF W_ORECO <> -1 )
        *     Ogni volta che si cerca una riga (nel seguente ciclo while), si controlla che la riga stessa non sia gi� inserita in w_righegiaaggiornate
        *     (e in questo caso si continua a cercare un'altra riga con le stesse caratteristiche)
        do while "|"+ALLTRIM( STR( this.w_ORECO ) ) $ this.w_RIGHEGIAAGGIORNATE OR this.w_ORECO = 0
          this.w_ORECO = this.w_PADRE.Search("t_MVRIFCAC ="+ cp_ToStr( this.pRowNum )+"And t_MVCODICE="+cp_ToStr(this.w_KCODICE) +"And t_MVCACONT="+cp_ToStr(this.w_CATCON),this.w_ORECO)
        enddo
        if this.w_ORECO <> -1
          this.w_RIGHEGIAAGGIORNATE = this.w_RIGHEGIAAGGIORNATE+"|"+ ALLTRIM( STR( this.w_ORECO ) )
          * --- Sposto il focus sul numero di riga...
          *     x evitare problemi nel caso di spostamenti (Repos) che provocano il refresh
          *     e quindi la potenziale chiamata ad un valid su di un control avendo sotto una riga diversa
          this.w_PADRE.SetRow(this.w_ORECO)     
          ah_msg( "Aggiorna contributi: %1",.t.,.f.,.f.,this.w_PADRE.w_MVCODICE )
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Carica il Temporaneo
          this.w_PADRE.SaveRow()     
        endif
      else
        * --- Nuova esplosione
        ah_msg( "Inserisce contributo: %1",.t.,.f.,.f.,this.w_PADRE.w_MVCODICE )
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Carica il Temporaneo
        this.w_PADRE.SaveRow()     
      endif
      Select CurEspl
      if Not Eof("CurEspl")
        Skip
      endif
    enddo
     
 Select CurEspl 
 Use
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo,pPadre,pRowNum,pRowOrd,pStep,w_RESULT)
    this.pTipo=pTipo
    this.pPadre=pPadre
    this.pRowNum=pRowNum
    this.pRowOrd=pRowOrd
    this.pStep=pStep
    this.w_RESULT=w_RESULT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='UNIMIS'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo,pPadre,pRowNum,pRowOrd,pStep,w_RESULT"
endproc
