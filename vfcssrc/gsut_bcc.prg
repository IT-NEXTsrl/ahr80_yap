* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bcc                                                        *
*              Maschera cifratura CNF                                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_323]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-06                                                      *
* Last revis.: 2009-11-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Scrittura
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bcc",oParentObject,m.w_Scrittura)
return(i_retval)

define class tgsut_bcc as StdBatch
  * --- Local variables
  w_Scrittura = space(1)
  w_Continue = .f.
  w_APICESINGOLO = .f.
  w_FileAperto = space(250)
  w_APICEDOPPIO = .f.
  w_CtrlStrIn = space(250)
  w_CtrlStrOut = space(250)
  w_IDRigaLetta = 0
  w_FileTop = 0
  w_FileEnd = 0
  w_FileHandle = 0
  w_StrLetta = space(0)
  w_RESULT = space(250)
  w_stato = .f.
  w_FileWriteHandle = 0
  w_LocAppRole = space(250)
  w_LocAppPass = space(250)
  w_LocConn = space(250)
  w_PosConnWr = .f.
  w_PosCryptWr = .f.
  w_WritedDBType = .f.
  w_PosAppRoleWr = .f.
  w_PosAppPWWr = .f.
  w_Temp = space(250)
  w_Valore = space(250)
  w_CharAtt = 0
  w_Ultimo = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- w_Scrittura Seleziona la modalit� operativa del batch
    *     S=Batch richimato in scrittura del CNF
    *     M=Batch richiamato a seguito dell'inserimento (o aggiornamento) manuale del file da caricare
    *     G=Batch richiamato a seguito della GETFILE per selezionare il CNF da leggere
    if this.w_Scrittura="S"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      if this.w_Scrittura="M"
        this.w_FileAperto = this.oParentObject.w_CNFIn
      else
        this.w_FileAperto = GETFILE("CNF",ah_Msgformat("CNF selezionato"),ah_Msgformat("Seleziona"),0,ah_msgformat("Seleziona CNF"))
      endif
      if NOT EMPTY(this.w_FileAperto)
        this.oParentObject.w_CNFIn = ""
        this.oParentObject.w_TIPCRYPTAR = ""
        this.oParentObject.w_TIPODB = ""
        this.oParentObject.w_PosDBType = 0
        this.oParentObject.w_PosConn = 0
        this.oParentObject.w_PosCrypt = 0
        this.oParentObject.w_PosAppRole = 0
        this.oParentObject.w_PosAppPW = 0
        this.oParentObject.w_CONN = ""
        this.oParentObject.w_CIFRCONN = ""
        this.oParentObject.w_AppRole = ""
        this.oParentObject.w_CIFRAppRole = ""
        this.oParentObject.w_PASS = ""
        this.oParentObject.w_CIFRPASS = ""
        this.oParentObject.w_FLGEN = .F.
        if this.w_Scrittura<>"M"
          this.oParentObject.w_CNFOUT = this.oParentObject.w_CNFIn
        else
          this.oParentObject.w_CNFOUT = ""
        endif
        this.oParentObject.w_CNFIn = this.w_FileAperto
        this.w_IDRigaLetta = 0
        * --- Apre il file
        this.w_FileHandle = FOPEN(this.oParentObject.w_CNFIn)
        * --- Posiziono il punatore prima alla fine del file e poi all'inizio, cos� posso
        *     verificare che il file esiste e non � vuoto
        this.w_FileEnd = FSEEK(this.w_FileHandle, 0, 2)
        this.w_FileTop = FSEEK(this.w_FileHandle, 0)
        * --- Imposto la variabile a database scoosciuto per prevenire gli errori
        *     derivanti da CNF inesistenti, vuoti o non validi
        *     Es. Manca la stringa CP_DBTYPE
        this.oParentObject.w_TIPODB="UNKNOWN"
        if this.w_FileEnd<=0
          ah_ErrorMsg("Il file di configurazione inserito � inesistente o vuoto","!","")
          this.oParentObject.w_CNFIn = ""
        else
          do while NOT FEOF(this.w_FileHandle)
            this.w_StrLetta = FGETS(this.w_FileHandle, this.w_FileEnd) 
            this.w_StrLetta = ALLTRIM(this.w_StrLetta)
            this.w_IDRigaLetta = this.w_IDRigaLetta+1
            if (LEFTC(this.w_StrLetta,1)<>"*" AND LEFTC(this.w_StrLetta,2)<>"&"+"&" )
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              do case
                case UPPER(this.w_result)="CP_DBTYPE"
                  this.Pag3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  this.oParentObject.w_TIPODB = this.w_valore
                  this.oParentObject.w_PosDBType = this.w_IDRigaLetta
                case UPPER(this.w_result)=="G_CRYPT"
                  this.Pag3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  this.oParentObject.w_TIPCRYPTAR = this.w_valore
                  this.oParentObject.w_PosCrypt = this.w_IDRigaLetta
                case UPPER(this.w_result)="G_APPROLE"
                  this.Pag3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  this.w_LocAppRole = this.w_valore
                  this.oParentObject.w_PosAppRole = this.w_IDRigaLetta
                case UPPER(this.w_result)="G_APPPSW"
                  this.Pag3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  this.w_LocAppPass = this.w_valore
                  this.oParentObject.w_PosAppPW = this.w_IDRigaLetta
                case UPPER(this.w_result)="CP_ODBCCONN"
                  this.Pag3()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  this.w_LocConn = this.w_valore
                  this.oParentObject.w_PosConn = this.w_IDRigaLetta
              endcase
            else
              if this.w_StrLetta=ah_Msgformat("* Generato tramite cifratura CNF")
                this.oParentObject.w_FLGEN = .T.
              endif
            endif
          enddo
        endif
        FCLOSE(this.w_FileHandle) 
        if this.oParentObject.w_TIPODB<>"SQLServer" AND this.oParentObject.w_TIPCRYPTAR="A"
          ah_ErrorMsg("Il database selezionato nel CNF (%1) non � SQL server, � quindi impossibile selezionare come criterio di protezione Application Role","stop","",this.oParentObject.w_TipoDB)
          this.oParentObject.w_TIPCRYPTAR = ""
          this.oParentObject.w_CONN = this.w_LocConn
          this.oParentObject.w_AppRole = this.w_LocAppRole
          this.oParentObject.w_PASS = this.w_LocAppPass
        else
          do case
            case this.oParentObject.w_TipCryptAR="N"
              this.oParentObject.w_CONN = this.w_LocConn
              this.oParentObject.w_AppRole = ""
              this.oParentObject.w_PASS = ""
            case this.oParentObject.w_TipCryptAR="A"
              this.oParentObject.w_CONN = this.w_LocConn
              this.oParentObject.w_CIFRAppRole = this.w_LocAppRole
              this.oParentObject.w_CIFRPASS = this.w_LocAppPass
            case this.oParentObject.w_TipCryptAR="C"
              this.oParentObject.w_CIFRCONN = this.w_LocConn
              this.oParentObject.w_AppRole = ""
              this.oParentObject.w_PASS = ""
            case EMPTY(this.oParentObject.w_TipCryptAR)
              this.oParentObject.w_CONN = this.w_LocConn
              this.oParentObject.w_AppRole = ""
              this.oParentObject.w_PASS = ""
          endcase
        endif
        if NOT EMPTY(this.oParentObject.w_TipCryptAR)
          if EMPTY(this.oParentObject.w_CONN) AND NOT EMPTY(this.oParentObject.w_CIFRCONN)
            this.oParentObject.w_CONN = CifraCNF( ALLTRIM(this.oParentObject.w_CIFRCONN) , "D" )
          endif
          if NOT EMPTY(this.oParentObject.w_CONN) AND EMPTY(this.oParentObject.w_CIFRCONN)
            this.oParentObject.w_CIFRCONN = CifraCNF( ALLTRIM(this.oParentObject.w_CONN) , "C" )
          endif
          if EMPTY(this.oParentObject.w_AppRole) AND NOT EMPTY(this.oParentObject.w_CIFRAPPROLE)
            this.oParentObject.w_AppRole = CifraCNF(ALLTRIM(this.oParentObject.w_CIFRAPPROLE),"D")
          endif
          if NOT EMPTY(this.oParentObject.w_AppRole) AND EMPTY(this.oParentObject.w_CIFRAPPROLE)
            this.oParentObject.w_CifrAppRole = CifraCNF(ALLTRIM(this.oParentObject.w_APPROLE),"C")
          endif
          if EMPTY(this.oParentObject.w_Pass) AND NOT EMPTY(this.oParentObject.w_CIFRPass)
            this.oParentObject.w_Pass = CifraCNF(ALLTRIM(this.oParentObject.w_CIFRPass),"D")
          endif
          if NOT EMPTY(this.oParentObject.w_Pass) AND EMPTY(this.oParentObject.w_CIFRPass)
            this.oParentObject.w_CifrPass = CifraCNF(ALLTRIM(this.oParentObject.w_Pass),"C")
          endif
        else
          if NOT EMPTY(this.oParentObject.w_CONN)
            this.oParentObject.w_TIPCRYPTAR = "C"
            this.oParentObject.w_CIFRCONN = CifraCNF( ALLTRIM(this.oParentObject.w_CONN) , "C" )
          endif
        endif
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Divisione della stringa (Nome variabile)
    this.w_RESULT = LEFTC(this.w_StrLetta,AT("=",this.w_StrLetta)-1)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Determina il contenuto della variabile
    local L_LINEA
    this.w_stato = .F.
    * --- Verifico che la variabile sia in uso
    if type(this.w_Result)<>"U"
      L_Linea="this.w_temp="+this.w_result
      &L_Linea
      this.w_stato = .T.
    endif
    * --- Identifico il valore di riga del CNF e lo assegno alla variabile di appoggio
    *     w_valore utilizzata per restituire la stringa alla procedura
    L_LINEA= this.w_StrLetta
    &L_LINEA
    L_Linea="this.w_valore="+this.w_result
    &L_Linea
    if not this.w_stato
      this.w_RESULT = .F.
    else
      L_Linea=this.w_result+"=( this.w_temp )"
      &L_Linea
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla la validit� delle informazioni immesse e scrive il nuovo CNF
    this.w_IDRigaLetta = 0
    this.w_PosAppPWWr = .F.
    this.w_PosAppRoleWr = .F.
    this.w_PosConnWr = .F.
    this.w_PosCryptWr = .F.
    this.w_WritedDBType = .F.
    this.w_Continue = .T.
    if NOT EMPTY(this.oParentObject.w_CNFOut)
      if AT(".",this.oParentObject.w_CNFOut)<=0
        this.oParentObject.w_CNFOUT = ALLTRIM(this.oParentObject.w_CNFOUT)+".CNF"
      endif
      FOR L_i=1 to LEN(ALLTRIM(this.oParentObject.w_CNFOut))
      if SUBSTR(this.oParentObject.w_CNFOut, L_i, 1) $ "&$?*+-<>=!.,;()|" OR Empty(SUBSTR(this.oParentObject.w_CNFOut, L_i, 1))
        if NOT (SUBSTR(this.oParentObject.w_CNFOut, L_i, 1)="." AND L_i<>LEN(ALLTRIM(this.oParentObject.w_CNFOut))-4)
          ah_ErrorMsg("Nel nome del file non possono essere inclusi spazi o i seguent caratteri: & $? * + - < > =!,; ( ) |","!","")
          this.w_Continue = .F.
        endif
      endif
      ENDFOR
    endif
    if EMPTY(this.oParentObject.w_CNFOut)
      ah_ErrorMsg("Inserire un file CNF su cui scrivere","!","")
      this.w_Continue = .F.
    else
      do case
        case this.oParentObject.w_TipCryptAR="N"
          if EMPTY(this.oParentObject.w_CONN)
            ah_ErrorMsg("Inserire una stringa per la connessione","!","")
            this.w_Continue = .F.
          endif
        case this.oParentObject.w_TipCryptAR="C"
          if EMPTY(this.oParentObject.w_CONN)
            ah_ErrorMsg("Inserire una stringa per la connessione","!","")
            this.w_Continue = .F.
          else
            this.oParentObject.w_CIFRCONN = CifraCNF(ALLTRIM(this.oParentObject.w_CONN),"C")
            if CifraCNF(CifraCNF(ALLTRIM(this.oParentObject.w_CONN) ,"C" ), "D" )<>ALLTRIM(this.oParentObject.w_CONN)
              ah_ErrorMsg("Impossibile cifrare la stringa, cifratura non biunivoca","!","")
              this.w_Continue = .F.
            endif
          endif
        case this.oParentObject.w_TipCryptAR="A"
          if EMPTY(this.oParentObject.w_AppRole)
            ah_ErrorMsg("Inserire il nome dell'Application Role","!","")
            this.w_Continue = .F.
          else
            if EMPTY(this.oParentObject.w_Pass)
              ah_ErrorMsg("Inserire la password dell'Application Role","!","")
              this.w_Continue = .F.
            else
              this.oParentObject.w_CIFRAppRole = CifraCnf( ALLTRIM(this.oParentObject.w_APPROLE) , "C" )
              if CifraCNF(CifraCNF(ALLTRIM(this.oParentObject.w_AppRole) ,"C" ), "D" )<>ALLTRIM(this.oParentObject.w_Approle)
                ah_ErrorMsg("Impossibile cifrare l'Application Role, cifratura non biunivoca","!","")
                this.w_Continue = .F.
              else
                this.oParentObject.w_CIFRPASS = CifraCnf( ALLTRIM(this.oParentObject.w_PASS) , "C" )
                if CifraCNF(CifraCNF(ALLTRIM(this.oParentObject.w_PASS) ,"C" ), "D" )<>ALLTRIM(this.oParentObject.w_PASS)
                  ah_ErrorMsg("Impossibile decifrare la password dell'Application Role, cifratura non biunivoca","!","")
                  this.w_Continue = .F.
                endif
              endif
            endif
          endif
        otherwise
          ah_ErrorMsg("Selezionare un tipo di protezione","!","")
          this.w_Continue = .F.
      endcase
      if this.w_Continue AND FILE(this.oParentObject.w_CNFOut)
        this.w_FileWriteHandle = FOPEN(this.oParentObject.w_CNFOut,1)
        if this.w_FileWriteHandle<0
          ah_ErrorMsg("Il file scelto per la scrittura non � modificabile. Impossibile eseguire la scrittura","!","")
          this.w_Continue = .F.
        else
          this.w_Continue = ah_YesNo("Sovrascrivere il file %1?","",ALLTRIM(this.oParentObject.w_CNFOut))
        endif
        FCLOSE(this.w_FileWriteHandle) 
      endif
    endif
    if this.w_Continue
      this.w_FileHandle = FOPEN(this.oParentObject.w_CNFIn) 
      * --- Apre il file in Scrittura
      * --- Cerca la stringa "* Generato tramite cifratura CNF"
      *     all'interno del file
      this.oParentObject.w_FLGEN = .F.
      this.w_FileEnd = FSEEK(this.w_FileHandle, 0, 2)
      this.w_FileTop = FSEEK(this.w_FileHandle, 0)
      do while NOT FEOF(this.w_FileHandle)
        this.w_StrLetta = FGETS(this.w_FileHandle, this.w_FileEnd) 
        this.w_StrLetta = ALLTRIM(this.w_StrLetta)
        this.w_IDRigaLetta = this.w_IDRigaLetta+1
        if this.w_StrLetta=ah_Msgformat("* Generato tramite cifratura CNF")
          this.oParentObject.w_FLGEN = .T.
        endif
      enddo
      this.w_FileWriteHandle = FCREATE(LEFTC(ALLTRIM(this.oParentObject.w_CNFOut),LEN(ALLTRIM(this.oParentObject.w_CNFOut))-3)+"TMP") 
      * --- Posiziono il puntatore prima alla fine del file e poi all'inizio, cos� posso
      *     verificare che il file esiste e non � vuoto
      this.w_FileEnd = FSEEK(this.w_FileHandle, 0, 2)
      this.w_FileTop = FSEEK(this.w_FileHandle, 0)
      if NOT this.oParentObject.w_FLGEN
        FPUTS(this.w_FileWriteHandle, ah_Msgformat("* Generato tramite cifratura CNF%1",space(2)) )
      endif
      do while NOT FEOF(this.w_FileHandle)
        this.w_StrLetta = FGETS(this.w_FileHandle, this.w_FileEnd) 
        this.w_StrLetta = ALLTRIM(this.w_StrLetta)
        this.w_IDRigaLetta = this.w_IDRigaLetta+1
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        do case
          case UPPER(this.w_result)="CP_DBTYPE"
            FPUTS(this.w_FileWriteHandle,this.w_StrLetta)
            this.w_WritedDBType = .T.
          case UPPER(this.w_result)=="G_CRYPT"
            if NOT this.w_PosCryptWr
              FPUTS(this.w_FileWriteHandle,"g_CRYPT='"+this.oParentObject.w_TipCryptAR+"'")
            endif
            this.w_PosCryptWr = .T.
          case UPPER(this.w_result)="G_APPROLE"
            if this.oParentObject.w_TipCryptAR<>"A"
              FPUTS(this.w_FileWriteHandle,this.w_StrLetta)
            endif
          case UPPER(this.w_result)="G_APPPSW"
            if this.oParentObject.w_TipCryptAR<>"A"
              FPUTS(this.w_FileWriteHandle,this.w_StrLetta)
            endif
          case UPPER(this.w_result)="CP_ODBCCONN"
            if this.oParentObject.w_TipCryptAR<>"A" 
              if this.oParentObject.w_TipCryptAR<>"N"
                this.w_CtrlStrIn = this.oParentObject.w_CIFRCONN
                this.Pag5()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                FPUTS(this.w_FileWriteHandle,"CP_ODBCCONN="+this.w_CtrlStrOut)
                * --- Rivalorizzo il campo indicando che � stato scritto
                this.w_PosConnWr = .T.
              else
                this.w_CtrlStrIn = this.oParentObject.w_CONN
                this.Pag5()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                FPUTS(this.w_FileWriteHandle,"CP_ODBCCONN="+this.w_CtrlStrOut)
                * --- Rivalorizzo il campo indicando che � stato scritto
                this.w_PosConnWr = .T.
              endif
            else
              if EMPTY(this.oParentObject.w_CONN)
                this.w_CtrlStrIn = CifraCnf( ALLTRIM(this.oParentObject.w_CIFRCONN) , "D" )
              else
                this.w_CtrlStrIn = ALLTRIM(this.oParentObject.w_CONN)
              endif
              this.Pag5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              FPUTS(this.w_FileWriteHandle,"CP_ODBCCONN="+this.w_CtrlStrOut)
              this.w_PosConnWr = .T.
            endif
          otherwise
            FPUTS(this.w_FileWriteHandle,this.w_StrLetta)
        endcase
      enddo
      if NOT this.w_WritedDBType
        FPUTS(this.w_FileWriteHandle,"CP_DBTYPE='"+this.oParentObject.w_TipoDB+"'")
      endif
      if (NOT this.w_PosConnWr) AND (this.oParentObject.w_TipCryptAR="C")
        this.w_CtrlStrIn = this.oParentObject.w_CIFRCONN
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        FPUTS(this.w_FileWriteHandle,"CP_ODBCCONN="+this.w_CtrlStrOut)
      endif
      if (NOT this.w_PosConnWr) AND (this.oParentObject.w_TipCryptAR="A")
        this.w_CtrlStrIn = this.oParentObject.w_CONN
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        FPUTS(this.w_FileWriteHandle,"CP_ODBCCONN="+this.w_CtrlStrOut)
      endif
      if (NOT this.w_PosConnWr) AND (this.oParentObject.w_TipCryptAR="N")
        this.w_CtrlStrIn = ALLTRIM(this.oParentObject.w_CONN)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        FPUTS(this.w_FileWriteHandle,"CP_ODBCCONN="+this.w_CtrlStrOut)
      endif
      if NOT this.w_PosCryptWr
        FPUTS(this.w_FileWriteHandle,"g_CRYPT='"+this.oParentObject.w_TipCryptAR+"'")
      endif
      if (NOT this.w_PosAppRoleWr) AND this.oParentObject.w_TipCryptAR="A"
        this.w_CtrlStrIn = this.oParentObject.w_CIFRAppRole
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        FPUTS(this.w_FileWriteHandle,"g_APPROLE="+this.w_CtrlStrOut)
      endif
      if (NOT this.w_PosAppPWWr) AND this.oParentObject.w_TipCryptAR="A"
        this.w_CtrlStrIn = this.oParentObject.w_CIFRPass
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        FPUTS(this.w_FileWriteHandle,"g_APPPSW="+this.w_CtrlStrOut)
      endif
      FCLOSE(this.w_FileHandle) 
      FCLOSE(this.w_FileWriteHandle) 
      iErrSav=ON("ERROR")
      i_Err=.F.
      ON ERROR i_Err=.T.
      ERASE ALLTRIM(this.oParentObject.w_CNFOut)
      RENAME LEFTC(ALLTRIM(this.oParentObject.w_CNFOut),LEN(ALLTRIM(this.oParentObject.w_CNFOut))-3)+"TMP" TO ; 
 LEFTC(ALLTRIM(this.oParentObject.w_CNFOut),LEN(ALLTRIM(this.oParentObject.w_CNFOut))-3)+"CNF"
      ON ERROR &iErrSav
      if i_Err
        ah_ErrorMsg("Scrittura del CNF fallita%0Possibili cause: nome del file errato, percorso specificato non valido o inesistente","stop","")
      else
        ah_ErrorMsg("Scrittura del CNF terminata con successo","i","")
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica la stringa prima di scriverla nel CNF, se contiene uno o pi�
    *     apici (singoli e doppi) li gestisce
    this.w_APICESINGOLO = At("'", this.w_CtrlStrIn )<>0
    this.w_APICEDOPPIO = At('"', this.w_CtrlStrIn )<>0
    if this.w_APICESINGOLO AND this.w_APICEDOPPIO
      this.w_CharAtt = 1
      this.w_Ultimo = 0
      this.w_CtrlStrOut = ""
      do while this.w_CharAtt<=LEN(this.w_CtrlStrIn)
        if SUBSTR(this.w_CtrlStrIn,this.w_CharAtt,1)='"'
          if this.w_Ultimo=0
            this.w_CtrlStrOut = "'"+SUBSTR(this.w_CtrlStrIn,1,this.w_CharAtt)+"'"
            this.w_Ultimo = this.w_CharAtt+1
          else
            this.w_CtrlStrOut = this.w_CtrlStrOut+"+'"+SUBSTR(this.w_CtrlStrIn,this.w_Ultimo,this.w_CharAtt-this.w_Ultimo+1)+"'"
            this.w_Ultimo = this.w_CharAtt+1
          endif
        endif
        if SUBSTR(this.w_CtrlStrIn,this.w_CharAtt,1)="'"
          if this.w_Ultimo=0
            this.w_CtrlStrOut = '"'+SUBSTR(this.w_CtrlStrIn,1,this.w_CharAtt)+'"'
            this.w_Ultimo = this.w_CharAtt+1
          else
            this.w_CtrlStrOut = this.w_CtrlStrOut+"+'"+SUBSTR(this.w_CtrlStrIn,this.w_Ultimo,this.w_CharAtt-this.w_Ultimo+1)+"'"
            this.w_Ultimo = this.w_CharAtt+1
          endif
        endif
        this.w_CharAtt = this.w_CharAtt+1
      enddo
      if NOT this.w_Ultimo>LEN(this.w_CtrlStrIn)
        this.w_CtrlStrOut = this.w_CtrlStrOut+"+'"+SUBSTR(this.w_CtrlStrIn,this.w_Ultimo,LEN(this.w_CtrlStrIn)-this.w_Ultimo+1)+"'"
      endif
    else
      if this.w_APICESINGOLO
        this.w_CtrlStrOut = '"'+this.w_CtrlStrIn+'"'
      endif
      if this.w_APICEDOPPIO
        this.w_CtrlStrOut = "'"+this.w_CtrlStrIn+"'"
      endif
    endif
    if NOT this.w_APICESINGOLO AND NOT this.w_APICEDOPPIO
      this.w_CtrlStrOut = "'"+this.w_CtrlStrIn+"'"
    endif
  endproc


  proc Init(oParentObject,w_Scrittura)
    this.w_Scrittura=w_Scrittura
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Scrittura"
endproc
