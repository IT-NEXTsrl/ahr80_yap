* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bm3                                                        *
*              Movimenti magazzino, cambia causale                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_149]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-04-05                                                      *
* Last revis.: 2001-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bm3",oParentObject)
return(i_retval)

define class tgsma_bm3 as StdBatch
  * --- Local variables
  w_RECO = 0
  w_NUREC = 0
  w_MESS = space(10)
  w_NabsRow = 0
  w_NRelRow = 0
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  SALDIART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna le Righe Documento al Della Causale di Magazzino in testata
    this.w_RECO = RECCOUNT(this.oParentObject.cTrsName)
    this.w_NabsRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
    this.w_NRelRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
    this.w_NUREC = RECNO(this.oParentObject.cTrsName)
    if this.w_RECO<=1 AND EMPTY(this.oParentObject.w_MMCODICE)
      * --- Se il corpo non e' stato ancora valorizzato esce
      i_retcode = 'stop'
      return
    endif
    this.w_MESS = "La causale di magazzino � stata modificata in: %1(Precedente: %2)%0Premere <OK> per aggiornare le righe del documento"
    ah_ErrorMsg(this.w_MESS,,"",this.oParentObject.w_MMTCAMAG,this.oParentObject.o_MMTCAMAG)
    * --- Aggiorna Righe Documento 
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMCAUCOL"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MMTCAMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMCAUCOL;
        from (i_cTable) where;
            CMCODICE = this.oParentObject.w_MMTCAMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_CAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    SCAN FOR t_CPROWORD<>0 AND NOT EMPTY(t_MMCODICE) AND NOT DELETED()
    * --- Legge i Dati del Temporaneo
    this.oParentObject.WorkFromTrs()
    this.oParentObject.SaveDependsOn()
    * --- Causale Magazzino
    this.oParentObject.w_MMCAUMAG = this.oParentObject.w_MMTCAMAG
    this.oParentObject.w_FLAVA1 = " "
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MMCAUMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMFLCOMM;
        from (i_cTable) where;
            CMCODICE = this.oParentObject.w_MMCAUMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_MMCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
      this.oParentObject.w_MMFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
      this.oParentObject.w_MMFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
      this.oParentObject.w_MMFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
      this.oParentObject.w_MMFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
      this.oParentObject.w_FLAVA1 = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
      this.oParentObject.w_MMFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
      this.oParentObject.w_FLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Eventuale Causale Magazzino Collegata
    this.oParentObject.w_MMF2CASC = " "
    this.oParentObject.w_MMF2ORDI = " "
    this.oParentObject.w_MMF2IMPE = " "
    this.oParentObject.w_MMF2RISE = " "
    if NOT EMPTY(this.oParentObject.w_MMCAUCOL)
      * --- Read from CAM_AGAZ
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CMFLCASC,CMFLIMPE,CMFLORDI,CMFLRISE"+;
          " from "+i_cTable+" CAM_AGAZ where ";
              +"CMCODICE = "+cp_ToStrODBC(this.oParentObject.w_MMCAUCOL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CMFLCASC,CMFLIMPE,CMFLORDI,CMFLRISE;
          from (i_cTable) where;
              CMCODICE = this.oParentObject.w_MMCAUCOL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_MMF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
        this.oParentObject.w_MMF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
        this.oParentObject.w_MMF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
        this.oParentObject.w_MMF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.oParentObject.w_MMKEYSAL = this.oParentObject.w_MMCODART
    this.oParentObject.w_MMCODMAT = IIF(NOT EMPTY(this.oParentObject.w_MMCAUCOL), IIF(EMPTY(this.oParentObject.w_MMCODMAT), this.oParentObject.w_MMCODMAG, this.oParentObject.w_MMCODMAT), SPACE(5))
    this.oParentObject.w_QTAPER = 0
    this.oParentObject.w_QTRPER = 0
    this.oParentObject.w_ULTCAR = cp_CharToDate("  -  -  ")
    this.oParentObject.w_ULTSCA = cp_CharToDate("  -  -  ")
    this.oParentObject.w_Q2APER = 0
    this.oParentObject.w_Q2RPER = 0
    if NOT EMPTY(this.oParentObject.w_MMKEYSAL) AND NOT EMPTY(this.oParentObject.w_MMCODMAG)
      * --- Read from SALDIART
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SLQTAPER,SLQTRPER,SLDATUCA,SLDATUPV"+;
          " from "+i_cTable+" SALDIART where ";
              +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_MMKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MMCODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SLQTAPER,SLQTRPER,SLDATUCA,SLDATUPV;
          from (i_cTable) where;
              SLCODICE = this.oParentObject.w_MMKEYSAL;
              and SLCODMAG = this.oParentObject.w_MMCODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_QTAPER = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
        this.oParentObject.w_QTRPER = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
        this.oParentObject.w_ULTCAR = NVL(cp_ToDate(_read_.SLDATUCA),cp_NullValue(_read_.SLDATUCA))
        this.oParentObject.w_ULTSCA = NVL(cp_ToDate(_read_.SLDATUPV),cp_NullValue(_read_.SLDATUPV))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY(this.oParentObject.w_MMCODMAT)
        * --- Read from SALDIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SLQTAPER,SLQTRPER"+;
            " from "+i_cTable+" SALDIART where ";
                +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_MMKEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MMCODMAT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SLQTAPER,SLQTRPER;
            from (i_cTable) where;
                SLCODICE = this.oParentObject.w_MMKEYSAL;
                and SLCODMAG = this.oParentObject.w_MMCODMAT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_Q2APER = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
          this.oParentObject.w_Q2RPER = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    this.oParentObject.w_QTDISP = this.oParentObject.w_QTAPER-this.oParentObject.w_QTRPER
    this.oParentObject.w_Q2DISP = this.oParentObject.w_Q2APER-this.oParentObject.w_Q2RPER
    this.oParentObject.w_MMFLULCA = IIF(this.oParentObject.w_FLAVA1="A" AND this.oParentObject.w_MMFLCASC="+" AND this.oParentObject.w_MMDATREG>=this.oParentObject.w_ULTCAR AND this.oParentObject.w_MMFLOMAG<>"S", "=", " ")
    this.oParentObject.w_MMFLULPV = IIF(this.oParentObject.w_FLAVA1="V" AND this.oParentObject.w_MMFLCASC="-" AND this.oParentObject.w_MMDATREG>=this.oParentObject.w_ULTSCA AND this.oParentObject.w_MMFLOMAG<>"S", "=", " ")
    * --- Carica il Temporaneo dei Dati
    this.oParentObject.TrsFromWork()
    * --- Flag Notifica Riga Variata
    if i_SRV<>"A"
      replace i_SRV with "U"
    endif
    ENDSCAN
    SELECT (this.oParentObject.cTrsName)
    if this.w_NUREC>0 AND this.w_NUREC<=RECCOUNT()
      GOTO this.w_NUREC
    else
      GO TOP
    endif
    With this.oParentObject
    .WorkFromTrs()
    .SaveDependsOn()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=this.w_NabsRow
    .oPgFrm.Page1.oPag.oBody.nRelRow=this.w_NRelRow
    EndWith
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='SALDIART'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
