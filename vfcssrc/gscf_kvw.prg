* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_kvw                                                        *
*              Visualizza log modifiche                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-01-31                                                      *
* Last revis.: 2013-05-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscf_kvw",oParentObject))

* --- Class definition
define class tgscf_kvw as StdForm
  Top    = 1
  Left   = 2

  * --- Standard Properties
  Width  = 802
  Height = 577
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-05-16"
  HelpContextID=48903529
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  REF_MAST_IDX = 0
  XDC_TABLE_IDX = 0
  cPrg = "gscf_kvw"
  cComment = "Visualizza log modifiche"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PARAM = space(40)
  w_FROMGEST = .F.
  w_TBMASTER = space(20)
  o_TBMASTER = space(20)
  w_TBDETAIL = space(20)
  o_TBDETAIL = space(20)
  w_FLDFILTER = space(20)
  w_TIPSEL = space(20)
  w_REGOLA = space(10)
  o_REGOLA = space(10)
  w_TABLENAME = space(20)
  w_DETAILNAME = space(20)
  w_VWDATA = space(1)
  w_RIGASEL = space(1)
  w_LOSERIAL = space(10)
  w_LOSTAMP = space(10)
  w_LOSTMRIG = space(10)
  w_ZDETCHG = space(20)
  w_FLMONINS = space(1)
  w_FLMONMOD = space(1)
  w_FLMONDEL = space(1)
  w_REDESCRI = space(50)
  w_TBMNAME = space(30)
  w_TBDNAME = space(30)
  w_LOFLDVAL = space(90)
  w_LONEWVAL = space(90)
  w_zoom = .NULL.
  w_zoomdett = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscf_kvwPag1","gscf_kvw",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oREGOLA_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_zoom = this.oPgFrm.Pages(1).oPag.zoom
    this.w_zoomdett = this.oPgFrm.Pages(1).oPag.zoomdett
    DoDefault()
    proc Destroy()
      this.w_zoom = .NULL.
      this.w_zoomdett = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='REF_MAST'
    this.cWorkTables[2]='XDC_TABLE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARAM=space(40)
      .w_FROMGEST=.f.
      .w_TBMASTER=space(20)
      .w_TBDETAIL=space(20)
      .w_FLDFILTER=space(20)
      .w_TIPSEL=space(20)
      .w_REGOLA=space(10)
      .w_TABLENAME=space(20)
      .w_DETAILNAME=space(20)
      .w_VWDATA=space(1)
      .w_RIGASEL=space(1)
      .w_LOSERIAL=space(10)
      .w_LOSTAMP=space(10)
      .w_LOSTMRIG=space(10)
      .w_ZDETCHG=space(20)
      .w_FLMONINS=space(1)
      .w_FLMONMOD=space(1)
      .w_FLMONDEL=space(1)
      .w_REDESCRI=space(50)
      .w_TBMNAME=space(30)
      .w_TBDNAME=space(30)
      .w_LOFLDVAL=space(90)
      .w_LONEWVAL=space(90)
        .w_PARAM = this.oParentObject
        .w_FROMGEST = vartype(.w_PARAM)="O"
        .w_TBMASTER = iif(.w_FROMGEST and vartype(.w_PARAM.w_TABLENAME)="C", .w_PARAM.w_TABLENAME, space(20))
        .w_TBDETAIL = iif(.w_FROMGEST and vartype(.w_PARAM.w_DETAILNAME)="C", .w_PARAM.w_DETAILNAME, space(20))
        .w_FLDFILTER = iif(.w_FROMGEST and vartype(.w_PARAM.w_FLDFILTER)="C", .w_PARAM.w_FLDFILTER, space(20))
        .w_TIPSEL = iif(.w_FROMGEST and vartype(.w_PARAM.pTIPSEL)="C", .w_PARAM.pTIPSEL, "T")
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_REGOLA))
          .link_1_7('Full')
        endif
        .w_TABLENAME = iif(empty(.w_TABLENAME), .w_TBMASTER, .w_TABLENAME)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_TABLENAME))
          .link_1_8('Full')
        endif
        .w_DETAILNAME = iif(empty(.w_DETAILNAME), .w_TBDETAIL, .w_DETAILNAME)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_DETAILNAME))
          .link_1_9('Full')
        endif
        .w_VWDATA = .w_TIPSEL
        .w_RIGASEL = IIF(.w_VWDATA="R","R","X")
      .oPgFrm.Page1.oPag.zoom.Calculate(.w_VWDATA)
        .w_LOSERIAL = .w_zoom.getvar("LOSERIAL")
        .w_LOSTAMP = .w_zoom.getvar("LOSTAMP")
        .w_LOSTMRIG = NVL(.w_zoom.getvar("LOSTMRIG"), SPACE(10))
        .w_ZDETCHG = .w_VWDATA+.w_FLMONINS+.w_FLMONMOD+.w_FLMONDEL+.w_LOSERIAL+.w_LOSTAMP+.w_LOSTMRIG
        .w_FLMONINS = 'I'
        .w_FLMONMOD = 'R'
        .w_FLMONDEL = 'D'
      .oPgFrm.Page1.oPag.zoomdett.Calculate(.w_ZDETCHG)
          .DoRTCalc(19,21,.f.)
        .w_LOFLDVAL = .w_zoomdett.getvar("LOFLDVAL")
        .w_LONEWVAL = .w_zoomdett.getvar("LONEWVAL")
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
            .w_TBDETAIL = iif(.w_FROMGEST and vartype(.w_PARAM.w_DETAILNAME)="C", .w_PARAM.w_DETAILNAME, space(20))
            .w_FLDFILTER = iif(.w_FROMGEST and vartype(.w_PARAM.w_FLDFILTER)="C", .w_PARAM.w_FLDFILTER, space(20))
            .w_TIPSEL = iif(.w_FROMGEST and vartype(.w_PARAM.pTIPSEL)="C", .w_PARAM.pTIPSEL, "T")
        .DoRTCalc(7,7,.t.)
        if .o_TBMASTER<>.w_TBMASTER.or. .o_REGOLA<>.w_REGOLA
            .w_TABLENAME = iif(empty(.w_TABLENAME), .w_TBMASTER, .w_TABLENAME)
          .link_1_8('Full')
        endif
        if .o_TBDETAIL<>.w_TBDETAIL.or. .o_REGOLA<>.w_REGOLA
            .w_DETAILNAME = iif(empty(.w_DETAILNAME), .w_TBDETAIL, .w_DETAILNAME)
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.t.)
            .w_RIGASEL = IIF(.w_VWDATA="R","R","X")
        .oPgFrm.Page1.oPag.zoom.Calculate(.w_VWDATA)
            .w_LOSERIAL = .w_zoom.getvar("LOSERIAL")
            .w_LOSTAMP = .w_zoom.getvar("LOSTAMP")
            .w_LOSTMRIG = NVL(.w_zoom.getvar("LOSTMRIG"), SPACE(10))
            .w_ZDETCHG = .w_VWDATA+.w_FLMONINS+.w_FLMONMOD+.w_FLMONDEL+.w_LOSERIAL+.w_LOSTAMP+.w_LOSTMRIG
        .oPgFrm.Page1.oPag.zoomdett.Calculate(.w_ZDETCHG)
        .DoRTCalc(16,21,.t.)
            .w_LOFLDVAL = .w_zoomdett.getvar("LOFLDVAL")
            .w_LONEWVAL = .w_zoomdett.getvar("LONEWVAL")
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.zoom.Calculate(.w_VWDATA)
        .oPgFrm.Page1.oPag.zoomdett.Calculate(.w_ZDETCHG)
    endwith
  return

  proc Calculate_HHRNKLDFXO()
    with this
          * --- cambio le query
          gscf_bvw(this;
              ,"BLANK";
             )
          .w_ZOOM.CCPQUERYNAME = "QUERY\GSCF_KVW"
          .w_ZOOMDETT.CCPQUERYNAME = "QUERY\GSCFDKVW"
    endwith
  endproc
  proc Calculate_SLPNZRTLMQ()
    with this
          * --- Drop tmptable
          gscf_bvw(this;
              ,"DONE";
             )
    endwith
  endproc
  proc Calculate_TTIXBSOVWW()
    with this
          * --- init lostamp x zoom dett
          .w_LOSTAMP = .w_zoom.getvar("LOSTAMP")
          gscf_bvw(this;
              ,"INIT";
             )
    endwith
  endproc
  proc Calculate_ZRWGHEDQOV()
    with this
          * --- colore zoom
          gscf_bvw(this;
              ,"COLORE";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oREGOLA_1_7.enabled = this.oPgFrm.Page1.oPag.oREGOLA_1_7.mCond()
    this.oPgFrm.Page1.oPag.oTABLENAME_1_8.enabled = this.oPgFrm.Page1.oPag.oTABLENAME_1_8.mCond()
    this.oPgFrm.Page1.oPag.oDETAILNAME_1_9.enabled = this.oPgFrm.Page1.oPag.oDETAILNAME_1_9.mCond()
    this.oPgFrm.Page1.oPag.oVWDATA_1_10.enabled = this.oPgFrm.Page1.oPag.oVWDATA_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLMONDEL_1_23.visible=!this.oPgFrm.Page1.oPag.oFLMONDEL_1_23.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_32.visible=!this.oPgFrm.Page1.oPag.oBtn_1_32.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_HHRNKLDFXO()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.zoomdett.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_SLPNZRTLMQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_TTIXBSOVWW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_zoom after query")
          .Calculate_ZRWGHEDQOV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=REGOLA
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REF_MAST_IDX,3]
    i_lTable = "REF_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2], .t., this.REF_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_REGOLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gscf_mre',True,'REF_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RESERIAL like "+cp_ToStrODBC(trim(this.w_REGOLA)+"%");

          i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI,RETABMST,RETABDTL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RESERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RESERIAL',trim(this.w_REGOLA))
          select RESERIAL,REDESCRI,RETABMST,RETABDTL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RESERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_REGOLA)==trim(_Link_.RESERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" REDESCRI like "+cp_ToStrODBC(trim(this.w_REGOLA)+"%");

            i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI,RETABMST,RETABDTL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" REDESCRI like "+cp_ToStr(trim(this.w_REGOLA)+"%");

            select RESERIAL,REDESCRI,RETABMST,RETABDTL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_REGOLA) and !this.bDontReportError
            deferred_cp_zoom('REF_MAST','*','RESERIAL',cp_AbsName(oSource.parent,'oREGOLA_1_7'),i_cWhere,'gscf_mre',"REGOLE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI,RETABMST,RETABDTL";
                     +" from "+i_cTable+" "+i_lTable+" where RESERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RESERIAL',oSource.xKey(1))
            select RESERIAL,REDESCRI,RETABMST,RETABDTL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_REGOLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI,RETABMST,RETABDTL";
                   +" from "+i_cTable+" "+i_lTable+" where RESERIAL="+cp_ToStrODBC(this.w_REGOLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RESERIAL',this.w_REGOLA)
            select RESERIAL,REDESCRI,RETABMST,RETABDTL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_REGOLA = NVL(_Link_.RESERIAL,space(10))
      this.w_REDESCRI = NVL(_Link_.REDESCRI,space(50))
      this.w_TABLENAME = NVL(_Link_.RETABMST,space(20))
      this.w_DETAILNAME = NVL(_Link_.RETABDTL,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_REGOLA = space(10)
      endif
      this.w_REDESCRI = space(50)
      this.w_TABLENAME = space(20)
      this.w_DETAILNAME = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])+'\'+cp_ToStr(_Link_.RESERIAL,1)
      cp_ShowWarn(i_cKey,this.REF_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_REGOLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TABLENAME
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TABLENAME) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_TABLENAME)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_TABLENAME))
          select TBNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TABLENAME)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TBCOMMENT like "+cp_ToStrODBC(trim(this.w_TABLENAME)+"%");

            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TBCOMMENT like "+cp_ToStr(trim(this.w_TABLENAME)+"%");

            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TABLENAME) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oTABLENAME_1_8'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TABLENAME)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_TABLENAME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_TABLENAME)
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TABLENAME = NVL(_Link_.TBNAME,space(20))
      this.w_TBMNAME = NVL(_Link_.TBCOMMENT,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TABLENAME = space(20)
      endif
      this.w_TBMNAME = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TABLENAME Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DETAILNAME
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DETAILNAME) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_DETAILNAME)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_DETAILNAME))
          select TBNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DETAILNAME)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TBCOMMENT like "+cp_ToStrODBC(trim(this.w_DETAILNAME)+"%");

            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TBCOMMENT like "+cp_ToStr(trim(this.w_DETAILNAME)+"%");

            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DETAILNAME) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oDETAILNAME_1_9'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DETAILNAME)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_DETAILNAME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_DETAILNAME)
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DETAILNAME = NVL(_Link_.TBNAME,space(20))
      this.w_TBDNAME = NVL(_Link_.TBCOMMENT,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_DETAILNAME = space(20)
      endif
      this.w_TBDNAME = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DETAILNAME Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oREGOLA_1_7.value==this.w_REGOLA)
      this.oPgFrm.Page1.oPag.oREGOLA_1_7.value=this.w_REGOLA
    endif
    if not(this.oPgFrm.Page1.oPag.oTABLENAME_1_8.value==this.w_TABLENAME)
      this.oPgFrm.Page1.oPag.oTABLENAME_1_8.value=this.w_TABLENAME
    endif
    if not(this.oPgFrm.Page1.oPag.oDETAILNAME_1_9.value==this.w_DETAILNAME)
      this.oPgFrm.Page1.oPag.oDETAILNAME_1_9.value=this.w_DETAILNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oVWDATA_1_10.RadioValue()==this.w_VWDATA)
      this.oPgFrm.Page1.oPag.oVWDATA_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMONINS_1_21.RadioValue()==this.w_FLMONINS)
      this.oPgFrm.Page1.oPag.oFLMONINS_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMONMOD_1_22.RadioValue()==this.w_FLMONMOD)
      this.oPgFrm.Page1.oPag.oFLMONMOD_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMONDEL_1_23.RadioValue()==this.w_FLMONDEL)
      this.oPgFrm.Page1.oPag.oFLMONDEL_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREDESCRI_1_27.value==this.w_REDESCRI)
      this.oPgFrm.Page1.oPag.oREDESCRI_1_27.value=this.w_REDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTBMNAME_1_28.value==this.w_TBMNAME)
      this.oPgFrm.Page1.oPag.oTBMNAME_1_28.value=this.w_TBMNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oTBDNAME_1_29.value==this.w_TBDNAME)
      this.oPgFrm.Page1.oPag.oTBDNAME_1_29.value=this.w_TBDNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oLOFLDVAL_1_34.value==this.w_LOFLDVAL)
      this.oPgFrm.Page1.oPag.oLOFLDVAL_1_34.value=this.w_LOFLDVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oLONEWVAL_1_35.value==this.w_LONEWVAL)
      this.oPgFrm.Page1.oPag.oLONEWVAL_1_35.value=this.w_LONEWVAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TBMASTER = this.w_TBMASTER
    this.o_TBDETAIL = this.w_TBDETAIL
    this.o_REGOLA = this.w_REGOLA
    return

enddefine

* --- Define pages as container
define class tgscf_kvwPag1 as StdContainer
  Width  = 798
  height = 577
  stdWidth  = 798
  stdheight = 577
  resizeXpos=721
  resizeYpos=453
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oREGOLA_1_7 as StdField with uid="ZPJLDPKAJF",rtseq=7,rtrep=.f.,;
    cFormVar = "w_REGOLA", cQueryName = "REGOLA",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Regola selezionata",;
    HelpContextID = 53052694,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=53, Top=6, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="REF_MAST", cZoomOnZoom="gscf_mre", oKey_1_1="RESERIAL", oKey_1_2="this.w_REGOLA"

  func oREGOLA_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_TBMASTER) AND empty(.w_TBDETAIL))
    endwith
   endif
  endfunc

  func oREGOLA_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oREGOLA_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oREGOLA_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REF_MAST','*','RESERIAL',cp_AbsName(this.parent,'oREGOLA_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'gscf_mre',"REGOLE",'',this.parent.oContained
  endproc
  proc oREGOLA_1_7.mZoomOnZoom
    local i_obj
    i_obj=gscf_mre()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RESERIAL=this.parent.oContained.w_REGOLA
     i_obj.ecpSave()
  endproc

  add object oTABLENAME_1_8 as StdField with uid="KIPBJYZHPI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TABLENAME", cQueryName = "TABLENAME",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tabella master selezionata",;
    HelpContextID = 263599571,;
   bGlobalFont=.t.,;
    Height=21, Width=98, Left=351, Top=6, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_TABLENAME"

  func oTABLENAME_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_REGOLA) and empty(.w_TBMASTER) and empty(.w_TBDETAIL))
    endwith
   endif
  endfunc

  func oTABLENAME_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oTABLENAME_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTABLENAME_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oTABLENAME_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDETAILNAME_1_9 as StdField with uid="XHMFHKUOMU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DETAILNAME", cQueryName = "DETAILNAME",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tabella detail selezionata",;
    HelpContextID = 233610823,;
   bGlobalFont=.t.,;
    Height=21, Width=98, Left=351, Top=30, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_DETAILNAME"

  func oDETAILNAME_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_REGOLA) and empty(.w_TBMASTER) and empty(.w_TBDETAIL))
    endwith
   endif
  endfunc

  func oDETAILNAME_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oDETAILNAME_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDETAILNAME_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oDETAILNAME_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc


  add object oVWDATA_1_10 as StdCombo with uid="MMFHXGTLZJ",rtseq=10,rtrep=.f.,left=119,top=31,width=149,height=21;
    , HelpContextID = 60516182;
    , cFormVar="w_VWDATA",RowSource=""+"Tutto,"+"Solo Master,"+"Solo Detail,"+"Solo riga selezionata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oVWDATA_1_10.RadioValue()
    return(iif(this.value =1,"T",;
    iif(this.value =2,"M",;
    iif(this.value =3,"D",;
    iif(this.value =4,"R",;
    space(1))))))
  endfunc
  func oVWDATA_1_10.GetRadio()
    this.Parent.oContained.w_VWDATA = this.RadioValue()
    return .t.
  endfunc

  func oVWDATA_1_10.SetRadio()
    this.Parent.oContained.w_VWDATA=trim(this.Parent.oContained.w_VWDATA)
    this.value = ;
      iif(this.Parent.oContained.w_VWDATA=="T",1,;
      iif(this.Parent.oContained.w_VWDATA=="M",2,;
      iif(this.Parent.oContained.w_VWDATA=="D",3,;
      iif(this.Parent.oContained.w_VWDATA=="R",4,;
      0))))
  endfunc

  func oVWDATA_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TABLENAME<>.w_DETAILNAME and not empty(.w_DETAILNAME))
    endwith
   endif
  endfunc


  add object zoom as cp_zoombox with uid="WWYNUIXGYY",left=3, top=54, width=792,height=205,;
    caption='Object',;
   bGlobalFont=.t.,;
    bRetriveAllRows=.f.,cZoomFile="gscf_kvw",bOptions=.t.,bAdvOptions=.f.,cZoomOnZoom="",cTable="LOGCNTFL",bQueryOnLoad=.t.,bReadOnly=.t.,cMenuFile="",;
    cEvent = "Init,RequeryM",;
    nPag=1;
    , HelpContextID = 129094118

  add object oFLMONINS_1_21 as StdCheck with uid="IVQTWDUKYU",rtseq=16,rtrep=.f.,left=633, top=3, caption="Inserimento",;
    ToolTipText = "Se attivo: visualizza le operazioni di tipo inserimento",;
    HelpContextID = 189393833,;
    cFormVar="w_FLMONINS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLMONINS_1_21.RadioValue()
    return(iif(this.value =1,'I',;
    'N'))
  endfunc
  func oFLMONINS_1_21.GetRadio()
    this.Parent.oContained.w_FLMONINS = this.RadioValue()
    return .t.
  endfunc

  func oFLMONINS_1_21.SetRadio()
    this.Parent.oContained.w_FLMONINS=trim(this.Parent.oContained.w_FLMONINS)
    this.value = ;
      iif(this.Parent.oContained.w_FLMONINS=='I',1,;
      0)
  endfunc

  add object oFLMONMOD_1_22 as StdCheck with uid="KKEPELKXYN",rtseq=17,rtrep=.f.,left=633, top=34, caption="Modifica",;
    ToolTipText = "Se attivo: visualizza le operazioni di tipo modifica",;
    HelpContextID = 256502682,;
    cFormVar="w_FLMONMOD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLMONMOD_1_22.RadioValue()
    return(iif(this.value =1,'R',;
    'N'))
  endfunc
  func oFLMONMOD_1_22.GetRadio()
    this.Parent.oContained.w_FLMONMOD = this.RadioValue()
    return .t.
  endfunc

  func oFLMONMOD_1_22.SetRadio()
    this.Parent.oContained.w_FLMONMOD=trim(this.Parent.oContained.w_FLMONMOD)
    this.value = ;
      iif(this.Parent.oContained.w_FLMONMOD=='R',1,;
      0)
  endfunc

  add object oFLMONDEL_1_23 as StdCheck with uid="VFENKHIEET",rtseq=18,rtrep=.f.,left=633, top=18, caption="Cancellazione",;
    ToolTipText = "Se attivo: visualizza le operazioni di tipo cancellazione",;
    HelpContextID = 162927710,;
    cFormVar="w_FLMONDEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLMONDEL_1_23.RadioValue()
    return(iif(this.value =1,'D',;
    'N'))
  endfunc
  func oFLMONDEL_1_23.GetRadio()
    this.Parent.oContained.w_FLMONDEL = this.RadioValue()
    return .t.
  endfunc

  func oFLMONDEL_1_23.SetRadio()
    this.Parent.oContained.w_FLMONDEL=trim(this.Parent.oContained.w_FLMONDEL)
    this.value = ;
      iif(this.Parent.oContained.w_FLMONDEL=='D',1,;
      0)
  endfunc

  func oFLMONDEL_1_23.mHide()
    with this.Parent.oContained
      return (NOT empty(.w_TBMASTER))
    endwith
  endfunc

  add object oREDESCRI_1_27 as StdField with uid="EVVLALBFBN",rtseq=19,rtrep=.f.,;
    cFormVar = "w_REDESCRI", cQueryName = "REDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 93279583,;
   bGlobalFont=.t.,;
    Height=21, Width=165, Left=144, Top=6, InputMask=replicate('X',50)

  add object oTBMNAME_1_28 as StdField with uid="KTMXBCDDHK",rtseq=20,rtrep=.f.,;
    cFormVar = "w_TBMNAME", cQueryName = "TBMNAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 25632202,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=451, Top=6, InputMask=replicate('X',30)

  add object oTBDNAME_1_29 as StdField with uid="VPCZUQGJNN",rtseq=21,rtrep=.f.,;
    cFormVar = "w_TBDNAME", cQueryName = "TBDNAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 25669066,;
   bGlobalFont=.t.,;
    Height=21, Width=177, Left=451, Top=30, InputMask=replicate('X',30)


  add object zoomdett as cp_zoombox with uid="LSINFDSPCH",left=3, top=260, width=792,height=267,;
    caption='Object',;
   bGlobalFont=.t.,;
    bRetriveAllRows=.f.,cZoomFile="gscfdkvw",bOptions=.t.,bAdvOptions=.f.,cZoomOnZoom="",cTable="LOGCNTFL",bQueryOnLoad=.t.,bReadOnly=.t.,cMenuFile="",;
    cEvent = "Init,RequeryD",;
    nPag=1;
    , HelpContextID = 129094118


  add object oBtn_1_32 as StdButton with uid="JLCPPDXJAV",left=745, top=6, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 111313917;
    , Caption='\<Interroga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        GSCF_BVW(this.Parent.oContained,"REQUERY")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT empty(.w_TABLENAME) or NOT empty(.w_DETAILNAME))
      endwith
    endif
  endfunc

  func oBtn_1_32.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT empty(.w_TBMASTER) or NOT empty(.w_TBDETAIL))
     endwith
    endif
  endfunc

  add object oLOFLDVAL_1_34 as StdField with uid="WJPGMZDNLP",rtseq=22,rtrep=.f.,;
    cFormVar = "w_LOFLDVAL", cQueryName = "LOFLDVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(90), bMultilanguage =  .f.,;
    HelpContextID = 128352002,;
   bGlobalFont=.t.,;
    Height=21, Width=643, Left=96, Top=528, InputMask=replicate('X',90)

  add object oLONEWVAL_1_35 as StdField with uid="QTPTMDQTPC",rtseq=23,rtrep=.f.,;
    cFormVar = "w_LONEWVAL", cQueryName = "LONEWVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(90), bMultilanguage =  .f.,;
    HelpContextID = 147848962,;
   bGlobalFont=.t.,;
    Height=21, Width=643, Left=96, Top=550, InputMask=replicate('X',90)


  add object oBtn_1_36 as StdButton with uid="MTXNONHMHL",left=745, top=527, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 41586106;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_14 as StdString with uid="OVSHLRDDWJ",Visible=.t., Left=4, Top=532,;
    Alignment=1, Width=91, Height=18,;
    Caption="Vecchio valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="HLUBOSINTA",Visible=.t., Left=4, Top=554,;
    Alignment=1, Width=91, Height=18,;
    Caption="Nuovo valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="UWQVWEGEMQ",Visible=.t., Left=6, Top=34,;
    Alignment=0, Width=111, Height=18,;
    Caption="Dati da visualizzare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="HJKXPABXDQ",Visible=.t., Left=7, Top=10,;
    Alignment=1, Width=43, Height=18,;
    Caption="Regola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="WQOTKXBEWK",Visible=.t., Left=309, Top=10,;
    Alignment=1, Width=42, Height=18,;
    Caption="Master:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="FBEVZNUDQO",Visible=.t., Left=309, Top=34,;
    Alignment=1, Width=42, Height=18,;
    Caption="Detail:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscf_kvw','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
