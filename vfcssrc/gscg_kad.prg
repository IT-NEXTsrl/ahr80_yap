* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kad                                                        *
*              Visualizza anomalie dichiarazioni                               *
*                                                                              *
*      Author: ZUCCHETTI SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-07                                                      *
* Last revis.: 2009-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kad",oParentObject))

* --- Class definition
define class tgscg_kad as StdForm
  Top    = 0
  Left   = 17

  * --- Standard Properties
  Width  = 708
  Height = 488
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-10-21"
  HelpContextID=132785513
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  BUSIUNIT_IDX = 0
  TIP_DOCU_IDX = 0
  AZIENDA_IDX = 0
  DIC_INTE_IDX = 0
  cPrg = "gscg_kad"
  cComment = "Visualizza anomalie dichiarazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLVEAC = space(1)
  o_FLVEAC = space(1)
  w_CLADOC = space(2)
  o_CLADOC = space(2)
  w_FILDOC = space(2)
  w_TIPDOC = space(5)
  w_DESDOC = space(35)
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_FORSEL = space(15)
  w_CLISEL = space(15)
  w_DATINI = ctod('  /  /  ')
  w_CODCON = space(10)
  w_DATFIN = ctod('  /  /  ')
  w_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  w_SERIALE = space(10)
  w_DESCON = space(40)
  w_PARAME = space(3)
  w_TCATDOC = space(2)
  w_TFLVEAC = space(1)
  w_TIPATT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CLADOC = space(2)
  w_ANCODICE = space(15)
  w_ANTIPCON = space(1)
  w_RIFDIC   = space(10)
  w_ZoomDoc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kadPag1","gscg_kad",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLVEAC_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDoc = this.oPgFrm.Pages(1).oPag.ZoomDoc
    DoDefault()
    proc Destroy()
      this.w_ZoomDoc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='BUSIUNIT'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='DIC_INTE'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLVEAC=space(1)
      .w_CLADOC=space(2)
      .w_FILDOC=space(2)
      .w_TIPDOC=space(5)
      .w_DESDOC=space(35)
      .w_TIPCON=space(1)
      .w_FORSEL=space(15)
      .w_CLISEL=space(15)
      .w_DATINI=ctod("  /  /  ")
      .w_CODCON=space(10)
      .w_DATFIN=ctod("  /  /  ")
      .w_DOCINI=ctod("  /  /  ")
      .w_DOCFIN=ctod("  /  /  ")
      .w_SERIALE=space(10)
      .w_DESCON=space(40)
      .w_PARAME=space(3)
      .w_TCATDOC=space(2)
      .w_TFLVEAC=space(1)
      .w_TIPATT=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CLADOC=space(2)
      .w_ANCODICE=space(15)
      .w_ANTIPCON=space(1)
      .w_RIFDIC  =space(10)
        .w_FLVEAC = 'A'
        .w_CLADOC = 'DT'
        .w_FILDOC = .w_CLADOC
        .w_TIPDOC = SPACE(5)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_TIPDOC))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_TIPCON = iif(.w_FLVEAC='A','F','C')
        .w_FORSEL = SPACE(15)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_FORSEL))
          .link_1_7('Full')
        endif
        .w_CLISEL = SPACE(15)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CLISEL))
          .link_1_8('Full')
        endif
        .w_DATINI = g_INIESE
        .w_CODCON = IIF(.w_TIPCON = 'F', .w_FORSEL, .w_CLISEL)
        .w_DATFIN = g_FINESE
        .w_DOCINI = g_INIESE
        .w_DOCFIN = g_FINESE
      .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .w_SERIALE = .w_ZoomDoc.getVar('MVSERIAL')
          .DoRTCalc(15,15,.f.)
        .w_PARAME = .w_ZoomDoc.getVar('MVFLVEAC')+.w_ZoomDoc.getVar('MVCLADOC')
          .DoRTCalc(17,18,.f.)
        .w_TIPATT = 'A'
        .w_OBTEST = i_INIDAT
          .DoRTCalc(21,21,.f.)
        .w_CLADOC = 'DT'
        .w_ANCODICE = .w_CLISEL
        .w_ANTIPCON = .w_TIPCON
    endwith
    this.DoRTCalc(25,25,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CLADOC<>.w_CLADOC
            .w_FILDOC = .w_CLADOC
        endif
        if .o_FLVEAC<>.w_FLVEAC.or. .o_CLADOC<>.w_CLADOC
            .w_TIPDOC = SPACE(5)
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_FLVEAC<>.w_FLVEAC
            .w_TIPCON = iif(.w_FLVEAC='A','F','C')
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_FORSEL = SPACE(15)
          .link_1_7('Full')
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_CLISEL = SPACE(15)
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.t.)
            .w_CODCON = IIF(.w_TIPCON = 'F', .w_FORSEL, .w_CLISEL)
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .DoRTCalc(11,13,.t.)
            .w_SERIALE = .w_ZoomDoc.getVar('MVSERIAL')
        .DoRTCalc(15,15,.t.)
            .w_PARAME = .w_ZoomDoc.getVar('MVFLVEAC')+.w_ZoomDoc.getVar('MVCLADOC')
        .DoRTCalc(17,22,.t.)
            .w_ANCODICE = .w_CLISEL
            .w_ANTIPCON = .w_TIPCON
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(25,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFORSEL_1_7.enabled = this.oPgFrm.Page1.oPag.oFORSEL_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCLISEL_1_8.enabled = this.oPgFrm.Page1.oPag.oCLISEL_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCLADOC_1_2.visible=!this.oPgFrm.Page1.oPag.oCLADOC_1_2.mHide()
    this.oPgFrm.Page1.oPag.oFORSEL_1_7.visible=!this.oPgFrm.Page1.oPag.oFORSEL_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCLISEL_1_8.visible=!this.oPgFrm.Page1.oPag.oCLISEL_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCLADOC_1_34.visible=!this.oPgFrm.Page1.oPag.oCLADOC_1_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomDoc.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPDOC
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_1_4'),i_cWhere,'GSVE_ATD',"Tipi documenti",'GSVE1QZM.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_TCATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_TFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_TCATDOC = space(2)
      this.w_TFLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TFLVEAC=.w_FLVEAC AND (.w_TCATDOC=.w_CLADOC)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente")
        endif
        this.w_TIPDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_TCATDOC = space(2)
        this.w_TFLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORSEL
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_FORSEL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORSEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORSEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORSEL_1_7'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORSEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_FORSEL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORSEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORSEL = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        endif
        this.w_FORSEL = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLISEL
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLISEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACL',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CLISEL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLISEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLISEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLISEL_1_8'),i_cWhere,'GSAR_ACL',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLISEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLISEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CLISEL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLISEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLISEL = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        endif
        this.w_CLISEL = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLISEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLVEAC_1_1.RadioValue()==this.w_FLVEAC)
      this.oPgFrm.Page1.oPag.oFLVEAC_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOC_1_2.RadioValue()==this.w_CLADOC)
      this.oPgFrm.Page1.oPag.oCLADOC_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_4.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_4.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_5.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_5.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_6.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFORSEL_1_7.value==this.w_FORSEL)
      this.oPgFrm.Page1.oPag.oFORSEL_1_7.value=this.w_FORSEL
    endif
    if not(this.oPgFrm.Page1.oPag.oCLISEL_1_8.value==this.w_CLISEL)
      this.oPgFrm.Page1.oPag.oCLISEL_1_8.value=this.w_CLISEL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_9.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_9.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_11.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_12.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_12.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_13.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_13.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_26.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_26.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOC_1_34.RadioValue()==this.w_CLADOC)
      this.oPgFrm.Page1.oPag.oCLADOC_1_34.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TFLVEAC=.w_FLVEAC AND (.w_TCATDOC=.w_CLADOC))  and not(empty(.w_TIPDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPDOC_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(!.w_tipcon ='F')  and (.w_tipcon ='F')  and not(empty(.w_FORSEL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFORSEL_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(!.w_tipcon ='C')  and (.w_tipcon ='C')  and not(empty(.w_CLISEL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLISEL_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
          case   ((empty(.w_DATINI)) or not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DATFIN)) or not(.w_DATINI<=.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_11.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DOCINI)) or not(.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCINI_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DOCINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DOCFIN)) or not(.w_DOCINI<=.w_DOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCFIN_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DOCFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLVEAC = this.w_FLVEAC
    this.o_CLADOC = this.w_CLADOC
    this.o_TIPCON = this.w_TIPCON
    return

enddefine

* --- Define pages as container
define class tgscg_kadPag1 as StdContainer
  Width  = 704
  height = 488
  stdWidth  = 704
  stdheight = 488
  resizeXpos=628
  resizeYpos=268
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLVEAC_1_1 as StdCombo with uid="ZQSVNGVRJV",rtseq=1,rtrep=.f.,left=92,top=6,width=83,height=21;
    , ToolTipText = "Tipo gestione interessata";
    , HelpContextID = 9401514;
    , cFormVar="w_FLVEAC",RowSource=""+"Vendite,"+"Acquisti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLVEAC_1_1.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oFLVEAC_1_1.GetRadio()
    this.Parent.oContained.w_FLVEAC = this.RadioValue()
    return .t.
  endfunc

  func oFLVEAC_1_1.SetRadio()
    this.Parent.oContained.w_FLVEAC=trim(this.Parent.oContained.w_FLVEAC)
    this.value = ;
      iif(this.Parent.oContained.w_FLVEAC=='V',1,;
      iif(this.Parent.oContained.w_FLVEAC=='A',2,;
      0))
  endfunc


  add object oCLADOC_1_2 as StdCombo with uid="DVOJYWQWFC",rtseq=2,rtrep=.f.,left=238,top=6,width=142,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 263308506;
    , cFormVar="w_CLADOC",RowSource=""+"Documenti interni,"+"Ordini,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ordini previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLADOC_1_2.RadioValue()
    return(iif(this.value =1,'DI',;
    iif(this.value =2,'OR',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'OP',;
    space(2))))))))
  endfunc
  func oCLADOC_1_2.GetRadio()
    this.Parent.oContained.w_CLADOC = this.RadioValue()
    return .t.
  endfunc

  func oCLADOC_1_2.SetRadio()
    this.Parent.oContained.w_CLADOC=trim(this.Parent.oContained.w_CLADOC)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOC=='DI',1,;
      iif(this.Parent.oContained.w_CLADOC=='OR',2,;
      iif(this.Parent.oContained.w_CLADOC=='DT',3,;
      iif(this.Parent.oContained.w_CLADOC=='FA',4,;
      iif(this.Parent.oContained.w_CLADOC=='NC',5,;
      iif(this.Parent.oContained.w_CLADOC=='OP',6,;
      0))))))
  endfunc

  func oCLADOC_1_2.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oTIPDOC_1_4 as StdField with uid="JAINMPNBRJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Codice tipo documento di selezione",;
    HelpContextID = 263247562,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=422, Top=7, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Tipi documenti",'GSVE1QZM.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPDOC_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPDOC
     i_obj.ecpSave()
  endproc

  add object oDESDOC_1_5 as StdField with uid="GLKHKTUIJU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 263236554,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=485, Top=8, InputMask=replicate('X',35)


  add object oTIPCON_1_6 as StdCombo with uid="GAGLAKOTEP",rtseq=6,rtrep=.f.,left=92,top=40,width=84,height=21, enabled=.f.;
    , ToolTipText = "Tipo intestatario - cliente o fornitore";
    , HelpContextID = 78763722;
    , cFormVar="w_TIPCON",RowSource=""+"Clienti,"+"Fornitori", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPCON_1_6.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_6.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      0))
  endfunc

  func oTIPCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_FORSEL)
        bRes2=.link_1_7('Full')
      endif
      if .not. empty(.w_CLISEL)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oFORSEL_1_7 as StdField with uid="PFRQYKBUPL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FORSEL", cQueryName = "FORSEL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente oppure obsoleto",;
    ToolTipText = "Fornitore di selezione scadenze",;
    HelpContextID = 121745834,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=190, Top=39, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORSEL"

  func oFORSEL_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_tipcon ='F')
    endwith
   endif
  endfunc

  func oFORSEL_1_7.mHide()
    with this.Parent.oContained
      return (!.w_tipcon ='F')
    endwith
  endfunc

  func oFORSEL_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORSEL_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORSEL_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORSEL_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oFORSEL_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_FORSEL
     i_obj.ecpSave()
  endproc

  add object oCLISEL_1_8 as StdField with uid="BXVLTGMVIP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CLISEL", cQueryName = "CLISEL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice cliente inesistente oppure obsoleto",;
    ToolTipText = "Cliente di selezione scadenze",;
    HelpContextID = 121783514,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=190, Top=39, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_ACL", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLISEL"

  func oCLISEL_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_tipcon ='C')
    endwith
   endif
  endfunc

  func oCLISEL_1_8.mHide()
    with this.Parent.oContained
      return (!.w_tipcon ='C')
    endwith
  endfunc

  func oCLISEL_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLISEL_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLISEL_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLISEL_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACL',"Clienti",'',this.parent.oContained
  endproc
  proc oCLISEL_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CLISEL
     i_obj.ecpSave()
  endproc

  add object oDATINI_1_9 as StdField with uid="VQIOCNGNZD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di inizio selezione",;
    HelpContextID = 163291082,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=92, Top=72

  func oDATINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_11 as StdField with uid="WTJZXNNZRA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di fine selezione",;
    HelpContextID = 84844490,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=249, Top=72

  func oDATFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oDOCINI_1_12 as StdField with uid="AQIFKTREOL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento di inizio selezione",;
    HelpContextID = 163357130,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=420, Top=72

  func oDOCINI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN))
    endwith
    return bRes
  endfunc

  add object oDOCFIN_1_13 as StdField with uid="CNUAUUXTHP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento di fine selezione",;
    HelpContextID = 84910538,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=573, Top=72

  func oDOCFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN)
    endwith
    return bRes
  endfunc


  add object oBtn_1_14 as StdButton with uid="QYBPKEREZO",left=649, top=35, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rieseguire la ricerca con le nuove selezioni";
    , HelpContextID = 148087366;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSCG_BLD(this.Parent.oContained,"RIC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="IVUYXSPMRW",left=9, top=439, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il documento associato alla riga selezionata";
    , HelpContextID = 1188410;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_SERIALE, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="PXBGFOADEM",left=649, top=440, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 226595862;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomDoc as cp_zoombox with uid="FMGYZDFNXS",left=11, top=109, width=685,height=325,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSCG_KAD",bOptions=.t.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,bAdvOptions=.t.,bReadOnly=.t.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 45212134

  add object oDESCON_1_26 as StdField with uid="AZDEMLKLLW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 78752714,;
   bGlobalFont=.t.,;
    Height=21, Width=292, Left=343, Top=39, InputMask=replicate('X',40)


  add object oCLADOC_1_34 as StdCombo with uid="JLZJACBQTB",rtseq=22,rtrep=.f.,left=238,top=6,width=142,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 263308506;
    , cFormVar="w_CLADOC",RowSource=""+"Documenti interni,"+"Ordini,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLADOC_1_34.RadioValue()
    return(iif(this.value =1,'DI',;
    iif(this.value =2,'OR',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    space(2)))))))
  endfunc
  func oCLADOC_1_34.GetRadio()
    this.Parent.oContained.w_CLADOC = this.RadioValue()
    return .t.
  endfunc

  func oCLADOC_1_34.SetRadio()
    this.Parent.oContained.w_CLADOC=trim(this.Parent.oContained.w_CLADOC)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOC=='DI',1,;
      iif(this.Parent.oContained.w_CLADOC=='OR',2,;
      iif(this.Parent.oContained.w_CLADOC=='DT',3,;
      iif(this.Parent.oContained.w_CLADOC=='FA',4,;
      iif(this.Parent.oContained.w_CLADOC=='NC',5,;
      0)))))
  endfunc

  func oCLADOC_1_34.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="KCXNCIRHVM",Visible=.t., Left=5, Top=77,;
    Alignment=1, Width=86, Height=15,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="JYYRYYQBHK",Visible=.t., Left=168, Top=77,;
    Alignment=1, Width=80, Height=15,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="KUTBRHJKUV",Visible=.t., Left=178, Top=7,;
    Alignment=1, Width=57, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="PEGPUESRJN",Visible=.t., Left=389, Top=7,;
    Alignment=1, Width=31, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="XEJOWZAGNC",Visible=.t., Left=323, Top=77,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="HRIKGDXKQT",Visible=.t., Left=493, Top=77,;
    Alignment=1, Width=76, Height=15,;
    Caption="A data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="NYIFVFJARA",Visible=.t., Left=5, Top=7,;
    Alignment=1, Width=86, Height=15,;
    Caption="Ciclo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="UERGAHZWIL",Visible=.t., Left=5, Top=43,;
    Alignment=1, Width=86, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kad','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
