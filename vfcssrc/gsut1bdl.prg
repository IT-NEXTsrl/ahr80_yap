* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1bdl                                                        *
*              Driver per terminali datalogic (Documenti)                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-13                                                      *
* Last revis.: 2011-08-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut1bdl",oParentObject,m.pCursore,m.pTXTFile,m.pObjMsg,m.p_ArrayFieldName,m.p_ArrayFieldPosition)
return(i_retval)

define class tgsut1bdl as StdBatch
  * --- Local variables
  pCursore = space(10)
  pTXTFile = space(254)
  pObjMsg = .NULL.
  p_ArrayFieldName = space(40)
  p_ArrayFieldPosition = space(40)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Driver per datalogic.
    *     ========================================================
    *     Utilizzare questo batch come driver per caricare dati da foglio elettronico
    *     tramite l'INSERIMENTO DATI (GSAR_BPG=>GSAR_MPG).
    * --- La procedura riceve come parametro il nome del cursore che dovr� riempire.
    *     La struttura del cursore � fissa ed �:
    *     
    *     Create Cursor ( w_CURSORE ) ( CODICE C(20) NULL, UNIMIS C(3) NULL, QTAMOV N(12,3) NULL, PREZZO N(18,5) NULL,;
    *     LOTTO CHAR(20) NULL , CODMAG C(5) NULL,UBICAZIONE C(20) NULL , MATRICOLA C(40) NULL,SERRIF C(10) NULL )
    *     
    *     All'interno del batch vi � una chiamata per aprire una maschera di selezione 
    *     per caricare un file (*.csv) se questo non � stato precedentemente settato
    *     Se il secondo parametro � gi� stato valorizzato questa non apparir�.
    *     Il secondo parametro rappresenta appunto il file csv (con path relativo o assoluto)
    *     dal quale la procedura prelever� i dati. (Da specificare, se fisso, nell'anagrafica
    *     dispositivi nella casella parametro)
    *     
    *     Il terzo parametro rappresenta l'oggetto da utilizzare per visualizzare i messaggi (deve contenere un campo memo w_MSG).
    *     
    *     Il quarto e quinto parametro contengono le informazioni riguardanti posizione e nome campo (sono array passati come riferimento)
    *     In questo driver  non sono utilizzati, MA E' NECESSARIO CHE SIANO PRESENTI PER EVITARE SPIACEVOLI MALFUNZIONAMENTI!
    *     
    do GSUT_BDL with this.oParentObject, this.pCursore, this.pTXTFile, this.pObjMsg, this.p_ArrayFieldName, this.p_ArrayFieldPosition
  endproc


  proc Init(oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition)
    this.pCursore=pCursore
    this.pTXTFile=pTXTFile
    this.pObjMsg=pObjMsg
    this.p_ArrayFieldName=p_ArrayFieldName
    this.p_ArrayFieldPosition=p_ArrayFieldPosition
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition"
endproc
