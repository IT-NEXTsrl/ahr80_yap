* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bcr                                                        *
*              Check distinte ragg. effetti                                    *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_34]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-11                                                      *
* Last revis.: 2015-05-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper,pCURS,pFLBANC,pTipDis,pKTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bcr",oParentObject,m.pOper,m.pCURS,m.pFLBANC,m.pTipDis,m.pKTipo)
return(i_retval)

define class tgste_bcr as StdBatch
  * --- Local variables
  pOper = space(10)
  pCURS = space(10)
  pFLBANC = space(1)
  pTipDis = space(1)
  pKTipo = space(1)
  w_BRESULT = 0
  w_MESS = space(40)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dato un cursore Visual Fox Pro, verifica se al suo interno vi sono delle
    *     scadenze che vanno in negativo viste come effetti.
    *     
    *     Quindi esegue un raggruppamento per le partite associate ad un cliente
    *     con ANFLRAGG='S' e se queste sono positive le lascio altrimenti, le elimino 
    * --- pOper mi dice se devo segnalare o meno eventuali problemi,
    *     se 'D' eleimina solo le scadenze (nel caso sia lanciato da GSTE_BCP non segnala
    *     niente ed elimina gli effetti negativi per il tipo di distinta che si st� svolgendo..)
    *     
    *     Se 'C' allora non elimina ma visualizza una maschera con l'elenco degli effetti
    *     negativi!
    *     
    * --- Nome del cursore Visual Fox
    * --- Devo sapere come raggruppare le scadenze (su quale banca)
    *     Se non distinta non occorre passare questo parametro
    * --- Devo sapere come raggruppare le scadenze (se Distinta di tipo CA=Cambiale / Tratta non devo raggruppare)
    *     Se non distinta non occorre passare questo parametro
    * --- Occorre il tipo predominante della distinta (Attiva 'C' o Passiva 'F')
    this.w_BRESULT = ""
    * --- Determino l'elenco degli effetti negativi..
    * --- Determinazione Segno importo partita..
    *     Intestatario (C/F)                   C C C C F F F F
    *     Segno(D/A)                            D D A A D D A A
    *     Tipo dis.(Att C/Pass F)          A P A P A P A P
    *                                                       1-1-1 1 1-1-1 1
    if Type( this.pCurs+".Totimp" )<>"U"
      * --- Non posso utilizzare Xchk se non esiste sul tracciato, per cui creo una
      *     condizione di where differente se da GSTE_BCP (senza XCHK ) e da Check
      *     (con Xchk)
      if Type( this.pCurs+".Xchk" )="U"
        L_CmdWhere =" ANFLRAGG='T'"
      else
        L_CmdWhere =" ANFLRAGG='T' And  Xchk=1 "
      endif
      Select Sum( Totimp*iif(TipCon="C",1,-1)*iif( Segno="D" ,1,-1 )*IIF( TipCon=this.pKtipo , 1 , -1 ) ) As Totale, DatSca, TipCon, CodCon , ModPag, IIF( this.pFLBANC="A" , BANAPP , ; 
 IIF( this.pFLBANC="S" , BANNOS , Space(10) )) As Banca , PtCodRag As CodRag,IIF(this.ptipdis $ "BO-SC",Nvl(PTNUMCOR,Space(25)),Space(25)) as NumCor; 
 From ( this.pCurs ) Where &L_CmdWhere; 
 Group By DAtSca,TipCon,CodCon,ModPag,Banca,CodRag,NumCor ; 
 Having Totale<0 into Cursor tmp_negativi NoFilter
    else
      * --- In questo caso (controllo alla conferma della distinta) il check va girato
      *     al contrario sul segno (st� valutando partite di saldo)
      Select Sum( t_PTTOTIMP*iif(T_ptTipCon="C",1,-1)*iif( t_PT_Segno="D" ,-1,1 )*IIF( t_PTTipCon=this.pKtipo , 1 , -1 ) ) As Totale, t_PTDATSCA As DatSca, ; 
 t_PTTIPCON As TipCon, t_PTCODCON As CodCon , t_PTMODPAG As ModPag, IIF( this.pFLBANC="A" , t_PTBANAPP , ; 
 IIF( this.pFLBANC="S" , t_PTBANNOS , Space(10) )) As Banca , t_PtCodRag As CodRag , IIF(this.ptipdis="BO",Nvl(t_PTNUMCOR,Space(25)),Space(25)) as NumCor; 
 From ( this.pCurs ) Where t_FLRAGG="T"; 
 Group By DAtSca,TipCon,CodCon,ModPag,Banca,CodRag,NumCor ; 
 Having Totale<0 into Cursor tmp_negativi NoFilter
    endif
    if RecCount("Tmp_Negativi")>0
      if this.pOper="D"
        Delete From ( this.pCurs ) Where ; 
 DTOC(CP_TODATE(DatSca)) +TipCon+PADR(CodCon, 15, " ")+PADR(ModPag, 10, " ")+; 
 Padr( IIF( this.pFLBANC="A" , BANAPP , IIF( this.pFLBANC="S" , BANNOS , Space(10) )),10," ")+; 
 Padr( PtCodRag ,10," ") +IIF(this.ptipdis $ "BO-SC",Padr( Ptnumcor ,35," ") ,Space(25)) In; 
 ( Select DTOC(CP_TODATE(DatSca)) +TipCon+PADR(CodCon, 15, " ")+PADR(ModPag, 10, " ")+; 
 Padr( Banca ,10," ")+Padr( CodRag ,10 , " " )++Padr( NumCor ,10 , " " ) ; 
 From Tmp_Negativi )
      else
        * --- Segnalo il primo che incontro, questo evento dovrebbe essere infatti molto raro
        *     accade solo se l'utente seleziona, a fronte di una nota di credito e di una
        *     fattura, solo la nota di credito e tenta di abbinarla alla distinta..
         
 Select Tmp_Negativi 
 Go Top
        do case
          case Tipcon="C"
            this.w_MESS = ah_Msgformat("L'effetto del %1 con pagamento %2 relativo al cliente %3%0a causa delle selezioni, risulta negativo [%4]%0%0Per confermare selezionare scadenze ad esso associabili per riportarlo in positivo", DTOC(CP_TODATE(DATSCA)), Alltrim( ModPag ), Alltrim( CodCon ),ALLTRIM(TRAN( Totale , v_PV[38+VVP])) )
          case Tipcon="F"
            this.w_MESS = ah_Msgformat("L'effetto del %1 con pagamento %2 relativo al fornitore %3%0a causa delle selezioni, risulta negativo [%4]%0%0Per confermare selezionare scadenze ad esso associabili per riportarlo in positivo", DTOC(CP_TODATE(DATSCA)), Alltrim( ModPag ), Alltrim( CodCon ),ALLTRIM(TRAN( Totale , v_PV[38+VVP])) )
          otherwise
            this.w_MESS = ah_Msgformat("L'effetto del %1 con pagamento %2 relativo al conto %3%0a causa delle selezioni, risulta negativo [%4]%0%0Per confermare selezionare scadenze ad esso associabili per riportarlo in positivo", DTOC(CP_TODATE(DATSCA)), Alltrim( ModPag ), Alltrim( CodCon ),ALLTRIM(TRAN( Totale , v_PV[38+VVP])) )
        endcase
        this.w_BRESULT = this.w_MESS
      endif
    endif
    Use In Tmp_Negativi
    i_retcode = 'stop'
    i_retval = this.w_BRESULT
    return
  endproc


  proc Init(oParentObject,pOper,pCURS,pFLBANC,pTipDis,pKTipo)
    this.pOper=pOper
    this.pCURS=pCURS
    this.pFLBANC=pFLBANC
    this.pTipDis=pTipDis
    this.pKTipo=pKTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper,pCURS,pFLBANC,pTipDis,pKTipo"
endproc
