* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bao                                                        *
*              Seleziona acconto da ordine                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_21]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-14                                                      *
* Last revis.: 2008-04-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bao",oParentObject)
return(i_retval)

define class tgsve_bao as StdBatch
  * --- Local variables
  w_MVCODCON = space(15)
  w_MVTIPCON = space(1)
  w_MVCODESE = space(4)
  w_MVCODVAL = space(3)
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_DECUNI = 0
  w_MVDATDOC = ctod("  /  /  ")
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVSERIAL = space(10)
  w_MVACCONT = 0
  w_CAMBIO = 0
  w_DATADOC = ctod("  /  /  ")
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge gli Ordini Cliente e Seleziona l'Acconto da Fatturare (da GSVE_KDA)
    * --- Carica Parametri di ricerca dal Documento
    WITH this.oParentObject.oParentObject
    this.w_MVCODCON = .w_MVCODCON
    this.w_MVTIPCON = .w_MVTIPCON
    this.w_MVCODESE = .w_MVCODESE
    this.w_MVCODVAL = .w_MVCODVAL
    this.w_DECUNI = .w_DECUNI
    this.w_DATADOC = .w_MVDATDOC
    ENDWITH
    this.w_MVNUMDOC = 0
    this.w_MVALFDOC = Space(10)
    this.w_MVDATDOC = cp_CharToDate("  -  -  ")
    this.w_MVACCONT = 0
    this.w_MVSERIAL = SPACE(10)
    this.w_CODVAL = "   "
    this.w_CAOVAL = 0
    this.w_CAMBIO = 0
    * --- Seleziona Viste del Form
    do gsve_kao with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if NOT EMPTY(NVL(this.w_MVSERIAL," "))
      * --- Se Selezionato Record Notifica Riga Variata
      this.oParentObject.w_NUMDOC = NVL(this.w_MVNUMDOC, 0)
      this.oParentObject.w_ALFDOC = NVL(this.w_MVALFDOC, Space(10))
      this.oParentObject.w_DATDOC = CP_TODATE(this.w_MVDATDOC)
      this.oParentObject.w_MVRIFORD = this.w_MVSERIAL
      this.w_CAMBIO = GETCAM(this.w_MVCODVAL,this.w_DATADOC)
      if this.w_CODVAL<>this.w_MVCODVAL AND this.w_CAOVAL<>0
        this.w_MVACCONT = VAL2MON(this.w_MVACCONT,this.w_CAOVAL, 1, this.w_DATADOC,g_PERVAL,this.w_DECUNI)
      endif
      this.oParentObject.w_MVPREZZO = IIF(this.oParentObject.w_MVPREZZO=0, NVL(this.w_MVACCONT,0), this.oParentObject.w_MVPREZZO)
      if EMPTY(this.oParentObject.w_MVDESSUP)
        this.oParentObject.w_MVDESSUP = this.oParentObject.w_MVDESSUP + ah_Msgformat("Riferimento ordine n.: %1 - del %2", ALLTRIM(STR(this.oParentObject.w_NUMDOC,15)) + IIF(NOT EMPTY(this.oParentObject.w_ALFDOC), "/"+Alltrim(this.oParentObject.w_ALFDOC),""), DTOC(this.oParentObject.w_DATDOC) )
      else
        this.oParentObject.w_MVDESSUP = this.oParentObject.w_MVDESSUP + ah_Msgformat("%0Riferimento ordine n.: %1 - del %2", ALLTRIM(STR(this.oParentObject.w_NUMDOC,15)) + IIF(NOT EMPTY(this.oParentObject.w_ALFDOC), "/"+Alltrim(this.oParentObject.w_ALFDOC),""), DTOC(this.oParentObject.w_DATDOC) )
      endif
      SELECT (this.oParentObject.oParentObject.cTrsName)
      if I_SRV=" " AND NOT DELETED()
        REPLACE i_SRV WITH "U"
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
