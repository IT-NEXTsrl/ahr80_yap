* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mpr                                                        *
*              Progressivi numerazioni                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_10]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2015-03-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mpr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mpr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mpr")
  return

* --- Class definition
define class tgsar_mpr as StdPCForm
  Width  = 302
  Height = 137
  Top    = 153
  Left   = 243
  cComment = "Progressivi numerazioni"
  cPrg = "gsar_mpr"
  HelpContextID=147421033
  add object cnt as tcgsar_mpr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mpr as PCContext
  w_PRCODAZI = space(5)
  w_PRCODESE = space(4)
  w_PRTIPPAG = space(2)
  w_DESPAG = space(25)
  w_PRTIPCON = space(2)
  w_PRNUMPRO = 0
  proc Save(i_oFrom)
    this.w_PRCODAZI = i_oFrom.w_PRCODAZI
    this.w_PRCODESE = i_oFrom.w_PRCODESE
    this.w_PRTIPPAG = i_oFrom.w_PRTIPPAG
    this.w_DESPAG = i_oFrom.w_DESPAG
    this.w_PRTIPCON = i_oFrom.w_PRTIPCON
    this.w_PRNUMPRO = i_oFrom.w_PRNUMPRO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PRCODAZI = this.w_PRCODAZI
    i_oTo.w_PRCODESE = this.w_PRCODESE
    i_oTo.w_PRTIPPAG = this.w_PRTIPPAG
    i_oTo.w_DESPAG = this.w_DESPAG
    i_oTo.w_PRTIPCON = this.w_PRTIPCON
    i_oTo.w_PRNUMPRO = this.w_PRNUMPRO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mpr as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 302
  Height = 137
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-24"
  HelpContextID=147421033
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PRO_NUME_IDX = 0
  cFile = "PRO_NUME"
  cKeySelect = "PRCODAZI,PRCODESE"
  cKeyWhere  = "PRCODAZI=this.w_PRCODAZI and PRCODESE=this.w_PRCODESE"
  cKeyDetail  = "PRCODAZI=this.w_PRCODAZI and PRCODESE=this.w_PRCODESE and PRTIPPAG=this.w_PRTIPPAG and PRTIPCON=this.w_PRTIPCON"
  cKeyWhereODBC = '"PRCODAZI="+cp_ToStrODBC(this.w_PRCODAZI)';
      +'+" and PRCODESE="+cp_ToStrODBC(this.w_PRCODESE)';

  cKeyDetailWhereODBC = '"PRCODAZI="+cp_ToStrODBC(this.w_PRCODAZI)';
      +'+" and PRCODESE="+cp_ToStrODBC(this.w_PRCODESE)';
      +'+" and PRTIPPAG="+cp_ToStrODBC(this.w_PRTIPPAG)';
      +'+" and PRTIPCON="+cp_ToStrODBC(this.w_PRTIPCON)';

  cKeyWhereODBCqualified = '"PRO_NUME.PRCODAZI="+cp_ToStrODBC(this.w_PRCODAZI)';
      +'+" and PRO_NUME.PRCODESE="+cp_ToStrODBC(this.w_PRCODESE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mpr"
  cComment = "Progressivi numerazioni"
  i_nRowNum = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PRCODAZI = space(5)
  w_PRCODESE = space(4)
  w_PRTIPPAG = space(2)
  w_DESPAG = space(25)
  w_PRTIPCON = space(2)
  w_PRNUMPRO = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mprPag1","gsar_mpr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PRO_NUME'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRO_NUME_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRO_NUME_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mpr'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PRO_NUME where PRCODAZI=KeySet.PRCODAZI
    *                            and PRCODESE=KeySet.PRCODESE
    *                            and PRTIPPAG=KeySet.PRTIPPAG
    *                            and PRTIPCON=KeySet.PRTIPCON
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PRO_NUME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_NUME_IDX,2],this.bLoadRecFilter,this.PRO_NUME_IDX,"gsar_mpr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRO_NUME')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRO_NUME.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PRO_NUME '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PRCODAZI',this.w_PRCODAZI  ,'PRCODESE',this.w_PRCODESE  )
      select * from (i_cTable) PRO_NUME where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PRCODAZI = NVL(PRCODAZI,space(5))
        .w_PRCODESE = NVL(PRCODESE,space(4))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PRO_NUME')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESPAG = space(25)
          .w_PRTIPPAG = NVL(PRTIPPAG,space(2))
          .w_PRTIPCON = NVL(PRTIPCON,space(2))
          .w_PRNUMPRO = NVL(PRNUMPRO,0)
        .oPgFrm.Page1.oPag.oObj_2_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_6.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace PRTIPPAG with .w_PRTIPPAG
          replace PRTIPCON with .w_PRTIPCON
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_PRCODAZI=space(5)
      .w_PRCODESE=space(4)
      .w_PRTIPPAG=space(2)
      .w_DESPAG=space(25)
      .w_PRTIPCON=space(2)
      .w_PRNUMPRO=0
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_2_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRO_NUME')
    this.DoRTCalc(1,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_2_5.enabled = i_bVal
      .Page1.oPag.oObj_2_6.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PRO_NUME',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRO_NUME_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODAZI,"PRCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODESE,"PRCODESE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PRTIPPAG C(2);
      ,t_DESPAG C(25);
      ,t_PRTIPCON C(2);
      ,t_PRNUMPRO N(6);
      ,PRTIPPAG C(2);
      ,PRTIPCON C(2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mprbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPPAG_2_1.controlsource=this.cTrsName+'.t_PRTIPPAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESPAG_2_2.controlsource=this.cTrsName+'.t_DESPAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCON_2_3.controlsource=this.cTrsName+'.t_PRTIPCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRNUMPRO_2_4.controlsource=this.cTrsName+'.t_PRNUMPRO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(34)
    this.AddVLine(181)
    this.AddVLine(209)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPPAG_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRO_NUME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_NUME_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRO_NUME_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_NUME_IDX,2])
      *
      * insert into PRO_NUME
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRO_NUME')
        i_extval=cp_InsertValODBCExtFlds(this,'PRO_NUME')
        i_cFldBody=" "+;
                  "(PRCODAZI,PRCODESE,PRTIPPAG,PRTIPCON,PRNUMPRO,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PRCODAZI)+","+cp_ToStrODBC(this.w_PRCODESE)+","+cp_ToStrODBC(this.w_PRTIPPAG)+","+cp_ToStrODBC(this.w_PRTIPCON)+","+cp_ToStrODBC(this.w_PRNUMPRO)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRO_NUME')
        i_extval=cp_InsertValVFPExtFlds(this,'PRO_NUME')
        cp_CheckDeletedKey(i_cTable,0,'PRCODAZI',this.w_PRCODAZI,'PRCODESE',this.w_PRCODESE,'PRTIPPAG',this.w_PRTIPPAG,'PRTIPCON',this.w_PRTIPCON)
        INSERT INTO (i_cTable) (;
                   PRCODAZI;
                  ,PRCODESE;
                  ,PRTIPPAG;
                  ,PRTIPCON;
                  ,PRNUMPRO;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PRCODAZI;
                  ,this.w_PRCODESE;
                  ,this.w_PRTIPPAG;
                  ,this.w_PRTIPCON;
                  ,this.w_PRNUMPRO;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PRO_NUME_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_NUME_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_PRTIPPAG<>space(2) AND t_PRTIPCON<>' ') and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_NUME')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and PRTIPPAG="+cp_ToStrODBC(&i_TN.->PRTIPPAG)+;
                 " and PRTIPCON="+cp_ToStrODBC(&i_TN.->PRTIPCON)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_NUME')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and PRTIPPAG=&i_TN.->PRTIPPAG;
                      and PRTIPCON=&i_TN.->PRTIPCON;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_PRTIPPAG<>space(2) AND t_PRTIPCON<>' ') and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and PRTIPPAG="+cp_ToStrODBC(&i_TN.->PRTIPPAG)+;
                            " and PRTIPCON="+cp_ToStrODBC(&i_TN.->PRTIPCON)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and PRTIPPAG=&i_TN.->PRTIPPAG;
                            and PRTIPCON=&i_TN.->PRTIPCON;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace PRTIPPAG with this.w_PRTIPPAG
              replace PRTIPCON with this.w_PRTIPCON
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PRO_NUME
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PRO_NUME')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PRNUMPRO="+cp_ToStrODBC(this.w_PRNUMPRO)+;
                     ",PRTIPPAG="+cp_ToStrODBC(this.w_PRTIPPAG)+;
                     ",PRTIPCON="+cp_ToStrODBC(this.w_PRTIPCON)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and PRTIPPAG="+cp_ToStrODBC(PRTIPPAG)+;
                             " and PRTIPCON="+cp_ToStrODBC(PRTIPCON)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PRO_NUME')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PRNUMPRO=this.w_PRNUMPRO;
                     ,PRTIPPAG=this.w_PRTIPPAG;
                     ,PRTIPCON=this.w_PRTIPCON;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and PRTIPPAG=&i_TN.->PRTIPPAG;
                                      and PRTIPCON=&i_TN.->PRTIPCON;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PRO_NUME_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRO_NUME_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_PRTIPPAG<>space(2) AND t_PRTIPCON<>' ') and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PRO_NUME
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and PRTIPPAG="+cp_ToStrODBC(&i_TN.->PRTIPPAG)+;
                            " and PRTIPCON="+cp_ToStrODBC(&i_TN.->PRTIPCON)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and PRTIPPAG=&i_TN.->PRTIPPAG;
                              and PRTIPCON=&i_TN.->PRTIPCON;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_PRTIPPAG<>space(2) AND t_PRTIPCON<>' ') and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRO_NUME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRO_NUME_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_2_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_6.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_6.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_2_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_6.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPPAG_2_1.value==this.w_PRTIPPAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPPAG_2_1.value=this.w_PRTIPPAG
      replace t_PRTIPPAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPPAG_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPAG_2_2.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPAG_2_2.value=this.w_DESPAG
      replace t_DESPAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESPAG_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCON_2_3.value==this.w_PRTIPCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCON_2_3.value=this.w_PRTIPCON
      replace t_PRTIPCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNUMPRO_2_4.value==this.w_PRNUMPRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNUMPRO_2_4.value=this.w_PRNUMPRO
      replace t_PRNUMPRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRNUMPRO_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'PRO_NUME')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_PRTIPPAG $ 'SE-SC-SD-RB-BO-MA-CA-RD-RI-AB-BE  ') and (.w_PRTIPPAG<>space(2) AND .w_PRTIPCON<>' ')
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPPAG_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Indicare un pagamento congruente: (RB, BO, MA, CA, RD, RI,SD,SC,SE)")
        case   not(alltrim(.w_PRTIPCON) $ 'CF ' OR .w_PRTIPPAG='AB') and (.w_PRTIPPAG<>space(2) AND .w_PRTIPCON<>' ')
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRTIPCON_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Indicare: C =cliente o F =fornitore")
      endcase
      if .w_PRTIPPAG<>space(2) AND .w_PRTIPCON<>' '
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_PRTIPPAG<>space(2) AND t_PRTIPCON<>' ')
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PRTIPPAG=space(2)
      .w_DESPAG=space(25)
      .w_PRTIPCON=space(2)
      .w_PRNUMPRO=0
        .oPgFrm.Page1.oPag.oObj_2_5.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_6.Calculate()
    endwith
    this.DoRTCalc(1,6,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PRTIPPAG = t_PRTIPPAG
    this.w_DESPAG = t_DESPAG
    this.w_PRTIPCON = t_PRTIPCON
    this.w_PRNUMPRO = t_PRNUMPRO
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PRTIPPAG with this.w_PRTIPPAG
    replace t_DESPAG with this.w_DESPAG
    replace t_PRTIPCON with this.w_PRTIPCON
    replace t_PRNUMPRO with this.w_PRNUMPRO
    if i_srv='A'
      replace PRTIPPAG with this.w_PRTIPPAG
      replace PRTIPCON with this.w_PRTIPCON
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mprPag1 as StdContainer
  Width  = 298
  height = 137
  stdWidth  = 298
  stdheight = 137
  resizeXpos=103
  resizeYpos=95
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=0, width=292,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="PRTIPPAG",Label1="Tipo",Field2="DESPAG",Label2="Descrizione",Field3="PRTIPCON",Label3="C/F",Field4="PRNUMPRO",Label4="Progressivo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168669818

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=19,;
    width=288+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=20,width=287+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oObj_2_5 as cp_runprogram with uid="UKRXAKCNJU",width=160,height=18,;
   left=184, top=157,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='CHKMPR("1")',;
    cEvent = "w_PRTIPPAG Changed",;
    nPag=2;
    , HelpContextID = 30576614

  add object oObj_2_6 as cp_runprogram with uid="NNOYHTVRZS",width=160,height=18,;
   left=182, top=178,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='CHKMPR("0")',;
    cEvent = "Load",;
    nPag=2;
    , HelpContextID = 30576614

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mprBodyRow as CPBodyRowCnt
  Width=278
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPRTIPPAG_2_1 as StdTrsField with uid="COWTLMQBAL",rtseq=3,rtrep=.t.,;
    cFormVar="w_PRTIPPAG",value=space(2),isprimarykey=.t.,;
    ToolTipText = "Tipo pagamento",;
    HelpContextID = 58384323,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Indicare un pagamento congruente: (RB, BO, MA, CA, RD, RI,SD,SC,SE)",;
   bGlobalFont=.t.,;
    Height=17, Width=30, Left=-2, Top=0, cSayPict=['!!'], cGetPict=['!!'], InputMask=replicate('X',2)

  func oPRTIPPAG_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRTIPPAG $ 'SE-SC-SD-RB-BO-MA-CA-RD-RI-AB-BE  ')
    endwith
    return bRes
  endfunc

  add object oDESPAG_2_2 as StdTrsField with uid="DOMOYTOPKQ",rtseq=4,rtrep=.t.,;
    cFormVar="w_DESPAG",value=space(25),enabled=.f.,;
    HelpContextID = 224656842,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=142, Left=33, Top=0, InputMask=replicate('X',25)

  add object oPRTIPCON_2_3 as StdTrsField with uid="UMKZRXBIKG",rtseq=5,rtrep=.t.,;
    cFormVar="w_PRTIPCON",value=space(2),isprimarykey=.t.,;
    ToolTipText = "Tipo: cliente o fornitore oppure serie in caso di avviso",;
    HelpContextID = 8052668,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Indicare: C =cliente o F =fornitore",;
   bGlobalFont=.t.,;
    Height=17, Width=23, Left=180, Top=0, cSayPict=['!!'], cGetPict=['!!'], InputMask=replicate('X',2)

  func oPRTIPCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_PRTIPCON) $ 'CF ' OR .w_PRTIPPAG='AB')
    endwith
    return bRes
  endfunc

  add object oPRNUMPRO_2_4 as StdTrsField with uid="MKGXSOCXUJ",rtseq=6,rtrep=.t.,;
    cFormVar="w_PRNUMPRO",value=0,;
    ToolTipText = "Progressivo numerazione",;
    HelpContextID = 207667269,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=65, Left=208, Top=0, cSayPict=["999999"], cGetPict=["999999"]
  add object oLast as LastKeyMover
  * ---
  func oPRTIPPAG_2_1.When()
    return(.t.)
  proc oPRTIPPAG_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPRTIPPAG_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mpr','PRO_NUME','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PRCODAZI=PRO_NUME.PRCODAZI";
  +" and "+i_cAliasName2+".PRCODESE=PRO_NUME.PRCODESE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
