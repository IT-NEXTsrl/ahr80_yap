* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bou                                                        *
*              Salva output utente                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-12                                                      *
* Last revis.: 2008-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bou",oParentObject,m.pAzione)
return(i_retval)

define class tgsut_bou as StdBatch
  * --- Local variables
  pAzione = space(2)
  NC = space(10)
  w_POS = 0
  w_ERRORE = space(200)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- parametro
    * --- Nome cursore collegato allo zoom
    this.NC = this.oParentObject.w_ZoomSel.cCursor
    * --- Test parametro
    do case
      case this.pAzione = "SELEZIONE"
        if used(this.NC)
          SELECT (this.NC)
          this.w_POS = RECNO ((this.NC))
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
          if this.w_POS>0
            GOTO this.w_POS
          endif
        endif
      case this.pAzione = "SALVA"
        SELECT * FROM (this.NC) where XCHK=1 into CURSOR DATI
        if USED("DATI")
          SELECT DATI
          if RECCOUNT() = 0
            ah_errormsg( "Non � stato selezionato nessun output utente")
            i_retcode = 'stop'
            return
          endif
          this.w_ERRORE = scriveinf(this.oParentObject.w_PATH , "DATI" , this.oParentObject.w_APPLICAZIONE, this.oParentObject.w_LOCALIZZAZIONE , this.oParentObject.w_MOTORE)
          if NOT EMPTY (this.w_ERRORE)
            if this.w_ERRORE <>"NO MESSAGE"
              ah_errormsg( this.w_ERRORE)
            endif
          else
            ah_errormsg( "Elaborazione terminata con successo")
          endif
          SELECT DATI 
 USE
        else
          ah_errormsg( "Non � stato selezionato nessun output utente")
        endif
      case this.pAzione = "PATH"
        this.oParentObject.w_PATH = GETFILE()
    endcase
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
