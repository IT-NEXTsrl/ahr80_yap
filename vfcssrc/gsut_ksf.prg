* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_ksf                                                        *
*              Configurazione stampa su file                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-07-08                                                      *
* Last revis.: 2010-04-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_ksf",oParentObject))

* --- Class definition
define class tgsut_ksf as StdForm
  Top    = 3
  Left   = 2

  * --- Standard Properties
  Width  = 642
  Height = 414
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-04-26"
  HelpContextID=99173225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_ksf"
  cComment = "Configurazione stampa su file"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_WORKSTATION = 0
  o_WORKSTATION = 0
  w_PCNAME = space(20)
  o_PCNAME = space(20)

  * --- Children pointers
  GSUT_MSF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSUT_MSF additive
    with this
      .Pages(1).addobject("oPag","tgsut_ksfPag1","gsut_ksf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oWORKSTATION_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSUT_MSF
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSUT_MSF = CREATEOBJECT('stdDynamicChild',this,'GSUT_MSF',this.oPgFrm.Page1.oPag.oLinkPC_1_3)
    this.GSUT_MSF.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSUT_MSF)
      this.GSUT_MSF.DestroyChildrenChain()
      this.GSUT_MSF=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_3')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSUT_MSF.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSUT_MSF.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSUT_MSF.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSUT_MSF.SetKey(;
            .w_PCNAME,"SFPCNAME";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSUT_MSF.ChangeRow(this.cRowID+'      1',1;
             ,.w_PCNAME,"SFPCNAME";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSUT_MSF)
        i_f=.GSUT_MSF.BuildFilter()
        if !(i_f==.GSUT_MSF.cQueryFilter)
          i_fnidx=.GSUT_MSF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSUT_MSF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSUT_MSF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSUT_MSF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSUT_MSF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSUT_MSF(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSUT_MSF.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSUT_MSF(i_ask)
    if this.GSUT_MSF.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP))
      cp_BeginTrs()
      this.GSUT_MSF.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_WORKSTATION=0
      .w_PCNAME=space(20)
        .w_WORKSTATION = 1
        .w_PCNAME = IIF( .w_WORKSTATION = 1, Left( Sys(0), Rat('#',Sys( 0 ))-1) , "INSTALLAZIONE")
      .GSUT_MSF.NewDocument()
      .GSUT_MSF.ChangeRow('1',1,.w_PCNAME,"SFPCNAME")
      if not(.GSUT_MSF.bLoaded)
        .GSUT_MSF.SetKey(.w_PCNAME,"SFPCNAME")
      endif
      .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_ksf
    endproc
    proc Save_GSUT_MSF(i_ask)
       if this.GSUT_MSF.IsAChildUpdated() and (!i_ask or ah_YesNo(MSG_SAVE_CHANGES_QP))
         cp_BeginTrs()
         this.GSUT_MSF.mReplace(.t.)
         cp_EndTrs()
       endif
    endproc
    * --- Ridefinisco EcpQuit, premendo esc non effettuava la checkform se
    * --- salvo le modifiche (modificato messaggio)
    proc ecpQuit()
      * --- Move without activating controls
      this.cFunction='Filter'
      this.__dummy__.enabled=.t.
      this.__dummy__.Setfocus()
      * ---
      *this.Save_GSUT_MSF(.t.)
      if !this.GSUT_MSF.IsAChildUpdated() or ah_YesNo(MSG_DISCARD_CHANGES_QP)
        this.Hide()
        this.NotifyEvent("Edit Aborted")
        this.NotifyEvent("Done")
        this.Release()
      endif
    endproc
    proc QueryUnload()
      *this.Save_GSUT_MSF(.t.)
      if this.GSUT_MSF.IsAChildUpdated()
        if ah_YesNo(MSG_DISCARD_CHANGES_QP)
          this.Hide()
          this.NotifyEvent("Edit Aborted")
          this.NotifyEvent("Done")
        else
          nodefault
        endif
      else
        this.Hide()
        this.NotifyEvent("Edit Aborted")
        this.NotifyEvent("Done")
      endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSUT_MSF.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSUT_MSF.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_WORKSTATION<>.w_WORKSTATION
            .w_PCNAME = IIF( .w_WORKSTATION = 1, Left( Sys(0), Rat('#',Sys( 0 ))-1) , "INSTALLAZIONE")
        endif
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_PCNAME<>.o_PCNAME
          .Save_GSUT_MSF(.t.)
          .GSUT_MSF.NewDocument()
          .GSUT_MSF.ChangeRow('1',1,.w_PCNAME,"SFPCNAME")
          if not(.GSUT_MSF.bLoaded)
            .GSUT_MSF.SetKey(.w_PCNAME,"SFPCNAME")
          endif
        endif
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_6.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oWORKSTATION_1_1.enabled = this.oPgFrm.Page1.oPag.oWORKSTATION_1_1.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_6.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_ksf
    * --- Modifico il tooltip in base alla workstation
    local l_obj
    if cEvent = 'w_WORKSTATION Changed'
        l_obj = iif(this.w_WORKSTATION=2,this.GetCtrl('\<Ripristina'),this.GetCtrl('A\<pplica'))
        l_obj.ToolTipText=iif(this.w_WORKSTATION=1,'Premere per impostare i valori di default','Premere per applicare a tutti le impostazioni salvate')
        l_obj.Caption=iif(this.w_WORKSTATION=1,'\<Ripristina','A\<pplica')
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oWORKSTATION_1_1.RadioValue()==this.w_WORKSTATION)
      this.oPgFrm.Page1.oPag.oWORKSTATION_1_1.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSUT_MSF.CheckForm()
      if i_bres
        i_bres=  .GSUT_MSF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_WORKSTATION = this.w_WORKSTATION
    this.o_PCNAME = this.w_PCNAME
    * --- GSUT_MSF : Depends On
    this.GSUT_MSF.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsut_ksfPag1 as StdContainer
  Width  = 638
  height = 414
  stdWidth  = 638
  stdheight = 414
  resizeXpos=247
  resizeYpos=192
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oWORKSTATION_1_1 as StdCombo with uid="SJJMUQJCIM",rtseq=1,rtrep=.f.,left=96,top=10,width=146,height=21;
    , ToolTipText = "Selezionare la configurazione da modificare";
    , HelpContextID = 60581194;
    , cFormVar="w_WORKSTATION",RowSource=""+"Workstation,"+"Installazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oWORKSTATION_1_1.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    0)))
  endfunc
  func oWORKSTATION_1_1.GetRadio()
    this.Parent.oContained.w_WORKSTATION = this.RadioValue()
    return .t.
  endfunc

  func oWORKSTATION_1_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_WORKSTATION==1,1,;
      iif(this.Parent.oContained.w_WORKSTATION==2,2,;
      0))
  endfunc

  func oWORKSTATION_1_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (cp_IsAdministrator(.t.))
    endwith
   endif
  endfunc


  add object oLinkPC_1_3 as stdDynamicChildContainer with uid="BHVZGGBTUT",left=8, top=40, width=627, height=318, bOnScreen=.t.;



  add object oBtn_1_4 as StdButton with uid="RJEVQFFKBX",left=9, top=364, width=48,height=45,;
    CpPicture="bmp\parametri.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per impostare i valori di default";
    , HelpContextID = 99173130;
    , Caption='\<Ripristina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .NotifyEvent('Default')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_6 as cp_runprogram with uid="NJRASKZXDM",left=4, top=438, width=238,height=20,;
    caption='GSUT_BSF(DEFA)',;
   bGlobalFont=.t.,;
    prg="GSUT_BSF('DEFA')",;
    cEvent = "Default",;
    nPag=1;
    , HelpContextID = 263912236


  add object oBtn_1_9 as StdButton with uid="ZCZOEJDSMM",left=529, top=364, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le operazioni svolte";
    , HelpContextID = 260208150;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_10 as StdButton with uid="OFLKLPHFIY",left=580, top=364, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare le operazioni svolte";
    , HelpContextID = 260208150;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="KURISVUTDN",Visible=.t., Left=11, Top=10,;
    Alignment=1, Width=81, Height=17,;
    Caption="Validi per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="RCTMHWPLNR",Visible=.t., Left=3, Top=465,;
    Alignment=0, Width=397, Height=18,;
    Caption="Attenzione: se viene modificata la caption del bottone ripristina"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="AGSXCLZKWQ",Visible=.t., Left=82, Top=482,;
    Alignment=0, Width=333, Height=18,;
    Caption="deve essere modificata l'area manuale 'notify event init'"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_ksf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
