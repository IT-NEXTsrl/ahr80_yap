* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bcs                                                        *
*              Cambia stato PDA                                                *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_30]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-20                                                      *
* Last revis.: 2014-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bcs",oParentObject,m.pAzione)
return(i_retval)

define class tgsac_bcs as StdBatch
  * --- Local variables
  pAzione = space(2)
  NC = space(10)
  w_OK = .f.
  w_Nrecsel = 0
  w_MESS = space(10)
  w_PDSERIAL = space(10)
  w_PDCODCON = space(15)
  w_STATO = space(1)
  w_PDROWNUM = 0
  w_PDCODMAG = space(5)
  * --- WorkFile variables
  PDA_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cambia lo stato delle PDA (da GSAC_KCS)
    * --- Nome cursore collegato allo zoom
    this.NC = this.oParentObject.w_ZoomSel.cCursor
    this.w_OK = .T.
    do case
      case this.pAzione = "SS"
        * --- Seleziona/Deseleziona tutte le righe dello zoom
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            UPDATE (this.NC) SET xChk=iif(this.oParentObject.w_SELEZI="S",1,0)
          endif
        endif
      case this.pAzione = "INTERROGA"
        * --- Visualizza Zoom Elendo ODP periodo
        This.oParentObject.NotifyEvent("Interroga")
        * --- Attiva la pagina 2 automaticamente
        this.oParentObject.oPgFrm.ActivePage = 2
      case this.pAzione = "CAMBIA"
        * --- Aggiorna lo stato delle PDA selezionate
        Select (this.NC)
        count for xchk=1 to this.w_Nrecsel
        if this.w_Nrecsel=0
          ah_ErrorMsg("Nessun record selezionato","!","")
        else
          * --- Se Suggerito => Confermato
          this.w_STATO = "D"
          * --- begin transaction
          cp_BeginTrs()
          scan for xchk=1
          this.w_PDSERIAL = pdserial
          this.w_PDROWNUM = pdrownum
          * --- Controlla il magazzino e il fornitore
          this.w_PDCODMAG = nvl(pdcodmag,space(5))
          this.w_PDCODCON = nvl(pdcodcon,space(15))
          this.w_OK = not empty(this.w_PDCODMAG)
          * --- In precedenza
          *     and not empty(w_PDCODCON)
          if this.w_OK
            * --- Write into PDA_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PDA_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PDA_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PDA_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PDSTATUS ="+cp_NullLink(cp_ToStrODBC(this.w_STATO),'PDA_DETT','PDSTATUS');
                  +i_ccchkf ;
              +" where ";
                  +"PDSERIAL = "+cp_ToStrODBC(this.w_PDSERIAL);
                  +" and PDROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
                     )
            else
              update (i_cTable) set;
                  PDSTATUS = this.w_STATO;
                  &i_ccchkf. ;
               where;
                  PDSERIAL = this.w_PDSERIAL;
                  and PDROWNUM = this.w_PDROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            if not used("_err_")
              CREATE CURSOR _err_ ;
              (pdserial C(10), pdrownum N(8,0), pdcodmag C(5), pdcodcon C(15))
            endif
            SELECT _err_
            APPEND BLANK
            SCATTER MEMVAR
            pdserial = this.w_PDSERIAL
            pdrownum = this.w_PDROWNUM
            pdcodmag = this.w_PDCODMAG
            pdcodcon = this.w_PDCODCON
            GATHER MEMVAR
            Select (this.NC)
          endif
          Endscan
          * --- commit
          cp_EndTrs(.t.)
          This.oParentObject.NotifyEvent("Interroga")
        endif
        if used("_err_")
          this.w_MESS = "Non � stato possibile confermare alcune PDA a causa di dati incompleti"
          ah_ErrorMsg(this.w_MESS,"!","")
          * --- Stampa report errori
          SELECT * FROM _err_ INTO CURSOR __tmp__ ORDER BY 1,2
          USE IN _err_
          cp_chprn("query\gsac_ser", " ", this)
        endif
    endcase
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PDA_DETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
