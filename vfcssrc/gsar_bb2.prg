* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bb2                                                        *
*              Check form nominativi                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-08                                                      *
* Last revis.: 2009-02-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bb2",oParentObject)
return(i_retval)

define class tgsar_bb2 as StdBatch
  * --- Local variables
  w_PARIVA = space(12)
  w_CODFIS = space(16)
  w_MESG = space(500)
  * --- WorkFile variables
  OFF_NOMI_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla che il badge non sia usato da altri cred. deb. Bage
    this.oParentObject.w_RESCHK = 0
    * --- Verifica codice fiscale / partita IVA con conto  collegato:
    *     Se il beneficiario (se attivo check debitore creditore diverso) ha
    *     partita IVA e/o codice fiscale pieni e uguale al codice fiscale / partita iva
    *     del cliente collegato allora impedisco il salvataggio...
    if this.oParentObject.w_NOFLGBEN="B" And Not Empty( this.oParentObject.w_NOCODCON ) And Not Empty( this.oParentObject.w_NOPARIVA + this.oParentObject.w_NOCODFIS ) And this.oParentObject.w_NOTIPCON<>"G"
      * --- Per costruzione non dovrei mai trovare il conto collegato obsoleto...
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANPARIVA,ANCODFIS"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_NOTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_NOCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANPARIVA,ANCODFIS;
          from (i_cTable) where;
              ANTIPCON = this.oParentObject.w_NOTIPCON;
              and ANCODICE = this.oParentObject.w_NOCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PARIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
        this.w_CODFIS = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.oParentObject.w_NOPARIVA=this.w_PARIVA And Not Empty( this.oParentObject.w_NOPARIVA )
        ah_errormsg("Partita iva coincidente con conto collegato%0Impossibile confermare")
        this.oParentObject.w_RESCHK = -1
      endif
      if this.oParentObject.w_NOCODFIS=this.w_CODFIS And Not Empty( this.oParentObject.w_NOCODFIS ) And this.oParentObject.w_RESCHK=0
        ah_errormsg("Codice fiscale coincidente con conto collegato%0Impossibile confermare")
        this.oParentObject.w_RESCHK = -1
      endif
    endif
    if this.oParentObject.w_RESCHK=0 And Not ( this.oParentObject.w_NOSOGGET="EN" OR EMPTY (this.oParentObject.w_NOBADGE) )
      * --- Verifico univocit� badge...
      this.w_MESG = ""
      * --- Select from OFF_NOMI
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select NOCODICE as CODICE  from "+i_cTable+" OFF_NOMI ";
            +" where NOBADGE="+cp_ToStrODBC(this.oParentObject.w_NOBADGE)+"";
             ,"_Curs_OFF_NOMI")
      else
        select NOCODICE as CODICE from (i_cTable);
         where NOBADGE=this.oParentObject.w_NOBADGE;
          into cursor _Curs_OFF_NOMI
      endif
      if used('_Curs_OFF_NOMI')
        select _Curs_OFF_NOMI
        locate for 1=1
        do while not(eof())
        if _Curs_OFF_NOMI.CODICE<>this.oParentObject.w_NOCODICE
          this.w_MESG = this.w_MESG+ah_MSGFORMAT("Codice badge gi� utilizzato dal  debitore/creditore diverso %1%0Impossibile confermare", _Curs_OFF_NOMI.CODICE)
        endif
          select _Curs_OFF_NOMI
          continue
        enddo
        use
      endif
      if NOT EMPTY (this.w_MESG)
        ah_errormsg(this.w_MESG)
        this.oParentObject.w_RESCHK = -1
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_OFF_NOMI')
      use in _Curs_OFF_NOMI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
