* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gs___bng                                                        *
*              Creazione nuovo gruppo                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-11                                                      *
* Last revis.: 2005-04-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODE,pMOD
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgs___bng",oParentObject,m.pCODE,m.pMOD)
return(i_retval)

define class tgs___bng as StdBatch
  * --- Local variables
  pCODE = 0
  pMOD = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch scatta alla creazione di un nuovo gruppo, il batch � sotto transazione
    *     per bloccare la creazione in caso di errore occore utilizzare una 
    *     "Transaction Error"
    * --- Parametri:
    *     pCODE = Codice Gruppo da inserire
    *     pMOD = .t. modifica gruppo, .f. nuovo gruppo
    * --- Affinch� il batch non ritorni errore "Property MCALLEDBATCHEND is not found"
    *     devo porre bUpdateParentObject a false
    this.bUpdateParentObject = .F.
  endproc


  proc Init(oParentObject,pCODE,pMOD)
    this.pCODE=pCODE
    this.pMOD=pMOD
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODE,pMOD"
endproc
