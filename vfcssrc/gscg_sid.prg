* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sid                                                        *
*              Stampa dettagliata IVA                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-06                                                      *
* Last revis.: 2011-07-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sid",oParentObject))

* --- Class definition
define class tgscg_sid as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 534
  Height = 334
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-07-05"
  HelpContextID=258614633
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  VOCIIVA_IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gscg_sid"
  cComment = "Stampa dettagliata IVA"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(5)
  w_ESE = space(4)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_COMINI = ctod('  /  /  ')
  w_COMFIN = ctod('  /  /  ')
  w_TIPCLF = space(1)
  w_CODINI = space(15)
  w_CODFIN = space(15)
  w_CODIVA = space(5)
  w_TIPREG = space(1)
  o_TIPREG = space(1)
  w_NUMREG = 0
  w_DEFI = space(10)
  w_TIPOCLF = space(1)
  w_ONUME = 0
  w_DESIVA = space(35)
  w_DESCRI1 = space(40)
  w_DESCRI2 = space(40)
  w_TIPO = space(1)
  w_FLBLLS = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_sidPag1","gscg_sid",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oESE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VOCIIVA'
    this.cWorkTables[3]='ESERCIZI'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(5)
      .w_ESE=space(4)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_COMINI=ctod("  /  /  ")
      .w_COMFIN=ctod("  /  /  ")
      .w_TIPCLF=space(1)
      .w_CODINI=space(15)
      .w_CODFIN=space(15)
      .w_CODIVA=space(5)
      .w_TIPREG=space(1)
      .w_NUMREG=0
      .w_DEFI=space(10)
      .w_TIPOCLF=space(1)
      .w_ONUME=0
      .w_DESIVA=space(35)
      .w_DESCRI1=space(40)
      .w_DESCRI2=space(40)
      .w_TIPO=space(1)
      .w_FLBLLS=space(1)
        .w_AZIENDA = i_CODAZI
        .w_ESE = g_CODESE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ESE))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,6,.f.)
        .w_TIPCLF = 'T'
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODINI))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODFIN))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODIVA))
          .link_1_10('Full')
        endif
        .w_TIPREG = 'T'
        .w_NUMREG = 0
        .w_DEFI = ''
        .w_TIPOCLF = IIF(.w_TIPCLF='T', ' ', .w_TIPCLF)
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
          .DoRTCalc(15,18,.f.)
        .w_TIPO = IIF(.w_tipreg='T',' ',.w_tipreg)
    endwith
    this.DoRTCalc(20,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_AZIENDA = i_CODAZI
        .DoRTCalc(2,11,.t.)
        if .o_TIPREG<>.w_TIPREG
            .w_NUMREG = 0
        endif
        .DoRTCalc(13,13,.t.)
            .w_TIPOCLF = IIF(.w_TIPCLF='T', ' ', .w_TIPCLF)
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .DoRTCalc(15,18,.t.)
        if .o_TIPREG<>.w_TIPREG
            .w_TIPO = IIF(.w_tipreg='T',' ',.w_tipreg)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(20,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODINI_1_8.enabled = this.oPgFrm.Page1.oPag.oCODINI_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCODFIN_1_9.enabled = this.oPgFrm.Page1.oPag.oCODFIN_1_9.mCond()
    this.oPgFrm.Page1.oPag.oNUMREG_1_12.enabled = this.oPgFrm.Page1.oPag.oNUMREG_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_ESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESE_1_2'),i_cWhere,'GSAR_KES',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_ESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESE = NVL(_Link_.ESCODESE,space(4))
      this.w_DATINI = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESE = space(4)
      endif
      this.w_DATINI = ctod("  /  /  ")
      this.w_DATFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLF;
                     ,'ANCODICE',trim(this.w_CODINI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODINI_1_8'),i_cWhere,'',"Clienti / fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLF;
                       ,'ANCODICE',this.w_CODINI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(15)
      endif
      this.w_DESCRI1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLF;
                     ,'ANCODICE',trim(this.w_CODFIN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_9'),i_cWhere,'',"Clienti / fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLF;
                       ,'ANCODICE',this.w_CODFIN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI2 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(15)
      endif
      this.w_DESCRI2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_CODIVA))
          select IVCODIVA,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCODIVA_1_10'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oESE_1_2.value==this.w_ESE)
      this.oPgFrm.Page1.oPag.oESE_1_2.value=this.w_ESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_3.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_3.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_4.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_4.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMINI_1_5.value==this.w_COMINI)
      this.oPgFrm.Page1.oPag.oCOMINI_1_5.value=this.w_COMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMFIN_1_6.value==this.w_COMFIN)
      this.oPgFrm.Page1.oPag.oCOMFIN_1_6.value=this.w_COMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCLF_1_7.RadioValue()==this.w_TIPCLF)
      this.oPgFrm.Page1.oPag.oTIPCLF_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_8.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_8.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_9.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_9.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIVA_1_10.value==this.w_CODIVA)
      this.oPgFrm.Page1.oPag.oCODIVA_1_10.value=this.w_CODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPREG_1_11.RadioValue()==this.w_TIPREG)
      this.oPgFrm.Page1.oPag.oTIPREG_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMREG_1_12.value==this.w_NUMREG)
      this.oPgFrm.Page1.oPag.oNUMREG_1_12.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDEFI_1_14.RadioValue()==this.w_DEFI)
      this.oPgFrm.Page1.oPag.oDEFI_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_21.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_21.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_26.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_26.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI2_1_27.value==this.w_DESCRI2)
      this.oPgFrm.Page1.oPag.oDESCRI2_1_27.value=this.w_DESCRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oFLBLLS_1_36.RadioValue()==this.w_FLBLLS)
      this.oPgFrm.Page1.oPag.oFLBLLS_1_36.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_DATFIN) or .w_DATINI<=.w_DATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_DATINI) or .w_DATINI<=.w_DATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_COMFIN) or .w_COMINI<=.w_COMFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_COMINI) or .w_COMINI<=.w_COMFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMFIN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPREG = this.w_TIPREG
    return

enddefine

* --- Define pages as container
define class tgscg_sidPag1 as StdContainer
  Width  = 530
  height = 334
  stdWidth  = 530
  stdheight = 334
  resizeXpos=303
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oESE_1_2 as StdField with uid="CTFGUWXZPD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ESE", cQueryName = "ESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 258309562,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=111, Top=9, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESE"

  func oESE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oESE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"Esercizi",'',this.parent.oContained
  endproc
  proc oESE_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_ESE
     i_obj.ecpSave()
  endproc

  add object oDATINI_1_3 as StdField with uid="KQPPUKNLMW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data registrazione di inizio stampa",;
    HelpContextID = 20684746,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=111, Top=35

  func oDATINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATFIN) or .w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_4 as StdField with uid="JWAYBCEWUW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data registrazione di fine stampa",;
    HelpContextID = 57761846,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=111, Top=61

  func oDATFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATINI) or .w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oCOMINI_1_5 as StdField with uid="FDEQDLHQAB",rtseq=5,rtrep=.f.,;
    cFormVar = "w_COMINI", cQueryName = "COMINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data competenza IVA di inizio stampa",;
    HelpContextID = 20709850,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=441, Top=35

  func oCOMINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_COMFIN) or .w_COMINI<=.w_COMFIN)
    endwith
    return bRes
  endfunc

  add object oCOMFIN_1_6 as StdField with uid="TXVKABHBIC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_COMFIN", cQueryName = "COMFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data competenza IVA di fine stampa",;
    HelpContextID = 57736742,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=441, Top=61

  func oCOMFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_COMINI) or .w_COMINI<=.w_COMFIN)
    endwith
    return bRes
  endfunc


  add object oTIPCLF_1_7 as StdCombo with uid="LXJVDJQCLV",rtseq=7,rtrep=.f.,left=112,top=90,width=79,height=21;
    , ToolTipText = "Tipo conto";
    , HelpContextID = 73520842;
    , cFormVar="w_TIPCLF",RowSource=""+"Cliente,"+"Fornitore,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCLF_1_7.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPCLF_1_7.GetRadio()
    this.Parent.oContained.w_TIPCLF = this.RadioValue()
    return .t.
  endfunc

  func oTIPCLF_1_7.SetRadio()
    this.Parent.oContained.w_TIPCLF=trim(this.Parent.oContained.w_TIPCLF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCLF=='C',1,;
      iif(this.Parent.oContained.w_TIPCLF=='F',2,;
      iif(this.Parent.oContained.w_TIPCLF=='T',3,;
      0)))
  endfunc

  func oTIPCLF_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODINI)
        bRes2=.link_1_8('Full')
      endif
      if .not. empty(.w_CODFIN)
        bRes2=.link_1_9('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODINI_1_8 as StdField with uid="TPNOXTFDUA",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente o fornitore di inizio selezione (spazio=no selezione iniziale)",;
    HelpContextID = 20746714,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=112, Top=120, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODINI"

  func oCODINI_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLF<>'T')
    endwith
   endif
  endfunc

  func oCODINI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODINI_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti / fornitori",'',this.parent.oContained
  endproc

  add object oCODFIN_1_9 as StdField with uid="RQWNUFXJVT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente o fornitore di fine selezione (spazio=no selezione finale)",;
    HelpContextID = 57699878,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=112, Top=149, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFIN"

  func oCODFIN_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLF<>'T')
    endwith
   endif
  endfunc

  func oCODFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFIN_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti / fornitori",'',this.parent.oContained
  endproc

  add object oCODIVA_1_10 as StdField with uid="YRFZEUQXXF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODIVA", cQueryName = "CODIVA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA selezionato",;
    HelpContextID = 146575834,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=112, Top=178, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_CODIVA"

  func oCODIVA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIVA_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIVA_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCODIVA_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oCODIVA_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_CODIVA
     i_obj.ecpSave()
  endproc


  add object oTIPREG_1_11 as StdCombo with uid="ARGSWPJTSO",rtseq=11,rtrep=.f.,left=112,top=207,width=157,height=21;
    , ToolTipText = "Tipo registro IVA selezionato";
    , HelpContextID = 63100618;
    , cFormVar="w_TIPREG",RowSource=""+"Vendite,"+"Acquisti,"+"Corr.scorporo,"+"Corr.ventilazione,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPREG_1_11.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,'E',;
    iif(this.value =5,'T',;
    space(1)))))))
  endfunc
  func oTIPREG_1_11.GetRadio()
    this.Parent.oContained.w_TIPREG = this.RadioValue()
    return .t.
  endfunc

  func oTIPREG_1_11.SetRadio()
    this.Parent.oContained.w_TIPREG=trim(this.Parent.oContained.w_TIPREG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPREG=='V',1,;
      iif(this.Parent.oContained.w_TIPREG=='A',2,;
      iif(this.Parent.oContained.w_TIPREG=='C',3,;
      iif(this.Parent.oContained.w_TIPREG=='E',4,;
      iif(this.Parent.oContained.w_TIPREG=='T',5,;
      0)))))
  endfunc

  add object oNUMREG_1_12 as StdField with uid="DRHWZSEZDP",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registro IVA selezionato (0 = nessuna selezione)",;
    HelpContextID = 63109930,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=490, Top=207, cSayPict='"99"', cGetPict='"99"'

  func oNUMREG_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPREG<>'N')
    endwith
   endif
  endfunc


  add object oDEFI_1_14 as StdCombo with uid="NAELGZRXQU",value=3,rtseq=13,rtrep=.f.,left=112,top=233,width=128,height=21;
    , ToolTipText = "Tipo registrazioni selezionate";
    , HelpContextID = 253524938;
    , cFormVar="w_DEFI",RowSource=""+"Confermate,"+"Provvisorie,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDEFI_1_14.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(10)))))
  endfunc
  func oDEFI_1_14.GetRadio()
    this.Parent.oContained.w_DEFI = this.RadioValue()
    return .t.
  endfunc

  func oDEFI_1_14.SetRadio()
    this.Parent.oContained.w_DEFI=trim(this.Parent.oContained.w_DEFI)
    this.value = ;
      iif(this.Parent.oContained.w_DEFI=='N',1,;
      iif(this.Parent.oContained.w_DEFI=='S',2,;
      iif(this.Parent.oContained.w_DEFI=='',3,;
      0)))
  endfunc

  add object oDESIVA_1_21 as StdField with uid="UEJAXPZWXP",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 146516938,;
   bGlobalFont=.t.,;
    Height=21, Width=339, Left=181, Top=178, InputMask=replicate('X',35)

  add object oDESCRI1_1_26 as StdField with uid="SLXLJCZILR",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 251548726,;
   bGlobalFont=.t.,;
    Height=21, Width=274, Left=246, Top=121, InputMask=replicate('X',40)

  add object oDESCRI2_1_27 as StdField with uid="AZDHJXTVYP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCRI2", cQueryName = "DESCRI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 251548726,;
   bGlobalFont=.t.,;
    Height=21, Width=274, Left=246, Top=150, InputMask=replicate('X',40)


  add object oObj_1_31 as cp_outputCombo with uid="ZLERXIVPWT",left=112, top=261, width=406,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 187818470


  add object oBtn_1_32 as StdButton with uid="BJLYJVMVHQ",left=422, top=286, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 151610406;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY( .w_OREP ))
      endwith
    endif
  endfunc


  add object oBtn_1_33 as StdButton with uid="MYAOMVHQFA",left=472, top=286, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 251297210;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLBLLS_1_36 as StdCheck with uid="YSPVMQCVZZ",rtseq=20,rtrep=.f.,left=292, top=208, caption="Fiscalit� privilegiata",;
    ToolTipText = "Se attivato, filtra gli intestatari identificati come operatori economici con sede, residenza o domicilio negli Stati o territori a regime privilegiato",;
    HelpContextID = 145115990,;
    cFormVar="w_FLBLLS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLBLLS_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLBLLS_1_36.GetRadio()
    this.Parent.oContained.w_FLBLLS = this.RadioValue()
    return .t.
  endfunc

  func oFLBLLS_1_36.SetRadio()
    this.Parent.oContained.w_FLBLLS=trim(this.Parent.oContained.w_FLBLLS)
    this.value = ;
      iif(this.Parent.oContained.w_FLBLLS=='S',1,;
      0)
  endfunc

  add object oStr_1_13 as StdString with uid="GRDWYGJWHT",Visible=.t., Left=66, Top=90,;
    Alignment=1, Width=43, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="NAOBDZRIZI",Visible=.t., Left=19, Top=233,;
    Alignment=1, Width=90, Height=15,;
    Caption="Registrazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="WRMIESLCDW",Visible=.t., Left=433, Top=207,;
    Alignment=1, Width=55, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="JKCGCPZJDW",Visible=.t., Left=26, Top=207,;
    Alignment=1, Width=83, Height=15,;
    Caption="Tipo registro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="HODXRUVTMY",Visible=.t., Left=23, Top=178,;
    Alignment=1, Width=86, Height=18,;
    Caption="Codice I.V.A.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="WDNQZBBARJ",Visible=.t., Left=9, Top=35,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="DLNOYWOPBH",Visible=.t., Left=22, Top=61,;
    Alignment=1, Width=86, Height=18,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="YQJRSYTHOM",Visible=.t., Left=341, Top=35,;
    Alignment=1, Width=98, Height=18,;
    Caption="Da data comp.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="LHAXNXOLHD",Visible=.t., Left=349, Top=58,;
    Alignment=1, Width=90, Height=18,;
    Caption="A data comp.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="NZTZXWHAVR",Visible=.t., Left=76, Top=120,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="ZCHUGDBCPO",Visible=.t., Left=81, Top=149,;
    Alignment=1, Width=29, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="YBVGGYKCUF",Visible=.t., Left=14, Top=261,;
    Alignment=1, Width=95, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="XLYJAJWLJZ",Visible=.t., Left=22, Top=9,;
    Alignment=1, Width=86, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sid','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
