* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bwb                                                        *
*              Inizializzazione check wizard                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-04-20                                                      *
* Last revis.: 2016-04-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bwb",oParentObject,m.pParam)
return(i_retval)

define class tgsut_bwb as StdBatch
  * --- Local variables
  pParam = .f.
  w_CDINFURL = space(254)
  w_COPATHDO = space(254)
  w_CHECKSUM = space(10)
  w_CDCODCLA = space(15)
  w_CPROWNUM = 0
  w_IDSERIAL = space(15)
  w_UTCODICE = 0
  w_PATH = space(10)
  w_COUNTUSER = 0
  * --- WorkFile variables
  PARA_EDS_idx=0
  CONTROPA_idx=0
  PROMCLAS_idx=0
  PRODCLAS_idx=0
  PROMINDI_idx=0
  UTE_NTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo inizializzazione flag
    * --- Verifico se sono su un sistema a 64bit
    if Directory(GETENV("WINDIR")+ "\SysWOW64\")
      * --- Ricavo il path della cartella SYSWOW64
      this.w_PATH=ADDBS( ADDBS(GETENV("WINDIR"))+ "SysWOW64")
    else
      * --- Ricavo il path della cartella SYSTEM32
      this.w_PATH=ADDBS( ADDBS(GETENV("WINDIR"))+ "SYSTEM32")
    endif
    if this.pParam
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    * --- non sono state installate le dll
    if not cp_fileexist(this.w_PATH+"\SyncroDllClient.DLL") or not cp_fileexist(this.w_PATH+"\ZLIB1.DLL")
      this.oParentObject.w_INSTDLL = "S"
    else
      * --- Calcolo la checksum per sapere se la dll � aggiornata
      this.w_CHECKSUM = SYS(2007, FILETOSTR(this.w_PATH+"\SyncroDllClient.DLL"))
      if this.w_CHECKSUM<>"35172"
        this.oParentObject.w_INSTDLL = "S"
      else
        * --- Calcolo la checksum per sapere se la dll � aggiornata
        this.w_CHECKSUM = SYS(2007, FILETOSTR(this.w_PATH+"\ZLIB1.DLL"))
        if this.w_CHECKSUM<>"13301"
          this.oParentObject.w_INSTDLL = "S"
        endif
      endif
    endif
    * --- mancano dei parametri
    * --- Read from PARA_EDS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PARA_EDS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PARA_EDS_idx,2],.t.,this.PARA_EDS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CDINFURL,CDWIZSEC"+;
        " from "+i_cTable+" PARA_EDS where ";
            +"CDCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CDINFURL,CDWIZSEC;
        from (i_cTable) where;
            CDCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CDINFURL = NVL(cp_ToDate(_read_.CDINFURL),cp_NullValue(_read_.CDINFURL))
      this.oParentObject.w_CDWIZSEC = NVL(cp_ToDate(_read_.CDWIZSEC),cp_NullValue(_read_.CDWIZSEC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COPATHDO"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COPATHDO;
        from (i_cTable) where;
            COCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COPATHDO = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(NVL(this.w_CDINFURL,"")) or EMPTY(NVL(this.w_COPATHDO,""))
      this.oParentObject.w_CONFVAR = "S"
    endif
    * --- Select from PROMCLAS
    i_nConn=i_TableProp[this.PROMCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MAX(CDCODCLA) as CDCODCLA  from "+i_cTable+" PROMCLAS ";
          +" where CDMODALL<>'I'";
           ,"_Curs_PROMCLAS")
    else
      select MAX(CDCODCLA) as CDCODCLA from (i_cTable);
       where CDMODALL<>"I";
        into cursor _Curs_PROMCLAS
    endif
    if used('_Curs_PROMCLAS')
      select _Curs_PROMCLAS
      locate for 1=1
      do while not(eof())
      this.w_CDCODCLA = _Curs_PROMCLAS.CDCODCLA
        select _Curs_PROMCLAS
        continue
      enddo
      use
    endif
    * --- Ci sono delle classi da aggiornare
    if NOT EMPTY(NVL(this.w_CDCODCLA,""))
      this.oParentObject.w_VALIDATECLASS = "S"
    else
      * --- Select from PRODCLAS
      i_nConn=i_TableProp[this.PRODCLAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MAX(CDCODCLA) as CDCODCLA  from "+i_cTable+" PRODCLAS ";
            +" where CDATTINF is null or CDATTINF='               '";
             ,"_Curs_PRODCLAS")
      else
        select MAX(CDCODCLA) as CDCODCLA from (i_cTable);
         where CDATTINF is null or CDATTINF="               ";
          into cursor _Curs_PRODCLAS
      endif
      if used('_Curs_PRODCLAS')
        select _Curs_PRODCLAS
        locate for 1=1
        do while not(eof())
        this.w_CDCODCLA = _Curs_PRODCLAS.CDCODCLA
          select _Curs_PRODCLAS
          continue
        enddo
        use
      endif
      if NOT EMPTY(NVL(this.w_CDCODCLA,""))
        this.oParentObject.w_VALIDATECLASS = "S"
      endif
    endif
    * --- Mancano degli indici da inviare
    * --- Select from PROMINDI
    i_nConn=i_TableProp[this.PROMINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MAX(IDSERIAL) as IDSERIAL  from "+i_cTable+" PROMINDI ";
          +" where IDWEBAPP<>'T'";
           ,"_Curs_PROMINDI")
    else
      select MAX(IDSERIAL) as IDSERIAL from (i_cTable);
       where IDWEBAPP<>"T";
        into cursor _Curs_PROMINDI
    endif
    if used('_Curs_PROMINDI')
      select _Curs_PROMINDI
      locate for 1=1
      do while not(eof())
      this.w_IDSERIAL = _Curs_PROMINDI.IDSERIAL
        select _Curs_PROMINDI
        continue
      enddo
      use
    endif
    if NOT EMPTY(NVL(this.w_IDSERIAL,""))
      this.oParentObject.w_CONVERTIND = "S"
    endif
    * --- Bisogna ancora configurare degli utenti
    * --- Select from UTE_NTI
    i_nConn=i_TableProp[this.UTE_NTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2],.t.,this.UTE_NTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MAX(UTCODICE) as UTCODICE  from "+i_cTable+" UTE_NTI ";
          +" where UTSERVCP='S' AND UTIDUSER is null";
           ,"_Curs_UTE_NTI")
    else
      select MAX(UTCODICE) as UTCODICE from (i_cTable);
       where UTSERVCP="S" AND UTIDUSER is null;
        into cursor _Curs_UTE_NTI
    endif
    if used('_Curs_UTE_NTI')
      select _Curs_UTE_NTI
      locate for 1=1
      do while not(eof())
      this.w_UTCODICE = _Curs_UTE_NTI.UTCODICE
        select _Curs_UTE_NTI
        continue
      enddo
      use
    endif
    * --- Select from UTE_NTI
    i_nConn=i_TableProp[this.UTE_NTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UTE_NTI_idx,2],.t.,this.UTE_NTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select count(*) as COUNTUSER  from "+i_cTable+" UTE_NTI ";
          +" where UTSERVCP='S'";
           ,"_Curs_UTE_NTI")
    else
      select count(*) as COUNTUSER from (i_cTable);
       where UTSERVCP="S";
        into cursor _Curs_UTE_NTI
    endif
    if used('_Curs_UTE_NTI')
      select _Curs_UTE_NTI
      locate for 1=1
      do while not(eof())
      this.w_COUNTUSER = _Curs_UTE_NTI.COUNTUSER
        select _Curs_UTE_NTI
        continue
      enddo
      use
    endif
    if this.w_UTCODICE<>0 OR this.w_COUNTUSER=0
      this.oParentObject.w_CONFUSER = "S"
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riassegno codici attributi
    Select (this.oParentObject.w_ZOOMATTR.cCursor)
    SCAN FOR XCHK=1
    this.w_CDCODCLA = CDCODCLA
    this.w_CPROWNUM = CPROWNUM
    * --- Write into PRODCLAS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRODCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODCLAS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CDATTINF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDATTINF),'PRODCLAS','CDATTINF');
      +",CDATTOPE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.oParentObject.w_CDATTOPE),'PRODCLAS','CDATTOPE');
          +i_ccchkf ;
      +" where ";
          +"CDCODCLA = "+cp_ToStrODBC(this.w_CDCODCLA);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          CDATTINF = this.oParentObject.w_CDATTINF;
          ,CDATTOPE = this.oParentObject.oParentObject.w_CDATTOPE;
          &i_ccchkf. ;
       where;
          CDCODCLA = this.w_CDCODCLA;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ENDSCAN
    this.oParentObject.NotifyEvent("Requery")
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='PARA_EDS'
    this.cWorkTables[2]='CONTROPA'
    this.cWorkTables[3]='PROMCLAS'
    this.cWorkTables[4]='PRODCLAS'
    this.cWorkTables[5]='PROMINDI'
    this.cWorkTables[6]='UTE_NTI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_PROMCLAS')
      use in _Curs_PROMCLAS
    endif
    if used('_Curs_PRODCLAS')
      use in _Curs_PRODCLAS
    endif
    if used('_Curs_PROMINDI')
      use in _Curs_PROMINDI
    endif
    if used('_Curs_UTE_NTI')
      use in _Curs_UTE_NTI
    endif
    if used('_Curs_UTE_NTI')
      use in _Curs_UTE_NTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
