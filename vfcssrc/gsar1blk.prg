* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1blk                                                        *
*              Inserisce record in ASLISCLF                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-08                                                      *
* Last revis.: 2013-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1blk",oParentObject)
return(i_retval)

define class tgsar1blk as StdBatch
  * --- Local variables
  w_CODLIS = space(5)
  w_ASPRILIS = 0
  * --- WorkFile variables
  ASLISCLF_idx=0
  LISTINI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_CODLIS = this.oParentObject.w_NUMLIS
    * --- Read from LISTINI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.LISTINI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LSPRIORI"+;
        " from "+i_cTable+" LISTINI where ";
            +"LSCODLIS = "+cp_ToStrODBC(this.w_CODLIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LSPRIORI;
        from (i_cTable) where;
            LSCODLIS = this.w_CODLIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ASPRILIS = NVL(cp_ToDate(_read_.LSPRIORI),cp_NullValue(_read_.LSPRIORI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Insert into ASLISCLF
    i_nConn=i_TableProp[this.ASLISCLF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASLISCLF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ASLISCLF_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ASTIPCLF"+",ASCODCLF"+",ASCODLIS"+",CPROWNUM"+",ASPRILIS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("C"),'ASLISCLF','ASTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCL),'ASLISCLF','ASCODCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUMLIS),'ASLISCLF','ASCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(1),'ASLISCLF','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ASPRILIS),'ASLISCLF','ASPRILIS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ASTIPCLF',"C",'ASCODCLF',this.oParentObject.w_CODCL,'ASCODLIS',this.oParentObject.w_NUMLIS,'CPROWNUM',1,'ASPRILIS',this.w_ASPRILIS)
      insert into (i_cTable) (ASTIPCLF,ASCODCLF,ASCODLIS,CPROWNUM,ASPRILIS &i_ccchkf. );
         values (;
           "C";
           ,this.oParentObject.w_CODCL;
           ,this.oParentObject.w_NUMLIS;
           ,1;
           ,this.w_ASPRILIS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ASLISCLF'
    this.cWorkTables[2]='LISTINI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
