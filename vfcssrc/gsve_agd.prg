* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_agd                                                        *
*              Log generatore documentale                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-26                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_agd"))

* --- Class definition
define class tgsve_agd as StdForm
  Top    = 12
  Left   = 16

  * --- Standard Properties
  Width  = 772
  Height = 296+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=42611305
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  GENERDOC_IDX = 0
  cFile = "GENERDOC"
  cKeySelect = "GDSERIAL"
  cKeyWhere  = "GDSERIAL=this.w_GDSERIAL"
  cKeyWhereODBC = '"GDSERIAL="+cp_ToStrODBC(this.w_GDSERIAL)';

  cKeyWhereODBCqualified = '"GENERDOC.GDSERIAL="+cp_ToStrODBC(this.w_GDSERIAL)';

  cPrg = "gsve_agd"
  cComment = "Log generatore documentale"
  icon = "anag.ico"
  cAutoZoom = 'GSVE_AGD'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GDSERIAL = space(10)
  w_GDDATGEN = ctod('  /  /  ')
  w_GDPARAME = space(25)
  w_GDNOTE = space(0)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0
  w_GDTIPOEL = space(1)
  w_GDFLRSPE = space(1)
  w_GDFLRRIG = space(1)
  w_GDFLRTIP = space(1)
  w_GDFLRSDO = space(1)
  w_GDFLCHKD = space(1)
  w_GDFLSIMU = space(1)
  w_GDNOTE = space(0)

  * --- Children pointers
  GSVE_MLD = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GENERDOC','gsve_agd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_agdPag1","gsve_agd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati generali")
      .Pages(1).HelpContextID = 233372660
      .Pages(2).addobject("oPag","tgsve_agdPag2","gsve_agd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Documenti generati")
      .Pages(2).HelpContextID = 166889253
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGDSERIAL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='GENERDOC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GENERDOC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GENERDOC_IDX,3]
  return

  function CreateChildren()
    this.GSVE_MLD = CREATEOBJECT('stdDynamicChild',this,'GSVE_MLD',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSVE_MLD)
      this.GSVE_MLD.DestroyChildrenChain()
      this.GSVE_MLD=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSVE_MLD.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSVE_MLD.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSVE_MLD.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSVE_MLD.SetKey(;
            .w_GDSERIAL,"LDSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSVE_MLD.ChangeRow(this.cRowID+'      1',1;
             ,.w_GDSERIAL,"LDSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSVE_MLD)
        i_f=.GSVE_MLD.BuildFilter()
        if !(i_f==.GSVE_MLD.cQueryFilter)
          i_fnidx=.GSVE_MLD.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSVE_MLD.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSVE_MLD.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSVE_MLD.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSVE_MLD.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_GDSERIAL = NVL(GDSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GENERDOC where GDSERIAL=KeySet.GDSERIAL
    *
    i_nConn = i_TableProp[this.GENERDOC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENERDOC_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GENERDOC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GENERDOC.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GENERDOC '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GDSERIAL',this.w_GDSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_GDSERIAL = NVL(GDSERIAL,space(10))
        .w_GDDATGEN = NVL(cp_ToDate(GDDATGEN),ctod("  /  /  "))
        .w_GDPARAME = NVL(GDPARAME,space(25))
        .w_GDNOTE = NVL(GDNOTE,space(0))
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_GDTIPOEL = NVL(GDTIPOEL,space(1))
        .w_GDFLRSPE = NVL(GDFLRSPE,space(1))
        .w_GDFLRRIG = NVL(GDFLRRIG,space(1))
        .w_GDFLRTIP = NVL(GDFLRTIP,space(1))
        .w_GDFLRSDO = NVL(GDFLRSDO,space(1))
        .w_GDFLCHKD = NVL(GDFLCHKD,space(1))
        .w_GDFLSIMU = NVL(GDFLSIMU,space(1))
        .w_GDNOTE = NVL(GDNOTE,space(0))
        cp_LoadRecExtFlds(this,'GENERDOC')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GDSERIAL = space(10)
      .w_GDDATGEN = ctod("  /  /  ")
      .w_GDPARAME = space(25)
      .w_GDNOTE = space(0)
      .w_UTCC = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_UTCV = 0
      .w_GDTIPOEL = space(1)
      .w_GDFLRSPE = space(1)
      .w_GDFLRRIG = space(1)
      .w_GDFLRTIP = space(1)
      .w_GDFLRSDO = space(1)
      .w_GDFLCHKD = space(1)
      .w_GDFLSIMU = space(1)
      .w_GDNOTE = space(0)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'GENERDOC')
    this.DoRTCalc(1,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGDSERIAL_1_2.enabled = i_bVal
      .Page1.oPag.oGDNOTE_1_8.enabled = i_bVal
      .Page1.oPag.oGDNOTE_1_28.enabled = i_bVal
      .Page1.oPag.oBtn_1_9.enabled = .Page1.oPag.oBtn_1_9.mCond()
      if i_cOp = "Edit"
        .Page1.oPag.oGDSERIAL_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oGDSERIAL_1_2.enabled = .t.
      endif
    endwith
    this.GSVE_MLD.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'GENERDOC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSVE_MLD.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GENERDOC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDSERIAL,"GDSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDDATGEN,"GDDATGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDPARAME,"GDPARAME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDNOTE,"GDNOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDTIPOEL,"GDTIPOEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDFLRSPE,"GDFLRSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDFLRRIG,"GDFLRRIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDFLRTIP,"GDFLRTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDFLRSDO,"GDFLRSDO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDFLCHKD,"GDFLCHKD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDFLSIMU,"GDFLSIMU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDNOTE,"GDNOTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GENERDOC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENERDOC_IDX,2])
    i_lTable = "GENERDOC"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GENERDOC_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSVE_SGD with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GENERDOC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENERDOC_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GENERDOC_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GENERDOC
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GENERDOC')
        i_extval=cp_InsertValODBCExtFlds(this,'GENERDOC')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(GDSERIAL,GDDATGEN,GDPARAME,GDNOTE,UTCC"+;
                  ",UTDC,UTDV,UTCV,GDTIPOEL,GDFLRSPE"+;
                  ",GDFLRRIG,GDFLRTIP,GDFLRSDO,GDFLCHKD,GDFLSIMU "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_GDSERIAL)+;
                  ","+cp_ToStrODBC(this.w_GDDATGEN)+;
                  ","+cp_ToStrODBC(this.w_GDPARAME)+;
                  ","+cp_ToStrODBC(this.w_GDNOTE)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_GDTIPOEL)+;
                  ","+cp_ToStrODBC(this.w_GDFLRSPE)+;
                  ","+cp_ToStrODBC(this.w_GDFLRRIG)+;
                  ","+cp_ToStrODBC(this.w_GDFLRTIP)+;
                  ","+cp_ToStrODBC(this.w_GDFLRSDO)+;
                  ","+cp_ToStrODBC(this.w_GDFLCHKD)+;
                  ","+cp_ToStrODBC(this.w_GDFLSIMU)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GENERDOC')
        i_extval=cp_InsertValVFPExtFlds(this,'GENERDOC')
        cp_CheckDeletedKey(i_cTable,0,'GDSERIAL',this.w_GDSERIAL)
        INSERT INTO (i_cTable);
              (GDSERIAL,GDDATGEN,GDPARAME,GDNOTE,UTCC,UTDC,UTDV,UTCV,GDTIPOEL,GDFLRSPE,GDFLRRIG,GDFLRTIP,GDFLRSDO,GDFLCHKD,GDFLSIMU  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_GDSERIAL;
                  ,this.w_GDDATGEN;
                  ,this.w_GDPARAME;
                  ,this.w_GDNOTE;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
                  ,this.w_GDTIPOEL;
                  ,this.w_GDFLRSPE;
                  ,this.w_GDFLRRIG;
                  ,this.w_GDFLRTIP;
                  ,this.w_GDFLRSDO;
                  ,this.w_GDFLCHKD;
                  ,this.w_GDFLSIMU;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GENERDOC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENERDOC_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GENERDOC_IDX,i_nConn)
      *
      * update GENERDOC
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GENERDOC')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GDDATGEN="+cp_ToStrODBC(this.w_GDDATGEN)+;
             ",GDPARAME="+cp_ToStrODBC(this.w_GDPARAME)+;
             ",GDNOTE="+cp_ToStrODBC(this.w_GDNOTE)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",GDTIPOEL="+cp_ToStrODBC(this.w_GDTIPOEL)+;
             ",GDFLRSPE="+cp_ToStrODBC(this.w_GDFLRSPE)+;
             ",GDFLRRIG="+cp_ToStrODBC(this.w_GDFLRRIG)+;
             ",GDFLRTIP="+cp_ToStrODBC(this.w_GDFLRTIP)+;
             ",GDFLRSDO="+cp_ToStrODBC(this.w_GDFLRSDO)+;
             ",GDFLCHKD="+cp_ToStrODBC(this.w_GDFLCHKD)+;
             ",GDFLSIMU="+cp_ToStrODBC(this.w_GDFLSIMU)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GENERDOC')
        i_cWhere = cp_PKFox(i_cTable  ,'GDSERIAL',this.w_GDSERIAL  )
        UPDATE (i_cTable) SET;
              GDDATGEN=this.w_GDDATGEN;
             ,GDPARAME=this.w_GDPARAME;
             ,GDNOTE=this.w_GDNOTE;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,GDTIPOEL=this.w_GDTIPOEL;
             ,GDFLRSPE=this.w_GDFLRSPE;
             ,GDFLRRIG=this.w_GDFLRRIG;
             ,GDFLRTIP=this.w_GDFLRTIP;
             ,GDFLRSDO=this.w_GDFLRSDO;
             ,GDFLCHKD=this.w_GDFLCHKD;
             ,GDFLSIMU=this.w_GDFLSIMU;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSVE_MLD : Saving
      this.GSVE_MLD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_GDSERIAL,"LDSERIAL";
             )
      this.GSVE_MLD.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSVE_MLD : Deleting
    this.GSVE_MLD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_GDSERIAL,"LDSERIAL";
           )
    this.GSVE_MLD.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GENERDOC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENERDOC_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GENERDOC_IDX,i_nConn)
      *
      * delete GENERDOC
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'GDSERIAL',this.w_GDSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GENERDOC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENERDOC_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_NVSAVRVVPQ()
    with this
          * --- Cancella Generazione GSVE_BCG
          gsve_bcg(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oGDNOTE_1_8.visible=!this.oPgFrm.Page1.oPag.oGDNOTE_1_8.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_9.visible=!this.oPgFrm.Page1.oPag.oBtn_1_9.mHide()
    this.oPgFrm.Page1.oPag.oGDFLRSPE_1_16.visible=!this.oPgFrm.Page1.oPag.oGDFLRSPE_1_16.mHide()
    this.oPgFrm.Page1.oPag.oGDFLRRIG_1_17.visible=!this.oPgFrm.Page1.oPag.oGDFLRRIG_1_17.mHide()
    this.oPgFrm.Page1.oPag.oGDFLRTIP_1_18.visible=!this.oPgFrm.Page1.oPag.oGDFLRTIP_1_18.mHide()
    this.oPgFrm.Page1.oPag.oGDFLRSDO_1_19.visible=!this.oPgFrm.Page1.oPag.oGDFLRSDO_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oGDFLCHKD_1_23.visible=!this.oPgFrm.Page1.oPag.oGDFLCHKD_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page2.oPag.oLinkPC_2_1.visible=!this.oPgFrm.Page2.oPag.oLinkPC_2_1.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oGDFLSIMU_1_27.visible=!this.oPgFrm.Page1.oPag.oGDFLSIMU_1_27.mHide()
    this.oPgFrm.Page1.oPag.oGDNOTE_1_28.visible=!this.oPgFrm.Page1.oPag.oGDNOTE_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Delete start")
          .Calculate_NVSAVRVVPQ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGDSERIAL_1_2.value==this.w_GDSERIAL)
      this.oPgFrm.Page1.oPag.oGDSERIAL_1_2.value=this.w_GDSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oGDDATGEN_1_4.value==this.w_GDDATGEN)
      this.oPgFrm.Page1.oPag.oGDDATGEN_1_4.value=this.w_GDDATGEN
    endif
    if not(this.oPgFrm.Page1.oPag.oGDPARAME_1_6.value==this.w_GDPARAME)
      this.oPgFrm.Page1.oPag.oGDPARAME_1_6.value=this.w_GDPARAME
    endif
    if not(this.oPgFrm.Page1.oPag.oGDNOTE_1_8.value==this.w_GDNOTE)
      this.oPgFrm.Page1.oPag.oGDNOTE_1_8.value=this.w_GDNOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oGDTIPOEL_1_14.RadioValue()==this.w_GDTIPOEL)
      this.oPgFrm.Page1.oPag.oGDTIPOEL_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGDFLRSPE_1_16.RadioValue()==this.w_GDFLRSPE)
      this.oPgFrm.Page1.oPag.oGDFLRSPE_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGDFLRRIG_1_17.RadioValue()==this.w_GDFLRRIG)
      this.oPgFrm.Page1.oPag.oGDFLRRIG_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGDFLRTIP_1_18.RadioValue()==this.w_GDFLRTIP)
      this.oPgFrm.Page1.oPag.oGDFLRTIP_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGDFLRSDO_1_19.RadioValue()==this.w_GDFLRSDO)
      this.oPgFrm.Page1.oPag.oGDFLRSDO_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGDFLCHKD_1_23.RadioValue()==this.w_GDFLCHKD)
      this.oPgFrm.Page1.oPag.oGDFLCHKD_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGDFLSIMU_1_27.RadioValue()==this.w_GDFLSIMU)
      this.oPgFrm.Page1.oPag.oGDFLSIMU_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGDNOTE_1_28.value==this.w_GDNOTE)
      this.oPgFrm.Page1.oPag.oGDNOTE_1_28.value=this.w_GDNOTE
    endif
    cp_SetControlsValueExtFlds(this,'GENERDOC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSVE_MLD.CheckForm()
      if i_bres
        i_bres=  .GSVE_MLD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSVE_MLD : Depends On
    this.GSVE_MLD.SaveDependsOn()
    return

  func CanAdd()
    local i_res
    i_res=.F.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Caricamento non consentito"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsve_agdPag1 as StdContainer
  Width  = 770
  height = 300
  stdWidth  = 770
  stdheight = 300
  resizeXpos=695
  resizeYpos=205
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGDSERIAL_1_2 as StdField with uid="POJJVDUKLJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_GDSERIAL", cQueryName = "GDSERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 199247538,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=105, Left=132, Top=15, InputMask=replicate('X',10)

  add object oGDDATGEN_1_4 as StdField with uid="EROPVUUBAX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GDDATGEN", cQueryName = "GDDATGEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 167466676,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=132, Top=51

  add object oGDPARAME_1_6 as StdField with uid="OMCXDIQTEQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GDPARAME", cQueryName = "GDPARAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    HelpContextID = 203680085,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=368, Top=16, InputMask=replicate('X',25)

  add object oGDNOTE_1_8 as StdMemo with uid="FCCWQZTVZO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_GDNOTE", cQueryName = "GDNOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 133564826,;
   bGlobalFont=.t.,;
    Height=83, Width=676, Left=24, Top=195

  func oGDNOTE_1_8.mHide()
    with this.Parent.oContained
      return (!g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc


  add object oBtn_1_9 as StdButton with uid="DBKXRVJYQY",left=704, top=234, width=48,height=45,;
    CpPicture="bmp\log.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il log dell'elaborazione";
    , HelpContextID = 42298954;
    , caption='\<Log elab.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      do GSVE_KLE with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_GDSERIAL) OR  !g_APPLICATION = "ad hoc ENTERPRISE")
     endwith
    endif
  endfunc


  add object oGDTIPOEL_1_14 as StdCombo with uid="XCDWXQDONM",rtseq=9,rtrep=.f.,left=368,top=51,width=188,height=21, enabled=.f.;
    , HelpContextID = 29644466;
    , cFormVar="w_GDTIPOEL",RowSource=""+"Generazione da documenti,"+"Creazione documenti,"+"Nessuna", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGDTIPOEL_1_14.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'N',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func oGDTIPOEL_1_14.GetRadio()
    this.Parent.oContained.w_GDTIPOEL = this.RadioValue()
    return .t.
  endfunc

  func oGDTIPOEL_1_14.SetRadio()
    this.Parent.oContained.w_GDTIPOEL=trim(this.Parent.oContained.w_GDTIPOEL)
    this.value = ;
      iif(this.Parent.oContained.w_GDTIPOEL=='E',1,;
      iif(this.Parent.oContained.w_GDTIPOEL=='N',2,;
      iif(this.Parent.oContained.w_GDTIPOEL=='X',3,;
      0)))
  endfunc

  add object oGDFLRSPE_1_16 as StdCheck with uid="CEHSIFHXGB",rtseq=10,rtrep=.f.,left=24, top=111, caption="Dati di spedizione", enabled=.f.,;
    ToolTipText = "Rottura per dati di spedizione",;
    HelpContextID = 98989739,;
    cFormVar="w_GDFLRSPE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGDFLRSPE_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGDFLRSPE_1_16.GetRadio()
    this.Parent.oContained.w_GDFLRSPE = this.RadioValue()
    return .t.
  endfunc

  func oGDFLRSPE_1_16.SetRadio()
    this.Parent.oContained.w_GDFLRSPE=trim(this.Parent.oContained.w_GDFLRSPE)
    this.value = ;
      iif(this.Parent.oContained.w_GDFLRSPE=='S',1,;
      0)
  endfunc

  func oGDFLRSPE_1_16.mHide()
    with this.Parent.oContained
      return (!g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oGDFLRRIG_1_17 as StdCheck with uid="NEYWBGUIWH",rtseq=11,rtrep=.f.,left=24, top=135, caption="Raggruppamenti di riga", enabled=.f.,;
    ToolTipText = "Rottura per flag raggruppamenti di riga",;
    HelpContextID = 186222931,;
    cFormVar="w_GDFLRRIG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGDFLRRIG_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGDFLRRIG_1_17.GetRadio()
    this.Parent.oContained.w_GDFLRRIG = this.RadioValue()
    return .t.
  endfunc

  func oGDFLRRIG_1_17.SetRadio()
    this.Parent.oContained.w_GDFLRRIG=trim(this.Parent.oContained.w_GDFLRRIG)
    this.value = ;
      iif(this.Parent.oContained.w_GDFLRRIG=='S',1,;
      0)
  endfunc

  func oGDFLRRIG_1_17.mHide()
    with this.Parent.oContained
      return (!g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oGDFLRTIP_1_18 as StdCheck with uid="FBKHDTDZXY",rtseq=12,rtrep=.f.,left=204, top=111, caption="Classe documento", enabled=.f.,;
    ToolTipText = "Rottura per classe documento",;
    HelpContextID = 152668490,;
    cFormVar="w_GDFLRTIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGDFLRTIP_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGDFLRTIP_1_18.GetRadio()
    this.Parent.oContained.w_GDFLRTIP = this.RadioValue()
    return .t.
  endfunc

  func oGDFLRTIP_1_18.SetRadio()
    this.Parent.oContained.w_GDFLRTIP=trim(this.Parent.oContained.w_GDFLRTIP)
    this.value = ;
      iif(this.Parent.oContained.w_GDFLRTIP=='S',1,;
      0)
  endfunc

  func oGDFLRTIP_1_18.mHide()
    with this.Parent.oContained
      return (!g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oGDFLRSDO_1_19 as StdCheck with uid="LIOJBVOJNR",rtseq=13,rtrep=.f.,left=204, top=135, caption="Singolo documento", enabled=.f.,;
    ToolTipText = "Rottura per singolo documento",;
    HelpContextID = 98989749,;
    cFormVar="w_GDFLRSDO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGDFLRSDO_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGDFLRSDO_1_19.GetRadio()
    this.Parent.oContained.w_GDFLRSDO = this.RadioValue()
    return .t.
  endfunc

  func oGDFLRSDO_1_19.SetRadio()
    this.Parent.oContained.w_GDFLRSDO=trim(this.Parent.oContained.w_GDFLRSDO)
    this.value = ;
      iif(this.Parent.oContained.w_GDFLRSDO=='S',1,;
      0)
  endfunc

  func oGDFLRSDO_1_19.mHide()
    with this.Parent.oContained
      return (!g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc


  add object oGDFLCHKD_1_23 as StdCombo with uid="SFMFQZEGEY",rtseq=14,rtrep=.f.,left=441,top=113,width=253,height=21, enabled=.f.;
    , ToolTipText = "Check disponibilitÓ";
    , HelpContextID = 101288278;
    , cFormVar="w_GDFLCHKD",RowSource=""+"Nessun controllo,"+"Controlla tutti gli articoli,"+"Controlla secondo impostazioni articolo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGDFLCHKD_1_23.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oGDFLCHKD_1_23.GetRadio()
    this.Parent.oContained.w_GDFLCHKD = this.RadioValue()
    return .t.
  endfunc

  func oGDFLCHKD_1_23.SetRadio()
    this.Parent.oContained.w_GDFLCHKD=trim(this.Parent.oContained.w_GDFLCHKD)
    this.value = ;
      iif(this.Parent.oContained.w_GDFLCHKD=='N',1,;
      iif(this.Parent.oContained.w_GDFLCHKD=='S',2,;
      iif(this.Parent.oContained.w_GDFLCHKD=='A',3,;
      0)))
  endfunc

  func oGDFLCHKD_1_23.mHide()
    with this.Parent.oContained
      return (!g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc


  add object oGDFLSIMU_1_27 as StdCombo with uid="JTVBGOGLGS",rtseq=15,rtrep=.f.,left=441,top=140,width=253,height=21, enabled=.f.;
    , ToolTipText = "Elaborazione normale o simulata";
    , HelpContextID = 67733829;
    , cFormVar="w_GDFLSIMU",RowSource=""+"Normale,"+"Simulazione con log a video,"+"Simulazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGDFLSIMU_1_27.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'V',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oGDFLSIMU_1_27.GetRadio()
    this.Parent.oContained.w_GDFLSIMU = this.RadioValue()
    return .t.
  endfunc

  func oGDFLSIMU_1_27.SetRadio()
    this.Parent.oContained.w_GDFLSIMU=trim(this.Parent.oContained.w_GDFLSIMU)
    this.value = ;
      iif(this.Parent.oContained.w_GDFLSIMU=='N',1,;
      iif(this.Parent.oContained.w_GDFLSIMU=='V',2,;
      iif(this.Parent.oContained.w_GDFLSIMU=='S',3,;
      0)))
  endfunc

  func oGDFLSIMU_1_27.mHide()
    with this.Parent.oContained
      return (!g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oGDNOTE_1_28 as StdMemo with uid="KATKCQCJTE",rtseq=16,rtrep=.f.,;
    cFormVar = "w_GDNOTE", cQueryName = "GDNOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 133564826,;
   bGlobalFont=.t.,;
    Height=165, Width=676, Left=25, Top=113

  func oGDNOTE_1_28.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_1 as StdString with uid="NHZCYTKRQK",Visible=.t., Left=9, Top=15,;
    Alignment=1, Width=117, Height=18,;
    Caption="Seriale generazione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="MFOPDQDNUQ",Visible=.t., Left=84, Top=51,;
    Alignment=1, Width=42, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QQGZVIUOOH",Visible=.t., Left=239, Top=15,;
    Alignment=1, Width=127, Height=18,;
    Caption="Parametro di lancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="YGHQVTMNXJ",Visible=.t., Left=251, Top=51,;
    Alignment=1, Width=115, Height=18,;
    Caption="Tipo elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="OKNTEDCVBM",Visible=.t., Left=24, Top=93,;
    Alignment=0, Width=146, Height=18,;
    Caption="Rotture / raggruppamenti"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (!g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="FOZAWABWPQ",Visible=.t., Left=24, Top=171,;
    Alignment=0, Width=47, Height=18,;
    Caption="Note"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (!g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="PHQAQCRNVK",Visible=.t., Left=352, Top=93,;
    Alignment=0, Width=142, Height=18,;
    Caption="Elaborazioni"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (!g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_25 as StdString with uid="RZZJBAXEXB",Visible=.t., Left=351, Top=115,;
    Alignment=1, Width=85, Height=18,;
    Caption="DisponibilitÓ:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (!g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="WPHPZSPRZG",Visible=.t., Left=351, Top=142,;
    Alignment=1, Width=85, Height=18,;
    Caption="ModalitÓ:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (!g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="OPGPAGUYHL",Visible=.t., Left=24, Top=93,;
    Alignment=0, Width=47, Height=18,;
    Caption="Note"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oBox_1_20 as StdBox with uid="BZYWFLGZYO",left=24, top=107, width=680,height=2
enddefine
define class tgsve_agdPag2 as StdContainer
  Width  = 770
  height = 300
  stdWidth  = 770
  stdheight = 300
  resizeXpos=528
  resizeYpos=133
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="FCFTHZQDCL",left=0, top=13, width=770, height=287, bOnScreen=.t.;
    , tabstop=.f., caption='\<Dettaglio'

  func oLinkPC_2_1.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_GDSERIAL))
     endwith
    endif
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsve_mld",lower(this.oContained.GSVE_MLD.class))=0
        this.oContained.GSVE_MLD.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_agd','GENERDOC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GDSERIAL=GENERDOC.GDSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
