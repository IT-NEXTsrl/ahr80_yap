* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kgs                                                        *
*              Gadget library                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-02-20                                                      *
* Last revis.: 2016-03-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kgs",oParentObject))

* --- Class definition
define class tgsut_kgs as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 178
  Height = 217
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-22"
  HelpContextID=32064361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  MOD_GADG_IDX = 0
  GRP_GADG_IDX = 0
  cPrg = "gsut_kgs"
  cComment = "Gadget library"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_NAME = space(50)
  w_MODEL = space(50)
  w_AREA = space(5)
  w_UTEESC = space(1)
  w_TIPNEG = space(1)
  w_RET = .F.
  w_PREVPAGE = space(5)
  w_STOREGRID = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_kgs
  *--- La dichiaro form di sistema per non essere utilizzata nella sua stessa Drop Zone
    bSysForm = .T.
  
  Proc AddGadget(pCodTheme, pXCoord, pYCoord)
    Local nAddType,bNewGroup
    With this
      If .w_StoreGrid.aSelected.Count>0
        *--- Calcolo il tipo di inserimento:
        *--- nAddType = 0   inserimento singolo gadget o inserimento da pagina diversa dalla Home
        *--- nAddType = 1   inserimento multiplo a partire dalla Home di gadget tutti nella Home
        *--- nAddType = 2   inserimento multiplo a partire dalla Home di gadget smistati nei relativi gruppi
        If .w_StoreGrid.aSelected.Count==1 Or This.oParentObject.nActivePage<>1
          m.nAddType = 0
        Else
          m.bNewGroup = .f.
          *--- Controllo per ogni codice gruppo se � gia presente la relativa pagina sull'interfaccia
          For Each oItem in .w_StoreGrid.aSelected
            If !Empty(Nvl(oItem.cGadGroup, ''))
              m.bNewGroup = m.bNewGroup Or Empty(This.oParentObject.oTabPage.GetNameByKey(oItem.cGadGroup))
            Endif
          EndFor
          If m.bNewGroup And !ah_YesNo("Sono stati selezionati gadget assegnati ad aree non presenti.%0Aggiungere i gadget all'area Home?%0(S�=Area Home, No=Relative aree)", 32)
            m.nAddType = 2
          Else
            m.nAddType = 1
          Endif
        Endif
        
        m.cCodTheme = Iif(Vartype(pCodTheme)<>'C',This.oParentObject.oCharmBar.w_MGDTHEME, pCodTheme)
        
        *-- Inserimento
        For Each oItem in .w_StoreGrid.aSelected
          GSUT_BGG(This.oParentObject, "AddFromLibrary", oItem.cKeyGadget, .f., m.cCodTheme, m.nAddType, m.pXCoord, m.pYCoord)
        EndFor 
        *--- Aggiorno Elenco
        .NotifyEvent("Filtra")
      EndIf
    EndWith
  EndProc
  
  Proc RemoveGadget()
     With This
       If .w_StoreGrid.aSelected.Count>0 And Ah_YesNo(MSG_CONFIRM_DELETING_QP)     
          *-- Cancellazione
          For Each oItem in .w_StoreGrid.aSelected
            GSUT_BGG(This.oParentObject, "RemoveFromLibrary", oItem.cKeyGadget)
          EndFor 
          *--- Aggiorno Elenco
          .NotifyEvent("Filtra")
       EndIf
     Endwith
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kgsPag1","gsut_kgs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNAME_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_STOREGRID = this.oPgFrm.Pages(1).oPag.STOREGRID
    DoDefault()
    proc Destroy()
      this.w_STOREGRID = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='MOD_GADG'
    this.cWorkTables[2]='GRP_GADG'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_kgs
    *--- Controllo che la dimensione non sia troppo piccola
    *--- in tal caso mostro la griglia 2x4
    If This.Width<320 And This.Height<395
       This.Move(This.Left, This.Top, 570, 395)
    Endif
    
    
    This.w_StoreGrid.cExecOnEnter = [This.Parent.oContained.AddGadget()]
    This.w_StoreGrid.cExecOnCanc = [Iif(Alltrim(This.Parent.oContained.w_UTEESC)<>'S', '', This.Parent.oContained.RemoveGadget())]
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NAME=space(50)
      .w_MODEL=space(50)
      .w_AREA=space(5)
      .w_UTEESC=space(1)
      .w_TIPNEG=space(1)
      .w_RET=.f.
      .w_PREVPAGE=space(5)
          .DoRTCalc(1,1,.f.)
        .w_MODEL = ''
        .w_AREA = ''
        .w_UTEESC = 'N'
        .w_TIPNEG = 'V'
      .oPgFrm.Page1.oPag.STOREGRID.Calculate()
    endwith
    this.DoRTCalc(6,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.STOREGRID.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.STOREGRID.Calculate()
    endwith
  return

  proc Calculate_LHEDUQHNEJ()
    with this
          * --- Aggiorna area gadget
      if !(This.oParentObject.aPage.GetKey(This.oParentObject.nActivePage)=='HOME') Or !(This.oParentObject.aPage.GetKey(This.oParentObject.nActivePage)==.w_PREVPAGE)
          .w_AREA = This.oParentObject.aPage.GetKey(This.oParentObject.nActivePage)
      endif
          .w_PREVPAGE = This.oParentObject.aPage.GetKey(This.oParentObject.nActivePage)
          .w_AREA = IIF(.w_AREA=="HOME", "", .w_AREA)
          .w_RET = this.mEnableControls()
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAREA_1_4.enabled = this.oPgFrm.Page1.oPag.oAREA_1_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.visible=!this.oPgFrm.Page1.oPag.oBtn_1_8.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Filtra") or lower(cEvent)==lower("Init")
          .Calculate_LHEDUQHNEJ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.STOREGRID.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNAME_1_1.value==this.w_NAME)
      this.oPgFrm.Page1.oPag.oNAME_1_1.value=this.w_NAME
    endif
    if not(this.oPgFrm.Page1.oPag.oMODEL_1_3.RadioValue()==this.w_MODEL)
      this.oPgFrm.Page1.oPag.oMODEL_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAREA_1_4.RadioValue()==this.w_AREA)
      this.oPgFrm.Page1.oPag.oAREA_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTEESC_1_5.RadioValue()==this.w_UTEESC)
      this.oPgFrm.Page1.oPag.oUTEESC_1_5.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kgsPag1 as StdContainer
  Width  = 174
  height = 217
  stdWidth  = 174
  stdheight = 217
  resizeXpos=80
  resizeYpos=180
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNAME_1_1 as AH_SEARCHFLD with uid="OIZKKQEBNX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NAME", cQueryName = "NAME",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 27209002,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=55, Top=7, InputMask=replicate('X',50)


  add object oBtn_1_2 as StdButton with uid="YUKMZAPQQP",left=132, top=7, width=32,height=33,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , HelpContextID = 213858086;
  , bGlobalFont=.t.

    proc oBtn_1_2.Click()
      this.parent.oContained.NotifyEvent("Filtra")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oMODEL_1_3 as StdTableCombo with uid="EROLRQCUGN",rtseq=2,rtrep=.f.,left=55,top=32,width=73,height=20;
    , cDescEmptyElement = "Tutti";
    , HelpContextID = 215985978;
    , cFormVar="w_MODEL",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='MOD_GADG',cKey='MGSRCPRG',cValue='MGDESCRI',cOrderBy='MGDESCRI',xDefault=space(50);
  , bGlobalFont=.t.



  add object oAREA_1_4 as StdTableNoItemCombo with uid="CMMZMEUDAR",rtseq=3,rtrep=.f.,left=55,top=55,width=73,height=20;
    , cDescEmptyElement = "Tutte";
    , ToolTipText = "Filtra i gadget in base all'area di appartenenza";
    , HelpContextID = 27499770;
    , cFormVar="w_AREA",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='GRP_GADG',cKey='GGCODICE',cValue='GGDESCRI',cOrderBy='GGDESCRI',xDefault=space(5);
  , bGlobalFont=.t.


  func oAREA_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.oParentObject.aPage.GetKey(.oParentObject.nActivePage)=='HOME')
    endwith
   endif
  endfunc

  add object oUTEESC_1_5 as StdCheck with uid="TCOTYMPCPA",rtseq=4,rtrep=.f.,left=55, top=76, caption="Solo esclusivi",;
    ToolTipText = "Filtra solo i gadget esclusivi per l'utente loggato",;
    HelpContextID = 158308794,;
    cFormVar="w_UTEESC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUTEESC_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oUTEESC_1_5.GetRadio()
    this.Parent.oContained.w_UTEESC = this.RadioValue()
    return .t.
  endfunc

  func oUTEESC_1_5.SetRadio()
    this.Parent.oContained.w_UTEESC=trim(this.Parent.oContained.w_UTEESC)
    this.value = ;
      iif(this.Parent.oContained.w_UTEESC=='S',1,;
      0)
  endfunc

  proc oUTEESC_1_5.mAfter
    with this.Parent.oContained
      This.Parent.oContained.NotifyEvent('Filtra')
    endwith
  endproc


  add object oBtn_1_7 as StdButton with uid="UTFIRKQPGX",left=132, top=107, width=32,height=33,;
    CpPicture="BMP\PLUS.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiungere i gadget selezionati";
    , HelpContextID = 31628026;
  , bGlobalFont=.t.

    proc oBtn_1_7.Click()
      with this.Parent.oContained
        Thisform.AddGadget()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="RXLLNDLCCO",left=132, top=140, width=32,height=33,;
    CpPicture="BMP\CANC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per cancellare i gadget esclusivi selezionati";
    , HelpContextID = 183301910;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      with this.Parent.oContained
        Thisform.RemoveGadget()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_UTEESC<>'S')
     endwith
    endif
  endfunc


  add object STOREGRID as LibraryGrid with uid="MYWAQZMRWW",left=7, top=107, width=121,height=102,;
    caption='GadgetLibrary',;
   bGlobalFont=.t.,;
    cQuery="QUERY\GSUT_KGS.VQR",HotTracking=2,bDropZone=.t.,;
    cEvent = "Init,Filtra,w_MODEL Changed,w_AREA Changed",;
    nPag=1;
    , HelpContextID = 253957393

  add object oStr_1_12 as StdString with uid="ETBPXUFUYA",Visible=.t., Left=14, Top=11,;
    Alignment=1, Width=40, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="SUNIBVBDER",Visible=.t., Left=4, Top=34,;
    Alignment=1, Width=50, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="WRWCDXYBHX",Visible=.t., Left=4, Top=57,;
    Alignment=1, Width=50, Height=18,;
    Caption="Area:"  ;
  , bGlobalFont=.t.

  add object oBox_1_14 as StdBox with uid="CKGVMPRDEY",left=6, top=99, width=160,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kgs','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kgs
Define Class StdTableNoItemCombo As StdTableCombo
   Proc Init
     DoDefault()
     With This
       .nValues = This.nValues + 1
       Dimension .ComboValues(.nValues)
       .ComboValues[.nValues] = '#####'
       .AddItem('Nessuna')
     Endwith
   Endproc
Enddefine
* --- Fine Area Manuale
