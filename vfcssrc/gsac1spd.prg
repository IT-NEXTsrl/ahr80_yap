* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac1spd                                                        *
*              Stampa PDA                                                      *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-11                                                      *
* Last revis.: 2014-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsac1spd",oParentObject))

* --- Class definition
define class tgsac1spd as StdForm
  Top    = -1
  Left   = 1

  * --- Standard Properties
  Width  = 836
  Height = 424
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-03-26"
  HelpContextID=144205673
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=52

  * --- Constant Properties
  _IDX = 0
  MAGAZZIN_IDX = 0
  ART_ICOL_IDX = 0
  PDA_DETT_IDX = 0
  KEY_ARTI_IDX = 0
  CONTI_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  CATEGOMO_IDX = 0
  GRUMERC_IDX = 0
  FAM_ARTI_IDX = 0
  MARCHI_IDX = 0
  cPrg = "gsac1spd"
  cComment = "Stampa PDA"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PDAINI = space(15)
  o_PDAINI = space(15)
  w_ROWNUMIN = 0
  w_PDAFIN = space(15)
  w_ROWNUMFI = 0
  w_DATINI = ctod('  /  /  ')
  w_DATINI1 = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DATFIN1 = ctod('  /  /  ')
  w_ORDSTATO = space(10)
  o_ORDSTATO = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_TIPATT = space(1)
  w_TIPCON = space(1)
  w_DATOBSO1 = ctod('  /  /  ')
  w_FAMPRO = space(5)
  w_PDCODINI = space(20)
  o_PDCODINI = space(20)
  w_PDCODFIN = space(20)
  o_PDCODFIN = space(20)
  w_PDFAMAIN = space(5)
  w_PDFAMAFI = space(5)
  w_PDGRUINI = space(5)
  w_PDGRUFIN = space(5)
  w_PDCATINI = space(5)
  w_PDCATFIN = space(5)
  w_PDMARINI = space(5)
  w_PDMARFIN = space(5)
  w_PDMAGINI = space(5)
  w_DESMAGI = space(30)
  w_PDMAGFIN = space(5)
  w_DESMAGF = space(30)
  w_DESFAMAI = space(35)
  w_DESGRUI = space(35)
  w_DESCATI = space(35)
  w_DESFAMAF = space(35)
  w_DESGRUF = space(35)
  w_DESCATF = space(35)
  w_DEMARINI = space(35)
  w_DEMARFIN = space(35)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_DATOBS1 = ctod('  /  /  ')
  w_ARCODINI = space(20)
  w_ARCODFIN = space(20)
  w_PDCODCOM = space(15)
  w_PDCODATT = space(15)
  w_PDCODCON = space(15)
  w_DESCOM = space(30)
  w_DESATT = space(30)
  w_DESFOR = space(40)
  w_ARPROPREI = space(1)
  w_ARPROPREF = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsac1spdPag1","gsac1spd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPDAINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='PDA_DETT'
    this.cWorkTables[4]='KEY_ARTI'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='CAN_TIER'
    this.cWorkTables[7]='ATTIVITA'
    this.cWorkTables[8]='CATEGOMO'
    this.cWorkTables[9]='GRUMERC'
    this.cWorkTables[10]='FAM_ARTI'
    this.cWorkTables[11]='MARCHI'
    return(this.OpenAllTables(11))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PDAINI=space(15)
      .w_ROWNUMIN=0
      .w_PDAFIN=space(15)
      .w_ROWNUMFI=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATINI1=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DATFIN1=ctod("  /  /  ")
      .w_ORDSTATO=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPATT=space(1)
      .w_TIPCON=space(1)
      .w_DATOBSO1=ctod("  /  /  ")
      .w_FAMPRO=space(5)
      .w_PDCODINI=space(20)
      .w_PDCODFIN=space(20)
      .w_PDFAMAIN=space(5)
      .w_PDFAMAFI=space(5)
      .w_PDGRUINI=space(5)
      .w_PDGRUFIN=space(5)
      .w_PDCATINI=space(5)
      .w_PDCATFIN=space(5)
      .w_PDMARINI=space(5)
      .w_PDMARFIN=space(5)
      .w_PDMAGINI=space(5)
      .w_DESMAGI=space(30)
      .w_PDMAGFIN=space(5)
      .w_DESMAGF=space(30)
      .w_DESFAMAI=space(35)
      .w_DESGRUI=space(35)
      .w_DESCATI=space(35)
      .w_DESFAMAF=space(35)
      .w_DESGRUF=space(35)
      .w_DESCATF=space(35)
      .w_DEMARINI=space(35)
      .w_DEMARFIN=space(35)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DATOBS1=ctod("  /  /  ")
      .w_ARCODINI=space(20)
      .w_ARCODFIN=space(20)
      .w_PDCODCOM=space(15)
      .w_PDCODATT=space(15)
      .w_PDCODCON=space(15)
      .w_DESCOM=space(30)
      .w_DESATT=space(30)
      .w_DESFOR=space(40)
      .w_ARPROPREI=space(1)
      .w_ARPROPREF=space(1)
        .w_PDAINI = ' '
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PDAINI))
          .link_1_1('Full')
        endif
        .w_ROWNUMIN = 1
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ROWNUMIN))
          .link_1_2('Full')
        endif
        .w_PDAFIN = .w_PDAINI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PDAFIN))
          .link_1_3('Full')
        endif
        .w_ROWNUMFI = 999999
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ROWNUMFI))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,8,.f.)
        .w_ORDSTATO = 'T'
        .w_OBTEST = i_INIDAT
          .DoRTCalc(11,11,.f.)
        .w_OBTEST = i_DATSYS
        .w_TIPATT = 'A'
        .w_TIPCON = 'F'
          .DoRTCalc(15,15,.f.)
        .w_FAMPRO = ' '
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_PDCODINI))
          .link_1_28('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_PDCODFIN))
          .link_1_29('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_PDFAMAIN))
          .link_1_31('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_PDFAMAFI))
          .link_1_32('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_PDGRUINI))
          .link_1_33('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_PDGRUFIN))
          .link_1_34('Full')
        endif
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_PDCATINI))
          .link_1_35('Full')
        endif
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_PDCATFIN))
          .link_1_36('Full')
        endif
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_PDMARINI))
          .link_1_37('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_PDMARFIN))
          .link_1_38('Full')
        endif
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_PDMAGINI))
          .link_1_41('Full')
        endif
        .DoRTCalc(28,29,.f.)
        if not(empty(.w_PDMAGFIN))
          .link_1_44('Full')
        endif
          .DoRTCalc(30,42,.f.)
        .w_ARCODINI = .w_ARCODINI
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_ARCODINI))
          .link_1_66('Full')
        endif
        .w_ARCODFIN = .w_ARCODFIN
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_ARCODFIN))
          .link_1_67('Full')
        endif
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_PDCODCOM))
          .link_1_69('Full')
        endif
        .DoRTCalc(46,46,.f.)
        if not(empty(.w_PDCODATT))
          .link_1_70('Full')
        endif
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_PDCODCON))
          .link_1_71('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
    endwith
    this.DoRTCalc(48,52,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_73.enabled = this.oPgFrm.Page1.oPag.oBtn_1_73.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_74.enabled = this.oPgFrm.Page1.oPag.oBtn_1_74.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_ORDSTATO<>.w_ORDSTATO
            .w_PDAINI = ' '
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.t.)
        if .o_PDAINI<>.w_PDAINI
            .w_PDAFIN = .w_PDAINI
          .link_1_3('Full')
        endif
        .DoRTCalc(4,9,.t.)
            .w_OBTEST = i_INIDAT
        .DoRTCalc(11,42,.t.)
        if .o_PDCODINI<>.w_PDCODINI
            .w_ARCODINI = .w_ARCODINI
          .link_1_66('Full')
        endif
        if .o_PDCODFIN<>.w_PDCODFIN
            .w_ARCODFIN = .w_ARCODFIN
          .link_1_67('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(45,52,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPDCODCOM_1_69.enabled = this.oPgFrm.Page1.oPag.oPDCODCOM_1_69.mCond()
    this.oPgFrm.Page1.oPag.oPDCODATT_1_70.enabled = this.oPgFrm.Page1.oPag.oPDCODATT_1_70.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_73.enabled = this.oPgFrm.Page1.oPag.oBtn_1_73.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDESFOR_1_82.visible=!this.oPgFrm.Page1.oPag.oDESFOR_1_82.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_72.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PDAINI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PDA_DETT_IDX,3]
    i_lTable = "PDA_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2], .t., this.PDA_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PDA_DETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PDSERIAL like "+cp_ToStrODBC(trim(this.w_PDAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PDSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PDSERIAL',trim(this.w_PDAINI))
          select PDSERIAL,PDROWNUM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PDSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDAINI)==trim(_Link_.PDSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDAINI) and !this.bDontReportError
            deferred_cp_zoom('PDA_DETT','*','PDSERIAL',cp_AbsName(oSource.parent,'oPDAINI_1_1'),i_cWhere,'',"Codice PDA",'GSDB_SPO.PDA_DETT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                     +" from "+i_cTable+" "+i_lTable+" where PDSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDSERIAL',oSource.xKey(1))
            select PDSERIAL,PDROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                   +" from "+i_cTable+" "+i_lTable+" where PDSERIAL="+cp_ToStrODBC(this.w_PDAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDSERIAL',this.w_PDAINI)
            select PDSERIAL,PDROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDAINI = NVL(_Link_.PDSERIAL,space(15))
      this.w_ROWNUMIN = NVL(_Link_.PDROWNUM,0)
    else
      if i_cCtrl<>'Load'
        this.w_PDAINI = space(15)
      endif
      this.w_ROWNUMIN = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDAINI<=.w_PDAFIN or empty(.w_PDAFIN) 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDAINI = space(15)
        this.w_ROWNUMIN = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])+'\'+cp_ToStr(_Link_.PDSERIAL,1)
      cp_ShowWarn(i_cKey,this.PDA_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROWNUMIN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PDA_DETT_IDX,3]
    i_lTable = "PDA_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2], .t., this.PDA_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROWNUMIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PDA_DETT')
        if i_nConn<>0
          i_cWhere = " PDROWNUM="+cp_ToStrODBC(this.w_ROWNUMIN);
                   +" and PDSERIAL="+cp_ToStrODBC(this.w_PDAINI);

          i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PDSERIAL',this.w_PDAINI;
                     ,'PDROWNUM',this.w_ROWNUMIN)
          select PDSERIAL,PDROWNUM;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_ROWNUMIN) and !this.bDontReportError
            deferred_cp_zoom('PDA_DETT','*','PDSERIAL,PDROWNUM',cp_AbsName(oSource.parent,'oROWNUMIN_1_2'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PDAINI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select PDSERIAL,PDROWNUM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                     +" from "+i_cTable+" "+i_lTable+" where PDROWNUM="+cp_ToStrODBC(oSource.xKey(2));
                     +" and PDSERIAL="+cp_ToStrODBC(this.w_PDAINI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDSERIAL',oSource.xKey(1);
                       ,'PDROWNUM',oSource.xKey(2))
            select PDSERIAL,PDROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROWNUMIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                   +" from "+i_cTable+" "+i_lTable+" where PDROWNUM="+cp_ToStrODBC(this.w_ROWNUMIN);
                   +" and PDSERIAL="+cp_ToStrODBC(this.w_PDAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDSERIAL',this.w_PDAINI;
                       ,'PDROWNUM',this.w_ROWNUMIN)
            select PDSERIAL,PDROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROWNUMIN = NVL(_Link_.PDROWNUM,0)
    else
      if i_cCtrl<>'Load'
        this.w_ROWNUMIN = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ROWNUMIN<=.w_ROWNUMFI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
        endif
        this.w_ROWNUMIN = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])+'\'+cp_ToStr(_Link_.PDSERIAL,1)+'\'+cp_ToStr(_Link_.PDROWNUM,1)
      cp_ShowWarn(i_cKey,this.PDA_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROWNUMIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDAFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PDA_DETT_IDX,3]
    i_lTable = "PDA_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2], .t., this.PDA_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PDA_DETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PDSERIAL like "+cp_ToStrODBC(trim(this.w_PDAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PDSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PDSERIAL',trim(this.w_PDAFIN))
          select PDSERIAL,PDROWNUM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PDSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDAFIN)==trim(_Link_.PDSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDAFIN) and !this.bDontReportError
            deferred_cp_zoom('PDA_DETT','*','PDSERIAL',cp_AbsName(oSource.parent,'oPDAFIN_1_3'),i_cWhere,'',"Codice PDA",'GSDB_SPO.PDA_DETT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                     +" from "+i_cTable+" "+i_lTable+" where PDSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDSERIAL',oSource.xKey(1))
            select PDSERIAL,PDROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                   +" from "+i_cTable+" "+i_lTable+" where PDSERIAL="+cp_ToStrODBC(this.w_PDAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDSERIAL',this.w_PDAFIN)
            select PDSERIAL,PDROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDAFIN = NVL(_Link_.PDSERIAL,space(15))
      this.w_ROWNUMFI = NVL(_Link_.PDROWNUM,0)
    else
      if i_cCtrl<>'Load'
        this.w_PDAFIN = space(15)
      endif
      this.w_ROWNUMFI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDAINI<=.w_PDAFIN or empty(.w_PDAFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("PDA iniziale maggiore del PDA finale")
        endif
        this.w_PDAFIN = space(15)
        this.w_ROWNUMFI = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])+'\'+cp_ToStr(_Link_.PDSERIAL,1)
      cp_ShowWarn(i_cKey,this.PDA_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ROWNUMFI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PDA_DETT_IDX,3]
    i_lTable = "PDA_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2], .t., this.PDA_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROWNUMFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'PDA_DETT')
        if i_nConn<>0
          i_cWhere = " PDROWNUM="+cp_ToStrODBC(this.w_ROWNUMFI);
                   +" and PDSERIAL="+cp_ToStrODBC(this.w_PDAFIN);

          i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PDSERIAL',this.w_PDAFIN;
                     ,'PDROWNUM',this.w_ROWNUMFI)
          select PDSERIAL,PDROWNUM;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_ROWNUMFI) and !this.bDontReportError
            deferred_cp_zoom('PDA_DETT','*','PDSERIAL,PDROWNUM',cp_AbsName(oSource.parent,'oROWNUMFI_1_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PDAFIN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select PDSERIAL,PDROWNUM;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                     +" from "+i_cTable+" "+i_lTable+" where PDROWNUM="+cp_ToStrODBC(oSource.xKey(2));
                     +" and PDSERIAL="+cp_ToStrODBC(this.w_PDAFIN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDSERIAL',oSource.xKey(1);
                       ,'PDROWNUM',oSource.xKey(2))
            select PDSERIAL,PDROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROWNUMFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDSERIAL,PDROWNUM";
                   +" from "+i_cTable+" "+i_lTable+" where PDROWNUM="+cp_ToStrODBC(this.w_ROWNUMFI);
                   +" and PDSERIAL="+cp_ToStrODBC(this.w_PDAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDSERIAL',this.w_PDAFIN;
                       ,'PDROWNUM',this.w_ROWNUMFI)
            select PDSERIAL,PDROWNUM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROWNUMFI = NVL(_Link_.PDROWNUM,0)
    else
      if i_cCtrl<>'Load'
        this.w_ROWNUMFI = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ROWNUMIN<=.w_ROWNUMFI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
        endif
        this.w_ROWNUMFI = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PDA_DETT_IDX,2])+'\'+cp_ToStr(_Link_.PDSERIAL,1)+'\'+cp_ToStr(_Link_.PDROWNUM,1)
      cp_ShowWarn(i_cKey,this.PDA_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROWNUMFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCODINI
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PDCODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PDCODINI))
          select CACODICE,CADESART,CADTOBSO,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_PDCODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_PDCODINI)+"%");

            select CACODICE,CADESART,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PDCODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPDCODINI_1_28'),i_cWhere,'',"Articoli",'gsve_zpd.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PDCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PDCODINI)
            select CACODICE,CADESART,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_ARCODINI = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_ARCODINI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PDCODFIN)) OR  (.w_PDCODINI <= .w_PDCODFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'articolo iniziale � maggiore di quello finale, obsoleto oppure non di provenienza esterna")
        endif
        this.w_PDCODINI = space(20)
        this.w_DESINI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_ARCODINI = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCODFIN
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PDCODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PDCODFIN))
          select CACODICE,CADESART,CADTOBSO,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_PDCODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_PDCODFIN)+"%");

            select CACODICE,CADESART,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PDCODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPDCODFIN_1_29'),i_cWhere,'',"Articoli",'gsve_zpd.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CADTOBSO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PDCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PDCODFIN)
            select CACODICE,CADESART,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
      this.w_DATOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_ARCODFIN = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DATOBS1 = ctod("  /  /  ")
      this.w_ARCODFIN = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PDCODINI <= .w_PDCODFIN) or (empty(.w_PDCODFIN))) and (EMPTY(.w_DATOBS1) OR .w_DATOBS1>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'articolo iniziale � maggiore di quello finale, obsoleto oppure non di provenienza esterna")
        endif
        this.w_PDCODFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DATOBS1 = ctod("  /  /  ")
        this.w_ARCODFIN = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDFAMAIN
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDFAMAIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_PDFAMAIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_PDFAMAIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDFAMAIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDFAMAIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oPDFAMAIN_1_31'),i_cWhere,'',"FAMIGLIE ARTICOLI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDFAMAIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_PDFAMAIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_PDFAMAIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDFAMAIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAI = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDFAMAIN = space(5)
      endif
      this.w_DESFAMAI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDFAMAIN <= .w_PDFAMAFI OR EMPTY(.w_PDFAMAFI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDFAMAIN = space(5)
        this.w_DESFAMAI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDFAMAIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDFAMAFI
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDFAMAFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_PDFAMAFI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_PDFAMAFI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDFAMAFI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDFAMAFI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oPDFAMAFI_1_32'),i_cWhere,'',"FAMIGLIE ARTICOLI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDFAMAFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_PDFAMAFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_PDFAMAFI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDFAMAFI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAMAF = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDFAMAFI = space(5)
      endif
      this.w_DESFAMAF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDFAMAIN <= .w_PDFAMAFI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDFAMAFI = space(5)
        this.w_DESFAMAF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDFAMAFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDGRUINI
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDGRUINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_PDGRUINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_PDGRUINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDGRUINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDGRUINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oPDGRUINI_1_33'),i_cWhere,'',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDGRUINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_PDGRUINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_PDGRUINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDGRUINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUI = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDGRUINI = space(5)
      endif
      this.w_DESGRUI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDGRUINI <= .w_PDGRUFIN OR EMPTY(.w_PDGRUFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDGRUINI = space(5)
        this.w_DESGRUI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDGRUINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDGRUFIN
  func Link_1_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDGRUFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_PDGRUFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_PDGRUFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDGRUFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDGRUFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oPDGRUFIN_1_34'),i_cWhere,'',"GRUPPI MERCEOLOGICI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDGRUFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_PDGRUFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_PDGRUFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDGRUFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRUF = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDGRUFIN = space(5)
      endif
      this.w_DESGRUF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDGRUINI <= .w_PDGRUFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDGRUFIN = space(5)
        this.w_DESGRUF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDGRUFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCATINI
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_PDCATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_PDCATINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCATINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCATINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oPDCATINI_1_35'),i_cWhere,'',"CATEGORIE OMOGENEE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_PDCATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_PDCATINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCATINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATI = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDCATINI = space(5)
      endif
      this.w_DESCATI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDCATINI <= .w_PDCATFIN OR EMPTY(.w_PDCATFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDCATINI = space(5)
        this.w_DESCATI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCATFIN
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_PDCATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_PDCATFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCATFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCATFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oPDCATFIN_1_36'),i_cWhere,'',"CATEGORIE OMOGENEE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_PDCATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_PDCATFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCATFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATF = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDCATFIN = space(5)
      endif
      this.w_DESCATF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDCATINI <= .w_PDCATFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDCATFIN = space(5)
        this.w_DESCATF = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDMARINI
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDMARINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_PDMARINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_PDMARINI))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDMARINI)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDMARINI) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oPDMARINI_1_37'),i_cWhere,'',"MARCHI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDMARINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_PDMARINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_PDMARINI)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDMARINI = NVL(_Link_.MACODICE,space(5))
      this.w_DEMARINI = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDMARINI = space(5)
      endif
      this.w_DEMARINI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDMARINI <= .w_PDMARFIN OR EMPTY(.w_PDMARFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDMARINI = space(5)
        this.w_DEMARINI = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDMARINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDMARFIN
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDMARFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_PDMARFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_PDMARFIN))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDMARFIN)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDMARFIN) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oPDMARFIN_1_38'),i_cWhere,'',"MARCHI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDMARFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_PDMARFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_PDMARFIN)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDMARFIN = NVL(_Link_.MACODICE,space(5))
      this.w_DEMARFIN = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PDMARFIN = space(5)
      endif
      this.w_DEMARFIN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDMARINI <= .w_PDMARFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDMARFIN = space(5)
        this.w_DEMARFIN = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDMARFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDMAGINI
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDMAGINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PDMAGINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PDMAGINI))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDMAGINI)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDMAGINI) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPDMAGINI_1_41'),i_cWhere,'',"MAGAZZINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDMAGINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PDMAGINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PDMAGINI)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDMAGINI = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGI = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PDMAGINI = space(5)
      endif
      this.w_DESMAGI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDMAGINI <= .w_PDMAGFIN OR EMPTY(.w_PDMAGFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDMAGINI = space(5)
        this.w_DESMAGI = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDMAGINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDMAGFIN
  func Link_1_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDMAGFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PDMAGFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PDMAGFIN))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDMAGFIN)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDMAGFIN) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPDMAGFIN_1_44'),i_cWhere,'',"MAGAZZINI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDMAGFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PDMAGFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PDMAGFIN)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDMAGFIN = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAGF = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PDMAGFIN = space(5)
      endif
      this.w_DESMAGF = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PDMAGINI <= .w_PDMAGFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PDMAGFIN = space(5)
        this.w_DESMAGF = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDMAGFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCODINI
  func Link_1_66(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARPROPRE";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARCODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARCODINI)
            select ARCODART,ARPROPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODINI = NVL(_Link_.ARCODART,space(20))
      this.w_ARPROPREI = NVL(_Link_.ARPROPRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODINI = space(20)
      endif
      this.w_ARPROPREI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCODFIN
  func Link_1_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARPROPRE";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARCODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARCODFIN)
            select ARCODART,ARPROPRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODFIN = NVL(_Link_.ARCODART,space(20))
      this.w_ARPROPREF = NVL(_Link_.ARPROPRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODFIN = space(20)
      endif
      this.w_ARPROPREF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCODCOM
  func Link_1_69(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_PDCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_PDCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oPDCODCOM_1_69'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_PDCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_PDCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCODATT
  func Link_1_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_PDCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_PDCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_PDCODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_PDCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oPDCODATT_1_70'),i_cWhere,'',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PDCODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_PDCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_PDCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_PDCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_PDCODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_PDCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDCODCON
  func Link_1_71(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PDCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PDCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PDCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PDCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PDCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPDCODCON_1_71'),i_cWhere,'',"Fornitori",'GSDB_KCS2.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un fornitore")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PDCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PDCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO1 = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PDCODCON = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Empty(.w_DATOBSO1) OR .w_OBTEST<.w_DATOBSO1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un fornitore")
        endif
        this.w_PDCODCON = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO1 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPDAINI_1_1.value==this.w_PDAINI)
      this.oPgFrm.Page1.oPag.oPDAINI_1_1.value=this.w_PDAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oROWNUMIN_1_2.value==this.w_ROWNUMIN)
      this.oPgFrm.Page1.oPag.oROWNUMIN_1_2.value=this.w_ROWNUMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDAFIN_1_3.value==this.w_PDAFIN)
      this.oPgFrm.Page1.oPag.oPDAFIN_1_3.value=this.w_PDAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oROWNUMFI_1_4.value==this.w_ROWNUMFI)
      this.oPgFrm.Page1.oPag.oROWNUMFI_1_4.value=this.w_ROWNUMFI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_5.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_5.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI1_1_6.value==this.w_DATINI1)
      this.oPgFrm.Page1.oPag.oDATINI1_1_6.value=this.w_DATINI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_7.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_7.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN1_1_8.value==this.w_DATFIN1)
      this.oPgFrm.Page1.oPag.oDATFIN1_1_8.value=this.w_DATFIN1
    endif
    if not(this.oPgFrm.Page1.oPag.oORDSTATO_1_9.RadioValue()==this.w_ORDSTATO)
      this.oPgFrm.Page1.oPag.oORDSTATO_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODINI_1_28.value==this.w_PDCODINI)
      this.oPgFrm.Page1.oPag.oPDCODINI_1_28.value=this.w_PDCODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODFIN_1_29.value==this.w_PDCODFIN)
      this.oPgFrm.Page1.oPag.oPDCODFIN_1_29.value=this.w_PDCODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDFAMAIN_1_31.value==this.w_PDFAMAIN)
      this.oPgFrm.Page1.oPag.oPDFAMAIN_1_31.value=this.w_PDFAMAIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDFAMAFI_1_32.value==this.w_PDFAMAFI)
      this.oPgFrm.Page1.oPag.oPDFAMAFI_1_32.value=this.w_PDFAMAFI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDGRUINI_1_33.value==this.w_PDGRUINI)
      this.oPgFrm.Page1.oPag.oPDGRUINI_1_33.value=this.w_PDGRUINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDGRUFIN_1_34.value==this.w_PDGRUFIN)
      this.oPgFrm.Page1.oPag.oPDGRUFIN_1_34.value=this.w_PDGRUFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCATINI_1_35.value==this.w_PDCATINI)
      this.oPgFrm.Page1.oPag.oPDCATINI_1_35.value=this.w_PDCATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCATFIN_1_36.value==this.w_PDCATFIN)
      this.oPgFrm.Page1.oPag.oPDCATFIN_1_36.value=this.w_PDCATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMARINI_1_37.value==this.w_PDMARINI)
      this.oPgFrm.Page1.oPag.oPDMARINI_1_37.value=this.w_PDMARINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMARFIN_1_38.value==this.w_PDMARFIN)
      this.oPgFrm.Page1.oPag.oPDMARFIN_1_38.value=this.w_PDMARFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMAGINI_1_41.value==this.w_PDMAGINI)
      this.oPgFrm.Page1.oPag.oPDMAGINI_1_41.value=this.w_PDMAGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGI_1_42.value==this.w_DESMAGI)
      this.oPgFrm.Page1.oPag.oDESMAGI_1_42.value=this.w_DESMAGI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDMAGFIN_1_44.value==this.w_PDMAGFIN)
      this.oPgFrm.Page1.oPag.oPDMAGFIN_1_44.value=this.w_PDMAGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAGF_1_45.value==this.w_DESMAGF)
      this.oPgFrm.Page1.oPag.oDESMAGF_1_45.value=this.w_DESMAGF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAI_1_46.value==this.w_DESFAMAI)
      this.oPgFrm.Page1.oPag.oDESFAMAI_1_46.value=this.w_DESFAMAI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUI_1_47.value==this.w_DESGRUI)
      this.oPgFrm.Page1.oPag.oDESGRUI_1_47.value=this.w_DESGRUI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATI_1_48.value==this.w_DESCATI)
      this.oPgFrm.Page1.oPag.oDESCATI_1_48.value=this.w_DESCATI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAMAF_1_52.value==this.w_DESFAMAF)
      this.oPgFrm.Page1.oPag.oDESFAMAF_1_52.value=this.w_DESFAMAF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRUF_1_53.value==this.w_DESGRUF)
      this.oPgFrm.Page1.oPag.oDESGRUF_1_53.value=this.w_DESGRUF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATF_1_54.value==this.w_DESCATF)
      this.oPgFrm.Page1.oPag.oDESCATF_1_54.value=this.w_DESCATF
    endif
    if not(this.oPgFrm.Page1.oPag.oDEMARINI_1_58.value==this.w_DEMARINI)
      this.oPgFrm.Page1.oPag.oDEMARINI_1_58.value=this.w_DEMARINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDEMARFIN_1_60.value==this.w_DEMARFIN)
      this.oPgFrm.Page1.oPag.oDEMARFIN_1_60.value=this.w_DEMARFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_62.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_62.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_63.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_63.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODCOM_1_69.value==this.w_PDCODCOM)
      this.oPgFrm.Page1.oPag.oPDCODCOM_1_69.value=this.w_PDCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODATT_1_70.value==this.w_PDCODATT)
      this.oPgFrm.Page1.oPag.oPDCODATT_1_70.value=this.w_PDCODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oPDCODCON_1_71.value==this.w_PDCODCON)
      this.oPgFrm.Page1.oPag.oPDCODCON_1_71.value=this.w_PDCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_78.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_78.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_80.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_80.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_82.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_82.value=this.w_DESFOR
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_PDAINI<=.w_PDAFIN or empty(.w_PDAFIN) )  and not(empty(.w_PDAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDAINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ROWNUMIN<=.w_ROWNUMFI)  and not(empty(.w_ROWNUMIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oROWNUMIN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not(.w_PDAINI<=.w_PDAFIN or empty(.w_PDAFIN))  and not(empty(.w_PDAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDAFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("PDA iniziale maggiore del PDA finale")
          case   not(.w_ROWNUMIN<=.w_ROWNUMFI)  and not(empty(.w_ROWNUMFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oROWNUMFI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATINI<=.w_DATINI1 or empty(.w_DATINI1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI1_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATFIN<=.w_DATFIN1 or empty(.w_DATFIN1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DATFIN<=.w_DATFIN1 or empty(.w_DATFIN1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN1_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_PDCODFIN)) OR  (.w_PDCODINI <= .w_PDCODFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) )  and not(empty(.w_PDCODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCODINI_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'articolo iniziale � maggiore di quello finale, obsoleto oppure non di provenienza esterna")
          case   not(((.w_PDCODINI <= .w_PDCODFIN) or (empty(.w_PDCODFIN))) and (EMPTY(.w_DATOBS1) OR .w_DATOBS1>.w_OBTEST))  and not(empty(.w_PDCODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCODFIN_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'articolo iniziale � maggiore di quello finale, obsoleto oppure non di provenienza esterna")
          case   not(.w_PDFAMAIN <= .w_PDFAMAFI OR EMPTY(.w_PDFAMAFI))  and not(empty(.w_PDFAMAIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDFAMAIN_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDFAMAIN <= .w_PDFAMAFI)  and not(empty(.w_PDFAMAFI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDFAMAFI_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDGRUINI <= .w_PDGRUFIN OR EMPTY(.w_PDGRUFIN))  and not(empty(.w_PDGRUINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDGRUINI_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDGRUINI <= .w_PDGRUFIN)  and not(empty(.w_PDGRUFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDGRUFIN_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDCATINI <= .w_PDCATFIN OR EMPTY(.w_PDCATFIN))  and not(empty(.w_PDCATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCATINI_1_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDCATINI <= .w_PDCATFIN)  and not(empty(.w_PDCATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCATFIN_1_36.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDMARINI <= .w_PDMARFIN OR EMPTY(.w_PDMARFIN))  and not(empty(.w_PDMARINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDMARINI_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDMARINI <= .w_PDMARFIN)  and not(empty(.w_PDMARFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDMARFIN_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDMAGINI <= .w_PDMAGFIN OR EMPTY(.w_PDMAGFIN))  and not(empty(.w_PDMAGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDMAGINI_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PDMAGINI <= .w_PDMAGFIN)  and not(empty(.w_PDMAGFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDMAGFIN_1_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(Empty(.w_DATOBSO1) OR .w_OBTEST<.w_DATOBSO1)  and not(empty(.w_PDCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDCODCON_1_71.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un fornitore")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PDAINI = this.w_PDAINI
    this.o_ORDSTATO = this.w_ORDSTATO
    this.o_PDCODINI = this.w_PDCODINI
    this.o_PDCODFIN = this.w_PDCODFIN
    return

enddefine

* --- Define pages as container
define class tgsac1spdPag1 as StdContainer
  Width  = 832
  height = 424
  stdWidth  = 832
  stdheight = 424
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPDAINI_1_1 as StdField with uid="NNEQWSJYCC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PDAINI", cQueryName = "PDAINI",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Numero proposta di acquisto di inizio selezione",;
    HelpContextID = 93647350,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=89, Top=22, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PDA_DETT", oKey_1_1="PDSERIAL", oKey_1_2="this.w_PDAINI"

  func oPDAINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
      if .not. empty(.w_ROWNUMIN)
        bRes2=.link_1_2('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPDAINI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDAINI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PDA_DETT','*','PDSERIAL',cp_AbsName(this.parent,'oPDAINI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codice PDA",'GSDB_SPO.PDA_DETT_VZM',this.parent.oContained
  endproc

  add object oROWNUMIN_1_2 as StdField with uid="VUQLZUMVLA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ROWNUMIN", cQueryName = "ROWNUMIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento iniziale selezionato",;
    HelpContextID = 99918492,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=186, Top=22, cSayPict='"999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="PDA_DETT", oKey_1_1="PDSERIAL", oKey_1_2="this.w_PDAINI", oKey_2_1="PDROWNUM", oKey_2_2="this.w_ROWNUMIN"

  func oROWNUMIN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oROWNUMIN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROWNUMIN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.PDA_DETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"PDSERIAL="+cp_ToStrODBC(this.Parent.oContained.w_PDAINI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"PDSERIAL="+cp_ToStr(this.Parent.oContained.w_PDAINI)
    endif
    do cp_zoom with 'PDA_DETT','*','PDSERIAL,PDROWNUM',cp_AbsName(this.parent,'oROWNUMIN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oPDAFIN_1_3 as StdField with uid="EPNAGQLILF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PDAFIN", cQueryName = "PDAFIN",nZero=15,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "PDA iniziale maggiore del PDA finale",;
    ToolTipText = "Numero proposta di acquisto di fine selezione",;
    HelpContextID = 172093942,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=89, Top=45, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PDA_DETT", oKey_1_1="PDSERIAL", oKey_1_2="this.w_PDAFIN"

  func oPDAFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
      if .not. empty(.w_ROWNUMFI)
        bRes2=.link_1_4('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPDAFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDAFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PDA_DETT','*','PDSERIAL',cp_AbsName(this.parent,'oPDAFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codice PDA",'GSDB_SPO.PDA_DETT_VZM',this.parent.oContained
  endproc

  add object oROWNUMFI_1_4 as StdField with uid="DTJLRFYAFU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ROWNUMFI", cQueryName = "ROWNUMFI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento finale selezionato",;
    HelpContextID = 168516959,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=186, Top=45, cSayPict='"999999"', cGetPict='"999999"', bHasZoom = .t. , cLinkFile="PDA_DETT", oKey_1_1="PDSERIAL", oKey_1_2="this.w_PDAFIN", oKey_2_1="PDROWNUM", oKey_2_2="this.w_ROWNUMFI"

  func oROWNUMFI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oROWNUMFI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oROWNUMFI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.PDA_DETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"PDSERIAL="+cp_ToStrODBC(this.Parent.oContained.w_PDAFIN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"PDSERIAL="+cp_ToStr(this.Parent.oContained.w_PDAFIN)
    endif
    do cp_zoom with 'PDA_DETT','*','PDSERIAL,PDROWNUM',cp_AbsName(this.parent,'oROWNUMFI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDATINI_1_5 as StdField with uid="KADSLBXBRA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'intervallo della data di inizio lavorazione",;
    HelpContextID = 93724214,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=358, Top=22

  func oDATINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATINI1_1_6 as StdField with uid="ODRUJEKWCF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATINI1", cQueryName = "DATINI1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'intervallo della data di inizio lavorazione",;
    HelpContextID = 93724214,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=358, Top=45

  func oDATINI1_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATINI1 or empty(.w_DATINI1))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_7 as StdField with uid="CKVGPKUDPP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'intervallo della data di fine lavorazione",;
    HelpContextID = 172170806,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=532, Top=22

  func oDATFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFIN<=.w_DATFIN1 or empty(.w_DATFIN1))
    endwith
    return bRes
  endfunc

  add object oDATFIN1_1_8 as StdField with uid="PWYQVYUXKE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATFIN1", cQueryName = "DATFIN1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Seleziona l'intervallo della data di fine lavorazione",;
    HelpContextID = 172170806,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=532, Top=45

  func oDATFIN1_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFIN<=.w_DATFIN1 or empty(.w_DATFIN1))
    endwith
    return bRes
  endfunc


  add object oORDSTATO_1_9 as StdCombo with uid="PZEKYGPJGX",rtseq=9,rtrep=.f.,left=707,top=22,width=107,height=21;
    , ToolTipText = "Stato dell'ordine di c\lavoro";
    , HelpContextID = 234827829;
    , cFormVar="w_ORDSTATO",RowSource=""+"Suggerita,"+"Confermata,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oORDSTATO_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'T',;
    space(10)))))
  endfunc
  func oORDSTATO_1_9.GetRadio()
    this.Parent.oContained.w_ORDSTATO = this.RadioValue()
    return .t.
  endfunc

  func oORDSTATO_1_9.SetRadio()
    this.Parent.oContained.w_ORDSTATO=trim(this.Parent.oContained.w_ORDSTATO)
    this.value = ;
      iif(this.Parent.oContained.w_ORDSTATO=='S',1,;
      iif(this.Parent.oContained.w_ORDSTATO=='D',2,;
      iif(this.Parent.oContained.w_ORDSTATO=='T',3,;
      0)))
  endfunc

  add object oPDCODINI_1_28 as StdField with uid="CWVZMPJNZR",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PDCODINI", cQueryName = "PDCODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "L'articolo iniziale � maggiore di quello finale, obsoleto oppure non di provenienza esterna",;
    ToolTipText = "Codice articolo iniziale",;
    HelpContextID = 184872385,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=114, Top=98, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_PDCODINI"

  func oPDCODINI_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODINI_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODINI_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPDCODINI_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'gsve_zpd.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oPDCODFIN_1_29 as StdField with uid="HPPPNNCKLL",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PDCODFIN", cQueryName = "PDCODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "L'articolo iniziale � maggiore di quello finale, obsoleto oppure non di provenienza esterna",;
    ToolTipText = "Codice articolo finale",;
    HelpContextID = 235204028,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=114, Top=122, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_PDCODFIN"

  func oPDCODFIN_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODFIN_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODFIN_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPDCODFIN_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'gsve_zpd.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oPDFAMAIN_1_31 as StdField with uid="BAUZFWPTWC",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PDFAMAIN", cQueryName = "PDFAMAIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di inizio selezione",;
    HelpContextID = 42122684,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=114, Top=146, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_PDFAMAIN"

  func oPDFAMAIN_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDFAMAIN_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDFAMAIN_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oPDFAMAIN_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FAMIGLIE ARTICOLI",'',this.parent.oContained
  endproc

  add object oPDFAMAFI_1_32 as StdField with uid="GHDDKOKVIG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PDFAMAFI", cQueryName = "PDFAMAFI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia articolo di fine selezione",;
    HelpContextID = 226312767,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=514, Top=146, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_PDFAMAFI"

  proc oPDFAMAFI_1_32.mDefault
    with this.Parent.oContained
      if empty(.w_PDFAMAFI)
        .w_PDFAMAFI = .w_PDFAMAIN
      endif
    endwith
  endproc

  func oPDFAMAFI_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDFAMAFI_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDFAMAFI_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oPDFAMAFI_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FAMIGLIE ARTICOLI",'',this.parent.oContained
  endproc

  add object oPDGRUINI_1_33 as StdField with uid="VYQYSTJFCH",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PDGRUINI", cQueryName = "PDGRUINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di inizio selezione",;
    HelpContextID = 166833601,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=114, Top=169, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_PDGRUINI"

  func oPDGRUINI_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDGRUINI_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDGRUINI_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oPDGRUINI_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc

  add object oPDGRUFIN_1_34 as StdField with uid="BKTBROMCMD",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PDGRUFIN", cQueryName = "PDGRUFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico di fine selezione",;
    HelpContextID = 217165244,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=514, Top=169, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_PDGRUFIN"

  proc oPDGRUFIN_1_34.mDefault
    with this.Parent.oContained
      if empty(.w_PDGRUFIN)
        .w_PDGRUFIN = .w_PDGRUINI
      endif
    endwith
  endproc

  func oPDGRUFIN_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDGRUFIN_1_34.ecpDrop(oSource)
    this.Parent.oContained.link_1_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDGRUFIN_1_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oPDGRUFIN_1_34'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI MERCEOLOGICI",'',this.parent.oContained
  endproc

  add object oPDCATINI_1_35 as StdField with uid="WQNLFQEXDD",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PDCATINI", cQueryName = "PDCATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 169012673,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=114, Top=192, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_PDCATINI"

  func oPDCATINI_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCATINI_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCATINI_1_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oPDCATINI_1_35'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE OMOGENEE",'',this.parent.oContained
  endproc

  add object oPDCATFIN_1_36 as StdField with uid="WHQQEZYTLE",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PDCATFIN", cQueryName = "PDCATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 219344316,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=514, Top=192, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_PDCATFIN"

  proc oPDCATFIN_1_36.mDefault
    with this.Parent.oContained
      if empty(.w_PDCATFIN)
        .w_PDCATFIN = .w_PDCATINI
      endif
    endwith
  endproc

  func oPDCATFIN_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCATFIN_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCATFIN_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oPDCATFIN_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE OMOGENEE",'',this.parent.oContained
  endproc

  add object oPDMARINI_1_37 as StdField with uid="WBGKIJDARW",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PDMARINI", cQueryName = "PDMARINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di inizio selezione",;
    HelpContextID = 171068865,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=114, Top=215, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_PDMARINI"

  func oPDMARINI_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDMARINI_1_37.ecpDrop(oSource)
    this.Parent.oContained.link_1_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDMARINI_1_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oPDMARINI_1_37'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MARCHI",'',this.parent.oContained
  endproc

  add object oPDMARFIN_1_38 as StdField with uid="LATPHZOTCT",rtseq=26,rtrep=.f.,;
    cFormVar = "w_PDMARFIN", cQueryName = "PDMARFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea di fine selezione",;
    HelpContextID = 221400508,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=514, Top=215, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_PDMARFIN"

  proc oPDMARFIN_1_38.mDefault
    with this.Parent.oContained
      if empty(.w_PDMARFIN)
        .w_PDMARFIN = .w_PDMARINI
      endif
    endwith
  endproc

  func oPDMARFIN_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDMARFIN_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDMARFIN_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oPDMARFIN_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MARCHI",'',this.parent.oContained
  endproc

  add object oPDMAGINI_1_41 as StdField with uid="WWAXPPIMPZ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PDMAGINI", cQueryName = "PDMAGINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 182603201,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=115, Top=238, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PDMAGINI"

  func oPDMAGINI_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDMAGINI_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDMAGINI_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPDMAGINI_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MAGAZZINI",'',this.parent.oContained
  endproc

  add object oDESMAGI_1_42 as StdField with uid="QONUNYBHAK",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESMAGI", cQueryName = "DESMAGI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 221638090,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=187, Top=238, InputMask=replicate('X',30)

  add object oPDMAGFIN_1_44 as StdField with uid="IPBLQAUROZ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_PDMAGFIN", cQueryName = "PDMAGFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 232934844,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=514, Top=238, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PDMAGFIN"

  proc oPDMAGFIN_1_44.mDefault
    with this.Parent.oContained
      if empty(.w_PDMAGFIN)
        .w_PDMAGFIN = .w_PDMAGINI
      endif
    endwith
  endproc

  func oPDMAGFIN_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDMAGFIN_1_44.ecpDrop(oSource)
    this.Parent.oContained.link_1_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDMAGFIN_1_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPDMAGFIN_1_44'),iif(empty(i_cWhere),.f.,i_cWhere),'',"MAGAZZINI",'',this.parent.oContained
  endproc

  add object oDESMAGF_1_45 as StdField with uid="EBXVFVWORM",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESMAGF", cQueryName = "DESMAGF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 46797366,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=585, Top=238, InputMask=replicate('X',30)

  add object oDESFAMAI_1_46 as StdField with uid="AOYBYAMKXI",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESFAMAI", cQueryName = "DESFAMAI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 147001983,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=187, Top=146, InputMask=replicate('X',35)

  add object oDESGRUI_1_47 as StdField with uid="ZABORREMDH",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESGRUI", cQueryName = "DESGRUI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 237759946,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=187, Top=169, InputMask=replicate('X',35)

  add object oDESCATI_1_48 as StdField with uid="ZFWYARIWSP",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESCATI", cQueryName = "DESCATI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 4189642,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=187, Top=192, InputMask=replicate('X',35)

  add object oDESFAMAF_1_52 as StdField with uid="WOCDSQXKSA",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESFAMAF", cQueryName = "DESFAMAF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 147001980,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=585, Top=146, InputMask=replicate('X',35)

  add object oDESGRUF_1_53 as StdField with uid="PYGQUHDJMJ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESGRUF", cQueryName = "DESGRUF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 30675510,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=585, Top=169, InputMask=replicate('X',35)

  add object oDESCATF_1_54 as StdField with uid="SXSGGSHCII",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESCATF", cQueryName = "DESCATF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 264245814,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=585, Top=192, InputMask=replicate('X',35)

  add object oDEMARINI_1_58 as StdField with uid="YGJVDCJIRK",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DEMARINI", cQueryName = "DEMARINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 171068801,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=187, Top=215, InputMask=replicate('X',35)

  add object oDEMARFIN_1_60 as StdField with uid="DJYWVFQILW",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DEMARFIN", cQueryName = "DEMARFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 221400444,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=585, Top=215, InputMask=replicate('X',35)

  add object oDESINI_1_62 as StdField with uid="LIJOMNDAQG",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 93721142,;
   bGlobalFont=.t.,;
    Height=21, Width=389, Left=286, Top=98, InputMask=replicate('X',40)

  add object oDESFIN_1_63 as StdField with uid="GTOXOUJFXY",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 172167734,;
   bGlobalFont=.t.,;
    Height=21, Width=389, Left=286, Top=122, InputMask=replicate('X',40)

  add object oPDCODCOM_1_69 as StdField with uid="GGQGKFAWGY",rtseq=45,rtrep=.f.,;
    cFormVar = "w_PDCODCOM", cQueryName = "PDCODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 17100221,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=115, Top=262, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_PDCODCOM"

  func oPDCODCOM_1_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCAN='S')
    endwith
   endif
  endfunc

  func oPDCODCOM_1_69.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_69('Part',this)
      if .not. empty(.w_PDCODATT)
        bRes2=.link_1_70('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPDCODCOM_1_69.ecpDrop(oSource)
    this.Parent.oContained.link_1_69('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODCOM_1_69.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oPDCODCOM_1_69'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oPDCODATT_1_70 as StdField with uid="BJCGFCEHYO",rtseq=46,rtrep=.f.,;
    cFormVar = "w_PDCODATT", cQueryName = "PDCODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 217780810,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=115, Top=285, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_PDCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_PDCODATT"

  func oPDCODATT_1_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_PDCODCOM) and g_COMM='S')
    endwith
   endif
  endfunc

  func oPDCODATT_1_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_70('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODATT_1_70.ecpDrop(oSource)
    this.Parent.oContained.link_1_70('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODATT_1_70.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_PDCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_PDCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oPDCODATT_1_70'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc

  add object oPDCODCON_1_71 as StdField with uid="GRXGWWUYUW",rtseq=47,rtrep=.f.,;
    cFormVar = "w_PDCODCON", cQueryName = "PDCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un fornitore",;
    ToolTipText = "Codice fornitore",;
    HelpContextID = 17100220,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=115, Top=311, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PDCODCON"

  func oPDCODCON_1_71.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_71('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDCODCON_1_71.ecpDrop(oSource)
    this.Parent.oContained.link_1_71('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDCODCON_1_71.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPDCODCON_1_71'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Fornitori",'GSDB_KCS2.CONTI_VZM',this.parent.oContained
  endproc


  add object oObj_1_72 as cp_outputCombo with uid="ZLERXIVPWT",left=112, top=364, width=412,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 33791974


  add object oBtn_1_73 as StdButton with uid="IQSBSYXBUY",left=719, top=365, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 215175702;
    , Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_73.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_73.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!Empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_74 as StdButton with uid="OTRIUSNJDO",left=769, top=365, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 215175702;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_74.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCOM_1_78 as StdField with uid="BCJHQPAVGJ",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 161485366,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=250, Top=262, InputMask=replicate('X',30)

  add object oDESATT_1_80 as StdField with uid="TTNMSCEJIW",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 15602230,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=250, Top=285, InputMask=replicate('X',30)

  add object oDESFOR_1_82 as StdField with uid="RJLIHNJXRO",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 245568054,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=250, Top=311, InputMask=replicate('X',40)

  func oDESFOR_1_82.mHide()
    with this.Parent.oContained
      return (g_COLA<>"S")
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="HKWLVKSBYR",Visible=.t., Left=15, Top=25,;
    Alignment=1, Width=72, Height=15,;
    Caption="Da PDA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ZIUJHJHOOO",Visible=.t., Left=15, Top=47,;
    Alignment=1, Width=72, Height=15,;
    Caption="A PDA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="JHUWVZKKSV",Visible=.t., Left=10, Top=1,;
    Alignment=0, Width=135, Height=15,;
    Caption="Selezione PDA"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="SYQXMJZNYT",Visible=.t., Left=631, Top=23,;
    Alignment=1, Width=72, Height=15,;
    Caption="Stato PDA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="VGMKEHVEFL",Visible=.t., Left=10, Top=71,;
    Alignment=0, Width=135, Height=15,;
    Caption="Altre selezioni"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="CGKMUWGJNM",Visible=.t., Left=246, Top=22,;
    Alignment=1, Width=109, Height=15,;
    Caption="Da data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="DFAYSLSKFL",Visible=.t., Left=249, Top=45,;
    Alignment=1, Width=106, Height=15,;
    Caption="A data inizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="QZYVSVNFWE",Visible=.t., Left=434, Top=22,;
    Alignment=1, Width=95, Height=18,;
    Caption="Da data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="GNOODMLOOO",Visible=.t., Left=444, Top=45,;
    Alignment=1, Width=85, Height=18,;
    Caption="A data fine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="DFDAZGHWTK",Visible=.t., Left=51, Top=100,;
    Alignment=1, Width=61, Height=15,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="TWCCZXZZKY",Visible=.t., Left=60, Top=123,;
    Alignment=1, Width=52, Height=15,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="KBYTQWVWFL",Visible=.t., Left=30, Top=239,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="QPBYNHFHPK",Visible=.t., Left=428, Top=239,;
    Alignment=1, Width=83, Height=15,;
    Caption="A magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="AKMDEUWFDE",Visible=.t., Left=42, Top=146,;
    Alignment=1, Width=70, Height=15,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="EWRTNGDSDH",Visible=.t., Left=29, Top=169,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="RWXHDLRPLM",Visible=.t., Left=22, Top=192,;
    Alignment=1, Width=90, Height=15,;
    Caption="Da cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="BETNLUNOYI",Visible=.t., Left=441, Top=146,;
    Alignment=1, Width=70, Height=15,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="DDVVDZKRKO",Visible=.t., Left=424, Top=169,;
    Alignment=1, Width=87, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="VUGHKWWVJN",Visible=.t., Left=431, Top=192,;
    Alignment=1, Width=80, Height=15,;
    Caption="A cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="PQHJWMLKLX",Visible=.t., Left=22, Top=217,;
    Alignment=1, Width=90, Height=17,;
    Caption="Da marchio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="RGIGIXXNHK",Visible=.t., Left=431, Top=217,;
    Alignment=1, Width=80, Height=17,;
    Caption="A marchio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="CPUIYIXXTN",Visible=.t., Left=9, Top=367,;
    Alignment=1, Width=101, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="ITSFXNBZJR",Visible=.t., Left=10, Top=341,;
    Alignment=0, Width=135, Height=15,;
    Caption="Selezione stampa"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="ODMTRHWNGJ",Visible=.t., Left=5, Top=263,;
    Alignment=1, Width=108, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="MSVOMVFWHL",Visible=.t., Left=5, Top=287,;
    Alignment=1, Width=108, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="UZFXVUFHLH",Visible=.t., Left=5, Top=312,;
    Alignment=1, Width=108, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oBox_1_10 as StdBox with uid="FTPEYJPHMM",left=6, top=17, width=796,height=1

  add object oBox_1_15 as StdBox with uid="KWBXXRLPZN",left=6, top=87, width=817,height=1

  add object oBox_1_75 as StdBox with uid="HZZUJLZEMD",left=6, top=358, width=817,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac1spd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
