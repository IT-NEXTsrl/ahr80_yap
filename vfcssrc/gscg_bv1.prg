* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bv1                                                        *
*              VARIA DESCRIZIONE PRIMANOTA                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-10-13                                                      *
* Last revis.: 2011-04-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bv1",oParentObject,m.pOper)
return(i_retval)

define class tgscg_bv1 as StdBatch
  * --- Local variables
  pOper = space(5)
  w_ROWNUM = 0
  w_AGGIO = .f.
  w_PADRE = .NULL.
  w_GEST = .NULL.
  w_PROG = .NULL.
  w_OBJ1 = .NULL.
  w_OBJ = .NULL.
  * --- WorkFile variables
  PNT_MAST_idx=0
  PNT_DETT_idx=0
  MOVICOST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Varia note (da GSVE_KVD)
    * --- Chiudo la Maschera dello Zoom Primanota (GSVE_KV1)
    if this.pOper<>"DOCUM"
      this.w_PADRE = This.oparentobject
      this.w_GEST = This.oparentobject.oparentobject
      do case
        case this.pOper="SELEZ"
          if NOT EMPTY(this.oParentObject.w_SERIALE)
            * --- Seleziona il documento da modificare
            this.w_GEST.w_SERIALE = this.w_PADRE.w_SERIALE
            this.w_GEST.w_NUMREG = this.w_PADRE.w_NUMREG
            this.w_GEST.w_UTENTE = this.w_PADRE.w_UTENTE
            this.w_GEST.w_DATREG = this.w_PADRE.w_DATREG
            This.bUpdateParentObject=.f.
            this.w_GEST.NotifyEvent("Ricerca")     
            * --- Setta Proprieta' Campi del Cursore
            SELECT (this.w_GEST.w_ZOOM.cCursor)
            GO TOP
            this.w_GEST.w_ZOOM.refresh()     
            this.w_GEST.w_PNDESSUP = PNDESSUP
            this.w_GEST.w_PNDESRIG = PNDESRIG
            this.w_PADRE.EcpSave()     
            this.w_PADRE = .Null.
          endif
        case this.pOper="AGGIO"
          this.w_PADRE = This.oparentobject
          NC = this.w_PADRE.w_ZOOM.cCursor
          * --- Try
          local bErr_03D5D050
          bErr_03D5D050=bTrsErr
          this.Try_03D5D050()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Impossibile aggiornare la registrazione: %1",,"", i_errmsg)
          endif
          bTrsErr=bTrsErr or bErr_03D5D050
          * --- End
        case this.pOper="MARCA"
          NC = this.w_PADRE.w_ZOOM.cCursor
          SELECT (NC)
          if RECCOUNT(NC)>0
             
 REPLACE PNDESRIG WITH this.oParentObject.w_PNDESRIG 
 REPLACE PNINICOM WITH this.oParentObject.w_PNINICOM 
 REPLACE PNFINCOM WITH this.oParentObject.w_PNFINCOM 
 REPLACE AGGANA WITH this.oParentObject.w_AGGANA 
 REPLACE TIPO WITH "S"
            SELECT (NC)
          else
            ah_ErrorMsg("Non ci sono documenti da aggiornare")
          endif
      endcase
    else
      * --- Apre masch VARIAZIONE DATI DESCRITTIVI DEL DOCUMENTO
      this.w_PROG = GSCG_KVD()
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_PROG.bSec1)
        i_retcode = 'stop'
        return
      endif
      this.w_PROG.w_PNSERIAL = this.oParentObject.w_PNSERIAL
      this.w_PROG.w_SERIALE = this.oParentObject.w_PNSERIAL
      this.w_PROG.w_NUMREG = this.oParentObject.w_PNNUMRER
      this.w_PROG.w_UTENTE = this.oParentObject.w_PNCODUTE
      this.w_PROG.w_DATREG = this.oParentObject.w_PNDATREG
      this.w_PROG.w_PNDESSUP = this.oParentObject.w_PNDESSUP
      this.w_PROG.w_TIPCON = this.oParentObject.w_PNTIPCLF
      this.w_PROG.w_CODCON = this.oParentObject.w_PNCODCLF
      this.w_PROG.w_DATINI = this.oParentObject.w_PNDATREG
      this.w_PROG.w_DATFIN = this.oParentObject.w_PNDATREG
      this.w_PROG.w_CODCAU = this.oParentObject.w_PNCODCAU
      this.w_PROG.w_PNDESRIG = this.oParentObject.w_PNDESRIG
      this.w_PROG.w_PNINICOM = this.oParentObject.w_PNINICOM
      this.w_PROG.w_PNFINCOM = this.oParentObject.w_PNFINCOM
      this.w_PROG.w_NUMINI = this.oParentObject.w_PNNUMDOC
      this.w_PROG.w_NUMFIN = this.oParentObject.w_PNNUMDOC
      this.w_PROG.w_ALFINI = this.oParentObject.w_PNALFDOC
      this.w_PROG.w_ALFFIN = this.oParentObject.w_PNALFDOC
      this.w_PROG.w_DADOCINI = this.oParentObject.w_PNDATDOC
      this.w_PROG.w_DADOCFIN = this.oParentObject.w_PNDATDOC
      this.w_PROG.w_CODUTINI = this.oParentObject.w_PNCODUTE
      this.w_PROG.w_CODUTFIN = this.oParentObject.w_PNCODUTE
      this.w_PROG.w_NREGINI = this.oParentObject.w_PNNUMRER
      this.w_PROG.w_NREGFIN = this.oParentObject.w_PNNUMRER
      = setvaluelinked ( "M" , this.w_PROG , "w_CODCAU" , this.oParentObject.w_PNCODCAU)
      = setvaluelinked ( "M" , this.w_PROG , "w_CODCON" , this.oParentObject.w_PNCODCLF,this.oParentObject.w_PNTIPCLF)
      this.w_PROG.SetControlsValue()     
      * --- Setta Proprieta' Campi del Cursore
      SELECT (this.w_PROG.w_ZOOM.cCursor)
      GO TOP
      this.w_PROG.NotifyEvent("Ricerca")
      this.w_PROG.w_ZOOM.refresh()     
    endif
  endproc
  proc Try_03D5D050()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Aggiorna Note generali primanota (pndessup)
    * --- Write into PNT_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PNDESSUP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDESSUP),'PNT_MAST','PNDESSUP');
          +i_ccchkf ;
      +" where ";
          +"PNSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
             )
    else
      update (i_cTable) set;
          PNDESSUP = this.oParentObject.w_PNDESSUP;
          &i_ccchkf. ;
       where;
          PNSERIAL = this.oParentObject.w_SERIALE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_AGGIO = .T.
    * --- Aggiorna descrizione di riga (pndesrig) e le date di competenza(pninicom, pnfincom)
     
 SELECT (NC) 
 GO TOP 
 SCAN
    * --- Se il flag aggiorna analitica � attivo, modifico anche le date di inizio e fine competenza
    this.w_ROWNUM = nvl(CPROWNUM,0)
    this.oParentObject.w_PNDESRIG = nvl(PNDESRIG,"")
    this.oParentObject.w_PNINICOM = CP_TODATE(PNINICOM)
    this.oParentObject.w_PNFINCOM = CP_TODATE(PNFINCOM)
    * --- Write into PNT_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PNDESRIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +",PNINICOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNINICOM),'PNT_DETT','PNINICOM');
      +",PNFINCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNFINCOM),'PNT_DETT','PNFINCOM');
          +i_ccchkf ;
      +" where ";
          +"PNSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
             )
    else
      update (i_cTable) set;
          PNDESRIG = this.oParentObject.w_PNDESRIG;
          ,PNINICOM = this.oParentObject.w_PNINICOM;
          ,PNFINCOM = this.oParentObject.w_PNFINCOM;
          &i_ccchkf. ;
       where;
          PNSERIAL = this.oParentObject.w_SERIALE;
          and CPROWNUM = this.w_ROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_AGGIO = .T.
    if this.oParentObject.w_AGGANA="S"
      * --- Write into MOVICOST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOVICOST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVICOST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MRINICOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNINICOM),'MOVICOST','MRINICOM');
        +",MRFINCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNFINCOM),'MOVICOST','MRFINCOM');
            +i_ccchkf ;
        +" where ";
            +"MRSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
            +" and MRROWORD = "+cp_ToStrODBC(this.w_ROWNUM);
               )
      else
        update (i_cTable) set;
            MRINICOM = this.oParentObject.w_PNINICOM;
            ,MRFINCOM = this.oParentObject.w_PNFINCOM;
            &i_ccchkf. ;
         where;
            MRSERIAL = this.oParentObject.w_SERIALE;
            and MRROWORD = this.w_ROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    ENDSCAN
    if this.oParentObject.w_AGGRIG="S"
      * --- Write into PNT_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNDESRIG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PNDESSUP),'PNT_DETT','PNDESRIG');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
               )
      else
        update (i_cTable) set;
            PNDESRIG = this.oParentObject.w_PNDESSUP;
            &i_ccchkf. ;
         where;
            PNSERIAL = this.oParentObject.w_SERIALE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_AGGIO = .T.
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_AGGIO
      ah_ErrorMsg("Aggiornamento terminato con successo")
    endif
    * --- Setta Proprieta' Campi del Cursore
    SELECT (this.w_PADRE.w_ZOOM.cCursor)
    GO TOP
    This.OparentObject.NotifyEvent("Ricerca")
    this.w_PADRE.w_ZOOM.refresh()     
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='PNT_DETT'
    this.cWorkTables[3]='MOVICOST'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
