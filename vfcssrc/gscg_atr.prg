* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_atr                                                        *
*              Codici tributo                                                  *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-14                                                      *
* Last revis.: 2013-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_atr"))

* --- Class definition
define class tgscg_atr as StdForm
  Top    = 58
  Left   = 65

  * --- Standard Properties
  Width  = 585
  Height = 92+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-08-26"
  HelpContextID=92939625
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  COD_TRIB_IDX = 0
  cFile = "COD_TRIB"
  cKeySelect = "TRCODICE"
  cKeyWhere  = "TRCODICE=this.w_TRCODICE"
  cKeyWhereODBC = '"TRCODICE="+cp_ToStrODBC(this.w_TRCODICE)';

  cKeyWhereODBCqualified = '"COD_TRIB.TRCODICE="+cp_ToStrODBC(this.w_TRCODICE)';

  cPrg = "gscg_atr"
  cComment = "Codici tributo"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TRCODICE = space(5)
  w_TRTIPTRI = space(10)
  w_TRDESCRI = space(0)
  w_TRFLMERF = space(1)
  w_TRFLCODA = space(1)
  w_TRIDOPER = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'COD_TRIB','gscg_atr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_atrPag1","gscg_atr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Codici tributo")
      .Pages(1).HelpContextID = 254730566
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTRCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='COD_TRIB'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.COD_TRIB_IDX,5],7]
    this.nPostItConn=i_TableProp[this.COD_TRIB_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TRCODICE = NVL(TRCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from COD_TRIB where TRCODICE=KeySet.TRCODICE
    *
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('COD_TRIB')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "COD_TRIB.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' COD_TRIB '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TRCODICE',this.w_TRCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TRCODICE = NVL(TRCODICE,space(5))
        .w_TRTIPTRI = NVL(TRTIPTRI,space(10))
        .w_TRDESCRI = NVL(TRDESCRI,space(0))
        .w_TRFLMERF = NVL(TRFLMERF,space(1))
        .w_TRFLCODA = NVL(TRFLCODA,space(1))
        .w_TRIDOPER = NVL(TRIDOPER,space(1))
        cp_LoadRecExtFlds(this,'COD_TRIB')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TRCODICE = space(5)
      .w_TRTIPTRI = space(10)
      .w_TRDESCRI = space(0)
      .w_TRFLMERF = space(1)
      .w_TRFLCODA = space(1)
      .w_TRIDOPER = space(1)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'COD_TRIB')
    this.DoRTCalc(1,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTRCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oTRTIPTRI_1_5.enabled = i_bVal
      .Page1.oPag.oTRDESCRI_1_6.enabled = i_bVal
      .Page1.oPag.oTRFLMERF_1_7.enabled = i_bVal
      .Page1.oPag.oTRFLCODA_1_8.enabled = i_bVal
      .Page1.oPag.oTRIDOPER_1_9.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTRCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTRCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'COD_TRIB',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODICE,"TRCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRTIPTRI,"TRTIPTRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRDESCRI,"TRDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRFLMERF,"TRFLMERF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRFLCODA,"TRFLCODA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRIDOPER,"TRIDOPER",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    i_lTable = "COD_TRIB"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.COD_TRIB_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCG_STR with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.COD_TRIB_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into COD_TRIB
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'COD_TRIB')
        i_extval=cp_InsertValODBCExtFlds(this,'COD_TRIB')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TRCODICE,TRTIPTRI,TRDESCRI,TRFLMERF,TRFLCODA"+;
                  ",TRIDOPER "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TRCODICE)+;
                  ","+cp_ToStrODBC(this.w_TRTIPTRI)+;
                  ","+cp_ToStrODBC(this.w_TRDESCRI)+;
                  ","+cp_ToStrODBC(this.w_TRFLMERF)+;
                  ","+cp_ToStrODBC(this.w_TRFLCODA)+;
                  ","+cp_ToStrODBC(this.w_TRIDOPER)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'COD_TRIB')
        i_extval=cp_InsertValVFPExtFlds(this,'COD_TRIB')
        cp_CheckDeletedKey(i_cTable,0,'TRCODICE',this.w_TRCODICE)
        INSERT INTO (i_cTable);
              (TRCODICE,TRTIPTRI,TRDESCRI,TRFLMERF,TRFLCODA,TRIDOPER  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TRCODICE;
                  ,this.w_TRTIPTRI;
                  ,this.w_TRDESCRI;
                  ,this.w_TRFLMERF;
                  ,this.w_TRFLCODA;
                  ,this.w_TRIDOPER;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.COD_TRIB_IDX,i_nConn)
      *
      * update COD_TRIB
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'COD_TRIB')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TRTIPTRI="+cp_ToStrODBC(this.w_TRTIPTRI)+;
             ",TRDESCRI="+cp_ToStrODBC(this.w_TRDESCRI)+;
             ",TRFLMERF="+cp_ToStrODBC(this.w_TRFLMERF)+;
             ",TRFLCODA="+cp_ToStrODBC(this.w_TRFLCODA)+;
             ",TRIDOPER="+cp_ToStrODBC(this.w_TRIDOPER)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'COD_TRIB')
        i_cWhere = cp_PKFox(i_cTable  ,'TRCODICE',this.w_TRCODICE  )
        UPDATE (i_cTable) SET;
              TRTIPTRI=this.w_TRTIPTRI;
             ,TRDESCRI=this.w_TRDESCRI;
             ,TRFLMERF=this.w_TRFLMERF;
             ,TRFLCODA=this.w_TRFLCODA;
             ,TRIDOPER=this.w_TRIDOPER;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.COD_TRIB_IDX,i_nConn)
      *
      * delete COD_TRIB
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TRCODICE',this.w_TRCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTRFLCODA_1_8.visible=!this.oPgFrm.Page1.oPag.oTRFLCODA_1_8.mHide()
    this.oPgFrm.Page1.oPag.oTRIDOPER_1_9.visible=!this.oPgFrm.Page1.oPag.oTRIDOPER_1_9.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTRCODICE_1_1.value==this.w_TRCODICE)
      this.oPgFrm.Page1.oPag.oTRCODICE_1_1.value=this.w_TRCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTRTIPTRI_1_5.value==this.w_TRTIPTRI)
      this.oPgFrm.Page1.oPag.oTRTIPTRI_1_5.value=this.w_TRTIPTRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTRDESCRI_1_6.value==this.w_TRDESCRI)
      this.oPgFrm.Page1.oPag.oTRDESCRI_1_6.value=this.w_TRDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTRFLMERF_1_7.RadioValue()==this.w_TRFLMERF)
      this.oPgFrm.Page1.oPag.oTRFLMERF_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRFLCODA_1_8.RadioValue()==this.w_TRFLCODA)
      this.oPgFrm.Page1.oPag.oTRFLCODA_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIDOPER_1_9.RadioValue()==this.w_TRIDOPER)
      this.oPgFrm.Page1.oPag.oTRIDOPER_1_9.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'COD_TRIB')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TRTIPTRI = 'Erario' or .w_TRTIPTRI ='Regione' or .w_TRTIPTRI = 'Enti local' or .w_TRTIPTRI = 'ICI' or .w_TRTIPTRI = 'Accise' or .w_TRTIPTRI = 'Altri trib' or .w_TRTIPTRI = 'IMU')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRTIPTRI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un valore fra: 'enti local'- 'erario' - 'regione' - 'ICI' - 'Accise' - 'Altri trib' - 'IMU'")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_atrPag1 as StdContainer
  Width  = 581
  height = 92
  stdWidth  = 581
  stdheight = 92
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRCODICE_1_1 as StdField with uid="IHNTRXKNQU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TRCODICE", cQueryName = "TRCODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo",;
    HelpContextID = 134832763,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=44, Left=85, Top=16, InputMask=replicate('X',5)

  add object oTRTIPTRI_1_5 as StdField with uid="LWNMQGKKCW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TRTIPTRI", cQueryName = "TRTIPTRI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un valore fra: 'enti local'- 'erario' - 'regione' - 'ICI' - 'Accise' - 'Altri trib' - 'IMU'",;
    ToolTipText = "Tipo tributo (erario - regione - enti local - ICI - Accise - Altri trib - IMU) ",;
    HelpContextID = 63206015,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=85, Top=44, InputMask=replicate('X',10)

  func oTRTIPTRI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TRTIPTRI = 'Erario' or .w_TRTIPTRI ='Regione' or .w_TRTIPTRI = 'Enti local' or .w_TRTIPTRI = 'ICI' or .w_TRTIPTRI = 'Accise' or .w_TRTIPTRI = 'Altri trib' or .w_TRTIPTRI = 'IMU')
    endwith
    return bRes
  endfunc

  add object oTRDESCRI_1_6 as StdMemo with uid="AATTNKMHMX",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TRDESCRI", cQueryName = "TRDESCRI",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione completa del codice selezionato",;
    HelpContextID = 49246847,;
   bGlobalFont=.t.,;
    Height=51, Width=331, Left=236, Top=15, DisabledBackColor=RGB(255,255,255)

  add object oTRFLMERF_1_7 as StdCheck with uid="TJNUTNJLVF",rtseq=4,rtrep=.f.,left=85, top=68, caption="Obbligo mese rif. in mod. F24",;
    ToolTipText = "Se attivo, indica l'obbligo di inserire il mese di riferimento nel mod. F24",;
    HelpContextID = 76976764,;
    cFormVar="w_TRFLMERF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTRFLMERF_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTRFLMERF_1_7.GetRadio()
    this.Parent.oContained.w_TRFLMERF = this.RadioValue()
    return .t.
  endfunc

  func oTRFLMERF_1_7.SetRadio()
    this.Parent.oContained.w_TRFLMERF=trim(this.Parent.oContained.w_TRFLMERF)
    this.value = ;
      iif(this.Parent.oContained.w_TRFLMERF=='S',1,;
      0)
  endfunc

  add object oTRFLCODA_1_8 as StdCheck with uid="SXMJHXXHMQ",rtseq=5,rtrep=.f.,left=285, top=68, caption="Codice atto",;
    ToolTipText = "Se attivo al tributo vengono associati il codice atto e il codice ufficio specificati sulla maschera F24",;
    HelpContextID = 234263159,;
    cFormVar="w_TRFLCODA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTRFLCODA_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTRFLCODA_1_8.GetRadio()
    this.Parent.oContained.w_TRFLCODA = this.RadioValue()
    return .t.
  endfunc

  func oTRFLCODA_1_8.SetRadio()
    this.Parent.oContained.w_TRFLCODA=trim(this.Parent.oContained.w_TRFLCODA)
    this.value = ;
      iif(this.Parent.oContained.w_TRFLCODA=='S',1,;
      0)
  endfunc

  func oTRFLCODA_1_8.mHide()
    with this.Parent.oContained
      return (UPPER (ALLTRIM(NVL (.w_TRTIPTRI,'')))<>'ERARIO')
    endwith
  endfunc

  add object oTRIDOPER_1_9 as StdCheck with uid="ZBQNIVWABD",rtseq=6,rtrep=.f.,left=421, top=68, caption="Identificativo operazione",;
    ToolTipText = "Se attivo, il codice tributo prevede l' identificativo operazione",;
    HelpContextID = 263111304,;
    cFormVar="w_TRIDOPER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTRIDOPER_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTRIDOPER_1_9.GetRadio()
    this.Parent.oContained.w_TRIDOPER = this.RadioValue()
    return .t.
  endfunc

  func oTRIDOPER_1_9.SetRadio()
    this.Parent.oContained.w_TRIDOPER=trim(this.Parent.oContained.w_TRIDOPER)
    this.value = ;
      iif(this.Parent.oContained.w_TRIDOPER=='S',1,;
      0)
  endfunc

  func oTRIDOPER_1_9.mHide()
    with this.Parent.oContained
      return (NOT ( UPPER(.w_TRTIPTRI) = 'ENTI LOCAL' or  UPPER(.w_TRTIPTRI )= 'ICI'  or  UPPER(.w_TRTIPTRI) = 'ALTRI TRIB' or  UPPER(.w_TRTIPTRI) = 'IMU'))
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="PPATPADNNT",Visible=.t., Left=40, Top=16,;
    Alignment=1, Width=42, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_3 as StdString with uid="OLQXXCVOUP",Visible=.t., Left=150, Top=16,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="SGKRMQODEH",Visible=.t., Left=8, Top=47,;
    Alignment=1, Width=74, Height=15,;
    Caption="Tipo tributo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_atr','COD_TRIB','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TRCODICE=COD_TRIB.TRCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
