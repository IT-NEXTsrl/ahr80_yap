* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_brc                                                        *
*              Rivaloriz. trasf. e consumi                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_101]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-01                                                      *
* Last revis.: 2013-08-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_brc",oParentObject)
return(i_retval)

define class tgsma_brc as StdBatch
  * --- Local variables
  w_Articolo = space(20)
  w_Prezzo = 0
  w_Message = space(20)
  w_ValMag = 0
  w_CosTotale = 0
  w_ImpNaz = 0
  w_oImpNaz = 0
  w_oImpComm = 0
  w_CosMedPon = 0
  w_FLRIVA = space(1)
  w_AGGVAL = space(1)
  w_CosUltimo = 0
  w_CRIVAL = space(2)
  w_CosStand = 0
  w_FLAVAL = space(1)
  w_QtaAcqui = 0
  w_QtaEsi = 0
  w_FLCASC = space(1)
  w_Serial = space(10)
  w_QTAUM1 = 0
  w_CpRowNum = 0
  w_Numrif = 0
  w_DECUNI = 0
  w_APPO = 0
  w_DECTOT = 0
  w_MVVALNAZ = space(3)
  w_CAONAZ = 0
  w_DECEUR = 0
  w_DECLIR = 0
  w_DATADOCU = ctod("  /  /  ")
  w_MMCODCOM = space(15)
  w_CNCODVAL = space(3)
  w_MMCODATT = space(15)
  w_MMCODCOS = space(5)
  w_MMFLCOCO = space(1)
  w_MMFLORCO = space(1)
  w_MMIMPCOM = 0
  w_ImpCom = 0
  w_CSIMPCOM = 0
  w_NO_MOV = 0
  * --- WorkFile variables
  MVM_DETT_idx=0
  DOC_DETT_idx=0
  PAR_RIMA_idx=0
  PAR_RIOR_idx=0
  INVENTAR_idx=0
  ESERCIZI_idx=0
  VALUTE_idx=0
  MA_COSTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rivalorizzazione Trasferimenti e Consumi (da GSMA_KRC)
    * --- Questa procedura ricalcola il valore dei movimenti che utilizzano causali per le quali e'
    *     prevista la rivalorizzazione a costo medio ponderato od ultimo costo.
    *     La query contiene, oltre ai movimenti da rivalorizzare, le
    *     registrazioni necessarie per il calcolo del costo medio ponderato alle diverse date.
    * --- Lettura valuta dell'Esercizio (dell'inventario)
    * --- Read from ESERCIZI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ESVALNAZ"+;
        " from "+i_cTable+" ESERCIZI where ";
            +"ESCODAZI = "+cp_ToStrODBC(i_codazi);
            +" and ESCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ESVALNAZ;
        from (i_cTable) where;
            ESCODAZI = i_codazi;
            and ESCODESE = this.oParentObject.w_CODESE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVVALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura del cambio della valuta dell'Esercizio (dell'inventario)
    this.w_CAONAZ = getcam(this.w_MVVALNAZ, g_FINESE)
    * --- Le due letture seguenti vengono effettuate all'inizio della procedura:
    *     in tal modo evitiamo di leggere i decimali della valuta associata alla commessa (pu� essere solo Euro/Lire)
    * --- Lettura dei decimali totali
    this.w_DECEUR = GETVALUT(g_PERVAL,"VADECTOT")
    this.w_Articolo = ""
    this.w_QtaAcqui = 0
    this.w_Message = ""
    this.w_Serial = ""
    this.w_CosTotale = 0
    this.w_CpRowNum = 0
    this.w_CosMedPon = 0
    this.w_Prezzo = 0
    this.w_CosUltimo = 0
    this.w_ValMag = 0
    this.w_CosStand = 0
    this.w_ImpNaz = 0
    this.w_ImpCom = 0
    this.w_CSIMPCOM = 0
    this.w_NO_MOV = 0
    * --- Aggiornamento movimenti di magazzino, documenti di vendita ed archivi collegati
    ah_Msg("Lettura movimenti da valorizzare...")
    * --- Il magazzzino di filtro �, se esiste, il magazzino di raggruppamento
    *     altrimenti il magazzino selezionato
    *     (la rivalorizzazione deve considerare anche i movimenti 
    *     appartenenti al solito raggruppamento fiscale)
    this.oParentObject.w_MAGRAG = iif( Empty( this.oParentObject.w_MAGRAG ) , this.oParentObject.w_CODMAG , this.oParentObject.w_MAGRAG )
    * --- Try
    local bErr_03F8AE90
    bErr_03F8AE90=bTrsErr
    this.Try_03F8AE90()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("IMPOSSIBILE AGGIORNARE LE REGISTRAZIONI%0%0OPERAZIONE ANNULLATA")
    endif
    bTrsErr=bTrsErr or bErr_03F8AE90
    * --- End
  endproc
  proc Try_03F8AE90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Scorre i movimenti ed i documenti interessati dalla rivalorizzazione e ricalcola gli importi
    this.w_Articolo = "???###!!!"
    * --- Select from GSMAN3BRC
    do vq_exec with 'GSMAN3BRC',this,'_Curs_GSMAN3BRC','',.f.,.t.
    if used('_Curs_GSMAN3BRC')
      select _Curs_GSMAN3BRC
      locate for 1=1
      do while not(eof())
      * --- Per ogni Nuovo Articolo/Variante riparte con il calcolo delle valorizzazioni dagli importi letti in Inventario
      if this.w_Articolo<>_Curs_GSMAN3BRC.MMCODART
        this.w_Articolo = MMCODART
        this.w_Message = "Elaborazione movimenti articolo %1..."
        ah_Msg(this.w_Message,.T.,.F.,.F., ALLTRIM(this.w_Articolo) )
        * --- Inizializza i Valori con quelli presenti nell'inventario
        this.w_CosMedPon = nvl( _Curs_GSMAN3BRC.DICOSMPA , 0 )
        this.w_CosUltimo = nvl( _Curs_GSMAN3BRC.DICOSULT , 0 )
        this.w_CosStand = nvl( _Curs_GSMAN3BRC.DICOSSTA , 0 )
        this.w_QtaAcqui = nvl( _Curs_GSMAN3BRC.DIQTAACQ , 0 )
        this.w_QtaEsi = nvl( _Curs_GSMAN3BRC.DIQTAESI,0)
        * --- L'inventario ha il flag Costo Medio Ponderato Continuo attivo
        if this.oParentObject.w_ESCONT="S"
          this.w_CosTotale = this.w_CosMedPon * this.w_QtaEsi
        else
          this.w_CosTotale = this.w_CosMedPon * this.w_QtaAcqui
        endif
      endif
      this.w_QTAUM1 = NVL(_Curs_GSMAN3BRC.MMQTAUM1, 0)
      this.w_ImpNaz = NVL( _Curs_GSMAN3BRC.MMIMPNAZ, 0)
      this.w_oImpNaz = NVL( _Curs_GSMAN3BRC.MMIMPNAZ, 0)
      this.w_DECUNI = NVL(_Curs_GSMAN3BRC.VADECUNI, 0)
      this.w_DECTOT = NVL(_Curs_GSMAN3BRC.VADECTOT, 0)
      * --- Per ogni riga legge i criteri di valorizzazione
      this.w_FLRIVA = NVL(_Curs_GSMAN3BRC.CMFLRIVA," ")
      this.w_CRIVAL = NVL(_Curs_GSMAN3BRC.CRIVAL,"  ")
      this.w_FLAVAL = NVL(_Curs_GSMAN3BRC.FLAVAL," ")
      this.w_FLCASC = NVL(_Curs_GSMAN3BRC.FLCASC," ")
      this.w_AGGVAL = NVL(_Curs_GSMAN3BRC.CMAGGVAL," ")
      * --- Per ogni riga legge i dati relativi alla commessa
      this.w_DATADOCU = IIF(EMPTY(CP_TODATE(_Curs_GSMAN3BRC.MMDATDOC)), _Curs_GSMAN3BRC.MMDATREG, _Curs_GSMAN3BRC.MMDATDOC) 
      this.w_MMCODCOM = NVL(_Curs_GSMAN3BRC.MMCODCOM,"               ")
      this.w_CNCODVAL = NVL(_Curs_GSMAN3BRC.CNCODVAL,"   ")
      this.w_MMCODATT = NVL(_Curs_GSMAN3BRC.MMCODATT,"               ")
      this.w_MMCODCOS = NVL(_Curs_GSMAN3BRC.MMCODCOS,"     ")
      this.w_MMFLCOCO = NVL(_Curs_GSMAN3BRC.MMFLCOCO," ")
      this.w_MMFLORCO = NVL(_Curs_GSMAN3BRC.MMFLORCO," ")
      this.w_MMIMPCOM = NVL(_Curs_GSMAN3BRC.MMIMPCOM, 0)
      this.w_oImpComm = NVL(_Curs_GSMAN3BRC.MMIMPCOM, 0)
      * --- Movimento da Rivalorizzare
      *     Aggiorna il valore Per i Criteri Previsti (il filtro sui soli Mov. di Carico/Scarico e' per ulteriore sicurezza)
      *     Nel caso in cui attivi il check riv.ultimi costi standard modifico solo i movimenti 
      *     con rivalorizzazione a costo standard..
      if this.w_FLRIVA="S" AND this.w_FLAVAL $ "CS" And ((this.oParentObject.w_RICOST="S" And this.w_CRIVAL="CS")Or(this.oParentObject.w_RICOST<>"S" And this.w_CRIVAL$ "CM|UC|CS"))
        * --- Imposta la chiave della riga da aggiornare
        this.w_Serial = _Curs_GSMAN3BRC.MMSERIAL
        this.w_CpRowNum = _Curs_GSMAN3BRC.CPROWNUM
        this.w_Numrif = _Curs_GSMAN3BRC.MMNUMRIF
        * --- Calcola i valori da aggiornare sulla riga del movimento di magazzino o del documento di vendita
        *     I calcoli sono riferiti alla valuta nazionale perch� i movimenti in valuta estera vengono filtrati dalle query.
        this.w_Prezzo = 0
        do case
          case this.w_CRIVAL="CM"
            this.w_Prezzo = this.w_CosMedPon
          case this.w_CRIVAL="UC"
            this.w_Prezzo = this.w_CosUltimo
          case this.w_CRIVAL="CS"
            if this.oParentObject.w_RICOST="S"
              * --- Legge il Costo Standard presente sull'Anagrafica
              *     Controllo  prima se esiste il costo standard per quel magazzino
              * --- Read from PAR_RIMA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PAR_RIMA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIMA_idx,2],.t.,this.PAR_RIMA_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PRCOSSTA"+;
                  " from "+i_cTable+" PAR_RIMA where ";
                      +"PRCODART = "+cp_ToStrODBC(this.w_Articolo);
                      +" and PRCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PRCOSSTA;
                  from (i_cTable) where;
                      PRCODART = this.w_Articolo;
                      and PRCODMAG = this.oParentObject.w_CODMAG;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_Prezzo = NVL(cp_ToDate(_read_.PRCOSSTA),cp_NullValue(_read_.PRCOSSTA))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Atrimenti seleziona il costo standard per tutti i magazzini
              if this.w_Prezzo=0
                * --- Read from PAR_RIOR
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PRCOSSTA"+;
                    " from "+i_cTable+" PAR_RIOR where ";
                        +"PRCODART = "+cp_ToStrODBC(this.w_Articolo);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PRCOSSTA;
                    from (i_cTable) where;
                        PRCODART = this.w_Articolo;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_Prezzo = NVL(cp_ToDate(_read_.PRCOSSTA),cp_NullValue(_read_.PRCOSSTA))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
            else
              this.w_Prezzo = this.w_CosStand
            endif
        endcase
        * --- Aggiornamento archivi
        this.w_ImpNaz = cp_ROUND(this.w_QTAUM1 * this.w_Prezzo, this.w_DECTOT)
        * --- Importo in Commessa: usato solo nella scrittura in DOC_DETT (in MVM_DETT la commessa � nascosta)
        this.w_ImpCom = IIF(this.w_MVVALNAZ=this.w_CNCODVAL,this.w_ImpNaz,MON2VAL(this.w_ImpNaz, this.w_CAONAZ, 1, this.w_DATADOCU,this.w_MVVALNAZ, this.w_DECEUR))
        * --- Se devo modificare modifico altrimenti passo otlre senza scrivere sul database..
        if ( this.w_IMPNAZ<>this.w_oIMPNAZ ) Or ( this.w_oIMPCOMM<> this.w_IMPCOM )
          if this.w_Numrif = -10
            * --- Aggiorna la riga di dettaglio del movimento di magazzino
            * --- Write into MVM_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MVM_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MVM_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MVM_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MMIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_ImpNaz),'MVM_DETT','MMIMPNAZ');
                  +i_ccchkf ;
              +" where ";
                  +"MMSERIAL = "+cp_ToStrODBC(this.w_Serial);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CpRowNum);
                  +" and MMNUMRIF = "+cp_ToStrODBC(this.w_Numrif);
                     )
            else
              update (i_cTable) set;
                  MMIMPNAZ = this.w_ImpNaz;
                  &i_ccchkf. ;
               where;
                  MMSERIAL = this.w_Serial;
                  and CPROWNUM = this.w_CpRowNum;
                  and MMNUMRIF = this.w_Numrif;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Aggiorna la riga di dettaglio del documento di vendita
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_ImpNaz),'DOC_DETT','MVIMPNAZ');
              +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_ImpCom),'DOC_DETT','MVIMPCOM');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_Serial);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CpRowNum);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_Numrif);
                     )
            else
              update (i_cTable) set;
                  MVIMPNAZ = this.w_ImpNaz;
                  ,MVIMPCOM = this.w_ImpCom;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_Serial;
                  and CPROWNUM = this.w_CpRowNum;
                  and MVNUMRIF = this.w_Numrif;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if NOT EMPTY(this.w_MMCODCOM) AND NOT EMPTY(this.w_MMCODATT) AND NOT EMPTY(this.w_MMCODCOS)
            this.w_CSIMPCOM = this.w_ImpCom-this.w_MMIMPCOM
            * --- Write into MA_COSTI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.MA_COSTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_MMFLCOCO,'CSCONSUN','this.w_CSIMPCOM',this.w_CSIMPCOM,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_MMFLORCO,'CSORDIN','this.w_CSIMPCOM',this.w_CSIMPCOM,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
              +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
                  +i_ccchkf ;
              +" where ";
                  +"CSCODCOM = "+cp_ToStrODBC(this.w_MMCODCOM);
                  +" and CSTIPSTR = "+cp_ToStrODBC("A");
                  +" and CSCODMAT = "+cp_ToStrODBC(this.w_MMCODATT);
                  +" and CSCODCOS = "+cp_ToStrODBC(this.w_MMCODCOS);
                     )
            else
              update (i_cTable) set;
                  CSCONSUN = &i_cOp1.;
                  ,CSORDIN = &i_cOp2.;
                  &i_ccchkf. ;
               where;
                  CSCODCOM = this.w_MMCODCOM;
                  and CSTIPSTR = "A";
                  and CSCODMAT = this.w_MMCODATT;
                  and CSCODCOS = this.w_MMCODCOS;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Esistono movimenti da rivalorizzare..
          this.w_NO_MOV = this.w_NO_MOV + 1
        endif
      endif
      * --- Calcola le Rivalorizzazioni
      *     Considera solo i records relativi agli acquisti o Carichi con attivo flag 
      *     aggiornamento valore
      if this.w_AGGVAL="S" And this.w_FLAVAL $ "A-C" AND this.w_QTAUM1<>0
        if this.w_FLCASC="+"
          this.w_CosUltimo = cp_ROUND(this.w_ImpNaz/this.w_QTAUM1, this.w_DECUNI)
          * --- L'inventario ha il flag Costo Medio Ponderato Continuo attivo
          if this.oParentObject.w_ESCONT="S"
            this.w_APPO = this.w_QtaEsi + this.w_QTAUM1
            this.w_CosMedPon = IIF(this.w_APPO=0, 0, cp_ROUND(((this.w_CosMedPon*this.w_QtaEsi) + this.w_IMPNAZ)/this.w_APPO, this.w_DECUNI))
            this.w_QtaEsi = this.w_QtaEsi + this.w_QTAUM1
          else
            this.w_APPO = this.w_QtaAcqui + this.w_QTAUM1
            this.w_CosMedPon = IIF(this.w_APPO=0, 0, cp_ROUND(((this.w_CosMedPon*this.w_QtaAcqui) + this.w_IMPNAZ)/this.w_APPO, this.w_DECUNI))
            this.w_QtaAcqui = this.w_QtaAcqui + this.w_QTAUM1
          endif
          this.w_CosTotale = this.w_CosTotale + this.w_ImpNaz
        else
          * --- Dopo 3.0 non � possibile avere un movimento di Acquisto/Carico che 
          *     aggiorni il valore e diminuisca esistenza (check causale)
          *     L'inventario ha il flag Costo Medio Ponderato Continuo attivo
          if this.oParentObject.w_ESCONT="S"
            this.w_APPO = this.w_QtaEsi - this.w_QTAUM1
            this.w_CosMedPon = IIF(this.w_APPO=0, 0, cp_ROUND(((this.w_CosMedPon*this.w_QtaEsi) - this.w_IMPNAZ)/this.w_APPO, this.w_DECUNI))
            this.w_QtaEsi = this.w_QtaEsi - this.w_QTAUM1
          else
            this.w_APPO = this.w_QtaAcqui - this.w_QTAUM1
            this.w_CosMedPon = IIF(this.w_APPO=0, 0, cp_ROUND(((this.w_CosMedPon*this.w_QtaAcqui) - this.w_IMPNAZ)/this.w_APPO, this.w_DECUNI))
            this.w_QtaAcqui = this.w_QtaAcqui - this.w_QTAUM1
          endif
          this.w_CosTotale = this.w_CosTotale - this.w_ImpNaz
        endif
      endif
        select _Curs_GSMAN3BRC
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_NO_MOV=0
      ah_ErrorMsg("Per la selezione effettuata non esistono dati da elaborare")
    else
      ah_ErrorMsg("Valorizzazione terminata su %1 righe di magazzino",,"", Alltrim(Str(this.w_NO_MOV)) )
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='MVM_DETT'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='PAR_RIMA'
    this.cWorkTables[4]='PAR_RIOR'
    this.cWorkTables[5]='INVENTAR'
    this.cWorkTables[6]='ESERCIZI'
    this.cWorkTables[7]='VALUTE'
    this.cWorkTables[8]='MA_COSTI'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_GSMAN3BRC')
      use in _Curs_GSMAN3BRC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
