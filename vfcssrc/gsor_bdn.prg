* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_bdn                                                        *
*              Elabora disp. nel tempo                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_227]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-21                                                      *
* Last revis.: 2015-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Output
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsor_bdn",oParentObject,m.w_Output)
return(i_retval)

define class tgsor_bdn as StdBatch
  * --- Local variables
  w_Output = space(1)
  w_DatIni = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_PrimoGiorno = space(10)
  w_Messaggio = space(10)
  w_TmpVar = 0
  w_TipoPeri = space(1)
  w_FLDETMOV = space(1)
  w_ProgDisp = 0
  w_Trovato = space(1)
  w_Progress = 0
  w_DisNegat = space(1)
  w_InizArt = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Disponibilita' Nel Tempo (da GSOR_SDT)
    * --- Parametri
    * --- w_OutPut : S=Stampa, G=Grafico, N=Nessuno
    if empty( this.w_Output )
      this.w_Output = "S"
    endif
    if this.w_Output="D"
      * --- Chiusura cursori
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Variabili della maschera
    * --- Variabili locali
    this.w_DatIni = cp_CharToDate("  -  -  ")
    this.w_DATFIN = this.oParentObject.w_DATSIS
    this.w_PrimoGiorno = val(sys(11,this.w_DATINI))
    this.w_Messaggio = ""
    this.w_TmpVar = 0
    this.w_TipoPeri = ""
    this.w_FLDETMOV = "N"
    * --- Calcola progressivamente la Disponibilit�
    this.w_Progress = 0
    * --- Controlla se la Disponibilit� assume valori negativi
    * --- Gestione tipo periodicita'
    do case
      case this.oParentObject.w_SelPer = 1
        this.w_TipoPeri = "G"
      case this.oParentObject.w_SelPer = 7
        this.w_TipoPeri = "S"
      case this.oParentObject.w_SelPer = 30
        this.w_TipoPeri = "M"
      case this.oParentObject.w_SelPer = 99
        this.w_TipoPeri = "G"
        this.w_FLDETMOV = "S"
        * --- Questo � il caso del Dettaglio Movimenti
    endcase
    * --- La data in testata viene valorizzata con la data della fine del periodo che precede quello contente la data di sistema
    this.w_DATFIN = ELABPDT(i_datsys, this.w_TipoPeri, this.w_DatIni, this.w_DatFin, "I") -1
    * --- Variabili per il report
    DATFIN = this.w_DATFIN
    CODPRO = this.oParentObject.w_CODPRO
    CODFOR = this.oParentObject.w_CODFOR
    CODMAG = this.oParentObject.w_CODMAG
    CODINI = this.oParentObject.w_CODINI
    CODFIN = this.oParentObject.w_CODFIN
    CODFAM = this.oParentObject.w_CODFAM
    SELORD = this.oParentObject.w_SELORD
    CODGRU = this.oParentObject.w_CODGRU
    SELART = this.oParentObject.w_SELART
    CODCAT = this.oParentObject.w_CODCAT
    SELPER = this.oParentObject.w_SELPER
    CODMAR = this.oParentObject.w_CODMAR
    SELMOV = this.oParentObject.w_SELMOV
    MD = this.oParentObject.w_MD
    MA = this.oParentObject.w_MA
    * --- Creazione cursore
    ah_Msg("Calcolo disponibilit� articoli...",.T.)
    if this.oParentObject.w_GRUPPO="M"
      vq_exec("query\GSOR3SDG",this,"SALDIART")
      vq_exec("query\GSOR_SDG",this,"MOVIART")
    else
      vq_exec("query\GSOR5SDG",this,"SALDIART")
      vq_exec("query\GSOR6SDG",this,"MOVIART")
    endif
    if reccount("SALDIART")=0
      ah_ErrorMsg("Per la selezione effettuata non esistono dati da elaborare")
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    if g_PROD="S"
      * --- Lettura Movimenti da ODL
      if this.oParentObject.w_GRUPPO="M"
        vq_exec("..\COLA\EXE\QUERY\GSCO_SDG",this,"MOVIODL")
      else
        vq_exec("..\COLA\EXE\QUERY\GSCO2SDG",this,"MOVIODL")
      endif
      if reccount("MOVIODL")>0
        * --- Mette in Union con MOVIART
        SELECT * FROM MOVIART ;
        UNION ALL SELECT Magazzino, Articolo, Desart, TipProv, MMDATREG, Ordinato, Impegnato, Riservato, ;
        TDFLDTPR, MVDATEVA, 999999999999999 + VAL(NUMDOC) - 999999999999999 AS NUMDOC, ALFDOC, CAUMAG, ;
        SLQTAPER, SLQTRPER, SLQTOPER, SLQTIPER, ARUNMIS1, CPROWNUM, MVTIPCON, MVCODCON, ANDESCRI ;
        FROM MOVIODL INTO CURSOR MOVIART
      endif
      if used("MOVIODL")
        select MOVIODL
        use
      endif
    endif
    * --- Seleziona la data da prendere come riferimento fra quella di registrazione e quella di prevista evasione
    Select *, ;
    CP_TODATE(MVDATEVA) as XXDATREG ;
    from MOVIART into cursor MOVIART
    * --- Aggiunge i periodi di riferimento
    * --- Non � possibile fare un'unica select con la seguente perch� il fox entra in loop.
    Select *, ;
    ELABPDT(cp_todate(xxdatreg), this.w_TipoPeri, this.w_DatIni, this.w_DatFin, "I") as PeriIniz, ;
    ELABPDT(cp_todate(xxdatreg), this.w_TipoPeri, this.w_DatIni, this.w_DatFin, "F") as PeriFine, ;
    Magazzino+Articolo as Dettaglio ;
    from MOVIART into cursor MOVIART
    if this.w_FLDETMOV="N"
      * --- Raggruppa i dati secondo il criterio selezionato
      ah_Msg("Raggruppamento dati...",.T.)
      select Magazzino, Articolo, MAX(Desart) as Desart, "M" as TipProv, MIN(MMDATREG) as MMDATREG, ;
      sum(Ordinato) as Ordinato,;
      sum(Impegnato) as Impegnato,;
      sum(Riservato) as Riservato,;
      " " as TDFLDTPR, MIN(MVDATEVA) as MVDATEVA, MIN(NUMDOC) AS NUMDOC, MIN(ALFDOC) AS ALFDOC, MIN(CAUMAG) AS CAUMAG, ;
      MIN(SLQTAPER) AS SLQTAPER, MIN(SLQTRPER) AS SLQTRPER, MIN(SLQTOPER) AS SLQTOPER, MIN(SLQTIPER) AS SLQTIPER, ;
      MAX(ARUNMIS1) AS ARUNMIS1, SUM(cprownum) as cprownum, " " AS MVTIPCON, SPACE(15) AS MVCODCON,; 
 SPACE(40) AS ANDESCRI, MIN(XXDATREG) AS XXDATREG, PERIINIZ, PERIFINE, ;
      MAX(Dettaglio) as Dettaglio ;
      from MOVIART ;
      group by Magazzino, Articolo, periiniz, perifine ;
      into cursor MOVIART
    endif
    * --- Impostazione del criterio di ordinamento
    if this.oParentObject.w_SelOrd = "A"
      * --- Per articolo
      w_Ordina = "1, 2,24,4,23,11,12"
    else
      * --- Per Descrizione
      w_Ordina = "1, 3, 2,24,4,23,11,12"
    endif
    select *, " " AS MVTIPCON, SPACE(15) AS MVCODCON, SPACE(40) AS ANDESCRI, cp_CharToDate("  -  -  ") AS XXDATREG, cp_CharToDate("  -  -  ") AS Periiniz, cp_CharToDate("  -  -  ") AS PeriFine, ;
    Magazzino+Articolo as Dettaglio FROM SALDIART UNION ALL SELECT * FROM MOVIART ;
    ORDER BY &w_ORDINA INTO CURSOR PROV
    SELECT MOVIART
    USE
    SELECT SALDIART
    USE
    CREATE CURSOR INVER (Magazzino C(5), Articolo C(20), ;
    Desart C(40), Tipprov C(1), Ordinato N(12,3), Impegnato N(12,3), ;
    Riservato N(12,3), Numdoc N(15), Alfdoc C(10), Caumag C(5), ;
    slqtaper N(12,3), slqtrper N(12,3), slqtoper N(12,3), slqtiper N(12,3), ;
    Arunmis1 C(3), CPROWNUM N(4), MVTIPCON C(1), MVCODCON C(15), ANDESCRI C(40), xxdatreg D(8), Periiniz D(8), ;
    Perifine D(8), Disponibil N(12,3), DispNeg C(1))
    SELECT PROV
    GO TOP
    * --- La Disponibilit� � inizializzata con il valore dell'esistenza - il valore del riservato (contenuti nella prima riga (quella proveniente dai Saldi))
    do while .not. eof()
      * --- Per ogni Articolo assegna alla Disponibilit� (w_ProgDisp) :
      * --- Esistenza + Totale righe Ordinato  - Totale Righe Impegnato;
      * --- se la Disponibilit� assume valori negativi allora w_DisNegat vale 'S'
      * --- questo valore sar� inserito nel cursore per poter valere 'S' per tutte le righe relative all'Articolo.
      if TipProv = "A"
        this.w_ProgDisp = slqtaper - slqtrper
        this.w_DisNegat = "N"
      else
        this.w_ProgDisp = this.w_ProgDisp + Ordinato - Impegnato
      endif
      do case
        case this.w_ProgDisp<0
          this.w_DisNegat = "S"
        case this.w_ProgDisp=0
          this.w_DisNegat = "Z"
      endcase
      INSERT INTO INVER (Magazzino, Articolo, Desart, TipProv, ;
      Ordinato , Impegnato, Riservato, Numdoc, Alfdoc, Caumag, ;
      slqtaper, slqtrper, slqtoper, slqtiper, Arunmis1, CPROWNUM, MVTIPCON, MVCODCON, ANDESCRI, xxdatreg, ;
      Periiniz, Perifine, Disponibil, DispNeg) VALUES ;
      (PROV.Magazzino , PROV.Articolo, PROV.Desart, PROV.TipProv, ;
      PROV.Ordinato, PROV.Impegnato, PROV.Riservato, PROV.Numdoc, PROV.Alfdoc, PROV.Caumag, ;
      PROV.slqtaper, PROV.slqtrper, PROV.slqtoper, PROV.slqtiper, PROV.Arunmis1, CPROWNUM, NVL(PROV.MVTIPCON," "), NVL(PROV.MVCODCON,SPACE(15)), NVL(PROV.ANDESCRI,SPACE(40)), ;
      PROV.xxdatreg, PROV.Periiniz, PROV.Perifine, this.w_ProgDisp, this.w_DisNegat)
      select PROV
      skip
    enddo
    SELECT PROV
    USE
    CREATE CURSOR __TMP__ (Magazzino C(5), Articolo C(20), ;
    Desart C(40), Tipprov C(1), Ordinato N(12,3), Impegnato N(12,3), ;
    Riservato N(12,3), Numdoc N(15), Alfdoc C(10), Caumag C(5), ;
    slqtaper N(12,3), slqtrper N(12,3), slqtoper N(12,3), slqtiper N(12,3), ;
    Arunmis1 C(3), CPROWNUM N(4), MVTIPCON C(1), MVCODCON C(15), ANDESCRI C(40), xxdatreg D(8), Periiniz D(8),; 
 Perifine D(8), Disponibil N(12,3), DispNeg C(1), Progress N(12))
    SELECT INVER
    GO BOTTOM
    this.w_Progress = 0
    this.w_Trovato = "N"
    this.w_InizArt = "S"
    do while .not. bof()
      * --- Per ogni Articolo assegna la Disponibilit� Attuale:
      * --- Esistenza + Totale righe Ordinato Scadute - Totale Righe Impegnato e Riservato Scadute;
      * --- l'eventuale valore 'S' di INVER.DispNeg si propaga per tutto l'articolo.
      this.w_Progress = this.w_Progress+1
      if TipProv="A"
        * --- E' la prima riga relativa all'articolo, la Disponibilit� Attuale viene
        * --- inserita nel campo disponibilit�
        if this.w_Trovato = "N"
          this.w_ProgDisp = Disponibil
        else
          this.w_Trovato = "N"
        endif
        * --- L'eventuale valore 'S' di INVER.DispNeg viene assegnato alla prima riga relativa all'articolo.
        if this.w_InizArt="S"
          this.w_DisNegat = INVER.DispNeg
        else
          this.w_InizArt = "S"
        endif
        INSERT INTO __TMP__ (Magazzino, Articolo, Desart, TipProv, ;
        Ordinato , Impegnato, Riservato, Numdoc, Alfdoc, Caumag, ;
        slqtaper, slqtrper, slqtoper, slqtiper, Arunmis1, CPROWNUM, MVTIPCON, MVCODCON, ANDESCRI, xxdatreg, ;
        Periiniz, Perifine, Disponibil, DispNeg, Progress) VALUES ;
        (INVER.Magazzino, INVER.Articolo, INVER.Desart, INVER.TipProv, ;
        INVER.Ordinato, INVER.Impegnato, INVER.Riservato, INVER.Numdoc, INVER.Alfdoc, INVER.Caumag, ;
        INVER.slqtaper, INVER.slqtrper, INVER.slqtoper, INVER.slqtiper, INVER.Arunmis1, INVER.CPROWNUM, ;
        INVER.MVTIPCON, INVER.MVCODCON, INVER.ANDESCRI, INVER.xxdatreg, INVER.Periiniz, this.w_DatFin, this.w_ProgDisp, this.w_DisNegat, this.w_Progress)
        * --- PeriFine prende il valore w_DatFin per valorizzare il primo elemento del "grafico";
        * --- nel caso di "stampa" questo valore non viene usato.
      else
        * --- Per le altre righe relative all'articolo la Disponibilit� rimane invariata
        if PeriFine< i_datsys and this.w_Trovato = "N"
          this.w_Trovato = "S"
          * --- Prendo come Disponibilit� Teorica quella della riga esaminata al ciclo precedente
          * --- Ovvero quella che non � scaduta con la data minore
          this.w_ProgDisp = Disponibil
        endif
        * --- L'eventuale valore 'S' di INVER.DispNeg viene assegnato a tutte le riga relative all'articolo.
        if this.w_InizArt="S"
          this.w_DisNegat = INVER.DispNeg
          this.w_InizArt = "N"
        endif
        INSERT INTO __TMP__ (Magazzino, Articolo, Desart, TipProv, ;
        Ordinato , Impegnato, Riservato, Numdoc, Alfdoc, Caumag, ;
        slqtaper, slqtrper, slqtoper, slqtiper, Arunmis1, CPROWNUM, MVTIPCON, MVCODCON, ANDESCRI, xxdatreg, ;
        Periiniz, Perifine, Disponibil, DispNeg, Progress) VALUES ;
        (INVER.Magazzino, INVER.Articolo, INVER.Desart, INVER.TipProv, ;
        INVER.Ordinato, INVER.Impegnato, INVER.Riservato, INVER.Numdoc, INVER.Alfdoc, INVER.Caumag, ;
        INVER.slqtaper, INVER.slqtrper, INVER.slqtoper, INVER.slqtiper, INVER.Arunmis1, INVER.CPROWNUM, ;
        INVER.MVTIPCON, INVER.MVCODCON, INVER.ANDESCRI, INVER.xxdatreg, INVER.Periiniz, INVER.Perifine, INVER.Disponibil, this.w_DisNegat, this.w_Progress)
      endif
      select INVER
      skip -1
    enddo
    select INVER
    USE
    * --- Riordinamento del cursore per
    *     articolo
    *     magazzino
    *     tipo riga
    *     data reg
    *     num doc
    *     alfa doc
    *     progressivo Desc  (perch� viene � stata eseguita una go bottom per la sua valorizzazione)
    if this.oParentObject.w_SelOrd = "A"
      * --- Per articolo
      w_Ordina = "1, 2,21,4,20,8,9,25 DESC "
    else
      * --- Per Descrizione
      w_Ordina = "1, 3, 2,21,4,20,8,9,25 DESC"
    endif
    select * from __TMP__ ORDER BY &w_ORDINA INTO CURSOR __TMP__
    * --- Gestione output
    do case
      case this.w_OutPut = "S"
        * --- Esecuzione report
        if this.w_FLDETMOV="N"
          cp_chprn("QUERY\GSOR_SDN.FRX", " ", this)
        else
          cp_chprn("QUERY\GSOR_SDG.FRX", " ", this)
        endif
      case this.w_OutPut = "G"
        * --- Esecuzione grafico
        * --- Considera solo le colonne necessarie
        select Perifine as Data, Disponibil from __TMP__ ;
        where TipProv = "A" or Perifine >= i_datsys ;
        order by Perifine;
        into cursor __grf__
        * --- Richiama la maschera con il grafico
        do GSOR_SDN with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursori
    if used("SALDIART")
      select SALDIART
      use
    endif
    if used("MOVIODL")
      select MOVIODL
      use
    endif
    if used("MOVIART")
      select MOVIART
      use
    endif
    if used("PROV")
      select PROV
      use
    endif
    if used("INVER")
      select INVER
      use
    endif
    if used("__tmp__")
      select __tmp__
      use
    endif
    if used("__grf__")
      select __grf__
      use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_Output)
    this.w_Output=w_Output
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Output"
endproc
