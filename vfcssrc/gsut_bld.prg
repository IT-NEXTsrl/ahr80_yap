* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bld                                                        *
*              GES.dispositivo installato                                      *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_101]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-05-19                                                      *
* Last revis.: 2017-11-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo,pCodPen,pCodCas,pCodNeg,pOffice,pPMerge,pStaArc,pScanner,pCodCen,pOfficeBit,pOfficeTypeConv,pAutoOffice
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bld",oParentObject,m.pTipo,m.pCodPen,m.pCodCas,m.pCodNeg,m.pOffice,m.pPMerge,m.pStaArc,m.pScanner,m.pCodCen,m.pOfficeBit,m.pOfficeTypeConv,m.pAutoOffice)
return(i_retval)

define class tgsut_bld as StdBatch
  * --- Local variables
  pTipo = space(2)
  pCodPen = space(5)
  pCodCas = space(5)
  pCodNeg = space(3)
  pOffice = space(1)
  pPMerge = space(1)
  pStaArc = space(40)
  pScanner = space(250)
  pCodCen = space(5)
  pOfficeBit = space(1)
  pOfficeTypeConv = space(1)
  pAutoOffice = 0
  w_FileCnf = space(250)
  w_Handle = space(12)
  w_MESS = space(10)
  w_NomeWorkstation = space(20)
  w_TIPO = space(1)
  w_NEGOZIO = .f.
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_NOERR = .f.
  w_RIMOZIONE = .f.
  w_NOERR = .f.
  w_RIMOZIONE = .f.
  w_TMPPEN = space(5)
  w_TMPCAS = space(5)
  w_TMPOFF = space(1)
  w_TMPPME = space(1)
  w_TEMPARCHI = space(40)
  w_TEMPSCANN = space(40)
  w_TMPCEN = space(5)
  w_TMPOFFBIT = space(1)
  w_TMPOFFCON = space(1)
  w_TMPAUTOFF = .f.
  * --- WorkFile variables
  DIS_INST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge / Scrive su un file posto nella cartella temporanea
    *     il codice dispositivo da utilizzare
    * --- pTipo : 'L' Lettura variabili , 'S' Scrittura variabili nel file AHR/E_DISP, 'E' cancellazione Dispositivo
    *                  Se a pTipo viene passata una delle lettere suindicate pi� 'N' (Es.: 'SN') indica che si sta scrivendo il codice negozio dalla maschera GSPS_KNM
    *     pCodPen: codice penna
    *     pCodCas: codice cassa
    *     pCodNeg: codice negozio
    *     pOffice: Suite Office
    *     pPMerge: Tipo Stampa Merge OpenOffice
    if TYPE ("pAutoOffice")="L"
      this.pAutoOffice = IIF (this.pAutoOffice,1,0)
    endif
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    * --- Dichiarazione API di windows che restituisce il nome della macchina.
    DECLARE INTEGER GetComputerName IN Win32API AS GetName STRING @CompName, INTEGER @DimCompName
    CompName = SPACE(20)
    DimCompName = 20
    * --- Prima lettara passata a pTipo (solo nel caso di lancio da maschera GSPS_KNM del Pos 
    *     vengono passate due lettere per aggiornamento variabile pubblica codice negozio)
    this.w_TIPO = Left(this.pTipo,1)
    if this.w_TIPO <>"L"
      this.pScanner = IIF(VARTYPE(this.pScanner)="C",ALLTRIM(this.pScanner),"")
    endif
    this.w_NEGOZIO = Len(this.pTipo)=2
    if TYPE("g_PhoneService")="U"
      public g_PhoneService 
 g_PhoneService = space(5)
    endif
    if type("g_CNFDISP")<>"U"
      * --- Lettura tabella database
      RetVal=GetName(@CompName, @DimCompName)
      this.w_NomeWorkstation = SPACE(20)
      if g_TERMINALSS
        * --- La variabile di ambiente CLIENTNAME contiene il nome della macchina 
        *     su cui sta girando TS
        this.w_NomeWorkstation = GETENV("CLIENTNAME")
      else
        this.w_NomeWorkstation = LEFT(CompName,DimCompName)
      endif
      if ! Empty(this.w_NomeWorkstation)
        do case
          case this.w_TIPO="L"
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.w_TIPO="S"
            * --- Ogni volta che scrivo elimino e ricreo il file...
            this.w_NOERR = .T.
            if this.w_NEGOZIO
              if g_APPLICATION="ADHOC REVOLUTION"
                * --- Scrivo il codice negozio
                * --- Try
                local bErr_048B3D08
                bErr_048B3D08=bTrsErr
                this.Try_048B3D08()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- Try
                  local bErr_048B4A58
                  bErr_048B4A58=bTrsErr
                  this.Try_048B4A58()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    this.w_NOERR = .F.
                  endif
                  bTrsErr=bTrsErr or bErr_048B4A58
                  * --- End
                endif
                bTrsErr=bTrsErr or bErr_048B3D08
                * --- End
                if not empty(this.pCodNeg)
                  this.w_oPart = this.w_oMess.AddMsgPartNL("Il negozio %1 � stato legato a questa postazione")
                  this.w_oPart.AddParam(this.pCodNeg)     
                else
                  this.w_oMess.AddMsgPartNL("Nessun negozio associato alla postazione%0Verr� quindi utilizzato quello predefinito")     
                endif
              endif
            else
              * --- Scrivo il codice dispositivo...
              * --- Try
              local bErr_048B4998
              bErr_048B4998=bTrsErr
              this.Try_048B4998()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- Try
                local bErr_048959A0
                bErr_048959A0=bTrsErr
                this.Try_048959A0()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  this.w_NOERR = .F.
                endif
                bTrsErr=bTrsErr or bErr_048959A0
                * --- End
              endif
              bTrsErr=bTrsErr or bErr_048B4998
              * --- End
              if UPPER(this.oParentObject.class)="TGSUT_KSD"
                if not empty(this.pCodPen)
                  this.w_oPart = this.w_oMess.AddMsgPartNL("Dispositivo %1 installato correttamente")
                  this.w_oPart.AddParam(this.pCodPen)     
                else
                  this.w_oMess.AddMsgPartNL("Dispositivo penna disinstallato")     
                endif
                if g_APPLICATION="ADHOC REVOLUTION"
                  if not empty(this.pCodCas)
                    this.w_oPart = this.w_oMess.AddMsgPartNL("Dispositivo %1 installato correttamente")
                    this.w_oPart.AddParam(this.pCodCas)     
                  else
                    this.w_oMess.AddMsgPartNL("Dispositivo registratore di cassa disinstallato")     
                  endif
                  if not empty(this.pScanner)
                    this.w_oPart = this.w_oMess.AddMsgPartNL("Dispositivo %1 installato correttamente")
                    this.w_oPart.AddParam(this.pScanner)     
                  else
                    this.w_oMess.AddMsgPartNL("Dispositivo acquisizione immagini disinstallato")     
                  endif
                  if not empty(this.pCODCEN)
                    this.w_oPart = this.w_oMess.AddMsgPartNL("Dispositivo %1 installato correttamente")
                    this.w_oPart.AddParam(this.pCODCEN)     
                  else
                    this.w_oMess.AddMsgPartNL("Dispositivo centralino disinstallato")     
                  endif
                endif
              endif
              if not empty(this.pOffice) AND ( UPPER(this.oParentObject.class)="TGSUT_KSD" Or UPPER(this.oParentObject.class)="TGSAR_KOF" )
                if this.pOffice="N"
                  this.w_oMess.AddMsgPartNL("Suite Office non installata")     
                else
                  if this.pOfficeBit="3"
                    if this.pOffice="M"
                      this.w_oMess.AddMsgPartNL("Suite Microsoft Office 32 bit installata correttamente")     
                    else
                      this.w_oMess.AddMsgPartNL("Suite OpenOffice 32 bit installata correttamente")     
                    endif
                  else
                    if this.pOffice="M"
                      this.w_oMess.AddMsgPartNL("Suite Microsoft Office 64 bit installata correttamente")     
                    else
                      this.w_oMess.AddMsgPartNL("Suite OpenOffice 64 bit installata correttamente")     
                    endif
                  endif
                endif
              endif
            endif
            if this.w_NOERR
              this.w_oMess.Ah_ErrorMsg("!")     
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.oParentObject.ecpQuit()
            endif
          case this.w_TIPO="E"
            this.w_RIMOZIONE = .F.
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if g_CODPEN=this.pCodPen
              this.w_RIMOZIONE = .T.
              this.w_NOERR = .T.
              * --- Write into DIS_INST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DIS_INST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIS_INST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_INST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DICODPEN ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DIS_INST','DICODPEN');
                    +i_ccchkf ;
                +" where ";
                    +"DIPCNAME = "+cp_ToStrODBC(this.w_NomeWorkstation);
                       )
              else
                update (i_cTable) set;
                    DICODPEN = SPACE(5);
                    &i_ccchkf. ;
                 where;
                    DIPCNAME = this.w_NomeWorkstation;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              this.w_oMess.AddMsgPartNL("Il dispositivo penna eliminato era attualmente installato, occorre ridefinire il dispositivo installato correntemente")     
            endif
            if g_CODCAS=this.pCodCas and this.pCodCas<>"     "
              this.w_RIMOZIONE = .T.
              this.w_NOERR = .T.
              * --- Write into DIS_INST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DIS_INST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIS_INST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_INST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DICODCAS ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DIS_INST','DICODCAS');
                    +i_ccchkf ;
                +" where ";
                    +"DIPCNAME = "+cp_ToStrODBC(this.w_NomeWorkstation);
                       )
              else
                update (i_cTable) set;
                    DICODCAS = SPACE(5);
                    &i_ccchkf. ;
                 where;
                    DIPCNAME = this.w_NomeWorkstation;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              if g_APPLICATION="ADHOC REVOLUTION"
                this.w_oMess.AddMsgPartNL("Il dispositivo registratore di cassa eliminato era attualmente installato, occorre ridefinire il dispositivo installato correntemente")     
              endif
            endif
            if g_PhoneService=this.pCodCen And g_AGFA="S"
              this.w_RIMOZIONE = .T.
              this.w_NOERR = .T.
              * --- Write into DIS_INST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DIS_INST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIS_INST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_INST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DICODCEN ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DIS_INST','DICODCEN');
                    +i_ccchkf ;
                +" where ";
                    +"DIPCNAME = "+cp_ToStrODBC(this.w_NomeWorkstation);
                       )
              else
                update (i_cTable) set;
                    DICODCEN = SPACE(5);
                    &i_ccchkf. ;
                 where;
                    DIPCNAME = this.w_NomeWorkstation;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              this.w_oMess.AddMsgPartNL("Il dispositivo centralino era attualmente installato, occorre ridefinire il dispositivo installato correntemente")     
            endif
            if this.w_NOERR
              if this.w_RIMOZIONE
                this.w_oMess.Ah_ErrorMsg("!")     
              endif
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
        endcase
      else
        ah_Msg("Impossibile risolvere nome computer",.T.)
        i_retcode = 'stop'
        return
      endif
    else
      * --- File CNF locale ad ogni macchina
      if g_APPLICATION="ad hoc ENTERPRISE"
        this.w_FileCnf = tempadhoc()+"\AHE_DISP.CNF"
      else
        this.w_FileCnf = tempadhoc()+"\AHR_DISP.CNF"
      endif
      do case
        case this.w_TIPO="L"
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.w_TIPO="S"
          * --- Ogni volta che scrivo elimino e ricreo il file...
          this.w_NOERR = .T.
          this.w_Handle = FCREATE(this.w_FileCnf,0)
          * --- Scrivo il codice dispositivo...
          if FPUTS( this.w_Handle , "Public g_CODPEN") = 0
            ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
            this.w_NOERR = .F.
          else
            if FPUTS( this.w_Handle, 'g_CODPEN="'+this.pCodPen+'"')= 0
              ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
              this.w_NOERR = .F.
            endif
            if Not(this.w_NEGOZIO) And UPPER(this.oParentObject.class)="TGSUT_KSD"
              * --- Non emetto le informazioni relative alla Penna se lanciato da associazione negozio/postazione
              if not empty(this.pCodPen)
                this.w_oPart = this.w_oMess.AddMsgPartNL("Dispositivo %1 installato correttamente")
                this.w_oPart.AddParam(this.pCodPen)     
              else
                this.w_oMess.AddMsgPartNL("Dispositivo penna disinstallato")     
              endif
            endif
          endif
          if FPUTS( this.w_Handle , "Public g_CODCAS") = 0
            ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
            this.w_NOERR = .F.
          else
            if FPUTS( this.w_Handle, 'g_CODCAS="'+this.pCodCas+'"')= 0
              ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
              this.w_NOERR = .F.
            endif
            if g_APPLICATION="ADHOC REVOLUTION"and Not(this.w_NEGOZIO) And UPPER(this.oParentObject.class)="TGSUT_KSD"
              * --- Non emetto le informazioni relative alla Penna se lanciato da associazione negozio/postazione
              if not empty(this.pCodCas)
                this.w_oPart = this.w_oMess.AddMsgPartNL("Dispositivo %1 installato correttamente")
                this.w_oPart.AddParam(this.pCodCas)     
              else
                this.w_oMess.AddMsgPartNL("Dispositivo registratore di cassa disinstallato")     
              endif
            endif
          endif
          if FPUTS( this.w_Handle , "Public g_OFFICE") = 0
            ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
            this.w_NOERR = .F.
          else
            if FPUTS( this.w_Handle, 'g_OFFICE="'+this.pOffice+'"')= 0
              ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
              this.w_NOERR = .F.
            endif
            if not empty(this.pOffice) AND ( UPPER(this.oParentObject.class)="TGSUT_KSD" Or UPPER(this.oParentObject.class)="TGSAR_KOF" )
              if this.pOffice="N"
                this.w_oMess.AddMsgPartNL("Suite Office non installata")     
              else
                if this.pOfficeBit="3"
                  if this.pOffice="M"
                    this.w_oMess.AddMsgPartNL("Suite Microsoft Office 32 bit installata correttamente")     
                  else
                    this.w_oMess.AddMsgPartNL("Suite OpenOffice 32 bit installata correttamente")     
                  endif
                else
                  if this.pOffice="M"
                    this.w_oMess.AddMsgPartNL("Suite Microsoft Office 64 bit installata correttamente")     
                  else
                    this.w_oMess.AddMsgPartNL("Suite OpenOffice 64 bit installata correttamente")     
                  endif
                endif
              endif
              if !EMPTY(this.pOfficeTypeConv) 
                do case
                  case this.pOfficeTypeConv="N"
                    this.w_oMess.AddMsgPartNL("Conversione caratteri disabilitata")     
                  case this.pOfficeTypeConv="S"
                    this.w_oMess.AddMsgPartNL("Conversione caratteri accentati con non accentati attivata")     
                  case this.pOfficeTypeConv="C"
                    this.w_oMess.AddMsgPartNL("Conversione caratteri accentati con non accentati e apostrofo attivata")     
                endcase
              endif
              if FPUTS( this.w_Handle , "Public g_OFFICE_BIT") = 0
                ah_errormsg("Errore scrittura 1 file di configurazione")
                this.w_NOERR = .F.
              else
                if FPUTS( this.w_Handle, 'g_OFFICE_BIT="'+this.pOfficeBit+'"')= 0
                  ah_errormsg("Errore scrittura 2 file di configurazione")
                  this.w_NOERR = .F.
                endif
              endif
              if FPUTS( this.w_Handle , "Public g_OFFICE_TYPE_CONV") = 0
                ah_errormsg("Errore scrittura 1 file di configurazione")
                this.w_NOERR = .F.
              else
                if FPUTS( this.w_Handle, 'g_OFFICE_TYPE_CONV="'+this.pOfficeTypeConv+'"')= 0
                  ah_errormsg("Errore scrittura 2 file di configurazione")
                  this.w_NOERR = .F.
                endif
              endif
            endif
          endif
          if FPUTS( this.w_Handle , "Public g_AUTOFFICE") = 0
            ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
            this.w_NOERR = .F.
          else
            if FPUTS( this.w_Handle, "g_AUTOFFICE= "+Alltrim(Tran( this.pAutoOffice)))= 0
              ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
              this.w_NOERR = .F.
            endif
          endif
          if FPUTS( this.w_Handle , "Public g_PRINTMERGE") = 0
            ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
            this.w_NOERR = .F.
          else
            if FPUTS( this.w_Handle, 'g_PRINTMERGE="'+this.pPMerge+'"')= 0
              ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
              this.w_NOERR = .F.
            endif
          endif
          if FPUTS( this.w_Handle , "Public g_PRINTERARCHI") = 0
            ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
            this.w_NOERR = .F.
          else
            if FPUTS( this.w_Handle, 'g_PRINTERARCHI="'+this.pStaArc+'"')= 0
              ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
              this.w_NOERR = .F.
            endif
          endif
          if FPUTS( this.w_Handle , "Public g_SCANNER") = 0
            ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
            this.w_NOERR = .F.
          else
            if FPUTS( this.w_Handle, 'g_SCANNER="'+Alltrim(this.pScanner)+'"')= 0
              ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
              this.w_NOERR = .F.
            endif
            if UPPER(this.oParentObject.class)="TGSUT_KSD" And g_APPLICATION="ADHOC REVOLUTION"and Not(this.w_NEGOZIO)
              if Not Empty(this.pScanner)
                this.w_oPart = this.w_oMess.AddMsgPartNL("Dispositivo %1 installato correttamente")
                this.w_oPart.AddParam(Alltrim(this.pScanner))     
              else
                this.w_oMess.AddMsgPartNL("Dispositivo di acquisizione immagini disinstallato")     
              endif
            endif
          endif
          if vartype (this.pCodCen)="C"
            if FPUTS( this.w_Handle , "Public g_PhoneService") = 0
              ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
              this.w_NOERR = .F.
            else
              if FPUTS( this.w_Handle, 'g_PhoneService="'+Alltrim(this.pCodCen)+'"')= 0
                ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
                this.w_NOERR = .F.
              endif
              if UPPER(this.oParentObject.class)="TGSUT_KSD" And g_APPLICATION="ADHOC REVOLUTION"and Not(this.w_NEGOZIO)
                if Not Empty(this.pCodCen)
                  this.w_oPart = this.w_oMess.AddMsgPartNL("Dispositivo %1 installato correttamente")
                  this.w_oPart.AddParam(Alltrim(this.pCodCen))     
                else
                  this.w_oMess.AddMsgPartNL("Dispositivo centralino disinstallato")     
                endif
              endif
            endif
          endif
          if g_APPLICATION="ADHOC REVOLUTION"
            if Not Empty(this.pCodNeg)
              * --- Se impostato un codice negozio scrivo nel file AHR_DISP il codice negozio
              if FPUTS( this.w_Handle , "Public g_CODNEG") = 0
                ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
                this.w_NOERR = .F.
              else
                if FPUTS( this.w_Handle, 'g_CODNEG="'+this.pCodNeg+'"')= 0
                  ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
                  this.w_NOERR = .F.
                endif
                if this.w_NEGOZIO and this.w_NOERR
                  * --- Emetto le informazioni relative al negozio solo se lanciato dalla maschera GSPS_KNM
                  this.w_oPart = this.w_oMess.AddMsgPartNL("Il negozio %1 � stato legato a questa postazione")
                  this.w_oPart.AddParam(this.pCodNeg)     
                endif
              endif
            else
              GSUTABLD(this,"1")
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_oMess.AddMsgPartNL("Nessun negozio associato alla postazione%0Verr� quindi utilizzato quello predefinito")     
            endif
          endif
          FClose( this.w_Handle )
          * --- Se lanciato da GSAR_AOF non visualizzo alcun messaggio.
          if this.w_NOERR and Upper( This.OparentObject.Class )<>"TCGSAR_AOF"
            this.w_oMess.Ah_ErrorMsg("!")     
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.ecpQuit()
          endif
        case this.w_TIPO="E"
          this.w_RIMOZIONE = .F.
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if g_CODPEN=this.pCodPen
            this.w_RIMOZIONE = .T.
            this.w_NOERR = .T.
            this.w_Handle = FCREATE(this.w_FileCnf,0)
            * --- Scrivo il codice dispositivo...
            if FPUTS( this.w_Handle , "Public g_CODPEN") = 0
              ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
              this.w_NOERR = .F.
            else
              if FPUTS( this.w_Handle, 'g_CODPEN="     "')= 0
                ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
                this.w_NOERR = .F.
              endif
              this.w_oMess.AddMsgPartNL("Il dispositivo penna eliminato era attualmente installato, occorre ridefinire il dispositivo installato correntemente")     
            endif
          else
            this.w_NOERR = .T.
            this.w_Handle = FCREATE(this.w_FileCnf,0)
            * --- Scrivo il codice dispositivo...
            if FPUTS( this.w_Handle , "Public g_CODPEN") = 0
              ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
              this.w_NOERR = .F.
            else
              if FPUTS( this.w_Handle, 'g_CODPEN="'+g_CODPEN+'"')= 0
                ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
                this.w_NOERR = .F.
              endif
              if not empty(this.pCodPen)
                this.w_oPart = this.w_oMess.AddMsgPartNL("Dispositivo %1 installato correttamente")
                this.w_oPart.AddParam(this.pCodPen)     
              else
                this.w_oMess.AddMsgPartNL("Dispositivo penna disinstallato")     
              endif
            endif
          endif
          if g_CODCAS=this.pCodCas and this.pCodCas<>"     "
            this.w_RIMOZIONE = .T.
            if FPUTS( this.w_Handle , "Public g_CODCAS") = 0
              ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
              this.w_NOERR = .F.
            else
              if FPUTS( this.w_Handle, 'g_CODCAS="     "')= 0
                ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
                this.w_NOERR = .F.
              endif
              if g_APPLICATION="ADHOC REVOLUTION"
                this.w_oMess.AddMsgPartNL("Il dispositivo registratore di cassa eliminato era attualmente installato, occorre ridefinire il dispositivo installato correntemente")     
              endif
            endif
          else
            if FPUTS( this.w_Handle , "Public g_CODCAS") = 0
              ah_ErrorMsg("Errore scrittura 1 file di configurazione",,"")
              this.w_NOERR = .F.
            else
              if FPUTS( this.w_Handle, 'g_CODCAS="'+g_CODCAS+'"')= 0
                ah_ErrorMsg("Errore scrittura 2 file di configurazione",,"")
                this.w_NOERR = .F.
              endif
              if g_APPLICATION="ADHOC REVOLUTION"
                if not empty(this.pCodCas)
                  this.w_oPart = this.w_oMess.AddMsgPartNL("Dispositivo %1 installato correttamente")
                  this.w_oPart.AddParam(this.pCodCas)     
                else
                  this.w_oMess.AddMsgPartNL("Dispositivo registratore di cassa disinstallato")     
                endif
              endif
            endif
          endif
          FClose( this.w_Handle )
          if this.w_NOERR
            if this.w_RIMOZIONE
              this.w_oMess.Ah_ErrorMsg("!")     
            endif
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
      endcase
    endif
  endproc
  proc Try_048B3D08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DIS_INST
    i_nConn=i_TableProp[this.DIS_INST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_INST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DIS_INST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DIPCNAME"+",DICODNEG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_NomeWorkstation),'DIS_INST','DIPCNAME');
      +","+cp_NullLink(cp_ToStrODBC(this.pCODNEG),'DIS_INST','DICODNEG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DIPCNAME',this.w_NomeWorkstation,'DICODNEG',this.pCODNEG)
      insert into (i_cTable) (DIPCNAME,DICODNEG &i_ccchkf. );
         values (;
           this.w_NomeWorkstation;
           ,this.pCODNEG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_048B4A58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DIS_INST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DIS_INST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_INST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_INST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DICODNEG ="+cp_NullLink(cp_ToStrODBC(this.pCODNEG),'DIS_INST','DICODNEG');
          +i_ccchkf ;
      +" where ";
          +"DIPCNAME = "+cp_ToStrODBC(this.w_NomeWorkstation);
             )
    else
      update (i_cTable) set;
          DICODNEG = this.pCODNEG;
          &i_ccchkf. ;
       where;
          DIPCNAME = this.w_NomeWorkstation;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_048B4998()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DIS_INST
    i_nConn=i_TableProp[this.DIS_INST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_INST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DIS_INST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DIPCNAME"+",DICODPEN"+",DICODCAS"+",DIOFFICE"+",DIPRNMRG"+",DISTAARC"+",DI__SCAN"+",DICODCEN"+",DIOFFBIT"+",DIOFFCON"+",DIAUTOFF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_NomeWorkstation),'DIS_INST','DIPCNAME');
      +","+cp_NullLink(cp_ToStrODBC(this.pCODPEN),'DIS_INST','DICODPEN');
      +","+cp_NullLink(cp_ToStrODBC(this.pCODCAS),'DIS_INST','DICODCAS');
      +","+cp_NullLink(cp_ToStrODBC(this.pOFFICE),'DIS_INST','DIOFFICE');
      +","+cp_NullLink(cp_ToStrODBC(this.pPMerge),'DIS_INST','DIPRNMRG');
      +","+cp_NullLink(cp_ToStrODBC(this.pStaArc),'DIS_INST','DISTAARC');
      +","+cp_NullLink(cp_ToStrODBC(this.pScanner),'DIS_INST','DI__SCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.pCODCEN),'DIS_INST','DICODCEN');
      +","+cp_NullLink(cp_ToStrODBC(this.pOfficeBit),'DIS_INST','DIOFFBIT');
      +","+cp_NullLink(cp_ToStrODBC(this.pOfficeTypeConv),'DIS_INST','DIOFFCON');
      +","+cp_NullLink(cp_ToStrODBC(this.pAutoOffice),'DIS_INST','DIAUTOFF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DIPCNAME',this.w_NomeWorkstation,'DICODPEN',this.pCODPEN,'DICODCAS',this.pCODCAS,'DIOFFICE',this.pOFFICE,'DIPRNMRG',this.pPMerge,'DISTAARC',this.pStaArc,'DI__SCAN',this.pScanner,'DICODCEN',this.pCODCEN,'DIOFFBIT',this.pOfficeBit,'DIOFFCON',this.pOfficeTypeConv,'DIAUTOFF',this.pAutoOffice)
      insert into (i_cTable) (DIPCNAME,DICODPEN,DICODCAS,DIOFFICE,DIPRNMRG,DISTAARC,DI__SCAN,DICODCEN,DIOFFBIT,DIOFFCON,DIAUTOFF &i_ccchkf. );
         values (;
           this.w_NomeWorkstation;
           ,this.pCODPEN;
           ,this.pCODCAS;
           ,this.pOFFICE;
           ,this.pPMerge;
           ,this.pStaArc;
           ,this.pScanner;
           ,this.pCODCEN;
           ,this.pOfficeBit;
           ,this.pOfficeTypeConv;
           ,this.pAutoOffice;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_048959A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DIS_INST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DIS_INST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_INST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_INST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DICODPEN ="+cp_NullLink(cp_ToStrODBC(this.pCODPEN),'DIS_INST','DICODPEN');
      +",DICODCAS ="+cp_NullLink(cp_ToStrODBC(this.pCODCAS),'DIS_INST','DICODCAS');
      +",DIOFFICE ="+cp_NullLink(cp_ToStrODBC(this.pOFFICE),'DIS_INST','DIOFFICE');
      +",DIPRNMRG ="+cp_NullLink(cp_ToStrODBC(this.pPMerge),'DIS_INST','DIPRNMRG');
      +",DISTAARC ="+cp_NullLink(cp_ToStrODBC(this.pStaArc),'DIS_INST','DISTAARC');
      +",DI__SCAN ="+cp_NullLink(cp_ToStrODBC(this.pScanner),'DIS_INST','DI__SCAN');
      +",DICODCEN ="+cp_NullLink(cp_ToStrODBC(this.pCODCEN),'DIS_INST','DICODCEN');
      +",DIOFFBIT ="+cp_NullLink(cp_ToStrODBC(this.pOfficeBit),'DIS_INST','DIOFFBIT');
      +",DIOFFCON ="+cp_NullLink(cp_ToStrODBC(this.pOfficeTypeConv),'DIS_INST','DIOFFCON');
      +",DIAUTOFF ="+cp_NullLink(cp_ToStrODBC(this.pAutoOffice),'DIS_INST','DIAUTOFF');
          +i_ccchkf ;
      +" where ";
          +"DIPCNAME = "+cp_ToStrODBC(this.w_NomeWorkstation);
             )
    else
      update (i_cTable) set;
          DICODPEN = this.pCODPEN;
          ,DICODCAS = this.pCODCAS;
          ,DIOFFICE = this.pOFFICE;
          ,DIPRNMRG = this.pPMerge;
          ,DISTAARC = this.pStaArc;
          ,DI__SCAN = this.pScanner;
          ,DICODCEN = this.pCODCEN;
          ,DIOFFBIT = this.pOfficeBit;
          ,DIOFFCON = this.pOfficeTypeConv;
          ,DIAUTOFF = this.pAutoOffice;
          &i_ccchkf. ;
       where;
          DIPCNAME = this.w_NomeWorkstation;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Leggo il contenuto
    this.w_Handle = fopen( this.w_FileCnf )
    do while !feof(this.w_Handle)
      * --- Valorizzo le variabili pubbliche opportune
       
 L_Read=fgets( this.w_Handle ) 
 &L_Read
    enddo
    FClose( this.w_HANDLE )
    if this.w_NEGOZIO
      if Not Empty(g_CODNEG)
        * --- Imposto le variabili relative al Flag codifica numerica e alla maschera per il codice cliente
        GSUTABLD(this,"2")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        GSUTABLD(this,"3")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if TYPE ("g_AUTOFFICE")="N"
      g_AUTOFFICE=IIF(g_AUTOFFICE=1, .T. , .F. ) 
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura variabili pubbliche relative a codice penna Ottica, codice cassa e codice negozio
    if this.w_NEGOZIO
      * --- Lettura codice negozio dalla tabella DIS_INST
      * --- Read from DIS_INST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIS_INST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIS_INST_idx,2],.t.,this.DIS_INST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DICODNEG"+;
          " from "+i_cTable+" DIS_INST where ";
              +"DIPCNAME = "+cp_ToStrODBC(this.w_NomeWorkstation);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DICODNEG;
          from (i_cTable) where;
              DIPCNAME = this.w_NomeWorkstation;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        g_CODNEG = NVL(cp_ToDate(_read_.DICODNEG),cp_NullValue(_read_.DICODNEG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if !Empty(g_CODNEG)
        * --- Se esiste il codice negozio associato alla macchina leggo anche il flag codifica numerica cliente
        *     e la maschera relativa al codice cliente........
        GSUTABLD(this,"4")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- ......altrimenti definisco come codice negozio quello con il flag Negozio Predefinito attivato
        GSUTABLD(this,"5")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    Public g_CODPEN
    Public g_CODCAS
    Public g_SCANNER
    Public g_PhoneService
    Public g_AUTOFFICE
    * --- Try
    local bErr_04843FD0
    bErr_04843FD0=bTrsErr
    this.Try_04843FD0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_TMPPEN = SPACE(5)
      this.w_TMPCAS = SPACE(5)
      this.w_TMPOFF = SPACE(1)
      this.w_TMPPME = "2"
      this.w_TEMPARCHI = SPACE(5)
      this.w_TEMPSCANN = ""
      this.w_TMPCEN = SPACE(5)
      this.w_TMPOFFBIT = "3"
      this.w_TMPOFFCON = "N"
      this.w_TMPAUTOFF = .T.
    endif
    bTrsErr=bTrsErr or bErr_04843FD0
    * --- End
    g_CODPEN=this.w_TMPPEN
    g_CODCAS=this.w_TMPCAS
    g_OFFICE=this.w_TMPOFF
    g_OFFICE_BIT = this.w_TMPOFFBIT
    g_OFFICE_TYPE_CONV = this.w_TMPOFFCON
    g_PRINTMERGE=this.w_TMPPME
    g_PRINTERARCHI=this.w_TEMPARCHI
    g_SCANNER = this.w_TEMPSCANN
    g_PhoneService=this.w_TMPCEN
    g_AUTOFFICE=IIF (this.w_TMPAUTOFF=1 , .T. , .F.)
  endproc
  proc Try_04843FD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Read from DIS_INST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIS_INST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_INST_idx,2],.t.,this.DIS_INST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DICODPEN,DICODCAS,DIOFFICE,DIPRNMRG,DISTAARC,DI__SCAN,DICODCEN,DIOFFBIT,DIOFFCON,DIAUTOFF"+;
        " from "+i_cTable+" DIS_INST where ";
            +"DIPCNAME = "+cp_ToStrODBC(this.w_NomeWorkstation);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DICODPEN,DICODCAS,DIOFFICE,DIPRNMRG,DISTAARC,DI__SCAN,DICODCEN,DIOFFBIT,DIOFFCON,DIAUTOFF;
        from (i_cTable) where;
            DIPCNAME = this.w_NomeWorkstation;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TMPPEN = NVL(cp_ToDate(_read_.DICODPEN),cp_NullValue(_read_.DICODPEN))
      this.w_TMPCAS = NVL(cp_ToDate(_read_.DICODCAS),cp_NullValue(_read_.DICODCAS))
      this.w_TMPOFF = NVL(cp_ToDate(_read_.DIOFFICE),cp_NullValue(_read_.DIOFFICE))
      this.w_TMPPME = NVL(cp_ToDate(_read_.DIPRNMRG),cp_NullValue(_read_.DIPRNMRG))
      this.w_TEMPARCHI = NVL(cp_ToDate(_read_.DISTAARC),cp_NullValue(_read_.DISTAARC))
      this.w_TEMPSCANN = NVL(cp_ToDate(_read_.DI__SCAN),cp_NullValue(_read_.DI__SCAN))
      this.w_TMPCEN = NVL(cp_ToDate(_read_.DICODCEN),cp_NullValue(_read_.DICODCEN))
      this.w_TMPOFFBIT = NVL(cp_ToDate(_read_.DIOFFBIT),cp_NullValue(_read_.DIOFFBIT))
      this.w_TMPOFFCON = NVL(cp_ToDate(_read_.DIOFFCON),cp_NullValue(_read_.DIOFFCON))
      this.w_TMPAUTOFF = NVL(cp_ToDate(_read_.DIAUTOFF),cp_NullValue(_read_.DIAUTOFF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo,pCodPen,pCodCas,pCodNeg,pOffice,pPMerge,pStaArc,pScanner,pCodCen,pOfficeBit,pOfficeTypeConv,pAutoOffice)
    this.pTipo=pTipo
    this.pCodPen=pCodPen
    this.pCodCas=pCodCas
    this.pCodNeg=pCodNeg
    this.pOffice=pOffice
    this.pPMerge=pPMerge
    this.pStaArc=pStaArc
    this.pScanner=pScanner
    this.pCodCen=pCodCen
    this.pOfficeBit=pOfficeBit
    this.pOfficeTypeConv=pOfficeTypeConv
    this.pAutoOffice=pAutoOffice
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DIS_INST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo,pCodPen,pCodCas,pCodNeg,pOffice,pPMerge,pStaArc,pScanner,pCodCen,pOfficeBit,pOfficeTypeConv,pAutoOffice"
endproc
