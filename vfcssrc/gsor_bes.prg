* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_bes                                                        *
*              Esist. nella disp. nel tempo                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_203]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-21                                                      *
* Last revis.: 2000-06-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsor_bes",oParentObject)
return(i_retval)

define class tgsor_bes as StdBatch
  * --- Local variables
  * --- WorkFile variables
  SALDIART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializzazione dell'esistenza nella Disponibilita' Nel Tempo quando varia l'articolo o il magazzino da GSOR_SZD
    * --- Variabili della maschera
    * --- Read from SALDIART
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SLQTAPER,SLQTRPER"+;
        " from "+i_cTable+" SALDIART where ";
            +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODART);
            +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SLQTAPER,SLQTRPER;
        from (i_cTable) where;
            SLCODICE = this.oParentObject.w_CODART;
            and SLCODMAG = this.oParentObject.w_CODMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_SLQTAPER = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
      this.oParentObject.w_SLQTRPER = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SALDIART'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
