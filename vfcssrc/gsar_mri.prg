* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mri                                                        *
*              Saldi registri IVA                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_55]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-20                                                      *
* Last revis.: 2011-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_mri"))

* --- Class definition
define class tgsar_mri as StdTrsForm
  Top    = 7
  Left   = 3

  * --- Standard Properties
  Width  = 810
  Height = 296+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-01"
  HelpContextID=154568855
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  PRI_MAST_IDX = 0
  PRI_DETT_IDX = 0
  VOCIIVA_IDX = 0
  VALUTE_IDX = 0
  cFile = "PRI_MAST"
  cFileDetail = "PRI_DETT"
  cKeySelect = "TR__ANNO,TRNUMPER,TRTIPREG,TRNUMREG"
  cKeyWhere  = "TR__ANNO=this.w_TR__ANNO and TRNUMPER=this.w_TRNUMPER and TRTIPREG=this.w_TRTIPREG and TRNUMREG=this.w_TRNUMREG"
  cKeyDetail  = "TR__ANNO=this.w_TR__ANNO and TRNUMPER=this.w_TRNUMPER and TRTIPREG=this.w_TRTIPREG and TRNUMREG=this.w_TRNUMREG and TRCODIVA=this.w_TRCODIVA"
  cKeyWhereODBC = '"TR__ANNO="+cp_ToStrODBC(this.w_TR__ANNO)';
      +'+" and TRNUMPER="+cp_ToStrODBC(this.w_TRNUMPER)';
      +'+" and TRTIPREG="+cp_ToStrODBC(this.w_TRTIPREG)';
      +'+" and TRNUMREG="+cp_ToStrODBC(this.w_TRNUMREG)';

  cKeyDetailWhereODBC = '"TR__ANNO="+cp_ToStrODBC(this.w_TR__ANNO)';
      +'+" and TRNUMPER="+cp_ToStrODBC(this.w_TRNUMPER)';
      +'+" and TRTIPREG="+cp_ToStrODBC(this.w_TRTIPREG)';
      +'+" and TRNUMREG="+cp_ToStrODBC(this.w_TRNUMREG)';
      +'+" and TRCODIVA="+cp_ToStrODBC(this.w_TRCODIVA)';

  cKeyWhereODBCqualified = '"PRI_DETT.TR__ANNO="+cp_ToStrODBC(this.w_TR__ANNO)';
      +'+" and PRI_DETT.TRNUMPER="+cp_ToStrODBC(this.w_TRNUMPER)';
      +'+" and PRI_DETT.TRTIPREG="+cp_ToStrODBC(this.w_TRTIPREG)';
      +'+" and PRI_DETT.TRNUMREG="+cp_ToStrODBC(this.w_TRNUMREG)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mri"
  cComment = "Saldi registri IVA"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TR__ANNO = space(4)
  w_TRNUMPER = 0
  w_TRTIPREG = space(1)
  w_TRNUMREG = 0
  w_TRFLSTAM = space(1)
  w_TRFLSTAR = space(1)
  w_TRCODVAL = space(3)
  w_TRIMPVEN = 0
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTCV = 0
  w_UTDV = ctot('')
  w_TRCODIVA = space(5)
  w_DESIVA = space(35)
  w_TRIMPSTA = 0
  w_TRIVASTA = 0
  w_TRIVISTA = 0
  w_TRMACSTA = 0
  w_TRIMPPRE = 0
  w_TRIVAPRE = 0
  w_TRIVIPRE = 0
  w_TRMACPRE = 0
  w_TRIMPSEG = 0
  w_TRIVASEG = 0
  w_TRIVISEG = 0
  w_TRMACSEG = 0
  w_TRIMPFAD = 0
  w_TRIVAFAD = 0
  w_TRIVIFAD = 0
  w_TRMACFAD = 0
  w_TRIMPIND = 0
  w_TRIVAIND = 0
  w_TRIVIIND = 0
  w_OBTEST = ctod('  /  /  ')
  w_TRMACIND = 0
  w_TOTIMP = 0
  w_TOTIVA = 0
  w_TOTIVI = 0
  w_TOTMAC = 0
  w_DECTOT = 0
  o_DECTOT = 0
  w_CALCPIC = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PRI_MAST','gsar_mri')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mriPag1","gsar_mri",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Saldo")
      .Pages(1).HelpContextID = 9547558
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTR__ANNO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='PRI_MAST'
    this.cWorkTables[4]='PRI_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRI_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRI_MAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_TR__ANNO = NVL(TR__ANNO,space(4))
      .w_TRNUMPER = NVL(TRNUMPER,0)
      .w_TRTIPREG = NVL(TRTIPREG,space(1))
      .w_TRNUMREG = NVL(TRNUMREG,0)
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_17_joined
    link_1_17_joined=.f.
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from PRI_MAST where TR__ANNO=KeySet.TR__ANNO
    *                            and TRNUMPER=KeySet.TRNUMPER
    *                            and TRTIPREG=KeySet.TRTIPREG
    *                            and TRNUMREG=KeySet.TRNUMREG
    *
    i_nConn = i_TableProp[this.PRI_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRI_MAST_IDX,2],this.bLoadRecFilter,this.PRI_MAST_IDX,"gsar_mri")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRI_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRI_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"PRI_DETT.","PRI_MAST.")
      i_cTable = i_cTable+' PRI_MAST '
      link_1_17_joined=this.AddJoinedLink_1_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TR__ANNO',this.w_TR__ANNO  ,'TRNUMPER',this.w_TRNUMPER  ,'TRTIPREG',this.w_TRTIPREG  ,'TRNUMREG',this.w_TRNUMREG  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_DECTOT = 0
        .w_TR__ANNO = NVL(TR__ANNO,space(4))
        .w_TRNUMPER = NVL(TRNUMPER,0)
        .w_TRTIPREG = NVL(TRTIPREG,space(1))
        .w_TRNUMREG = NVL(TRNUMREG,0)
        .w_TRFLSTAM = NVL(TRFLSTAM,space(1))
        .w_TRFLSTAR = NVL(TRFLSTAR,space(1))
        .w_TRCODVAL = NVL(TRCODVAL,space(3))
          if link_1_17_joined
            this.w_TRCODVAL = NVL(VACODVAL117,NVL(this.w_TRCODVAL,space(3)))
            this.w_DECTOT = NVL(VADECTOT117,0)
          else
          .link_1_17('Load')
          endif
        .w_TRIMPVEN = NVL(TRIMPVEN,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PRI_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from PRI_DETT where TR__ANNO=KeySet.TR__ANNO
      *                            and TRNUMPER=KeySet.TRNUMPER
      *                            and TRTIPREG=KeySet.TRTIPREG
      *                            and TRNUMREG=KeySet.TRNUMREG
      *                            and TRCODIVA=KeySet.TRCODIVA
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.PRI_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRI_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('PRI_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "PRI_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" PRI_DETT"
        link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'TR__ANNO',this.w_TR__ANNO  ,'TRNUMPER',this.w_TRNUMPER  ,'TRTIPREG',this.w_TRTIPREG  ,'TRNUMREG',this.w_TRNUMREG  )
        select * from (i_cTable) PRI_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_DESIVA = space(35)
        .w_OBTEST = i_INIDAT
          .w_TRCODIVA = NVL(TRCODIVA,space(5))
          if link_2_1_joined
            this.w_TRCODIVA = NVL(IVCODIVA201,NVL(this.w_TRCODIVA,space(5)))
            this.w_DESIVA = NVL(IVDESIVA201,space(35))
          else
          .link_2_1('Load')
          endif
          .w_TRIMPSTA = NVL(TRIMPSTA,0)
          .w_TRIVASTA = NVL(TRIVASTA,0)
          .w_TRIVISTA = NVL(TRIVISTA,0)
          .w_TRMACSTA = NVL(TRMACSTA,0)
          .w_TRIMPPRE = NVL(TRIMPPRE,0)
          .w_TRIVAPRE = NVL(TRIVAPRE,0)
          .w_TRIVIPRE = NVL(TRIVIPRE,0)
          .w_TRMACPRE = NVL(TRMACPRE,0)
          .w_TRIMPSEG = NVL(TRIMPSEG,0)
          .w_TRIVASEG = NVL(TRIVASEG,0)
          .w_TRIVISEG = NVL(TRIVISEG,0)
          .w_TRMACSEG = NVL(TRMACSEG,0)
          .w_TRIMPFAD = NVL(TRIMPFAD,0)
          .w_TRIVAFAD = NVL(TRIVAFAD,0)
          .w_TRIVIFAD = NVL(TRIVIFAD,0)
          .w_TRMACFAD = NVL(TRMACFAD,0)
          .w_TRIMPIND = NVL(TRIMPIND,0)
          .w_TRIVAIND = NVL(TRIVAIND,0)
          .w_TRIVIIND = NVL(TRIVIIND,0)
          .w_TRMACIND = NVL(TRMACIND,0)
        .w_TOTIMP = (.w_TRIMPSTA+.w_TRIMPSEG+.w_TRIMPIND)-(.w_TRIMPPRE+.w_TRIMPFAD)
        .w_TOTIVA = (.w_TRIVASTA+.w_TRIVASEG+.w_TRIVAIND)-(.w_TRIVAPRE+.w_TRIVAFAD)
        .w_TOTIVI = (.w_TRIVISTA+.w_TRIVISEG+.w_TRIVIIND)-(.w_TRIVIPRE+.w_TRIVIFAD)
        .w_TOTMAC = (.w_TRMACSTA+.w_TRMACSEG+.w_TRMACIND)-(.w_TRMACPRE+.w_TRMACFAD)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace TRCODIVA with .w_TRCODIVA
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_TR__ANNO=space(4)
      .w_TRNUMPER=0
      .w_TRTIPREG=space(1)
      .w_TRNUMREG=0
      .w_TRFLSTAM=space(1)
      .w_TRFLSTAR=space(1)
      .w_TRCODVAL=space(3)
      .w_TRIMPVEN=0
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTCV=0
      .w_UTDV=ctot("")
      .w_TRCODIVA=space(5)
      .w_DESIVA=space(35)
      .w_TRIMPSTA=0
      .w_TRIVASTA=0
      .w_TRIVISTA=0
      .w_TRMACSTA=0
      .w_TRIMPPRE=0
      .w_TRIVAPRE=0
      .w_TRIVIPRE=0
      .w_TRMACPRE=0
      .w_TRIMPSEG=0
      .w_TRIVASEG=0
      .w_TRIVISEG=0
      .w_TRMACSEG=0
      .w_TRIMPFAD=0
      .w_TRIVAFAD=0
      .w_TRIVIFAD=0
      .w_TRMACFAD=0
      .w_TRIMPIND=0
      .w_TRIVAIND=0
      .w_TRIVIIND=0
      .w_OBTEST=ctod("  /  /  ")
      .w_TRMACIND=0
      .w_TOTIMP=0
      .w_TOTIVA=0
      .w_TOTIVI=0
      .w_TOTMAC=0
      .w_DECTOT=0
      .w_CALCPIC=0
      if .cFunction<>"Filter"
        .w_TR__ANNO = ALLTRIM(STR(YEAR(i_DATSYS)))
        .DoRTCalc(2,2,.f.)
        .w_TRTIPREG = 'V'
        .w_TRNUMREG = 1
        .DoRTCalc(5,6,.f.)
        .w_TRCODVAL = G_PERVAL
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_TRCODVAL))
         .link_1_17('Full')
        endif
        .DoRTCalc(8,8,.f.)
        .w_UTCC = i_codute
        .w_UTDC = i_datsys
        .w_UTCV = i_codute
        .w_UTDV = i_datsys
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_TRCODIVA))
         .link_2_1('Full')
        endif
        .DoRTCalc(14,33,.f.)
        .w_OBTEST = i_INIDAT
        .DoRTCalc(35,35,.f.)
        .w_TOTIMP = (.w_TRIMPSTA+.w_TRIMPSEG+.w_TRIMPIND)-(.w_TRIMPPRE+.w_TRIMPFAD)
        .w_TOTIVA = (.w_TRIVASTA+.w_TRIVASEG+.w_TRIVAIND)-(.w_TRIVAPRE+.w_TRIVAFAD)
        .w_TOTIVI = (.w_TRIVISTA+.w_TRIVISEG+.w_TRIVIIND)-(.w_TRIVIPRE+.w_TRIVIFAD)
        .w_TOTMAC = (.w_TRMACSTA+.w_TRMACSEG+.w_TRMACIND)-(.w_TRMACPRE+.w_TRMACFAD)
        .DoRTCalc(40,40,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRI_MAST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTR__ANNO_1_1.enabled = i_bVal
      .Page1.oPag.oTRNUMPER_1_2.enabled = i_bVal
      .Page1.oPag.oTRTIPREG_1_3.enabled = i_bVal
      .Page1.oPag.oTRNUMREG_1_4.enabled = i_bVal
      .Page1.oPag.oTRFLSTAM_1_5.enabled = i_bVal
      .Page1.oPag.oTRFLSTAR_1_6.enabled = i_bVal
      .Page1.oPag.oTRCODVAL_1_17.enabled = i_bVal
      .Page1.oPag.oTRIMPVEN_1_18.enabled = i_bVal
      .Page1.oPag.oTRIMPSTA_2_3.enabled = i_bVal
      .Page1.oPag.oTRIVASTA_2_4.enabled = i_bVal
      .Page1.oPag.oTRIVISTA_2_5.enabled = i_bVal
      .Page1.oPag.oTRMACSTA_2_6.enabled = i_bVal
      .Page1.oPag.oTRIMPPRE_2_7.enabled = i_bVal
      .Page1.oPag.oTRIVAPRE_2_8.enabled = i_bVal
      .Page1.oPag.oTRIVIPRE_2_9.enabled = i_bVal
      .Page1.oPag.oTRMACPRE_2_10.enabled = i_bVal
      .Page1.oPag.oTRIMPSEG_2_11.enabled = i_bVal
      .Page1.oPag.oTRIVASEG_2_12.enabled = i_bVal
      .Page1.oPag.oTRIVISEG_2_13.enabled = i_bVal
      .Page1.oPag.oTRMACSEG_2_14.enabled = i_bVal
      .Page1.oPag.oTRIMPFAD_2_15.enabled = i_bVal
      .Page1.oPag.oTRIVAFAD_2_16.enabled = i_bVal
      .Page1.oPag.oTRIVIFAD_2_17.enabled = i_bVal
      .Page1.oPag.oTRMACFAD_2_18.enabled = i_bVal
      .Page1.oPag.oTRIMPIND_2_19.enabled = i_bVal
      .Page1.oPag.oTRIVAIND_2_20.enabled = i_bVal
      .Page1.oPag.oTRIVIIND_2_21.enabled = i_bVal
      .Page1.oPag.oTRMACIND_2_23.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTR__ANNO_1_1.enabled = .f.
        .Page1.oPag.oTRNUMPER_1_2.enabled = .f.
        .Page1.oPag.oTRTIPREG_1_3.enabled = .f.
        .Page1.oPag.oTRNUMREG_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTR__ANNO_1_1.enabled = .t.
        .Page1.oPag.oTRNUMPER_1_2.enabled = .t.
        .Page1.oPag.oTRTIPREG_1_3.enabled = .t.
        .Page1.oPag.oTRNUMREG_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PRI_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRI_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TR__ANNO,"TR__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRNUMPER,"TRNUMPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRTIPREG,"TRTIPREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRNUMREG,"TRNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRFLSTAM,"TRFLSTAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRFLSTAR,"TRFLSTAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODVAL,"TRCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRIMPVEN,"TRIMPVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRI_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRI_MAST_IDX,2])
    i_lTable = "PRI_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PRI_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_TRCODIVA C(5);
      ,t_DESIVA C(35);
      ,t_TRIMPSTA N(18,4);
      ,t_TRIVASTA N(18,4);
      ,t_TRIVISTA N(18,4);
      ,t_TRMACSTA N(18,4);
      ,t_TRIMPPRE N(18,4);
      ,t_TRIVAPRE N(18,4);
      ,t_TRIVIPRE N(18,4);
      ,t_TRMACPRE N(18,4);
      ,t_TRIMPSEG N(18,4);
      ,t_TRIVASEG N(18,4);
      ,t_TRIVISEG N(18,4);
      ,t_TRMACSEG N(18,4);
      ,t_TRIMPFAD N(18,4);
      ,t_TRIVAFAD N(18,4);
      ,t_TRIVIFAD N(18,4);
      ,t_TRMACFAD N(18,4);
      ,t_TRIMPIND N(18,4);
      ,t_TRIVAIND N(18,4);
      ,t_TRIVIIND N(18,4);
      ,t_TRMACIND N(18,4);
      ,t_TOTIMP N(18,4);
      ,t_TOTIVA N(18,4);
      ,t_TOTIVI N(18,4);
      ,t_TOTMAC N(18,4);
      ,TRCODIVA C(5);
      ,t_OBTEST D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mribodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODIVA_2_1.controlsource=this.cTrsName+'.t_TRCODIVA'
    this.oPgFRm.Page1.oPag.oDESIVA_2_2.controlsource=this.cTrsName+'.t_DESIVA'
    this.oPgFRm.Page1.oPag.oTRIMPSTA_2_3.controlsource=this.cTrsName+'.t_TRIMPSTA'
    this.oPgFRm.Page1.oPag.oTRIVASTA_2_4.controlsource=this.cTrsName+'.t_TRIVASTA'
    this.oPgFRm.Page1.oPag.oTRIVISTA_2_5.controlsource=this.cTrsName+'.t_TRIVISTA'
    this.oPgFRm.Page1.oPag.oTRMACSTA_2_6.controlsource=this.cTrsName+'.t_TRMACSTA'
    this.oPgFRm.Page1.oPag.oTRIMPPRE_2_7.controlsource=this.cTrsName+'.t_TRIMPPRE'
    this.oPgFRm.Page1.oPag.oTRIVAPRE_2_8.controlsource=this.cTrsName+'.t_TRIVAPRE'
    this.oPgFRm.Page1.oPag.oTRIVIPRE_2_9.controlsource=this.cTrsName+'.t_TRIVIPRE'
    this.oPgFRm.Page1.oPag.oTRMACPRE_2_10.controlsource=this.cTrsName+'.t_TRMACPRE'
    this.oPgFRm.Page1.oPag.oTRIMPSEG_2_11.controlsource=this.cTrsName+'.t_TRIMPSEG'
    this.oPgFRm.Page1.oPag.oTRIVASEG_2_12.controlsource=this.cTrsName+'.t_TRIVASEG'
    this.oPgFRm.Page1.oPag.oTRIVISEG_2_13.controlsource=this.cTrsName+'.t_TRIVISEG'
    this.oPgFRm.Page1.oPag.oTRMACSEG_2_14.controlsource=this.cTrsName+'.t_TRMACSEG'
    this.oPgFRm.Page1.oPag.oTRIMPFAD_2_15.controlsource=this.cTrsName+'.t_TRIMPFAD'
    this.oPgFRm.Page1.oPag.oTRIVAFAD_2_16.controlsource=this.cTrsName+'.t_TRIVAFAD'
    this.oPgFRm.Page1.oPag.oTRIVIFAD_2_17.controlsource=this.cTrsName+'.t_TRIVIFAD'
    this.oPgFRm.Page1.oPag.oTRMACFAD_2_18.controlsource=this.cTrsName+'.t_TRMACFAD'
    this.oPgFRm.Page1.oPag.oTRIMPIND_2_19.controlsource=this.cTrsName+'.t_TRIMPIND'
    this.oPgFRm.Page1.oPag.oTRIVAIND_2_20.controlsource=this.cTrsName+'.t_TRIVAIND'
    this.oPgFRm.Page1.oPag.oTRIVIIND_2_21.controlsource=this.cTrsName+'.t_TRIVIIND'
    this.oPgFRm.Page1.oPag.oTRMACIND_2_23.controlsource=this.cTrsName+'.t_TRMACIND'
    this.oPgFRm.Page1.oPag.oTOTIMP_2_24.controlsource=this.cTrsName+'.t_TOTIMP'
    this.oPgFRm.Page1.oPag.oTOTIVA_2_25.controlsource=this.cTrsName+'.t_TOTIVA'
    this.oPgFRm.Page1.oPag.oTOTIVI_2_26.controlsource=this.cTrsName+'.t_TOTIVI'
    this.oPgFRm.Page1.oPag.oTOTMAC_2_27.controlsource=this.cTrsName+'.t_TOTMAC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODIVA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRI_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRI_MAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PRI_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRI_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'PRI_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(TR__ANNO,TRNUMPER,TRTIPREG,TRNUMREG,TRFLSTAM"+;
                  ",TRFLSTAR,TRCODVAL,TRIMPVEN,UTCC,UTDC"+;
                  ",UTCV,UTDV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_TR__ANNO)+;
                    ","+cp_ToStrODBC(this.w_TRNUMPER)+;
                    ","+cp_ToStrODBC(this.w_TRTIPREG)+;
                    ","+cp_ToStrODBC(this.w_TRNUMREG)+;
                    ","+cp_ToStrODBC(this.w_TRFLSTAM)+;
                    ","+cp_ToStrODBC(this.w_TRFLSTAR)+;
                    ","+cp_ToStrODBCNull(this.w_TRCODVAL)+;
                    ","+cp_ToStrODBC(this.w_TRIMPVEN)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRI_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'PRI_MAST')
        cp_CheckDeletedKey(i_cTable,0,'TR__ANNO',this.w_TR__ANNO,'TRNUMPER',this.w_TRNUMPER,'TRTIPREG',this.w_TRTIPREG,'TRNUMREG',this.w_TRNUMREG)
        INSERT INTO (i_cTable);
              (TR__ANNO,TRNUMPER,TRTIPREG,TRNUMREG,TRFLSTAM,TRFLSTAR,TRCODVAL,TRIMPVEN,UTCC,UTDC,UTCV,UTDV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_TR__ANNO;
                  ,this.w_TRNUMPER;
                  ,this.w_TRTIPREG;
                  ,this.w_TRNUMREG;
                  ,this.w_TRFLSTAM;
                  ,this.w_TRFLSTAR;
                  ,this.w_TRCODVAL;
                  ,this.w_TRIMPVEN;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTCV;
                  ,this.w_UTDV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRI_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRI_DETT_IDX,2])
      *
      * insert into PRI_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(TR__ANNO,TRNUMPER,TRTIPREG,TRNUMREG,TRCODIVA"+;
                  ",TRIMPSTA,TRIVASTA,TRIVISTA,TRMACSTA,TRIMPPRE"+;
                  ",TRIVAPRE,TRIVIPRE,TRMACPRE,TRIMPSEG,TRIVASEG"+;
                  ",TRIVISEG,TRMACSEG,TRIMPFAD,TRIVAFAD,TRIVIFAD"+;
                  ",TRMACFAD,TRIMPIND,TRIVAIND,TRIVIIND,TRMACIND,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TR__ANNO)+","+cp_ToStrODBC(this.w_TRNUMPER)+","+cp_ToStrODBC(this.w_TRTIPREG)+","+cp_ToStrODBC(this.w_TRNUMREG)+","+cp_ToStrODBCNull(this.w_TRCODIVA)+;
             ","+cp_ToStrODBC(this.w_TRIMPSTA)+","+cp_ToStrODBC(this.w_TRIVASTA)+","+cp_ToStrODBC(this.w_TRIVISTA)+","+cp_ToStrODBC(this.w_TRMACSTA)+","+cp_ToStrODBC(this.w_TRIMPPRE)+;
             ","+cp_ToStrODBC(this.w_TRIVAPRE)+","+cp_ToStrODBC(this.w_TRIVIPRE)+","+cp_ToStrODBC(this.w_TRMACPRE)+","+cp_ToStrODBC(this.w_TRIMPSEG)+","+cp_ToStrODBC(this.w_TRIVASEG)+;
             ","+cp_ToStrODBC(this.w_TRIVISEG)+","+cp_ToStrODBC(this.w_TRMACSEG)+","+cp_ToStrODBC(this.w_TRIMPFAD)+","+cp_ToStrODBC(this.w_TRIVAFAD)+","+cp_ToStrODBC(this.w_TRIVIFAD)+;
             ","+cp_ToStrODBC(this.w_TRMACFAD)+","+cp_ToStrODBC(this.w_TRIMPIND)+","+cp_ToStrODBC(this.w_TRIVAIND)+","+cp_ToStrODBC(this.w_TRIVIIND)+","+cp_ToStrODBC(this.w_TRMACIND)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'TR__ANNO',this.w_TR__ANNO,'TRNUMPER',this.w_TRNUMPER,'TRTIPREG',this.w_TRTIPREG,'TRNUMREG',this.w_TRNUMREG,'TRCODIVA',this.w_TRCODIVA)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_TR__ANNO,this.w_TRNUMPER,this.w_TRTIPREG,this.w_TRNUMREG,this.w_TRCODIVA"+;
                ",this.w_TRIMPSTA,this.w_TRIVASTA,this.w_TRIVISTA,this.w_TRMACSTA,this.w_TRIMPPRE"+;
                ",this.w_TRIVAPRE,this.w_TRIVIPRE,this.w_TRMACPRE,this.w_TRIMPSEG,this.w_TRIVASEG"+;
                ",this.w_TRIVISEG,this.w_TRMACSEG,this.w_TRIMPFAD,this.w_TRIVAFAD,this.w_TRIVIFAD"+;
                ",this.w_TRMACFAD,this.w_TRIMPIND,this.w_TRIVAIND,this.w_TRIVIIND,this.w_TRMACIND,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.PRI_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRI_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update PRI_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'PRI_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " TRFLSTAM="+cp_ToStrODBC(this.w_TRFLSTAM)+;
             ",TRFLSTAR="+cp_ToStrODBC(this.w_TRFLSTAR)+;
             ",TRCODVAL="+cp_ToStrODBCNull(this.w_TRCODVAL)+;
             ",TRIMPVEN="+cp_ToStrODBC(this.w_TRIMPVEN)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'PRI_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'TR__ANNO',this.w_TR__ANNO  ,'TRNUMPER',this.w_TRNUMPER  ,'TRTIPREG',this.w_TRTIPREG  ,'TRNUMREG',this.w_TRNUMREG  )
          UPDATE (i_cTable) SET;
              TRFLSTAM=this.w_TRFLSTAM;
             ,TRFLSTAR=this.w_TRFLSTAR;
             ,TRCODVAL=this.w_TRCODVAL;
             ,TRIMPVEN=this.w_TRIMPVEN;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTCV=this.w_UTCV;
             ,UTDV=this.w_UTDV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_TRCODIVA<>space(5)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.PRI_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.PRI_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from PRI_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and TRCODIVA="+cp_ToStrODBC(&i_TN.->TRCODIVA)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and TRCODIVA=&i_TN.->TRCODIVA;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace TRCODIVA with this.w_TRCODIVA
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PRI_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " TRIMPSTA="+cp_ToStrODBC(this.w_TRIMPSTA)+;
                     ",TRIVASTA="+cp_ToStrODBC(this.w_TRIVASTA)+;
                     ",TRIVISTA="+cp_ToStrODBC(this.w_TRIVISTA)+;
                     ",TRMACSTA="+cp_ToStrODBC(this.w_TRMACSTA)+;
                     ",TRIMPPRE="+cp_ToStrODBC(this.w_TRIMPPRE)+;
                     ",TRIVAPRE="+cp_ToStrODBC(this.w_TRIVAPRE)+;
                     ",TRIVIPRE="+cp_ToStrODBC(this.w_TRIVIPRE)+;
                     ",TRMACPRE="+cp_ToStrODBC(this.w_TRMACPRE)+;
                     ",TRIMPSEG="+cp_ToStrODBC(this.w_TRIMPSEG)+;
                     ",TRIVASEG="+cp_ToStrODBC(this.w_TRIVASEG)+;
                     ",TRIVISEG="+cp_ToStrODBC(this.w_TRIVISEG)+;
                     ",TRMACSEG="+cp_ToStrODBC(this.w_TRMACSEG)+;
                     ",TRIMPFAD="+cp_ToStrODBC(this.w_TRIMPFAD)+;
                     ",TRIVAFAD="+cp_ToStrODBC(this.w_TRIVAFAD)+;
                     ",TRIVIFAD="+cp_ToStrODBC(this.w_TRIVIFAD)+;
                     ",TRMACFAD="+cp_ToStrODBC(this.w_TRMACFAD)+;
                     ",TRIMPIND="+cp_ToStrODBC(this.w_TRIMPIND)+;
                     ",TRIVAIND="+cp_ToStrODBC(this.w_TRIVAIND)+;
                     ",TRIVIIND="+cp_ToStrODBC(this.w_TRIVIIND)+;
                     ",TRMACIND="+cp_ToStrODBC(this.w_TRMACIND)+;
                     " ,TRCODIVA="+cp_ToStrODBC(this.w_TRCODIVA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and TRCODIVA="+cp_ToStrODBC(TRCODIVA)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      TRIMPSTA=this.w_TRIMPSTA;
                     ,TRIVASTA=this.w_TRIVASTA;
                     ,TRIVISTA=this.w_TRIVISTA;
                     ,TRMACSTA=this.w_TRMACSTA;
                     ,TRIMPPRE=this.w_TRIMPPRE;
                     ,TRIVAPRE=this.w_TRIVAPRE;
                     ,TRIVIPRE=this.w_TRIVIPRE;
                     ,TRMACPRE=this.w_TRMACPRE;
                     ,TRIMPSEG=this.w_TRIMPSEG;
                     ,TRIVASEG=this.w_TRIVASEG;
                     ,TRIVISEG=this.w_TRIVISEG;
                     ,TRMACSEG=this.w_TRMACSEG;
                     ,TRIMPFAD=this.w_TRIMPFAD;
                     ,TRIVAFAD=this.w_TRIVAFAD;
                     ,TRIVIFAD=this.w_TRIVIFAD;
                     ,TRMACFAD=this.w_TRMACFAD;
                     ,TRIMPIND=this.w_TRIMPIND;
                     ,TRIVAIND=this.w_TRIVAIND;
                     ,TRIVIIND=this.w_TRIVIIND;
                     ,TRMACIND=this.w_TRMACIND;
                     ,TRCODIVA=this.w_TRCODIVA;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and TRCODIVA=&i_TN.->TRCODIVA;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_TRCODIVA<>space(5)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.PRI_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PRI_DETT_IDX,2])
        *
        * delete PRI_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and TRCODIVA="+cp_ToStrODBC(&i_TN.->TRCODIVA)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and TRCODIVA=&i_TN.->TRCODIVA;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.PRI_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PRI_MAST_IDX,2])
        *
        * delete PRI_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_TRCODIVA<>space(5)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRI_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRI_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,10,.t.)
          .w_UTCV = i_codute
          .w_UTDV = i_datsys
        .DoRTCalc(13,35,.t.)
          .w_TOTIMP = (.w_TRIMPSTA+.w_TRIMPSEG+.w_TRIMPIND)-(.w_TRIMPPRE+.w_TRIMPFAD)
          .w_TOTIVA = (.w_TRIVASTA+.w_TRIVASEG+.w_TRIVAIND)-(.w_TRIVAPRE+.w_TRIVAFAD)
          .w_TOTIVI = (.w_TRIVISTA+.w_TRIVISEG+.w_TRIVIIND)-(.w_TRIVIPRE+.w_TRIVIFAD)
          .w_TOTMAC = (.w_TRMACSTA+.w_TRMACSEG+.w_TRMACIND)-(.w_TRMACPRE+.w_TRMACFAD)
        .DoRTCalc(40,40,.t.)
        if .o_DECTOT<>.w_DECTOT
          .w_CALCPIC = DEFPIC(.w_DECTOT)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_OBTEST with this.w_OBTEST
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTRIMPVEN_1_18.enabled = this.oPgFrm.Page1.oPag.oTRIMPVEN_1_18.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oTRMACSTA_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTRMACSTA_2_6.mCond()
    this.oPgFrm.Page1.oPag.oTRMACPRE_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTRMACPRE_2_10.mCond()
    this.oPgFrm.Page1.oPag.oTRMACSEG_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTRMACSEG_2_14.mCond()
    this.oPgFrm.Page1.oPag.oTRMACFAD_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTRMACFAD_2_18.mCond()
    this.oPgFrm.Page1.oPag.oTRMACIND_2_23.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oTRMACIND_2_23.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTRIMPVEN_1_18.visible=!this.oPgFrm.Page1.oPag.oTRIMPVEN_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oTRIVISTA_2_5.visible=!this.oPgFrm.Page1.oPag.oTRIVISTA_2_5.mHide()
    this.oPgFrm.Page1.oPag.oTRMACSTA_2_6.visible=!this.oPgFrm.Page1.oPag.oTRMACSTA_2_6.mHide()
    this.oPgFrm.Page1.oPag.oTRIVIPRE_2_9.visible=!this.oPgFrm.Page1.oPag.oTRIVIPRE_2_9.mHide()
    this.oPgFrm.Page1.oPag.oTRMACPRE_2_10.visible=!this.oPgFrm.Page1.oPag.oTRMACPRE_2_10.mHide()
    this.oPgFrm.Page1.oPag.oTRIVISEG_2_13.visible=!this.oPgFrm.Page1.oPag.oTRIVISEG_2_13.mHide()
    this.oPgFrm.Page1.oPag.oTRMACSEG_2_14.visible=!this.oPgFrm.Page1.oPag.oTRMACSEG_2_14.mHide()
    this.oPgFrm.Page1.oPag.oTRIVIFAD_2_17.visible=!this.oPgFrm.Page1.oPag.oTRIVIFAD_2_17.mHide()
    this.oPgFrm.Page1.oPag.oTRMACFAD_2_18.visible=!this.oPgFrm.Page1.oPag.oTRMACFAD_2_18.mHide()
    this.oPgFrm.Page1.oPag.oTRIVIIND_2_21.visible=!this.oPgFrm.Page1.oPag.oTRIVIIND_2_21.mHide()
    this.oPgFrm.Page1.oPag.oTRMACIND_2_23.visible=!this.oPgFrm.Page1.oPag.oTRMACIND_2_23.mHide()
    this.oPgFrm.Page1.oPag.oTOTIVI_2_26.visible=!this.oPgFrm.Page1.oPag.oTOTIVI_2_26.mHide()
    this.oPgFrm.Page1.oPag.oTOTMAC_2_27.visible=!this.oPgFrm.Page1.oPag.oTOTMAC_2_27.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TRCODVAL
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_TRCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_TRCODVAL))
          select VACODVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oTRCODVAL_1_17'),i_cWhere,'GSAR_AVL',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_TRCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_TRCODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_TRCODVAL = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_17.VACODVAL as VACODVAL117"+ ",link_1_17.VADECTOT as VADECTOT117"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_17 on PRI_MAST.TRCODVAL=link_1_17.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_17"
          i_cKey=i_cKey+'+" and PRI_MAST.TRCODVAL=link_1_17.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TRCODIVA
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_TRCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_TRCODIVA))
          select IVCODIVA,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_TRCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_TRCODIVA)+"%");

            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TRCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oTRCODIVA_2_1'),i_cWhere,'GSAR_AIV',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_TRCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_TRCODIVA)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.IVCODIVA as IVCODIVA201"+ ",link_2_1.IVDESIVA as IVDESIVA201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on PRI_DETT.TRCODIVA=link_2_1.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and PRI_DETT.TRCODIVA=link_2_1.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTR__ANNO_1_1.value==this.w_TR__ANNO)
      this.oPgFrm.Page1.oPag.oTR__ANNO_1_1.value=this.w_TR__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oTRNUMPER_1_2.value==this.w_TRNUMPER)
      this.oPgFrm.Page1.oPag.oTRNUMPER_1_2.value=this.w_TRNUMPER
    endif
    if not(this.oPgFrm.Page1.oPag.oTRTIPREG_1_3.RadioValue()==this.w_TRTIPREG)
      this.oPgFrm.Page1.oPag.oTRTIPREG_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRNUMREG_1_4.value==this.w_TRNUMREG)
      this.oPgFrm.Page1.oPag.oTRNUMREG_1_4.value=this.w_TRNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oTRFLSTAM_1_5.RadioValue()==this.w_TRFLSTAM)
      this.oPgFrm.Page1.oPag.oTRFLSTAM_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRFLSTAR_1_6.RadioValue()==this.w_TRFLSTAR)
      this.oPgFrm.Page1.oPag.oTRFLSTAR_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRCODVAL_1_17.RadioValue()==this.w_TRCODVAL)
      this.oPgFrm.Page1.oPag.oTRCODVAL_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIMPVEN_1_18.value==this.w_TRIMPVEN)
      this.oPgFrm.Page1.oPag.oTRIMPVEN_1_18.value=this.w_TRIMPVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_2_2.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_2_2.value=this.w_DESIVA
      replace t_DESIVA with this.oPgFrm.Page1.oPag.oDESIVA_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIMPSTA_2_3.value==this.w_TRIMPSTA)
      this.oPgFrm.Page1.oPag.oTRIMPSTA_2_3.value=this.w_TRIMPSTA
      replace t_TRIMPSTA with this.oPgFrm.Page1.oPag.oTRIMPSTA_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIVASTA_2_4.value==this.w_TRIVASTA)
      this.oPgFrm.Page1.oPag.oTRIVASTA_2_4.value=this.w_TRIVASTA
      replace t_TRIVASTA with this.oPgFrm.Page1.oPag.oTRIVASTA_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIVISTA_2_5.value==this.w_TRIVISTA)
      this.oPgFrm.Page1.oPag.oTRIVISTA_2_5.value=this.w_TRIVISTA
      replace t_TRIVISTA with this.oPgFrm.Page1.oPag.oTRIVISTA_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRMACSTA_2_6.value==this.w_TRMACSTA)
      this.oPgFrm.Page1.oPag.oTRMACSTA_2_6.value=this.w_TRMACSTA
      replace t_TRMACSTA with this.oPgFrm.Page1.oPag.oTRMACSTA_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIMPPRE_2_7.value==this.w_TRIMPPRE)
      this.oPgFrm.Page1.oPag.oTRIMPPRE_2_7.value=this.w_TRIMPPRE
      replace t_TRIMPPRE with this.oPgFrm.Page1.oPag.oTRIMPPRE_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIVAPRE_2_8.value==this.w_TRIVAPRE)
      this.oPgFrm.Page1.oPag.oTRIVAPRE_2_8.value=this.w_TRIVAPRE
      replace t_TRIVAPRE with this.oPgFrm.Page1.oPag.oTRIVAPRE_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIVIPRE_2_9.value==this.w_TRIVIPRE)
      this.oPgFrm.Page1.oPag.oTRIVIPRE_2_9.value=this.w_TRIVIPRE
      replace t_TRIVIPRE with this.oPgFrm.Page1.oPag.oTRIVIPRE_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRMACPRE_2_10.value==this.w_TRMACPRE)
      this.oPgFrm.Page1.oPag.oTRMACPRE_2_10.value=this.w_TRMACPRE
      replace t_TRMACPRE with this.oPgFrm.Page1.oPag.oTRMACPRE_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIMPSEG_2_11.value==this.w_TRIMPSEG)
      this.oPgFrm.Page1.oPag.oTRIMPSEG_2_11.value=this.w_TRIMPSEG
      replace t_TRIMPSEG with this.oPgFrm.Page1.oPag.oTRIMPSEG_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIVASEG_2_12.value==this.w_TRIVASEG)
      this.oPgFrm.Page1.oPag.oTRIVASEG_2_12.value=this.w_TRIVASEG
      replace t_TRIVASEG with this.oPgFrm.Page1.oPag.oTRIVASEG_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIVISEG_2_13.value==this.w_TRIVISEG)
      this.oPgFrm.Page1.oPag.oTRIVISEG_2_13.value=this.w_TRIVISEG
      replace t_TRIVISEG with this.oPgFrm.Page1.oPag.oTRIVISEG_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRMACSEG_2_14.value==this.w_TRMACSEG)
      this.oPgFrm.Page1.oPag.oTRMACSEG_2_14.value=this.w_TRMACSEG
      replace t_TRMACSEG with this.oPgFrm.Page1.oPag.oTRMACSEG_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIMPFAD_2_15.value==this.w_TRIMPFAD)
      this.oPgFrm.Page1.oPag.oTRIMPFAD_2_15.value=this.w_TRIMPFAD
      replace t_TRIMPFAD with this.oPgFrm.Page1.oPag.oTRIMPFAD_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIVAFAD_2_16.value==this.w_TRIVAFAD)
      this.oPgFrm.Page1.oPag.oTRIVAFAD_2_16.value=this.w_TRIVAFAD
      replace t_TRIVAFAD with this.oPgFrm.Page1.oPag.oTRIVAFAD_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIVIFAD_2_17.value==this.w_TRIVIFAD)
      this.oPgFrm.Page1.oPag.oTRIVIFAD_2_17.value=this.w_TRIVIFAD
      replace t_TRIVIFAD with this.oPgFrm.Page1.oPag.oTRIVIFAD_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRMACFAD_2_18.value==this.w_TRMACFAD)
      this.oPgFrm.Page1.oPag.oTRMACFAD_2_18.value=this.w_TRMACFAD
      replace t_TRMACFAD with this.oPgFrm.Page1.oPag.oTRMACFAD_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIMPIND_2_19.value==this.w_TRIMPIND)
      this.oPgFrm.Page1.oPag.oTRIMPIND_2_19.value=this.w_TRIMPIND
      replace t_TRIMPIND with this.oPgFrm.Page1.oPag.oTRIMPIND_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIVAIND_2_20.value==this.w_TRIVAIND)
      this.oPgFrm.Page1.oPag.oTRIVAIND_2_20.value=this.w_TRIVAIND
      replace t_TRIVAIND with this.oPgFrm.Page1.oPag.oTRIVAIND_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRIVIIND_2_21.value==this.w_TRIVIIND)
      this.oPgFrm.Page1.oPag.oTRIVIIND_2_21.value=this.w_TRIVIIND
      replace t_TRIVIIND with this.oPgFrm.Page1.oPag.oTRIVIIND_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTRMACIND_2_23.value==this.w_TRMACIND)
      this.oPgFrm.Page1.oPag.oTRMACIND_2_23.value=this.w_TRMACIND
      replace t_TRMACIND with this.oPgFrm.Page1.oPag.oTRMACIND_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMP_2_24.value==this.w_TOTIMP)
      this.oPgFrm.Page1.oPag.oTOTIMP_2_24.value=this.w_TOTIMP
      replace t_TOTIMP with this.oPgFrm.Page1.oPag.oTOTIMP_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIVA_2_25.value==this.w_TOTIVA)
      this.oPgFrm.Page1.oPag.oTOTIVA_2_25.value=this.w_TOTIVA
      replace t_TOTIVA with this.oPgFrm.Page1.oPag.oTOTIVA_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIVI_2_26.value==this.w_TOTIVI)
      this.oPgFrm.Page1.oPag.oTOTIVI_2_26.value=this.w_TOTIVI
      replace t_TOTIVI with this.oPgFrm.Page1.oPag.oTOTIVI_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTMAC_2_27.value==this.w_TOTMAC)
      this.oPgFrm.Page1.oPag.oTOTMAC_2_27.value=this.w_TOTMAC
      replace t_TOTMAC with this.oPgFrm.Page1.oPag.oTOTMAC_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODIVA_2_1.value==this.w_TRCODIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODIVA_2_1.value=this.w_TRCODIVA
      replace t_TRCODIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODIVA_2_1.value
    endif
    cp_SetControlsValueExtFlds(this,'PRI_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(VAL(.w_TR__ANNO)>0)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oTR__ANNO_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Anno non valorizzato")
          case   not(.w_TRNUMPER>0 AND (.w_TRNUMPER<5 OR (.w_TRNUMPER<13 AND g_TIPDEN="M")))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oTRNUMPER_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero periodo non congruente")
          case   (empty(.w_TRNUMREG) or not(.w_TRNUMREG>0))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oTRNUMREG_1_4.SetFocus()
            i_bnoObbl = !empty(.w_TRNUMREG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero registro IVA non valorizzato")
          case   (empty(.w_TRCODVAL))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oTRCODVAL_1_17.SetFocus()
            i_bnoObbl = !empty(.w_TRCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .w_TRCODIVA<>space(5)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DECTOT = this.w_DECTOT
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_TRCODIVA<>space(5))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_TRCODIVA=space(5)
      .w_DESIVA=space(35)
      .w_TRIMPSTA=0
      .w_TRIVASTA=0
      .w_TRIVISTA=0
      .w_TRMACSTA=0
      .w_TRIMPPRE=0
      .w_TRIVAPRE=0
      .w_TRIVIPRE=0
      .w_TRMACPRE=0
      .w_TRIMPSEG=0
      .w_TRIVASEG=0
      .w_TRIVISEG=0
      .w_TRMACSEG=0
      .w_TRIMPFAD=0
      .w_TRIVAFAD=0
      .w_TRIVIFAD=0
      .w_TRMACFAD=0
      .w_TRIMPIND=0
      .w_TRIVAIND=0
      .w_TRIVIIND=0
      .w_OBTEST=ctod("  /  /  ")
      .w_TRMACIND=0
      .w_TOTIMP=0
      .w_TOTIVA=0
      .w_TOTIVI=0
      .w_TOTMAC=0
      .DoRTCalc(1,13,.f.)
      if not(empty(.w_TRCODIVA))
        .link_2_1('Full')
      endif
      .DoRTCalc(14,33,.f.)
        .w_OBTEST = i_INIDAT
      .DoRTCalc(35,35,.f.)
        .w_TOTIMP = (.w_TRIMPSTA+.w_TRIMPSEG+.w_TRIMPIND)-(.w_TRIMPPRE+.w_TRIMPFAD)
        .w_TOTIVA = (.w_TRIVASTA+.w_TRIVASEG+.w_TRIVAIND)-(.w_TRIVAPRE+.w_TRIVAFAD)
        .w_TOTIVI = (.w_TRIVISTA+.w_TRIVISEG+.w_TRIVIIND)-(.w_TRIVIPRE+.w_TRIVIFAD)
        .w_TOTMAC = (.w_TRMACSTA+.w_TRMACSEG+.w_TRMACIND)-(.w_TRMACPRE+.w_TRMACFAD)
    endwith
    this.DoRTCalc(40,41,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_TRCODIVA = t_TRCODIVA
    this.w_DESIVA = t_DESIVA
    this.w_TRIMPSTA = t_TRIMPSTA
    this.w_TRIVASTA = t_TRIVASTA
    this.w_TRIVISTA = t_TRIVISTA
    this.w_TRMACSTA = t_TRMACSTA
    this.w_TRIMPPRE = t_TRIMPPRE
    this.w_TRIVAPRE = t_TRIVAPRE
    this.w_TRIVIPRE = t_TRIVIPRE
    this.w_TRMACPRE = t_TRMACPRE
    this.w_TRIMPSEG = t_TRIMPSEG
    this.w_TRIVASEG = t_TRIVASEG
    this.w_TRIVISEG = t_TRIVISEG
    this.w_TRMACSEG = t_TRMACSEG
    this.w_TRIMPFAD = t_TRIMPFAD
    this.w_TRIVAFAD = t_TRIVAFAD
    this.w_TRIVIFAD = t_TRIVIFAD
    this.w_TRMACFAD = t_TRMACFAD
    this.w_TRIMPIND = t_TRIMPIND
    this.w_TRIVAIND = t_TRIVAIND
    this.w_TRIVIIND = t_TRIVIIND
    this.w_OBTEST = t_OBTEST
    this.w_TRMACIND = t_TRMACIND
    this.w_TOTIMP = t_TOTIMP
    this.w_TOTIVA = t_TOTIVA
    this.w_TOTIVI = t_TOTIVI
    this.w_TOTMAC = t_TOTMAC
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_TRCODIVA with this.w_TRCODIVA
    replace t_DESIVA with this.w_DESIVA
    replace t_TRIMPSTA with this.w_TRIMPSTA
    replace t_TRIVASTA with this.w_TRIVASTA
    replace t_TRIVISTA with this.w_TRIVISTA
    replace t_TRMACSTA with this.w_TRMACSTA
    replace t_TRIMPPRE with this.w_TRIMPPRE
    replace t_TRIVAPRE with this.w_TRIVAPRE
    replace t_TRIVIPRE with this.w_TRIVIPRE
    replace t_TRMACPRE with this.w_TRMACPRE
    replace t_TRIMPSEG with this.w_TRIMPSEG
    replace t_TRIVASEG with this.w_TRIVASEG
    replace t_TRIVISEG with this.w_TRIVISEG
    replace t_TRMACSEG with this.w_TRMACSEG
    replace t_TRIMPFAD with this.w_TRIMPFAD
    replace t_TRIVAFAD with this.w_TRIVAFAD
    replace t_TRIVIFAD with this.w_TRIVIFAD
    replace t_TRMACFAD with this.w_TRMACFAD
    replace t_TRIMPIND with this.w_TRIMPIND
    replace t_TRIVAIND with this.w_TRIVAIND
    replace t_TRIVIIND with this.w_TRIVIIND
    replace t_OBTEST with this.w_OBTEST
    replace t_TRMACIND with this.w_TRMACIND
    replace t_TOTIMP with this.w_TOTIMP
    replace t_TOTIVA with this.w_TOTIVA
    replace t_TOTIVI with this.w_TOTIVI
    replace t_TOTMAC with this.w_TOTMAC
    if i_srv='A'
      replace TRCODIVA with this.w_TRCODIVA
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mriPag1 as StdContainer
  Width  = 806
  height = 296
  stdWidth  = 806
  stdheight = 296
  resizeYpos=265
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTR__ANNO_1_1 as StdField with uid="DTZUPLXXJO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TR__ANNO", cQueryName = "TR__ANNO",nZero=4,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Anno non valorizzato",;
    HelpContextID = 195809413,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=68, Top=11, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  func oTR__ANNO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_TR__ANNO)>0)
    endwith
    return bRes
  endfunc

  add object oTRNUMPER_1_2 as StdField with uid="PLJSZICLKR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TRNUMPER", cQueryName = "TR__ANNO,TRNUMPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero periodo non congruente",;
    ToolTipText = "Periodo: 1..4 per trimestrale; 1..12 per mensile",;
    HelpContextID = 27213688,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=30, Left=195, Top=11, cSayPict='"99"', cGetPict='"99"'

  func oTRNUMPER_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TRNUMPER>0 AND (.w_TRNUMPER<5 OR (.w_TRNUMPER<13 AND g_TIPDEN="M")))
    endwith
    return bRes
  endfunc


  add object oTRTIPREG_1_3 as StdCombo with uid="TILCDJGXAO",rtseq=3,rtrep=.f.,left=307,top=11,width=132,height=21;
    , ToolTipText = "Tipo registro IVA";
    , HelpContextID = 259710851;
    , cFormVar="w_TRTIPREG",RowSource=""+"Vendite,"+"Acquisti,"+"Corr.scorporo,"+"Corr.ventilazione", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oTRTIPREG_1_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRTIPREG,&i_cF..t_TRTIPREG),this.value)
    return(iif(xVal =1,'V',;
    iif(xVal =2,'A',;
    iif(xVal =3,'C',;
    iif(xVal =4,'E',;
    space(1))))))
  endfunc
  func oTRTIPREG_1_3.GetRadio()
    this.Parent.oContained.w_TRTIPREG = this.RadioValue()
    return .t.
  endfunc

  func oTRTIPREG_1_3.ToRadio()
    this.Parent.oContained.w_TRTIPREG=trim(this.Parent.oContained.w_TRTIPREG)
    return(;
      iif(this.Parent.oContained.w_TRTIPREG=='V',1,;
      iif(this.Parent.oContained.w_TRTIPREG=='A',2,;
      iif(this.Parent.oContained.w_TRTIPREG=='C',3,;
      iif(this.Parent.oContained.w_TRTIPREG=='E',4,;
      0)))))
  endfunc

  func oTRTIPREG_1_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oTRNUMREG_1_4 as StdField with uid="TPWSGYUUHK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TRNUMREG", cQueryName = "TR__ANNO,TRNUMPER,TRTIPREG,TRNUMREG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero registro IVA non valorizzato",;
    ToolTipText = "Numero registro IVA",;
    HelpContextID = 262094723,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=30, Left=474, Top=11, cSayPict='"99"', cGetPict='"99"'

  func oTRNUMREG_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_TRNUMREG>0)
    endwith
    return bRes
  endfunc

  add object oTRFLSTAM_1_5 as StdCheck with uid="WMUIFSEXNN",rtseq=5,rtrep=.f.,left=584, top=11, caption="Liquidazione",;
    ToolTipText = "Se attivo: stampata liquidazione IVA",;
    HelpContextID = 45564035,;
    cFormVar="w_TRFLSTAM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTRFLSTAM_1_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRFLSTAM,&i_cF..t_TRFLSTAM),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oTRFLSTAM_1_5.GetRadio()
    this.Parent.oContained.w_TRFLSTAM = this.RadioValue()
    return .t.
  endfunc

  func oTRFLSTAM_1_5.ToRadio()
    this.Parent.oContained.w_TRFLSTAM=trim(this.Parent.oContained.w_TRFLSTAM)
    return(;
      iif(this.Parent.oContained.w_TRFLSTAM=='S',1,;
      0))
  endfunc

  func oTRFLSTAM_1_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oTRFLSTAR_1_6 as StdCheck with uid="FBCBHEQYNM",rtseq=6,rtrep=.f.,left=584, top=29, caption="Dichiar. riepilogo",;
    ToolTipText = "Se attivo: stampata dichiarazione riepilogativa",;
    HelpContextID = 45564040,;
    cFormVar="w_TRFLSTAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTRFLSTAR_1_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRFLSTAR,&i_cF..t_TRFLSTAR),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oTRFLSTAR_1_6.GetRadio()
    this.Parent.oContained.w_TRFLSTAR = this.RadioValue()
    return .t.
  endfunc

  func oTRFLSTAR_1_6.ToRadio()
    this.Parent.oContained.w_TRFLSTAR=trim(this.Parent.oContained.w_TRFLSTAR)
    return(;
      iif(this.Parent.oContained.w_TRFLSTAR=='S',1,;
      0))
  endfunc

  func oTRFLSTAR_1_6.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oTRCODVAL_1_17 as StdCombo with uid="IJBVBUCDLJ",rtseq=7,rtrep=.f.,left=68,top=38,width=92,height=21;
    , ToolTipText = "Valuta di conto riferita all'anno e periodo considerato (Lire o Euro)";
    , HelpContextID = 63574146;
    , cFormVar="w_TRCODVAL",RowSource=""+"Valuta di conto,"+"Valuta nazionale", bObbl = .t. , nPag = 1;
    , cLinkFile="VALUTE";
  , bGlobalFont=.t.


  func oTRCODVAL_1_17.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRCODVAL,&i_cF..t_TRCODVAL),this.value)
    return(iif(xVal =1,ALLTR(g_CODEUR),;
    iif(xVal =2,ALLTR(g_CODLIR),;
    space(3))))
  endfunc
  func oTRCODVAL_1_17.GetRadio()
    this.Parent.oContained.w_TRCODVAL = this.RadioValue()
    return .t.
  endfunc

  func oTRCODVAL_1_17.ToRadio()
    this.Parent.oContained.w_TRCODVAL=trim(this.Parent.oContained.w_TRCODVAL)
    return(;
      iif(this.Parent.oContained.w_TRCODVAL==ALLTR(g_CODEUR),1,;
      iif(this.Parent.oContained.w_TRCODVAL==ALLTR(g_CODLIR),2,;
      0)))
  endfunc

  func oTRCODVAL_1_17.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTRCODVAL_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCODVAL_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oTRIMPVEN_1_18 as StdField with uid="PWTUPCRTRO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TRIMPVEN", cQueryName = "TRIMPVEN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale dei corrispettivi da ventilare",;
    HelpContextID = 192384892,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=370, Top=38, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oTRIMPVEN_1_18.mCond()
    with this.Parent.oContained
      return (.w_TRTIPREG="E")
    endwith
  endfunc

  func oTRIMPVEN_1_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>"E")
    endwith
    endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=73, width=75,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=1,Field1="TRCODIVA",Label1="Cod.IVA",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 66211206

  add object oStr_1_7 as StdString with uid="GJXVUADINC",Visible=.t., Left=123, Top=11,;
    Alignment=1, Width=70, Height=15,;
    Caption="Periodo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="OXEZUIELPN",Visible=.t., Left=229, Top=11,;
    Alignment=1, Width=75, Height=15,;
    Caption="Registro:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="CBYVVWKQWH",Visible=.t., Left=446, Top=11,;
    Alignment=1, Width=27, Height=15,;
    Caption="N.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="VIKRMKFZAB",Visible=.t., Left=3, Top=11,;
    Alignment=1, Width=63, Height=15,;
    Caption="Anno:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="JORNGJBGHM",Visible=.t., Left=81, Top=130,;
    Alignment=0, Width=15, Height=15,;
    Caption="(-)"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="GWIZXFPTQQ",Visible=.t., Left=81, Top=156,;
    Alignment=0, Width=15, Height=15,;
    Caption="(+)"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="SYVFQBXSES",Visible=.t., Left=81, Top=182,;
    Alignment=0, Width=15, Height=15,;
    Caption="(-)"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="UMWILHDIAV",Visible=.t., Left=81, Top=208,;
    Alignment=0, Width=15, Height=15,;
    Caption="(+)"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="TKAWRLKSTF",Visible=.t., Left=81, Top=242,;
    Alignment=0, Width=15, Height=15,;
    Caption="(=)"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="BOGGODFQTS",Visible=.t., Left=187, Top=38,;
    Alignment=1, Width=181, Height=15,;
    Caption="Corrispettivi da ventilare:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_TRTIPREG<>"E")
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="JZGFEEGLEH",Visible=.t., Left=101, Top=104,;
    Alignment=1, Width=144, Height=15,;
    Caption="Documenti del periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="HLEYPLHQRJ",Visible=.t., Left=115, Top=130,;
    Alignment=1, Width=130, Height=15,;
    Caption="Periodo precedente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="XYTJOOVUJC",Visible=.t., Left=115, Top=156,;
    Alignment=1, Width=130, Height=15,;
    Caption="Periodo seguente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="UYGLXJMRCW",Visible=.t., Left=115, Top=182,;
    Alignment=1, Width=130, Height=15,;
    Caption="Ad esigibilitÓ differita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="VEPLXTTGNV",Visible=.t., Left=115, Top=208,;
    Alignment=1, Width=130, Height=15,;
    Caption="Incassi/pag. esig.diff.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="QCQGQGRNXJ",Visible=.t., Left=248, Top=76,;
    Alignment=2, Width=133, Height=15,;
    Caption="Imponibile"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="OKDWZPXZQR",Visible=.t., Left=390, Top=76,;
    Alignment=2, Width=132, Height=15,;
    Caption="IVA detraibile"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (.w_TRTIPREG<>'A')
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="CEICCPFIFR",Visible=.t., Left=529, Top=76,;
    Alignment=2, Width=131, Height=15,;
    Caption="IVA indetraibile"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_TRTIPREG<>'A')
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="XVTUYDRBJK",Visible=.t., Left=99, Top=242,;
    Alignment=1, Width=146, Height=15,;
    Caption="Totali per competenza:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="NGAJBEQTAN",Visible=.t., Left=665, Top=76,;
    Alignment=2, Width=137, Height=15,;
    Caption="Monte acq.ventilazione"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_TRTIPREG<>'E')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="SCNSZGQFTA",Visible=.t., Left=388, Top=76,;
    Alignment=2, Width=132, Height=18,;
    Caption="IVA esigibile"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_TRTIPREG='A')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="DVQXIEARPA",Visible=.t., Left=3, Top=38,;
    Alignment=1, Width=63, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oBox_1_10 as StdBox with uid="XHFTCXJBUH",left=247, top=72, width=556,height=23

  add object oBox_1_29 as StdBox with uid="HIUHDDEEAV",left=384, top=73, width=2,height=193

  add object oBox_1_30 as StdBox with uid="VTREXMFNFG",left=523, top=73, width=2,height=193

  add object oBox_1_31 as StdBox with uid="UKERDWQNNB",left=662, top=73, width=2,height=193

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=92,;
    width=71+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=93,width=70+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VOCIIVA|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESIVA_2_2.Refresh()
      this.Parent.oTRIMPSTA_2_3.Refresh()
      this.Parent.oTRIVASTA_2_4.Refresh()
      this.Parent.oTRIVISTA_2_5.Refresh()
      this.Parent.oTRMACSTA_2_6.Refresh()
      this.Parent.oTRIMPPRE_2_7.Refresh()
      this.Parent.oTRIVAPRE_2_8.Refresh()
      this.Parent.oTRIVIPRE_2_9.Refresh()
      this.Parent.oTRMACPRE_2_10.Refresh()
      this.Parent.oTRIMPSEG_2_11.Refresh()
      this.Parent.oTRIVASEG_2_12.Refresh()
      this.Parent.oTRIVISEG_2_13.Refresh()
      this.Parent.oTRMACSEG_2_14.Refresh()
      this.Parent.oTRIMPFAD_2_15.Refresh()
      this.Parent.oTRIVAFAD_2_16.Refresh()
      this.Parent.oTRIVIFAD_2_17.Refresh()
      this.Parent.oTRMACFAD_2_18.Refresh()
      this.Parent.oTRIMPIND_2_19.Refresh()
      this.Parent.oTRIVAIND_2_20.Refresh()
      this.Parent.oTRIVIIND_2_21.Refresh()
      this.Parent.oTRMACIND_2_23.Refresh()
      this.Parent.oTOTIMP_2_24.Refresh()
      this.Parent.oTOTIVA_2_25.Refresh()
      this.Parent.oTOTIVI_2_26.Refresh()
      this.Parent.oTOTMAC_2_27.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VOCIIVA'
        oDropInto=this.oBodyCol.oRow.oTRCODIVA_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESIVA_2_2 as StdTrsField with uid="JMNEGPCIOW",rtseq=14,rtrep=.t.,;
    cFormVar="w_DESIVA",value=space(35),enabled=.f.,;
    HelpContextID = 266666550,;
    cTotal="", bFixedPos=.t., cQueryName = "DESIVA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=106, Top=272, InputMask=replicate('X',35)

  add object oTRIMPSTA_2_3 as StdTrsField with uid="IMXKFLKXPN",rtseq=15,rtrep=.t.,;
    cFormVar="w_TRIMPSTA",value=0,;
    ToolTipText = "Totale imponibile dei documenti stampati nel periodo selezionato",;
    HelpContextID = 242716553,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIMPSTA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=248, Top=105, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTRIVASTA_2_4 as StdTrsField with uid="JKOUVFHLVP",rtseq=16,rtrep=.t.,;
    cFormVar="w_TRIVASTA",value=0,;
    ToolTipText = "Totale imposta detraibile dei documenti stampati nel periodo selezionato",;
    HelpContextID = 257855369,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIVASTA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=387, Top=105, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTRIVISTA_2_5 as StdTrsField with uid="DLMRYKXBBI",rtseq=17,rtrep=.t.,;
    cFormVar="w_TRIVISTA",value=0,;
    ToolTipText = "Totale imposta indetraibile dei documenti stampati nel periodo selezionato",;
    HelpContextID = 249466761,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIVISTA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=526, Top=105, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oTRIVISTA_2_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>'A')
    endwith
    endif
  endfunc

  add object oTRMACSTA_2_6 as StdTrsField with uid="ZWWEYIWDGU",rtseq=18,rtrep=.t.,;
    cFormVar="w_TRMACSTA",value=0,;
    ToolTipText = "Totale monte acquisti dei documenti stampati nel periodo selezionato",;
    HelpContextID = 257118089,;
    cTotal="", bFixedPos=.t., cQueryName = "TRMACSTA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=665, Top=105, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oTRMACSTA_2_6.mCond()
    with this.Parent.oContained
      return (.w_TRTIPREG='E')
    endwith
  endfunc

  func oTRMACSTA_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>'E')
    endwith
    endif
  endfunc

  add object oTRIMPPRE_2_7 as StdTrsField with uid="WKUQFTEWJQ",rtseq=19,rtrep=.t.,;
    cFormVar="w_TRIMPPRE",value=0,;
    ToolTipText = "Totale imponibile dei documenti di competenza del periodo precedente",;
    HelpContextID = 243822715,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIMPPRE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=248, Top=131, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTRIVAPRE_2_8 as StdTrsField with uid="YVLULLQBTI",rtseq=20,rtrep=.t.,;
    cFormVar="w_TRIVAPRE",value=0,;
    ToolTipText = "Totale imposta detraibile dei documenti di competenza del periodo precedente",;
    HelpContextID = 228683899,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIVAPRE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=387, Top=131, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTRIVIPRE_2_9 as StdTrsField with uid="KZLFALUEJE",rtseq=21,rtrep=.t.,;
    cFormVar="w_TRIVIPRE",value=0,;
    ToolTipText = "Totale imposta indetraibile dei documenti di competenza del periodo precedente",;
    HelpContextID = 237072507,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIVIPRE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=526, Top=131, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oTRIVIPRE_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>'A')
    endwith
    endif
  endfunc

  add object oTRMACPRE_2_10 as StdTrsField with uid="YDPRTHCMVL",rtseq=22,rtrep=.t.,;
    cFormVar="w_TRMACPRE",value=0,;
    ToolTipText = "Totale monte acquisti dei documenti di competenza del periodo precedente",;
    HelpContextID = 229421179,;
    cTotal="", bFixedPos=.t., cQueryName = "TRMACPRE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=665, Top=131, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oTRMACPRE_2_10.mCond()
    with this.Parent.oContained
      return (.w_TRTIPREG='E')
    endwith
  endfunc

  func oTRMACPRE_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>'E')
    endwith
    endif
  endfunc

  add object oTRIMPSEG_2_11 as StdTrsField with uid="RUDGBEZBAF",rtseq=23,rtrep=.t.,;
    cFormVar="w_TRIMPSEG",value=0,;
    ToolTipText = "Totale imponibile dei documenti di competenza, ma emessi nel periodo seguente",;
    HelpContextID = 242716547,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIMPSEG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=248, Top=157, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTRIVASEG_2_12 as StdTrsField with uid="YHYNPVVCJJ",rtseq=24,rtrep=.t.,;
    cFormVar="w_TRIVASEG",value=0,;
    ToolTipText = "Totale imposta detraibile dei documenti di competenza, ma emessi nel periodo seg",;
    HelpContextID = 257855363,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIVASEG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=387, Top=157, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTRIVISEG_2_13 as StdTrsField with uid="YCFCLBPEYH",rtseq=25,rtrep=.t.,;
    cFormVar="w_TRIVISEG",value=0,;
    ToolTipText = "Totale imposta indetraibile dei documenti di competenza, ma emessi nel periodo S",;
    HelpContextID = 249466755,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIVISEG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=526, Top=157, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oTRIVISEG_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>'A')
    endwith
    endif
  endfunc

  add object oTRMACSEG_2_14 as StdTrsField with uid="NNZWESBVIP",rtseq=26,rtrep=.t.,;
    cFormVar="w_TRMACSEG",value=0,;
    ToolTipText = "Totale monte acquisti dei documenti di competenza, ma emessi nel periodo S",;
    HelpContextID = 257118083,;
    cTotal="", bFixedPos=.t., cQueryName = "TRMACSEG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=665, Top=157, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oTRMACSEG_2_14.mCond()
    with this.Parent.oContained
      return (.w_TRTIPREG='E')
    endwith
  endfunc

  func oTRMACSEG_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>'E')
    endwith
    endif
  endfunc

  add object oTRIMPFAD_2_15 as StdTrsField with uid="ATNCEJOFJP",rtseq=27,rtrep=.t.,;
    cFormVar="w_TRIMPFAD",value=0,;
    ToolTipText = "Totale imponibile dei documenti ad esigibilitÓ differita",;
    HelpContextID = 76050554,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIMPFAD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=248, Top=183, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTRIVAFAD_2_16 as StdTrsField with uid="UJVHZQWTYZ",rtseq=28,rtrep=.t.,;
    cFormVar="w_TRIVAFAD",value=0,;
    ToolTipText = "Totale imposta detraibile dei documenti ad esigibilitÓ differita",;
    HelpContextID = 60911738,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIVAFAD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=387, Top=183, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTRIVIFAD_2_17 as StdTrsField with uid="WUYRJDNTLU",rtseq=29,rtrep=.t.,;
    cFormVar="w_TRIVIFAD",value=0,;
    ToolTipText = "Totale imposta indetraibile dei documenti ad esigibilitÓ differita",;
    HelpContextID = 69300346,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIVIFAD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=526, Top=183, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oTRIVIFAD_2_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>'A')
    endwith
    endif
  endfunc

  add object oTRMACFAD_2_18 as StdTrsField with uid="RYWMXOCVRR",rtseq=30,rtrep=.t.,;
    cFormVar="w_TRMACFAD",value=0,;
    ToolTipText = "Totale monte acquisti dei documenti ad esigibilitÓ differita",;
    HelpContextID = 61649018,;
    cTotal="", bFixedPos=.t., cQueryName = "TRMACFAD",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=665, Top=183, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oTRMACFAD_2_18.mCond()
    with this.Parent.oContained
      return (.w_TRTIPREG='E')
    endwith
  endfunc

  func oTRMACFAD_2_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>'E')
    endwith
    endif
  endfunc

  add object oTRIMPIND_2_19 as StdTrsField with uid="WJGWEOTRAQ",rtseq=31,rtrep=.t.,;
    cFormVar="w_TRIMPIND",value=0,;
    ToolTipText = "Totale imponibile degli incassi/pagamenti effettuati ad esigibilitÓ differita",;
    HelpContextID = 126382202,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIMPIND",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=248, Top=209, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTRIVAIND_2_20 as StdTrsField with uid="EEESORUAMD",rtseq=32,rtrep=.t.,;
    cFormVar="w_TRIVAIND",value=0,;
    ToolTipText = "Totale imposta detraibile degli incassi/pagamenti effettuati ad esigibilitÓ dif",;
    HelpContextID = 111243386,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIVAIND",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=387, Top=209, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oTRIVIIND_2_21 as StdTrsField with uid="VSWXRAEVAW",rtseq=33,rtrep=.t.,;
    cFormVar="w_TRIVIIND",value=0,;
    ToolTipText = "Totale imposta indetraibile degli incassi/pagamenti effettuati ad esigibilitÓ diff.",;
    HelpContextID = 119631994,;
    cTotal="", bFixedPos=.t., cQueryName = "TRIVIIND",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=526, Top=209, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oTRIVIIND_2_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>'A')
    endwith
    endif
  endfunc

  add object oTRMACIND_2_23 as StdTrsField with uid="MRQATCNAGF",rtseq=35,rtrep=.t.,;
    cFormVar="w_TRMACIND",value=0,;
    ToolTipText = "Totale monte acquisti degli incassi/pagamenti effettuati ad esigibilitÓ diff.",;
    HelpContextID = 111980666,;
    cTotal="", bFixedPos=.t., cQueryName = "TRMACIND",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=665, Top=209, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oTRMACIND_2_23.mCond()
    with this.Parent.oContained
      return (.w_TRTIPREG='E')
    endwith
  endfunc

  func oTRMACIND_2_23.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>'E')
    endwith
    endif
  endfunc

  add object oTOTIMP_2_24 as StdTrsField with uid="HIIKWMXYUS",rtseq=36,rtrep=.t.,;
    cFormVar="w_TOTIMP",value=0,enabled=.f.,;
    HelpContextID = 240459062,;
    cTotal="", bFixedPos=.t., cQueryName = "TOTIMP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=248, Top=242, cSayPict=[v_PV(40+VVL)], cGetPict=[v_PV(40+VVL)]

  add object oTOTIVA_2_25 as StdTrsField with uid="UOVMWKAPZG",rtseq=37,rtrep=.t.,;
    cFormVar="w_TOTIVA",value=0,enabled=.f.,;
    HelpContextID = 266673462,;
    cTotal="", bFixedPos=.t., cQueryName = "TOTIVA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=387, Top=242, cSayPict=[v_PV(40+VVL)], cGetPict=[v_PV(40+VVL)]

  add object oTOTIVI_2_26 as StdTrsField with uid="GSIWJHFMLF",rtseq=38,rtrep=.t.,;
    cFormVar="w_TOTIVI",value=0,enabled=.f.,;
    HelpContextID = 132455734,;
    cTotal="", bFixedPos=.t., cQueryName = "TOTIVI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=526, Top=242, cSayPict=[v_PV(40+VVL)], cGetPict=[v_PV(40+VVL)]

  func oTOTIVI_2_26.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>'A')
    endwith
    endif
  endfunc

  add object oTOTMAC_2_27 as StdTrsField with uid="NNQADHENUQ",rtseq=39,rtrep=.t.,;
    cFormVar="w_TOTMAC",value=0,enabled=.f.,;
    HelpContextID = 10034486,;
    cTotal="", bFixedPos=.t., cQueryName = "TOTMAC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=665, Top=242, cSayPict=[v_PV(40+VVL)], cGetPict=[v_PV(40+VVL)]

  func oTOTMAC_2_27.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPREG<>'E')
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mriBodyRow as CPBodyRowCnt
  Width=61
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oTRCODIVA_2_1 as StdTrsField with uid="OMLGWSKAYW",rtseq=13,rtrep=.t.,;
    cFormVar="w_TRCODIVA",value=space(5),isprimarykey=.t.,;
    ToolTipText = "Codice IVA",;
    HelpContextID = 154529673,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_TRCODIVA"

  func oTRCODIVA_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCODIVA_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oTRCODIVA_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oTRCODIVA_2_1.readonly and this.parent.oTRCODIVA_2_1.isprimarykey)
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oTRCODIVA_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"",'',this.parent.oContained
   endif
  endproc
  proc oTRCODIVA_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_TRCODIVA
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oTRCODIVA_2_1.When()
    return(.t.)
  proc oTRCODIVA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oTRCODIVA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mri','PRI_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TR__ANNO=PRI_MAST.TR__ANNO";
  +" and "+i_cAliasName2+".TRNUMPER=PRI_MAST.TRNUMPER";
  +" and "+i_cAliasName2+".TRTIPREG=PRI_MAST.TRTIPREG";
  +" and "+i_cAliasName2+".TRNUMREG=PRI_MAST.TRNUMREG";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
