* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bpe                                                        *
*              Salva/elimina login/password SMTP                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-14                                                      *
* Last revis.: 2014-12-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bpe",oParentObject,m.pOPER)
return(i_retval)

define class tgsut_bpe as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_PSW = space(254)
  * --- WorkFile variables
  AUT_LPE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSUT_KLP (obsoleto)
    * --- Salva/Elimina Login e password per server SMTP
    * --- pOPER='S' Salva Login/Psw
    *     pOPER='E' Elmina Login/Psw
    * --- Utente:
    * --- Cripto la password
    this.w_PSW = CifraCnf( ALLTRIM(this.oParentObject.w_NEWPSW) , "C" )
    do case
      case this.pOPER = "S"
        * --- Insert/Update UTCODUTE, UTLOGIN, UTPSW
        * --- Utente:
        this.oParentObject.w_UTELOG = this.oParentObject.w_UTENTE
        * --- Try
        local bErr_03C1B5A8
        bErr_03C1B5A8=bTrsErr
        this.Try_03C1B5A8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Try
          local bErr_03C1C5C8
          bErr_03C1C5C8=bTrsErr
          this.Try_03C1C5C8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_ErrorMsg("Errore durante il salvataggio delle login e password dell'utente%1" ,48,"",chr(32) + ALLTRIM( STR( this.oParentObject.w_UTENTE ) ))
          endif
          bTrsErr=bTrsErr or bErr_03C1C5C8
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_03C1B5A8
        * --- End
      case this.pOPER = "E"
        * --- Delete UTCODUTE, UTLOGIN, UTPSW
        * --- Try
        local bErr_03C25878
        bErr_03C25878=bTrsErr
        this.Try_03C25878()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Non � possibile eliminare login e password dell'utente%1" ,48,"",chr(32) + ALLTRIM( STR( this.oParentObject.w_UTENTE ) ))
        endif
        bTrsErr=bTrsErr or bErr_03C25878
        * --- End
    endcase
  endproc
  proc Try_03C1B5A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into AUT_LPE
    i_nConn=i_TableProp[this.AUT_LPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AUT_LPE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.AUT_LPE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LPCODUTE"+",LPLOGIN"+",LPPSW"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_UTENTE),'AUT_LPE','LPCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LOGIN),'AUT_LPE','LPLOGIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PSW),'AUT_LPE','LPPSW');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LPCODUTE',this.oParentObject.w_UTENTE,'LPLOGIN',this.oParentObject.w_LOGIN,'LPPSW',this.w_PSW)
      insert into (i_cTable) (LPCODUTE,LPLOGIN,LPPSW &i_ccchkf. );
         values (;
           this.oParentObject.w_UTENTE;
           ,this.oParentObject.w_LOGIN;
           ,this.w_PSW;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg( "Salvate login e password dell'utente %1",.t.,,,ALLTRIM( STR( this.oParentObject.w_UTENTE ) ) )
    return
  proc Try_03C1C5C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AUT_LPE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AUT_LPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AUT_LPE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AUT_LPE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LPLOGIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LOGIN),'AUT_LPE','LPLOGIN');
      +",LPPSW ="+cp_NullLink(cp_ToStrODBC(this.w_PSW),'AUT_LPE','LPPSW');
          +i_ccchkf ;
      +" where ";
          +"LPCODUTE = "+cp_ToStrODBC(this.oParentObject.w_UTENTE);
             )
    else
      update (i_cTable) set;
          LPLOGIN = this.oParentObject.w_LOGIN;
          ,LPPSW = this.w_PSW;
          &i_ccchkf. ;
       where;
          LPCODUTE = this.oParentObject.w_UTENTE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg( "Salvate login e password dell'utente %1",.t.,,,ALLTRIM( STR( this.oParentObject.w_UTENTE ) ) )
    this.oParentObject.ecpQuit
    return
  proc Try_03C25878()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from AUT_LPE
    i_nConn=i_TableProp[this.AUT_LPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AUT_LPE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"LPCODUTE = "+cp_ToStrODBC(this.oParentObject.w_UTENTE);
             )
    else
      delete from (i_cTable) where;
            LPCODUTE = this.oParentObject.w_UTENTE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    ah_msg( "Eliminate login e password dell'utente %1",.t.,,,ALLTRIM( STR( this.oParentObject.w_UTENTE ) ) )
    * --- Ripulisco la maschera
    this.oParentObject.BlankRec()
    * --- Setto il fuoco sul campo w_UTENTE
     
 local l_obj 
 l_obj=this.oParentObject.GetCtrl("w_UTENTE") 
 l_obj.SetFocus()
    return


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AUT_LPE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
