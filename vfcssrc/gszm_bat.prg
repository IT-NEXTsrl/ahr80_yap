* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bat                                                        *
*              Menu contestuale attivita                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-15                                                      *
* Last revis.: 2014-02-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFUNZ,pATSERIAL,PWOraRinv
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bat",oParentObject,m.pFUNZ,m.pATSERIAL,m.PWOraRinv)
return(i_retval)

define class tgszm_bat as StdBatch
  * --- Local variables
  pFUNZ = space(1)
  pATSERIAL = space(15)
  PWOraRinv = ctot("")
  w_OBJECT = .NULL.
  w_ATSERIAL = space(15)
  w_CONFERMA = space(1)
  w_CURSOR = space(10)
  w_RETVAL = space(254)
  w_SERATT = space(20)
  w_MESS = space(254)
  w_ATFLPROM = space(1)
  w_ATPROMEM = ctot("")
  w_DataIniAttivita = ctot("")
  w_DataFinAttivita = ctot("")
  w_Durata = 0
  w_ATSTATUS = space(1)
  w_PADRE = .NULL.
  COD_PRA = space(15)
  w_GRAPAT = space(1)
  w_ATFLATRI = space(1)
  w_OK = .f.
  w_FLRINV = space(1)
  w_DATARINV = ctod("  /  /  ")
  w_ORERINV = space(2)
  w_MINRINV = space(2)
  w_DATINIOR = ctod("  /  /  ")
  w_OREINIOR = space(2)
  w_MININIOR = space(2)
  Prosegui = .f.
  w_TIPOATT = space(20)
  w_ATOGGETT = space(254)
  w_ATOGGETTOR = space(254)
  w_ATCAUATT = space(20)
  w_ATNOTPIA = space(0)
  w_ATGGPREA = 0
  w_ATNUMPRI = 0
  w_ATCODATT = space(5)
  w_ATSTAATT = 0
  w_AT_EMAIL = space(0)
  FL_NON_SOG_PRE = space(1)
  w_FL_ATTCOMPL = space(1)
  w_TIPO_RIS = space(1)
  w_SERIAL = space(20)
  w_ATCODNOM = space(15)
  w_ATCONTAT = space(5)
  w_ATOPERAT = 0
  w_ATCODESI = space(5)
  w_ATLOCALI = space(30)
  w_AT__ENTE = space(10)
  w_ATUFFICI = space(10)
  w_ATFLATRI = space(1)
  w_ATDATINI = ctot("")
  w_ATDATFIN = ctot("")
  w_ATPERSON = space(60)
  w_ATTELEFO = space(18)
  w_AT___FAX = space(18)
  w_ATCELLUL = space(18)
  w_ATCODLIS = space(5)
  w_ATCODVAL = space(3)
  w_CAITER = space(20)
  w_OLDATDATRIN = ctod("  /  /  ")
  w_OLDATTIPRIS = space(2)
  w_OLDUTCV = 0
  w_OLDUTDV = ctod("  /  /  ")
  w_OLDATSTATUS = space(1)
  w_CONTA = space(1)
  w_TIPGEN = space(1)
  w_FLGNOT = space(1)
  w_ONLYFIRST = space(1)
  w_LSERIAL = space(20)
  w_DATA_INI = ctot("")
  w_CACHKINI = space(1)
  w_CAITER = space(20)
  w_CACHKFOR = space(1)
  w_DESC_ATT = space(254)
  w_DES_PRAT = space(100)
  w_FLTRIS = space(1)
  w_TIPORIS = space(1)
  w_DATINIOR = ctod("  /  /  ")
  w_OREINIOR = space(2)
  w_MININIOR = space(2)
  w_ATOGGETTOR = space(254)
  w_ATCAUATT = space(20)
  w_FL_ATTCOMPL = space(1)
  w_MODSCHED = space(1)
  w_DATINI = ctod("  /  /  ")
  w_ORAINI = space(2)
  w_MININI = space(2)
  w_DATFIN = ctod("  /  /  ")
  w_ORAFIN = space(2)
  w_MINFIN = space(2)
  w_DATINIOR = ctod("  /  /  ")
  w_OREINIOR = space(2)
  w_MININIOR = space(2)
  w_DATFINOR = ctod("  /  /  ")
  w_OREFINOR = space(2)
  w_MINFINOR = space(2)
  w_ATOGGETTOR = space(254)
  w_ATCAUATT = space(20)
  w_ATCAITER = space(20)
  w_ATRIFSER = space(20)
  w_ATFLNOTI = space(1)
  w_ATSERIAL = space(20)
  w_SERDOC = space(10)
  w_RESOCON1 = space(0)
  w_ANNULLA = .f.
  w_MESBLOK = space(0)
  w_READAZI = space(5)
  w_PACHKCAR = space(1)
  w_ResMsg = .NULL.
  w_RETVAL = space(254)
  w_GIOSCA = 0
  w_SER_ATT = space(20)
  NUMRIGA = 0
  w_DATOBSO = ctod("  /  /  ")
  w_CNFLAPON = space(1)
  w_CNFLVALO = space(1)
  w_CNFLVALO1 = space(1)
  w_CNIMPORT = 0
  w_CNCALDIR = space(1)
  w_CNCOECAL = 0
  w_PARASS = 0
  w_FLAMPA = space(1)
  COD_PART = space(5)
  w_ENTE = space(10)
  w_CNTARTEM = space(1)
  w_CNTARCON = 0
  w_CONTRACN = space(5)
  w_TIPCONCO = space(1)
  w_LISCOLCO = space(5)
  w_STATUSCO = space(1)
  w_NUMDECUNI = 0
  CONTINUA = .f.
  w_TIPOPRAT = space(10)
  w_FLGIUD = space(1)
  w_CNMATOBB = space(1)
  w_CNASSCTP = space(1)
  w_CNCOMPLX = space(1)
  w_CNPROFMT = space(1)
  w_CNESIPOS = space(1)
  w_CNPERPLX = 0
  w_CNPERPOS = 0
  w_CNFLVMLQ = space(1)
  w_NUMRIG = 0
  w_CODSER = space(20)
  w_CODART = space(20)
  w_UNIMIS = space(3)
  w_TIPART = space(2)
  w_DESPREST = space(40)
  w_DESSUP = space(0)
  w_COSORA = 0
  FLDEFF = space(1)
  QUANTITA = 0
  PREZZO_UN = 0
  w_TATIPPRE = space(1)
  w_ONORARBI = space(1)
  w_FLPRPE = space(1)
  w_NOTIFICA = space(1)
  w_MANSION = space(5)
  w_TAIMPORT = 0
  PREZZO_MIN = 0
  PREZZO_MAX = 0
  w_CALPRZ = 0
  w_DAGAZUFF = space(6)
  w_ENTE_CAL = space(10)
  w_CAUATT = space(20)
  w_TAPARAM = space(1)
  COD_PRES = space(20)
  COD_ART = space(20)
  TIPO_ART = space(2)
  ROW_NUM = 0
  w_ATDATRIN = ctot("")
  w_MODSCHED = space(1)
  w_ATT_SERIAL = space(15)
  w_PAFLESAT = space(1)
  w_DESPRA = space(100)
  w_DATASYS = ctod("  /  /  ")
  w_PAATTRIS = space(20)
  w_CODPRA = space(15)
  w_MODSCHED = space(1)
  * --- WorkFile variables
  OFF_ATTI_idx=0
  OFFDATTI_idx=0
  RIN_PART_idx=0
  OFF_PART_idx=0
  CAN_TIER_idx=0
  CAU_ATTI_idx=0
  CAUMATTI_idx=0
  ART_ICOL_idx=0
  DIPENDEN_idx=0
  PRA_ENTI_idx=0
  KEY_ARTI_idx=0
  DET_GEN_idx=0
  VALUTE_idx=0
  ATT_DCOL_idx=0
  PRA_CONT_idx=0
  PAR_AGEN_idx=0
  LISTINI_idx=0
  PRA_TIPI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per avvio funzionalit� legate alle Attivit�, da men� contestuale (tasto destro)
    * --- A apri pratica
    *     C completa
    *     D evadi
    *     E elimina
    *     O attivit� scadute
    *     P perentorie
    *     R rinvio
    *     S sposta
    *     T pone attivit� in riserva
    *     K apre maschera Documenti associati
    *     W Completa e genera collegata
    *     J Genera collegata
    *     B Evadi e gen collegata
    *     L rinvio attivit� legata ad evento PolisWeb
    this.w_CONFERMA = "S"
    this.w_READAZI = i_codazi
    if TYPE("g_omenu.class") = "C"
      this.w_PADRE = g_oMenu.oParentObject
      * --- Assegno alla variabile il valore relativo alla chiave primaria 1 (ATSERIAL) delle Attivit�
      this.w_ATSERIAL = g_oMenu.getbyindexkeyvalue(1)
    else
      * --- Non lanciato da menu
      this.w_ATSERIAL = this.pATSERIAL
    endif
    * --- Read from OFF_ATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATFLATRI,ATOGGETT,ATCAUATT,ATDATINI,ATDATFIN,ATCAITER,ATRIFSER,ATCODPRA,ATSTATUS"+;
        " from "+i_cTable+" OFF_ATTI where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATFLATRI,ATOGGETT,ATCAUATT,ATDATINI,ATDATFIN,ATCAITER,ATRIFSER,ATCODPRA,ATSTATUS;
        from (i_cTable) where;
            ATSERIAL = this.w_ATSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ATFLATRI = NVL(cp_ToDate(_read_.ATFLATRI),cp_NullValue(_read_.ATFLATRI))
      this.w_ATOGGETT = NVL(cp_ToDate(_read_.ATOGGETT),cp_NullValue(_read_.ATOGGETT))
      this.w_ATCAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
      this.w_ATDATINI = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
      this.w_ATDATFIN = NVL((_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
      this.w_ATCAITER = NVL(cp_ToDate(_read_.ATCAITER),cp_NullValue(_read_.ATCAITER))
      this.w_ATRIFSER = NVL(cp_ToDate(_read_.ATRIFSER),cp_NullValue(_read_.ATRIFSER))
      this.COD_PRA = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
      this.w_ATSTATUS = NVL(cp_ToDate(_read_.ATSTATUS),cp_NullValue(_read_.ATSTATUS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Not IsRisAtt( this.w_ATSERIAL , this.w_ATFLATRI ) 
      ah_ErrorMsg("Operazione non consentita, attivit� riservata")
      i_retcode = 'stop'
      return
    endif
    do case
      case this.pFUNZ="E"
        * --- Eliminazione attivit�
        if Type("g_oMenu.oParentObject")="O" and Upper(g_oMenu.oParentObject.class)="TGSAG_AAT"
          g_oMenu.oParentObject.Ecpdelete()
        else
          this.w_OK = .T.
          * --- Read from DET_GEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DET_GEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DET_GEN_idx,2],.t.,this.DET_GEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MDSERIAL"+;
              " from "+i_cTable+" DET_GEN where ";
                  +"MDSERATT = "+cp_ToStrODBC(this.w_ATSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MDSERIAL;
              from (i_cTable) where;
                  MDSERATT = this.w_ATSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERATT = NVL(cp_ToDate(_read_.MDSERIAL),cp_NullValue(_read_.MDSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if I_ROWS<>0
            this.w_MESS = ah_Msgformat("Attenzione:attivit� inserita in un piano di generazione attivit� (%1)%0La cancellazione manuale di attivit� non comporta gli aggiornamenti automatici delle date prossima attivit� sugli elementi contratto%0Confermi ugualmente?",this.w_SERATT)
            this.w_OK = ah_YesNo(this.w_MESS)
          endif
          if this.w_OK
            this.w_CURSOR = SYS(2015)
            CREATE CURSOR (this.w_CURSOR) (ATSERIAL C(20))
            INSERT INTO (this.w_CURSOR) VALUES (this.w_ATSERIAL)
            this.w_RETVAL = GSAG_BEL(this, "ELIMSIN", this.w_CURSOR, .T.)
            if Not EMPTY(this.w_RETVAL)
              ah_ErrorMsg("%1",48,"",this.w_RETVAL)
            endif
          else
            this.w_CONFERMA = "N"
            ah_ErrorMsg("Operazione interrotta come richiesto",64)
          endif
        endif
      case this.pFUNZ="R" OR this.pFUNZ="L"
        * --- RINVIO ATTIVITA'
        *     
        *     Attenzione: si � deciso in data 30.10.07 che il rinvio di un'attivit� pu� essere eseguito soltanto con il
        *     tasto destro per evitare problemi in Gestione Attivit� (es. se si modificasse la data di rinvio di una
        *     attivit� gi� rinviata) : non � quindi pi� editabile la data del rinvio in Gestione Attivit�.
        * --- Read from CAUMATTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAUMATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CAFLRINV"+;
            " from "+i_cTable+" CAUMATTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_ATCAUATT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CAFLRINV;
            from (i_cTable) where;
                CACODICE = this.w_ATCAUATT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLRINV = NVL(cp_ToDate(_read_.CAFLRINV),cp_NullValue(_read_.CAFLRINV))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_FLRINV<>"S"
          ah_ErrorMsg("Operazione non consentita: il tipo dell'attivit� non gestisce il rinvio.")
          i_retcode = 'stop'
          return
        endif
        this.Prosegui = .T.
        this.w_CONFERMA = "N"
        this.w_DATARINV = ctod("  -  -  ")
        this.w_ORERINV = "09"
        this.w_MINRINV = "00"
        this.w_DATINIOR = TTOD(this.w_ATDATINI)
        this.w_OREINIOR = right("00"+ALLTRIM(STR(HOUR(this.w_ATDATINI),2,0)),2)
        this.w_MININIOR = right("00"+ALLTRIM(STR(MINUTE(this.w_ATDATINI),2,0)),2)
        this.w_ATOGGETTOR = this.w_ATOGGETT
        this.w_FL_ATTCOMPL = "N"
        * --- Legge le note attivit� (g_oMenu.getFieldsValue non riesce a leggere il campo memo ATNOTPIA)
        * --- Read from OFF_ATTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AT___FAX,AT__ENTE,AT_EMAIL,ATCAUATT,ATCELLUL,ATCODESI,ATCODLIS,ATCODNOM,ATCODVAL,ATCONTAT,ATDATFIN,ATDATINI,ATFLATRI,ATFLNOTI,ATFLPROM,ATLOCALI,ATNOTPIA,ATOPERAT,ATPERSON,ATPROMEM,ATTELEFO,ATTIPRIS,ATUFFICI,ATSTATUS,ATCAITER,ATDATRIN,UTCV,UTDV"+;
            " from "+i_cTable+" OFF_ATTI where ";
                +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AT___FAX,AT__ENTE,AT_EMAIL,ATCAUATT,ATCELLUL,ATCODESI,ATCODLIS,ATCODNOM,ATCODVAL,ATCONTAT,ATDATFIN,ATDATINI,ATFLATRI,ATFLNOTI,ATFLPROM,ATLOCALI,ATNOTPIA,ATOPERAT,ATPERSON,ATPROMEM,ATTELEFO,ATTIPRIS,ATUFFICI,ATSTATUS,ATCAITER,ATDATRIN,UTCV,UTDV;
            from (i_cTable) where;
                ATSERIAL = this.w_ATSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_AT___FAX = NVL(cp_ToDate(_read_.AT___FAX),cp_NullValue(_read_.AT___FAX))
          this.w_AT__ENTE = NVL(cp_ToDate(_read_.AT__ENTE),cp_NullValue(_read_.AT__ENTE))
          this.w_AT_EMAIL = NVL(cp_ToDate(_read_.AT_EMAIL),cp_NullValue(_read_.AT_EMAIL))
          this.w_ATCAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
          this.w_TIPOATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
          this.w_ATCELLUL = NVL(cp_ToDate(_read_.ATCELLUL),cp_NullValue(_read_.ATCELLUL))
          this.w_ATCODESI = NVL(cp_ToDate(_read_.ATCODESI),cp_NullValue(_read_.ATCODESI))
          this.w_ATCODLIS = NVL(cp_ToDate(_read_.ATCODLIS),cp_NullValue(_read_.ATCODLIS))
          this.w_ATCODNOM = NVL(cp_ToDate(_read_.ATCODNOM),cp_NullValue(_read_.ATCODNOM))
          this.w_ATCODVAL = NVL(cp_ToDate(_read_.ATCODVAL),cp_NullValue(_read_.ATCODVAL))
          this.w_ATCONTAT = NVL(cp_ToDate(_read_.ATCONTAT),cp_NullValue(_read_.ATCONTAT))
          this.w_DataFinAttivita = NVL((_read_.ATDATFIN),cp_NullValue(_read_.ATDATFIN))
          this.w_DataIniAttivita = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
          this.w_ATDATINI = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
          this.w_ATFLATRI = NVL(cp_ToDate(_read_.ATFLATRI),cp_NullValue(_read_.ATFLATRI))
          this.w_ATFLNOTI = NVL(cp_ToDate(_read_.ATFLNOTI),cp_NullValue(_read_.ATFLNOTI))
          this.w_ATFLPROM = NVL(cp_ToDate(_read_.ATFLPROM),cp_NullValue(_read_.ATFLPROM))
          this.w_ATLOCALI = NVL(cp_ToDate(_read_.ATLOCALI),cp_NullValue(_read_.ATLOCALI))
          this.w_ATNOTPIA = NVL(cp_ToDate(_read_.ATNOTPIA),cp_NullValue(_read_.ATNOTPIA))
          this.w_ATOPERAT = NVL(cp_ToDate(_read_.ATOPERAT),cp_NullValue(_read_.ATOPERAT))
          this.w_ATPERSON = NVL(cp_ToDate(_read_.ATPERSON),cp_NullValue(_read_.ATPERSON))
          this.w_ATPROMEM = NVL((_read_.ATPROMEM),cp_NullValue(_read_.ATPROMEM))
          this.w_ATTELEFO = NVL(cp_ToDate(_read_.ATTELEFO),cp_NullValue(_read_.ATTELEFO))
          this.w_TIPO_RIS = NVL(cp_ToDate(_read_.ATTIPRIS),cp_NullValue(_read_.ATTIPRIS))
          this.w_ATUFFICI = NVL(cp_ToDate(_read_.ATUFFICI),cp_NullValue(_read_.ATUFFICI))
          this.w_ATSTATUS = NVL(cp_ToDate(_read_.ATSTATUS),cp_NullValue(_read_.ATSTATUS))
          this.w_CAITER = NVL(cp_ToDate(_read_.ATCAITER),cp_NullValue(_read_.ATCAITER))
          this.w_OLDATDATRIN = NVL(cp_ToDate(_read_.ATDATRIN),cp_NullValue(_read_.ATDATRIN))
          this.w_OLDATTIPRIS = NVL(cp_ToDate(_read_.ATTIPRIS),cp_NullValue(_read_.ATTIPRIS))
          this.w_OLDUTCV = NVL(cp_ToDate(_read_.UTCV),cp_NullValue(_read_.UTCV))
          this.w_OLDUTDV = NVL(cp_ToDate(_read_.UTDV),cp_NullValue(_read_.UTDV))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_OLDATSTATUS = this.w_ATSTATUS
        this.w_Durata = this.w_DataFinAttivita-this.w_DataIniAttivita
        * --- Azzero i partecipanti
        * --- Delete from RIN_PART
        i_nConn=i_TableProp[this.RIN_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIN_PART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Inserimento partecipanti nella maschera di Rinvio
        this.w_CONTA = 0
        * --- Select from OFF_PART
        i_nConn=i_TableProp[this.OFF_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2],.t.,this.OFF_PART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PACODRIS,PATIPRIS,PAGRURIS  from "+i_cTable+" OFF_PART ";
              +" where PASERIAL="+cp_ToStrODBC(this.w_ATSERIAL)+"";
              +" order by CPROWORD";
               ,"_Curs_OFF_PART")
        else
          select PACODRIS,PATIPRIS,PAGRURIS from (i_cTable);
           where PASERIAL=this.w_ATSERIAL;
           order by CPROWORD;
            into cursor _Curs_OFF_PART
        endif
        if used('_Curs_OFF_PART')
          select _Curs_OFF_PART
          locate for 1=1
          do while not(eof())
          this.w_CONTA = this.w_CONTA+1
          * --- Insert into RIN_PART
          i_nConn=i_TableProp[this.RIN_PART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIN_PART_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RIN_PART_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PASERIAL"+",CPROWNUM"+",PACODRIS"+",PATIPRIS"+",PAGRURIS"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_ATSERIAL),'RIN_PART','PASERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_conta),'RIN_PART','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_OFF_PART.PACODRIS),'RIN_PART','PACODRIS');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_OFF_PART.PATIPRIS),'RIN_PART','PATIPRIS');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_OFF_PART.PAGRURIS),'RIN_PART','PAGRURIS');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PASERIAL',this.w_ATSERIAL,'CPROWNUM',this.w_conta,'PACODRIS',_Curs_OFF_PART.PACODRIS,'PATIPRIS',_Curs_OFF_PART.PATIPRIS,'PAGRURIS',_Curs_OFF_PART.PAGRURIS)
            insert into (i_cTable) (PASERIAL,CPROWNUM,PACODRIS,PATIPRIS,PAGRURIS &i_ccchkf. );
               values (;
                 this.w_ATSERIAL;
                 ,this.w_conta;
                 ,_Curs_OFF_PART.PACODRIS;
                 ,_Curs_OFF_PART.PATIPRIS;
                 ,_Curs_OFF_PART.PAGRURIS;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
            select _Curs_OFF_PART
            continue
          enddo
          use
        endif
        * --- Dopo il lancio di GSAG_KRV non si deve pi� usare l'oggetto g_oMenu poich� se si esegue un'operazione 
        *     tramite tasto destro su un campo di GSAG_KRV il suddetto oggetto risulta non pi� definito.
        this.w_ONLYFIRST = IIF(Empty(this.w_ORERINV) ,"N","P")
        if this.pFunz="L" AND VARTYPE(this.PWOraRinv)="T"
          this.w_DATARINV = TTOD(this.PWOraRinv)
          this.w_ORERINV = PADL(ALLTRIM(STR(HOUR(this.PWOraRinv),2,0)),2,"0")
          this.w_MINRINV = PADL(ALLTRIM(STR(MINUTE(this.PWOraRinv),2,0)),2,"0")
          this.w_CONFERMA = "S"
        else
          do GSAG_KRV with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Not empty(this.w_DATARINV)
          this.w_ATDATRIN = cp_CharToDatetime(DTOC(this.w_DATARINV)+" "+this.w_ORERINV+":"+this.w_MINRINV+":00")
          this.w_SERIAL = space(20)
          if Not Empty(this.w_CAITER) and this.w_FLGNOT $ "N-P"
            this.w_ATNOTPIA = ""
          endif
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_LSERIAL = this.w_SERIAL
          this.w_SERIAL = this.w_ATSERIAL
          this.w_DATA_INI = this.w_ATDATRIN
          * --- Cerco attivit� collegate
          if this.Prosegui
            this.Prosegui = .T.
            if Not Empty(this.w_CAITER) 
              if this.w_TIPGEN= "I"
                this.w_OK = GSAG_BAI(.Null.,this.w_CAITER,this.COD_PRA,this.w_DATINIOR,this.w_CACHKINI,this.w_CACHKFOR,this.w_ATCODVAL,this.w_ATSERIAL,"","","","","",this.w_ATCODNOM,this.w_ORERINV,this.w_MINRINV,"",this.w_ATNOTPIA,this.w_FLGNOT,this.w_ONLYFIRST)
              endif
            endif
            if Not Empty(this.w_ATCAITER) and Ah_Yesno("Si desidera visualizzare le attivit� collegate all'udienza rinviata")
              this.w_SERIAL = this.w_ATSERIAL
              this.w_CAITER = this.w_ATCAITER
              this.w_DATA_INI = this.w_DataIniAttivita
              this.w_DESC_ATT = this.w_ATOGGETT
              this.w_DES_PRAT = ""
              do GSAG_KAC with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        endif
      case this.pFUNZ="T"
        * --- PONE ATTIVITA' IN "RISERVA"
        * --- Read from OFF_ATTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATCAUATT,ATDATINI,ATCODPRA,ATOGGETT"+;
            " from "+i_cTable+" OFF_ATTI where ";
                +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATCAUATT,ATDATINI,ATCODPRA,ATOGGETT;
            from (i_cTable) where;
                ATSERIAL = this.w_ATSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ATCAUATT = NVL(cp_ToDate(_read_.ATCAUATT),cp_NullValue(_read_.ATCAUATT))
          this.w_DataIniAttivita = NVL((_read_.ATDATINI),cp_NullValue(_read_.ATDATINI))
          this.COD_PRA = NVL(cp_ToDate(_read_.ATCODPRA),cp_NullValue(_read_.ATCODPRA))
          this.w_ATOGGETTOR = NVL(cp_ToDate(_read_.ATOGGETT),cp_NullValue(_read_.ATOGGETT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from CAUMATTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAUMATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CAFLTRIS"+;
            " from "+i_cTable+" CAUMATTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_ATCAUATT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CAFLTRIS;
            from (i_cTable) where;
                CACODICE = this.w_ATCAUATT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLTRIS = NVL(cp_ToDate(_read_.CAFLTRIS),cp_NullValue(_read_.CAFLTRIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_FLTRIS<>"S"
          ah_ErrorMsg("Operazione non consentita: il tipo dell'attivit� non gestisce la riserva.")
          i_retcode = 'stop'
          return
        endif
        this.w_TIPORIS = SPACE(1)
        this.w_CONFERMA = "N"
        this.w_DATINIOR = TTOD(this.w_DataIniAttivita)
        this.w_OREINIOR = right("00"+ALLTRIM(STR(HOUR(this.w_DataIniAttivita),2,0)),2)
        this.w_MININIOR = right("00"+ALLTRIM(STR(MINUTE(this.w_DataIniAttivita),2,0)),2)
        this.w_FL_ATTCOMPL = "N"
        * --- Dopo il lancio di GSAG_KRS non si deve pi� usare l'oggetto g_oMenu poich� se si esegue un'operazione 
        *     tramite tasto destro su un campo di GSAG_KRS il suddetto oggetto risulta non pi� definito.
        do GSAG_KRS with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_CONFERMA="S"
          * --- begin transaction
          cp_BeginTrs()
          * --- Try
          local bErr_0361E5E8
          bErr_0361E5E8=bTrsErr
          this.Try_0361E5E8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Attenzione: impossibile porre in riserva l'attivit�.")
          endif
          bTrsErr=bTrsErr or bErr_0361E5E8
          * --- End
        endif
      case this.pFUNZ="S"
        * --- Spostamento date iniziale e finale attivit�
        this.w_DATINI = ctod("  -  -  ")
        this.w_ORAINI = "09"
        this.w_MININI = "00"
        this.w_DATFIN = ctod("  -  -  ")
        this.w_ORAFIN = "09"
        this.w_MINFIN = "00"
        * --- Le variabili w_ATOGGETT , w_ATCAUATT, w_ATDATINI, w_ATDATFIN, w_ATCAITER, w_ATRIFSER
        *     sono lette nella READ from OFF_ATTI prima del case
        this.w_ATOGGETTOR = this.w_ATOGGETT
        this.w_DATINIOR = TTOD( this.w_ATDATINI )
        this.w_OREINIOR = right("00"+ALLTRIM(STR(HOUR( this.w_ATDATINI ),2,0)),2)
        this.w_MININIOR = right("00"+ALLTRIM(STR(MINUTE( this.w_ATDATINI ),2,0)),2)
        this.w_DATFINOR = TTOD( this.w_ATDATFIN )
        this.w_OREFINOR = right("00"+ALLTRIM(STR(HOUR( this.w_ATDATFIN ),2,0)),2)
        this.w_MINFINOR = right("00"+ALLTRIM(STR(MINUTE( this.w_ATDATFIN ),2,0)),2)
        this.w_ATRIFSER = this.w_ATRIFSER
        * --- Invio post-it o e-mail di segnalazione modifica
        this.w_CURSOR = SYS(2015)
        CREATE CURSOR (this.w_CURSOR) (ATSERIAL C(20))
        INSERT INTO (this.w_CURSOR) VALUES (this.w_ATSERIAL)
        * --- verifico se posso cancellare documenti attivit� collegate
        * --- Dopo il lancio di GSAG_KSP non si deve pi� usare l'oggetto g_oMenu poich� se si esegue un'operazione 
        *     tramite tasto destro su un campo di GSAG_KSP il suddetto oggetto risulta non pi� definito.
        if EMPTY(this.w_RETVAL)
          * --- Read from PAR_AGEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PACHKCAR"+;
              " from "+i_cTable+" PAR_AGEN where ";
                  +"PACODAZI = "+cp_ToStrODBC(this.w_READAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PACHKCAR;
              from (i_cTable) where;
                  PACODAZI = this.w_READAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PACHKCAR = NVL(cp_ToDate(_read_.PACHKCAR),cp_NullValue(_read_.PACHKCAR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          Select (this.w_CURSOR) 
 Go top
          if GSAG_BEL(this, "SPOSTA", this.w_CURSOR, .T.)
            * --- Se il men� contestuale viene lanciato da Gestione Attivit�
            if Type("this.w_PADRE")="O" and upper(this.w_PADRE.cprg) = "GSAG_AAT"
              * --- Vengono aggiornati i campi dell'Attivit�
              this.w_PADRE.LoadRec()
            endif
          else
            if this.w_PACHKCAR="B"
              this.w_MESBLOK = this.w_ResMsg.ComposeMessage()
            else
              this.w_RESOCON1 = this.w_ResMsg.ComposeMessage()
            endif
            do GSVE_KLG with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_ResMsg = .NULL.
        else
          ah_ErrorMsg("Attenzione: impossibile eseguire lo spostamento delle date.%0%1",48,"",this.w_RETVAL)
        endif
      case this.pFUNZ $ "B-D"
        * --- Evasione attivit�
        if this.w_ATSTATUS="P"
          ah_ErrorMsg("Impossibile evadere un'attivit� completata.")
          i_retcode = 'stop'
          return
        endif
        this.w_CURSOR = SYS(2015)
        CREATE CURSOR (this.w_CURSOR) (ATSERIAL C(20))
        INSERT INTO (this.w_CURSOR) VALUES (this.w_ATSERIAL)
        this.w_RETVAL = GSAG_BEM(this, this.w_CURSOR, .T.)
        if !EMPTY(this.w_RETVAL)
          ah_ErrorMsg("%1",48,"",this.w_RETVAL)
        else
          if ISALT()
            this.Pag6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Se il men� contestuale viene lanciato da Gestione Attivit�
          if Type("this.w_PADRE")="O" and upper(this.w_PADRE.cprg) = "GSAG_AAT"
            * --- Vengono aggiornati i campi dell'Attivit�
            this.w_PADRE.LoadRec()
          endif
        endif
      case this.pFUNZ $ "C-W"
        * --- Imposta l'attivit� come Completeta
        * --- Controllo se l'attivit� � gi� completata
        if this.w_ATSTATUS= "P"
          ah_ErrorMsg("Attenzione: attivit� gi� completata.")
          i_retcode = 'stop'
          return
        endif
        this.w_CURSOR = SYS(2015)
        CREATE CURSOR (this.w_CURSOR) (ATSERIAL C(20))
        INSERT INTO (this.w_CURSOR) VALUES (this.w_ATSERIAL)
        this.w_RETVAL = GSAG_BEL(this, "COMPLETA", this.w_CURSOR, .T.)
        if EMPTY(this.w_RETVAL)
          if ISALT()
            this.Pag6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Se il men� contestuale viene lanciato da Gestione Attivit�
          if Type("this.w_PADRE")="O" and upper(this.w_PADRE.cprg) = "GSAG_AAT"
            * --- Vengono aggiornati i campi dell'Attivit�
            this.w_PADRE.LoadRec()
          endif
        else
          ah_ErrorMsg("%1",48,"",this.w_RETVAL)
        endif
      case this.pFUNZ="A"
        * --- Apre pratica
        if Not Empty(this.COD_PRA)
           
 LCODPRA=this.COD_PRA 
 =OPENGEST("A","gspr_acn","CNCODCAN",LCODPRA)
        else
          ah_ErrorMsg("Pratica non valorizzata")
        endif
      case this.pFUNZ="O" or this.pFUNZ="P"
        this.w_OBJECT = gsag_kra("L")
        * --- Read from PAR_AGEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PAGIOSCA"+;
            " from "+i_cTable+" PAR_AGEN where ";
                +"PACODAZI = "+cp_ToStrODBC(this.w_READAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PAGIOSCA;
            from (i_cTable) where;
                PACODAZI = this.w_READAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_GIOSCA = NVL(cp_ToDate(_read_.PAGIOSCA),cp_NullValue(_read_.PAGIOSCA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.pFUNZ="O"
          this.w_OBJECT.w_DATINI = cp_chartodate("    -  -  ")
          this.w_OBJECT.w_DATFIN = i_datsys-this.w_GIOSCA
          this.w_OBJECT.w_STATO = "N"
          this.w_OBJECT.caption = AH_MSGFORMAT("Elenco attivit� scadute")
        else
          this.w_OBJECT.w_PRIORITA = 4
          this.w_OBJECT.caption = AH_MSGFORMAT("Elenco attivit� in scadenza termine")
        endif
        this.w_OBJECT.mcalc(.t.)     
        this.w_OBJECT.NotifyEvent("Ricerca")     
        this.w_OBJECT = .null.
      case this.pFUNZ="K"
        * --- Legge eventuale documento associato all'attivit�
        * --- Read from ATT_DCOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ATT_DCOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATT_DCOL_idx,2],.t.,this.ATT_DCOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ADSERIAL"+;
            " from "+i_cTable+" ATT_DCOL where ";
                +"ADSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ADSERIAL;
            from (i_cTable) where;
                ADSERIAL = this.w_ATSERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SER_ATT = NVL(cp_ToDate(_read_.ADSERIAL),cp_NullValue(_read_.ADSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.w_SER_ATT)
          do GSAG_KDO with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          AH_ErrorMsg("Non sono presenti documenti associati")
        endif
    endcase
    do case
      case VarType(this.w_PADRE)="O" and upper(this.w_PADRE.cprg)="GSAG_KRA" AND this.w_CONFERMA="S"
        * --- Se il men� contestuale (default_OFF_ATTI.vmn) viene lanciato da Elenco Attivit�
        * --- ATTENZIONE: la seguente parte di codice deve essere eseguita alla fine del presente batch.
        this.w_OBJECT = this.w_PADRE
        * --- Creo un timer pubblico per aggiornare lo zoom in modo differito
        *     La classe � definita nell'area manuale
         
 public w_TIMER 
 w_TIMER = createobject("MyTimer", This.w_OBJECT) 
 w_TIMER.Interval = 100
      case VarType(this.w_PADRE)="O" and upper(this.w_PADRE.cprg)="GSAG_KRP"
        * --- Se il men� contestuale (GSAG_ZRP_OFF_ATTI.vmn) viene lanciato da Elenco Prestazioni
        GSAG_BSP(this.w_PADRE, "RICERCA_ZOOM")
    endcase
    if this.pFunz="L" AND VARTYPE(this.PWOraRinv)="T"
      i_retcode = 'stop'
      i_retval = this.w_LSERIAL
      return
    else
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_0361E5E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Nell'attivit� aggiorna il Tipo riserva e pone lo stato come 'Evasa' .
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATTIPRIS ="+cp_NullLink(cp_ToStrODBC(this.w_TIPORIS),'OFF_ATTI','ATTIPRIS');
      +",ATSTATUS ="+cp_NullLink(cp_ToStrODBC("F"),'OFF_ATTI','ATSTATUS');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OFF_ATTI','UTCV');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'OFF_ATTI','UTDV');
          +i_ccchkf ;
      +" where ";
          +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
             )
    else
      update (i_cTable) set;
          ATTIPRIS = this.w_TIPORIS;
          ,ATSTATUS = "F";
          ,UTCV = i_CODUTE;
          ,UTDV = SetInfoDate( g_CALUTD );
          &i_ccchkf. ;
       where;
          ATSERIAL = this.w_ATSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_FL_ATTCOMPL="S"
      * --- Nell'attivit� pone lo stato come 'completata'.
      * --- Write into OFF_ATTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATSTATUS ="+cp_NullLink(cp_ToStrODBC("P"),'OFF_ATTI','ATSTATUS');
        +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OFF_ATTI','UTCV');
        +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'OFF_ATTI','UTDV');
            +i_ccchkf ;
        +" where ";
            +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
               )
      else
        update (i_cTable) set;
            ATSTATUS = "P";
            ,UTCV = i_CODUTE;
            ,UTDV = SetInfoDate( g_CALUTD );
            &i_ccchkf. ;
         where;
            ATSERIAL = this.w_ATSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_FL_ATTCOMPL="S"
      * --- Generazione documenti interni dell'attivit�
      * --- Variabile per ripristinare il valore di g_SCHEDULER
      this.w_MODSCHED = "N"
      if VARTYPE(g_SCHEDULER)<>"C" 
        public g_SCHEDULER
        g_SCHEDULER="S"
        this.w_MODSCHED = "S"
      else
        if g_SCHEDULER<>"S"
          g_SCHEDULER="S"
          this.w_MODSCHED = "S"
        endif
      endif
      * --- Istanzio l'anagrafica
      this.w_OBJECT = GSAG_AAT()
      * --- Carico il record selezionato
      this.w_OBJECT.ECPFILTER()     
      this.w_OBJECT.w_ATSERIAL = this.w_ATSERIAL
      this.w_OBJECT.ECPSAVE()     
      * --- Vado in modifica
      this.w_OBJECT.ECPEDIT()     
      this.w_OBJECT.ECPSAVE()     
      this.w_OBJECT.ECPQUIT()     
      if this.w_MODSCHED="S"
        g_SCHEDULER=" "
      endif
    endif
    if ISALT()
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Se il men� contestuale viene lanciato da Gestione Attivit�
    if Type("this.w_PADRE")="O" and upper(this.w_PADRE.cprg) = "GSAG_AAT"
      * --- Vengono aggiornati i campi dell'Attivit�
      this.w_PADRE.LoadRec()
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- INSERIMENTO DEI PARTECIPANTI DELLA SINGOLA ATTIVITA'
    *                                                                           (tale pagina � eseguita sotto transazione - vedi pag. 1)
    this.NUMRIGA = 0
    * --- Legge i partecipanti sulla maschera di Rinvio (detail GSAG_MRP)
    * --- Select from RIN_PART
    i_nConn=i_TableProp[this.RIN_PART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIN_PART_idx,2],.t.,this.RIN_PART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select PACODRIS,PATIPRIS,PAGRURIS  from "+i_cTable+" RIN_PART ";
          +" where PASERIAL="+cp_ToStrODBC(this.w_ATSERIAL)+"";
          +" order by CPROWNUM";
           ,"_Curs_RIN_PART")
    else
      select PACODRIS,PATIPRIS,PAGRURIS from (i_cTable);
       where PASERIAL=this.w_ATSERIAL;
       order by CPROWNUM;
        into cursor _Curs_RIN_PART
    endif
    if used('_Curs_RIN_PART')
      select _Curs_RIN_PART
      locate for 1=1
      do while not(eof())
      * --- Read from DIPENDEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIPENDEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DPDTOBSO"+;
          " from "+i_cTable+" DIPENDEN where ";
              +"DPCODICE = "+cp_ToStrODBC(_Curs_RIN_PART.PACODRIS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DPDTOBSO;
          from (i_cTable) where;
              DPCODICE = _Curs_RIN_PART.PACODRIS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.DPDTOBSO),cp_NullValue(_read_.DPDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se il partecipante non � obsoleto
      if EMPTY(this.w_DATOBSO) OR this.w_DATOBSO>this.w_DATARINV
        this.NUMRIGA = this.NUMRIGA + 1
        * --- Inserisce partecipante
        * --- Insert into OFF_PART
        i_nConn=i_TableProp[this.OFF_PART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_PART_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_PART_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PASERIAL"+",CPROWNUM"+",CPROWORD"+",PACODRIS"+",PATIPRIS"+",PAGRURIS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'OFF_PART','PASERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.NUMRIGA),'OFF_PART','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.NUMRIGA * 10),'OFF_PART','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_RIN_PART.PACODRIS),'OFF_PART','PACODRIS');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_RIN_PART.PATIPRIS),'OFF_PART','PATIPRIS');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_RIN_PART.PAGRURIS),'OFF_PART','PAGRURIS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PASERIAL',this.w_SERIAL,'CPROWNUM',this.NUMRIGA,'CPROWORD',this.NUMRIGA * 10,'PACODRIS',_Curs_RIN_PART.PACODRIS,'PATIPRIS',_Curs_RIN_PART.PATIPRIS,'PAGRURIS',_Curs_RIN_PART.PAGRURIS)
          insert into (i_cTable) (PASERIAL,CPROWNUM,CPROWORD,PACODRIS,PATIPRIS,PAGRURIS &i_ccchkf. );
             values (;
               this.w_SERIAL;
               ,this.NUMRIGA;
               ,this.NUMRIGA * 10;
               ,_Curs_RIN_PART.PACODRIS;
               ,_Curs_RIN_PART.PATIPRIS;
               ,_Curs_RIN_PART.PAGRURIS;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
        select _Curs_RIN_PART
        continue
      enddo
      use
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- INSERIMENTO DELLE PRESTAZIONI DELLA SINGOLA ATTIVITA'
    *                                                                              (tale pagina � eseguita sotto transazione - vedi pag. 1)
    * --- Numero decimali per valori unitari
    this.w_CNFLAPON = space(1)
    this.w_CNFLVALO = space(1)
    this.w_CNIMPORT = 0
    this.w_CNCALDIR = space(1)
    this.w_CNCOECAL = 0
    this.w_PARASS = 0
    this.w_FLAMPA = "N"
    if NOT(EMPTY(this.COD_PRA) OR ISNULL(this.COD_PRA))
      * --- Legge i dati relativi alla pratica dell'attivit� da rinviare
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CN__ENTE,CNTARTEM,CNTARCON,CNCONTRA,CNFLDIND,CNTIPPRA,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNFLVMLQ"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.COD_PRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNFLAPON,CNFLVALO,CNIMPORT,CNCALDIR,CNCOECAL,CNPARASS,CNFLAMPA,CN__ENTE,CNTARTEM,CNTARCON,CNCONTRA,CNFLDIND,CNTIPPRA,CNMATOBB,CNASSCTP,CNCOMPLX,CNPROFMT,CNESIPOS,CNPERPLX,CNPERPOS,CNFLVMLQ;
          from (i_cTable) where;
              CNCODCAN = this.COD_PRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CNFLAPON = NVL(cp_ToDate(_read_.CNFLAPON),cp_NullValue(_read_.CNFLAPON))
        this.w_CNFLVALO = NVL(cp_ToDate(_read_.CNFLVALO),cp_NullValue(_read_.CNFLVALO))
        this.w_CNIMPORT = NVL(cp_ToDate(_read_.CNIMPORT),cp_NullValue(_read_.CNIMPORT))
        this.w_CNCALDIR = NVL(cp_ToDate(_read_.CNCALDIR),cp_NullValue(_read_.CNCALDIR))
        this.w_CNCOECAL = NVL(cp_ToDate(_read_.CNCOECAL),cp_NullValue(_read_.CNCOECAL))
        this.w_PARASS = NVL(cp_ToDate(_read_.CNPARASS),cp_NullValue(_read_.CNPARASS))
        this.w_FLAMPA = NVL(cp_ToDate(_read_.CNFLAMPA),cp_NullValue(_read_.CNFLAMPA))
        this.w_ENTE = NVL(cp_ToDate(_read_.CN__ENTE),cp_NullValue(_read_.CN__ENTE))
        this.w_CNTARTEM = NVL(cp_ToDate(_read_.CNTARTEM),cp_NullValue(_read_.CNTARTEM))
        this.w_CNTARCON = NVL(cp_ToDate(_read_.CNTARCON),cp_NullValue(_read_.CNTARCON))
        this.w_CONTRACN = NVL(cp_ToDate(_read_.CNCONTRA),cp_NullValue(_read_.CNCONTRA))
        this.w_CNFLVALO1 = NVL(cp_ToDate(_read_.CNFLDIND),cp_NullValue(_read_.CNFLDIND))
        this.w_TIPOPRAT = NVL(cp_ToDate(_read_.CNTIPPRA),cp_NullValue(_read_.CNTIPPRA))
        this.w_CNMATOBB = NVL(cp_ToDate(_read_.CNMATOBB),cp_NullValue(_read_.CNMATOBB))
        this.w_CNASSCTP = NVL(cp_ToDate(_read_.CNASSCTP),cp_NullValue(_read_.CNASSCTP))
        this.w_CNCOMPLX = NVL(cp_ToDate(_read_.CNCOMPLX),cp_NullValue(_read_.CNCOMPLX))
        this.w_CNPROFMT = NVL(cp_ToDate(_read_.CNPROFMT),cp_NullValue(_read_.CNPROFMT))
        this.w_CNESIPOS = NVL(cp_ToDate(_read_.CNESIPOS),cp_NullValue(_read_.CNESIPOS))
        this.w_CNPERPLX = NVL(cp_ToDate(_read_.CNPERPLX),cp_NullValue(_read_.CNPERPLX))
        this.w_CNPERPOS = NVL(cp_ToDate(_read_.CNPERPOS),cp_NullValue(_read_.CNPERPOS))
        this.w_CNFLVMLQ = NVL(cp_ToDate(_read_.CNFLVMLQ),cp_NullValue(_read_.CNFLVMLQ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Legge la classificazione del tipo pratica
      * --- Read from PRA_TIPI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRA_TIPI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRA_TIPI_idx,2],.t.,this.PRA_TIPI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TPFLGIUD"+;
          " from "+i_cTable+" PRA_TIPI where ";
              +"TPCODICE = "+cp_ToStrODBC(this.w_TIPOPRAT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TPFLGIUD;
          from (i_cTable) where;
              TPCODICE = this.w_TIPOPRAT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLGIUD = NVL(cp_ToDate(_read_.TPFLGIUD),cp_NullValue(_read_.TPFLGIUD))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT(EMPTY(this.w_CONTRACN) OR ISNULL(this.w_CONTRACN))
        * --- Legge i dati relativi al contratto della pratica dell'attivit� da rinviare
        * --- Read from PRA_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRA_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRA_CONT_idx,2],.t.,this.PRA_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CPTIPCON,CPLISCOL,CPSTATUS"+;
            " from "+i_cTable+" PRA_CONT where ";
                +"CPCODCON = "+cp_ToStrODBC(this.w_CONTRACN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CPTIPCON,CPLISCOL,CPSTATUS;
            from (i_cTable) where;
                CPCODCON = this.w_CONTRACN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPCONCO = NVL(cp_ToDate(_read_.CPTIPCON),cp_NullValue(_read_.CPTIPCON))
          this.w_LISCOLCO = NVL(cp_ToDate(_read_.CPLISCOL),cp_NullValue(_read_.CPLISCOL))
          this.w_STATUSCO = NVL(cp_ToDate(_read_.CPSTATUS),cp_NullValue(_read_.CPSTATUS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    * --- Numero riga
    * --- Costo orario responsabile prestazione
    * --- Legge il numero di decimali per valori unitari
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECUNI"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_ATCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECUNI;
        from (i_cTable) where;
            VACODVAL = this.w_ATCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NUMDECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_NUMRIG = 0
    this.w_CAUATT = this.w_TIPOATT
    * --- Ciclo sulle prestazioni della causale
    * --- Select from CAU_ATTI
    i_nConn=i_TableProp[this.CAU_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_ATTI_idx,2],.t.,this.CAU_ATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CACODSER,CADESSER,CADESAGG,CAFLDEFF,CAFLRESP,CAKEYART  from "+i_cTable+" CAU_ATTI ";
          +" where CACODICE = "+cp_ToStrODBC(this.w_CAUATT)+"";
           ,"_Curs_CAU_ATTI")
    else
      select CACODSER,CADESSER,CADESAGG,CAFLDEFF,CAFLRESP,CAKEYART from (i_cTable);
       where CACODICE = this.w_CAUATT;
        into cursor _Curs_CAU_ATTI
    endif
    if used('_Curs_CAU_ATTI')
      select _Curs_CAU_ATTI
      locate for 1=1
      do while not(eof())
      this.w_CODSER = iif(Isahe(),Nvl(_Curs_CAU_ATTI.CAKEYART,Space(41)),Nvl(_Curs_CAU_ATTI.CACODSER,Space(20)))
      this.w_DESPREST = _Curs_CAU_ATTI.CADESSER
      this.w_DESSUP = _Curs_CAU_ATTI.CADESAGG
      this.FLDEFF = _Curs_CAU_ATTI.CAFLDEFF
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODSER);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART;
          from (i_cTable) where;
              CACODICE = this.w_CODSER;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARTIPART,ARPRESTA,ARUNMIS1,ARSTASUP,ARSTACOD,ARFLPRPE,ARPARAME"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARTIPART,ARPRESTA,ARUNMIS1,ARSTASUP,ARSTACOD,ARFLPRPE,ARPARAME;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
        this.w_TATIPPRE = NVL(cp_ToDate(_read_.ARPRESTA),cp_NullValue(_read_.ARPRESTA))
        this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
        this.w_ONORARBI = NVL(cp_ToDate(_read_.ARSTASUP),cp_NullValue(_read_.ARSTASUP))
        this.w_NOTIFICA = NVL(cp_ToDate(_read_.ARSTACOD),cp_NullValue(_read_.ARSTACOD))
        this.w_FLPRPE = NVL(cp_ToDate(_read_.ARFLPRPE),cp_NullValue(_read_.ARFLPRPE))
        this.w_TAPARAM = NVL(cp_ToDate(_read_.ARPARAME),cp_NullValue(_read_.ARPARAME))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_UNIMIS = NVL(this.w_UNIMIS, SPACE(3))
      this.QUANTITA = IIF(this.w_TIPART="DE", 0, 1)
      this.CONTINUA = .T.
      * --- Legge i partecipanti sulla maschera di Rinvio (detail GSAG_MRP)
      * --- Select from RIN_PART
      i_nConn=i_TableProp[this.RIN_PART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIN_PART_idx,2],.t.,this.RIN_PART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PACODRIS  from "+i_cTable+" RIN_PART ";
            +" where PASERIAL="+cp_ToStrODBC(this.w_ATSERIAL)+"";
             ,"_Curs_RIN_PART")
      else
        select PACODRIS from (i_cTable);
         where PASERIAL=this.w_ATSERIAL;
          into cursor _Curs_RIN_PART
      endif
      if used('_Curs_RIN_PART')
        select _Curs_RIN_PART
        locate for 1=1
        do while not(eof())
        * --- Se siamo sul primo partecipante oppure se � attivo il check "Prestazione ripetuta"
        if this.CONTINUA OR _Curs_CAU_ATTI.CAFLRESP = "S"
          this.CONTINUA = .F.
          this.w_NUMRIG = this.w_NUMRIG+1
          this.COD_PART = _Curs_RIN_PART.PACODRIS
          * --- Lettura costo orario e mansione del responsabile della prestazione
          * --- Read from DIPENDEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIPENDEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPCOSORA,DPMANSION"+;
              " from "+i_cTable+" DIPENDEN where ";
                  +"DPCODICE = "+cp_ToStrODBC(this.COD_PART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPCOSORA,DPMANSION;
              from (i_cTable) where;
                  DPCODICE = this.COD_PART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_COSORA = NVL(cp_ToDate(_read_.DPCOSORA),cp_NullValue(_read_.DPCOSORA))
            this.w_MANSION = NVL(cp_ToDate(_read_.DPMANSION),cp_NullValue(_read_.DPMANSION))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Se � un diritto con valore pratica indeterminabile viene considerato il check
          *     "Calcolo dei diritti per valore indeterminabile" presente sulla pratica.
          if this.w_TATIPPRE $ "RC" AND this.w_CNFLVALO $ "IPS"
            this.w_CNFLVALO = this.w_CNFLVALO1
            this.w_CNCALDIR = "M"
            this.w_CNCOECAL = 50
          endif
          * --- Valore Pratica
          do case
            case this.w_CNFLVALO="D"
              this.w_TAIMPORT = this.w_CNIMPORT
            case this.w_CNFLVALO="I"
              this.w_TAIMPORT = -1
            case this.w_CNFLVALO$"PS"
              this.w_TAIMPORT = -2
          endcase
          * --- Se sono valorizzati il listino e la pratica all'interno dell'attivit� da rinviare
          if NOT(EMPTY(this.COD_PRA) OR ISNULL(this.COD_PRA)) AND NOT(EMPTY(this.w_ATCODLIS) OR ISNULL(this.w_ATCODLIS))
            * --- Dichiarazione array prezzi
            DECLARE ARRPRE (4,1)
            * --- Azzero l'Array che verr� riempito dalla Funzione
            ARRPRE(1)=0
            this.w_ENTE_CAL = ""
            if !empty(this.w_ENTE)
              * --- Read from PRA_ENTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PRA_ENTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRA_ENTI_idx,2],.t.,this.PRA_ENTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "EPCALTAR"+;
                  " from "+i_cTable+" PRA_ENTI where ";
                      +"EPCODICE = "+cp_ToStrODBC(this.w_ENTE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  EPCALTAR;
                  from (i_cTable) where;
                      EPCODICE = this.w_ENTE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_ENTE_CAL = NVL(cp_ToDate(_read_.EPCALTAR),cp_NullValue(_read_.EPCALTAR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if ALLTRIM(NVL(this.w_ENTE_CAL, "")) == ""
                this.w_ENTE_CAL = this.w_ENTE
              endif
            endif
            * --- Calcolo del prezzo unitario
            if IsAlt()
              do case
                case this.w_TATIPPRE="P" AND (this.w_CNTARTEM="C" OR (this.w_CNTARTEM="D" AND this.w_TIPCONCO<>"L" AND this.w_STATUSCO="I"))
                  * --- Se Prestazione a tempo e (Tipo di tariffazione a tempo uguale a Tariffa Concordata o 
                  *     (Tipo di tariffazione a tempo uguale a Tariffa Da Contratto e Tipo condizioni del contratto diverso da monte ore/listino collegato)):
                  *     applicazione tariffa concordata
                  this.PREZZO_UN = this.w_CNTARCON
                  this.PREZZO_MIN = this.w_CNTARCON
                  this.PREZZO_MAX = this.w_CNTARCON
                  this.w_DAGAZUFF = ""
                case this.w_TATIPPRE="P" AND this.w_CNTARTEM="D" AND this.w_TIPCONCO="L" AND this.w_STATUSCO="I"
                  * --- Se Prestazione a tempo e Tipo di tariffazione a tempo uguale a Tariffa Da Contratto e Tipo condizioni del contratto uguale a Da monte ore/listino collegato
                  *     applicazione listino collegato (da contratto)
                  this.w_GRAPAT = "N"
                  this.w_CALPRZ = CALPRAT(this.w_CODART, this.w_TATIPPRE, this.w_ONORARBI, this.w_FLPRPE, this.w_NOTIFICA, this.w_CNFLAPON, this.w_LISCOLCO, this.w_GRAPAT, this.w_ENTE_CAL, this.COD_PART, this.w_MANSION, this.w_DATARINV, this.w_CNFLVALO, this.w_TAIMPORT, this.w_CNCALDIR, this.w_CNCOECAL, this.w_PARASS, this.w_FLAMPA, @ARRPRE, this.w_NUMDECUNI, .F., "*NNNNN"+this.w_CNFLVMLQ+this.w_TAPARAM, 0, 0 )
                  this.PREZZO_UN = ARRPRE(1)
                  this.PREZZO_MIN = ARRPRE(2)
                  this.PREZZO_MAX = ARRPRE(3)
                  this.w_DAGAZUFF = ARRPRE(4)
                otherwise
                  * --- Caso normale
                  *     applicazione listino (della pratica)
                  * --- Read from LISTINI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.LISTINI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "LSGRAPAT"+;
                      " from "+i_cTable+" LISTINI where ";
                          +"LSCODLIS = "+cp_ToStrODBC(this.w_ATCODLIS);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      LSGRAPAT;
                      from (i_cTable) where;
                          LSCODLIS = this.w_ATCODLIS;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_GRAPAT = NVL(cp_ToDate(_read_.LSGRAPAT),cp_NullValue(_read_.LSGRAPAT))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.w_CALPRZ = CALPRAT(this.w_CODART, this.w_TATIPPRE, this.w_ONORARBI, this.w_FLPRPE, this.w_NOTIFICA, this.w_CNFLAPON, this.w_ATCODLIS, this.w_GRAPAT, this.w_ENTE_CAL, this.COD_PART, this.w_MANSION, this.w_DATARINV, this.w_CNFLVALO, this.w_TAIMPORT, this.w_CNCALDIR, this.w_CNCOECAL, this.w_PARASS, this.w_FLAMPA, @ARRPRE, this.w_NUMDECUNI, .F., this.w_FLGIUD+this.w_CNMATOBB+this.w_CNASSCTP+this.w_CNCOMPLX+this.w_CNPROFMT+this.w_CNESIPOS+this.w_CNFLVMLQ+this.w_TAPARAM, this.w_CNPERPLX,this.w_CNPERPOS )
                  this.PREZZO_UN = ARRPRE(1)
                  this.PREZZO_MIN = ARRPRE(2)
                  this.PREZZO_MAX = ARRPRE(3)
                  this.w_DAGAZUFF = ARRPRE(4)
              endcase
            else
              this.PREZZO_UN = 0
              this.PREZZO_MIN = 0
              this.PREZZO_MAX = 0
              this.w_DAGAZUFF = ""
            endif
          else
            this.PREZZO_UN = 0
            this.PREZZO_MIN = 0
            this.PREZZO_MAX = 0
            this.w_DAGAZUFF = ""
          endif
          * --- Inserisce prestazione
          * --- Insert into OFFDATTI
          i_nConn=i_TableProp[this.OFFDATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFFDATTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CPROWNUM"+",CPROWORD"+",DACODATT"+",DACODICE"+",DACODOPE"+",DACODRES"+",DACOSINT"+",DADATMOD"+",DADESAGG"+",DADESATT"+",DAFLDEFF"+",DAGAZUFF"+",DAMINEFF"+",DAOREEFF"+",DAPREMAX"+",DAPREMIN"+",DAPREZZO"+",DAQTAMOV"+",DASERIAL"+",DAUNIMIS"+",DAVALRIG"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'OFFDATTI','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG * 10),'OFFDATTI','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODSER),'OFFDATTI','DACODATT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODSER),'OFFDATTI','DACODICE');
            +","+cp_NullLink(cp_ToStrODBC(i_codute),'OFFDATTI','DACODOPE');
            +","+cp_NullLink(cp_ToStrODBC(this.COD_PART),'OFFDATTI','DACODRES');
            +","+cp_NullLink(cp_ToStrODBC(0),'OFFDATTI','DACOSINT');
            +","+cp_NullLink(cp_ToStrODBC(i_datsys),'OFFDATTI','DADATMOD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'OFFDATTI','DADESAGG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESPREST),'OFFDATTI','DADESATT');
            +","+cp_NullLink(cp_ToStrODBC(this.FLDEFF),'OFFDATTI','DAFLDEFF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DAGAZUFF),'OFFDATTI','DAGAZUFF');
            +","+cp_NullLink(cp_ToStrODBC(0),'OFFDATTI','DAMINEFF');
            +","+cp_NullLink(cp_ToStrODBC(0),'OFFDATTI','DAOREEFF');
            +","+cp_NullLink(cp_ToStrODBC(this.PREZZO_MAX),'OFFDATTI','DAPREMAX');
            +","+cp_NullLink(cp_ToStrODBC(this.PREZZO_MIN),'OFFDATTI','DAPREMIN');
            +","+cp_NullLink(cp_ToStrODBC(this.PREZZO_UN),'OFFDATTI','DAPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(this.QUANTITA),'OFFDATTI','DAQTAMOV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'OFFDATTI','DASERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'OFFDATTI','DAUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(this.QUANTITA * this.PREZZO_UN),'OFFDATTI','DAVALRIG');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',this.w_NUMRIG,'CPROWORD',this.w_NUMRIG * 10,'DACODATT',this.w_CODSER,'DACODICE',this.w_CODSER,'DACODOPE',i_codute,'DACODRES',this.COD_PART,'DACOSINT',0,'DADATMOD',i_datsys,'DADESAGG',this.w_DESSUP,'DADESATT',this.w_DESPREST,'DAFLDEFF',this.FLDEFF,'DAGAZUFF',this.w_DAGAZUFF)
            insert into (i_cTable) (CPROWNUM,CPROWORD,DACODATT,DACODICE,DACODOPE,DACODRES,DACOSINT,DADATMOD,DADESAGG,DADESATT,DAFLDEFF,DAGAZUFF,DAMINEFF,DAOREEFF,DAPREMAX,DAPREMIN,DAPREZZO,DAQTAMOV,DASERIAL,DAUNIMIS,DAVALRIG &i_ccchkf. );
               values (;
                 this.w_NUMRIG;
                 ,this.w_NUMRIG * 10;
                 ,this.w_CODSER;
                 ,this.w_CODSER;
                 ,i_codute;
                 ,this.COD_PART;
                 ,0;
                 ,i_datsys;
                 ,this.w_DESSUP;
                 ,this.w_DESPREST;
                 ,this.FLDEFF;
                 ,this.w_DAGAZUFF;
                 ,0;
                 ,0;
                 ,this.PREZZO_MAX;
                 ,this.PREZZO_MIN;
                 ,this.PREZZO_UN;
                 ,this.QUANTITA;
                 ,this.w_SERIAL;
                 ,this.w_UNIMIS;
                 ,this.QUANTITA * this.PREZZO_UN;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
          select _Curs_RIN_PART
          continue
        enddo
        use
      endif
        select _Curs_CAU_ATTI
        continue
      enddo
      use
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione prestazioni ad importo zero e di tipo  'A valore'
    * --- Select from OFFDATTI
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2],.t.,this.OFFDATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select DACODATT,DAPREZZO,CPROWNUM,DACODICE  from "+i_cTable+" OFFDATTI ";
          +" where DASERIAL="+cp_ToStrODBC(this.w_ATSERIAL)+"";
           ,"_Curs_OFFDATTI")
    else
      select DACODATT,DAPREZZO,CPROWNUM,DACODICE from (i_cTable);
       where DASERIAL=this.w_ATSERIAL;
        into cursor _Curs_OFFDATTI
    endif
    if used('_Curs_OFFDATTI')
      select _Curs_OFFDATTI
      locate for 1=1
      do while not(eof())
      this.COD_PRES = iif(Isahe(),_Curs_OFFDATTI.DACODICE,_Curs_OFFDATTI.DACODATT)
      this.ROW_NUM = _Curs_OFFDATTI.CPROWNUM
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.COD_PRES);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART;
          from (i_cTable) where;
              CACODICE = this.COD_PRES;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.COD_ART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARTIPART"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.COD_ART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARTIPART;
          from (i_cTable) where;
              ARCODART = this.COD_ART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.TIPO_ART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.TIPO_ART=="FO" AND _Curs_OFFDATTI.DAPREZZO=0
        * --- Try
        local bErr_0389D0E0
        bErr_0389D0E0=bTrsErr
        this.Try_0389D0E0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0389D0E0
        * --- End
      endif
        select _Curs_OFFDATTI
        continue
      enddo
      use
    endif
  endproc
  proc Try_0389D0E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from OFFDATTI
    i_nConn=i_TableProp[this.OFFDATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFFDATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DASERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.ROW_NUM);
             )
    else
      delete from (i_cTable) where;
            DASERIAL = this.w_ATSERIAL;
            and CPROWNUM = this.ROW_NUM;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_CONFERMA="S"
      * --- Try
      local bErr_038A04A0
      bErr_038A04A0=bTrsErr
      this.Try_038A04A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.Prosegui = .F.
        ah_ErrorMsg("Attenzione: impossibile eseguire il rinvio.")
      endif
      bTrsErr=bTrsErr or bErr_038A04A0
      * --- End
      if this.Prosegui 
        if this.w_ATFLPROM=="S" AND !EMPTY(this.w_ATPROMEM)
          * --- C'� promemoria, deve essere shiftato dello stesso tempo che intercorre tra il "vecchio" atdatini ed il nuovo ricalcolato
          this.w_ATPROMEM = this.w_ATPROMEM+(cp_CharToDatetime(DTOC(this.w_DATARINV)+" "+this.w_ORERINV+":"+this.w_MINRINV+":00")-this.w_DataIniAttivita)
        else
          this.w_ATPROMEM = cp_CharToDateTime("  -  -       :  :  ")
          this.w_ATFLPROM = "N"
        endif
        * --- Try
        local bErr_036008B8
        bErr_036008B8=bTrsErr
        this.Try_036008B8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_036008B8
        * --- End
      endif
    endif
  endproc
  proc Try_038A04A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Nell'attivit� aggiorna la data e l'ora di rinvio e pone lo stato come 'Evasa' .
    * --- Nell'attivit� pone lo stato come 'completata'.
    this.w_ATSTATUS = IIF(this.w_FL_ATTCOMPL="S","P","F")
    * --- In presenza di un'attivit� di tipo riserva devo azzerarla
    * --- Write into OFF_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATDATRIN ="+cp_NullLink(cp_ToStrODBC(this.w_ATDATRIN),'OFF_ATTI','ATDATRIN');
      +",ATSTATUS ="+cp_NullLink(cp_ToStrODBC(this.w_ATSTATUS),'OFF_ATTI','ATSTATUS');
      +",ATTIPRIS ="+cp_NullLink(cp_ToStrODBC(SPACE(1)),'OFF_ATTI','ATTIPRIS');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OFF_ATTI','UTCV');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'OFF_ATTI','UTDV');
          +i_ccchkf ;
      +" where ";
          +"ATSERIAL = "+cp_ToStrODBC(this.w_ATSERIAL);
             )
    else
      update (i_cTable) set;
          ATDATRIN = this.w_ATDATRIN;
          ,ATSTATUS = this.w_ATSTATUS;
          ,ATTIPRIS = SPACE(1);
          ,UTCV = i_CODUTE;
          ,UTDV = SetInfoDate( g_CALUTD );
          &i_ccchkf. ;
       where;
          ATSERIAL = this.w_ATSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_FL_ATTCOMPL="S"
      * --- Generazione documenti interni dell'attivit�
      * --- Variabile per ripristinare il valore di g_SCHEDULER
      this.w_MODSCHED = "N"
      if VARTYPE(g_SCHEDULER)<>"C" 
        public g_SCHEDULER
        g_SCHEDULER="S"
        this.w_MODSCHED = "S"
      else
        if g_SCHEDULER<>"S"
          g_SCHEDULER="S"
          this.w_MODSCHED = "S"
        endif
      endif
      * --- Istanzio l'anagrafica
      this.w_OBJECT = GSAG_AAT()
      * --- Carico il record selezionato
      this.w_OBJECT.ECPFILTER()     
      this.w_OBJECT.w_ATSERIAL = this.w_ATSERIAL
      this.w_OBJECT.ECPSAVE()     
      * --- Vado in modifica
      this.w_OBJECT.ECPEDIT()     
      this.w_OBJECT.ECPSAVE()     
      this.w_OBJECT.ECPQUIT()     
      if this.w_MODSCHED="S"
        g_SCHEDULER=" "
      endif
    endif
    * --- Se il men� contestuale viene lanciato da Gestione Attivit�
    if Type("this.w_PADRE")="O" and upper(this.w_PADRE.cprg) = "GSAG_AAT"
      * --- Vengono aggiornati i campi dell'Attivit�
      this.w_PADRE.LoadRec()
    endif
    return
  proc Try_036008B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.OFF_ATTI_IDX, 3]
    * --- Legge il progressivo, lo incrementa di 1 (ponendo il valore ottenuto in w_SERIAL) ed aggiorna la tabella
    cp_NextTableProg(this, i_Conn, "SEATT", "i_codazi,w_SERIAL")
    * --- Legge campi dal tipo attivit�
    * --- Read from CAUMATTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAUMATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2],.t.,this.CAUMATTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CAGGPREA,CAPRIORI,CATIPATT,CADISPON,CAFLNSAP"+;
        " from "+i_cTable+" CAUMATTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_TIPOATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CAGGPREA,CAPRIORI,CATIPATT,CADISPON,CAFLNSAP;
        from (i_cTable) where;
            CACODICE = this.w_TIPOATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ATGGPREA = NVL(cp_ToDate(_read_.CAGGPREA),cp_NullValue(_read_.CAGGPREA))
      this.w_ATNUMPRI = NVL(cp_ToDate(_read_.CAPRIORI),cp_NullValue(_read_.CAPRIORI))
      this.w_ATCODATT = NVL(cp_ToDate(_read_.CATIPATT),cp_NullValue(_read_.CATIPATT))
      this.w_ATSTAATT = NVL(cp_ToDate(_read_.CADISPON),cp_NullValue(_read_.CADISPON))
      this.FL_NON_SOG_PRE = NVL(cp_ToDate(_read_.CAFLNSAP),cp_NullValue(_read_.CAFLNSAP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Inserisce Attivit�
    * --- Insert into OFF_ATTI
    i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"UTDV"+",UTDC"+",UTCV"+",UTCC"+",ATUFFICI"+",ATTIPRIS"+",ATTELEFO"+",ATSTATUS"+",ATSTAATT"+",ATSERIAL"+",ATRIFSER"+",ATRIFDOC"+",ATPROMEM"+",ATPERSON"+",ATPERCOM"+",ATOREEFF"+",ATOPERAT"+",ATOGGETT"+",ATNUMPRI"+",ATNUMGIO"+",ATNOTPIA"+",ATMINEFF"+",ATLOCALI"+",ATKEYOUT"+",ATGGPREA"+",ATFLPROM"+",ATFLNOTI"+",ATFLATRI"+",ATDATRIN"+",ATDATPRO"+",ATDATOUT"+",ATDATINI"+",ATDATFIN"+",ATCONTAT"+",ATCODVAL"+",ATCODPRA"+",ATCODNOM"+",ATCODLIS"+",ATCODESI"+",ATCODATT"+",ATCELLUL"+",ATCAUATT"+",ATCAITER"+",AT_ESITO"+",AT_EMAIL"+",AT__ENTE"+",AT___FAX"+",ATDATDOC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'OFF_ATTI','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'OFF_ATTI','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'OFF_ATTI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATUFFICI),'OFF_ATTI','ATUFFICI');
      +","+cp_NullLink(cp_ToStrODBC(space(1)),'OFF_ATTI','ATTIPRIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATTELEFO),'OFF_ATTI','ATTELEFO');
      +","+cp_NullLink(cp_ToStrODBC("D"),'OFF_ATTI','ATSTATUS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATSTAATT),'OFF_ATTI','ATSTAATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'OFF_ATTI','ATSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(space(20)),'OFF_ATTI','ATRIFSER');
      +","+cp_NullLink(cp_ToStrODBC(space(10)),'OFF_ATTI','ATRIFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATPROMEM),'OFF_ATTI','ATPROMEM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATPERSON),'OFF_ATTI','ATPERSON');
      +","+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','ATPERCOM');
      +","+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','ATOREEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATOPERAT),'OFF_ATTI','ATOPERAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATOGGETT),'OFF_ATTI','ATOGGETT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATNUMPRI),'OFF_ATTI','ATNUMPRI');
      +","+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','ATNUMGIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATNOTPIA),'OFF_ATTI','ATNOTPIA');
      +","+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','ATMINEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATLOCALI),'OFF_ATTI','ATLOCALI');
      +","+cp_NullLink(cp_ToStrODBC(0),'OFF_ATTI','ATKEYOUT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATGGPREA),'OFF_ATTI','ATGGPREA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATFLPROM),'OFF_ATTI','ATFLPROM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATFLNOTI),'OFF_ATTI','ATFLNOTI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATFLATRI),'OFF_ATTI','ATFLATRI');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDatetime("  -  -       :  :  ")),'OFF_ATTI','ATDATRIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATDATINI),'OFF_ATTI','ATDATPRO');
      +","+cp_NullLink(cp_ToStrODBC(i_datsys),'OFF_ATTI','ATDATOUT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATDATRIN),'OFF_ATTI','ATDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATDATRIN+this.w_Durata),'OFF_ATTI','ATDATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCONTAT),'OFF_ATTI','ATCONTAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCODVAL),'OFF_ATTI','ATCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.COD_PRA),'OFF_ATTI','ATCODPRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCODNOM),'OFF_ATTI','ATCODNOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCODLIS),'OFF_ATTI','ATCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCODESI),'OFF_ATTI','ATCODESI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCODATT),'OFF_ATTI','ATCODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ATCELLUL),'OFF_ATTI','ATCELLUL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPOATT),'OFF_ATTI','ATCAUATT');
      +","+cp_NullLink(cp_ToStrODBC(Space(20)),'OFF_ATTI','ATCAITER');
      +","+cp_NullLink(cp_ToStrODBC(space(0)),'OFF_ATTI','AT_ESITO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AT_EMAIL),'OFF_ATTI','AT_EMAIL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AT__ENTE),'OFF_ATTI','AT__ENTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AT___FAX),'OFF_ATTI','AT___FAX');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATARINV),'OFF_ATTI','ATDATDOC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'UTDV',cp_CharToDate("  -  -    "),'UTDC',SetInfoDate( g_CALUTD ),'UTCV',0,'UTCC',i_CODUTE,'ATUFFICI',this.w_ATUFFICI,'ATTIPRIS',space(1),'ATTELEFO',this.w_ATTELEFO,'ATSTATUS',"D",'ATSTAATT',this.w_ATSTAATT,'ATSERIAL',this.w_SERIAL,'ATRIFSER',space(20),'ATRIFDOC',space(10))
      insert into (i_cTable) (UTDV,UTDC,UTCV,UTCC,ATUFFICI,ATTIPRIS,ATTELEFO,ATSTATUS,ATSTAATT,ATSERIAL,ATRIFSER,ATRIFDOC,ATPROMEM,ATPERSON,ATPERCOM,ATOREEFF,ATOPERAT,ATOGGETT,ATNUMPRI,ATNUMGIO,ATNOTPIA,ATMINEFF,ATLOCALI,ATKEYOUT,ATGGPREA,ATFLPROM,ATFLNOTI,ATFLATRI,ATDATRIN,ATDATPRO,ATDATOUT,ATDATINI,ATDATFIN,ATCONTAT,ATCODVAL,ATCODPRA,ATCODNOM,ATCODLIS,ATCODESI,ATCODATT,ATCELLUL,ATCAUATT,ATCAITER,AT_ESITO,AT_EMAIL,AT__ENTE,AT___FAX,ATDATDOC &i_ccchkf. );
         values (;
           cp_CharToDate("  -  -    ");
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,i_CODUTE;
           ,this.w_ATUFFICI;
           ,space(1);
           ,this.w_ATTELEFO;
           ,"D";
           ,this.w_ATSTAATT;
           ,this.w_SERIAL;
           ,space(20);
           ,space(10);
           ,this.w_ATPROMEM;
           ,this.w_ATPERSON;
           ,0;
           ,0;
           ,this.w_ATOPERAT;
           ,this.w_ATOGGETT;
           ,this.w_ATNUMPRI;
           ,0;
           ,this.w_ATNOTPIA;
           ,0;
           ,this.w_ATLOCALI;
           ,0;
           ,this.w_ATGGPREA;
           ,this.w_ATFLPROM;
           ,this.w_ATFLNOTI;
           ,this.w_ATFLATRI;
           ,cp_CharToDatetime("  -  -       :  :  ");
           ,this.w_ATDATINI;
           ,i_datsys;
           ,this.w_ATDATRIN;
           ,this.w_ATDATRIN+this.w_Durata;
           ,this.w_ATCONTAT;
           ,this.w_ATCODVAL;
           ,this.COD_PRA;
           ,this.w_ATCODNOM;
           ,this.w_ATCODLIS;
           ,this.w_ATCODESI;
           ,this.w_ATCODATT;
           ,this.w_ATCELLUL;
           ,this.w_TIPOATT;
           ,Space(20);
           ,space(0);
           ,this.w_AT_EMAIL;
           ,this.w_AT__ENTE;
           ,this.w_AT___FAX;
           ,this.w_DATARINV;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Inserisce Partecipanti attivit�
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Se la causale ha il check "Non soggetta a prestazione" disattivato
    if this.FL_NON_SOG_PRE # "S"
      * --- Inserisce Prestazioni attivit�
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo attivit� non evase
    * --- Se l'attivit� � associata ad una pratica
    if NOT EMPTY(this.COD_PRA)
      this.w_DATASYS = i_datsys
      * --- Lettura parametro Controllo inesistenza attivit�
      * --- Read from PAR_AGEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_AGEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_AGEN_idx,2],.t.,this.PAR_AGEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PAFLESAT,PAATTRIS"+;
          " from "+i_cTable+" PAR_AGEN where ";
              +"PACODAZI = "+cp_ToStrODBC(i_codazi);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PAFLESAT,PAATTRIS;
          from (i_cTable) where;
              PACODAZI = i_codazi;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PAFLESAT = NVL(cp_ToDate(_read_.PAFLESAT),cp_NullValue(_read_.PAFLESAT))
        this.w_PAATTRIS = NVL(cp_ToDate(_read_.PAATTRIS),cp_NullValue(_read_.PAATTRIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_PAFLESAT="S"
        * --- Attivit� non evase con data uguale o successiva a quella di ingresso
        * --- Select from OFF_ATTI
        i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ATSERIAL  from "+i_cTable+" OFF_ATTI ";
              +" where ATCODPRA="+cp_ToStrODBC(this.COD_PRA)+" AND ATDATINI>= "+cp_ToStrODBC(this.w_DATASYS)+" AND (ATSTATUS='T' OR ATSTATUS='D' OR ATSTATUS='I' )";
               ,"_Curs_OFF_ATTI")
        else
          select ATSERIAL from (i_cTable);
           where ATCODPRA=this.COD_PRA AND ATDATINI>= this.w_DATASYS AND (ATSTATUS="T" OR ATSTATUS="D" OR ATSTATUS="I" );
            into cursor _Curs_OFF_ATTI
        endif
        if used('_Curs_OFF_ATTI')
          select _Curs_OFF_ATTI
          locate for 1=1
          do while not(eof())
          this.w_ATT_SERIAL = _Curs_OFF_ATTI.ATSERIAL
            select _Curs_OFF_ATTI
            continue
          enddo
          use
        endif
        * --- In assenza di attivit� non evase
        if EMPTY(this.w_ATT_SERIAL) 
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNDESCAN"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(this.COD_PRA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNDESCAN;
              from (i_cTable) where;
                  CNCODCAN = this.COD_PRA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESPRA = NVL(cp_ToDate(_read_.CNDESCAN),cp_NullValue(_read_.CNDESCAN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if ah_Yesno("Attenzione: nessuna attivit� da evadere, in corso o provvisoria presente nella pratica %1 a partire dal %2.%0Si desidera caricare un'altra attivit�?" , "", alltrim(this.COD_PRA)+" ("+alltrim(this.w_DESPRA)+")", dtoc(this.w_DATASYS))
            this.w_CODPRA = this.COD_PRA
            Gsag_bra(this,"N","A","",this.w_PAATTRIS)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.pFUNZ$ "W-J-B"
      * --- Generazione attivit� collegata
      this.w_MODSCHED = "N"
      if VARTYPE(g_SCHEDULER)<>"C" 
        public g_SCHEDULER
        g_SCHEDULER="S"
        this.w_MODSCHED = "S"
      else
        if g_SCHEDULER<>"S"
          g_SCHEDULER="S"
          this.w_MODSCHED = "S"
        endif
      endif
      * --- Istanzio l'anagrafica
      this.w_OBJECT = GSAG_AAT()
      * --- Carico il record selezionato
      this.w_OBJECT.ECPFILTER()     
      this.w_OBJECT.w_ATSERIAL = this.w_ATSERIAL
      this.w_OBJECT.ECPSAVE()     
      * --- Vado in modifica
      this.w_OBJECT.ECPEDIT()     
      if this.w_MODSCHED="S"
        g_SCHEDULER=" "
      endif
      do GSAG_KCL with this.w_OBJECT
      if Type("this.w_OBJECT.class")="C"
        this.w_OBJECT.ECPQUIT()     
      endif
      if Type("this.w_OBJECT.class")="C"
        this.w_OBJECT.ECPQUIT()     
      endif
      this.w_OBJECT = .Null.
    endif
  endproc


  proc Init(oParentObject,pFUNZ,pATSERIAL,PWOraRinv)
    this.pFUNZ=pFUNZ
    this.pATSERIAL=pATSERIAL
    this.PWOraRinv=PWOraRinv
    this.w_ResMsg=createobject("Ah_Message")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,18)]
    this.cWorkTables[1]='OFF_ATTI'
    this.cWorkTables[2]='OFFDATTI'
    this.cWorkTables[3]='RIN_PART'
    this.cWorkTables[4]='OFF_PART'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='CAU_ATTI'
    this.cWorkTables[7]='CAUMATTI'
    this.cWorkTables[8]='ART_ICOL'
    this.cWorkTables[9]='DIPENDEN'
    this.cWorkTables[10]='PRA_ENTI'
    this.cWorkTables[11]='KEY_ARTI'
    this.cWorkTables[12]='DET_GEN'
    this.cWorkTables[13]='VALUTE'
    this.cWorkTables[14]='ATT_DCOL'
    this.cWorkTables[15]='PRA_CONT'
    this.cWorkTables[16]='PAR_AGEN'
    this.cWorkTables[17]='LISTINI'
    this.cWorkTables[18]='PRA_TIPI'
    return(this.OpenAllTables(18))

  proc CloseCursors()
    if used('_Curs_OFF_PART')
      use in _Curs_OFF_PART
    endif
    if used('_Curs_RIN_PART')
      use in _Curs_RIN_PART
    endif
    if used('_Curs_CAU_ATTI')
      use in _Curs_CAU_ATTI
    endif
    if used('_Curs_RIN_PART')
      use in _Curs_RIN_PART
    endif
    if used('_Curs_OFFDATTI')
      use in _Curs_OFFDATTI
    endif
    if used('_Curs_OFF_ATTI')
      use in _Curs_OFF_ATTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gszm_bat
  enddef
  
  define class MyTimer as timer
    * -- Ulteriore propriet� risprtto alla classe standard timer
    pParent=.null.
    
  proc init(p_parent)
    * -- Viene eseguita al momento della creazione dell'oggetto MyTimer
    this.pParent=p_parent
    interval=0     && In tal modo non viene eseguita la proc Timer()
   
  proc Timer()
    * -- Viene eseguita quando viene posto Interval a 100
    this.pParent.NotifyEvent("Ricerca")
    this.interval=0
    release w_TIMER
    this.destroy()
  endproc 
  
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFUNZ,pATSERIAL,PWOraRinv"
endproc
