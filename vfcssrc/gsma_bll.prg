* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bll                                                        *
*              Visualizza livello di servizio                                  *
*                                                                              *
*      Author: ZUCCHETTI SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-02-07                                                      *
* Last revis.: 2011-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,PTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bll",oParentObject,m.PTipo)
return(i_retval)

define class tgsma_bll as StdBatch
  * --- Local variables
  PTipo = space(1)
  w_MSK = .NULL.
  w_ZOOM = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_MSK = this.OparentObject
    if this.pTIPO = "E"
      if Upper(This.oParentObject.Class )="TGSMA_SLS"
        this.w_ZOOM = this.w_MSK.w_ZOOMLIV
        do case
          case this.oParentObject.w_TIPOANA="R"
            * --- Ritardo medio di consegna
            this.w_ZOOM.cCpQueryName = "QUERY\GSMA4SLS"
          case this.oParentObject.w_TIPOANA="P"
            * --- Percentuale ordini inevasi
            this.w_ZOOM.cCpQueryName = "QUERY\GSMA7SLS"
          case this.oParentObject.w_TIPOANA="S"
            * --- Scostamento medio contrattato
            this.w_ZOOM.cCpQueryName = "QUERY\GSMA12SLS"
        endcase
        this.w_MSK.NotifyEvent("Interroga")     
      else
        * --- chiamata da GSAR_KCD e GSAR_KFO
        if !EMPTY(NVL(this.oParentObject.w_CODCON,"")) AND this.oParentObject.w_BANAV
          do case
            case this.oParentObject.w_TIPOANA="R"
              * --- Ritardo medio di consegna
              vq_exec("QUERY\GSMA4SLS",this,"valore") 
 select valore 
 go top
              this.oParentObject.w_VALORE = cp_ROUND(valore.VALORE,5)
            case this.oParentObject.w_TIPOANA="P"
              * --- Percentuale ordini inevasi
              vq_exec("QUERY\GSMA7SLS",this,"valore") 
 select valore 
 go top
              this.oParentObject.w_VALORE = cp_ROUND(valore.VALORE,5)
            case this.oParentObject.w_TIPOANA="S"
              vq_exec("QUERY\GSMA12SLS",this,"valore") 
 select valore 
 go top
              this.oParentObject.w_VALORE = cp_ROUND(valore.VALORE,5)
          endcase
        else
          this.oParentObject.w_VALORE = 0
          this.oParentObject.w_BANAV = .T.
        endif
      endif
      USE IN SELECT("valore")
    else
      this.w_ZOOM = this.w_MSK.w_ZOOMLIV
      SELECT (this.w_ZOOM.cCursor)
      if RECCOUNT() > 0
        select * from (this.w_ZOOM.cCursor) into cursor "__TMP__"
        && ###start remove from setup###
        if g_ADHOCONE
          CP_CHPRN(FULLPATH("..\..\exe\QUERY\GSMA_SLS.XLT"),"",this.oParentObject)
        else
          && ###end remove from setup###
          CP_CHPRN("QUERY\GSMA_SLS.XLT","",this.oParentObject)
          && ###start remove from setup###
        endif
        && ###end remove from setup###
      else
        AH_ERRORMSG("Non ci sono dati da stampare",48,"")
      endif
      USE IN SELECT("__TMP__")
    endif
  endproc


  proc Init(oParentObject,PTipo)
    this.PTipo=PTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="PTipo"
endproc
