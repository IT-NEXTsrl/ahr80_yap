* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_b04                                                        *
*              Controlla validit� calendario                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-04-28                                                      *
* Last revis.: 2010-05-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEVENTO,pCALENDARIO,pESCLUSIONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_b04",oParentObject,m.pEVENTO,m.pCALENDARIO,m.pESCLUSIONE)
return(i_retval)

define class tgsar_b04 as StdBatch
  * --- Local variables
  pEVENTO = space(1)
  pCALENDARIO = space(5)
  pESCLUSIONE = space(1)
  w_CALENDARIO = space(5)
  w_FOUND = .f.
  w_DATAINIZIALE = ctod("  /  /  ")
  w_CAGIOLAV = space(1)
  * --- WorkFile variables
  CAL_AZIE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pEVENTO = E : Modificata la combo di esclusione
    * --- pEVENTO = C : Modificato il calendario
    * --- pCALENDARIO : calendario da controllare
    * --- pESCLUSIONE : 'N' nessuna, 'C' da calendario
    this.w_CALENDARIO = this.pCALENDARIO
    * --- Se non c'� esclusione non ci sono calendari da controllare
    if this.pESCLUSIONE = "N"
      this.w_CALENDARIO = "     "
    endif
    this.w_FOUND = .F.
    this.w_DATAINIZIALE = i_DATSYS
    this.w_CAGIOLAV = "S"
    if NOT EMPTY( this.w_CALENDARIO )
      * --- Select from CAL_AZIE
      i_nConn=i_TableProp[this.CAL_AZIE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAL_AZIE_idx,2],.t.,this.CAL_AZIE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" CAL_AZIE ";
            +" where CACODCAL = "+cp_ToStrODBC(this.w_CALENDARIO)+" AND CAGIORNO > "+cp_ToStrODBC(this.w_DATAINIZIALE)+" AND CAGIOLAV = "+cp_ToStrODBC(this.w_CAGIOLAV)+"";
             ,"_Curs_CAL_AZIE")
      else
        select * from (i_cTable);
         where CACODCAL = this.w_CALENDARIO AND CAGIORNO > this.w_DATAINIZIALE AND CAGIOLAV = this.w_CAGIOLAV;
          into cursor _Curs_CAL_AZIE
      endif
      if used('_Curs_CAL_AZIE')
        select _Curs_CAL_AZIE
        locate for 1=1
        do while not(eof())
        this.w_FOUND = .T.
        exit
          select _Curs_CAL_AZIE
          continue
        enddo
        use
      endif
      if NOT this.w_FOUND
        AH_ERRORMSG("Attenzione: il calendario selezionato non contiene giorni lavorativi per le date future")
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_FOUND
    return
  endproc


  proc Init(oParentObject,pEVENTO,pCALENDARIO,pESCLUSIONE)
    this.pEVENTO=pEVENTO
    this.pCALENDARIO=pCALENDARIO
    this.pESCLUSIONE=pESCLUSIONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAL_AZIE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CAL_AZIE')
      use in _Curs_CAL_AZIE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEVENTO,pCALENDARIO,pESCLUSIONE"
endproc
