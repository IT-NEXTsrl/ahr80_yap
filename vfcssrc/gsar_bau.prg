* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bau                                                        *
*              Autonumber cli/for                                              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_11]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-10                                                      *
* Last revis.: 2006-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bau",oParentObject,m.pParam)
return(i_retval)

define class tgsar_bau as StdBatch
  * --- Local variables
  pParam = space(1)
  w_OK = .f.
  * --- WorkFile variables
  PAR_RIOR_idx=0
  SED_STOR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pParam="A"
        * --- Aggiusta il Codice Cliente/Fornitore calcolato dall'Autonumber alla effettiva lunghezza della Picture Parametrica p_CLF
        * --- Chiamato all'evento New record da (GSAR_ACL e GSAR_AFR)
        if g_CFNUME="S"
          if LEN(ALLTRIM(p_CLF))<>0
            this.oParentObject.w_ANCODICE = RIGHT(this.oParentObject.w_ANCODICE, LEN(ALLTRIM(p_CLF)))
          endif
        else
          * --- Anche se non gestito, l'autonumber ritorna sempre una stringa di zeri in questo caso, deve essere suotata
          this.oParentObject.w_ANCODICE = SPACE(15)
        endif
        this.oParentObject.op_ANCODICE = this.oParentObject.w_ANCODICE
      case this.pParam="D"
        * --- Esegue controlli in Cancellazione
        this.w_OK = .T.
        * --- Select from PAR_RIOR
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PRCODFOR,PRTIPCON  from "+i_cTable+" PAR_RIOR ";
              +" where PRCODFOR="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" AND PRTIPCON="+cp_ToStrODBC(this.oParentObject.w_ANTIPCON)+"";
               ,"_Curs_PAR_RIOR")
        else
          select PRCODFOR,PRTIPCON from (i_cTable);
           where PRCODFOR=this.oParentObject.w_ANCODICE AND PRTIPCON=this.oParentObject.w_ANTIPCON;
            into cursor _Curs_PAR_RIOR
        endif
        if used('_Curs_PAR_RIOR')
          select _Curs_PAR_RIOR
          locate for 1=1
          do while not(eof())
          this.w_OK = .F.
          Exit
            select _Curs_PAR_RIOR
            continue
          enddo
          use
        endif
        if Not this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=ah_MsgFormat("Attenzione, fornitore utilizzato come fornitore abituale negli articoli, impossibile cancellare")
        endif
      case this.pParam="P"
        * --- Eseguo controlli al cambiare della paritta iva
        if This.oParentObject.cFunction="Edit"
          * --- Select from SED_STOR
          i_nConn=i_TableProp[this.SED_STOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SED_STOR_idx,2],.t.,this.SED_STOR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select SSTIPCON,SSCODICE,SSPARIVA  from "+i_cTable+" SED_STOR ";
                +" where SSTIPCON="+cp_ToStrODBC(this.oParentObject.w_ANTIPCON)+" AND SSCODICE="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" ";
                 ,"_Curs_SED_STOR")
          else
            select SSTIPCON,SSCODICE,SSPARIVA from (i_cTable);
             where SSTIPCON=this.oParentObject.w_ANTIPCON AND SSCODICE=this.oParentObject.w_ANCODICE ;
              into cursor _Curs_SED_STOR
          endif
          if used('_Curs_SED_STOR')
            select _Curs_SED_STOR
            locate for 1=1
            do while not(eof())
            if Nvl(_Curs_SED_STOR.SSPARIVA,Space(12))<>this.oParentObject.w_ANPARIVA AND Not Empty(Nvl(_Curs_SED_STOR.SSPARIVA,Space(12)))
              this.oParentObject.w_ANPARIVA = Nvl(_Curs_SED_STOR.SSPARIVA,Space(12))
              ah_ErrorMsg("Attenzione,esistono sedi storicizzate con partita IVA diversa. Impossibile modificare!",,"")
              exit
            endif
              select _Curs_SED_STOR
              continue
            enddo
            use
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_RIOR'
    this.cWorkTables[2]='SED_STOR'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PAR_RIOR')
      use in _Curs_PAR_RIOR
    endif
    if used('_Curs_SED_STOR')
      use in _Curs_SED_STOR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
