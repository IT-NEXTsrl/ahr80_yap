* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kto                                                        *
*              Visualizza struttura aziendale                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-11                                                      *
* Last revis.: 2009-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kto",oParentObject))

* --- Class definition
define class tgsar_kto as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 460
  Height = 561
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-17"
  HelpContextID=186026135
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  DIPENDEN_IDX = 0
  RIS_ESPLO_IDX = 0
  cPrg = "gsar_kto"
  cComment = "Visualizza struttura aziendale"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLTIPOPE = space(1)
  w_FLTCODRI = space(5)
  o_FLTCODRI = space(5)
  w_FLTGRUAP = space(5)
  o_FLTGRUAP = space(5)
  w_RELIVMIN = 0
  w_LEVGRPST = 0
  w_FLTLIVPR = 0
  w_FLRISGRU = space(1)
  w_FLRISUMA = space(1)
  w_TMPCODRI = space(5)
  w_FLTRWFIL = .F.
  w_PATIPRIS = space(1)
  o_PATIPRIS = space(1)
  w_RisSelez = space(5)
  o_RisSelez = space(5)
  w_TOTIPRIS = space(1)
  w_TOCODICE = space(5)
  w_OLDCPCCH = space(10)
  w_DENOM_PART = space(100)
  w_COGNPART = space(40)
  w_NOMEPART = space(40)
  w_DESCRPART = space(40)
  w_DPTIPRIS = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_ORGANIGR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ktoPag1","gsar_kto",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLTCODRI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ORGANIGR = this.oPgFrm.Pages(1).oPag.ORGANIGR
    DoDefault()
    proc Destroy()
      this.w_ORGANIGR = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='RIS_ESPLO'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLTIPOPE=space(1)
      .w_FLTCODRI=space(5)
      .w_FLTGRUAP=space(5)
      .w_RELIVMIN=0
      .w_LEVGRPST=0
      .w_FLTLIVPR=0
      .w_FLRISGRU=space(1)
      .w_FLRISUMA=space(1)
      .w_TMPCODRI=space(5)
      .w_FLTRWFIL=.f.
      .w_PATIPRIS=space(1)
      .w_RisSelez=space(5)
      .w_TOTIPRIS=space(1)
      .w_TOCODICE=space(5)
      .w_OLDCPCCH=space(10)
      .w_DENOM_PART=space(100)
      .w_COGNPART=space(40)
      .w_NOMEPART=space(40)
      .w_DESCRPART=space(40)
      .w_DPTIPRIS=space(1)
      .w_OB_TEST=ctod("  /  /  ")
        .w_FLTIPOPE = 'T'
          .DoRTCalc(2,2,.f.)
        .w_FLTGRUAP = ''
          .DoRTCalc(4,4,.f.)
        .w_LEVGRPST = IIF(.w_LEVGRPST=0 OR .w_LEVGRPST<.w_RELIVMIN, .w_RELIVMIN, .w_LEVGRPST)
        .w_FLTLIVPR = 1
      .oPgFrm.Page1.oPag.ORGANIGR.Calculate()
        .w_FLRISGRU = 'G'
        .w_FLRISUMA = 'P'
          .DoRTCalc(9,9,.f.)
        .w_FLTRWFIL = .F.
        .w_PATIPRIS = 'P'
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_RisSelez))
          .link_1_20('Full')
        endif
        .w_TOTIPRIS = .w_ORGANIGR.getVar("TOTIPRIS")
        .w_TOCODICE = .w_ORGANIGR.getVar("TOCODICE")
          .DoRTCalc(15,15,.f.)
        .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
          .DoRTCalc(17,20,.f.)
        .w_OB_TEST = i_INIDAT
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_FLTCODRI<>.w_FLTCODRI
            .w_FLTGRUAP = ''
        endif
        .DoRTCalc(4,4,.t.)
        if .o_FLTGRUAP<>.w_FLTGRUAP
            .w_LEVGRPST = IIF(.w_LEVGRPST=0 OR .w_LEVGRPST<.w_RELIVMIN, .w_RELIVMIN, .w_LEVGRPST)
        endif
        .oPgFrm.Page1.oPag.ORGANIGR.Calculate()
        .DoRTCalc(6,11,.t.)
        if .o_RisSelez<>.w_RisSelez.or. .o_PATIPRIS<>.w_PATIPRIS
          .link_1_20('Full')
        endif
            .w_TOTIPRIS = .w_ORGANIGR.getVar("TOTIPRIS")
            .w_TOCODICE = .w_ORGANIGR.getVar("TOCODICE")
        .DoRTCalc(15,15,.t.)
        if .o_RisSelez<>.w_RisSelez
            .w_DENOM_PART = IIF(.w_DPTIPRIS='P',alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART),ALLTRIM(.w_DESCRPART))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ORGANIGR.Calculate()
    endwith
  return

  proc Calculate_SSWGKLLFVB()
    with this
          * --- CHIUSURA CURSORI - GSAR_BTO(END)
          gsar_bto(this;
              ,'END';
             )
    endwith
  endproc
  proc Calculate_XZLJZTTMRN()
    with this
          * --- Apre men� contestuale Treeview _ GSAR_BTO(NRC)
          gsar_bto(this;
              ,'NRC';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLTGRUAP_1_3.enabled = this.oPgFrm.Page1.oPag.oFLTGRUAP_1_3.mCond()
    this.oPgFrm.Page1.oPag.oLEVGRPST_1_5.enabled = this.oPgFrm.Page1.oPag.oLEVGRPST_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLTCODRI_1_2.visible=!this.oPgFrm.Page1.oPag.oFLTCODRI_1_2.mHide()
    this.oPgFrm.Page1.oPag.oFLTGRUAP_1_3.visible=!this.oPgFrm.Page1.oPag.oFLTGRUAP_1_3.mHide()
    this.oPgFrm.Page1.oPag.oRELIVMIN_1_4.visible=!this.oPgFrm.Page1.oPag.oRELIVMIN_1_4.mHide()
    this.oPgFrm.Page1.oPag.oLEVGRPST_1_5.visible=!this.oPgFrm.Page1.oPag.oLEVGRPST_1_5.mHide()
    this.oPgFrm.Page1.oPag.oFLTLIVPR_1_6.visible=!this.oPgFrm.Page1.oPag.oFLTLIVPR_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ORGANIGR.Event(cEvent)
        if lower(cEvent)==lower("Done")
          .Calculate_SSWGKLLFVB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ORGANIGR NodeRightClick")
          .Calculate_XZLJZTTMRN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RisSelez
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RisSelez) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BDZ',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_RisSelez)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_PATIPRIS;
                     ,'DPCODICE',trim(this.w_RisSelez))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RisSelez)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_RisSelez)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_RisSelez)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_PATIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_RisSelez)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_RisSelez)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_PATIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStrODBC(trim(this.w_RisSelez)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPDESCRI like "+cp_ToStr(trim(this.w_RisSelez)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_PATIPRIS);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RisSelez) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oRisSelez_1_20'),i_cWhere,'GSAR_BDZ',"Partecipanti",'GSAG_MPA.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PATIPRIS<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RisSelez)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_RisSelez);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_PATIPRIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_PATIPRIS;
                       ,'DPCODICE',this.w_RisSelez)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME,DPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RisSelez = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
      this.w_DESCRPART = NVL(_Link_.DPDESCRI,space(40))
      this.w_DPTIPRIS = NVL(_Link_.DPTIPRIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_RisSelez = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
      this.w_DESCRPART = space(40)
      this.w_DPTIPRIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RisSelez Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLTCODRI_1_2.value==this.w_FLTCODRI)
      this.oPgFrm.Page1.oPag.oFLTCODRI_1_2.value=this.w_FLTCODRI
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTGRUAP_1_3.value==this.w_FLTGRUAP)
      this.oPgFrm.Page1.oPag.oFLTGRUAP_1_3.value=this.w_FLTGRUAP
    endif
    if not(this.oPgFrm.Page1.oPag.oRELIVMIN_1_4.value==this.w_RELIVMIN)
      this.oPgFrm.Page1.oPag.oRELIVMIN_1_4.value=this.w_RELIVMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oLEVGRPST_1_5.value==this.w_LEVGRPST)
      this.oPgFrm.Page1.oPag.oLEVGRPST_1_5.value=this.w_LEVGRPST
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTLIVPR_1_6.value==this.w_FLTLIVPR)
      this.oPgFrm.Page1.oPag.oFLTLIVPR_1_6.value=this.w_FLTLIVPR
    endif
    if not(this.oPgFrm.Page1.oPag.oPATIPRIS_1_19.RadioValue()==this.w_PATIPRIS)
      this.oPgFrm.Page1.oPag.oPATIPRIS_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRisSelez_1_20.value==this.w_RisSelez)
      this.oPgFrm.Page1.oPag.oRisSelez_1_20.value=this.w_RisSelez
    endif
    if not(this.oPgFrm.Page1.oPag.oDENOM_PART_1_28.value==this.w_DENOM_PART)
      this.oPgFrm.Page1.oPag.oDENOM_PART_1_28.value=this.w_DENOM_PART
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(GSAR_BTO(this.Parent.oContained, 'CRI'))  and not(.w_FLTIPOPE='T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFLTCODRI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(GSAR_BTO(this.Parent.oContained, 'CGR'))  and not(.w_FLTIPOPE='T')  and (!EMPTY(.w_FLTCODRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFLTGRUAP_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_LEVGRPST>0 AND .w_LEVGRPST>=.w_RELIVMIN)  and not(.w_FLTIPOPE='T')  and (!EMPTY(.w_FLTGRUAP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLEVGRPST_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Livello iniziale analisi inferiore al livello del gruppo d'appartenenza")
          case   not(.w_FLTLIVPR>0)  and not(.w_FLTIPOPE='T')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFLTLIVPR_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero di livelli da analizzare deve essere maggiore di zero")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLTCODRI = this.w_FLTCODRI
    this.o_FLTGRUAP = this.w_FLTGRUAP
    this.o_PATIPRIS = this.w_PATIPRIS
    this.o_RisSelez = this.w_RisSelez
    return

enddefine

* --- Define pages as container
define class tgsar_ktoPag1 as StdContainer
  Width  = 456
  height = 561
  stdWidth  = 456
  stdheight = 561
  resizeXpos=186
  resizeYpos=332
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLTCODRI_1_2 as StdField with uid="BAZXNNHNLJ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FLTCODRI", cQueryName = "FLTCODRI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona da impersonificare",;
    HelpContextID = 196142689,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=189, Top=41, InputMask=replicate('X',5), bHasZoom = .t. 

  func oFLTCODRI_1_2.mHide()
    with this.Parent.oContained
      return (.w_FLTIPOPE='T')
    endwith
  endfunc

  func oFLTCODRI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (GSAR_BTO(this.Parent.oContained, 'CRI'))
    endwith
    return bRes
  endfunc

  proc oFLTCODRI_1_2.mZoom
      with this.Parent.oContained
        GSAR_BTO(this.Parent.oContained,"ZRI")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oFLTGRUAP_1_3 as StdField with uid="UMXENLXHHP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FLTGRUAP", cQueryName = "FLTGRUAP",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo appartenenza persona selezionata",;
    HelpContextID = 175957594,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=189, Top=66, InputMask=replicate('X',5), bHasZoom = .t. 

  func oFLTGRUAP_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_FLTCODRI))
    endwith
   endif
  endfunc

  func oFLTGRUAP_1_3.mHide()
    with this.Parent.oContained
      return (.w_FLTIPOPE='T')
    endwith
  endfunc

  func oFLTGRUAP_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (GSAR_BTO(this.Parent.oContained, 'CGR'))
    endwith
    return bRes
  endfunc

  proc oFLTGRUAP_1_3.mZoom
      with this.Parent.oContained
        GSAR_BTO(this.Parent.oContained,"ZGR")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oRELIVMIN_1_4 as StdField with uid="DUHXAAVNIN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_RELIVMIN", cQueryName = "RELIVMIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Livello minimo analizzabile nella struttura aziendale",;
    HelpContextID = 230986596,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=353, Top=64

  func oRELIVMIN_1_4.mHide()
    with this.Parent.oContained
      return (.w_FLTIPOPE='T')
    endwith
  endfunc

  add object oLEVGRPST_1_5 as StdField with uid="TWTTVDGJXG",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LEVGRPST", cQueryName = "LEVGRPST",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Livello iniziale analisi inferiore al livello del gruppo d'appartenenza",;
    ToolTipText = "Livello iniziale per analisi struttura aziendale",;
    HelpContextID = 259837174,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=320, Top=505, cSayPict='"999"', cGetPict='"999"'

  func oLEVGRPST_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_FLTGRUAP))
    endwith
   endif
  endfunc

  func oLEVGRPST_1_5.mHide()
    with this.Parent.oContained
      return (.w_FLTIPOPE='T')
    endwith
  endfunc

  func oLEVGRPST_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LEVGRPST>0 AND .w_LEVGRPST>=.w_RELIVMIN)
    endwith
    return bRes
  endfunc

  add object oFLTLIVPR_1_6 as StdField with uid="BOGRYGKILY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_FLTLIVPR", cQueryName = "FLTLIVPR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero di livelli da analizzare deve essere maggiore di zero",;
    ToolTipText = "Numero di livelli da analizzare",;
    HelpContextID = 168289880,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=320, Top=529, cSayPict='"999"', cGetPict='"999"'

  func oFLTLIVPR_1_6.mHide()
    with this.Parent.oContained
      return (.w_FLTIPOPE='T')
    endwith
  endfunc

  func oFLTLIVPR_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FLTLIVPR>0)
    endwith
    return bRes
  endfunc


  add object oBtn_1_7 as StdButton with uid="EXPISZDTIZ",left=401, top=505, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la struttura aziendale";
    , HelpContextID = 186026230;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSAR_BTO(this.Parent.oContained,"FTW")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLTIPOPE='T' OR (.w_FLTIPOPE='A' AND !EMPTY(.w_FLTGRUAP)  AND !EMPTY(.w_FLTLIVPR) ))
      endwith
    endif
  endfunc


  add object ORGANIGR as cp_Treeview with uid="JPRMTJILIM",left=14, top=33, width=435,height=469,;
    caption='ORGANIGR',;
   bGlobalFont=.t.,;
    nIndent=20,cCursor="organingr",cShowFields="DESCRI",cNodeShowField="",cLeafShowField="",cNodeBmp="Kit.bmp,Utepos.bmp,weadd.bmp",cLeafBmp="",;
    cEvent = "CaricaTree",;
    nPag=1;
    , HelpContextID = 154947640


  add object oPATIPRIS_1_19 as StdCombo with uid="TDINOKNPPO",rtseq=11,rtrep=.f.,left=4,top=5,width=76,height=21;
    , HelpContextID = 40177481;
    , cFormVar="w_PATIPRIS",RowSource=""+"Persona,"+"Risorsa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPATIPRIS_1_19.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oPATIPRIS_1_19.GetRadio()
    this.Parent.oContained.w_PATIPRIS = this.RadioValue()
    return .t.
  endfunc

  func oPATIPRIS_1_19.SetRadio()
    this.Parent.oContained.w_PATIPRIS=trim(this.Parent.oContained.w_PATIPRIS)
    this.value = ;
      iif(this.Parent.oContained.w_PATIPRIS=='P',1,;
      iif(this.Parent.oContained.w_PATIPRIS=='R',2,;
      0))
  endfunc

  func oPATIPRIS_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_RisSelez)
        bRes2=.link_1_20('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oRisSelez_1_20 as StdField with uid="QJYWRUDDRU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_RisSelez", cQueryName = "RisSelez",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice persona di inizio selezione",;
    HelpContextID = 230762384,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=81, Top=5, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", cZoomOnZoom="GSAR_BDZ", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_PATIPRIS", oKey_2_1="DPCODICE", oKey_2_2="this.w_RisSelez"

  func oRisSelez_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oRisSelez_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRisSelez_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_PATIPRIS)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_PATIPRIS)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oRisSelez_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BDZ',"Partecipanti",'GSAG_MPA.DIPENDEN_VZM',this.parent.oContained
  endproc
  proc oRisSelez_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BDZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.DPTIPRIS=w_PATIPRIS
     i_obj.w_DPCODICE=this.parent.oContained.w_RisSelez
     i_obj.ecpSave()
  endproc


  add object oBtn_1_21 as StdButton with uid="EAUEFBZEDV",left=14, top=504, width=48,height=45,;
    CpPicture="BMP\ESPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Espande la struttura";
    , HelpContextID = 108658758;
    , Caption='\<Esplodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        .w_ORGANIGR.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLTRWFIL)
      endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="TEAUCDDQOP",left=63, top=504, width=48,height=45,;
    CpPicture="BMP\IMPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Chiude la struttura";
    , HelpContextID = 108657286;
    , Caption='\<Implodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        .w_ORGANIGR.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLTRWFIL)
      endwith
    endif
  endfunc

  add object oDENOM_PART_1_28 as StdField with uid="SKXCKTCAOH",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 12907625,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=144, Top=5, InputMask=replicate('X',100)

  add object oStr_1_12 as StdString with uid="JRZNMGXRSF",Visible=.t., Left=16, Top=43,;
    Alignment=1, Width=170, Height=17,;
    Caption="Codice persona:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_FLTIPOPE='T')
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="ROIZRHOTBN",Visible=.t., Left=16, Top=67,;
    Alignment=1, Width=170, Height=17,;
    Caption="Gruppo appartenenza:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (.w_FLTIPOPE='T')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="BPBHMGDAIK",Visible=.t., Left=130, Top=507,;
    Alignment=1, Width=188, Height=18,;
    Caption="Livello iniziale da analizzare:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_FLTIPOPE='T')
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="XQVVZVGGBQ",Visible=.t., Left=115, Top=531,;
    Alignment=1, Width=203, Height=18,;
    Caption="Numero livelli da analizzare:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_FLTIPOPE='T')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="RQLVQRAZTP",Visible=.t., Left=279, Top=67,;
    Alignment=1, Width=70, Height=18,;
    Caption="livello:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_FLTIPOPE='T')
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="ITVNNWKONH",Visible=.t., Left=17, Top=7,;
    Alignment=1, Width=170, Height=17,;
    Caption="Modalit� visualizzazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kto','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
