* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kcp                                                        *
*              Impostazione pubblicazione codici di ricerca                    *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_67]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-07-15                                                      *
* Last revis.: 2016-05-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kcp",oParentObject))

* --- Class definition
define class tgsma_kcp as StdForm
  Top    = 13
  Left   = 12

  * --- Standard Properties
  Width  = 673
  Height = 491
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-12"
  HelpContextID=169182359
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  ART_ICOL_IDX = 0
  GRUMERC_IDX = 0
  CACOARTI_IDX = 0
  CATEGOMO_IDX = 0
  FAM_ARTI_IDX = 0
  KEY_ARTI_IDX = 0
  cPrg = "gsma_kcp"
  cComment = "Impostazione pubblicazione codici di ricerca"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CATIPBAR = space(1)
  w_CATIPCON = space(1)
  o_CATIPCON = space(1)
  w_CACODCON = space(15)
  w_ARTGPUBL = space(1)
  w_TIPART = space(1)
  o_TIPART = space(1)
  w_CODARTIN = space(20)
  w_CODARTFI = space(20)
  w_ARGRUMER = space(5)
  w_ARCATCON = space(5)
  w_ARCATOMO = space(5)
  w_ARCODFAM = space(5)
  w_OPZIONI = space(3)
  w_PUBBLICA = space(1)
  w_TIPART2 = space(2)
  w_ARFLCOMP = space(1)
  w_CODART = space(20)
  w_ZOOMPRO = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kcpPag1","gsma_kcp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCATIPCON_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMPRO = this.oPgFrm.Pages(1).oPag.ZOOMPRO
    DoDefault()
    proc Destroy()
      this.w_ZOOMPRO = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='GRUMERC'
    this.cWorkTables[4]='CACOARTI'
    this.cWorkTables[5]='CATEGOMO'
    this.cWorkTables[6]='FAM_ARTI'
    this.cWorkTables[7]='KEY_ARTI'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_CATIPBAR=space(1)
      .w_CATIPCON=space(1)
      .w_CACODCON=space(15)
      .w_ARTGPUBL=space(1)
      .w_TIPART=space(1)
      .w_CODARTIN=space(20)
      .w_CODARTFI=space(20)
      .w_ARGRUMER=space(5)
      .w_ARCATCON=space(5)
      .w_ARCATOMO=space(5)
      .w_ARCODFAM=space(5)
      .w_OPZIONI=space(3)
      .w_PUBBLICA=space(1)
      .w_TIPART2=space(2)
      .w_ARFLCOMP=space(1)
      .w_CODART=space(20)
        .w_DATOBSO = i_DATSYS
        .w_OBTEST = i_DATSYS
        .w_CATIPBAR = '0'
        .w_CATIPCON = 'R'
        .w_CACODCON = SPACE(15)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CACODCON))
          .link_1_5('Full')
        endif
        .w_ARTGPUBL = 'Z'
        .w_TIPART = 'A'
        .w_CODARTIN = space(20)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODARTIN))
          .link_1_8('Full')
        endif
        .w_CODARTFI = space(20)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODARTFI))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_ARGRUMER))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_ARCATCON))
          .link_1_11('Full')
        endif
        .w_ARCATOMO = iif(.w_TIPART='S',Space(5),.w_ARCATOMO)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_ARCATOMO))
          .link_1_12('Full')
        endif
        .w_ARCODFAM = iif(.w_TIPART='S',space(5),.w_ARCODFAM)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_ARCODFAM))
          .link_1_13('Full')
        endif
      .oPgFrm.Page1.oPag.ZOOMPRO.Calculate()
        .w_OPZIONI = 'DES'
        .w_PUBBLICA = 'S'
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .DoRTCalc(16,18,.f.)
        if not(empty(.w_CODART))
          .link_1_39('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CATIPCON<>.w_CATIPCON
            .w_CACODCON = SPACE(15)
          .link_1_5('Full')
        endif
        .DoRTCalc(6,7,.t.)
        if .o_TIPART<>.w_TIPART
            .w_CODARTIN = space(20)
          .link_1_8('Full')
        endif
        if .o_TIPART<>.w_TIPART
            .w_CODARTFI = space(20)
          .link_1_9('Full')
        endif
        .DoRTCalc(10,11,.t.)
        if .o_TIPART<>.w_TIPART
            .w_ARCATOMO = iif(.w_TIPART='S',Space(5),.w_ARCATOMO)
          .link_1_12('Full')
        endif
        if .o_TIPART<>.w_TIPART
            .w_ARCODFAM = iif(.w_TIPART='S',space(5),.w_ARCODFAM)
          .link_1_13('Full')
        endif
        .oPgFrm.Page1.oPag.ZOOMPRO.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .DoRTCalc(14,17,.t.)
          .link_1_39('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMPRO.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCACODCON_1_5.enabled = this.oPgFrm.Page1.oPag.oCACODCON_1_5.mCond()
    this.oPgFrm.Page1.oPag.oARCATOMO_1_12.enabled = this.oPgFrm.Page1.oPag.oARCATOMO_1_12.mCond()
    this.oPgFrm.Page1.oPag.oARCODFAM_1_13.enabled = this.oPgFrm.Page1.oPag.oARCODFAM_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsma_kcp
    If Lower(cEvent) = 'w_codartin lostfocus'
    	*-- Checking su filtro articoli
    	local oObj
    	
    	If Not Empty(This.w_CODARTIN) And Not ((EMPTY(This.w_DATOBSO) OR This.w_DATOBSO>This.w_OBTEST) and ((This.w_TIPART2 $ 'FM-FO-DE' AND This.w_TIPART='S') or (This.w_TIPART2 $ 'PF-SE-MP-PH' AND This.w_TIPART='A') or (This.w_TIPART2 $ 'PF-SE-MP-PH' AND This.w_ARFLCOMP='S' AND This.w_TIPART='C')) and (This.w_CODARTIN<=This.w_CODARTFI or empty(This.w_CODARTFI)))
    	
    		cp_ErrorMsg ( 'Articolo iniziale: Prodotto obsoleto, di tipologia differente da quella selezionata o minore di quello iniziale' )
    		m.oObj = This.GetCtrl('w_CODARTIN')
    		m.oObj.Value = ''
    		m.oObj.Valid()
    
    	Endif
    Endif
    
    If Lower(cEvent) = 'w_codartfi lostfocus'
      *-- Checking su filtro articoli
      local oObj
      
      If Not Empty(This.w_CODARTFI) And Not ((EMPTY(This.w_DATOBSO) OR This.w_DATOBSO>This.w_OBTEST) and ((This.w_TIPART2 $ 'FM-FO-DE' AND This.w_TIPART='S') or (This.w_TIPART2 $ 'PF-SE-MP-PH' AND This.w_TIPART='A') or (This.w_TIPART2 $ 'PF-SE-MP-PH' AND This.w_ARFLCOMP='S' AND This.w_TIPART='C')) and (This.w_CODARTIN<=This.w_CODARTFI or empty(This.w_CODARTFI)))
    
    	  cp_ErrorMsg ( 'Articolo finale: Prodotto obsoleto, di tipologia differente da quella selezionata o minore di quello iniziale' )
    		m.oObj = This.GetCtrl('w_CODARTFI')
    		m.oObj.Value = ''
    		m.oObj.Valid()
    
      Endif
    Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMPRO.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODCON
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CACODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CATIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CATIPCON;
                     ,'ANCODICE',trim(this.w_CACODCON))
          select ANTIPCON,ANCODICE,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCACODCON_1_5'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CATIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CATIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CACODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CATIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CATIPCON;
                       ,'ANCODICE',this.w_CACODCON)
            select ANTIPCON,ANCODICE,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CACODCON = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_CACODCON = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODARTIN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARTIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODARTIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADTOBSO,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODARTIN))
          select CACODICE,CADTOBSO,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARTIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODARTIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODARTIN_1_8'),i_cWhere,'',"Codici di ricerca",'GSMAKKCP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADTOBSO,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARTIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADTOBSO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODARTIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODARTIN)
            select CACODICE,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARTIN = NVL(_Link_.CACODICE,space(20))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_CODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODARTIN = space(20)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARTIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODARTFI
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARTFI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODARTFI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADTOBSO,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODARTFI))
          select CACODICE,CADTOBSO,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARTFI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODARTFI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODARTFI_1_9'),i_cWhere,'',"Codici di ricerca",'GSMAKKCP.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADTOBSO,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARTFI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADTOBSO,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODARTFI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODARTFI)
            select CACODICE,CADTOBSO,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARTFI = NVL(_Link_.CACODICE,space(20))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_CODART = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODARTFI = space(20)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARTFI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARGRUMER
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARGRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_ARGRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_ARGRUMER))
          select GMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARGRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARGRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oARGRUMER_1_10'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARGRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_ARGRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_ARGRUMER)
            select GMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARGRUMER = NVL(_Link_.GMCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ARGRUMER = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARGRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCATCON
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_lTable = "CACOARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2], .t., this.CACOARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC1',True,'CACOARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_ARCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_ARCATCON))
          select C1CODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCATCON)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOARTI','*','C1CODICE',cp_AbsName(oSource.parent,'oARCATCON_1_11'),i_cWhere,'GSAR_AC1',"Categorie contabili articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_ARCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_ARCATCON)
            select C1CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCATCON = NVL(_Link_.C1CODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ARCATCON = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCATOMO
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCATOMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_ARCATOMO)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_ARCATOMO))
          select OMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCATOMO)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCATOMO) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oARCATOMO_1_12'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCATOMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_ARCATOMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_ARCATOMO)
            select OMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCATOMO = NVL(_Link_.OMCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ARCATOMO = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCATOMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARCODFAM
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARCODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_ARCODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_ARCODFAM))
          select FACODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARCODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARCODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oARCODFAM_1_13'),i_cWhere,'GSAR_AFA',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARCODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_ARCODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_ARCODFAM)
            select FACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARCODFAM = NVL(_Link_.FACODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ARCODFAM = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARCODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARTIPART,ARFLCOMP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARTIPART,ARFLCOMP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_TIPART2 = NVL(_Link_.ARTIPART,space(2))
      this.w_ARFLCOMP = NVL(_Link_.ARFLCOMP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_TIPART2 = space(2)
      this.w_ARFLCOMP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCATIPCON_1_4.RadioValue()==this.w_CATIPCON)
      this.oPgFrm.Page1.oPag.oCATIPCON_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCACODCON_1_5.value==this.w_CACODCON)
      this.oPgFrm.Page1.oPag.oCACODCON_1_5.value=this.w_CACODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oARTGPUBL_1_6.RadioValue()==this.w_ARTGPUBL)
      this.oPgFrm.Page1.oPag.oARTGPUBL_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPART_1_7.RadioValue()==this.w_TIPART)
      this.oPgFrm.Page1.oPag.oTIPART_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODARTIN_1_8.value==this.w_CODARTIN)
      this.oPgFrm.Page1.oPag.oCODARTIN_1_8.value=this.w_CODARTIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODARTFI_1_9.value==this.w_CODARTFI)
      this.oPgFrm.Page1.oPag.oCODARTFI_1_9.value=this.w_CODARTFI
    endif
    if not(this.oPgFrm.Page1.oPag.oARGRUMER_1_10.value==this.w_ARGRUMER)
      this.oPgFrm.Page1.oPag.oARGRUMER_1_10.value=this.w_ARGRUMER
    endif
    if not(this.oPgFrm.Page1.oPag.oARCATCON_1_11.value==this.w_ARCATCON)
      this.oPgFrm.Page1.oPag.oARCATCON_1_11.value=this.w_ARCATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oARCATOMO_1_12.value==this.w_ARCATOMO)
      this.oPgFrm.Page1.oPag.oARCATOMO_1_12.value=this.w_ARCATOMO
    endif
    if not(this.oPgFrm.Page1.oPag.oARCODFAM_1_13.value==this.w_ARCODFAM)
      this.oPgFrm.Page1.oPag.oARCODFAM_1_13.value=this.w_ARCODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oOPZIONI_1_27.RadioValue()==this.w_OPZIONI)
      this.oPgFrm.Page1.oPag.oOPZIONI_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUBBLICA_1_28.RadioValue()==this.w_PUBBLICA)
      this.oPgFrm.Page1.oPag.oPUBBLICA_1_28.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_CATIPCON$"CF")  and not(empty(.w_CACODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCACODCON_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CATIPCON = this.w_CATIPCON
    this.o_TIPART = this.w_TIPART
    return

enddefine

* --- Define pages as container
define class tgsma_kcpPag1 as StdContainer
  Width  = 669
  height = 491
  stdWidth  = 669
  stdheight = 491
  resizeXpos=273
  resizeYpos=266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCATIPCON_1_4 as StdCombo with uid="VCGCMXTSUV",rtseq=4,rtrep=.f.,left=101,top=30,width=161,height=21;
    , ToolTipText = "Indica la provenienza del codice: interna, cliente o fornitore o altro";
    , HelpContextID = 228324748;
    , cFormVar="w_CATIPCON",RowSource=""+"Interna,"+"Cliente,"+"Fornitore,"+"A quantit� e valore,"+"A valore,"+"Descrittivo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATIPCON_1_4.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    iif(this.value =4,'M',;
    iif(this.value =5,'I',;
    iif(this.value =6,'D',;
    space(1))))))))
  endfunc
  func oCATIPCON_1_4.GetRadio()
    this.Parent.oContained.w_CATIPCON = this.RadioValue()
    return .t.
  endfunc

  func oCATIPCON_1_4.SetRadio()
    this.Parent.oContained.w_CATIPCON=trim(this.Parent.oContained.w_CATIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_CATIPCON=='R',1,;
      iif(this.Parent.oContained.w_CATIPCON=='C',2,;
      iif(this.Parent.oContained.w_CATIPCON=='F',3,;
      iif(this.Parent.oContained.w_CATIPCON=='M',4,;
      iif(this.Parent.oContained.w_CATIPCON=='I',5,;
      iif(this.Parent.oContained.w_CATIPCON=='D',6,;
      0))))))
  endfunc

  func oCATIPCON_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CACODCON)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCACODCON_1_5 as StdField with uid="SRAGWYTNZN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CACODCON", cQueryName = "CACODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Eventuale cliente / fornitore di provenienza",;
    HelpContextID = 240584076,;
   bGlobalFont=.t.,;
    Height=21, Width=136, Left=101, Top=53, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CATIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CACODCON"

  func oCACODCON_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CATIPCON$"CF")
    endwith
   endif
  endfunc

  func oCACODCON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODCON_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCACODCON_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CATIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CATIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCACODCON_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'',this.parent.oContained
  endproc
  proc oCACODCON_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_CATIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CACODCON
     i_obj.ecpSave()
  endproc


  add object oARTGPUBL_1_6 as StdCombo with uid="MMQGNNFKPH",rtseq=6,rtrep=.f.,left=101,top=78,width=136,height=21;
    , ToolTipText = "Codici di ricerca da visualizzare (solo quelli gi� pubblicati, solo quelli trasferiti oppure tutti)";
    , HelpContextID = 194897070;
    , cFormVar="w_ARTGPUBL",RowSource=""+"Solo pubblicati,"+"Solo non pubblicati,"+"Solo trasferiti,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARTGPUBL_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    iif(this.value =4,'Z',;
    space(1))))))
  endfunc
  func oARTGPUBL_1_6.GetRadio()
    this.Parent.oContained.w_ARTGPUBL = this.RadioValue()
    return .t.
  endfunc

  func oARTGPUBL_1_6.SetRadio()
    this.Parent.oContained.w_ARTGPUBL=trim(this.Parent.oContained.w_ARTGPUBL)
    this.value = ;
      iif(this.Parent.oContained.w_ARTGPUBL=='S',1,;
      iif(this.Parent.oContained.w_ARTGPUBL=='N',2,;
      iif(this.Parent.oContained.w_ARTGPUBL=='T',3,;
      iif(this.Parent.oContained.w_ARTGPUBL=='Z',4,;
      0))))
  endfunc


  add object oTIPART_1_7 as StdCombo with uid="NVFMIGUCDO",rtseq=7,rtrep=.f.,left=351,top=30,width=131,height=21;
    , ToolTipText = "Tipologia prodotto";
    , HelpContextID = 209988810;
    , cFormVar="w_TIPART",RowSource=""+"Articoli,"+"Servizi,"+"Articoli composti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPART_1_7.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oTIPART_1_7.GetRadio()
    this.Parent.oContained.w_TIPART = this.RadioValue()
    return .t.
  endfunc

  func oTIPART_1_7.SetRadio()
    this.Parent.oContained.w_TIPART=trim(this.Parent.oContained.w_TIPART)
    this.value = ;
      iif(this.Parent.oContained.w_TIPART=='A',1,;
      iif(this.Parent.oContained.w_TIPART=='S',2,;
      iif(this.Parent.oContained.w_TIPART=='C',3,;
      0)))
  endfunc

  add object oCODARTIN_1_8 as StdField with uid="VYCMQKPDYL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODARTIN", cQueryName = "CODARTIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Selezionare un codice per inizio intervallo",;
    HelpContextID = 58398836,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=351, Top=53, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODARTIN"

  func oCODARTIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARTIN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARTIN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODARTIN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'GSMAKKCP.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oCODARTFI_1_9 as StdField with uid="OYWWUHTFUS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODARTFI", cQueryName = "CODARTFI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Selezionare un codice per inizio intervallo",;
    HelpContextID = 58398831,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=351, Top=78, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", oKey_1_1="CACODICE", oKey_1_2="this.w_CODARTFI"

  func oCODARTFI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARTFI_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARTFI_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODARTFI_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici di ricerca",'GSMAKKCP.KEY_ARTI_VZM',this.parent.oContained
  endproc

  add object oARGRUMER_1_10 as StdField with uid="JIFUJUUTOV",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ARGRUMER", cQueryName = "ARGRUMER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico articolo",;
    HelpContextID = 213666648,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=610, Top=30, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_ARGRUMER"

  func oARGRUMER_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oARGRUMER_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARGRUMER_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oARGRUMER_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oARGRUMER_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_ARGRUMER
     i_obj.ecpSave()
  endproc

  add object oARCATCON_1_11 as StdField with uid="AQSAFMQEGX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ARCATCON", cQueryName = "ARCATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria contabile dell'articolo",;
    HelpContextID = 224720044,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=610, Top=53, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOARTI", cZoomOnZoom="GSAR_AC1", oKey_1_1="C1CODICE", oKey_1_2="this.w_ARCATCON"

  func oARCATCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCATCON_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCATCON_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOARTI','*','C1CODICE',cp_AbsName(this.parent,'oARCATCON_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC1',"Categorie contabili articoli",'',this.parent.oContained
  endproc
  proc oARCATCON_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_ARCATCON
     i_obj.ecpSave()
  endproc

  add object oARCATOMO_1_12 as StdField with uid="DGHSPVPOCR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ARCATOMO", cQueryName = "ARCATOMO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria omogenea di appartenenza",;
    HelpContextID = 23393451,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=610, Top=78, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_ARCATOMO"

  func oARCATOMO_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPART<>'S')
    endwith
   endif
  endfunc

  func oARCATOMO_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCATOMO_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCATOMO_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oARCATOMO_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oARCATOMO_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_ARCATOMO
     i_obj.ecpSave()
  endproc

  add object oARCODFAM_1_13 as StdField with uid="JUEJYJIIXA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ARCODFAM", cQueryName = "ARCODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Famiglia di appartenenza dell'articolo",;
    HelpContextID = 190248109,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=610, Top=103, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_ARCODFAM"

  func oARCODFAM_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPART<>'S')
    endwith
   endif
  endfunc

  func oARCODFAM_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oARCODFAM_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARCODFAM_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oARCODFAM_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglie articoli",'',this.parent.oContained
  endproc
  proc oARCODFAM_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_ARCODFAM
     i_obj.ecpSave()
  endproc


  add object oBtn_1_14 as StdButton with uid="XAOFRYAIID",left=15, top=105, width=48,height=45,;
    CpPicture="BMP\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Esegue interrogazione";
    , HelpContextID = 190766314;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        .notifyevent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOMPRO as cp_szoombox with uid="UUZEOMCYDT",left=5, top=149, width=658,height=281,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="KEY_ARTI",cZoomFile="GSMA_KCP",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomOnZoom="",cMenuFile="",;
    cEvent = "Ricerca,w_TIPART Changed",;
    nPag=1;
    , HelpContextID = 189690906

  add object oOPZIONI_1_27 as StdRadio with uid="BZYHCYCHXU",rtseq=14,rtrep=.f.,left=9, top=435, width=129,height=32;
    , ToolTipText = "Flag per selezionare o deselezionare tutto";
    , cFormVar="w_OPZIONI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oOPZIONI_1_27.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 223640038
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 223640038
      this.Buttons(2).Top=15
      this.SetAll("Width",127)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Flag per selezionare o deselezionare tutto")
      StdRadio::init()
    endproc

  func oOPZIONI_1_27.RadioValue()
    return(iif(this.value =1,'SEL',;
    iif(this.value =2,'DES',;
    space(3))))
  endfunc
  func oOPZIONI_1_27.GetRadio()
    this.Parent.oContained.w_OPZIONI = this.RadioValue()
    return .t.
  endfunc

  func oOPZIONI_1_27.SetRadio()
    this.Parent.oContained.w_OPZIONI=trim(this.Parent.oContained.w_OPZIONI)
    this.value = ;
      iif(this.Parent.oContained.w_OPZIONI=='SEL',1,;
      iif(this.Parent.oContained.w_OPZIONI=='DES',2,;
      0))
  endfunc


  add object oPUBBLICA_1_28 as StdCombo with uid="ZWKIGEDNGE",rtseq=15,rtrep=.f.,left=241,top=435,width=148,height=21;
    , ToolTipText = "Pubblica, non pubblica i codici di ricerca selezionati";
    , HelpContextID = 132382921;
    , cFormVar="w_PUBBLICA",RowSource=""+"Pubblica su web,"+"Non pubblicare su web", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUBBLICA_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oPUBBLICA_1_28.GetRadio()
    this.Parent.oContained.w_PUBBLICA = this.RadioValue()
    return .t.
  endfunc

  func oPUBBLICA_1_28.SetRadio()
    this.Parent.oContained.w_PUBBLICA=trim(this.Parent.oContained.w_PUBBLICA)
    this.value = ;
      iif(this.Parent.oContained.w_PUBBLICA=='S',1,;
      iif(this.Parent.oContained.w_PUBBLICA=='N',2,;
      0))
  endfunc


  add object oObj_1_30 as cp_runprogram with uid="AWLGTIXIOW",left=1, top=498, width=247,height=19,;
    caption='GSMA_BPU(OPT)',;
   bGlobalFont=.t.,;
    prg="gsma_bpc('OPT')",;
    cEvent = "w_OPZIONI Changed",;
    nPag=1;
    , HelpContextID = 181076421


  add object oBtn_1_31 as StdButton with uid="SBZGRQGKZG",left=510, top=441, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue interrogazione";
    , HelpContextID = 225898970;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      do GSMA_SGC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_32 as StdButton with uid="SKBBJFTYSB",left=562, top=441, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'elaborazione";
    , HelpContextID = 169211110;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        gsma_bpc(this.Parent.oContained,"MOD")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_33 as StdButton with uid="KEVDUGZIVH",left=614, top=441, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 176499782;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_38 as cp_outputCombo with uid="ZLERXIVPWT",left=1, top=520, width=247,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 189690906

  add object oStr_1_15 as StdString with uid="XCJOQRCLVF",Visible=.t., Left=292, Top=496,;
    Alignment=1, Width=55, Height=18,;
    Caption="Barcode:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="FTRQHEUGXN",Visible=.t., Left=8, Top=5,;
    Alignment=1, Width=73, Height=18,;
    Caption="Selezioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="YFQHFMPQWB",Visible=.t., Left=18, Top=30,;
    Alignment=1, Width=78, Height=18,;
    Caption="Tipo codifica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="GAIUHZQLVL",Visible=.t., Left=22, Top=55,;
    Alignment=1, Width=74, Height=18,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="KJXTEUCRSJ",Visible=.t., Left=266, Top=55,;
    Alignment=1, Width=78, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="BKKTBAEAXQ",Visible=.t., Left=542, Top=30,;
    Alignment=1, Width=62, Height=18,;
    Caption="Gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="FYDCOSZJQK",Visible=.t., Left=541, Top=53,;
    Alignment=1, Width=63, Height=18,;
    Caption="Cat. cont.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="DWVJCHOOVX",Visible=.t., Left=532, Top=78,;
    Alignment=1, Width=72, Height=18,;
    Caption="Cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="IIBGHIWWXY",Visible=.t., Left=553, Top=103,;
    Alignment=1, Width=51, Height=18,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="WHLLZDGAZA",Visible=.t., Left=16, Top=81,;
    Alignment=1, Width=80, Height=18,;
    Caption="Stato attuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="UJSDYRRSFK",Visible=.t., Left=136, Top=435,;
    Alignment=1, Width=100, Height=18,;
    Caption="Web application:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="BVAVXFHPXE",Visible=.t., Left=290, Top=30,;
    Alignment=1, Width=54, Height=18,;
    Caption="Prodotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="KUAFGZPPAX",Visible=.t., Left=277, Top=81,;
    Alignment=1, Width=67, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oBox_1_17 as StdBox with uid="USHSGQOJDJ",left=4, top=23, width=661,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kcp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
