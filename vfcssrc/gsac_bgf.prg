* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bgf                                                        *
*              Generazione PDA                                                 *
*                                                                              *
*      Author: ZUCCHETTI TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_169]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-12-22                                                      *
* Last revis.: 2014-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bgf",oParentObject)
return(i_retval)

define class tgsac_bgf as StdBatch
  * --- Local variables
  w_CODART = space(20)
  w_CODCON = space(15)
  w_TIPATT = space(1)
  w_DATORD = ctod("  /  /  ")
  w_DATEVA = ctod("  /  /  ")
  w_PERASS = space(3)
  w_FABNET = 0
  w_appo = space(10)
  w_UNIMIS = space(3)
  w_CODCOM = space(15)
  w_VOCCOS = space(15)
  w_PUNRIO = 0
  w_CODMAG = space(5)
  w_CODATT = space(15)
  w_SERIAL = space(10)
  w_QTAORD = 0
  w_PZ = 0
  w_ROWNUM = 0
  w_QTAUM1 = 0
  w_S1 = 0
  w_CODICE = space(41)
  w_STATUS = space(1)
  w_S2 = 0
  w_CODCEN = space(15)
  w_S3 = 0
  w_S4 = 0
  w_CODARTVAR = space(20)
  w_ORDPROGR = 0
  w_CAO = 0
  w_OLDCODICE = space(20)
  w_OPERAT = space(1)
  w_VAL = space(3)
  w_LOTRIO = 0
  w_MOLTIP = 0
  w_QTAMINTROV = 0
  w_QTAMIN = 0
  TmpN = 0
  w_QTAMINPARZ = 0
  w_LOTRIODEF = 0
  w_TIME = space(5)
  w_QTAMINSCAN = 0
  w_QTAMINDEF = 0
  w_ARTIPGES = space(1)
  w_LOTRIOSCAN = 0
  w_GIOAPP = 0
  CercaForAbi = .f.
  CercaContr = .f.
  w_CONUMERO = space(15)
  w_CRITERIO = space(1)
  w_MESSAGG = space(80)
  w_ARDTOBSO = ctod("  /  /  ")
  TmpC = space(100)
  w_LNumErr = 0
  w_PPFLCCOS = space(1)
  w_PPCENCOS = space(15)
  w_STATORD = space(1)
  w_PPCALSTA = space(5)
  w_CALEND = .f.
  w_ARFLCOMM = space(1)
  w_CRELAFAB = space(1)
  w_CRELAPDA = space(1)
  w_SERPDA = space(15)
  w_RIGPDA = 0
  w_CODARTVARMG = space(20)
  w_CRIELAB = space(1)
  w_CODARTPAR = space(40)
  w_TIPCON = space(1)
  w_GENERA = .f.
  w_QTAMAX = 0
  w_QTARES = 0
  DatRisul = ctod("  /  /  ")
  DatInput = space(8)
  w_DatErr = .f.
  w_DatErrMsg = space(10)
  w_TipoPianificazione = space(1)
  * --- WorkFile variables
  PDA_DETT_idx=0
  PAR_PROD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera Proposte d'Acquisto - PDA (da GSAC_BFB)
    this.w_CONUMERO = space(15)
    this.w_CRITERIO = space(1)
    * --- modalita' non gestite
    if this.oParentObject.w_CRIFOR $ "PR"
      ah_ErrorMsg("Criterio non gestito%0Elaborazione sospesa","!","")
      i_retcode = 'stop'
      return
    endif
    * --- Elimina tutte le PDA Suggerite
    * --- Try
    local bErr_0498F840
    bErr_0498F840=bTrsErr
    this.Try_0498F840()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Errore eliminazione tabella PDA_DETT%0%1",,"",MESSAGE())
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_0498F840
    * --- End
    ah_Msg("Interrogazione base dati in corso...",.T.)
    * --- Carico il calendario (se � stato inserito nei parametri)
    * --- Read from PAR_PROD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2],.t.,this.PAR_PROD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPCALSTA,PPFLCCOS,PPCENCOS"+;
        " from "+i_cTable+" PAR_PROD where ";
            +"PPCODICE = "+cp_ToStrODBC("AA");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPCALSTA,PPFLCCOS,PPCENCOS;
        from (i_cTable) where;
            PPCODICE = "AA";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PPCALSTA = NVL(cp_ToDate(_read_.PPCALSTA),cp_NullValue(_read_.PPCALSTA))
      this.w_PPFLCCOS = NVL(cp_ToDate(_read_.PPFLCCOS),cp_NullValue(_read_.PPFLCCOS))
      this.w_PPCENCOS = NVL(cp_ToDate(_read_.PPCENCOS),cp_NullValue(_read_.PPCENCOS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT EMPTY(this.w_PPCALSTA) and Used("Calend")
      this.w_CALEND = .T.
    endif
    * --- Carica in memoria i fabbisogni
    do vq_exec with "query\Gsac_bgf",this,"Fabbisogni"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Select Fabbisogni
    if reccount()=0
      ah_ErrorMsg("Non ci sono dati da elaborare","!","")
      use in Fabbisogni
      i_retcode = 'stop'
      return
    endif
    =WrCursor("Fabbisogni")
    INDEX ON FBCODART TAG CODICE
    * --- Carica in memoria le PDA Confermate e quelle Da ordinare
    do vq_exec with "query\Gsac7bgf",this,"PDAconf"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Aggiorna i Fabbisogni
    Select PDAConf
    scan
    this.w_CRELAPDA = NVL(PDCRIELA, "A")
    this.w_CODCOM = NVL(PDCODCOM, SPACE(15))
    this.w_RIGPDA = PDROWNUM
    this.w_SERPDA = PDSERIAL+STR(this.w_RIGPDA, 4, 0)
    this.w_CODARTVAR = pdcodart
    this.w_CODARTVARMG = pdcodart + pdcodmag
    this.w_DATEVA = CP_TODATE(PDDATEVA)
    this.w_FABNET = pdqtaum1
    SELECT FABBISOGNI
    this.w_ARFLCOMM = NVL( ARFLCOMM, "N")
    this.w_CRELAFAB = NVL(FBCRIELA, "A")
    if this.w_CRELAFAB="A"
      if this.w_ARFLCOMM="S"
        Replace all fbfabnet with fbfabnet-this.w_FABNET for fbcodart=this.w_CODARTVAR and fbcodcom=this.w_CODCOM and CP_TODATE(FBDATEVA)>=this.w_DATEVA
      else
        Replace all fbfabnet with fbfabnet-this.w_FABNET for fbcodart=this.w_CODARTVAR and CP_TODATE(FBDATEVA)>=this.w_DATEVA
      endif
    else
      if this.w_ARFLCOMM="S"
        Replace all fbfabnet with fbfabnet-this.w_FABNET for fbcodart+fbcodmag=this.w_CODARTVARMG and fbcodcom=this.w_CODCOM AND CP_TODATE(FBDATEVA)>=this.w_DATEVA
      else
        Replace all fbfabnet with fbfabnet-this.w_FABNET for fbcodart+fbcodmag=this.w_CODARTVARMG AND CP_TODATE(FBDATEVA)>=this.w_DATEVA
      endif
    endif
    Select PDAConf
    endscan
    * --- Elimina righe con Qta richista <=0
    Select Fabbisogni
    delete all for fbfabnet<=0
    if reccount()=0
      ah_ErrorMsg("Non ci sono dati da elaborare","!","")
      use in Fabbisogni
      use in PDAconf
      i_retcode = 'stop'
      return
    endif
    use in PDAconf
    * --- Carica in memoria i Fornitori Abituali
    do vq_exec with "query\Gsac1bgf",this,"FornAbi"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    =WrCursor("FornAbi")
    INDEX ON FBCODART TAG CODICE
    * --- Carica in memoria i Contratti
    if this.oParentObject.w_CRIFOR="Z"
      * --- Criterio di Scelta Fornitore = Prezzo
      do vq_exec with "query\Gsac5bgf",this,"Contratti"
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      do vq_exec with "query\Gsac2bgf",this,"Contratti"
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    =WrCursor("Contratti")
    INDEX ON FBCODART TAG CODICE
    * --- Carica in memoria i Codici di Ricerca relativi ai fornitori
    do vq_exec with "query\Gsac3bgf",this,"CodFor"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    =WrCursor("CodFor")
    INDEX ON FBCODART TAG CODICE
    * --- Cursore vuoto con il tracciato record di "Dettaglio PDA"
    do vq_exec with "query\Gsac4bgf",this,"PDA"
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    =WrCursor("PDA")
    if Not this.w_CALEND
      * --- Se c'� il calendario nei parametri, nel cursore sono gi� presenti
      * --- Tabella periodi nelle PDA
      do vq_exec with "query\Gsac_bcb",this,"Periodi"
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    ah_Msg("Elaborazione PDA in corso...",.T.)
    * --- Scansione fabbisogni
    Select Fabbisogni
    scan
    this.w_CRIELAB = FBCRIELA
    this.w_CODART = FBCODART
    this.w_ARFLCOMM = NVL( ARFLCOMM, "N")
    this.w_DATEVA = cp_ToDate(FBDATEVA)
    this.w_FABNET = FBFABNET
    this.w_UNIMIS = FBUNIMIS
    this.w_CODMAG = FBCODMAG
    this.w_CODCEN = SPACE(15)
    * --- Gestione centro di costo
    if g_PERCCR="S"
      do case
        case this.w_PPFLCCOS="A"
          this.w_CODCEN = NVL(FBCODCEN, SPACE(15))
        case this.w_PPFLCCOS="P"
          this.w_CODCEN = this.w_PPCENCOS
        case this.w_PPFLCCOS="E"
          this.w_CODCEN = NVL(FBCODCEN, NVL(this.w_PPCENCOS, SPACE(15)) )
      endcase
    endif
    this.w_VOCCOS = ARVOCCEN
    this.w_CODCOM = FBCODCOM
    this.w_TIPATT = FBTIPATT
    this.w_CODATT = FBCODATT
    this.w_CODARTVAR = this.w_CODART 
    this.w_CODARTPAR = this.w_CODART
    if this.w_CRIELAB="A"
      this.w_CODARTVAR = this.w_CODART
      this.w_CODARTPAR = this.w_CODARTVAR
      if this.w_ARFLCOMM="S"
        this.w_CODARTVAR = this.w_CODART + this.w_CODCOM
      endif
    else
      this.w_CODARTVAR = this.w_CODART + this.w_CODMAG
      this.w_CODARTPAR = this.w_CODARTVAR
      if this.w_ARFLCOMM="S"
        this.w_CODARTVAR = this.w_CODARTVAR + this.w_CODCOM
      endif
    endif
    this.w_ARTIPGES = nvl( ARTIPGES, " ")
    this.w_ARDTOBSO = NVL(ARDTOBSO, cp_CharToDate("  -  -  "))
    this.w_TIPCON = NVL(FBTIPCON, " ")
    * --- Controlla se si sta analizzando un nuovo articolo
    if this.w_CODARTVAR<>this.w_OLDCODICE
      this.w_OLDCODICE = this.w_CODARTVAR
      this.w_CODCON = space(15)
      this.CercaForAbi = .T.
      this.w_ORDPROGR = 0
      this.w_LOTRIODEF = 0
      this.w_QTAMINDEF = 0
      this.w_LOTRIO = 0
      this.w_QTAMIN = 0
      this.w_GIOAPP = 0
      this.w_QTAMAX = 0
    endif
    if this.CercaForAbi
      this.w_GIOAPP = 0
      this.w_QTAMAX = 0
      * --- Cerca il fornitore Abituale (preferenziale)
      if this.w_CRIELAB="A"
        Select * from FornAbi where fbcodart=this.w_CODARTPAR into cursor tempor
      else
        Select * from FornAbi where fbcodart+fbcodmag=this.w_CODARTPAR into cursor tempor
        Select tempor
        if reccount()=0
          * --- Non ho trovato niente per articolo+magazzino cerco solo per aticolo
          Select * from FornAbi where fbcodart=this.w_CODART into cursor tempor
        endif
      endif
      Select tempor
      if reccount()>0
        this.w_CODCON = nvl(prcodfor,space(15))
        if this.w_ARFLCOMM="S"
          this.w_LOTRIODEF = 0
          this.w_QTAMINDEF = 0
          this.w_QTAMAX = 0
        else
          this.w_LOTRIODEF = nvl(prlotrio,0)
          this.w_QTAMINDEF = nvl(prqtamin,0)
          this.w_QTAMAX = nvl(prqtamax,0)
        endif
        this.w_LOTRIO = this.w_LOTRIODEF
        this.w_QTAMIN = this.w_QTAMINDEF
        this.w_GIOAPP = nvl(prgioapp,0)
      endif
      * --- Se non trova il fornitore preferenziale guarda nei contratti
      this.CercaContr = empty(this.w_CODCON)
      * --- Se trova il fornitore preferenziale cerca il codice di ricerca dell'articolo e del fornitore
      this.CercaForAbi = .F.
    endif
    Select Fabbisogni
    if eof()
      this.w_GENERA = TRUE
    else
      skip
      if this.w_CRIELAB="A"
        if NVL(ARFLCOMM , "N")="S"
          this.w_GENERA = this.w_CODARTVAR<>fbcodart+fbcodcom or this.w_DATEVA<>cp_ToDate(fbdateva)
        else
          this.w_GENERA = this.w_CODARTVAR<>fbcodart or this.w_DATEVA<>cp_ToDate(fbdateva)
        endif
      else
        if NVL(ARFLCOMM , "N")="S"
          this.w_GENERA = this.w_CODARTVAR<>fbcodart+fbcodmag+fbcodcom or this.w_DATEVA<>cp_ToDate(fbdateva)
        else
          this.w_GENERA = this.w_CODARTVAR<>fbcodart+fbcodmag or this.w_DATEVA<>cp_ToDate(fbdateva) or this.w_CODCOM<>fbcodcom
        endif
      endif
      skip -1
    endif
    * --- Il fabbisogno netto � progressivo (come il PAB)
    if this.w_GENERA and this.w_FABNET > this.w_ORDPROGR
      * --- Quantita che resta da Ordinare (non considera la Qta Minima x che' potrei non conoscere il fornitore)
      this.w_QTAUM1 = this.w_FABNET - this.w_ORDPROGR
      if this.CercaContr and g_GESCON="S"
        * --- and Contratti abilitati
        * --- Se non c'e' Fornitore Abituale Cerca  nei Contratti
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Nessun contratto 
        this.w_CONUMERO = space(15)
        this.w_CRITERIO = " "
        this.w_CODICE = this.w_CODART
      endif
      if NOT EMPTY(this.w_CODICE)
        * --- Gestione a Fabbisogno oppure trovato un contratto valido
        if this.w_ARTIPGES="F" or not empty(this.w_CONUMERO)
          * --- Vincolo quantit� minima
          if this.w_QTAUM1 < this.w_QTAMIN
            this.w_QTAUM1 = this.w_QTAMIN
          endif
          * --- Vincolo lotto di riordino
          if this.w_LOTRIO > 0
            this.TmpN = this.w_QTAUM1/this.w_LOTRIO
            if this.TmpN<>ceiling(this.TmpN)
              this.w_QTAUM1 = this.w_LOTRIO * ceiling(this.TmpN)
            endif
          endif
        endif
        this.w_ORDPROGR = this.w_ORDPROGR + this.w_QTAUM1
        this.w_QTAORD = this.w_QTAUM1
        if this.w_CALEND And this.w_GIOAPP>0
          * --- Gestione calendario (parametri PDA)
          if this.w_ARTIPGES="S"
            * --- Gestione a scorta
            this.w_TipoPianificazione = "A"
            this.w_DATORD = i_DATSYS
            this.DatInput = dtos(this.w_DATORD)
          else
            * --- Gestione a fabbisogno
            this.w_TipoPianificazione = "I"
            this.DatInput = dtos(this.w_DATEVA)
          endif
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- La data dell'impegno non deve essere successiva alla data di richiesta
          if this.w_ARTIPGES="S"
            * --- Gestione a scorta
            this.w_DATEVA = iif(this.DatRisul>this.w_DATORD and not empty(this.DatRisul), this.DatRisul, this.w_DATORD)
          else
            * --- Gestione a fabbisogno
            this.w_DATORD = iif(this.DatRisul<this.w_Dateva and not empty(this.DatRisul), this.DatRisul, this.w_Dateva)
          endif
        else
          if this.w_ARTIPGES="S"
            * --- Gestione a scorta
            this.w_DATORD = i_DATSYS
            this.w_DATEVA = this.w_DATORD + this.w_GIOAPP
          else
            this.w_DATORD = this.w_DATEVA - this.w_GIOAPP
          endif
        endif
        if not empty(this.w_DATEVA)
          if NOT this.w_CALEND
            * --- Gestione calendario (parametri PDA)
            Select Periodi
            Locate for tpdatini<=this.w_DATEVA and this.w_DATEVA<=tpdatfin
            if found()
              this.w_PERASS = tpperass
            else
              this.w_PERASS = "   "
            endif
          else
            Select Calend 
 index on datidx to datidx
            if seek(dtos(this.w_DATEVA))
              this.w_PERASS = Calend.tpperass
            else
              this.w_PERASS = "   "
            endif
            Select Calend 
 index on datidx to datidx for giolav
          endif
          this.w_QTARES = this.w_QTAUM1
          do while this.w_QTARES>0
            * --- Questo ciclo serve a spezzare gli ordini che non rispettano il vincolo di quantit� massima, se il vincolo non c'� la procedura fa sempre un solo 'giro'
            * --- Inserisce la PDA in un cursore
            Select PDA
            scatter memvar blank
            if this.w_QTAMAX<>0 and this.w_QTARES>this.w_QTAMAX
              * --- Applico politica di lottizzazione quantit� massima
              this.w_QTAUM1 = this.w_QTAMAX
              this.w_QTAORD = this.w_QTAUM1
              * --- Fabbisogno residuo (da valutare al prossimo giro)
              this.w_QTARES = this.w_QTARES-this.w_QTAUM1
            else
              if this.w_QTAMAX=0
                * --- Se la quantit� massima non � gestita faccio un solo giro nello while
                this.w_QTARES = 0
              else
                * --- Se sono all'ultimo giro tengo l'avanzo
                this.w_QTAUM1 = this.w_QTARES
                this.w_QTAORD = this.w_QTAUM1
                this.w_QTARES = 0
              endif
            endif
            pdcodice = this.w_CODICE
            pdcodart = this.w_CODART
            pddateva = this.w_DATEVA
            pddatord = this.w_DATORD
            pdperass = this.w_PERASS
            pdcodmag = this.w_CODMAG
            pdcodcon = this.w_CODCON
            pdunimis = this.w_UNIMIS
            pdqtaord = this.w_QTAORD
            pdqtaum1 = this.w_QTAUM1
            pdcodcen = this.w_CODCEN
            pdcodcom = this.w_CODCOM
            pdtipatt = this.w_TIPATT
            pdcodatt = this.w_CODATT
            pdcontra=this.w_CONUMERO
            pdcrifor=this.oParentObject.w_CRIFOR
            pdvoccos=this.w_VOCCOS
            ardtobso = this.w_ARDTOBSO
            pdltperr = this.w_DatErrMsg
            PDCRIELA = this.w_CRIELAB
            Append blank
            gather memvar
          enddo
        endif
      endif
    endif
    Select Fabbisogni
    endscan
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if USED("PdaElaErr")
      this.w_LNumErr = reccount("pdaElaErr")
      if this.w_LNumErr > 0
        * --- Errore durante la generazione delle PDA
        this.TmpC = "Si sono verificati %1 errori di pianificazione durante la generazione PDA%0Desideri la stampa dell'elenco degli errori?"
        if ah_YesNo(this.TmpC,"", alltrim(str(this.w_LNumErr,5,0)) )
          * --- Lancia Stampa
          SELECT * from pdaelaerr into cursor __tmp__
          cp_chprn("query\gsac_kgf", " ", this)
        endif
      endif
      use in PdaElaErr
    endif
  endproc
  proc Try_0498F840()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from PDA_DETT
    i_nConn=i_TableProp[this.PDA_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PDA_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".PDSERIAL = "+i_cQueryTable+".PDSERIAL";
            +" and "+i_cTable+".PDROWNUM = "+i_cQueryTable+".PDROWNUM";
    
      do vq_exec with 'GSACDBGF',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cerca il miglior fornitore dai contratti
    * --- Seleziona Criterio di scelta
    do case
      case this.oParentObject.w_CRIFOR="A"
        * --- Affidabilit�
        OrdinaPer = "Order by coaffida desc"
      case this.oParentObject.w_CRIFOR="P"
        * --- Proporzione
        OrdinaPer = "Order by copriori desc"
      case this.oParentObject.w_CRIFOR="T"
        * --- Tempo = giorni di approvvigionamento
        OrdinaPer = "Order by cogioapp"
      case this.oParentObject.w_CRIFOR="R"
        * --- Ripartizione
        OrdinaPer = "Order by coperrip"
      case this.oParentObject.w_CRIFOR="I"
        * --- Priorit�
        OrdinaPer = "Order by copriori desc"
    endcase
    if this.oParentObject.w_CRIFOR<>"Z"
      * --- Seleziona i contratti validi e li ordina per il criterio di scelta
      Select * from Contratti ;
      where fbcodart=this.w_CODARTVAR and codatini<=this.w_DATEVA and this.w_DATEVA<=codatfin ;
      &OrdinaPer into cursor tempor
    else
      * --- Calcola Prezzo da Contratto
      * --- Prima genera un temporaneo contenente le righe articolo interessate dei contratti validi
      * --- ordinate per Fornitore + Qta Scaglione
      * --- Vengono gia' filtrati gli scaglioni con qta max.< di quella che devo ordinare
      Select * from Contratti ;
      where fbcodart=this.w_CODARTVAR and codatini<=this.w_DATEVA and this.w_DATEVA<=codatfin ;
      and nvl(coquanti,0)>=this.w_QTAUM1 and not empty(nvl(cocodclf," ")) ;
      order by cocodclf, coquanti into cursor Contprez
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- A questo punto ho selezionato solo uno scaglione ciascun Fornitore
      * --- Inoltre tutti i prezzi riportati, sono gia' scontati e e riferiti alla Valuta di conto per poterli confrontare
      * --- Ordino per il prezzo piu' basso
      Select * from ContPrez where not deleted() ;
      Order By Coprezzo into cursor tempor
      if used("ContPrez")
        Select contPrez
        use
      endif
    endif
    Select tempor
    if reccount()=0
      * --- Nessun contratto valido trovato
      this.w_CODCON = space(15)
      this.w_LOTRIO = this.w_LOTRIODEF
      this.w_QTAMIN = this.w_QTAMINDEF
      this.CercaForAbi = .F.
      this.w_CODICE = this.w_CODART
      this.w_CONUMERO = space(15)
      this.w_CRITERIO = " "
    else
      go top
      if this.oParentObject.w_CRIFOR="P"
        * --- Criterio di scelta: Proporzione
        this.w_QTAMINTROV = 999999999.999
        scan
        this.w_QTAMINSCAN = nvl(coqtamin,0)
        this.w_LOTRIOSCAN = nvl(colotmul,0)
        this.w_QTAMINPARZ = this.w_QTAUM1
        * --- Vincolo quantit� minima
        if this.w_QTAMINPARZ < this.w_QTAMINSCAN
          this.w_QTAMINPARZ = this.w_QTAMINSCAN
        endif
        * --- Vincolo lotto di riordino
        if this.w_LOTRIOSCAN > 0
          this.TmpN = this.w_QTAMINPARZ/this.w_LOTRIOSCAN
          if this.TmpN<>int(this.TmpN)
            this.w_QTAMINPARZ = (int(this.TmpN)+1)*this.w_LOTRIOSCAN
          endif
        endif
        if this.w_QTAMINPARZ < this.w_QTAMINTROV
          * --- Migliore Proporzione trovata
          this.w_QTAMINTROV = this.w_QTAMINPARZ
          this.w_CODCON = nvl(cocodclf,space(15))
          this.w_QTAMIN = this.w_QTAMINSCAN
          this.w_LOTRIO = this.w_LOTRIOSCAN
          this.w_GIOAPP = nvl(cogioapp,0)
        endif
        endscan
      else
        this.w_CODCON = nvl(cocodclf,space(15))
        this.w_QTAMIN = nvl(coqtamin,0)
        this.w_LOTRIO = nvl(colotmul,0)
        this.w_GIOAPP = nvl(cogioapp,0)
      endif
      this.w_CODICE = this.w_CODART
      this.w_CONUMERO = nvl(conumero,space(15))
      * --- Aggiorno contratto e criterio di scelta con valori significativi
      this.w_CRITERIO = this.oParentObject.w_CRIFOR
      this.CercaForAbi = .F.
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Prezzo da Contratto
    * --- Elabora Criterio per Miglior Prezzo
    =WrCursor("Contprez")
    Index On cocodclf+str(coquanti,12,3) Tag Codice
    * --- Per ciascun Fornitore, prendo la prima riga contratto valida (cioe' lo scaglione piu' basso)
    this.w_appo = "######"
    select Contprez
    go top
    scan
    if cocodclf <> this.w_APPO
      * --- Nuovo Fornitore, Calcola Prezzo da Contratto
      this.w_PZ = NVL(COPREZZO, 0)
      this.w_S1 = NVL(COSCONT1, 0)
      this.w_S2 = NVL(COSCONT2, 0)
      this.w_S3 = NVL(COSCONT3, 0)
      this.w_S4 = NVL(COSCONT4, 0)
      this.w_PZ = this.w_PZ * (1+this.w_S1/100) * (1+this.w_S2/100) * (1+this.w_S3/100) * (1+this.w_S4/100)
      this.w_VAL = NVL(COCODVAL, g_PERVAL)
      this.w_CAO = GETCAM(this.w_VAL, i_DATSYS, 0)
      if this.w_VAL<>g_PERVAL
        * --- Se altra valuta, riporta alla Valuta di conto
        if this.w_CAO<>0
          this.w_PZ = VAL2MON(this.w_PZ, this.w_CAO, 1, i_DATSYS, g_PERVAL)
          replace coprezzo with this.w_PZ
        else
          * --- Cambio incongruente, record non utilizzabile
          this.w_PZ = -999
        endif
        select Contprez
      endif
      if this.w_PZ=-999
        delete
      else
        this.w_appo = cocodclf
      endif
    else
      * --- Elimina record
      delete
    endif
    endscan
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica Database
    ah_Msg("Aggiornamento database in corso",.T.)
    if not used("PdaElaErr")
      * --- Crea cursore di stampa degli errori
      create cursor PdaElaErr (pdserial c(10), pdrownum n(8,0), pdcodart c(20), Quan N(18,6), Datini D(8), Datfin D(8), Messagg C(80))
    endif
    * --- begin transaction
    cp_BeginTrs()
    * --- Prende il progressivo per la chiave primaria
    this.w_SERIAL = cp_GetProg("PDA_DETT", "SEPDA", this.w_SERIAL, i_CODAZI)
    Select PDA
    scan
    scatter memvar
    this.w_ROWNUM = this.w_ROWNUM + 1
    this.w_CODICE = pdcodice
    this.w_CODART = pdcodart
    this.w_DATEVA = pddateva
    this.w_DATORD = pddatord
    this.w_PERASS = pdperass
    this.w_CODMAG = pdcodmag
    this.w_CODCON = pdcodcon
    this.w_UNIMIS = pdunimis
    this.w_QTAORD = pdqtaord
    this.w_QTAUM1 = pdqtaum1
    this.w_CODCEN = pdcodcen
    this.w_CODCOM = PDCODCOM
    this.w_TIPATT = pdtipatt
    this.w_CODATT = SPACE(15)
    this.w_CONUMERO = pdcontra
    this.w_CRITERIO = pdcrifor
    this.w_VOCCOS = pdvoccos
    this.w_ARDTOBSO = ARDTOBSO
    this.w_CRIELAB = PDCRIELA
    this.w_DatErrMsg = pdltperr
    this.w_STATORD = iif(this.oParentObject.w_STAORD="C","D","S")
    if not empty(this.w_DatErrMsg)
      if used("PdaElaErr")
        this.w_MESSAGG = ah_Msgformat(this.w_DatErrMsg)
        SELECT PdaElaErr
        INSERT INTO PdaElaErr (pdserial, pdrownum, pdcodart, Quan, Datini, Datfin, Messagg) ;
        VALUES (this.w_SERIAL, this.w_ROWNUM, this.w_CODART, this.w_QTAORD, this.w_DATORD, this.w_DATEVA, this.w_MESSAGG)
        Select PDA
      endif
    else
      if this.w_DATORD < i_DATSYS
        if used("PdaElaErr")
          if this.w_DATEVA < i_DATSYS
            this.w_MESSAGG = ah_Msgformat("Pianificato nel passato")
          else
            this.w_MESSAGG = ah_Msgformat("PDA in ritardo")
          endif
          SELECT PdaElaErr
          INSERT INTO PdaElaErr (pdserial, pdrownum, pdcodart, Quan, Datini, Datfin, Messagg) ;
          VALUES (this.w_SERIAL, this.w_ROWNUM, this.w_CODART, this.w_QTAORD, this.w_DATORD, this.w_DATEVA, this.w_MESSAGG)
          Select PDA
        endif
      endif
    endif
    if not (this.w_ARDTOBSO > this.w_DATEVA or empty(this.w_ARDTOBSO))
      if used("PdaElaErr")
        this.w_MESSAGG = ah_Msgformat("PDA per articolo obsoleto (al %1)", dtoc(this.w_ARDTOBSO) )
        SELECT PdaElaErr
        INSERT INTO PdaElaErr (pdserial, pdrownum, pdcodart, Quan, Datini, Datfin, Messagg) ;
        VALUES (this.w_SERIAL, this.w_ROWNUM, this.w_CODART, this.w_QTAORD, this.w_DATORD, this.w_DATEVA, this.w_MESSAGG)
        Select PDA
      endif
    endif
    * --- Il messaggio sull'errore di transazione viene dato due volte 
    *      ma non era possibile tradurre il messaggio parametrico direttamente nell'error message
    * --- Try
    local bErr_04AE8598
    bErr_04AE8598=bTrsErr
    this.Try_04AE8598()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Errore inserimento tabella PDA_DETT%0%1",,"",MESSAGE())
    endif
    bTrsErr=bTrsErr or bErr_04AE8598
    * --- End
    endscan
    * --- Inserisce la data, l'ora e l'utente che ha generato le PDA
    this.w_TIME = time()
    * --- Try
    local bErr_04AEA098
    bErr_04AEA098=bTrsErr
    this.Try_04AEA098()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04AEA098
    * --- End
    * --- Write into PAR_PROD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PPELAODL ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PAR_PROD','PPELAODL');
      +",PPORAODL ="+cp_NullLink(cp_ToStrODBC(this.w_TIME),'PAR_PROD','PPORAODL');
      +",PPOPEODL ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PAR_PROD','PPOPEODL');
      +",PPFLDIGE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CRIFOR),'PAR_PROD','PPFLDIGE');
          +i_ccchkf ;
      +" where ";
          +"PPCODICE = "+cp_ToStrODBC("AA");
             )
    else
      update (i_cTable) set;
          PPELAODL = i_DATSYS;
          ,PPORAODL = this.w_TIME;
          ,PPOPEODL = i_CODUTE;
          ,PPFLDIGE = this.oParentObject.w_CRIFOR;
          &i_ccchkf. ;
       where;
          PPCODICE = "AA";

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    if used("Fabbisogni")
      use in Fabbisogni
    endif
    if used("FornAbi")
      use in FornAbi
    endif
    if used("Contratti")
      use in Contratti
    endif
    if used("CodFor")
      use in CodFor
    endif
    if used("PDA")
      use in PDA
    endif
    if used("Periodi")
      use in Periodi
    endif
    if used("Tempor")
      use in Tempor
    endif
    use in select("Calend")
  endproc
  proc Try_04AE8598()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PDA_DETT
    i_nConn=i_TableProp[this.PDA_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PDA_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PDA_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PDSERIAL"+",PDROWNUM"+",PDCODICE"+",PDCODART"+",PDDATEVA"+",PDDATORD"+",PDCODMAG"+",PDUMORDI"+",PDUNIMIS"+",PDQTAORD"+",PDQTAUM1"+",PDCODCEN"+",PDCODCOM"+",PDTIPATT"+",PDCODATT"+",PDSTATUS"+",PDTIPCON"+",PDCODCON"+",PDPERASS"+",PDCONTRA"+",PDCRIFOR"+",PDVOCCOS"+",PDCRIELA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'PDA_DETT','PDSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PDA_DETT','PDROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'PDA_DETT','PDCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'PDA_DETT','PDCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATEVA),'PDA_DETT','PDDATEVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATORD),'PDA_DETT','PDDATORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODMAG),'PDA_DETT','PDCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'PDA_DETT','PDUMORDI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'PDA_DETT','PDUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTAUM1),'PDA_DETT','PDQTAORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QTAUM1),'PDA_DETT','PDQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCEN),'PDA_DETT','PDCODCEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PDA_DETT','PDCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPATT),'PDA_DETT','PDTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODATT),'PDA_DETT','PDCODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_STATORD),'PDA_DETT','PDSTATUS');
      +","+cp_NullLink(cp_ToStrODBC("F"),'PDA_DETT','PDTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PDA_DETT','PDCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERASS),'PDA_DETT','PDPERASS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONUMERO),'PDA_DETT','PDCONTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CRITERIO),'PDA_DETT','PDCRIFOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VOCCOS),'PDA_DETT','PDVOCCOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CRIELAB),'PDA_DETT','PDCRIELA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PDSERIAL',this.w_SERIAL,'PDROWNUM',this.w_ROWNUM,'PDCODICE',this.w_CODICE,'PDCODART',this.w_CODART,'PDDATEVA',this.w_DATEVA,'PDDATORD',this.w_DATORD,'PDCODMAG',this.w_CODMAG,'PDUMORDI',this.w_UNIMIS,'PDUNIMIS',this.w_UNIMIS,'PDQTAORD',this.w_QTAUM1,'PDQTAUM1',this.w_QTAUM1,'PDCODCEN',this.w_CODCEN)
      insert into (i_cTable) (PDSERIAL,PDROWNUM,PDCODICE,PDCODART,PDDATEVA,PDDATORD,PDCODMAG,PDUMORDI,PDUNIMIS,PDQTAORD,PDQTAUM1,PDCODCEN,PDCODCOM,PDTIPATT,PDCODATT,PDSTATUS,PDTIPCON,PDCODCON,PDPERASS,PDCONTRA,PDCRIFOR,PDVOCCOS,PDCRIELA &i_ccchkf. );
         values (;
           this.w_SERIAL;
           ,this.w_ROWNUM;
           ,this.w_CODICE;
           ,this.w_CODART;
           ,this.w_DATEVA;
           ,this.w_DATORD;
           ,this.w_CODMAG;
           ,this.w_UNIMIS;
           ,this.w_UNIMIS;
           ,this.w_QTAUM1;
           ,this.w_QTAUM1;
           ,this.w_CODCEN;
           ,this.w_CODCOM;
           ,this.w_TIPATT;
           ,this.w_CODATT;
           ,this.w_STATORD;
           ,"F";
           ,this.w_CODCON;
           ,this.w_PERASS;
           ,this.w_CONUMERO;
           ,this.w_CRITERIO;
           ,this.w_VOCCOS;
           ,this.w_CRIELAB;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04AEA098()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_PROD
    i_nConn=i_TableProp[this.PAR_PROD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_PROD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PPCODICE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC("AA"),'PAR_PROD','PPCODICE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PPCODICE',"AA")
      insert into (i_cTable) (PPCODICE &i_ccchkf. );
         values (;
           "AA";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- GSDB_BLT
    * --- Calcola la data di inizio in base al Lead Time
    SET_NEAR = Set("NEAR")
    SET NEAR ON
    this.DatRisul = {}
    this.w_DatErr = FALSE
    this.w_DatErrMsg = ""
    select Calend
    if seek(this.DatInput)
      * --- Data evasione trovata
      if this.w_TipoPianificazione="I"
        skip -ceiling(this.w_GIOAPP)
        if bof()
          * --- Data anteriore all'orizzonte di pianificazione
          this.w_DatErrMsg = "Raggiunto limite inferiore dell'orizzonte di pianificazione"
          this.w_DatErr = TRUE
        endif
      else
        skip +ceiling(this.w_GIOAPP)
        if eof()
          * --- Data posteriore all'orizzonte di pianificazione
          this.w_DatErrMsg = "Raggiunto limite superiore dell'orizzonte di pianificazione"
          this.w_DatErr = TRUE
        endif
      endif
    else
      if EOF()
        * --- Data posteriore all'orizzonte di pianificazione
        this.w_DatErrMsg = "Raggiunto limite superiore dell'orizzonte di pianificazione"
        this.w_DatErr = TRUE
      else
        if this.w_TipoPianificazione="I"
          skip -(ceiling(this.w_GIOAPP)+1)
          if bof()
            * --- Data anteriore all'orizzonte di pianificazione
            this.w_DatErrMsg = "Raggiunto limite inferiore dell'orizzonte di pianificazione"
            this.w_DatErr = TRUE
          endif
        else
          skip +ceiling(this.w_GIOAPP)
          if eof()
            * --- Data posteriore all'orizzonte di pianificazione
            this.w_DatErrMsg = "Raggiunto limite superiore dell'orizzonte di pianificazione"
            this.w_DatErr = TRUE
          endif
        endif
      endif
    endif
    this.DatRisul = cp_todate(cagiorno)
    SET NEAR &SET_NEAR
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PDA_DETT'
    this.cWorkTables[2]='PAR_PROD'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
