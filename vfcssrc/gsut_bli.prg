* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bli                                                        *
*              Controllo modifica libreria                                     *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][22]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-20                                                      *
* Last revis.: 2005-05-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bli",oParentObject,m.pOper)
return(i_retval)

define class tgsut_bli as StdBatch
  * --- Local variables
  pOper = space(1)
  w_PADRE = .NULL.
  w_GSUT_KSI = .NULL.
  w_DIRINI = space(254)
  w_DIRFIN = space(254)
  * --- WorkFile variables
  CLA_ALLE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da GSUT_MLI per controllare la modifica dei persorsi di memorizzazione immagini
    this.oParentObject.w_MODIFICA = .F.
    if UPPER(this.oParentObject.w_LICODICE)= "GESTFILE"
      * --- Cambiamento path memorizzazione allegati
      this.w_PADRE = This.oParentObject
      this.w_PADRE.MarkPos()     
      this.w_PADRE.FirstRow()     
      do while Not this.w_PADRE.Eof_Trs()
        this.w_PADRE.SetRow()     
        * --- Uso '==' come confrontatore poich� in alcuni casi dava problemi
        if this.w_PADRE.FullRow() And Not( Alltrim(this.oParentObject.w_LIPATIMG)==Alltrim(this.oParentObject.w_OPATIMG) )
          do case
            case this.pOper="C"
              * --- Se controllo indico che su una riga � stato modificato il Path di Memorizzazione
              this.oParentObject.w_MODIFICA = .T.
            case this.pOper="M"
              * --- Se salvataggio con riga modificata lancio la gestione di modifica path
              this.w_DIRINI = Alltrim(this.oParentObject.w_OPATIMG)+IIF(Right(Alltrim(this.oParentObject.w_OPATIMG),1)="\","","\")
              this.w_DIRFIN = Alltrim(this.oParentObject.w_LIPATIMG)+IIF(Right(Alltrim(this.oParentObject.w_LIPATIMG),1)="\","","\")
              do GSUT_BKS with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
          endcase
        endif
        this.w_PADRE.NextRow()     
      enddo
      this.w_PADRE.RePos()     
    endif
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CLA_ALLE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
