* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_aao                                                        *
*              Add-on                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_26]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-08-03                                                      *
* Last revis.: 2008-09-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_aao"))

* --- Class definition
define class tgsut_aao as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 420
  Height = 128+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-01"
  HelpContextID=125222039
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  ADDON_IDX = 0
  cFile = "ADDON"
  cKeySelect = "ADCODICE"
  cKeyWhere  = "ADCODICE=this.w_ADCODICE"
  cKeyWhereODBC = '"ADCODICE="+cp_ToStrODBC(this.w_ADCODICE)';

  cKeyWhereODBCqualified = '"ADDON.ADCODICE="+cp_ToStrODBC(this.w_ADCODICE)';

  cPrg = "gsut_aao"
  cComment = "Add-on"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ADCODICE = space(8)
  w_ADDESCRI = space(40)
  w_ADDESPRO = space(40)
  w_INFO = space(30)
  w_ADNOMMOD = space(8)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ADDON','gsut_aao')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_aaoPag1","gsut_aao",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Add-on")
      .Pages(1).HelpContextID = 56990458
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oADCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ADDON'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ADDON_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ADDON_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ADCODICE = NVL(ADCODICE,space(8))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ADDON where ADCODICE=KeySet.ADCODICE
    *
    i_nConn = i_TableProp[this.ADDON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ADDON_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ADDON')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ADDON.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ADDON '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ADCODICE',this.w_ADCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ADCODICE = NVL(ADCODICE,space(8))
        .w_ADDESCRI = NVL(ADDESCRI,space(40))
        .w_ADDESPRO = NVL(ADDESPRO,space(40))
        .w_INFO = iif(.w_ADCODICE="A","Add-On 3^ Parti",iif(.w_ADCODICE="V","Verticalizzazione",""))
        .w_ADNOMMOD = NVL(ADNOMMOD,space(8))
        cp_LoadRecExtFlds(this,'ADDON')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ADCODICE = space(8)
      .w_ADDESCRI = space(40)
      .w_ADDESPRO = space(40)
      .w_INFO = space(30)
      .w_ADNOMMOD = space(8)
      if .cFunction<>"Filter"
          .DoRTCalc(1,3,.f.)
        .w_INFO = iif(.w_ADCODICE="A","Add-On 3^ Parti",iif(.w_ADCODICE="V","Verticalizzazione",""))
      endif
    endwith
    cp_BlankRecExtFlds(this,'ADDON')
    this.DoRTCalc(5,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oADCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oADDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oADDESPRO_1_5.enabled = i_bVal
      .Page1.oPag.oADNOMMOD_1_8.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oADCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oADCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ADDON',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ADDON_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ADCODICE,"ADCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ADDESCRI,"ADDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ADDESPRO,"ADDESPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ADNOMMOD,"ADNOMMOD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ADDON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ADDON_IDX,2])
    i_lTable = "ADDON"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ADDON_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ADDON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ADDON_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ADDON_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ADDON
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ADDON')
        i_extval=cp_InsertValODBCExtFlds(this,'ADDON')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ADCODICE,ADDESCRI,ADDESPRO,ADNOMMOD "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ADCODICE)+;
                  ","+cp_ToStrODBC(this.w_ADDESCRI)+;
                  ","+cp_ToStrODBC(this.w_ADDESPRO)+;
                  ","+cp_ToStrODBC(this.w_ADNOMMOD)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ADDON')
        i_extval=cp_InsertValVFPExtFlds(this,'ADDON')
        cp_CheckDeletedKey(i_cTable,0,'ADCODICE',this.w_ADCODICE)
        INSERT INTO (i_cTable);
              (ADCODICE,ADDESCRI,ADDESPRO,ADNOMMOD  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ADCODICE;
                  ,this.w_ADDESCRI;
                  ,this.w_ADDESPRO;
                  ,this.w_ADNOMMOD;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ADDON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ADDON_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ADDON_IDX,i_nConn)
      *
      * update ADDON
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ADDON')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ADDESCRI="+cp_ToStrODBC(this.w_ADDESCRI)+;
             ",ADDESPRO="+cp_ToStrODBC(this.w_ADDESPRO)+;
             ",ADNOMMOD="+cp_ToStrODBC(this.w_ADNOMMOD)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ADDON')
        i_cWhere = cp_PKFox(i_cTable  ,'ADCODICE',this.w_ADCODICE  )
        UPDATE (i_cTable) SET;
              ADDESCRI=this.w_ADDESCRI;
             ,ADDESPRO=this.w_ADDESPRO;
             ,ADNOMMOD=this.w_ADNOMMOD;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ADDON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ADDON_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ADDON_IDX,i_nConn)
      *
      * delete ADDON
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ADCODICE',this.w_ADCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ADDON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ADDON_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
            .w_INFO = iif(.w_ADCODICE="A","Add-On 3^ Parti",iif(.w_ADCODICE="V","Verticalizzazione",""))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oADCODICE_1_1.value==this.w_ADCODICE)
      this.oPgFrm.Page1.oPag.oADCODICE_1_1.value=this.w_ADCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oADDESCRI_1_3.value==this.w_ADDESCRI)
      this.oPgFrm.Page1.oPag.oADDESCRI_1_3.value=this.w_ADDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oADDESPRO_1_5.value==this.w_ADDESPRO)
      this.oPgFrm.Page1.oPag.oADDESPRO_1_5.value=this.w_ADDESPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oINFO_1_7.value==this.w_INFO)
      this.oPgFrm.Page1.oPag.oINFO_1_7.value=this.w_INFO
    endif
    if not(this.oPgFrm.Page1.oPag.oADNOMMOD_1_8.value==this.w_ADNOMMOD)
      this.oPgFrm.Page1.oPag.oADNOMMOD_1_8.value=this.w_ADNOMMOD
    endif
    cp_SetControlsValueExtFlds(this,'ADDON')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(len(alltrim(.w_ADCODICE)) >= 4)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oADCODICE_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice dell'add-on / verticalizzazione deve essere lungo almeno 4 caratteri.")
          case   (empty(.w_ADDESCRI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oADDESCRI_1_3.SetFocus()
            i_bnoObbl = !empty(.w_ADDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ADDESPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oADDESPRO_1_5.SetFocus()
            i_bnoObbl = !empty(.w_ADDESPRO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!(alltrim(.w_ADNOMMOD)$"ACQU|OFFE|COMM|STAT|CESP|RITE|IMPO|DIBA|BILC|COLA"))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oADNOMMOD_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il nome dell'analisi non � valido. Non � possibile utilizzare un nome gi� utilizzato nei moduli standard.")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_aaoPag1 as StdContainer
  Width  = 416
  height = 128
  stdWidth  = 416
  stdheight = 128
  resizeXpos=291
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oADCODICE_1_1 as StdField with uid="GKAUJJHCRL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ADCODICE", cQueryName = "ADCODICE",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice dell'add-on / verticalizzazione deve essere lungo almeno 4 caratteri.",;
    ToolTipText = "A: add-on 3^ parti; V: verticalizzazione;",;
    HelpContextID = 183880373,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=113, Top=21, cSayPict="'!!!!!!!!'", cGetPict="'!!!!!!!!'", InputMask=replicate('X',8)

  func oADCODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (len(alltrim(.w_ADCODICE)) >= 4)
    endwith
    return bRes
  endfunc

  add object oADDESCRI_1_3 as StdField with uid="TLZTQMBVRD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ADDESCRI", cQueryName = "ADDESCRI",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 1030833,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=113, Top=48, InputMask=replicate('X',40)

  add object oADDESPRO_1_5 as StdField with uid="YLNAYRESGN",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ADDESPRO", cQueryName = "ADDESPRO",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 51362475,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=113, Top=75, InputMask=replicate('X',40)

  add object oINFO_1_7 as StdField with uid="UTVVREZGZO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_INFO", cQueryName = "INFO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 130707334,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=192, Top=21, InputMask=replicate('X',30)

  add object oADNOMMOD_1_8 as StdField with uid="LZNTMWVNFR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ADNOMMOD", cQueryName = "ADNOMMOD",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    sErrorMsg = "Il nome dell'analisi non � valido. Non � possibile utilizzare un nome gi� utilizzato nei moduli standard.",;
    ToolTipText = "Nome del file di analisi associato al modulo (deve coincidere con il nome della cartella in cui verr� generato)",;
    HelpContextID = 107289270,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=113, Top=102, cSayPict="'!!!!!!!!'", cGetPict="'!!!!!!!!'", InputMask=replicate('X',8)

  func oADNOMMOD_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!(alltrim(.w_ADNOMMOD)$"ACQU|OFFE|COMM|STAT|CESP|RITE|IMPO|DIBA|BILC|COLA"))
    endwith
    return bRes
  endfunc

  add object oStr_1_2 as StdString with uid="VQWESYDFYK",Visible=.t., Left=20, Top=21,;
    Alignment=1, Width=91, Height=18,;
    Caption="Codice add-on:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="PLOCXOBLBE",Visible=.t., Left=20, Top=48,;
    Alignment=1, Width=91, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="KANRMIRHMN",Visible=.t., Left=20, Top=75,;
    Alignment=1, Width=91, Height=18,;
    Caption="Produttore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="POMAXDESHP",Visible=.t., Left=5, Top=102,;
    Alignment=1, Width=106, Height=18,;
    Caption="Nome analisi:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_aao','ADDON','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ADCODICE=ADDON.ADCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
