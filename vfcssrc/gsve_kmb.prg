* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kmb                                                        *
*              Modifica banca appoggio/ns banca                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_71]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-12                                                      *
* Last revis.: 2011-03-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kmb",oParentObject))

* --- Class definition
define class tgsve_kmb as StdForm
  Top    = 18
  Left   = 19

  * --- Standard Properties
  Width  = 593
  Height = 399+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-07"
  HelpContextID=199897705
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Constant Properties
  _IDX = 0
  BAN_CONTI_IDX = 0
  BAN_CHE_IDX = 0
  COC_MAST_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsve_kmb"
  cComment = "Modifica banca appoggio/ns banca"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLVEAC = space(1)
  w_TIPO = space(1)
  w_CODCLF = space(15)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_TIPOELENCO = space(10)
  o_TIPOELENCO = space(10)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_SELEZ = space(1)
  w_BANAPP = space(10)
  o_BANAPP = space(10)
  w_CONCOR = space(25)
  w_BANNOS = space(15)
  w_DESBAN = space(50)
  w_DESNOS = space(35)
  w_FLSELE = 0
  w_BANCAAP = space(10)
  w_DESAPP = space(50)
  w_CONTOC = space(25)
  w_DESCRI = space(40)
  w_DT = space(1)
  w_NC = space(1)
  w_DI = space(1)
  w_RF = space(1)
  w_OP = space(1)
  w_FA = space(1)
  w_OR = space(1)
  w_SERIALE = space(10)
  w_TESTATA = space(1)
  w_CONSBF = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_BANNOS = space(15)
  w_ZoomScad = .NULL.
  w_ZoomDocu = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsve_kmb
  * --- In questa A.M. viene inizializzata la Variabilei Globale del Form che Identifica
  * --- La Categoria Documento in base al Valore del Parametro.
  * --- tipo Documento
  pflveac = ''
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSVE_KMB'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kmbPag1","gsve_kmb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(2).addobject("oPag","tgsve_kmbPag2","gsve_kmb",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCLF_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomScad = this.oPgFrm.Pages(1).oPag.ZoomScad
    this.w_ZoomDocu = this.oPgFrm.Pages(1).oPag.ZoomDocu
    DoDefault()
    proc Destroy()
      this.w_ZoomScad = .NULL.
      this.w_ZoomDocu = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='BAN_CONTI'
    this.cWorkTables[2]='BAN_CHE'
    this.cWorkTables[3]='COC_MAST'
    this.cWorkTables[4]='CONTI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLVEAC=space(1)
      .w_TIPO=space(1)
      .w_CODCLF=space(15)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_TIPOELENCO=space(10)
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_SELEZ=space(1)
      .w_BANAPP=space(10)
      .w_CONCOR=space(25)
      .w_BANNOS=space(15)
      .w_DESBAN=space(50)
      .w_DESNOS=space(35)
      .w_FLSELE=0
      .w_BANCAAP=space(10)
      .w_DESAPP=space(50)
      .w_CONTOC=space(25)
      .w_DESCRI=space(40)
      .w_DT=space(1)
      .w_NC=space(1)
      .w_DI=space(1)
      .w_RF=space(1)
      .w_OP=space(1)
      .w_FA=space(1)
      .w_OR=space(1)
      .w_SERIALE=space(10)
      .w_TESTATA=space(1)
      .w_CONSBF=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_BANNOS=space(15)
        .w_FLVEAC = this.oParentObject
        .w_TIPO = IIF(.w_FLVEAC='V', 'C','F')
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODCLF))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,5,.f.)
        .w_TIPOELENCO = ' '
      .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .w_TIPCON = .w_ZoomScad.getVar('MVTIPCON')
        .w_CODCON = .w_ZoomScad.getVar('MVCODCON')
        .w_SELEZ = 'D'
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_BANAPP))
          .link_1_12('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CONCOR))
          .link_1_13('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_BANNOS))
          .link_1_14('Full')
        endif
          .DoRTCalc(13,14,.f.)
        .w_FLSELE = 0
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_BANCAAP))
          .link_2_1('Full')
        endif
        .DoRTCalc(17,18,.f.)
        if not(empty(.w_CONTOC))
          .link_2_5('Full')
        endif
          .DoRTCalc(19,19,.f.)
        .w_DT = 'DT'
        .w_NC = 'NC'
        .w_DI = 'DI'
        .w_RF = 'RF'
        .w_OP = 'OP'
        .w_FA = 'FA'
        .w_OR = 'OR'
        .w_SERIALE = IIF( EMPTY(.w_TIPOELENCO ) , .w_ZoomScad.getVar('MVSERIAL') , .w_ZoomDocu.getVar('MVSERIAL') )
        .w_TESTATA = 'S'
      .oPgFrm.Page1.oPag.ZoomDocu.Calculate()
          .DoRTCalc(29,29,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_BANNOS))
          .link_1_36('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_TIPO = IIF(.w_FLVEAC='V', 'C','F')
        .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .DoRTCalc(3,6,.t.)
            .w_TIPCON = .w_ZoomScad.getVar('MVTIPCON')
            .w_CODCON = .w_ZoomScad.getVar('MVCODCON')
        .DoRTCalc(9,10,.t.)
        if .o_BANAPP<>.w_BANAPP
          .link_1_13('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .DoRTCalc(12,26,.t.)
            .w_SERIALE = IIF( EMPTY(.w_TIPOELENCO ) , .w_ZoomScad.getVar('MVSERIAL') , .w_ZoomDocu.getVar('MVSERIAL') )
        .oPgFrm.Page1.oPag.ZoomDocu.Calculate()
        if .o_TIPOELENCO<>.w_TIPOELENCO
          .Calculate_ZNTFXPFFLH()
        endif
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.ZoomDocu.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
    endwith
  return

  proc Calculate_ZNTFXPFFLH()
    with this
          * --- Visualizza zoom documenti o rate
          .w_ZOOMSCAD.VISIBLE = EMPTY( .w_TIPOELENCO )
          .w_ZOOMDOCU.VISIBLE = .w_TIPOELENCO = 'S'
          .w_TESTATA = IIF( .w_TIPOELENCO = 'S' , 'S' , ' ' )
    endwith
  endproc
  proc Calculate_TTVPLTAKKO()
    with this
          * --- Aggiorna testata
          .w_TESTATA = IIF  (upper(g_APPLICATION) <> "ADHOC REVOLUTION",'S',.w_TESTATA)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTESTATA_1_30.enabled = this.oPgFrm.Page1.oPag.oTESTATA_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBANNOS_1_14.visible=!this.oPgFrm.Page1.oPag.oBANNOS_1_14.mHide()
    this.oPgFrm.Page2.oPag.oRF_2_9.visible=!this.oPgFrm.Page2.oPag.oRF_2_9.mHide()
    this.oPgFrm.Page2.oPag.oOP_2_10.visible=!this.oPgFrm.Page2.oPag.oOP_2_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    this.oPgFrm.Page1.oPag.oTESTATA_1_30.visible=!this.oPgFrm.Page1.oPag.oTESTATA_1_30.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_34.visible=!this.oPgFrm.Page1.oPag.oBtn_1_34.mHide()
    this.oPgFrm.Page1.oPag.oBANNOS_1_36.visible=!this.oPgFrm.Page1.oPag.oBANNOS_1_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomScad.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomDocu.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_ZNTFXPFFLH()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_TTVPLTAKKO()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCLF
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPO;
                     ,'ANCODICE',trim(this.w_CODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLF_1_3'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Inserire un codice cliente o fornitore")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPO;
                       ,'ANCODICE',this.w_CODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLF = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=not empty(.w_CODCLF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un codice cliente o fornitore")
        endif
        this.w_CODCLF = space(15)
        this.w_DESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANAPP
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANAPP)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANAPP))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANAPP)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BANAPP) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oBANAPP_1_12'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANAPP)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANAPP = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_BANAPP = space(10)
      endif
      this.w_DESBAN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONCOR
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
    i_lTable = "BAN_CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2], .t., this.BAN_CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BAN_CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCONCOR like "+cp_ToStrODBC(trim(this.w_CONCOR)+"%");
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_CODCLF);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_BANAPP);

          i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPCON',this.w_TIPO;
                     ,'CCCODCON',this.w_CODCLF;
                     ,'CCCODBAN',this.w_BANAPP;
                     ,'CCCONCOR',trim(this.w_CONCOR))
          select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONCOR)==trim(_Link_.CCCONCOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONCOR) and !this.bDontReportError
            deferred_cp_zoom('BAN_CONTI','*','CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR',cp_AbsName(oSource.parent,'oCONCOR_1_13'),i_cWhere,'',"Elenco conti correnti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPO<>oSource.xKey(1);
           .or. this.w_CODCLF<>oSource.xKey(2);
           .or. this.w_BANAPP<>oSource.xKey(3);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                     +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(oSource.xKey(4));
                     +" and CCTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     +" and CCCODCON="+cp_ToStrODBC(this.w_CODCLF);
                     +" and CCCODBAN="+cp_ToStrODBC(this.w_BANAPP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',oSource.xKey(1);
                       ,'CCCODCON',oSource.xKey(2);
                       ,'CCCODBAN',oSource.xKey(3);
                       ,'CCCONCOR',oSource.xKey(4))
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                   +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(this.w_CONCOR);
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_CODCLF);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_BANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',this.w_TIPO;
                       ,'CCCODCON',this.w_CODCLF;
                       ,'CCCODBAN',this.w_BANAPP;
                       ,'CCCONCOR',this.w_CONCOR)
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONCOR = NVL(_Link_.CCCONCOR,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_CONCOR = space(25)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPCON,1)+'\'+cp_ToStr(_Link_.CCCODCON,1)+'\'+cp_ToStr(_Link_.CCCODBAN,1)+'\'+cp_ToStr(_Link_.CCCONCOR,1)
      cp_ShowWarn(i_cKey,this.BAN_CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANNOS
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANNOS))
          select BACODBAN,BADESCRI,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANNOS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_BANNOS)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_BANNOS)+"%");

            select BACODBAN,BADESCRI,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_BANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oBANNOS_1_14'),i_cWhere,'GSTE_ACB',"Conti banca",'GSVE2KMB.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANNOS)
            select BACODBAN,BADESCRI,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANNOS = NVL(_Link_.BACODBAN,space(15))
      this.w_DESNOS = NVL(_Link_.BADESCRI,space(35))
      this.w_CONSBF = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_BANNOS = space(15)
      endif
      this.w_DESNOS = space(35)
      this.w_CONSBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CONSBF='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Impossibile selezionare: conto banca di tipo salvo buon fine")
        endif
        this.w_BANNOS = space(15)
        this.w_DESNOS = space(35)
        this.w_CONSBF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANCAAP
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANCAAP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANCAAP)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANCAAP))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANCAAP)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BANCAAP) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oBANCAAP_2_1'),i_cWhere,'GSAR_ABA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANCAAP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANCAAP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANCAAP)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANCAAP = NVL(_Link_.BACODBAN,space(10))
      this.w_DESAPP = NVL(_Link_.BADESBAN,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_BANCAAP = space(10)
      endif
      this.w_DESAPP = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANCAAP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONTOC
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
    i_lTable = "BAN_CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2], .t., this.BAN_CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONTOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BAN_CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCONCOR like "+cp_ToStrODBC(trim(this.w_CONTOC)+"%");
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_BANCAAP);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_CODCLF);
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_TIPO);

          i_ret=cp_SQL(i_nConn,"select CCCODBAN,CCCODCON,CCTIPCON,CCCONCOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODBAN,CCCODCON,CCTIPCON,CCCONCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODBAN',this.w_BANCAAP;
                     ,'CCCODCON',this.w_CODCLF;
                     ,'CCTIPCON',this.w_TIPO;
                     ,'CCCONCOR',trim(this.w_CONTOC))
          select CCCODBAN,CCCODCON,CCTIPCON,CCCONCOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODBAN,CCCODCON,CCTIPCON,CCCONCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONTOC)==trim(_Link_.CCCONCOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONTOC) and !this.bDontReportError
            deferred_cp_zoom('BAN_CONTI','*','CCCODBAN,CCCODCON,CCTIPCON,CCCONCOR',cp_AbsName(oSource.parent,'oCONTOC_2_5'),i_cWhere,'',"Elenco conti correnti",'gste_kmb.BAN_CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_BANCAAP<>oSource.xKey(1);
           .or. this.w_CODCLF<>oSource.xKey(2);
           .or. this.w_TIPO<>oSource.xKey(3);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODBAN,CCCODCON,CCTIPCON,CCCONCOR";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCCODBAN,CCCODCON,CCTIPCON,CCCONCOR;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODBAN,CCCODCON,CCTIPCON,CCCONCOR";
                     +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(oSource.xKey(4));
                     +" and CCCODBAN="+cp_ToStrODBC(this.w_BANCAAP);
                     +" and CCCODCON="+cp_ToStrODBC(this.w_CODCLF);
                     +" and CCTIPCON="+cp_ToStrODBC(this.w_TIPO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODBAN',oSource.xKey(1);
                       ,'CCCODCON',oSource.xKey(2);
                       ,'CCTIPCON',oSource.xKey(3);
                       ,'CCCONCOR',oSource.xKey(4))
            select CCCODBAN,CCCODCON,CCTIPCON,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONTOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODBAN,CCCODCON,CCTIPCON,CCCONCOR";
                   +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(this.w_CONTOC);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_BANCAAP);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_CODCLF);
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_TIPO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODBAN',this.w_BANCAAP;
                       ,'CCCODCON',this.w_CODCLF;
                       ,'CCTIPCON',this.w_TIPO;
                       ,'CCCONCOR',this.w_CONTOC)
            select CCCODBAN,CCCODCON,CCTIPCON,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONTOC = NVL(_Link_.CCCONCOR,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_CONTOC = space(25)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])+'\'+cp_ToStr(_Link_.CCCODBAN,1)+'\'+cp_ToStr(_Link_.CCCODCON,1)+'\'+cp_ToStr(_Link_.CCTIPCON,1)+'\'+cp_ToStr(_Link_.CCCONCOR,1)
      cp_ShowWarn(i_cKey,this.BAN_CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONTOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANNOS
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACC',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BANUMCOR like "+cp_ToStrODBC(trim(this.w_BANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BAFLANEV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BANUMCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BANUMCOR',trim(this.w_BANNOS))
          select BANUMCOR,BADESCRI,BAFLANEV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BANUMCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANNOS)==trim(_Link_.BANUMCOR) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_BANNOS)+"%");

            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BAFLANEV";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_BANNOS)+"%");

            select BANUMCOR,BADESCRI,BAFLANEV;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_BANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BANUMCOR',cp_AbsName(oSource.parent,'oBANNOS_1_36'),i_cWhere,'GSTE_ACC',"Conti banca",'GSTE5MCC.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BAFLANEV";
                     +" from "+i_cTable+" "+i_lTable+" where BANUMCOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BANUMCOR',oSource.xKey(1))
            select BANUMCOR,BADESCRI,BAFLANEV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI,BAFLANEV";
                   +" from "+i_cTable+" "+i_lTable+" where BANUMCOR="+cp_ToStrODBC(this.w_BANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BANUMCOR',this.w_BANNOS)
            select BANUMCOR,BADESCRI,BAFLANEV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANNOS = NVL(_Link_.BANUMCOR,space(15))
      this.w_DESNOS = NVL(_Link_.BADESCRI,space(35))
      this.w_CONSBF = NVL(_Link_.BAFLANEV,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_BANNOS = space(15)
      endif
      this.w_DESNOS = space(35)
      this.w_CONSBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BANUMCOR,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCLF_1_3.value==this.w_CODCLF)
      this.oPgFrm.Page1.oPag.oCODCLF_1_3.value=this.w_CODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_4.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_4.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_5.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_5.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOELENCO_1_6.RadioValue()==this.w_TIPOELENCO)
      this.oPgFrm.Page1.oPag.oTIPOELENCO_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZ_1_11.RadioValue()==this.w_SELEZ)
      this.oPgFrm.Page1.oPag.oSELEZ_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBANAPP_1_12.value==this.w_BANAPP)
      this.oPgFrm.Page1.oPag.oBANAPP_1_12.value=this.w_BANAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oCONCOR_1_13.value==this.w_CONCOR)
      this.oPgFrm.Page1.oPag.oCONCOR_1_13.value=this.w_CONCOR
    endif
    if not(this.oPgFrm.Page1.oPag.oBANNOS_1_14.value==this.w_BANNOS)
      this.oPgFrm.Page1.oPag.oBANNOS_1_14.value=this.w_BANNOS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBAN_1_15.value==this.w_DESBAN)
      this.oPgFrm.Page1.oPag.oDESBAN_1_15.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOS_1_16.value==this.w_DESNOS)
      this.oPgFrm.Page1.oPag.oDESNOS_1_16.value=this.w_DESNOS
    endif
    if not(this.oPgFrm.Page2.oPag.oBANCAAP_2_1.value==this.w_BANCAAP)
      this.oPgFrm.Page2.oPag.oBANCAAP_2_1.value=this.w_BANCAAP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAPP_2_4.value==this.w_DESAPP)
      this.oPgFrm.Page2.oPag.oDESAPP_2_4.value=this.w_DESAPP
    endif
    if not(this.oPgFrm.Page2.oPag.oCONTOC_2_5.value==this.w_CONTOC)
      this.oPgFrm.Page2.oPag.oCONTOC_2_5.value=this.w_CONTOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_27.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_27.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDT_2_6.RadioValue()==this.w_DT)
      this.oPgFrm.Page2.oPag.oDT_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNC_2_7.RadioValue()==this.w_NC)
      this.oPgFrm.Page2.oPag.oNC_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDI_2_8.RadioValue()==this.w_DI)
      this.oPgFrm.Page2.oPag.oDI_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRF_2_9.RadioValue()==this.w_RF)
      this.oPgFrm.Page2.oPag.oRF_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOP_2_10.RadioValue()==this.w_OP)
      this.oPgFrm.Page2.oPag.oOP_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFA_2_11.RadioValue()==this.w_FA)
      this.oPgFrm.Page2.oPag.oFA_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOR_2_12.RadioValue()==this.w_OR)
      this.oPgFrm.Page2.oPag.oOR_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTESTATA_1_30.RadioValue()==this.w_TESTATA)
      this.oPgFrm.Page1.oPag.oTESTATA_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBANNOS_1_36.value==this.w_BANNOS)
      this.oPgFrm.Page1.oPag.oBANNOS_1_36.value=this.w_BANNOS
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CODCLF)) or not(not empty(.w_CODCLF)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCLF_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODCLF)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un codice cliente o fornitore")
          case   not((.w_DATINI<=.w_DATFIN) or (empty(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data registrazione iniziale � maggiore della scadenza finale")
          case   not((.w_datini<=.w_datfin) or (empty(.w_datini)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data registrazione iniziale � maggiore della scadenza finale")
          case   not(.w_CONSBF='C')  and not(upper(g_APPLICATION) <> "ADHOC REVOLUTION")  and not(empty(.w_BANNOS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBANNOS_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile selezionare: conto banca di tipo salvo buon fine")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOELENCO = this.w_TIPOELENCO
    this.o_BANAPP = this.w_BANAPP
    return

enddefine

* --- Define pages as container
define class tgsve_kmbPag1 as StdContainer
  Width  = 589
  height = 399
  stdWidth  = 589
  stdheight = 399
  resizeXpos=408
  resizeYpos=196
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCLF_1_3 as StdField with uid="WJKDXJUWRS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODCLF", cQueryName = "CODCLF",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un codice cliente o fornitore",;
    ToolTipText = "Cliente/fornitore di selezione scadenze",;
    HelpContextID = 14851802,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=81, Top=11, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLF"

  func oCODCLF_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
      if .not. empty(.w_CONCOR)
        bRes2=.link_1_13('Full')
      endif
      if .not. empty(.w_CONTOC)
        bRes2=.link_2_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCLF_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLF_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLF_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'',this.parent.oContained
  endproc
  proc oCODCLF_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPO
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCLF
     i_obj.ecpSave()
  endproc

  add object oDATINI_1_4 as StdField with uid="SCLWXOZGEQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data registrazione iniziale � maggiore della scadenza finale",;
    HelpContextID = 230403274,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=81, Top=39

  func oDATINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINI<=.w_DATFIN) or (empty(.w_DATFIN)))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_5 as StdField with uid="VAKJQMESIL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data registrazione iniziale � maggiore della scadenza finale",;
    HelpContextID = 151956682,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=202, Top=39

  func oDATFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_datini<=.w_datfin) or (empty(.w_datini)))
    endwith
    return bRes
  endfunc

  add object oTIPOELENCO_1_6 as StdCheck with uid="UQTTNLWUKL",rtseq=6,rtrep=.f.,left=342, top=39, caption="Documenti senza rate",;
    HelpContextID = 79327156,;
    cFormVar="w_TIPOELENCO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPOELENCO_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oTIPOELENCO_1_6.GetRadio()
    this.Parent.oContained.w_TIPOELENCO = this.RadioValue()
    return .t.
  endfunc

  func oTIPOELENCO_1_6.SetRadio()
    this.Parent.oContained.w_TIPOELENCO=trim(this.Parent.oContained.w_TIPOELENCO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOELENCO=='S',1,;
      0)
  endfunc


  add object ZoomScad as cp_szoombox with uid="LAUPSHFERA",left=0, top=65, width=587,height=179,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_RATE",cZoomFile="GSVE_KMB",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Ricerca1",;
    nPag=1;
    , HelpContextID = 246535398


  add object oBtn_1_8 as StdButton with uid="OSKJUCDDJL",left=535, top=11, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la query";
    , HelpContextID = 245459990;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        iif(.w_TIPOELENCO = 'S', .notifyevent('Ricerca2'), .notifyevent('Ricerca1'))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZ_1_11 as StdRadio with uid="RMUJOFEZEL",rtseq=9,rtrep=.f.,left=4, top=249, width=247,height=23;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZ", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZ_1_11.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 100673498
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 100673498
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZ_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZ_1_11.GetRadio()
    this.Parent.oContained.w_SELEZ = this.RadioValue()
    return .t.
  endfunc

  func oSELEZ_1_11.SetRadio()
    this.Parent.oContained.w_SELEZ=trim(this.Parent.oContained.w_SELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZ=='S',1,;
      iif(this.Parent.oContained.w_SELEZ=='D',2,;
      0))
  endfunc

  add object oBANAPP_1_12 as StdField with uid="ATCNHELMXG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_BANAPP", cQueryName = "BANAPP",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca di appoggio da inserire",;
    HelpContextID = 111414506,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=118, Top=277, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANAPP"

  func oBANAPP_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
      if .not. empty(.w_CONCOR)
        bRes2=.link_1_13('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oBANAPP_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANAPP_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oBANAPP_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oBANAPP_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANAPP
     i_obj.ecpSave()
  endproc

  add object oCONCOR_1_13 as StdField with uid="CCBHFXPYGS",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CONCOR", cQueryName = "CONCOR",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Conto corrente del cliente/fornitore da inserire",;
    HelpContextID = 78773978,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=118, Top=302, InputMask=replicate('X',25), bHasZoom = .t. , cLinkFile="BAN_CONTI", oKey_1_1="CCTIPCON", oKey_1_2="this.w_TIPO", oKey_2_1="CCCODCON", oKey_2_2="this.w_CODCLF", oKey_3_1="CCCODBAN", oKey_3_2="this.w_BANAPP", oKey_4_1="CCCONCOR", oKey_4_2="this.w_CONCOR"

  func oCONCOR_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONCOR_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONCOR_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BAN_CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStrODBC(this.Parent.oContained.w_CODCLF)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStrODBC(this.Parent.oContained.w_BANAPP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStr(this.Parent.oContained.w_CODCLF)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStr(this.Parent.oContained.w_BANAPP)
    endif
    do cp_zoom with 'BAN_CONTI','*','CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR',cp_AbsName(this.parent,'oCONCOR_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco conti correnti",'',this.parent.oContained
  endproc

  add object oBANNOS_1_14 as StdField with uid="UXGTTWXAIZ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_BANNOS", cQueryName = "BANNOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Impossibile selezionare: conto banca di tipo salvo buon fine",;
    ToolTipText = "Nostra banca da inserire",;
    HelpContextID = 61279466,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=118, Top=327, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANNOS"

  func oBANNOS_1_14.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  func oBANNOS_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANNOS_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANNOS_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oBANNOS_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banca",'GSVE2KMB.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oBANNOS_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANNOS
     i_obj.ecpSave()
  endproc

  add object oDESBAN_1_15 as StdField with uid="IBEJNXIWZL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 160610506,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=225, Top=277, InputMask=replicate('X',50)

  add object oDESNOS_1_16 as StdField with uid="HHVHPAOJGO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESNOS", cQueryName = "DESNOS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 61257930,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=262, Top=327, InputMask=replicate('X',35)


  add object oBtn_1_22 as StdButton with uid="VRONCUQWAW",left=535, top=354, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 192580282;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="VKSUQUQADC",left=482, top=354, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare la banca d'appoggio/ns banca";
    , HelpContextID = 199868954;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        GSVE_BMB(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_25 as cp_runprogram with uid="OOFZJBQMXG",left=-5, top=428, width=220,height=21,;
    caption='GSVE_BMB(S)',;
   bGlobalFont=.t.,;
    prg='GSVE_BMB("S")',;
    cEvent = "w_SELEZ Changed",;
    nPag=1;
    , HelpContextID = 61641944

  add object oDESCRI_1_27 as StdField with uid="YOSBZBIGLV",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 226605258,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=202, Top=11, InputMask=replicate('X',40)


  add object oBtn_1_29 as StdButton with uid="BVKLRPSWXT",left=8, top=354, width=48,height=45,;
    CpPicture="bmp\documenti.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento";
    , HelpContextID = 262047590;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        gsar_bzm(this.Parent.oContained,.w_SERIALE,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  func oBtn_1_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
     endwith
    endif
  endfunc

  add object oTESTATA_1_30 as StdCheck with uid="BOROVIGMLT",rtseq=28,rtrep=.f.,left=468, top=249, caption="Aggiorna testata",;
    HelpContextID = 58767306,;
    cFormVar="w_TESTATA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTESTATA_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTESTATA_1_30.GetRadio()
    this.Parent.oContained.w_TESTATA = this.RadioValue()
    return .t.
  endfunc

  func oTESTATA_1_30.SetRadio()
    this.Parent.oContained.w_TESTATA=trim(this.Parent.oContained.w_TESTATA)
    this.value = ;
      iif(this.Parent.oContained.w_TESTATA=='S',1,;
      0)
  endfunc

  func oTESTATA_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOELENCO <> 'S')
    endwith
   endif
  endfunc

  func oTESTATA_1_30.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc


  add object ZoomDocu as cp_szoombox with uid="YZZQGCBUFB",left=0, top=65, width=587,height=179,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSVE_KMB",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Ricerca2",;
    nPag=1;
    , HelpContextID = 246535398


  add object oBtn_1_34 as StdButton with uid="URSKZTIAYJ",left=8, top=354, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento";
    , HelpContextID = 262047590;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        gsma_bzm(this.Parent.oContained,.w_SERIALE,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  func oBtn_1_34.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
     endwith
    endif
  endfunc

  add object oBANNOS_1_36 as StdField with uid="XSEKFZEYQW",rtseq=31,rtrep=.f.,;
    cFormVar = "w_BANNOS", cQueryName = "BANNOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Nostra banca da inserire",;
    HelpContextID = 61279466,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=118, Top=327, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACC", oKey_1_1="BANUMCOR", oKey_1_2="this.w_BANNOS"

  func oBANNOS_1_36.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  func oBANNOS_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANNOS_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANNOS_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BANUMCOR',cp_AbsName(this.parent,'oBANNOS_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACC',"Conti banca",'GSTE5MCC.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oBANNOS_1_36.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BANUMCOR=this.parent.oContained.w_BANNOS
     i_obj.ecpSave()
  endproc


  add object oObj_1_37 as cp_runprogram with uid="HZLKJJGOHS",left=-5, top=450, width=220,height=21,;
    caption='GSVE_BMB(D)',;
   bGlobalFont=.t.,;
    prg="GSVE_BMB('D')",;
    cEvent = "ZoomDocu row unchecked",;
    nPag=1;
    , HelpContextID = 61645784


  add object oObj_1_38 as cp_runprogram with uid="YVHIHTKUCV",left=-5, top=474, width=220,height=21,;
    caption='GSVE_BMB(C)',;
   bGlobalFont=.t.,;
    prg="GSVE_BMB('C')",;
    cEvent = "ZoomDocu row checked",;
    nPag=1;
    , HelpContextID = 61646040


  add object oObj_1_39 as cp_runprogram with uid="VFCVCHGISH",left=-5, top=498, width=220,height=21,;
    caption='GSVE_BMB(D)',;
   bGlobalFont=.t.,;
    prg="GSVE_BMB('D')",;
    cEvent = "ZoomScad row unchecked",;
    nPag=1;
    , HelpContextID = 61645784


  add object oObj_1_40 as cp_runprogram with uid="QLUQWRFBSW",left=-5, top=522, width=220,height=21,;
    caption='GSVE_BMB(C)',;
   bGlobalFont=.t.,;
    prg="GSVE_BMB('C')",;
    cEvent = "ZoomScad row checked",;
    nPag=1;
    , HelpContextID = 61646040


  add object oObj_1_41 as cp_runprogram with uid="CTSBIGGYPP",left=-5, top=544, width=220,height=21,;
    caption='GSVE_BMB(M)',;
   bGlobalFont=.t.,;
    prg="GSVE_BMB('M')",;
    cEvent = "ZoomScad rowcheckall,ZoomScad rowcheckfrom,ZoomScad rowcheckto,ZoomScad rowuncheckall,ZoomScad rowinvertselection",;
    nPag=1;
    , HelpContextID = 61643480


  add object oObj_1_42 as cp_runprogram with uid="VZMOJMJDLK",left=-5, top=566, width=220,height=21,;
    caption='GSVE_BMB(M)',;
   bGlobalFont=.t.,;
    prg="GSVE_BMB('M')",;
    cEvent = "ZoomDocu rowcheckall,ZoomDocu rowcheckfrom,ZoomDocu rowcheckto,ZoomDocu rowuncheckall,ZoomDocu rowinvertselection",;
    nPag=1;
    , HelpContextID = 61643480

  add object oStr_1_17 as StdString with uid="MDFRADOWOF",Visible=.t., Left=29, Top=327,;
    Alignment=1, Width=86, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="WZFZIYWSMO",Visible=.t., Left=6, Top=277,;
    Alignment=1, Width=109, Height=15,;
    Caption="Banca di appoggio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="LGOROPBWUR",Visible=.t., Left=19, Top=302,;
    Alignment=1, Width=96, Height=18,;
    Caption="Conto corrente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="WTQLGGWCTW",Visible=.t., Left=20, Top=39,;
    Alignment=1, Width=58, Height=18,;
    Caption="Doc. dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="DTORALBLTB",Visible=.t., Left=182, Top=39,;
    Alignment=1, Width=21, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="XHOHKFOZNY",Visible=.t., Left=6, Top=11,;
    Alignment=1, Width=72, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsve_kmbPag2 as StdContainer
  Width  = 589
  height = 399
  stdWidth  = 589
  stdheight = 399
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oBANCAAP_2_1 as StdField with uid="BPPONRRTGU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_BANCAAP", cQueryName = "BANCAAP",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca di appoggio del cliente/fornitore di selezione",;
    HelpContextID = 110234858,;
   bGlobalFont=.t.,;
    Height=21, Width=131, Left=123, Top=89, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANCAAP"

  func oBANCAAP_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
      if .not. empty(.w_CONTOC)
        bRes2=.link_2_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oBANCAAP_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANCAAP_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oBANCAAP_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"",'',this.parent.oContained
  endproc
  proc oBANCAAP_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANCAAP
     i_obj.ecpSave()
  endproc

  add object oDESAPP_2_4 as StdField with uid="BMHZLTTRGC",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESAPP", cQueryName = "DESAPP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 111392970,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=261, Top=89, InputMask=replicate('X',50)

  add object oCONTOC_2_5 as StdField with uid="VWKVBJPAQY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CONTOC", cQueryName = "CONTOC",;
    bObbl = .f. , nPag = 2, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Conto corrente del cliente/fornitore di selezione",;
    HelpContextID = 60882650,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=123, Top=121, InputMask=replicate('X',25), bHasZoom = .t. , cLinkFile="BAN_CONTI", oKey_1_1="CCCODBAN", oKey_1_2="this.w_BANCAAP", oKey_2_1="CCCODCON", oKey_2_2="this.w_CODCLF", oKey_3_1="CCTIPCON", oKey_3_2="this.w_TIPO", oKey_4_1="CCCONCOR", oKey_4_2="this.w_CONTOC"

  func oCONTOC_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONTOC_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONTOC_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BAN_CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStrODBC(this.Parent.oContained.w_BANCAAP)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStrODBC(this.Parent.oContained.w_CODCLF)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStr(this.Parent.oContained.w_BANCAAP)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStr(this.Parent.oContained.w_CODCLF)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPO)
    endif
    do cp_zoom with 'BAN_CONTI','*','CCCODBAN,CCCODCON,CCTIPCON,CCCONCOR',cp_AbsName(this.parent,'oCONTOC_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco conti correnti",'gste_kmb.BAN_CONTI_VZM',this.parent.oContained
  endproc

  add object oDT_2_6 as StdCheck with uid="RKDMDICWMO",rtseq=20,rtrep=.f.,left=13, top=16, caption="Documenti di trasporto",;
    HelpContextID = 199875018,;
    cFormVar="w_DT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDT_2_6.RadioValue()
    return(iif(this.value =1,'DT',;
    'XX'))
  endfunc
  func oDT_2_6.GetRadio()
    this.Parent.oContained.w_DT = this.RadioValue()
    return .t.
  endfunc

  func oDT_2_6.SetRadio()
    this.Parent.oContained.w_DT=trim(this.Parent.oContained.w_DT)
    this.value = ;
      iif(this.Parent.oContained.w_DT=='DT',1,;
      0)
  endfunc

  add object oNC_2_7 as StdCheck with uid="OKTUDJLIDA",rtseq=21,rtrep=.f.,left=248, top=15, caption="Note di credito",;
    HelpContextID = 199879210,;
    cFormVar="w_NC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oNC_2_7.RadioValue()
    return(iif(this.value =1,'NC',;
    'XX'))
  endfunc
  func oNC_2_7.GetRadio()
    this.Parent.oContained.w_NC = this.RadioValue()
    return .t.
  endfunc

  func oNC_2_7.SetRadio()
    this.Parent.oContained.w_NC=trim(this.Parent.oContained.w_NC)
    this.value = ;
      iif(this.Parent.oContained.w_NC=='NC',1,;
      0)
  endfunc

  add object oDI_2_8 as StdCheck with uid="RXGSTJVOKZ",rtseq=22,rtrep=.f.,left=13, top=35, caption="Documenti interni",;
    HelpContextID = 199877834,;
    cFormVar="w_DI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDI_2_8.RadioValue()
    return(iif(this.value =1,'DI',;
    'XX'))
  endfunc
  func oDI_2_8.GetRadio()
    this.Parent.oContained.w_DI = this.RadioValue()
    return .t.
  endfunc

  func oDI_2_8.SetRadio()
    this.Parent.oContained.w_DI=trim(this.Parent.oContained.w_DI)
    this.value = ;
      iif(this.Parent.oContained.w_DI=='DI',1,;
      0)
  endfunc

  add object oRF_2_9 as StdCheck with uid="CSMVFNVWRN",rtseq=23,rtrep=.f.,left=248, top=35, caption="Corrispettivi",;
    HelpContextID = 199878378,;
    cFormVar="w_RF", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oRF_2_9.RadioValue()
    return(iif(this.value =1,'RF',;
    'XX'))
  endfunc
  func oRF_2_9.GetRadio()
    this.Parent.oContained.w_RF = this.RadioValue()
    return .t.
  endfunc

  func oRF_2_9.SetRadio()
    this.Parent.oContained.w_RF=trim(this.Parent.oContained.w_RF)
    this.value = ;
      iif(this.Parent.oContained.w_RF=='RF',1,;
      0)
  endfunc

  func oRF_2_9.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oOP_2_10 as StdCheck with uid="QHWJWBSBRP",rtseq=24,rtrep=.f.,left=248, top=35, caption="Ordini previsionali",;
    HelpContextID = 199875866,;
    cFormVar="w_OP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oOP_2_10.RadioValue()
    return(iif(this.value =1,'OP',;
    'XX'))
  endfunc
  func oOP_2_10.GetRadio()
    this.Parent.oContained.w_OP = this.RadioValue()
    return .t.
  endfunc

  func oOP_2_10.SetRadio()
    this.Parent.oContained.w_OP=trim(this.Parent.oContained.w_OP)
    this.value = ;
      iif(this.Parent.oContained.w_OP=='OP',1,;
      0)
  endfunc

  func oOP_2_10.mHide()
    with this.Parent.oContained
      return (upper(g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oFA_2_11 as StdCheck with uid="BKSBRGDDEC",rtseq=25,rtrep=.f.,left=13, top=55, caption="Fattura",;
    HelpContextID = 199879850,;
    cFormVar="w_FA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFA_2_11.RadioValue()
    return(iif(this.value =1,'FA',;
    'XX'))
  endfunc
  func oFA_2_11.GetRadio()
    this.Parent.oContained.w_FA = this.RadioValue()
    return .t.
  endfunc

  func oFA_2_11.SetRadio()
    this.Parent.oContained.w_FA=trim(this.Parent.oContained.w_FA)
    this.value = ;
      iif(this.Parent.oContained.w_FA=='FA',1,;
      0)
  endfunc

  add object oOR_2_12 as StdCheck with uid="GXLIRXEDSF",rtseq=26,rtrep=.f.,left=248, top=55, caption="Ordini",;
    HelpContextID = 199875354,;
    cFormVar="w_OR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oOR_2_12.RadioValue()
    return(iif(this.value =1,'OR',;
    'XX'))
  endfunc
  func oOR_2_12.GetRadio()
    this.Parent.oContained.w_OR = this.RadioValue()
    return .t.
  endfunc

  func oOR_2_12.SetRadio()
    this.Parent.oContained.w_OR=trim(this.Parent.oContained.w_OR)
    this.value = ;
      iif(this.Parent.oContained.w_OR=='OR',1,;
      0)
  endfunc

  add object oStr_2_2 as StdString with uid="JJMIJROXXH",Visible=.t., Left=4, Top=89,;
    Alignment=1, Width=115, Height=18,;
    Caption="Banca d'appoggio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="SELTCGDWFM",Visible=.t., Left=23, Top=121,;
    Alignment=1, Width=96, Height=18,;
    Caption="Conto corrente:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kmb','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
