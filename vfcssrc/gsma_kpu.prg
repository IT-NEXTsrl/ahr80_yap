* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kpu                                                        *
*              Pubblicazione prodotti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-11                                                      *
* Last revis.: 2011-08-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kpu",oParentObject))

* --- Class definition
define class tgsma_kpu as StdForm
  Top    = 3
  Left   = 4

  * --- Standard Properties
  Width  = 761
  Height = 491
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-08-30"
  HelpContextID=149584745
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  FAM_ARTI_IDX = 0
  MARCHI_IDX = 0
  cPrg = "gsma_kpu"
  cComment = "Pubblicazione prodotti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPART = space(1)
  o_TIPART = space(1)
  w_GRUMER = space(5)
  w_CODFAM = space(5)
  w_FLINCBAR = space(1)
  w_ARTGPUBL = space(1)
  w_CATOMO = space(5)
  w_CODMAR = space(5)
  w_ARARTWIP = space(1)
  w_DACODICE = space(20)
  w_ACODICE = space(20)
  w_CONSOBSO = space(1)
  w_OPZIONI = space(3)
  w_PUBBLICA = space(1)
  w_PUBBLICA = space(1)
  w_PUBBLICA = space(1)
  w_ADESART = space(40)
  w_DADESART = space(40)
  w_TIP1 = space(2)
  w_TIP2 = space(2)
  w_TIP3 = space(2)
  w_TIP4 = space(2)
  w_TIP5 = space(2)
  w_TIP6 = space(2)
  w_TIP7 = space(2)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_TIPART2 = space(2)
  w_ZOOMPRO = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kpuPag1","gsma_kpu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPART_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMPRO = this.oPgFrm.Pages(1).oPag.ZOOMPRO
    DoDefault()
    proc Destroy()
      this.w_ZOOMPRO = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='GRUMERC'
    this.cWorkTables[3]='CATEGOMO'
    this.cWorkTables[4]='FAM_ARTI'
    this.cWorkTables[5]='MARCHI'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        gsma_bpu(this,"MOD")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPART=space(1)
      .w_GRUMER=space(5)
      .w_CODFAM=space(5)
      .w_FLINCBAR=space(1)
      .w_ARTGPUBL=space(1)
      .w_CATOMO=space(5)
      .w_CODMAR=space(5)
      .w_ARARTWIP=space(1)
      .w_DACODICE=space(20)
      .w_ACODICE=space(20)
      .w_CONSOBSO=space(1)
      .w_OPZIONI=space(3)
      .w_PUBBLICA=space(1)
      .w_PUBBLICA=space(1)
      .w_PUBBLICA=space(1)
      .w_ADESART=space(40)
      .w_DADESART=space(40)
      .w_TIP1=space(2)
      .w_TIP2=space(2)
      .w_TIP3=space(2)
      .w_TIP4=space(2)
      .w_TIP5=space(2)
      .w_TIP6=space(2)
      .w_TIP7=space(2)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPART2=space(2)
        .w_TIPART = 'A'
        .w_GRUMER = space(5)
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_GRUMER))
          .link_1_2('Full')
        endif
        .w_CODFAM = space(5)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODFAM))
          .link_1_3('Full')
        endif
        .w_FLINCBAR = 'N'
        .w_ARTGPUBL = 'T'
        .w_CATOMO = space(5)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CATOMO))
          .link_1_6('Full')
        endif
        .w_CODMAR = space(5)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODMAR))
          .link_1_7('Full')
        endif
        .w_ARARTWIP = IIF(g_CILA<>'S' OR .w_TIPART='S','T','E')
        .w_DACODICE = space(20)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_DACODICE))
          .link_1_9('Full')
        endif
        .w_ACODICE = space(20)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_ACODICE))
          .link_1_10('Full')
        endif
        .w_CONSOBSO = 'N'
      .oPgFrm.Page1.oPag.ZOOMPRO.Calculate()
        .w_OPZIONI = 'SEL'
        .w_PUBBLICA = 'S'
        .w_PUBBLICA = 'S'
        .w_PUBBLICA = 'S'
          .DoRTCalc(16,17,.f.)
        .w_TIP1 = iif(.w_TIPART='S', 'FM',IIF(.w_TIPART='A','PF','AC'))
        .w_TIP2 = iif(.w_TIPART='S', 'FO',IIF(.w_TIPART='A','SE',''))
        .w_TIP3 = iif(.w_TIPART='S', 'DE',IIF(.w_TIPART='A','MP',''))
        .w_TIP4 = iif(.w_TIPART='A', 'MC','')
        .w_TIP5 = iif(.w_TIPART='A', 'MA','')
        .w_TIP6 = iif(.w_TIPART='A', 'IM','')
        .w_TIP7 = iif(.w_TIPART='A', 'PH','')
          .DoRTCalc(25,25,.f.)
        .w_OBTEST = i_datsys
      .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
    endwith
    this.DoRTCalc(27,27,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_TIPART<>.w_TIPART
            .w_GRUMER = space(5)
          .link_1_2('Full')
        endif
        if .o_TIPART<>.w_TIPART
            .w_CODFAM = space(5)
          .link_1_3('Full')
        endif
        .DoRTCalc(4,5,.t.)
        if .o_TIPART<>.w_TIPART
            .w_CATOMO = space(5)
          .link_1_6('Full')
        endif
        if .o_TIPART<>.w_TIPART
            .w_CODMAR = space(5)
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.t.)
        if .o_TIPART<>.w_TIPART
            .w_DACODICE = space(20)
          .link_1_9('Full')
        endif
        if .o_TIPART<>.w_TIPART
            .w_ACODICE = space(20)
          .link_1_10('Full')
        endif
        .oPgFrm.Page1.oPag.ZOOMPRO.Calculate()
        .DoRTCalc(11,17,.t.)
            .w_TIP1 = iif(.w_TIPART='S', 'FM',IIF(.w_TIPART='A','PF','AC'))
            .w_TIP2 = iif(.w_TIPART='S', 'FO',IIF(.w_TIPART='A','SE',''))
            .w_TIP3 = iif(.w_TIPART='S', 'DE',IIF(.w_TIPART='A','MP',''))
            .w_TIP4 = iif(.w_TIPART='A', 'MC','')
            .w_TIP5 = iif(.w_TIPART='A', 'MA','')
            .w_TIP6 = iif(.w_TIPART='A', 'IM','')
            .w_TIP7 = iif(.w_TIPART='A', 'PH','')
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(25,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMPRO.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODFAM_1_3.enabled = this.oPgFrm.Page1.oPag.oCODFAM_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCATOMO_1_6.enabled = this.oPgFrm.Page1.oPag.oCATOMO_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCODMAR_1_7.enabled = this.oPgFrm.Page1.oPag.oCODMAR_1_7.mCond()
    this.oPgFrm.Page1.oPag.oARARTWIP_1_8.enabled = this.oPgFrm.Page1.oPag.oARARTWIP_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oARARTWIP_1_8.visible=!this.oPgFrm.Page1.oPag.oARARTWIP_1_8.mHide()
    this.oPgFrm.Page1.oPag.oPUBBLICA_1_15.visible=!this.oPgFrm.Page1.oPag.oPUBBLICA_1_15.mHide()
    this.oPgFrm.Page1.oPag.oPUBBLICA_1_16.visible=!this.oPgFrm.Page1.oPag.oPUBBLICA_1_16.mHide()
    this.oPgFrm.Page1.oPag.oPUBBLICA_1_17.visible=!this.oPgFrm.Page1.oPag.oPUBBLICA_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMPRO.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GRUMER
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMER))
          select GMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMER_1_2'),i_cWhere,'',"Elenco gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMER)
            select GMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMER = NVL(_Link_.GMCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMER = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_1_3'),i_cWhere,'',"Elenco famiglie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATOMO
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATOMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATOMO)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATOMO))
          select OMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATOMO)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATOMO) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATOMO_1_6'),i_cWhere,'',"Elenco categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATOMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATOMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATOMO)
            select OMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATOMO = NVL(_Link_.OMCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CATOMO = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATOMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAR
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_CODMAR))
          select MACODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oCODMAR_1_7'),i_cWhere,'',"Elenco marche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_CODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_CODMAR)
            select MACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAR = NVL(_Link_.MACODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAR = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODICE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_DACODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_DACODICE))
          select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODICE)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACODICE) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oDACODICE_1_9'),i_cWhere,'',"Elenco prodotti",'GSCP1KPU.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DACODICE)
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODICE = NVL(_Link_.ARCODART,space(20))
      this.w_DADESART = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPART2 = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_DACODICE = space(20)
      endif
      this.w_DADESART = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPART2 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and ((.w_TIPART2 $ 'FM-FO-DE' AND .w_TIPART='S') or (.w_TIPART2 $ 'PF-SE-MP-MC-MA-IM-PM' AND .w_TIPART='A') or (.w_TIPART2='AC' AND .w_TIPART='C')) and (.w_DACODICE<=.w_ACODICE or empty(.w_ACODICE))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prodotto obsoleto, di tipologia differente da quella selezionata o maggiore di quello finale")
        endif
        this.w_DACODICE = space(20)
        this.w_DADESART = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPART2 = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ACODICE
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ACODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ACODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ACODICE))
          select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ACODICE)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ACODICE) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oACODICE_1_10'),i_cWhere,'',"Elenco prodotti",'GSCP1KPU.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ACODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ACODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ACODICE)
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ACODICE = NVL(_Link_.ARCODART,space(20))
      this.w_ADESART = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPART2 = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_ACODICE = space(20)
      endif
      this.w_ADESART = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPART2 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and ((.w_TIPART2 $ 'FM-FO-DE' AND .w_TIPART='S') or (.w_TIPART2 $ 'PF-SE-MP-MC-MA-IM-PM' AND .w_TIPART='A') or (.w_TIPART2='AC' AND .w_TIPART='C')) and (.w_ACODICE>=.w_DACODICE or empty(.w_DACODICE))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Prodotto obsoleto, di tipologia differente da quella selezionata o minore di quello iniziale")
        endif
        this.w_ACODICE = space(20)
        this.w_ADESART = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPART2 = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ACODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPART_1_1.RadioValue()==this.w_TIPART)
      this.oPgFrm.Page1.oPag.oTIPART_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUMER_1_2.value==this.w_GRUMER)
      this.oPgFrm.Page1.oPag.oGRUMER_1_2.value=this.w_GRUMER
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAM_1_3.value==this.w_CODFAM)
      this.oPgFrm.Page1.oPag.oCODFAM_1_3.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oFLINCBAR_1_4.RadioValue()==this.w_FLINCBAR)
      this.oPgFrm.Page1.oPag.oFLINCBAR_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARTGPUBL_1_5.RadioValue()==this.w_ARTGPUBL)
      this.oPgFrm.Page1.oPag.oARTGPUBL_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATOMO_1_6.value==this.w_CATOMO)
      this.oPgFrm.Page1.oPag.oCATOMO_1_6.value=this.w_CATOMO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAR_1_7.value==this.w_CODMAR)
      this.oPgFrm.Page1.oPag.oCODMAR_1_7.value=this.w_CODMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oARARTWIP_1_8.RadioValue()==this.w_ARARTWIP)
      this.oPgFrm.Page1.oPag.oARARTWIP_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODICE_1_9.value==this.w_DACODICE)
      this.oPgFrm.Page1.oPag.oDACODICE_1_9.value=this.w_DACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oACODICE_1_10.value==this.w_ACODICE)
      this.oPgFrm.Page1.oPag.oACODICE_1_10.value=this.w_ACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCONSOBSO_1_12.RadioValue()==this.w_CONSOBSO)
      this.oPgFrm.Page1.oPag.oCONSOBSO_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOPZIONI_1_14.RadioValue()==this.w_OPZIONI)
      this.oPgFrm.Page1.oPag.oOPZIONI_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUBBLICA_1_15.RadioValue()==this.w_PUBBLICA)
      this.oPgFrm.Page1.oPag.oPUBBLICA_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUBBLICA_1_16.RadioValue()==this.w_PUBBLICA)
      this.oPgFrm.Page1.oPag.oPUBBLICA_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUBBLICA_1_17.RadioValue()==this.w_PUBBLICA)
      this.oPgFrm.Page1.oPag.oPUBBLICA_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oADESART_1_18.value==this.w_ADESART)
      this.oPgFrm.Page1.oPag.oADESART_1_18.value=this.w_ADESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDADESART_1_19.value==this.w_DADESART)
      this.oPgFrm.Page1.oPag.oDADESART_1_19.value=this.w_DADESART
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and ((.w_TIPART2 $ 'FM-FO-DE' AND .w_TIPART='S') or (.w_TIPART2 $ 'PF-SE-MP-MC-MA-IM-PM' AND .w_TIPART='A') or (.w_TIPART2='AC' AND .w_TIPART='C')) and (.w_DACODICE<=.w_ACODICE or empty(.w_ACODICE)))  and not(empty(.w_DACODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACODICE_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Prodotto obsoleto, di tipologia differente da quella selezionata o maggiore di quello finale")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and ((.w_TIPART2 $ 'FM-FO-DE' AND .w_TIPART='S') or (.w_TIPART2 $ 'PF-SE-MP-MC-MA-IM-PM' AND .w_TIPART='A') or (.w_TIPART2='AC' AND .w_TIPART='C')) and (.w_ACODICE>=.w_DACODICE or empty(.w_DACODICE)))  and not(empty(.w_ACODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oACODICE_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Prodotto obsoleto, di tipologia differente da quella selezionata o minore di quello iniziale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPART = this.w_TIPART
    return

enddefine

* --- Define pages as container
define class tgsma_kpuPag1 as StdContainer
  Width  = 757
  height = 491
  stdWidth  = 757
  stdheight = 491
  resizeXpos=571
  resizeYpos=295
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPART_1_1 as StdCombo with uid="NVFMIGUCDO",rtseq=1,rtrep=.f.,left=88,top=10,width=97,height=21;
    , ToolTipText = "Tipologia prodotto";
    , HelpContextID = 8114998;
    , cFormVar="w_TIPART",RowSource=""+"Articolo,"+"Servizio,"+"Kit", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPART_1_1.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oTIPART_1_1.GetRadio()
    this.Parent.oContained.w_TIPART = this.RadioValue()
    return .t.
  endfunc

  func oTIPART_1_1.SetRadio()
    this.Parent.oContained.w_TIPART=trim(this.Parent.oContained.w_TIPART)
    this.value = ;
      iif(this.Parent.oContained.w_TIPART=='A',1,;
      iif(this.Parent.oContained.w_TIPART=='S',2,;
      iif(this.Parent.oContained.w_TIPART=='C',3,;
      0)))
  endfunc

  add object oGRUMER_1_2 as StdField with uid="CUKTTLPBYB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GRUMER", cQueryName = "GRUMER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico",;
    HelpContextID = 230173542,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=303, Top=5, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMER"

  func oGRUMER_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMER_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMER_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMER_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco gruppi merceologici",'',this.parent.oContained
  endproc

  add object oCODFAM_1_3 as StdField with uid="FLUTMTEINO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Famiglia",;
    HelpContextID = 141563942,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=447, Top=5, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPART $ 'AC')
    endwith
   endif
  endfunc

  func oCODFAM_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco famiglie",'',this.parent.oContained
  endproc

  add object oFLINCBAR_1_4 as StdCheck with uid="RZWADHHKRF",rtseq=4,rtrep=.f.,left=543, top=5, caption="Includi articoli barcode",;
    ToolTipText = "Se attivo mostra anche gli articoli identificati come articolo barcode",;
    HelpContextID = 228091304,;
    cFormVar="w_FLINCBAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLINCBAR_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLINCBAR_1_4.GetRadio()
    this.Parent.oContained.w_FLINCBAR = this.RadioValue()
    return .t.
  endfunc

  func oFLINCBAR_1_4.SetRadio()
    this.Parent.oContained.w_FLINCBAR=trim(this.Parent.oContained.w_FLINCBAR)
    this.value = ;
      iif(this.Parent.oContained.w_FLINCBAR=='S',1,;
      0)
  endfunc


  add object oARTGPUBL_1_5 as StdCombo with uid="MMQGNNFKPH",rtseq=5,rtrep=.f.,left=88,top=40,width=136,height=21;
    , ToolTipText = "Articoli da visualizzare (solo quelli gi� pubblicati, solo quelli ancora da pubblicare oppure tutti)";
    , HelpContextID = 23206738;
    , cFormVar="w_ARTGPUBL",RowSource=""+"Solo pubblicati,"+"Solo non pubblicati,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARTGPUBL_1_5.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oARTGPUBL_1_5.GetRadio()
    this.Parent.oContained.w_ARTGPUBL = this.RadioValue()
    return .t.
  endfunc

  func oARTGPUBL_1_5.SetRadio()
    this.Parent.oContained.w_ARTGPUBL=trim(this.Parent.oContained.w_ARTGPUBL)
    this.value = ;
      iif(this.Parent.oContained.w_ARTGPUBL=='P',1,;
      iif(this.Parent.oContained.w_ARTGPUBL=='N',2,;
      iif(this.Parent.oContained.w_ARTGPUBL=='T',3,;
      0)))
  endfunc

  add object oCATOMO_1_6 as StdField with uid="KOYDETHRRG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CATOMO", cQueryName = "CATOMO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categ. omogenea",;
    HelpContextID = 188353062,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=303, Top=36, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATOMO"

  func oCATOMO_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPART $ 'AC')
    endwith
   endif
  endfunc

  func oCATOMO_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATOMO_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATOMO_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATOMO_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco categorie omogenee",'',this.parent.oContained
  endproc

  add object oCODMAR_1_7 as StdField with uid="RRWTOYMRLD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODMAR", cQueryName = "CODMAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Marca",;
    HelpContextID = 225908774,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=447, Top=36, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", oKey_1_1="MACODICE", oKey_1_2="this.w_CODMAR"

  func oCODMAR_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPART $ 'AC')
    endwith
   endif
  endfunc

  func oCODMAR_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAR_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAR_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oCODMAR_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco marche",'',this.parent.oContained
  endproc


  add object oARARTWIP_1_8 as StdCombo with uid="GPWWUWEHDN",rtseq=8,rtrep=.f.,left=609,top=35,width=123,height=21;
    , ToolTipText = "Articoli da visualizzare (solo gli articoli WIP, escludi codici WIP oppure visualizza tutti gli articoli)";
    , HelpContextID = 206836906;
    , cFormVar="w_ARARTWIP",RowSource=""+"Escludi codici WIP,"+"Solo codici WIP,"+"Tutti gli articoli", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oARARTWIP_1_8.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oARARTWIP_1_8.GetRadio()
    this.Parent.oContained.w_ARARTWIP = this.RadioValue()
    return .t.
  endfunc

  func oARARTWIP_1_8.SetRadio()
    this.Parent.oContained.w_ARARTWIP=trim(this.Parent.oContained.w_ARARTWIP)
    this.value = ;
      iif(this.Parent.oContained.w_ARARTWIP=='E',1,;
      iif(this.Parent.oContained.w_ARARTWIP=='S',2,;
      iif(this.Parent.oContained.w_ARARTWIP=='T',3,;
      0)))
  endfunc

  func oARARTWIP_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPART='A')
    endwith
   endif
  endfunc

  func oARARTWIP_1_8.mHide()
    with this.Parent.oContained
      return (g_CILA<>'S')
    endwith
  endfunc

  add object oDACODICE_1_9 as StdField with uid="GGVKFIDLJF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DACODICE", cQueryName = "DACODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Prodotto obsoleto, di tipologia differente da quella selezionata o maggiore di quello finale",;
    ToolTipText = "Selezionare un codice per inizio intervallo",;
    HelpContextID = 78183035,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=88, Top=68, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_DACODICE"

  func oDACODICE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODICE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODICE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oDACODICE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco prodotti",'GSCP1KPU.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oACODICE_1_10 as StdField with uid="LUMXSMKZYT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ACODICE", cQueryName = "ACODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Prodotto obsoleto, di tipologia differente da quella selezionata o minore di quello iniziale",;
    ToolTipText = "Selezionare un codice per fine intervallo",;
    HelpContextID = 250526726,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=88, Top=92, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_ACODICE"

  func oACODICE_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oACODICE_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oACODICE_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oACODICE_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco prodotti",'GSCP1KPU.ART_ICOL_VZM',this.parent.oContained
  endproc


  add object oBtn_1_11 as StdButton with uid="YQWTLIBGOF",left=703, top=70, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'interrogazione";
    , HelpContextID = 1966314;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      this.parent.oContained.NotifyEvent("Interroga")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCONSOBSO_1_12 as StdCheck with uid="NCOBNSURLR",rtseq=11,rtrep=.f.,left=555, top=67, caption="Includi obsoleti",;
    ToolTipText = "Se attivo vengono elencati anche gli articoli obsoleti",;
    HelpContextID = 241023093,;
    cFormVar="w_CONSOBSO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONSOBSO_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCONSOBSO_1_12.GetRadio()
    this.Parent.oContained.w_CONSOBSO = this.RadioValue()
    return .t.
  endfunc

  func oCONSOBSO_1_12.SetRadio()
    this.Parent.oContained.w_CONSOBSO=trim(this.Parent.oContained.w_CONSOBSO)
    this.value = ;
      iif(this.Parent.oContained.w_CONSOBSO=='S',1,;
      0)
  endfunc


  add object ZOOMPRO as cp_szoombox with uid="SOCIPGHJMX",left=3, top=121, width=751,height=313,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="gsma_kpu",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="ART_ICOL",;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 28412902

  add object oOPZIONI_1_14 as StdRadio with uid="BZYHCYCHXU",rtseq=12,rtrep=.f.,left=13, top=453, width=129,height=32;
    , ToolTipText = "Flag per selezionare o deselezionare tutto";
    , cFormVar="w_OPZIONI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oOPZIONI_1_14.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 95127066
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 95127066
      this.Buttons(2).Top=15
      this.SetAll("Width",127)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Flag per selezionare o deselezionare tutto")
      StdRadio::init()
    endproc

  func oOPZIONI_1_14.RadioValue()
    return(iif(this.value =1,'SEL',;
    iif(this.value =2,'DES',;
    space(3))))
  endfunc
  func oOPZIONI_1_14.GetRadio()
    this.Parent.oContained.w_OPZIONI = this.RadioValue()
    return .t.
  endfunc

  func oOPZIONI_1_14.SetRadio()
    this.Parent.oContained.w_OPZIONI=trim(this.Parent.oContained.w_OPZIONI)
    this.value = ;
      iif(this.Parent.oContained.w_OPZIONI=='SEL',1,;
      iif(this.Parent.oContained.w_OPZIONI=='DES',2,;
      0))
  endfunc


  add object oPUBBLICA_1_15 as StdCombo with uid="TRDWDIKJHD",rtseq=13,rtrep=.f.,left=301,top=459,width=174,height=22;
    , ToolTipText = "Pubblica, trasferisci o non pubblicare i prodotti selezionati";
    , HelpContextID = 85720887;
    , cFormVar="w_PUBBLICA",RowSource=""+"Pubblica su web,"+"Trasferisci su web,"+"Non pubblicare su web", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUBBLICA_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'T',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oPUBBLICA_1_15.GetRadio()
    this.Parent.oContained.w_PUBBLICA = this.RadioValue()
    return .t.
  endfunc

  func oPUBBLICA_1_15.SetRadio()
    this.Parent.oContained.w_PUBBLICA=trim(this.Parent.oContained.w_PUBBLICA)
    this.value = ;
      iif(this.Parent.oContained.w_PUBBLICA=='S',1,;
      iif(this.Parent.oContained.w_PUBBLICA=='T',2,;
      iif(this.Parent.oContained.w_PUBBLICA=='N',3,;
      0)))
  endfunc

  func oPUBBLICA_1_15.mHide()
    with this.Parent.oContained
      return (g_CPIN='S')
    endwith
  endfunc


  add object oPUBBLICA_1_16 as StdCombo with uid="IHWVKYWUDX",rtseq=14,rtrep=.f.,left=301,top=459,width=174,height=22;
    , ToolTipText = "Pubblica, trasferisci o non pubblicare i prodotti selezionati";
    , HelpContextID = 85720887;
    , cFormVar="w_PUBBLICA",RowSource=""+"Pubblica su web,"+"Trasferisci su web", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUBBLICA_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oPUBBLICA_1_16.GetRadio()
    this.Parent.oContained.w_PUBBLICA = this.RadioValue()
    return .t.
  endfunc

  func oPUBBLICA_1_16.SetRadio()
    this.Parent.oContained.w_PUBBLICA=trim(this.Parent.oContained.w_PUBBLICA)
    this.value = ;
      iif(this.Parent.oContained.w_PUBBLICA=='S',1,;
      iif(this.Parent.oContained.w_PUBBLICA=='T',2,;
      0))
  endfunc

  func oPUBBLICA_1_16.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S' OR .w_ARTGPUBL = 'N')
    endwith
  endfunc


  add object oPUBBLICA_1_17 as StdCombo with uid="PADKLVZHBA",rtseq=15,rtrep=.f.,left=301,top=459,width=174,height=22;
    , ToolTipText = "Pubblica, trasferisci o non pubblicare i prodotti selezionati";
    , HelpContextID = 85720887;
    , cFormVar="w_PUBBLICA",RowSource=""+"Pubblica su web,"+"Non pubblicare su web,"+"Trasferisci su web", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPUBBLICA_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oPUBBLICA_1_17.GetRadio()
    this.Parent.oContained.w_PUBBLICA = this.RadioValue()
    return .t.
  endfunc

  func oPUBBLICA_1_17.SetRadio()
    this.Parent.oContained.w_PUBBLICA=trim(this.Parent.oContained.w_PUBBLICA)
    this.value = ;
      iif(this.Parent.oContained.w_PUBBLICA=='S',1,;
      iif(this.Parent.oContained.w_PUBBLICA=='N',2,;
      iif(this.Parent.oContained.w_PUBBLICA=='T',3,;
      0)))
  endfunc

  func oPUBBLICA_1_17.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S' OR .w_ARTGPUBL $ 'P-T')
    endwith
  endfunc

  add object oADESART_1_18 as StdField with uid="KCHIAKZEPK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ADESART", cQueryName = "ADESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione prodotto",;
    HelpContextID = 226303238,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=258, Top=92, InputMask=replicate('X',40)

  add object oDADESART_1_19 as StdField with uid="VPBUBAUSJX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DADESART", cQueryName = "DADESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione prodotto",;
    HelpContextID = 227478154,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=258, Top=68, InputMask=replicate('X',40)


  add object oObj_1_36 as cp_runprogram with uid="AWLGTIXIOW",left=8, top=506, width=247,height=19,;
    caption='GSMA_BPU(OPT)',;
   bGlobalFont=.t.,;
    prg="gsma_bpu('OPT')",;
    cEvent = "w_OPZIONI Changed",;
    nPag=1;
    , HelpContextID = 37027387


  add object oBtn_1_38 as StdButton with uid="OKBBFJJKZS",left=637, top=438, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire l'elaborazione";
    , HelpContextID = 149555994;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      with this.Parent.oContained
        gsma_bpu(this.Parent.oContained,"MOD")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_39 as StdButton with uid="TSEJOICTJS",left=691, top=438, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142267322;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_27 as StdString with uid="BVAVXFHPXE",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=83, Height=18,;
    Caption="Prodotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="PIIGYOTNQY",Visible=.t., Left=9, Top=72,;
    Alignment=1, Width=77, Height=18,;
    Caption="Dal prodotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="PGRNPWRIEG",Visible=.t., Left=9, Top=96,;
    Alignment=1, Width=77, Height=18,;
    Caption="Al prodotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="DTWVBZXLUB",Visible=.t., Left=395, Top=40,;
    Alignment=1, Width=46, Height=18,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="WCUWPBIPMR",Visible=.t., Left=390, Top=9,;
    Alignment=1, Width=51, Height=18,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="OGESOYCZYL",Visible=.t., Left=228, Top=40,;
    Alignment=1, Width=72, Height=18,;
    Caption="Cat. omog.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="CRZJBGDMHT",Visible=.t., Left=205, Top=9,;
    Alignment=1, Width=95, Height=18,;
    Caption="Gru. Merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="UJSDYRRSFK",Visible=.t., Left=165, Top=463,;
    Alignment=1, Width=131, Height=18,;
    Caption="Prodotto selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="WHLLZDGAZA",Visible=.t., Left=3, Top=40,;
    Alignment=1, Width=83, Height=18,;
    Caption="Stato attuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="CBHRNSQSNX",Visible=.t., Left=540, Top=39,;
    Alignment=1, Width=65, Height=17,;
    Caption="WIP fase:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (g_CILA<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kpu','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
