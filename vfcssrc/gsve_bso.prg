* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bso                                                        *
*              Stampa sovraccolli                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_52]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-12                                                      *
* Last revis.: 2002-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bso",oParentObject)
return(i_retval)

define class tgsve_bso as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_QTACOL = 0
  w_CODAZI = space(5)
  w_INSERT = 0
  w_END = 0
  w_NUMCOL = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  OUTPUTMP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Sovraccolli (GSVE_SSC)
    * --- Creo la tabella temporanea per output utente
    * --- Create temporary table OUTPUTMP
    i_nIdx=cp_AddTableDef('OUTPUTMP') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsve2ssc',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.OUTPUTMP_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from OUTPUTMP
    i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2],.t.,this.OUTPUTMP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select *  from "+i_cTable+" OUTPUTMP ";
           ,"_Curs_OUTPUTMP")
    else
      select * from (i_cTable);
        into cursor _Curs_OUTPUTMP
    endif
    if used('_Curs_OUTPUTMP')
      select _Curs_OUTPUTMP
      locate for 1=1
      do while not(eof())
      this.w_SERIAL = _Curs_OUTPUTMP.MVSERIAL
      this.w_QTACOL = NVL(_Curs_OUTPUTMP.MVQTACOL,0)
      this.w_CODAZI = _Curs_OUTPUTMP.CODAZI
      this.w_END = (this.w_QTACOL * this.oParentObject.w_NUMCOP)
      this.w_INSERT = 2
      * --- Duplico i seriali in base al numero di copie
      do while this.w_END >= this.w_INSERT
        this.w_NUMCOL = IIF(MOD(this.w_INSERT,this.w_QTACOL)=0,this.w_QTACOL,MOD(this.w_INSERT,this.w_QTACOL))
        * --- Insert into OUTPUTMP
        i_nConn=i_TableProp[this.OUTPUTMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OUTPUTMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OUTPUTMP_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MVSERIAL"+",MVQTACOL"+",NUMCOL"+",CODAZI"+",CONTA"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'OUTPUTMP','MVSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_QTACOL),'OUTPUTMP','MVQTACOL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMCOL),'OUTPUTMP','NUMCOL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'OUTPUTMP','CODAZI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_INSERT),'OUTPUTMP','CONTA');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_SERIAL,'MVQTACOL',this.w_QTACOL,'NUMCOL',this.w_NUMCOL,'CODAZI',this.w_CODAZI,'CONTA',this.w_INSERT)
          insert into (i_cTable) (MVSERIAL,MVQTACOL,NUMCOL,CODAZI,CONTA &i_ccchkf. );
             values (;
               this.w_SERIAL;
               ,this.w_QTACOL;
               ,this.w_NUMCOL;
               ,this.w_CODAZI;
               ,this.w_INSERT;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_INSERT = this.w_INSERT + 1
      enddo
        select _Curs_OUTPUTMP
        continue
      enddo
      use
    endif
    * --- Lancio il report
    VX_EXEC(""+ALLTRIM(this.oParentObject.w_OQRY)+", "+ALLTRIM(this.oParentObject.w_OREP)+"",THIS)
    * --- Rimuovo il cursore e la tabella tmp
    * --- Drop temporary table OUTPUTMP
    i_nIdx=cp_GetTableDefIdx('OUTPUTMP')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('OUTPUTMP')
    endif
    if USED("__TMP__")
       
 Select __tmp__ 
 Use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='*OUTPUTMP'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_OUTPUTMP')
      use in _Curs_OUTPUTMP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
