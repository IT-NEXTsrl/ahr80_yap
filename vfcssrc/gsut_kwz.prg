* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kwz                                                        *
*              Lista campi                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-03-18                                                      *
* Last revis.: 2012-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kwz",oParentObject))

* --- Class definition
define class tgsut_kwz as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 536
  Height = 484
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-07-04"
  HelpContextID=236371095
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kwz"
  cComment = "Lista campi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CURCAMPI = space(10)
  w_FIELD = space(240)
  w_FIELDUSED = space(240)
  w_DESCFIELDUSED = space(240)
  w_DESCFIELD = space(240)
  w_FIELDTYPE = space(1)
  w_FIELDUSEDTYPE = space(1)
  w_CURPARAM = space(10)
  w_FLDLIST = .NULL.
  w_FLDLISTUSED = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kwzPag1","gsut_kwz",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_FLDLIST = this.oPgFrm.Pages(1).oPag.FLDLIST
    this.w_FLDLISTUSED = this.oPgFrm.Pages(1).oPag.FLDLISTUSED
    DoDefault()
    proc Destroy()
      this.w_FLDLIST = .NULL.
      this.w_FLDLISTUSED = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CURCAMPI=space(10)
      .w_FIELD=space(240)
      .w_FIELDUSED=space(240)
      .w_DESCFIELDUSED=space(240)
      .w_DESCFIELD=space(240)
      .w_FIELDTYPE=space(1)
      .w_FIELDUSEDTYPE=space(1)
      .w_CURPARAM=space(10)
        .w_CURCAMPI = this.oParentObject.w_CURCAMPI
      .oPgFrm.Page1.oPag.FLDLIST.Calculate()
      .oPgFrm.Page1.oPag.FLDLISTUSED.Calculate()
        .w_FIELD = NVL(.w_FLDLIST.GETVAR('FIELDNAME'),' ')
        .w_FIELDUSED = NVL(.w_FLDLISTUSED.GETVAR('FIELDNAME'),' ')
        .w_DESCFIELDUSED = NVL(.w_FLDLISTUSED.GETVAR('FIELDDESC'),' ')
        .w_DESCFIELD = NVL(.w_FLDLIST.GETVAR('FIELDDESC'),' ')
        .w_FIELDTYPE = NVL(.w_FLDLIST.GETVAR('FIELDTYPE'),' ')
        .w_FIELDUSEDTYPE = NVL(.w_FLDLISTUSED.GETVAR('FIELDTYPE'),' ')
        .w_CURPARAM = this.oParentObject.w_CURPARAM
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CURCAMPI = this.oParentObject.w_CURCAMPI
        .oPgFrm.Page1.oPag.FLDLIST.Calculate()
        .oPgFrm.Page1.oPag.FLDLISTUSED.Calculate()
            .w_FIELD = NVL(.w_FLDLIST.GETVAR('FIELDNAME'),' ')
            .w_FIELDUSED = NVL(.w_FLDLISTUSED.GETVAR('FIELDNAME'),' ')
            .w_DESCFIELDUSED = NVL(.w_FLDLISTUSED.GETVAR('FIELDDESC'),' ')
            .w_DESCFIELD = NVL(.w_FLDLIST.GETVAR('FIELDDESC'),' ')
            .w_FIELDTYPE = NVL(.w_FLDLIST.GETVAR('FIELDTYPE'),' ')
            .w_FIELDUSEDTYPE = NVL(.w_FLDLISTUSED.GETVAR('FIELDTYPE'),' ')
            .w_CURPARAM = this.oParentObject.w_CURPARAM
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.FLDLIST.Calculate()
        .oPgFrm.Page1.oPag.FLDLISTUSED.Calculate()
    endwith
  return

  proc Calculate_SDFHFBUDMJ()
    with this
          * --- Chiusura maschera alla selezione di un record nuovo
          CHIUSURA(this;
              ,'NOTUSED';
             )
    endwith
  endproc
  proc Calculate_WMJLLGRGDI()
    with this
          * --- Aggiorno lo zoom con lista campi  corrente
          GSUT_BRW(this;
              ,'FIELDZOOM';
             )
    endwith
  endproc
  proc Calculate_THBZVTPXOT()
    with this
          * --- Chiusura maschera alla selezione di un record usato
          CHIUSURA(this;
              ,'USED';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.FLDLIST.Event(cEvent)
      .oPgFrm.Page1.oPag.FLDLISTUSED.Event(cEvent)
        if lower(cEvent)==lower("w_FLDLIST selected")
          .Calculate_SDFHFBUDMJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_WMJLLGRGDI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_FLDLISTUSED selected")
          .Calculate_THBZVTPXOT()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kwzPag1 as StdContainer
  Width  = 532
  height = 484
  stdWidth  = 532
  stdheight = 484
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object FLDLIST as cp_zoombox with uid="MRWPZALZWA",left=5, top=24, width=523,height=212,;
    caption='Object',;
   bGlobalFont=.t.,;
    bReadOnly=.t.,bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnLoad=.t.,cTable="WIZARD",cMenuFile="",cZoomFile="REPORT",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 122502170


  add object FLDLISTUSED as cp_zoombox with uid="BGDGHFFTTA",left=5, top=266, width=523,height=212,;
    caption='Object',;
   bGlobalFont=.t.,;
    bReadOnly=.t.,bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",bRetriveAllRows=.f.,bQueryOnLoad=.t.,cTable="WIZARD2",cMenuFile="",cZoomFile="REPORT",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 122502170

  add object oStr_1_12 as StdString with uid="ODHMUNIPOL",Visible=.t., Left=5, Top=247,;
    Alignment=0, Width=132, Height=18,;
    Caption="Lista campi gi� utilizzati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="UPNFIFGQCU",Visible=.t., Left=5, Top=4,;
    Alignment=0, Width=132, Height=18,;
    Caption="Lista campi cursore:"  ;
  , bGlobalFont=.t.

  add object oBox_1_7 as StdBox with uid="EVWWAVARVS",left=2, top=245, width=527,height=237

  add object oBox_1_14 as StdBox with uid="PRMRMBDDJZ",left=2, top=3, width=527,height=244
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kwz','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kwz
PROC CHIUSURA()
  PARAMETERS oPAR, oTIPO  
   IF oTIPO == 'NOTUSED'
      oPAR.oParentObject.w_WRCURFLD = oPAR.w_FIELD
      oPAR.oParentObject.w_WRFLDESC = oPAR.w_DESCFIELD
      if lower(oPAR.oParentObject.class) == 'tcgsut_mmd'
         oPAR.oParentObject.w_FIELDTYPE = oPAR.w_FIELDTYPE
      endif
   ELSE
      oPAR.oParentObject.w_WRCURFLD = oPAR.w_FIELDUSED
      oPAR.oParentObject.w_WRFLDESC = 'Rip. '+  ALLTRIM(oPAR.w_DESCFIELDUSED)
      if lower(oPAR.oParentObject.class) == 'tcgsut_mmd'
         oPAR.oParentObject.w_FIELDTYPE = oPAR.w_FIELDUSEDTYPE
      endif
   ENDIF
   oPAR.oParentObject.NotifyEvent('AGGLISTA')
   oPAR.ECPSAVE()     
ENDPROC  


* --- Fine Area Manuale
