* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bfa                                                        *
*              Calcoli fattura                                                 *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_494]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-10                                                      *
* Last revis.: 2014-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bfa",oParentObject,m.pParam)
return(i_retval)

define class tgsar_bfa as StdBatch
  * --- Local variables
  pParam = space(1)
  w_MYSELF = .NULL.
  w_contrate = 0
  w_ISALT = .f.
  w_FLSCOM = space(1)
  w_FLFOSC = space(1)
  w_SOLORATE = .f.
  w_ACQAUT = space(1)
  w_CAUCON = space(5)
  w_TIPDOC = space(2)
  w_RET = .NULL.
  pDOCINFO = .NULL.
  w_TIPRIG = space(1)
  w_ROWORD = 0
  w_CODART = space(20)
  w_TCODIVA = space(3)
  w_TQTAMOV = 0
  w_TQTAUM1 = 0
  w_TPERIVA = 0
  w_TVALMAG = 0
  w_PERIND = 0
  w_RESARRO = 0
  w_OLDMVIMPFIN = 0
  w_RICRATE = .f.
  w_OK = .f.
  w_APPO = 0
  w_PARAM = space(1)
  w_TIPREG = space(1)
  w_MESS = space(10)
  w_ARRSUP = 0
  w_FLESEN = .f.
  w_MVAIMPS = 0
  w_NVAIMPN = 0
  w_MVAIMPN = 0
  w_NVAIMPS = 0
  w_MVACIVA = space(5)
  w_NVACIVA = space(5)
  w_MVAFLOM = space(1)
  w_NVAFLOM = space(1)
  w_OK1 = .f.
  w_EXITERR = .f.
  w_OLDDOC = 0
  w_NEWDOC = 0
  w_PADRE = .NULL.
  w_GSVE_MRT = .NULL.
  w_LOOP = 0
  w_TmpC = space(3)
  w_NDIFF = 0
  w_RECNUM = 0
  w_TINDIVA = 0
  w_TVALMAG = 0
  w_TIMPCOM = 0
  w_TCODCOM = space(15)
  w_DECCOM = 0
  w_LINDIVE = 0
  w_COCODVAL = space(3)
  w_TFLEVAS = space(1)
  w_LOOP_AI = 0
  * --- WorkFile variables
  CAN_TIER_idx=0
  CAU_CONT_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  PAG_AMEN_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Documenti ; Calcoli Finali (da GSVE_MDV, GSOR_MDV, GSAC_MDV, GSVE_BFD, GSIM_BRD, GSCO_BGD, GSAR_BEA, GSEP_BGO, GSAC_BGO)
    * --- Documenti ; Calcoli Finali (da GSVE_MDV, GSOR_MDV, GSAC_MDV, GSVE_BFD, GSIM_BRD, GSCO_BGD, GSAR_BEA, GSEP_BGO, GSAC_BGO)
    *     da GSVE_MDV, GSOR_MDV, GSAC_MDV: A =Lanciato da Evento CalcoliFinali (A.M. CheckForm) ; B =Lanciato da Bottone Calcoli
    *     da GSVE_BFD, GSIM_BRD, GSCO_BGD, GSDS_BEA, GSEP_BCO, GSAC_BGO, GSPS_BGD, GSOF_BGD: D =Lanciato dalla Generazione Fatture Differite/Import Documenti
    *     da generazione GPOS = DP
    *     
    *     AI[] - Vettore Castelletto IVA : 
    *     1^Indice Num.Aliquota
    *     2^Indice : 
    *     1 =Codice IVA
    *     2 =Spese Incasso
    *     3 =Spese Imballo
    *     4 =Spese Trasporto
    *     5 =Imponibile
    *     6 =% IVA
    *     7 =Importo IVA
    *     8 =Imponibile + IVA da Scorporare (Per Documenti con Scorporo a Fine Fattura)
    *     9 =Flag Bolli Esenti
    *     10 =Flag Omaggi
    *     11 =Arrotondamenti
    *     12 = Reverse Charge
    *     13=Imponibile Righe Spese accessorie (da aggiungere al termine delle varie ripartizioni..)
    *           In pratica quanto letto sulle righe del documento
    *     14=Somma MVVALMAG dettaglio (check eventuale resto per errore calcolo numerico se scorporo)
    * --- RA[] - Vettore Rate Scadenze : 1^Indice Num.Rata
    *     
    *     2^Indice : 
    *     1 =Data Rata
    *     2 =Netto
    *     3 =IVA
    *     4 =Spese
    *     5 =Tipo Pagamento
    *     6 =Descrizione Rata
    this.w_PARAM = Left(this.pParam,1)
    this.w_PADRE = this.oParentObject
    this.w_ISALT = isalt()
    if this.w_PARAM<>"D"
      this.w_GSVE_MRT = this.w_PADRE.GSVE_MRT
    endif
    * --- Calcola Sconti su Omaggi
    if Type("This.oparentobject.w_MVFLSCOM")="C" 
      * --- Se dichiarato nel padre prendo il valore (nei documenti � editabile nella maschera di testata)
      *     Dal POS nel Batch GSPS_BGD viene dichiarata sempre a 'N'. Unico caso di generazione
      this.w_FLSCOM = this.oParentObject.w_MVFLSCOM
    else
      * --- Altrimenti prendo quello definito nei dati azienda
      this.w_FLSCOM = IIF(g_FLSCOM="S" AND iif(Type("This.oparentobject.w_MVDATDOC")<>"U",this.oParentObject.w_MVDATDOC,i_DATSYS)<g_DTSCOM AND !empty(nvl(g_DTSCOM,cp_CharToDate("  /  /    ")))," ",g_FLSCOM)
    endif
    * --- Sconto forzato, non � detto che tutte le gestioni che utilizzino il GSAR_BFA
    *     abbiamo dichiarato la var. w_MVFLFOSC,  se non � dichiarata la considero 
    *     vuota..
    this.w_FLFOSC = IIF ( VarType( this.oParentObject.w_MVFLFOSC )="C" , this.oParentObject.w_MVFLFOSC , " " )
    * --- Eseguo solo il calcolo delle rate tenendo fermo il castelletto IVA precedentemente calcolato
    this.w_SOLORATE = Type("This.oparentobject.w_SOLORATE")="C" and This.oparentobject.w_SOLORATE="S"
    * --- Il tipo documento lo rileggo sempre per gestire correttamente il caso anche
    *     per i batch di generazione massiva
    if vartype(this.w_PADRE.w_MVCAUCON)="C" And Not Empty( this.w_PADRE.w_MVCAUCON )
      this.w_CAUCON = this.w_PADRE.w_MVCAUCON
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCTIPDOC"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_CAUCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCTIPDOC;
          from (i_cTable) where;
              CCCODICE = this.w_CAUCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_ACQAUT = IIF(this.w_TIPDOC $ "AU-NU","S","N")
    else
      this.w_ACQAUT = "N"
      this.w_TIPDOC = "NO"
    endif
    if this.w_PARAM<>"D" Or (this.w_PADRE.Class="Tgsps_bgd" and this.w_PADRE.w_TIPREG="E")
      * --- Usato per test Documenti Corrispettivi in Ventilazione
      * --- Se w_TIPREG='E' , i Calcoli sul Documento avverranno sempre al Lordo di IVA (IVA non gestita)
      this.w_TIPREG = this.w_PADRE.w_TIPREG
    else
      this.w_TIPREG = " "
    endif
    do case
      case this.w_PARAM="A"
        * --- Lanciato da Evento 'CalcoliFinali' in A.M. CheckForm (i check del Form sono gia' stati fatti)
        * --- considero sempre Errore: a fondo batch w_RESCHK viene rimesso a 0.
        *     In caso di Errore ci sono le Stop che bloccano l'elaborazione, quindi w-RESCHK rimane -1
        if this.oParentObject.w_MVFLSCAF<>"S"
          * --- Se non scadenze confermate ricalcola sempre le scadenze
          this.w_GSVE_MRT.w_AGGIORNO = .T.
        endif
      case this.w_PARAM="B" 
        * --- Premuto Bottone Calcoli o Cambiato Pagamento/Data Doc. sul Documento
        if this.oParentObject.w_MVFLSCAF="S"
          * --- Se Scadenze Confermate non esegue alcun Ricalcolo
          Ah_ErrorMsg("Scadenza confermata, ricalcolo non consentito!",48,"")
          i_retcode = 'stop'
          return
        else
          this.w_GSVE_MRT.w_AGGIORNO = .T.
          if this.pParam="BP"
            * --- Cambiato Pagamento/Data Documento; Notifica solo che deve riaggiornare le rate Scadenze
            if this.oParentObject.w_MVCLADOC="FA" and this.oParentObject.w_MVFLVEAC="V"
              if EVL(NVL(this.oParentObject.w_OLDINCASS," "),"N")<>EVL(NVL(this.oParentObject.w_PAINCASS," "),"N") OR EVL(NVL(this.oParentObject.w_OLDSALDO," "),"N")<>EVL(NVL(this.oParentObject.w_PASALDO," "),"N")
                Ah_ErrorMsg("Attenzione, inserito codice pagamento con impostazioni acconto contestuale e sconto finanziario differenti dal precedente, i calcoli dei campi corrispondenti verranno rieseguiti")
                this.oParentObject.w_MVIMPFIN = 0
                this.oParentObject.w_MVFLSFIN = " "
                this.oParentObject.w_TROVSAL = 0
                this.oParentObject.w_MVACCONT = 0
                this.oParentObject.w_TROVPAG = 0
              endif
            endif
            i_retcode = 'stop'
            return
          else
            * --- Preso da A.M. CheckForm
            if this.w_ISALT and this.pPARAM=="B"
              this.w_PADRE.w_MVTOTENA = 0
              this.w_PADRE.w_MVTOTRIT = 0
              this.w_PADRE.w_TOTMDR = 0
            endif
            if this.w_FLFOSC<>"S"
              * --- Se no Sconti Forzati, Ricalcola lo Sconto Globale
              if isalt()
                this.oParentObject.w_MVSCONTI = Calsco(this.oParentObject.w_TOTMERCE-this.oParentObject.w_TOTRIP3-this.oParentObject.w_TOTRIP4, this.oParentObject.w_MVSCOCL1, this.oParentObject.w_MVSCOCL2, this.oParentObject.w_MVSCOPAG, this.oParentObject.w_DECTOT)
              else
                this.oParentObject.w_MVSCONTI = Calsco(this.oParentObject.w_TOTMERCE, this.oParentObject.w_MVSCOCL1, this.oParentObject.w_MVSCOCL2, this.oParentObject.w_MVSCOPAG, this.oParentObject.w_DECTOT)
              endif
            endif
            * --- Se attivo modulo contributi accessori e causale e intestatario
            *     lo gestiscono lancio la routine di controllo dettaglio...
            * --- Verifica Congruita' Spese Accessorie/Sconti di Testata
            if Not this.w_ISALT
              ah_Msg("Ripartisco spese accessorie e sconti globali...",.T.)
            endif
            if g_COAC="S"
              this.oParentObject.w_RESCHK = 0
              this.w_PADRE.NotifyEvent("ChkFinali")     
              * --- Se il risultato 
              if this.oParentObject.w_RESCHK=-1
                i_retcode = 'stop'
                return
              endif
            else
              * --- Rilancio la ripartizione solo se non � gi� stato fatto dall'evento chkFinali
              this.w_PADRE.NotifyEvent("RipaEnt")     
            endif
          endif
        endif
    endcase
    if this.w_PARAM<>"D" And this.oParentObject.w_MVFLVEAC="V"
      * --- Ricalcolo le provvigioni per tutto il dettaglio se non
      *     da generazione automatica e nelcaso di ciclo vendite
      this.w_PADRE.NotifyEvent("Calcprov_BFA")     
    endif
    * --- Non blocco in caso di generazione fatture differite o se premuto bottone calcoli
    *     Blocco se manca il listino di testata ed � attivo il flag calcola sconto
    if this.w_PARAM="A" AND this.w_PADRE.w_RESCHK=-1 
      i_retcode = 'stop'
      return
    endif
    if (this.w_PARAM <>"D" And g_VEFA="S" And this.oParentObject.w_MVTIPIMB<>"N" And Not(this.w_PARAM = "B" And this.oParentObject.w_MVFLFOCA="S"))
      * --- In caricamento devo calcolare le cauzioni degli imballi solo se cliente e causale gestite a cauzioni
      *     In modifica, se gestiti gli imballi, devo lanciarlo sempre poich� questo batch serve anche per far si che i transitori vengano riempiti
      *     cos� che nel GSAR_BEA possano essere utilizzati
      *     
      *     Anche se non attive le cauzioni devo lanciare il calcola imballi per la esplosione automatica imballi che potrebbe essere attiva sull'articolo.
      *     Nel batch GSVA_BCI poi inibisco il calcolo della cauzione se non attiva sul cliente e sul documento
      this.w_PADRE.NotifyEvent("CalcolaImballi")     
    endif
    DIMENSION RA[1000, 11]
    DIMENSION TMP_AI[6, 12]
    this.w_MYSELF = This
    * --- Inizializza Variabili Globali/Locali
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Applico Castiva
    this.pDOCINFO=createobject("DOCOBJ",this.w_PADRE)
    this.pDOCINFO.cRead = "N"
    this.pDOCINFO.cParam = this.pPARAM
    this.pDOCINFO.cAzione = iif(this.w_PARAM="D","Load",this.w_PADRE.cFunction)
    * --- Devo rileggere i dati dall'oggetto padre
    this.pDOCINFO.mcHeader.AppendBlank()     
    if this.w_SOLORATE
      this.pDOCINFO.cSoloRate = .T.
      this.pDOCINFO.nTOTIMPOS = this.oParentObject.w_TOTIMPOS
      this.pDOCINFO.nTOTIMPON = this.oParentObject.w_TOTIMPON
      this.pDOCINFO.nIMPSPE = IIF(NOT EMPTY(this.oParentObject.w_MVIVAINC),this.oParentObject.w_MVSPEINC,0) + IIF(NOT EMPTY(this.oParentObject.w_MVIVAIMB),this.oParentObject.w_MVSPEIMB,0) + IIF(NOT EMPTY(this.oParentObject.w_MVIVATRA),this.oParentObject.w_MVSPETRA,0)+ this.oParentObject.w_MVIMPARR
    endif
    this.pDOCINFO.mcHeader.MVCODIVE = this.w_PADRE.w_MVCODIVE
    this.pDOCINFO.mcHeader.PERIVE = this.w_PADRE.w_PERIVE
    this.pDOCINFO.mcHeader.BOLIVE = this.w_PADRE.w_BOLIVE
    this.pDOCINFO.mcHeader.MVFLSCOR = this.w_PADRE.w_MVFLSCOR
    this.pDOCINFO.mcHeader.MVFLSCOM = this.w_FLSCOM
    this.pDOCINFO.mcHeader.CCTIPDOC = this.w_TIPDOC
    this.pDOCINFO.mcHeader.DECTOT = this.w_PADRE.w_DECTOT
    this.pDOCINFO.mcHeader.MVFLFOSC = IIF ( VarType( this.w_PADRE.w_MVFLFOSC )="C" , this.w_PADRE.w_MVFLFOSC , " " )
    this.pDOCINFO.mcHeader.MVCODPAG = this.w_PADRE.w_MVCODPAG
    this.pDOCINFO.mcHeader.MVSCOCL1 = this.w_PADRE.w_MVSCOCL1
    this.pDOCINFO.mcHeader.MVSCOCL2 = this.w_PADRE.w_MVSCOCL2
    this.pDOCINFO.mcHeader.MVSCOPAG = this.w_PADRE.w_MVSCOPAG
    this.pDOCINFO.mcHeader.MVSCOPAG = this.w_PADRE.w_MVSCOPAG
    this.pDOCINFO.mcHeader.MVSCONTI = this.w_PADRE.w_MVSCONTI
    this.pDOCINFO.mcHeader.MVDATDOC = this.w_PADRE.w_MVDATDOC
    this.pDOCINFO.mcHeader.MVFLVEAC = this.w_PADRE.w_MVFLVEAC
    this.pDOCINFO.mcHeader.MVIMPARR = this.w_PADRE.w_MVIMPARR
    this.pDOCINFO.mcHeader.PEIARR = iif(Type("this.w_PADRE.w_PEIARR")="N",this.w_PADRE.w_PEIARR,0)
    this.pDOCINFO.mcHeader.MVSPETRA = this.w_PADRE.w_MVSPETRA
    this.pDOCINFO.mcHeader.MVSPEINC = this.w_PADRE.w_MVSPEINC
    this.pDOCINFO.mcHeader.MVSPEIMB = this.w_PADRE.w_MVSPEIMB
    this.pDOCINFO.mcHeader.MVIVATRA = this.w_PADRE.w_MVIVATRA
    this.pDOCINFO.mcHeader.MVIVAIMB = this.w_PADRE.w_MVIVAIMB
    this.pDOCINFO.mcHeader.MVIVAINC = this.w_PADRE.w_MVIVAINC
    this.pDOCINFO.mcHeader.PEITRA = this.w_PADRE.w_PEITRA
    this.pDOCINFO.mcHeader.BOLTRA = this.w_PADRE.w_BOLTRA
    this.pDOCINFO.mcHeader.PEIIMB = this.w_PADRE.w_PEIIMB
    this.pDOCINFO.mcHeader.BOLIMB = this.w_PADRE.w_BOLIMB
    this.pDOCINFO.mcHeader.PEIINC = this.w_PADRE.w_PEIINC
    this.pDOCINFO.mcHeader.BOLINC = this.w_PADRE.w_BOLINC
    this.pDOCINFO.mcHeader.MVFLRTRA = this.w_PADRE.w_MVFLRTRA
    this.pDOCINFO.mcHeader.MVCODORN = iif(Type("this.w_PADRE.w_MVCODORN")="C",this.w_PADRE.w_MVCODORN,"     ")
    this.pDOCINFO.mcHeader.MVCLADOC = this.w_PADRE.w_MVCLADOC
    this.pDOCINFO.mcHeader.MVCODVAL = this.w_PADRE.w_MVCODVAL
    this.pDOCINFO.mcHeader.MVFLRINC = this.w_PADRE.w_MVFLRINC
    this.pDOCINFO.mcHeader.MVFLRIMB = this.w_PADRE.w_MVFLRIMB
    this.pDOCINFO.mcHeader.GIOFIS = this.w_PADRE.w_GIOFIS
    this.pDOCINFO.mcHeader.GIORN1 = this.w_PADRE.w_GIORN1
    this.pDOCINFO.mcHeader.GIORN2 = this.w_PADRE.w_GIORN2
    this.pDOCINFO.mcHeader.MESE1 = this.w_PADRE.w_MESE1
    this.pDOCINFO.mcHeader.MESE2 = this.w_PADRE.w_MESE2
    this.pDOCINFO.mcHeader.ACQINT = this.w_PADRE.w_ACQINT
    this.pDOCINFO.mcHeader.BOLSUP = this.w_PADRE.w_BOLSUP
    this.pDOCINFO.mcHeader.BOLESE = this.w_PADRE.w_BOLESE
    this.pDOCINFO.mcHeader.MVACCPRE = this.w_PADRE.w_MVACCPRE
    this.pDOCINFO.mcHeader.MVTOTENA = this.w_PADRE.w_MVTOTENA
    this.pDOCINFO.mcHeader.MVTOTRIT = this.w_PADRE.w_MVTOTRIT
    this.pDOCINFO.mcHeader.MVACCONT = this.w_PADRE.w_MVACCONT
    this.pDOCINFO.mcHeader.MVDATDIV = this.w_PADRE.w_MVDATDIV
    this.pDOCINFO.mcHeader.MVFLSALD = this.w_PADRE.w_MVFLSALD
    this.pDOCINFO.mcHeader.CLBOLFAT = this.w_PADRE.w_CLBOLFAT
    this.pDOCINFO.mcHeader.BOLMIN = this.w_PADRE.w_BOLMIN
    this.pDOCINFO.mcHeader.BOLARR = this.w_PADRE.w_BOLARR
    this.pDOCINFO.mcHeader.BOLCAM = this.w_PADRE.w_BOLCAM
    this.pDOCINFO.mcHeader.MVSPEBOL = this.w_PADRE.w_MVSPEBOL
    this.pDOCINFO.mcHeader.MVIVABOL = this.w_PADRE.w_MVIVABOL
    this.pDOCINFO.mcHeader.MVCAUIMB = iif(Type("this.w_PADRE.w_MVCAUIMB")="N",this.w_PADRE.w_MVCAUIMB,0)
    this.pDOCINFO.mcHeader.BOLCAU = iif(Type("this.w_PADRE.w_BOLCAU")="C",this.w_PADRE.w_BOLCAU,"     ")
    this.pDOCINFO.mcHeader.MVIVACAU = iif(Type("this.w_PADRE.w_MVIVACAU")="C",this.w_PADRE.w_MVIVACAU,"     ")
    this.pDOCINFO.mcHeader.REVCAU = iif(Type("this.w_PADRE.w_REVCAU")="C",this.w_PADRE.w_REVCAU," ")
    this.pDOCINFO.mcHeader.MVRIFACC = iif(Type("this.w_PADRE.w_MVRIFACC")="C",this.w_PADRE.w_MVRIFACC," ")
    * --- Se ho il campo ritenute gestito da modulo valorizzato le ritenute attive
    *     non concorrono al calcolo delle rate
    this.pDOCINFO.mcHeader.MVRITATT = iif(Type("this.w_PADRE.w_MVRITATT")="N" AND this.w_PADRE.w_MVTOTRIT=0,this.w_PADRE.w_MVRITATT , 0 )
    this.pDOCINFO.cFLINCA = iif(Type("this.w_PADRE.w_FLINCA")="C",this.w_PADRE.w_FLINCA," ")
    this.pDOCINFO.nTROVPAG = iif(Type("this.w_PADRE.w_TROVPAG")="N",this.w_PADRE.w_TROVPAG,0)
    this.pDOCINFO.nTROVSAL = iif(Type("this.w_PADRE.w_TROVSAL")="N",this.w_PADRE.w_TROVSAL,0)
    if this.w_PARAM<>"D"
      this.pDOCINFO.mcHeader.MVIVAARR = this.w_PADRE.w_MVIVAARR
      this.pDOCINFO.mcHeader.CCTIPREG = this.w_PADRE.w_TIPREG
      this.pDOCINFO.mcHeader.MVACCSUC = this.w_PADRE.w_MVACCSUC
      this.pDOCINFO.mcHeader.MVTIPCON = this.w_PADRE.w_MVTIPCON
      this.pDOCINFO.mcHeader.MVRITPRE = this.w_PADRE.w_MVRITPRE
      this.pDOCINFO.mcHeader.MVPERFIN = this.w_PADRE.w_MVPERFIN
      this.pDOCINFO.mcHeader.MVIMPFIN = this.w_PADRE.w_MVIMPFIN
      this.pDOCINFO.mcHeader.MVFLSFIN = this.w_PADRE.w_MVFLSFIN
    else
      this.pDOCINFO.mcHeader.CCTIPREG = iif(Type("this.w_PADRE.oParentobject.w_CCTIPREG")="C",this.w_PADRE.oParentobject.w_CCTIPREG," ")
      if Type("this.w_PADRE.oParentobject.w_MVTIPCON")="C"
        this.pDOCINFO.mcHeader.MVTIPCON = this.w_PADRE.oParentobject.w_MVTIPCON
      else
        if Type("this.w_PADRE.w_MVTIPCON")="C"
          this.pDOCINFO.mcHeader.MVTIPCON = this.w_PADRE.w_MVTIPCON
        endif
      endif
    endif
    if this.w_PARAM<>"D"
      * --- Segno la posizione sul Temporaneo
      this.w_PADRE.MarkPos()     
    endif
    * --- Testa Uscita dal temporaneo per errore
    this.w_EXITERR = .F.
    if this.w_PARAM="A" And this.oParentObject.w_CHKTOT="S"
      * --- Se da gestione documenti, verifico se esiste almeno una riga a 0, se 
      *     esiste lo segnala (check su casuale documenti)
      this.w_MYSELF.Exec_Select("_Tmp_BFA","Count(*) As Conta ","(NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND NVL(IIF("+Cp_TOStrODBC(this.w_PARAM)+"='D', t_MVFLOMAG,  IIF( t_MVFLOMAG=1,'X',' ' ) ), ' ')='X' And t_FLSERA<>'S' And t_MVPREZZO = 0","","","")     
      if Nvl(_Tmp_BFA.Conta,0)>0
        this.w_MESS = "Attenzione: righe a valore 0. Impossibile salvare"
        ah_ErrorMsg(this.w_MESS,,"")
        this.w_EXITERR = .T.
        this.w_PADRE.w_RESCHK = -1
      endif
      Use in _Tmp_BFA
    endif
    if Not this.w_EXITERR
      this.w_MYSELF.FirstRow()     
      do while Not this.w_MYSELF.Eof_Trs()
        this.w_MYSELF.SetRow()     
        if this.w_PARAM $ "AB" And (NOT EMPTY( this.w_MYSELF.Get("w_MVCODICE"))) AND this.w_MYSELF.Get("w_MVTIPRIG") $ "RFMA"
          this.w_CODART = this.w_MYSELF.Get("w_MVCODART")
          this.w_TQTAMOV = this.w_MYSELF.Get("w_MVQTAMOV")
          this.w_TQTAUM1 = this.w_MYSELF.Get("w_MVQTAUM1")
          * --- Controlli Preliminari all Conferma del Documento
          do case
            case EMPTY(this.w_MYSELF.Get("w_MVCODART"))
              this.w_MESS = "Codice articolo non definito"
              this.w_EXITERR = .T.
            case EMPTY(this.w_MYSELF.Get("w_MVCODIVA"))
              this.w_MESS = "Codice IVA non definito"
              this.w_EXITERR = .T.
            case EMPTY(this.w_MYSELF.Get("w_MVUNIMIS")) AND this.w_MYSELF.Get("w_MVTIPRIG") $ "RM"
              this.w_MESS = "Unit� di misura non definita"
              this.w_EXITERR = .T.
            case (EMPTY(this.w_TQTAMOV) OR EMPTY(this.w_TQTAUM1)) AND this.w_MYSELF.Get("w_MVTIPRIG")$ "RM"
              if this.w_TQTAUM1=0
                this.w_MESS = "Quantit� nella 1^UM inesistente"
              else
                this.w_MESS = "Quantit� inesistente"
              endif
              this.w_EXITERR = .T.
          endcase
          if this.w_EXITERR
            this.w_ROWORD = this.w_MYSELF.Get("w_CPROWORD")
            this.w_MESS = ah_MsgFormat("Verificare riga: %1%0%2", ALLTRIM(STR(this.w_ROWORD)), ah_MsgFormat(this.w_MESS))
            ah_ErrorMsg(this.w_MESS,,"")
            this.w_PADRE.w_RESCHK = -1
            EXIT
          endif
        endif
        this.pDOCINFO.mcDetail.AppendBlank()     
        this.pDOCINFO.mcDetail.MVCODICE = this.w_MYSELF.Get("w_MVCODICE")
        this.pDOCINFO.mcDetail.MVTIPRIG = this.w_MYSELF.Get("w_MVTIPRIG")
        if this.w_PARAM<>"D"
          this.pDOCINFO.mcDetail.MVFLOMAG = ICASE(this.w_MYSELF.Get("w_MVFLOMAG")=1,"X",this.w_MYSELF.Get("w_MVFLOMAG")=2,"S",this.w_MYSELF.Get("w_MVFLOMAG")=3,"I","E")
        else
          this.pDOCINFO.mcDetail.MVFLOMAG = this.w_MYSELF.Get("w_MVFLOMAG")
        endif
        if this.w_MYSELF.GetType("w_MVDATEVA")="D" 
          this.pDOCINFO.mcDetail.MVDATEVA = this.w_MYSELF.Get("w_MVDATEVA")
        endif
        this.pDOCINFO.mcDetail.MVVALRIG = this.w_MYSELF.Get("w_MVVALRIG")
        this.pDOCINFO.mcDetail.MVIMPSCO = this.w_MYSELF.Get("w_MVIMPSCO")
        this.pDOCINFO.mcDetail.MVCODIVA = this.w_MYSELF.Get("w_MVCODIVA")
        this.pDOCINFO.mcDetail.PERIVA = this.w_MYSELF.Get("w_PERIVA")
        this.pDOCINFO.mcDetail.FLSERA = this.w_MYSELF.Get("w_FLSERA")
        this.pDOCINFO.mcDetail.BOLIVA = this.w_MYSELF.Get("w_BOLIVA")
        this.pDOCINFO.mcDetail.MVVALMAG = this.w_MYSELF.Get("w_MVVALMAG")
        this.pDOCINFO.mcDetail.MVIMPNAZ = this.w_MYSELF.Get("w_MVIMPNAZ")
        this.pDOCINFO.mcDetail.MVIMPCOM = this.w_MYSELF.Get("w_MVIMPCOM")
        this.w_MYSELF.NextRow()     
      enddo
    endif
    if this.w_PARAM<>"D" And this.w_EXITERR
      * --- Mi riposiziono
      this.w_PADRE.RePos()     
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.pDOCINFO.cNoRelease = .T.
    this.w_RET = CASTIVARATE(.null., this.pDOCINFO)
    this.oParentObject.w_MVSPEBOL = this.w_RET.nMVSPEBOL
    this.oParentObject.w_TOTIMPON = this.w_RET.nTOTIMPON
    this.oParentObject.w_TOTIMPOS = this.w_RET.nTOTIMPOS
    if Type("This.oparentobject.w_MVIMPFIN")="N"
      this.oParentObject.w_MVIMPFIN = this.w_RET.nMVIMPFIN
    endif
    if Type("This.oparentobject.w_MVACCONT")="N"
      this.oParentObject.w_MVACCONT = this.w_RET.nMVACCONT
    endif
    if Type("This.oparentobject.w_TROVPAG")="N"
      this.oParentObject.w_TROVPAG = this.w_RET.nTROVPAG
    endif
    if Type("This.oparentobject.w_TROVSAL")="N"
      this.oParentObject.w_TROVSAL = this.w_RET.nTROVSAL
    endif
    if Type("This.oparentobject.w_MVFLSALD")="C"
      this.oParentObject.w_MVFLSALD = this.w_RET.nMVFLSALD
    endif
    this.oParentObject.w_BOLARR = this.w_RET.nBOLARR
    if this.w_PARAM<>"D"
      * --- Se Chiamato dai Documenti e Ricevuta Fiscale e Incassata...
      this.oParentObject.w_MVACCONT = this.w_RET.nMVACCONT
      this.oParentObject.w_MVFLSALD = this.w_RET.nMVFLSALD
      if Not Empty(this.w_RET.nErrorLog)
        ah_ErrorMsg("%1" ,"","",Alltrim(this.w_RET.nErrorLog))
        if this.w_PARAM<>"D"
          this.w_PADRE.w_RESCHK = -1
          this.w_PADRE.RePos()     
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
    else
      if Not Empty(this.w_RET.nErrorLog) and Vartype(This.oparentobject.w_oERRORLOG)="O"
        This.oparentobject.w_RETBFA=.t.
        if vartype(This.oparentobject.w_MESS_BFA)="C"
          This.oparentobject.w_MESS_BFA=this.w_RET.nErrorLog
        endif
      endif
    endif
    * --- Calcoli Finali
    if this.w_PARAM<>"D"
      * --- Aggiorna i Riepiloghi IVA
      if Not Empty(this.w_RET.cWarningLog )
        ah_ErrorMsg("%1" ,"","",Alltrim( this.w_RET.cWarningLog ))
      endif
      this.w_MVACIVA = SPACE(5)
      this.w_MVAIMPN = 0
      this.w_MVAIMPS = 0
      this.w_NVACIVA = SPACE(5)
      this.w_NVAIMPN = 0
      this.w_NVAIMPS = 0
      this.w_OLDDOC = 0
      this.w_NEWDOC = 0
      this.w_OK = .F.
      this.w_OK1 = .F.
      this.w_contrate = 1
      this.w_RET.mcCastiva.gotop()     
      do while not this.w_RET.mcCastiva.eof()
        y = ALLTRIM(STR(this.w_contrate))
        this.w_MVACIVA = this.w_PADRE.w_MVACIVA&y
        this.w_MVAIMPN = this.w_PADRE.w_MVAIMPN&y
        this.w_MVAIMPS = this.w_PADRE.w_MVAIMPS&y
        this.w_MVAFLOM = this.w_PADRE.w_MVAFLOM&y
        this.w_NVACIVA = this.w_RET.mcCastiva.CODIVA
        this.w_NVAIMPN = this.w_RET.mcCastiva.IMPON
        this.w_NVAIMPS = this.w_RET.mcCastiva.IMPOS
        this.w_NVAFLOM = this.w_RET.mcCastiva.FLOMAG
        this.w_OLDDOC = this.w_OLDDOC + (this.w_MVAIMPN + this.w_MVAIMPS)
        this.w_NEWDOC = this.w_NEWDOC + (this.w_NVAIMPN + this.w_NVAIMPS)
        * --- Se diventa .T., i Totali sarebbero da Ricalcolare..
        this.w_OK = this.w_OK OR this.w_MVAIMPN<>this.w_NVAIMPN OR this.w_MVAIMPS<>this.w_NVAIMPS
        this.w_OK1 = this.w_OK1 OR this.w_MVACIVA<>this.w_NVACIVA OR this.w_MVAFLOM<>this.w_NVAFLOM
        this.w_contrate = this.w_contrate + 1
        this.w_RET.mcCastiva.Next()     
      enddo
      * --- Test per Aggiornamento dei Riepiloghi IVA
      if this.oParentObject.w_MVFLVEAC="V"
        * --- Se Ciclo Attivo Aggiorna Sempre
        this.w_OK = .T.
      else
        if this.w_OK1 OR (this.w_OLDDOC=0 AND this.w_NEWDOC<>0)
          * --- Variati i Codici IVA oppure i Riepiloghi non sono ancora Stati Inseriti, Ricalcola Sempre
          this.w_OK = .T.
        else
          if this.w_OK
            * --- Se I totali sono Stati Inseriti e sono Diversi dal Calcolo, chiede Conferma
            this.w_OK = ah_YesNo("Aggiorno anche i riepiloghi IVA?")
          endif
        endif
      endif
      * --- Aggiornamento Riepiloghi IVA
      this.oParentObject.w_TOTIMPOS = 0
      this.oParentObject.w_TOTIMPON = 0
      this.w_contrate = 1
      this.w_RET.mcCastiva.gotop()     
      do while this.w_contrate<=6
        y = ALLTRIM(STR(this.w_contrate))
        if this.w_RET.mcCastiva.eof()
          this.w_PADRE.w_MVACIVA&y = Space(5)
          this.w_PADRE.w_MVAIMPN&y = 0
          this.w_PADRE.w_MVAIMPS&y = 0
          this.w_PADRE.w_MVAFLOM&y = Space(1)
        else
          * --- Eseguo controllo IVA indetraibile
          this.w_MVACIVA = this.w_RET.mcCastiva.CODIVA
          if this.w_OK
            if this.w_TIPREG="V"
              * --- Read from VOCIIVA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.VOCIIVA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "IVPERIND"+;
                  " from "+i_cTable+" VOCIIVA where ";
                      +"IVCODIVA = "+cp_ToStrODBC(this.w_MVACIVA);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  IVPERIND;
                  from (i_cTable) where;
                      IVCODIVA = this.w_MVACIVA;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_PERIND<>0
                ah_ErrorMsg("Riga castelletto: %1 codice IVA [%2] di tipo indetraibile" ,"i","",y,Alltrim(this.w_MVACIVA))
                this.w_PADRE.w_RESCHK = -1
                if this.w_PARAM<>"D"
                  this.w_PADRE.RePos()     
                endif
                this.Pag4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
            this.w_PADRE.w_MVACIVA&y = this.w_RET.mcCastiva.CODIVA
            this.w_PADRE.w_MVAIMPN&y = this.w_RET.mcCastiva.IMPON
            this.w_PADRE.w_MVAIMPS&y = this.w_RET.mcCastiva.IMPOS
            this.w_PADRE.w_MVAFLOM&y = this.w_RET.mcCastiva.FLOMAG
            if this.w_RET.mcCastiva.ARRSCORP<>0
              * --- Nel caso in cui eseguendo lo scorporo sulla somma delle righe
              *     ottengo un valore differente rispetto alla somma dei singoli scorpori 
              *     vado ad aggiustare MVVALMAG asul dettaglio per evitare
              *     squadrature in sede di contabilizzazione.
              *     Vado a ricercare la prima riga del dettaglio per l'aliquota, ed aggiorno
              *     su di essa i vari importi di riga a partire da MVVALMAG.
              *     MVVALRIG non � modificato perch� questo importo � l'unico
              *     non toccato dalla ripartizione (che ad ogni ricalcolo rivalorizza gli importi
              *     senza questo aggiustamento)
              this.w_TmpC = this.w_RET.mcCastiva.CODIVA
              this.w_NDIFF = this.w_RET.mcCastiva.ARRSCORP
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Azzerata Forzatura Castelletto IVA
            this.w_OLDDOC = this.w_NEWDOC
          endif
          * --- Totale Documento
          this.oParentObject.w_TOTIMPOS = this.oParentObject.w_TOTIMPOS + IIF(this.w_PADRE.w_MVAFLOM&y $ "XI", this.w_PADRE.w_MVAIMPS&y, 0) 
          this.oParentObject.w_TOTIMPON = this.oParentObject.w_TOTIMPON + IIF(this.w_PADRE.w_MVAFLOM&y = "X", this.w_PADRE.w_MVAIMPN&y, 0) 
          this.w_RET.mcCastiva.Next()     
        endif
        this.w_contrate = this.w_contrate+1
      enddo
      this.oParentObject.w_TOTIMPON = this.oParentObject.w_TOTIMPON - this.oParentObject.w_MVSPEBOL
      this.oParentObject.w_TOTFATTU = this.oParentObject.w_TOTIMPON + this.oParentObject.w_TOTIMPOS + this.oParentObject.w_MVSPEBOL
      if this.oParentObject.w_TROVSAL=1 and this.oParentObject.w_MVCLADOC="FA" and this.oParentObject.w_MVPERFIN=0
        this.w_RESARRO = (this.oParentObject.w_TOTFATTU-(this.oParentObject.w_MVTOTRIT+this.oParentObject.w_MVTOTENA+this.oParentObject.w_MVRITPRE)) -(this.oParentObject.w_MVACCONT+this.oParentObject.w_MVACCPRE) 
        if TYPE("this.oParentObject.w_MVIMPFIN")="N" 
          if this.w_RESARRO<>0
            this.oParentObject.w_MVIMPFIN=this.w_RESARRO
            this.oParentObject.w_MVFLSFIN="S"
          else
            this.oParentObject.w_MVIMPFIN=0
            this.oParentObject.w_MVFLSFIN=" "
          endif
        endif
      else
        if this.oParentObject.w_TOTFATTU-(this.oParentObject.w_MVTOTRIT+this.oParentObject.w_MVTOTENA+this.oParentObject.w_MVRITPRE+this.oParentObject.w_MVIMPFIN)<(this.oParentObject.w_MVACCONT+this.oParentObject.w_MVACCPRE) and this.w_PADRE.w_PAGATO_DA_NEWDOC <> "S"
          * --- Se Gli Acconti sono Maggiori del Documento Avverte
          this.w_MESS = "Totale acconti e/o importi ritenute superiori al totale documento!"
          ah_ErrorMsg(this.w_MESS,,"")
          this.w_PADRE.w_RESCHK = -1
          if this.w_PARAM<>"D"
            this.w_PADRE.RePos()     
          endif
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      if (this.w_OLDDOC<>this.w_NEWDOC And (Type("This.oparentobject.w_MVIMPFIN")="U" Or this.oParentObject.w_MVPERFIN=0 )) OR (Type("This.oparentobject.w_MVIMPFIN")<>"U" AND this.oParentObject.w_MVIMPFIN<>this.w_RET.nMVIMPFIN)
        * --- RIchiamo il calcolo solo per le rate mantenendo il totale imponibiel imposta
        *     che trovo sul castelletto IVA...
        this.pDOCINFO.cSoloRate = .T.
        this.pDOCINFO.nTOTIMPOS = this.oParentObject.w_TOTIMPOS
        this.pDOCINFO.nTOTIMPON = this.oParentObject.w_TOTIMPON
        this.pDOCINFO.nIMPSPE = this.w_RET.nIMPSPE
        this.pDOCINFO.nMVSPEBOL = this.w_RET.nMVSPEBOL
        this.pDOCINFO.nMVACCONT = this.w_RET.nMVACCONT
        this.pDOCINFO.mcHeader.MVIMPFIN = this.oParentObject.w_MVIMPFIN
        this.w_OLDMVIMPFIN = this.w_RET.nMVIMPFIN
        this.w_RET = CASTIVARATE(.null., this.pDOCINFO)
        * --- Ricalcola le rate solo se gli Importi sono cambiati
        if this.w_OLDMVIMPFIN<>this.w_RET.nMVIMPFIN
          this.w_GSVE_MRT.w_AGGIORNO = .T.
        else
          this.w_GSVE_MRT.w_AGGIORNO = .F.
        endif
      endif
      * --- Se l'utente decide di non aggioranre il castelletto IVA ed il totale di questo
      *     differisce da quanto calcolato sull'array, ricalcolo le rate in base a quanto presente sul
      *     castelletto IVA.
      *     (Se non presente lo sconto finanziario)
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Totale Documento
      this.oParentObject.w_TOTFATTU = this.oParentObject.w_TOTIMPON + this.oParentObject.w_TOTIMPOS + this.oParentObject.w_MVSPEBOL 
      * --- Aggiornamento Riepiloghi IVA
      if !this.w_SOLORATE
        this.w_contrate = 1
        this.w_RET.mcCastiva.gotop()     
        do while this.w_contrate<=6
          y = ALLTRIM(STR(this.w_contrate))
          if not this.w_RET.mcCastiva.eof()
            this.w_PADRE.w_MVACIVA&y = this.w_RET.mcCastiva.CODIVA
            this.w_PADRE.w_MVAIMPN&y = this.w_RET.mcCastiva.IMPON
            this.w_PADRE.w_MVAIMPS&y = this.w_RET.mcCastiva.IMPOS
            this.w_PADRE.w_MVAFLOM&y = this.w_RET.mcCastiva.FLOMAG
            if this.w_RET.mcCastiva.ARRSCORP<>0
              * --- Nel caso in cui eseguendo lo scorporo sulla somma delle righe
              *     ottengo un valore differente rispetto alla somma dei singoli scorpori 
              *     vado ad aggiustare MVVALMAG asul dettaglio per evitare
              *     squadrature in sede di contabilizzazione.
              *     Vado a ricercare la prima riga del dettaglio per l'aliquota, ed aggiorno
              *     su di essa i vari importi di riga a partire da MVVALMAG.
              *     MVVALRIG non � modificato perch� questo importo � l'unico
              *     non toccato dalla ripartizione (che ad ogni ricalcolo rivalorizza gli importi
              *     senza questo aggiustamento)
              this.w_TmpC = this.w_RET.mcCastiva.CODIVA
              this.w_NDIFF = this.w_RET.mcCastiva.ARRSCORP
              this.Pag3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.w_RET.mcCastiva.Next()     
          else
            this.w_PADRE.w_MVACIVA&y = Space(5)
            this.w_PADRE.w_MVAIMPN&y = 0
            this.w_PADRE.w_MVAIMPS&y = 0
            this.w_PADRE.w_MVAFLOM&y = Space(1)
          endif
          this.w_contrate = this.w_contrate+1
        enddo
      endif
      * --- Carica il Vettore Rate Per le Fatture Differite
      this.w_contrate = 1
      this.w_RET.mcRate.gotop()     
      do while not this.w_RET.mcRate.eof()
         
 DR[this.w_contrate,1] = this.w_RET.mcRate.DATRAT 
 DR[this.w_contrate,2] = this.w_RET.mcRate.IMPNET 
 DR[this.w_contrate,3] = this.w_RET.mcRate.MODPAG 
 DR[this.w_contrate,4] = "" 
 DR[this.w_contrate,5] = "" 
 DR[this.w_contrate,6] = "" 
 DR[this.w_contrate,7] = "" 
 DR[this.w_contrate,8] = "" 
 DR[this.w_contrate,9] = this.w_RET.mcRate.FLPROV
        this.w_contrate = this.w_contrate+1
        this.w_RET.mcRate.Next()     
      enddo
      DR[1000,2] =this.w_RET.nTOTRATE
    endif
    * --- Calcolo ritenute, valorizza i campi di testata MVTOTRIT,MVTOTENA,MVRITPRE
    *     dalla gestione GSVE_MDR calcoli ritenute (Batch GSCG_BRT)
    if this.w_PARAM<>"D"
      if ((g_RITE="S" and not empty( this.oParentObject.w_MVCODCON ) and nvl( this.oParentObject.w_GESRIT," " )="S" and ((this.oParentObject.w_FLRITE$"CS" AND this.oParentObject.w_MVTIPCON = "F") OR (this.oParentObject.w_FLGRITE = "S" AND this.oParentObject.w_MVTIPCON = "C")) AND this.oParentObject.w_MVCLADOC $ "FA-NC") OR (this.w_ISALT and (nvl( this.oParentObject.w_GESRIT," " )="S" or Empty(this.oParentObject.w_MVCAUCON)) and not empty( this.oParentObject.w_MVCODCON ) and this.oParentObject.w_FLGRITE = "S" AND this.oParentObject.w_MVTIPCON = "C" AND this.oParentObject.w_MVCLADOC $ "FA-NC-DI"))
        * --- Se cambia il totale IVA indetraibile apro la gestione ritenute
        if (this.w_PARAM$ "A" or (this.pParam=="B" and this.w_ISALT)) And this.oParentObject.w_TOTFATTU<>this.oParentObject.w_TOTMDR And this.oParentObject.w_TOTIMPON<>0 
          * --- Aggiornamento Riepiloghi IVA
          this.oParentObject.w_TOTIMPOS = 0
          this.w_LOOP = 1
          do while this.w_LOOP <= 6
            y = ALLTRIM(STR( this.w_LOOP ))
            this.oParentObject.w_TOTIMPOS = this.oParentObject.w_TOTIMPOS + IIF(this.oParentObject.w_MVAFLOM&y $ "XI", this.oParentObject.w_MVAIMPS&y ,0)
            this.w_LOOP = this.w_LOOP + 1
          enddo
          if VARTYPE(g_SCHEDULER)="C" AND g_SCHEDULER="S"
            this.w_PADRE.GSVE_MDR.LinkPCClick(.t.)     
          else
            this.w_PADRE.GSVE_MDR.LinkPCClick(iif(this.pParam=="B" and this.w_ISALT,.t.,.f.))     
            if this.pParam=="B"
              this.w_PADRE.GSVE_MDR.cnt.bonscreen = .f.
            endif
          endif
          if this.w_ISALT and this.w_PARAM= "A"
            * --- Impostiamo il valore di ForzaChiusura in modo che alla conferma della maschera si chiuda anche GSVE_MDV
            this.w_PADRE.GSVE_MDR.cnt.w_ForzaChiusura = 1
            * --- Impostiamo il valore di Da_Ritenute in modo che alla conferma della maschera non venga eseguito il ricalcolo delle Spese generali e del CPA (in GSAR_BST)
            this.w_PADRE.w_Da_Ritenute = "S"
          endif
          * --- Se <>-1 allora ricalcolo i dati delle ritenute
          if this.oParentObject.w_TESTRITE
            this.w_PADRE.GSVE_MDR.cnt.Notifyevent("Aggiorna")     
            this.w_RICRATE = .t.
          endif
          if Upper( this.w_PADRE.GSVE_MDR.class)<>"STDLAZYCHILD" AND this.w_PARAM= "A"
            this.w_PADRE.GSVE_MDR.cnt.w_CALCOLI = .T.
          endif
          if this.w_PARAM="A"
            this.w_PADRE.w_RESCHK = -1
          endif
        endif
      else
        if this.w_ISALT AND ((this.w_PADRE.w_TOTDAPAG<> this.w_PADRE.w_TOTFATTU - this.w_PADRE.w_MVTOTRIT - this.w_PADRE.w_MVIMPFIN) OR this.w_PADRE.w_TOTDAPAG=0 ) AND this.w_PADRE.w_PAGATO_DA_NEWDOC = "S"
          this.w_RICRATE = .T.
        endif
      endif
    endif
    if this.w_RICRATE AND this.w_ISALT AND this.oParentObject.w_RESCHK<>-1
      if this.w_PADRE.w_PAGATO_DA_NEWDOC = "S"
        * --- Viene valorizzato il campo Acconto contestuale
        this.w_PADRE.w_MVACCONT = this.w_PADRE.w_TOTFATTU - this.w_PADRE.w_MVTOTRIT - this.w_PADRE.w_MVIMPFIN
      endif
      if this.w_PARAM $ "B-A" 
        * --- RIchiamo il calcolo solo per le rate mantenendo il totale imponibile imposta
        *     che trovo sul castelletto IVA...
        this.pDOCINFO.cSoloRate = .T.
        this.pDOCINFO.nTOTIMPOS = this.oParentObject.w_TOTIMPOS
        this.pDOCINFO.nTOTIMPON = this.oParentObject.w_TOTIMPON
        this.pDOCINFO.nIMPSPE = this.w_RET.nIMPSPE
        this.pDOCINFO.nMVSPEBOL = this.w_RET.nMVSPEBOL
        if this.w_PADRE.w_MVACCONT<> this.pDOCINFO.nMVACCONT AND this.w_PADRE.w_PAGATO_DA_NEWDOC = "S" AND Vartype(this.w_PADRE.Gsve_acp.cnt)="O"
          * --- Valorizzo contropartita di incasso
          this.w_PADRE.Gsve_acp.cnt.w_TOTACC = this.w_PADRE.w_MVACCONT
          this.w_PADRE.Gsve_acp.cnt.w_TOTRES = 0
          this.w_PADRE.Gsve_acp.cnt.w_CPIMPPRE = this.w_PADRE.w_MVACCONT
        endif
        this.pDOCINFO.nMVACCONT = this.w_PADRE.w_MVACCONT
        this.pDOCINFO.mcHeader.MVTOTENA = this.w_PADRE.w_MVTOTENA
        this.pDOCINFO.mcHeader.MVTOTRIT = this.w_PADRE.w_MVTOTRIT
        this.w_RET = CASTIVARATE(.null., this.pDOCINFO)
        * --- Ricalcola le rate solo se gli Importi sono cambiati
        this.w_GSVE_MRT.w_AGGIORNO = .T.
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.w_PARAM<>"D"
      this.w_PADRE.RePos()     
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Globali
    * --- Variabili Locali
    * --- Test Doc.Esente
    this.w_FLESEN = NOT EMPTY(this.oParentObject.w_MVCODIVE)
    this.w_LOOP = 1
    do while this.w_LOOP<=1000
       
 RA[ this.w_LOOP , 1] = cp_CharToDate("  -  -  ") 
 RA[ this.w_LOOP , 2] = 0 
 RA[ this.w_LOOP , 3] = 0 
 RA[ this.w_LOOP , 4] = 0 
 RA[ this.w_LOOP , 5] = "  " 
 RA[ this.w_LOOP , 6] = "  " 
 RA[ this.w_LOOP , 7] = "  " 
 RA[ this.w_LOOP , 8] = "  " 
 RA[ this.w_LOOP , 9] = " " 
 RA[ this.w_LOOP , 10] = " " 
 RA[ this.w_LOOP , 11] = " "
      if this.w_PARAM="D"
        * --- Vettore Definito il GSVE_BFD (Fatture Differite)
         
 DR[ this.w_LOOP , 1] = cp_CharToDate("  -  -  ") 
 DR[ this.w_LOOP, 2] = 0 
 DR[ this.w_LOOP, 3] = "  "
      endif
      this.w_LOOP = this.w_LOOP + 1
    enddo
    if !this.w_SOLORATE
      this.oParentObject.w_TOTIMPON = 0
      this.oParentObject.w_TOTIMPOS = 0
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cerca la Prima Riga corrispondente all'Aliquota e aggiorna il Valore Fiscale
    *     w_TMPC = Codice IVa
    *     w_NDIFF = Resto da imputare
    this.w_RECNUM = this.w_MYSELF.Search( "(t_MVCODIVA='"+ this.w_TmpC +"' OR NOT EMPTY("+iif(this.w_PARAM<>"D","this.","this.oParentObject.")+"w_MVCODIVE)) AND NOT DELETED() AND t_MVVALMAG<>0 AND NVL(IIF("+Cp_ToStrODBC( this.w_PARAM )+"='D', t_MVFLOMAG,  IIF( t_MVFLOMAG=1,'X',' ' )  ), ' ') = 'X'" , 0 )
    if this.w_RECNUM=-1
      this.w_RECNUM = this.w_MYSELF.Search( "(t_MVCODIVA='"+ this.w_TmpC +"' OR NOT EMPTY("+iif(this.w_PARAM<>"D","this.","this.oParentObject.")+"w_MVCODIVE)) AND NOT DELETED() AND t_MVVALMAG<>0 AND NVL(IIF("+Cp_ToStrODBC( this.w_PARAM )+"='D',  t_MVFLOMAG  ,IIF( t_MVFLOMAG=3,'I', IIF( t_MVFLOMAG=4,'E',' ' ) ) ), ' ') $ 'IE'" , 0 )
    endif
    if this.w_RECNUM<>-1
      * --- Aggiorna importi di riga
      this.w_MYSELF.SetRow(this.w_RECNUM)     
      * --- In alcuni casi (Generazione fatture differite) PERIVA e INDIVA non sono nel transitorio
      if this.w_MYSELF.GetType("w_PERIVA")<>"U"
        this.w_TPERIVA = this.w_MYSELF.Get("w_PERIVA")
      else
        this.w_TPERIVA = 0
      endif
      if this.w_MYSELF.GetType("w_INDIVA")<>"U"
        this.w_TINDIVA = this.w_MYSELF.Get("w_INDIVA")
      else
        this.w_TINDIVA = 0
      endif
      * --- Aggiungo a MVVALMAG il resto
      this.w_TVALMAG = this.w_MYSELF.Get("w_MVVALMAG") + this.w_NDIFF
      * --- Aggiorno anche la prorpiet� w_ per evitare problemi con la replace al termine della mCalc
      this.w_MYSELF.Set("w_MVVALMAG" , this.w_TVALMAG, .f., .T.)     
      if Type("This.oParentObject.w_INDIVE")="U"
        * --- Nel caso GSAR_BFA venga lanciato dalle generazioni massive o import
        *     potrebbe non essere definita la variabile w_INDIVE nel chiamante.
        *     Quindi la rileggo
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIND"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVCODIVE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIND;
            from (i_cTable) where;
                IVCODIVA = this.oParentObject.w_MVCODIVE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LINDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        this.w_LINDIVE = this.oParentObject.w_INDIVE
      endif
      this.w_APPO = CAIMPNAZ(this.oParentObject.w_MVFLVEAC, this.w_TVALMAG, this.oParentObject.w_MVCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ, this.oParentObject.w_MVCODVAL, this.oParentObject.w_MVCODIVE, this.oParentObject.w_PERIVE, this.w_LINDIVE, Nvl(this.w_TPERIVA,0), Nvl(this.w_TINDIVA,0) )
      this.w_MYSELF.Set("w_MVIMPNAZ" , this.w_APPO, .T., .T.)     
      if this.w_MYSELF.GetType("w_VISNAZ")<>"U" And Type("this.oparentobject.w_DECTOP")="N"
        this.w_MYSELF.Set("w_VISNAZ" , cp_ROUND(this.w_APPO, this.oParentObject.w_DECTOP), .T., .T.)     
      endif
      this.w_TCODCOM = this.w_MYSELF.Get("w_MVCODCOM")
      if Not Empty(Nvl(this.w_TCODCOM," ")) And g_COMM="S"
        * --- Ricerca della valuta della commessa e dei suoi decimali
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNCODVAL"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_TCODCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNCODVAL;
            from (i_cTable) where;
                CNCODCAN = this.w_TCODCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COCODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_COCODVAL<>g_PERVAL
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADECTOT"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_COCODVAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADECTOT;
              from (i_cTable) where;
                  VACODVAL = this.w_COCODVAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.w_DECCOM = g_PERPVL
        endif
        if this.w_MYSELF.GetType("w_MVFLEVAS")<>"U"
          this.w_TFLEVAS = this.w_MYSELF.Get("w_MVFLEVAS")
          * --- Nel caso in cui la gestione usi MVFLEVAS come check � di tipo numerico sul transitorio
          if VarType( this.w_TFLEVAS )="N"
            this.w_TFLEVAS = IIF( this.w_TFLEVAS=1 , "S" ," " )
          endif
        else
          this.w_TFLEVAS = " "
        endif
        this.w_TIMPCOM = CAIMPCOM( IIF(Empty(this.w_TCODCOM),"S", this.w_TFLEVAS), this.oParentObject.w_MVVALNAZ, this.w_COCODVAL, this.w_APPO, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.w_DECCOM )
        this.w_MYSELF.Set("w_MVIMPCOM" , this.w_TIMPCOM , .T., .T.)     
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esco e rilascio i cursori delle rate
    this.pDOCINFO.Done()     
    this.pDOCINFO = .Null.
    i_retcode = 'stop'
    return
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Notifica che il Batch ha avuto successo (Salvo ulteriore conferma in GSVE_BRT)
    this.w_PADRE.w_RESCHK = 0
    * --- Aggiorna archivio rate Scadenze (GSVE_BRT)
    this.w_contrate = 1
    * --- Reinizializza l'array RA
    this.w_LOOP_AI = 1
    do while this.w_LOOP_AI<=1000
       
 RA[ this.w_LOOP_AI , 1] = cp_CharToDate("  -  -  ") 
 RA[ this.w_LOOP_AI , 2] = 0 
 RA[ this.w_LOOP_AI , 3] = 0 
 RA[ this.w_LOOP_AI , 4] = 0 
 RA[ this.w_LOOP_AI , 5] = "  " 
 RA[ this.w_LOOP_AI , 6] = "  " 
 RA[ this.w_LOOP_AI , 7] = "  " 
 RA[ this.w_LOOP_AI , 8] = "  " 
 RA[ this.w_LOOP_AI , 9] = " " 
 RA[ this.w_LOOP_AI , 10] = " " 
 RA[ this.w_LOOP_AI , 11] = " "
      this.w_LOOP_AI = this.w_LOOP_AI + 1
    enddo
    this.w_RET.mcRate.gotop()     
    * --- Per le rate aggiorno solo gli importi (la modifica dello sconto non pu� modificare altro di fatto)
    do while not this.w_RET.mcRate.eof()
       
 RA[this.w_contrate,1] = this.w_RET.mcRate.DATRAT 
 RA[this.w_contrate,2] = this.w_RET.mcRate.IMPNET 
 RA[this.w_contrate,3] = this.w_RET.mcRate.IMPIVA 
 RA[this.w_contrate,4] = this.w_RET.mcRate.IMPSPE 
 RA[this.w_contrate,5] = this.w_RET.mcRate.MODPAG 
 RA[this.w_contrate,6] = this.w_RET.mcRate.FLPROV 
 RA[this.w_contrate,7] = "" 
 RA[this.w_contrate,8] = "" 
 RA[this.w_contrate,9] = "" 
 RA[this.w_contrate,10] = ""
      this.w_contrate = this.w_contrate+1
      this.w_RET.mcRate.Next()     
    enddo
    RA[1000,2] =this.w_RET.nTOTRATE
    this.w_GSVE_MRT.NotifyEvent("CaricaRate")     
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='VOCIIVA'
    this.cWorkTables[5]='PAG_AMEN'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- gsar_bfa
  * Il batch gestisce sia il transitorio dei documenti
  * che un transitorio costruito da batch per la generazione
  * documenti (Es. GSVE_BFD)
  * Definiti quindi metodi propri di questo batch che in base
  * al parametro lanciano metodi sul transitorio della gestione
  * o su GENEAPP (cursore creato dalle generazioni)
  
  Procedure Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
  LOCAL cName_1,cName_2,cCmdSql,cOlderr,cMsgErr
   If This.w_PARAM='D'
  	* Assegnamenti..
  	cOlderr=ON('error')
  	cMsgErr=''
  	On error cMsgErr=Message(1)+': '+MESSAGE()
  	
  	cTmp=   IIF( EMPTY(cTmp)   , '__Tmp__', cTmp )
  	cFields=IIF( EMPTY(cFields), '*'      , cFields )
  	cWhere= IIF( EMPTY(cWhere) , '1=1'    , cWhere )	
  	cOrder= IIF( EMPTY(cOrder) , '1'      , cOrder )	
  	
  	cOrder=IIF( EMPTY(cGroupBy),'',' GROUP BY '+(cGroupBy)+IIF( EMPTY(cHaving),'',' HAVING '+(cHaving) ) )+' Order By '+cOrder
  	
  	cName_1='GENEAPP'
  	
  	cCmdSql='Select '+cFields+' From '+cName_1			
  	cCmdSql=cCmdSql+' Where '
  	
  	cCmdSql=cCmdSql+cWhere+' &cOrder Into Cursor '+cTmp+' Nofilter '
  	* Eseguo la frase..
  	&cCmdSql
  
  	* Ripristino la vecchia gestione errori
  	ON ERROR &cOldErr
  
  	* Se Qualcosa va storto lo segnalo ed esco...
  	  IF NOT EMPTY(cMsgErr)
        ah_ErrorMsg(cMsgErr,'stop','')
        * memorizzo nella clipboard la frase generata in caso di errore
        _ClipText=cCmdSql
      ELSE
  		  * mi posiziono sul temporaneo al primo record..
  		  SELECT (cTmp)
  		  GO Top
      ENDIF  
   Else
    This.w_PADRE.Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
   Endif
  Endproc
  
  Procedure FirstRow()
   LOCAL cOldArea
   If This.w_PARAM='D'	
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
    Select GENEAPP
    Go Top
  	* ripristino la vecchia area
  	SELECT(coldArea)  
   else
    this.w_PADRE.FirstRow()
   endif
  EndProc
  
  PROCEDURE SetRow(id_row) 
  LOCAL cOldArea
   if This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se paramento valorizzato mi posiziono sulla riga passata 
  	* altrimenti sulla riga attuale...
  	IF TYPE( 'id_row' )='N'
  		SELECT( 'GENEAPP' )
  		goto (id_row) 	
  	EndIf
  		
  	* ripristino la vecchia area
  	SELECT(coldArea)	  
   else
  	 this.w_PADRE.SetRow( id_Row )
   endif
  
  Endproc
  
  FUNCTION GET(Item)
  LOCAL cOldArea,cFldName
   if This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se le prima due lettere sono w_ le sostituisco con t_
  	* altrimenti lascio quanto trovo...
  	IF lower(LEFT(Item,2))='w_' 
  		cFldName='t_'+SUBSTR(Item,3,LEN(Item))
  	ELSE
  		cFldName=Item
  	ENDIF
  
  	SELECT( 'GENEAPP' )
  	* Leggo il campo...
  	Result=eval(cFldName)
  	
  	* ripristino la vecchia area
  	SELECT(coldArea)	
  	
  	* Restituisco il valore recuperato..
  	Return(Result)
     
   else
    Return( This.w_PADRE.Get(Item) )
   Endif
  EndFunc
  
  Procedure NextRow()
  LOCAL cOldArea
   If This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
   
    Select 'GENEAPP'
    Skip
    
  	* ripristino la vecchia area
  	SELECT(coldArea)  
    
   Else
    This.w_PADRE.NextRow()
   Endif
  	
  EndProc
  
  Function Eof_Trs() 
   if This.w_PARAM='D'
    RETURN (EOF('GENEAPP'))
   else
  	 RETURN (this.w_PADRE.Eof_Trs())
   endif
  ENDFUNC
  
  Function Search(Criterio,StartFrom)
  LOCAL cOldArea,cName_1, cCursName,nResult,cCond
     If This.w_PARAM='D'
    	* Memorizzo l'area corrente per ripristino
    	cOldarea=SELECT()
    	cName_1='GENEAPP'
    	cCursName=SYS(2015)	
    	* Se ricerca a partire da un certo record aggiungo la condizione..
    	IF TYPE('StartFrom')='N' AND StartFrom<>0
    	cCond=' RECNO(cName_1)>'+ALLTRIM(STR(StartFrom))
    	ELSE
    	cCond=' 1=1 '
    	Endif
    		SELECT MIN(RECNO()) As Riga FROM (cName_1) WHERE  &cCond AND ;
    		&Criterio INTO CURSOR &cCursName
    	* leggo il risultato
    	SELECT(cCursName)
    	GO Top
    	nResult=NVL(&cCursName..Riga,-1) 
    	nResult=IIF(nResult=0,-1, nResult)
    	* rimuovo il cursore di appoggio
        if used(cCursName)
          select (cCursName)
          use
        ENDIF    
    	* ripristino la vecchia area
    	SELECT(coldArea)
    	* restituisco il risultato
    	RETURN nResult
      
     Else
      Return(This.w_PADRE.Search(Criterio,StartFrom))
     Endif
    
    EndFunc
  
   
  
  Procedure SET(cItem,vValue,bNoUpd,bUpdProp)
  LOCAL cOldArea,cFldName,bResult
   If This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se le prima due lettere sono w_ le sostituisco con t_
  	* altrimenti lascio quanto trovo...
  	IF lower(LEFT(cItem,2))='w_' 
  		cFldName='t_'+SUBSTR(cItem,3,LEN(cItem))
  	ELSE
  		cFldName=cItem
  	ENDIF
  
  	* Aggiorno il transitorio normale...
  	SELECT( 'GENEAPP' )
  		
  	* Se cambio allora svolgo la Replace..
  	IF &cFldName<>vValue
  		Replace &cFldName WITH vValue
  	endif			
  	* ripristino la vecchia area
  	SELECT(coldArea) 
   Else
    This.w_PADRE.Set(cItem,vValue,bNoUpd,bUpdProp)
   Endif
  EndProc
  
  FUNCTION GetType(cFieldsName)
   If This.w_PARAM='D'
   	LOCAL cOldArea,cFldName
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se le prima due lettere sono w_ le sostituisco con t_
  	* altrimenti lascio quanto trovo...
  	IF lower(LEFT(cFieldsName,2))='w_' 
  		cFldName='t_'+SUBSTR(cFieldsName,3,LEN(cFieldsName))
  	ELSE
  		cFldName=cFieldsName
  	ENDIF	
  	
  		SELECT( 'GENEAPP'  )
  			
  	* Leggo il campo...
  	Result=Type(cFldName)
  				
  	* ripristino la vecchia area
  	SELECT(coldArea)		
  	Return( Result )
   ELSE
   Return(This.w_PADRE.GetType( cFieldsName ))
   endif
  EndFunc
  
  Procedure Done()
   *Azzerro la variabile di comodo, se non lo facessi rimarebbe la classe
   *del batch appesa in memoria, impedendo tra l'altro la compilazione
   *all'interno di ad hoc
   this.w_MYSELF=.Null.
   DoDefault()
  EndpRoc
  
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
