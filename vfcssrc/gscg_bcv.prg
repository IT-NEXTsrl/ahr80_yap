* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bcv                                                        *
*              Contabilizzazione documenti                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_910]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-23                                                      *
* Last revis.: 2018-02-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bcv",oParentObject)
return(i_retval)

define class tgscg_bcv as StdBatch
  * --- Local variables
  w_SEARCH_TABLE = space(25)
  w_APPO = space(10)
  w_NUDOC = 0
  w_OKDOC = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_ROWCOUNT = 0
  w_WARNMSG = space(254)
  w_NUMFAT = space(50)
  w_MES_DIM = 0
  w_WADOC = 0
  w_LASTWAR = space(10)
  w_LASTWARROW = space(10)
  w_oSEGNLOG = .NULL.
  w_oERRORLOG = .NULL.
  w_oSEGNLOG = .NULL.
  w_OSERIAL = space(10)
  w_SIMVAL = space(3)
  w_PNDESRIG = space(50)
  w_IMPSPL = 0
  w_IMPSPL = 0
  w_PNAGG_01 = space(15)
  w_PNAGG_02 = space(15)
  w_PNAGG_03 = space(15)
  w_PNAGG_04 = space(15)
  w_PNAGG_05 = ctod("  /  /  ")
  w_PNAGG_06 = ctod("  /  /  ")
  w_IVAOMAIND = 0
  w_LPERIVA = 0
  w_TDBOLDOG = space(1)
  w_FLSCOM = space(1)
  w_CAUGIRO = space(5)
  w_CONCESP = .f.
  w_IVAREV_DA_TOG = 0
  w_MESS_ERR = space(200)
  w_OPNCODCAU = space(5)
  w_TOTCOMP = 0
  w_FDAVE = space(1)
  w_IMPFIN = 0
  w_RITATT = 0
  w_AGGIORNA = .f.
  w_LOOP = 0
  w_SEGNALA = space(254)
  w_COD_IVA_CNT = 0
  w_OKPARSPL = .f.
  w_RIGASPL = 0
  w_LOOP = 0
  w_BLOOP = 0
  w_RIGPAR = space(1)
  w_LPARTSN = space(1)
  w_CODPAG1 = space(5)
  w_AREC = 0
  w_RIMP = 0
  w_RIVA = 0
  w_IIMP = 0
  w_IIVA = 0
  w_TOTIVA = 0
  w_REVCHA = space(1)
  w_CODINV = space(5)
  w_CODAUT = space(5)
  w_AICONDET = space(15)
  w_IVTIPCOP = space(1)
  w_PERINDV = 0
  w_PERINDA = 0
  w_OK = .f.
  w_TROV = .f.
  ASPE = space(10)
  w_ALIVA = 0
  w_APPINC = 0
  w_APPIMB = 0
  w_APPTRA = 0
  w_ELAINC = 0
  w_ELAIMB = 0
  w_ELATRA = 0
  w_TOTMER = 0
  w_IMPSC2 = 0
  w_SCOCL1 = 0
  w_SCOCL2 = 0
  w_SCOPAG = 0
  w_PNSERIAL = space(10)
  w_APPCURFLOMAG = space(1)
  w_PNCODCAU = space(5)
  w_PNVALNAZ = space(3)
  w_PNCOMIVA = ctod("  /  /  ")
  w_CCNUMREG = 0
  w_PNNUMRER = 0
  w_PNTIPREG = space(1)
  w_PNCODVAL = space(3)
  w_PNDESSUP = space(50)
  w_DATBLO = ctod("  /  /  ")
  w_PNNUMPRO = 0
  w_PNFLIVDF = space(1)
  w_PNCAOVAL = 0
  w_PNCODUTE = 0
  w_PNPRD = space(2)
  w_PNTOTDOC = 0
  w_PNNUMREG = 0
  w_PNTIPCLF = space(1)
  w_PNDATREG = ctod("  /  /  ")
  w_PNPRP = space(2)
  w_CPROWNUM = 0
  w_AINUMREG = 0
  w_PNCODCLF = space(15)
  w_PNALFDOC = space(10)
  w_CPROWORD = 0
  w_PNTIPDOC = space(2)
  w_PNTIPCLF = space(1)
  w_PNDATDOC = ctod("  /  /  ")
  w_PNDATPLA = ctod("  /  /  ")
  w_PNPRG = space(8)
  w_PNCODESE = space(4)
  w_PNALFPRO = space(10)
  w_PNFLREGI = space(1)
  w_ARRSUP = 0
  w_PNCOMPET = space(4)
  w_PNFLPROV = space(1)
  w_PNNUMDOC = 0
  w_PNCODPAG = space(5)
  w_PNCODAGE = space(5)
  w_PTCODAGE = space(5)
  w_PNFLVABD = space(1)
  w_PTFLVABD = space(1)
  w_FLOMAG = space(1)
  w_CODPAG = space(5)
  w_CODIVE = space(5)
  w_APPO = space(10)
  w_PNFLPART = space(1)
  w_CONVEA = space(15)
  w_SEZCLF = space(1)
  w_FLPDOC = space(1)
  w_APPO1 = 0
  w_CFCATCON = space(3)
  w_CONOMA = space(15)
  w_APPSEZ = space(1)
  w_FLPPRO = space(1)
  w_APPO2 = 0
  w_ARCATCON = space(3)
  w_CONOMI = space(15)
  w_CODVAC = space(3)
  w_PNIMPAVE = 0
  w_APPO3 = 0
  w_CODIVA = space(5)
  w_CONNAC = space(15)
  w_TOTDAR = 0
  w_PNIMPDAR = 0
  w_PARTSN = space(1)
  w_PERIVA = 0
  w_DECTOT = 0
  w_TOTAVE = 0
  w_PNTIPCON = space(1)
  w_IVAIND = 0
  w_PERIND = 0
  w_TOTDOC = 0
  w_TOTCLF = 0
  w_APPVAL = 0
  w_PNCODCON = space(15)
  w_PNANNDOC = space(4)
  w_VALMAG = 0
  w_TOTVAL = 0
  w_IVAGLO = 0
  w_CCCONIVA = space(15)
  w_PNANNPRO = space(4)
  w_CONTRO = space(15)
  w_CAONAZ = 0
  w_IVADET = 0
  w_CCCONINT = space(15)
  w_DECVAC = 0
  w_CONIND = space(15)
  w_DECTOP = 0
  w_IVAOMA = 0
  w_SPEINC = 0
  w_FLRINC = space(1)
  w_IVFLOMAG = space(1)
  w_PTNUMPAR = space(31)
  w_PNIMPIND = 0
  w_SPEIMB = 0
  w_FLRIMB = space(1)
  w_IVCODIVA = space(5)
  w_PTDATSCA = ctod("  /  /  ")
  w_FLIND = .f.
  w_SPETRA = 0
  w_FLRTRA = space(1)
  w_IVPERIND = 0
  w_PTFLSOSP = space(1)
  w_ACCPRE = 0
  w_SPEBOL = 0
  w_FLVAL = space(1)
  w_IVCONTRO = space(15)
  w_IVIMPONI = 0
  w_PTMODPAG = space(10)
  w_MVRIFACC = space(10)
  w_ACCONT = 0
  AI = space(10)
  w_IVIMPIVA = 0
  w_PTTOTIMP = 0
  w_PTDESRIG = space(50)
  w_TROV = .f.
  w_FLERR = .f.
  w_MAXDAT = ctod("  /  /  ")
  w_CODINT = space(5)
  w_ATTIVI = space(5)
  w_CODAZI = space(5)
  w_CONTA = 0
  w_VABENE = .f.
  w_FLAINT = space(1)
  w_DATIVA = ctod("  /  /  ")
  w_ASSEG = .f.
  w_INICOMD = ctod("  /  /  ")
  w_MAXCOMIVA = ctod("  /  /  ")
  w_REGOK = .f.
  w_MESS = space(90)
  w_MESS1 = space(10)
  w_FINCOMD = ctod("  /  /  ")
  w_PNINICOM = ctod("  /  /  ")
  w_INICOM = ctod("  /  /  ")
  w_DATOBSO = ctod("  /  /  ")
  w_PTBANAPP = space(10)
  w_NUDOC = 0
  w_PNFINCOM = ctod("  /  /  ")
  w_FINCOM = ctod("  /  /  ")
  w_TIPO = space(1)
  w_IVTIPREG = space(1)
  w_OKDOC = 0
  w_VALRIT = 0
  w_PNFLZERO = space(1)
  w_MVCONCON = space(1)
  w_ACQINT = space(1)
  w_ACQAUT = space(1)
  w_ABDIFF = 0
  w_VALENA = 0
  w_MVCODSPE = space(3)
  SMER = space(10)
  w_ROWNUM = 0
  w_SCOMER = 0
  w_BANNOS = space(15)
  w_BANAPP = space(10)
  w_NUMCOR = space(15)
  w_PTBANNOS = space(15)
  w_TOTPAR = 0
  w_TESVAL = 0
  w_PARFOR = 0
  w_RESTO = 0
  w_PNFLABAN = space(6)
  w_ROWINI = 0
  w_DIFCON = 0
  w_STATUS = space(1)
  w_SALINI = space(1)
  w_FLSCOR = space(1)
  w_IVATRA = space(5)
  w_IVAIMB = space(5)
  w_IVAINC = space(5)
  w_IVABOL = space(5)
  w_OROWNUM = 0
  w_OROWORD = 0
  w_OAPPSEZ = space(1)
  w_RIGLOR = 0
  w_IMPNET = 0
  w_CODAGE = space(5)
  w_FLVABD = space(1)
  w_PAGCON = space(10)
  w_DATVAL = ctod("  /  /  ")
  w_CCFLCRED = space(1)
  w_CCFLDEBI = space(1)
  w_CCFLCOMM = space(1)
  w_COSCOM = 0
  w_COMVAL = 0
  w_IMPCRE = 0
  w_IMPDEB = 0
  w_TIPCAU = space(1)
  w_CONBAN = space(15)
  w_CAUMOV = space(5)
  w_FLMOVC = space(1)
  w_TIPBAN = space(1)
  w_FLCRDE = space(1)
  w_CALFES = space(3)
  w_IMPDEB = 0
  w_IMPCRE = 0
  w_CODVAL = space(3)
  w_PTNUMCOR = space(25)
  w_CONCOR = space(25)
  w_LCONCAU = space(15)
  w_LIVACAU = space(5)
  w_LIMPCAU = 0
  w_FLAACC = space(1)
  w_PARACC = 0
  w_DATACC = ctod("  /  /  ")
  w_PAGACC = space(10)
  w_TOTACC = 0
  w_ROWORD = 0
  w_ROWNUM1 = 0
  w_TESTACC = .f.
  w_DIFFACC = 0
  w_APPO4 = space(10)
  w_NETTO = 0
  w_NURATE = 0
  w_GIORN2 = 0
  w_MESE1 = 0
  w_MESE2 = 0
  w_ESCL2 = space(4)
  w_ESCL1 = space(4)
  w_GIORN1 = 0
  w_NUMRATE = 0
  w_RESIDUO = 0
  w_APPSEZ1 = space(1)
  w_FLSCAF = space(1)
  w_RIGACC = space(50)
  w_CLADOC = space(2)
  w_GIOFIS = 0
  w_CHKTOT = space(1)
  w_MRCODICE = space(15)
  w_MRCODVOC = space(15)
  w_MRPARAME = 0
  w_ROWANA = 0
  w_MR_SEGNO = space(1)
  w_MRTOTIMP = 0
  w_RTOT = 0
  w_TOTPAC = 0
  w_NR = space(10)
  w_ITOT = 0
  w_FLANAL = space(1)
  w_FLELAN = space(1)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CCTIPAGG = space(1)
  w_OKAN = .f.
  w_GESRIT = space(1)
  w_TRFLGSTO = space(1)
  w_MODRITPA = space(10)
  w_RITPRE = 0
  w_LRITPRE = 0
  w_DTOBSO = ctod("  /  /  ")
  w_CONTROPARTITA = space(15)
  w_CCCONACQ = space(15)
  w_TOTPERIVA = 0
  w_CCCALDOC = space(1)
  w_CCDESRIG = space(254)
  w_CCDESSUP = space(254)
  w_RITATT = 0
  w_ANNO = 0
  w_PN__MESE = 0
  w_ANCONRIF = space(15)
  w_IVA = 0
  w_GIORN1 = 0
  w_GIORN2 = 0
  w_GIOFIS = 0
  w_MESE1 = 0
  w_MESE2 = 0
  w_NURATE = 0
  w_CODPAG = space(5)
  w_GESTPAR = space(100)
  w_ROWCOMP = 0
  w_RIFCOMP = 0
  w_TIPPAG = space(2)
  w_RIFORD = 0
  w_RIFOLD = 0
  w_TEST = space(1)
  w_LTOTIMP = 0
  w_FLSOSP = space(1)
  w_TEST = space(1)
  w_LTOTIMP = 0
  w_FLSOSP = space(1)
  w_PARTSN3 = space(1)
  w_LCODAGE = space(5)
  w_RIFFIN = 0
  w_RIFRIT = 0
  w_TOTALEACC = 0
  w_ROWTIPCON = space(1)
  w_ACCEVAS = space(1)
  w_TmpN = 0
  w_TROVACC = .f.
  w_IMPSPL = 0
  w_MPSPLPAY = space(1)
  w_ROWSPL = 0
  w_APPSEZSPL = space(1)
  w_PT_SEGNO = space(1)
  w_MESS_ERR = space(200)
  w_SERIALE = space(10)
  w_MVCESSER = space(10)
  w_CPORDCES = 0
  w_INSCESP = .f.
  w_NOGENCES = .f.
  w_TIPSOT = space(1)
  w_SEARCH_CAU_CONT = space(5)
  w_SEARCH_IVCODIVA = space(5)
  * --- WorkFile variables
  ATTIDETT_idx=0
  AZIENDA_idx=0
  CAUIVA_idx=0
  CAUIVA1_idx=0
  CAU_CONT_idx=0
  CCM_DETT_idx=0
  COC_MAST_idx=0
  CONTI_idx=0
  CONTVEAC_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  ESERCIZI_idx=0
  MOD_PAGA_idx=0
  PAR_TITE_idx=0
  PNT_DETT_idx=0
  PNT_IVA_idx=0
  PNT_MAST_idx=0
  SALDICON_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  COLLCENT_idx=0
  MOVICOST_idx=0
  MASTRI_idx=0
  PNT_CESP_idx=0
  DATIRITE_idx=0
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametro Esegue: I = Non Spunta Documenti; D = Spunta Documenti
    * --- Contabilizzazione Documenti (Attivi e Passivi)
    * --- Parametri dalla Maschera
    this.w_WARNMSG = space(10)
    this.w_CODAZI = i_CODAZI
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- AI[] - Vettore Castelletto IVA : 1^Indice Num.Aliquota
    * --- 2^Indice : 1 =Flag Omaggio
    * --- 2 =Codice IVA
    * --- 3 =Imponibile
    * --- 4 =Imposta
    * --- 5 =%IVA
    * --- 6 =%Indetraibile
    DIMENSION ARRSCA[999,6]
    * --- Dimensione Array messaggi errori
    this.w_MES_DIM = 31
    DIMENSION AI[6, 8], MES[ this.w_MES_DIM ], SMER[10,2]
    * --- Carica i Documenti da Contabilizzare
    ah_Msg("Ricerca documenti da contabilizzare...",.T.)
    vq_exec("query\GSVE_QCD.VQR",this,"CONTADOC")
    if USED("CONTADOC")
      * --- Inizio Aggiornamento vero e proprio
      this.w_NUDOC = 0
      this.w_OKDOC = 0
      this.w_WADOC = 0
      * --- Ultima registrazione che ha generato un warning, utilizzata per decidere
      *     se riportare o meno i riferimenti del documento in caso di messaggio di warning
      this.w_LASTWAR = Repl("X",10)
      this.w_LASTWARROW = "XXXXX"
      if RECCOUNT("CONTADOC") > 0
        ah_Msg("Inizio fase di contabilizzazione...",.T.)
        * --- Fase di Blocco
        * --- 1 - Blocco la Prima Nota con data = Max (data reg.)
        * --- 2 - Blocco tutti i Registri Iva con data = Max( Data reg)
        * --- Calcolo il Max Data reg.
        if this.oParentObject.w_FLDATC="U"
          this.w_MAXDAT = this.oParentObject.w_DATCON
        else
          CALCULATE MAX(MVDATREG) FOR NOT EMPTY(NVL(MVSERIAL," ")) AND NOT EMPTY(NVL(MVCAUCON," ")) TO this.w_MAXDAT
        endif
        CALCULATE MAX(NVL(MVDATCIV, this.w_PNDATREG)) FOR NOT EMPTY(NVL(MVSERIAL," ")) AND NOT EMPTY(NVL(MVCAUCON," ")) TO this.w_MAXCOMIVA
        this.w_VABENE = .T.
        this.w_ASSEG = .F.
        * --- Leggo la data dei blocco e la data stampa del libro giornale
        this.w_MESS = BLOC_AZI( .T. , this.w_MAXDAT, .F. ) 
        if not Empty( this.w_MESS )
          ah_ErrorMsg("%1","!","", this.w_MESS)
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Try
        local bErr_051B6478
        bErr_051B6478=bTrsErr
        this.Try_051B6478()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          * --- Rimuovo i blocchi
          this.w_MESS = BLOC_AZI( .F. ) 
          if not Empty( this.w_MESS )
            ah_ErrorMsg("%1","!","",this.w_MESS)
          endif
          ah_ErrorMsg(i_errmsg,,"")
          this.Page_7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_051B6478
        * --- End
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Rimuovo i blocchi
        this.w_MESS = BLOC_AZI( .F. ) 
        if not Empty( this.w_MESS )
          ah_ErrorMsg("%1","!","",this.w_MESS)
        endif
        * --- Stampa Libro giornale e Registri Iva
        * --- Try
        local bErr_05184250
        bErr_05184250=bTrsErr
        this.Try_05184250()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Impossibile rimuovere i blocchi. Rimuoverli dai dati azienda",,"")
        endif
        bTrsErr=bTrsErr or bErr_05184250
        * --- End
        this.w_oMess=createobject("Ah_Message")
        this.w_oMess.AddMsgPartNL("Operazione completata")     
        this.w_oPart = this.w_oMess.AddMsgPartNL("N.%1 documenti contabilizzati")
        this.w_oPart.AddParam(ALLTRIM(STR(this.w_OKDOC)))     
        if this.w_WADOC<>0
          this.w_oPart = this.w_oMess.AddMsgPartNL("con %1 avvertimento/i.")
          this.w_oPart.AddParam(ALLTRIM(STR(this.w_WADOC)))     
        endif
        this.w_oPart = this.w_oMess.AddMsgPartNL("Su %1 documenti da contabilizzare.")
        this.w_oPart.AddParam(ALLTRIM(STR(this.w_NUDOC)))     
        this.w_oMess.Ah_ErrorMsg()     
        * --- LOG Errori
        if this.w_NUDOC>0 AND (this.w_OKDOC<>this.w_NUDOC OR this.w_WADOC>0) 
          if Isalt() AND Vartype(g_SCHEDULER)="C" and g_SCHEDULER="S"
            if Vartype(g_log)<>"U"
               
 g_log=this.w_oERRORLOG
            endif
          else
            this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori /warning riscontrati")     
          endif
        endif
        if Not EMpty(this.w_SEGNALA) and Isalt() and this.w_OKDOC>0 and !(Vartype(g_SCHEDULER)="C" and g_SCHEDULER="S") 
          this.w_oSEGNLOG.AddMsgLogNoTranslate(Ah_msgformat("Elenco documenti contabilizzati%0"))     
          this.w_oSEGNLOG.AddMsgLogNoTranslate(this.w_SEGNALA)     
          this.w_oSEGNLOG.PrintLog(This,"Elenco documenti contabilizzati","Si desidera stampare log generazione","Si desidera stampare log generazione")     
          this.w_oSEGNLOG = .Null.
        endif
      else
        ah_ErrorMsg("Per l'intervallo selezionato non esistono documenti da contabilizzare",,"")
      endif
      * --- Chiudo la Maschera
      This.bUpdateParentObject=.f.
      This.oParentObject.bUpdated=.f.
      This.oParentObject.ecpQuit()
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_051B6478()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Tento il blocco dei Registri IVA
    * --- se g_catazi vuoto esci e segnala nessuna attivit�
    if empty(nvl(g_catazi,"")) AND this.w_VABENE
      this.w_VABENE = .F.
      * --- Raise
      i_Error="Nessuna attivit� presente. Inserirne una per proseguire"
      return
    endif
    * --- Se il numero e il tipo del registro non ha attivit� significa che non � stato ancora stampato
    * --- infatti la stampa IVA crea in automatico una voce nell'attivit�, questo non esclude che qualche utentedecida di stampare i  regi
    * --- durante la contabilizzaione => associo il registro all'attivit� di default
    if this.w_VABENE
      select max(NVL(MVDATCIV, MVDATREG)) as MVDATREG,CCTIPREG,CCNUMREG FROM ;
      CONTADOC GROUP BY CCTIPREG,CCNUMREG ;
      WHERE NOT EMPTY(NVL(MVSERIAL," ")) AND NOT EMPTY(NVL(MVCAUCON," ")) ;
      into cursor reg_iva1
      vq_exec("query\GSVE3QCD.VQR",this,"reg_iva2")
      if USED("reg_iva2")
        SELECT * FROM reg_iva1 INTO CURSOR reg_iva4 UNION ;
        SELECT * FROM reg_iva2
        SELECT reg_iva2
        USE
      else
        SELECT * FROM reg_iva1 INTO CURSOR reg_iva4
      endif
      SELECT reg_iva1
      USE
      vq_exec("query\GSVE4QCD.VQR",this,"reg_iva3")
      if USED("reg_iva3")
        SELECT * FROM reg_iva4 INTO CURSOR reg_iva5 UNION ;
        SELECT * FROM reg_iva3
        SELECT reg_iva3
        USE
      else
        SELECT * FROM reg_iva4 INTO CURSOR reg_iva5
      endif
      SELECT reg_iva4
      USE
      select max(NVL(MVDATREG, this.w_PNDATREG)) as MVDATREG,CCTIPREG,CCNUMREG FROM ;
      reg_iva5 GROUP BY CCTIPREG,CCNUMREG ;
      into cursor reg_iva
      SELECT reg_iva5
      USE
      SELECT reg_iva
      scan
      this.w_MAXDAT = MVDATREG
      this.w_PNNUMREG = NVL(CCNUMREG, 1)
      this.w_PNTIPREG = NVL(CCTIPREG, "N")
      * --- Se ha un tipo IVA buono
      if this.w_PNTIPREG<>"N"
        this.w_CONTA = 0
        * --- Select from ATTIDETT
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ATDATBLO,ATCODATT  from "+i_cTable+" ATTIDETT ";
              +" where ATNUMREG="+cp_ToStrODBC(this.w_PNNUMREG)+" AND ATTIPREG="+cp_ToStrODBC(this.w_PNTIPREG)+"";
               ,"_Curs_ATTIDETT")
        else
          select ATDATBLO,ATCODATT from (i_cTable);
           where ATNUMREG=this.w_PNNUMREG AND ATTIPREG=this.w_PNTIPREG;
            into cursor _Curs_ATTIDETT
        endif
        if used('_Curs_ATTIDETT')
          select _Curs_ATTIDETT
          locate for 1=1
          do while not(eof())
          this.w_DATBLO = NVL(_Curs_ATTIDETT.ATDATBLO, cp_CharToDate("  -  -  "))
          this.w_ATTIVI = NVL(_Curs_ATTIDETT.ATCODATT, cp_CharToDate("  -  -  "))
          this.w_CONTA = this.w_CONTA+1
            select _Curs_ATTIDETT
            continue
          enddo
          use
        endif
        if this.w_conta>1
          * --- Raise
          i_Error="Esistono registri dello stesso numero e tipo associati ad una/pi� attivit�"
          return
          this.w_VABENE = .F.
        endif
        this.w_CONTA = 0
        if empty(NVL(this.w_datblo,cp_CharToDate("  -  -  "))) and this.w_VABENE
          if EMPTY(NVL(this.w_ATTIVI,""))
            * --- Se l'attivit� non la trovo per default � quella principale
            this.w_ATTIVI = g_CATAZI
            * --- Insert into ATTIDETT
            i_nConn=i_TableProp[this.ATTIDETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATTIDETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"ATCODATT"+",ATNUMREG"+",ATTIPREG"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_ATTIVI),'ATTIDETT','ATCODATT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'ATTIDETT','ATNUMREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'ATTIDETT','ATTIPREG');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'ATCODATT',this.w_ATTIVI,'ATNUMREG',this.w_PNNUMREG,'ATTIPREG',this.w_PNTIPREG)
              insert into (i_cTable) (ATCODATT,ATNUMREG,ATTIPREG &i_ccchkf. );
                 values (;
                   this.w_ATTIVI;
                   ,this.w_PNNUMREG;
                   ,this.w_PNTIPREG;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            * --- sotto tranzazione � meglio non mettere messaggi per avvisare l'utente di questa impostazione
            this.w_ASSEG = .T.
          endif
          * --- Inserisce <Blocco> per Primanota
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_MAXDAT),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATCODATT = "+cp_ToStrODBC(this.w_ATTIVI);
                +" and ATNUMREG = "+cp_ToStrODBC(this.w_PNNUMREG);
                +" and ATTIPREG = "+cp_ToStrODBC(this.w_PNTIPREG);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_MAXDAT;
                &i_ccchkf. ;
             where;
                ATCODATT = this.w_ATTIVI;
                and ATNUMREG = this.w_PNNUMREG;
                and ATTIPREG = this.w_PNTIPREG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- UN ALTRO STA STAMPANDO REGISTRI IVA - controllo concorrenza
          this.w_VABENE = .F.
          * --- Raise
          i_Error="Contabilizzazione annullata. Blocco stampa registri IVA"
          return
        endif
      endif
      endscan
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_05184250()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    select reg_iva
    scan for NVL(cctipreg,"")<>"N"
    this.w_PNNUMREG = NVL(CCNUMREG, 1)
    this.w_PNTIPREG = NVL(CCTIPREG, "N")
    * --- Write into ATTIDETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ATTIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
          +i_ccchkf ;
      +" where ";
          +"ATNUMREG = "+cp_ToStrODBC(this.w_PNNUMREG);
          +" and ATTIPREG = "+cp_ToStrODBC(this.w_PNTIPREG);
             )
    else
      update (i_cTable) set;
          ATDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          ATNUMREG = this.w_PNNUMREG;
          and ATTIPREG = this.w_PNTIPREG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ENDSCAN
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera i Documenti da Contabilizzare
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    this.w_oSEGNLOG=createobject("AH_ERRORLOG")
    * --- Testa il Cambio di Documento
    this.w_OSERIAL = REPL("#", 10)
    SELECT CONTADOC
    GO TOP
    SCAN FOR NOT EMPTY(NVL(MVSERIAL," ")) AND NOT EMPTY(NVL(MVCAUCON," "))
    * --- Testa Cambio Documento
    if this.w_OSERIAL<>ContaDoc.MvSerial
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT CONTADOC
      this.w_OSERIAL = ContaDoc.MvSerial
      * --- Inizializza i dati di Testata della Nuova Registrazione P.N.
      this.w_PNDATREG = IIF(this.oParentObject.w_FLDATC="U", this.oParentObject.w_DATCON, CP_TODATE(MVDATREG))
      this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
      this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
      this.w_PNCODCAU = MVCAUCON
      this.w_PNCODESE = NVL(MVCODESE, SPACE(4))
      this.w_PNCOMPET = NVL(MVCODESE, SPACE(4))
      this.w_INICOM = CP_TODATE(MVTINCOM)
      this.w_FINCOM = CP_TODATE(MVTFICOM)
      this.w_PNTIPREG = NVL(CCTIPREG, "N")
      this.w_PNFLIVDF = NVL(CCFLIVDF, " ")
      this.w_PNNUMREG = NVL(CCNUMREG, 1)
      this.w_PNNUMDOC = NVL(MVNUMDOC, 0)
      this.w_PNALFDOC = NVL(MVALFDOC, "  ")
      this.w_PNDATDOC = CP_TODATE(MVDATDOC)
      this.w_PNCOMIVA = CP_TODATE(IIF(this.oParentObject.w_FLVEAC="V" , NVL(MVDATCIV, this.w_PNDATREG), this.w_PNDATREG))
      this.w_PNDATPLA = CP_TODATE(MVDATPLA)
      this.w_VALRIT = NVL(MVTOTRIT, 0)
      this.w_VALENA = NVL(MVTOTENA, 0)
      this.w_BANNOS = NVL(MVCODBA2, SPACE(15))
      this.w_FLAACC = NVL(ANFLAACC, " ")
      this.w_CODAGE = NVL(MVCODAGE,SPACE(5))
      this.w_FLVABD = NVL(MVFLVABD," ")
      this.w_FLSCAF = NVL(MVFLSCAF," ")
      this.w_CLADOC = NVL(MVCLADOC,"  ")
      this.w_SERIALE = NVL(MVSERIAL,SPACE(10))
      this.w_FLANAL = NVL(FLANAL, " ")
      this.w_CHKTOT = Nvl(CHKTOT," ")
      this.w_GESRIT = NVL(CCGESRIT," ")
      this.w_RITPRE = NVL(MVRITPRE, 0)
      this.w_IMPFIN = NVL(MVIMPFIN, 0)
      this.w_RITATT = NVL(MVRITATT, 0)
      this.w_ANNO = Nvl(MV__ANNO,0)
      this.w_PN__MESE = Nvl(MV__MESE,0)
      this.w_NUMFAT = NVL(MVNUMFAT,"")
      * --- Read from COC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "BACONCOL,BACALFES,BATIPCON,BACODVAL"+;
          " from "+i_cTable+" COC_MAST where ";
              +"BACODBAN = "+cp_ToStrODBC(this.w_BANNOS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          BACONCOL,BACALFES,BATIPCON,BACODVAL;
          from (i_cTable) where;
              BACODBAN = this.w_BANNOS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NUMCOR = NVL(cp_ToDate(_read_.BACONCOL),cp_NullValue(_read_.BACONCOL))
        this.w_CALFES = NVL(cp_ToDate(_read_.BACALFES),cp_NullValue(_read_.BACALFES))
        this.w_TIPBAN = NVL(cp_ToDate(_read_.BATIPCON),cp_NullValue(_read_.BATIPCON))
        this.w_CODVAL = NVL(cp_ToDate(_read_.BACODVAL),cp_NullValue(_read_.BACODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Controllo Integrit� Prima Nota
      *     Controllo che il documento possa essere contabilizzato
      *     1 - Controllo che la data reg. sia maggiore ultima stampa libro giornale e data blocco prima Nota
      *     2 - Data reg. maggiore Data Stampa Registri Iva
      *     All'inizio della procedura vengono bloccati tutti i registri coinvolti e messa la data di blocco prima nota
      *     Controllo se posso registrarla - rileggo i dati ogni volta la contabilizzazione potrebbe protrarsi a lungo
      *     3 - Data Consolidamento
      this.w_REGOK = .T.
      * --- Non controllo la data blocco (l'ho appena impostata per impedire ad altri
      *     di caricare e non alle istruzioni che seguono..)
      this.w_MESS = CHKINPNT(this.w_PNDATREG,this.w_FLANAL,this.w_PNTIPREG,this.w_PNNUMREG,this.w_PNCOMIVA,NVL(CONTADOC.CCTIPDOC, "  ") ,"Load","SNSSS")
      if NOT EMPTY(this.w_MESS)
        this.w_REGOK = .F.
      else
        * --- Controllo anche x i Registri IVA
        this.w_DATIVA = cp_CharToDate("  -  -  ")
        * --- Select from ATTIDETT
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ATDATSTA  from "+i_cTable+" ATTIDETT ";
              +" where ATNUMREG="+cp_ToStrODBC(this.w_PNNUMREG)+" AND ATTIPREG="+cp_ToStrODBC(this.w_PNTIPREG)+"";
               ,"_Curs_ATTIDETT")
        else
          select ATDATSTA from (i_cTable);
           where ATNUMREG=this.w_PNNUMREG AND ATTIPREG=this.w_PNTIPREG;
            into cursor _Curs_ATTIDETT
        endif
        if used('_Curs_ATTIDETT')
          select _Curs_ATTIDETT
          locate for 1=1
          do while not(eof())
          this.w_DATIVA = NVL(CP_TODATE(_Curs_ATTIDETT.ATDATSTA),cp_CharToDate("  -  -  "))
            select _Curs_ATTIDETT
            continue
          enddo
          use
        endif
        * --- Normalmente la competenza IVA � sempre inferiore alla data di registrazione (fatt. differite)
        * --- Non capita ma se capita � meglio controllare anche la data registrazione
        this.w_REGOK = MIN(this.w_PNDATREG,this.w_PNCOMIVA)>this.w_DATIVA
        * --- Il messaggio lo inizializzo sempre - se serve lo prendo altrimenti no
        this.w_MESS = ah_MsgFormat("Data reg. � inferiore all'ultima stampa dei registri IVA")
      endif
      if this.w_REGOK
        SELECT CONTADOC
        this.w_PNTIPDOC = NVL(CCTIPDOC, "  ")
        this.w_FLPDOC = NVL(CCFLPDOC, "N")
        this.w_FLPPRO = NVL(CCFLPPRO, "N")
        this.w_PNPRD = IIF(this.w_PNTIPREG="V", NVL(MVPRD, "  "), "NN")
        this.w_PNPRP = NVL(MVPRP, "NN")
        this.w_PNANNPRO = NVL(MVANNPRO, "    ")
        this.w_PNALFPRO = IIF(this.oParentObject.w_FLVEAC="V" AND this.w_PNANNPRO<>"    ", NVL(CCSERPRO, "  "), NVL(MVALFEST, "  "))
        this.w_PNNUMPRO = NVL(MVNUMEST, 0)
        this.w_PNVALNAZ = NVL(MVVALNAZ, g_PERVAL)
        this.w_PNCODVAL = NVL(MVCODVAL, g_PERVAL)
        this.w_SIMVAL = Alltrim( NVL(VASIMVAL, "") )
        this.w_PNCAOVAL = NVL(MVCAOVAL, 1)
        this.w_PNTIPCLF = NVL(MVTIPCON, " ")
        this.w_PNCODCLF = NVL(MVCODCON, SPACE(15))
        this.w_PNTIPCLF = NVL(MVTIPCON, " ")
        this.w_ANCONRIF = NVL(ANCONRIF, " ")
        this.w_BANAPP = NVL(MVCODBAN, SPACE(10))
        this.w_CONCOR = NVL(MVNUMCOR, SPACE(25))
        this.w_CCCONIVA = NVL(CCCONIVA, SPACE(15))
        this.w_PNFLREGI = " "
        this.w_PNFLPROV = "N"
        this.w_PNDESRIG = NVL(MVDESDOC,SPACE(50))
        this.w_PNDESSUP = NVL(MVDESDOC,SPACE(50))
        this.w_DECTOT = NVL(VADECTOT, 0)
        this.w_TESVAL = NVL(VACAOVAL, 0)
        this.w_CFCATCON = NVL(ANCATCON, SPACE(3))
        this.w_ACQINT = IIF(NVL(AFFLINTR, "N")="S" AND this.oParentObject.w_FLVEAC="A" AND this.w_PNTIPDOC $ "FE-NE", "S", "N")
        this.w_ACQAUT = IIF(this.w_PNTIPDOC $ "AU-NU", "S", "N")
        this.w_MVCONCON = NVL(MVCONCON, " ")
        this.w_MVCODSPE = NVL(MVCODSPE, "   ")
        * --- Riferimento contabilizzazione acconto contestuale
        this.w_MVRIFACC = NVL(MVRIFACC, SPACE(10))
        this.w_CODPAG = NVL(MVCODPAG, SPACE(5))
        this.w_CODIVE = NVL(MVCODIVE, SPACE(5))
        this.w_PARTSN = "N"
        this.w_FLSCOR = NVL(ANSCORPO, " ")
        if g_PERPAR="S" AND NVL(ANPARTSN," ")="S"
          this.w_PARTSN = IIF(NVL(CCFLPART, " ") $ "ACS", CCFLPART, "N")
        endif
        this.w_CAONAZ = GETCAM(g_PERVAL, i_DATSYS)
        this.w_DECTOP = g_PERPVL
        * --- Se altra Valuta di Esercizio
        if this.w_PNVALNAZ<>g_PERVAL
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADECTOT"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_PNVALNAZ);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADECTOT;
              from (i_cTable) where;
                  VACODVAL = this.w_PNVALNAZ;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DECTOP = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, i_DATSYS)
          SELECT CONTADOC
        endif
        this.w_SPEINC = NVL(MVSPEINC, 0)
        this.w_FLRINC = NVL(MVFLRINC, " ")
        this.w_IVAINC = NVL(MVIVAINC, SPACE(5))
        this.w_SPEIMB = NVL(MVSPEIMB, 0)
        this.w_FLRIMB = NVL(MVFLRIMB, " ")
        this.w_IVAIMB = NVL(MVIVAIMB, SPACE(5))
        this.w_SPETRA = NVL(MVSPETRA, 0)
        this.w_FLRTRA = NVL(MVFLRTRA, " ")
        this.w_IVATRA = NVL(MVIVATRA, SPACE(5))
        this.w_SPEBOL = NVL(MVSPEBOL, 0)
        this.w_IVABOL = NVL(MVIVABOL, SPACE(5))
        this.w_ACCONT = NVL(MVACCONT, 0)
        this.w_ACCPRE = NVL(MVACCPRE, 0)
        this.w_LCONCAU = Nvl(ANCONCAU,Space(15))
        this.w_LIVACAU = Nvl(MVIVACAU, Space(5))
        this.w_LIMPCAU = Nvl(MVCAUIMB,0)
        * --- Nel caso in cui la causale ed il conto di storno split payment
        *     risultano non valorizzati non gestisco la scrittura di storno
        this.w_IMPSPL = IIF(EMPTY(this.oParentObject.w_CONSPL) AND EMPTY(this.oParentObject.w_CAUSPL),0,NVL(IMPSPLIT, 0))
        this.w_ACCEVAS = IIF(this.w_FLAACC="S" AND this.w_FLSCAF<>"S" AND this.w_CLADOC="FA" and this.w_IMPSPL=0 ,"S"," ")
        if EMPTY(this.oParentObject.w_CONSPL) AND EMPTY(this.oParentObject.w_CAUSPL) AND NVL(IMPSPLIT,0)>0
          this.w_MESS_ERR = ah_MsgFormat("Sul documento � presente una rata con flag split payment attivo ma non � stato inserito nessun conto di storno nei parametri vendite")
          this.w_NR = "YY"
          this.w_FLERR = .F.
          this.Page_14()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_DTOBSO = NVL(ANDTOBSO,CP_CHARTODATE("  -  -  "))
        this.w_CONTROPARTITA = NVL(MVCONTRO,SPACE(15))
        * --- Assegno variabili gestione libera
        this.w_PNAGG_01 = MVAGG_01
        this.w_PNAGG_02 = MVAGG_02
        this.w_PNAGG_03 = MVAGG_03
        this.w_PNAGG_04 = MVAGG_04
        this.w_PNAGG_05 = MVAGG_05
        this.w_PNAGG_06 = MVAGG_06
        if Empty(this.w_LCONCAU)
          * --- Se non c'� la contropartita per le cauzioni sul cliente 
          *     utilizzo quella impostata nelle contropartite vendite
          this.w_LCONCAU = this.oParentObject.w_CONCAU
        endif
        if this.w_DTOBSO<=this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DTOBSO)
          this.w_MESS_ERR = ah_MsgFormat("Il conto di contropartita %1 � obsoleto",this.w_CONTROPARTITA)
          this.w_NR = "YY"
          this.w_FLERR = .T.
          this.Page_14()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_REGOK = .F.
          this.w_NUDOC = this.w_NUDOC + 1
        endif
        if Empty(this.w_LCONCAU) And this.w_LIMPCAU<>0
          this.w_MESS_ERR = ah_MsgFormat("Inserire la causale di contropartita per le cauzioni sul cliente o nei parametri vendite.")
          this.w_NR = "YY"
          this.w_FLERR = .T.
          this.Page_14()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_REGOK = .F.
          this.w_NUDOC = this.w_NUDOC + 1
        endif
        this.w_IVAGLO = 0
        this.w_IVADET = 0
        this.w_IVAOMA = 0
        this.w_IVAOMAIND = 0
        this.w_IVAIND = 0
        this.w_TOTVAL = 0
        this.w_TOTCLF = 0
        this.w_TOTPERIVA = 0
        FOR L_i = 1 TO 6
        y = ALLTRIM(STR(L_i))
         
 AI[L_i, 2] = NVL(MVACIVA&y, SPACE(5)) 
 AI[L_i, 3] = NVL(MVAIMPN&y, 0) 
 AI[L_i, 4] = NVL(MVAIMPS&y, 0)
        * --- Rivalorizzo AI[L_i, 1] (se imponibile e iva = 0 per righe zero) ="Z"
        if AI[L_i, 3] = 0 AND AI[L_i, 4]=0
          AI[L_i, 1] = "Z"
        else
           AI[L_i, 1] = NVL(MVAFLOM&y, " ")
        endif
        * --- Legge %Iva e %Indetraibile
        this.w_PERIVA = 0
        this.w_PERIND = 0
        this.w_CODINT = SPACE(5)
        this.w_CODINV = SPACE(5)
        this.w_CODAUT = SPACE(5)
        this.w_FLAINT = " "
        if NOT EMPTY(AI[L_i, 2]) OR this.w_ACQINT="S" OR this.w_ACQAUT="S"
          this.w_APPO = AI[L_i, 2]
          w_WORKAREA=ALIAS()
          this.w_SEARCH_TABLE = "VOCIIVA"
          this.w_SEARCH_IVCODIVA = this.w_APPO
          this.Pag16()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_PERIVA = Nvl(IVPERIVA,0)
          this.w_PERIND = Nvl(IVPERIND,0)
          this.w_CODINT = Nvl(IVCODINT," ")
          this.w_FLAINT = Nvl(IVACQINT," ")
          this.w_REVCHA = Nvl(IVREVCHA," ")
          this.w_CODINV = Nvl(IVCODINV," ")
          this.w_CODAUT = Nvl(IVCODAUV," ")
          if NOT EMPTY(w_WORKAREA)
            Select (w_WORKAREA)
          endif
          * --- Se Doc. INTRA prende il Codice Iva apposito (se esiste)
          if this.w_ACQINT="S" AND NOT EMPTY(this.w_CODINT) AND this.w_FLAINT<>"A"
            AI[L_i, 2] = this.w_CODINT
          endif
          SELECT CONTADOC
        endif
         
 AI[L_i, 5] = this.w_PERIVA 
 AI[L_i, 6] = this.w_PERIND 
 AI[L_i, 7] = this.w_CODINV 
 AI[L_i, 8] = this.w_CODAUT
        * --- Totale Documento (in Valuta)  (Le righe Sconto Merce non vengono memorizzate)
        this.w_TOTVAL = this.w_TOTVAL + (IIF(AI[L_i, 1] $ "IE" , 0, AI[L_i, 3]) + IIF(AI[L_i, 1] = "E", 0, AI[L_i, 4]))
        * --- Se Documento Acquisto INTRA Ricalcola l' IVA
        if this.w_ACQINT="S" OR this.w_ACQAUT="S" and this.w_REVCHA="S"
          * --- Totale Riga Cli/For (no IVA)
          this.w_TOTCLF = this.w_TOTCLF + IIF(AI[L_i, 1] $ "IE", 0, AI[L_i, 3]) 
          if this.w_PNCODVAL<>this.w_PNVALNAZ
            * --- Converte in Moneta di Conto
            AI[L_i, 3] = VAL2MON(AI[L_i, 3],this.w_PNCAOVAL,this.w_CAONAZ,this.w_PNDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
          endif
          * --- Variabili x calcolo Arrotondamenti
          this.w_ARRSUP = IIF(this.w_PNVALNAZ=g_CODLIR, .499, 0)
          AI[L_i, 4] = cp_ROUND((AI[L_i, 3] * this.w_PERIVA / 100)+this.w_ARRSUP, this.w_DECTOP)
          * --- Totale IVA Globale per creare riga Iva vendite sezione  opposta
          this.w_IVAGLO = this.w_IVAGLO + AI[L_i, 4]
        else
          * --- Totale Riga Cli/For
          this.w_TOTCLF = this.w_TOTCLF + (IIF(AI[L_i, 1] $ "IE" , 0, AI[L_i, 3]) + IIF(AI[L_i, 1] = "E", 0, AI[L_i, 4]))
          if this.w_PNCODVAL<>this.w_PNVALNAZ
            * --- Converte in Moneta di Conto
            AI[L_i, 3] = VAL2MON(AI[L_i, 3],this.w_PNCAOVAL,this.w_CAONAZ,this.w_PNDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
            AI[L_i, 4] = VAL2MON(AI[L_i, 4],this.w_PNCAOVAL,this.w_CAONAZ,this.w_PNDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
          endif
        endif
        * --- Totale percentuale iva
        this.w_TOTPERIVA = this.w_TOTPERIVA+this.w_PERIVA
        * --- Calcola la Parte Indetraibile sull'Ammontare dell'Aliquota
        this.w_APPO1 = IIF(AI[L_i, 4]=0 OR AI[L_i, 6]=0, 0, cp_ROUND((AI[L_i, 4] * AI[L_i, 6]) / 100, this.w_DECTOP))
        * --- Totale IVA Indetraibile
        this.w_IVAIND = this.w_IVAIND + this.w_APPO1
        * --- Totale IVA Detraibile
        this.w_IVADET = this.w_IVADET + (AI[L_i, 4] - this.w_APPO1)
        * --- Totale IVA su Omaggi
        this.w_IVAOMA = this.w_IVAOMA + IIF(AI[L_i, 1]="E", AI[L_i, 4], 0)
        * --- Totale IVA Indetraibile Omaggi
        this.w_IVAOMAIND = this.w_IVAOMAIND + IIF(AI[L_i, 1]="E", this.w_APPO1 , 0)
        ENDFOR
        * --- Controllo se causale no iva e iva esente
        if this.w_PNTIPREG="N" AND this.w_TOTPERIVA<>0
          this.w_MESS_ERR = ah_MsgFormat("Causale contabile no IVA e castelletto IVA non completamente esente")
          this.w_NR = "YY"
          this.w_FLERR = .T.
          this.Page_14()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_REGOK = .F.
          this.w_NUDOC = this.w_NUDOC + 1
        else
          this.w_PNTOTDOC = 0
          * --- Totale Documento in Moneta di Conto
          this.w_TOTDOC = this.w_TOTCLF
          if this.w_PNCODVAL<>this.w_PNVALNAZ
            this.w_TOTDOC = VAL2MON(this.w_TOTCLF,this.w_PNCAOVAL,this.w_CAONAZ,this.w_PNDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
          endif
          this.w_PNTOTDOC = this.w_TOTVAL
          * --- Sezione Cli/For (Dare/Avere)
          if this.oParentObject.w_FLVEAC="V"
            this.w_SEZCLF = IIF(this.w_PNTIPDOC<>"NO",IIF(this.w_PNTIPDOC $ "NC-NE-NU", "A", "D"),IIF(this.w_CLADOC $ "NC-NE-NU", "A", "D"))
          else
            this.w_SEZCLF = IIF( this.w_PNTIPDOC<>"NO",IIF(this.w_PNTIPDOC $ "NC-NE-NU", "D", "A"),IIF(this.w_CLADOC $ "NC-NE-NU", "D", "A"))
          endif
        endif
      else
        * --- Segnalo l'errore
        this.w_MESS_ERR = this.w_MESS
        this.w_NR = "YY"
        this.w_FLERR = .T.
        this.Page_14()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_NUDOC = this.w_NUDOC + 1
      endif
    endif
    SELECT CONTADOC
    this.w_OSERIAL = ContaDoc.MvSerial
    if this.w_REGOK
      * --- Scrive nuova Riga sul Temporaneo di Appoggio
      this.w_INICOMD = CP_TODATE(MVINICOM)
      this.w_FINCOMD = CP_TODATE(MVFINCOM)
      this.w_ARCATCON = NVL(MVCATCON, SPACE(3))
      this.w_CODIVA = NVL(MVCODIVA, SPACE(5))
      if Not Empty(this.w_CODIVE)
        w_WORKAREA=ALIAS()
        this.w_SEARCH_TABLE = "VOCIIVA"
        this.w_SEARCH_IVCODIVA = this.w_CODIVA
        this.Pag16()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_LPERIVA = Nvl(IVPERIVA,0)
        if NOT EMPTY(w_WORKAREA)
          Select (w_WORKAREA)
        endif
        * --- Se ho un'esenzione in testata e la riga ha un codice IVA esente allora
        *     mantengo il codice IVA di riga altrimenti metto quello di testata..
        this.w_CODIVA = IIF(this.w_LPERIVA=0, this.w_CODIVA, this.w_CODIVE)
      endif
      if this.w_ACQINT="S" AND NOT EMPTY(this.w_CODIVA) AND this.w_FLAINT<>"A" 
        w_WORKAREA=ALIAS()
        this.w_SEARCH_TABLE = "VOCIIVA"
        this.w_SEARCH_IVCODIVA = this.w_CODIVA
        this.Pag16()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_CODINT = Nvl(IVCODINT," ")
        if NOT EMPTY(w_WORKAREA)
          Select (w_WORKAREA)
        endif
        if Not Empty( this.w_CODINT )
          this.w_CODIVA = this.w_CODINT
        endif
      endif
      * --- Se Acquisti Intra prende l'aliquota specifica
      this.w_CONTRO = NVL(MVCONTRO, SPACE(15))
      this.w_FLMOVC = NVL(FLMOVC, " ")
      this.w_CAUMOV = NVL(CAUMOV, SPACE(5))
      this.w_FLELAN = NVL(FLELAN, " ")
      * --- Bolla Doganale...
      this.w_TDBOLDOG = NVL( TDBOLDOG , " ")
      this.w_CONIND = NVL(MVCONIND, SPACE(15))
      this.w_CCCALDOC = NVL(CCCALDOC," ")
      this.w_FLOMAG = NVL(MVFLOMAG, " ")
      this.w_SCOCL1 = NVL(MVSCOCL1, 0)
      this.w_SCOCL2 = NVL(MVSCOCL2, 0)
      this.w_SCOPAG = NVL(MVSCOPAG, 0)
      this.w_VALMAG = NVL(VALMAG, 0)
      this.w_RIGLOR = NVL(RIGLOR, 0)
      this.w_FLSCOM = NVL(MVFLSCOM, "N")
      if this.w_FLSCOR="S"
        this.w_IMPSC2 = IIF(this.w_FLOMAG$"IE" AND (this.w_FLSCOM="S"), cp_ROUND(this.w_RIGLOR * (1+this.w_SCOCL1/100)*(1+this.w_SCOCL2/100)*(1+this.w_SCOPAG/100), this.w_DECTOT)-this.w_RIGLOR, 0)
      else
        this.w_IMPSC2 = IIF(this.w_FLOMAG$"IE" AND (this.w_FLSCOM="S"), cp_ROUND(this.w_VALMAG * (1+this.w_SCOCL1/100)*(1+this.w_SCOCL2/100)*(1+this.w_SCOPAG/100), this.w_DECTOT)-this.w_VALMAG, 0)
      endif
      this.w_APPO = NVL(VALMAG, 0)
      if this.w_IMPSC2<>0 AND this.w_FLSCOR="S"
        this.w_APPO = this.w_RIGLOR + this.w_IMPSC2
        * --- Se Scorporo a Piede Fattura
        w_WORKAREA=ALIAS()
        this.w_SEARCH_TABLE = "VOCIIVA"
        this.w_SEARCH_IVCODIVA = this.w_CODIVA
        this.Pag16()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_PERIVA = Nvl(IVPERIVA,0)
        if NOT EMPTY(w_WORKAREA)
          Select (w_WORKAREA)
        endif
        this.w_APPO = CALNET(this.w_APPO, this.w_PERIVA, this.w_DECTOT, SPACE(5), 0)
        this.w_VALMAG = this.w_APPO
      else
        this.w_VALMAG = this.w_APPO + this.w_IMPSC2
      endif
      * --- Legge Contropartite Vendite/Acquisti
      this.w_CONVEA = SPACE(15)
      this.w_CONOMA = SPACE(15)
      this.w_CONOMI = SPACE(15)
      this.w_CONNAC = SPACE(15)
      if NOT EMPTY(this.w_ARCATCON) AND NOT EMPTY(this.w_CFCATCON)
        if this.oParentObject.w_FLVEAC="V"
          * --- Contropartite Vendite
          * --- Read from CONTVEAC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTVEAC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2],.t.,this.CONTVEAC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CVCONRIC,CVCONOMV,CVIVAOMA,CVCONNCC"+;
              " from "+i_cTable+" CONTVEAC where ";
                  +"CVCODART = "+cp_ToStrODBC(this.w_ARCATCON);
                  +" and CVCODCLI = "+cp_ToStrODBC(this.w_CFCATCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CVCONRIC,CVCONOMV,CVIVAOMA,CVCONNCC;
              from (i_cTable) where;
                  CVCODART = this.w_ARCATCON;
                  and CVCODCLI = this.w_CFCATCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONVEA = NVL(cp_ToDate(_read_.CVCONRIC),cp_NullValue(_read_.CVCONRIC))
            this.w_CONOMA = NVL(cp_ToDate(_read_.CVCONOMV),cp_NullValue(_read_.CVCONOMV))
            this.w_CONOMI = NVL(cp_ToDate(_read_.CVIVAOMA),cp_NullValue(_read_.CVIVAOMA))
            this.w_CONNAC = NVL(cp_ToDate(_read_.CVCONNCC),cp_NullValue(_read_.CVCONNCC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Contropartite Acquisti
          * --- Read from CONTVEAC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTVEAC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTVEAC_idx,2],.t.,this.CONTVEAC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CVCONACQ,CVCONOMA,CVIVAOMC,CVCONNCF"+;
              " from "+i_cTable+" CONTVEAC where ";
                  +"CVCODART = "+cp_ToStrODBC(this.w_ARCATCON);
                  +" and CVCODCLI = "+cp_ToStrODBC(this.w_CFCATCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CVCONACQ,CVCONOMA,CVIVAOMC,CVCONNCF;
              from (i_cTable) where;
                  CVCODART = this.w_ARCATCON;
                  and CVCODCLI = this.w_CFCATCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONVEA = NVL(cp_ToDate(_read_.CVCONACQ),cp_NullValue(_read_.CVCONACQ))
            this.w_CONOMA = NVL(cp_ToDate(_read_.CVCONOMA),cp_NullValue(_read_.CVCONOMA))
            this.w_CONOMI = NVL(cp_ToDate(_read_.CVIVAOMC),cp_NullValue(_read_.CVIVAOMC))
            this.w_CONNAC = NVL(cp_ToDate(_read_.CVCONNCF),cp_NullValue(_read_.CVCONNCF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        SELECT CONTADOC
      endif
      if this.w_PNTIPREG="N"
        this.w_CONTRO = IIF(EMPTY(this.w_CONTRO), IIF(this.w_CLADOC = "FA", this.w_CONVEA, this.w_CONNAC), this.w_CONTRO)
      else
        this.w_CONTRO = IIF(EMPTY(this.w_CONTRO), IIF(this.w_PNTIPDOC $ "FA-FE-AU-FC", this.w_CONVEA, this.w_CONNAC), this.w_CONTRO)
      endif
      this.w_CONIND = IIF(EMPTY(this.w_CONIND), this.w_CONTRO, this.w_CONIND)
       
 Insert Into DettDocu (FLOMAG, CONTRO, CONOMA, CONOMI, ; 
 CONIND, VALRIG, CODIVA, CATCLF, CATART, INICOM, FINCOM, RIGLOR) ; 
 Values (this.w_FLOMAG, this.w_CONTRO, this.w_CONOMA, this.w_CONOMI, ; 
 this.w_CONIND, this.w_VALMAG, this.w_CODIVA, this.w_CFCATCON, this.w_ARCATCON, ; 
 IIF(empty(this.w_INICOMD),this.w_INICOM,this.w_INICOMD), IIF(empty(this.w_FINCOMD),this.w_FINCOM,this.w_FINCOMD), this.w_RIGLOR)
    endif
    SELECT CONTADOC
    ENDSCAN
    * --- Testa l'Ultima Uscita
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo che tutte le contropartite non siano obsolete alla data fine selezione
    this.w_TIPO = "G"
    if NOT EMPTY(this.oParentObject.w_CONINC) AND this.oParentObject.w_FLVEAC<>"A"
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONINC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONINC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<=this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DATOBSO)
        ah_ErrorMsg("Contropartita spese incasso obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_CONIMB) AND this.oParentObject.w_FLVEAC<>"A"
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONIMB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONIMB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<=this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DATOBSO)
        ah_ErrorMsg("Contropartita spese imballo obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_CONTRA) AND this.oParentObject.w_FLVEAC<>"A"
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONTRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONTRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<=this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DATOBSO)
        ah_ErrorMsg("Contropartita spese trasporto obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if this.oParentObject.w_TIPPAB="XXXXXXXXXX" 
      ah_ErrorMsg("'Pagamento per Abbuoni/Anticipi non definito in Contropartite Parametri'",,"")
      i_retcode = 'stop'
      return
    endif
    if NOT EMPTY(this.oParentObject.w_CONBOL) AND this.oParentObject.w_FLVEAC<>"A"
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONBOL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONBOL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<=this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DATOBSO)
        ah_ErrorMsg("Contropartita spese bolli obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_CONDIC)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONDIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONDIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<=this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DATOBSO)
        ah_ErrorMsg("Contropartita diff. di conversione obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_CONRIT) and this.oParentObject.w_FLVEAC="A" And this.w_TRFLGSTO="S"
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONRIT);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONRIT;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<=this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DATOBSO)
        ah_ErrorMsg("Contropartita erario C/Ritenute obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_CONENA) and this.oParentObject.w_FLVEAC="A" And this.w_TRFLGSTO="S"
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONENA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONENA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<=this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DATOBSO)
        ah_ErrorMsg("Contropartita ENASARCO obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_CAURIT) AND this.oParentObject.w_FLVEAC="A" And this.w_TRFLGSTO="S"
      w_WORKAREA=ALIAS()
      this.w_SEARCH_TABLE = "CAU_CONT"
      this.w_SEARCH_CAU_CONT = this.oParentObject.w_CAURIT
      this.Pag16()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_DATOBSO = Nvl(CCDTOBSO,{})
      if NOT EMPTY(w_WORKAREA)
        Select (w_WORKAREA)
      endif
      if this.w_DATOBSO<=this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DATOBSO)
        ah_ErrorMsg("Causale ritenuta obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_CORIPR) And this.w_TRFLGSTO="S" AND this.oParentObject.w_FLVEAC="A"
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CORIPR);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CORIPR;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<=this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DATOBSO)
        ah_ErrorMsg("Codice rit. previdenziali obsoleto",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Leggo la casuale di riga da utilizzare per le righe reltive ad omaggi
    if this.oParentObject.w_FLVEAC="A"
      * --- Read from CONTROPA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COCAUACO"+;
          " from "+i_cTable+" CONTROPA where ";
              +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COCAUACO;
          from (i_cTable) where;
              COCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAUGIRO = NVL(cp_ToDate(_read_.COCAUACO),cp_NullValue(_read_.COCAUACO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from CONTROPA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COCAUVEO"+;
          " from "+i_cTable+" CONTROPA where ";
              +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COCAUVEO;
          from (i_cTable) where;
              COCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAUGIRO = NVL(cp_ToDate(_read_.COCAUVEO),cp_NullValue(_read_.COCAUVEO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se non e' il Primo Ingresso, Scrive la Nuova Registrazione Contabile, se 
    *     tutto ok (w_REGOK)
    if this.w_OSERIAL <> REPL("#", 10) AND RECCOUNT("DettDocu") > 0 And this.w_REGOK
      * --- Scorpora le Eventuali Spese Ripartite al Lorto
      if (this.oParentObject.w_FLVEAC="V" OR this.oParentObject.w_FLVEAC="A" ) AND this.w_FLSCOR="S"
        if (this.w_SPEINC<>0 AND EMPTY(this.w_IVAINC)) OR (this.w_SPEIMB<>0 AND EMPTY(this.w_IVAIMB)) OR (this.w_SPETRA<>0 AND EMPTY(this.w_IVATRA))
          this.Page_11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      * --- Inizializza Messaggistica di Errore
      this.w_FLERR = .F.
      this.w_AGGIORNA = .F.
      * --- Variabili utilizzate per poter sapere se associato al documento sono
      *     presenti movimenti cespiti
      this.w_INSCESP = .F.
      this.w_NOGENCES = .F.
      this.w_CONCESP = .F.
      * --- Svuoto l'array che conterr� eventuali messaggi di errore
      MES= ""
      * --- Calcola MVSERIAL, MVNUMREG, MVNUMDOC
      this.w_PNSERIAL = SPACE(10)
      this.w_PNNUMRER = 0
      this.w_PNANNDOC = CALPRO(IIF(EMPTY(this.w_PNDATDOC), this.w_PNDATREG, this.w_PNDATDOC), this.w_PNCOMPET, this.w_FLPDOC)
      this.w_PNINICOM = cp_CharToDate("  -  -  ")
      this.w_PNFINCOM = cp_CharToDate("  -  -  ")
      this.w_PNFLABAN = SPACE(6)
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_TOTDAR = 0
      this.w_TOTAVE = 0
      this.w_SCOMER = 1
      this.w_PARFOR = 0
      this.w_NUDOC = this.w_NUDOC + 1
      this.w_OKAN = .T.
      * --- Try
      local bErr_050EE290
      bErr_050EE290=bTrsErr
      this.Try_050EE290()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if Not this.w_FLERR
          * --- Errore non Compreso nei Controlli
          *     Segnalo il messaggio di errore riscontrato
          this.w_FLERR = .T.
          this.w_MESS_ERR = ah_MsgFormat("Anomalia: %1 %2",Message(),Message(1))
          this.Page_14()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_050EE290
      * --- End
    endif
    * --- Azzera il Temporaneo di Appoggio
     
 CREATE CURSOR DettDocu ; 
 (FLOMAG C(1), CONTRO C(15), CONOMA C(15), CONOMI C(15), CONIND C(15), ; 
 VALRIG N(18,4), CODIVA C(5), CATCLF C(5), CATART C(5), INICOM D(8), FINCOM D(8), RIGLOR N(18,4))
    this.w_TOTVAL = 0
    this.w_TOTCLF = 0
    this.w_TOTDAR = 0
    this.w_TOTDOC = 0
    this.w_TOTAVE = 0
  endproc
  proc Try_050EE290()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
    cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
    * --- Controllo Totale documento <>0 solo se sulla causale documento � attivo il Flag
    if this.w_CHKTOT = "S"
      * --- La query esegue una Sum su MVVALRIG delle righe Normali e non Servizi accessori 
      *     del documento che si sta esaminando (w_OSERIAL). Esegue un Having sulla SUM di MVVALRIG = 0
      * --- Select from query\gscg_qd0
      do vq_exec with 'query\gscg_qd0',this,'_Curs_query_gscg_qd0','',.f.,.t.
      if used('_Curs_query_gscg_qd0')
        select _Curs_query_gscg_qd0
        locate for 1=1
        do while not(eof())
        MES[28]=ah_MsgFormat("Righe documento a valore 0. Impossibile contabilizzare")
        this.w_FLERR = .T.
          select _Curs_query_gscg_qd0
          continue
        enddo
        use
      endif
    endif
    if this.oParentObject.w_FLVEAC="V" AND this.w_PNANNPRO<>"    "
      cp_NextTableProg(this, i_Conn, "PRPRO", "i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
    endif
    if EMPTY(this.w_PNCODCLF) OR EMPTY (this.w_PNTIPCLF)
      if this.oParentObject.w_FLVEAC="V"
        MES[1]=ah_MsgFormat("Cliente non definito")
      else
        MES[1]=ah_MsgFormat("Fornitore non definito")
      endif
      this.w_FLERR = .T.
    endif
    * --- Verifico se causale contabile obsoleta
    this.w_DATOBSO = cp_CharToDate("  /  /    ")
    if this.w_PNTIPREG<>"N" AND this.w_PNTIPDOC $ "FA-NC-FC-FE-NE-AU-NU" AND this.w_PNTIPCLF $ "C-F" 
      if this.w_ANNO=0
        this.w_ANNO = Year(this.w_PNDATDOC)
      endif
      if this.w_PN__MESE=0
        this.w_PN__MESE = Month(this.w_PNDATDOC)
      endif
    endif
    w_WORKAREA=ALIAS()
    this.w_SEARCH_TABLE = "CAU_CONT"
    this.w_SEARCH_CAU_CONT = this.w_PNCODCAU
    this.Pag16()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CCDESSUP = Nvl(CCDESSUP," ")
    this.w_CCDESRIG = Nvl(CCDESRIG," ")
    this.w_FDAVE = Nvl(CCCFDAVE," ")
    if NOT EMPTY(w_WORKAREA)
      Select (w_WORKAREA)
    endif
     
 DIMENSION ARPARAM[13,2] 
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_PNNUMDOC,15)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.w_PNALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_PNDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]=this.w_PNALFPRO 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]=ALLTRIM(this.w_PNCODCLF) 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]=ALLTRIM(STR(this.w_PNNUMPRO,15)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=this.w_PNTIPCLF 
 ARPARAM[13,1]="NUMFAT" 
 ARPARAM[13,2]=this.w_NUMFAT
    if Empty(this.w_PNDESSUP) and Not Empty(this.w_CCDESSUP)
      * --- Array elenco parametri per descrizioni di riga e testata
      this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
    endif
    this.w_APPO = ALLTRIM(STR(this.w_PNNUMRER))+"Del.:"+DTOC(this.w_PNDATREG)
    ah_Msg("Scrivo reg. n.: %1 del %2",.T.,.F.,.F.,ALLTRIM(STR(this.w_PNNUMRER)), DTOC(this.w_PNDATREG) )
    * --- Scrive la Testata
    if g_RITE="S"
      * --- Effettuo una lettura sulla tabella Tab_Rite per verificare se � stato settato oppure
      *     no il flag Storno immediato.
      this.w_TRFLGSTO = LOOKTAB("TAB_RITE",IIF(this.w_PNTIPCLF="F","TRFLGSTO","TRFLGST1"),"TRCODAZI",this.w_CODAZI,"TRTIPRIT",IIF(this.w_PNTIPCLF="F","A","V"))
      this.w_MODRITPA = Nvl ( LOOKTAB("TAB_RITE",IIF(this.w_PNTIPCLF="F","TRMODPAG","TRMODPA1"),"TRCODAZI",this.w_CODAZI,"TRTIPRIT",IIF(this.w_PNTIPCLF="F","A","V")) ,Space(10))
    else
      this.w_TRFLGSTO = "S"
    endif
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",PNCODUTE"+",PNNUMRER"+",PNCODESE"+",PNCODCAU"+",PNCOMPET"+",PNDATREG"+",PNFLIVDF"+",PNNUMREG"+",PNTIPREG"+",PNPRD"+",PNALFDOC"+",PNTIPDOC"+",PNDATDOC"+",PNALFPRO"+",PNNUMDOC"+",PNVALNAZ"+",PNCODVAL"+",PNNUMPRO"+",PNCAOVAL"+",PNDESSUP"+",PNPRG"+",PNCODCLF"+",PNTOTDOC"+",PNTIPCLF"+",PNFLREGI"+",PNFLPROV"+",PNCOMIVA"+",PNDATPLA"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+",PNRIFDOC"+",PNANNDOC"+",PNANNPRO"+",PNPRP"+",PNFLGSTO"+",PNTOTENA"+",PNNUMTRA"+",PNNUMTR2"+",PN__ANNO"+",PN__MESE"+",PNAGG_01"+",PNAGG_02"+",PNAGG_03"+",PNAGG_04"+",PNAGG_05"+",PNAGG_06"+",PNNUMFAT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLIVDF),'PNT_MAST','PNFLIVDF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_MAST','PNNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRD),'PNT_MAST','PNPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PNT_MAST','PNALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPDOC),'PNT_MAST','PNTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PNT_MAST','PNDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFPRO),'PNT_MAST','PNALFPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PNT_MAST','PNNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMPRO),'PNT_MAST','PNNUMPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PNT_MAST','PNCODCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PNT_MAST','PNTOTDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PNT_MAST','PNTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLREGI),'PNT_MAST','PNFLREGI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPROV),'PNT_MAST','PNFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMIVA),'PNT_MAST','PNCOMIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATPLA),'PNT_MAST','PNDATPLA');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PNT_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PNT_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'PNT_MAST','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'PNT_MAST','PNRIFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNDOC),'PNT_MAST','PNANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNPRO),'PNT_MAST','PNANNPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRP),'PNT_MAST','PNPRP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRFLGSTO),'PNT_MAST','PNFLGSTO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALENA),'PNT_MAST','PNTOTENA');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','PNNUMTRA');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','PNNUMTR2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANNO),'PNT_MAST','PN__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PN__MESE),'PNT_MAST','PN__MESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNAGG_01),'PNT_MAST','PNAGG_01');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNAGG_02),'PNT_MAST','PNAGG_02');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNAGG_03),'PNT_MAST','PNAGG_03');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNAGG_04),'PNT_MAST','PNAGG_04');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNAGG_05),'PNT_MAST','PNAGG_05');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNAGG_06),'PNT_MAST','PNAGG_06');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMFAT),'PNT_MAST','PNNUMFAT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNCODUTE',this.w_PNCODUTE,'PNNUMRER',this.w_PNNUMRER,'PNCODESE',this.w_PNCODESE,'PNCODCAU',this.w_PNCODCAU,'PNCOMPET',this.w_PNCOMPET,'PNDATREG',this.w_PNDATREG,'PNFLIVDF',this.w_PNFLIVDF,'PNNUMREG',this.w_PNNUMREG,'PNTIPREG',this.w_PNTIPREG,'PNPRD',this.w_PNPRD,'PNALFDOC',this.w_PNALFDOC)
      insert into (i_cTable) (PNSERIAL,PNCODUTE,PNNUMRER,PNCODESE,PNCODCAU,PNCOMPET,PNDATREG,PNFLIVDF,PNNUMREG,PNTIPREG,PNPRD,PNALFDOC,PNTIPDOC,PNDATDOC,PNALFPRO,PNNUMDOC,PNVALNAZ,PNCODVAL,PNNUMPRO,PNCAOVAL,PNDESSUP,PNPRG,PNCODCLF,PNTOTDOC,PNTIPCLF,PNFLREGI,PNFLPROV,PNCOMIVA,PNDATPLA,UTCC,UTDC,UTCV,UTDV,PNRIFDOC,PNANNDOC,PNANNPRO,PNPRP,PNFLGSTO,PNTOTENA,PNNUMTRA,PNNUMTR2,PN__ANNO,PN__MESE,PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05,PNAGG_06,PNNUMFAT &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_PNCODUTE;
           ,this.w_PNNUMRER;
           ,this.w_PNCODESE;
           ,this.w_PNCODCAU;
           ,this.w_PNCOMPET;
           ,this.w_PNDATREG;
           ,this.w_PNFLIVDF;
           ,this.w_PNNUMREG;
           ,this.w_PNTIPREG;
           ,this.w_PNPRD;
           ,this.w_PNALFDOC;
           ,this.w_PNTIPDOC;
           ,this.w_PNDATDOC;
           ,this.w_PNALFPRO;
           ,this.w_PNNUMDOC;
           ,this.w_PNVALNAZ;
           ,this.w_PNCODVAL;
           ,this.w_PNNUMPRO;
           ,this.w_PNCAOVAL;
           ,this.w_PNDESSUP;
           ,this.w_PNPRG;
           ,this.w_PNCODCLF;
           ,this.w_PNTOTDOC;
           ,this.w_PNTIPCLF;
           ,this.w_PNFLREGI;
           ,this.w_PNFLPROV;
           ,this.w_PNCOMIVA;
           ,this.w_PNDATPLA;
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           ,this.w_OSERIAL;
           ,this.w_PNANNDOC;
           ,this.w_PNANNPRO;
           ,this.w_PNPRP;
           ,this.w_TRFLGSTO;
           ,this.w_VALENA;
           ,0;
           ,0;
           ,this.w_ANNO;
           ,this.w_PN__MESE;
           ,this.w_PNAGG_01;
           ,this.w_PNAGG_02;
           ,this.w_PNAGG_03;
           ,this.w_PNAGG_04;
           ,this.w_PNAGG_05;
           ,this.w_PNAGG_06;
           ,this.w_NUMFAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Reset delle variabili utilizzate per il warning sul numero di righe massimo da inviare ad Apri
    this.w_ROWCOUNT = 0
    this.w_WARNMSG = space(10)
    * --- Inserisco i dati ritenute se ho attivato il modulo ritenute, se la causale contabile
    *     associata ha il flag attivo di gestione delle ritenute,
    if this.w_GESRIT="S" and g_RITE="S"
      * --- Insert into DATIRITE
      i_nConn=i_TableProp[this.DATIRITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DATIRITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsve_qcd_n",this.DATIRITE_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- Scrive il Dettaglio: Riga Cliente/Fornitore
    this.w_PNTIPCON = this.w_PNTIPCLF
    this.w_PNCODCON = this.w_PNCODCLF
    this.w_APPVAL = this.w_TOTDOC
    this.w_APPSEZ = this.w_SEZCLF
    this.w_PNCODPAG = this.w_CODPAG
    this.w_PNCODAGE = this.w_CODAGE
    this.w_PNFLVABD = this.w_FLVABD
    this.w_FLVAL = "N"
    this.w_PNINICOM = cp_CharToDate("  -  -  ")
    this.w_PNFINCOM = cp_CharToDate("  -  -  ")
    this.w_PNIMPIND = 0
    this.w_GESTPAR = "INTESTA"
    this.w_CCCONACQ = " "
    if g_CESP="S" 
      this.Page_15()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_GESTPAR = ""
    this.w_PNCODAGE = SPACE(5)
    this.w_PNCODPAG = SPACE(5)
    this.w_PNFLVABD = " "
    this.w_PNTIPCON = "G"
    this.w_PNIMPIND = 0
    * --- Scrive il Dettaglio: Conto IVA Detraibile
    *     Se esiste iva Detraibile, creo una riga nel
    *     castelletto contabile utilizzando il conto impostato nella causale contabile
    if this.w_IVADET<>0 
      if EMPTY(this.w_CCCONIVA)
        MES[2]=ah_MsgFormat("Conto IVA detraibile non definito")
        this.w_FLERR = .T.
      else
        this.w_IVAREV_DA_TOG = 0
        if (this.w_ACQINT="S" OR this.w_ACQAUT="S") AND this.w_IVAGLO<>0
          * --- Read from CAUIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAUIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA_idx,2],.t.,this.CAUIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AICONDET,AINUMREG"+;
              " from "+i_cTable+" CAUIVA where ";
                  +"AICODCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                  +" and AITIPCLF = "+cp_ToStrODBC(this.w_PNTIPCLF);
                  +" and AICODCLF = "+cp_ToStrODBC(this.w_PNCODCLF);
                  +" and AITIPREG = "+cp_ToStrODBC("A");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AICONDET,AINUMREG;
              from (i_cTable) where;
                  AICODCAU = this.w_PNCODCAU;
                  and AITIPCLF = this.w_PNTIPCLF;
                  and AICODCLF = this.w_PNCODCLF;
                  and AITIPREG = "A";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CCCONACQ = NVL(cp_ToDate(_read_.AICONDET),cp_NullValue(_read_.AICONDET))
            this.w_AINUMREG = NVL(cp_ToDate(_read_.AINUMREG),cp_NullValue(_read_.AINUMREG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows=0 OR EMPTY(this.w_CCCONACQ)
            * --- Read from CAUIVA1
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAUIVA1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2],.t.,this.CAUIVA1_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AICONDET,AINUMREG"+;
                " from "+i_cTable+" CAUIVA1 where ";
                    +"AICODCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                    +" and AITIPREG = "+cp_ToStrODBC("A");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AICONDET,AINUMREG;
                from (i_cTable) where;
                    AICODCAU = this.w_PNCODCAU;
                    and AITIPREG = "A";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CCCONACQ = NVL(cp_ToDate(_read_.AICONDET),cp_NullValue(_read_.AICONDET))
              this.w_AINUMREG = NVL(cp_ToDate(_read_.AINUMREG),cp_NullValue(_read_.AINUMREG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if NOT EMPTY(NVL(this.w_CCCONACQ,"")) AND this.w_CCCONACQ<>this.w_CCCONIVA 
            this.w_IVAREV_DA_TOG = this.w_IVAGLO
          endif
        endif
        this.w_PNCODCON = this.w_CCCONIVA
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "N"
        this.w_PNINICOM = cp_CharToDate("  -  -  ")
        this.w_PNFINCOM = cp_CharToDate("  -  -  ")
        this.w_PNCODPAG = IIF(this.w_PNFLIVDF="S", g_SAPAGA, SPACE(5))
        this.w_PNCODAGE = this.w_CODAGE
        * --- Se esigibilit� differita, costruisco le partite.
        if this.w_PNFLIVDF="S"
          this.w_GESTPAR = "ESDIF"
        endif
        * --- Se ho omaggi duplico il conto IVA. Sulla causale di testata
        *     la parte non omaggio (escludo anche omaggio di indetraibile) , mentre la parte omaggio la creo con causale
        *     di riga omaggio
        if this.w_IVAOMA=0 Or ( this.w_IVAOMA - this.w_IVAOMAIND )=0 Or Empty( this.w_CAUGIRO )
          this.w_APPVAL = this.w_IVADET-this.w_IVAREV_DA_TOG
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_APPVAL = this.w_IVADET - ( this.w_IVAOMA - this.w_IVAOMAIND ) - this.w_IVAREV_DA_TOG
          * --- Se tutto omaggio...
          if this.w_APPVAL<>0
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_APPVAL = this.w_IVAOMA - this.w_IVAOMAIND
          this.w_OPNCODCAU = this.w_PNCODCAU
          this.w_PNCODCAU = this.w_CAUGIRO
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_PNCODCAU = this.w_OPNCODCAU
        endif
        this.w_GESTPAR = ""
      endif
    endif
    * --- Scrive il Dettaglio: Conti IVA Indetraibile
    this.w_PNCODPAG = SPACE(5)
    this.w_PNCODAGE = SPACE(5)
    this.w_PNFLVABD = " "
    this.w_PNIMPIND = 0
    this.w_FLIND = .F.
    * --- Scrive il Dettaglio: Conto IVA Indetraibile.
    *     Raggruppo tutte le righe (escluse sconto merce) per la loro contropartita
    *     indetraibile e la loro controapartita...
    *     Da qui determino la parte indetraibile per creare la riga sul castelletto contabile
    if this.w_IVAIND<>0
      if this.w_TDBOLDOG="S"
         
 Select CONIND, CODIVA, SUM(VALRIG), CATCLF, CATART, CONTRO,FLOMAG,INICOM, FINCOM FROM DettDocu ; 
 WHERE FLOMAG = "X" AND NVL(VALRIG,0)<>0 INTO ARRAY AppArr GROUP BY CONIND, CONTRO, CODIVA,INICOM, FINCOM
      else
        if Empty( this.w_CAUGIRO )
           
 Select CONIND, CODIVA, SUM(VALRIG), CATCLF, CATART, CONTRO,FLOMAG,INICOM, FINCOM FROM DettDocu ; 
 WHERE FLOMAG $ "XIE" AND NVL(VALRIG,0)<>0 INTO ARRAY AppArr GROUP BY CONIND, CONTRO, CODIVA, INICOM, FINCOM
        else
          * --- Divido le righe per contropartita e per tipo (Omaggio).
          *     Questo per ottenere una girocontazione precisa tra
          *     conto ed omaggio (le righe ad omaggio hanno una casuale differente )
           
 Select CONIND, CODIVA, SUM(VALRIG), CATCLF, CATART, CONTRO,FLOMAG,INICOM, FINCOM FROM DettDocu ; 
 WHERE FLOMAG $ "XI" AND NVL(VALRIG,0)<>0 GROUP BY CONIND, CONTRO, CODIVA,INICOM, FINCOM,FLOMAG ; 
 Union All ; 
 Select CONIND, CODIVA, SUM(VALRIG), CATCLF, CATART, CONTRO,FLOMAG,INICOM, FINCOM FROM DettDocu ; 
 WHERE FLOMAG = "E" AND NVL(VALRIG,0)<>0 GROUP BY CONIND, CONTRO, CODIVA, INICOM, FINCOM,FLOMAG ; 
 INTO ARRAY AppArr
        endif
      endif
      this.w_APPO1 = this.w_IVAIND
      this.w_LOOP = 1
      do while this.w_LOOP<= ALEN(AppArr, 1)
        if this.w_PNCODVAL<>this.w_PNVALNAZ
          * --- Converte in Moneta di Conto
          AppArr[ this.w_LOOP , 3] = VAL2MON(AppArr[this.w_LOOP, 3],this.w_PNCAOVAL,this.w_CAONAZ,this.w_PNDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
        endif
        this.w_APPO2 = 0
        this.w_BLOOP = 1
        * --- Prende la Parte Indetraibile
        do while this.w_BLOOP<=6
          * --- Array dettaglio IVA, ricerco il solito codice IVA.e medesimo tipo se omaggio imposta
          *     ed attiva la doppia casuale
          if AI[ this.w_BLOOP , 2] = AppArr[this.w_LOOP, 2] And ( AI[ this.w_BLOOP , 1]=AppArr[this.w_LOOP, 7] Or AppArr[this.w_LOOP, 7] <>"E" Or Empty( this.w_CAUGIRO ))
            * --- Determino la parte indetraibile...
            if Not ( AppArr[ this.w_LOOP , 3]=0 OR AI[ this.w_BLOOP , 5]=0 OR AI[ this.w_BLOOP , 6]=0 )
              this.w_APPO2 = cp_Round(((AppArr[this.w_LOOP, 3] * AI[ this.w_BLOOP , 5]) / 100) , this.w_DECTOP)
              this.w_APPO2 = cp_ROUND((this.w_APPO2 * AI[ this.w_BLOOP , 6]) / 100, this.w_DECTOP)
            endif
            * --- Esco dal ciclo While...
            this.w_BLOOP = 7
          else
            this.w_BLOOP = this.w_BLOOP + 1
          endif
        enddo
        * --- Scrive Importo Indetraibile sul Vettore
        AppArr[ this.w_LOOP , 3] = this.w_APPO2
        this.w_APPO1 = this.w_APPO1 - this.w_APPO2
        this.w_LOOP = this.w_LOOP + 1
      enddo
      if ALEN(AppArr, 1)>0
        * --- Mette l'eventuale Resto sulla Prima Riga
        AppArr[1, 3] = AppArr[1, 3] + this.w_APPO1
      endif
      * --- Alla Fine Scrive le Righe P.N.
      this.w_APPVAL = 0
      this.w_LOOP = 1
      do while this.w_LOOP<= ALEN(AppArr, 1)
        if AppArr[ this.w_LOOP , 1] = AppArr[ this.w_LOOP , 6]
          * --- Se Il Conto IVA Indetraibile=Conto Contropartita butta tutto su quest'ultima
          this.w_FLIND = .T.
        else
          this.w_PNCODCON = AppArr[ this.w_LOOP , 1]
          if EMPTY(this.w_PNCODCON)
            MES[3]=ah_MsgFormat("Conto IVA indetraibile non definito (cat.CF: %1 cat.art: %2)",AppArr[this.w_LOOP,4],AppArr[this.w_LOOP,5])
            this.w_FLERR = .T.
          endif
          * --- Somma gli Importi delle Aliquote IVA
          this.w_APPVAL = this.w_APPVAL + AppArr[ this.w_LOOP , 3]
          this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
          this.w_FLVAL = "N"
          this.w_PNINICOM = cp_CharToDate("  -  -  ")
          this.w_PNFINCOM = cp_CharToDate("  -  -  ")
          if this.w_LOOP= ALEN(AppArr, 1)
            * --- Se siamo in Fondo Scrive
            this.w_OPNCODCAU = this.w_PNCODCAU
            * --- Se riga omaggio utilizzo la cauale contabile omaggi
            this.w_PNCODCAU = IIF( Empty( this.w_CAUGIRO ) Or AppArr[this.w_LOOP, 7]<>"E", this.w_PNCODCAU , this.w_CAUGIRO )
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_PNCODCAU = this.w_OPNCODCAU
          else
            if AppArr[ this.w_LOOP +1, 1] <> this.w_PNCODCON OR AppArr[ this.w_LOOP +1, 1] = AppArr[ this.w_LOOP+1 , 6]
              * --- ...Oppure Scrive se il Successivo Conto e' diverso
              this.w_OPNCODCAU = this.w_PNCODCAU
              this.w_PNCODCAU = IIF( Empty( this.w_CAUGIRO ) Or AppArr[this.w_LOOP, 7]<>"E" , this.w_PNCODCAU, this.w_CAUGIRO )
              this.Page_5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PNCODCAU = this.w_OPNCODCAU
              this.w_APPVAL = 0
            endif
          endif
        endif
        this.w_LOOP = this.w_LOOP + 1
      enddo
    endif
    this.w_PNIMPIND = 0
    * --- Se Documento Acquisto INTRA Scrive riga Conto IVA su Sezione Opposta
    if (this.w_ACQINT="S" OR this.w_ACQAUT="S") AND this.w_IVAGLO<>0
      * --- Read from CAUIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAUIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA_idx,2],.t.,this.CAUIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AICONDET,AINUMREG"+;
          " from "+i_cTable+" CAUIVA where ";
              +"AICODCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
              +" and AITIPCLF = "+cp_ToStrODBC(this.w_PNTIPCLF);
              +" and AICODCLF = "+cp_ToStrODBC(this.w_PNCODCLF);
              +" and AITIPREG = "+cp_ToStrODBC("V");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AICONDET,AINUMREG;
          from (i_cTable) where;
              AICODCAU = this.w_PNCODCAU;
              and AITIPCLF = this.w_PNTIPCLF;
              and AICODCLF = this.w_PNCODCLF;
              and AITIPREG = "V";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CCCONINT = NVL(cp_ToDate(_read_.AICONDET),cp_NullValue(_read_.AICONDET))
        this.w_AINUMREG = NVL(cp_ToDate(_read_.AINUMREG),cp_NullValue(_read_.AINUMREG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows=0 OR EMPTY(this.w_CCCONINT)
        * --- Read from CAUIVA1
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAUIVA1_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2],.t.,this.CAUIVA1_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AICONDET,AINUMREG"+;
            " from "+i_cTable+" CAUIVA1 where ";
                +"AICODCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                +" and AITIPREG = "+cp_ToStrODBC("V");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AICONDET,AINUMREG;
            from (i_cTable) where;
                AICODCAU = this.w_PNCODCAU;
                and AITIPREG = "V";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CCCONINT = NVL(cp_ToDate(_read_.AICONDET),cp_NullValue(_read_.AICONDET))
          this.w_AINUMREG = NVL(cp_ToDate(_read_.AINUMREG),cp_NullValue(_read_.AINUMREG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if i_Rows=0 OR EMPTY(this.w_CCCONINT)
        MES[13]=ah_MsgFormat("Conto IVA vendite per acquisti INTRA o autofattura non definito")
        this.w_FLERR = .T.
      else
        this.w_PNCODCON = this.w_CCCONINT
        this.w_APPVAL = this.w_IVAGLO
        this.w_APPSEZ = this.w_SEZCLF
        this.w_FLVAL = "N"
        this.w_PNINICOM = cp_CharToDate("  -  -  ")
        this.w_PNFINCOM = cp_CharToDate("  -  -  ")
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- DA MODIFICARE
      if NOT EMPTY(NVL(this.w_CCCONACQ,"")) AND this.w_CCCONACQ<>this.w_CCCONIVA 
        this.w_PNCODCON = this.w_CCCONACQ
        this.w_APPVAL = this.w_IVAGLO
        this.w_APPSEZ = IIF(this.w_SEZCLF="D","A","D")
        this.w_FLVAL = "N"
        this.w_PNINICOM = cp_CharToDate("  -  -  ")
        this.w_PNFINCOM = cp_CharToDate("  -  -  ")
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Scrive il Dettaglio: Riga Contropartite
    if this.w_TDBOLDOG="S"
       
 Select CONTRO, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, FINCOM,FLOMAG FROM DettDocu ; 
 WHERE FLOMAG $ "X" AND VALRIG<>0 INTO CURSOR AppCur GROUP BY CONTRO, INICOM, FINCOM
    else
      if Empty( this.w_CAUGIRO )
         
 Select CONTRO, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, FINCOM,FLOMAG FROM DettDocu ; 
 WHERE FLOMAG $ "XIE" AND VALRIG<>0 GROUP BY CONTRO, INICOM, FINCOM,FLOMAG ; 
 INTO CURSOR AppCur NoFilter
      else
        * --- Divido le righe per contropartita e per tipo (Omaggio).
        *     Questo per ottenere una girocontazione precisa tra
        *     conto ed omaggio (le righe ad omaggio hanno una casuale differente )
         
 Select CONTRO, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, FINCOM,"X" As FLOMAG FROM DettDocu ; 
 WHERE FLOMAG = "X" AND VALRIG<>0 GROUP BY CONTRO, INICOM, FINCOM,FLOMAG ; 
 Union All ; 
 Select CONTRO, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, FINCOM,"O" As FLOMAG FROM DettDocu ; 
 WHERE FLOMAG $ "IE" AND VALRIG<>0 GROUP BY CONTRO, INICOM, FINCOM ; 
 INTO CURSOR AppCur NoFilter
      endif
    endif
     
 Select CATCLF, CATART FROM DettDocu ; 
 WHERE EMPTY(CONTRO) INTO CURSOR LOG GROUP BY CATCLF, CATART
     
 Select LOG 
 Go Top 
 Scan
    this.w_MESS_ERR = ah_MsgFormat("Conto contropartita non definito (cat.CF: %1 cat.art: %2)",CATCLF,CATART)
    this.w_FLERR = .T.
    this.Page_14()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    EndScan
    this.w_OPNCODCAU = this.w_PNCODCAU
     
 Select AppCur 
 Go Top 
 Scan For NVL(VALAPP,0)<>0 OR NOT EMPTY(NVL(CONTRO," "))
    this.w_SCOMER = 0
    if !EMPTY(CONTRO)
      do case
        case AppCur.Flomag="O" 
          * --- Omaggio
          this.w_PNCODCAU = IIF( Empty( this.w_CAUGIRO ) , this.w_PNCODCAU , this.w_CAUGIRO )
        case AI[1,1]="Z" and LMAVANZATO()
          * --- Iva a zero
          * --- Con  il modulo 'Trasferimento studio' installato/abilitato,   utilizzo come causale di riga la causale di giroconto impostata in 'Tabella trasferimento studio'
          * --- Select from QUERY\CAUGIR
          do vq_exec with 'QUERY\CAUGIR',this,'_Curs_QUERY_CAUGIR','',.f.,.t.
          if used('_Curs_QUERY_CAUGIR')
            select _Curs_QUERY_CAUGIR
            locate for 1=1
            do while not(eof())
            this.w_PNCODCAU = NVL(_Curs_QUERY_CAUGIR.LMCAUGIR," ")
              select _Curs_QUERY_CAUGIR
              continue
            enddo
            use
          endif
           
 Select AppCur
          if EMPTY(this.w_PNCODCAU)
            this.w_PNCODCAU = this.w_OPNCODCAU
          endif
        otherwise
          this.w_PNCODCAU = this.w_OPNCODCAU
      endcase
      this.w_APPCURFLOMAG = AppCur.Flomag
      this.w_PNCODCON = CONTRO
      this.w_APPVAL = VALAPP
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_FLVAL = "S"
      this.w_PNINICOM = IIF(empty(INICOM),this.w_INICOM,INICOM)
      this.w_PNFINCOM = IIF(empty(FINCOM),this.w_FINCOM,FINCOM)
      this.w_PNIMPIND = 0
      if this.w_FLIND
        this.w_FLVAL = "N"
        * --- Deve riportare tutto alla Valuta di PNIMPIND
        if this.w_PNCODVAL<>this.w_PNVALNAZ
          * --- Se arriva un Importo in Valuta e il Documento e' in Valuta Converte in Moneta di Conto
          this.w_APPVAL = VAL2MON(this.w_APPVAL,this.w_PNCAOVAL,this.w_CAONAZ,this.w_PNDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
        endif
        this.w_LOOP = 1
        do while this.w_LOOP <= ALEN(AppArr, 1)
          if AppArr[ this.w_LOOP , 1] = AppArr[ this.w_LOOP , 6] AND AppArr[ this.w_LOOP , 1] = this.w_PNCODCON AND AppArr[ this.w_LOOP , 8] = this.w_PNINICOM AND AppArr[ this.w_LOOP , 9] = this.w_PNFINCOM AND iif(AppArr[ this.w_LOOP , 7] $ "IE","O",AppArr[ this.w_LOOP , 7])= this.w_APPCURFLOMAG
            * --- Se Il Conto IVA Indetraibile=Conto Contropartita butta tutto su quest'ultima
            * --- Somma gli Importi delle Aliquote IVA
            this.w_PNIMPIND = this.w_PNIMPIND + AppArr[ this.w_LOOP , 3]
          endif
          this.w_LOOP = this.w_LOOP + 1
        enddo
        this.w_APPVAL = this.w_APPVAL + this.w_PNIMPIND
        * --- L'importo Indetraibile e' sempre in valore assoluto
        this.w_PNIMPIND = ABS(this.w_PNIMPIND)
      endif
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT AppCur
    endif
    EndScan
    this.w_PNCODCAU = this.w_OPNCODCAU
    this.w_PNIMPIND = 0
    if this.w_LIMPCAU<>0 And Not Empty(this.w_LCONCAU)
      this.w_PNCODCON = this.w_LCONCAU
      this.w_PNTIPCON = "G"
      this.w_APPVAL = ABS(this.w_LIMPCAU)
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      if this.w_LIMPCAU<0
        * --- Se la cauzione � negativa cambio sezione
        this.w_APPSEZ = IIF(this.w_APPSEZ="D", "A", "D")
      endif
      this.w_FLVAL = "N"
      this.w_PNINICOM = cp_CharToDate("  -  -  ")
      this.w_PNFINCOM = cp_CharToDate("  -  -  ")
      this.w_GESTPAR = " "
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Riassegno nel caso in cui le contropartite gestiscano le partite per
    *     creare le partite dell'intestatario non create immediatamente
    this.w_PNFLPART = "N"
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_TDBOLDOG<>"S"
      * --- Verifico se ho tra le partite dell'intestatario, pagamenti di tipo compensazione
      *     Se non bolla doganale
      this.w_TOTCOMP = 0
      * --- Select from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PTMODPAG,PTTOTIMP  from "+i_cTable+" PAR_TITE ";
            +" where PTSERIAL="+cp_ToStrODBC(this.w_PNSERIAL)+" And PTROWORD=1";
             ,"_Curs_PAR_TITE")
      else
        select PTMODPAG,PTTOTIMP from (i_cTable);
         where PTSERIAL=this.w_PNSERIAL And PTROWORD=1;
          into cursor _Curs_PAR_TITE
      endif
      if used('_Curs_PAR_TITE')
        select _Curs_PAR_TITE
        locate for 1=1
        do while not(eof())
        * --- Read from MOD_PAGA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2],.t.,this.MOD_PAGA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MPTIPPAG"+;
            " from "+i_cTable+" MOD_PAGA where ";
                +"MPCODICE = "+cp_ToStrODBC(_Curs_PAR_TITE.PTMODPAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MPTIPPAG;
            from (i_cTable) where;
                MPCODICE = _Curs_PAR_TITE.PTMODPAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPPAG = NVL(cp_ToDate(_read_.MPTIPPAG),cp_NullValue(_read_.MPTIPPAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_TIPPAG="CC"
          this.w_TOTCOMP = this.w_TOTCOMP + _Curs_PAR_TITE.PTTOTIMP
        endif
          select _Curs_PAR_TITE
          continue
        enddo
        use
      endif
      if this.w_TOTCOMP<>0
        * --- Aggiungo Righe Pagamento di Compensazione
        this.w_PNTIPCON = this.w_PNTIPCLF
        this.w_PNCODCON = this.w_PNCODCLF
        this.w_PNCODPAG = this.w_CODPAG
        this.w_PNCODAGE = this.w_CODAGE
        this.w_PNFLVABD = this.w_FLVABD
        this.w_PNINICOM = cp_CharToDate("  -  -  ")
        this.w_PNFINCOM = cp_CharToDate("  -  -  ")
        this.w_APPVAL = this.w_TOTCOMP
        this.w_APPSEZ = IIF(this.w_FDAVE="D","A","D")
        this.w_FLVAL = "S"
        this.w_GESTPAR = "COMPENSA"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_GESTPAR = ""
        * --- Se sulle scadenze o sul documento non ho specificato la Ns banca
        *     lo segnalo e abortisco la reg. di prima nota
        if EMPTY(this.w_NUMCOR)
          MES[16]=ah_MsgFormat("Ns banca non definita nel documento")
          this.w_FLERR = .T.
        else
          this.w_PNTIPCON = "G"
          this.w_PNCODCON = this.w_NUMCOR
          this.w_PNINICOM = cp_CharToDate("  -  -  ")
          this.w_PNFINCOM = cp_CharToDate("  -  -  ")
          this.w_PNCODAGE = SPACE(5)
          this.w_PNFLVABD = " "
          this.w_APPVAL = this.w_TOTCOMP
          this.w_APPSEZ = this.w_FDAVE
          this.w_FLVAL = "S"
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Genero Relativi Movimenti C\C
          if g_BANC="S" AND this.w_FLMOVC="S"
            if EMPTY(this.w_CAUMOV)
              MES[17]=ah_MsgFormat("Causale movimenti C\C non definita nella causale contabile")
              this.w_FLERR = .T.
            else
              * --- Select from gscg_bmc
              do vq_exec with 'gscg_bmc',this,'_Curs_gscg_bmc','',.f.,.t.
              if used('_Curs_gscg_bmc')
                select _Curs_gscg_bmc
                locate for 1=1
                do while not(eof())
                this.w_TIPCAU = _Curs_gscg_bmc.CATIPCON
                this.w_FLCRDE = _Curs_gscg_bmc.CAFLCRDE
                  select _Curs_gscg_bmc
                  continue
                enddo
                use
              endif
              this.w_CONBAN = this.w_PTBANNOS
              if this.w_TIPCAU<>"S"
                MES[18]=ah_MsgFormat("Causale movimenti corrente specificata nella causale contabile non di tipo compensazione")
                this.w_FLERR = .T.
              else
                if this.w_TIPBAN<>"S"
                  MES[19]=ah_MsgFormat("Conto corrente non di tipo compensazione")
                  this.w_FLERR = .T.
                else
                  if this.w_CODVAL<>this.w_PNCODVAL
                    MES[19]=ah_MsgFormat("Valuta del conto corrente incongruente con la valuta del documento")
                    this.w_FLERR = .T.
                  else
                    this.w_APPVAL = this.w_TOTCOMP
                    this.w_IMPDEB = IIF(this.oParentObject.w_FLVEAC="V",0,this.w_APPVAL)
                    this.w_IMPCRE = IIF(this.oParentObject.w_FLVEAC="V",this.w_APPVAL,0)
                    this.w_CCFLCRED = IIF(this.w_FLCRDE="C", "+", " ")
                    this.w_CCFLDEBI = IIF(this.w_FLCRDE="D", "+", " ")
                    this.w_CCFLCOMM = "+"
                    this.w_DATVAL = CALCFEST(this.w_PNDATREG, this.w_CALFES, this.w_CONBAN, this.w_CAUMOV, this.w_TIPCAU, 1)
                    this.w_ROWNUM = this.w_CPROWNUM
                    * --- Insert into CCM_DETT
                    i_nConn=i_TableProp[this.CCM_DETT_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.CCM_DETT_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCM_DETT_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"CCSERIAL"+",CCROWRIF"+",CCCODCAU"+",CCNUMCOR"+",CCDATVAL"+",CCIMPCRE"+",CCIMPDEB"+",CPROWNUM"+",CPROWORD"+",CCFLDEBI"+",CCFLCRED"+",CCFLCOMM"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'CCM_DETT','CCSERIAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'CCM_DETT','CCROWRIF');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CAUMOV),'CCM_DETT','CCCODCAU');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CONBAN),'CCM_DETT','CCNUMCOR');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_DATVAL),'CCM_DETT','CCDATVAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPCRE),'CCM_DETT','CCIMPCRE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDEB),'CCM_DETT','CCIMPDEB');
                      +","+cp_NullLink(cp_ToStrODBC(1),'CCM_DETT','CPROWNUM');
                      +","+cp_NullLink(cp_ToStrODBC(10),'CCM_DETT','CPROWORD');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLDEBI),'CCM_DETT','CCFLDEBI');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCRED),'CCM_DETT','CCFLCRED');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCOMM),'CCM_DETT','CCFLCOMM');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_PNSERIAL,'CCROWRIF',this.w_ROWNUM,'CCCODCAU',this.w_CAUMOV,'CCNUMCOR',this.w_CONBAN,'CCDATVAL',this.w_DATVAL,'CCIMPCRE',this.w_IMPCRE,'CCIMPDEB',this.w_IMPDEB,'CPROWNUM',1,'CPROWORD',10,'CCFLDEBI',this.w_CCFLDEBI,'CCFLCRED',this.w_CCFLCRED,'CCFLCOMM',this.w_CCFLCOMM)
                      insert into (i_cTable) (CCSERIAL,CCROWRIF,CCCODCAU,CCNUMCOR,CCDATVAL,CCIMPCRE,CCIMPDEB,CPROWNUM,CPROWORD,CCFLDEBI,CCFLCRED,CCFLCOMM &i_ccchkf. );
                         values (;
                           this.w_PNSERIAL;
                           ,this.w_ROWNUM;
                           ,this.w_CAUMOV;
                           ,this.w_CONBAN;
                           ,this.w_DATVAL;
                           ,this.w_IMPCRE;
                           ,this.w_IMPDEB;
                           ,1;
                           ,10;
                           ,this.w_CCFLDEBI;
                           ,this.w_CCFLCRED;
                           ,this.w_CCFLCOMM;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error=MSG_INSERT_ERROR
                      return
                    endif
                    * --- Aggiorno Saldi C\C
                    * --- Write into COC_MAST
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.COC_MAST_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.COC_MAST_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"BASALCRE =BASALCRE+ "+cp_ToStrODBC(this.w_IMPCRE);
                      +",BASALDEB =BASALDEB+ "+cp_ToStrODBC(this.w_IMPDEB);
                          +i_ccchkf ;
                      +" where ";
                          +"BACODBAN = "+cp_ToStrODBC(this.w_CONBAN);
                             )
                    else
                      update (i_cTable) set;
                          BASALCRE = BASALCRE + this.w_IMPCRE;
                          ,BASALDEB = BASALDEB + this.w_IMPDEB;
                          &i_ccchkf. ;
                       where;
                          BACODBAN = this.w_CONBAN;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                endif
              endif
            endif
          endif
        endif
      endif
    endif
    if this.w_IMPFIN<>0 and this.w_PNTIPDOC<>"FC"
      this.w_AGGIORNA = .T.
      if (EMPTY(this.oParentObject.w_CAUSAL) AND this.w_PNTIPCLF="C") OR (EMPTY(this.oParentObject.w_CAUSAF) AND this.w_PNTIPCLF="F")
        if this.w_PNTIPCLF="C"
          MES[30]=ah_MsgFormat("Causale saldac. clienti non definita nei parametri saldaconto")
        else
          MES[30]=ah_MsgFormat("Causale saldac. fornitori non definita nei parametri saldaconto")
        endif
        this.w_FLERR = .T.
      else
        this.w_PNCODCON = this.w_PNCODCLF
        this.w_PNTIPCON = this.w_PNTIPCLF
        this.w_APPVAL = this.w_IMPFIN + this.w_RITATT
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "S"
        this.w_PNCODCAU = iif(this.w_PNTIPCLF="C",this.oParentObject.w_CAUSAL,this.oParentObject.w_CAUSAF)
        this.w_PNINICOM = cp_CharToDate("  -  -  ")
        this.w_PNFINCOM = cp_CharToDate("  -  -  ")
        this.w_GESTPAR = "FINANZIA"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_GESTPAR = " "
      endif
      if ((this.w_IMPFIN<0 And this.w_PNTIPCLF="C") Or (this.w_IMPFIN>0 And this.w_PNTIPCLF="F")) And Empty(this.oParentObject.w_SAABBA)
        MES[29] = ah_MsgFormat("Conto Abbuoni Attivi, non Definito nei Parametri Diff. e Abbuoni")
        this.w_FLERR = .T.
      endif
      if ((this.w_IMPFIN>0 And this.w_PNTIPCLF="C") Or (this.w_IMPFIN<0 And this.w_PNTIPCLF="F")) And Empty(this.oParentObject.w_SAABBP)
        MES[29] = ah_MsgFormat("Conto Abbuoni Passivi, non Definito nei Parametri Diff. e Abbuoni")
        this.w_FLERR = .T.
      endif
      if Not this.w_FLERR
        if (this.w_IMPFIN<0 And this.w_PNTIPCLF="C") Or (this.w_IMPFIN>0 And this.w_PNTIPCLF="F")
          * --- Abbuoni attivi
          this.w_PNCODCON = this.oParentObject.w_SAABBA
        endif
        if (this.w_IMPFIN>0 And this.w_PNTIPCLF="C") Or (this.w_IMPFIN<0 And this.w_PNTIPCLF="F")
          * --- Abbuoni passivi
          this.w_PNCODCON = this.oParentObject.w_SAABBP
        endif
        this.w_PNTIPCON = "G"
        this.w_APPVAL = this.w_IMPFIN
        this.w_APPSEZ = this.w_SEZCLF
        this.w_FLVAL = "S"
        this.w_PNCODCAU = iif(this.w_PNTIPCLF="C",this.oParentObject.w_CAUSAL,this.oParentObject.w_CAUSAF)
        this.w_PNINICOM = cp_CharToDate("  -  -  ")
        this.w_PNFINCOM = cp_CharToDate("  -  -  ")
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.w_RITATT<>0 and this.w_PNTIPDOC<>"FC" AND this.w_PNTIPCLF="C" and (g_RITE<>"S" or (g_RITE="S" AND this.w_VALRIT=0))
      if Not this.w_AGGIORNA
        if EMPTY(this.oParentObject.w_CAUSAL) AND this.w_PNTIPCLF="C"
          MES[30]=ah_MsgFormat("Causale saldac. clienti non definita nei parametri saldaconto")
          this.w_FLERR = .T.
        else
          this.w_PNCODCON = this.w_PNCODCLF
          this.w_PNTIPCON = this.w_PNTIPCLF
          this.w_APPVAL = this.w_RITATT
          this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
          this.w_FLVAL = "N"
          this.w_PNCODCAU = this.oParentObject.w_CAUSAL
          this.w_PNINICOM = cp_CharToDate("  -  -  ")
          this.w_PNFINCOM = cp_CharToDate("  -  -  ")
          this.w_GESTPAR = "RITATTI"
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_GESTPAR = " "
        endif
      endif
      if EMPTY(this.oParentObject.w_CRITAT)
        MES[31]=ah_MsgFormat("Conto ritenute c/erario non definito nei parametri vendite")
        this.w_FLERR = .T.
      else
        this.w_PNCODCON = this.oParentObject.w_CRITAT
        this.w_PNTIPCON = "G"
        this.w_APPVAL = this.w_RITATT
        this.w_APPSEZ = this.w_SEZCLF
        this.w_FLVAL = "N"
        this.w_PNCODCAU = this.oParentObject.w_CAUSAL
        this.w_PNINICOM = cp_CharToDate("  -  -  ")
        this.w_PNFINCOM = cp_CharToDate("  -  -  ")
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if Not this.w_FLERR
      if this.w_VALRIT<>0 OR this.w_VALENA<>0 Or this.w_RITPRE<>0
        * --- Se Ritenuta d'Acconto o Enasarco Acconto da Contabilizzare aggiorna Righe
        * --- (Attenzione: Non Spostare il Riferimento al w_CPROWNUM, viene usato nella Pag.9)
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Se ho qualche problema per la gestione Cespiti annullo la registrazione
    *     posso dare questo messaggio solo dopo aver esaminato tutto il castelletto contabile...
    if this.w_NOGENCES
      * --- Verifico se l'errore � dovuto al fatto che non � presente nessuna contropartita
      *     di tipo cespite ma sono stati abbinati movimenti cespiti al documento.
      * --- Distinguo la messaggistica a seconda della mancanza della contropartita
      *     oppure del movimento cespite
      if this.w_INSCESP
        this.w_MESS_ERR = ah_MsgFormat("Non esistono contropartite di tipologia cespite.")
      else
        this.w_MESS_ERR = ah_MsgFormat("E' presente una contropartita di tipo cespite ma nessun movimento cespite associato.")
      endif
      this.w_NR = "YY"
      this.w_FLERR = .T.
      this.Page_14()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_REGOK = .F.
      this.w_NUDOC = this.w_NUDOC + 1
    endif
    if this.w_FLERR
      * --- Lancio pag14 per costruire, se manca, l'intestazione del documento,
      *     w_MESS_ERR='NO' per evitare di visualizzare due volte un eventuale errore
      *     gia segnalato (Es. manca quadratura). Non posso svuotarlo perch� se
      *     vuoto scatta il messaggio automatico...
      this.w_MESS_ERR = "NO"
      this.Page_14()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_LOOP = 1
      * --- Scorro l'array contenente i messaggi di errore
      do while this.w_LOOP<= this.w_MES_DIM
        if NOT EMPTY(MES[ this.w_LOOP ])
          this.w_oERRORLOG.AddMsgLogNoTranslate(SPACE(10)+MES[ this.w_LOOP ])     
        endif
        this.w_LOOP = this.w_LOOP + 1
      enddo
      * --- Abbandona la Registrazione
      * --- Raise
      i_Error="Errore"
      return
    else
      * --- Scrive Flag Contabilizzato sul Documento Origine
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLCONT ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLCONT');
        +",MVRIFCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'DOC_MAST','MVRIFCON');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_OSERIAL);
               )
      else
        update (i_cTable) set;
            MVFLCONT = "S";
            ,MVRIFCON = this.w_PNSERIAL;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_OSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Verifico Quadratura (se modifico a mano l'imposta o l'imponibile sul documento)
      if this.w_TOTDAR<>this.w_TOTAVE
        this.w_MESS_ERR = ah_MsgFormat("Manca quadratura %1",Tran(this.w_TOTDAR-this.w_TOTAVE,"@Z "+v_PV[20*(this.w_DECTOP+2)]))
        this.w_NR = "YY"
        this.Page_14()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- commit
      cp_EndTrs(.t.)
      this.w_SEGNALA = this.w_SEGNALA + ah_MsgFormat("Reg. %1 del. %2 cliente %3 %0",ALLTRIM(STR(this.w_PNNUMRER,15)),DTOC(this.w_PNDATREG),Alltrim(this.w_PNCODCLF))
      this.w_OKDOC = this.w_OKDOC + 1
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Dettaglio P.N. e Saldi
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    if this.w_FLVAL="S" AND this.w_PNCODVAL<>this.w_PNVALNAZ
      * --- Se arriva un Importo in Valuta e il Documento e' in Valuta Converte in Moneta di Conto
      this.w_APPVAL = VAL2MON(this.w_APPVAL,this.w_PNCAOVAL,this.w_CAONAZ,this.w_PNDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
    endif
    * --- Inverte la sezione se Negativo..
    this.w_APPSEZ = IIF(this.w_APPVAL<0, IIF(this.w_APPSEZ="D", "A", "D"), this.w_APPSEZ)
    this.Page_9()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Se il Documento e' un fattura Corrispettivi, Scrive Riga su Sezione Opposta
    if this.w_PNTIPDOC="FC"
      * --- Salva Impostazioni Riga Origine
      this.w_OROWNUM = this.w_CPROWNUM
      this.w_OROWORD = this.w_CPROWORD 
      this.w_OAPPSEZ = this.w_APPSEZ
      this.w_CPROWNUM = this.w_CPROWNUM + 50
      this.w_CPROWORD = this.w_CPROWORD + 500
      this.w_APPSEZ = IIF(this.w_APPSEZ="D", "A", "D")
      this.Page_9()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Ripristina Impostazioni Riga Origine
      this.w_CPROWNUM = this.w_OROWNUM
      this.w_CPROWORD = this.w_OROWORD
      this.w_APPSEZ = this.w_OAPPSEZ
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive le contropartite per le righe:
    *     Sconto merce
    *     Omaggi imponibile
    *     Omaggi iva+imponibile
    *     Inoltre gestisce le spese in dati generali...
    * --- Costruisco l'elenco delle contropartite associate a righe sconto merce..
    *     ( se non bolla doganale )
    if this.w_TOTDOC=0 AND this.w_IVAGLO=0 AND this.w_SCOMER=1 AND Not this.w_FLERR And Not this.w_TDBOLDOG="S"
       
 Select CONTRO, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, FINCOM FROM DettDocu ; 
 WHERE FLOMAG="S" OR VALRIG=0 INTO CURSOR AppCur GROUP BY CONTRO, INICOM, FINCOM
       
 Select AppCur 
 Go Top 
 Scan
      if EMPTY(CONTRO)
        MES[4]=ah_MsgFormat("Conto contropartita non definito (cat.CF: %1 cat.art: %2)",CATCLF,CATART)
        this.w_FLERR = .T.
      else
        this.w_SCOMER = 10
        this.w_PNCODCON = CONTRO
        this.w_APPVAL = 0
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "N"
        this.w_PNINICOM = IIF(empty(INICOM),this.w_INICOM,INICOM)
        this.w_PNFINCOM = IIF(empty(FINCOM),this.w_FINCOM,FINCOM)
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      SELECT AppCur
      EndScan
      * --- Scrive Riga IVA Sc.Merce
      if this.w_SCOMER=10 And NOT EMPTY(this.w_CCCONIVA)
        this.w_PNCODCON = this.w_CCCONIVA
        this.w_APPVAL = 0
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "N"
        this.w_PNINICOM = cp_CharToDate("  -  -  ")
        this.w_PNFINCOM = cp_CharToDate("  -  -  ")
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Prevede Righe IVA Sconto merce
        *     Sbianco l'array che conterr� i vari codici da inserire nel castelletto IVA
        *     (Pag 10)
         
 FOR L_i = 1 TO 10 
 SMER[L_i,1] = SPACE(5) 
 SMER[L_i,2] = SPACE(5) 
 ENDFOR 
 Select CODIVA,CONTRO From DettDocu Where FLOMAG="S" OR VALRIG=0 And Not Empty( CODIVA ) Into Cursor AppCur Group By CODIVA NoFilter 
 Select AppCur 
 Go Top
        this.w_COD_IVA_CNT = 0
        Scan
        this.w_COD_IVA_CNT = this.w_COD_IVA_CNT + 1
        this.w_CODINT = SPACE(5)
        this.w_FLAINT = " "
         
 SMER[ this.w_COD_IVA_CNT,1 ] = CODIVA 
 SMER[ this.w_COD_IVA_CNT,2 ] = CONTRO
        this.w_APPO = NVL( CODIVA , "     " )
        if NOT EMPTY( this.w_APPO ) OR this.w_ACQINT="S" OR this.w_ACQAUT="S"
          w_WORKAREA=ALIAS()
          this.w_SEARCH_TABLE = "VOCIIVA"
          this.w_SEARCH_IVCODIVA = this.w_APPO
          this.Pag16()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_CODINT = Nvl(IVCODINT," ")
          this.w_FLAINT = Nvl(IVACQINT," ")
          if NOT EMPTY(w_WORKAREA)
            Select (w_WORKAREA)
          endif
          * --- Se Doc. INTRA prende il Codice Iva apposito (se esiste)
          if this.w_ACQINT="S" AND NOT EMPTY(this.w_CODINT) AND this.w_FLAINT<>"A"
             
 SMER[ this.w_COD_IVA_CNT,1 ] = this.w_CODINT
          endif
        endif
        EndScan
        * --- Variabile di stato, mi segnalo che ho trovato almeno un codice iva 
        *     assocciato ad una riga sconto merce
        this.w_SCOMER = IIF( this.w_COD_IVA_CNT>0 , 100 , this.w_SCOMER )
      endif
    endif
    * --- Scrive le Spese - Incasso
    if this.w_SPEINC<>0 AND this.w_FLRINC<>"S"
      if EMPTY(this.oParentObject.w_CONINC)
        MES[5]=ah_MsgFormat("Conto spese di incasso non definito")
        this.w_FLERR = .T.
      else
        this.w_PNCODCON = this.oParentObject.w_CONINC
        if this.w_FLSCOR="S"
          if NOT EMPTY(this.w_IVAINC)
            * --- Scorpora le Spese
            this.w_APPO = 0
            w_WORKAREA=ALIAS()
            this.w_SEARCH_TABLE = "VOCIIVA"
            this.w_SEARCH_IVCODIVA = this.w_IVAINC
            this.Pag16()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_APPO = IVPERIVA
            if NOT EMPTY(w_WORKAREA)
              Select (w_WORKAREA)
            endif
            if this.w_APPO<>0
              this.w_IMPNET = cp_ROUND((this.w_SPEINC/(1+(this.w_APPO/100)))+IIF(this.w_PNCODVAL=g_CODLIR,.499,0), this.w_DECTOT)
              this.w_APPO1 = this.w_SPEINC - this.w_IMPNET
              this.w_SPEINC = this.w_IMPNET
            endif
          endif
        endif
        this.w_APPVAL = this.w_SPEINC
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "S"
        this.w_PNINICOM = this.w_INICOM
        this.w_PNFINCOM = this.w_FINCOM
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Scrive le Spese - Imballo
    if this.w_SPEIMB<>0 AND this.w_FLRIMB<>"S"
      if EMPTY(this.oParentObject.w_CONIMB)
        MES[6]=ah_MsgFormat("Conto spese di imballo non definito")
        this.w_FLERR = .T.
      else
        this.w_PNCODCON = this.oParentObject.w_CONIMB
        if this.w_FLSCOR="S"
          if NOT EMPTY(this.w_IVAIMB)
            * --- Scorpora le Spese
            this.w_APPO = 0
            w_WORKAREA=ALIAS()
            this.w_SEARCH_TABLE = "VOCIIVA"
            this.w_SEARCH_IVCODIVA = this.w_IVAIMB
            this.Pag16()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_APPO = IVPERIVA
            if NOT EMPTY(w_WORKAREA)
              Select (w_WORKAREA)
            endif
            if this.w_APPO<>0
              this.w_IMPNET = cp_ROUND((this.w_SPEIMB/(1+(this.w_APPO/100)))+IIF(this.w_PNCODVAL=g_CODLIR,.499,0), this.w_DECTOT)
              this.w_APPO1 = this.w_SPEIMB - this.w_IMPNET
              this.w_SPEIMB = this.w_IMPNET
            endif
          endif
        endif
        this.w_APPVAL = this.w_SPEIMB
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "S"
        this.w_PNINICOM = this.w_INICOM
        this.w_PNFINCOM = this.w_FINCOM
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Scrive le Spese - Trasporto
    if this.w_SPETRA<>0 AND this.w_FLRTRA<>"S"
      if EMPTY(this.oParentObject.w_CONTRA)
        MES[7]=ah_MsgFormat("Conto spese di trasporto non definito")
        this.w_FLERR = .T.
      else
        this.w_PNCODCON = this.oParentObject.w_CONTRA
        if this.w_FLSCOR="S"
          if NOT EMPTY(this.w_IVATRA)
            * --- Scorpora le Spese
            this.w_APPO = 0
            w_WORKAREA=ALIAS()
            this.w_SEARCH_TABLE = "VOCIIVA"
            this.w_SEARCH_IVCODIVA = this.w_IVATRA
            this.Pag16()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_APPO = Nvl(IVPERIVA,0)
            if NOT EMPTY(w_WORKAREA)
              Select (w_WORKAREA)
            endif
            if this.w_APPO<>0
              this.w_IMPNET = cp_ROUND((this.w_SPETRA/(1+(this.w_APPO/100)))+IIF(this.w_PNCODVAL=g_CODLIR,.499,0), this.w_DECTOT)
              this.w_APPO1 = this.w_SPETRA - this.w_IMPNET
              this.w_SPETRA = this.w_IMPNET
            endif
          endif
        endif
        this.w_APPVAL = this.w_SPETRA
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "S"
        this.w_PNINICOM = this.w_INICOM
        this.w_PNFINCOM = this.w_FINCOM
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Scrive le Spese - Bolli
    if this.w_SPEBOL<>0 AND this.oParentObject.w_FLVEAC<>"A"
      if EMPTY(this.oParentObject.w_CONBOL)
        MES[8]=ah_MsgFormat("Conto spese bolli non definito")
        this.w_FLERR = .T.
      else
        this.w_PNCODCON = this.oParentObject.w_CONBOL
        this.w_APPVAL = this.w_SPEBOL
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "S"
        this.w_PNINICOM = this.w_INICOM
        this.w_PNFINCOM = this.w_FINCOM
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Scrive il Dettaglio: Omaggi di Imponibile... (Sommo le righe di omaggio e 
    *     le associo alla contropartite omaggio in Contropartite e Vincoli...)
    *     Se non bolla doganale
    if Not this.w_TDBOLDOG="S"
      this.w_PNCODCAU = IIF( Empty( this.w_CAUGIRO ) , this.w_PNCODCAU , this.w_CAUGIRO )
       
 Select CONOMA, SUM(VALRIG) AS VALAPP, CATCLF, CATART, INICOM, FINCOM FROM DettDocu ; 
 WHERE FLOMAG $ "IE" INTO CURSOR AppCur GROUP BY CONOMA, INICOM, FINCOM
       
 Select AppCur 
 Go Top 
 Scan For ValApp<>0
      if EMPTY(APPCUR.CONOMA)
        MES[9]=ah_MsgFormat("Conto omaggi di imponibile non definito (cat.CF: %1 cat.art: %2)",CATCLF,CATART)
        this.w_FLERR = .T.
      else
        this.w_PNCODCON = APPCUR.CONOMA
        this.w_APPVAL = APPCUR.VALAPP
        this.w_APPSEZ = this.w_SEZCLF
        this.w_FLVAL = "S"
        this.w_PNINICOM = IIF(empty(APPCUR.INICOM),this.w_INICOM,APPCUR.INICOM)
        this.w_PNFINCOM = IIF(empty(APPCUR.FINCOM),this.w_FINCOM,APPCUR.FINCOM)
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT AppCur
      endif
      EndScan
      this.w_PNCODCAU = this.w_OPNCODCAU
    endif
    * --- Scrive eventuale riga di split payment per il conto di storno
    this.w_OPNCODCAU = this.w_PNCODCAU
    if this.w_IMPSPL<>0 
      this.w_PNCODCON = this.oParentObject.w_CONSPL
      this.w_APPVAL = this.w_IMPSPL
      this.w_APPSEZ = this.w_SEZCLF
      this.w_FLVAL = "S"
      this.w_PNINICOM = this.w_INICOM
      this.w_PNFINCOM = this.w_FINCOM
      this.w_PNCODCAU = this.oParentObject.w_CAUSPL
      this.w_PNINICOM = cp_CharToDate("  -  -  ")
      this.w_PNFINCOM = cp_CharToDate("  -  -  ")
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Scrive eventuale riga di split payment per il cliente
    this.w_OKPARSPL = .f.
    this.w_RIGASPL = 0
    if this.w_IMPSPL<>0 
      * --- Aggiorna Riga Cliente/Fornitore
      this.w_PNTIPCON = this.w_PNTIPCLF
      this.w_PNCODCON = this.w_PNCODCLF
      this.w_APPVAL = this.w_IMPSPL
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_FLVAL = "S"
      this.w_PNINICOM = this.w_INICOM
      this.w_PNFINCOM = this.w_FINCOM
      this.w_PNFLPART = IIF(this.w_PARTSN $ "CS" , "S", "N")
      this.w_PNCODPAG = this.w_CODPAG
      this.w_PNCODCAU = this.oParentObject.w_CAUSPL
      this.w_GESTPAR = "INTESTA"
      this.w_PNINICOM = cp_CharToDate("  -  -  ")
      this.w_PNFINCOM = cp_CharToDate("  -  -  ")
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_GESTPAR = " "
      if this.w_PNFLPART="S"
        * --- Le partite di saldo le creo in pagina 13 qui memorizzo dati diversi e se � il caso do farle
        this.w_RIGASPL = this.w_CPROWNUM
        this.w_OKPARSPL = .t.
        this.w_APPSEZSPL = this.w_APPSEZ
      endif
      this.w_PNFLPART = "N"
      this.w_PNCODPAG = ""
      this.w_PNCODAGE = SPACE(5)
      this.w_PNCODPAG = SPACE(5)
      this.w_PNIMPIND = 0
      this.w_PNTIPCON = "G"
      this.w_PNCODCAU = this.w_OPNCODCAU
    endif
    * --- Scrive il Dettaglio: Conti IVA su Omaggi Imposta
    *     Associo alla contropartita omaggio imposta le righe a omaggio imponibile..
    *     Se non bolla doganale
    if this.w_IVAOMA<>0
       
 Select CONOMI, CODIVA, SUM(VALRIG) AS VALAPP, CATCLF, CATART FROM DettDocu ; 
 WHERE FLOMAG = "E" INTO ARRAY AppArr GROUP BY CONOMI, CODIVA
      this.w_APPO1 = this.w_IVAOMA
      this.w_LOOP = 1
      * --- Se bolla doganale impedisco la contabilizzazione di righe
      *     ad omaggio di imponibile + iva, in questo caso infatti eliminare
      *     le righe omaggio provoca una squadratura
      if this.w_TDBOLDOG="S" And ALEN(AppArr, 1)>0
        this.w_MESS_ERR = ah_MsgFormat("Bolla doganale con rig %1 ad omaggio di imponibile + IVA",iif(ALEN(AppArr,1)=1,"a","he"))
        this.w_NR = "YY"
        this.w_FLERR = .T.
        this.Page_14()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_REGOK = .F.
      else
        this.w_PNCODCAU = IIF( Empty( this.w_CAUGIRO ) , this.w_PNCODCAU , this.w_CAUGIRO )
        do while this.w_LOOP<= ALEN(AppArr, 1)
          if this.w_PNCODVAL<>this.w_PNVALNAZ
            * --- Converte in Moneta di Conto
            AppArr[ this.w_LOOP , 3] = VAL2MON(AppArr[this.w_LOOP, 3],this.w_PNCAOVAL,this.w_CAONAZ,this.w_PNDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
          endif
          this.w_APPO2 = 0
          this.w_BLOOP = 1
          do while this.w_BLOOP<=6
            * --- Array dettaglio IVA, ricerco il solito codice IVA...
            if AI[ this.w_BLOOP , 2] = AppArr[this.w_LOOP, 2]
              * --- Determino l'imposta...
              if Not ( AppArr[ this.w_LOOP , 3]=0 OR AI[ this.w_BLOOP , 5]=0 )
                this.w_APPO2 = cp_Round(((AppArr[this.w_LOOP, 3] * AI[ this.w_BLOOP , 5]) / 100) + IIF(this.w_DECTOP=0, .499, .00499), this.w_DECTOP)
              endif
              * --- Esco dal ciclo While...
              this.w_BLOOP = 7
            else
              this.w_BLOOP = this.w_BLOOP + 1
            endif
          enddo
          * --- Scrive Imposta sul Vettore e Storna dal Totale IVA Omaggi
          AppArr[ this.w_LOOP , 3] = this.w_APPO2
          this.w_APPO1 = this.w_APPO1 - this.w_APPO2
          this.w_LOOP = this.w_LOOP + 1
        enddo
        * --- Alla Fine Scrive le Righe P.N.
        this.w_APPVAL = 0
        this.w_LOOP = 1
        do while this.w_LOOP<= ALEN(AppArr, 1)
          this.w_PNCODCON = AppArr[this.w_LOOP, 1]
          if EMPTY(this.w_PNCODCON)
            MES[10]=ah_MsgFormat("Conto IVA su omaggi non definito (cat.CF: %1 cat.art: %2)",AppArr[this.w_LOOP,4],AppArr[this.w_LOOP,5])
            this.w_FLERR = .T.
          endif
          * --- Somma gli Importi delle Aliquote IVA e Mette l'eventuale Resto sulla Prima Riga
          this.w_APPVAL = this.w_APPVAL + AppArr[this.w_LOOP, 3] + IIF(this.w_LOOP = 1, this.w_APPO1, 0)
          this.w_APPSEZ = this.w_SEZCLF
          this.w_FLVAL = "N"
          this.w_PNINICOM = cp_CharToDate("  -  -  ")
          this.w_PNFINCOM = cp_CharToDate("  -  -  ")
          * --- Se siamo in Fondo Scrive ...Oppure Scrive se il Successivo Conto e' diverso
          if this.w_LOOP = ALEN(AppArr, 1)
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Verifico il prssimo conto...
            if AppArr[this.w_LOOP+1, 1] <> this.w_PNCODCON
              this.Page_5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_APPVAL = 0
            endif
          endif
          this.w_LOOP = this.w_LOOP + 1
        enddo
        this.w_PNCODCAU = this.w_OPNCODCAU
      endif
    endif
    * --- Scrive il Dettaglio: Eventuali Differenze di Conversione (Solo alla Fine)
    this.w_DIFCON = 0
    if this.w_TOTDAR<>this.w_TOTAVE AND this.w_PNCODVAL<>this.w_PNVALNAZ
      * --- Da riportare nella sezione Opposta al Segno 
      this.w_DIFCON = this.w_TOTAVE - this.w_TOTDAR
      if EMPTY(this.oParentObject.w_CONDIC)
        MES[12] = "Conto Differenze di Conversione non Definito"
        this.w_FLERR = .T.
      else
        this.w_PNCODCON = this.oParentObject.w_CONDIC
        this.w_APPSEZ = IIF(this.w_DIFCON < 0, "A", "D")
        this.w_APPVAL = ABS(this.w_DIFCON)
        this.w_FLVAL = "N"
        this.w_PNINICOM = this.w_INICOM
        this.w_PNFINCOM = this.w_FINCOM
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Riassegno nel caso in cui le contropartite gestiscano le partite per
    *     creare le partite dell'intestatario non create immediatamente
    this.w_PNFLPART = "N"
    if Not this.w_FLERR AND this.oParentObject.w_TIPPAB="XXXXXXXXXX" AND this.w_PARTSN $ "ACS"
      MES[21]=ah_MsgFormat("Pagamento per abbuoni/anticipi non definito in contropartite parametri")
      this.w_FLERR = .T.
    endif
    * --- Scrive il Castelletto IVA
    if Not this.w_FLERR
      this.Page_10()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_PNFLPART="N"
        this.w_GESTPAR = "LEGGERATE"
        * --- Rileggo w_RIGPAR, tra la creazione della riga contabile e
        *     la creazione delle partite collegate, la causale contabile cambia
        *     su riga con le relative variabili ad essa legate...
        w_WORKAREA=ALIAS()
        this.w_SEARCH_TABLE = "CAU_CONT"
        this.w_SEARCH_CAU_CONT = this.w_PNCODCAU
        this.Pag16()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_RIGPAR = Nvl(CCFLPART," ")
        if NOT EMPTY(w_WORKAREA)
          Select (w_WORKAREA)
        endif
        this.Page_13()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_GESTPAR = ""
      endif
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Cursori di Appoggio
    if USED("CONTADOC")
      SELECT CONTADOC
      USE
    endif
    if USED("DettDocu")
      SELECT DettDocu
      USE
    endif
    if USED("AppCur")
      SELECT AppCur
      USE
    endif
    if USED("AppCur2")
      SELECT AppCur2
      USE
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    if USED("reg_iva")
      SELECT reg_iva
      USE
    endif
    if USED("AppIva")
      SELECT AppIva
      USE
    endif
    if USED("LOG")
      SELECT LOG
      USE
    endif
    if USED("CURS_VOCIIVA")
      Select CURS_VOCIIVA 
 Use
    endif
    if USED("CURS_CAU_CONT")
      Select CURS_CAU_CONT 
 Use
    endif
    i_retcode = 'stop'
    return
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Erario C/Ritenute e/o Enasarco
    if ( this.w_VALRIT<>0 Or this.w_RITPRE<>0 ) And this.w_TRFLGSTO="S"
      do case
        case EMPTY(this.oParentObject.w_CAURIT) 
          MES[14]=ah_MsgFormat("Causale ritenute non definita")
          this.w_FLERR = .T.
        case EMPTY(this.oParentObject.w_CONRIT) and this.w_VALRIT<>0 
          MES[14]=ah_MsgFormat("Erario C/Ritenute non definito")
          this.w_FLERR = .T.
        case EMPTY(this.oParentObject.w_CORIPR) And this.w_RITPRE<>0 
          MES[14]=ah_MsgFormat("Conto ritenute previdenziali non definito")
          this.w_FLERR = .T.
        otherwise
          if this.w_VALRIT<>0
            * --- Aggiorna Riga Erario C/Ritenute
            this.w_PNCODCON = this.oParentObject.w_CONRIT
            this.w_PNCODCAU = this.oParentObject.w_CAURIT
            this.w_APPVAL = this.w_VALRIT + IIF(this.oParentObject.w_CORIPR=this.oParentObject.w_CONRIT,this.w_RITPRE,0)
            this.w_APPSEZ = this.w_SEZCLF
            this.w_FLVAL = "S"
            this.w_PNINICOM = this.w_INICOM
            this.w_PNFINCOM = this.w_FINCOM
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if this.oParentObject.w_CORIPR<>this.oParentObject.w_CONRIT AND this.w_RITPRE<>0
            * --- Aggiorna Riga Erario C/Ritenute per Ritenute Previdenziali
            this.w_PNCODCON = this.oParentObject.w_CORIPR
            this.w_PNCODCAU = this.oParentObject.w_CAURIT
            this.w_APPVAL = this.w_RITPRE
            this.w_APPSEZ = this.w_SEZCLF
            this.w_FLVAL = "S"
            this.w_PNINICOM = this.w_INICOM
            this.w_PNFINCOM = this.w_FINCOM
            this.Page_5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
      endcase
    endif
    if this.w_VALENA<>0 
      if EMPTY(this.oParentObject.w_CONENA)
        MES[15]=ah_MsgFormat("Conto ENASARCO non definito")
        this.w_FLERR = .T.
      else
        * --- La causale di riga pu� variare, se ho delle ritenute prendo la causale 
        *     associata alle ritenute altrimenti prendo l'ultima causale di riga utilizzata
        this.w_PNCODCON = this.oParentObject.w_CONENA
        this.w_PNCODCAU = this.oParentObject.w_CAURIT
        this.w_APPVAL = this.w_VALENA
        this.w_APPSEZ = this.w_SEZCLF
        this.w_FLVAL = "S"
        this.w_PNINICOM = this.w_INICOM
        this.w_PNFINCOM = this.w_FINCOM
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if (this.w_VALRIT<>0 OR this.w_VALENA<>0 or this.w_RITPRE<>0) AND this.w_TRFLGSTO="S"
      * --- Aggiorna Riga Fornitore
      this.w_PNTIPCON = this.w_PNTIPCLF
      this.w_LRITPRE = this.w_RITPRE
      this.w_PNCODCON = this.w_PNCODCLF
      this.w_APPVAL = this.w_VALRIT+this.w_VALENA+this.w_RITPRE
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_FLVAL = "S"
      this.w_PNINICOM = this.w_INICOM
      this.w_PNFINCOM = this.w_FINCOM
      this.w_PNCODPAG = this.w_CODPAG
      this.w_PNFLVABD = this.w_FLVABD
      this.w_ABDIFF = this.w_VALRIT+this.w_VALENA+this.w_RITPRE
      this.w_FLSOSP = " "
      this.w_GESTPAR = "RITENUTE"
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      if (this.w_VALRIT<>0 OR this.w_RITPRE<>0) 
        this.w_PNFLPART = "S"
        this.w_ABDIFF = this.w_VALRIT+ this.w_RITPRE
        this.w_FLSOSP = "S"
        this.w_GESTPAR = "RITENUTE"
        this.Page_13()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_VALENA<>0
        * --- Aggiorna Riga Fornitore
        this.w_PNTIPCON = this.w_PNTIPCLF
        this.w_PNCODCON = this.w_PNCODCLF
        this.w_APPVAL = this.w_VALENA
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_FLVAL = "S"
        this.w_FLSOSP = " "
        this.w_PNINICOM = this.w_INICOM
        this.w_PNFINCOM = this.w_FINCOM
        this.w_PNCODPAG = this.w_CODPAG
        this.w_PNFLVABD = this.w_FLVABD
        this.w_ABDIFF = this.w_VALENA
        this.w_GESTPAR = "ENASARCO"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    this.w_GESTPAR = ""
    this.w_PNCODPAG = SPACE(5)
    this.w_PNFLVABD = " "
    this.w_PNTIPCON = "G"
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Dettaglio Righe Primanota
    if this.w_APPSEZ="D"
      this.w_PNIMPDAR = ABS(cp_ROUND(this.w_APPVAL, this.w_DECTOP))
      this.w_PNIMPAVE = 0
    else
      this.w_PNIMPDAR = 0
      this.w_PNIMPAVE = ABS(cp_ROUND(this.w_APPVAL, this.w_DECTOP))
    endif
    * --- Costruisco il flag aggiornamento Partite (stesso calcolo che in GSCG_MPN)
    * --- Leggo la data obsolescenza ed il tipo conto, gestione partite e pagamento
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANPARTSN,ANCODPAG,ANDTOBSO,ANTIPSOT"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANPARTSN,ANCODPAG,ANDTOBSO,ANTIPSOT;
        from (i_cTable) where;
            ANTIPCON = this.w_PNTIPCON;
            and ANCODICE = this.w_PNCODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LPARTSN = NVL(cp_ToDate(_read_.ANPARTSN),cp_NullValue(_read_.ANPARTSN))
      this.w_CODPAG1 = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
      this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
      this.w_TIPSOT = NVL(cp_ToDate(_read_.ANTIPSOT),cp_NullValue(_read_.ANTIPSOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    w_WORKAREA=ALIAS()
    this.w_SEARCH_TABLE = "CAU_CONT"
    this.w_SEARCH_CAU_CONT = this.w_PNCODCAU
    this.Pag16()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_RIGPAR = Nvl(CCFLPART," ")
    this.w_CCDESSUP = Nvl(CCDESSUP," ")
    this.w_CCDESRIG = Nvl(CCDESRIG," ")
    this.w_DATOBSO = Nvl(CCDTOBSO,{})
    if NOT EMPTY(w_WORKAREA)
      Select (w_WORKAREA)
    endif
    if this.w_DATOBSO<=this.w_PNDATREG AND NOT EMPTY(this.w_DATOBSO)
      MES[22]=ah_MsgFormat("Causale contabile %1 obsoleta",this.w_PNCODCAU)
      this.w_FLERR = .T.
    else
      do case
        case this.w_GESTPAR="ESDIF"
          * --- Se conto IVA per esigibilit� differita ha attivo il flag partite.
          *     In questo caso perch� se il conto IVA non � definito a partite debbo cmq recuperare
          *     il pagamento
          if this.w_LPARTSN="S" 
            this.w_PNCODPAG = this.w_CODPAG1
          endif
          * --- Nel caso di esigibilit� differit� creo ugualmente le partite anche
          *     se il conto non le gestisce
          this.w_PNFLPART = IIF(this.w_PNFLIVDF="S", "C", "N")
          if this.w_PNFLPART="N"
            MES[21] = "Conto IVA ad esigibilit� differita con causale di riga non gestista a partite (Verificare se documento con omaggi) "
            this.w_FLERR = .T.
          endif
        case this.w_GESTPAR = "COMPENSA" OR this.w_GESTPAR = "FINANZIA" OR (this.w_GESTPAR= "RITATTI" and g_rite<>"S")
          * --- Se st� eseguendo una compensazione allora, se conto gestito a partite
          *     ignoro il tipo partita della causale e creo la riga a saldo partite.
          this.w_PNFLPART = IIF(this.w_LPARTSN="S", "S", "N")
        otherwise
          this.w_PNFLPART = IIF(this.w_LPARTSN="S", this.w_RIGPAR, "N")
      endcase
      * --- Se creazione partite legate a conto generico riporto anche il codice pagamento
      *     legato al conto
      if Empty( this.w_GESTPAR ) 
        * --- Se vuoto il codice pagamento utilizzo quello per Abbuoni specificato
        *     nelle contropartite abbuoni
        if Not Empty( this.w_CODPAG1 )
          this.w_PNCODPAG = this.w_CODPAG1
        else
          this.w_PNCODPAG = g_SAPAGA
        endif
      endif
      * --- Eventuali Righe non Valorizzate
      this.w_PNFLZERO = IIF(this.w_PNIMPDAR=0 AND this.w_PNIMPAVE=0, "S", " ")
      this.w_TOTDAR = this.w_TOTDAR + this.w_PNIMPDAR
      this.w_TOTAVE = this.w_TOTAVE + this.w_PNIMPAVE
      this.w_PNIMPIND = IIF(this.oParentObject.w_FLVEAC="A" AND this.w_PNTIPCON="G", this.w_PNIMPIND, 0)
      if g_CESP="S"
        * --- Controllo se associato al documento sono presenti movimenti cespiti.
        *     Se associati devo verificare se la contropartita associata � di tipo cespite
        *     Non verifico il check sulla causale per consentire di contabilizzare
        *     anche documenti anteriori all'intervento
        *     
        *     Nel caso di documento con contropartita cespite ma nessun movimento associato
        *     devo comunque contabilizzare
        * --- w_NOGENCES a true significa che ho un conto cespiti senza movimento
        *     oppure ho il movimento ma non ho trovato un conto contabile di tipo Cespite
        if this.w_INSCESP
          if NOT this.w_CONCESP
            if this.w_TIPSOT<>"M"
              this.w_NOGENCES = .T.
            else
              * --- Utilizzo questa variabile per non effettuare pi� i controlli.
              this.w_CONCESP = .T.
              this.w_NOGENCES = .F.
            endif
          endif
        endif
      endif
      if Empty(this.w_PNDESRIG) and Not Empty(this.w_CCDESRIG)
         
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(this.w_PNCODAGE) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]=ALLTRIM(this.w_PTNUMPAR) 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=DTOC(this.w_PTDATSCA)
        this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
      endif
      this.w_ROWCOUNT = this.w_ROWCOUNT + 1
      if g_TRAEXP $ "A-C-G" AND this.w_PNTIPREG $ "VACE" AND this.w_ROWCOUNT>10 AND EMPTY(this.w_WARNMSG)
        this.w_WARNMSG = ah_MsgFormat("Attenzione:%0La procedura APRI non consente la registrazione di fatture con pi� di 10 righe di dettaglio. Non sar� quindi possibile esportare la registrazione.")
        if this.oParentObject.w_FLVEAC="V"
          this.w_WARNMSG = this.w_WARNMSG + ah_MsgFormat("Verificare doc.N.: %1 del.: %2",ALLTRIM(STR(this.w_PNNUMDOC,15))+IIF(EMPTY(this.w_PNALFDOC),"","/"+Alltrim(this.w_PNALFDOC)),DTOC(this.w_PNDATDOC))
        else
          this.w_WARNMSG = this.w_WARNMSG + ah_MsgFormat("Verificare prot.N.: %1 del.: %2",ALLTRIM(STR(this.w_PNNUMPRO,15))+IIF(EMPTY(this.w_PNALFPRO),"","/"+Alltrim(this.w_PNALFPRO)),DTOC(this.w_PNDATDOC))
        endif
        this.w_oERRORLOG.AddMsgLogNoTranslate(this.w_WARNMSG)     
        this.w_WADOC = this.w_WADOC + 1
      endif
      if this.w_PNFLPART<>"N"
        this.w_PNCODAGE = this.w_CODAGE
      else
        this.w_PNCODAGE = " "
      endif
      * --- Insert into PNT_DETT
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNDESRIG"+",PNCAURIG"+",PNCODPAG"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+",PNINICOM"+",PNFINCOM"+",PNIMPIND"+",PNFLABAN"+",PNCODAGE"+",PNFLVABD"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PNT_DETT','PNFLPART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODPAG),'PNT_DETT','PNCODPAG');
        +","+cp_NullLink(cp_ToStrODBC("+"),'PNT_DETT','PNFLSALD');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPIND),'PNT_DETT','PNIMPIND');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLABAN),'PNT_DETT','PNFLABAN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODAGE),'PNT_DETT','PNCODAGE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLVABD),'PNT_DETT','PNFLVABD');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',this.w_PNFLPART,'PNFLZERO',this.w_PNFLZERO,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCODCAU,'PNCODPAG',this.w_PNCODPAG)
        insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNDESRIG,PNCAURIG,PNCODPAG,PNFLSALD,PNFLSALI,PNFLSALF,PNINICOM,PNFINCOM,PNIMPIND,PNFLABAN,PNCODAGE,PNFLVABD &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,this.w_PNTIPCON;
             ,this.w_PNCODCON;
             ,this.w_PNIMPDAR;
             ,this.w_PNIMPAVE;
             ,this.w_PNFLPART;
             ,this.w_PNFLZERO;
             ,this.w_PNDESRIG;
             ,this.w_PNCODCAU;
             ,this.w_PNCODPAG;
             ,"+";
             ," ";
             ," ";
             ,this.w_PNINICOM;
             ,this.w_PNFINCOM;
             ,this.w_PNIMPIND;
             ,this.w_PNFLABAN;
             ,this.w_PNCODAGE;
             ,this.w_PNFLVABD;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_NR = ALLTRIM(STR(this.w_CPROWORD))
      this.w_PNFLABAN = SPACE(6)
      * --- Aggiorna Saldo
      * --- Try
      local bErr_05543898
      bErr_05543898=bTrsErr
      this.Try_05543898()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_05543898
      * --- End
      * --- Write into SALDICON
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
        +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
            +i_ccchkf ;
        +" where ";
            +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
            +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
            +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
               )
      else
        update (i_cTable) set;
            SLDARPER = SLDARPER + this.w_PNIMPDAR;
            ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
            &i_ccchkf. ;
         where;
            SLTIPCON = this.w_PNTIPCON;
            and SLCODICE = this.w_PNCODCON;
            and SLCODESE = this.w_PNCODESE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Gestione Analitica
      if this.w_FLANAL="S" And this.w_FLELAN<>"S" And this.w_PNTIPCON="G" And g_PERCCR="S" And g_COAN="S"
        this.w_MESS_ERR = GSAR_BRA(This,this.w_PNSERIAL, this.w_CPROWNUM, this.w_PNTIPCON, this.w_PNCODCON, this.w_APPVAL)
        if Not EMPTY(this.w_MESS_ERR)
          * --- Aggiorno file di log senza bloccare la contabilizzazione
          this.Page_14()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      * --- Gestione Partite
      this.Page_13()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_05543898()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive il Dettaglio: Eventuali arrotondamenti 
    if this.w_TOTDAR - this.w_TOTAVE <>0
      if EMPTY(this.oParentObject.w_CONARR)
        MES[12]=ah_MsgFormat("Conto arrotondamenti non definito")
        this.w_FLERR = .T.
      else
        this.w_PNCODCON = this.oParentObject.w_CONARR
        this.w_APPSEZ = IIF(this.w_TOTDAR - this.w_TOTAVE>0, "A", "D")
        this.w_APPVAL = ABS(this.w_TOTDAR - this.w_TOTAVE)
        this.w_FLVAL = "N"
        this.w_PNINICOM = this.w_INICOM
        this.w_PNFINCOM = this.w_FINCOM
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_MESS_ERR = ah_MsgFormat("Inserito conto arrotondamento [%1] per un importo pari A. %2",Alltrim(this.oParentObject.w_CONARR),Alltrim(Tran(this.w_APPVAL,v_PV[38+VVL])))
        this.Page_14()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Scrittura del Castelletto IVA
    * --- Cursore di Appoggio dati IVA
     
 CREATE CURSOR AppIva ; 
 (CODIVA C(5),CODINV C(5),CODAUT C(5), CONTRO C(15), FLOMAG C(1), PERIND N(3, 0), IMPONI N(18, 4), IMPIVA N(18, 4))
    * --- Chiusura Preventiva dei Cursori di Appoggio
    if USED("AppCur")
      SELECT AppCur
      USE
    endif
    if USED("AppCur2")
      SELECT AppCur2
      USE
    endif
    * --- Se previsto Dettaglio Contropartite
    if g_DETCON="S"
      * --- Costruzione Cursore dettaglio Contropartite
       
 SELECT CONTRO, CODIVA, FLOMAG, SUM(VALRIG) AS VALAPP FROM DettDocu ; 
 WHERE FLOMAG $ "XIES" INTO CURSOR AppCur2 GROUP BY CONTRO, CODIVA, FLOMAG NoFilter
      * --- Aggiunge eventuali Spese Accessorie+Bolli (se non ripartiti)
      = WRCURSOR("AppCur2")
      SELECT AppCur2
      * --- Attribuisco le spese alla riga IVA con controaprtita uguale se gia esiste 
      *     (sul dettaglio ho gia la contropartita assocaita alla spese) altrimenti 
      *     creo una nuova riga con l'importo delle spese e aliquota collegata.
      if this.w_SPEINC<>0 AND this.w_FLRINC<>"S" AND NOT EMPTY(this.oParentObject.w_CONINC) AND NOT EMPTY(this.w_IVAINC)
        GO TOP
        LOCATE FOR CONTRO=this.oParentObject.w_CONINC AND CODIVA=this.w_IVAINC AND FLOMAG="X"
        if FOUND()
          REPLACE VALAPP WITH (VALAPP+this.w_SPEINC)
        else
          APPEND BLANK
          REPLACE CONTRO WITH this.oParentObject.w_CONINC, CODIVA WITH this.w_IVAINC, FLOMAG WITH "X", VALAPP WITH this.w_SPEINC
        endif
      endif
      if this.w_SPEIMB<>0 AND this.w_FLRIMB<>"S" AND NOT EMPTY(this.oParentObject.w_CONIMB) AND NOT EMPTY(this.w_IVAIMB)
        GO TOP
        LOCATE FOR CONTRO=this.oParentObject.w_CONIMB AND CODIVA=this.w_IVAIMB AND FLOMAG="X"
        if FOUND()
          REPLACE VALAPP WITH (VALAPP+this.w_SPEIMB)
        else
          APPEND BLANK
          REPLACE CONTRO WITH this.oParentObject.w_CONIMB, CODIVA WITH this.w_IVAIMB, FLOMAG WITH "X", VALAPP WITH this.w_SPEIMB
        endif
      endif
      if this.w_SPETRA<>0 AND this.w_FLRTRA<>"S" AND NOT EMPTY(this.oParentObject.w_CONTRA) AND NOT EMPTY(this.w_IVATRA)
        GO TOP
        LOCATE FOR CONTRO=this.oParentObject.w_CONTRA AND CODIVA=this.w_IVATRA AND FLOMAG="X"
        if FOUND()
          REPLACE VALAPP WITH (VALAPP+this.w_SPETRA)
        else
          APPEND BLANK
          REPLACE CONTRO WITH this.oParentObject.w_CONTRA, CODIVA WITH this.w_IVATRA, FLOMAG WITH "X", VALAPP WITH this.w_SPETRA
        endif
      endif
      if this.w_SPEBOL<>0 AND this.oParentObject.w_FLVEAC<>"A" AND NOT EMPTY(this.oParentObject.w_CONBOL) AND NOT EMPTY(this.w_IVABOL)
        GO TOP
        LOCATE FOR CONTRO=this.oParentObject.w_CONBOL AND CODIVA=this.w_IVABOL AND FLOMAG="X"
        if FOUND()
          REPLACE VALAPP WITH (VALAPP+this.w_SPEBOL)
        else
          APPEND BLANK
          REPLACE CONTRO WITH this.oParentObject.w_CONBOL, CODIVA WITH this.w_IVABOL, FLOMAG WITH "X", VALAPP WITH this.w_SPEBOL
        endif
      endif
       
 SELECT CONTRO, CODIVA, FLOMAG, SUM(VALAPP) AS VALORE FROM AppCur2 ; 
 WHERE FLOMAG $ "XIES" INTO CURSOR AppCur GROUP BY CONTRO, CODIVA, FLOMAG
      SELECT AppCur2
      USE
      SELECT AppCur
      * --- Se non trovato niente chiude
      if RECCOUNT()=0
        USE
      endif
    endif
    * --- Cicla sul Vettore delle Aliquote IVA e carica AppIva
    this.w_LOOP = 1
    do while this.w_LOOP<=6
      if (NOT EMPTY(AI[ this.w_LOOP , 2])) AND ((AI[ this.w_LOOP , 3]<>0 OR AI[ this.w_LOOP , 4]<>0) OR AI[ this.w_LOOP , 1]="Z")
        * --- Se specificato applico codice IVA INTRA su vendite altrimenti
        *     replico codice utilizzato per riga acquisti
        this.w_IVCODIVA = AI[ this.w_LOOP , 2]
        this.w_IVCONTRO = SPACE(15)
        this.w_IVPERIND = AI[ this.w_LOOP , 6]
        this.w_IVIMPONI = AI[ this.w_LOOP , 3]
        this.w_IVIMPIVA = AI[ this.w_LOOP , 4]
        this.w_IVFLOMAG = IIF(this.w_IVIMPONI=0 AND this.w_IVIMPIVA=0, "S", AI[ this.w_LOOP , 1])
        this.w_CODINV = AI[ this.w_LOOP , 7]
        this.w_CODAUT = AI[ this.w_LOOP , 8]
        if g_DETCON="S" And USED("AppCur")
          this.w_AREC = -1
          * --- Memorizza i Resti
          this.w_RIMP = this.w_IVIMPONI
          this.w_RIVA = this.w_IVIMPIVA
          * --- 'Spalma' sulle varie aliquote
          SELECT AppCur
          GO TOP
          * --- Totale per calcolo proporzione
          SUM VALORE FOR CODIVA=this.w_IVCODIVA AND FLOMAG=this.w_IVFLOMAG TO this.w_APPO2
          if this.w_APPO2<>0
            GO TOP
            SCAN FOR CODIVA=this.w_IVCODIVA AND FLOMAG=this.w_IVFLOMAG 
            this.w_IVCONTRO = CONTRO
            this.w_IIMP = cp_ROUND((this.w_IVIMPONI * VALORE) / this.w_APPO2, this.w_DECTOP)
            this.w_IIVA = cp_ROUND((this.w_IVIMPIVA * VALORE) / this.w_APPO2, this.w_DECTOP)
            SELECT AppIva
            APPEND BLANK
            REPLACE CODIVA WITH this.w_IVCODIVA
            REPLACE CODINV WITH this.w_CODINV
            REPLACE CODAUT WITH this.w_CODAUT
            REPLACE CONTRO WITH this.w_IVCONTRO
            REPLACE FLOMAG WITH this.w_IVFLOMAG
            REPLACE PERIND WITH this.w_IVPERIND
            REPLACE IMPONI WITH this.w_IIMP
            REPLACE IMPIVA WITH this.w_IIVA
            * --- Memorizza il primo record inserito per aggiungere eventuali resto (e' il motivo per cui l'aggiornamento e' eseguito coi metodi xbase)
            this.w_AREC = IIF(this.w_AREC = -1, RECNO(), this.w_AREC)
            * --- Storna i Resti
            this.w_RIMP = this.w_RIMP - this.w_IIMP
            this.w_RIVA = this.w_RIVA - this.w_IIVA
            SELECT AppCur
            ENDSCAN 
          endif
          if this.w_AREC=-1
            * --- Non e' stato trovato nulla o w_APPO2=0 (?) scrive comunque il singolo record dai vettori
            SELECT DETTDOCU 
 GO TOP
            this.w_IVCONTRO = DETTDOCU.CONTRO
            INSERT INTO AppIva (CODIVA,CODINV, CODAUT,CONTRO, FLOMAG, PERIND, IMPONI, IMPIVA) ;
            VALUES (this.w_IVCODIVA, this.w_CODINV,this.w_CODAUT,this.w_IVCONTRO, this.w_IVFLOMAG, this.w_IVPERIND, this.w_IVIMPONI, this.w_IVIMPIVA)
          else
            SELECT AppIva
            if this.w_AREC>0 AND this.w_AREC<=RECCOUNT() AND (this.w_RIMP<>0 OR this.w_RIVA<>0)
              * --- Gestione dei Resti
              GOTO this.w_AREC
              REPLACE IMPONI WITH IMPONI + this.w_RIMP
              REPLACE IMPIVA WITH IMPIVA + this.w_RIVA
            endif
          endif
        else
           
 INSERT INTO AppIva (CODIVA,CODINV, CODAUT,CONTRO, FLOMAG, PERIND, IMPONI, IMPIVA) ; 
 VALUES (this.w_IVCODIVA, this.w_CODINV,this.w_CODAUT,this.w_IVCONTRO, this.w_IVFLOMAG, this.w_IVPERIND, this.w_IVIMPONI, this.w_IVIMPIVA)
        endif
      endif
      this.w_LOOP = this.w_LOOP + 1
    enddo
    this.w_ROWNUM = 0
    * --- Calcola eventuale Fuori Campo IVA
    this.w_DIFCON = 0
    if USED("AppIva")
      * --- Cicla sul Cursore IVA e scrive il Castelletto
      this.w_TOTIVA = 0
      SELECT AppIva
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CODIVA,""))
      this.w_IVCODIVA = CODIVA
      this.w_IVCONTRO = CONTRO
      this.w_IVFLOMAG = FLOMAG
      this.w_IVPERIND = PERIND
      this.w_IVIMPONI = IMPONI
      this.w_IVIMPIVA = IMPIVA
      if this.w_ACQAUT="S" 
        * --- Leggo Flag Reverse Charge
        w_WORKAREA=ALIAS()
        this.w_SEARCH_TABLE = "VOCIIVA"
        this.w_SEARCH_IVCODIVA = this.w_IVCODIVA
        this.Pag16()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_REVCHA = Nvl(IVREVCHA," ")
        if NOT EMPTY(w_WORKAREA)
          Select (w_WORKAREA)
        endif
      endif
      * --- Controllo congruenza Tot Documento con righe iva per riporto diff.conversione su Fuori campo iva
      this.w_TOTIVA = this.w_TOTIVA + (IIF(this.w_IVFLOMAG $ "IE", 0, this.w_IVIMPONI) + IIF(this.w_IVFLOMAG = "E" OR this.w_ACQINT="S" OR (this.w_ACQAUT="S" AND this.w_REVCHA="S"), 0, this.w_IVIMPIVA))
      this.w_IVTIPREG = this.w_PNTIPREG
      * --- Controllo se causale no iva e iva esente
      if NOT(this.w_PNTIPREG="N" AND (this.w_TOTPERIVA=0))
        if NOT(EMPTY(this.w_IVIMPONI) AND EMPTY(this.w_IVIMPIVA) AND this.w_IVFLOMAG<>"S")
          if g_LEMC="S" AND g_TRAEXP<>"N" AND this.w_IVFLOMAG="I" AND this.w_TDBOLDOG="S" AND Not empty(this.w_PNCODCLF)
            * --- Nel caso di trasferimento studio attivo e bolla doganale e riga di tipo omaggio imponibile 
            *     applico come contropartita studio l'intestatario del documento se presente
            this.w_IVTIPCOP = this.w_PNTIPCLF
            this.w_IVCONTRO = this.w_PNCODCLF
          else
            this.w_IVTIPCOP = "G"
          endif
          this.w_ROWNUM = this.w_ROWNUM + 1
          * --- Insert into PNT_IVA
          i_nConn=i_TableProp[this.PNT_IVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_IVA_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"IVSERIAL"+",CPROWNUM"+",IVCODIVA"+",IVTIPCON"+",IVCODCON"+",IVPERIND"+",IVTIPREG"+",IVNUMREG"+",IVIMPONI"+",IVIMPIVA"+",IVFLOMAG"+",IVCFLOMA"+",IVCONTRO"+",IVTIPCOP"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_IVA','IVSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PNT_IVA','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVCODIVA),'PNT_IVA','IVCODIVA');
            +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CCCONIVA),'PNT_IVA','IVCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVPERIND),'PNT_IVA','IVPERIND');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPREG),'PNT_IVA','IVTIPREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_IVA','IVNUMREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVIMPONI),'PNT_IVA','IVIMPONI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVIMPIVA),'PNT_IVA','IVIMPIVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVFLOMAG),'PNT_IVA','IVFLOMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVFLOMAG),'PNT_IVA','IVCFLOMA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVCONTRO),'PNT_IVA','IVCONTRO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPCOP),'PNT_IVA','IVTIPCOP');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'IVSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_ROWNUM,'IVCODIVA',this.w_IVCODIVA,'IVTIPCON',"G",'IVCODCON',this.w_CCCONIVA,'IVPERIND',this.w_IVPERIND,'IVTIPREG',this.w_IVTIPREG,'IVNUMREG',this.w_PNNUMREG,'IVIMPONI',this.w_IVIMPONI,'IVIMPIVA',this.w_IVIMPIVA,'IVFLOMAG',this.w_IVFLOMAG,'IVCFLOMA',this.w_IVFLOMAG)
            insert into (i_cTable) (IVSERIAL,CPROWNUM,IVCODIVA,IVTIPCON,IVCODCON,IVPERIND,IVTIPREG,IVNUMREG,IVIMPONI,IVIMPIVA,IVFLOMAG,IVCFLOMA,IVCONTRO,IVTIPCOP &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 ,this.w_ROWNUM;
                 ,this.w_IVCODIVA;
                 ,"G";
                 ,this.w_CCCONIVA;
                 ,this.w_IVPERIND;
                 ,this.w_IVTIPREG;
                 ,this.w_PNNUMREG;
                 ,this.w_IVIMPONI;
                 ,this.w_IVIMPIVA;
                 ,this.w_IVFLOMAG;
                 ,this.w_IVFLOMAG;
                 ,this.w_IVCONTRO;
                 ,this.w_IVTIPCOP;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        if this.w_ACQINT="S" OR (this.w_ACQAUT="S" AND this.w_REVCHA="S" )
          * --- Documento di Acquisto UE, scrive anche Riga IVA Vendite
          this.w_IVTIPREG = "V"
          if g_LEMC="S" AND g_TRAEXP $ "A-C-G" and Not empty(nvl(this.w_ANCONRIF,""))
            this.w_IVTIPCOP = "C"
            this.w_IVCONTRO = this.w_ANCONRIF
          else
            this.w_IVTIPCOP = "G"
          endif
          * --- Leggo il registro IVA
          * --- Read from CAUIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAUIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA_idx,2],.t.,this.CAUIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AINUMREG,AICONDET"+;
              " from "+i_cTable+" CAUIVA where ";
                  +"AICODCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                  +" and AITIPCLF = "+cp_ToStrODBC(this.w_PNTIPCLF);
                  +" and AICODCLF = "+cp_ToStrODBC(this.w_PNCODCLF);
                  +" and AITIPREG = "+cp_ToStrODBC("V");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AINUMREG,AICONDET;
              from (i_cTable) where;
                  AICODCAU = this.w_PNCODCAU;
                  and AITIPCLF = this.w_PNTIPCLF;
                  and AICODCLF = this.w_PNCODCLF;
                  and AITIPREG = "V";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_AINUMREG = NVL(cp_ToDate(_read_.AINUMREG),cp_NullValue(_read_.AINUMREG))
            this.w_AICONDET = NVL(cp_ToDate(_read_.AICONDET),cp_NullValue(_read_.AICONDET))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_PERINDV = 0
          if this.w_ACQINT="S" 
            this.w_CODINV = Nvl(CODINV,Space(5))
            this.w_IVCODIVA = IIF(Not Empty(Nvl(CODINV,Space(5))),CODINV,this.w_IVCODIVA)
            if Not Empty(this.w_CODINV)
              w_WORKAREA=ALIAS()
              this.w_SEARCH_TABLE = "VOCIIVA"
              this.w_SEARCH_IVCODIVA = this.w_CODINV
              this.Pag16()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PERINDV = Nvl(IVPERIND,0)
              if NOT EMPTY(w_WORKAREA)
                Select (w_WORKAREA)
              endif
            else
              this.w_PERINDV = this.w_IVPERIND
            endif
          else
            if Not empty(this.w_CODAUT)
              w_WORKAREA=ALIAS()
              this.w_SEARCH_TABLE = "VOCIIVA"
              this.w_SEARCH_IVCODIVA = this.w_CODAUT
              this.Pag16()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PERINDA = Nvl(IVPERIND,0)
              if NOT EMPTY(w_WORKAREA)
                Select (w_WORKAREA)
              endif
              this.w_PERINDV = this.w_PERINDA
            else
              this.w_PERINDV = this.w_IVPERIND
            endif
            this.w_IVCODIVA = IIF(Not Empty(Nvl(CODAUT,Space(5))),CODAUT,this.w_IVCODIVA)
          endif
          if i_Rows=0 OR this.w_AINUMREG=0
            * --- Read from CAUIVA1
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAUIVA1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2],.t.,this.CAUIVA1_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AINUMREG,AICONDET"+;
                " from "+i_cTable+" CAUIVA1 where ";
                    +"AICODCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                    +" and AITIPREG = "+cp_ToStrODBC("V");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AINUMREG,AICONDET;
                from (i_cTable) where;
                    AICODCAU = this.w_PNCODCAU;
                    and AITIPREG = "V";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_AINUMREG = NVL(cp_ToDate(_read_.AINUMREG),cp_NullValue(_read_.AINUMREG))
              this.w_AICONDET = NVL(cp_ToDate(_read_.AICONDET),cp_NullValue(_read_.AICONDET))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.w_AICONDET = IIF (EMPTY (this.w_AICONDET) , this.w_CCCONIVA , this.w_AICONDET )
          if NOT EMPTY(this.w_AINUMREG) AND this.w_AINUMREG>0
            this.w_ROWNUM = this.w_ROWNUM + 1
            * --- Insert into PNT_IVA
            i_nConn=i_TableProp[this.PNT_IVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_IVA_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"IVSERIAL"+",CPROWNUM"+",IVCODIVA"+",IVTIPCON"+",IVCODCON"+",IVPERIND"+",IVTIPREG"+",IVNUMREG"+",IVIMPONI"+",IVIMPIVA"+",IVFLOMAG"+",IVCFLOMA"+",IVCONTRO"+",IVTIPCOP"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_IVA','IVSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PNT_IVA','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVCODIVA),'PNT_IVA','IVCODIVA');
              +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_AICONDET),'PNT_IVA','IVCODCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PERINDV),'PNT_IVA','IVPERIND');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPREG),'PNT_IVA','IVTIPREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_AINUMREG),'PNT_IVA','IVNUMREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVIMPONI),'PNT_IVA','IVIMPONI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVIMPIVA),'PNT_IVA','IVIMPIVA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVFLOMAG),'PNT_IVA','IVFLOMAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVFLOMAG),'PNT_IVA','IVCFLOMA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVCONTRO),'PNT_IVA','IVCONTRO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPCOP),'PNT_IVA','IVTIPCOP');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'IVSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_ROWNUM,'IVCODIVA',this.w_IVCODIVA,'IVTIPCON',"G",'IVCODCON',this.w_AICONDET,'IVPERIND',this.w_PERINDV,'IVTIPREG',this.w_IVTIPREG,'IVNUMREG',this.w_AINUMREG,'IVIMPONI',this.w_IVIMPONI,'IVIMPIVA',this.w_IVIMPIVA,'IVFLOMAG',this.w_IVFLOMAG,'IVCFLOMA',this.w_IVFLOMAG)
              insert into (i_cTable) (IVSERIAL,CPROWNUM,IVCODIVA,IVTIPCON,IVCODCON,IVPERIND,IVTIPREG,IVNUMREG,IVIMPONI,IVIMPIVA,IVFLOMAG,IVCFLOMA,IVCONTRO,IVTIPCOP &i_ccchkf. );
                 values (;
                   this.w_PNSERIAL;
                   ,this.w_ROWNUM;
                   ,this.w_IVCODIVA;
                   ,"G";
                   ,this.w_AICONDET;
                   ,this.w_PERINDV;
                   ,this.w_IVTIPREG;
                   ,this.w_AINUMREG;
                   ,this.w_IVIMPONI;
                   ,this.w_IVIMPIVA;
                   ,this.w_IVFLOMAG;
                   ,this.w_IVFLOMAG;
                   ,this.w_IVCONTRO;
                   ,this.w_IVTIPCOP;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            this.w_ROWNUM = this.w_ROWNUM + 1
            * --- Insert into PNT_IVA
            i_nConn=i_TableProp[this.PNT_IVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_IVA_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"IVSERIAL"+",CPROWNUM"+",IVCODIVA"+",IVTIPCON"+",IVCODCON"+",IVPERIND"+",IVTIPREG"+",IVNUMREG"+",IVIMPONI"+",IVIMPIVA"+",IVFLOMAG"+",IVCFLOMA"+",IVCONTRO"+",IVTIPCOP"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_IVA','IVSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PNT_IVA','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVCODIVA),'PNT_IVA','IVCODIVA');
              +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_AICONDET),'PNT_IVA','IVCODCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PERINDV),'PNT_IVA','IVPERIND');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPREG),'PNT_IVA','IVTIPREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_IVA','IVNUMREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVIMPONI),'PNT_IVA','IVIMPONI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVIMPIVA),'PNT_IVA','IVIMPIVA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVFLOMAG),'PNT_IVA','IVFLOMAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVFLOMAG),'PNT_IVA','IVCFLOMA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVCONTRO),'PNT_IVA','IVCONTRO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPCOP),'PNT_IVA','IVTIPCOP');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'IVSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_ROWNUM,'IVCODIVA',this.w_IVCODIVA,'IVTIPCON',"G",'IVCODCON',this.w_AICONDET,'IVPERIND',this.w_PERINDV,'IVTIPREG',this.w_IVTIPREG,'IVNUMREG',this.w_PNNUMREG,'IVIMPONI',this.w_IVIMPONI,'IVIMPIVA',this.w_IVIMPIVA,'IVFLOMAG',this.w_IVFLOMAG,'IVCFLOMA',this.w_IVFLOMAG)
              insert into (i_cTable) (IVSERIAL,CPROWNUM,IVCODIVA,IVTIPCON,IVCODCON,IVPERIND,IVTIPREG,IVNUMREG,IVIMPONI,IVIMPIVA,IVFLOMAG,IVCFLOMA,IVCONTRO,IVTIPCOP &i_ccchkf. );
                 values (;
                   this.w_PNSERIAL;
                   ,this.w_ROWNUM;
                   ,this.w_IVCODIVA;
                   ,"G";
                   ,this.w_AICONDET;
                   ,this.w_PERINDV;
                   ,this.w_IVTIPREG;
                   ,this.w_PNNUMREG;
                   ,this.w_IVIMPONI;
                   ,this.w_IVIMPIVA;
                   ,this.w_IVFLOMAG;
                   ,this.w_IVFLOMAG;
                   ,this.w_IVCONTRO;
                   ,this.w_IVTIPCOP;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      endif
      * --- Se il Documento e' un fattura Corrispettivi, Storna l'IVA
      if this.w_PNTIPDOC="FC"
        this.w_ROWNUM = this.w_ROWNUM + 1
        * --- Insert into PNT_IVA
        i_nConn=i_TableProp[this.PNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_IVA_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IVSERIAL"+",CPROWNUM"+",IVCODIVA"+",IVTIPCON"+",IVCODCON"+",IVPERIND"+",IVTIPREG"+",IVNUMREG"+",IVIMPONI"+",IVIMPIVA"+",IVFLOMAG"+",IVCFLOMA"+",IVCONTRO"+",IVTIPCOP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_IVA','IVSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PNT_IVA','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVCODIVA),'PNT_IVA','IVCODIVA');
          +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CCCONIVA),'PNT_IVA','IVCODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVPERIND),'PNT_IVA','IVPERIND');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPREG),'PNT_IVA','IVTIPREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_IVA','IVNUMREG');
          +","+cp_NullLink(cp_ToStrODBC(-this.w_IVIMPONI),'PNT_IVA','IVIMPONI');
          +","+cp_NullLink(cp_ToStrODBC(-this.w_IVIMPIVA),'PNT_IVA','IVIMPIVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVFLOMAG),'PNT_IVA','IVFLOMAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVFLOMAG),'PNT_IVA','IVCFLOMA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVCONTRO),'PNT_IVA','IVCONTRO');
          +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCOP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IVSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_ROWNUM,'IVCODIVA',this.w_IVCODIVA,'IVTIPCON',"G",'IVCODCON',this.w_CCCONIVA,'IVPERIND',this.w_IVPERIND,'IVTIPREG',this.w_IVTIPREG,'IVNUMREG',this.w_PNNUMREG,'IVIMPONI',-this.w_IVIMPONI,'IVIMPIVA',-this.w_IVIMPIVA,'IVFLOMAG',this.w_IVFLOMAG,'IVCFLOMA',this.w_IVFLOMAG)
          insert into (i_cTable) (IVSERIAL,CPROWNUM,IVCODIVA,IVTIPCON,IVCODCON,IVPERIND,IVTIPREG,IVNUMREG,IVIMPONI,IVIMPIVA,IVFLOMAG,IVCFLOMA,IVCONTRO,IVTIPCOP &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_ROWNUM;
               ,this.w_IVCODIVA;
               ,"G";
               ,this.w_CCCONIVA;
               ,this.w_IVPERIND;
               ,this.w_IVTIPREG;
               ,this.w_PNNUMREG;
               ,-this.w_IVIMPONI;
               ,-this.w_IVIMPIVA;
               ,this.w_IVFLOMAG;
               ,this.w_IVFLOMAG;
               ,this.w_IVCONTRO;
               ,"G";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      SELECT AppIva
      ENDSCAN 
      * --- Eventuale Differenza tra Totale Documento e Importi su castelletto IVA da riportare come Fuori Campo IVA
      this.w_DIFCON = cp_ROUND(ABS(this.w_TOTDOC) - ABS(this.w_TOTIVA), this.w_DECTOP)
    endif
    if USED("AppIva")
      * --- Chiude
      SELECT AppIva
      USE
    endif
    * --- Se Previste Righe IVA Sconto Merce
    if this.w_SCOMER=100
      FOR L_i = 1 TO 10
      if NOT EMPTY(SMER[L_i,1])
        this.w_ROWNUM = this.w_ROWNUM + 1
        this.w_IVCODIVA = SMER[L_i,1]
        this.w_IVTIPREG = this.w_PNTIPREG
        this.w_IVCONTRO = SMER[L_i,2]
        * --- Insert into PNT_IVA
        i_nConn=i_TableProp[this.PNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_IVA_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IVSERIAL"+",CPROWNUM"+",IVCODIVA"+",IVTIPCON"+",IVCODCON"+",IVPERIND"+",IVTIPREG"+",IVNUMREG"+",IVIMPONI"+",IVIMPIVA"+",IVFLOMAG"+",IVCFLOMA"+",IVCONTRO"+",IVTIPCOP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_IVA','IVSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PNT_IVA','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVCODIVA),'PNT_IVA','IVCODIVA');
          +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CCCONIVA),'PNT_IVA','IVCODCON');
          +","+cp_NullLink(cp_ToStrODBC(0),'PNT_IVA','IVPERIND');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPREG),'PNT_IVA','IVTIPREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_IVA','IVNUMREG');
          +","+cp_NullLink(cp_ToStrODBC(0),'PNT_IVA','IVIMPONI');
          +","+cp_NullLink(cp_ToStrODBC(0),'PNT_IVA','IVIMPIVA');
          +","+cp_NullLink(cp_ToStrODBC("S"),'PNT_IVA','IVFLOMAG');
          +","+cp_NullLink(cp_ToStrODBC("S"),'PNT_IVA','IVCFLOMA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IVCONTRO),'PNT_IVA','IVCONTRO');
          +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCOP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IVSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_ROWNUM,'IVCODIVA',this.w_IVCODIVA,'IVTIPCON',"G",'IVCODCON',this.w_CCCONIVA,'IVPERIND',0,'IVTIPREG',this.w_IVTIPREG,'IVNUMREG',this.w_PNNUMREG,'IVIMPONI',0,'IVIMPIVA',0,'IVFLOMAG',"S",'IVCFLOMA',"S")
          insert into (i_cTable) (IVSERIAL,CPROWNUM,IVCODIVA,IVTIPCON,IVCODCON,IVPERIND,IVTIPREG,IVNUMREG,IVIMPONI,IVIMPIVA,IVFLOMAG,IVCFLOMA,IVCONTRO,IVTIPCOP &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_ROWNUM;
               ,this.w_IVCODIVA;
               ,"G";
               ,this.w_CCCONIVA;
               ,0;
               ,this.w_IVTIPREG;
               ,this.w_PNNUMREG;
               ,0;
               ,0;
               ,"S";
               ,"S";
               ,this.w_IVCONTRO;
               ,"G";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        if this.w_ACQINT="S" OR this.w_ACQAUT="S"
          * --- Se documento Intra con importi a zero devo creare riga registro vendite di tipo
          *     Sconto merce con importi a zero
          this.w_IVTIPREG = "V"
          * --- Leggo il registro IVA
          * --- Read from CAUIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAUIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA_idx,2],.t.,this.CAUIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AINUMREG,AICONDET"+;
              " from "+i_cTable+" CAUIVA where ";
                  +"AICODCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                  +" and AITIPCLF = "+cp_ToStrODBC(this.w_PNTIPCLF);
                  +" and AICODCLF = "+cp_ToStrODBC(this.w_PNCODCLF);
                  +" and AITIPREG = "+cp_ToStrODBC("V");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AINUMREG,AICONDET;
              from (i_cTable) where;
                  AICODCAU = this.w_PNCODCAU;
                  and AITIPCLF = this.w_PNTIPCLF;
                  and AICODCLF = this.w_PNCODCLF;
                  and AITIPREG = "V";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_AINUMREG = NVL(cp_ToDate(_read_.AINUMREG),cp_NullValue(_read_.AINUMREG))
            this.w_AICONDET = NVL(cp_ToDate(_read_.AICONDET),cp_NullValue(_read_.AICONDET))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows=0 OR this.w_AINUMREG=0
            * --- Read from CAUIVA1
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAUIVA1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2],.t.,this.CAUIVA1_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AINUMREG,AICONDET"+;
                " from "+i_cTable+" CAUIVA1 where ";
                    +"AICODCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                    +" and AITIPREG = "+cp_ToStrODBC("V");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AINUMREG,AICONDET;
                from (i_cTable) where;
                    AICODCAU = this.w_PNCODCAU;
                    and AITIPREG = "V";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_AINUMREG = NVL(cp_ToDate(_read_.AINUMREG),cp_NullValue(_read_.AINUMREG))
              this.w_AICONDET = NVL(cp_ToDate(_read_.AICONDET),cp_NullValue(_read_.AICONDET))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.w_AICONDET = IIF (EMPTY (this.w_AICONDET) , this.w_CCCONIVA , this.w_AICONDET )
          if NOT EMPTY(this.w_AINUMREG) AND this.w_AINUMREG>0
            this.w_ROWNUM = this.w_ROWNUM + 1
            * --- Insert into PNT_IVA
            i_nConn=i_TableProp[this.PNT_IVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_IVA_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"IVSERIAL"+",CPROWNUM"+",IVCODIVA"+",IVTIPCON"+",IVCODCON"+",IVPERIND"+",IVTIPREG"+",IVNUMREG"+",IVIMPONI"+",IVIMPIVA"+",IVFLOMAG"+",IVCFLOMA"+",IVCONTRO"+",IVTIPCOP"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_IVA','IVSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PNT_IVA','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVCODIVA),'PNT_IVA','IVCODIVA');
              +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_AICONDET),'PNT_IVA','IVCODCON');
              +","+cp_NullLink(cp_ToStrODBC(0),'PNT_IVA','IVPERIND');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPREG),'PNT_IVA','IVTIPREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_AINUMREG),'PNT_IVA','IVNUMREG');
              +","+cp_NullLink(cp_ToStrODBC(0),'PNT_IVA','IVIMPONI');
              +","+cp_NullLink(cp_ToStrODBC(0),'PNT_IVA','IVIMPIVA');
              +","+cp_NullLink(cp_ToStrODBC("S"),'PNT_IVA','IVFLOMAG');
              +","+cp_NullLink(cp_ToStrODBC("S"),'PNT_IVA','IVCFLOMA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IVCONTRO),'PNT_IVA','IVCONTRO');
              +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCOP');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'IVSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_ROWNUM,'IVCODIVA',this.w_IVCODIVA,'IVTIPCON',"G",'IVCODCON',this.w_AICONDET,'IVPERIND',0,'IVTIPREG',this.w_IVTIPREG,'IVNUMREG',this.w_AINUMREG,'IVIMPONI',0,'IVIMPIVA',0,'IVFLOMAG',"S",'IVCFLOMA',"S")
              insert into (i_cTable) (IVSERIAL,CPROWNUM,IVCODIVA,IVTIPCON,IVCODCON,IVPERIND,IVTIPREG,IVNUMREG,IVIMPONI,IVIMPIVA,IVFLOMAG,IVCFLOMA,IVCONTRO,IVTIPCOP &i_ccchkf. );
                 values (;
                   this.w_PNSERIAL;
                   ,this.w_ROWNUM;
                   ,this.w_IVCODIVA;
                   ,"G";
                   ,this.w_AICONDET;
                   ,0;
                   ,this.w_IVTIPREG;
                   ,this.w_AINUMREG;
                   ,0;
                   ,0;
                   ,"S";
                   ,"S";
                   ,this.w_IVCONTRO;
                   ,"G";
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
      endif
      ENDFOR
    endif
    if this.w_DIFCON<>0 AND NOT EMPTY(g_COIDIF)
      * --- Se presente Differenza di Conversione aggiunge una riga nel castelletto Iva
      this.w_APPO = g_COIDIF
      w_WORKAREA=ALIAS()
      this.w_SEARCH_TABLE = "VOCIIVA"
      this.w_SEARCH_IVCODIVA = this.w_APPO
      this.Pag16()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PERIND = Nvl(IVPERIND,0)
      if NOT EMPTY(w_WORKAREA)
        Select (w_WORKAREA)
      endif
      this.w_IVCONTRO = IIF(g_DETCON="S", this.oParentObject.w_CONDIC, SPACE(15))
      this.w_ROWNUM = this.w_ROWNUM + 1
      * --- Insert into PNT_IVA
      i_nConn=i_TableProp[this.PNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_IVA_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"IVSERIAL"+",CPROWNUM"+",IVCODIVA"+",IVTIPCON"+",IVCODCON"+",IVPERIND"+",IVTIPREG"+",IVNUMREG"+",IVIMPONI"+",IVIMPIVA"+",IVFLOMAG"+",IVCFLOMA"+",IVCONTRO"+",IVTIPCOP"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_IVA','IVSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PNT_IVA','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_APPO),'PNT_IVA','IVCODIVA');
        +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCONIVA),'PNT_IVA','IVCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PERIND),'PNT_IVA','IVPERIND');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_IVA','IVTIPREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_IVA','IVNUMREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DIFCON),'PNT_IVA','IVIMPONI');
        +","+cp_NullLink(cp_ToStrODBC(0),'PNT_IVA','IVIMPIVA');
        +","+cp_NullLink(cp_ToStrODBC("X"),'PNT_IVA','IVFLOMAG');
        +","+cp_NullLink(cp_ToStrODBC("X"),'PNT_IVA','IVCFLOMA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IVCONTRO),'PNT_IVA','IVCONTRO');
        +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCOP');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'IVSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_ROWNUM,'IVCODIVA',this.w_APPO,'IVTIPCON',"G",'IVCODCON',this.w_CCCONIVA,'IVPERIND',this.w_PERIND,'IVTIPREG',this.w_PNTIPREG,'IVNUMREG',this.w_PNNUMREG,'IVIMPONI',this.w_DIFCON,'IVIMPIVA',0,'IVFLOMAG',"X",'IVCFLOMA',"X")
        insert into (i_cTable) (IVSERIAL,CPROWNUM,IVCODIVA,IVTIPCON,IVCODCON,IVPERIND,IVTIPREG,IVNUMREG,IVIMPONI,IVIMPIVA,IVFLOMAG,IVCFLOMA,IVCONTRO,IVTIPCOP &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.w_ROWNUM;
             ,this.w_APPO;
             ,"G";
             ,this.w_CCCONIVA;
             ,this.w_PERIND;
             ,this.w_PNTIPREG;
             ,this.w_PNNUMREG;
             ,this.w_DIFCON;
             ,0;
             ,"X";
             ,"X";
             ,this.w_IVCONTRO;
             ,"G";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_MESS_ERR = ah_MsgFormat("%1%0Inserita riga di fuori campo nel castelletto IVA per un importo pari a %2",Repl("-",50),Alltrim(Tran((this.w_DIFCON),v_PV[38+VVL])))
      this.Page_14()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ASPE[] - Vettore Ripartizioni Spese Accessorie x Aliquota IVA : 1^Indice Num.Aliquota
    * --- 2^Indice : 1 =Codice IVA
    * --- 2 =Spese Incasso
    * --- 3 =Spese Imballo
    * --- 4 =Spese Trasporto
    * --- 5 =Imponibile
    * --- 6 =% IVA
    * --- Variabili Locali
    * --- Spese Accessorie da Ripartire (senza Codice IVA)
    this.w_ELAINC = IIF(EMPTY(this.w_IVAINC), this.w_SPEINC, 0)
    this.w_ELAIMB = IIF(EMPTY(this.w_IVAIMB), this.w_SPEIMB, 0)
    this.w_ELATRA = IIF(EMPTY(this.w_IVATRA), this.w_SPETRA, 0)
    * --- Riazzera le spese da Rielaborare
    this.w_SPEINC = IIF(EMPTY(this.w_IVAINC), 0, this.w_SPEINC)
    this.w_SPEIMB = IIF(EMPTY(this.w_IVAIMB), 0, this.w_SPEIMB)
    this.w_SPETRA = IIF(EMPTY(this.w_IVATRA), 0, this.w_SPETRA)
    DIMENSION ASPE[6, 6]
    FOR i = 1 TO 6
    ASPE[i, 1] = SPACE(5)
    ASPE[i, 2] = 0
    ASPE[i, 3] = 0
    ASPE[i, 4] = 0
    ASPE[i, 5] = 0
    ASPE[i, 6] = 0
    ENDFOR
    * --- Aliquote IVA
    this.w_ALIVA = 0
    this.w_TOTMER = 0
    * --- Lettura righe Fattura dal Temporaneo
    SELECT DettDocu
    GO TOP
    SCAN FOR VALRIG<>0 AND FLOMAG="X"
    * --- Netto Merce (o Lordo se Gestito Scorporo Piede Fattura)
    this.w_TOTMER = this.w_TOTMER + RIGLOR
    * --- Calcolo aliquote castelletto IVA
    this.w_TROV = .F.
    FOR i = 1 TO this.w_ALIVA
    if ASPE[i, 1] = CODIVA
      * --- Aliquota gia' esistente
      ASPE[i, 5] = ASPE[i, 5] + RIGLOR
      this.w_TROV = .T.
      EXIT
    endif
    ENDFOR
    if this.w_TROV = .F.
      * --- Se aliquota inesistente
      this.w_ALIVA = this.w_ALIVA + 1
      * --- Aggiunge nuova aliquota
      this.w_APPO1 = 0
      this.w_APPO = CODIVA
      w_WORKAREA=ALIAS()
      this.w_SEARCH_TABLE = "VOCIIVA"
      this.w_SEARCH_IVCODIVA = this.w_APPO
      this.Pag16()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_APPO1 = Nvl(IVPERIVA,0)
      if NOT EMPTY(w_WORKAREA)
        Select (w_WORKAREA)
      endif
      SELECT DettDocu
      ASPE[this.w_ALIVA, 1] = CODIVA
      ASPE[this.w_ALIVA, 5] = RIGLOR
      ASPE[this.w_ALIVA, 6] = this.w_APPO1
    endif
    ENDSCAN
    this.w_APPINC = this.w_ELAINC
    this.w_APPIMB = this.w_ELAIMB
    this.w_APPTRA = this.w_ELATRA
    if this.w_TOTMER<>0
      FOR i = 1 TO this.w_ALIVA
      * --- Se Aliquota Imponibile
      if NOT EMPTY(ASPE[i, 1])
        if this.w_ELAINC<>0
          ASPE[i, 2] = cp_ROUND((this.w_ELAINC * ASPE[i, 5]) / this.w_TOTMER, this.w_DECTOT)
          this.w_APPINC = this.w_APPINC - ASPE[i, 2]
        endif
        if this.w_ELAIMB<>0
          ASPE[i, 3] = cp_ROUND((this.w_ELAIMB * ASPE[i, 5]) / this.w_TOTMER, this.w_DECTOT)
          this.w_APPIMB = this.w_APPIMB - ASPE[i, 3]
        endif
        if this.w_ELATRA<>0
          ASPE[i, 4] = cp_ROUND((this.w_ELATRA * ASPE[i, 5]) / this.w_TOTMER, this.w_DECTOT)
          this.w_APPTRA = this.w_APPTRA - ASPE[i, 4]
        endif
      endif
      ENDFOR
    endif
    if this.w_APPINC<>0 OR this.w_APPIMB<>0 OR this.w_APPTRA<>0
      * --- Aggiunge alla Prima Aliquota l'eventuale Resto
      FOR i = 1 TO this.w_ALIVA
      if NOT EMPTY(ASPE[i, 1])
        * --- Se Aliquota Imponibile
        ASPE[i, 2] = ASPE[i, 2] + this.w_APPINC
        ASPE[i, 3] = ASPE[i, 3] + this.w_APPIMB
        ASPE[i, 4] = ASPE[i, 4] + this.w_APPTRA
        EXIT
      endif
      ENDFOR
    endif
    * --- Calcola Imponibile Spese Accessorie 
    FOR L_i = 1 TO this.w_ALIVA
    if NOT EMPTY(ASPE[L_i, 1])
      * --- Scorporo dell' IVA ...
      if this.w_ELAINC<>0 AND ASPE[L_i, 2]<>0
        this.w_APPO = ASPE[L_i, 2]
        this.w_APPO1 = 0
        this.w_IMPNET = this.w_APPO
        if ASPE[L_i, 6]<>0
          this.w_IMPNET = cp_ROUND((this.w_APPO / (1 + (ASPE[L_i, 6] / 100))) + IIF(this.w_PNCODVAL=g_CODLIR, .499, 0), this.w_DECTOT)
          this.w_APPO1 = this.w_APPO - this.w_IMPNET
        endif
        this.w_SPEINC = this.w_SPEINC + this.w_IMPNET
      endif
      if this.w_ELAIMB<>0 AND ASPE[L_i, 3]<>0
        this.w_APPO = ASPE[L_i, 3]
        this.w_APPO1 = 0
        this.w_IMPNET = this.w_APPO
        if ASPE[L_i, 6]<>0
          this.w_IMPNET = cp_ROUND((this.w_APPO / (1 + (ASPE[L_i, 6] / 100))) + IIF(this.w_PNCODVAL=g_CODLIR, .499, 0), this.w_DECTOT)
          this.w_APPO1 = this.w_APPO - this.w_IMPNET
        endif
        this.w_SPEIMB = this.w_SPEIMB + this.w_IMPNET
      endif
      if this.w_ELATRA<>0 AND ASPE[L_i, 4]<>0
        this.w_APPO = ASPE[L_i, 4]
        this.w_APPO1 = 0
        this.w_IMPNET = this.w_APPO
        if ASPE[L_i, 6]<>0
          this.w_IMPNET = cp_ROUND((this.w_APPO / (1 + (ASPE[L_i, 6] / 100))) + IIF(this.w_PNCODVAL=g_CODLIR, .499, 0), this.w_DECTOT)
          this.w_APPO1 = this.w_APPO - this.w_IMPNET
        endif
        this.w_SPETRA = this.w_SPETRA + this.w_IMPNET
      endif
    endif
    ENDFOR
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Locali
    * --- Variabili Accorpamento Acconti
    * --- Variabili Analitica
    this.w_LRITPRE = 0
    * --- Controllo se causale no iva e iva esente
    * --- Controllo se importo Cli/for = IVA e Omaggio imponibile
    * --- Ritenuta attiva
  endproc


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Partite, pilotato dalla variabile w_GESTPAR
    do case
      case this.w_GESTPAR = "COMPENSA"
        * --- Partita di compensazione (Da pag4)
        *     Leggo le partite create sulla riga intestatario di tipo pagamento compensazione.
        *     Per ognuna di queste (normalmente una), creo una partita di saldo
        this.w_ROWCOMP = 0
        this.w_ROWNUM = this.w_CPROWNUM
        * --- Select from PAR_TITE
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PTMODPAG,PTTOTIMP,PTMODPAG,PTDATSCA,PTNUMPAR,PTFLSOSP,PTBANNOS,PTBANAPP,CPROWNUM,PTNUMCOR  from "+i_cTable+" PAR_TITE ";
              +" where PTSERIAL="+cp_ToStrODBC(this.w_PNSERIAL)+" And PTROWORD=1";
               ,"_Curs_PAR_TITE")
        else
          select PTMODPAG,PTTOTIMP,PTMODPAG,PTDATSCA,PTNUMPAR,PTFLSOSP,PTBANNOS,PTBANAPP,CPROWNUM,PTNUMCOR from (i_cTable);
           where PTSERIAL=this.w_PNSERIAL And PTROWORD=1;
            into cursor _Curs_PAR_TITE
        endif
        if used('_Curs_PAR_TITE')
          select _Curs_PAR_TITE
          locate for 1=1
          do while not(eof())
          * --- Read from MOD_PAGA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2],.t.,this.MOD_PAGA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MPTIPPAG"+;
              " from "+i_cTable+" MOD_PAGA where ";
                  +"MPCODICE = "+cp_ToStrODBC(_Curs_PAR_TITE.PTMODPAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MPTIPPAG;
              from (i_cTable) where;
                  MPCODICE = _Curs_PAR_TITE.PTMODPAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPPAG = NVL(cp_ToDate(_read_.MPTIPPAG),cp_NullValue(_read_.MPTIPPAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_TIPPAG="CC"
            this.w_PAGCON = _Curs_PAR_TITE.PTMODPAG
            this.w_PTTOTIMP = _Curs_PAR_TITE.PTTOTIMP
            this.w_PTDATSCA = _Curs_PAR_TITE.PTDATSCA
            this.w_PTNUMPAR = _Curs_PAR_TITE.PTNUMPAR
            this.w_PTFLSOSP = _Curs_PAR_TITE.PTFLSOSP
            this.w_PTBANNOS = _Curs_PAR_TITE.PTBANNOS
            this.w_PTBANAPP = _Curs_PAR_TITE.PTBANAPP
            this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
            this.w_ROWCOMP = this.w_ROWCOMP + 1
            this.w_RIFCOMP = _Curs_PAR_TITE.CPROWNUM
            this.w_PTNUMCOR = _Curs_PAR_TITE.PTNUMCOR
            * --- Insert into PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMDIS"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTFLCRSA"+",PTFLIMPE"+",PTTOTABB"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','PTROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWCOMP),'PAR_TITE','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PAGCON),'PAR_TITE','PTMODPAG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
              +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
              +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
              +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
              +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLCRSA');
              +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
              +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODAGE),'PAR_TITE','PTCODAGE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLVABD),'PAR_TITE','PTFLVABD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERRIF');
              +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTORDRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_RIFCOMP),'PAR_TITE','PTNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_ROWNUM,'CPROWNUM',this.w_ROWCOMP,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PT_SEGNO',this.w_APPSEZ,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PNCODVAL,'PTCAOVAL',this.w_PNCAOVAL,'PTCAOAPE',this.w_PNCAOVAL)
              insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMDIS,PTFLINDI,PTBANNOS,PTFLRAGG,PTFLCRSA,PTFLIMPE,PTTOTABB,PTDESRIG,PTCODAGE,PTFLVABD,PTSERRIF,PTORDRIF,PTNUMRIF,PTDATREG,PTNUMCOR &i_ccchkf. );
                 values (;
                   this.w_PNSERIAL;
                   ,this.w_ROWNUM;
                   ,this.w_ROWCOMP;
                   ,this.w_PTNUMPAR;
                   ,this.w_PTDATSCA;
                   ,this.w_PNTIPCLF;
                   ,this.w_PNCODCLF;
                   ,this.w_APPSEZ;
                   ,this.w_PTTOTIMP;
                   ,this.w_PNCODVAL;
                   ,this.w_PNCAOVAL;
                   ,this.w_PNCAOVAL;
                   ,this.w_PNDATDOC;
                   ,this.w_PNNUMDOC;
                   ,this.w_PNALFDOC;
                   ,this.w_PNDATDOC;
                   ,this.w_PNTOTDOC;
                   ,this.w_PAGCON;
                   ,this.w_PTFLSOSP;
                   ,this.w_PTBANAPP;
                   ," ";
                   ," ";
                   ,this.w_PTBANNOS;
                   ," ";
                   ,"S";
                   ,"  ";
                   ,0;
                   ,this.w_PNDESSUP;
                   ,this.w_PNCODAGE;
                   ,this.w_PNFLVABD;
                   ,this.w_PNSERIAL;
                   ,1;
                   ,this.w_RIFCOMP;
                   ,this.w_PNDATREG;
                   ,this.w_PTNUMCOR;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
            select _Curs_PAR_TITE
            continue
          enddo
          use
        endif
      case this.w_GESTPAR = "RITENUTE" OR this.w_GESTPAR = "ENASARCO"
        if this.w_PNFLPART="S"
          * --- Aggiorna Partite a Saldo
          this.w_ROWNUM = 0
          this.w_TOTPAR = 0
          this.w_RESTO = this.w_ABDIFF
          this.w_ROWINI = 0
          * --- Leggo il CPROWNUM pi� alto per aggiungere altre partite..
          * --- Select from PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select Max( CPROWNUM ) As Riga  from "+i_cTable+" PAR_TITE ";
                +" where PTSERIAL="+cp_ToStrODBC(this.w_PNSERIAL)+" And PTROWORD=1";
                 ,"_Curs_PAR_TITE")
          else
            select Max( CPROWNUM ) As Riga from (i_cTable);
             where PTSERIAL=this.w_PNSERIAL And PTROWORD=1;
              into cursor _Curs_PAR_TITE
          endif
          if used('_Curs_PAR_TITE')
            select _Curs_PAR_TITE
            locate for 1=1
            do while not(eof())
            this.w_RIFORD = Nvl( _Curs_PAR_TITE.RIGA , 0 )
              select _Curs_PAR_TITE
              continue
            enddo
            use
          endif
          this.w_RIFOLD = this.w_RIFORD+1
          * --- Se ho creato alemno una partita non di acconto sull'intestatario le scorro
          *     altrimenti genero una partita pari all'acconto delle ritenute...
          if this.w_PARFOR<>0
            * --- Scorro le partite dell'intestatario per costruzione poste nella riga 1
            *     di creazione (escludo eventuali acconti nel documento o partite Sospese)
            * --- Gestisco il caso di una partita sospesa non della ritenuta.
            *     Caso storno immediato esclude le sospese, caso storno differito le considera
            if this.w_TRFLGSTO="S"
              * --- Select from PAR_TITE
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
                    +" where PTSERIAL="+cp_ToStrODBC(this.w_PNSERIAL)+" AND PTROWORD=1 AND PTTOTIMP<>0 and PTFLCRSA='C' and PTFLSOSP<>'S'";
                    +" order by CPROWNUM";
                     ,"_Curs_PAR_TITE")
              else
                select * from (i_cTable);
                 where PTSERIAL=this.w_PNSERIAL AND PTROWORD=1 AND PTTOTIMP<>0 and PTFLCRSA="C" and PTFLSOSP<>"S";
                 order by CPROWNUM;
                  into cursor _Curs_PAR_TITE
              endif
              if used('_Curs_PAR_TITE')
                select _Curs_PAR_TITE
                locate for 1=1
                do while not(eof())
                * --- Devo verificare se tra le partite ve ne sono alcune relative ad un acconto
                *     abbinato. Queste escono dal giro di abbinamento contestuale (Es. Ritenute).
                *     L'acconto � una partita che se esiste punta alla partita della registrazione
                *     creanda.
                *     L'acconto pu� esistere se ho acconti precedenti o contestuali oppure
                *     attivo il falg accorpa acconti.
                if ( this.w_ACCONT<> 0 ) Or ( this.w_ACCPRE<> 0 ) Or this.w_FLAACC="S"
                  * --- Read from PAR_TITE
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "PTFLCRSA"+;
                      " from "+i_cTable+" PAR_TITE where ";
                          +"PTSERRIF = "+cp_ToStrODBC(_Curs_PAR_TITE.PTSERIAL);
                          +" and PTORDRIF = "+cp_ToStrODBC(_Curs_PAR_TITE.PTROWORD);
                          +" and PTNUMRIF = "+cp_ToStrODBC(_Curs_PAR_TITE.CPROWNUM);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      PTFLCRSA;
                      from (i_cTable) where;
                          PTSERRIF = _Curs_PAR_TITE.PTSERIAL;
                          and PTORDRIF = _Curs_PAR_TITE.PTROWORD;
                          and PTNUMRIF = _Curs_PAR_TITE.CPROWNUM;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_TEST = NVL(cp_ToDate(_read_.PTFLCRSA),cp_NullValue(_read_.PTFLCRSA))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                else
                  this.w_TEST = ""
                endif
                if this.w_TEST<>"A"
                  * --- Se c'e' un Acconto, scarta la prima riga Partite (Acconto)
                  this.w_PTNUMPAR = _Curs_PAR_TITE.PTNUMPAR
                  this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
                  this.w_PTTOTIMP = NVL(_Curs_PAR_TITE.PTTOTIMP, 0)
                  this.w_PTMODPAG = _Curs_PAR_TITE.PTMODPAG
                  this.w_PTBANAPP = _Curs_PAR_TITE.PTBANAPP
                  this.w_PTDESRIG = IIF(EMPTY(NVL(_Curs_PAR_TITE.PTDESRIG, "")), this.w_PNDESSUP, PTDESRIG)
                  this.w_ROWNUM = this.w_ROWNUM + 1
                  this.w_PTNUMCOR = _Curs_PAR_TITE.PTNUMCOR
                  * --- Puntatore alla Prima Riga buona delle Partite di origine (per eventuale resto)
                  this.w_ROWINI = IIF(this.w_ROWNUM=1, _Curs_PAR_TITE.CPROWNUM , this.w_ROWINI)
                  * --- 'Spalma' sulle Partite di Apertura Altrimenti Salda per l'intero Importo
                  this.w_RIFORD = this.w_RIFORD + 1
                  if this.w_GESRIT="S" and g_RITE="S" And this.w_TRFLGSTO="N"
                    this.w_PTTOTIMP = cp_ROUND(this.w_ABDIFF , this.w_DECTOT)
                    this.w_PTDATSCA = this.w_PNDATREG
                    this.w_RESTO = 0
                    * --- Se Enasarco con storno differito  Devo creare la partita di Saldo e di Creazione
                    *     nella prima riga  del Fornitore
                    if this.w_GESTPAR="ENASARCO"
                      * --- Nel caso di Enasarco con Storno Differito devo creare Partita di saldo per l'importo 
                      *     dell'enasarco
                      * --- Insert into PAR_TITE
                      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_ccchkf=''
                      i_ccchkv=''
                      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                    " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTCODVAL"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTDATAPE"+",PTDATDOC"+",PTFLCRSA"+",PTFLIMPE"+",PTFLINDI"+",PTFLRAGG"+",PTFLSOSP"+",PTIMPDOC"+",PTMODPAG"+",PTNUMDIS"+",PTNUMDOC"+",PTTOTABB"+",PTTOTIMP"+",PTDESRIG"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
                        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATSCA');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PAR_TITE','PTFLCRSA');
                        +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                        +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                        +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERRIF');
                        +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTORDRIF');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_RIFORD),'PAR_TITE','PTNUMRIF');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                             +i_ccchkv+")")
                      else
                        cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_CPROWNUM,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PNDATDOC,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PTCODVAL',this.w_PNCODVAL,'PT_SEGNO',this.w_APPSEZ,'PTALFDOC',this.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS)
                        insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTDATAPE,PTDATDOC,PTFLCRSA,PTFLIMPE,PTFLINDI,PTFLRAGG,PTFLSOSP,PTIMPDOC,PTMODPAG,PTNUMDIS,PTNUMDOC,PTTOTABB,PTTOTIMP,PTDESRIG,PTSERRIF,PTORDRIF,PTNUMRIF,PTDATREG,PTNUMCOR &i_ccchkf. );
                           values (;
                             this.w_PNSERIAL;
                             ,this.w_CPROWNUM;
                             ,this.w_ROWNUM;
                             ,this.w_PTNUMPAR;
                             ,this.w_PNDATDOC;
                             ,this.w_PNTIPCLF;
                             ,this.w_PNCODCLF;
                             ,this.w_PNCODVAL;
                             ,this.w_APPSEZ;
                             ,this.w_PNALFDOC;
                             ,this.w_PTBANAPP;
                             ,this.w_PTBANNOS;
                             ,this.w_PNCAOVAL;
                             ,this.w_PNCAOVAL;
                             ,this.w_PNDATDOC;
                             ,this.w_PNDATDOC;
                             ,this.w_PNFLPART;
                             ,"  ";
                             ," ";
                             ," ";
                             ," ";
                             ,this.w_PNTOTDOC;
                             ,this.w_PTMODPAG;
                             ,SPACE(10);
                             ,this.w_PNNUMDOC;
                             ,0;
                             ,this.w_PTTOTIMP;
                             ,this.w_PTDESRIG;
                             ,this.w_PNSERIAL;
                             ,1;
                             ,this.w_RIFORD;
                             ,this.w_PNDATREG;
                             ,this.w_PTNUMCOR;
                             &i_ccchkv. )
                        i_Rows=iif(bTrsErr,0,1)
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if i_Rows<0 or bTrsErr
                        * --- Error: insert not accepted
                        i_Error=MSG_INSERT_ERROR
                        return
                      endif
                    endif
                    this.w_APPSEZ = IIF(this.w_SEZCLF="D", "D", "A")
                    * --- Insert into PAR_TITE
                    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTCODVAL"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTDATAPE"+",PTDATDOC"+",PTFLCRSA"+",PTFLIMPE"+",PTFLINDI"+",PTFLRAGG"+",PTFLSOSP"+",PTIMPDOC"+",PTMODPAG"+",PTNUMDIS"+",PTNUMDOC"+",PTTOTABB"+",PTTOTIMP"+",PTDESRIG"+",PTNUMCOR"+",PTDATREG"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                      +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTROWORD');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_RIFORD),'PAR_TITE','CPROWNUM');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATSCA');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                      +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
                      +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_FLSOSP),'PAR_TITE','PTFLSOSP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',1,'CPROWNUM',this.w_RIFORD,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PNDATDOC,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PTCODVAL',this.w_PNCODVAL,'PT_SEGNO',this.w_APPSEZ,'PTALFDOC',this.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS)
                      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTDATAPE,PTDATDOC,PTFLCRSA,PTFLIMPE,PTFLINDI,PTFLRAGG,PTFLSOSP,PTIMPDOC,PTMODPAG,PTNUMDIS,PTNUMDOC,PTTOTABB,PTTOTIMP,PTDESRIG,PTNUMCOR,PTDATREG &i_ccchkf. );
                         values (;
                           this.w_PNSERIAL;
                           ,1;
                           ,this.w_RIFORD;
                           ,this.w_PTNUMPAR;
                           ,this.w_PNDATDOC;
                           ,this.w_PNTIPCLF;
                           ,this.w_PNCODCLF;
                           ,this.w_PNCODVAL;
                           ,this.w_APPSEZ;
                           ,this.w_PNALFDOC;
                           ,this.w_PTBANAPP;
                           ,this.w_PTBANNOS;
                           ,this.w_PNCAOVAL;
                           ,this.w_PNCAOVAL;
                           ,this.w_PNDATDOC;
                           ,this.w_PNDATDOC;
                           ,"C";
                           ,"  ";
                           ," ";
                           ," ";
                           ,this.w_FLSOSP;
                           ,this.w_PNTOTDOC;
                           ,this.w_PTMODPAG;
                           ,SPACE(10);
                           ,this.w_PNNUMDOC;
                           ,0;
                           ,this.w_PTTOTIMP;
                           ,this.w_PNDESSUP;
                           ,this.w_PTNUMCOR;
                           ,this.w_PNDATREG;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error=MSG_INSERT_ERROR
                      return
                    endif
                    Exit
                  else
                    this.w_PTTOTIMP = cp_ROUND((this.w_ABDIFF * this.w_PTTOTIMP) / this.w_PARFOR, this.w_DECTOT)
                    this.w_PTDATSCA = _Curs_PAR_TITE.PTDATSCA
                    this.w_LTOTIMP = cp_ROUND(((this.w_ABDIFF) * NVL(_Curs_PAR_TITE.PTTOTIMP, 0)) / this.w_PARFOR, this.w_DECTOT)
                    this.w_RESTO = this.w_RESTO - this.w_PTTOTIMP
                    * --- Insert into PAR_TITE
                    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTCODAGE"+",PTCODVAL"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTDATAPE"+",PTDATDOC"+",PTFLCRSA"+",PTFLIMPE"+",PTFLINDI"+",PTFLRAGG"+",PTFLSOSP"+",PTIMPDOC"+",PTMODPAG"+",PTNUMDIS"+",PTNUMDOC"+",PTTOTABB"+",PTTOTIMP"+",PTDESRIG"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PAR_TITE','PTFLCRSA');
                      +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTSERIAL),'PAR_TITE','PTSERRIF');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTROWORD),'PAR_TITE','PTORDRIF');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.CPROWNUM),'PAR_TITE','PTNUMRIF');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_CPROWNUM,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PTCODAGE',this.w_PTCODAGE,'PTCODVAL',this.w_PNCODVAL,'PT_SEGNO',this.w_APPSEZ,'PTALFDOC',this.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP)
                      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODAGE,PTCODVAL,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTDATAPE,PTDATDOC,PTFLCRSA,PTFLIMPE,PTFLINDI,PTFLRAGG,PTFLSOSP,PTIMPDOC,PTMODPAG,PTNUMDIS,PTNUMDOC,PTTOTABB,PTTOTIMP,PTDESRIG,PTSERRIF,PTORDRIF,PTNUMRIF,PTDATREG,PTNUMCOR &i_ccchkf. );
                         values (;
                           this.w_PNSERIAL;
                           ,this.w_CPROWNUM;
                           ,this.w_ROWNUM;
                           ,this.w_PTNUMPAR;
                           ,this.w_PTDATSCA;
                           ,this.w_PNTIPCLF;
                           ,this.w_PNCODCLF;
                           ,this.w_PTCODAGE;
                           ,this.w_PNCODVAL;
                           ,this.w_APPSEZ;
                           ,this.w_PNALFDOC;
                           ,this.w_PTBANAPP;
                           ,this.w_PTBANNOS;
                           ,this.w_PNCAOVAL;
                           ,this.w_PNCAOVAL;
                           ,this.w_PNDATDOC;
                           ,this.w_PNDATDOC;
                           ,this.w_PNFLPART;
                           ,"  ";
                           ," ";
                           ," ";
                           ," ";
                           ,this.w_PNTOTDOC;
                           ,this.w_PTMODPAG;
                           ,SPACE(10);
                           ,this.w_PNNUMDOC;
                           ,0;
                           ,this.w_PTTOTIMP;
                           ,this.w_PTDESRIG;
                           ,_Curs_PAR_TITE.PTSERIAL;
                           ,_Curs_PAR_TITE.PTROWORD;
                           ,_Curs_PAR_TITE.CPROWNUM;
                           ,this.w_PNDATREG;
                           ,this.w_PTNUMCOR;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error=MSG_INSERT_ERROR
                      return
                    endif
                    * --- Aggiorno importo partita di apertura per la parte relativa alle ritenute.
                    * --- Write into PAR_TITE
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"PTTOTIMP =PTTOTIMP+ "+cp_ToStrODBC(this.w_LTOTIMP);
                          +i_ccchkf ;
                      +" where ";
                          +"PTSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                          +" and PTROWORD = "+cp_ToStrODBC(1);
                          +" and CPROWNUM = "+cp_ToStrODBC(_Curs_PAR_TITE.CPROWNUM);
                             )
                    else
                      update (i_cTable) set;
                          PTTOTIMP = PTTOTIMP + this.w_LTOTIMP;
                          &i_ccchkf. ;
                       where;
                          PTSERIAL = this.w_PNSERIAL;
                          and PTROWORD = 1;
                          and CPROWNUM = _Curs_PAR_TITE.CPROWNUM;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                endif
                  select _Curs_PAR_TITE
                  continue
                enddo
                use
              endif
            else
              * --- Select from PAR_TITE
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
                    +" where PTSERIAL="+cp_ToStrODBC(this.w_PNSERIAL)+" AND PTROWORD=1 AND PTTOTIMP<>0 and PTFLCRSA='C'";
                    +" order by CPROWNUM";
                     ,"_Curs_PAR_TITE")
              else
                select * from (i_cTable);
                 where PTSERIAL=this.w_PNSERIAL AND PTROWORD=1 AND PTTOTIMP<>0 and PTFLCRSA="C";
                 order by CPROWNUM;
                  into cursor _Curs_PAR_TITE
              endif
              if used('_Curs_PAR_TITE')
                select _Curs_PAR_TITE
                locate for 1=1
                do while not(eof())
                * --- Devo verificare se tra le partite ve ne sono alcune relative ad un acconto
                *     abbinato. Queste escono dal giro di abbinamento contestuale (Es. Ritenute).
                *     L'acconto � una partita che se esiste punta alla partita della registrazione
                *     creanda.
                *     L'acconto pu� esistere se ho acconti precedenti o contestuali oppure
                *     attivo il falg accorpa acconti.
                if ( this.w_ACCONT<> 0 ) Or ( this.w_ACCPRE<> 0 ) Or this.w_FLAACC="S"
                  * --- Read from PAR_TITE
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "PTFLCRSA"+;
                      " from "+i_cTable+" PAR_TITE where ";
                          +"PTSERRIF = "+cp_ToStrODBC(_Curs_PAR_TITE.PTSERIAL);
                          +" and PTORDRIF = "+cp_ToStrODBC(_Curs_PAR_TITE.PTROWORD);
                          +" and PTNUMRIF = "+cp_ToStrODBC(_Curs_PAR_TITE.CPROWNUM);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      PTFLCRSA;
                      from (i_cTable) where;
                          PTSERRIF = _Curs_PAR_TITE.PTSERIAL;
                          and PTORDRIF = _Curs_PAR_TITE.PTROWORD;
                          and PTNUMRIF = _Curs_PAR_TITE.CPROWNUM;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_TEST = NVL(cp_ToDate(_read_.PTFLCRSA),cp_NullValue(_read_.PTFLCRSA))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                else
                  this.w_TEST = ""
                endif
                if this.w_TEST<>"A"
                  * --- Se c'e' un Acconto, scarta la prima riga Partite (Acconto)
                  this.w_PTNUMPAR = _Curs_PAR_TITE.PTNUMPAR
                  this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
                  this.w_PTTOTIMP = NVL(_Curs_PAR_TITE.PTTOTIMP, 0)
                  this.w_PTMODPAG = _Curs_PAR_TITE.PTMODPAG
                  this.w_PTBANAPP = _Curs_PAR_TITE.PTBANAPP
                  this.w_PTDESRIG = IIF(EMPTY(NVL(_Curs_PAR_TITE.PTDESRIG, "")), this.w_PNDESSUP, PTDESRIG)
                  this.w_ROWNUM = this.w_ROWNUM + 1
                  this.w_PTNUMCOR = _Curs_PAR_TITE.PTNUMCOR
                  * --- Puntatore alla Prima Riga buona delle Partite di origine (per eventuale resto)
                  this.w_ROWINI = IIF(this.w_ROWNUM=1, _Curs_PAR_TITE.CPROWNUM , this.w_ROWINI)
                  * --- 'Spalma' sulle Partite di Apertura Altrimenti Salda per l'intero Importo
                  this.w_RIFORD = this.w_RIFORD + 1
                  if this.w_GESRIT="S" and g_RITE="S" And this.w_TRFLGSTO="N"
                    this.w_PTTOTIMP = cp_ROUND(this.w_ABDIFF , this.w_DECTOT)
                    this.w_PTDATSCA = this.w_PNDATREG
                    this.w_RESTO = 0
                    * --- Se Enasarco con storno differito  Devo creare la partita di Saldo e di Creazione
                    *     nella prima riga  del Fornitore
                    if this.w_GESTPAR="ENASARCO"
                      * --- Nel caso di Enasarco con Storno Differito devo creare Partita di saldo per l'importo 
                      *     dell'enasarco
                      * --- Insert into PAR_TITE
                      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_ccchkf=''
                      i_ccchkv=''
                      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                    " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTCODVAL"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTDATAPE"+",PTDATDOC"+",PTFLCRSA"+",PTFLIMPE"+",PTFLINDI"+",PTFLRAGG"+",PTFLSOSP"+",PTIMPDOC"+",PTMODPAG"+",PTNUMDIS"+",PTNUMDOC"+",PTTOTABB"+",PTTOTIMP"+",PTDESRIG"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
                        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATSCA');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PAR_TITE','PTFLCRSA');
                        +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                        +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                        +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERRIF');
                        +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTORDRIF');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_RIFORD),'PAR_TITE','PTNUMRIF');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                             +i_ccchkv+")")
                      else
                        cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_CPROWNUM,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PNDATDOC,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PTCODVAL',this.w_PNCODVAL,'PT_SEGNO',this.w_APPSEZ,'PTALFDOC',this.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS)
                        insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTDATAPE,PTDATDOC,PTFLCRSA,PTFLIMPE,PTFLINDI,PTFLRAGG,PTFLSOSP,PTIMPDOC,PTMODPAG,PTNUMDIS,PTNUMDOC,PTTOTABB,PTTOTIMP,PTDESRIG,PTSERRIF,PTORDRIF,PTNUMRIF,PTDATREG,PTNUMCOR &i_ccchkf. );
                           values (;
                             this.w_PNSERIAL;
                             ,this.w_CPROWNUM;
                             ,this.w_ROWNUM;
                             ,this.w_PTNUMPAR;
                             ,this.w_PNDATDOC;
                             ,this.w_PNTIPCLF;
                             ,this.w_PNCODCLF;
                             ,this.w_PNCODVAL;
                             ,this.w_APPSEZ;
                             ,this.w_PNALFDOC;
                             ,this.w_PTBANAPP;
                             ,this.w_PTBANNOS;
                             ,this.w_PNCAOVAL;
                             ,this.w_PNCAOVAL;
                             ,this.w_PNDATDOC;
                             ,this.w_PNDATDOC;
                             ,this.w_PNFLPART;
                             ,"  ";
                             ," ";
                             ," ";
                             ," ";
                             ,this.w_PNTOTDOC;
                             ,this.w_PTMODPAG;
                             ,SPACE(10);
                             ,this.w_PNNUMDOC;
                             ,0;
                             ,this.w_PTTOTIMP;
                             ,this.w_PTDESRIG;
                             ,this.w_PNSERIAL;
                             ,1;
                             ,this.w_RIFORD;
                             ,this.w_PNDATREG;
                             ,this.w_PTNUMCOR;
                             &i_ccchkv. )
                        i_Rows=iif(bTrsErr,0,1)
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if i_Rows<0 or bTrsErr
                        * --- Error: insert not accepted
                        i_Error=MSG_INSERT_ERROR
                        return
                      endif
                    endif
                    this.w_APPSEZ = IIF(this.w_SEZCLF="D", "D", "A")
                    * --- Insert into PAR_TITE
                    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTCODVAL"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTDATAPE"+",PTDATDOC"+",PTFLCRSA"+",PTFLIMPE"+",PTFLINDI"+",PTFLRAGG"+",PTFLSOSP"+",PTIMPDOC"+",PTMODPAG"+",PTNUMDIS"+",PTNUMDOC"+",PTTOTABB"+",PTTOTIMP"+",PTDESRIG"+",PTNUMCOR"+",PTDATREG"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                      +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTROWORD');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_RIFORD),'PAR_TITE','CPROWNUM');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATSCA');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                      +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
                      +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_FLSOSP),'PAR_TITE','PTFLSOSP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',1,'CPROWNUM',this.w_RIFORD,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PNDATDOC,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PTCODVAL',this.w_PNCODVAL,'PT_SEGNO',this.w_APPSEZ,'PTALFDOC',this.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS)
                      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTDATAPE,PTDATDOC,PTFLCRSA,PTFLIMPE,PTFLINDI,PTFLRAGG,PTFLSOSP,PTIMPDOC,PTMODPAG,PTNUMDIS,PTNUMDOC,PTTOTABB,PTTOTIMP,PTDESRIG,PTNUMCOR,PTDATREG &i_ccchkf. );
                         values (;
                           this.w_PNSERIAL;
                           ,1;
                           ,this.w_RIFORD;
                           ,this.w_PTNUMPAR;
                           ,this.w_PNDATDOC;
                           ,this.w_PNTIPCLF;
                           ,this.w_PNCODCLF;
                           ,this.w_PNCODVAL;
                           ,this.w_APPSEZ;
                           ,this.w_PNALFDOC;
                           ,this.w_PTBANAPP;
                           ,this.w_PTBANNOS;
                           ,this.w_PNCAOVAL;
                           ,this.w_PNCAOVAL;
                           ,this.w_PNDATDOC;
                           ,this.w_PNDATDOC;
                           ,"C";
                           ,"  ";
                           ," ";
                           ," ";
                           ,this.w_FLSOSP;
                           ,this.w_PNTOTDOC;
                           ,this.w_PTMODPAG;
                           ,SPACE(10);
                           ,this.w_PNNUMDOC;
                           ,0;
                           ,this.w_PTTOTIMP;
                           ,this.w_PNDESSUP;
                           ,this.w_PTNUMCOR;
                           ,this.w_PNDATREG;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error=MSG_INSERT_ERROR
                      return
                    endif
                    Exit
                  else
                    this.w_PTTOTIMP = cp_ROUND((this.w_ABDIFF * this.w_PTTOTIMP) / this.w_PARFOR, this.w_DECTOT)
                    this.w_PTDATSCA = _Curs_PAR_TITE.PTDATSCA
                    this.w_LTOTIMP = cp_ROUND(((this.w_ABDIFF) * NVL(_Curs_PAR_TITE.PTTOTIMP, 0)) / this.w_PARFOR, this.w_DECTOT)
                    this.w_RESTO = this.w_RESTO - this.w_PTTOTIMP
                    * --- Insert into PAR_TITE
                    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTCODAGE"+",PTCODVAL"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTDATAPE"+",PTDATDOC"+",PTFLCRSA"+",PTFLIMPE"+",PTFLINDI"+",PTFLRAGG"+",PTFLSOSP"+",PTIMPDOC"+",PTMODPAG"+",PTNUMDIS"+",PTNUMDOC"+",PTTOTABB"+",PTTOTIMP"+",PTDESRIG"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PAR_TITE','PTFLCRSA');
                      +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                      +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTSERIAL),'PAR_TITE','PTSERRIF');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.PTROWORD),'PAR_TITE','PTORDRIF');
                      +","+cp_NullLink(cp_ToStrODBC(_Curs_PAR_TITE.CPROWNUM),'PAR_TITE','PTNUMRIF');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_CPROWNUM,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PTCODAGE',this.w_PTCODAGE,'PTCODVAL',this.w_PNCODVAL,'PT_SEGNO',this.w_APPSEZ,'PTALFDOC',this.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP)
                      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODAGE,PTCODVAL,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTDATAPE,PTDATDOC,PTFLCRSA,PTFLIMPE,PTFLINDI,PTFLRAGG,PTFLSOSP,PTIMPDOC,PTMODPAG,PTNUMDIS,PTNUMDOC,PTTOTABB,PTTOTIMP,PTDESRIG,PTSERRIF,PTORDRIF,PTNUMRIF,PTDATREG,PTNUMCOR &i_ccchkf. );
                         values (;
                           this.w_PNSERIAL;
                           ,this.w_CPROWNUM;
                           ,this.w_ROWNUM;
                           ,this.w_PTNUMPAR;
                           ,this.w_PTDATSCA;
                           ,this.w_PNTIPCLF;
                           ,this.w_PNCODCLF;
                           ,this.w_PTCODAGE;
                           ,this.w_PNCODVAL;
                           ,this.w_APPSEZ;
                           ,this.w_PNALFDOC;
                           ,this.w_PTBANAPP;
                           ,this.w_PTBANNOS;
                           ,this.w_PNCAOVAL;
                           ,this.w_PNCAOVAL;
                           ,this.w_PNDATDOC;
                           ,this.w_PNDATDOC;
                           ,this.w_PNFLPART;
                           ,"  ";
                           ," ";
                           ," ";
                           ," ";
                           ,this.w_PNTOTDOC;
                           ,this.w_PTMODPAG;
                           ,SPACE(10);
                           ,this.w_PNNUMDOC;
                           ,0;
                           ,this.w_PTTOTIMP;
                           ,this.w_PTDESRIG;
                           ,_Curs_PAR_TITE.PTSERIAL;
                           ,_Curs_PAR_TITE.PTROWORD;
                           ,_Curs_PAR_TITE.CPROWNUM;
                           ,this.w_PNDATREG;
                           ,this.w_PTNUMCOR;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error=MSG_INSERT_ERROR
                      return
                    endif
                    * --- Aggiorno importo partita di apertura per la parte relativa alle ritenute.
                    * --- Write into PAR_TITE
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"PTTOTIMP =PTTOTIMP+ "+cp_ToStrODBC(this.w_LTOTIMP);
                          +i_ccchkf ;
                      +" where ";
                          +"PTSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                          +" and PTROWORD = "+cp_ToStrODBC(1);
                          +" and CPROWNUM = "+cp_ToStrODBC(_Curs_PAR_TITE.CPROWNUM);
                             )
                    else
                      update (i_cTable) set;
                          PTTOTIMP = PTTOTIMP + this.w_LTOTIMP;
                          &i_ccchkf. ;
                       where;
                          PTSERIAL = this.w_PNSERIAL;
                          and PTROWORD = 1;
                          and CPROWNUM = _Curs_PAR_TITE.CPROWNUM;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                endif
                  select _Curs_PAR_TITE
                  continue
                enddo
                use
              endif
            endif
            if this.w_RESTO<>0
              * --- Mette il resto sulla Prima Rata oppure sulla prima partita di creazione relativa alle ritenute.
              this.w_ROWNUM = IIF(this.w_GESRIT="S" and g_RITE="S" And this.w_TRFLGSTO="N", this.w_RIFOLD, 1)
              * --- Write into PAR_TITE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PTTOTIMP =PTTOTIMP+ "+cp_ToStrODBC(this.w_RESTO);
                    +i_ccchkf ;
                +" where ";
                    +"PTSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                    +" and PTROWORD = "+cp_ToStrODBC(this.w_CPROWNUM);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                       )
              else
                update (i_cTable) set;
                    PTTOTIMP = PTTOTIMP + this.w_RESTO;
                    &i_ccchkf. ;
                 where;
                    PTSERIAL = this.w_PNSERIAL;
                    and PTROWORD = this.w_CPROWNUM;
                    and CPROWNUM = this.w_ROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Partita di Apertura
              * --- Write into PAR_TITE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PTTOTIMP =PTTOTIMP+ "+cp_ToStrODBC(this.w_RESTO);
                    +i_ccchkf ;
                +" where ";
                    +"PTSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                    +" and PTROWORD = "+cp_ToStrODBC(1);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWINI);
                       )
              else
                update (i_cTable) set;
                    PTTOTIMP = PTTOTIMP + this.w_RESTO;
                    &i_ccchkf. ;
                 where;
                    PTSERIAL = this.w_PNSERIAL;
                    and PTROWORD = 1;
                    and CPROWNUM = this.w_ROWINI;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          else
            if this.w_RIFORD>0
              * --- Se non ho rate sul documento (Es.acconti+ritenute=tot doc) devo generare
              *     da zero scadenze per entrambe le righe intestatario di creazione e saldo contestuale.
              *     Creo una singola partita, ignoro il tipo pagamento dell'intestatario
              this.w_RIFORD = this.w_RIFORD + 1
              this.w_APPSEZ = IIF(this.w_SEZCLF="D", "D", "A")
              this.w_PTNUMPAR = CANUMPAR("N", this.w_PNCODESE, this.w_PNNUMDOC, this.w_PNALFDOC)
              this.w_PTBANNOS = this.w_BANNOS
              this.w_PTBANAPP = this.w_BANAPP
              this.w_PTNUMCOR = this.w_CONCOR
              this.w_PTMODPAG = this.oParentObject.w_TIPPAB
              * --- Insert into PAR_TITE
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTCODVAL"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTDATAPE"+",PTDATDOC"+",PTFLCRSA"+",PTFLIMPE"+",PTFLINDI"+",PTFLRAGG"+",PTFLSOSP"+",PTIMPDOC"+",PTMODPAG"+",PTNUMDIS"+",PTNUMDOC"+",PTTOTABB"+",PTTOTIMP"+",PTDESRIG"+",PTNUMCOR"+",PTDATREG"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTROWORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_RIFORD),'PAR_TITE','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATSCA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
                +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_FLSOSP),'PAR_TITE','PTFLSOSP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ABDIFF),'PAR_TITE','PTTOTIMP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',1,'CPROWNUM',this.w_RIFORD,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PNDATDOC,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PTCODVAL',this.w_PNCODVAL,'PT_SEGNO',this.w_APPSEZ,'PTALFDOC',this.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS)
                insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTDATAPE,PTDATDOC,PTFLCRSA,PTFLIMPE,PTFLINDI,PTFLRAGG,PTFLSOSP,PTIMPDOC,PTMODPAG,PTNUMDIS,PTNUMDOC,PTTOTABB,PTTOTIMP,PTDESRIG,PTNUMCOR,PTDATREG &i_ccchkf. );
                   values (;
                     this.w_PNSERIAL;
                     ,1;
                     ,this.w_RIFORD;
                     ,this.w_PTNUMPAR;
                     ,this.w_PNDATDOC;
                     ,this.w_PNTIPCLF;
                     ,this.w_PNCODCLF;
                     ,this.w_PNCODVAL;
                     ,this.w_APPSEZ;
                     ,this.w_PNALFDOC;
                     ,this.w_PTBANAPP;
                     ,this.w_PTBANNOS;
                     ,this.w_PNCAOVAL;
                     ,this.w_PNCAOVAL;
                     ,this.w_PNDATDOC;
                     ,this.w_PNDATDOC;
                     ,"C";
                     ,"  ";
                     ," ";
                     ," ";
                     ,this.w_FLSOSP;
                     ,this.w_PNTOTDOC;
                     ,this.w_PTMODPAG;
                     ,SPACE(10);
                     ,this.w_PNNUMDOC;
                     ,0;
                     ,this.w_ABDIFF;
                     ,this.w_PNDESSUP;
                     ,this.w_PTNUMCOR;
                     ,this.w_PNDATREG;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              if this.w_GESTPAR = "ENASARCO" OR this.w_TRFLGSTO="S"
                * --- Costruisco l'equivalente partita di chiusura
                this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
                * --- Insert into PAR_TITE
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTCODVAL"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTDATAPE"+",PTDATDOC"+",PTFLCRSA"+",PTFLIMPE"+",PTFLINDI"+",PTFLRAGG"+",PTFLSOSP"+",PTIMPDOC"+",PTMODPAG"+",PTNUMDIS"+",PTNUMDOC"+",PTTOTABB"+",PTTOTIMP"+",PTDESRIG"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTNUMCOR"+",PTDATREG"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
                  +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','CPROWNUM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATSCA');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                  +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLCRSA');
                  +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                  +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                  +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                  +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                  +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                  +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ABDIFF),'PAR_TITE','PTTOTIMP');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERRIF');
                  +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTORDRIF');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_RIFORD),'PAR_TITE','PTNUMRIF');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_CPROWNUM,'CPROWNUM',1,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PNDATDOC,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PTCODVAL',this.w_PNCODVAL,'PT_SEGNO',this.w_APPSEZ,'PTALFDOC',this.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS)
                  insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTDATAPE,PTDATDOC,PTFLCRSA,PTFLIMPE,PTFLINDI,PTFLRAGG,PTFLSOSP,PTIMPDOC,PTMODPAG,PTNUMDIS,PTNUMDOC,PTTOTABB,PTTOTIMP,PTDESRIG,PTSERRIF,PTORDRIF,PTNUMRIF,PTNUMCOR,PTDATREG &i_ccchkf. );
                     values (;
                       this.w_PNSERIAL;
                       ,this.w_CPROWNUM;
                       ,1;
                       ,this.w_PTNUMPAR;
                       ,this.w_PNDATDOC;
                       ,this.w_PNTIPCLF;
                       ,this.w_PNCODCLF;
                       ,this.w_PNCODVAL;
                       ,this.w_APPSEZ;
                       ,this.w_PNALFDOC;
                       ,this.w_PTBANAPP;
                       ,this.w_PTBANNOS;
                       ,this.w_PNCAOVAL;
                       ,this.w_PNCAOVAL;
                       ,this.w_PNDATDOC;
                       ,this.w_PNDATDOC;
                       ,"S";
                       ,"  ";
                       ," ";
                       ," ";
                       ," ";
                       ,this.w_PNTOTDOC;
                       ,this.w_PTMODPAG;
                       ,SPACE(10);
                       ,this.w_PNNUMDOC;
                       ,0;
                       ,this.w_ABDIFF;
                       ,this.w_PNDESSUP;
                       ,this.w_PNSERIAL;
                       ,1;
                       ,this.w_RIFORD;
                       ,this.w_PTNUMCOR;
                       ,this.w_PNDATREG;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              endif
            endif
          endif
        endif
      case this.w_GESTPAR = "INTESTA"
        * --- Caso riga intestatario le partite le creo al temrine (LEGGERATE) case quindi vuoto
      case this.w_GESTPAR = "LEGGERATE"
        * --- Aggiorna le Partite
        * --- Costruisco il flag aggiornamento Partite (stesso calcolo che in GSCG_MPN)
        *     Nel caso dell'intestatario rileggo le informazioni da CONTI (questo giro viene 
        *     svolto al termine...)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANPARTSN,ANCODAG1"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANPARTSN,ANCODAG1;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCLF;
                and ANCODICE = this.w_PNCODCLF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PARTSN3 = NVL(cp_ToDate(_read_.ANPARTSN),cp_NullValue(_read_.ANPARTSN))
          this.w_LCODAGE = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PNFLPART = IIF(this.w_PARTSN3="S", this.w_RIGPAR, "N")
        if this.w_PNFLPART $ "ACS"
          if this.w_PNTIPDOC="FC"
            MES[27]=ah_MsgFormat("Impossibile contabilizzare una fattura corrispettivi che gestisce partite. Disabilitare flag gestisce partite sulla causale contabile [%1]",this.w_PNCODCAU)
            this.w_FLERR = .T.
          else
            if this.w_CLADOC$ "NC-NE-NU" AND this.w_PNFLPART="A"
              this.w_PTNUMPAR = CANUMPAR("S", this.w_PNCODESE)
              this.w_PTCODAGE = this.w_LCODAGE
            else
              this.w_PTNUMPAR = CANUMPAR("N", this.w_PNCODESE, this.w_PNNUMDOC, this.w_PNALFDOC)
              this.w_PTCODAGE = this.w_CODAGE
            endif
            this.w_PTFLVABD = this.w_FLVABD
            this.w_ROWNUM = 0
            this.w_PTDATSCA = this.w_PNDATDOC
            this.w_PTFLSOSP = " "
            this.w_PTMODPAG = this.oParentObject.w_TIPPAB
            * --- Creo Partita Relativa Sconto Finanziario
            if this.w_IMPFIN<>0
              this.w_ROWNUM = this.w_ROWNUM + 1
              this.w_RIFFIN = this.w_ROWNUM
              * --- Inverte la sezione se Negativo..
              this.w_APPSEZ = this.w_SEZCLF
              this.w_PTTOTIMP = this.w_IMPFIN
              if this.w_PTTOTIMP<0
                * --- Se negativo cambio segno
                this.w_APPSEZ = IIF(this.w_APPSEZ="D","A", "D")
                this.w_PTTOTIMP = ABS(this.w_PTTOTIMP)
              endif
              * --- Insert into PAR_TITE
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMDIS"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTFLCRSA"+",PTFLIMPE"+",PTTOTABB"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTROWORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_BANAPP),'PAR_TITE','PTBANAPP');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_BANNOS),'PAR_TITE','PTBANNOS');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
                +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CONCOR),'PAR_TITE','PTNUMCOR');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',1,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PT_SEGNO',this.w_APPSEZ,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PNCODVAL,'PTCAOVAL',this.w_PNCAOVAL,'PTCAOAPE',this.w_PNCAOVAL)
                insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMDIS,PTFLINDI,PTBANNOS,PTFLRAGG,PTFLCRSA,PTFLIMPE,PTTOTABB,PTDESRIG,PTCODAGE,PTFLVABD,PTDATREG,PTNUMCOR &i_ccchkf. );
                   values (;
                     this.w_PNSERIAL;
                     ,1;
                     ,this.w_ROWNUM;
                     ,this.w_PTNUMPAR;
                     ,this.w_PTDATSCA;
                     ,this.w_PNTIPCLF;
                     ,this.w_PNCODCLF;
                     ,this.w_APPSEZ;
                     ,this.w_PTTOTIMP;
                     ,this.w_PNCODVAL;
                     ,this.w_PNCAOVAL;
                     ,this.w_PNCAOVAL;
                     ,this.w_PNDATDOC;
                     ,this.w_PNNUMDOC;
                     ,this.w_PNALFDOC;
                     ,this.w_PNDATDOC;
                     ,this.w_PNTOTDOC;
                     ,this.w_PTMODPAG;
                     ,this.w_PTFLSOSP;
                     ,this.w_BANAPP;
                     ," ";
                     ," ";
                     ,this.w_BANNOS;
                     ," ";
                     ,"C";
                     ,"  ";
                     ,0;
                     ,this.w_PNDESSUP;
                     ,this.w_PTCODAGE;
                     ,this.w_PTFLVABD;
                     ,this.w_PNDATREG;
                     ,this.w_CONCOR;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
            * --- Creo Partita Relativa alla ritenuta attiva
            if this.w_RITATT>0 and (g_RITE<>"S" OR (g_RITE="S" AND this.w_VALRIT=0))
              this.w_ROWNUM = this.w_ROWNUM + 1
              this.w_RIFRIT = this.w_ROWNUM
              * --- Inverte la sezione se Negativo..
              this.w_APPSEZ = this.w_SEZCLF
              this.w_PTTOTIMP = this.w_RITATT
              * --- Insert into PAR_TITE
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMDIS"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTFLCRSA"+",PTFLIMPE"+",PTTOTABB"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTROWORD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_BANAPP),'PAR_TITE','PTBANAPP');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_BANNOS),'PAR_TITE','PTBANNOS');
                +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
                +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CONCOR),'PAR_TITE','PTNUMCOR');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',1,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PT_SEGNO',this.w_APPSEZ,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PNCODVAL,'PTCAOVAL',this.w_PNCAOVAL,'PTCAOAPE',this.w_PNCAOVAL)
                insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMDIS,PTFLINDI,PTBANNOS,PTFLRAGG,PTFLCRSA,PTFLIMPE,PTTOTABB,PTDESRIG,PTCODAGE,PTFLVABD,PTDATREG,PTNUMCOR &i_ccchkf. );
                   values (;
                     this.w_PNSERIAL;
                     ,1;
                     ,this.w_ROWNUM;
                     ,this.w_PTNUMPAR;
                     ,this.w_PTDATSCA;
                     ,this.w_PNTIPCLF;
                     ,this.w_PNCODCLF;
                     ,this.w_APPSEZ;
                     ,this.w_PTTOTIMP;
                     ,this.w_PNCODVAL;
                     ,this.w_PNCAOVAL;
                     ,this.w_PNCAOVAL;
                     ,this.w_PNDATDOC;
                     ,this.w_PNNUMDOC;
                     ,this.w_PNALFDOC;
                     ,this.w_PNDATDOC;
                     ,this.w_PNTOTDOC;
                     ,this.w_PTMODPAG;
                     ,this.w_PTFLSOSP;
                     ,this.w_BANAPP;
                     ," ";
                     ," ";
                     ,this.w_BANNOS;
                     ," ";
                     ,"C";
                     ,"  ";
                     ,0;
                     ,this.w_PNDESSUP;
                     ,this.w_PTCODAGE;
                     ,this.w_PTFLVABD;
                     ,this.w_PNDATREG;
                     ,this.w_CONCOR;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
            if this.w_ACCONT<>0 
              * --- Se l'acconto non � ancora contabilizzato, la procedura non ha modo di
              *     collegare l'acconto sul documento all'acconto da contabilizzare.
              if Empty ( this.w_MVRIFACC ) 
                * --- Segnalo la mancata contabilizzazione dell'acconto
                MES[25]=ah_MsgFormat("Acconto contestuale non contabilizzato")
                this.w_FLERR = .T.
              else
                this.w_ROWNUM = this.w_ROWNUM + 1
                * --- Inverte la sezione se Negativo..
                this.w_APPSEZ = IIF(this.w_ACCONT<0, IIF(this.w_SEZCLF="D", "A", "D"), this.w_SEZCLF)
                this.w_PTTOTIMP = ABS(this.w_ACCONT)
                * --- Insert into PAR_TITE
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMDIS"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTFLCRSA"+",PTFLIMPE"+",PTTOTABB"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTROWORD');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_BANAPP),'PAR_TITE','PTBANAPP');
                  +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
                  +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_BANNOS),'PAR_TITE','PTBANNOS');
                  +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                  +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
                  +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                  +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CONCOR),'PAR_TITE','PTNUMCOR');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',1,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PT_SEGNO',this.w_APPSEZ,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PNCODVAL,'PTCAOVAL',this.w_PNCAOVAL,'PTCAOAPE',this.w_PNCAOVAL)
                  insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMDIS,PTFLINDI,PTBANNOS,PTFLRAGG,PTFLCRSA,PTFLIMPE,PTTOTABB,PTDESRIG,PTCODAGE,PTFLVABD,PTDATREG,PTNUMCOR &i_ccchkf. );
                     values (;
                       this.w_PNSERIAL;
                       ,1;
                       ,this.w_ROWNUM;
                       ,this.w_PTNUMPAR;
                       ,this.w_PTDATSCA;
                       ,this.w_PNTIPCLF;
                       ,this.w_PNCODCLF;
                       ,this.w_APPSEZ;
                       ,this.w_PTTOTIMP;
                       ,this.w_PNCODVAL;
                       ,this.w_PNCAOVAL;
                       ,this.w_PNCAOVAL;
                       ,this.w_PNDATDOC;
                       ,this.w_PNNUMDOC;
                       ,this.w_PNALFDOC;
                       ,this.w_PNDATDOC;
                       ,this.w_PNTOTDOC;
                       ,this.w_PTMODPAG;
                       ,this.w_PTFLSOSP;
                       ,this.w_BANAPP;
                       ," ";
                       ," ";
                       ,this.w_BANNOS;
                       ," ";
                       ,"C";
                       ,"  ";
                       ,0;
                       ,this.w_PNDESSUP;
                       ,this.w_PTCODAGE;
                       ,this.w_PTFLVABD;
                       ,this.w_PNDATREG;
                       ,this.w_CONCOR;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
                * --- Modifico la partita generata dalla contabilizzazione acconti mettendo
                *     i riferimenti alla partita appena inserita.
                *     Aggiorno anche numero partite agente e flag deperibili nel caso
                *     l'utente modifichi la registrazione tra contabilizzazione acconto e 
                *     documento.
                * --- Write into PAR_TITE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERRIF');
                  +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTORDRIF');
                  +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','PTNUMRIF');
                  +",PTNUMPAR ="+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                  +",PTCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
                  +",PTFLVABD ="+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
                      +i_ccchkf ;
                  +" where ";
                      +"PTSERIAL = "+cp_ToStrODBC(this.w_MVRIFACC);
                      +" and PTROWORD = "+cp_ToStrODBC(1);
                      +" and CPROWNUM = "+cp_ToStrODBC(1);
                         )
                else
                  update (i_cTable) set;
                      PTSERRIF = this.w_PNSERIAL;
                      ,PTORDRIF = 1;
                      ,PTNUMRIF = this.w_ROWNUM;
                      ,PTNUMPAR = this.w_PTNUMPAR;
                      ,PTCODAGE = this.w_PTCODAGE;
                      ,PTFLVABD = this.w_PTFLVABD;
                      &i_ccchkf. ;
                   where;
                      PTSERIAL = this.w_MVRIFACC;
                      and PTROWORD = 1;
                      and CPROWNUM = 1;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
            * --- Se l'acconto sul documento � pari al totale documento
            *     non occorre generare ulteriori partite da acconti ne dal documento.
            *     Se esiste un acconto precedente w_TMPN sar� maggiore di zero
            this.w_TmpN = this.w_TOTCLF- this.w_ACCONT - this.w_VALRIT- this.w_RITPRE- this.w_VALENA - this.w_IMPFIN
            * --- Attenzione Esistono due Select in DOC_RATE  nei due rami dell'IF eventuali modifiche
            *     devono essere ripetute su entrambe!!
            * --- Accorpamento Acconti
            if this.w_TmpN>0
              * --- Gestione acconti, se accorpa acconti in automatico o documento con
              *     acconto precedente.
              if (this.w_FLAACC="S" OR this.w_ACCPRE<>0) 
                * --- Verifico che in presenza di acconti precedenti, vi siano contabilizzati
                *     un totale almeno pari all'acconto precedente sul documento con 
                *     data scadenza inferiore od uguale...
                if this.w_ACCPRE<>0 
                  this.w_ROWTIPCON = "X"
                  this.w_TOTALEACC = 0
                  * --- Select from GSCGABCV
                  do vq_exec with 'GSCGABCV',this,'_Curs_GSCGABCV','',.f.,.t.
                  if used('_Curs_GSCGABCV')
                    select _Curs_GSCGABCV
                    locate for 1=1
                    do while not(eof())
                    this.w_TOTALEACC = Nvl( _Curs_GSCGABCV.ACCONTI , 0 ) 
                    this.w_ROWTIPCON = NVL(_Curs_GSCGABCV.PTTIPCON," ")
                      select _Curs_GSCGABCV
                      continue
                    enddo
                    use
                  endif
                  * --- Segnalo la mancata contabilizzazione dell'acconto
                  if this.w_TOTALEACC< this.w_ACCPRE and Not Empty(this.w_ROWTIPCON)
                    MES[26]=ah_MsgFormat("Acconto/i precedenti non tutti contabilizzati. Acconto precedente [%1 %2], acconti contabilizzati [%3 %2]",alltrim(Tran(this.w_ACCPRE,v_PV[20*(this.w_DECTOP+2)])),this.w_SIMVAL,alltrim(Tran(Nvl(this.w_TOTALEACC,0),v_PV[20*(this.w_DECTOP+2)])))
                    this.w_FLERR = .T.
                  endif
                endif
                * --- Se ho il flag accorpa acconti e non ho scadenze confermate e sto contabilizzando
                *     una fattura recupero anhe gli acconti generici non collegati (evasi dalla fattura)
                *     Passato alla query per recuperare o meno anche gli acconti non legati alla
                *     fattura (Se 'S' prende tutti gli acconti)
                * --- Genero acconti se:
                *     1) Ho flag accorpa acconti
                *     2) Ho acconti precedenti su documento
                if (this.w_ACCEVAS="S" OR this.w_ACCPRE<>0) 
                  * --- Acconti da Abbinare
                  this.w_PTNUMPAR = CANUMPAR("N", this.w_PNCODESE, this.w_PNNUMDOC, this.w_PNALFDOC)
                  * --- Nel caso degli acconti non si � in grado di recuperare le banche direttamente
                  *     dalle scadenze, in questo caso si valorizzano tali campi con le informazioni 
                  *     eventualmente presenti sui dati generali del documento.
                  *     Queste informazioni sono riportate su tutte le partite generate per
                  *     questa casistica
                  this.w_PTBANNOS = this.w_BANNOS
                  this.w_PTBANAPP = this.w_BANAPP
                  this.w_PTNUMCOR = this.w_CONCOR
                  * --- w_TROVACC se .F. non e' stato trovato nessun acconto da Accorpare sul Documento
                  *     in Questo caso l'acconto va riportato spalmato sulle varie rate della partita Cliente. 
                  this.w_TROVACC = .F.
                  this.w_TESTACC = .T.
                  * --- Contiene totale acconti utilizzati per saldare documento
                  *     (tutti sia generici che precedenti)
                  this.w_TOTACC = 0
                  this.w_RESIDUO = 0
                  * --- Recupero acconti precedenti o generici (esclude eventuali acconti contestuali)
                  * --- Select from GSCGCQRC
                  do vq_exec with 'GSCGCQRC',this,'_Curs_GSCGCQRC','',.f.,.t.
                  if used('_Curs_GSCGCQRC')
                    select _Curs_GSCGCQRC
                    locate for 1=1
                    do while not(eof())
                    if this.w_TESTACC
                      this.w_TROVACC = .T.
                      this.w_APPO4 = NVL(_Curs_GSCGCQRC.PTSERIAL," ")
                      this.w_ROWORD = NVL(_Curs_GSCGCQRC.PTROWORD,0)
                      this.w_ROWNUM1 = NVL(_Curs_GSCGCQRC.CPROWNUM,0)
                      this.w_RIGACC = NVL(_Curs_GSCGCQRC.PTDESRIG, " ")
                      this.w_APPSEZ = IIF(NVL(_Curs_GSCGCQRC.PT_SEGNO,"D")="D","A", "D")
                      this.w_PARACC = abs(NVL(_Curs_GSCGCQRC.PTTOTIMP, 0))
                      this.w_DATACC = NVL(CP_TODATE(_Curs_GSCGCQRC.PTDATSCA),cp_CharToDate("    -  -  "))
                      this.w_PAGACC = NVL(_Curs_GSCGCQRC.PTMODPAG, SPACE(10))
                      this.w_TOTACC = this.w_TOTACC + this.w_PARACC
                      this.w_RIGACC = IIF(EMPTY(this.w_RIGACC), this.w_PNDESSUP, this.w_RIGACC)
                      this.w_RESIDUO = cp_Round(ABS(this.w_TmpN)-ABS(this.w_TOTACC), this.w_DECTOT)
                      this.w_ROWNUM = this.w_ROWNUM + 1
                      * --- inserisco partita pari all'intero importo dell'acconto
                      * --- Insert into PAR_TITE
                      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_ccchkf=''
                      i_ccchkv=''
                      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                    " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMDIS"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTFLCRSA"+",PTFLIMPE"+",PTTOTABB"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTNUMCOR"+",PTDATREG"+i_ccchkf+") values ("+;
                        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                        +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTROWORD');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_DATACC),'PAR_TITE','PTDATSCA');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PARACC),'PAR_TITE','PTTOTIMP');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
                        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PAR_TITE','PTFLCRSA');
                        +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                        +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_RIGACC),'PAR_TITE','PTDESRIG');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                             +i_ccchkv+")")
                      else
                        cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',1,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_DATACC,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PT_SEGNO',this.w_APPSEZ,'PTTOTIMP',this.w_PARACC,'PTCODVAL',this.w_PNCODVAL,'PTCAOVAL',this.w_PNCAOVAL,'PTCAOAPE',this.w_PNCAOVAL)
                        insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMDIS,PTFLINDI,PTBANNOS,PTFLRAGG,PTFLCRSA,PTFLIMPE,PTTOTABB,PTDESRIG,PTCODAGE,PTFLVABD,PTNUMCOR,PTDATREG &i_ccchkf. );
                           values (;
                             this.w_PNSERIAL;
                             ,1;
                             ,this.w_ROWNUM;
                             ,this.w_PTNUMPAR;
                             ,this.w_DATACC;
                             ,this.w_PNTIPCLF;
                             ,this.w_PNCODCLF;
                             ,this.w_APPSEZ;
                             ,this.w_PARACC;
                             ,this.w_PNCODVAL;
                             ,this.w_PNCAOVAL;
                             ,this.w_PNCAOVAL;
                             ,this.w_PNDATDOC;
                             ,this.w_PNNUMDOC;
                             ,this.w_PNALFDOC;
                             ,this.w_PNDATDOC;
                             ,this.w_PNTOTDOC;
                             ,this.w_PTMODPAG;
                             ,this.w_PTFLSOSP;
                             ,this.w_PTBANAPP;
                             ," ";
                             ," ";
                             ,this.w_PTBANNOS;
                             ," ";
                             ,this.w_PNFLPART;
                             ,"  ";
                             ,0;
                             ,this.w_RIGACC;
                             ,this.w_PTCODAGE;
                             ,this.w_PTFLVABD;
                             ,this.w_PTNUMCOR;
                             ,this.w_PNDATREG;
                             &i_ccchkv. )
                        i_Rows=iif(bTrsErr,0,1)
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if i_Rows<0 or bTrsErr
                        * --- Error: insert not accepted
                        i_Error=MSG_INSERT_ERROR
                        return
                      endif
                      * --- Aggiorno numero partita dell'acconto accorpato e riferimenti
                      * --- Write into PAR_TITE
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"PTNUMPAR ="+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                        +",PTCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
                        +",PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERRIF');
                        +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTORDRIF');
                        +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','PTNUMRIF');
                            +i_ccchkf ;
                        +" where ";
                            +"PTSERIAL = "+cp_ToStrODBC(this.w_APPO4);
                            +" and PTROWORD = "+cp_ToStrODBC(this.w_ROWORD);
                            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM1);
                               )
                      else
                        update (i_cTable) set;
                            PTNUMPAR = this.w_PTNUMPAR;
                            ,PTCODAGE = this.w_PTCODAGE;
                            ,PTSERRIF = this.w_PNSERIAL;
                            ,PTORDRIF = 1;
                            ,PTNUMRIF = this.w_ROWNUM;
                            &i_ccchkf. ;
                         where;
                            PTSERIAL = this.w_APPO4;
                            and PTROWORD = this.w_ROWORD;
                            and CPROWNUM = this.w_ROWNUM1;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                      if this.w_RESIDUO<=0
                        * --- Se acconto superiore a Totale documento creo partita di acconto per la differenza
                        if this.w_RESIDUO<0
                          * --- Inserisco partita di acconto residua nuovamente disponibile
                          this.w_RESIDUO = ABS(this.w_RESIDUO)
                          this.w_ROWNUM = this.w_ROWNUM + 1
                          this.w_APPSEZ1 = IIF(this.w_APPSEZ="D","A", "D")
                          this.w_PTNUMPAR = CANUMPAR("S", this.w_PNCODESE)
                          * --- Insert into PAR_TITE
                          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_ccchkf=''
                          i_ccchkv=''
                          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                        " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMDIS"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTFLCRSA"+",PTFLIMPE"+",PTTOTABB"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTNUMCOR"+",PTDATREG"+i_ccchkf+") values ("+;
                            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                            +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTROWORD');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATSCA');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ1),'PAR_TITE','PT_SEGNO');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_RESIDUO),'PAR_TITE','PTTOTIMP');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
                            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                            +","+cp_NullLink(cp_ToStrODBC("A"),'PAR_TITE','PTFLCRSA');
                            +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                            +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_RIGACC),'PAR_TITE','PTDESRIG');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_LCODAGE),'PAR_TITE','PTCODAGE');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                                 +i_ccchkv+")")
                          else
                            cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',1,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PNDATDOC,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PT_SEGNO',this.w_APPSEZ1,'PTTOTIMP',this.w_RESIDUO,'PTCODVAL',this.w_PNCODVAL,'PTCAOVAL',this.w_PNCAOVAL,'PTCAOAPE',this.w_PNCAOVAL)
                            insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMDIS,PTFLINDI,PTBANNOS,PTFLRAGG,PTFLCRSA,PTFLIMPE,PTTOTABB,PTDESRIG,PTCODAGE,PTFLVABD,PTNUMCOR,PTDATREG &i_ccchkf. );
                               values (;
                                 this.w_PNSERIAL;
                                 ,1;
                                 ,this.w_ROWNUM;
                                 ,this.w_PTNUMPAR;
                                 ,this.w_PNDATDOC;
                                 ,this.w_PNTIPCLF;
                                 ,this.w_PNCODCLF;
                                 ,this.w_APPSEZ1;
                                 ,this.w_RESIDUO;
                                 ,this.w_PNCODVAL;
                                 ,this.w_PNCAOVAL;
                                 ,this.w_PNCAOVAL;
                                 ,this.w_PNDATDOC;
                                 ,this.w_PNNUMDOC;
                                 ,this.w_PNALFDOC;
                                 ,this.w_PNDATDOC;
                                 ,this.w_PNTOTDOC;
                                 ,this.w_PTMODPAG;
                                 ,this.w_PTFLSOSP;
                                 ,this.w_PTBANAPP;
                                 ," ";
                                 ," ";
                                 ,this.w_PTBANNOS;
                                 ," ";
                                 ,"A";
                                 ,"  ";
                                 ,0;
                                 ,this.w_RIGACC;
                                 ,this.w_LCODAGE;
                                 ,this.w_PTFLVABD;
                                 ,this.w_PTNUMCOR;
                                 ,this.w_PNDATREG;
                                 &i_ccchkv. )
                            i_Rows=iif(bTrsErr,0,1)
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if i_Rows<0 or bTrsErr
                            * --- Error: insert not accepted
                            i_Error=MSG_INSERT_ERROR
                            return
                          endif
                        endif
                        * --- Non ricerco pi� acconti ho esaurito l'intero importo documento
                        this.w_TESTACC = .F.
                        * --- Ho chiuso l'intero documento, il totale acconti � quindi pari al totale
                        *     documento disponibile
                        this.w_TOTACC = ABS(this.w_TmpN)
                      endif
                    else
                      Exit
                    endif
                      select _Curs_GSCGCQRC
                      continue
                    enddo
                    use
                  endif
                  if this.w_FLAACC="S" AND this.w_ACCEVAS="S"
                    * --- Ho trovato almeno un acconto...
                    if this.w_TROVACC
                      * --- Se w_TESTACC allora gli acconti non compensano completamente tutto
                      *     il documento, devo rigenerare le rate considerando gli acconti trovati.
                      if this.w_TESTACC
                        * --- Se acconto inferiore al totale documento ricalcolo le rate per il residuo
                        * --- Read from CONTI
                        i_nOldArea=select()
                        if used('_read_')
                          select _read_
                          use
                        endif
                        i_nConn=i_TableProp[this.CONTI_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                        if i_nConn<>0
                          cp_sqlexec(i_nConn,"select "+;
                            "AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS"+;
                            " from "+i_cTable+" CONTI where ";
                                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                                 ,"_read_")
                          i_Rows=iif(used('_read_'),reccount(),0)
                        else
                          select;
                            AN1MESCL,AN2MESCL,ANGIOSC1,ANGIOSC2,ANGIOFIS;
                            from (i_cTable) where;
                                ANTIPCON = this.w_PNTIPCLF;
                                and ANCODICE = this.w_PNCODCLF;
                             into cursor _read_
                          i_Rows=_tally
                        endif
                        if used('_read_')
                          locate for 1=1
                          w_MESE = NVL(cp_ToDate(_read_.AN1MESCL),cp_NullValue(_read_.AN1MESCL))
                          this.w_MESE1 = NVL(cp_ToDate(_read_.AN2MESCL),cp_NullValue(_read_.AN2MESCL))
                          this.w_GIORN1 = NVL(cp_ToDate(_read_.ANGIOSC1),cp_NullValue(_read_.ANGIOSC1))
                          this.w_GIORN2 = NVL(cp_ToDate(_read_.ANGIOSC2),cp_NullValue(_read_.ANGIOSC2))
                          this.w_GIOFIS = NVL(cp_ToDate(_read_.ANGIOFIS),cp_NullValue(_read_.ANGIOFIS))
                          use
                        else
                          * --- Error: sql sentence error.
                          i_Error = MSG_READ_ERROR
                          return
                        endif
                        select (i_nOldArea)
                        this.w_ESCL2 = STR(this.w_MESE1, 2, 0) + STR(this.w_GIORN2, 2, 0)
                        this.w_ESCL1 = STR(w_MESE, 2, 0) + STR(this.w_GIORN1, 2, 0) + STR(this.w_GIOFIS, 2, 0)
                        this.w_NETTO = (this.w_TmpN-this.w_TOTACC)-this.w_IVADET
                        this.w_NURATE = SCADENZE("ARRSCA", this.w_CODPAG, this.w_PNDATDOC, this.w_NETTO, this.w_IVADET, 0, this.w_ESCL1, this.w_ESCL2, this.w_DECTOT)
                        * --- Il numero di scadenze non cambia cambia solo la ripartizione degli importi
                        * --- Select from DOC_RATE
                        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2],.t.,this.DOC_RATE_idx)
                        if i_nConn<>0
                          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_RATE ";
                              +" where RSSERIAL="+cp_ToStrODBC(this.w_OSERIAL)+"";
                              +" order by RSDATRAT";
                               ,"_Curs_DOC_RATE")
                        else
                          select * from (i_cTable);
                           where RSSERIAL=this.w_OSERIAL;
                           order by RSDATRAT;
                            into cursor _Curs_DOC_RATE
                        endif
                        if used('_Curs_DOC_RATE')
                          select _Curs_DOC_RATE
                          locate for 1=1
                          do while not(eof())
                          this.w_NUMRATE = _Curs_DOC_RATE.RSNUMRAT
                          this.w_PTTOTIMP = IIF(NVL(ARRSCA[this.w_NUMRATE, 2] + ARRSCA[this.w_NUMRATE, 3],0)<>0,NVL(ARRSCA[this.w_NUMRATE, 2] + ARRSCA[this.w_NUMRATE, 3],0),NVL(_Curs_DOC_RATE.RSIMPRAT, 0))
                          * --- Aggiorno importo delle rate
                          * --- Write into DOC_RATE
                          i_commit = .f.
                          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                            cp_BeginTrs()
                            i_commit = .t.
                          endif
                          i_nConn=i_TableProp[this.DOC_RATE_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
                          i_ccchkf=''
                          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_RATE_idx,i_nConn)
                          if i_nConn<>0
                            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                            +"RSIMPRAT ="+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'DOC_RATE','RSIMPRAT');
                                +i_ccchkf ;
                            +" where ";
                                +"RSSERIAL = "+cp_ToStrODBC(this.w_OSERIAL);
                                +" and RSNUMRAT = "+cp_ToStrODBC(this.w_NUMRATE);
                                   )
                          else
                            update (i_cTable) set;
                                RSIMPRAT = this.w_PTTOTIMP;
                                &i_ccchkf. ;
                             where;
                                RSSERIAL = this.w_OSERIAL;
                                and RSNUMRAT = this.w_NUMRATE;

                            i_Rows = _tally
                          endif
                          if i_commit
                            cp_EndTrs(.t.)
                          endif
                          if bTrsErr
                            i_Error=MSG_WRITE_ERROR
                            return
                          endif
                            select _Curs_DOC_RATE
                            continue
                          enddo
                          use
                        endif
                        this.w_ACCEVAS = " "
                      else
                        * --- Cancello Rate dal Documento
                        * --- Delete from DOC_RATE
                        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        if i_nConn<>0
                          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                                +"RSSERIAL = "+cp_ToStrODBC(this.w_OSERIAL);
                                 )
                        else
                          delete from (i_cTable) where;
                                RSSERIAL = this.w_OSERIAL;

                          i_Rows=_tally
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if bTrsErr
                          * --- Error: delete not accepted
                          i_Error=MSG_DELETE_ERROR
                          return
                        endif
                      endif
                      * --- Dal totale acconti storno gli acconti precedenti perch� w_TOTACC
                      *     mi occorre per aggiornare gli acconti precedenti sul documento
                      this.w_TOTACC = ABS(this.w_TOTACC) - ABS(this.w_ACCPRE)
                      * --- Write into DOC_MAST
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"MVACCPRE =MVACCPRE+ "+cp_ToStrODBC(this.w_TOTACC);
                        +",MVACCOLD ="+cp_NullLink(cp_ToStrODBC(this.w_ACCPRE),'DOC_MAST','MVACCOLD');
                            +i_ccchkf ;
                        +" where ";
                            +"MVSERIAL = "+cp_ToStrODBC(this.w_OSERIAL);
                               )
                      else
                        update (i_cTable) set;
                            MVACCPRE = MVACCPRE + this.w_TOTACC;
                            ,MVACCOLD = this.w_ACCPRE;
                            &i_ccchkf. ;
                         where;
                            MVSERIAL = this.w_OSERIAL;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                    else
                      this.w_ACCEVAS = " "
                    endif
                  endif
                else
                  this.w_ACCEVAS = " "
                endif
              else
                this.w_ACCEVAS = " "
              endif
              if Empty( this.w_ACCEVAS )
                this.w_ROWSPL = 0
                * --- Select from DOC_RATE
                i_nConn=i_TableProp[this.DOC_RATE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2],.t.,this.DOC_RATE_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_RATE ";
                      +" where RSSERIAL="+cp_ToStrODBC(this.w_OSERIAL)+"";
                      +" order by RSDATRAT";
                       ,"_Curs_DOC_RATE")
                else
                  select * from (i_cTable);
                   where RSSERIAL=this.w_OSERIAL;
                   order by RSDATRAT;
                    into cursor _Curs_DOC_RATE
                endif
                if used('_Curs_DOC_RATE')
                  select _Curs_DOC_RATE
                  locate for 1=1
                  do while not(eof())
                  this.w_ROWNUM = this.w_ROWNUM + 1
                  this.w_PTDATSCA = _Curs_DOC_RATE.RSDATRAT
                  this.w_PTFLSOSP = NVL(_Curs_DOC_RATE.RSFLSOSP, " ")
                  this.w_PTMODPAG = NVL(_Curs_DOC_RATE.RSMODPAG, SPACE(10))
                  * --- Inverte la sezione se Negativo..
                  this.w_APPSEZ = IIF(NVL(_Curs_DOC_RATE.RSIMPRAT,0)<0, IIF(this.w_SEZCLF="D", "A", "D"), this.w_SEZCLF)
                  this.w_PTTOTIMP = ABS(NVL(_Curs_DOC_RATE.RSIMPRAT, 0))
                  this.w_PTBANNOS = IIF(NOT EMPTY(NVL(_Curs_DOC_RATE.RSBANNOS, SPACE(15))),_Curs_DOC_RATE.RSBANNOS,this.w_BANNOS)
                  this.w_PTBANAPP = IIF(NOT EMPTY(NVL(_Curs_DOC_RATE.RSBANAPP, SPACE(10))),_Curs_DOC_RATE.RSBANAPP,this.w_BANAPP)
                  this.w_PTNUMCOR = IIF(NOT EMPTY(NVL(_Curs_DOC_RATE.RSCONCOR, SPACE(25))),_Curs_DOC_RATE.RSCONCOR,this.w_CONCOR)
                  this.w_PTDESRIG = IIF(EMPTY(NVL(_Curs_DOC_RATE.RSDESRIG, "")), this.w_PNDESSUP, _Curs_DOC_RATE.RSDESRIG)
                  if this.w_IMPSPL<>0
                    * --- Variabili di appoggio per split payment
                    this.w_ROWSPL = this.w_ROWNUM
                    * --- Read from MOD_PAGA
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2],.t.,this.MOD_PAGA_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "MPSPLPAY"+;
                        " from "+i_cTable+" MOD_PAGA where ";
                            +"MPCODICE = "+cp_ToStrODBC(this.w_PTMODPAG);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        MPSPLPAY;
                        from (i_cTable) where;
                            MPCODICE = this.w_PTMODPAG;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_MPSPLPAY = NVL(cp_ToDate(_read_.MPSPLPAY),cp_NullValue(_read_.MPSPLPAY))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    if NVL(this.w_MPSPLPAY,"N")="S"
                      if this.w_OKPARSPL
                        * --- Aggiorna Partite a Saldo
                        * --- Insert into PAR_TITE
                        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                        i_commit = .f.
                        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                          cp_BeginTrs()
                          i_commit = .t.
                        endif
                        i_ccchkf=''
                        i_ccchkv=''
                        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                        if i_nConn<>0
                          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                      " ("+"CPROWNUM"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTCODCON"+",PTCODVAL"+",PTDATAPE"+",PTDATDOC"+",PTDATSCA"+",PTFLCRSA"+",PTFLIMPE"+",PTFLSOSP"+",PTMODPAG"+",PTNUMDOC"+",PTNUMPAR"+",PTROWORD"+",PTSERIAL"+",PTTIPCON"+",PTTOTIMP"+",PTCODAGE"+",PTDESRIG"+",PTSERRIF"+",PTNUMRIF"+",PTORDRIF"+",PTFLVABD"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
                          cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','CPROWNUM');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZSPL),'PAR_TITE','PT_SEGNO');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
                          +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLCRSA');
                          +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLIMPE');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_RIGASPL),'PAR_TITE','PTROWORD');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'PAR_TITE','PTCODAGE');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERRIF');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWSPL),'PAR_TITE','PTNUMRIF');
                          +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTORDRIF');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                               +i_ccchkv+")")
                        else
                          cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',1,'PT_SEGNO',this.w_APPSEZSPL,'PTALFDOC',this.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS,'PTCAOAPE',this.w_PNCAOVAL,'PTCAOVAL',this.w_PNCAOVAL,'PTCODCON',this.w_PNCODCLF,'PTCODVAL',this.w_PNCODVAL,'PTDATAPE',this.w_PNDATDOC,'PTDATDOC',this.w_PNDATDOC,'PTDATSCA',this.w_PTDATSCA)
                          insert into (i_cTable) (CPROWNUM,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTCODCON,PTCODVAL,PTDATAPE,PTDATDOC,PTDATSCA,PTFLCRSA,PTFLIMPE,PTFLSOSP,PTMODPAG,PTNUMDOC,PTNUMPAR,PTROWORD,PTSERIAL,PTTIPCON,PTTOTIMP,PTCODAGE,PTDESRIG,PTSERRIF,PTNUMRIF,PTORDRIF,PTFLVABD,PTDATREG,PTNUMCOR &i_ccchkf. );
                             values (;
                               1;
                               ,this.w_APPSEZSPL;
                               ,this.w_PNALFDOC;
                               ,this.w_PTBANAPP;
                               ,this.w_PTBANNOS;
                               ,this.w_PNCAOVAL;
                               ,this.w_PNCAOVAL;
                               ,this.w_PNCODCLF;
                               ,this.w_PNCODVAL;
                               ,this.w_PNDATDOC;
                               ,this.w_PNDATDOC;
                               ,this.w_PTDATSCA;
                               ,"S";
                               ," ";
                               ,this.w_PTFLSOSP;
                               ,this.w_PTMODPAG;
                               ,this.w_PNNUMDOC;
                               ,this.w_PTNUMPAR;
                               ,this.w_RIGASPL;
                               ,this.w_PNSERIAL;
                               ,this.w_PNTIPCLF;
                               ,this.w_PTTOTIMP;
                               ,this.w_CODAGE;
                               ,this.w_PTDESRIG;
                               ,this.w_PNSERIAL;
                               ,this.w_ROWSPL;
                               ,1;
                               ,this.w_PTFLVABD;
                               ,this.w_PNDATREG;
                               ,this.w_PTNUMCOR;
                               &i_ccchkv. )
                          i_Rows=iif(bTrsErr,0,1)
                        endif
                        if i_commit
                          cp_EndTrs(.t.)
                        endif
                        if i_Rows<0 or bTrsErr
                          * --- Error: insert not accepted
                          i_Error=MSG_INSERT_ERROR
                          return
                        endif
                      endif
                    endif
                  endif
                  * --- Totale Partite per Eventuale Abbinamento
                  this.w_PARFOR = this.w_PARFOR + this.w_PTTOTIMP
                  * --- Insert into PAR_TITE
                  i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMDIS"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTFLCRSA"+",PTFLIMPE"+",PTTOTABB"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTROWORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PAR_TITE','PTFLCRSA');
                    +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                    +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',1,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PT_SEGNO',this.w_APPSEZ,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PNCODVAL,'PTCAOVAL',this.w_PNCAOVAL,'PTCAOAPE',this.w_PNCAOVAL)
                    insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMDIS,PTFLINDI,PTBANNOS,PTFLRAGG,PTFLCRSA,PTFLIMPE,PTTOTABB,PTDESRIG,PTCODAGE,PTFLVABD,PTDATREG,PTNUMCOR &i_ccchkf. );
                       values (;
                         this.w_PNSERIAL;
                         ,1;
                         ,this.w_ROWNUM;
                         ,this.w_PTNUMPAR;
                         ,this.w_PTDATSCA;
                         ,this.w_PNTIPCLF;
                         ,this.w_PNCODCLF;
                         ,this.w_APPSEZ;
                         ,this.w_PTTOTIMP;
                         ,this.w_PNCODVAL;
                         ,this.w_PNCAOVAL;
                         ,this.w_PNCAOVAL;
                         ,this.w_PNDATDOC;
                         ,this.w_PNNUMDOC;
                         ,this.w_PNALFDOC;
                         ,this.w_PNDATDOC;
                         ,this.w_PNTOTDOC;
                         ,this.w_PTMODPAG;
                         ,this.w_PTFLSOSP;
                         ,this.w_PTBANAPP;
                         ," ";
                         ," ";
                         ,this.w_PTBANNOS;
                         ," ";
                         ,this.w_PNFLPART;
                         ,"  ";
                         ,0;
                         ,this.w_PTDESRIG;
                         ,this.w_PTCODAGE;
                         ,this.w_PTFLVABD;
                         ,this.w_PNDATREG;
                         ,this.w_PTNUMCOR;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                    select _Curs_DOC_RATE
                    continue
                  enddo
                  use
                endif
              endif
            endif
          endif
        endif
      case this.w_GESTPAR = "FINANZIA"
        if this.w_RIFFIN>0
          * --- Creo Partita di Saldo Relativa allo sconto finanziario
          * --- Read from PAR_TITE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PTNUMPAR,PTDATSCA,PTTOTIMP,PTMODPAG,PTBANAPP,PTDESRIG,PTNUMCOR,PT_SEGNO"+;
              " from "+i_cTable+" PAR_TITE where ";
                  +"PTSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                  +" and PTROWORD = "+cp_ToStrODBC(1);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFFIN);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PTNUMPAR,PTDATSCA,PTTOTIMP,PTMODPAG,PTBANAPP,PTDESRIG,PTNUMCOR,PT_SEGNO;
              from (i_cTable) where;
                  PTSERIAL = this.w_PNSERIAL;
                  and PTROWORD = 1;
                  and CPROWNUM = this.w_RIFFIN;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PTNUMPAR = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
            this.w_PTDATSCA = NVL(cp_ToDate(_read_.PTDATSCA),cp_NullValue(_read_.PTDATSCA))
            this.w_PTTOTIMP = NVL(cp_ToDate(_read_.PTTOTIMP),cp_NullValue(_read_.PTTOTIMP))
            this.w_PTMODPAG = NVL(cp_ToDate(_read_.PTMODPAG),cp_NullValue(_read_.PTMODPAG))
            this.w_PTBANAPP = NVL(cp_ToDate(_read_.PTBANAPP),cp_NullValue(_read_.PTBANAPP))
            this.w_PTDESRIG = NVL(cp_ToDate(_read_.PTDESRIG),cp_NullValue(_read_.PTDESRIG))
            this.w_PTNUMCOR = NVL(cp_ToDate(_read_.PTNUMCOR),cp_NullValue(_read_.PTNUMCOR))
            this.w_APPSEZ = NVL(cp_ToDate(_read_.PT_SEGNO),cp_NullValue(_read_.PT_SEGNO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_APPSEZ = IIF(this.w_APPSEZ="D", "A", "D")
          this.w_PTDESRIG = IIF(EMPTY(this.w_PTDESRIG), this.w_PNDESSUP, this.w_PTDESRIG)
          * --- Insert into PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTCODVAL"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTDATAPE"+",PTDATDOC"+",PTFLCRSA"+",PTFLIMPE"+",PTFLINDI"+",PTFLRAGG"+",PTFLSOSP"+",PTIMPDOC"+",PTMODPAG"+",PTNUMDIS"+",PTNUMDOC"+",PTTOTABB"+",PTTOTIMP"+",PTDESRIG"+",PTNUMCOR"+",PTDATREG"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
            +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
            +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLCRSA');
            +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTORDRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_RIFFIN),'PAR_TITE','PTNUMRIF');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_CPROWNUM,'CPROWNUM',1,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PNDATDOC,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PTCODVAL',this.w_PNCODVAL,'PT_SEGNO',this.w_APPSEZ,'PTALFDOC',this.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS)
            insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTDATAPE,PTDATDOC,PTFLCRSA,PTFLIMPE,PTFLINDI,PTFLRAGG,PTFLSOSP,PTIMPDOC,PTMODPAG,PTNUMDIS,PTNUMDOC,PTTOTABB,PTTOTIMP,PTDESRIG,PTNUMCOR,PTDATREG,PTSERRIF,PTORDRIF,PTNUMRIF &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 ,this.w_CPROWNUM;
                 ,1;
                 ,this.w_PTNUMPAR;
                 ,this.w_PNDATDOC;
                 ,this.w_PNTIPCLF;
                 ,this.w_PNCODCLF;
                 ,this.w_PNCODVAL;
                 ,this.w_APPSEZ;
                 ,this.w_PNALFDOC;
                 ,this.w_PTBANAPP;
                 ,this.w_PTBANNOS;
                 ,this.w_PNCAOVAL;
                 ,this.w_PNCAOVAL;
                 ,this.w_PNDATDOC;
                 ,this.w_PNDATDOC;
                 ,"S";
                 ,"  ";
                 ," ";
                 ," ";
                 ," ";
                 ,this.w_PNTOTDOC;
                 ,this.w_PTMODPAG;
                 ,SPACE(10);
                 ,this.w_PNNUMDOC;
                 ,0;
                 ,this.w_PTTOTIMP;
                 ,this.w_PNDESSUP;
                 ,this.w_PTNUMCOR;
                 ,this.w_PNDATREG;
                 ,this.w_PNSERIAL;
                 ,1;
                 ,this.w_RIFFIN;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        if this.w_RIFRIT>0
          * --- Creo Partita di Saldo Relativa alla ritenuta attiva
          * --- Read from PAR_TITE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PTNUMPAR,PTDATSCA,PTTOTIMP,PTMODPAG,PTBANAPP,PTDESRIG,PTNUMCOR,PT_SEGNO"+;
              " from "+i_cTable+" PAR_TITE where ";
                  +"PTSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                  +" and PTROWORD = "+cp_ToStrODBC(1);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFRIT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PTNUMPAR,PTDATSCA,PTTOTIMP,PTMODPAG,PTBANAPP,PTDESRIG,PTNUMCOR,PT_SEGNO;
              from (i_cTable) where;
                  PTSERIAL = this.w_PNSERIAL;
                  and PTROWORD = 1;
                  and CPROWNUM = this.w_RIFRIT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PTNUMPAR = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
            this.w_PTDATSCA = NVL(cp_ToDate(_read_.PTDATSCA),cp_NullValue(_read_.PTDATSCA))
            this.w_PTTOTIMP = NVL(cp_ToDate(_read_.PTTOTIMP),cp_NullValue(_read_.PTTOTIMP))
            this.w_PTMODPAG = NVL(cp_ToDate(_read_.PTMODPAG),cp_NullValue(_read_.PTMODPAG))
            this.w_PTBANAPP = NVL(cp_ToDate(_read_.PTBANAPP),cp_NullValue(_read_.PTBANAPP))
            this.w_PTDESRIG = NVL(cp_ToDate(_read_.PTDESRIG),cp_NullValue(_read_.PTDESRIG))
            this.w_PTNUMCOR = NVL(cp_ToDate(_read_.PTNUMCOR),cp_NullValue(_read_.PTNUMCOR))
            this.w_APPSEZ = NVL(cp_ToDate(_read_.PT_SEGNO),cp_NullValue(_read_.PT_SEGNO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_APPSEZ = IIF(this.w_APPSEZ="D", "A", "D")
          this.w_PTDESRIG = IIF(EMPTY(this.w_PTDESRIG), this.w_PNDESSUP, this.w_PTDESRIG)
          * --- Insert into PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTCODVAL"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTDATAPE"+",PTDATDOC"+",PTFLCRSA"+",PTFLIMPE"+",PTFLINDI"+",PTFLRAGG"+",PTFLSOSP"+",PTIMPDOC"+",PTMODPAG"+",PTNUMDIS"+",PTNUMDOC"+",PTTOTABB"+",PTTOTIMP"+",PTDESRIG"+",PTNUMCOR"+",PTDATREG"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
            +","+cp_NullLink(cp_ToStrODBC(2),'PAR_TITE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
            +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLCRSA');
            +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTORDRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_RIFRIT),'PAR_TITE','PTNUMRIF');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_CPROWNUM,'CPROWNUM',2,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PNDATDOC,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PTCODVAL',this.w_PNCODVAL,'PT_SEGNO',this.w_APPSEZ,'PTALFDOC',this.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS)
            insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTDATAPE,PTDATDOC,PTFLCRSA,PTFLIMPE,PTFLINDI,PTFLRAGG,PTFLSOSP,PTIMPDOC,PTMODPAG,PTNUMDIS,PTNUMDOC,PTTOTABB,PTTOTIMP,PTDESRIG,PTNUMCOR,PTDATREG,PTSERRIF,PTORDRIF,PTNUMRIF &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 ,this.w_CPROWNUM;
                 ,2;
                 ,this.w_PTNUMPAR;
                 ,this.w_PNDATDOC;
                 ,this.w_PNTIPCLF;
                 ,this.w_PNCODCLF;
                 ,this.w_PNCODVAL;
                 ,this.w_APPSEZ;
                 ,this.w_PNALFDOC;
                 ,this.w_PTBANAPP;
                 ,this.w_PTBANNOS;
                 ,this.w_PNCAOVAL;
                 ,this.w_PNCAOVAL;
                 ,this.w_PNDATDOC;
                 ,this.w_PNDATDOC;
                 ,"S";
                 ,"  ";
                 ," ";
                 ," ";
                 ," ";
                 ,this.w_PNTOTDOC;
                 ,this.w_PTMODPAG;
                 ,SPACE(10);
                 ,this.w_PNNUMDOC;
                 ,0;
                 ,this.w_PTTOTIMP;
                 ,this.w_PNDESSUP;
                 ,this.w_PTNUMCOR;
                 ,this.w_PNDATREG;
                 ,this.w_PNSERIAL;
                 ,1;
                 ,this.w_RIFRIT;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      case this.w_GESTPAR = "RITATTI"
        if this.w_RIFRIT>0
          * --- Creo Partita di Saldo Relativa alla ritenuta attiva
          * --- Read from PAR_TITE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PTNUMPAR,PTDATSCA,PTTOTIMP,PTMODPAG,PTBANAPP,PTDESRIG,PTNUMCOR,PT_SEGNO"+;
              " from "+i_cTable+" PAR_TITE where ";
                  +"PTSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                  +" and PTROWORD = "+cp_ToStrODBC(1);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIFRIT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PTNUMPAR,PTDATSCA,PTTOTIMP,PTMODPAG,PTBANAPP,PTDESRIG,PTNUMCOR,PT_SEGNO;
              from (i_cTable) where;
                  PTSERIAL = this.w_PNSERIAL;
                  and PTROWORD = 1;
                  and CPROWNUM = this.w_RIFRIT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PTNUMPAR = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
            this.w_PTDATSCA = NVL(cp_ToDate(_read_.PTDATSCA),cp_NullValue(_read_.PTDATSCA))
            this.w_PTTOTIMP = NVL(cp_ToDate(_read_.PTTOTIMP),cp_NullValue(_read_.PTTOTIMP))
            this.w_PTMODPAG = NVL(cp_ToDate(_read_.PTMODPAG),cp_NullValue(_read_.PTMODPAG))
            this.w_PTBANAPP = NVL(cp_ToDate(_read_.PTBANAPP),cp_NullValue(_read_.PTBANAPP))
            this.w_PTDESRIG = NVL(cp_ToDate(_read_.PTDESRIG),cp_NullValue(_read_.PTDESRIG))
            this.w_PTNUMCOR = NVL(cp_ToDate(_read_.PTNUMCOR),cp_NullValue(_read_.PTNUMCOR))
            this.w_APPSEZ = NVL(cp_ToDate(_read_.PT_SEGNO),cp_NullValue(_read_.PT_SEGNO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_APPSEZ = IIF(this.w_APPSEZ="D", "A", "D")
          this.w_PTDESRIG = IIF(EMPTY(this.w_PTDESRIG), this.w_PNDESSUP, this.w_PTDESRIG)
          * --- Insert into PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTCODVAL"+",PT_SEGNO"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTCAOAPE"+",PTCAOVAL"+",PTDATAPE"+",PTDATDOC"+",PTFLCRSA"+",PTFLIMPE"+",PTFLINDI"+",PTFLRAGG"+",PTFLSOSP"+",PTIMPDOC"+",PTMODPAG"+",PTNUMDIS"+",PTNUMDOC"+",PTTOTABB"+",PTTOTIMP"+",PTDESRIG"+",PTNUMCOR"+",PTDATREG"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
            +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PAR_TITE','PTTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PAR_TITE','PTCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PAR_TITE','PTCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PAR_TITE','PTCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
            +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLCRSA');
            +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERRIF');
            +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','PTORDRIF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_RIFRIT),'PAR_TITE','PTNUMRIF');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_CPROWNUM,'CPROWNUM',1,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PNDATDOC,'PTTIPCON',this.w_PNTIPCLF,'PTCODCON',this.w_PNCODCLF,'PTCODVAL',this.w_PNCODVAL,'PT_SEGNO',this.w_APPSEZ,'PTALFDOC',this.w_PNALFDOC,'PTBANAPP',this.w_PTBANAPP,'PTBANNOS',this.w_PTBANNOS)
            insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL,PT_SEGNO,PTALFDOC,PTBANAPP,PTBANNOS,PTCAOAPE,PTCAOVAL,PTDATAPE,PTDATDOC,PTFLCRSA,PTFLIMPE,PTFLINDI,PTFLRAGG,PTFLSOSP,PTIMPDOC,PTMODPAG,PTNUMDIS,PTNUMDOC,PTTOTABB,PTTOTIMP,PTDESRIG,PTNUMCOR,PTDATREG,PTSERRIF,PTORDRIF,PTNUMRIF &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 ,this.w_CPROWNUM;
                 ,1;
                 ,this.w_PTNUMPAR;
                 ,this.w_PNDATDOC;
                 ,this.w_PNTIPCLF;
                 ,this.w_PNCODCLF;
                 ,this.w_PNCODVAL;
                 ,this.w_APPSEZ;
                 ,this.w_PNALFDOC;
                 ,this.w_PTBANAPP;
                 ,this.w_PTBANNOS;
                 ,this.w_PNCAOVAL;
                 ,this.w_PNCAOVAL;
                 ,this.w_PNDATDOC;
                 ,this.w_PNDATDOC;
                 ,"S";
                 ,"  ";
                 ," ";
                 ," ";
                 ," ";
                 ,this.w_PNTOTDOC;
                 ,this.w_PTMODPAG;
                 ,SPACE(10);
                 ,this.w_PNNUMDOC;
                 ,0;
                 ,this.w_PTTOTIMP;
                 ,this.w_PNDESSUP;
                 ,this.w_PTNUMCOR;
                 ,this.w_PNDATREG;
                 ,this.w_PNSERIAL;
                 ,1;
                 ,this.w_RIFRIT;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      otherwise
        if this.w_GESTPAR = "ESDIF" And this.w_PNFLPART="N"
          this.w_PTNUMPAR = CANUMPAR("N", this.w_PNCODESE, this.w_PNNUMDOC, this.w_PNALFDOC)
          this.w_PTDATSCA = this.w_PNDATDOC
          this.w_PTTOTIMP = ABS(this.w_APPVAL)
          * --- Inverte la sezione se Negativo..
          this.w_APPSEZ = IIF(this.w_APPVAL<0, IIF(this.w_APPSEZ="D", "A", "D"), this.w_APPSEZ)
          * --- Insert into PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMDIS"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTFLCRSA"+",PTFLIMPE"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
            +","+cp_NullLink(cp_ToStrODBC(1),'PAR_TITE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PAR_TITE','PTTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PAR_TITE','PTCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_APPSEZ),'PAR_TITE','PT_SEGNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PAR_TITE','PTCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAONAZ),'PAR_TITE','PTCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAONAZ),'PAR_TITE','PTCAOAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPPAB),'PAR_TITE','PTMODPAG');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTBANAPP');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTBANNOS');
            +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
            +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODAGE),'PAR_TITE','PTCODAGE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLVABD),'PAR_TITE','PTFLVABD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(25)),'PAR_TITE','PTNUMCOR');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_CPROWNUM,'CPROWNUM',1,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_PNTIPCON,'PTCODCON',this.w_PNCODCON,'PT_SEGNO',this.w_APPSEZ,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PNVALNAZ,'PTCAOVAL',this.w_CAONAZ,'PTCAOAPE',this.w_CAONAZ)
            insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMDIS,PTFLINDI,PTBANNOS,PTFLRAGG,PTFLCRSA,PTFLIMPE,PTDESRIG,PTCODAGE,PTFLVABD,PTDATREG,PTNUMCOR &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 ,this.w_CPROWNUM;
                 ,1;
                 ,this.w_PTNUMPAR;
                 ,this.w_PTDATSCA;
                 ,this.w_PNTIPCON;
                 ,this.w_PNCODCON;
                 ,this.w_APPSEZ;
                 ,this.w_PTTOTIMP;
                 ,this.w_PNVALNAZ;
                 ,this.w_CAONAZ;
                 ,this.w_CAONAZ;
                 ,this.w_PNDATDOC;
                 ,this.w_PNNUMDOC;
                 ,this.w_PNALFDOC;
                 ,this.w_PNDATDOC;
                 ,this.w_PNTOTDOC;
                 ,this.oParentObject.w_TIPPAB;
                 ," ";
                 ," ";
                 ," ";
                 ," ";
                 ," ";
                 ," ";
                 ,"C";
                 ,"  ";
                 ,this.w_PNDESSUP;
                 ,this.w_PNCODAGE;
                 ,this.w_PNFLVABD;
                 ,this.w_PNDATREG;
                 ,SPACE(25);
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          do case
            case this.w_PNFLPART="C"
              if Not Empty( this.w_PNCODPAG )
                * --- Leggo le informazioni per calcolare le scadenze
                * --- Read from CONTI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ANGIOSC1,ANGIOSC2,AN1MESCL,AN2MESCL,ANGIOFIS"+;
                    " from "+i_cTable+" CONTI where ";
                        +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                        +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ANGIOSC1,ANGIOSC2,AN1MESCL,AN2MESCL,ANGIOFIS;
                    from (i_cTable) where;
                        ANTIPCON = this.w_PNTIPCON;
                        and ANCODICE = this.w_PNCODCON;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_GIORN1 = NVL(cp_ToDate(_read_.ANGIOSC1),cp_NullValue(_read_.ANGIOSC1))
                  this.w_GIORN2 = NVL(cp_ToDate(_read_.ANGIOSC2),cp_NullValue(_read_.ANGIOSC2))
                  this.w_MESE1 = NVL(cp_ToDate(_read_.AN1MESCL),cp_NullValue(_read_.AN1MESCL))
                  this.w_MESE2 = NVL(cp_ToDate(_read_.AN2MESCL),cp_NullValue(_read_.AN2MESCL))
                  this.w_GIOFIS = NVL(cp_ToDate(_read_.ANGIOFIS),cp_NullValue(_read_.ANGIOFIS))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                DIMENSION w_ARRSCA[999,6]
                this.oParentObject.w_DATINI = IIF(EMPTY(this.w_PNDATDOC), this.w_PNDATREG, this.w_PNDATDOC)
                this.w_IVA = IIF(this.w_CCCALDOC="E", this.w_TOTIVA , 0)
                this.w_NETTO = ABS(this.w_APPVAL)
                this.w_ESCL1 = STR(this.w_MESE1, 2, 0) + STR(this.w_GIORN1, 2, 0) + STR(this.w_GIOFIS, 2, 0)
                this.w_ESCL2 = STR(this.w_MESE2, 2, 0) + STR(this.w_GIORN2, 2, 0)
                this.w_NETTO = this.w_NETTO - this.w_IVA
                this.w_NURATE = SCADENZE("w_ARRSCA", this.w_PNCODPAG, this.oParentObject.w_DATINI, this.w_NETTO, this.w_IVA, 0, this.w_ESCL1, this.w_ESCL2, this.w_DECTOT)
                * --- Ciclo sull'Array per determinare le scadenze
                this.w_LOOP = 1
                do while this.w_LOOP<=this.w_NURATE
                  this.w_PTNUMPAR = CANUMPAR("N", this.w_PNCODESE, this.w_PNNUMDOC, this.w_PNALFDOC)
                  this.w_PTDATSCA = w_ARRSCA[ this.w_LOOP , 1]
                  * --- Segno della partita uguale a segno della riga di prima nota
                  this.w_PT_SEGNO = this.w_APPSEZ
                  this.w_PTTOTIMP = w_ARRSCA[ this.w_LOOP , 2] + w_ARRSCA[ this.w_LOOP , 3]
                  this.w_PTMODPAG = w_ARRSCA[ this.w_LOOP , 5]
                  * --- Insert into PAR_TITE
                  i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMDIS"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTFLCRSA"+",PTFLIMPE"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_LOOP),'PAR_TITE','CPROWNUM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PAR_TITE','PTTIPCON');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PAR_TITE','PTCODCON');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PAR_TITE','PTCODVAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CAONAZ),'PAR_TITE','PTCAOVAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CAONAZ),'PAR_TITE','PTCAOAPE');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATAPE');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PAR_TITE','PTNUMDOC');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PAR_TITE','PTALFDOC');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PAR_TITE','PTDATDOC');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PAR_TITE','PTIMPDOC');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTBANAPP');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTBANNOS');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
                    +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
                    +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PAR_TITE','PTDESRIG');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODAGE),'PAR_TITE','PTCODAGE');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLVABD),'PAR_TITE','PTFLVABD');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
                    +","+cp_NullLink(cp_ToStrODBC(SPACE(25)),'PAR_TITE','PTNUMCOR');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_CPROWNUM,'CPROWNUM',this.w_LOOP,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_PNTIPCON,'PTCODCON',this.w_PNCODCON,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PNVALNAZ,'PTCAOVAL',this.w_CAONAZ,'PTCAOAPE',this.w_CAONAZ)
                    insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMDIS,PTFLINDI,PTBANNOS,PTFLRAGG,PTFLCRSA,PTFLIMPE,PTDESRIG,PTCODAGE,PTFLVABD,PTDATREG,PTNUMCOR &i_ccchkf. );
                       values (;
                         this.w_PNSERIAL;
                         ,this.w_CPROWNUM;
                         ,this.w_LOOP;
                         ,this.w_PTNUMPAR;
                         ,this.w_PTDATSCA;
                         ,this.w_PNTIPCON;
                         ,this.w_PNCODCON;
                         ,this.w_PT_SEGNO;
                         ,this.w_PTTOTIMP;
                         ,this.w_PNVALNAZ;
                         ,this.w_CAONAZ;
                         ,this.w_CAONAZ;
                         ,this.w_PNDATDOC;
                         ,this.w_PNNUMDOC;
                         ,this.w_PNALFDOC;
                         ,this.w_PNDATDOC;
                         ,this.w_PNTOTDOC;
                         ,this.w_PTMODPAG;
                         ," ";
                         ," ";
                         ," ";
                         ," ";
                         ," ";
                         ," ";
                         ,"C";
                         ,"  ";
                         ,this.w_PNDESSUP;
                         ,this.w_PNCODAGE;
                         ,this.w_PNFLVABD;
                         ,this.w_PNDATREG;
                         ,SPACE(25);
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                  this.w_LOOP = this.w_LOOP + 1
                enddo
              else
                this.w_MESS_ERR = ah_MsgFormat("Pagamento non definito, impossibile creare le partite! - provvedere manualmente")
                this.Page_14()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            case this.w_PNFLPART="A" Or this.w_PNFLPART="S"
              * --- Segnalo la mancanza di partite di saldo e proseguo...
              if this.w_PNFLPART="S"
                this.w_MESS_ERR = ah_MsgFormat("Partite di saldo inesistentii! - provvedere manualmente")
              else
                this.w_MESS_ERR = ah_MsgFormat("Partite di acconto inesistente! - provvedere manualmente")
              endif
              this.Page_14()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
          endcase
        endif
    endcase
  endproc


  procedure Page_14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Log Errori, scrive dati relativi alla registrazione
    *     Se prima scrittura non metto separatore...
    * --- Questa variabile contiene il messaggio di errore da stampare..
    * --- inserisco al cambio registrazione le informazioni di testata
    if this.w_LASTWAR<> this.w_OSERIAL
      if this.w_oErrorLog.IsFullLog()
        this.w_oERRORLOG.AddMsgLogNoTranslate(Repl("=",70))     
      endif
      if this.oParentObject.w_FLVEAC="V"
        this.w_APPO = ah_MsgFormat("Verificare doc.N.: %1 del.: %2",ALLTRIM(STR(this.w_PNNUMDOC,15))+IIF(EMPTY(this.w_PNALFDOC),"","/"+Alltrim(this.w_PNALFDOC)),DTOC(this.w_PNDATDOC))
      else
        this.w_APPO = ah_MsgFormat("Verificare prot.N.: %1 del.: %2",ALLTRIM(STR(this.w_PNNUMPRO,15))+IIF(EMPTY(this.w_PNALFPRO),"","/"+Alltrim(this.w_PNALFPRO)),DTOC(this.w_PNDATDOC))
      endif
      this.w_oERRORLOG.AddMsgLogNoTranslate(this.w_APPO)     
      this.w_oERRORLOG.AddMsgLogNoTranslate(Repl("=",70))     
      this.w_LASTWAR = this.w_OSERIAL
      this.w_LASTWARROW = "XXXXXX"
      * --- Se Warning conto le registrazioni...
      if Not this.w_FLERR
        this.w_WADOC = this.w_WADOC + 1
      endif
    endif
    if Not this.w_FLERR
      * --- Inserisco al cambio di riga il numero di riga (solo Warning)
      *     Se YY non segnalo il numero di riga.
      if this.w_LASTWARROW<> this.w_NR And this.w_NR<>"YY"
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(4), "Verificare riga primanota %1", this.w_NR)     
        this.w_LASTWARROW = this.w_NR
      endif
    endif
    * --- Messaggio di errore...
    if Empty( this.w_MESS_ERR )
      this.w_MESS_ERR = ah_MsgFormat("Anomalia non codificata")
    endif
    * --- Al termine di pag4, se ho problemi debbo visualizzare l'elenco degli errori
    *     riscontrati presenti nell'array quindi w_MESS_ERR non � significativo
    if this.w_MESS_ERR<>"NO"
      this.w_oERRORLOG.AddMsgLogNoTranslate(space(10) + this.w_MESS_ERR)     
    endif
  endproc


  procedure Page_15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SCRIVE RIFERIMENTI CESPITI NELLA TABELLA  PNT_CESP
    this.w_CPORDCES = 0
    * --- Select from query\GSVE5QCD
    do vq_exec with 'query\GSVE5QCD',this,'_Curs_query_GSVE5QCD','',.f.,.t.
    if used('_Curs_query_GSVE5QCD')
      select _Curs_query_GSVE5QCD
      locate for 1=1
      do while not(eof())
      this.w_CPORDCES = this.w_CPORDCES+10
      this.w_MVCESSER = NVL(_Curs_query_GSVE5QCD.MVCESSER,SPACE(10))
      if NOT EMPTY(this.w_MVCESSER)
        * --- Try
        local bErr_058B8B00
        bErr_058B8B00=bTrsErr
        this.Try_058B8B00()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_058B8B00
        * --- End
        this.w_INSCESP = .T.
      endif
        select _Curs_query_GSVE5QCD
        continue
      enddo
      use
    endif
  endproc
  proc Try_058B8B00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PNT_CESP
    i_nConn=i_TableProp[this.PNT_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ACSERIAL"+",ACMOVCES"+",ACTIPASS"+",CPROWORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_CESP','ACSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCESSER),'PNT_CESP','ACMOVCES');
      +","+cp_NullLink(cp_ToStrODBC("D"),'PNT_CESP','ACTIPASS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPORDCES),'PNT_CESP','CPROWORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ACSERIAL',this.w_PNSERIAL,'ACMOVCES',this.w_MVCESSER,'ACTIPASS',"D",'CPROWORD',this.w_CPORDCES)
      insert into (i_cTable) (ACSERIAL,ACMOVCES,ACTIPASS,CPROWORD &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_MVCESSER;
           ,"D";
           ,this.w_CPORDCES;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag16
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_SEARCH_TABLE="CAU_CONT"
        if USED("CURS_CAU_CONT")
          Select CURS_CAU_CONT 
 LOCATE FOR CCCODICE=this.w_SEARCH_CAU_CONT
          if NOT FOUND( ) AND NOT EMPTY(NVL(this.w_SEARCH_CAU_CONT,""))
            VQ_EXEC("QUERY\GSVECBCV.VQR",this,"TMP_CURS_CAU_CONT")
            if USED("TMP_CURS_CAU_CONT")
              Select TMP_CURS_CAU_CONT
              scatter memvar
              Insert into CURS_CAU_CONT from memvar
              Select TMP_CURS_CAU_CONT 
 Use 
 Select CURS_CAU_CONT
            endif
          endif
        else
          VQ_EXEC("QUERY\GSVECBCV.VQR",this,"CURS_CAU_CONT") 
 wr_curs_cau_cont=WRCURSOR("CURS_CAU_CONT") 
 Select CURS_CAU_CONT
        endif
      case this.w_SEARCH_TABLE="VOCIIVA"
        if USED("CURS_VOCIIVA")
          Select CURS_VOCIIVA 
 LOCATE FOR IVCODIVA=this.w_SEARCH_IVCODIVA
          if NOT FOUND( ) AND NOT EMPTY(NVL(this.w_SEARCH_IVCODIVA,"")) 
            VQ_EXEC("QUERY\GSVEVBCV.VQR",this,"TMP_CURS_VOCIIVA")
            if USED("TMP_CURS_VOCIIVA")
              Select TMP_CURS_VOCIIVA
              scatter memvar
              Insert into CURS_VOCIIVA from memvar
              Select TMP_CURS_VOCIIVA 
 Use 
 Select CURS_VOCIIVA
            endif
          endif
        else
          VQ_EXEC("QUERY\GSVEVBCV.VQR",this,"CURS_VOCIIVA") 
 wr_CURS_VOCIIVA=WRCURSOR("CURS_VOCIIVA") 
 Select CURS_VOCIIVA
        endif
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,26)]
    this.cWorkTables[1]='ATTIDETT'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='CAUIVA'
    this.cWorkTables[4]='CAUIVA1'
    this.cWorkTables[5]='CAU_CONT'
    this.cWorkTables[6]='CCM_DETT'
    this.cWorkTables[7]='COC_MAST'
    this.cWorkTables[8]='CONTI'
    this.cWorkTables[9]='CONTVEAC'
    this.cWorkTables[10]='DOC_MAST'
    this.cWorkTables[11]='DOC_RATE'
    this.cWorkTables[12]='ESERCIZI'
    this.cWorkTables[13]='MOD_PAGA'
    this.cWorkTables[14]='PAR_TITE'
    this.cWorkTables[15]='PNT_DETT'
    this.cWorkTables[16]='PNT_IVA'
    this.cWorkTables[17]='PNT_MAST'
    this.cWorkTables[18]='SALDICON'
    this.cWorkTables[19]='VALUTE'
    this.cWorkTables[20]='VOCIIVA'
    this.cWorkTables[21]='COLLCENT'
    this.cWorkTables[22]='MOVICOST'
    this.cWorkTables[23]='MASTRI'
    this.cWorkTables[24]='PNT_CESP'
    this.cWorkTables[25]='DATIRITE'
    this.cWorkTables[26]='CONTROPA'
    return(this.OpenAllTables(26))

  proc CloseCursors()
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_query_gscg_qd0')
      use in _Curs_query_gscg_qd0
    endif
    if used('_Curs_QUERY_CAUGIR')
      use in _Curs_QUERY_CAUGIR
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_gscg_bmc')
      use in _Curs_gscg_bmc
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_GSCGABCV')
      use in _Curs_GSCGABCV
    endif
    if used('_Curs_GSCGCQRC')
      use in _Curs_GSCGCQRC
    endif
    if used('_Curs_DOC_RATE')
      use in _Curs_DOC_RATE
    endif
    if used('_Curs_DOC_RATE')
      use in _Curs_DOC_RATE
    endif
    if used('_Curs_query_GSVE5QCD')
      use in _Curs_query_GSVE5QCD
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
