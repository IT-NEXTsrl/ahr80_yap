* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bpo                                                        *
*              Batch Gadget CalendarPlus                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-01-13                                                      *
* Last revis.: 2015-02-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bpo",oParentObject)
return(i_retval)

define class tgsut_bpo as StdBatch
  * --- Local variables
  cCursor = space(10)
  cNewTime = space(20)
  cOldTime = space(0)
  nCount = 0
  nInterval = 0
  nHourInit = 0
  nHourFin = 0
  nQueryHour = 0
  cFirstTime = space(4)
  cLastTime = space(4)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.cCursor = this.oParentObject.w_oGrid.cCursor
    this.nCount = 1
    =WRCURSOR( this.cCursor )
    Select (this.cCursor)
    Go Top
    SCAN
    this.cOldTime = Nvl(Time, "")
    * --- Calcolo la durata minima del postit in secondi
    this.nInterval = (Val(Left(this.oParentObject.w_INTERVAL,At(":",this.oParentObject.w_INTERVAL)-1))*3600) + (Val(Right(this.oParentObject.w_INTERVAL,At(":",this.oParentObject.w_INTERVAL)-1))*60)
    if TTOD( DateStart ) = this.oParentObject.w_POSTDATE
      * --- Sono nel primo giorno valido del postit
      this.cFirstTime = SUBSTR(TTOC( DateStart ),12,5)
      this.nQueryHour = (Hour(DateStart)*3600) + (Minute(DateStart)*60)
      this.nHourFin = (Val(Left(this.oParentObject.w_WORKFIN,At(":",this.oParentObject.w_WORKFIN)-1))*3600) + (Val(Right(this.oParentObject.w_WORKFIN,At(":",this.oParentObject.w_WORKFIN)-1))*60)
      if this.nQueryHour+this.nInterval > this.nHourFin
        * --- Correggo l'orario di fine in modo che sia pi� grande di quello di inizio
        this.cLastTime = Right("00"+Alltrim(Transform(Min(this.nQueryHour+this.nInterval,86399)/60)),2)+":"+Right("00"+Alltrim(Transform(Min(this.nQueryHour+this.nInterval,86399)/3600)),2)
      else
        this.cLastTime = this.oParentObject.w_WORKFIN
      endif
    endif
    if !Empty(Nvl(DateStop, "")) And TTOD( DateStop ) = this.oParentObject.w_POSTDATE
      * --- Sono nell'ultimo giorno valido del postit
      this.cLastTime = SUBSTR(TTOC( DateStop ),12,5)
      this.nQueryHour = (Hour(DateStop)*3600) + (Minute(DateStop)*60)
      this.nHourInit = (Val(Left(this.oParentObject.w_WORKINI,At(":",this.oParentObject.w_WORKINI)-1))*3600) + (Val(Right(this.oParentObject.w_WORKINI,At(":",this.oParentObject.w_WORKINI)-1))*60)
      if this.nQueryHour-this.nInterval < this.nHourInit
        * --- Correggo l'orario di inizio in modo che sia pi� piccolo di quello di fine
        this.cFirstTime = Right("00"+Alltrim(Transform(Max(this.nQueryHour-this.nInterval, 0)/60)),2)+":"+Right("00"+Alltrim(Transform(Max(this.nQueryHour-this.nInterval, 0)/3600)),2)
      else
        this.cFirstTime = Evl( this.cFirstTime, this.oParentObject.w_WORKINI)
      endif
    endif
    * --- Se siamo in mezzo all'orario di inizio e fine, inserisco gli orari di inizio e fine lavoro
    this.cFirstTime = Evl( this.cFirstTime, this.oParentObject.w_WORKINI)
    this.cLastTime = Evl( this.cLastTime, this.oParentObject.w_WORKFIN)
    this.cNewTime = this.cFirstTime + " - " + this.cLastTime
    if !(this.cOldTime == this.cNewTime)
      Update (this.cCursor) Set Time = this.cNewTime Where Recno() = this.nCount
      Goto (this.nCount)
    endif
    this.nCount = this.nCount + 1
    Select (this.cCursor)
    ENDSCAN
    Go Top
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
