* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bcg                                                        *
*              Configurazioni gestione                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-17                                                      *
* Last revis.: 2012-02-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPER,w_ROWNUM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bcg",oParentObject,m.w_OPER,m.w_ROWNUM)
return(i_retval)

define class tgsut_bcg as StdBatch
  * --- Local variables
  w_OPER = space(1)
  w_ROWNUM = 0
  w_pPARENT = .NULL.
  w_GSUT_MMC = .NULL.
  w_GSDESC = space(50)
  w_GSNOME = space(30)
  w_CGMODSAV = space(1)
  w_CGDESCFG = space(50)
  w_NOMEAP = space(30)
  * --- WorkFile variables
  MODDCONF_idx=0
  PRCFGGES_idx=0
  CFG_GEST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge configurazioni gestione
    this.w_pPARENT = this.oParentObject
    this.bUpdateParentObject=.f.
    * --- Nome Gestione
    this.w_GSNOME = PADR(UPPER(this.w_pPARENT.getSecurityCode()), 30)
    do case
      case this.w_OPER="T"
        * --- Tipologia menu
        * --- Read from MODDCONF
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MODDCONF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MODDCONF_idx,2],.t.,this.MODDCONF_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" MODDCONF where ";
                +"MCNOMGES = "+cp_ToStrODBC(this.w_GSNOME);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                MCNOMGES = this.w_GSNOME;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows > 0
          * --- Ho un modello, cerco le configurazioni,  restituisco il cursore con le descrizioni
          *     delle cfg
          USE IN SELECT("CURSCFGGEST")
          vq_exec(".\QUERY\GSUT_BLC.VQR", THIS, "CURSCFGGEST")
          i_retcode = 'stop'
          i_retval = RECCOUNT("CURSCFGGEST")
          return
        else
          * --- Non ho un modello per la gestione
          i_retcode = 'stop'
          i_retval = -1
          return
        endif
      case this.w_OPER="M"
        * --- Carica modello
        * --- Inserisco la nuova gestione
        this.w_GSUT_MMC = GSUT_MMC()
        if !(this.w_GSUT_MMC.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Descrizione gestione
        this.w_GSDESC = Left(this.w_pPARENT.Caption, 50)
        this.w_GSUT_MMC.ecpLoad()     
        this.w_GSUT_MMC.WorkFromTrs()     
        this.w_GSUT_MMC.w_MCNOMGES = this.w_GSNOME
        this.w_GSUT_MMC.w_MCDESGES = this.w_GSDESC
        this.w_GSUT_MMC.TrsFromWork()     
        this.w_GSUT_MMC.mCalc(.t.)     
        * --- Carico parametri
        GSUT_BCP(this,this.w_pPARENT, this.w_GSUT_MMC, "C")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      case this.w_OPER="S" Or this.w_OPER="F"
        * --- Salva
        * --- Read from CFG_GEST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CFG_GEST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CFG_GEST_idx,2],.t.,this.CFG_GEST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CGMODSAV"+;
            " from "+i_cTable+" CFG_GEST where ";
                +"CGNOMGES = "+cp_ToStrODBC(this.w_GSNOME);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_pPARENT.nCfgGest);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CGMODSAV;
            from (i_cTable) where;
                CGNOMGES = this.w_GSNOME;
                and CPROWNUM = this.w_pPARENT.nCfgGest;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CGMODSAV = NVL(cp_ToDate(_read_.CGMODSAV),cp_NullValue(_read_.CGMODSAV))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_OPER="F" Or this.w_CGMODSAV="A" Or this.w_CGMODSAV="D" 
          * --- Salva
          * --- Controllo se devo aggiornare
          * --- Select from PRCFGGES
          i_nConn=i_TableProp[this.PRCFGGES_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2],.t.,this.PRCFGGES_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select PRNOMVAR,PRVALVAR  from "+i_cTable+" PRCFGGES ";
                +" where PRNOMGES="+cp_ToStrODBC(this.w_GSNOME)+" And PRROWNUM="+cp_ToStrODBC(this.w_pPARENT.nCfgGest)+"";
                +" order by CPROWORD";
                 ,"_Curs_PRCFGGES")
          else
            select PRNOMVAR,PRVALVAR from (i_cTable);
             where PRNOMGES=this.w_GSNOME And PRROWNUM=this.w_pPARENT.nCfgGest;
             order by CPROWORD;
              into cursor _Curs_PRCFGGES
          endif
          if used('_Curs_PRCFGGES')
            select _Curs_PRCFGGES
            locate for 1=1
            do while not(eof())
             
 w_NOMEVAR=_Curs_PRCFGGES.PRNOMVAR 
 w_VALORE=_Curs_PRCFGGES.PRVALVAR
            if !(this.w_pPARENT.&w_NOMEVAR==&w_VALORE)
              * --- Appena ne trovo uno diverso salvo
              if this.w_CGMODSAV<>"D" Or this.w_OPER="F"
                GSUT_BCP(this,this.w_pPARENT, .NULL., "S", this.w_GSNOME, this.w_pPARENT.nCfgGest)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                EXIT
              else
                if Ah_YesNo("Salvo la configurazione corrente?")
                  GSUT_BCP(this,this.w_pPARENT, .NULL., "S", this.w_GSNOME, this.w_pPARENT.nCfgGest)
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  EXIT
                else
                  EXIT
                endif
              endif
            endif
              select _Curs_PRCFGGES
              continue
            enddo
            use
          endif
        endif
      case this.w_OPER="A"
        * --- Salva con nome
        do GSUT_KCG with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.w_OPER="L"
        * --- Carica cfg
        * --- Read from CFG_GEST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CFG_GEST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CFG_GEST_idx,2],.t.,this.CFG_GEST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CGMODSAV"+;
            " from "+i_cTable+" CFG_GEST where ";
                +"CGNOMGES = "+cp_ToStrODBC(this.w_GSNOME);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_pPARENT.nCfgGest);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CGMODSAV;
            from (i_cTable) where;
                CGNOMGES = this.w_GSNOME;
                and CPROWNUM = this.w_pPARENT.nCfgGest;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CGMODSAV = NVL(cp_ToDate(_read_.CGMODSAV),cp_NullValue(_read_.CGMODSAV))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_CGMODSAV="A" Or (this.w_CGMODSAV="D" And Ah_YesNo("Salvo la configurazione corrente?"))
          * --- Salva
          GSUT_BCP(this,this.w_pPARENT, .NULL., "S", this.w_GSNOME, this.w_pPARENT.nCfgGest)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Ripristiniamo oParentObject
        if cp_getEntityType(this.w_pParent.cPrg)="Dialog Window" AND TYPE("This.oParentOBJECT.oParentObject")="O"
          oParentObject=This.oParentOBJECT.oParentObject
        endif
        * --- Eseguo la BlankRec per ripulire la maschera prima di valorizzare i campi
        this.w_pPARENT.BlankRec()     
        * --- Eseguo la InitAutonumber per rivalorizzare gli autonumber dopo la blankrec
        if cp_getEntityType(this.w_pParent.cPrg)<>"Dialog Window"
          this.w_pPARENT.InitAutonumber()     
        endif
        if Type( "this.w_pPARENT.cTrsName" )="C" And Used( this.w_pPARENT.cTrsName+"_bis" )
           
 select ( this.w_pPARENT.cTrsName+"_bis" ) 
 zap
          this.w_pPARENT.trsFromWork()     
        endif
        * --- Carica configurazione
        * --- Select from PRCFGGES
        i_nConn=i_TableProp[this.PRCFGGES_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2],.t.,this.PRCFGGES_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PRNOMVAR,PRVALVAR  from "+i_cTable+" PRCFGGES ";
              +" where PRNOMGES="+cp_ToStrODBC(this.w_GSNOME)+" AND PRROWNUM="+cp_ToStrODBC(this.w_ROWNUM)+"";
              +" order by CPROWORD";
               ,"_Curs_PRCFGGES")
        else
          select PRNOMVAR,PRVALVAR from (i_cTable);
           where PRNOMGES=this.w_GSNOME AND PRROWNUM=this.w_ROWNUM;
           order by CPROWORD;
            into cursor _Curs_PRCFGGES
        endif
        if used('_Curs_PRCFGGES')
          select _Curs_PRCFGGES
          locate for 1=1
          do while not(eof())
          * --- Valorizzo parametro maschera
          GSUT_BVP(this,this.w_pPARENT, _Curs_PRCFGGES.PRNOMVAR, _Curs_PRCFGGES.PRVALVAR)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
            select _Curs_PRCFGGES
            continue
          enddo
          use
        endif
        * --- Memorizzo il CPROWNUM della configurazione
        this.w_pPARENT.nCfgGest = this.w_ROWNUM
        * --- Cambio caption della maschera
        * --- Read from CFG_GEST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CFG_GEST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CFG_GEST_idx,2],.t.,this.CFG_GEST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CGDESCFG"+;
            " from "+i_cTable+" CFG_GEST where ";
                +"CGNOMGES = "+cp_ToStrODBC(this.w_GSNOME);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_pPARENT.nCfgGest);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CGDESCFG;
            from (i_cTable) where;
                CGNOMGES = this.w_GSNOME;
                and CPROWNUM = this.w_pPARENT.nCfgGest;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CGDESCFG = NVL(cp_ToDate(_read_.CGDESCFG),cp_NullValue(_read_.CGDESCFG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if RAT(SPACE(5)+"(",this.w_pPARENT.Caption)>0
          this.w_pPARENT.Caption = SUBSTR(this.w_pPARENT.Caption, 1, RAT(SPACE(5)+"(",this.w_pPARENT.Caption)-1) + Space(5) + "("+ALLTRIM(this.w_CGDESCFG)+")"
        else
          this.w_pPARENT.Caption = this.w_pPARENT.Caption + Space(5) + "("+ALLTRIM(this.w_CGDESCFG)+")"
        endif
        this.w_pPARENT.NotifyEvent("CfgGestLoaded")     
      case this.w_OPER="C"
        * --- Controllo esistenza default, lanciato da checkform (GSUT_KCG)
        this.w_NOMEAP = this.oParentObject.w_GSNOME
        * --- Controllo se esiste gi� una cfg di default
        if this.oParentObject.w_DEFCFG="S"
          * --- Read from CFG_GEST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CFG_GEST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CFG_GEST_idx,2],.t.,this.CFG_GEST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" CFG_GEST where ";
                  +"CGNOMGES = "+cp_ToStrODBC(this.w_NOMEAP);
                  +" and CGDEFCFG = "+cp_ToStrODBC("S");
                  +" and CGTIPCFG = "+cp_ToStrODBC(this.oParentObject.w_TIPCFG);
                  +" and CGUTEGRP = "+cp_ToStrODBC(this.oParentObject.w_UTEGRP);
                  +" and CGCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CGNOMGES = this.w_NOMEAP;
                  and CGDEFCFG = "S";
                  and CGTIPCFG = this.oParentObject.w_TIPCFG;
                  and CGUTEGRP = this.oParentObject.w_UTEGRP;
                  and CGCODAZI = this.oParentObject.w_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows>0
            Ah_ErrorMsg("Esiste gi� una configurazione di default", "stop")
            i_retcode = 'stop'
            i_retval = .F.
            return
          endif
        endif
        i_retcode = 'stop'
        i_retval = .T.
        return
      case this.w_OPER="D"
        * --- Controllo cancellazione cfg
        * --- Select from CFG_GEST
        i_nConn=i_TableProp[this.CFG_GEST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CFG_GEST_idx,2],.t.,this.CFG_GEST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CGNOMGES  from "+i_cTable+" CFG_GEST ";
              +" where CGNOMGES="+cp_ToStrODBC(this.w_pPARENT.w_MCNOMGES)+"";
              +" group by CGNOMGES";
               ,"_Curs_CFG_GEST")
        else
          select CGNOMGES from (i_cTable);
           where CGNOMGES=this.w_pPARENT.w_MCNOMGES;
           group by CGNOMGES;
            into cursor _Curs_CFG_GEST
        endif
        if used('_Curs_CFG_GEST')
          select _Curs_CFG_GEST
          locate for 1=1
          do while not(eof())
          i_retcode = 'stop'
          i_retval = .F.
          return
            select _Curs_CFG_GEST
            continue
          enddo
          use
        endif
        i_retcode = 'stop'
        i_retval = .T.
        return
    endcase
  endproc


  proc Init(oParentObject,w_OPER,w_ROWNUM)
    this.w_OPER=w_OPER
    this.w_ROWNUM=w_ROWNUM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MODDCONF'
    this.cWorkTables[2]='PRCFGGES'
    this.cWorkTables[3]='CFG_GEST'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_PRCFGGES')
      use in _Curs_PRCFGGES
    endif
    if used('_Curs_PRCFGGES')
      use in _Curs_PRCFGGES
    endif
    if used('_Curs_CFG_GEST')
      use in _Curs_CFG_GEST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPER,w_ROWNUM"
endproc
