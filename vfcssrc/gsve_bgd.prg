* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bgd                                                        *
*              Generazione ENASARCO su file                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_124]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-25                                                      *
* Last revis.: 2005-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bgd",oParentObject)
return(i_retval)

define class tgsve_bgd as StdBatch
  * --- Local variables
  w_VALCH = space(13)
  w_Separa = space(2)
  w_Decimali = 0
  w_Handle = 0
  w_Record = space(120)
  w_NumDis = space(7)
  w_Lunghezza = 0
  w_StrInt = space(1)
  w_StrDec = space(1)
  w_ANNO = space(2)
  w_POSENA = space(8)
  w_NUMTR1 = space(4)
  w_NUMTR2 = space(4)
  w_NUMTR3 = space(4)
  w_NUMTR4 = space(4)
  w_NUMTR5 = space(4)
  w_GENFIR = space(1)
  w_TRIMESTRE = space(1)
  w_ANFIRR = space(4)
  w_ANNOES = space(4)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_CONTA = 0
  w_CODAGE = space(5)
  w_FISAGE = space(16)
  w_DATMAND = ctod("  /  /  ")
  w_CONPRE = 0
  w_CONASS = 0
  w_VALORE = 0
  w_TOTPRE = space(13)
  w_TOTFIRR = space(13)
  w_SUMPRE = 0
  w_SUMASS = 0
  w_SUMFIRR = 0
  w_PREVIDENZA = space(13)
  w_ASSISTENZA = space(13)
  w_FIRR = space(13)
  w_DATVER = ctod("  /  /  ")
  w_DATFIR = ctod("  /  /  ")
  w_NUMFILE = space(4)
  w_DATA1 = space(8)
  w_DATA2 = space(8)
  w_MANDATO = space(6)
  w_DATAVERS = space(8)
  w_DATAFIRR = space(8)
  w_CODENA = space(15)
  w_MESS = space(200)
  w_RENAME = space(40)
  w_NOERR = .f.
  w_ANNOFIRR = space(4)
  w_ANNOPREV = space(4)
  w_SCRIVETRI = space(1)
  w_oERRORLOG = .NULL.
  w_AGENTE = space(5)
  * --- WorkFile variables
  PAR_PROV_idx=0
  AZIENDA_idx=0
  AGENTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Distinta: versamento su file
    this.w_Separa = iif(this.oParentObject.w_SEPREC="S", chr(13)+chr(10), chr(13))
    this.w_Decimali = 2
    this.w_Handle = 0
    this.w_Record = space(120)
    this.w_NumDis = "0000000"
    this.w_Lunghezza = 0
    this.w_StrInt = space(1)
    this.w_StrDec = space(1)
    this.w_ANNO = SPACE(2)
    this.w_ANNOES = SPACE(4)
    this.w_DATA1 = dtoc(CP_TODATE(this.oParentObject.w_DATA))
    this.w_DATA2 = dtoc(CP_TODATE(this.oParentObject.w_DATFIN))
    this.w_MESS = SPACE(200)
    this.w_NOERR = .T.
    * --- Se il tipo di distinta � gi� Eseguita, rinomino il file prima di rigenerarlo.
    if this.oParentObject.w_TIPGEN="E"
      if file(ALLTRIM(this.oParentObject.w_DIRGEN))
        this.w_RENAME = this.oParentObject.w_DIRGEN+".BAK"
        ah_ErrorMsg("Il file � gi� presente e verr� rinominato in: %1",,"", this.w_RENAME)
        if File(this.w_RENAME)
          * --- sposto .bak  direttamente nel cestino
          DELETE FILE( this.w_RENAME ) RECYCLE
        endif
        if File(this.w_RENAME)
          * --- .bak gi� presente nel cestino eliminato direttamente
          DELETE FILE( this.w_RENAME )
        endif
        RENAME(ALLTRIM(this.oParentObject.w_DIRGEN)+".") TO ( this.w_RENAME )
      endif
    endif
    * --- Controllo che il patch sia valido
    if EMPTY(this.oParentObject.w_DIRGEN)
      ah_ErrorMsg("Inserire un path valido")
      i_retcode = 'stop'
      return
    endif
    * --- Read from PAR_PROV
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_PROV_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PPPOSENA,PPNUMTR1,PPNUMTR2,PPNUMTR3,PPNUMTR4,PPNUMTR5,PPGENFIR,PPTRIPRE,PPANPREV,PPANFIRR"+;
        " from "+i_cTable+" PAR_PROV where ";
            +"PPCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PPPOSENA,PPNUMTR1,PPNUMTR2,PPNUMTR3,PPNUMTR4,PPNUMTR5,PPGENFIR,PPTRIPRE,PPANPREV,PPANFIRR;
        from (i_cTable) where;
            PPCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_POSENA = NVL(cp_ToDate(_read_.PPPOSENA),cp_NullValue(_read_.PPPOSENA))
      this.w_NUMTR1 = NVL(cp_ToDate(_read_.PPNUMTR1),cp_NullValue(_read_.PPNUMTR1))
      this.w_NUMTR2 = NVL(cp_ToDate(_read_.PPNUMTR2),cp_NullValue(_read_.PPNUMTR2))
      this.w_NUMTR3 = NVL(cp_ToDate(_read_.PPNUMTR3),cp_NullValue(_read_.PPNUMTR3))
      this.w_NUMTR4 = NVL(cp_ToDate(_read_.PPNUMTR4),cp_NullValue(_read_.PPNUMTR4))
      this.w_NUMTR5 = NVL(cp_ToDate(_read_.PPNUMTR5),cp_NullValue(_read_.PPNUMTR5))
      this.w_GENFIR = NVL(cp_ToDate(_read_.PPGENFIR),cp_NullValue(_read_.PPGENFIR))
      this.w_TRIMESTRE = NVL(cp_ToDate(_read_.PPTRIPRE),cp_NullValue(_read_.PPTRIPRE))
      this.w_ANNOPREV = NVL(cp_ToDate(_read_.PPANPREV),cp_NullValue(_read_.PPANPREV))
      this.w_ANNOFIRR = NVL(cp_ToDate(_read_.PPANFIRR),cp_NullValue(_read_.PPANFIRR))
      this.w_SCRIVETRI = NVL(cp_ToDate(_read_.PPTRIPRE),cp_NullValue(_read_.PPTRIPRE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCOFAZI,AZPIVAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCOFAZI,AZPIVAZI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_Handle = fcreate(ALLTRIM(this.oParentObject.w_DIRGEN))
    if this.w_Handle = -1
      ah_ErrorMsg("Non � possibile creare il file versamenti ENASARCO")
      fclose(this.w_Handle) 
      i_retcode = 'stop'
      return
    endif
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Creazione cursore ENASARCO
    VQ_EXEC("QUERY\GSVE3KGD.VQR",this,"ENASARCO")
    if reccount("ENASARCO") > 0
       
 Select ENASARCO 
 Go Top
      * --- Record di Testata - Tipo 1
      this.w_ANNO = right(STR(YEAR(VTDATFIN)),2)
      this.w_ANNOES = right(STR(YEAR(VTDATFIN)),4)
      * --- Controllo su codice Posizione Enasarco Azienda
      if empty(NVL(this.w_POSENA,""))
        this.w_oERRORLOG.AddMsgLog("Codice posizione ENASARCO azienda non inserito")     
        this.w_NOERR = .F.
      endif
      * --- Controllo sul nome del file
      this.w_NUMFILE = RIGHT(this.oParentObject.w_NOMEFILE,4)
      if empty(NVL(this.w_NUMFILE,""))
        this.w_oERRORLOG.AddMsgLog("Numero ENASARCO non inserito")     
        this.w_NOERR = .F.
      endif
      this.w_POSENA = right("00000000" + alltrim(NVL(this.w_POSENA,SPACE(8))), 8)
      * --- Controllo su codice Codice Fiscale Azienda
      if empty(NVL(this.w_COFAZI,""))
        this.w_oERRORLOG.AddMsgLog("Codice fiscale azienda non inserito")     
        this.w_NOERR = .F.
      endif
      this.w_DATA1 = left(this.w_DATA1,2) + substr(this.w_DATA1,4,2) + right(this.w_DATA1,4)
      this.w_DATA2 = left(this.w_DATA2,2) + substr(this.w_DATA2,4,2) + right(this.w_DATA2,4)
      this.w_Record = "10" + this.w_ANNO + this.oParentObject.w_TRIRIF + this.w_POSENA + "00000000" + "0" + "1"+ "0000" + this.w_ANNOES+"1"+ this.oParentObject.w_TRIRIF+this.w_NUMFILE +this.w_POSENA+"0"+ this.w_COFAZI+this.w_DATA1+this.w_DATA2+"00"+SPACE(19)+"E"
      if this.w_NOERR
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Ciclo sui versamenti
      ah_Msg("Scrittura file ENASARCO...")
      scan
      * --- Record Righe Agenti - Tipo 2
      this.w_CONTA = RECNO()
      this.w_CONTA = right("0000" + alltrim(STR(this.w_CONTA)), 4)
      this.w_AGENTE = NVL(VTCODAGE,SPACE(5))
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGFISAGE,AGFINENA,AGCODENA"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_AGENTE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGFISAGE,AGFINENA,AGCODENA;
          from (i_cTable) where;
              AGCODAGE = this.w_AGENTE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FISAGE = NVL(cp_ToDate(_read_.AGFISAGE),cp_NullValue(_read_.AGFISAGE))
        this.w_DATMAND = NVL(cp_ToDate(_read_.AGFINENA),cp_NullValue(_read_.AGFINENA))
        this.w_CODENA = NVL(cp_ToDate(_read_.AGCODENA),cp_NullValue(_read_.AGCODENA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if empty(NVL(this.w_CODENA,""))
        this.w_oERRORLOG.AddMsgLog("Codice ENASARCO dell'agente %1 non inserito", alltrim(this.w_AGENTE))     
        this.w_NOERR = .F.
      endif
      if empty(NVL(this.w_FISAGE,""))
        this.w_oERRORLOG.AddMsgLog("Codice fiscale dell'agente %1 non inserito", alltrim(this.w_AGENTE))     
        this.w_NOERR = .F.
      endif
      * --- Contributo Previdenza
      this.w_MANDATO = dtoc(CP_TODATE(this.w_DATMAND))
      this.w_MANDATO = IIF(EMPTY(CP_TODATE(this.w_DATMAND)),"000000", left(this.w_MANDATO,2) + substr(this.w_MANDATO,4,2) + right(this.w_MANDATO,2))
      this.w_CODENA = right("00000000" + alltrim(NVL(this.w_CODENA,SPACE(8))), 8)
      this.w_VALORE = NVL(VTCONPRE,0)
      this.w_Lunghezza = 10
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_TOTPRE = this.w_VALCH
      * --- Contributo Assistenza
      this.w_VALORE = NVL(VTCONASS,0)
      this.w_Lunghezza = 10
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_TOTPRE = ALLTRIM(this.w_TOTPRE)+this.w_VALCH
      * --- Contributo FIRR
      this.w_VALORE = VFCONFI1+VFCONFI2+VFCONFI3
      this.w_Lunghezza = 10
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_TOTFIRR = this.w_VALCH
      this.w_Record = "10" + this.w_ANNO + this.oParentObject.w_TRIRIF + this.w_POSENA + "00000000" + "0" + "2"+this.w_CONTA + this.w_ANNOES+"1"+ this.oParentObject.w_TRIRIF+this.w_NUMFILE +this.w_CODENA+"0"+ this.w_FISAGE+this.w_MANDATO+this.w_TOTPRE+ this.w_TOTFIRR+SPACE(2)
      if this.w_NOERR
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_SUMPRE = this.w_SUMPRE+ NVL(VTCONPRE,0)
      this.w_SUMASS = this.w_SUMASS+ NVL(VTCONASS,0)
      this.w_SUMFIRR = this.w_SUMFIRR+ (VFCONFI1+VFCONFI2+VFCONFI3)
      this.w_DATAVERS = IIF(NOT EMPTY(CP_TODATE(VTDATVER)), LEFT(dtoc(CP_TODATE(VTDATVER)),2) + substr(dtoc(CP_TODATE(VTDATVER)),4,2) + RIGHT(dtoc(CP_TODATE(VTDATVER)),4), "00000000")
      this.w_DATAFIRR = IIF(NOT EMPTY(CP_TODATE(VFDATVER)), LEFT(dtoc(CP_TODATE(VFDATVER)),2) + substr(dtoc(CP_TODATE(VFDATVER)),4,2) + RIGHT(dtoc(CP_TODATE(VFDATVER)),4), "00000000")
      endscan
      * --- Record Totali - Tipo 3
      * --- Totale Previdenza
      this.w_VALORE = this.w_SUMPRE
      this.w_Lunghezza = 12
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PREVIDENZA = this.w_VALCH
      * --- Totale Assistenza
      this.w_VALORE = this.w_SUMASS
      this.w_Lunghezza = 12
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_ASSISTENZA = this.w_VALCH
      * --- Totale FIRR
      this.w_VALORE = this.w_SUMFIRR
      this.w_Lunghezza = 12
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_FIRR = this.w_VALCH
      this.w_Record = "10" + this.w_ANNO + this.oParentObject.w_TRIRIF + this.w_POSENA + "00000000" + "0" + "9"+ "9999" + this.w_ANNOES+"1"+ this.oParentObject.w_TRIRIF+this.w_NUMFILE +this.w_PREVIDENZA+ this.w_ASSISTENZA+ this.w_FIRR+ this.w_DATAVERS+this.w_DATAFIRR+SPACE(2)+"00"+SPACE(7)
      if this.w_NOERR
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_NOERR
        if fclose(this.w_Handle) 
          ah_ErrorMsg("Generazione file ENASARCO terminata")
        endif
        do case
          case this.oParentObject.w_TIPDIS="F" AND this.w_GENFIR="S"
            this.w_ANNOFIRR = this.oParentObject.w_ANPREV
          case this.oParentObject.w_TIPDIS="P" AND this.w_GENFIR="S"
            this.w_ANNOPREV = this.oParentObject.w_ANPREV
            this.w_SCRIVETRI = this.oParentObject.w_TRIRIF
          otherwise
            this.w_ANNOFIRR = this.oParentObject.w_ANPREV
            this.w_ANNOPREV = this.oParentObject.w_ANPREV
            this.w_SCRIVETRI = this.oParentObject.w_TRIRIF
        endcase
        if this.oParentObject.w_TIPGEN="D"
          * --- Write into PAR_PROV
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_PROV_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_PROV_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PPANPREV ="+cp_NullLink(cp_ToStrODBC(this.w_ANNOPREV),'PAR_PROV','PPANPREV');
            +",PPTRIPRE ="+cp_NullLink(cp_ToStrODBC(this.w_SCRIVETRI),'PAR_PROV','PPTRIPRE');
            +",PPANFIRR ="+cp_NullLink(cp_ToStrODBC(this.w_ANNOFIRR),'PAR_PROV','PPANFIRR');
            +",PPDIRGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DIR),'PAR_PROV','PPDIRGEN');
                +i_ccchkf ;
            +" where ";
                +"PPCODAZI = "+cp_ToStrODBC(I_CODAZI);
                   )
          else
            update (i_cTable) set;
                PPANPREV = this.w_ANNOPREV;
                ,PPTRIPRE = this.w_SCRIVETRI;
                ,PPANFIRR = this.w_ANNOFIRR;
                ,PPDIRGEN = this.oParentObject.w_DIR;
                &i_ccchkf. ;
             where;
                PPCODAZI = I_CODAZI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      else
        fclose(this.w_Handle) 
        NAMEFILE=this.oParentObject.w_DIRGEN
        DELETE FILE (NAMEFILE)
      endif
      if ! this.w_NOERR
        ah_ErrorMsg("Dati mancanti: impossibile generare il file")
        this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
      endif
    else
      ah_ErrorMsg("Per la selezione effettuata non esistono versamenti da elaborare")
      fclose(this.w_Handle) 
    endif
    if used( "ENASARCO" )
       
 select ENASARCO 
 use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura record su file ascii
    * --- Scrittura record su File R.I.D.
    w_t = fwrite(this.w_Handle, this.w_Record+this.w_Separa)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Trasformazione degli importi nel formato richiesto dal tracciato R.I.D.
    * --- UTILIZZO
    * --- Prima della chiamata a questa pagina del batch devono essere impostate le variabili:
    * --- w_Valore          = importo da convertire in stringa
    * --- w_Lunghezza  = lunghezza della stringa
    * --- Il risultato della conversione � disponibile nella variabile w_Valore.
    * --- CONVENZIONI
    * --- La virgola deve essere eliminata
    * --- Se la valuta � Euro le ultime due cifre rappresentano i decimali
    * --- Il valore deve essere allineato a destra
    * --- E' richiesto il riempimento con 0 delle cifre non significative
    * --- Calcolo parte intera
    this.w_StrInt = alltrim( str( int(this.w_Valore), this.w_Lunghezza, 0 ) )
    * --- Calcolo parte decimale
    this.w_StrDec = ""
    if this.w_Decimali > 0
      this.w_StrDec = right( str( this.w_Valore, this.w_Lunghezza, this.w_Decimali ) , this.w_Decimali )
    endif
    * --- Risultato
    this.w_VALCH = right( repl("0",this.w_Lunghezza) + this.w_StrInt + this.w_StrDec , this.w_Lunghezza )
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PAR_PROV'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='AGENTI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
