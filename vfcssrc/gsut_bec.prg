* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bec                                                        *
*              Cruscotto elaborazioni KPI                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-07-09                                                      *
* Last revis.: 2016-04-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bec",oParentObject,m.pOper)
return(i_retval)

define class tgsut_bec as StdBatch
  * --- Local variables
  pOper = space(4)
  w_CODELA = space(10)
  w_CHKEND = space(1)
  w_ATTIVA = space(1)
  w_PADRE = .NULL.
  w_NC = space(10)
  w_LOGSERIAL = space(10)
  w_GESTFAT = .NULL.
  w_NELAB = 0
  w_RESXML = space(0)
  w_CODAZI = space(5)
  w_YESNOMSG = space(50)
  w_ESITO = 0
  w_CODUTE = 0
  w_MINUTI = 0
  * --- WorkFile variables
  ELABKPIM_idx=0
  RESELKPI_idx=0
  ELABKPID_idx=0
  LOGELKPI_idx=0
  TMRELKPI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Funzionalit� per la gestione del cruscotto elaborazioni kpi
    this.pOper = Upper(this.pOper)
    this.w_CODAZI = i_CODAZI
    this.w_PADRE = This.oParentObject
    if Upper(this.w_PADRE.Class)=="TGSUT_KEC"
      this.w_NC = this.w_PADRE.w_ZoomKPI.cCursor
    endif
    do case
      case this.pOper=="EXEC" OR this.pOper=="BGEX" OR this.pOper=="DEL" OR this.pOper=="CLRL"
        this.w_NELAB = -1
        Local l_count 
 Select(this.w_NC) 
 Count For XCHK=1 To m.l_count 
 
        this.w_YESNOMSG = iCase(this.pOper="EXEC", "Eseguire", this.pOper="BGEX", "Eseguire in background", this.pOper="DEL", "Cancellare", this.pOper="CLRL", "Eseguire pulizia risultati e log per")
        if l_count>0 AND Ah_YesNo("%1 %2 elaborazioni?","?", this.w_YESNOMSG, Trans(l_count))
          this.w_NELAB = 0
          Select(this.w_NC) 
 Go Top 
 Scan for XCHK=1
          this.w_CODELA = Alltrim(EKCODICE)
          this.w_ATTIVA = Nvl(EKCHKATT,"N")
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          Endscan
        else
          * --- Se l_count=0 si prende in considerazione l'elaborazione corrente
          if l_count=0
            this.w_CODELA = Alltrim(Nvl(this.oParentObject.w_CODICE,""))
            this.w_ATTIVA = Nvl(this.oParentObject.w_ATTIVAZ,"N")
            if ! Empty(this.w_CODELA)
              if Ah_YesNo("%1 l'elaborazione: %2 ?","?", this.w_YESNOMSG, this.w_CODELA)
                this.w_NELAB = 0
                this.Pag6()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            else
              Ah_ErrorMsg("Nessuna elaborazione selezionata","!")
            endif
          endif
        endif
        this.w_YESNOMSG = iCase(this.pOper="EXEC", "eseguite", this.pOper="DEL", "cancellate", this.pOper="CLRL", "resettate")
        if this.pOper<>"BGEX" And this.w_NELAB>=0
          Ah_ErrorMsg("%1 elaborazioni %2","i","",Trans(this.w_NELAB), this.w_YESNOMSG)
          this.w_PADRE.NotifyEvent("AggiornaCruscotto")     
          Select(this.w_NC) 
 Locate For Alltrim(EKCODICE)=this.w_CODELA 
 this.w_PADRE.w_ZoomKPI.Refresh()
        endif
      case this.pOper=="ACTV" OR this.pOper=="DEAC"
        this.w_NELAB = 0
        Select(this.w_NC) 
 Scan for XCHK=1
        this.w_CODELA = Alltrim(EKCODICE)
        * --- Write into ELABKPIM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ELABKPIM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ELABKPIM_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ELABKPIM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"EKCHKATT ="+cp_NullLink(cp_ToStrODBC(iif(this.pOper=="ACTV","S","N")),'ELABKPIM','EKCHKATT');
              +i_ccchkf ;
          +" where ";
              +"EKCODICE = "+cp_ToStrODBC(this.w_CODELA);
                 )
        else
          update (i_cTable) set;
              EKCHKATT = iif(this.pOper=="ACTV","S","N");
              &i_ccchkf. ;
           where;
              EKCODICE = this.w_CODELA;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_NELAB = this.w_NELAB+1
        Endscan
        if this.w_NELAB=0
          * --- Se non ci sono elaborazioni selezionate controllo se ce n'� una con l'highlight
          if ! Empty(Nvl(this.oParentObject.w_CODICE,""))
            * --- Write into ELABKPIM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ELABKPIM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ELABKPIM_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ELABKPIM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"EKCHKATT ="+cp_NullLink(cp_ToStrODBC(iif(this.pOper=="ACTV","S","N")),'ELABKPIM','EKCHKATT');
                  +i_ccchkf ;
              +" where ";
                  +"EKCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODICE);
                     )
            else
              update (i_cTable) set;
                  EKCHKATT = iif(this.pOper=="ACTV","S","N");
                  &i_ccchkf. ;
               where;
                  EKCODICE = this.oParentObject.w_CODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_NELAB = 1
          else
            Ah_ErrorMsg("Nessuna elaborazione selezionata","!")
            i_retcode = 'stop'
            return
          endif
        endif
        Ah_ErrorMsg("%1 elaborazioni %2","i","",Trans(this.w_NELAB),iif(this.pOper=="ACTV","attivate!","disattivate!"))
        this.w_PADRE.NotifyEvent("AggiornaCruscotto")     
      case this.pOper=="RESU"
        this.w_CHKEND = " "
        this.w_RESXML = .Null.
        this.w_CODELA = this.oParentObject.w_CODICE
        if !Empty(Nvl(this.w_CODELA,""))
          this.oParentObject.w_RCODKPI = this.oParentObject.w_CODICE
          if Used(this.oParentObject.w_RESCUR)
            Use In Select(this.oParentObject.w_RESCUR)
          endif
          if get_result_kpi(This, this.w_CODELA, "", "w_RDATELA", "w_RRESSIN", this.oParentObject.w_RESCUR, "w_RLOGMSG")
            this.w_PADRE.NotifyEvent("MostraRisultati")     
            this.w_PADRE.oPgFrm.ActivePage = 2
          else
            this.oParentObject.w_RCODKPI = ""
            this.oParentObject.w_RDATELA = cp_chartodate("")
            this.oParentObject.w_RRESSIN = 0
            this.oParentObject.w_RLOGMSG = ""
            Ah_ErrorMsg("Nessun risultato disponibile!","!")
            this.w_PADRE.oPgFrm.ActivePage = 1
          endif
        else
          Ah_ErrorMsg("Nessuna elaborazione selezionata","!")
          this.w_PADRE.oPgFrm.ActivePage = 1
        endif
      case this.pOper=="ATMR"
        this.w_CODUTE = i_CODUTE
        if Vartype(g_oTimerKPI)=="U"
          Public g_oTimerKPI
        endif
        * --- Leggo le impostazioni del Timer per l'utente
        this.w_MINUTI = 0
        if Lower(this.w_PADRE.Class)=="tgsut_ktk"
          this.w_MINUTI = this.w_PADRE.GSUT_ATK.w_TKMINUTI
        else
          * --- Read from TMRELKPI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TMRELKPI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMRELKPI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TKMINUTI"+;
              " from "+i_cTable+" TMRELKPI where ";
                  +"TKCODUTE = "+cp_ToStrODBC(this.w_CODUTE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TKMINUTI;
              from (i_cTable) where;
                  TKCODUTE = this.w_CODUTE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MINUTI = NVL(cp_ToDate(_read_.TKMINUTI),cp_NullValue(_read_.TKMINUTI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Se necessario creo l'oggetto timer, altrimenti mi limito a riattivarlo
        if Vartype(g_oTimerKPI)<>"O"
          g_oTimerKPI = CreateObject("KPITimer")
        endif
        g_oTimerKPI.Interval = this.w_MINUTI 
 g_oTimerKPI.Enabled=.t.
      case this.pOper=="DTMR"
        * --- Disattivo il timer se necessario
        if Vartype(g_oTimerKPI)="O"
          g_oTimerKPI.Enabled = .F.
        endif
      case this.pOper="ITMR"
        if Vartype(i_DDEchannel)="N" And i_DDEchannel<>-1
          * --- Leggo le impostazioni per l'utente
          this.w_CODUTE = i_CODUTE
          this.w_ATTIVA = "N"
          * --- Read from TMRELKPI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TMRELKPI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMRELKPI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TK_AVVIO,TKMINUTI"+;
              " from "+i_cTable+" TMRELKPI where ";
                  +"TKCODUTE = "+cp_ToStrODBC(this.w_CODUTE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TK_AVVIO,TKMINUTI;
              from (i_cTable) where;
                  TKCODUTE = this.w_CODUTE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ATTIVA = NVL(cp_ToDate(_read_.TK_AVVIO),cp_NullValue(_read_.TK_AVVIO))
            this.w_MINUTI = NVL(cp_ToDate(_read_.TKMINUTI),cp_NullValue(_read_.TKMINUTI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Upper(this.w_ATTIVA)="S"
            * --- Dichiarazione variabili
            if Vartype(g_oTimerKPI)=="U"
              Public g_oTimerKPI
            endif
            * --- Se necessario creo l'oggetto timer, altrimenti mi limito a riattivarlo
            if Vartype(g_oTimerKPI)<>"O"
              g_oTimerKPI = CreateObject("KPITimer")
            endif
            g_oTimerKPI.Interval = this.w_MINUTI 
 g_oTimerKPI.Enabled=.t.
          else
            * --- Disattivo il timer se necessario
            if Vartype(g_oTimerKPI)="O"
              g_oTimerKPI.Enabled = .F.
            endif
          endif
        else
          * --- Disattivo il timer se necessario
          if Vartype(g_oTimerKPI)="O"
            g_oTimerKPI.Enabled = .F.
          endif
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    private w_CODKPI
    m.w_CODKPI = space(10)
    * --- Tento la cancellazione:
    *     
    *     risultati
    *     log
    *     dettaglio
    *     testata
    * --- Try
    local bErr_0486E8B8
    bErr_0486E8B8=bTrsErr
    this.Try_0486E8B8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      Ah_ErrorMsg("Errore tentando di cancellare l'elaborazione %1:%2%3","!","",this.w_CODELA, CHR(10)+CHR(13), Message())
    endif
    bTrsErr=bTrsErr or bErr_0486E8B8
    * --- End
  endproc
  proc Try_0486E8B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Cancello i risultati
    * --- Delete from RESELKPI
    i_nConn=i_TableProp[this.RESELKPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RESELKPI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RKCODKPI = "+cp_ToStrODBC(this.w_CODELA);
             )
    else
      delete from (i_cTable) where;
            RKCODKPI = this.w_CODELA;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Cancello le registrazioni di log
    * --- Setto a "Nessuno" la tipologia di benchmark dell'eleaborazioni che linkavano a quella che sto cancellando
    * --- Select from ELABKPIM
    i_nConn=i_TableProp[this.ELABKPIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELABKPIM_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select EKCODICE  from "+i_cTable+" ELABKPIM ";
          +" where EKBENCOD="+cp_ToStrODBC(this.w_CODELA)+"";
           ,"_Curs_ELABKPIM")
    else
      select EKCODICE from (i_cTable);
       where EKBENCOD=this.w_CODELA;
        into cursor _Curs_ELABKPIM
    endif
    if used('_Curs_ELABKPIM')
      select _Curs_ELABKPIM
      locate for 1=1
      do while not(eof())
      m.w_CODKPI = Alltrim(_Curs_ELABKPIM.EKCODICE)
      * --- Write into ELABKPIM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ELABKPIM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ELABKPIM_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ELABKPIM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"EKBENCHM ="+cp_NullLink(cp_ToStrODBC("NUL"),'ELABKPIM','EKBENCHM');
        +",EKBENCOD ="+cp_NullLink(cp_ToStrODBC(""),'ELABKPIM','EKBENCOD');
            +i_ccchkf ;
        +" where ";
            +"EKCODICE = "+cp_ToStrODBC(m.w_CODKPI);
               )
      else
        update (i_cTable) set;
            EKBENCHM = "NUL";
            ,EKBENCOD = "";
            &i_ccchkf. ;
         where;
            EKCODICE = m.w_CODKPI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_ELABKPIM
        continue
      enddo
      use
    endif
    * --- Delete from ELABKPID
    i_nConn=i_TableProp[this.ELABKPID_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELABKPID_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"EKCODICE = "+cp_ToStrODBC(this.w_CODELA);
             )
    else
      delete from (i_cTable) where;
            EKCODICE = this.w_CODELA;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ELABKPIM
    i_nConn=i_TableProp[this.ELABKPIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELABKPIM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"EKCODICE = "+cp_ToStrODBC(this.w_CODELA);
             )
    else
      delete from (i_cTable) where;
            EKCODICE = this.w_CODELA;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_NELAB = this.w_NELAB+1
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue singola elaborazione
    this.w_ESITO = GSUT_BEE(This, this.w_CODELA)
    if this.w_ESITO<>-1
      this.w_NELAB = this.w_NELAB + this.w_ESITO
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Pulizia completa tabelle risultati e log
    * --- Delete from RESELKPI
    i_nConn=i_TableProp[this.RESELKPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RESELKPI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RKCODKPI = "+cp_ToStrODBC(this.w_CODELA);
             )
    else
      delete from (i_cTable) where;
            RKCODKPI = this.w_CODELA;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from LOGELKPI
    i_nConn=i_TableProp[this.LOGELKPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGELKPI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"LKCODKPI = "+cp_ToStrODBC(this.w_CODELA);
            +" and LKCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      delete from (i_cTable) where;
            LKCODKPI = this.w_CODELA;
            and LKCODAZI = this.w_CODAZI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_NELAB = this.w_NELAB+1
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia l'esecuzione in BackGround del batch di esecuzione KPI passandogli il codice
    ah_taskexec("GSUT_BEE", 2, .null., this.w_CODELA)
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scelta operazione da eseguira su elaborazione corrente:
    *     
    *     w_CODELA : codice elaborazione
    *     w_ATTIVA : check attivazione
    do case
      case this.pOper=="EXEC"
        if Nvl(this.w_ATTIVA,"N")=="S"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOper=="BGEX"
        if Nvl(this.w_ATTIVA,"N")=="S"
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOper=="DEL"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper=="CLRL"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ELABKPIM'
    this.cWorkTables[2]='RESELKPI'
    this.cWorkTables[3]='ELABKPID'
    this.cWorkTables[4]='LOGELKPI'
    this.cWorkTables[5]='TMRELKPI'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_ELABKPIM')
      use in _Curs_ELABKPIM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
