* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_brr                                                        *
*              Controllo report                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_2]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-05                                                      *
* Last revis.: 2016-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,bTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_brr",oParentObject,m.bTipo)
return(i_retval)

define class tgsut_brr as StdBatch
  * --- Local variables
  bTipo = .f.
  w_FILESEARCH = space(10)
  w_i = 0
  w_FULLPATH = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se bTIpo=.t. compila i report solo testo altrimenti esegue operazioni di pulizia
    *     sui report
    * --- Pulizia report
    * --- Check pulizia font
    * --- Check pulizia printer enviroment
    * --- Check settaggio trim mode
    * --- Percorso da controllare
    * --- Combo settaggio colore
    if this.bTipo
      if Cp_FileExist( this.oParentObject.w_PATHCMP )
        L_ExeComp="Comp "+this.oParentObject.w_PATHCMP 
 &L_ExeComp
        AH_ErrorMsg("Compilazione terminata", "i")
      else
        AH_ErrorMsg(MSG_FILE__NOT_FOUND_QM, "i" , , this.oParentObject.w_PATHCMP )
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Ricerco in modo ricorsivo tutti i report
    this.w_FILESEARCH = "*.FRX"
    * --- Creo un array con la funzione ricorsiva visitaDir
     
 DIMENSION AFNAME(1) 
 visitaDir(this.oParentObject.w_PATH,this.w_FILESEARCH,@AFNAME,.T.)
    if TYPE("AFNAME[1,1]")="C"
      this.w_i = 1
      do while this.w_i<=ALEN(AFNAME,1)
        * --- Se nella colonna 5 dell'array � presente una 'D' significa che sto trattando una directory...
        if AT("D",ALLTRIM(AFNAME[this.w_i,5]))=0
          this.w_FULLPATH = ALLTRIM(UPPER(AFNAME[this.w_i,1]))
          ah_Msg("Esame %1 in corso...",.T.,.F.,.F., DISPLAYPATH(this.w_FULLPATH,235) )
          if FILE(this.w_FULLPATH)
             
 nOldArea = SELECT(0) 
 SELECT 0
            * --- Apro il report
             
 L_errsav=on("ERROR") 
 Messaggio="" 
 ON ERROR Messaggio=Message()
            USE (this.w_FULLPATH) EXCLUSIVE
            on error &L_errsav
            if Not Empty(Messaggio)
              AH_ErrorMsg("Si � verificato un errore durante l'apertura del report: %1%0La procedura sar� terminata", "stop",, this.w_FULLPATH)
              i_retcode = 'stop'
              return
            endif
            * --- Pulizia report - Print enviroment
            if this.oParentObject.w_CHKPRN
              GO TOP
              LOCATE FOR Objtype=1 AND Objcode=53
              if FOUND()
                * --- Legge impostazioni correnti
                 
 w_SetOrientation=substr(mline(expr,atcline("ORIENTATION=",expr)),len("ORIENTATION=")+1) 
 w_SetPaperSize=substr(mline(expr,atcline("PAPERSIZE=",expr)),len("PAPERSIZE=")+1) 
 w_SetColor=substr(mline(expr,atcline("COLOR=",expr)),len("COLOR=")+1)
                * --- Prepara nuove impostazioni
                 
 w_Set = "" 
 w_Set = w_Set+ "ORIENTATION="+iif(empty(w_SetOrientation),"0",w_SetOrientation)+CHR(13)+CHR(10) 
 w_Set = w_Set+ "PAPERSIZE="+iif(empty(w_SetPaperSize),"9",w_SetPaperSize)+CHR(13)+CHR(10) 
 w_Set = w_Set+ "DEFAULTSOURCE=15"+CHR(13)+CHR(10) 
 w_Set = w_Set+ "YRESOLUTION=0" 
 w_Set = w_Set+ IIF( this.oParentObject.w_CMBCOLOR=0, IIF(Empty(w_SetColor), "", CHR(13)+CHR(10)+"COLOR="+w_SetColor), CHR(13)+CHR(10)+ "COLOR="+Alltrim(Str(this.oParentObject.w_CMBCOLOR)))
                * --- Nuove Impostazioni
                REPLACE EXPR WITH w_Set
                * --- Clear TAG e TAG2
                 
 REPLACE TAG WITH "" 
 REPLACE TAG2 WITH ""
              endif
            endif
            * --- Pulizia report - Font
            if this.oParentObject.w_CHKFONT
              GO TOP
              SCAN FOR NOT EMPTY(FontFace)
              w_Appo = UPPER(ALLTRIM(FontFace))
              if "ARIAL (W1)" $ w_Appo
                REPLACE FontFace WITH "Arial"
              endif
              ENDSCAN
            endif
            * --- Pulizia report - Trim mode
            if this.oParentObject.w_CHKTRIM
              GO TOP
              SCAN FOR ObjType=8
              REPLACE Rulerlines WITH 2
              ENDSCAN
            endif
            * --- Problema non allineamento oggetti con valori ripetuti stampati
            *     ( TAB Print When, Print repeated values a Yes )
            *     che hanno almeno uno dei 2 check
            *     a)  In first whole band of a new page...Whe
            *     b) When first data group expression change
            *     spenti, se print repeated value  a .t. vanno entrambi accesi.
            if this.oParentObject.w_CHKREPVAL
              REPLACE SUPRPCOL WITH 3, SUPGROUP with 0 For SUPALWAYS AND ( SUPGROUP<5 OR SUPRPCOL<>3)
              if _tally>0
                * --- Devo modificare anche il file FRT (x non scompagnare le versioni su Project) per cui vado a modificare, se modificato almeno
                *     un record anche il primo campo del report nelle informazioni da passare al driver 
                *     di stampa
                REPLACE EXPR with EXPR+" " RECORD 1
              endif
            endif
            USE
            SELECT (nOldArea)
          endif
        endif
        this.w_i = this.w_i+1
      enddo
      AH_ErrorMsg("Elaborazione terminata", "i")
    endif
  endproc


  proc Init(oParentObject,bTipo)
    this.bTipo=bTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="bTipo"
endproc
