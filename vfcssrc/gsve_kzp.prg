* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kzp                                                        *
*              Numero registrazione                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_16]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-16                                                      *
* Last revis.: 2005-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kzp",oParentObject))

* --- Class definition
define class tgsve_kzp as StdForm
  Top    = 41
  Left   = 96

  * --- Standard Properties
  Width  = 470
  Height = 199
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2005-07-08"
  HelpContextID=250229353
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsve_kzp"
  cComment = "Numero registrazione"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODSER = space(10)
  w_PREGNO = 0
  w_ANNO = space(4)
  w_DATREG = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CODVAL = space(3)
  w_SIMVAL = space(5)
  w_VTR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kzpPag1","gsve_kzp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_VTR = this.oPgFrm.Pages(1).oPag.VTR
    DoDefault()
    proc Destroy()
      this.w_VTR = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODSER=space(10)
      .w_PREGNO=0
      .w_ANNO=space(4)
      .w_DATREG=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CODVAL=space(3)
      .w_SIMVAL=space(5)
      .w_CODSER=oParentObject.w_CODSER
      .w_PREGNO=oParentObject.w_PREGNO
      .w_ANNO=oParentObject.w_ANNO
      .w_DATREG=oParentObject.w_DATREG
      .w_DATINI=oParentObject.w_DATINI
      .w_DATFIN=oParentObject.w_DATFIN
      .w_CODVAL=oParentObject.w_CODVAL
      .w_SIMVAL=oParentObject.w_SIMVAL
        .w_CODSER = .w_VTR.getVar('VTSERIAL')
      .oPgFrm.Page1.oPag.VTR.Calculate(.F.)
      .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
        .w_PREGNO = .w_VTR.getVar('VTNUMREG')
        .w_ANNO = .w_VTR.getVar('VT__ANNO')
        .w_DATREG = .w_VTR.getVar('VTDATREG')
        .w_DATINI = .w_VTR.getVar('VTDATINI')
        .w_DATFIN = .w_VTR.getVar('VTDATFIN')
        .w_CODVAL = .w_VTR.getVar('VTCODVAL')
        .w_SIMVAL = .w_VTR.getVar('VTSIMVAL')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CODSER=.w_CODSER
      .oParentObject.w_PREGNO=.w_PREGNO
      .oParentObject.w_ANNO=.w_ANNO
      .oParentObject.w_DATREG=.w_DATREG
      .oParentObject.w_DATINI=.w_DATINI
      .oParentObject.w_DATFIN=.w_DATFIN
      .oParentObject.w_CODVAL=.w_CODVAL
      .oParentObject.w_SIMVAL=.w_SIMVAL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CODSER = .w_VTR.getVar('VTSERIAL')
        .oPgFrm.Page1.oPag.VTR.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
            .w_PREGNO = .w_VTR.getVar('VTNUMREG')
            .w_ANNO = .w_VTR.getVar('VT__ANNO')
            .w_DATREG = .w_VTR.getVar('VTDATREG')
            .w_DATINI = .w_VTR.getVar('VTDATINI')
            .w_DATFIN = .w_VTR.getVar('VTDATFIN')
            .w_CODVAL = .w_VTR.getVar('VTCODVAL')
            .w_SIMVAL = .w_VTR.getVar('VTSIMVAL')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.VTR.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.VTR.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_3.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsve_kzpPag1 as StdContainer
  Width  = 466
  height = 199
  stdWidth  = 466
  stdheight = 199
  resizeXpos=296
  resizeYpos=169
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object VTR as cp_zoombox with uid="GLEPMQBISF",left=4, top=4, width=452,height=187,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='VTR_MAST',cZoomFile='PREVI',bOptions=.F.,bAdvOptions=.F.,;
    nPag=1;
    , HelpContextID = 72231706


  add object oObj_1_3 as cp_runprogram with uid="DYCTTXKCNJ",left=345, top=206, width=125,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSVE_BOO',;
    cEvent = "w_vtr selected",;
    nPag=1;
    , HelpContextID = 72231706
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kzp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
