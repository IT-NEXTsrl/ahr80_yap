* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bsp                                                        *
*              Partita saldata?                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-02-01                                                      *
* Last revis.: 2005-02-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_PTSERIAL,w_PTROWORD,w_CPROWNUM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bsp",oParentObject,m.w_PTSERIAL,m.w_PTROWORD,m.w_CPROWNUM)
return(i_retval)

define class tgste_bsp as StdBatch
  * --- Local variables
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_TmpN = 0
  w_RESULT = .f.
  w_NUMPAR = space(31)
  w_DATSCA = ctod("  /  /  ")
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODVAL = space(5)
  * --- WorkFile variables
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Data la chiave database di una partita verifica se � stata saldata parzialemente o meno
    *     Restituisce .t. se saldata...
    this.w_RESULT = .F.
    * --- Read from PAR_TITE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CPROWNUM"+;
        " from "+i_cTable+" PAR_TITE where ";
            +"PTSERRIF = "+cp_ToStrODBC(this.w_PTSERIAL);
            +" and PTORDRIF = "+cp_ToStrODBC(this.w_PTROWORD);
            +" and PTNUMRIF = "+cp_ToStrODBC(this.w_CPROWNUM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CPROWNUM;
        from (i_cTable) where;
            PTSERRIF = this.w_PTSERIAL;
            and PTORDRIF = this.w_PTROWORD;
            and PTNUMRIF = this.w_CPROWNUM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TmpN = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Ho trovato una partita che punta la creazione
    this.w_RESULT = i_Rows<>0
    if Not this.w_RESULT
      * --- Ricerco una partita di acconto o saldo che ha gli stessi estremi
      *     della partita di creazione...
      * --- Read from PAR_TITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL"+;
          " from "+i_cTable+" PAR_TITE where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PTCODVAL;
          from (i_cTable) where;
              PTSERIAL = this.w_PTSERIAL;
              and PTROWORD = this.w_PTROWORD;
              and CPROWNUM = this.w_CPROWNUM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NUMPAR = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
        this.w_DATSCA = NVL(cp_ToDate(_read_.PTDATSCA),cp_NullValue(_read_.PTDATSCA))
        this.w_TIPCON = NVL(cp_ToDate(_read_.PTTIPCON),cp_NullValue(_read_.PTTIPCON))
        this.w_CODCON = NVL(cp_ToDate(_read_.PTCODCON),cp_NullValue(_read_.PTCODCON))
        this.w_CODVAL = NVL(cp_ToDate(_read_.PTCODVAL),cp_NullValue(_read_.PTCODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Select from PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PTSERIAL, PTFLIMPE, PTFLINDI,PTSERRIF  from "+i_cTable+" PAR_TITE ";
            +" where PTFLCRSA In ('S','A') And PTNUMPAR="+cp_ToStrODBC(this.w_NUMPAR)+" And PTTIPCON="+cp_ToStrODBC(this.w_TIPCON)+" And PTCODCON="+cp_ToStrODBC(this.w_CODCON)+" And PTCODVAL="+cp_ToStrODBC(this.w_CODVAL)+" And PTDATSCA="+cp_ToStrODBC(this.w_DATSCA)+"";
             ,"_Curs_PAR_TITE")
      else
        select PTSERIAL, PTFLIMPE, PTFLINDI,PTSERRIF from (i_cTable);
         where PTFLCRSA In ("S","A") And PTNUMPAR=this.w_NUMPAR And PTTIPCON=this.w_TIPCON And PTCODCON=this.w_CODCON And PTCODVAL=this.w_CODVAL And PTDATSCA=this.w_DATSCA;
          into cursor _Curs_PAR_TITE
      endif
      if used('_Curs_PAR_TITE')
        select _Curs_PAR_TITE
        locate for 1=1
        do while not(eof())
        * --- Se partita di saldo senza riferimento...
        if Empty( Nvl ( _Curs_PAR_TITE.PTFLIMPE ,"" ) + Nvl( _Curs_PAR_TITE.PTFLINDI ,"" ) + Nvl( _Curs_PAR_TITE.PTSERRIF ,"" ) )
          this.w_RESULT = Not Empty ( Nvl( _Curs_PAR_TITE.PTSERIAL ,"" ))
          Exit
        endif
          select _Curs_PAR_TITE
          continue
        enddo
        use
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_RESULT
    return
  endproc


  proc Init(oParentObject,w_PTSERIAL,w_PTROWORD,w_CPROWNUM)
    this.w_PTSERIAL=w_PTSERIAL
    this.w_PTROWORD=w_PTROWORD
    this.w_CPROWNUM=w_CPROWNUM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_TITE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_PTSERIAL,w_PTROWORD,w_CPROWNUM"
endproc
