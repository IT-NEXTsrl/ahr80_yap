* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bvu                                                        *
*              Verifica esistenza file (AET)                                   *
*                                                                              *
*      Author: Gianluca B.                                                     *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-28                                                      *
* Last revis.: 2009-05-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bvu",oParentObject)
return(i_retval)

define class tgsut_bvu as StdBatch
  * --- Local variables
  w_IMGPAT = space(254)
  w_LIPATIMG = space(250)
  w_CRITPERI = space(1)
  w_NOMEFILE = space(60)
  w_ZOOM = .NULL.
  w_CURSORE = space(10)
  * --- WorkFile variables
  PROMCLAS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Funzione per la verifica dell'esistenza fisica del file passato come parametro
    if ! empty(this.oParentObject.w_CODCLAS)
      * --- Read from PROMCLAS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PROMCLAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CDPATSTD,CDTIPRAG"+;
          " from "+i_cTable+" PROMCLAS where ";
              +"CDCODCLA = "+cp_ToStrODBC(this.oParentObject.w_CODCLAS);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CDPATSTD,CDTIPRAG;
          from (i_cTable) where;
              CDCODCLA = this.oParentObject.w_CODCLAS;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LIPATIMG = NVL(cp_ToDate(_read_.CDPATSTD),cp_NullValue(_read_.CDPATSTD))
        this.w_CRITPERI = NVL(cp_ToDate(_read_.CDTIPRAG),cp_NullValue(_read_.CDTIPRAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_ZOOM = this.oParentObject.w_PRATICHE
      this.w_CURSORE = this.w_ZOOM.cCursor
       
 Select(this.w_CURSORE) 
 Go Top
      SCAN
      do case
        case IDMODALL=="F"
          * --- Gestione del periodo (determina percorsi...) => Determina w_ImgPat
          * --- Verifico se � presente il percorso assoluto di archiviazione dell'allegato
          if not empty(IDPATALL)
            if empty(JustDrive(IDPATALL))
              * --- Se presente un percorso relativo, lo completo
              this.w_IMGPAT = addbs(sys(5)+sys(2003))+alltrim(IDPATALL)
            else
              this.w_IMGPAT = alltrim(IDPATALL)
            endif
          else
            this.w_IMGPAT = DCMPATAR(this.w_LIPATIMG, this.w_CRITPERI, PERIODO, this.oParentObject.w_CODCLAS)
          endif
        case IDMODALL =="L"
          * --- Nel caso di collegamento il percorso � quello in ORIFIL
          this.w_IMGPAT = JustPath(IDORIFIL)
      endcase
      this.w_NOMEFILE = Alltrim(IDNOMFIL)
      * --- Acquisizione della directory corrente - PARTE COMUNE
      if  cp_fileexist(addbs(this.w_IMGPAT)+this.w_NOMEFILE)
        replace FILEXIST with "S",IDPATALL with this.w_IMGPAT
      endif
      ENDSCAN
       
 Select(this.w_CURSORE) 
 Go Top
      this.w_ZOOM.Refresh()     
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PROMCLAS'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
