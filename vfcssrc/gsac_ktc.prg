* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_ktc                                                        *
*              Tracciabilità commessa                                          *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-16                                                      *
* Last revis.: 2016-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsac_ktc",oParentObject))

* --- Class definition
define class tgsac_ktc as StdForm
  Top    = 3
  Left   = 9

  * --- Standard Properties
  Width  = 786
  Height = 484
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-09-12"
  HelpContextID=82470761
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=29

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  KEY_ARTI_IDX = 0
  MAGAZZIN_IDX = 0
  CONTI_IDX = 0
  ART_ICOL_IDX = 0
  cPrg = "gsac_ktc"
  cComment = "Tracciabilità commessa"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_pCODCOM = space(15)
  w_DESCAN = space(30)
  w_pCODICE = space(20)
  w_DESRIC = space(40)
  w_pCODMAG = space(5)
  w_DESMAG = space(30)
  w_TIPCLI = space(1)
  w_pCODCLI = space(15)
  w_DESCLI = space(40)
  w_pTIPCLI = space(1)
  w_TIPFOR = space(1)
  w_CLIFOR = space(1)
  o_CLIFOR = space(1)
  w_pCODFOR = space(15)
  w_DESFOR = space(40)
  w_pTIPFOR = space(1)
  w_pDATINI = ctod('  /  /  ')
  w_pDATFIN = ctod('  /  /  ')
  w_DESART = space(41)
  w_TIPO = space(3)
  w_PARAME = space(3)
  w_MVSERIAL = space(10)
  w_OLCODODL = space(15)
  w_PDSERIAL = space(10)
  w_PDROWNUM = 0
  w_CODCOM = space(15)
  w_CODART = space(20)
  w_TIPCF = space(1)
  w_FLCOMM = space(1)
  w_ELENCO = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsac_ktcPag1","gsac_ktc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.opCODCOM_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ELENCO = this.oPgFrm.Pages(1).oPag.ELENCO
    DoDefault()
    proc Destroy()
      this.w_ELENCO = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='ART_ICOL'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec("QUERY\GSAC5KTC.VQR",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_pCODCOM=space(15)
      .w_DESCAN=space(30)
      .w_pCODICE=space(20)
      .w_DESRIC=space(40)
      .w_pCODMAG=space(5)
      .w_DESMAG=space(30)
      .w_TIPCLI=space(1)
      .w_pCODCLI=space(15)
      .w_DESCLI=space(40)
      .w_pTIPCLI=space(1)
      .w_TIPFOR=space(1)
      .w_CLIFOR=space(1)
      .w_pCODFOR=space(15)
      .w_DESFOR=space(40)
      .w_pTIPFOR=space(1)
      .w_pDATINI=ctod("  /  /  ")
      .w_pDATFIN=ctod("  /  /  ")
      .w_DESART=space(41)
      .w_TIPO=space(3)
      .w_PARAME=space(3)
      .w_MVSERIAL=space(10)
      .w_OLCODODL=space(15)
      .w_PDSERIAL=space(10)
      .w_PDROWNUM=0
      .w_CODCOM=space(15)
      .w_CODART=space(20)
      .w_TIPCF=space(1)
      .w_FLCOMM=space(1)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_pCODCOM))
          .link_1_7('Full')
        endif
        .DoRTCalc(3,4,.f.)
        if not(empty(.w_pCODICE))
          .link_1_9('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_pCODMAG))
          .link_1_11('Full')
        endif
          .DoRTCalc(7,7,.f.)
        .w_TIPCLI = "C"
        .w_pCODCLI = ''
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_pCODCLI))
          .link_1_14('Full')
        endif
          .DoRTCalc(10,10,.f.)
        .w_pTIPCLI = iif(empty(.w_pCODCLI), " ", "C")
        .w_TIPFOR = "F"
        .w_CLIFOR = 'C'
        .w_pCODFOR = ''
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_pCODFOR))
          .link_1_19('Full')
        endif
          .DoRTCalc(15,15,.f.)
        .w_pTIPFOR = iif(empty(.w_pCODFOR), " ", "F")
      .oPgFrm.Page1.oPag.ELENCO.Calculate()
          .DoRTCalc(17,18,.f.)
        .w_DESART = .w_ELENCO.getvar("DESCRI")
        .w_TIPO = .w_ELENCO.getvar("TIPO")
        .w_PARAME = .w_ELENCO.getvar("MVPARAM")
        .w_MVSERIAL = .w_ELENCO.getvar("MVSERIAL")
        .w_OLCODODL = iif(.w_TIPO $ "ODL-OCL-ODA", .w_ELENCO.getvar("OLCODODL"), space(15))
        .w_PDSERIAL = iif(.w_TIPO="PDA", left(.w_ELENCO.getvar("OLCODODL"),10), space(10))
        .w_PDROWNUM = iif(.w_TIPO="PDA", .w_ELENCO.getvar("PDROWNUM"), 0)
        .w_CODCOM = NVL(.w_ELENCO.getvar("CODCOM"),"")
          .DoRTCalc(27,27,.f.)
        .w_TIPCF = IIF(.w_CLIFOR='C','C',(IIF(.w_CLIFOR='F','F','N')))
        .w_FLCOMM = 'S'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,8,.t.)
        if .o_CLIFOR<>.w_CLIFOR
            .w_pCODCLI = ''
          .link_1_14('Full')
        endif
        .DoRTCalc(10,10,.t.)
            .w_pTIPCLI = iif(empty(.w_pCODCLI), " ", "C")
        .DoRTCalc(12,13,.t.)
        if .o_CLIFOR<>.w_CLIFOR
            .w_pCODFOR = ''
          .link_1_19('Full')
        endif
        .DoRTCalc(15,15,.t.)
            .w_pTIPFOR = iif(empty(.w_pCODFOR), " ", "F")
        .oPgFrm.Page1.oPag.ELENCO.Calculate()
        .DoRTCalc(17,18,.t.)
            .w_DESART = .w_ELENCO.getvar("DESCRI")
            .w_TIPO = .w_ELENCO.getvar("TIPO")
            .w_PARAME = .w_ELENCO.getvar("MVPARAM")
            .w_MVSERIAL = .w_ELENCO.getvar("MVSERIAL")
            .w_OLCODODL = iif(.w_TIPO $ "ODL-OCL-ODA", .w_ELENCO.getvar("OLCODODL"), space(15))
            .w_PDSERIAL = iif(.w_TIPO="PDA", left(.w_ELENCO.getvar("OLCODODL"),10), space(10))
            .w_PDROWNUM = iif(.w_TIPO="PDA", .w_ELENCO.getvar("PDROWNUM"), 0)
            .w_CODCOM = NVL(.w_ELENCO.getvar("CODCOM"),"")
        .DoRTCalc(27,27,.t.)
        if .o_CLIFOR<>.w_CLIFOR
            .w_TIPCF = IIF(.w_CLIFOR='C','C',(IIF(.w_CLIFOR='F','F','N')))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(29,29,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ELENCO.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.opCODCOM_1_7.enabled = this.oPgFrm.Page1.oPag.opCODCOM_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.opCODCLI_1_14.visible=!this.oPgFrm.Page1.oPag.opCODCLI_1_14.mHide()
    this.oPgFrm.Page1.oPag.oDESCLI_1_15.visible=!this.oPgFrm.Page1.oPag.oDESCLI_1_15.mHide()
    this.oPgFrm.Page1.oPag.opCODFOR_1_19.visible=!this.oPgFrm.Page1.oPag.opCODFOR_1_19.mHide()
    this.oPgFrm.Page1.oPag.oDESFOR_1_20.visible=!this.oPgFrm.Page1.oPag.oDESFOR_1_20.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_34.visible=!this.oPgFrm.Page1.oPag.oBtn_1_34.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_35.visible=!this.oPgFrm.Page1.oPag.oBtn_1_35.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_36.visible=!this.oPgFrm.Page1.oPag.oBtn_1_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ELENCO.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=pCODCOM
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_pCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_pCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_pCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_pCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_pCODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_pCODCOM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_pCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'opCODCOM_1_7'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_pCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_pCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_pCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_pCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_pCODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_pCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=pCODICE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_pCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_pCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARFLCOMM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_pCODICE))
          select ARCODART,ARDESART,ARFLCOMM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_pCODICE)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_pCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARFLCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_pCODICE)+"%");

            select ARCODART,ARDESART,ARFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_pCODICE) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'opCODICE_1_9'),i_cWhere,'',"Articoli",'gsac_ztc.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARFLCOMM";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_pCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARFLCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_pCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_pCODICE)
            select ARCODART,ARDESART,ARFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_pCODICE = NVL(_Link_.ARCODART,space(20))
      this.w_DESRIC = NVL(_Link_.ARDESART,space(40))
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_FLCOMM = NVL(_Link_.ARFLCOMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_pCODICE = space(20)
      endif
      this.w_DESRIC = space(40)
      this.w_CODART = space(20)
      this.w_FLCOMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLCOMM='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo inesistente o non gestito a commessa")
        endif
        this.w_pCODICE = space(20)
        this.w_DESRIC = space(40)
        this.w_CODART = space(20)
        this.w_FLCOMM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_pCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=pCODMAG
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_pCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_pCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_pCODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_pCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_pCODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_pCODMAG)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_pCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'opCODMAG_1_11'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_pCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_pCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_pCODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_pCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_pCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_pCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=pCODCLI
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_pCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_pCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLI;
                     ,'ANCODICE',trim(this.w_pCODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_pCODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_pCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'opCODCLI_1_14'),i_cWhere,'',"Clienti",'GSAR0ACL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_pCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_pCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLI;
                       ,'ANCODICE',this.w_pCODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_pCODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_pCODCLI = space(15)
      endif
      this.w_DESCLI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_pCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=pCODFOR
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_pCODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_pCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPFOR;
                     ,'ANCODICE',trim(this.w_pCODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_pCODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_pCODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'opCODFOR_1_19'),i_cWhere,'',"Fornitori",'GSAR0AFR.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_pCODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_pCODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPFOR;
                       ,'ANCODICE',this.w_pCODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_pCODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_pCODFOR = space(15)
      endif
      this.w_DESFOR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_pCODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.opCODCOM_1_7.value==this.w_pCODCOM)
      this.oPgFrm.Page1.oPag.opCODCOM_1_7.value=this.w_pCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_8.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_8.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.opCODICE_1_9.value==this.w_pCODICE)
      this.oPgFrm.Page1.oPag.opCODICE_1_9.value=this.w_pCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIC_1_10.value==this.w_DESRIC)
      this.oPgFrm.Page1.oPag.oDESRIC_1_10.value=this.w_DESRIC
    endif
    if not(this.oPgFrm.Page1.oPag.opCODMAG_1_11.value==this.w_pCODMAG)
      this.oPgFrm.Page1.oPag.opCODMAG_1_11.value=this.w_pCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_12.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_12.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.opCODCLI_1_14.value==this.w_pCODCLI)
      this.oPgFrm.Page1.oPag.opCODCLI_1_14.value=this.w_pCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_15.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_15.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIFOR_1_18.RadioValue()==this.w_CLIFOR)
      this.oPgFrm.Page1.oPag.oCLIFOR_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.opCODFOR_1_19.value==this.w_pCODFOR)
      this.oPgFrm.Page1.oPag.opCODFOR_1_19.value=this.w_pCODFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_20.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_20.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.opDATINI_1_22.value==this.w_pDATINI)
      this.oPgFrm.Page1.oPag.opDATINI_1_22.value=this.w_pDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.opDATFIN_1_23.value==this.w_pDATFIN)
      this.oPgFrm.Page1.oPag.opDATFIN_1_23.value=this.w_pDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_26.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_26.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_38.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_38.value=this.w_CODCOM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_FLCOMM='S')  and not(empty(.w_pCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.opCODICE_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo inesistente o non gestito a commessa")
          case   not(empty(.w_pDATFIN) or (.w_pDATINI<=.w_pDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.opDATINI_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_pDATINI) or (.w_pDATINI<=.w_pDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.opDATFIN_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CLIFOR = this.w_CLIFOR
    return

enddefine

* --- Define pages as container
define class tgsac_ktcPag1 as StdContainer
  Width  = 782
  height = 484
  stdWidth  = 782
  stdheight = 484
  resizeXpos=577
  resizeYpos=257
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object opCODCOM_1_7 as StdField with uid="YLQVWTCUFO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_pCODCOM", cQueryName = "pCODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 24194314,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=120, Top=7, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_pCODCOM"

  func opCODCOM_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM="S" or g_PERCAN="S")
    endwith
   endif
  endfunc

  func opCODCOM_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc opCODCOM_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc opCODCOM_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'opCODCOM_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oDESCAN_1_8 as StdField with uid="PJHERXXTYJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 43118026,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=284, Top=7, InputMask=replicate('X',30)

  add object opCODICE_1_9 as StdField with uid="WXSMCRBREQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_pCODICE", cQueryName = "pCODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo inesistente o non gestito a commessa",;
    ToolTipText = "Codice articolo",;
    HelpContextID = 49206006,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=120, Top=30, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_pCODICE"

  func opCODICE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc opCODICE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc opCODICE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'opCODICE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'gsac_ztc.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDESRIC_1_10 as StdField with uid="YEFXVVACGA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESRIC", cQueryName = "DESRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 218295754,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=284, Top=30, InputMask=replicate('X',40)

  add object opCODMAG_1_11 as StdField with uid="ICWELCFBZD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_pCODMAG", cQueryName = "pCODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 19845878,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=120, Top=54, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_pCODMAG"

  func opCODMAG_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc opCODMAG_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc opCODMAG_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'opCODMAG_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oDESMAG_1_12 as StdField with uid="PMQVWSOUJN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 159903178,;
   bGlobalFont=.t.,;
    Height=21, Width=361, Left=186, Top=54, InputMask=replicate('X',30)

  add object opCODCLI_1_14 as StdField with uid="HRPIORURYL",rtseq=9,rtrep=.f.,;
    cFormVar = "w_pCODCLI", cQueryName = "pCODCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente",;
    HelpContextID = 74525962,;
   bGlobalFont=.t.,;
    Height=21, Width=151, Left=247, Top=77, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLI", oKey_2_1="ANCODICE", oKey_2_2="this.w_pCODCLI"

  func opCODCLI_1_14.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR<>'C')
    endwith
  endfunc

  func opCODCLI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc opCODCLI_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc opCODCLI_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLI)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'opCODCLI_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti",'GSAR0ACL.CONTI_VZM',this.parent.oContained
  endproc

  add object oDESCLI_1_15 as StdField with uid="LGMOHBWFDT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 115469770,;
   bGlobalFont=.t.,;
    Height=21, Width=316, Left=400, Top=77, InputMask=replicate('X',40)

  func oDESCLI_1_15.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR<>'C')
    endwith
  endfunc


  add object oCLIFOR_1_18 as StdCombo with uid="KLTXNPKPJS",value=3,rtseq=13,rtrep=.f.,left=120,top=77,width=123,height=21;
    , HelpContextID = 38828326;
    , cFormVar="w_CLIFOR",RowSource=""+"Cliente,"+"Fornitore,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLIFOR_1_18.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oCLIFOR_1_18.GetRadio()
    this.Parent.oContained.w_CLIFOR = this.RadioValue()
    return .t.
  endfunc

  func oCLIFOR_1_18.SetRadio()
    this.Parent.oContained.w_CLIFOR=trim(this.Parent.oContained.w_CLIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_CLIFOR=='C',1,;
      iif(this.Parent.oContained.w_CLIFOR=='F',2,;
      iif(this.Parent.oContained.w_CLIFOR=='',3,;
      0)))
  endfunc

  add object opCODFOR_1_19 as StdField with uid="XBWHLQVIAX",rtseq=14,rtrep=.f.,;
    cFormVar = "w_pCODFOR", cQueryName = "pCODFOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice fornitore",;
    HelpContextID = 247386870,;
   bGlobalFont=.t.,;
    Height=21, Width=151, Left=249, Top=78, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_pCODFOR"

  func opCODFOR_1_19.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR<>'F')
    endwith
  endfunc

  func opCODFOR_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc opCODFOR_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc opCODFOR_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'opCODFOR_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Fornitori",'GSAR0AFR.CONTI_VZM',this.parent.oContained
  endproc

  add object oDESFOR_1_20 as StdField with uid="VBGRGTFUAN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 38867510,;
   bGlobalFont=.t.,;
    Height=21, Width=316, Left=402, Top=78, InputMask=replicate('X',40)

  func oDESFOR_1_20.mHide()
    with this.Parent.oContained
      return (.w_CLIFOR<>'F')
    endwith
  endfunc

  add object opDATINI_1_22 as StdField with uid="GDNIVEXSZA",rtseq=17,rtrep=.f.,;
    cFormVar = "w_pDATINI", cQueryName = "pDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 33688586,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=642, Top=31

  func opDATINI_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_pDATFIN) or (.w_pDATINI<=.w_pDATFIN))
    endwith
    return bRes
  endfunc

  add object opDATFIN_1_23 as StdField with uid="FYJFDJERZO",rtseq=18,rtrep=.f.,;
    cFormVar = "w_pDATFIN", cQueryName = "pDATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 120720394,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=642, Top=54

  func opDATFIN_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_pDATINI) or (.w_pDATINI<=.w_pDATFIN))
    endwith
    return bRes
  endfunc


  add object ELENCO as cp_zoombox with uid="TTPLCLMYLW",left=3, top=104, width=775,height=326,;
    caption='ELENCO',;
   bGlobalFont=.t.,;
    cZoomFile="GSAC_KTC",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="PDA_DETT",bRetriveAllRows=.f.,cZoomOnZoom="",cMenuFile="",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 23578298


  add object oBtn_1_25 as StdButton with uid="FTEUPABWGN",left=727, top=54, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue interrogazione in base ai parametri di selezione";
    , HelpContextID = 82468842;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        .NotifyEvent('Interroga')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESART_1_26 as StdField with uid="SPNPVQNITU",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 75239990,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=128, Top=445, InputMask=replicate('X',41)


  add object oBtn_1_34 as StdButton with uid="XJVYSVMPXO",left=8, top=435, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato alla riga selezionata";
    , HelpContextID = 219309114;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_MVSERIAL, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MVSERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_34.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_MVSERIAL))
     endwith
    endif
  endfunc


  add object oBtn_1_35 as StdButton with uid="SRAVMDUWII",left=8, top=435, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza l'ODL della riga selezionata";
    , HelpContextID = 219309114;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      with this.Parent.oContained
        GSAC_BTM(this.Parent.oContained,.w_OLCODODL, "VISUALIZZA_ODP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_35.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_OLCODODL))
      endwith
    endif
  endfunc

  func oBtn_1_35.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_OLCODODL))
     endwith
    endif
  endfunc


  add object oBtn_1_36 as StdButton with uid="EMYZHFVYPQ",left=8, top=435, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza la PDA della riga selezionata";
    , HelpContextID = 219309114;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        GSAC_BTM(this.Parent.oContained,.w_PDSERIAL, "VISUALIZZA_ODP", .w_PDROWNUM)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PDSERIAL) and NOT EMPTY(.w_PDROWNUM))
      endwith
    endif
  endfunc

  func oBtn_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_PDSERIAL) or EMPTY(.w_PDROWNUM))
     endwith
    endif
  endfunc

  add object oCODCOM_1_38 as StdField with uid="KSKNTHPXUX",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 45274074,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=532, Top=445, InputMask=replicate('X',15)


  add object oBtn_1_39 as StdButton with uid="VIALZUPESJ",left=671, top=435, width=48,height=45,;
    CpPicture="bmp\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 204280697;
    , tabStop=.f.,Caption='\<Stampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      vx_exec("QUERY\GSAC5KTC.VQR",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_40 as StdButton with uid="IPBTJCBBGJ",left=726, top=435, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75153338;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="XCTGVZYJLK",Visible=.t., Left=7, Top=9,;
    Alignment=1, Width=109, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="MJWHEOMANN",Visible=.t., Left=7, Top=32,;
    Alignment=1, Width=109, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="HAZBSMSRMY",Visible=.t., Left=7, Top=56,;
    Alignment=1, Width=109, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="AXZWKQBIUS",Visible=.t., Left=561, Top=33,;
    Alignment=1, Width=78, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="XUUNKNNRZR",Visible=.t., Left=561, Top=56,;
    Alignment=1, Width=78, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="PVSBNWZPLZ",Visible=.t., Left=68, Top=447,;
    Alignment=1, Width=59, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="MFXBUDKSWC",Visible=.t., Left=440, Top=447,;
    Alignment=1, Width=90, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="SPLPETRBCZ",Visible=.t., Left=7, Top=78,;
    Alignment=1, Width=109, Height=18,;
    Caption="Cliente/fornitore:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac_ktc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
