* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_biu                                                        *
*              Gestione lista incongruenze prezzi e U.C.A.                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-02-01                                                      *
* Last revis.: 2012-12-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_biu",oParentObject,m.pOPER)
return(i_retval)

define class tgsve_biu as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_PADRE = .NULL.
  w_STARTPRZ = 0
  w_ALTIPOPE = space(1)
  w_FL_SAVED = .f.
  w_OBJ_MASK = .NULL.
  w_CALPRZ = 0
  w_DL__GUID = space(10)
  w_FLPRZLIS = space(1)
  w_FLPRZPRO = space(1)
  w_SER_LIST = space(10)
  w_SER_PROM = space(10)
  w_LSCODLIS = space(5)
  w_LSCODPRO = space(5)
  w_SCONTO_RIGA_PERCENTUALE = 0
  w_VALORE_SCONTO_PAGAMENTO = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = this.oParentObject
    do case
      case this.pOPER=="INIT"
        SELECT "_tmpUCA_"
        GO TOP
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
          this.w_PADRE.w_LIKEYLIS = _tmpUCA_.t_KEYLISTB
        endif
        this.w_PADRE.w_LICODVAL = _tmpUCA_.t_MVCODVAL
        this.w_PADRE.w_LITIPCON = _tmpUCA_.t_MVTIPCON
        this.w_PADRE.w_LICODCON = _tmpUCA_.t_MVCODCON
        this.w_PADRE.w_LICAOVAL = _tmpUCA_.t_MVCAOVAL
        this.w_PADRE.w_LIDATDOC = _tmpUCA_.t_MVDATDOC
        SCAN
        this.w_PADRE.AddRow()     
        this.w_PADRE.w_LIROWNUM = _tmpUCA_.CPROWNUM
        this.w_PADRE.w_LIROWORD = _tmpUCA_.t_CPROWORD
        this.w_PADRE.w_LICODICE = _tmpUCA_.t_MVCODICE
        this.w_PADRE.w_LIDESART = _tmpUCA_.t_MVDESART
        this.w_PADRE.w_LIUNIMIS = _tmpUCA_.t_MVUNIMIS
        this.w_PADRE.w_LIQTAMOV = _tmpUCA_.t_MVQTAMOV
        this.w_PADRE.w_LIPREZZO = _tmpUCA_.t_MVPREZZO
        if g_PERVAL<>this.w_PADRE.w_LICODVAL
          this.w_PADRE.w_LIPREZZO = this.w_PADRE.w_LIPREZZO / this.w_PADRE.w_LICAOVAL
        endif
        this.w_PADRE.w_LIFLCPRO = _tmpUCA_.t_MVFLCPRO
        this.w_PADRE.w_LIKEYSAL = _tmpUCA_.t_MVKEYSAL
        this.w_PADRE.w_LICODMAG = _tmpUCA_.t_MVCODMAG
        this.w_PADRE.w_LIIMPNAZ = _tmpUCA_.t_MVIMPNAZ
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
          this.w_PADRE.w_LIIMPCOL = _tmpUCA_.t_MVIMPCOL
          if g_PERVAL<>this.w_PADRE.w_LICODVAL
            this.w_PADRE.w_LIIMPCOL = this.w_PADRE.w_LIIMPCOL / this.w_PADRE.w_LICAOVAL
          endif
        endif
        this.w_PADRE.w_LIIMPPRO = _tmpUCA_.t_MVIMPPRO
        this.w_PADRE.w_LISCONT1 = _tmpUCA_.t_MVSCONT1
        this.w_PADRE.w_LISCONT2 = _tmpUCA_.t_MVSCONT2
        this.w_PADRE.w_LISCONT3 = _tmpUCA_.t_MVSCONT3
        this.w_PADRE.w_LISCONT4 = _tmpUCA_.t_MVSCONT4
        this.w_PADRE.w_LIPERPRO = _tmpUCA_.t_MVPERPRO
        this.w_PADRE.w_LIIMPUCA = _tmpUCA_.t_UCA
        this.w_PADRE.w_LISCOCL1 = _tmpUCA_.t_MVSCOCL1
        this.w_PADRE.w_LISCOCL2 = _tmpUCA_.t_MVSCOCL2
        this.w_PADRE.w_LIDATUCA = _tmpUCA_.t_DATE_UCA
        this.w_PADRE.w_LICODLIS = _tmpUCA_.t_MVCODLIS
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
          this.w_PADRE.w_LISCOLIS = _tmpUCA_.t_MVSCOLIS
          this.w_PADRE.w_LIPROLIS = _tmpUCA_.t_MVPROLIS
          this.w_PADRE.w_LIPROSCO = _tmpUCA_.t_MVPROSCO
          this.w_PADRE.w_LILISCHK = _tmpUCA_.t_LISCHK
        endif
        this.w_PADRE.w_LIQTAUM1 = _tmpUCA_.t_MVQTAUM1
        this.w_PADRE.w_LICHKUCA = _tmpUCA_.t_ARCHKUCA
        this.w_PADRE.w_LIIMPSCO = _tmpUCA_.t_MVIMPSCO
        this.w_PADRE.mCalc(.T.)     
        this.w_PADRE.SaveRow()     
        SELECT "_tmpUCA_"
        ENDSCAN
        this.w_PADRE.SetRow(1)     
      case this.pOPER=="SIMULATE"
        this.w_STARTPRZ = this.oParentObject.w_LIIMPUCA + this.oParentObject.w_LIIMPUCA * this.oParentObject.w_LIMARGIN / 100
        this.w_SCONTO_RIGA_PERCENTUALE = (100 + this.oParentObject.w_LISCONT1) / 100 * (100 + this.oParentObject.w_LISCONT2) / 100 * (100 + this.oParentObject.w_LISCONT3) / 100 * (100 + this.oParentObject.w_LISCONT4) / 100
        * --- Arrotonda per difetto alla prima cifra decimale della percentuale
        this.w_SCONTO_RIGA_PERCENTUALE = INT( 100 * this.w_SCONTO_RIGA_PERCENTUALE ) / 100
        do case
          case this.oParentObject.w_LITIPMAR="R"
            * --- Calcolo su netto riga
            this.w_VALORE_SCONTO_PAGAMENTO = 0
          case this.oParentObject.w_LITIPMAR="D"
            * --- Calcolo su netto documento
            this.w_VALORE_SCONTO_PAGAMENTO = this.oParentObject.w_LIIMPSCO / this.oParentObject.w_LIQTAUM1
          case this.oParentObject.w_LITIPMAR="P"
            * --- Calcolo su netto provvigioni
            this.w_VALORE_SCONTO_PAGAMENTO = ( this.oParentObject.w_LIIMPSCO - IIF( this.oParentObject.w_LITIPMAR="P" , this.w_PADRE.w_LIIMPNAZ - this.w_PADRE.w_LIIMPNPR , 0) ) / this.oParentObject.w_LIQTAUM1
        endcase
        if this.w_PADRE.w_LIPREZZO * this.w_SCONTO_RIGA_PERCENTUALE + this.w_VALORE_SCONTO_PAGAMENTO = 0
          * --- Non ci sono sconti cliente o pagamento
          this.oParentObject.w_LIPRZPRO = this.w_STARTPRZ / this.w_SCONTO_RIGA_PERCENTUALE
        else
          * --- Ci sono sconti cliente o pagamento
          this.oParentObject.w_LIPRZPRO = ( this.w_STARTPRZ * this.w_PADRE.w_LIPREZZO ) / ( this.w_PADRE.w_LIPREZZO * this.w_SCONTO_RIGA_PERCENTUALE + this.w_VALORE_SCONTO_PAGAMENTO )
        endif
      case this.pOPER=="SAVE"
        if this.oParentObject.w_LIPRZPRO<>0 AND lower(this.w_PADRE.oparentObject.class)=="tgsve_miu"
          do case
            case this.oParentObject.w_LIUNIMIS=this.oParentObject.w_ARUNMIS1
              this.w_PADRE.oParentObject.Set("w_LIPREZZO" , this.oParentObject.w_LIPRZPRO * IIF( g_PERVAL<>this.w_PADRE.w_LICODVAL, this.w_PADRE.w_LICAOVAL , 1), .F., .T.)     
            case this.oParentObject.w_LIUNIMIS=this.oParentObject.w_ARUNMIS2
              this.w_PADRE.oParentObject.Set("w_LIPREZZO" , this.oParentObject.w_LIPRZPRO*IIF(this.oParentObject.w_AROPERAT="/", this.oParentObject.w_ARMOLTIP, 1/this.oParentObject.w_ARMOLTIP) * IIF( g_PERVAL<>this.w_PADRE.w_LICODVAL, this.w_PADRE.w_LICAOVAL , 1), .F., .T.)     
            case this.oParentObject.w_LIUNIMIS=this.oParentObject.w_CAUNIMIS
              this.w_PADRE.oParentObject.Set("w_LIPREZZO" , this.oParentObject.w_LIPRZPRO*IIF(this.oParentObject.w_CAOPERAT="/", this.oParentObject.w_CAMOLTIP, 1/this.oParentObject.w_CAMOLTIP) * IIF( g_PERVAL<>this.w_PADRE.w_LICODVAL, this.w_PADRE.w_LICAOVAL , 1), .F., .T.)     
          endcase
          * --- Aggiorna campi collegati al prezzo
          this.w_PADRE.oParentObject.mCalc(.T.)     
        endif
      case this.pOPER=="CONFIRM"
        this.w_PADRE.Exec_Select("_NewPrz_", "t_LIROWNUM AS CPROWNUM, t_LIPREZZO AS LIPREZZO", "!EMPTY(t_LIPREZZO)", "t_LIROWNUM")     
      case this.pOPER=="UPDLIST"
        this.w_FL_SAVED = .F.
        this.w_OBJ_MASK = GSVE_KAD(this)
        if this.w_OBJ_MASK.bSec1 AND this.w_FL_SAVED
          * --- L'utente ha confermato la maschera applico il prezzo ai listini
          * --- Recupero il dettaglio applicato in modo da aggiornare il prezzo o
          *     inserirne uno identico con data d'inizio uguale a i_datsys e prezzo
          *     calcolato
          DECLARE ARR_PRZ(17,1)
          this.w_CALPRZ = CalPrzSt(this.oParentObject.w_LIKEYLIS," ","" ," "," ",this.oParentObject.w_ARCODART,this.oParentObject.w_CACODVAR,this.oParentObject.w_LIQTAUM1,this.oParentObject.w_LICODMAG,this.oParentObject.w_LICODVAL,this.oParentObject.w_LICAOVAL,this.oParentObject.w_LIDATDOC,@ARR_PRZ, ; 
 this.oParentObject.w_LIUNIMIS,this.oParentObject.w_CAPREZUM,"N"," ","V"+this.oParentObject.w_LITIPCON+this.oParentObject.w_LICODCON,this.oParentObject.w_GESTGUID,this.oParentObject.w_GUID_CLF,"R", "N", .T., this.oParentObject.w_LIQTAMOV, " ")
          this.w_SER_LIST = ARR_PRZ[16]
          this.w_SER_PROM = ARR_PRZ[17]
          this.w_LSCODLIS = ARR_PRZ[10]
          this.w_LSCODPRO = ARR_PRZ[11]
          if EMPTY(this.w_SER_LIST) AND EMPTY(this.w_SER_PROM)
            ah_ErrorMsg("Nessun listino trovato. Impossibile proseguire")
          else
            * --- Chiamata routine esterna per ovviare al problema delle tabelle non presenti su ahr
            *     che fanno fallire l'opentable
            GSVE1BIU(this,this.w_LSCODLIS, this.w_LSCODPRO, this.w_SER_LIST, this.w_SER_PROM )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          if this.w_OBJ_MASK.bSec1
            ah_ErrorMsg("Operazione annullata come richiesto",64)
          else
            ah_ErrorMsg("Impossibile accedere alla maschera di selezione tipologia aggiornamento listino.%0Impossibile proseguire")
          endif
        endif
        this.w_OBJ_MASK = .NULL.
      case this.pOPER=="BLANK"
        * --- Il link viene forzato da batch per evitare l'esecuzione del link su campi inesisteni AHE/AHR
        if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CADESART,CACODART,CACODVAR,CAPREZUM,CAUNIMIS,CAOPERAT,CAMOLTIP"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_LICODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CADESART,CACODART,CACODVAR,CAPREZUM,CAUNIMIS,CAOPERAT,CAMOLTIP;
              from (i_cTable) where;
                  CACODICE = this.oParentObject.w_LICODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_LIDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
            this.oParentObject.w_ARCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
            this.oParentObject.w_CACODVAR = NVL(cp_ToDate(_read_.CACODVAR),cp_NullValue(_read_.CACODVAR))
            this.oParentObject.w_CAPREZUM = NVL(cp_ToDate(_read_.CAPREZUM),cp_NullValue(_read_.CAPREZUM))
            this.oParentObject.w_CAUNIMIS = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
            this.oParentObject.w_CAOPERAT = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
            this.oParentObject.w_CAMOLTIP = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARCODRIC,GESTGUID,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARTIPART"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARCODRIC,GESTGUID,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP,ARTIPART;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_ARCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_ARCODRIC = NVL(cp_ToDate(_read_.ARCODRIC),cp_NullValue(_read_.ARCODRIC))
            this.oParentObject.w_GESTGUID = NVL(cp_ToDate(_read_.GESTGUID),cp_NullValue(_read_.GESTGUID))
            this.oParentObject.w_ARUNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            this.oParentObject.w_ARUNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
            this.oParentObject.w_AROPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
            this.oParentObject.w_ARMOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
            this.oParentObject.w_ARTIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CADESART,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_LICODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CADESART,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP;
              from (i_cTable) where;
                  CACODICE = this.oParentObject.w_LICODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_LIDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
            this.oParentObject.w_ARCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
            this.oParentObject.w_CACODVAR = NVL(cp_ToDate(_read_.CACODVAR),cp_NullValue(_read_.CACODVAR))
            this.oParentObject.w_CAUNIMIS = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
            this.oParentObject.w_CAOPERAT = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
            this.oParentObject.w_CAMOLTIP = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARCODRIC,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARCODRIC,ARUNMIS1,ARUNMIS2,AROPERAT,ARMOLTIP;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_ARCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_ARCODRIC = NVL(cp_ToDate(_read_.ARCODRIC),cp_NullValue(_read_.ARCODRIC))
            this.oParentObject.w_ARUNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            this.oParentObject.w_ARUNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
            this.oParentObject.w_AROPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
            this.oParentObject.w_ARMOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.oParentObject.w_LICODRIC = this.oParentObject.w_ARCODRIC
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
