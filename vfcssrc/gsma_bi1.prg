* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bi1                                                        *
*              Stampa inventario                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_17]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-05                                                      *
* Last revis.: 2008-11-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bi1",oParentObject)
return(i_retval)

define class tgsma_bi1 as StdBatch
  * --- Local variables
  w_INDAZI = space(25)
  w_LOCAZI = space(35)
  w_CAPAZI = space(5)
  w_PROAZI = space(2)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_PAGINE = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa Inventario
    vq_exec(this.oParentObject.w_OQRY,this,"__tmp__")
    * --- PER NUMERAZIONE PAGINE IN TESTATA
    this.w_PAGINE = 0
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
      this.w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
      this.w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
      this.w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
      this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    L_PAGINE=0
    L_PRPALI=this.oParentObject.w_PRPALI
    L_INTLIN=this.oParentObject.w_INTLIN
    L_PREFLI=this.oParentObject.w_PREFLI
    L_INDAZI=this.w_INDAZI
    L_LOCAZI=this.w_LOCAZI
    L_CAPAZI=this.w_CAPAZI
    L_PROAZI=this.w_PROAZI
    L_COFAZI=this.w_COFAZI
    L_PIVAZI=this.w_PIVAZI
    * --- ALTRE VARIABILI DA PASSARE AL REPORT
    L_VALCAMPO=this.oParentObject.w_VALCAMPO
    L_ARTINI=this.oParentObject.w_ARTINI
    L_ARTFIN=this.oParentObject.w_ARTFIN
    L_CODCAT=this.oParentObject.w_CODCAT
    L_CODGRU=this.oParentObject.w_CODGRU
    L_CODFAM=this.oParentObject.w_CODFAM
    L_MARCA=this.oParentObject.w_MARCA
    L_CODMAG=this.oParentObject.w_CODMAG
    L_DATASTAM=this.oParentObject.w_DATASTAM
    L_NUMINV=this.oParentObject.w_NUMINV
    L_CODESE=this.oParentObject.w_CODESE
    L_DATINV=this.oParentObject.w_DATINV
    L_STAINV=this.oParentObject.w_STAINV
    L_DESINV=this.oParentObject.w_DESINV
    L_NUMPRE=this.oParentObject.w_NUMPRE
    * --- LANCIO LA STAMPA
    CP_CHPRN(this.oParentObject.w_OREP," ", this)
    * --- Aggiorno il progressivo pagine solo se stampa standard e Per Categorie Omogenee e se Attivo il check intestazione
    if (this.oParentObject.w_ONUME=1 OR this.oParentObject.w_ONUME=2) AND this.oParentObject.w_INTLIN="S"
      this.w_PAGINE = L_PAGINE+this.oParentObject.w_PRPALI
      if ah_YesNo("Aggiorno il progressivo di pagina nei dati azienda?")
        this.oParentObject.w_PRPALI = this.w_PAGINE
        * --- Try
        local bErr_03A71140
        bErr_03A71140=bTrsErr
        this.Try_03A71140()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Impossibile aggiornare ultima pagina di stampa; verificare tabella dati azienda")
        endif
        bTrsErr=bTrsErr or bErr_03A71140
        * --- End
      endif
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
  endproc
  proc Try_03A71140()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZPRPALI ="+cp_NullLink(cp_ToStrODBC(this.w_PAGINE),'AZIENDA','AZPRPALI');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZPRPALI = this.w_PAGINE;
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
