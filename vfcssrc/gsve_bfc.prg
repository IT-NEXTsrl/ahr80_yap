* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bfc                                                        *
*              Forza calcolo listini/contratt                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_66]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-09                                                      *
* Last revis.: 2013-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bfc",oParentObject,m.pOPER)
return(i_retval)

define class tgsve_bfc as StdBatch
  * --- Local variables
  w_ARTIPOPE = space(10)
  w_MVTIPOPE = space(10)
  w_ANTIPOPE = space(10)
  w_ARCATOPE = space(2)
  w_MVCATOPE = space(2)
  w_ANCATOPE = space(10)
  w_CODIVA = space(5)
  w_PUNPAD = .NULL.
  pOPER = space(1)
  w_CODCON = space(15)
  w_GEST = .NULL.
  w_OLDTIPPR = space(2)
  w_OLDTIPPR2 = space(2)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcola il Prezzo se cambiato Listino/Contratto
    * --- Forza il calcolo del Prezzo/Sconti al Cambio Listino-Contratto (da GSVE_KDA-GSAC_KDA-GSOR_KDA)
    * --- pOper per visualizzare la tripla categoria codice IVA
    if Type("pOPER")="C"
      * --- Riporto la tripla categoria codice IVA  (se applicata)
      * --- Categoria Articolo
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARTIPOPE,ARCATOPE,ARCODIVA"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARTIPOPE,ARCATOPE,ARCODIVA;
          from (i_cTable) where;
              ARCODART = this.oParentObject.w_MVCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ARTIPOPE = NVL(cp_ToDate(_read_.ARTIPOPE),cp_NullValue(_read_.ARTIPOPE))
        this.w_ARCATOPE = NVL(cp_ToDate(_read_.ARCATOPE),cp_NullValue(_read_.ARCATOPE))
        this.w_CODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Categoria Operazione
      this.w_PUNPAD = this.oParentObject.oParentObject
      this.w_MVTIPOPE = nvl(this.w_PUNPAD.w_MVTIPOPE, space(10))
      this.w_MVCATOPE = this.w_PUNPAD.w_MVCATOPE
      this.w_CODCON = iif(!Empty(this.w_PUNPAD.w_XCONORN), this.w_PUNPAD.w_XCONORN,this.w_PUNPAD.w_MVCODCON)
      * --- Categoria Cliente \ Fornitore
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANTIPOPE"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PUNPAD.w_MVTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANTIPOPE;
          from (i_cTable) where;
              ANTIPCON = this.w_PUNPAD.w_MVTIPCON;
              and ANCODICE = this.w_CODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANTIPOPE = NVL(cp_ToDate(_read_.ANTIPOPE),cp_NullValue(_read_.ANTIPOPE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- ANCATOPE ='CF'
      this.w_ANCATOPE = "CF"
      if Empty(this.w_ARTIPOPE + this.w_MVTIPOPE + this.w_ANTIPOPE)
        ah_ErrorMsg("Non esiste nessuna combinazione valida per l'attribuzione del codice IVA")
      else
        * --- Visualazzazione Tripla Codice IVA
        this.w_CODIVA = CALCODIV(this.oParentObject.w_MVCODART, this.w_PUNPAD.w_MVTIPCON, this.w_CODCON , this.w_MVTIPOPE, iif ( this.w_PUNPAD.w_MVFLVEAC="V" , this.w_PUNPAD.w_MVDATREG , this.w_PUNPAD.w_MVDATDOC), this.w_ANTIPOPE, this.w_CODIVA, this.w_ARTIPOPE)
        do GSVE_KAC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Variabile contenente il vecchio valore di MVTIPPRO
    * --- Riempio OLDTIPPR prendendo il vecchio valore direttamente dal TRS.
    *     (La variabile o_MVTIPPRO contiene gi� il nuovo valore; non pu� essere presa in considerazione)
    this.w_GEST = this.oParentObject.oParentObject
    this.w_OLDTIPPR = NVL( this.w_GEST.Get("t_MVTIPPRO") , "  ")
    this.w_OLDTIPPR2 = NVL( this.w_GEST.Get("t_MVTIPPR2") , "  ")
    * --- Azzera i Magazzino se la Causale non li Gestisce
    if !(this.oParentObject.w_MVTIPRIG="R" AND (NOT EMPTY(ALLTRIM(this.oParentObject.w_FLCASC+this.oParentObject.w_FLRISE+this.oParentObject.w_FLORDI+this.oParentObject.w_FLIMPE)) Or (this.oParentObject.w_MV_FLAGG $"+-" And Empty(this.oParentObject.w_MVSERRIF))))
      this.w_GEST.w_MVCODMAG = SPACE(5)
    endif
    if this.oParentObject.w_MVTIPRIG<>"R" OR EMPTY(ALLTRIM(this.oParentObject.w_F2CASC+this.oParentObject.w_F2RISE+this.oParentObject.w_F2ORDI+this.oParentObject.w_F2IMPE)) OR EMPTY(this.oParentObject.w_MVCAUCOL)
      this.w_GEST.w_MVCODMAT = SPACE(5)
    endif
    * --- Se cambia il tipo provvigioni...
    if (this.w_OLDTIPPR<>this.oParentObject.w_MVTIPPRO And this.oParentObject.w_MVTIPPRO $"ST-CT-ET") OR (this.w_OLDTIPPR2<>this.oParentObject.w_MVTIPPR2 And this.oParentObject.w_MVTIPPR2 $"ST-CT-ET")
      * --- Ricalcolo la percentuale provvigione da tabella provvigioni
      this.w_GEST.NotifyEvent("Calcperc1")     
    endif
    * --- Se cambio il contratto oppure il tipo provvigioni ed il nuovo tipo � da contratto..
    if this.oParentObject.w_OCONTRA<>this.oParentObject.w_MVCONTRA Or ( this.w_OLDTIPPR<>this.oParentObject.w_MVTIPPRO And this.oParentObject.w_MVTIPPRO = "CC" ) OR ( this.w_OLDTIPPR2<>this.oParentObject.w_MVTIPPR2 And this.oParentObject.w_MVTIPPR2 = "CC" ) 
      if empty(this.oParentObject.w_MVCONTRA)
        this.w_GEST.NotifyEvent("RicNoCont")     
      else
        if Not( this.oParentObject.w_OCONTRA<>this.oParentObject.w_MVCONTRA )
          * --- Contratto valorizzato, modifico tipo promozione in CC (da contratto)
          *     Imposto la variabile MODPRO dei documenti a 1 in modo che nel 
          *     GSVE_BCD venga ricalcolata solo la percentuale provvigione da contratto
          *     Se contratto non modificato non ricalcolo i prezzi
          this.w_GEST.w_MODPRO = 1
        endif
        * --- Lancio il Ricalcola GSVE_BCD('R').
        this.w_GEST.NotifyEvent("Ricalcola")     
      endif
      if Not ( this.w_OLDTIPPR<>this.oParentObject.w_MVTIPPRO And this.oParentObject.w_MVTIPPRO = "CC" )
        * --- Se Cancello il contratto sulla riga, ricalcolo senza tenere in considerazoine i contratti
        *     GSVE_BCD('Z')
        if this.oParentObject.w_MVTIPCON="C"
          * --- Solo se nelle vendite
          if this.w_GEST.w_MVPERPRO=0
            this.w_GEST.w_MVTIPPRO = IIF(g_CALPRO="GI","CT",IIF(g_CALPRO="GD","ST","DC"))
          endif
          this.w_GEST.NotifyEvent("Calcperc1")     
        endif
      else
        * --- Se il contratto � vuoto e imposto Calcolato da Contratto non rieseguo i calcoli 
        *     e imposto la percentuale a 0
        this.w_GEST.w_MVPERPRO = 0
      endif
      if Not ( this.w_OLDTIPPR2<>this.oParentObject.w_MVTIPPR2 And this.oParentObject.w_MVTIPPR2 = "CC" )
        * --- Se Cancello il contratto sulla riga, ricalcolo senza tenere in considerazoine i contratti
        *     GSVE_BCD('Z')
        if this.oParentObject.w_MVTIPCON="C"
          * --- Solo se nelle vendite
          if this.w_GEST.w_MVPROCAP=0
            this.w_GEST.w_MVTIPPR2 = IIF(g_CALPRO="GI","CT",IIF(g_CALPRO="GD","ST","DC"))
          endif
          this.w_GEST.NotifyEvent("Calcperc1")     
        endif
      else
        * --- Se il contratto � vuoto e imposto Calcolato da Contratto non rieseguo i calcoli 
        *     e imposto la percentuale a 0
        this.w_GEST.w_MVPROCAP = 0
      endif
    endif
    * --- Ricalcola Totalizzatore Spese Accessorie
    if this.oParentObject.w_OIMPACC<>this.oParentObject.w_MVIMPACC
      this.w_GEST.w_TIMPACC = this.w_GEST.w_TIMPACC - this.oParentObject.w_OIMPACC + this.oParentObject.w_MVIMPACC
    endif
    * --- Abilita variazione di una Riga Descrittiva che origina una Differita
    if this.oParentObject.w_MVTIPRIG="D" AND this.oParentObject.w_OFLEVAS="S" AND this.oParentObject.w_MVFLEVAS=" " AND this.w_GEST.w_FLGEFA="S"
      this.w_GEST.w_MVDATGEN = cp_CharToDate("  -  -  ")
    endif
    if this.oParentObject.w_OCAUMAG<>this.oParentObject.w_MVCAUMAG
      * --- Verifica disponibilit� Contabile e Fisica al cambio della causale di magazzino
      this.w_GEST.w_GSVEBCA = .T.
      this.w_GEST.NotifyEvent("Checkdisp")     
    endif
    if this.oParentObject.w_MVFLNOAN="S"
      * --- Esclude Analitica
      this.w_GEST.w_MVCODCEN = SPACE(15)
      if Not (g_COMM="S" AND this.w_GEST.w_FLGCOM="S")
        * --- Se non ho la gestione Progetti Azzero anche campi Realtivi alla Commessa
        this.w_GEST.w_MVCODCOM = SPACE(15)
        this.w_GEST.w_MVCODATT = SPACE(15)
        this.w_GEST.w_MVVOCCEN = SPACE(15)
      endif
      this.w_GEST.w_MVFLELAN = " "
      this.oParentObject.w_MVVOCCEN = SPACE(15)
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
