* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_maa                                                        *
*              Aggiornamento analitica                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_302]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-21                                                      *
* Last revis.: 2014-09-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- gsca_maa
* --- inizializza Picture
VVP=20*g_PERPVL
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsca_maa")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsca_maa")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsca_maa")
  return

* --- Class definition
define class tgsca_maa as StdPCForm
  Width  = 659
  Height = 357
  Top    = 92
  Left   = 20
  cComment = "Aggiornamento analitica"
  cPrg = "gsca_maa"
  HelpContextID=137722519
  add object cnt as tcgsca_maa
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsca_maa as PCContext
  w_MVSERIAL = space(10)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = space(8)
  w_MVCODESE = space(4)
  w_MVTIPDOC = space(5)
  w_DATOBSO = space(8)
  w_OBTEST = space(8)
  w_CPROWORD = 0
  w_MVVOCCEN = space(15)
  w_MVCODCEN = space(15)
  w_MVCODCOM = space(15)
  w_MV_SEGNO = space(1)
  w_DESVOC = space(40)
  w_DESPIA = space(40)
  w_DESCAN = space(30)
  w_MVIMPNAZ = 0
  w_MVINICOM = space(8)
  w_MVFINCOM = space(8)
  w_TOTVIS = 0
  w_SEGTOT = space(1)
  w_DESCAU = space(35)
  w_TIPATT = space(10)
  w_MVFLVEAC = space(1)
  w_MVCODATT = space(15)
  w_DESATT = space(30)
  w_CPROWNUM = 0
  w_MVNUMRIF = 0
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_TIPVOC = space(1)
  w_CODCOS = space(5)
  w_CLADOC = space(2)
  w_MVQTAMOV = 0
  w_CHKRES = space(1)
  w_VOCTIP = space(1)
  w_CODCOM = space(15)
  w_OTIPATT = space(1)
  w_CODATT = space(15)
  w_OCODCOS = space(5)
  w_VOCCEN = space(10)
  proc Save(i_oFrom)
    this.w_MVSERIAL = i_oFrom.w_MVSERIAL
    this.w_MVNUMDOC = i_oFrom.w_MVNUMDOC
    this.w_MVALFDOC = i_oFrom.w_MVALFDOC
    this.w_MVDATDOC = i_oFrom.w_MVDATDOC
    this.w_MVCODESE = i_oFrom.w_MVCODESE
    this.w_MVTIPDOC = i_oFrom.w_MVTIPDOC
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_MVVOCCEN = i_oFrom.w_MVVOCCEN
    this.w_MVCODCEN = i_oFrom.w_MVCODCEN
    this.w_MVCODCOM = i_oFrom.w_MVCODCOM
    this.w_MV_SEGNO = i_oFrom.w_MV_SEGNO
    this.w_DESVOC = i_oFrom.w_DESVOC
    this.w_DESPIA = i_oFrom.w_DESPIA
    this.w_DESCAN = i_oFrom.w_DESCAN
    this.w_MVIMPNAZ = i_oFrom.w_MVIMPNAZ
    this.w_MVINICOM = i_oFrom.w_MVINICOM
    this.w_MVFINCOM = i_oFrom.w_MVFINCOM
    this.w_TOTVIS = i_oFrom.w_TOTVIS
    this.w_SEGTOT = i_oFrom.w_SEGTOT
    this.w_DESCAU = i_oFrom.w_DESCAU
    this.w_TIPATT = i_oFrom.w_TIPATT
    this.w_MVFLVEAC = i_oFrom.w_MVFLVEAC
    this.w_MVCODATT = i_oFrom.w_MVCODATT
    this.w_DESATT = i_oFrom.w_DESATT
    this.w_CPROWNUM = i_oFrom.w_CPROWNUM
    this.w_MVNUMRIF = i_oFrom.w_MVNUMRIF
    this.w_FLANAL = i_oFrom.w_FLANAL
    this.w_FLGCOM = i_oFrom.w_FLGCOM
    this.w_TIPVOC = i_oFrom.w_TIPVOC
    this.w_CODCOS = i_oFrom.w_CODCOS
    this.w_CLADOC = i_oFrom.w_CLADOC
    this.w_MVQTAMOV = i_oFrom.w_MVQTAMOV
    this.w_CHKRES = i_oFrom.w_CHKRES
    this.w_VOCTIP = i_oFrom.w_VOCTIP
    this.w_CODCOM = i_oFrom.w_CODCOM
    this.w_OTIPATT = i_oFrom.w_OTIPATT
    this.w_CODATT = i_oFrom.w_CODATT
    this.w_OCODCOS = i_oFrom.w_OCODCOS
    this.w_VOCCEN = i_oFrom.w_VOCCEN
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MVSERIAL = this.w_MVSERIAL
    i_oTo.w_MVNUMDOC = this.w_MVNUMDOC
    i_oTo.w_MVALFDOC = this.w_MVALFDOC
    i_oTo.w_MVDATDOC = this.w_MVDATDOC
    i_oTo.w_MVCODESE = this.w_MVCODESE
    i_oTo.w_MVTIPDOC = this.w_MVTIPDOC
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_MVVOCCEN = this.w_MVVOCCEN
    i_oTo.w_MVCODCEN = this.w_MVCODCEN
    i_oTo.w_MVCODCOM = this.w_MVCODCOM
    i_oTo.w_MV_SEGNO = this.w_MV_SEGNO
    i_oTo.w_DESVOC = this.w_DESVOC
    i_oTo.w_DESPIA = this.w_DESPIA
    i_oTo.w_DESCAN = this.w_DESCAN
    i_oTo.w_MVIMPNAZ = this.w_MVIMPNAZ
    i_oTo.w_MVINICOM = this.w_MVINICOM
    i_oTo.w_MVFINCOM = this.w_MVFINCOM
    i_oTo.w_TOTVIS = this.w_TOTVIS
    i_oTo.w_SEGTOT = this.w_SEGTOT
    i_oTo.w_DESCAU = this.w_DESCAU
    i_oTo.w_TIPATT = this.w_TIPATT
    i_oTo.w_MVFLVEAC = this.w_MVFLVEAC
    i_oTo.w_MVCODATT = this.w_MVCODATT
    i_oTo.w_DESATT = this.w_DESATT
    i_oTo.w_CPROWNUM = this.w_CPROWNUM
    i_oTo.w_MVNUMRIF = this.w_MVNUMRIF
    i_oTo.w_FLANAL = this.w_FLANAL
    i_oTo.w_FLGCOM = this.w_FLGCOM
    i_oTo.w_TIPVOC = this.w_TIPVOC
    i_oTo.w_CODCOS = this.w_CODCOS
    i_oTo.w_CLADOC = this.w_CLADOC
    i_oTo.w_MVQTAMOV = this.w_MVQTAMOV
    i_oTo.w_CHKRES = this.w_CHKRES
    i_oTo.w_VOCTIP = this.w_VOCTIP
    i_oTo.w_CODCOM = this.w_CODCOM
    i_oTo.w_OTIPATT = this.w_OTIPATT
    i_oTo.w_CODATT = this.w_CODATT
    i_oTo.w_OCODCOS = this.w_OCODCOS
    i_oTo.w_VOCCEN = this.w_VOCCEN
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsca_maa as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 659
  Height = 357
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-16"
  HelpContextID=137722519
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  AGG_ANAL_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  CAN_TIER_IDX = 0
  TIP_DOCU_IDX = 0
  ATTIVITA_IDX = 0
  PNT_DETT_IDX = 0
  cFile = "AGG_ANAL"
  cKeySelect = "MVSERIAL"
  cKeyWhere  = "MVSERIAL=this.w_MVSERIAL"
  cKeyDetail  = "MVSERIAL=this.w_MVSERIAL and CPROWNUM=this.w_CPROWNUM and MVNUMRIF=this.w_MVNUMRIF"
  cKeyWhereODBC = '"MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)';

  cKeyDetailWhereODBC = '"MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and MVNUMRIF="+cp_ToStrODBC(this.w_MVNUMRIF)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"AGG_ANAL.MVSERIAL="+cp_ToStrODBC(this.w_MVSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'AGG_ANAL.CPROWORD '
  cPrg = "gsca_maa"
  cComment = "Aggiornamento analitica"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  windowtype = 1
  minbutton = .f.
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MVSERIAL = space(10)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod('  /  /  ')
  o_MVDATDOC = ctod('  /  /  ')
  w_MVCODESE = space(4)
  w_MVTIPDOC = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CPROWORD = 0
  w_MVVOCCEN = space(15)
  w_MVCODCEN = space(15)
  w_MVCODCOM = space(15)
  o_MVCODCOM = space(15)
  w_MV_SEGNO = space(1)
  w_DESVOC = space(40)
  w_DESPIA = space(40)
  w_DESCAN = space(30)
  w_MVIMPNAZ = 0
  w_MVINICOM = ctod('  /  /  ')
  o_MVINICOM = ctod('  /  /  ')
  w_MVFINCOM = ctod('  /  /  ')
  w_TOTVIS = 0
  w_SEGTOT = space(1)
  w_DESCAU = space(35)
  w_TIPATT = space(10)
  w_MVFLVEAC = space(1)
  w_MVCODATT = space(15)
  w_DESATT = space(30)
  w_CPROWNUM = 0
  o_CPROWNUM = 0
  w_MVNUMRIF = 0
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_TIPVOC = space(1)
  w_CODCOS = space(5)
  w_CLADOC = space(2)
  w_MVQTAMOV = 0
  w_CHKRES = .F.
  w_VOCTIP = space(1)
  w_CODCOM = space(15)
  w_OTIPATT = space(1)
  w_CODATT = space(15)
  w_OCODCOS = space(5)
  w_VOCCEN = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsca_maaPag1","gsca_maa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CENCOST'
    this.cWorkTables[2]='VOC_COST'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='TIP_DOCU'
    this.cWorkTables[5]='ATTIVITA'
    this.cWorkTables[6]='PNT_DETT'
    this.cWorkTables[7]='AGG_ANAL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AGG_ANAL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AGG_ANAL_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsca_maa'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_6_joined
    link_1_6_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from AGG_ANAL where MVSERIAL=KeySet.MVSERIAL
    *                            and CPROWNUM=KeySet.CPROWNUM
    *                            and MVNUMRIF=KeySet.MVNUMRIF
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.AGG_ANAL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_ANAL_IDX,2],this.bLoadRecFilter,this.AGG_ANAL_IDX,"gsca_maa")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AGG_ANAL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AGG_ANAL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AGG_ANAL '
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MVSERIAL',this.w_MVSERIAL  )
      select * from (i_cTable) AGG_ANAL where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DATOBSO = ctod("  /  /  ")
        .w_TOTVIS = 0
        .w_DESCAU = space(35)
        .w_FLANAL = space(1)
        .w_FLGCOM = space(1)
        .w_CLADOC = space(2)
        .w_CHKRES = .f.
        .w_VOCTIP = space(1)
        .w_MVSERIAL = NVL(MVSERIAL,space(10))
        .w_MVNUMDOC = NVL(MVNUMDOC,0)
        .w_MVALFDOC = NVL(MVALFDOC,space(10))
        .w_MVDATDOC = NVL(cp_ToDate(MVDATDOC),ctod("  /  /  "))
        .w_MVCODESE = NVL(MVCODESE,space(4))
        .w_MVTIPDOC = NVL(MVTIPDOC,space(5))
          if link_1_6_joined
            this.w_MVTIPDOC = NVL(TDTIPDOC106,NVL(this.w_MVTIPDOC,space(5)))
            this.w_DESCAU = NVL(TDDESDOC106,space(35))
            this.w_FLANAL = NVL(TDFLANAL106,space(1))
            this.w_FLGCOM = NVL(TDFLCOMM106,space(1))
            this.w_CLADOC = NVL(TDCATDOC106,space(2))
            this.w_VOCTIP = NVL(TDVOCECR106,space(1))
          else
          .link_1_6('Load')
          endif
        .w_OBTEST = .w_MVDATDOC
        .w_SEGTOT = .w_MV_SEGNO
        .w_MVFLVEAC = NVL(MVFLVEAC,space(1))
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'AGG_ANAL')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTVIS = 0
      scan
        with this
          .w_DESVOC = space(40)
          .w_DESPIA = space(40)
          .w_DESCAN = space(30)
          .w_DESATT = space(30)
          .w_TIPVOC = space(1)
          .w_CODCOS = space(5)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_MVVOCCEN = NVL(MVVOCCEN,space(15))
          if link_2_2_joined
            this.w_MVVOCCEN = NVL(VCCODICE202,NVL(this.w_MVVOCCEN,space(15)))
            this.w_DESVOC = NVL(VCDESCRI202,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(VCDTOBSO202),ctod("  /  /  "))
            this.w_CODCOS = NVL(VCTIPCOS202,space(5))
            this.w_TIPVOC = NVL(VCTIPVOC202,space(1))
          else
          .link_2_2('Load')
          endif
          .w_MVCODCEN = NVL(MVCODCEN,space(15))
          if link_2_3_joined
            this.w_MVCODCEN = NVL(CC_CONTO203,NVL(this.w_MVCODCEN,space(15)))
            this.w_DESPIA = NVL(CCDESPIA203,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO203),ctod("  /  /  "))
          else
          .link_2_3('Load')
          endif
          .w_MVCODCOM = NVL(MVCODCOM,space(15))
          if link_2_4_joined
            this.w_MVCODCOM = NVL(CNCODCAN204,NVL(this.w_MVCODCOM,space(15)))
            this.w_DESCAN = NVL(CNDESCAN204,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(CNDTOBSO204),ctod("  /  /  "))
          else
          .link_2_4('Load')
          endif
          .w_MV_SEGNO = NVL(MV_SEGNO,space(1))
          .w_MVIMPNAZ = NVL(MVIMPNAZ,0)
          .w_MVINICOM = NVL(cp_ToDate(MVINICOM),ctod("  /  /  "))
          .w_MVFINCOM = NVL(cp_ToDate(MVFINCOM),ctod("  /  /  "))
        .w_TIPATT = 'A'
          .w_MVCODATT = NVL(MVCODATT,space(15))
          .link_2_13('Load')
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_MVNUMRIF = NVL(MVNUMRIF,0)
          .w_MVQTAMOV = NVL(MVQTAMOV,0)
        .w_CODCOM = IIF(Empty(.w_CODCOM) OR .w_CODCOM<>.w_MVCODCOM,.w_MVCODCOM,.w_CODCOM)
        .w_OTIPATT = IIF(Empty(.w_OTIPATT) OR .w_OTIPATT<>.w_TIPATT,.w_TIPATT,.w_OTIPATT)
        .w_CODATT = IIF(Empty(.w_CODATT) OR .w_CODATT<>.w_MVCODATT,.w_MVCODATT,.w_CODATT)
        .w_OCODCOS = IIF(Empty(.w_OCODCOS) OR .w_OCODCOS<>.w_CODCOS,.w_CODCOS,.w_OCODCOS)
        .w_VOCCEN = IIF(Empty(.w_VOCCEN) OR .w_VOCCEN<>.w_MVVOCCEN,.w_MVVOCCEN,.w_VOCCEN)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTVIS = .w_TOTVIS+.w_MVIMPNAZ
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_OBTEST = .w_MVDATDOC
        .w_SEGTOT = .w_MV_SEGNO
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MVSERIAL=space(10)
      .w_MVNUMDOC=0
      .w_MVALFDOC=space(10)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_MVCODESE=space(4)
      .w_MVTIPDOC=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_CPROWORD=10
      .w_MVVOCCEN=space(15)
      .w_MVCODCEN=space(15)
      .w_MVCODCOM=space(15)
      .w_MV_SEGNO=space(1)
      .w_DESVOC=space(40)
      .w_DESPIA=space(40)
      .w_DESCAN=space(30)
      .w_MVIMPNAZ=0
      .w_MVINICOM=ctod("  /  /  ")
      .w_MVFINCOM=ctod("  /  /  ")
      .w_TOTVIS=0
      .w_SEGTOT=space(1)
      .w_DESCAU=space(35)
      .w_TIPATT=space(10)
      .w_MVFLVEAC=space(1)
      .w_MVCODATT=space(15)
      .w_DESATT=space(30)
      .w_CPROWNUM=0
      .w_MVNUMRIF=0
      .w_FLANAL=space(1)
      .w_FLGCOM=space(1)
      .w_TIPVOC=space(1)
      .w_CODCOS=space(5)
      .w_CLADOC=space(2)
      .w_MVQTAMOV=0
      .w_CHKRES=.f.
      .w_VOCTIP=space(1)
      .w_CODCOM=space(15)
      .w_OTIPATT=space(1)
      .w_CODATT=space(15)
      .w_OCODCOS=space(5)
      .w_VOCCEN=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,6,.f.)
        if not(empty(.w_MVTIPDOC))
         .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        .w_OBTEST = .w_MVDATDOC
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_MVVOCCEN))
         .link_2_2('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_MVCODCEN))
         .link_2_3('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_MVCODCOM))
         .link_2_4('Full')
        endif
        .DoRTCalc(13,18,.f.)
        .w_MVFINCOM = iif(empty(.w_mvfincom)and not empty(.w_mvinicom),.w_mvinicom,iif(not empty(.w_MVFINCOM)and empty(.w_MVINICOM),.w_mvinicom,.w_mvfincom))
        .DoRTCalc(20,20,.f.)
        .w_SEGTOT = .w_MV_SEGNO
        .DoRTCalc(22,22,.f.)
        .w_TIPATT = 'A'
        .DoRTCalc(24,24,.f.)
        .w_MVCODATT = SPACE(15)
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_MVCODATT))
         .link_2_13('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .DoRTCalc(26,33,.f.)
        .w_MVQTAMOV = 0
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(35,36,.f.)
        .w_CODCOM = IIF(Empty(.w_CODCOM) OR .w_CODCOM<>.w_MVCODCOM,.w_MVCODCOM,.w_CODCOM)
        .w_OTIPATT = IIF(Empty(.w_OTIPATT) OR .w_OTIPATT<>.w_TIPATT,.w_TIPATT,.w_OTIPATT)
        .w_CODATT = IIF(Empty(.w_CODATT) OR .w_CODATT<>.w_MVCODATT,.w_MVCODATT,.w_CODATT)
        .w_OCODCOS = IIF(Empty(.w_OCODCOS) OR .w_OCODCOS<>.w_CODCOS,.w_CODCOS,.w_OCODCOS)
        .w_VOCCEN = IIF(Empty(.w_VOCCEN) OR .w_VOCCEN<>.w_MVVOCCEN,.w_MVVOCCEN,.w_VOCCEN)
      endif
    endwith
    cp_BlankRecExtFlds(this,'AGG_ANAL')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oMVINICOM_2_10.enabled = i_bVal
      .Page1.oPag.oMVFINCOM_2_11.enabled = i_bVal
      .Page1.oPag.oMVCODATT_2_13.enabled = i_bVal
      .Page1.oPag.oObj_1_25.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'AGG_ANAL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AGG_ANAL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MVSERIAL,"MVSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MVNUMDOC,"MVNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MVALFDOC,"MVALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MVDATDOC,"MVDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MVCODESE,"MVCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MVTIPDOC,"MVTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MVFLVEAC,"MVFLVEAC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_MVVOCCEN C(15);
      ,t_MVCODCEN C(15);
      ,t_MVCODCOM C(15);
      ,t_MV_SEGNO C(1);
      ,t_DESVOC C(40);
      ,t_DESPIA C(40);
      ,t_DESCAN C(30);
      ,t_MVIMPNAZ N(18,4);
      ,t_MVINICOM D(8);
      ,t_MVFINCOM D(8);
      ,t_MVCODATT C(15);
      ,t_DESATT C(30);
      ,CPROWNUM N(10);
      ,t_TIPATT C(10);
      ,t_CPROWNUM N(4);
      ,t_MVNUMRIF N(3);
      ,t_TIPVOC C(1);
      ,t_CODCOS C(5);
      ,t_MVQTAMOV N(12,3);
      ,t_CODCOM C(15);
      ,t_OTIPATT C(1);
      ,t_CODATT C(15);
      ,t_OCODCOS C(5);
      ,t_VOCCEN C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsca_maabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMVVOCCEN_2_2.controlsource=this.cTrsName+'.t_MVVOCCEN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODCEN_2_3.controlsource=this.cTrsName+'.t_MVCODCEN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODCOM_2_4.controlsource=this.cTrsName+'.t_MVCODCOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMV_SEGNO_2_5.controlsource=this.cTrsName+'.t_MV_SEGNO'
    this.oPgFRm.Page1.oPag.oDESVOC_2_6.controlsource=this.cTrsName+'.t_DESVOC'
    this.oPgFRm.Page1.oPag.oDESPIA_2_7.controlsource=this.cTrsName+'.t_DESPIA'
    this.oPgFRm.Page1.oPag.oDESCAN_2_8.controlsource=this.cTrsName+'.t_DESCAN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMVIMPNAZ_2_9.controlsource=this.cTrsName+'.t_MVIMPNAZ'
    this.oPgFRm.Page1.oPag.oMVINICOM_2_10.controlsource=this.cTrsName+'.t_MVINICOM'
    this.oPgFRm.Page1.oPag.oMVFINCOM_2_11.controlsource=this.cTrsName+'.t_MVFINCOM'
    this.oPgFRm.Page1.oPag.oMVCODATT_2_13.controlsource=this.cTrsName+'.t_MVCODATT'
    this.oPgFRm.Page1.oPag.oDESATT_2_14.controlsource=this.cTrsName+'.t_DESATT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(38)
    this.AddVLine(176)
    this.AddVLine(315)
    this.AddVLine(456)
    this.AddVLine(491)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.AGG_ANAL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_ANAL_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AGG_ANAL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGG_ANAL_IDX,2])
      *
      * insert into AGG_ANAL
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AGG_ANAL')
        i_extval=cp_InsertValODBCExtFlds(this,'AGG_ANAL')
        i_cFldBody=" "+;
                  "(MVSERIAL,MVNUMDOC,MVALFDOC,MVDATDOC,MVCODESE"+;
                  ",MVTIPDOC,CPROWORD,MVVOCCEN,MVCODCEN,MVCODCOM"+;
                  ",MV_SEGNO,MVIMPNAZ,MVINICOM,MVFINCOM,MVFLVEAC"+;
                  ",MVCODATT,MVNUMRIF,MVQTAMOV,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MVSERIAL)+","+cp_ToStrODBC(this.w_MVNUMDOC)+","+cp_ToStrODBC(this.w_MVALFDOC)+","+cp_ToStrODBC(this.w_MVDATDOC)+","+cp_ToStrODBC(this.w_MVCODESE)+;
             ","+cp_ToStrODBCNull(this.w_MVTIPDOC)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_MVVOCCEN)+","+cp_ToStrODBCNull(this.w_MVCODCEN)+","+cp_ToStrODBCNull(this.w_MVCODCOM)+;
             ","+cp_ToStrODBC(this.w_MV_SEGNO)+","+cp_ToStrODBC(this.w_MVIMPNAZ)+","+cp_ToStrODBC(this.w_MVINICOM)+","+cp_ToStrODBC(this.w_MVFINCOM)+","+cp_ToStrODBC(this.w_MVFLVEAC)+;
             ","+cp_ToStrODBCNull(this.w_MVCODATT)+","+cp_ToStrODBC(this.w_MVNUMRIF)+","+cp_ToStrODBC(this.w_MVQTAMOV)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AGG_ANAL')
        i_extval=cp_InsertValVFPExtFlds(this,'AGG_ANAL')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MVSERIAL',this.w_MVSERIAL,'CPROWNUM',this.w_CPROWNUM,'MVNUMRIF',this.w_MVNUMRIF)
        INSERT INTO (i_cTable) (;
                   MVSERIAL;
                  ,MVNUMDOC;
                  ,MVALFDOC;
                  ,MVDATDOC;
                  ,MVCODESE;
                  ,MVTIPDOC;
                  ,CPROWORD;
                  ,MVVOCCEN;
                  ,MVCODCEN;
                  ,MVCODCOM;
                  ,MV_SEGNO;
                  ,MVIMPNAZ;
                  ,MVINICOM;
                  ,MVFINCOM;
                  ,MVFLVEAC;
                  ,MVCODATT;
                  ,MVNUMRIF;
                  ,MVQTAMOV;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MVSERIAL;
                  ,this.w_MVNUMDOC;
                  ,this.w_MVALFDOC;
                  ,this.w_MVDATDOC;
                  ,this.w_MVCODESE;
                  ,this.w_MVTIPDOC;
                  ,this.w_CPROWORD;
                  ,this.w_MVVOCCEN;
                  ,this.w_MVCODCEN;
                  ,this.w_MVCODCOM;
                  ,this.w_MV_SEGNO;
                  ,this.w_MVIMPNAZ;
                  ,this.w_MVINICOM;
                  ,this.w_MVFINCOM;
                  ,this.w_MVFLVEAC;
                  ,this.w_MVCODATT;
                  ,this.w_MVNUMRIF;
                  ,this.w_MVQTAMOV;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.AGG_ANAL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGG_ANAL_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CPROWORD<>0 AND t_MVQTAMOV<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'AGG_ANAL')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " MVNUMDOC="+cp_ToStrODBC(this.w_MVNUMDOC)+;
                 ",MVALFDOC="+cp_ToStrODBC(this.w_MVALFDOC)+;
                 ",MVDATDOC="+cp_ToStrODBC(this.w_MVDATDOC)+;
                 ",MVCODESE="+cp_ToStrODBC(this.w_MVCODESE)+;
                 ",MVTIPDOC="+cp_ToStrODBCNull(this.w_MVTIPDOC)+;
                 ",MVFLVEAC="+cp_ToStrODBC(this.w_MVFLVEAC)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'AGG_ANAL')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  MVNUMDOC=this.w_MVNUMDOC;
                 ,MVALFDOC=this.w_MVALFDOC;
                 ,MVDATDOC=this.w_MVDATDOC;
                 ,MVCODESE=this.w_MVCODESE;
                 ,MVTIPDOC=this.w_MVTIPDOC;
                 ,MVFLVEAC=this.w_MVFLVEAC;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0 AND t_MVQTAMOV<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update AGG_ANAL
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'AGG_ANAL')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MVNUMDOC="+cp_ToStrODBC(this.w_MVNUMDOC)+;
                     ",MVALFDOC="+cp_ToStrODBC(this.w_MVALFDOC)+;
                     ",MVDATDOC="+cp_ToStrODBC(this.w_MVDATDOC)+;
                     ",MVCODESE="+cp_ToStrODBC(this.w_MVCODESE)+;
                     ",MVTIPDOC="+cp_ToStrODBCNull(this.w_MVTIPDOC)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",MVVOCCEN="+cp_ToStrODBCNull(this.w_MVVOCCEN)+;
                     ",MVCODCEN="+cp_ToStrODBCNull(this.w_MVCODCEN)+;
                     ",MVCODCOM="+cp_ToStrODBCNull(this.w_MVCODCOM)+;
                     ",MV_SEGNO="+cp_ToStrODBC(this.w_MV_SEGNO)+;
                     ",MVIMPNAZ="+cp_ToStrODBC(this.w_MVIMPNAZ)+;
                     ",MVINICOM="+cp_ToStrODBC(this.w_MVINICOM)+;
                     ",MVFINCOM="+cp_ToStrODBC(this.w_MVFINCOM)+;
                     ",MVFLVEAC="+cp_ToStrODBC(this.w_MVFLVEAC)+;
                     ",MVCODATT="+cp_ToStrODBCNull(this.w_MVCODATT)+;
                     ",MVQTAMOV="+cp_ToStrODBC(this.w_MVQTAMOV)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'AGG_ANAL')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MVNUMDOC=this.w_MVNUMDOC;
                     ,MVALFDOC=this.w_MVALFDOC;
                     ,MVDATDOC=this.w_MVDATDOC;
                     ,MVCODESE=this.w_MVCODESE;
                     ,MVTIPDOC=this.w_MVTIPDOC;
                     ,CPROWORD=this.w_CPROWORD;
                     ,MVVOCCEN=this.w_MVVOCCEN;
                     ,MVCODCEN=this.w_MVCODCEN;
                     ,MVCODCOM=this.w_MVCODCOM;
                     ,MV_SEGNO=this.w_MV_SEGNO;
                     ,MVIMPNAZ=this.w_MVIMPNAZ;
                     ,MVINICOM=this.w_MVINICOM;
                     ,MVFINCOM=this.w_MVFINCOM;
                     ,MVFLVEAC=this.w_MVFLVEAC;
                     ,MVCODATT=this.w_MVCODATT;
                     ,MVQTAMOV=this.w_MVQTAMOV;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AGG_ANAL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGG_ANAL_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 AND t_MVQTAMOV<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete AGG_ANAL
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 AND t_MVQTAMOV<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AGG_ANAL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGG_ANAL_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
          .link_1_6('Full')
        .DoRTCalc(7,7,.t.)
        if .o_MVDATDOC<>.w_MVDATDOC
          .w_OBTEST = .w_MVDATDOC
        endif
        .DoRTCalc(9,18,.t.)
        if .o_MVINICOM<>.w_MVINICOM
          .w_MVFINCOM = iif(empty(.w_mvfincom)and not empty(.w_mvinicom),.w_mvinicom,iif(not empty(.w_MVFINCOM)and empty(.w_MVINICOM),.w_mvinicom,.w_mvfincom))
        endif
        .DoRTCalc(20,20,.t.)
          .w_SEGTOT = .w_MV_SEGNO
        .DoRTCalc(22,22,.t.)
          .w_TIPATT = 'A'
        .DoRTCalc(24,24,.t.)
        if .o_MVCODCOM<>.w_MVCODCOM
          .w_MVCODATT = SPACE(15)
          .link_2_13('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(26,36,.t.)
        if .o_CPROWNUM<>.w_CPROWNUM
          .w_CODCOM = IIF(Empty(.w_CODCOM) OR .w_CODCOM<>.w_MVCODCOM,.w_MVCODCOM,.w_CODCOM)
        endif
        if .o_CPROWNUM<>.w_CPROWNUM
          .w_OTIPATT = IIF(Empty(.w_OTIPATT) OR .w_OTIPATT<>.w_TIPATT,.w_TIPATT,.w_OTIPATT)
        endif
        if .o_CPROWNUM<>.w_CPROWNUM
          .w_CODATT = IIF(Empty(.w_CODATT) OR .w_CODATT<>.w_MVCODATT,.w_MVCODATT,.w_CODATT)
        endif
        if .o_CPROWNUM<>.w_CPROWNUM
          .w_OCODCOS = IIF(Empty(.w_OCODCOS) OR .w_OCODCOS<>.w_CODCOS,.w_CODCOS,.w_OCODCOS)
        endif
        if .o_CPROWNUM<>.w_CPROWNUM
          .w_VOCCEN = IIF(Empty(.w_VOCCEN) OR .w_VOCCEN<>.w_MVVOCCEN,.w_MVVOCCEN,.w_VOCCEN)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TIPATT with this.w_TIPATT
      replace t_CPROWNUM with this.w_CPROWNUM
      replace t_MVNUMRIF with this.w_MVNUMRIF
      replace t_TIPVOC with this.w_TIPVOC
      replace t_CODCOS with this.w_CODCOS
      replace t_MVQTAMOV with this.w_MVQTAMOV
      replace t_CODCOM with this.w_CODCOM
      replace t_OTIPATT with this.w_OTIPATT
      replace t_CODATT with this.w_CODATT
      replace t_OCODCOS with this.w_OCODCOS
      replace t_VOCCEN with this.w_VOCCEN
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVVOCCEN_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVVOCCEN_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVCODCEN_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVCODCEN_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVCODCOM_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMVCODCOM_2_4.mCond()
    this.oPgFrm.Page1.oPag.oMVFINCOM_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMVFINCOM_2_11.mCond()
    this.oPgFrm.Page1.oPag.oMVCODATT_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oMVCODATT_2_13.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oMVCODATT_2_13.visible=!this.oPgFrm.Page1.oPag.oMVCODATT_2_13.mHide()
    this.oPgFrm.Page1.oPag.oDESATT_2_14.visible=!this.oPgFrm.Page1.oPag.oDESATT_2_14.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MVTIPDOC
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLANAL,TDFLCOMM,TDCATDOC,TDVOCECR";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MVTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MVTIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLANAL,TDFLCOMM,TDCATDOC,TDVOCECR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAU = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLANAL = NVL(_Link_.TDFLANAL,space(1))
      this.w_FLGCOM = NVL(_Link_.TDFLCOMM,space(1))
      this.w_CLADOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_VOCTIP = NVL(_Link_.TDVOCECR,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPDOC = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_FLANAL = space(1)
      this.w_FLGCOM = space(1)
      this.w_CLADOC = space(2)
      this.w_VOCTIP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.TDTIPDOC as TDTIPDOC106"+ ",link_1_6.TDDESDOC as TDDESDOC106"+ ",link_1_6.TDFLANAL as TDFLANAL106"+ ",link_1_6.TDFLCOMM as TDFLCOMM106"+ ",link_1_6.TDCATDOC as TDCATDOC106"+ ",link_1_6.TDVOCECR as TDVOCECR106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on AGG_ANAL.MVTIPDOC=link_1_6.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and AGG_ANAL.MVTIPDOC=link_1_6.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MVVOCCEN
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVVOCCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_MVVOCCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS,VCTIPVOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_MVVOCCEN))
          select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS,VCTIPVOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVVOCCEN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStrODBC(trim(this.w_MVVOCCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS,VCTIPVOC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VCDESCRI like "+cp_ToStr(trim(this.w_MVVOCCEN)+"%");

            select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS,VCTIPVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVVOCCEN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oMVVOCCEN_2_2'),i_cWhere,'GSCA_AVC',"Voci di costo o ricavo",'GSVE_KDA.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS,VCTIPVOC";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS,VCTIPVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVVOCCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS,VCTIPVOC";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_MVVOCCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_MVVOCCEN)
            select VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS,VCTIPVOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVVOCCEN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(5))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVVOCCEN = space(15)
      endif
      this.w_DESVOC = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CODCOS = space(5)
      this.w_TIPVOC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVOC=.w_VOCTIP AND CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Voce di Costo obsoleta!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MVVOCCEN = space(15)
        this.w_DESVOC = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CODCOS = space(5)
        this.w_TIPVOC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVVOCCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_COST_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.VCCODICE as VCCODICE202"+ ",link_2_2.VCDESCRI as VCDESCRI202"+ ",link_2_2.VCDTOBSO as VCDTOBSO202"+ ",link_2_2.VCTIPCOS as VCTIPCOS202"+ ",link_2_2.VCTIPVOC as VCTIPVOC202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on AGG_ANAL.MVVOCCEN=link_2_2.VCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and AGG_ANAL.MVVOCCEN=link_2_2.VCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MVCODCEN
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_MVCODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_MVCODCEN))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_MVCODCEN)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_MVCODCEN)+"%");

            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCODCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oMVCODCEN_2_3'),i_cWhere,'GSCA_ACC',"Centri di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_MVCODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_MVCODCEN)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESPIA = NVL(_Link_.CCDESPIA,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODCEN = space(15)
      endif
      this.w_DESPIA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MVCODCEN = space(15)
        this.w_DESPIA = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CENCOST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.CC_CONTO as CC_CONTO203"+ ",link_2_3.CCDESPIA as CCDESPIA203"+ ",link_2_3.CCDTOBSO as CCDTOBSO203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on AGG_ANAL.MVCODCEN=link_2_3.CC_CONTO"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and AGG_ANAL.MVCODCEN=link_2_3.CC_CONTO(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MVCODCOM
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_MVCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_MVCODCOM))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_MVCODCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_MVCODCOM)+"%");

            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oMVCODCOM_2_4'),i_cWhere,'GSAR_ACN',"Elenco commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_MVCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_MVCODCOM)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODCOM = space(15)
      endif
      this.w_DESCAN = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa inesistente oppure obsoleto")
        endif
        this.w_MVCODCOM = space(15)
        this.w_DESCAN = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.CNCODCAN as CNCODCAN204"+ ",link_2_4.CNDESCAN as CNDESCAN204"+ ",link_2_4.CNDTOBSO as CNDTOBSO204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on AGG_ANAL.MVCODCOM=link_2_4.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and AGG_ANAL.MVCODCOM=link_2_4.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MVCODATT
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_MVCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_MVCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_MVCODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_MVCODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_MVCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_MVCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_MVCODATT)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_MVCODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MVCODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oMVCODATT_2_13'),i_cWhere,'GSPC_BZZ',"Elenco attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVCODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_MVCODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_MVCODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_MVCODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_MVCODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_MVCODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMVNUMDOC_1_2.value==this.w_MVNUMDOC)
      this.oPgFrm.Page1.oPag.oMVNUMDOC_1_2.value=this.w_MVNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVALFDOC_1_3.value==this.w_MVALFDOC)
      this.oPgFrm.Page1.oPag.oMVALFDOC_1_3.value=this.w_MVALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATDOC_1_4.value==this.w_MVDATDOC)
      this.oPgFrm.Page1.oPag.oMVDATDOC_1_4.value=this.w_MVDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODESE_1_5.value==this.w_MVCODESE)
      this.oPgFrm.Page1.oPag.oMVCODESE_1_5.value=this.w_MVCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oMVTIPDOC_1_6.value==this.w_MVTIPDOC)
      this.oPgFrm.Page1.oPag.oMVTIPDOC_1_6.value=this.w_MVTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVOC_2_6.value==this.w_DESVOC)
      this.oPgFrm.Page1.oPag.oDESVOC_2_6.value=this.w_DESVOC
      replace t_DESVOC with this.oPgFrm.Page1.oPag.oDESVOC_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIA_2_7.value==this.w_DESPIA)
      this.oPgFrm.Page1.oPag.oDESPIA_2_7.value=this.w_DESPIA
      replace t_DESPIA with this.oPgFrm.Page1.oPag.oDESPIA_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_2_8.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_2_8.value=this.w_DESCAN
      replace t_DESCAN with this.oPgFrm.Page1.oPag.oDESCAN_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMVINICOM_2_10.value==this.w_MVINICOM)
      this.oPgFrm.Page1.oPag.oMVINICOM_2_10.value=this.w_MVINICOM
      replace t_MVINICOM with this.oPgFrm.Page1.oPag.oMVINICOM_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oMVFINCOM_2_11.value==this.w_MVFINCOM)
      this.oPgFrm.Page1.oPag.oMVFINCOM_2_11.value=this.w_MVFINCOM
      replace t_MVFINCOM with this.oPgFrm.Page1.oPag.oMVFINCOM_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTVIS_3_1.value==this.w_TOTVIS)
      this.oPgFrm.Page1.oPag.oTOTVIS_3_1.value=this.w_TOTVIS
    endif
    if not(this.oPgFrm.Page1.oPag.oSEGTOT_3_2.value==this.w_SEGTOT)
      this.oPgFrm.Page1.oPag.oSEGTOT_3_2.value=this.w_SEGTOT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_19.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_19.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oMVCODATT_2_13.value==this.w_MVCODATT)
      this.oPgFrm.Page1.oPag.oMVCODATT_2_13.value=this.w_MVCODATT
      replace t_MVCODATT with this.oPgFrm.Page1.oPag.oMVCODATT_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_2_14.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_2_14.value=this.w_DESATT
      replace t_DESATT with this.oPgFrm.Page1.oPag.oDESATT_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVVOCCEN_2_2.value==this.w_MVVOCCEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVVOCCEN_2_2.value=this.w_MVVOCCEN
      replace t_MVVOCCEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVVOCCEN_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODCEN_2_3.value==this.w_MVCODCEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODCEN_2_3.value=this.w_MVCODCEN
      replace t_MVCODCEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODCEN_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODCOM_2_4.value==this.w_MVCODCOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODCOM_2_4.value=this.w_MVCODCOM
      replace t_MVCODCOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODCOM_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMV_SEGNO_2_5.value==this.w_MV_SEGNO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMV_SEGNO_2_5.value=this.w_MV_SEGNO
      replace t_MV_SEGNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMV_SEGNO_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVIMPNAZ_2_9.value==this.w_MVIMPNAZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVIMPNAZ_2_9.value=this.w_MVIMPNAZ
      replace t_MVIMPNAZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVIMPNAZ_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'AGG_ANAL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsca_maa
      * --- Lancia Batch per verifica congruenza dati...
      if i_bRes
          * --- Controlli Finali
          This.w_CHKRES=.t.
          this.NotifyEvent('ControlliFinali')
          i_bRes = This.w_CHKRES
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_TIPVOC=.w_VOCTIP AND CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Voce di Costo obsoleta!",.F.)) and (g_PERCCR='S' AND .w_FLANAL='S') and not(empty(.w_MVVOCCEN)) and (.w_CPROWORD<>0 AND .w_MVQTAMOV<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVVOCCEN_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Centro di Ricavo Obsoleto!",.F.)) and (g_PERCCR='S' AND .w_FLANAL='S') and not(empty(.w_MVCODCEN)) and (.w_CPROWORD<>0 AND .w_MVQTAMOV<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODCEN_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
        case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Commessa obsoleto!",.F.)) and (((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S'))) and not(empty(.w_MVCODCOM)) and (.w_CPROWORD<>0 AND .w_MVQTAMOV<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMVCODCOM_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice commessa inesistente oppure obsoleto")
        case   (empty(.w_MVFINCOM) or not(.w_mvinicom<=.w_mvfincom)) and (not empty(.w_mvinicom)) and (.w_CPROWORD<>0 AND .w_MVQTAMOV<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oMVFINCOM_2_11
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Data fine competenza incongruente")
      endcase
      if .w_CPROWORD<>0 AND .w_MVQTAMOV<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MVDATDOC = this.w_MVDATDOC
    this.o_MVCODCOM = this.w_MVCODCOM
    this.o_MVINICOM = this.w_MVINICOM
    this.o_CPROWNUM = this.w_CPROWNUM
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 AND t_MVQTAMOV<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_MVVOCCEN=space(15)
      .w_MVCODCEN=space(15)
      .w_MVCODCOM=space(15)
      .w_MV_SEGNO=space(1)
      .w_DESVOC=space(40)
      .w_DESPIA=space(40)
      .w_DESCAN=space(30)
      .w_MVIMPNAZ=0
      .w_MVINICOM=ctod("  /  /  ")
      .w_MVFINCOM=ctod("  /  /  ")
      .w_TIPATT=space(10)
      .w_MVCODATT=space(15)
      .w_DESATT=space(30)
      .w_MVNUMRIF=0
      .w_TIPVOC=space(1)
      .w_CODCOS=space(5)
      .w_MVQTAMOV=0
      .w_CODCOM=space(15)
      .w_OTIPATT=space(1)
      .w_CODATT=space(15)
      .w_OCODCOS=space(5)
      .w_VOCCEN=space(10)
      .DoRTCalc(1,10,.f.)
      if not(empty(.w_MVVOCCEN))
        .link_2_2('Full')
      endif
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_MVCODCEN))
        .link_2_3('Full')
      endif
      .DoRTCalc(12,12,.f.)
      if not(empty(.w_MVCODCOM))
        .link_2_4('Full')
      endif
      .DoRTCalc(13,18,.f.)
        .w_MVFINCOM = iif(empty(.w_mvfincom)and not empty(.w_mvinicom),.w_mvinicom,iif(not empty(.w_MVFINCOM)and empty(.w_MVINICOM),.w_mvinicom,.w_mvfincom))
      .DoRTCalc(20,22,.f.)
        .w_TIPATT = 'A'
      .DoRTCalc(24,24,.f.)
        .w_MVCODATT = SPACE(15)
      .DoRTCalc(25,25,.f.)
      if not(empty(.w_MVCODATT))
        .link_2_13('Full')
      endif
      .DoRTCalc(26,33,.f.)
        .w_MVQTAMOV = 0
      .DoRTCalc(35,36,.f.)
        .w_CODCOM = IIF(Empty(.w_CODCOM) OR .w_CODCOM<>.w_MVCODCOM,.w_MVCODCOM,.w_CODCOM)
        .w_OTIPATT = IIF(Empty(.w_OTIPATT) OR .w_OTIPATT<>.w_TIPATT,.w_TIPATT,.w_OTIPATT)
        .w_CODATT = IIF(Empty(.w_CODATT) OR .w_CODATT<>.w_MVCODATT,.w_MVCODATT,.w_CODATT)
        .w_OCODCOS = IIF(Empty(.w_OCODCOS) OR .w_OCODCOS<>.w_CODCOS,.w_CODCOS,.w_OCODCOS)
        .w_VOCCEN = IIF(Empty(.w_VOCCEN) OR .w_VOCCEN<>.w_MVVOCCEN,.w_MVVOCCEN,.w_VOCCEN)
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_MVVOCCEN = t_MVVOCCEN
    this.w_MVCODCEN = t_MVCODCEN
    this.w_MVCODCOM = t_MVCODCOM
    this.w_MV_SEGNO = t_MV_SEGNO
    this.w_DESVOC = t_DESVOC
    this.w_DESPIA = t_DESPIA
    this.w_DESCAN = t_DESCAN
    this.w_MVIMPNAZ = t_MVIMPNAZ
    this.w_MVINICOM = t_MVINICOM
    this.w_MVFINCOM = t_MVFINCOM
    this.w_TIPATT = t_TIPATT
    this.w_MVCODATT = t_MVCODATT
    this.w_DESATT = t_DESATT
    this.w_CPROWNUM = t_CPROWNUM
    this.w_MVNUMRIF = t_MVNUMRIF
    this.w_TIPVOC = t_TIPVOC
    this.w_CODCOS = t_CODCOS
    this.w_MVQTAMOV = t_MVQTAMOV
    this.w_CODCOM = t_CODCOM
    this.w_OTIPATT = t_OTIPATT
    this.w_CODATT = t_CODATT
    this.w_OCODCOS = t_OCODCOS
    this.w_VOCCEN = t_VOCCEN
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_MVVOCCEN with this.w_MVVOCCEN
    replace t_MVCODCEN with this.w_MVCODCEN
    replace t_MVCODCOM with this.w_MVCODCOM
    replace t_MV_SEGNO with this.w_MV_SEGNO
    replace t_DESVOC with this.w_DESVOC
    replace t_DESPIA with this.w_DESPIA
    replace t_DESCAN with this.w_DESCAN
    replace t_MVIMPNAZ with this.w_MVIMPNAZ
    replace t_MVINICOM with this.w_MVINICOM
    replace t_MVFINCOM with this.w_MVFINCOM
    replace t_TIPATT with this.w_TIPATT
    replace t_MVCODATT with this.w_MVCODATT
    replace t_DESATT with this.w_DESATT
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_MVNUMRIF with this.w_MVNUMRIF
    replace t_TIPVOC with this.w_TIPVOC
    replace t_CODCOS with this.w_CODCOS
    replace t_MVQTAMOV with this.w_MVQTAMOV
    replace t_CODCOM with this.w_CODCOM
    replace t_OTIPATT with this.w_OTIPATT
    replace t_CODATT with this.w_CODATT
    replace t_OCODCOS with this.w_OCODCOS
    replace t_VOCCEN with this.w_VOCCEN
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTVIS = .w_TOTVIS-.w_mvimpnaz
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsca_maaPag1 as StdContainer
  Width  = 655
  height = 357
  stdWidth  = 655
  stdheight = 357
  resizeXpos=289
  resizeYpos=171
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVNUMDOC_1_2 as StdField with uid="JKOUXNVKKR",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MVNUMDOC", cQueryName = "MVNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 245385719,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=93, Top=13, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oMVALFDOC_1_3 as StdField with uid="XFAJNVOPHK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MVALFDOC", cQueryName = "MVALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 253368823,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=220, Top=13, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oMVDATDOC_1_4 as StdField with uid="XWQZPYPKNO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MVDATDOC", cQueryName = "MVDATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 239397367,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=354, Top=13

  add object oMVCODESE_1_5 as StdField with uid="ZBBCFXCMJE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MVCODESE", cQueryName = "MVCODESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio",;
    HelpContextID = 238483957,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=520, Top=13, InputMask=replicate('X',4)

  add object oMVTIPDOC_1_6 as StdField with uid="VJQURHHZTR",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MVTIPDOC", cQueryName = "MVTIPDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale documento",;
    HelpContextID = 243001847,;
   bGlobalFont=.t.,;
    Height=21, Width=77, Left=97, Top=37, InputMask=replicate('X',5), cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MVTIPDOC"

  func oMVTIPDOC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCAU_1_19 as StdField with uid="TWRUBQVUXL",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale",;
    HelpContextID = 242355146,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=176, Top=37, InputMask=replicate('X',35)


  add object oObj_1_25 as cp_runprogram with uid="BEQPUZMCSQ",left=9, top=359, width=172,height=23,;
    caption='GSCA_BKA',;
   bGlobalFont=.t.,;
    prg="GSCA_BKA",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 7013031


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=62, width=645,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Riga",Field2="MVVOCCEN",Label2="Voce costo/ricavo:",Field3="MVCODCEN",Label3="Centro di costo/ricavo",Field4="MVCODCOM",Label4="Commessa",Field5="MV_SEGNO",Label5="D/A",Field6="MVIMPNAZ",Label6="Importo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185377914

  add object oStr_1_7 as StdString with uid="XEIUJDRVSN",Visible=.t., Left=394, Top=245,;
    Alignment=1, Width=62, Height=15,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="JHLAMNJFHA",Visible=.t., Left=345, Top=303,;
    Alignment=1, Width=103, Height=15,;
    Caption="Competenza dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="RDKQALNQMV",Visible=.t., Left=532, Top=303,;
    Alignment=1, Width=20, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="NYESSGNSRO",Visible=.t., Left=5, Top=245,;
    Alignment=1, Width=93, Height=15,;
    Caption="Voce di C./R.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="QZHZTDZVIH",Visible=.t., Left=8, Top=272,;
    Alignment=1, Width=90, Height=15,;
    Caption="Centro di C./R.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ZSKYONBFTD",Visible=.t., Left=9, Top=299,;
    Alignment=1, Width=90, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="MDBCYMZTKN",Visible=.t., Left=17, Top=14,;
    Alignment=1, Width=72, Height=18,;
    Caption="Num. doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="ATJFSXZBVD",Visible=.t., Left=327, Top=14,;
    Alignment=1, Width=23, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="CKFXSXUBEU",Visible=.t., Left=449, Top=14,;
    Alignment=1, Width=66, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="DDSMQCWCMF",Visible=.t., Left=212, Top=13,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="KXECHFXVTT",Visible=.t., Left=8, Top=37,;
    Alignment=1, Width=81, Height=18,;
    Caption="Causale doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="ORDQGZUQBW",Visible=.t., Left=20, Top=329,;
    Alignment=1, Width=76, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return ((NOT( g_COMM='S' AND .w_FLGCOM='S')))
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=81,;
    width=641+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=82,width=640+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VOC_COST|CENCOST|CAN_TIER|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESVOC_2_6.Refresh()
      this.Parent.oDESPIA_2_7.Refresh()
      this.Parent.oDESCAN_2_8.Refresh()
      this.Parent.oMVINICOM_2_10.Refresh()
      this.Parent.oMVFINCOM_2_11.Refresh()
      this.Parent.oMVCODATT_2_13.Refresh()
      this.Parent.oDESATT_2_14.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VOC_COST'
        oDropInto=this.oBodyCol.oRow.oMVVOCCEN_2_2
      case cFile='CENCOST'
        oDropInto=this.oBodyCol.oRow.oMVCODCEN_2_3
      case cFile='CAN_TIER'
        oDropInto=this.oBodyCol.oRow.oMVCODCOM_2_4
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESVOC_2_6 as StdTrsField with uid="RGTUFPQLCI",rtseq=14,rtrep=.t.,;
    cFormVar="w_DESVOC",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione voce di costo o ricavo",;
    HelpContextID = 259984330,;
    cTotal="", bFixedPos=.t., cQueryName = "DESVOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=101, Top=246, InputMask=replicate('X',40)

  add object oDESPIA_2_7 as StdTrsField with uid="KVBWIIZOGD",rtseq=15,rtrep=.t.,;
    cFormVar="w_DESPIA",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione centro di costo o ricavo",;
    HelpContextID = 31787978,;
    cTotal="", bFixedPos=.t., cQueryName = "DESPIA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=101, Top=273, InputMask=replicate('X',40)

  add object oDESCAN_2_8 as StdTrsField with uid="JOPKQHESXB",rtseq=16,rtrep=.t.,;
    cFormVar="w_DESCAN",value=space(30),enabled=.f.,;
    ToolTipText = "Descrizione della commessa",;
    HelpContextID = 91360202,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCAN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=101, Top=301, InputMask=replicate('X',30)

  add object oMVINICOM_2_10 as StdTrsField with uid="LAGFXYUIVO",rtseq=18,rtrep=.t.,;
    cFormVar="w_MVINICOM",value=ctod("  /  /  "),;
    ToolTipText = "Data di inizio competenza; l'intervallo deve essere vuoto oppure finito",;
    HelpContextID = 266836461,;
    cTotal="", bFixedPos=.t., cQueryName = "MVINICOM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Data inizio competenza incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=451, Top=302, tabstop=.f.

  add object oMVFINCOM_2_11 as StdTrsField with uid="ZFLJTUCEAJ",rtseq=19,rtrep=.t.,;
    cFormVar="w_MVFINCOM",value=ctod("  /  /  "),;
    ToolTipText = "Data di fine competenza; l'intervallo deve essere vuoto oppure finito",;
    HelpContextID = 261933549,;
    cTotal="", bFixedPos=.t., cQueryName = "MVFINCOM",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Data fine competenza incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=555, Top=302, tabstop=.f.

  func oMVFINCOM_2_11.mCond()
    with this.Parent.oContained
      return (not empty(.w_mvinicom))
    endwith
  endfunc

  func oMVFINCOM_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_mvinicom<=.w_mvfincom)
    endwith
    return bRes
  endfunc

  add object oMVCODATT_2_13 as StdTrsField with uid="MLWQMTKPSN",rtseq=25,rtrep=.t.,;
    cFormVar="w_MVCODATT",value=space(15),;
    ToolTipText = "Codice attivit� di commessa (non gestita nel ciclo attivo)",;
    HelpContextID = 37157350,;
    cTotal="", bFixedPos=.t., cQueryName = "MVCODATT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=101, Top=329, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_MVCODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_MVCODATT"

  func oMVCODATT_2_13.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MVCODCOM) AND g_COMM='S' AND .w_FLGCOM='S')
    endwith
  endfunc

  func oMVCODATT_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((NOT( g_COMM='S' AND .w_FLGCOM='S')))
    endwith
    endif
  endfunc

  func oMVCODATT_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODATT_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oMVCODATT_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_MVCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_MVCODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oMVCODATT_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivit�",'',this.parent.oContained
  endproc
  proc oMVCODATT_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_MVCODCOM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_MVCODATT
    i_obj.ecpSave()
  endproc

  add object oDESATT_2_14 as StdTrsField with uid="TGPOZQWOEA",rtseq=26,rtrep=.t.,;
    cFormVar="w_DESATT",value=space(30),enabled=.f.,;
    ToolTipText = "Descrizione attivit�",;
    HelpContextID = 239340490,;
    cTotal="", bFixedPos=.t., cQueryName = "DESATT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=226, Top=329, InputMask=replicate('X',30), TabStop=.f.

  func oDESATT_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((NOT( g_COMM='S' AND .w_FLGCOM='S')))
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTVIS_3_1 as StdField with uid="OWZBTDYCXW",rtseq=20,rtrep=.f.,;
    cFormVar="w_TOTVIS",value=0,enabled=.f.,;
    HelpContextID = 266268874,;
    cQueryName = "TOTVIS",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=489, Top=245, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  add object oSEGTOT_3_2 as StdField with uid="CHWSHHVNTD",rtseq=21,rtrep=.f.,;
    cFormVar="w_SEGTOT",value=space(1),enabled=.f.,;
    HelpContextID = 243387098,;
    cQueryName = "SEGTOT",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=459, Top=245, InputMask=replicate('X',1)
enddefine

* --- Defining Body row
define class tgsca_maaBodyRow as CPBodyRowCnt
  Width=631
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="JRMHDPBOXY",rtseq=9,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 50729110,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oMVVOCCEN_2_2 as StdTrsField with uid="UIDDAPITPZ",rtseq=10,rtrep=.t.,;
    cFormVar="w_MVVOCCEN",value=space(15),;
    ToolTipText = "Codice voce di costo o ricavo",;
    HelpContextID = 263861780,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=133, Left=31, Top=1, cSayPict=[p_MCE], cGetPict=[p_MCE], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_MVVOCCEN"

  func oMVVOCCEN_2_2.mCond()
    with this.Parent.oContained
      return (g_PERCCR='S' AND .w_FLANAL='S')
    endwith
  endfunc

  func oMVVOCCEN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVVOCCEN_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMVVOCCEN_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oMVVOCCEN_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo o ricavo",'GSVE_KDA.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oMVVOCCEN_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_MVVOCCEN
    i_obj.ecpSave()
  endproc

  add object oMVCODCEN_2_3 as StdTrsField with uid="OMJYAJEIEX",rtseq=11,rtrep=.t.,;
    cFormVar="w_MVCODCEN",value=space(15),;
    ToolTipText = "Codice centro di costo e ricavo",;
    HelpContextID = 264832532,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=169, Top=1, cSayPict=[p_CEN], cGetPict=[p_CEN], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_MVCODCEN"

  func oMVCODCEN_2_3.mCond()
    with this.Parent.oContained
      return (g_PERCCR='S' AND .w_FLANAL='S')
    endwith
  endfunc

  func oMVCODCEN_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODCEN_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMVCODCEN_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oMVCODCEN_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo",'',this.parent.oContained
  endproc
  proc oMVCODCEN_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_MVCODCEN
    i_obj.ecpSave()
  endproc

  add object oMVCODCOM_2_4 as StdTrsField with uid="DYNITUPAPW",rtseq=12,rtrep=.t.,;
    cFormVar="w_MVCODCOM",value=space(15),;
    ToolTipText = "Codice commessa",;
    HelpContextID = 3602925,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=306, Top=1, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_MVCODCOM"

  func oMVCODCOM_2_4.mCond()
    with this.Parent.oContained
      return (((g_PERCAN='S' AND .w_FLANAL='S') OR (g_COMM='S' AND .w_FLGCOM='S')))
    endwith
  endfunc

  func oMVCODCOM_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
      if .not. empty(.w_MVCODATT)
        bRes2=.link_2_13('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMVCODCOM_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMVCODCOM_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oMVCODCOM_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'',this.parent.oContained
  endproc
  proc oMVCODCOM_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_MVCODCOM
    i_obj.ecpSave()
  endproc

  add object oMV_SEGNO_2_5 as StdTrsField with uid="NEHYGTHFBT",rtseq=13,rtrep=.t.,;
    cFormVar="w_MV_SEGNO",value=space(1),enabled=.f.,;
    HelpContextID = 203504107,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=451, Top=1, InputMask=replicate('X',1)

  add object oMVIMPNAZ_2_9 as StdTrsField with uid="TWOHHIGMOA",rtseq=17,rtrep=.t.,;
    cFormVar="w_MVIMPNAZ",value=0,enabled=.f.,;
    ToolTipText = "Importo",;
    HelpContextID = 75012576,;
    cTotal = "this.Parent.oContained.w_totvis", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=142, Left=484, Top=1, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsca_maa','AGG_ANAL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MVSERIAL=AGG_ANAL.MVSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
