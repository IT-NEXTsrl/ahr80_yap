* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gs___bdg                                                        *
*              Delete gruppo                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-25                                                      *
* Last revis.: 2005-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgs___bdg",oParentObject,m.pCODE)
return(i_retval)

define class tgs___bdg as StdBatch
  * --- Local variables
  pCODE = 0
  w_MESS = space(100)
  w_CODE = 0
  w_FLUTEGRP = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch scatta alla cancellazione di un gruppo, il batch � sotto transazione
    *     per bloccare la cancellazione in caso di errore occore utilizzare una 
    *     "Transaction Error"
    * --- Parametri:
    *     pCODE = Codice gruppo da cancellare
    this.w_CODE = this.pCODE
    this.w_FLUTEGRP = "G"
    * --- Cancello le autorizzazioni query di infopublisher legate al gruppo
    if Type ("g_IRDR") = "C" AND g_IRDR = "S"
      * --- Try
      local bErr_0361B5B8
      bErr_0361B5B8=bTrsErr
      this.Try_0361B5B8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = AH_MSGFORMAT("%1Impossibile cancellare il gruppo dalle autorizzazioni %2",message(),"InfoPublisher")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
      bTrsErr=bTrsErr or bErr_0361B5B8
      * --- End
    endif
    * --- Affinch� il batch non ritorni errore "Property MCALLEDBATCHEND is not found"
    *     devo porre bUpdateParentObject a false
    this.bUpdateParentObject = .F.
  endproc
  proc Try_0361B5B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do GS___BDW with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return


  proc Init(oParentObject,pCODE)
    this.pCODE=pCODE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODE"
endproc
