* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bev                                                        *
*              Eventi conferma maschera art.composti                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_22]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-26                                                      *
* Last revis.: 2001-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bev",oParentObject)
return(i_retval)

define class tgsar_bev as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi alla Conferma della Maschera Articoli Composti (da GSDS_MEA)
    WITH this.oParentObject.w_GSDS_BEA
    .w_CONFERMA=.T.
    .w_FLMGPR_P = this.oParentObject.w_FLMGPR_P
    .w_FLMTPR_P = this.oParentObject.w_FLMTPR_P
    .w_FLMGPR_C = this.oParentObject.w_FLMGPR_C
    .w_FLMTPR_C = this.oParentObject.w_FLMTPR_C
    .w_MAGDES_P = this.oParentObject.w_MAGPFI
    .w_MAGDES_C = this.oParentObject.w_MAGCOD
    .w_MATDES_P = this.oParentObject.w_MATPFI
    .w_MATDES_C = this.oParentObject.w_MATCOD
    .w_DITIPVAL = this.oParentObject.w_TIPVAL
    .w_DICODESE = this.oParentObject.w_CODESE
    .w_DINUMINV = this.oParentObject.w_NUMINV
    do case
      case this.oParentObject.w_TIPVAL="L"
        * --- Calcola su Listino
        .w_MMTCOLIS = this.oParentObject.w_CODLIS
        .w_MVCODVAL = this.oParentObject.w_VALUTA
        .w_MVCAOVAL = IIF(this.oParentObject.w_CAOVAL=0, this.oParentObject.w_CAOLIS, this.oParentObject.w_CAOVAL)
      case this.oParentObject.w_TIPVAL $ "SMUP"
        * --- Calcola su Inventario
        .w_MMTCOLIS = SPACE(5)
        .w_MVCODVAL = this.oParentObject.w_VALESE
        .w_MVCAOVAL = this.oParentObject.w_CAOESE
      case this.oParentObject.w_TIPVAL = "A"
        .w_MAGCOD = this.oParentObject.w_MAGCOD
      otherwise
        .w_MMTCOLIS = SPACE(5)
        .w_MVCODVAL = g_PERVAL
        .w_MVCAOVAL = g_CAOVAL
    endcase
    .w_TIPOLN = this.oParentObject.w_TIPOLN
    ENDWITH
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
