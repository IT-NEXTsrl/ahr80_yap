* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bcf                                                        *
*              Men� contestuale clienti/fornitori                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-17                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bcf",oParentObject)
return(i_retval)

define class tgszm_bcf as StdBatch
  * --- Local variables
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_OBJECT = .NULL.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per avvio anagrafica clienti/fornitori da men� contestuale 
    * --- Assegno i valori alle variabili
    this.w_ANTIPCON = g_oMenu.getbyindexkeyvalue(1)
    this.w_ANCODICE = g_oMenu.getbyindexkeyvalue(2)
    * --- Anagrafica Clienti\Conti\Fornitori
    this.w_OBJECT = iif(this.w_ANTIPCON="F",GSAR_AFR(),iif(this.w_ANTIPCON="C",GSAR_ACL(),GSAR_API()))
    if !(this.w_OBJECT.bSec1)
      i_retcode = 'stop'
      return
    endif
    this.w_OBJECT.EcpFilter()     
    * --- Valorizzo i campi chiave
    this.w_OBJECT.w_ANTIPCON = this.w_ANTIPCON
    this.w_OBJECT.w_ANCODICE = this.w_ANCODICE
    * --- Carico il record richiesto
    this.w_OBJECT.EcpSave()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
