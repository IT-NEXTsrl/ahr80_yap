* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_mac                                                        *
*              Caratteristiche articolo                                        *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-08                                                      *
* Last revis.: 2015-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsma_mac")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsma_mac")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsma_mac")
  return

* --- Class definition
define class tgsma_mac as StdPCForm
  Width  = 670
  Height = 457
  Top    = 84
  Left   = 15
  cComment = "Caratteristiche articolo"
  cPrg = "gsma_mac"
  HelpContextID=130710377
  add object cnt as tcgsma_mac
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsma_mac as PCContext
  w_CCCODART = space(20)
  w_CPROWORD = 0
  w_CCTIPOCA = space(1)
  w_TIPOCA = space(1)
  w_CCCODICE = space(5)
  w_CCDESCRI = space(40)
  w_OBTEST = space(8)
  w_CC__NOTE = space(10)
  proc Save(i_oFrom)
    this.w_CCCODART = i_oFrom.w_CCCODART
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_CCTIPOCA = i_oFrom.w_CCTIPOCA
    this.w_TIPOCA = i_oFrom.w_TIPOCA
    this.w_CCCODICE = i_oFrom.w_CCCODICE
    this.w_CCDESCRI = i_oFrom.w_CCDESCRI
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_CC__NOTE = i_oFrom.w_CC__NOTE
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CCCODART = this.w_CCCODART
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_CCTIPOCA = this.w_CCTIPOCA
    i_oTo.w_TIPOCA = this.w_TIPOCA
    i_oTo.w_CCCODICE = this.w_CCCODICE
    i_oTo.w_CCDESCRI = this.w_CCDESCRI
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_CC__NOTE = this.w_CC__NOTE
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsma_mac as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 670
  Height = 457
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-15"
  HelpContextID=130710377
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CONF_ART_IDX = 0
  CONF_CAR_IDX = 0
  cFile = "CONF_ART"
  cKeySelect = "CCCODART"
  cKeyWhere  = "CCCODART=this.w_CCCODART"
  cKeyDetail  = "CCCODART=this.w_CCCODART and CCTIPOCA=this.w_CCTIPOCA and CCCODICE=this.w_CCCODICE"
  cKeyWhereODBC = '"CCCODART="+cp_ToStrODBC(this.w_CCCODART)';

  cKeyDetailWhereODBC = '"CCCODART="+cp_ToStrODBC(this.w_CCCODART)';
      +'+" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA)';
      +'+" and CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';

  cKeyWhereODBCqualified = '"CONF_ART.CCCODART="+cp_ToStrODBC(this.w_CCCODART)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CONF_ART.CPROWORD'
  cPrg = "gsma_mac"
  cComment = "Caratteristiche articolo"
  i_nRowNum = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CCCODART = space(20)
  w_CPROWORD = 0
  w_CCTIPOCA = space(1)
  o_CCTIPOCA = space(1)
  w_TIPOCA = space(1)
  o_TIPOCA = space(1)
  w_CCCODICE = space(5)
  w_CCDESCRI = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_CC__NOTE = space(0)

  * --- Children pointers
  GSMA_MAD = .NULL.
  GSMA_AMP = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSMA_MAD additive
    *set procedure to GSMA_AMP additive
    with this
      .Pages(1).addobject("oPag","tgsma_macPag1","gsma_mac",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Caratteristiche")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSMA_MAD
    *release procedure GSMA_AMP
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONF_CAR'
    this.cWorkTables[2]='CONF_ART'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONF_ART_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONF_ART_IDX,3]
  return

  function CreateChildren()
    this.GSMA_MAD = CREATEOBJECT('stdDynamicChild',this,'GSMA_MAD',this.oPgFrm.Page1.oPag.oLinkPC_2_6)
    this.GSMA_MAD.createrealchild()
    this.GSMA_AMP = CREATEOBJECT('stdDynamicChild',this,'GSMA_AMP',this.oPgFrm.Page1.oPag.oLinkPC_2_7)
    this.GSMA_AMP.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgsma_mac'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSMA_MAD)
      this.GSMA_MAD.DestroyChildrenChain()
    endif
    if !ISNULL(this.GSMA_AMP)
      this.GSMA_AMP.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSMA_MAD.HideChildrenChain()
    this.GSMA_AMP.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSMA_MAD.ShowChildrenChain()
    this.GSMA_AMP.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSMA_MAD)
      this.GSMA_MAD.DestroyChildrenChain()
      this.GSMA_MAD=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_6')
    if !ISNULL(this.GSMA_AMP)
      this.GSMA_AMP.DestroyChildrenChain()
      this.GSMA_AMP=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_7')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMA_MAD.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSMA_AMP.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMA_MAD.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSMA_AMP.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMA_MAD.NewDocument()
    this.GSMA_AMP.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSMA_MAD.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_CCCODART,"CCCODART";
             ,.w_CCTIPOCA,"CCTIPOCA";
             ,.w_CCCODICE,"CCCODICE";
             )
      .GSMA_AMP.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_CCCODART,"CCCODART";
             ,.w_CCTIPOCA,"CCTIPOCA";
             ,.w_CCCODICE,"CCCODICE";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CONF_ART where CCCODART=KeySet.CCCODART
    *                            and CCTIPOCA=KeySet.CCTIPOCA
    *                            and CCCODICE=KeySet.CCCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CONF_ART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_ART_IDX,2],this.bLoadRecFilter,this.CONF_ART_IDX,"gsma_mac")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONF_ART')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONF_ART.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONF_ART '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCCODART',this.w_CCCODART  )
      select * from (i_cTable) CONF_ART where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_DATSYS
        .w_CCCODART = NVL(CCCODART,space(20))
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CONF_ART')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CC__NOTE = space(0)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CCTIPOCA = NVL(CCTIPOCA,space(1))
        .w_TIPOCA = .w_CCTIPOCA
          .w_CCCODICE = NVL(CCCODICE,space(5))
          .link_2_4('Load')
          .w_CCDESCRI = NVL(CCDESCRI,space(40))
          select (this.cTrsName)
          append blank
          replace CCCODART with .w_CCCODART
          replace CCTIPOCA with .w_CCTIPOCA
          replace CCCODICE with .w_CCCODICE
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_CCCODART=space(20)
      .w_CPROWORD=10
      .w_CCTIPOCA=space(1)
      .w_TIPOCA=space(1)
      .w_CCCODICE=space(5)
      .w_CCDESCRI=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_CC__NOTE=space(0)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_CCTIPOCA = "C"
        .w_TIPOCA = .w_CCTIPOCA
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CCCODICE))
         .link_2_4('Full')
        endif
        .DoRTCalc(6,6,.f.)
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONF_ART')
    this.DoRTCalc(8,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_1_4.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSMA_MAD.SetStatus(i_cOp)
    this.GSMA_AMP.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CONF_ART',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSMA_MAD.SetChildrenStatus(i_cOp)
  *  this.GSMA_AMP.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONF_ART_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODART,"CCCODART",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_TIPOCA N(3);
      ,t_CCCODICE C(5);
      ,t_CCDESCRI C(40);
      ,t_CC__NOTE M(10);
      ,CCCODART C(20);
      ,CCTIPOCA C(1);
      ,CCCODICE C(5);
      ,t_CCTIPOCA C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsma_macbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTIPOCA_2_3.controlsource=this.cTrsName+'.t_TIPOCA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODICE_2_4.controlsource=this.cTrsName+'.t_CCCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCCDESCRI_2_5.controlsource=this.cTrsName+'.t_CCDESCRI'
    this.oPgFRm.Page1.oPag.oCC__NOTE_2_9.controlsource=this.cTrsName+'.t_CC__NOTE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(74)
    this.AddVLine(184)
    this.AddVLine(269)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONF_ART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_ART_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- gsma_mac
    * --- Aggiorna il flag KEY_ARTI.CAGESCAR
    this.NotifyEvent('UPD_KEY')
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONF_ART_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_ART_IDX,2])
      *
      * insert into CONF_ART
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONF_ART')
        i_extval=cp_InsertValODBCExtFlds(this,'CONF_ART')
        i_cFldBody=" "+;
                  "(CCCODART,CPROWORD,CCTIPOCA,CCCODICE,CCDESCRI,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CCCODART)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_CCTIPOCA)+","+cp_ToStrODBCNull(this.w_CCCODICE)+","+cp_ToStrODBC(this.w_CCDESCRI)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONF_ART')
        i_extval=cp_InsertValVFPExtFlds(this,'CONF_ART')
        cp_CheckDeletedKey(i_cTable,0,'CCCODART',this.w_CCCODART,'CCTIPOCA',this.w_CCTIPOCA,'CCCODICE',this.w_CCCODICE)
        INSERT INTO (i_cTable) (;
                   CCCODART;
                  ,CPROWORD;
                  ,CCTIPOCA;
                  ,CCCODICE;
                  ,CCDESCRI;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CCCODART;
                  ,this.w_CPROWORD;
                  ,this.w_CCTIPOCA;
                  ,this.w_CCCODICE;
                  ,this.w_CCDESCRI;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CONF_ART_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_ART_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_CCCODART<>CCCODART
            i_bUpdAll = .t.
          endif
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CCCODICE))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CONF_ART')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CCTIPOCA="+cp_ToStrODBC(&i_TN.->CCTIPOCA)+;
                 " and CCCODICE="+cp_ToStrODBC(&i_TN.->CCCODICE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CONF_ART')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CCTIPOCA=&i_TN.->CCTIPOCA;
                      and CCCODICE=&i_TN.->CCCODICE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_CCCODART<>CCCODART
            i_bUpdAll = .t.
          endif
        endif
        scan for (not(Empty(t_CCCODICE))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSMA_MAD.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_CCCODART,"CCCODART";
                     ,this.w_CCTIPOCA,"CCTIPOCA";
                     ,this.w_CCCODICE,"CCCODICE";
                     )
              this.GSMA_AMP.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_CCCODART,"CCCODART";
                     ,this.w_CCTIPOCA,"CCTIPOCA";
                     ,this.w_CCCODICE,"CCCODICE";
                     )
              this.GSMA_MAD.mDelete()
              this.GSMA_AMP.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and CCTIPOCA="+cp_ToStrODBC(&i_TN.->CCTIPOCA)+;
                            " and CCCODICE="+cp_ToStrODBC(&i_TN.->CCCODICE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and CCTIPOCA=&i_TN.->CCTIPOCA;
                            and CCCODICE=&i_TN.->CCCODICE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace CCTIPOCA with this.w_CCTIPOCA
              replace CCCODICE with this.w_CCCODICE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CONF_ART
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CONF_ART')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CCDESCRI="+cp_ToStrODBC(this.w_CCDESCRI)+;
                     ",CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA)+;
                     ",CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and CCTIPOCA="+cp_ToStrODBC(CCTIPOCA)+;
                             " and CCCODICE="+cp_ToStrODBC(CCCODICE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CONF_ART')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CCDESCRI=this.w_CCDESCRI;
                     ,CCTIPOCA=this.w_CCTIPOCA;
                     ,CCCODICE=this.w_CCCODICE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and CCTIPOCA=&i_TN.->CCTIPOCA;
                                      and CCCODICE=&i_TN.->CCCODICE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_CCCODICE)))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSMA_MAD.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CCCODART,"CCCODART";
               ,this.w_CCTIPOCA,"CCTIPOCA";
               ,this.w_CCCODICE,"CCCODICE";
               )
          this.GSMA_MAD.mReplace()
          this.GSMA_AMP.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CCCODART,"CCCODART";
               ,this.w_CCTIPOCA,"CCTIPOCA";
               ,this.w_CCCODICE,"CCCODICE";
               )
          this.GSMA_AMP.mReplace()
          this.GSMA_MAD.bSaveContext=.f.
          this.GSMA_AMP.bSaveContext=.f.
        endif
      endscan
     this.GSMA_MAD.bSaveContext=.t.
     this.GSMA_AMP.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- gsma_mac
    * --- Aggiorna il flag KEY_ARTI.CAGESCAR
    this.NotifyEvent('UPD_KEY')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONF_ART_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_ART_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CCCODICE))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSMA_MAD : Deleting
        this.GSMA_MAD.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CCCODART,"CCCODART";
               ,this.w_CCTIPOCA,"CCTIPOCA";
               ,this.w_CCCODICE,"CCCODICE";
               )
        this.GSMA_MAD.mDelete()
        * --- GSMA_AMP : Deleting
        this.GSMA_AMP.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CCCODART,"CCCODART";
               ,this.w_CCTIPOCA,"CCTIPOCA";
               ,this.w_CCCODICE,"CCCODICE";
               )
        this.GSMA_AMP.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CONF_ART
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and CCTIPOCA="+cp_ToStrODBC(&i_TN.->CCTIPOCA)+;
                            " and CCCODICE="+cp_ToStrODBC(&i_TN.->CCCODICE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and CCTIPOCA=&i_TN.->CCTIPOCA;
                              and CCCODICE=&i_TN.->CCCODICE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CCCODICE))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsma_mac
    * --- Aggiorna il flag KEY_ARTI.CAGESCAR
    * --- this.NotifyEvent('UPD_KEY')
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONF_ART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_ART_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_CCTIPOCA<>.w_CCTIPOCA
          .w_TIPOCA = .w_CCTIPOCA
        endif
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        if .o_TIPOCA<>.w_TIPOCA
          .Calculate_LYESVDOCPQ()
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CCTIPOCA with this.w_CCTIPOCA
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_4.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_HHANBXQBTU()
    with this
          * --- w_CCCODICE Changed
     if g_CCAR='S'
          GSCR_BAC(this;
              ,"CAR_DETT";
             )
     endif
    endwith
  endproc
  proc Calculate_LYESVDOCPQ()
    with this
          * --- 
          .w_CCTIPOCA = .w_TIPOCA
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTIPOCA_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTIPOCA_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCCODICE_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCCCODICE_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oLinkPC_2_6.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_6.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_2_7.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_7.mHide()
    this.oPgFrm.Page1.oPag.oCC__NOTE_2_9.visible=!this.oPgFrm.Page1.oPag.oCC__NOTE_2_9.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_CCCODICE Changed")
          .Calculate_HHANBXQBTU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_4.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CCCODICE
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONF_CAR_IDX,3]
    i_lTable = "CONF_CAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2], .t., this.CONF_CAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONF_CAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CCCODICE)+"%");
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA);

          i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPOCA,CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPOCA',this.w_CCTIPOCA;
                     ,'CCCODICE',trim(this.w_CCCODICE))
          select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPOCA,CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODICE)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CCCODICE)+"%");
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA);

            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CCCODICE)+"%");
                   +" and CCTIPOCA="+cp_ToStr(this.w_CCTIPOCA);

            select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CCCODICE) and !this.bDontReportError
            deferred_cp_zoom('CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(oSource.parent,'oCCCODICE_2_4'),i_cWhere,'',"Caratteristiche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CCTIPOCA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',oSource.xKey(1);
                       ,'CCCODICE',oSource.xKey(2))
            select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CCCODICE);
                   +" and CCTIPOCA="+cp_ToStrODBC(this.w_CCTIPOCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPOCA',this.w_CCTIPOCA;
                       ,'CCCODICE',this.w_CCCODICE)
            select CCTIPOCA,CCCODICE,CCDESCRI,CC__NOTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODICE = NVL(_Link_.CCCODICE,space(5))
      this.w_CCDESCRI = NVL(_Link_.CCDESCRI,space(40))
      this.w_CC__NOTE = NVL(_Link_.CC__NOTE,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODICE = space(5)
      endif
      this.w_CCDESCRI = space(40)
      this.w_CC__NOTE = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONF_CAR_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPOCA,1)+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CONF_CAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCC__NOTE_2_9.value==this.w_CC__NOTE)
      this.oPgFrm.Page1.oPag.oCC__NOTE_2_9.value=this.w_CC__NOTE
      replace t_CC__NOTE with this.oPgFrm.Page1.oPag.oCC__NOTE_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPOCA_2_3.RadioValue()==this.w_TIPOCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPOCA_2_3.SetRadio()
      replace t_TIPOCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPOCA_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODICE_2_4.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODICE_2_4.value=this.w_CCCODICE
      replace t_CCCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODICE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDESCRI_2_5.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDESCRI_2_5.value=this.w_CCDESCRI
      replace t_CCDESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCDESCRI_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'CONF_ART')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_CCCODICE) and (.cFuncTion="Load" or empty(.w_CCCODICE)) and (not(Empty(.w_CCCODICE)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCCCODICE_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      i_bRes = i_bRes .and. .GSMA_MAD.CheckForm()
      i_bRes = i_bRes .and. .GSMA_AMP.CheckForm()
      if not(Empty(.w_CCCODICE))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CCTIPOCA = this.w_CCTIPOCA
    this.o_TIPOCA = this.w_TIPOCA
    * --- GSMA_MAD : Depends On
    this.GSMA_MAD.SaveDependsOn()
    * --- GSMA_AMP : Depends On
    this.GSMA_AMP.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CCCODICE)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_CCTIPOCA=space(1)
      .w_TIPOCA=space(1)
      .w_CCCODICE=space(5)
      .w_CCDESCRI=space(40)
      .w_CC__NOTE=space(0)
      .DoRTCalc(1,2,.f.)
        .w_CCTIPOCA = "C"
        .w_TIPOCA = .w_CCTIPOCA
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_CCCODICE))
        .link_2_4('Full')
      endif
    endwith
    this.DoRTCalc(6,8,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_CCTIPOCA = t_CCTIPOCA
    this.w_TIPOCA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPOCA_2_3.RadioValue(.t.)
    this.w_CCCODICE = t_CCCODICE
    this.w_CCDESCRI = t_CCDESCRI
    this.w_CC__NOTE = t_CC__NOTE
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_CCTIPOCA with this.w_CCTIPOCA
    replace t_TIPOCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTIPOCA_2_3.ToRadio()
    replace t_CCCODICE with this.w_CCCODICE
    replace t_CCDESCRI with this.w_CCDESCRI
    replace t_CC__NOTE with this.w_CC__NOTE
    if i_srv='A'
      replace CCTIPOCA with this.w_CCTIPOCA
      replace CCCODICE with this.w_CCCODICE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsma_macPag1 as StdContainer
  Width  = 666
  height = 457
  stdWidth  = 666
  stdheight = 457
  resizeXpos=560
  resizeYpos=350
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_4 as cp_runprogram with uid="GCTROANCNB",left=5, top=493, width=252,height=19,;
    caption='GSMA_BAC(UPD_CCAR)',;
   bGlobalFont=.t.,;
    prg="GSMA_BAC('UPD_CCAR')",;
    cEvent = "UPD_KEY",;
    nPag=1;
    , HelpContextID = 161821322


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=18, top=3, width=637,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Riga",Field2="TIPOCA",Label2="Tipo",Field3="CCCODICE",Label3="Caratt/modello",Field4="CCDESCRI",Label4="Descrizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185380474
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsma_mad",lower(this.oContained.GSMA_MAD.class))=0
        this.oContained.GSMA_MAD.createrealchild()
      endif
      if type('this.oContained')='O' and at("gsma_amp",lower(this.oContained.GSMA_AMP.class))=0
        this.oContained.GSMA_AMP.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=8,top=22,;
    width=633+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=9,top=23,width=632+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CONF_CAR|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCC__NOTE_2_9.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CONF_CAR'
        oDropInto=this.oBodyCol.oRow.oCCCODICE_2_4
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_6 as stdDynamicChildContainer with uid="XFZFJIDMSH",bOnScreen=.t.,width=604,height=284,;
   left=5, top=162;


  func oLinkPC_2_6.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPOCA<>"C")
    endwith
   endif
  endfunc

  add object oLinkPC_2_7 as stdDynamicChildContainer with uid="EGTSSMDSYA",bOnScreen=.t.,width=626,height=288,;
   left=5, top=162;


  func oLinkPC_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPOCA<>"M")
    endwith
   endif
  endfunc

  add object oCC__NOTE_2_9 as StdTrsMemo with uid="QYKYATVOWC",rtseq=8,rtrep=.t.,;
    cFormVar="w_CC__NOTE",value=space(0),enabled=.f.,;
    HelpContextID = 209370219,;
    cTotal="", bFixedPos=.t., cQueryName = "CC__NOTE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=288, Width=647, Left=5, Top=162

  func oCC__NOTE_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CCTIPOCA<>"D")
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsma_macBodyRow as CPBodyRowCnt
  Width=623
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="TOAKAMAILH",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 217708906,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=50, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oTIPOCA_2_3 as StdTrsCombo with uid="PXZBKJXFMP",rtrep=.t.,;
    cFormVar="w_TIPOCA", RowSource=""+"Caratteristica,"+"Modello,"+"Descrizione" , ;
    HelpContextID = 38153418,;
    Height=21, Width=103, Left=60, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oTIPOCA_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TIPOCA,&i_cF..t_TIPOCA),this.value)
    return(iif(xVal =1,"C",;
    iif(xVal =2,"M",;
    iif(xVal =3,"D",;
    space(1)))))
  endfunc
  func oTIPOCA_2_3.GetRadio()
    this.Parent.oContained.w_TIPOCA = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCA_2_3.ToRadio()
    this.Parent.oContained.w_TIPOCA=trim(this.Parent.oContained.w_TIPOCA)
    return(;
      iif(this.Parent.oContained.w_TIPOCA=="C",1,;
      iif(this.Parent.oContained.w_TIPOCA=="M",2,;
      iif(this.Parent.oContained.w_TIPOCA=="D",3,;
      0))))
  endfunc

  func oTIPOCA_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTIPOCA_2_3.mCond()
    with this.Parent.oContained
      return (empty(nvl(.w_CCCODICE,"")))
    endwith
  endfunc

  add object oCCCODICE_2_4 as StdTrsField with uid="SHFJHECKFE",rtseq=5,rtrep=.t.,;
    cFormVar="w_CCCODICE",value=space(5),isprimarykey=.t.,;
    HelpContextID = 97057899,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=81, Left=165, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CONF_CAR", oKey_1_1="CCTIPOCA", oKey_1_2="this.w_CCTIPOCA", oKey_2_1="CCCODICE", oKey_2_2="this.w_CCCODICE"

  func oCCCODICE_2_4.mCond()
    with this.Parent.oContained
      return (.cFuncTion="Load" or empty(.w_CCCODICE))
    endwith
  endfunc

  func oCCCODICE_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCODICE_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCCCODICE_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oCCCODICE_2_4.readonly and this.parent.oCCCODICE_2_4.isprimarykey)
    if i_TableProp[this.parent.oContained.CONF_CAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStrODBC(this.Parent.oContained.w_CCTIPOCA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPOCA="+cp_ToStr(this.Parent.oContained.w_CCTIPOCA)
    endif
    do cp_zoom with 'CONF_CAR','*','CCTIPOCA,CCCODICE',cp_AbsName(this.parent,'oCCCODICE_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Caratteristiche",'',this.parent.oContained
   endif
  endproc

  add object oCCDESCRI_2_5 as StdTrsField with uid="ZYUEJRUCMX",rtseq=6,rtrep=.t.,;
    cFormVar="w_CCDESCRI",value=space(40),;
    HelpContextID = 11471983,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=366, Left=252, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_mac','CONF_ART','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCCODART=CONF_ART.CCCODART";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
