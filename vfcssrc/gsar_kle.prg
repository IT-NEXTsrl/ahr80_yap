* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kle                                                        *
*              Dati originali da lettore ottico                                *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_1]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-25                                                      *
* Last revis.: 2010-03-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kle",oParentObject))

* --- Class definition
define class tgsar_kle as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 545
  Height = 395+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-03-10"
  HelpContextID=216627049
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsar_kle"
  cComment = "Dati originali da lettore ottico"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPOOBJ_GEST = space(1)
  w_GESMAT = space(1)
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_FLVEAC = space(1)
  w_FLPACK = space(1)
  w_FLESUL = space(1)
  w_CODICE = space(20)
  w_QTAMOV = 0
  w_PREZZO = 0
  w_CODUBI = space(20)
  w_CODUB2 = space(20)
  w_CODLOT = space(20)
  w_QTAUM1 = 0
  w_UNIMIS = space(3)
  w_CODMAG = space(5)
  w_CODMA2 = space(5)
  w_CODMAT = space(40)
  w_CODCEN_OR = space(15)
  w_CODCOM_OR = space(15)
  w_CODATT_OR = space(15)
  w_UNILOG = space(20)
  w_TRACCIATO = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_klePag1","gsar_kle",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati")
      .Pages(2).addobject("oPag","tgsar_klePag2","gsar_kle",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Tracciato")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOOBJ_GEST=space(1)
      .w_GESMAT=space(1)
      .w_FLANAL=space(1)
      .w_FLGCOM=space(1)
      .w_FLVEAC=space(1)
      .w_FLPACK=space(1)
      .w_FLESUL=space(1)
      .w_CODICE=space(20)
      .w_QTAMOV=0
      .w_PREZZO=0
      .w_CODUBI=space(20)
      .w_CODUB2=space(20)
      .w_CODLOT=space(20)
      .w_QTAUM1=0
      .w_UNIMIS=space(3)
      .w_CODMAG=space(5)
      .w_CODMA2=space(5)
      .w_CODMAT=space(40)
      .w_CODCEN_OR=space(15)
      .w_CODCOM_OR=space(15)
      .w_CODATT_OR=space(15)
      .w_UNILOG=space(20)
      .w_TRACCIATO=space(0)
      .w_TIPOOBJ_GEST=oParentObject.w_TIPOOBJ_GEST
      .w_GESMAT=oParentObject.w_GESMAT
      .w_FLANAL=oParentObject.w_FLANAL
      .w_FLGCOM=oParentObject.w_FLGCOM
      .w_FLVEAC=oParentObject.w_FLVEAC
      .w_FLPACK=oParentObject.w_FLPACK
      .w_FLESUL=oParentObject.w_FLESUL
      .w_CODICE=oParentObject.w_CODICE
      .w_QTAMOV=oParentObject.w_QTAMOV
      .w_PREZZO=oParentObject.w_PREZZO
      .w_CODUBI=oParentObject.w_CODUBI
      .w_CODUB2=oParentObject.w_CODUB2
      .w_CODLOT=oParentObject.w_CODLOT
      .w_QTAUM1=oParentObject.w_QTAUM1
      .w_UNIMIS=oParentObject.w_UNIMIS
      .w_CODMAG=oParentObject.w_CODMAG
      .w_CODMA2=oParentObject.w_CODMA2
      .w_CODMAT=oParentObject.w_CODMAT
      .w_CODCEN_OR=oParentObject.w_CODCEN_OR
      .w_CODCOM_OR=oParentObject.w_CODCOM_OR
      .w_CODATT_OR=oParentObject.w_CODATT_OR
      .w_UNILOG=oParentObject.w_UNILOG
    endwith
    this.DoRTCalc(1,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_kle
    LunghezzaArray = 0
    DoppioApice='"'
    IF type("g_ARRAYFIELDNAME")<>"U"
      LunghezzaArray = ALEN(g_ARRAYFIELDNAME)
    ENDIF
    IF type("g_ARRAYFIELDPOSITION")<>"U"
      LunghezzaArray = MIN( LunghezzaArray , ALEN(g_ARRAYFIELDPOSITION) )
    ENDIF
    
    CONTATORE = 1
    DO WHILE CONTATORE <= LunghezzaArray
    
      THIS.w_TRACCIATO = THIS.w_TRACCIATO + "g_ArrayFieldName[" + ALLTRIM(STR(CONTATORE)) + "]=" + DoppioApice
      NomeCampo = g_ArrayFieldName[CONTATORE]
      IF TYPE("NomeCampo")="C"
        THIS.w_TRACCIATO = THIS.w_TRACCIATO + NomeCampo
      endif
      THIS.w_TRACCIATO = THIS.w_TRACCIATO + DoppioApice
      THIS.w_TRACCIATO = THIS.w_TRACCIATO + CHR( 13 )
    
      THIS.w_TRACCIATO = THIS.w_TRACCIATO + "g_ArrayFieldPosition[" + ALLTRIM(STR(CONTATORE)) + "]="
      LunghezzaCampo = g_ArrayFieldPosition[CONTATORE]
      IF TYPE("LunghezzaCampo")="N"
        THIS.w_TRACCIATO = THIS.w_TRACCIATO + alltrim(str(LunghezzaCampo))
      endif
      THIS.w_TRACCIATO = THIS.w_TRACCIATO + CHR( 13 )
    
      IF CONTATORE < LunghezzaArray
        THIS.w_TRACCIATO = THIS.w_TRACCIATO + AH_MSGFORMAT( "Dimensione del campo: " )
        InizioCampo = g_ArrayFieldPosition[CONTATORE]
        FineCampo = g_ArrayFieldPosition[CONTATORE+1]
        IF TYPE("InizioCampo")="N" AND TYPE("FineCampo")="N"
          IF CONTATORE + 1 < LunghezzaArray
            THIS.w_TRACCIATO = THIS.w_TRACCIATO + alltrim(str(FineCampo-InizioCampo))
          ELSE
            THIS.w_TRACCIATO = THIS.w_TRACCIATO + AH_MSGFORMAT( "fino al termine della stringa" )
          ENDIF
        ENDIF
        THIS.w_TRACCIATO = THIS.w_TRACCIATO + CHR( 13 ) + CHR( 13 )
      ENDIF
    
      CONTATORE = CONTATORE + 1
    
    ENDDO
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIPOOBJ_GEST=.w_TIPOOBJ_GEST
      .oParentObject.w_GESMAT=.w_GESMAT
      .oParentObject.w_FLANAL=.w_FLANAL
      .oParentObject.w_FLGCOM=.w_FLGCOM
      .oParentObject.w_FLVEAC=.w_FLVEAC
      .oParentObject.w_FLPACK=.w_FLPACK
      .oParentObject.w_FLESUL=.w_FLESUL
      .oParentObject.w_CODICE=.w_CODICE
      .oParentObject.w_QTAMOV=.w_QTAMOV
      .oParentObject.w_PREZZO=.w_PREZZO
      .oParentObject.w_CODUBI=.w_CODUBI
      .oParentObject.w_CODUB2=.w_CODUB2
      .oParentObject.w_CODLOT=.w_CODLOT
      .oParentObject.w_QTAUM1=.w_QTAUM1
      .oParentObject.w_UNIMIS=.w_UNIMIS
      .oParentObject.w_CODMAG=.w_CODMAG
      .oParentObject.w_CODMA2=.w_CODMA2
      .oParentObject.w_CODMAT=.w_CODMAT
      .oParentObject.w_CODCEN_OR=.w_CODCEN_OR
      .oParentObject.w_CODCOM_OR=.w_CODCOM_OR
      .oParentObject.w_CODATT_OR=.w_CODATT_OR
      .oParentObject.w_UNILOG=.w_UNILOG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODUBI_1_14.visible=!this.oPgFrm.Page1.oPag.oCODUBI_1_14.mHide()
    this.oPgFrm.Page1.oPag.oCODUB2_1_15.visible=!this.oPgFrm.Page1.oPag.oCODUB2_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCODLOT_1_18.visible=!this.oPgFrm.Page1.oPag.oCODLOT_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oCODMAT_1_28.visible=!this.oPgFrm.Page1.oPag.oCODMAT_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oCODCEN_OR_1_30.visible=!this.oPgFrm.Page1.oPag.oCODCEN_OR_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oCODCOM_OR_1_33.visible=!this.oPgFrm.Page1.oPag.oCODCOM_OR_1_33.mHide()
    this.oPgFrm.Page1.oPag.oCODATT_OR_1_34.visible=!this.oPgFrm.Page1.oPag.oCODATT_OR_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_9.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_9.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAMOV_1_11.value==this.w_QTAMOV)
      this.oPgFrm.Page1.oPag.oQTAMOV_1_11.value=this.w_QTAMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oPREZZO_1_13.value==this.w_PREZZO)
      this.oPgFrm.Page1.oPag.oPREZZO_1_13.value=this.w_PREZZO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUBI_1_14.value==this.w_CODUBI)
      this.oPgFrm.Page1.oPag.oCODUBI_1_14.value=this.w_CODUBI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUB2_1_15.value==this.w_CODUB2)
      this.oPgFrm.Page1.oPag.oCODUB2_1_15.value=this.w_CODUB2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLOT_1_18.value==this.w_CODLOT)
      this.oPgFrm.Page1.oPag.oCODLOT_1_18.value=this.w_CODLOT
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAUM1_1_20.value==this.w_QTAUM1)
      this.oPgFrm.Page1.oPag.oQTAUM1_1_20.value=this.w_QTAUM1
    endif
    if not(this.oPgFrm.Page1.oPag.oUNIMIS_1_21.value==this.w_UNIMIS)
      this.oPgFrm.Page1.oPag.oUNIMIS_1_21.value=this.w_UNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_23.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_23.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMA2_1_24.value==this.w_CODMA2)
      this.oPgFrm.Page1.oPag.oCODMA2_1_24.value=this.w_CODMA2
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAT_1_28.value==this.w_CODMAT)
      this.oPgFrm.Page1.oPag.oCODMAT_1_28.value=this.w_CODMAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCEN_OR_1_30.value==this.w_CODCEN_OR)
      this.oPgFrm.Page1.oPag.oCODCEN_OR_1_30.value=this.w_CODCEN_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_OR_1_33.value==this.w_CODCOM_OR)
      this.oPgFrm.Page1.oPag.oCODCOM_OR_1_33.value=this.w_CODCOM_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_OR_1_34.value==this.w_CODATT_OR)
      this.oPgFrm.Page1.oPag.oCODATT_OR_1_34.value=this.w_CODATT_OR
    endif
    if not(this.oPgFrm.Page1.oPag.oUNILOG_1_37.value==this.w_UNILOG)
      this.oPgFrm.Page1.oPag.oUNILOG_1_37.value=this.w_UNILOG
    endif
    if not(this.oPgFrm.Page2.oPag.oTRACCIATO_2_1.value==this.w_TRACCIATO)
      this.oPgFrm.Page2.oPag.oTRACCIATO_2_1.value=this.w_TRACCIATO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_klePag1 as StdContainer
  Width  = 541
  height = 396
  stdWidth  = 541
  stdheight = 396
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODICE_1_9 as StdField with uid="VAWVHWVMHL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 211033126,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=145, Top=17, InputMask=replicate('X',20)

  add object oQTAMOV_1_11 as StdField with uid="EYBLYLQPGR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_QTAMOV", cQueryName = "QTAMOV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 240644614,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=145, Top=42, cSayPict="'@Z '+v_PQ(11)", cGetPict="'@Z '+v_GQ(11)"

  add object oPREZZO_1_13 as StdField with uid="OMHYNGPWVJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PREZZO", cQueryName = "PREZZO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 135606262,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=145, Top=117, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  add object oCODUBI_1_14 as StdField with uid="EEJWTVNQIO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODUBI", cQueryName = "CODUBI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 9444390,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=145, Top=192, InputMask=replicate('X',20)

  func oCODUBI_1_14.mHide()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST='P')
    endwith
  endfunc

  add object oCODUB2_1_15 as StdField with uid="UUNLNGOJKI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODUB2", cQueryName = "CODUB2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 107996122,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=145, Top=217, InputMask=replicate('X',20)

  func oCODUB2_1_15.mHide()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST='P')
    endwith
  endfunc

  add object oCODLOT_1_18 as StdField with uid="WTWKYYTZGA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODLOT", cQueryName = "CODLOT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 207035430,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=145, Top=242, InputMask=replicate('X',20)

  func oCODLOT_1_18.mHide()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST='P')
    endwith
  endfunc

  add object oQTAUM1_1_20 as StdField with uid="LMLOUJIELK",rtseq=14,rtrep=.f.,;
    cFormVar = "w_QTAUM1", cQueryName = "QTAUM1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 113249786,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=145, Top=67, cSayPict="'@Z '+v_PQ(11)", cGetPict="'@Z '+v_GQ(11)"

  add object oUNIMIS_1_21 as StdField with uid="VCEQXHLURF",rtseq=15,rtrep=.f.,;
    cFormVar = "w_UNIMIS", cQueryName = "UNIMIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 184052806,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=145, Top=92, InputMask=replicate('X',3)

  add object oCODMAG_1_23 as StdField with uid="BUHQMACLFK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 242752550,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=145, Top=142, InputMask=replicate('X',5)

  add object oCODMA2_1_24 as StdField with uid="XQTOKIGCRY",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODMA2", cQueryName = "CODMA2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 109568986,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=145, Top=167, InputMask=replicate('X',5)

  add object oCODMAT_1_28 as StdField with uid="ABIAESCCOL",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODMAT", cQueryName = "CODMAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 192420902,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=145, Top=267, InputMask=replicate('X',40)

  func oCODMAT_1_28.mHide()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST='P')
    endwith
  endfunc

  add object oCODCEN_OR_1_30 as StdField with uid="WMVTTGTFXL",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODCEN_OR", cQueryName = "CODCEN_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 95297941,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=145, Top=317, InputMask=replicate('X',15)

  func oCODCEN_OR_1_30.mHide()
    with this.Parent.oContained
      return ( Not inlist(.w_TIPOOBJ_GEST,'tgsor_mdv','tgsac_mdv','tgsve_mdv'))
    endwith
  endfunc

  add object oCODCOM_OR_1_33 as StdField with uid="GGVYJEAVJL",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODCOM_OR", cQueryName = "CODCOM_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 89006485,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=145, Top=342, InputMask=replicate('X',15)

  func oCODCOM_OR_1_33.mHide()
    with this.Parent.oContained
      return ( Not inlist(.w_TIPOOBJ_GEST,'tgsor_mdv','tgsac_mdv','tgsve_mdv'))
    endwith
  endfunc

  add object oCODATT_OR_1_34 as StdField with uid="FLFFMQCDVT",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CODATT_OR", cQueryName = "CODATT_OR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 211558805,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=145, Top=367, InputMask=replicate('X',15)

  func oCODATT_OR_1_34.mHide()
    with this.Parent.oContained
      return (Not inlist(.w_TIPOOBJ_GEST,'tgsor_mdv','tgsac_mdv','tgsve_mdv'))
    endwith
  endfunc

  add object oUNILOG_1_37 as StdField with uid="WNQDJAAHXQ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_UNILOG", cQueryName = "UNILOG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 257387590,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=145, Top=292, InputMask=replicate('X',20)


  add object oBtn_1_39 as StdButton with uid="MJNXVZGNOX",left=486, top=347, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 209309626;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_8 as StdString with uid="RGVPLKFBWT",Visible=.t., Left=6, Top=21,;
    Alignment=1, Width=134, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="CWDNHTYCSD",Visible=.t., Left=6, Top=46,;
    Alignment=1, Width=134, Height=18,;
    Caption="Quantit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="HNQQQGVWOE",Visible=.t., Left=6, Top=121,;
    Alignment=1, Width=134, Height=18,;
    Caption="Prezzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="IYUWAHPDSO",Visible=.t., Left=6, Top=196,;
    Alignment=1, Width=134, Height=18,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST='P')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="ERFFTXGJZG",Visible=.t., Left=6, Top=221,;
    Alignment=1, Width=134, Height=18,;
    Caption="Ubicazione collegata:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST='P')
    endwith
  endfunc

  add object oStr_1_19 as StdString with uid="IZYDRQJHTE",Visible=.t., Left=6, Top=246,;
    Alignment=1, Width=134, Height=18,;
    Caption="Lotto:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST='P')
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="TLGNTHEPAY",Visible=.t., Left=6, Top=96,;
    Alignment=1, Width=134, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="KDMCBNFSQQ",Visible=.t., Left=6, Top=146,;
    Alignment=1, Width=134, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="PGZGJPRMXA",Visible=.t., Left=6, Top=171,;
    Alignment=1, Width=134, Height=18,;
    Caption="Magazzino collegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="RDYVINRTUB",Visible=.t., Left=6, Top=71,;
    Alignment=1, Width=134, Height=18,;
    Caption="Quantit� 1^ U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="MNWBXLIFAQ",Visible=.t., Left=6, Top=271,;
    Alignment=1, Width=134, Height=18,;
    Caption="Matricola:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (.w_TIPOOBJ_GEST='P')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="VXYYKVMKHV",Visible=.t., Left=6, Top=321,;
    Alignment=1, Width=134, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC='V' or  Not inlist(.w_TIPOOBJ_GEST,'tgsor_mdv','tgsac_mdv','tgsve_mdv'))
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="TXKUQMJOXU",Visible=.t., Left=6, Top=346,;
    Alignment=1, Width=134, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (Not inlist(.w_TIPOOBJ_GEST,'tgsor_mdv','tgsac_mdv','tgsve_mdv'))
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="RZJPIOXOBV",Visible=.t., Left=6, Top=371,;
    Alignment=1, Width=134, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (Not inlist(.w_TIPOOBJ_GEST,'tgsor_mdv','tgsac_mdv','tgsve_mdv'))
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="MBIVCWAVHO",Visible=.t., Left=6, Top=321,;
    Alignment=1, Width=134, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC='A' or  Not inlist(.w_TIPOOBJ_GEST,'tgsor_mdv','tgsac_mdv','tgsve_mdv'))
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="VUZDLPLWCB",Visible=.t., Left=6, Top=296,;
    Alignment=1, Width=134, Height=18,;
    Caption="Unit� logistica:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsar_klePag2 as StdContainer
  Width  = 541
  height = 396
  stdWidth  = 541
  stdheight = 396
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRACCIATO_2_1 as StdMemo with uid="ZFTHHSEMWL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_TRACCIATO", cQueryName = "TRACCIATO",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 9303418,;
   bGlobalFont=.t.,;
    Height=387, Width=522, Left=12, Top=8, tabstop = .f., readonly = .t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kle','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
