* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bet                                                        *
*              Elimina tabelle temporanee                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-26                                                      *
* Last revis.: 2014-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bet",oParentObject)
return(i_retval)

define class tgsut_bet as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina tabelle temoranee su database Oracle e Postgres
    do case
      case i_ServerConn[1,6]="Oracle"
        sqlexec(i_ServerConn[1,2], "select owner,table_name from all_tables where table_name like 'CPTMP%'","CursorDropTable")
        if USED("CursorDropTable")
          Select CursorDropTable 
 Go Top
          SCAN
          sqlexec(i_ServerConn[1,2],"drop table "+alltrim(owner)+"."+alltrim(table_name))
          Select CursorDropTable
          ENDSCAN
          Select CursorDropTable 
 Use
        endif
        ah_errormsg("Operazione terminata")
      case i_ServerConn[1,6]="PostgreSQL"
        sqlexec(i_ServerConn[1,2], "select table_name from information_schema.tables where table_type='BASE TABLE' and table_schema='public' and table_name ilike 'CPTMP%'","CursorDropTable")
        if USED("CursorDropTable")
          Select CursorDropTable 
 Go Top
          SCAN
          sqlexec(i_ServerConn[1,2],"drop table "+alltrim(table_name))
          Select CursorDropTable
          ENDSCAN
          Select CursorDropTable 
 Use
        endif
        ah_errormsg("Operazione terminata")
      otherwise
        ah_errormsg("Funzionalitą non disponibile su database SQLServer")
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
