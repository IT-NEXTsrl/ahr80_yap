* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bai                                                        *
*              Abbinamento partite in distinta                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_337]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-01                                                      *
* Last revis.: 2013-04-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bai",oParentObject,m.pOper)
return(i_retval)

define class tgste_bai as StdBatch
  * --- Local variables
  pOper = space(5)
  w_OK = .f.
  w_ZOOM = space(10)
  w_ZOOM1 = .NULL.
  w_ZOOM2 = space(10)
  w_MESS = space(10)
  w_ABICOD = space(5)
  w_CODBAN = space(15)
  w_PTNUMCOR = space(25)
  w_MARK = .f.
  w_PADRE = .NULL.
  w_GSTE_ADI = .NULL.
  w_OBJCTRL = .NULL.
  w_PADRE = .NULL.
  w_MESS1 = space(10)
  w_DOPPIO = space(15)
  w_LTIPPAR = space(1)
  w_LCODEFF = space(15)
  w_FILROW = 0
  w_DATDOCINI = ctod("  /  /  ")
  w_DATDOCFIN = ctod("  /  /  ")
  w_ALFDOCFIN = space(10)
  w_ALFDOCINI = space(10)
  w_NUMDOCFIN = 0
  w_NUMDOCINI = 0
  w_CODZON = space(3)
  w_CONSUP = space(15)
  w_CATCOM = space(3)
  w_LOOP = 0
  w_PUN = 0
  * --- WorkFile variables
  PAR_TITE_idx=0
  DIS_ZOOM_idx=0
  BAN_CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Abbinamento Partite/Scadenze in Distinta (da GSTE_KAI)
    this.oParentObject.w_SCELTA = " "
    this.w_GSTE_ADI = this.oParentObject.oParentObject.oParentObject
    do case
      case this.pOper="CALC" OR this.pOper="CARI"
        * --- Se CARI, filtro per elenco di clienti/fornitori, altrimenti per intervallo di cienti/fornitori
        this.oParentObject.w_SCELTA = "A"
        this.w_PADRE = This.oParentObject
        if this.pOper="CARI"
          * --- Utilizzo il temporaneo del figlio integrato in pag.2 per capire se � stato inserito
          *     un codice ripetuto. 
          *     Questo per evitare il messaggio di chiave gi� utilizzata e che vengano sbiancati i codici 
          *     percedentemente inseriti.
          select Count(*) As CONTA, t_DICODCON from (this.w_PADRE.GSTE_MCF.cTrsName) ; 
 Where t_DICODCON Is Not Null And not deleted(); 
 group by t_DICODCON having CONTA >= 2 into cursor ELENCO
          this.w_DOPPIO = Nvl ( Elenco.t_DICODCON , "" )
           
 Select ELENCO 
 Use
          if Not Empty ( this.w_DOPPIO )
            ah_ErrorMsg("Codice intestatario %1 ripetuto",,"", ALLTRIM(this.w_DOPPIO) )
            i_retcode = 'stop'
            return
          endif
          this.oParentObject.w_SCELTA = "B"
          this.oParentObject.w_TESCAR = .T.
          * --- Salvo il contenuto della maschera sul database (non eseguo una ECPSAVE perch� voglio mantenere la finestra aperta)
          this.w_PADRE.Save_GSTE_MCF(.f.)     
          this.w_PADRE.mReplace(.T.)     
          * --- Ricarico il record per aggiornare il cpccchk (altrimenti al successivo applica
          *     riceverei record modificato da altro utente)
          this.w_PADRE.GSTE_MCF.LoadRec()     
          this.oParentObject.w_CODCON = SPACE(15)
          this.oParentObject.w_CODCON1 = SPACE(15)
          this.oParentObject.w_CODBAN1 = SPACE(15)
          this.oParentObject.w_DESBAN1 = SPACE(40)
          this.oParentObject.w_DESCRI = SPACE(40)
          this.oParentObject.w_DESCR1 = SPACE(40)
          this.w_CODBAN = this.oParentObject.w_CODBAN2
          this.oParentObject.w_DESEFF = Space(40)
          this.w_LTIPPAR = this.oParentObject.w_TIPPAR1
          this.w_LCODEFF = this.oParentObject.w_CODEFF1
          this.oParentObject.w_TIPPAR = "T"
          this.oParentObject.w_CODEFF = Space(15)
        else
          * --- Delete from DIS_ZOOM
          i_nConn=i_TableProp[this.DIS_ZOOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIS_ZOOM_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"DICODDIS = "+cp_ToStrODBC(this.oParentObject.w_NUMDIS);
                   )
          else
            delete from (i_cTable) where;
                  DICODDIS = this.oParentObject.w_NUMDIS;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          this.oParentObject.w_CODBAN2 = SPACE(15)
          this.oParentObject.w_DESBAN2 = SPACE(40)
          this.w_CODBAN = this.oParentObject.w_CODBAN1
          this.w_PADRE.GSTE_MCF.LoadRec()     
          this.w_PADRE.GSTE_MCF.w_DICODDIS = this.oParentObject.w_NUMDIS
          this.w_LTIPPAR = this.oParentObject.w_TIPPAR
          this.w_LCODEFF = this.oParentObject.w_CODEFF
          this.oParentObject.w_TIPPAR1 = "T"
          this.oParentObject.w_CODEFF1 = Space(15)
          this.oParentObject.w_DESEFF1 = Space(40)
        endif
        this.w_FILROW = IIF(this.w_LTIPPAR="A",-1,-3)
        * --- Creo il temporaneo con l'elenco delle partite non saldate
        * --- Il filtro sulla data documento lo applico sulla GSTE0KAI, visto 
        *     che, a norma, scadenza appartenenti alla stessa partita debbono
        *     avere questo campo uguale...
        this.w_DATDOCINI = this.w_GSTE_ADI.w_DATDOCINI
        this.w_DATDOCFIN = this.w_GSTE_ADI.w_DATDOCFIN
        this.w_NUMDOCINI = this.w_GSTE_ADI.w_NUMDOCINI
        this.w_NUMDOCFIN = this.w_GSTE_ADI.w_NUMDOCFIN
        this.w_ALFDOCFIN = this.w_GSTE_ADI.w_ALFDOCINI
        this.w_ALFDOCINI = this.w_GSTE_ADI.w_ALFDOCFIN
        this.w_CODZON = this.w_GSTE_ADI.w_CODZON
        this.w_CONSUP = this.w_GSTE_ADI.w_CONSUP
        this.w_CATCOM = this.w_GSTE_ADI.w_CATCOM
        GSTE_BPA (this, this.oParentObject.w_SCAINI , this.oParentObject.w_SCAFIN , this.oParentObject.w_TIPO , this.oParentObject.w_CODCON, this.oParentObject.w_CODCON1 , this.oParentObject.w_CODVAL , "", this.oParentObject.w_FLRB1,this.oParentObject.w_FLBO1,this.oParentObject.w_FLRD1,this.oParentObject.w_FLRI1,this.oParentObject.w_FLMA1,this.oParentObject.w_FLCA1, this.oParentObject.w_NUMDIS+this.oParentObject.w_SCELTA, "S", cp_CharToDate("  -  -  ") )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        vq_exec("query\GSTE0KAI.VQR",this,"PAR_APE")
        Select Sum(Abs(totimp)*iif(flcrsa="S",-1,1)) As Totale, IIF(NOT EMPTY(nvl(PTSERRIF,"")) AND FLCRSA<>"C" ,PTSERRIF,PTSERIAL) AS SERIAL, ; 
 IIF(nvl(PTORDRIF,0)<>0 AND FLCRSA<>"C",PTORDRIF,PTROWORD) AS RIGA, IIF(nvl(PTNUMRIF,0)<>0 AND FLCRSA<>"C", PTNUMRIF,CPROWNUM) AS NUMROW FROM PAR_APE; 
 Group By 2,3,4 ORDER BY 2,3,4 ; 
 Having Totale<0 into Cursor cancella NoFilter
        Delete From PAR_APE Where ; 
 ptserial +str(ptroword)+str(cprownum) In; 
 ( Select serial +str(riga)+str(numrow); 
 From cancella )
        Delete From PAR_APE Where ; 
 ptserrif +str(ptordrif)+str(ptnumrif) In; 
 ( Select serial +str(riga)+str(numrow); 
 From cancella )
         
 Use in cancella
        * --- Drop temporary table TMP_PART_APE
        i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_PART_APE')
        endif
        this.w_ABICOD = IIF(this.oParentObject.w_FLCABI1="S",this.oParentObject.w_CODABI1,SPACE(5))
        if RECCOUNT("PAR_APE")>0
          Cur = WrCursor("PAR_APE")
          GSTE_BCP(this,"D","PAR_APE",this.oParentObject.w_TIPSCA , this.oParentObject.w_FLBANC1 , this.oParentObject.w_TIPDIS )
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Filtro per pagamento della distinta e per Ns Banca, lo faccio adesso perch� prima devo calcolare la parte residua
          *     Eseguo una go top per problema riscontrato in Oracle\Db2 nella select successiva che ripescava record eliminati nella
          *     precedente delete
          select Par_ape 
 Go top
           
 Select * From PAR_APE Where TIPPAG IN (this.oParentObject.w_FLRD1,this.oParentObject.w_FLBO1,this.oParentObject.w_FLCA1,this.oParentObject.w_FLRI1,this.oParentObject.w_FLMA1,this.oParentObject.w_FLRB1) ; 
 And (NVL(BANNOS,SPACE(15))=this.w_CODBAN OR EMPTY(this.w_CODBAN)) And (NVL(CODABI,SPACE(5))=this.w_ABICOD ; 
 OR EMPTY(this.w_ABICOD)) AND (NVL(CICONEFA,SPACE(15))=this.w_LCODEFF OR EMPTY(this.w_LCODEFF)) ; 
 And (Empty( this.w_DATDOCINI ) Or Cp_Todate(DatDoc)>=this.w_DATDOCINI) ; 
 And (Empty( this.w_DATDOCFIN ) Or Cp_Todate(DatDoc)<=this.w_DATDOCFIN) ; 
 And (this.w_NUMDOCINI=0 Or Nvl(NumDoc,0)>=this.w_NUMDOCINI) ; 
 And (this.w_NUMDOCFIN=0 Or Nvl(NumDoc,0)<=this.w_NUMDOCFIN) ; 
 And (Empty( this.w_ALFDOCINI) Or Nvl(AlfDoc,Space(10))>=this.w_ALFDOCINI) ; 
 And (Empty( this.w_ALFDOCFIN) Or Nvl(AlfDoc,Space(10))<=this.w_ALFDOCFIN) ; 
 AND ((this.w_LTIPPAR $ "A-T" AND PTROWORD>=this.w_FILROW) OR (this.w_LTIPPAR = "C" AND PTROWORD =this.w_FILROW)) AND Not Deleted() Into Cursor Selepart NoFilter
          Cur = WrCursor("SELEPART")
        else
          * --- Non ci sono partite non saldate, se seleziono solo le saldate, non serve fare questo gito...
           
 Select Par_Ape 
 Zap 
 Select * From Par_Ape Into Cursor SelePart NoFilter
          Cur = WrCursor("SELEPART")
        endif
        * --- Chiudo Cursore Partite Aperte
         
 Use in Par_Ape
        * --- Creo lo zoom con una query vuota...
        This.OparentObject.NotifyEvent("Abbina")
        * --- Setta Proprieta' Campi del Cursore
        this.w_LOOP = 1
        do while this.w_LOOP<=this.oParentObject.w_CalcZoom.grd.ColumnCount
          this.oParentObject.w_CALCZOOM.grd.Columns[ this.w_LOOP ].Enabled = "XCHK" $ UPPER( this.oParentObject.w_CalcZoom.grd.Columns[ this.w_LOOP ].ControlSource)
          this.w_LOOP = this.w_LOOP + 1
        enddo
         
 Select SELEPART 
 Go Top
        * --- Riempio lo zoom con SELEPART
        do while Not Eof( "Selepart" )
          if Not Deleted()
             
 Scatter memVar 
 Select ( this.oParentObject.w_CALCZOOM.cCursor ) 
 Append Blank 
 Gather MemVar
          endif
          if Not Eof( "Selepart" )
             
 Select SelePart 
 Skip
          endif
        enddo
        Select ( this.oParentObject.w_CALCZOOM.cCursor ) 
 GO TOP
        this.oParentObject.w_CALCZOOM.Refresh()     
        * --- Chiude cursore query
        Use in SELEPART
        if this.pOper="CARI"
          * --- Eseguo la activate pag2 invece di utilizzare la set focus perch� con la set focus lo zoom rimane disabilitato.
          *     In questo modo mi sposto comunque sulla prima pagina ma lo zoom funziona correttamente.
          ah_Msg("Ricerca terminata.....",.T.)
          this.w_PADRE.oPgFrm.ActivePage = 1
          this.w_PADRE.oPgFrm.Click()     
        endif
      case this.pOper="SELE"
        * --- Seleziona/Deseleziona Tutto
        this.oParentObject.w_FLSELE = IIF ( this.oParentObject.w_SELEZI="S" ,1, 0 )
        UPDATE ( this.oParentObject.w_CalcZoom.cCursor ) SET XCHK = this.oParentObject.w_FLSELE
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="AGGIO"
        * --- Abbina Partite
        NC = this.oParentObject.w_CALCZOOM.cCursor
        if this.oParentObject.w_FLBANC1 $ "AS"
          * --- Controllo Presenza Banca di Appoggio
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Verifico se tutte gli effetti determinati dalla selezione sono positivi...
        *     Se non lo sono visualizzo il loro elenco e non proseguo..
        this.w_MESS = GSTE_BCR(This,"C",this.oParentObject.w_CALCZOOM.cCursor,this.oParentObject.w_FLBANC1,this.oParentObject.w_TIPDIS,this.oParentObject.w_TIPSCA)
        if Not Empty( this.w_MESS )
          * --- Esco se qualche effetto � negativo
          ah_ErrorMsg("%1",,"", this.w_MESS )
          i_retcode = 'stop'
          return
        endif
        this.w_MARK = .F.
        this.w_OK = .F.
        * --- Try
        local bErr_03A84F40
        bErr_03A84F40=bTrsErr
        this.Try_03A84F40()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Errore durante l'aggiornamento; Operazione abbandonata",,"")
        endif
        bTrsErr=bTrsErr or bErr_03A84F40
        * --- End
        if this.w_OK
          * --- Lancia lo Zoom
          this.w_GSTE_ADI.w_FLORI = .F.
          this.w_GSTE_ADI.NotifyEvent("Save")     
          this.w_GSTE_ADI.GotFocus()     
          this.w_GSTE_ADI.EcpSave()     
          * --- La EcpQuery viene eseguita per passare la maschera in modalit� 'Modifica'.
          *     Infatti con la build 49 per passare da 'Carica' a 'Modifica' occorre eseguire la EcpQuery.
          this.w_GSTE_ADI.EcpQuery()     
          this.w_GSTE_ADI.EcpFilter()     
          this.w_GSTE_ADI.w_DINUMDIS = this.oParentObject.w_DISERIAL
          this.w_GSTE_ADI.EcpSave()     
          this.w_GSTE_ADI.EcpEdit()     
          * --- Questi comandi servono per forzare la picture.
          *     Nel passare da 'carica' a 'modifica', il control rimaneva uguale al campo pttotimp e non veniva lanciata 
          *     la programmaticchange(). In questo modo viene forzata.
          this.w_OBJCTRL = this.w_GSTE_ADI.GSTE_MPD.GETBODYCTRL("w_PTTOTIMP")
          this.w_OBJCTRL.programmaticchange()     
          * --- Chiudo la Maschera
          This.oParentObject.ecpQuit()
        else
          ah_ErrorMsg("Non ci sono scadenze da abbinare",,"")
        endif
      case this.pOper="CHEK"
        this.w_PUN = Recno(this.oParentObject.w_CALCZOOM.cCursor)
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        Go this.w_PUN
    endcase
  endproc
  proc Try_03A84F40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Cicla sui record Selezionati
    SELECT (NC)
    GO TOP
    SCAN FOR XCHK<>0 AND NVL(TOTIMP, 0)<>0
    SELECT (this.w_GSTE_ADI.GSTE_MPD.cTrsName)
    this.w_GSTE_ADI.GSTE_MPD.AddRow()     
    if NOT this.w_MARK
      this.w_GSTE_ADI.GSTE_MPD.MarkPos()     
      this.w_MARK = .T.
    endif
    SELECT (NC)
    this.w_GSTE_ADI.GSTE_MPD.w_PTFLIMPE = IIF(EMPTY(this.oParentObject.w_CODCAU) AND g_COGE="S", "DI", "  ")
    this.w_GSTE_ADI.GSTE_MPD.w_PTNUMPAR = NVL(NUMPAR, SPACE(14))
    this.w_GSTE_ADI.GSTE_MPD.w_PTDATSCA = CP_TODATE(DATSCA)
    this.w_GSTE_ADI.GSTE_MPD.w_PTTIPCON = TIPCON
    this.w_GSTE_ADI.GSTE_MPD.w_PTCODCON = CODCON
    this.w_GSTE_ADI.GSTE_MPD.w_PTCODVAL = CODVAL
    * --- La partita in distinta (-2) avr� il segno contrario della partita aperta
    this.w_GSTE_ADI.GSTE_MPD.w_PT_SEGNO = IIF( SEGNO="D", "A", "D")
    this.w_GSTE_ADI.GSTE_MPD.w_PTTOTIMP = TOTIMP
    this.w_GSTE_ADI.GSTE_MPD.w_PTMODPAG = NVL(MODPAG, " ")
    this.w_GSTE_ADI.GSTE_MPD.w_PTBANNOS = NVL(BANNOS, " ")
    this.w_GSTE_ADI.GSTE_MPD.w_PTBANAPP = NVL(BANAPP, " ")
    this.w_GSTE_ADI.GSTE_MPD.w_PTCAOVAL = NVL(CAOVAL, 0)
    this.w_GSTE_ADI.GSTE_MPD.w_PTCAOAPE = NVL(CAOAPE, 0)
    this.w_GSTE_ADI.GSTE_MPD.w_PTDATAPE = CP_TODATE(DATAPE)
    this.w_GSTE_ADI.GSTE_MPD.w_PTNUMDOC = NVL(NUMDOC, 0)
    this.w_GSTE_ADI.GSTE_MPD.w_PTALFDOC = NVL(ALFDOC, Space(10))
    this.w_GSTE_ADI.GSTE_MPD.w_PTDATDOC = CP_TODATE(DATDOC)
    this.w_GSTE_ADI.GSTE_MPD.w_PTIMPDOC = NVL(IMPDOC, 0)
    this.w_GSTE_ADI.GSTE_MPD.w_PTFLINDI = IIF(EMPTY(NVL(FLINDI, 0)), " ", "S")
    this.w_GSTE_ADI.GSTE_MPD.w_PTFLRAGG = NVL(FLRAGG," ")
    this.w_GSTE_ADI.GSTE_MPD.w_PTCODAGE = NVL(CODAGE,SPACE(5))
    this.w_GSTE_ADI.GSTE_MPD.w_PTFLVABD = NVL(FLVABD," ")
    this.w_GSTE_ADI.GSTE_MPD.w_PTDESRIG = NVL(DESRIG, " ")
    this.w_GSTE_ADI.GSTE_MPD.w_PTDATREG = CP_TODATE(this.w_GSTE_ADI.w_DIDATDIS)
    this.w_GSTE_ADI.GSTE_MPD.w_PTSERRIF = PTSERIAL
    this.w_GSTE_ADI.GSTE_MPD.w_PTORDRIF = PTROWORD
    this.w_GSTE_ADI.GSTE_MPD.w_PTNUMRIF = CPROWNUM
    this.w_GSTE_ADI.GSTE_MPD.w_PTNUMCOR = NVL(PTNUMCOR, SPACE(25))
    this.w_GSTE_ADI.GSTE_MPD.w_PTDATVAL = IIF(this.w_GSTE_ADI.w_DATEFF="S",CP_TODATE(DATSCA), CP_TODATE(this.w_GSTE_ADI.w_DIDATVAL))
    ah_Msg("Aggiornamento scadenza del: %1",.T.,.F.,.F., DTOC(this.w_GSTE_ADI.GSTE_MPD.w_PTDATSCA) )
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    this.w_GSTE_ADI.GSTE_MPD.TrsFromWork()     
    this.w_OK = .T.
    SELECT (NC)
    ENDSCAN
    if this.w_MARK
      this.w_GSTE_ADI.GSTE_MPD.RePos()     
      * --- Marco il figlio come modificato
      this.w_GSTE_ADI.GSTE_MPD.bUpdated = .t.
    endif
    * --- Delete from DIS_ZOOM
    i_nConn=i_TableProp[this.DIS_ZOOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_ZOOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DICODDIS = "+cp_ToStrODBC(this.oParentObject.w_NUMDIS);
             )
    else
      delete from (i_cTable) where;
            DICODDIS = this.oParentObject.w_NUMDIS;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla banca Appoggio/Nostra Banca
    this.w_OK = .T.
    * --- seleziono il cursore
    SELECT &NC
    GO TOP
    * --- Cicla sui record Selezionati
    do case
      case this.oParentObject.w_FLBANC1="A"
        * --- CONTROLLA SE LE PARTITE SELEZIONATE HANNO LA BANCA DI APPOGGIO
        *     O IL CODICE IBAN O IL CODICE BBAN
        SCAN FOR XCHK<>0 AND (EMPTY(NVL(BANAPP,"")) OR (this.oParentObject.w_TIPDIS $ "BO-BE" AND this.oParentObject.w_TIPSCA="F" AND this.oParentObject.w_FLIBAN1="S" AND ((EMPTY(Nvl(AN__IBAN," ")) OR EMPTY(Nvl(AN__BBAN," "))))))
        * --- METTO A false  IL CHECK
        REPLACE XCHK WITH 0
        do case
          case EMPTY(NVL(BANAPP, ""))
            ah_ErrorMsg("La partita: %1 del %2%0non ha la banca di appoggio",,"", ALLTRIM(NVL(NUMPAR,"")), DTOC(CP_TODATE(DATSCA)) )
            this.w_OK = .F.
          case EMPTY(NVL(AN__IBAN, ""))
            ah_ErrorMsg("La partita: %1 del %2%0non ha il codice IBAN",,"", ALLTRIM(NVL(NUMPAR,"")), DTOC(CP_TODATE(DATSCA)) )
            this.w_OK = .F.
          case EMPTY(NVL(AN__BBAN, ""))
            ah_ErrorMsg("La partita: %1 del %2%0non ha il codice BBAN",,"", ALLTRIM(NVL(NUMPAR,"")), DTOC(CP_TODATE(DATSCA)) )
            this.w_OK = .F.
        endcase
        ENDSCAN
      case this.oParentObject.w_FLBANC1="S"
        SCAN FOR XCHK<>0
        * --- METTO A false  IL CHECK
        this.w_MESS = "La partita:"+ALLTRIM(NVL(NUMPAR,""))+"Del"+DTOC(CP_TODATE(DATSCA))
        this.w_MESS = this.w_MESS+CHR(13)+"ha la nostra banca di presentazione"
        do case
          case EMPTY(NVL(BANNOS,""))
            REPLACE XCHK WITH 0
            ah_ErrorMsg("La partita: %1 del %2%0ha la nostra banca di presentazione inesistente",,"", ALLTRIM(NVL(NUMPAR,"")), DTOC(CP_TODATE(DATSCA)) )
            this.w_OK = .F.
          case (this.oParentObject.w_TIPDIS $ "BO-BE" AND this.oParentObject.w_TIPSCA="C") OR (this.oParentObject.w_TIPDIS $ "RB-CA-RI" AND this.oParentObject.w_TIPSCA="F")
            if BANNOS<>this.oParentObject.w_BANRIF
              REPLACE XCHK WITH 0
              ah_ErrorMsg("La partita: %1 del %2%0ha la nostra banca di presentazione diversa dalla banca di presentazione distinta",,"", ALLTRIM(NVL(NUMPAR,"")), DTOC(CP_TODATE(DATSCA)) )
              this.w_OK = .F.
            endif
          case EMPTY(NVL(AN__IBAN, "")) AND this.oParentObject.w_TIPDIS $ "BO-BE" AND this.oParentObject.w_TIPSCA="F" AND this.oParentObject.w_FLIBAN1="S"
            REPLACE XCHK WITH 0
            ah_ErrorMsg("La partita: %1 del %2%0non ha il codice IBAN",,"", ALLTRIM(NVL(NUMPAR,"")), DTOC(CP_TODATE(DATSCA)) )
            this.w_OK = .F.
          case EMPTY(NVL(AN__BBAN, "")) AND this.oParentObject.w_TIPDIS $ "BO-BE" AND this.oParentObject.w_TIPSCA="F" AND this.oParentObject.w_FLIBAN1="S"
            REPLACE XCHK WITH 0
            ah_ErrorMsg("La partita: %1 del %2%0non ha il codice BBAN",,"", ALLTRIM(NVL(NUMPAR,"")), DTOC(CP_TODATE(DATSCA)) )
            this.w_OK = .F.
        endcase
        ENDSCAN
    endcase
    if this.w_OK=.F.
      * --- ALMENO UNA PARTITA NON HA LA BANCA DI APPOGGIO
      if not ah_YesNo("Assegno comunque le altre partite alla distinta?")
        * --- esco
        i_retcode = 'stop'
        return
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola totale scadenza selezionate
     
 Select ( this.oParentObject.w_CALCZOOM.cCursor ) 
 GO TOP 
 SUM TOTIMP*iif(TIPCON $ "C-G",1,-1)*iif( SEGNO="A" AND TIPCON<>"G" ,-1,1 )*IIF( TIPCON=this.oParentObject.w_TIPSCA or (TIPCON="G" AND ((SEGNO="A" AND this.oParentObject.w_TIPSCA="F") or (SEGNO="D" AND this.oParentObject.w_TIPSCA="C"))) , 1 , -1 ) TO this.oParentObject.w_IMPORTO FOR XCHK=1
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='DIS_ZOOM'
    this.cWorkTables[3]='BAN_CONTI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
