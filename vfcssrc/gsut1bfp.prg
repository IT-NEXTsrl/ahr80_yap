* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1bfp                                                        *
*              Penna ottica formula 734                                        *
*                                                                              *
*      Author: Emiliano Orrico                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-06-06                                                      *
* Last revis.: 2004-05-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut1bfp",oParentObject,m.pCursore,m.pTXTFile,m.pObjMsg,m.p_ArrayFieldName,m.p_ArrayFieldPosition)
return(i_retval)

define class tgsut1bfp as StdBatch
  * --- Local variables
  w_BITDAT = 0
  w_BUFRIC = 0
  w_CURSORE = space(10)
  w_TL = 0
  w_TIME = 0
  w_FILENAME = space(254)
  w_FILENAME1 = space(254)
  w_PRGGES = space(8)
  w_RITARD = 0
  w_PARITA = 0
  w_BUFTRA = 0
  w_BAURAT = 0
  w_BISTOP = 0
  w_PORTA = 0
  w_ETB = 0
  w_PARITY = space(1)
  w_LETTO = .f.
  w_RXCOD = space(41)
  w_OLEOBJECT = .NULL.
  w_CR = 0
  w_BAUD = space(5)
  w_TROV = .f.
  w_RXQTA = 0
  w_RITARDO = 0
  w_BK1 = space(10)
  w_STRINGA = space(50)
  w_RECINIT = .f.
  w_RN = 0
  w_NLET = 0
  w_BK2 = space(10)
  w_INSTRING = space(50)
  w_CONT = 0
  w_STOP = space(1)
  w_FOUTFILE = 0
  w_T1 = 0
  w_STRQTA = space(18)
  w_STRDAINS = space(250)
  w_T2 = 0
  w_FINFILE = 0
  w_GNEND = 0
  w_GNTOP = 0
  w_CARAT = space(50)
  w_RN1 = 0
  pCursore = space(10)
  pTXTFile = space(254)
  pObjMsg = .NULL.
  p_ArrayFieldName = space(40)
  p_ArrayFieldPosition = space(40)
  w_MESS = space(0)
  * --- WorkFile variables
  DIS_HARD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Programma di gestione per la penna ottica Formula 734.
    *     ==================================================================================
    *     Per programmare la penna � necessario caricare tramite il software SYSTOOL il file 734AE1.HEX.
    *     In questa modalit� per ogni articolo carico il CODICE e la QUANTITA'.
    *     Per poter interagire con la penna carico l'OCX MSCOMM32.OCX e setto le impostazioni della porta a cui
    *     � collegata la penna leggendo i dati da DIS_HARD.
    *     Le impostazioni per la porta sono le seguenti:
    *     
    *     RITARDO: 99
    *     BUFFER RICEZIONE: 4098
    *     BUFFER TRASMISSIONE: 2048
    *     BAUD: 9600
    *     PARITA': even
    *     BIT STOP: 1
    *     BIT DATI: 7
    *     
    *     Il batch legge dalla penna i dati relativi agli articoli e li scrive su di un file di testo creato nella directory temporanea
    *     dell'utente.
    *     Viene quindi lanciato il batch GSUT_BDT che, dato il file di testo appena creato, riempie il cursore passatogli come parametro.
    *     
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Leggo dalla tabella DIS_HARD le impostazioni relative alla penne ottica
    * --- Read from DIS_HARD
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIS_HARD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_HARD_idx,2],.t.,this.DIS_HARD_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DHPROGRA,DHRITARD,DHBUFRIC,DHBUFTRA,DH__BAUD,DHPARITA,DHBITSTP,DHBITDAT,DHNUMPOR"+;
        " from "+i_cTable+" DIS_HARD where ";
            +"DHCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODDISP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DHPROGRA,DHRITARD,DHBUFRIC,DHBUFTRA,DH__BAUD,DHPARITA,DHBITSTP,DHBITDAT,DHNUMPOR;
        from (i_cTable) where;
            DHCODICE = this.oParentObject.w_CODDISP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PRGGES = NVL(cp_ToDate(_read_.DHPROGRA),cp_NullValue(_read_.DHPROGRA))
      this.w_RITARD = NVL(cp_ToDate(_read_.DHRITARD),cp_NullValue(_read_.DHRITARD))
      this.w_BUFRIC = NVL(cp_ToDate(_read_.DHBUFRIC),cp_NullValue(_read_.DHBUFRIC))
      this.w_BUFTRA = NVL(cp_ToDate(_read_.DHBUFTRA),cp_NullValue(_read_.DHBUFTRA))
      this.w_BAURAT = NVL(cp_ToDate(_read_.DH__BAUD),cp_NullValue(_read_.DH__BAUD))
      this.w_PARITA = NVL(cp_ToDate(_read_.DHPARITA),cp_NullValue(_read_.DHPARITA))
      this.w_BISTOP = NVL(cp_ToDate(_read_.DHBITSTP),cp_NullValue(_read_.DHBITSTP))
      this.w_BITDAT = NVL(cp_ToDate(_read_.DHBITDAT),cp_NullValue(_read_.DHBITDAT))
      this.w_PORTA = NVL(cp_ToDate(_read_.DHNUMPOR),cp_NullValue(_read_.DHNUMPOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.w_PARITA="0"
        this.w_PARITY = "N"
      case this.w_PARITA="1"
        this.w_PARITY = "O"
      case this.w_PARITA="2"
        this.w_PARITY = "E"
      case this.w_PARITA="3"
        this.w_PARITY = "M"
      case this.w_PARITA="4"
        this.w_PARITY = "S"
    endcase
    CREATE CURSOR ( this.w_CURSORE) (CODART C(41), QTA N(12,6))
    if not AH_YESNO("Inserire il lettore nel calamaio e premere <S�>. <No> per uscire")
      * --- L'utente ha premuto NO
      i_retcode = 'stop'
      return
    endif
    * --- Aggiungo l'OCX  (MSCOMM32.OCX) al Batch
    * --- Per farlo occorre creare una form (un batch non � un container valido per gli OLE)
    fOleForm = create ("FORM")
    fOleForm.visible= .f.
    fOleForm.Addobject("OleObject","OleControl","MSCOMMLib.MSComm")
    this.w_OLEOBJECT = fOleForm.OleObject
    * --- Definisco la porta da aprire
    this.w_OLEOBJECT.CommPort = this.w_PORTA
    * --- Setto la porta
    this.w_OLEOBJECT.Settings = ALLTRIM(STR(this.w_BAURAT,5,0))+","+this.w_PARITY+","+ALLTRIM(STR( this.w_BITDAT ,2,0))+","+ALLTRIM(STR(this.w_BISTOP,1,0))
    * --- Setto Dimensione Buffer di Ricezione
    this.w_OLEOBJECT.InBufferSize = this.w_BUFRIC
    * --- Apro la Porta ???
    this.w_OLEOBJECT.PortOpen = .T.
    * --- Definisco il numero di caratteri letti per volta
    this.w_OLEOBJECT.InputLen = 1
    * --- Fase ricezione dati
    this.w_INSTRING = this.w_OLEOBJECT.Input
    this.w_FILENAME = tempadhoc()+"\"+SYS(2015)
    this.w_FOUTFILE = FCreate(this.w_FILENAME)
    this.w_CONT = 0
    * --- Ciclo di Sincronizzazione
    AddMsgNL("Inizio sincronizzazione...",this.pObjMsg)
    do while asc(this.w_INSTRING)=0
      this.w_INSTRING = this.w_OLEOBJECT.Input
      this.w_CONT = this.w_CONT+1
      this.w_MESS = Ah_MsgFormat("Connessione in corso")
      if AT(this.w_MESS, this.pObjMsg.w_Msg )>0
        this.w_StrDaIns = LEFT( this.pObjMsg.w_Msg , AT(this.w_MESS, this.pObjMsg.w_Msg )-1)
        this.pObjMsg.w_Msg = ""
      else
        this.w_StrDaIns = ""
      endif
      AddMsg("%1",this.pObjMsg,this.w_StrDaIns+this.w_MESS+replicate(chr(45),MOD(this.w_CONT,60))+chr(62))
      if this.w_CONT>100000
        if AH_YESNO("Impossibile connettersi alla penna ottica. Ritentare?")
          * --- Se non riesce a sincronizzarsi chiede di ripetere il ciclo (per evitare un Loop infinito)
          this.w_CONT = 0
        else
          i_retcode = 'stop'
          return
        endif
      endif
    enddo
    * --- Ritardo prima della lettura dati
    AddMsgNL("%0Sincronizzazione terminata",this.pObjMsg)
    if type("g_PENSYNC")="N"
      this.w_TIME = g_PENSYNC
    else
      this.w_TIME = 4
    endif
    AddMsgNL("Standby di %1 secondi",this.pObjMsg,alltrim(str(this.w_TIME)))
    this.w_TL = 1
    do while this.w_TL <= this.w_TIME
      wait "" timeout 1
      AddMsgNL("%1",this.pObjMsg,space(2)+str(this.w_TL)+repl(chr(46),3))
      this.w_TL = this.w_TL + 1
    enddo
    * --- Ciclo Lettura dati
    this.w_OLEOBJECT.InputLen = 256
    this.w_NLET = 1
    do while this.w_OLEOBJECT.InBufferCount>0
      this.w_INSTRING = this.w_OLEOBJECT.Input
      this.w_T1 = Seconds()
      this.w_T2 = Seconds()+this.w_RITARDO/1000
      do while this.w_T1<=this.w_T2
        this.w_T1 = Seconds()
      enddo
      this.w_NLET = this.w_NLET+1
      Write=fWrite(this.w_FOUTFILE,this.w_INSTRING)
    enddo
    Chiudo=Fclose(this.w_FOUTFILE)
    * --- Chiudo la connessione con la penna
    this.w_OLEOBJECT.PortOpen = .F.
    * --- Distruggo la Form
    AddMsgNL("Lettura dati terminata. Attendere completamento elaborazione",this.pObjMsg)
    fOleForm.release
    this.w_FOUTFILE = FOpen(this.w_FILENAME)
    this.w_FILENAME1 = tempadhoc()+"\"+SYS(2015)
    this.w_FINFILE = FCreate(this.w_FILENAME1)
    this.w_GNEND = FSeek(this.w_FOUTFILE,0,2)
    this.w_GNTOP = FSeek(this.w_FOUTFILE,0)
    if this.w_GNEND<=0
      AddMsgNL("Problemi in ricezione dati" ,this.pObjMsg)
    else
      this.w_STRINGA = ""
      this.w_CARAT = FGet(this.w_FOUTFILE,1)
      do while Len ( this.w_CARAT ) >0 And Not fEof(this.w_FOUTFILE)
        do while Asc ( this.w_CARAT ) <> 23 And Asc ( this.w_CARAT ) <>3 And Not fEof(this.w_FOUTFILE)
          if Asc(this.w_CARAT) <> 13
            this.w_STRINGA = this.w_STRINGA + this.w_CARAT
          endif
          this.w_CARAT = FGet(this.w_FOUTFILE,1)
        enddo
        AddMsgNL("Letto dato %1",this.pObjMsg,this.w_STRINGA)
        * --- Scrivo nel file di testo
        Write=fWrite(this.w_FINFILE,this.w_STRINGA+Chr(13)+Chr(10))
        if Asc(this.w_CARAT) = 3
          Exit
        endif
        this.w_STRINGA = ""
        this.w_CARAT = FGet(this.w_FOUTFILE,1)
      enddo
    endif
    Chiudo=Fclose(this.w_FINFILE)
    Chiudo=Fclose(this.w_FOUTFILE)
    this.w_FINFILE = FOpen(this.w_FILENAME1)
    this.w_GNEND = FSeek(this.w_FINFILE,0,2)
    this.w_GNTOP = FSeek(this.w_FINFILE,0)
    if this.w_GNEND<=0
      AddMsgNL("Problemi in ricezione dati",this.pObjMsg)
    else
      * --- Salto la prima stringa
      this.w_STRINGA = FGet(this.w_FINFILE)
      this.w_CONT = 0
      do while len (this.w_STRINGA ) > 0
        this.w_STRINGA = FGet(this.w_FINFILE)
        this.w_RXCOD = ""
        this.w_RXQTA = 1
        this.w_RN = AT ( this.w_BK1 , this.w_STRINGA )
        this.w_RN1 = AT ( this.w_BK2 , this.w_STRINGA )
        do case
          case this.w_RN>0 And this.w_RN1=0
            * --- Trovato articolo senza indicazione Q.t� (Default 1)
            this.w_RXCOD = SUBSTR ( this.w_STRINGA , this.w_RN + 1 , LEN ( this.w_STRINGA ) - this.w_RN )
          case this.w_RN>0 And this.w_RN1>0
            this.w_RXCOD = SUBSTR ( this.w_STRINGA , this.w_RN + 1 ,this.w_RN1 - this.w_RN - 1 )
            this.w_STRQTA = SUBSTR( this.w_STRINGA , this.w_RN1 + 1)
            if set("point")=","
              this.w_STRQTA = STRTRAN(this.w_STRQTA,".",",")
            endif
            this.w_RXQTA = VAL(this.w_STRQTA)
        endcase
        this.w_RXQTA = IIF(this.w_RXQTA=0,1,this.w_RXQTA)
        * --- Elimino l'ultimo carattere...
        this.w_RXCOD = substr(this.w_RXCOD,1,len(this.w_RXCOD)-1)
        * --- Aggiorno il cursore
        if LEN( this.w_RXCOD )<>0
          * --- Inserisco nel cursore i due valori (cODICE A BARRE E QUANTIT�)
          SELECT ( this.w_CURSORE )
          append blank
          replace CODART with this.w_RXCOD
          replace QTA with this.w_RXQTA
           
 INSERT INTO ( this.pCursore ) (CODICE,QTAMOV) ; 
 VALUES (this.w_RXCOD,this.w_RXQTA)
          AddMsgNL("Letto articolo %1",this.pObjMsg,this.w_RXCOD)
          this.w_TROV = .T.
          this.w_CONT = this.w_CONT+1
        endif
      enddo
    endif
    Chiudo=Fclose(this.w_FINFILE)
    if this.w_TROV
      AddMsgNL("Elaborati %1 articoli",this.pObjMsg,Alltrim(Str(this.w_CONT)))
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dichiarazione Variabili
    * --- Nome del  cursore creato
    this.w_LETTO = .F.
    this.w_ETB = 23
    this.w_TROV = .F.
    this.w_CR = 13
    this.w_STRINGA = ""
    this.w_BK1 = "["
    this.w_INSTRING = ""
    this.w_BK2 = "]"
    this.w_RECINIT = .T.
    this.w_BAUD = ALLTRIM(STR(this.w_BAURAT,5,0))
    this.w_RITARDO = this.w_RITARD*10
    this.w_STOP = ALLTRIM(STR(this.w_BISTOP,1,0))
    this.w_CURSORE = SYS(2015)
    * --- Trascodifica parit�
  endproc


  proc Init(oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition)
    this.pCursore=pCursore
    this.pTXTFile=pTXTFile
    this.pObjMsg=pObjMsg
    this.p_ArrayFieldName=p_ArrayFieldName
    this.p_ArrayFieldPosition=p_ArrayFieldPosition
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DIS_HARD'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition"
endproc
