* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bat                                                        *
*              Aggiorna da castivarate rate e castelletto IVA                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-11-16                                                      *
* Last revis.: 2012-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SERIAL,w_RET
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bat",oParentObject,m.w_SERIAL,m.w_RET)
return(i_retval)

define class tgsut_bat as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_RET = .NULL.
  w_contrate = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riceve oggetto w_RET restituito da CASTIVARATE ed il seriale
    *     sul database per aggiornare il documento
    * --- Aggiorno le rate...
    this.w_contrate = 1
    this.w_RET.mcRate.gotop()     
    * --- Per le rate aggiorno solo gli importi (la modifica dello sconto non pu� modificare altro di fatto)
    do while not this.w_RET.mcRate.eof()
      * --- Write into DOC_RATE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_RATE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_RATE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"RSDATRAT ="+cp_NullLink(cp_ToStrODBC( this.w_RET.mcRate.DATRAT),'DOC_RATE','RSDATRAT');
        +",RSIMPRAT ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.IMPNET + this.w_RET.mcRate.IMPIVA + this.w_RET.mcRate.IMPSPE),'DOC_RATE','RSIMPRAT');
        +",RSMODPAG ="+cp_NullLink(cp_ToStrODBC( this.w_RET.mcRate.MODPAG),'DOC_RATE','RSMODPAG');
        +",RSFLPROV ="+cp_NullLink(cp_ToStrODBC( this.w_RET.mcRate.FLPROV),'DOC_RATE','RSFLPROV');
            +i_ccchkf ;
        +" where ";
            +"RSSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
            +" and RSNUMRAT = "+cp_ToStrODBC(this.w_contrate);
               )
      else
        update (i_cTable) set;
            RSDATRAT =  this.w_RET.mcRate.DATRAT;
            ,RSIMPRAT = this.w_RET.mcRate.IMPNET + this.w_RET.mcRate.IMPIVA + this.w_RET.mcRate.IMPSPE;
            ,RSMODPAG =  this.w_RET.mcRate.MODPAG;
            ,RSFLPROV =  this.w_RET.mcRate.FLPROV;
            &i_ccchkf. ;
         where;
            RSSERIAL = this.w_SERIAL;
            and RSNUMRAT = this.w_contrate;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_ROWS=0
        * --- Insert into DOC_RATE
        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_RATE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"RSSERIAL"+",RSNUMRAT"+",RSDATRAT"+",RSIMPRAT"+",RSMODPAG"+",RSFLSOSP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'DOC_RATE','RSSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_contrate),'DOC_RATE','RSNUMRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.DATRAT),'DOC_RATE','RSDATRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.IMPNET + this.w_RET.mcRate.IMPIVA + this.w_RET.mcRate.IMPSPE),'DOC_RATE','RSIMPRAT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.MODPAG),'DOC_RATE','RSMODPAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_RET.mcRate.FLPROV),'DOC_RATE','RSFLSOSP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'RSSERIAL',this.w_SERIAL,'RSNUMRAT',this.w_contrate,'RSDATRAT',this.w_RET.mcRate.DATRAT,'RSIMPRAT',this.w_RET.mcRate.IMPNET + this.w_RET.mcRate.IMPIVA + this.w_RET.mcRate.IMPSPE,'RSMODPAG',this.w_RET.mcRate.MODPAG,'RSFLSOSP',this.w_RET.mcRate.FLPROV)
          insert into (i_cTable) (RSSERIAL,RSNUMRAT,RSDATRAT,RSIMPRAT,RSMODPAG,RSFLSOSP &i_ccchkf. );
             values (;
               this.w_SERIAL;
               ,this.w_contrate;
               ,this.w_RET.mcRate.DATRAT;
               ,this.w_RET.mcRate.IMPNET + this.w_RET.mcRate.IMPIVA + this.w_RET.mcRate.IMPSPE;
               ,this.w_RET.mcRate.MODPAG;
               ,this.w_RET.mcRate.FLPROV;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      this.w_contrate = this.w_contrate+1
      this.w_RET.mcRate.Next()     
    enddo
    * --- Aggiorno i totali e le spese bolli
    this.w_RET.mcCastiva.gotop()     
    this.w_contrate = 1
    do while not this.w_RET.mcCastiva.eof()
      do case
        case this.w_contrate=1
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN1');
            +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS1');
            +",MVACIVA1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA1');
            +",MVAFLOM1 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM1');
            +",MVSPEBOL ="+cp_NullLink(cp_ToStrODBC(this.w_RET.nMVSPEBOL),'DOC_MAST','MVSPEBOL');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                MVAIMPN1 = this.w_RET.mcCastiva.IMPON;
                ,MVAIMPS1 = this.w_RET.mcCastiva.IMPOS;
                ,MVACIVA1 = this.w_RET.mcCastiva.CODIVA;
                ,MVAFLOM1 = this.w_RET.mcCastiva.FLOMAG;
                ,MVSPEBOL = this.w_RET.nMVSPEBOL;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case this.w_contrate=2
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN2');
            +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS2');
            +",MVACIVA2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA2');
            +",MVAFLOM2 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM2');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                MVAIMPN2 = this.w_RET.mcCastiva.IMPON;
                ,MVAIMPS2 = this.w_RET.mcCastiva.IMPOS;
                ,MVACIVA2 = this.w_RET.mcCastiva.CODIVA;
                ,MVAFLOM2 = this.w_RET.mcCastiva.FLOMAG;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case this.w_contrate=3
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN3');
            +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS3');
            +",MVACIVA3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA3');
            +",MVAFLOM3 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM3');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                MVAIMPN3 = this.w_RET.mcCastiva.IMPON;
                ,MVAIMPS3 = this.w_RET.mcCastiva.IMPOS;
                ,MVACIVA3 = this.w_RET.mcCastiva.CODIVA;
                ,MVAFLOM3 = this.w_RET.mcCastiva.FLOMAG;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case this.w_contrate=4
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN4');
            +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS4');
            +",MVACIVA4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA4');
            +",MVAFLOM4 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM4');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                MVAIMPN4 = this.w_RET.mcCastiva.IMPON;
                ,MVAIMPS4 = this.w_RET.mcCastiva.IMPOS;
                ,MVACIVA4 = this.w_RET.mcCastiva.CODIVA;
                ,MVAFLOM4 = this.w_RET.mcCastiva.FLOMAG;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case this.w_contrate=5
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN5');
            +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS5');
            +",MVACIVA5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA5');
            +",MVAFLOM5 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM5');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                MVAIMPN5 = this.w_RET.mcCastiva.IMPON;
                ,MVAIMPS5 = this.w_RET.mcCastiva.IMPOS;
                ,MVACIVA5 = this.w_RET.mcCastiva.CODIVA;
                ,MVAFLOM5 = this.w_RET.mcCastiva.FLOMAG;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        case this.w_contrate=6
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPON),'DOC_MAST','MVAIMPN6');
            +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.IMPOS),'DOC_MAST','MVAIMPS6');
            +",MVACIVA6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.CODIVA),'DOC_MAST','MVACIVA6');
            +",MVAFLOM6 ="+cp_NullLink(cp_ToStrODBC(this.w_RET.mcCastiva.FLOMAG),'DOC_MAST','MVAFLOM6');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                MVAIMPN6 = this.w_RET.mcCastiva.IMPON;
                ,MVAIMPS6 = this.w_RET.mcCastiva.IMPOS;
                ,MVACIVA6 = this.w_RET.mcCastiva.CODIVA;
                ,MVAFLOM6 = this.w_RET.mcCastiva.FLOMAG;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
      endcase
      this.w_contrate = this.w_contrate+1
      this.w_RET.mcCastiva.Next()     
    enddo
  endproc


  proc Init(oParentObject,w_SERIAL,w_RET)
    this.w_SERIAL=w_SERIAL
    this.w_RET=w_RET
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DOC_RATE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SERIAL,w_RET"
endproc
