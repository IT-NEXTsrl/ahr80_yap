* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bo1                                                        *
*              Lancia Window creaz. clienti                                    *
*                                                                              *
*      Author: Mimmo Fasano                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2014-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNOCODICE,pNOTIPO,pFL_SOG_RIT,pNewTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bo1",oParentObject,m.pNOCODICE,m.pNOTIPO,m.pFL_SOG_RIT,m.pNewTipo)
return(i_retval)

define class tgsar_bo1 as StdBatch
  * --- Local variables
  pNOCODICE = space(20)
  pNOTIPO = space(1)
  pFL_SOG_RIT = space(1)
  pNewTipo = space(1)
  w_NOTIPNOM = space(1)
  w_NODESCRI = space(40)
  w_NOCODICE = space(15)
  w_NODESCR2 = space(40)
  w_NOINDIRI = space(35)
  w_NOINDIR2 = space(35)
  w_NO___CAP = space(8)
  w_NOLOCALI = space(30)
  w_NOPROVIN = space(2)
  w_NONAZION = space(3)
  w_NOCODFIS = space(16)
  w_NOPARIVA = space(12)
  w_NOTELEFO = space(18)
  w_NOTELFAX = space(18)
  w_NOINDWEB = space(50)
  w_NO_EMAIL = space(50)
  w_NO_EMPEC = space(50)
  w_NODTINVA = ctod("  /  /  ")
  w_NODTOBSO = ctod("  /  /  ")
  w_NOCODZON = space(3)
  w_NOCODLIN = space(3)
  w_NOCODVAL = space(5)
  w_NOCODAGE = space(5)
  w_NOTIPO = space(1)
  w_NOCODSAL = space(5)
  w_FL_SOG_RIT = space(1)
  w_NOCODCLI = space(15)
  w_NONUMLIS = space(5)
  w_NOCODBAN = space(10)
  w_NOCODBA2 = space(15)
  w_NOCATCOM = space(5)
  w_NONUMCEL = space(18)
  w_NO_SKYPE = space(50)
  w_NOSOGGET = space(2)
  w_NOCOGNOM = space(50)
  w_NO__NOME = space(50)
  w_NOLOCNAS = space(30)
  w_NOPRONAS = space(2)
  w_NODATNAS = ctod("  /  /  ")
  w_NONUMCAR = space(18)
  w_NOCHKSTA = space(1)
  w_NOCHKMAI = space(1)
  w_NOCHKPEC = space(1)
  w_NOCHKFAX = space(1)
  w_CCDESCRI = space(40)
  w_CCCODICE = space(15)
  w_CODICE = space(15)
  w_ANCODICE = space(15)
  w_CCDESCR2 = space(40)
  w_CCINDIRI = space(35)
  w_CCINDIR2 = space(35)
  w_CC___CAP = space(8)
  w_CCLOCALI = space(30)
  w_CCPROVIN = space(2)
  w_CCNAZION = space(3)
  w_CCCODFIS = space(16)
  w_CCPARIVA = space(12)
  w_CCTELEFO = space(18)
  w_CCTELFAX = space(18)
  w_CCINDWEB = space(50)
  w_CC_EMAIL = space(50)
  w_CC_EMPEC = space(50)
  w_CCDTINVA = ctod("  /  /  ")
  w_CCDTOBSO = ctod("  /  /  ")
  w_CCCODZON = space(3)
  w_CCCODLIN = space(3)
  w_CCCODVAL = space(5)
  w_CCCODAGE = space(5)
  w_NOTIFICA = space(1)
  w_AUTN = 0
  w_CODN = space(15)
  w_LENC = 0
  w_CCMASCON = space(15)
  w_CCCATCON = space(5)
  w_CCCODSAL = space(5)
  w_CCCODPAG = space(5)
  w_CCFLRITE = space(1)
  w_CCCODIRP = space(5)
  w_CCFLPRIV = space(1)
  w_CCFLESIG = space(1)
  w_CCNUMCEL = space(18)
  w_CC_SKYPE = space(50)
  w_CCSOGGET = space(2)
  w_CCCOGNOM = space(50)
  w_CC__NOME = space(50)
  w_CCLOCNAS = space(30)
  w_CCPRONAS = space(2)
  w_CCDATNAS = ctod("  /  /  ")
  w_CCNUMCAR = space(18)
  w_CCCHKSTA = space(1)
  w_CCCHKMAI = space(1)
  w_CCCHKPEC = space(1)
  w_CCCHKFAX = space(1)
  w_CCNOINDI_2 = space(35)
  w_NOINDI_2 = space(35)
  w_CCIVASOS = space(1)
  w_NewTipCon = space(1)
  w_BckTIPO = space(1)
  w_OldTipNom = space(1)
  w_CODSOGEST = space(15)
  w_CODAZI = space(5)
  w_ACTIVE = space(1)
  w_TIPGES = space(2)
  w_PERFIS = space(2)
  NoName = space(0)
  w_POCODPAG = space(5)
  w_OK = .f.
  Prosegui = .f.
  w_POFLCONF = space(1)
  w_CODAZI = space(5)
  w_MODSCHED = space(1)
  w_GSAR_KCC = .NULL.
  * --- WorkFile variables
  CONTI_idx=0
  OFF_NOMI_idx=0
  PAR_OFFE_idx=0
  NUMAUT_M_idx=0
  RIS_ESTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce Cliente in Anagrafica alla Conferma (da GSAR_ANO)
    * --- PARAMETRI:
    *     pNOCODICE: CODICE DEL NOMINATICO
    *     pNOTIPO: TIPO NOMINATIVO DA RIPRISTINARE IN CASO DI MANCATA SCRITTURA
    *     pFL_SOG_RIT: SOGGETTO A RITENUTE
    this.w_NOCODICE = this.pNOCODICE
    this.w_FL_SOG_RIT = IIF( EMPTY( this.pFL_SOG_RIT ) , " " , this.pFL_SOG_RIT )
    * --- Read from OFF_NOMI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" OFF_NOMI where ";
            +"NOCODICE = "+cp_ToStrODBC(this.w_NOCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            NOCODICE = this.w_NOCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NODESCRI = NVL(cp_ToDate(_read_.NODESCRI),cp_NullValue(_read_.NODESCRI))
      this.w_NODESCR2 = NVL(cp_ToDate(_read_.NODESCR2),cp_NullValue(_read_.NODESCR2))
      this.w_NOINDIRI = NVL(cp_ToDate(_read_.NOINDIRI),cp_NullValue(_read_.NOINDIRI))
      this.w_NOINDIR2 = NVL(cp_ToDate(_read_.NOINDIR2),cp_NullValue(_read_.NOINDIR2))
      this.w_NO___CAP = NVL(cp_ToDate(_read_.NO___CAP),cp_NullValue(_read_.NO___CAP))
      this.w_NOLOCALI = NVL(cp_ToDate(_read_.NOLOCALI),cp_NullValue(_read_.NOLOCALI))
      this.w_NOPROVIN = NVL(cp_ToDate(_read_.NOPROVIN),cp_NullValue(_read_.NOPROVIN))
      this.w_NONAZION = NVL(cp_ToDate(_read_.NONAZION),cp_NullValue(_read_.NONAZION))
      this.w_NOCODFIS = NVL(cp_ToDate(_read_.NOCODFIS),cp_NullValue(_read_.NOCODFIS))
      this.w_NOPARIVA = NVL(cp_ToDate(_read_.NOPARIVA),cp_NullValue(_read_.NOPARIVA))
      this.w_NOTELEFO = NVL(cp_ToDate(_read_.NOTELEFO),cp_NullValue(_read_.NOTELEFO))
      this.w_NOTELFAX = NVL(cp_ToDate(_read_.NOTELFAX),cp_NullValue(_read_.NOTELFAX))
      this.w_NOINDWEB = NVL(cp_ToDate(_read_.NOINDWEB),cp_NullValue(_read_.NOINDWEB))
      this.w_NO_EMAIL = NVL(cp_ToDate(_read_.NO_EMAIL),cp_NullValue(_read_.NO_EMAIL))
      this.w_NO_EMPEC = NVL(cp_ToDate(_read_.NO_EMPEC),cp_NullValue(_read_.NO_EMPEC))
      this.w_NODTINVA = NVL(cp_ToDate(_read_.NODTINVA),cp_NullValue(_read_.NODTINVA))
      this.w_NODTOBSO = NVL(cp_ToDate(_read_.NODTOBSO),cp_NullValue(_read_.NODTOBSO))
      this.w_NOCODZON = NVL(cp_ToDate(_read_.NOCODZON),cp_NullValue(_read_.NOCODZON))
      this.w_NOCODLIN = NVL(cp_ToDate(_read_.NOCODLIN),cp_NullValue(_read_.NOCODLIN))
      this.w_NOCODVAL = NVL(cp_ToDate(_read_.NOCODVAL),cp_NullValue(_read_.NOCODVAL))
      this.w_NOCODAGE = NVL(cp_ToDate(_read_.NOCODAGE),cp_NullValue(_read_.NOCODAGE))
      this.w_NOCODSAL = NVL(cp_ToDate(_read_.NOCODSAL),cp_NullValue(_read_.NOCODSAL))
      this.w_NONUMLIS = NVL(cp_ToDate(_read_.NONUMLIS),cp_NullValue(_read_.NONUMLIS))
      this.w_NOCODBAN = NVL(cp_ToDate(_read_.NOCODBAN),cp_NullValue(_read_.NOCODBAN))
      this.w_NOCODBA2 = NVL(cp_ToDate(_read_.NOCODBA2),cp_NullValue(_read_.NOCODBA2))
      this.w_NOCATCOM = NVL(cp_ToDate(_read_.NOCATCOM),cp_NullValue(_read_.NOCATCOM))
      this.w_NOTIPNOM = NVL(cp_ToDate(_read_.NOTIPNOM),cp_NullValue(_read_.NOTIPNOM))
      this.w_NOCODCLI = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
      this.w_CCCODPAG = NVL(cp_ToDate(_read_.NOCODPAG),cp_NullValue(_read_.NOCODPAG))
      this.w_NONUMCEL = NVL(cp_ToDate(_read_.NONUMCEL),cp_NullValue(_read_.NONUMCEL))
      this.w_NO_SKYPE = NVL(cp_ToDate(_read_.NO_SKYPE),cp_NullValue(_read_.NO_SKYPE))
      this.w_NOSOGGET = NVL(cp_ToDate(_read_.NOSOGGET),cp_NullValue(_read_.NOSOGGET))
      this.w_NOCOGNOM = NVL(cp_ToDate(_read_.NOCOGNOM),cp_NullValue(_read_.NOCOGNOM))
      this.w_NO__NOME = NVL(cp_ToDate(_read_.NO__NOME),cp_NullValue(_read_.NO__NOME))
      this.w_NOLOCNAS = NVL(cp_ToDate(_read_.NOLOCNAS),cp_NullValue(_read_.NOLOCNAS))
      this.w_NOPRONAS = NVL(cp_ToDate(_read_.NOPRONAS),cp_NullValue(_read_.NOPRONAS))
      this.w_NODATNAS = NVL(cp_ToDate(_read_.NODATNAS),cp_NullValue(_read_.NODATNAS))
      this.w_NONUMCAR = NVL(cp_ToDate(_read_.NONUMCAR),cp_NullValue(_read_.NONUMCAR))
      this.w_NOCHKSTA = NVL(cp_ToDate(_read_.NOCHKSTA),cp_NullValue(_read_.NOCHKSTA))
      this.w_NOCHKMAI = NVL(cp_ToDate(_read_.NOCHKMAI),cp_NullValue(_read_.NOCHKMAI))
      this.w_NOCHKPEC = NVL(cp_ToDate(_read_.NOCHKPEC),cp_NullValue(_read_.NOCHKPEC))
      this.w_NOCHKFAX = NVL(cp_ToDate(_read_.NOCHKFAX),cp_NullValue(_read_.NOCHKFAX))
      this.w_NOINDI_2 = NVL(cp_ToDate(_read_.NOINDI_2),cp_NullValue(_read_.NOINDI_2))
      this.w_OldTipNom = NVL(cp_ToDate(_read_.NOTIPNOM),cp_NullValue(_read_.NOTIPNOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_NOCODCLI = NVL(this.w_NOCODCLI , SPACE( 15 ) )
    if NOT EMPTY( this.pNOTIPO )
      this.w_BckTIPO = this.pNOTIPO
    else
      this.w_BckTIPO = this.w_OldTipNom
      if this.w_BckTIPO="C" OR this.w_BckTIPO="F"
        * --- Il valore era gi� stato salvato
        this.w_BckTIPO = "T"
      endif
    endif
    if TYPE("pNewTipo")="C"
      this.w_NOTIPO = this.pNewTipo
      this.w_NewTipCon = this.w_NOTIPO
    else
      this.w_NewTipCon = "C"
      this.w_NOTIPO = this.w_NOTIPNOM
    endif
    if ISALT() AND this.w_NewTipCon="F"
      * --- Legge nelle risorse esterne delle pratiche
      * --- Read from RIS_ESTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.RIS_ESTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESTE_idx,2],.t.,this.RIS_ESTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SECODSES"+;
          " from "+i_cTable+" RIS_ESTE where ";
              +"SECODSES = "+cp_ToStrODBC(this.w_NOCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SECODSES;
          from (i_cTable) where;
              SECODSES = this.w_NOCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODSOGEST = NVL(cp_ToDate(_read_.SECODSES),cp_NullValue(_read_.SECODSES))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se il nominativo � presente nelle risorse esterne delle pratiche
      if NOT EMPTY(this.w_CODSOGEST)
        ah_ErrorMsg("Attenzione, impossibile trasformare in fornitore un nominativo gi� presente in una pratica.")
        * --- Ripristina il valore precedente
        if NOT EMPTY(this.w_NOCODICE)
          this.w_NOTIPNOM = this.w_BckTIPO
          * --- Write into OFF_NOMI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NOTIPNOM ="+cp_NullLink(cp_ToStrODBC(this.w_NOTIPNOM),'OFF_NOMI','NOTIPNOM');
                +i_ccchkf ;
            +" where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_NOCODICE);
                   )
          else
            update (i_cTable) set;
                NOTIPNOM = this.w_NOTIPNOM;
                &i_ccchkf. ;
             where;
                NOCODICE = this.w_NOCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        i_retcode = 'stop'
        return
      endif
    endif
    this.w_LENC = MIN(LEN(ALLTRIM(p_CLI)), 15)
    if (this.w_NOTIPNOM="C" OR this.w_NOTIPNOM="F" OR lower(this.oParentObject.class)<>"tgsar_ano") AND EMPTY(this.w_NOCODCLI)
      * --- Se � stata definita la codifica numerica.
      this.w_TIPGES = "CL"
      this.w_CODAZI = I_CODAZI
      * --- Cliente o fornitore
      * --- Read from NUMAUT_M
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.NUMAUT_M_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.NUMAUT_M_idx,2],.t.,this.NUMAUT_M_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NAACTIVE"+;
          " from "+i_cTable+" NUMAUT_M where ";
              +"NACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
              +" and NATIPGES = "+cp_ToStrODBC(this.w_TIPGES);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NAACTIVE;
          from (i_cTable) where;
              NACODAZI = this.w_CODAZI;
              and NATIPGES = this.w_TIPGES;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ACTIVE = NVL(cp_ToDate(_read_.NAACTIVE),cp_NullValue(_read_.NAACTIVE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if isAlt()
        this.w_ACTIVE = "N"
      endif
      if this.w_ACTIVE<>"S"
        if g_CFNUME="S"
          * --- Propone il Primo Progressivo Clente Disponibile
          this.w_CODICE = LEFT(SPACE(15), LEN(ALLTRIM(p_CLI)))
          i_nConn = i_TableProp[this.CONTI_IDX,3]
          if this.w_NewTipCon="C"
            * --- Cliente
            cp_AskTableProg(this,i_nConn,"PRNUCL","i_CODAZI,w_CODICE") 
          else
            * --- Fornitore
            cp_AskTableProg(this,i_nConn,"PRNUFO","i_CODAZI,w_CODICE") 
          endif
          this.w_ANCODICE = RIGHT(this.w_CODICE, this.w_LENC)
        else
          * --- Previene eventuale inserimento di Codici non numerici dal calcolo Autonumber
          this.w_AUTN = 0
          this.w_CODN = ALLTRIM(this.w_NOCODICE)
          FOR p_POS=1 TO LEN(this.w_CODN)
          this.w_AUTN = IIF(SUBSTR(this.w_CODN, p_POS, 1) $ "0123456789", this.w_AUTN, 1)
          ENDFOR
          if g_OFNUME="S" AND this.w_AUTN=0
            this.w_ANCODICE = RIGHT(ALLTRIM(this.w_NOCODICE), this.w_LENC)
          else
            this.w_ANCODICE = LEFT(ALLTRIM(this.w_NOCODICE), this.w_LENC)
          endif
        endif
      endif
      * --- VARIABILI DA PASSARE ALLA MASCHERA
      this.w_CCDESCRI = this.w_NODESCRI
      this.w_CCCODICE = this.w_NOCODICE
      this.w_CCDESCR2 = this.w_NODESCR2
      this.w_CCINDIRI = this.w_NOINDIRI
      this.w_CCINDIR2 = this.w_NOINDIR2
      this.w_CC___CAP = this.w_NO___CAP
      this.w_CCLOCALI = this.w_NOLOCALI
      this.w_CCPROVIN = this.w_NOPROVIN
      this.w_CCNAZION = this.w_NONAZION
      this.w_CCCODFIS = this.w_NOCODFIS
      this.w_CCPARIVA = this.w_NOPARIVA
      this.w_CCTELEFO = this.w_NOTELEFO
      this.w_CCTELFAX = this.w_NOTELFAX
      this.w_CCINDWEB = this.w_NOINDWEB
      this.w_CC_EMAIL = this.w_NO_EMAIL
      this.w_CC_EMPEC = this.w_NO_EMPEC
      this.w_CCDTINVA = this.w_NODTINVA
      this.w_CCDTOBSO = this.w_NODTOBSO
      this.w_CCCODZON = this.w_NOCODZON
      this.w_CCCODLIN = this.w_NOCODLIN
      this.w_CCNOINDI_2 = this.w_NOINDI_2
      if EMPTY(this.w_CCCODLIN)
        this.w_CCCODLIN = g_CODLIN
      endif
      this.w_CCCODVAL = this.w_NOCODVAL
      this.w_CCCODAGE = this.w_NOCODAGE
      this.w_CCCODSAL = this.w_NOCODSAL
      this.w_CCFLRITE = this.w_FL_SOG_RIT
      this.w_CCFLPRIV = "N"
      this.w_CCFLESIG = "N"
      this.w_CCIVASOS = "N"
      this.w_CCNUMCEL = this.w_NONUMCEL
      this.w_CC_SKYPE = this.w_NO_SKYPE
      this.w_CCSOGGET = this.w_NOSOGGET
      this.w_PERFIS = this.w_CCSOGGET
      this.w_CCCOGNOM = this.w_NOCOGNOM
      this.w_CC__NOME = this.w_NO__NOME
      this.w_CCLOCNAS = this.w_NOLOCNAS
      this.w_CCPRONAS = this.w_NOPRONAS
      this.w_CCDATNAS = this.w_NODATNAS
      this.w_CCNUMCAR = this.w_NONUMCAR
      this.w_CCCHKSTA = this.w_NOCHKSTA
      this.w_CCCHKMAI = this.w_NOCHKMAI
      this.w_CCCHKPEC = this.w_NOCHKPEC
      this.w_CCCHKFAX = this.w_NOCHKFAX
      * --- Legge dalla tabella Parametri Nominativi il mastro e la categoria contabile dei clienti
      if this.w_NewTipCon="F"
        * --- Read from PAR_OFFE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "POMASCOF,POCATCOF,POCODPAF"+;
            " from "+i_cTable+" PAR_OFFE where ";
                +"POCODAZI = "+cp_ToStrODBC(i_codazi);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            POMASCOF,POCATCOF,POCODPAF;
            from (i_cTable) where;
                POCODAZI = i_codazi;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CCMASCON = NVL(cp_ToDate(_read_.POMASCOF),cp_NullValue(_read_.POMASCOF))
          this.w_CCCATCON = NVL(cp_ToDate(_read_.POCATCOF),cp_NullValue(_read_.POCATCOF))
          this.w_POCODPAG = NVL(cp_ToDate(_read_.POCODPAF),cp_NullValue(_read_.POCODPAF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from PAR_OFFE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "POMASCOC,POCATCOC,POCODPAG"+;
            " from "+i_cTable+" PAR_OFFE where ";
                +"POCODAZI = "+cp_ToStrODBC(i_codazi);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            POMASCOC,POCATCOC,POCODPAG;
            from (i_cTable) where;
                POCODAZI = i_codazi;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CCMASCON = NVL(cp_ToDate(_read_.POMASCOC),cp_NullValue(_read_.POMASCOC))
          this.w_CCCATCON = NVL(cp_ToDate(_read_.POCATCOC),cp_NullValue(_read_.POCATCOC))
          this.w_POCODPAG = NVL(cp_ToDate(_read_.POCODPAG),cp_NullValue(_read_.POCODPAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if EMPTY (this.w_CCCODPAG)
        * --- Imposta con quello letto da PAR_OFFE
        this.w_CCCODPAG = this.w_POCODPAG
      endif
      * --- Notifica Se Aggiornamento Cliente OK viene valorizzato a 'S' (in GSAR_BO4)
      this.w_NOTIFICA = "N"
      this.Prosegui = .T.
      * --- Legge dalla tabella Parametri Nominativi il check  "Non visualizza maschera di conferma clienti"
      this.w_CODAZI = i_CODAZI
      * --- Read from PAR_OFFE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "POFLCONF"+;
          " from "+i_cTable+" PAR_OFFE where ";
              +"POCODAZI = "+cp_ToStrODBC(i_codazi);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          POFLCONF;
          from (i_cTable) where;
              POCODAZI = i_codazi;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_POFLCONF = NVL(cp_ToDate(_read_.POFLCONF),cp_NullValue(_read_.POFLCONF))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se il check non � attivo si deve visualizzare la maschera GSAR_KCC di Conferma Clienti.
      * --- Se invece il check � attivo si devono eseguire in anticipo i medesimi controlli
      *     che vengono effettuati sulla maschera di Conferma Clienti.
      if this.w_POFLCONF # "S"
        this.Prosegui = .F.
      endif
      * --- Se non sono valorizzati i campi obbligatori
      if this.Prosegui AND (EMPTY(this.w_CCMASCON) OR (this.w_NewTipCon="C" AND EMPTY(this.w_CCCATCON)) OR EMPTY(this.w_CCCODLIN))
        this.Prosegui = .F.
      endif
      if this.Prosegui
        * --- Verifica se codice cliente/fornitore gi� presente in anagrafica
        * --- Select from CONTI
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ANCODICE  from "+i_cTable+" CONTI ";
              +" where ANTIPCON="+cp_ToStrODBC(this.w_NewTipCon)+" AND ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)+"";
               ,"_Curs_CONTI")
        else
          select ANCODICE from (i_cTable);
           where ANTIPCON=this.w_NewTipCon AND ANCODICE=this.w_ANCODICE;
            into cursor _Curs_CONTI
        endif
        if used('_Curs_CONTI')
          select _Curs_CONTI
          locate for 1=1
          do while not(eof())
          this.Prosegui = .F.
            select _Curs_CONTI
            continue
          enddo
          use
        endif
      endif
      if this.Prosegui
        * --- Lancia la maschera GSAR_KCC di Conferma Clienti senza visualizzarla
        this.w_MODSCHED = "N"
        if VARTYPE(g_SCHEDULER)<>"C" 
          public g_SCHEDULER
          g_SCHEDULER="S"
          this.w_MODSCHED = "S"
        else
          if g_SCHEDULER<>"S"
            g_SCHEDULER="S"
            this.w_MODSCHED = "S"
          endif
        endif
        this.w_GSAR_KCC = GSAR_KCC(THIS)
        this.w_GSAR_KCC.SetControlsValue()     
        this.w_GSAR_KCC.ecpSave()     
        if this.w_MODSCHED="S"
          g_SCHEDULER=" "
        endif
      else
        do GSAR_KCC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_NOTIFICA="N"
        * --- Se Abbandono ripristina il valore precedente (w_NOTIPO)
        if NOT EMPTY(this.w_NOCODICE)
          this.w_NOTIPNOM = this.w_BckTIPO
          * --- Write into OFF_NOMI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NOTIPNOM ="+cp_NullLink(cp_ToStrODBC(this.w_NOTIPNOM),'OFF_NOMI','NOTIPNOM');
                +i_ccchkf ;
            +" where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_NOCODICE);
                   )
          else
            update (i_cTable) set;
                NOTIPNOM = this.w_NOTIPNOM;
                &i_ccchkf. ;
             where;
                NOCODICE = this.w_NOCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      else
        if this.w_ACTIVE="S"
          * --- Write into OFF_NOMI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"NOTIPCLI ="+cp_NullLink(cp_ToStrODBC(this.w_NewTipCon),'OFF_NOMI','NOTIPCLI');
            +",NOCODCLI ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'OFF_NOMI','NOCODCLI');
            +",NOTIPNOM ="+cp_NullLink(cp_ToStrODBC(this.w_NOTIPNOM),'OFF_NOMI','NOTIPNOM');
                +i_ccchkf ;
            +" where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_NOCODICE);
                   )
          else
            update (i_cTable) set;
                NOTIPCLI = this.w_NewTipCon;
                ,NOCODCLI = this.w_ANCODICE;
                ,NOTIPNOM = this.w_NOTIPNOM;
                &i_ccchkf. ;
             where;
                NOCODICE = this.w_NOCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
      if VARTYPE( this.oparentobject.cPrg ) = "C"
        if UPPER( this.oparentobject.cPrg ) = "GSAR_ANO"
          this.oparentobject.LoadRecWarn()
          this.oparentobject.Refresh()
        endif
      endif
      if this.w_NOTIFICA="N"
        i_retcode = 'stop'
        i_retval = ""
        return
      else
        i_retcode = 'stop'
        i_retval = this.w_ANCODICE
        return
      endif
    endif
    i_retcode = 'stop'
    i_retval = ""
    return
  endproc


  proc Init(oParentObject,pNOCODICE,pNOTIPO,pFL_SOG_RIT,pNewTipo)
    this.pNOCODICE=pNOCODICE
    this.pNOTIPO=pNOTIPO
    this.pFL_SOG_RIT=pFL_SOG_RIT
    this.pNewTipo=pNewTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='OFF_NOMI'
    this.cWorkTables[3]='PAR_OFFE'
    this.cWorkTables[4]='NUMAUT_M'
    this.cWorkTables[5]='RIS_ESTE'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNOCODICE,pNOTIPO,pFL_SOG_RIT,pNewTipo"
endproc
