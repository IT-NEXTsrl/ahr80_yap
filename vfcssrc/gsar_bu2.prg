* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bu2                                                        *
*              Lancia anagrafiche sedi/ U. O.                                  *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-23                                                      *
* Last revis.: 2007-07-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bu2",oParentObject)
return(i_retval)

define class tgsar_bu2 as StdBatch
  * --- Local variables
  w_TIPO = space(1)
  w_PROG = space(8)
  w_DXBTN = .f.
  w_CODICE = space(20)
  w_CODAZI = space(20)
  * --- WorkFile variables
  SEDIAZIE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia l'anagrafica delle sedi o delle U. O.
    *     Viene lanciato da GSAR_ADP - Risorse Umane
    *     Se la variabile che riferisce all'ultimo form aperto � vuota esco
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      this.w_CODAZI = Nvl(g_oMenu.oKey(1,3),"")
      this.w_CODICE = Nvl(g_oMenu.oKey(2,3),"")
      * --- Read from SEDIAZIE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SEUNIORG"+;
          " from "+i_cTable+" SEDIAZIE where ";
              +"SECODAZI = "+cp_ToStrODBC(this.w_CODAZI);
              +" and SECODDES = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SEUNIORG;
          from (i_cTable) where;
              SECODAZI = this.w_CODAZI;
              and SECODDES = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPO = NVL(cp_ToDate(_read_.SEUNIORG),cp_NullValue(_read_.SEUNIORG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      if i_curform=.NULL.
        i_retcode = 'stop'
        return
      endif
      cCurs = i_curform.z1.cCursor
      this.w_TIPO = &cCurs..SEUNIORG
    endif
    if this.w_TIPO = "U"
      this.w_PROG = "GSAR_AUO"
    else
      * --- -- "S" o " "
      this.w_PROG = "GSAR_ASE"
    endif
    OpenGest(iif(this.w_DXBTN,g_oMenu.cBatchType,"S"),this.w_PROG,"SECODDES",this.w_CODICE,"SEUNIORG",this.w_TIPO)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SEDIAZIE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
