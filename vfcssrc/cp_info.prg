* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: cp_info                                                         *
*              Informazioni di sistema                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_23]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-10                                                      *
* Last revis.: 2013-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- cp_info
cp_ReadXdc()
* --- Fine Area Manuale
return(createobject("tcp_info",oParentObject))

* --- Class definition
define class tcp_info as StdForm
  Top    = 170
  Left   = 195

  * --- Standard Properties
  Width  = 493
  Height = 221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-12-02"
  HelpContextID=97842787
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cpusers_IDX = 0
  cPrg = "cp_info"
  cComment = "Informazioni di sistema"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_NOMPRO = space(8)
  w_LOGTAB = space(35)
  w_FISTAB = space(20)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_DES1 = space(20)
  w_DES2 = space(20)
  w_ULTDAT = space(15)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tcp_infoPag1","cp_info",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNOMPRO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='cpusers'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NOMPRO=space(8)
      .w_LOGTAB=space(35)
      .w_FISTAB=space(20)
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_DES1=space(20)
      .w_DES2=space(20)
      .w_ULTDAT=space(15)
      .w_UTCC=oParentObject.w_UTCC
      .w_UTCV=oParentObject.w_UTCV
      .w_UTDC=oParentObject.w_UTDC
      .w_UTDV=oParentObject.w_UTDV
        .w_NOMPRO = this.oParentOBJECT.cPrg
        .w_LOGTAB = i_dcx.GetTableDescr(i_dcx.GetTableIdx(upper(alltrim(this.oParentObject.cFile))))
        .w_FISTAB = alltrim(this.oParentObject.cFile)
        .w_UTCC = .w_UTCC
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_UTCC))
          .link_1_8('Full')
        endif
        .w_UTCV = .w_UTCV
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_UTCV))
          .link_1_9('Full')
        endif
          .DoRTCalc(6,9,.f.)
        .w_ULTDAT = this.oParentOBJECT.infodaterev
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_UTCC=.w_UTCC
      .oParentObject.w_UTCV=.w_UTCV
      .oParentObject.w_UTDC=.w_UTDC
      .oParentObject.w_UTDV=.w_UTDV
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_1_8('Full')
          .link_1_9('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(6,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=UTCC
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTCC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTCC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_UTCC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_UTCC)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTCC = NVL(_Link_.code,0)
      this.w_DES1 = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTCC = 0
      endif
      this.w_DES1 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTCC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTCV
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTCV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTCV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_UTCV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_UTCV)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTCV = NVL(_Link_.code,0)
      this.w_DES2 = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTCV = 0
      endif
      this.w_DES2 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTCV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNOMPRO_1_1.value==this.w_NOMPRO)
      this.oPgFrm.Page1.oPag.oNOMPRO_1_1.value=this.w_NOMPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oLOGTAB_1_2.value==this.w_LOGTAB)
      this.oPgFrm.Page1.oPag.oLOGTAB_1_2.value=this.w_LOGTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oFISTAB_1_3.value==this.w_FISTAB)
      this.oPgFrm.Page1.oPag.oFISTAB_1_3.value=this.w_FISTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oUTCC_1_8.value==this.w_UTCC)
      this.oPgFrm.Page1.oPag.oUTCC_1_8.value=this.w_UTCC
    endif
    if not(this.oPgFrm.Page1.oPag.oUTCV_1_9.value==this.w_UTCV)
      this.oPgFrm.Page1.oPag.oUTCV_1_9.value=this.w_UTCV
    endif
    if not(this.oPgFrm.Page1.oPag.oUTDC_1_10.value==this.w_UTDC)
      this.oPgFrm.Page1.oPag.oUTDC_1_10.value=this.w_UTDC
    endif
    if not(this.oPgFrm.Page1.oPag.oUTDV_1_11.value==this.w_UTDV)
      this.oPgFrm.Page1.oPag.oUTDV_1_11.value=this.w_UTDV
    endif
    if not(this.oPgFrm.Page1.oPag.oDES1_1_12.value==this.w_DES1)
      this.oPgFrm.Page1.oPag.oDES1_1_12.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page1.oPag.oDES2_1_13.value==this.w_DES2)
      this.oPgFrm.Page1.oPag.oDES2_1_13.value=this.w_DES2
    endif
    if not(this.oPgFrm.Page1.oPag.oULTDAT_1_15.value==this.w_ULTDAT)
      this.oPgFrm.Page1.oPag.oULTDAT_1_15.value=this.w_ULTDAT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tcp_infoPag1 as StdContainer
  Width  = 489
  height = 221
  stdWidth  = 489
  stdheight = 221
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNOMPRO_1_1 as StdField with uid="JKACCKCLUZ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NOMPRO", cQueryName = "NOMPRO",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Nome del programma",;
    HelpContextID = 186402977,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=120, Top=128, InputMask=replicate('X',8), readOnly=.T.

  add object oLOGTAB_1_2 as StdField with uid="XPACORHWJU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LOGTAB", cQueryName = "LOGTAB",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Nome logico della tabella",;
    HelpContextID = 171672223,;
   bGlobalFont=.t.,;
    Height=21, Width=209, Left=269, Top=128, InputMask=replicate('X',35), readOnly=.T.

  add object oFISTAB_1_3 as StdField with uid="TZPDAHCDVK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FISTAB", cQueryName = "FISTAB",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome fisico della tabella",;
    HelpContextID = 171675193,;
   bGlobalFont=.t.,;
    Height=21, Width=209, Left=269, Top=155, InputMask=replicate('X',20), readOnly=.T.


  add object oBtn_1_4 as StdButton with uid="YVVNOWRCVM",left=404, top=188, width=74,height=23,;
    caption="Button", nPag=1;
    , HelpContextID = 220967413;
    , caption='\<OK';
  , bGlobalFont=.t.

    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oUTCC_1_8 as StdField with uid="ENTKDWZZCZ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_UTCC", cQueryName = "UTCC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 98135800,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=99, Top=31, cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_UTCC"

  func oUTCC_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oUTCV_1_9 as StdField with uid="WMBYEVJBYU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_UTCV", cQueryName = "UTCV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 98213624,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=99, Top=59, cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_UTCV"

  func oUTCV_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oUTDC_1_10 as StdField with uid="WKMGVJFASA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_UTDC", cQueryName = "UTDC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 98136056,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=346, Top=31

  add object oUTDV_1_11 as StdField with uid="BNKDLQEFSN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_UTDV", cQueryName = "UTDV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 98213880,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=346, Top=59

  add object oDES1_1_12 as StdField with uid="QRLOWCAEAO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 98065911,;
   bGlobalFont=.t.,;
    Height=21, Width=170, Left=152, Top=31, InputMask=replicate('X',20)

  add object oDES2_1_13 as StdField with uid="IUYYVQTEGV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DES2", cQueryName = "DES2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 98070007,;
   bGlobalFont=.t.,;
    Height=21, Width=170, Left=152, Top=59, InputMask=replicate('X',20)

  add object oULTDAT_1_15 as StdField with uid="JYOBHAWQNS",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ULTDAT", cQueryName = "ULTDAT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Data di ultimo aggiornamento del programma",;
    HelpContextID = 190484344,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=120, Top=155, InputMask=replicate('X',15), readOnly=.T.

  add object oStr_1_5 as StdString with uid="RDKCKTHLBX",Visible=.t., Left=5, Top=31,;
    Alignment=1, Width=91, Height=15,;
    Caption="Inserito da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="KVNAYTIWSO",Visible=.t., Left=326, Top=59,;
    Alignment=1, Width=19, Height=15,;
    Caption="Il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="DLEKFHXNTL",Visible=.t., Left=326, Top=31,;
    Alignment=1, Width=19, Height=15,;
    Caption="Il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="AWWXPZBKYA",Visible=.t., Left=5, Top=6,;
    Alignment=0, Width=423, Height=15,;
    Caption="Informazioni utente"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="KGKBVEGBXD",Visible=.t., Left=5, Top=105,;
    Alignment=0, Width=145, Height=15,;
    Caption="Informazioni programma"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="TCIORJSLOU",Visible=.t., Left=6, Top=155,;
    Alignment=1, Width=111, Height=15,;
    Caption="Ultima modifica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="WMJCAKTJWJ",Visible=.t., Left=7, Top=128,;
    Alignment=1, Width=110, Height=15,;
    Caption="Programma:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="TZYRFKZBCM",Visible=.t., Left=5, Top=59,;
    Alignment=1, Width=91, Height=15,;
    Caption="Modificato da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="RXGKVQBIPI",Visible=.t., Left=269, Top=106,;
    Alignment=0, Width=145, Height=15,;
    Caption="Nome tabella"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="OIBSMWNTWV",Visible=.t., Left=212, Top=128,;
    Alignment=1, Width=54, Height=18,;
    Caption="Logico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="AAZOZHRFZA",Visible=.t., Left=212, Top=155,;
    Alignment=1, Width=54, Height=18,;
    Caption="Fisico:"  ;
  , bGlobalFont=.t.

  add object oBox_1_14 as StdBox with uid="QMFCBDXSWR",left=3, top=92, width=476,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('cp_info','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
