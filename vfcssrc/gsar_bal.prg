* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bal                                                        *
*              Elaborazione prezzi articolo                                    *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_268]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-26                                                      *
* Last revis.: 2015-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bal",oParentObject,m.pOPER)
return(i_retval)

define class tgsar_bal as StdBatch
  * --- Local variables
  pOPER = space(4)
  w_CODSEL = space(20)
  w_MAGSEL = space(5)
  w_LISSEL = space(5)
  w_PRESEL = 0
  w_OLDLIS = space(20)
  w_LISIVA = space(1)
  w_PERIVA = 0
  w_DECUNI = 0
  w_GEST = .NULL.
  w_ARTICOLO = .NULL.
  w_PADRE = .NULL.
  w_OSOURCE = .NULL.
  w_DESALT = space(40)
  w_MAGAZZINO = .NULL.
  w_OBJ = .NULL.
  w_OLDCODART = space(20)
  w_QTAMOV = 0
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_SC1 = 0
  w_SC2 = 0
  w_SC3 = 0
  w_SC4 = 0
  w_DATREG = ctod("  /  /  ")
  w_CATCLI = space(5)
  w_CATART = space(5)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MVUNIMIS = space(3)
  w_OLDKEY = space(10)
  w_CODKEY = space(3)
  w_LIUNIMIS = space(3)
  w_QTALIS = 0
  w_CALPRZ = 0
  * --- WorkFile variables
  ART_ICOL_idx=0
  CONTI_idx=0
  KEY_ARTI_idx=0
  TAB_SCON_idx=0
  ART_ALTE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora la situazione dei Prezzi Articolo sui Documenti a Cliente (da GSAR_KCA)
    * --- Filtri Su Articoli
    * --- Filtri su Listini
    * --- Variabili Locali
    this.w_PADRE = this.oParentObject
    this.w_GEST = this.oParentObject.oParentObject
    do case
      case this.pOPER="BLANK" 
        * --- A seconda della gestione di provenienza assegna il valore alle variabili caller
        do case
          case Lower ( this.w_GEST.class ) = "tgsve_mdv" Or Lower ( this.w_GEST.class ) = "tgsor_mdv" or Lower ( this.w_GEST.class ) = "tgsac_mdv"
            this.oParentObject.w_CODART = this.w_GEST.w_MVCODART
            this.oParentObject.w_CODICE = this.w_GEST.w_MVCODICE
            this.oParentObject.w_CODMAG = this.w_GEST.w_MVCODMAG
            this.oParentObject.w_FLSCOR = this.w_GEST.w_MVFLSCOR
            this.oParentObject.w_NUMSCO = this.w_GEST.w_NUMSCO
            this.oParentObject.w_OBTEST = this.w_GEST.w_OBTEST
            * --- Inizializza Variabili Riga Maschera
            this.oParentObject.w_MDESART = this.w_GEST.w_MVDESART
            this.oParentObject.w_MQTAMOV = this.w_GEST.w_MVQTAMOV
            this.oParentObject.w_MUNIMIS = this.w_GEST.w_MVUNIMIS
            this.oParentObject.w_MPREZZO = this.w_GEST.w_MVPREZZO
            this.oParentObject.w_MSCONT1 = IIF(this.oParentObject.w_NUMSCO<1, 0, this.w_GEST.w_MVSCONT1)
            this.oParentObject.w_MSCONT2 = IIF(this.oParentObject.w_NUMSCO<2, 0, this.w_GEST.w_MVSCONT2)
            this.oParentObject.w_MSCONT3 = IIF(this.oParentObject.w_NUMSCO<3, 0, this.w_GEST.w_MVSCONT3)
            this.oParentObject.w_MSCONT4 = IIF(this.oParentObject.w_NUMSCO<4, 0, this.w_GEST.w_MVSCONT4)
            this.oParentObject.w_CODVAL = this.w_GEST.w_MVCODVAL
            this.oParentObject.w_QTAUM1 = this.w_GEST.w_MVQTAUM1
            this.oParentObject.w_DATDOC = this.w_GEST.w_MVDATDOC
            this.w_LISSEL = this.w_GEST.w_MVCODLIS
            this.w_DATREG = IIF(EMPTY(this.oParentObject.w_DATDOC), this.w_GEST.w_MVDATREG, this.oParentObject.w_DATDOC)
            this.oParentObject.w_IVALIS = this.w_GEST.w_IVALIS
          case Lower ( this.w_GEST.class ) = "tcgsof_mdo"
            this.oParentObject.w_CODART = this.w_GEST.w_ODCODART
            this.oParentObject.w_CODICE = this.w_GEST.w_ODCODICE
            this.oParentObject.w_CODMAG = space(5)
            this.oParentObject.w_FLSCOR = this.w_GEST.w_FLSCOR
            this.oParentObject.w_NUMSCO = this.w_GEST.w_NUMSCO
            this.oParentObject.w_OBTEST = this.w_GEST.w_OBTEST
            * --- Inizializza Variabili Riga Maschera
            this.oParentObject.w_MDESART = this.w_GEST.w_ODDESART
            this.oParentObject.w_MQTAMOV = this.w_GEST.w_ODQTAMOV
            this.oParentObject.w_MUNIMIS = this.w_GEST.w_ODUNIMIS
            this.oParentObject.w_MPREZZO = this.w_GEST.w_ODPREZZO
            this.oParentObject.w_MSCONT1 = IIF(this.oParentObject.w_NUMSCO<1, 0, this.w_GEST.w_ODSCONT1)
            this.oParentObject.w_MSCONT2 = IIF(this.oParentObject.w_NUMSCO<2, 0, this.w_GEST.w_ODSCONT2)
            this.oParentObject.w_MSCONT3 = IIF(this.oParentObject.w_NUMSCO<3, 0, this.w_GEST.w_ODSCONT3)
            this.oParentObject.w_MSCONT4 = IIF(this.oParentObject.w_NUMSCO<4, 0, this.w_GEST.w_ODSCONT4)
            this.oParentObject.w_CODVAL = this.w_GEST.w_CODVAL
            this.oParentObject.w_QTAUM1 = this.w_GEST.w_ODQTAUM1
            this.oParentObject.w_DATDOC = this.w_GEST.w_DATDOC
            this.w_LISSEL = this.w_GEST.w_CODLIS
            this.w_DATREG = this.oParentObject.w_DATDOC
            this.oParentObject.w_IVALIS = this.w_GEST.w_IVALIS
            this.oParentObject.w_ALLMAG = "S"
          case Lower ( this.w_GEST.class ) = "tgsps_mvd"
            this.oParentObject.w_CODART = this.w_GEST.w_MDCODART
            this.oParentObject.w_CODICE = this.w_GEST.w_MDCODICE
            this.oParentObject.w_CODMAG = this.w_GEST.w_MDCODMAG
            * --- Se da POS svuoto FLSCOR in modo che vengano presi tutti i tipi di listini (Lordi - Netti)
            *     Dopo la creazione dello zoom in pag 2 per� rimetto FLSCOR a 'S' in modo che calcoli il prezzo correttamente
            this.oParentObject.w_FLSCOR = ""
            this.oParentObject.w_NUMSCO = this.w_GEST.w_NUMSCO
            this.oParentObject.w_OBTEST = this.w_GEST.w_OBTEST
            * --- Inizializza Variabili Riga Maschera
            this.oParentObject.w_MDESART = this.w_GEST.w_MDDESART
            this.oParentObject.w_MQTAMOV = this.w_GEST.w_MDQTAMOV
            this.oParentObject.w_MUNIMIS = this.w_GEST.w_MDUNIMIS
            this.oParentObject.w_MPREZZO = this.w_GEST.w_MDPREZZO
            this.oParentObject.w_MSCONT1 = IIF(this.oParentObject.w_NUMSCO<1, 0, this.w_GEST.w_MDSCONT1)
            this.oParentObject.w_MSCONT2 = IIF(this.oParentObject.w_NUMSCO<2, 0, this.w_GEST.w_MDSCONT2)
            this.oParentObject.w_MSCONT3 = IIF(this.oParentObject.w_NUMSCO<3, 0, this.w_GEST.w_MDSCONT3)
            this.oParentObject.w_MSCONT4 = IIF(this.oParentObject.w_NUMSCO<4, 0, this.w_GEST.w_MDSCONT4)
            this.oParentObject.w_CODVAL = this.w_GEST.w_MDCODVAL
            this.oParentObject.w_QTAUM1 = this.w_GEST.w_MDQTAUM1
            this.oParentObject.w_DATDOC = this.w_GEST.w_MDDATREG
            this.w_LISSEL = this.w_GEST.w_MDCODLIS
            this.w_DATREG = this.oParentObject.w_DATDOC
            this.oParentObject.w_IVALIS = this.w_GEST.w_LISIVA
        endcase
        this.oParentObject.w_MSIMVAL = IIF(EMPTY(this.w_GEST.w_SIMVAL), this.oParentObject.w_CODVAL, this.w_GEST.w_SIMVAL)
        this.oParentObject.w_CODORI = this.oParentObject.w_CODICE
        this.oParentObject.w_MAGORI = this.oParentObject.w_CODMAG
        this.oParentObject.w_MVCODART = this.oParentObject.w_CODART
        this.oParentObject.w_MCODICE = this.oParentObject.w_CODICE
        this.oParentObject.w_MCODMAG = this.oParentObject.w_CODMAG
        this.w_QTAMOV = this.oParentObject.w_MQTAMOV
        this.w_MVUNIMIS = this.oParentObject.w_MUNIMIS
        this.w_UNMIS1 = this.w_GEST.w_UNMIS1
        this.w_UNMIS2 = this.w_GEST.w_UNMIS2
        this.w_UNMIS3 = this.w_GEST.w_UNMIS3
        this.w_OPERAT = this.w_GEST.w_OPERAT
        this.w_OPERA3 = this.w_GEST.w_OPERA3
        this.w_DECUNI = this.w_GEST.w_DECUNI
        this.w_MOLTIP = this.w_GEST.w_MOLTIP
        this.w_MOLTI3 = this.w_GEST.w_MOLTI3
        this.w_PERIVA = this.w_GEST.w_PERIVA
        this.w_CATCLI = this.w_GEST.w_CATSCC
        this.oParentObject.w_QTAUM3 = CALQTA(this.oParentObject.w_QTAUM1,this.oParentObject.w_MUNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.oParentObject.w_MUNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
        this.oParentObject.w_QTAUM2 = CALQTA(this.oParentObject.w_QTAUM1,this.oParentObject.w_MUNMIS2, this.oParentObject.w_MUNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.oParentObject.w_MUNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
        * --- Inizializza gli Zoom
        this.w_PADRE.NotifyEvent("ZoomArtExec")     
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPER="ALLMAG" 
        * --- Inizializza gli Zoom
        this.oParentObject.w_CODMAG = IIF(this.oParentObject.w_ALLMAG=" ", this.oParentObject.w_MAGORI, SPACE(5))
        this.w_PADRE.NotifyEvent("ZoomArtExec")     
        this.w_PADRE.NotifyEvent("Aggprezzi")     
      case this.pOPER="SELART" 
        if this.oParentObject.w_MCODICE<>this.oParentObject.w_CODRIC
          Ah_ErrorMsg("Attenzione: l'articolo � stato cambiato, verificare che la quantit� di riga sia congruente")
        endif
        * --- Selezionato Articolo
        this.w_CODSEL = NVL( this.oParentObject.w_ZoomArt.GetVar("CODICE"), SPACE(20))
        this.w_MAGSEL = NVL( this.oParentObject.w_ZoomArt.GetVar("CODMAG"), SPACE(5))
        if NOT EMPTY(NVL(this.w_CODSEL,""))
          * --- Aggiorna lo Zoom
          if NOT EMPTY(this.w_MAGSEL)
            UPDATE ( this.oParentObject.w_ZoomArt.cCursor ) SET RECSEL=IIF(CODICE=this.w_CODSEL AND CODMAG=this.w_MAGSEL,"X"," ") WHERE NOT EMPTY(CODICE)
          else
            UPDATE ( this.oParentObject.w_ZoomArt.cCursor ) SET RECSEL=IIF(CODICE=this.w_CODSEL AND EMPTY(NVL(CODMAG,"")),"X"," ") WHERE NOT EMPTY(CODICE)
          endif
           
 Select ( this.oParentObject.w_ZoomArt.cCursor ) 
 Go Top
          this.oParentObject.w_ZoomArt.Refresh()     
          * --- Se seleziono un articolo dalla maschera non scatta pi� il meccanismo 
          *     degli articoli alternativi
          this.w_GEST.w_GIACON = "Y"
          * --- Assegna Il Codice di Ricerca sul Documento
          this.w_OSOURCE.xKey( 1 ) = this.w_CODSEL
          * --- Cerca l'oggetto puntato da MVCODICE
          *     Viene utilizzato direttamente il Link poich� su MVCODICE la ecpDrop esegue una SetFocus()
          *     che in questo caso causerebbe problemi nei posizionamenti.
          *     Il Link_2_1 non � quindi sostituibile con la ecpDrop()
          do case
            case Lower ( this.w_GEST.class ) = "tgsve_mdv" Or Lower ( this.w_GEST.class ) = "tgsor_mdv" or Lower ( this.w_GEST.class ) = "tgsac_mdv" or Lower ( this.w_GEST.class ) = "tcgsof_mdo"
              this.w_GEST.link_2_1("Drop",this.w_OSOURCE)     
            case Lower ( this.w_GEST.class ) = "tgsps_mvd"
              this.w_GEST.link_2_6("Drop",this.w_OSOURCE)     
          endcase
          this.w_GEST.mCalc(.T.)     
          this.w_GEST.SetupdateRow()     
          * --- Read from ART_ALTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ALTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ALTE_idx,2],.t.,this.ART_ALTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARDESALT"+;
              " from "+i_cTable+" ART_ALTE where ";
                  +"ARCODALT = "+cp_ToStrODBC(this.w_CODSEL);
                  +" and ARCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARDESALT;
              from (i_cTable) where;
                  ARCODALT = this.w_CODSEL;
                  and ARCODICE = this.oParentObject.w_MVCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESALT = NVL(cp_ToDate(_read_.ARDESALT),cp_NullValue(_read_.ARDESALT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          do case
            case Lower ( this.w_GEST.class ) = "tgsve_mdv" Or Lower ( this.w_GEST.class ) = "tgsor_mdv" or Lower ( this.w_GEST.class ) = "tgsac_mdv"
              this.w_GEST.w_MVDESART = this.w_DESALT
            case Lower ( this.w_GEST.class ) = "tcgsof_mdo"
              this.w_GEST.w_ODDESART = IIF(NOT EMPTY(this.w_DESALT),this.w_DESALT,this.w_GEST.w_ODDESART)
            case Lower ( this.w_GEST.class ) = "tgsps_mvd"
              this.w_GEST.w_MDDESART = IIF(NOT EMPTY(this.w_DESALT),this.w_DESALT,this.w_GEST.w_MDDESART)
          endcase
          if NOT EMPTY(this.w_MAGSEL)
            * --- Cerco Codmag nei campi fissi nel caso chiamata da ordini altrimenti
            *     ricero il control tramite getbodyctrl ( � sul dettaglio )
            if Lower ( this.w_GEST.class ) = "tgsor_mdv"
              this.w_MAGAZZINO = this.w_GEST.GetCtrl("w_MVCODMAG")
            else
              do case
                case Lower ( this.w_GEST.class ) = "tgsve_mdv" or Lower ( this.w_GEST.class ) = "tgsac_mdv"
                  this.w_MAGAZZINO = this.w_GEST.GetBodyCtrl("w_MVCODMAG")
                case Lower ( this.w_GEST.class ) = "tcgsof_mdo"
                  this.w_MAGAZZINO = this.w_GEST.GetBodyCtrl("w_CODMAG")
                case Lower ( this.w_GEST.class ) = "tgsps_mvd"
                  this.w_MAGAZZINO = this.w_GEST.GetCtrl("w_MDCODMAG")
              endcase
            endif
            this.w_OSOURCE.xKey( 1 ) = this.w_MAGSEL
            * --- Cerca l'oggetto puntato da MVCODICE
            this.w_MAGAZZINO.ecpDrop(this.w_OSOURCE)     
          endif
          * --- Chiudo Maschera Prezzi
          this.w_PADRE.ecpquit()     
        endif
      case this.pOPER="SELPRE"
        * --- Selezionato Listino
        this.w_LISSEL = NVL( this.oParentObject.w_ZoomPre.GetVar("CODLIS"), SPACE(5))
        if NOT EMPTY(NVL(this.w_LISSEL,""))
          this.w_PRESEL = NVL( this.oParentObject.w_ZoomPre.GetVar("PREZZO"), 0)
          this.w_LISIVA = NVL( this.oParentObject.w_ZoomPre.GetVar("IVALIS"), " ")
          this.oParentObject.w_MSCONT1 = IIF(this.oParentObject.w_NUMSCO<1, 0, NVL( this.oParentObject.w_ZoomPre.GetVar("SCONT1"), 0))
          this.oParentObject.w_MSCONT2 = IIF(this.oParentObject.w_NUMSCO<2, 0, NVL( this.oParentObject.w_ZoomPre.GetVar("SCONT2"), 0))
          this.oParentObject.w_MSCONT3 = IIF(this.oParentObject.w_NUMSCO<3, 0, NVL( this.oParentObject.w_ZoomPre.GetVar("SCONT3"), 0))
          this.oParentObject.w_MSCONT4 = IIF(this.oParentObject.w_NUMSCO<4, 0, NVL( this.oParentObject.w_ZoomPre.GetVar("SCONT4"), 0))
          * --- Aggiorna lo Zoom
           
 UPDATE ( this.oParentObject.w_ZoomPre.cCursor ) SET RECSEL=IIF(CODLIS=this.w_LISSEL,"X"," ") WHERE NOT DELETED() 
 Select ( this.oParentObject.w_ZoomPre.cCursor ) 
 Go Top
          this.oParentObject.w_ZoomPre.Refresh()     
          * --- Assegna Il Prezzo sul Documento
          this.w_UNMIS1 = this.w_GEST.w_UNMIS1
          this.w_UNMIS2 = this.w_GEST.w_UNMIS2
          this.w_UNMIS3 = this.w_GEST.w_UNMIS3
          this.w_OPERAT = this.w_GEST.w_OPERAT
          this.w_OPERA3 = this.w_GEST.w_OPERA3
          this.w_DECUNI = this.w_GEST.w_DECUNI
          this.w_MOLTIP = this.w_GEST.w_MOLTIP
          this.w_MOLTI3 = this.w_GEST.w_MOLTI3
          this.oParentObject.w_QTAUM3 = CALQTA(this.oParentObject.w_QTAUM1,this.oParentObject.w_MUNMIS3, Space(3),IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.oParentObject.w_MUNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
          this.oParentObject.w_QTAUM2 = CALQTA(this.oParentObject.w_QTAUM1,this.oParentObject.w_MUNMIS2, this.oParentObject.w_MUNMIS2,IIF(this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.oParentObject.w_MUNMIS3, IIF(this.w_OPERA3="/","*","/"), this.w_MOLTI3)
          this.w_LIUNIMIS = NVL(LIUNIMIS, SPACE(3))
          this.w_QTALIS = IIF(this.w_LIUNIMIS=this.w_UNMIS1,this.oParentObject.w_QTAUM1, IIF(this.w_LIUNIMIS=this.w_UNMIS2, this.oParentObject.w_QTAUM2, this.oParentObject.w_QTAUM3))
          do case
            case Lower ( this.w_GEST.class ) = "tgsve_mdv" or Lower ( this.w_GEST.class ) = "tgsac_mdv" or Lower ( this.w_GEST.class ) = "tgsor_mdv"
              this.oParentObject.w_FLSCOR = this.w_GEST.w_MVFLSCOR
              this.w_PERIVA = this.w_GEST.w_PERIVA
              this.w_DECUNI = this.w_GEST.w_DECUNI
              this.w_QTAMOV = this.w_GEST.w_MVQTAMOV
              if this.oParentObject.w_FLSCOR<>"S" AND this.w_LISIVA="L" AND this.w_PERIVA<>0
                this.w_GEST.w_MVPREZZO = CALNET(this.w_PRESEL, this.w_PERIVA, this.w_DECUNI, "   ", 0)
              else
                this.w_GEST.w_MVPREZZO = this.w_PRESEL
              endif
              this.w_GEST.NotifyEvent("w_MVPREZZO Changed")     
              this.w_GEST.w_MVSCONT1 = IIF(this.oParentObject.w_NUMSCO<1, 0, this.oParentObject.w_MSCONT1)
              this.w_GEST.NotifyEvent("w_MVSCONT1 Changed")     
              this.w_GEST.w_MVSCONT2 = IIF(this.oParentObject.w_NUMSCO<2, 0, this.oParentObject.w_MSCONT2)
              this.w_GEST.NotifyEvent("w_MVSCONT2 Changed")     
              this.w_GEST.w_MVSCONT3 = IIF(this.oParentObject.w_NUMSCO<3, 0, this.oParentObject.w_MSCONT3)
              this.w_GEST.NotifyEvent("w_MVSCONT3 Changed")     
              this.w_GEST.w_MVSCONT4 = IIF(this.oParentObject.w_NUMSCO<4, 0, this.oParentObject.w_MSCONT4)
              this.w_GEST.NotifyEvent("w_MVSCONT4 Changed")     
              * --- Esegue i Calcoli
              this.w_GEST.mCalc(.T.)     
              this.w_GEST.SaveDependsOn()     
              * --- Inizializza Variabili Riga Maschera
              this.oParentObject.w_MPREZZO = this.w_GEST.w_MVPREZZO
            case Lower ( this.w_GEST.class ) = "tcgsof_mdo"
              this.oParentObject.w_FLSCOR = this.w_GEST.w_FLSCOR
              this.w_PERIVA = this.w_GEST.w_PERIVA
              this.w_DECUNI = this.w_GEST.w_DECUNI
              this.w_QTAMOV = this.w_GEST.w_ODQTAMOV
              this.w_OBJ = this.w_GEST.GetbodyCtrl("w_ODPREZZO")
              if this.oParentObject.w_FLSCOR<>"S" AND this.w_LISIVA="L" AND this.w_PERIVA<>0
                this.w_GEST.w_ODPREZZO = CALNET(this.w_PRESEL, this.w_PERIVA, this.w_DECUNI, "   ", 0)
                this.w_OBJ.Value = CALNET(this.w_PRESEL, this.w_PERIVA, this.w_DECUNI, "   ", 0)
              else
                this.w_GEST.w_ODPREZZO = this.w_PRESEL
                this.w_OBJ.Value = this.w_PRESEL
              endif
              this.w_OBJ.mCalc(.T.)     
              this.w_GEST.w_ODSCONT1 = IIF(this.oParentObject.w_NUMSCO<1, 0, this.oParentObject.w_MSCONT1)
              this.w_GEST.NotifyEvent("w_ODSCONT1 Changed")     
              this.w_GEST.w_ODSCONT2 = IIF(this.oParentObject.w_NUMSCO<2, 0, this.oParentObject.w_MSCONT2)
              this.w_GEST.NotifyEvent("w_ODSCONT2 Changed")     
              this.w_GEST.w_ODSCONT3 = IIF(this.oParentObject.w_NUMSCO<3, 0, this.oParentObject.w_MSCONT3)
              this.w_GEST.NotifyEvent("w_ODSCONT3 Changed")     
              this.w_GEST.w_ODSCONT4 = IIF(this.oParentObject.w_NUMSCO<4, 0, this.oParentObject.w_MSCONT4)
              * --- Esegue i Calcoli
              this.w_GEST.mCalc(.T.)     
              this.w_GEST.SaveDependsOn()     
              * --- Inizializza Variabili Riga Maschera
              this.oParentObject.w_MPREZZO = this.w_GEST.w_ODPREZZO
            case Lower ( this.w_GEST.class ) = "tgsps_mvd"
              this.w_GEST.w_MDPREZZO = this.w_PRESEL
              this.oParentObject.w_FLSCOR = "S"
              this.w_PERIVA = this.w_GEST.w_PERIVA
              this.w_DECUNI = this.w_GEST.w_DECUNI
              this.w_QTAMOV = this.w_GEST.w_MDQTAMOV
              if this.oParentObject.w_FLSCOR<>"S" AND this.w_LISIVA="L" AND this.w_PERIVA<>0
                this.w_GEST.w_MDPREZZO = CALNET(this.w_PRESEL, this.w_PERIVA, this.w_DECUNI, "   ", 0)
              endif
              this.w_GEST.NotifyEvent("w_MDPREZZO Changed")     
              this.w_GEST.w_MDSCONT1 = IIF(this.oParentObject.w_NUMSCO<1, 0, this.oParentObject.w_MSCONT1)
              this.w_GEST.w_MDSCONT2 = IIF(this.oParentObject.w_NUMSCO<2, 0, this.oParentObject.w_MSCONT2)
              this.w_GEST.w_MDSCONT3 = IIF(this.oParentObject.w_NUMSCO<3, 0, this.oParentObject.w_MSCONT3)
              this.w_GEST.w_MDSCONT4 = IIF(this.oParentObject.w_NUMSCO<4, 0, this.oParentObject.w_MSCONT4)
              * --- Esegue i Calcoli
              this.w_GEST.mCalc(.T.)     
              this.w_GEST.SaveDependsOn()     
              * --- Inizializza Variabili Riga Maschera
              this.oParentObject.w_MPREZZO = this.w_GEST.w_MDPREZZO
          endcase
          * --- Chiudo Maschera Prezzi
          this.w_PADRE.ecpquit()     
        endif
      case this.pOPER="AGGPRE"
        this.w_OLDCODART = this.oParentObject.w_CODART
        this.oParentObject.w_CODART = IIF(NOT EMPTY(NVL(this.oParentObject.w_ZoomArt.GetVar("CACODART"),SPACE(20))), this.oParentObject.w_ZoomArt.GetVar("CACODART"), this.w_OLDCODART)
        this.oParentObject.w_CODRIC = NVL(this.oParentObject.w_ZoomArt.GetVar("CACODICE"), this.oParentObject.w_CODRIC)
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Ripristina il vecchio codice articolo che serve di filtro allo zoom articoli alternativi
        this.oParentObject.w_CODART = this.w_OLDCODART
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Prezzi
    * --- leggo le um con link perch� devo lanciare la query filtrando per le unit� di misura del codice articolo selezionato nello zoom sugli articoli alternativi
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARUNMIS1,ARUNMIS2"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARUNMIS1,ARUNMIS2;
        from (i_cTable) where;
            ARCODART = this.oParentObject.w_CODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_MUNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.oParentObject.w_MUNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CAUNIMIS"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_CODRIC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CAUNIMIS;
        from (i_cTable) where;
            CACODICE = this.oParentObject.w_CODRIC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_MUNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if Lower ( this.w_GEST.class ) = "tgsps_mvd"
      this.oParentObject.w_FLSCOR = ""
    endif
    this.w_PADRE.NotifyEvent("ZoomPreExec")     
    if Lower ( this.w_GEST.class ) = "tgsps_mvd"
      * --- dopo aver preso tutti i tipi di listini rimento lo scorporo a S
      this.oParentObject.w_FLSCOR = "S"
    endif
    if USED( this.oParentObject.w_ZoomPRE.cCursor )
      this.w_OLDLIS = "@##XX@##XX"
      this.w_OLDKEY = "@##XX@##XX"
       
 Select ( this.oParentObject.w_ZoomPRE.cCursor ) 
 Go Top
      SCAN FOR NOT EMPTY(NVL(CODLIS,""))
      this.w_LIUNIMIS = NVL(LIUNIMIS, SPACE(3))
      this.w_PRESEL = PREZZO
      REPLACE PREZZOLIS WITH this.w_PRESEL
      if this.w_PRESEL<>0 AND this.oParentObject.w_MUNIMIS<>NVL(LIUNIMIS,SPACE(3)) AND NOT EMPTY(NVL(LIUNIMIS,SPACE(3)))
        this.w_QTALIS = IIF(this.w_LIUNIMIS=this.w_UNMIS1,this.oParentObject.w_QTAUM1, IIF(this.w_LIUNIMIS=this.w_UNMIS2, this.oParentObject.w_QTAUM2, this.oParentObject.w_QTAUM3))
        this.w_PRESEL = cp_Round(CALMMPZ(this.w_PRESEL, this.oParentObject.w_MQTAMOV, this.w_QTALIS, this.oParentObject.w_IVALIS, IIF(this.oParentObject.w_FLSCOR="S",0,this.w_PERIVA), this.w_DECUNI),this.w_DECUNI)
      endif
      REPLACE PREZZO WITH this.w_PRESEL
      * --- Solo per il POS se il listino � al netto devo reincorporare l'iva
      if Lower ( this.w_GEST.class ) = "tgsps_mvd" And Nvl(IVALIS,"N")="N"
        * --- Aggiunge l'IVA
        this.w_DECUNI = this.w_GEST.w_DECUNI
        this.w_PERIVA = this.w_GEST.w_PERIVA
        this.w_PRESEL = this.w_PRESEL + cp_ROUND((this.w_PRESEL * this.w_PERIVA) / 100, this.w_DECUNI)
        REPLACE PREZZO WITH this.w_PRESEL
      endif
      if CODLIS=this.w_LISSEL
        REPLACE RECSEL WITH "X"
      endif
      if NVL(FLSCON, " ")<>"S"
        * --- Se Sconti Calcolati sugli Scaglioni Sconti/Maggiorazioni
        REPLACE SCONT1 WITH this.w_SC1, SCONT2 WITH this.w_SC2, SCONT3 WITH this.w_SC3, SCONT4 WITH this.w_SC4
      endif
      this.w_PRESEL = 0
      if NVL(PREZZO,0)<>0
        this.w_PRESEL = cp_ROUND(PREZZO * (1+NVL(SCONT1,0)/100)*(1+NVL(SCONT2,0)/100)*(1+NVL(SCONT3,0)/100)*(1+NVL(SCONT4,0)/100), this.w_DECUNI)
      endif
      REPLACE VALRIG WITH this.w_PRESEL
      * --- Azzera gli Sconti non Gestiti
      if this.oParentObject.w_NUMSCO<4
        REPLACE SCONT4 WITH 0
        if this.oParentObject.w_NUMSCO<3
          REPLACE SCONT3 WITH 0
          if this.oParentObject.w_NUMSCO<2
            REPLACE SCONT2 WITH 0
            if this.oParentObject.w_NUMSCO<1
              REPLACE SCONT1 WITH 0
            endif
          endif
        endif
      endif
      this.w_OLDKEY = CODLIS + LIUNIMIS
      ENDSCAN
      * --- Riporto lo zoom alla prima riga...
       
 Select ( this.oParentObject.w_ZoomPRE.cCursor ) 
 Go Top
      this.oParentObject.w_ZoomPre.Refresh()     
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricerca gli Sconti dalla Tabella Sconti Maggiorazioni
    DECLARE ARRCALC (16,1)
    * --- Azzero l'Array che verr� riempito dalla Funzione
    DIMENSION pArrUm[9]
    pArrUm [1] = " " 
 pArrUm [2] = " " 
 pArrUm [3] = 0 
 pArrUm [4] = " " 
 pArrUm [5] = 0 
 pArrUm [6] = " " 
 pArrUm [7] = 0 
 pArrUm [8] = " " 
 pArrUm[9] =0
    ARRCALC(1)=0
    * --- Rileggo solo gli sconti da tabella sconti maggiorazioni
    this.w_CALPRZ = CalPrzli( REPL("X",16) , " " , " " , this.oParentObject.w_CODART , " " , this.oParentObject.w_QTAUM1 , this.oParentObject.w_CODVAL , 0 , this.w_DATREG , this.w_CATCLI , "XXXXXX" , " ", " ", " ", " ", " ", 0,"T", @ARRCALC, " ", " ", "N",@pArrUm )
    this.w_SC1 = ARRCALC(1)
    this.w_SC2 = ARRCALC(2)
    this.w_SC3 = ARRCALC(3)
    this.w_SC4 = ARRCALC(4)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='TAB_SCON'
    this.cWorkTables[5]='ART_ALTE'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
