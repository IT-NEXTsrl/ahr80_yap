* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_ksd                                                        *
*              Dispositivi installati                                          *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_91]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-09-04                                                      *
* Last revis.: 2017-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_ksd",oParentObject))

* --- Class definition
define class tgsut_ksd as StdForm
  Top    = 10
  Left   = 17

  * --- Standard Properties
  Width  = 483
  Height = 335
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-10-18"
  HelpContextID=99173225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  _IDX = 0
  DIS_HARD_IDX = 0
  cPrg = "gsut_ksd"
  cComment = "Dispositivi installati"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODPEN = space(5)
  w_DESPEN = space(40)
  w_TIPPEN = space(1)
  w_CODCAS = space(5)
  w_DESCAS = space(40)
  w_TIPCAS = space(1)
  w_ARCHEASY = .F.
  o_ARCHEASY = .F.
  w_STAARCHI = space(25)
  o_STAARCHI = space(25)
  w_OFFICE = space(1)
  o_OFFICE = space(1)
  w_OFFICBIT = space(1)
  w_AUTOFFICE = .F.
  w_PRINTMERGE = space(1)
  w_SCANNER = space(250)
  w_CODCEN = space(5)
  w_DESCEN = space(40)
  w_TIPCEN = space(1)
  w_OFFTIPCO = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_ksdPag1","gsut_ksd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODPEN_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DIS_HARD'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_ksd
    * Leggo il file CNF...
    This.NotifyEvent('Inizio')
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODPEN=space(5)
      .w_DESPEN=space(40)
      .w_TIPPEN=space(1)
      .w_CODCAS=space(5)
      .w_DESCAS=space(40)
      .w_TIPCAS=space(1)
      .w_ARCHEASY=.f.
      .w_STAARCHI=space(25)
      .w_OFFICE=space(1)
      .w_OFFICBIT=space(1)
      .w_AUTOFFICE=.f.
      .w_PRINTMERGE=space(1)
      .w_SCANNER=space(250)
      .w_CODCEN=space(5)
      .w_DESCEN=space(40)
      .w_TIPCEN=space(1)
      .w_OFFTIPCO=space(1)
      .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .w_CODPEN = IIF( TYPE('g_CODPEN')='C',g_CODPEN, SPACE(5))
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODPEN))
          .link_1_2('Full')
        endif
          .DoRTCalc(2,3,.f.)
        .w_CODCAS = IIF( TYPE('g_CODCAS')='C',g_CODCAS, SPACE(5))
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODCAS))
          .link_1_6('Full')
        endif
          .DoRTCalc(5,6,.f.)
        .w_ARCHEASY = iif(type('g_PRINTERARCHI')<>'U' and not empty(g_PRINTERARCHI),.T.,.F.)
        .w_STAARCHI = IIF(.w_ARCHEASY,IIF(Empty(g_PRINTERARCHI),'Stampante Archeasy',g_PRINTERARCHI),'')
        .w_OFFICE = IIF( TYPE('g_OFFICE')='C',g_OFFICE, 'N')
        .w_OFFICBIT = IIF( VARTYPE(g_OFFICE_BIT)='C' and g_OFFICE_BIT$'3-6',g_OFFICE_BIT, '3') 
        .w_AUTOFFICE = .w_OFFICE = 'M'
        .w_PRINTMERGE = IIF( TYPE('g_PRINTMERGE')='C',g_PRINTMERGE, '2')
        .w_SCANNER = IIF( TYPE('g_SCANNER')='C',g_SCANNER, '')
        .w_CODCEN = IIF( TYPE('g_PhoneService')='C', g_PhoneService, SPACE(5))
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CODCEN))
          .link_1_22('Full')
        endif
          .DoRTCalc(15,16,.f.)
        .w_OFFTIPCO = IIF( EMPTY(.w_OFFTIPCO), IIF( VARTYPE(g_OFFICE_TYPE_CONV)='C' and g_OFFICE_TYPE_CONV$'N-S-C',g_OFFICE_TYPE_CONV, 'N'), .w_OFFTIPCO)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .DoRTCalc(1,6,.t.)
        if .o_STAARCHI<>.w_STAARCHI
            .w_ARCHEASY = iif(type('g_PRINTERARCHI')<>'U' and not empty(g_PRINTERARCHI),.T.,.F.)
        endif
        if .o_ARCHEASY<>.w_ARCHEASY
            .w_STAARCHI = IIF(.w_ARCHEASY,IIF(Empty(g_PRINTERARCHI),'Stampante Archeasy',g_PRINTERARCHI),'')
        endif
        .DoRTCalc(9,10,.t.)
        if .o_OFFICE<>.w_OFFICE
            .w_AUTOFFICE = .w_OFFICE = 'M'
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
    endwith
  return

  proc Calculate_VCBGBUVZLC()
    with this
          * --- Salva variabili pubbliche gsut_bld (S)
          gsut_bld(this;
              ,'S';
              ,.w_CODPEN;
              ,.w_CODCAS;
              ,IIF(type('g_CODNEG')<>'U',g_CODNEG,'');
              ,.w_OFFICE;
              ,.w_PRINTMERGE;
              ,.w_STAARCHI;
              ,.w_SCANNER;
              ,.w_CODCEN;
              ,.w_OFFICBIT;
              ,.w_OFFTIPCO;
              ,.w_AUTOFFICE;
             )
    endwith
  endproc
  proc Calculate_TQIVIOVEHF()
    with this
          * --- valorizza AUTOFFICE da dis_inst
          .w_AUTOFFICE = IIF( TYPE('g_AUTOFFICE')<>'U', g_AUTOFFICE, (.w_OFFICE='M'))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oARCHEASY_1_10.enabled = this.oPgFrm.Page1.oPag.oARCHEASY_1_10.mCond()
    this.oPgFrm.Page1.oPag.oSTAARCHI_1_11.enabled = this.oPgFrm.Page1.oPag.oSTAARCHI_1_11.mCond()
    this.oPgFrm.Page1.oPag.oAUTOFFICE_1_14.enabled = this.oPgFrm.Page1.oPag.oAUTOFFICE_1_14.mCond()
    this.oPgFrm.Page1.oPag.oPRINTMERGE_1_16.enabled_(this.oPgFrm.Page1.oPag.oPRINTMERGE_1_16.mCond())
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODCAS_1_6.visible=!this.oPgFrm.Page1.oPag.oCODCAS_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDESCAS_1_8.visible=!this.oPgFrm.Page1.oPag.oDESCAS_1_8.mHide()
    this.oPgFrm.Page1.oPag.oPRINTMERGE_1_16.visible=!this.oPgFrm.Page1.oPag.oPRINTMERGE_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCODCEN_1_22.visible=!this.oPgFrm.Page1.oPag.oCODCEN_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oDESCEN_1_27.visible=!this.oPgFrm.Page1.oPag.oDESCEN_1_27.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_1.Event(cEvent)
        if lower(cEvent)==lower("Salva")
          .Calculate_VCBGBUVZLC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_TQIVIOVEHF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODPEN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_lTable = "DIS_HARD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2], .t., this.DIS_HARD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APD',True,'DIS_HARD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DHCODICE like "+cp_ToStrODBC(trim(this.w_CODPEN)+"%");

          i_ret=cp_SQL(i_nConn,"select DHCODICE,DHDESCRI,DHTIPDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DHCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DHCODICE',trim(this.w_CODPEN))
          select DHCODICE,DHDESCRI,DHTIPDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DHCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPEN)==trim(_Link_.DHCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODPEN) and !this.bDontReportError
            deferred_cp_zoom('DIS_HARD','*','DHCODICE',cp_AbsName(oSource.parent,'oCODPEN_1_2'),i_cWhere,'GSAR_APD',"Elenco dispositivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DHCODICE,DHDESCRI,DHTIPDIS";
                     +" from "+i_cTable+" "+i_lTable+" where DHCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DHCODICE',oSource.xKey(1))
            select DHCODICE,DHDESCRI,DHTIPDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DHCODICE,DHDESCRI,DHTIPDIS";
                   +" from "+i_cTable+" "+i_lTable+" where DHCODICE="+cp_ToStrODBC(this.w_CODPEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DHCODICE',this.w_CODPEN)
            select DHCODICE,DHDESCRI,DHTIPDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPEN = NVL(_Link_.DHCODICE,space(5))
      this.w_DESPEN = NVL(_Link_.DHDESCRI,space(40))
      this.w_TIPPEN = NVL(_Link_.DHTIPDIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODPEN = space(5)
      endif
      this.w_DESPEN = space(40)
      this.w_TIPPEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPPEN='P'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Dispositivo non codificato o non di tipo penna ottica")
        endif
        this.w_CODPEN = space(5)
        this.w_DESPEN = space(40)
        this.w_TIPPEN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])+'\'+cp_ToStr(_Link_.DHCODICE,1)
      cp_ShowWarn(i_cKey,this.DIS_HARD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAS
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_lTable = "DIS_HARD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2], .t., this.DIS_HARD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APD',True,'DIS_HARD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DHCODICE like "+cp_ToStrODBC(trim(this.w_CODCAS)+"%");

          i_ret=cp_SQL(i_nConn,"select DHCODICE,DHDESCRI,DHTIPDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DHCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DHCODICE',trim(this.w_CODCAS))
          select DHCODICE,DHDESCRI,DHTIPDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DHCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAS)==trim(_Link_.DHCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAS) and !this.bDontReportError
            deferred_cp_zoom('DIS_HARD','*','DHCODICE',cp_AbsName(oSource.parent,'oCODCAS_1_6'),i_cWhere,'GSAR_APD',"Elenco dispositivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DHCODICE,DHDESCRI,DHTIPDIS";
                     +" from "+i_cTable+" "+i_lTable+" where DHCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DHCODICE',oSource.xKey(1))
            select DHCODICE,DHDESCRI,DHTIPDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DHCODICE,DHDESCRI,DHTIPDIS";
                   +" from "+i_cTable+" "+i_lTable+" where DHCODICE="+cp_ToStrODBC(this.w_CODCAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DHCODICE',this.w_CODCAS)
            select DHCODICE,DHDESCRI,DHTIPDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAS = NVL(_Link_.DHCODICE,space(5))
      this.w_DESCAS = NVL(_Link_.DHDESCRI,space(40))
      this.w_TIPCAS = NVL(_Link_.DHTIPDIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAS = space(5)
      endif
      this.w_DESCAS = space(40)
      this.w_TIPCAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPCAS='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Dispositivo non codificato o non di tipo penna ottica")
        endif
        this.w_CODCAS = space(5)
        this.w_DESCAS = space(40)
        this.w_TIPCAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])+'\'+cp_ToStr(_Link_.DHCODICE,1)
      cp_ShowWarn(i_cKey,this.DIS_HARD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCEN
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIS_HARD_IDX,3]
    i_lTable = "DIS_HARD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2], .t., this.DIS_HARD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APD',True,'DIS_HARD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DHCODICE like "+cp_ToStrODBC(trim(this.w_CODCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select DHCODICE,DHDESCRI,DHTIPDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DHCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DHCODICE',trim(this.w_CODCEN))
          select DHCODICE,DHDESCRI,DHTIPDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DHCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCEN)==trim(_Link_.DHCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCEN) and !this.bDontReportError
            deferred_cp_zoom('DIS_HARD','*','DHCODICE',cp_AbsName(oSource.parent,'oCODCEN_1_22'),i_cWhere,'GSAR_APD',"Elenco dispositivi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DHCODICE,DHDESCRI,DHTIPDIS";
                     +" from "+i_cTable+" "+i_lTable+" where DHCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DHCODICE',oSource.xKey(1))
            select DHCODICE,DHDESCRI,DHTIPDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DHCODICE,DHDESCRI,DHTIPDIS";
                   +" from "+i_cTable+" "+i_lTable+" where DHCODICE="+cp_ToStrODBC(this.w_CODCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DHCODICE',this.w_CODCEN)
            select DHCODICE,DHDESCRI,DHTIPDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCEN = NVL(_Link_.DHCODICE,space(5))
      this.w_DESCEN = NVL(_Link_.DHDESCRI,space(40))
      this.w_TIPCEN = NVL(_Link_.DHTIPDIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCEN = space(5)
      endif
      this.w_DESCEN = space(40)
      this.w_TIPCEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPCEN='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Dispositivo non codificato o non di tipo centralino")
        endif
        this.w_CODCEN = space(5)
        this.w_DESCEN = space(40)
        this.w_TIPCEN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIS_HARD_IDX,2])+'\'+cp_ToStr(_Link_.DHCODICE,1)
      cp_ShowWarn(i_cKey,this.DIS_HARD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODPEN_1_2.value==this.w_CODPEN)
      this.oPgFrm.Page1.oPag.oCODPEN_1_2.value=this.w_CODPEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPEN_1_4.value==this.w_DESPEN)
      this.oPgFrm.Page1.oPag.oDESPEN_1_4.value=this.w_DESPEN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAS_1_6.value==this.w_CODCAS)
      this.oPgFrm.Page1.oPag.oCODCAS_1_6.value=this.w_CODCAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAS_1_8.value==this.w_DESCAS)
      this.oPgFrm.Page1.oPag.oDESCAS_1_8.value=this.w_DESCAS
    endif
    if not(this.oPgFrm.Page1.oPag.oARCHEASY_1_10.RadioValue()==this.w_ARCHEASY)
      this.oPgFrm.Page1.oPag.oARCHEASY_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAARCHI_1_11.value==this.w_STAARCHI)
      this.oPgFrm.Page1.oPag.oSTAARCHI_1_11.value=this.w_STAARCHI
    endif
    if not(this.oPgFrm.Page1.oPag.oOFFICE_1_12.RadioValue()==this.w_OFFICE)
      this.oPgFrm.Page1.oPag.oOFFICE_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOFFICBIT_1_13.RadioValue()==this.w_OFFICBIT)
      this.oPgFrm.Page1.oPag.oOFFICBIT_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAUTOFFICE_1_14.RadioValue()==this.w_AUTOFFICE)
      this.oPgFrm.Page1.oPag.oAUTOFFICE_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRINTMERGE_1_16.RadioValue()==this.w_PRINTMERGE)
      this.oPgFrm.Page1.oPag.oPRINTMERGE_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSCANNER_1_21.value==this.w_SCANNER)
      this.oPgFrm.Page1.oPag.oSCANNER_1_21.value=this.w_SCANNER
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCEN_1_22.value==this.w_CODCEN)
      this.oPgFrm.Page1.oPag.oCODCEN_1_22.value=this.w_CODCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCEN_1_27.value==this.w_DESCEN)
      this.oPgFrm.Page1.oPag.oDESCEN_1_27.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oOFFTIPCO_1_30.RadioValue()==this.w_OFFTIPCO)
      this.oPgFrm.Page1.oPag.oOFFTIPCO_1_30.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPPEN='P')  and not(empty(.w_CODPEN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPEN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dispositivo non codificato o non di tipo penna ottica")
          case   not(.w_TIPCAS='R')  and not(g_APPLICATION <> "ADHOC REVOLUTION")  and not(empty(.w_CODCAS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCAS_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dispositivo non codificato o non di tipo penna ottica")
          case   not(.w_TIPCEN='C')  and not(g_AGFA<>'S')  and not(empty(.w_CODCEN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCEN_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Dispositivo non codificato o non di tipo centralino")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_ksd
      * - AL Salvataggio
      .NotifyEvent('Salva')
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ARCHEASY = this.w_ARCHEASY
    this.o_STAARCHI = this.w_STAARCHI
    this.o_OFFICE = this.w_OFFICE
    return

enddefine

* --- Define pages as container
define class tgsut_ksdPag1 as StdContainer
  Width  = 479
  height = 335
  stdWidth  = 479
  stdheight = 335
  resizeXpos=307
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_1 as cp_runprogram with uid="FZAYKELAJD",left=548, top=26, width=153,height=24,;
    caption='GSUT_BLD(L)',;
   bGlobalFont=.t.,;
    prg="GSUT_BLD('L',' ',' ',' ',' ',' ',' ',' ')",;
    cEvent = "Inizio",;
    nPag=1;
    , ToolTipText = "Legge file di configurazione legato alla macchina";
    , HelpContextID = 228375766

  add object oCODPEN_1_2 as StdField with uid="QCBNKYXEBT",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODPEN", cQueryName = "CODPEN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Dispositivo non codificato o non di tipo penna ottica",;
    ToolTipText = "Selezionare il dispositivo penna ottica da installare sulla macchina in uso",;
    HelpContextID = 213602342,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=129, Top=7, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIS_HARD", cZoomOnZoom="GSAR_APD", oKey_1_1="DHCODICE", oKey_1_2="this.w_CODPEN"

  func oCODPEN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPEN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPEN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIS_HARD','*','DHCODICE',cp_AbsName(this.parent,'oCODPEN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APD',"Elenco dispositivi",'',this.parent.oContained
  endproc
  proc oCODPEN_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DHCODICE=this.parent.oContained.w_CODPEN
     i_obj.ecpSave()
  endproc

  add object oDESPEN_1_4 as StdField with uid="DIMPXPTYVZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESPEN", cQueryName = "DESPEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 213661238,;
   bGlobalFont=.t.,;
    Height=21, Width=277, Left=194, Top=7, InputMask=replicate('X',40)

  add object oCODCAS_1_6 as StdField with uid="FCUWOHBIJT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODCAS", cQueryName = "CODCAS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Dispositivo non codificato o non di tipo penna ottica",;
    ToolTipText = "Selezionare il registratore di cassa da installare sulla macchina in uso",;
    HelpContextID = 24006694,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=129, Top=36, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIS_HARD", cZoomOnZoom="GSAR_APD", oKey_1_1="DHCODICE", oKey_1_2="this.w_CODCAS"

  func oCODCAS_1_6.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION")
    endwith
  endfunc

  func oCODCAS_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAS_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAS_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIS_HARD','*','DHCODICE',cp_AbsName(this.parent,'oCODCAS_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APD',"Elenco dispositivi",'',this.parent.oContained
  endproc
  proc oCODCAS_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DHCODICE=this.parent.oContained.w_CODCAS
     i_obj.ecpSave()
  endproc

  add object oDESCAS_1_8 as StdField with uid="EWOACDPETX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESCAS", cQueryName = "DESCAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 24065590,;
   bGlobalFont=.t.,;
    Height=21, Width=277, Left=194, Top=36, InputMask=replicate('X',40)

  func oDESCAS_1_8.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oARCHEASY_1_10 as StdCheck with uid="GYOWLKUGEG",rtseq=7,rtrep=.f.,left=130, top=63, caption="Installazione Archeasy",;
    ToolTipText = "Attiva la stampante di Archeasy",;
    HelpContextID = 263406431,;
    cFormVar="w_ARCHEASY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oARCHEASY_1_10.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oARCHEASY_1_10.GetRadio()
    this.Parent.oContained.w_ARCHEASY = this.RadioValue()
    return .t.
  endfunc

  func oARCHEASY_1_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ARCHEASY==.T.,1,;
      0)
  endfunc

  func oARCHEASY_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ARCHI='S')
    endwith
   endif
  endfunc

  add object oSTAARCHI_1_11 as StdField with uid="ZZTASNRLON",rtseq=8,rtrep=.f.,;
    cFormVar = "w_STAARCHI", cQueryName = "STAARCHI",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il nome della stampante di Archeasy",;
    HelpContextID = 226744721,;
   bGlobalFont=.t.,;
    Height=21, Width=343, Left=128, Top=87, InputMask=replicate('X',25)

  func oSTAARCHI_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ARCHEASY and g_ARCHI='S')
    endwith
   endif
  endfunc


  add object oOFFICE_1_12 as StdCombo with uid="AGNCCFAPSO",rtseq=9,rtrep=.f.,left=128,top=115,width=342,height=21;
    , ToolTipText = "Permette di selezionare la suite Office utilizzata localmente";
    , HelpContextID = 60057574;
    , cFormVar="w_OFFICE",RowSource=""+"Microsoft Office,"+"OpenOffice/StarOffice,"+"Non utilizzata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOFFICE_1_12.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'O',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oOFFICE_1_12.GetRadio()
    this.Parent.oContained.w_OFFICE = this.RadioValue()
    return .t.
  endfunc

  func oOFFICE_1_12.SetRadio()
    this.Parent.oContained.w_OFFICE=trim(this.Parent.oContained.w_OFFICE)
    this.value = ;
      iif(this.Parent.oContained.w_OFFICE=='M',1,;
      iif(this.Parent.oContained.w_OFFICE=='O',2,;
      iif(this.Parent.oContained.w_OFFICE=='N',3,;
      0)))
  endfunc


  add object oOFFICBIT_1_13 as StdCombo with uid="LINKLBYHWL",rtseq=10,rtrep=.f.,left=128,top=142,width=126,height=22;
    , HelpContextID = 258709446;
    , cFormVar="w_OFFICBIT",RowSource=""+"32 bit,"+"64 bit", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOFFICBIT_1_13.RadioValue()
    return(iif(this.value =1,'3',;
    iif(this.value =2,'6',;
    space(1))))
  endfunc
  func oOFFICBIT_1_13.GetRadio()
    this.Parent.oContained.w_OFFICBIT = this.RadioValue()
    return .t.
  endfunc

  func oOFFICBIT_1_13.SetRadio()
    this.Parent.oContained.w_OFFICBIT=trim(this.Parent.oContained.w_OFFICBIT)
    this.value = ;
      iif(this.Parent.oContained.w_OFFICBIT=='3',1,;
      iif(this.Parent.oContained.w_OFFICBIT=='6',2,;
      0))
  endfunc

  add object oAUTOFFICE_1_14 as StdCheck with uid="XOALXYWCAF",rtseq=11,rtrep=.f.,left=297, top=141, caption="Automazione office",;
    ToolTipText = "Se attivo utilizza l'automazione dei componenti office per la creazione dei file",;
    HelpContextID = 187999591,;
    cFormVar="w_AUTOFFICE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAUTOFFICE_1_14.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oAUTOFFICE_1_14.GetRadio()
    this.Parent.oContained.w_AUTOFFICE = this.RadioValue()
    return .t.
  endfunc

  func oAUTOFFICE_1_14.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_AUTOFFICE==.T.,1,;
      0)
  endfunc

  func oAUTOFFICE_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OFFICE = 'M')
    endwith
   endif
  endfunc

  add object oPRINTMERGE_1_16 as StdRadio with uid="RXBBDWMQJK",rtseq=12,rtrep=.f.,left=129, top=202, width=144,height=23;
    , ToolTipText = "Permette di inviare direttamente alla stampante i file di OpenOffice generati con il Mailmerge";
    , cFormVar="w_PRINTMERGE", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPRINTMERGE_1_16.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="File"
      this.Buttons(1).HelpContextID = 212463032
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("File","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Stampante"
      this.Buttons(2).HelpContextID = 212463032
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Stampante","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Permette di inviare direttamente alla stampante i file di OpenOffice generati con il Mailmerge")
      StdRadio::init()
    endproc

  func oPRINTMERGE_1_16.RadioValue()
    return(iif(this.value =1,'2',;
    iif(this.value =2,'1',;
    space(1))))
  endfunc
  func oPRINTMERGE_1_16.GetRadio()
    this.Parent.oContained.w_PRINTMERGE = this.RadioValue()
    return .t.
  endfunc

  func oPRINTMERGE_1_16.SetRadio()
    this.Parent.oContained.w_PRINTMERGE=trim(this.Parent.oContained.w_PRINTMERGE)
    this.value = ;
      iif(this.Parent.oContained.w_PRINTMERGE=='2',1,;
      iif(this.Parent.oContained.w_PRINTMERGE=='1',2,;
      0))
  endfunc

  func oPRINTMERGE_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_OFFICE='O')
    endwith
   endif
  endfunc

  func oPRINTMERGE_1_16.mHide()
    with this.Parent.oContained
      return (.w_OFFICE<>'O')
    endwith
  endfunc


  add object oBtn_1_18 as StdButton with uid="PWCUCFWQQP",left=372, top=284, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte sulla macchina client";
    , HelpContextID = 99152666;
    , caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="LNMWHHBLEZ",left=424, top=284, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 223547142;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSCANNER_1_21 as StdField with uid="BTDVDDBKCR",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SCANNER", cQueryName = "SCANNER",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Scanner utilizzato per catturare allegati",;
    HelpContextID = 71898406,;
   bGlobalFont=.t.,;
    Height=21, Width=315, Left=128, Top=231, InputMask=replicate('X',250)

  add object oCODCEN_1_22 as StdField with uid="AXBLFMGMNT",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODCEN", cQueryName = "CODCEN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Dispositivo non codificato o non di tipo centralino",;
    ToolTipText = "Selezionare il dispositivo centralino da installare sulla macchina in uso",;
    HelpContextID = 212750374,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=128, Top=258, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIS_HARD", cZoomOnZoom="GSAR_APD", oKey_1_1="DHCODICE", oKey_1_2="this.w_CODCEN"

  func oCODCEN_1_22.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  func oCODCEN_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCEN_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCEN_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DIS_HARD','*','DHCODICE',cp_AbsName(this.parent,'oCODCEN_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APD',"Elenco dispositivi",'',this.parent.oContained
  endproc
  proc oCODCEN_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DHCODICE=this.parent.oContained.w_CODCEN
     i_obj.ecpSave()
  endproc


  add object oBtn_1_24 as StdButton with uid="CNUJDIXTJR",left=447, top=231, width=24,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare lo scanner predefinito per questa postazione";
    , HelpContextID = 98972202;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      do GSUT_KSS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCEN_1_27 as StdField with uid="PGWYTJQGKR",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 212809270,;
   bGlobalFont=.t.,;
    Height=21, Width=277, Left=194, Top=258, InputMask=replicate('X',40)

  func oDESCEN_1_27.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc


  add object oOFFTIPCO_1_30 as StdCombo with uid="LLASEKSEGW",rtseq=17,rtrep=.f.,left=128,top=171,width=342,height=21;
    , ToolTipText = "Permette di impostare il tipo di conversione dei caratteri da utilizzare in caso di esportazione dati con suite office/openoffice a 64 bit";
    , HelpContextID = 251619381;
    , cFormVar="w_OFFTIPCO",RowSource=""+"Nessuna,"+"Caratteri accentati con non accentati,"+"Caratteri accentati con non accentati e apostrofo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOFFTIPCO_1_30.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oOFFTIPCO_1_30.GetRadio()
    this.Parent.oContained.w_OFFTIPCO = this.RadioValue()
    return .t.
  endfunc

  func oOFFTIPCO_1_30.SetRadio()
    this.Parent.oContained.w_OFFTIPCO=trim(this.Parent.oContained.w_OFFTIPCO)
    this.value = ;
      iif(this.Parent.oContained.w_OFFTIPCO=='N',1,;
      iif(this.Parent.oContained.w_OFFTIPCO=='S',2,;
      iif(this.Parent.oContained.w_OFFTIPCO=='C',3,;
      0)))
  endfunc

  add object oStr_1_3 as StdString with uid="ZRUSGLFDRH",Visible=.t., Left=64, Top=7,;
    Alignment=1, Width=64, Height=18,;
    Caption="Penna:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="LWPRHDGMCL",Visible=.t., Left=64, Top=36,;
    Alignment=1, Width=64, Height=18,;
    Caption="Cassa:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="CDVBUNFHTW",Visible=.t., Left=63, Top=115,;
    Alignment=1, Width=65, Height=18,;
    Caption="Suite Office:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="FRUQICREFR",Visible=.t., Left=42, Top=202,;
    Alignment=1, Width=86, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_OFFICE<>'O')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="BRHLQKZUEK",Visible=.t., Left=7, Top=87,;
    Alignment=1, Width=121, Height=18,;
    Caption="Stampante Archeasy:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="JMNDLXVBAN",Visible=.t., Left=24, Top=232,;
    Alignment=1, Width=104, Height=18,;
    Caption="Dispositivo twain:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="NRDNZORAFG",Visible=.t., Left=64, Top=258,;
    Alignment=1, Width=64, Height=18,;
    Caption="Centralino:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (g_AGFA<>'S')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="MLDBWGKTWY",Visible=.t., Left=45, Top=144,;
    Alignment=1, Width=83, Height=18,;
    Caption="Versione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="LXIKPXIOQO",Visible=.t., Left=45, Top=173,;
    Alignment=1, Width=83, Height=18,;
    Caption="Conversione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_ksd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
