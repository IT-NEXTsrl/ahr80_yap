* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bsv                                                        *
*              Lettura schede contabili                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_280]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-26                                                      *
* Last revis.: 2012-03-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEvento,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bsv",oParentObject,m.pEvento,m.pParam)
return(i_retval)

define class tgscg_bsv as StdBatch
  * --- Local variables
  pEvento = space(10)
  pParam = space(1)
  w_PRIESE = space(4)
  w_DATPRE = ctod("  /  /  ")
  w_PREFIN = ctod("  /  /  ")
  w_PREINI = ctod("  /  /  ")
  w_ESEINI = space(4)
  w_PREESE = space(4)
  w_PRESUC = space(4)
  w_ESEPRE = space(4)
  w_VALINI = space(3)
  w_SALFIN = 0
  w_CAMBIO = 0
  w_APPO = space(4)
  w_APPO1 = space(4)
  w_APPO2 = 0
  w_CODCON = space(15)
  w_CONTRO = space(15)
  w_IMPCON = 0
  w_RIGCON = 0
  w_CONSUP = space(15)
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_SALDO = 0
  w_SALPRO = 0
  w_SCRIVO = .f.
  w_TESTAT = .f.
  w_QUOZIENT = 0
  w_FLCALC = space(1)
  w_SERIAL = space(10)
  w_NURIGA = 0
  w_CODES1 = space(4)
  w_FLPRO1 = space(1)
  w_FLSALI = space(1)
  w_FLSALF = space(1)
  w_DATREG = ctod("  /  /  ")
  w_NUMRER = 0
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_NUMPRO = 0
  w_ALFPRO = space(10)
  w_DESSUP = space(50)
  w_DESRIG = space(50)
  w_FINES1 = ctod("  /  /  ")
  w_INICOM = ctod("  /  /  ")
  w_FINCOM = ctod("  /  /  ")
  w_OLDDAT = ctod("  /  /  ")
  w_DESCAU = space(35)
  w_DESCON = space(40)
  w_OLDES1 = space(4)
  w_ULTESE = space(4)
  w_NEWCON = space(15)
  w_OLDCON = space(15)
  w_DESCRI = space(50)
  w_PNCAOVAL = 0
  w_PNCODVAL = space(3)
  w_ZOOM = space(10)
  w_SALDO = 0
  w_SALINI = 0
  w_TOTDARC = 0
  w_TOTAVEC = 0
  w_FLCALC = space(1)
  w_CONSUP = space(15)
  w_BILSEZ = space(1)
  w_ESEINI = space(4)
  w_OLDCON = space(15)
  w_NEWCON = space(15)
  w_TIPO = space(1)
  w_CAUNOP = space(5)
  w_ESEGUI = .f.
  w_ROTTCONTR = space(32)
  w_CAURIG = space(5)
  w_ROWORD = 0
  w_FLZERO = space(1)
  w_DESPRI = space(50)
  w_FLPART = space(1)
  w_LFLPART = space(1)
  w_SALDOINI = 0
  w_PREDATREG = ctod("  /  /  ")
  w_SALDOFIN = 0
  w_CALC_CONTR = .f.
  w_MAXRIGA = 0
  w_MAXROWORD = 0
  w_TESTINS = .f.
  w_LNURIGA = 0
  w_CPROWORD = 0
  * --- WorkFile variables
  ESERCIZI_idx=0
  SALDICON_idx=0
  TMPSCO_idx=0
  TMPVEND1_idx=0
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora Lettura 
    *     Schede Contabili (da GSCG_SZM)
    *     Stampa Scheda (da GSCG_SMA)
    * --- Controllo se il batch � lanciato dalla Visualizzazione o dalla Stampa
    * --- pParam='V'  Visualizzazione
    *     pParam='S'  Stampa
    if this.pParam="V"
      * --- Se da Primanota Inizializza la Data Finale = Data Registrazione
      this.oParentObject.w_DATFIN = IIF(this.pEvento="Inizio" AND this.oParentObject.w_PNOTA=.T., this.oParentObject.w_PDATFIN, this.oParentObject.w_DATFIN)
    else
      do case
        case NOT EMPTY(this.oParentObject.w_CODFIN) AND this.oParentObject.w_CODFIN<this.oParentObject.w_CODINI
          ah_ErrorMsg("Codice di fine selezione minore del codice di inizio selezione",,"")
          i_retcode = 'stop'
          return
        case EMPTY(this.oParentObject.w_CODVAL) OR this.oParentObject.w_CAOVAL=0
          ah_ErrorMsg("Codice valuta di stampa non definita o cambio inesistente",,"")
          i_retcode = 'stop'
          return
      endcase
      * --- Read from CONTROPA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COCAUNOP"+;
          " from "+i_cTable+" CONTROPA where ";
              +"COCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COCAUNOP;
          from (i_cTable) where;
              COCODAZI = this.oParentObject.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAUNOP = NVL(cp_ToDate(_read_.COCAUNOP),cp_NullValue(_read_.COCAUNOP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_ESEGUI = IIF(this.pParam="V", this.pEvento="Esegui" OR (this.pEvento="Inizio" AND this.oParentObject.w_PNOTA=.T.), .T.)
    if this.w_ESEGUI
      if this.oParentObject.w_FLAPER<>"S"
        * --- Nel caso di Flag Considera solo movimenti del periodo non calcolo saldo iniziale
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.pParam="V"
        if EMPTY(this.oParentObject.w_CODICE)
          ah_ErrorMsg("Inserire un codice cliente o fornitore o conto",,"")
          i_retcode = 'stop'
          return
        endif
        * --- Create temporary table TMPSCO
        i_nIdx=cp_AddTableDef('TMPSCO') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('query\gscg_szm',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPSCO_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Se non inserito esercizio o la data di fine selezioe e' minore della fine esercizio non deve ricercare ev.reg esterne di competenza
        * --- Idem se la data di fine selezioe maggiore o uguale della data di fine esercizio successi (sarebbe compresa comunque)
        this.oParentObject.w_ZoomScad.cCpQueryName = "Query\GSCG_SZ1"
        this.oParentObject.NotifyEvent("Calcola")
        * --- Cambio gli importi non in moneta di Conto nel cursore a VIDEO
        this.w_ZOOM = this.oParentObject.w_ZoomScad
        * --- Ho 2 casi possibili o l'esercizio scelto (se vuoto quello valido alla data iniziale) e il precedente
        * --- hanno valute diverse, oppure l'esercizio scelto e quello attuale hanno valute diverse
        * --- Guardo le valute dei documenti all'interno della selezione - diverse da quella di conto
        SELECT VALNAZ FROM (this.w_ZOOM.cCursor) WHERE VALNAZ<>g_PERVAL AND NOT EMPTY(NVL(VALNAZ,"    ")) ;
        GROUP BY VALNAZ INTO CURSOR TESTVAL
        GO TOP
        * --- Non posso avere come risultato 2 divise
        * --- Se si devono gestire pi� valute posso calcolare tutti i tassi metterli in un array
        * --- assieme al codice valuta e utilizzare la ASCAN per recuperare il cambio da applicare
        if RECCOUNT("TESTVAL")>0
          * --- Calcolo il cambio dell'unica valuta che posso incontrare
          this.w_CAMBIO = GETCAM(TESTVAL.VALNAZ,GETVALUT(g_PERVAL, "VADATEUR"), 0)
          * --- Modifico il cursore a video
          if this.w_CAMBIO<>0
            SELECT (this.w_ZOOM.cCursor)
            GO TOP
            UPDATE (this.w_ZOOM.cCursor) SET IMPDAR = VAL2VAL(IMPDAR, CAOVAL, DATREG,g_FINESE, this.oParentObject.w_CAOVAL, GETVALUT(g_PERVAL, "VADECTOT")), ;
            IMPAVE = VAL2VAL(IMPAVE, CAOVAL, DATREG,g_FINESE, this.oParentObject.w_CAOVAL, GETVALUT(g_PERVAL, "VADECTOT")) ;
            WHERE VALNAZ<>g_PERVAL
          endif
        endif
        if used("TESTVAL")
          select TESTVAL
          use
        endif
        this.oParentObject.w_INIDAR = IIF(this.w_SALDO>0, this.w_SALDO, 0)
        this.oParentObject.w_INIAVE = IIF(this.w_SALDO<0, ABS(this.w_SALDO), 0)
        * --- Mi posizione sull'oggettino Zoom per fare i totali
        this.oParentObject.w_TOTDAR = 0
        this.oParentObject.w_TOTAVE = 0
        this.w_SALPRO = this.w_SALDO
        * --- Setta Riga che non concorrono al Calcolo (caso: Registrazioni interne all'intervallo ma di compenenza precedente..)
        SELECT (this.w_ZOOM.cCursor)
        GO TOP
        SCAN
        * --- Se 'N non concorre per i calcolo del saldo progressivo
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_FLCALC="N"
          REPLACE FLCALC WITH "N"
        endif
        this.w_SALPRO = this.w_SALPRO+ IIF(this.w_FLCALC="N" OR this.oParentObject.w_FLPROV="S", 0 , (IMPDAR-IMPAVE))
        if ((NOT EMPTY(this.oParentObject.w_CODESE) AND this.w_BILSEZ $ "CR") OR NOT this.w_BILSEZ $ "CR")
          REPLACE SALPRO WITH this.w_SALPRO
        endif
        ENDSCAN
        * --- Totali Progressivi di Riga
        GO TOP
        SUM IMPDAR, IMPAVE TO this.oParentObject.w_TOTDAR, this.oParentObject.w_TOTAVE FOR NVL(FLCALC, " ")<>"N"
        * --- Totali Saldi Finali (Stesso Filtro dei Totali Progressivi + Flag Confermate)
        this.w_TOTDARC = 0
        this.w_TOTAVEC = 0
        SELECT (this.w_ZOOM.cCursor)
        GO TOP
        SUM IMPDAR, IMPAVE TO this.w_TOTDARC, this.w_TOTAVEC FOR NVL(FLCALC, " ")<>"N" AND NVL(FLPROV, " ")<>"S" AND ;
        (COMPET=this.w_ULTESE OR NOT this.w_BILSEZ $ "CR")
        this.w_SALFIN = IIF(this.w_ULTESE=this.w_ESEPRE OR NOT this.w_BILSEZ $ "CR", this.w_SALDO, 0) + (this.w_TOTDARC - this.w_TOTAVEC)
        this.oParentObject.w_FINDAR = IIF(this.w_SALFIN>0, this.w_SALFIN, 0)
        this.oParentObject.w_FINAVE = IIF(this.w_SALFIN<0, ABS(this.w_SALFIN), 0)
        VVP=20*G_PERPVL
        * --- Rendo il cursore non modificabile dall'utente
        SELECT (this.w_ZOOM.cCursor)
        GO TOP
        this.w_ZOOM.refresh()
        this.w_ZOOM.grd.readonly=.t.
      else
        * --- Create temporary table TMPSCO
        i_nIdx=cp_AddTableDef('TMPSCO') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('gscg_svs',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMPSCO_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        vq_exec("GSCG_QSM.VQR",this,"RIGDET")
        if this.oParentObject.w_FLCONT $ "S-T"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_OLDCON = "###@@@XXXZZZ"
        this.w_OLDDAT = cp_CharToDate("  -  -  ")
        this.w_OLDES1 = "xxxx"
        SELECT RIGDET
        GO TOP
        SCAN FOR NOT EMPTY(NVL(CODCON, " ")) 
        * --- Per determinare le contropartite eseguo una rottura al cambio di
        *     SERIALE
        *     CONTO
        *     CAUSALE DI RIGA
        *     D o A per segno importo (Z righe a zero)
        *     (Tipo conto non necessario in quanto filtro sempre presente)
        * --- Causale di riga, utilizzata per determinare le contropartite
        this.w_LFLPART = Nvl(PNFLPART,"N")
        this.w_FLZERO = Nvl(RigDet.Flzero," ")
        this.w_ROWORD = RigDet.RowOrd
        this.w_CAURIG = CAURIG
        this.w_CODCON = CODCON
        this.w_DESPRI = Nvl(DESCON,Space(50))
        this.w_CONSUP = NVL(CONSUP, SPACE(15))
        this.w_TESTAT = .F.
        this.w_SALDO = 0
        this.w_PNCAOVAL = NVL(PNCAOVAL, 0)
        this.w_PNCODVAL = NVL(PNCODVAL, SPACE(3))
        if this.w_OLDCON<>this.w_CODCON
          this.w_SALPRO = 0
          this.w_SALDOFIN = 0
          this.w_OLDCON = this.w_CODCON
          this.w_SCRIVO = .T.
          if NOT EMPTY(this.w_CONSUP) 
            this.w_BILSEZ = CALCSEZ(this.w_CONSUP)
          endif
          if this.oParentObject.w_SEZBIL<>"S"
            * --- Elimina le Sezioni di Bilancio non selezionate
            this.w_SCRIVO = IIF(this.w_BILSEZ=this.oParentObject.w_SEZBIL, .T. , .F.)
          endif
          if this.oParentObject.w_FLAPER<>"S"
            * --- Ad ogni Nuovo Codice scrive (sul primo record il Saldo Iniziale)
            if this.w_SCRIVO 
              this.w_TESTAT = .T.
              this.w_SALDOINI = 0
              SELECT SALPRE
              GO TOP
              LOCATE FOR CODCON=this.w_CODCON
              if FOUND()
                this.w_PRIESE = NVL(PRIESE,SPACE(4))
                * --- Saldo Iniziale
                this.w_SALDO = SALREG
                if this.oParentObject.w_CODVAL<>g_PERVAL AND this.oParentObject.w_CAMREG<>"S"
                  * --- Converte alla Valuta di Stampa
                  this.w_SALDO = VAL2MON(this.w_SALDO, this.oParentObject.w_CAOVAL, 1,I_DATSYS, this.oParentObject.w_CODVAL , this.oParentObject.w_DECTOT)
                endif
                this.w_SALPRO = this.w_SALDO
                this.w_SALDOINI = this.w_SALDO
                this.w_SALDOFIN = this.w_SALDOINI
                this.w_BILSEZ = SEZBIL
              else
                this.w_SALPRO = 0
              endif
            endif
          endif
          SELECT RIGDET
        endif
        if this.w_SCRIVO
          this.w_IMPDAR = NVL(IMPDAR, 0)
          this.w_IMPAVE = NVL(IMPAVE, 0)
          this.w_VALINI = NVL(VALNAZ, g_PERVAL)
          if this.w_VALINI <> g_PERVAL
            * --- Converte registrazioni caricate in valuta di conto diversa alla Valuta di Conto in corso
            this.w_IMPDAR = VAL2VAL(this.w_IMPDAR, GETCAM(this.w_VALINI, I_DATSYS), I_DATSYS,g_FINESE, this.oParentObject.w_CAOVAL, this.oParentObject.w_DECTOT)
            this.w_IMPAVE = VAL2VAL(this.w_IMPAVE, GETCAM(this.w_VALINI, I_DATSYS), I_DATSYS,g_FINESE, this.oParentObject.w_CAOVAL, this.oParentObject.w_DECTOT)
          endif
          do case
            case this.oParentObject.w_CODVAL<>g_PERVAL and (this.oParentObject.w_CODVAL<> this.w_PNCODVAL and this.oParentObject.w_CAMREG="S") or this.oParentObject.w_CAMREG<>"S"
              * --- -Registrazione in valuta diversa dalla valuta di conto 
              *     -Attivato  cambio in registrazione e valuta registrazione diiversa dalla valuta di stampa
              *     Non sto stampando in valuta di conto
              *     devo applicare all'importo espresso gi� in valuta di conto della registrazione
              *     il cambio indicato in stampa
              this.w_IMPDAR = VAL2VAL(this.w_IMPDAR, 1, I_DATSYS,g_FINESE, this.oParentObject.w_CAOVAL, this.oParentObject.w_DECTOT)
              this.w_IMPAVE = VAL2VAL(this.w_IMPAVE, 1, I_DATSYS,g_FINESE, this.oParentObject.w_CAOVAL, this.oParentObject.w_DECTOT)
            case this.oParentObject.w_CODVAL<>g_PERVAL and this.oParentObject.w_CODVAL= this.w_PNCODVAL and this.oParentObject.w_CAMREG="S" and this.w_LFLPART="N"
              * --- -Attivato  cambio in registrazione e valuta registrazione uguale alla valuta di stampa
              *     Non sto stampando in valuta di conto
              *     devo applicare all'importo espresso gi� in valuta di conto della registrazione
              *     il cambio indicato nella registrazione
              this.w_IMPDAR = VAL2VAL(this.w_IMPDAR, 1, I_DATSYS,g_FINESE, this.w_PNCAOVAL, this.oParentObject.w_DECTOT)
              this.w_IMPAVE = VAL2VAL(this.w_IMPAVE, 1, I_DATSYS,g_FINESE, this.w_PNCAOVAL, this.oParentObject.w_DECTOT)
          endcase
          if this.oParentObject.w_PERCOM="S" AND NOT EMPTY(CP_TODATE(INICOM)) AND NOT EMPTY(CP_TODATE(FINCOM))
            * --- Determina l'importo di competenza relativo al periodo in questione
            * --- w_QUOZIENT conta i giorni di competenza non compresi nel periodo richiesto
            do case
              case CP_TODATE(FINCOM)<CP_TODATE(this.oParentObject.w_DATINI) OR CP_TODATE(INICOM)>CP_TODATE(this.oParentObject.w_DATFIN)
                this.w_QUOZIENT = CP_TODATE(FINCOM)-CP_TODATE(INICOM)+1
              case CP_TODATE(INICOM)<CP_TODATE(this.oParentObject.w_DATINI) AND NOT CP_TODATE(FINCOM)>CP_TODATE(this.oParentObject.w_DATFIN)
                this.w_QUOZIENT = CP_TODATE(this.oParentObject.w_DATINI)-CP_TODATE(INICOM)
              case CP_TODATE(INICOM)<CP_TODATE(this.oParentObject.w_DATINI) AND CP_TODATE(FINCOM)>CP_TODATE(this.oParentObject.w_DATFIN)
                this.w_QUOZIENT = CP_TODATE(this.oParentObject.w_DATINI)-CP_TODATE(INICOM)+CP_TODATE(FINCOM)-CP_TODATE(this.oParentObject.w_DATFIN)
              case NOT CP_TODATE(INICOM)<CP_TODATE(this.oParentObject.w_DATINI) AND NOT CP_TODATE(FINCOM)>CP_TODATE(this.oParentObject.w_DATFIN)
                this.w_QUOZIENT = 0
              case NOT CP_TODATE(INICOM)<CP_TODATE(this.oParentObject.w_DATINI) AND CP_TODATE(FINCOM)>CP_TODATE(this.oParentObject.w_DATFIN)
                this.w_QUOZIENT = CP_TODATE(FINCOM)-CP_TODATE(this.oParentObject.w_DATFIN)
            endcase
            * --- w_QUOZIENT diventa la porzione di competenza relativa al periodo in questione
            this.w_QUOZIENT = 1-(this.w_QUOZIENT/(CP_TODATE(FINCOM)-CP_TODATE(INICOM)+1))
            this.w_IMPDAR = cp_ROUND(this.w_IMPDAR * this.w_QUOZIENT, 6)
            this.w_IMPAVE = cp_ROUND(this.w_IMPAVE * this.w_QUOZIENT, 6)
          endif
          this.w_IMPDAR = cp_ROUND(this.w_IMPDAR, this.oParentObject.w_DECTOT)
          this.w_IMPAVE = cp_ROUND(this.w_IMPAVE, this.oParentObject.w_DECTOT)
          this.w_SALDO = cp_ROUND(this.w_SALDO, this.oParentObject.w_DECTOT)
          this.w_SALPRO = cp_ROUND(this.w_SALPRO, this.oParentObject.w_DECTOT)
          this.w_SERIAL = NVL(SERIAL, SPACE(10))
          this.w_NURIGA = NVL(NURIGA, 0)
          this.w_CODES1 = NVL(COMPET, SPACE(4))
          this.w_FLPRO1 = NVL(FLPROV, " ")
          this.w_FLSALI = NVL(SALINI, " ")
          this.w_FLSALF = NVL(SALFIN, " ")
          this.w_DATREG = CP_TODATE(DATREG)
          this.w_NUMRER = NVL(NUMRER, 0)
          this.w_NUMDOC = NVL(NUMDOC, 0)
          this.w_ALFDOC = NVL(ALFDOC, "  ")
          this.w_DATDOC = CP_TODATE(DATDOC)
          this.w_NUMPRO = NVL(NUMPRO, 0)
          this.w_ALFPRO = NVL(ALFPRO, Space(10))
          this.w_DESSUP = NVL(DESSUP, " ")
          this.w_DESRIG = NVL(DESRIG, " ")
          this.w_FINES1 = CP_TODATE(ESFINESE)
          this.w_INICOM = CP_TODATE(INICOM)
          this.w_FINCOM = CP_TODATE(FINCOM)
          this.w_DESCAU = NVL(DESCAU, " ")
          this.w_DESCON = SUBSTR (NVL(DESCON, " "),1,50)
          this.w_DESCRI = NVL(DESCRI, " ")
          if this.w_TESTAT
            * --- Scrive il Record di testata , per fare apparire il saldo come primo record detraggo
            *     0.1 da ROWORD
            * --- Insert into TMPSCO
            i_nConn=i_TableProp[this.TMPSCO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPSCO_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPSCO_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CODCON"+",TIPREC"+",DATREG"+",IMPDAR"+",IMPAVE"+",SALPRO"+",VALNAZ"+",SALREG"+",DESCON"+",NURIGA"+",SERIAL"+",SALINI"+",SALFIN"+",CONSUP"+",FLCALC"+",BILSEZ"+",DESPRI"+",TIPCON"+",CONTRO"+",DESSUP"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_CODCON),'TMPSCO','CODCON');
              +","+cp_NullLink(cp_ToStrODBC("A"),'TMPSCO','TIPREC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DATPRE),'TMPSCO','DATREG');
              +","+cp_NullLink(cp_ToStrODBC(0),'TMPSCO','IMPDAR');
              +","+cp_NullLink(cp_ToStrODBC(0),'TMPSCO','IMPAVE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SALPRO),'TMPSCO','SALPRO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_VALINI),'TMPSCO','VALNAZ');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SALDO),'TMPSCO','SALREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DESCON),'TMPSCO','DESCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD-0.1),'TMPSCO','NURIGA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMPSCO','SERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLSALI),'TMPSCO','SALINI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLSALF),'TMPSCO','SALFIN');
              +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'TMPSCO','CONSUP');
              +","+cp_NullLink(cp_ToStrODBC(" "),'TMPSCO','FLCALC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_BILSEZ),'TMPSCO','BILSEZ');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DESPRI),'TMPSCO','DESPRI');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPCON),'TMPSCO','TIPCON');
              +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'TMPSCO','CONTRO');
              +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'TMPSCO','DESSUP');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CODCON',this.w_CODCON,'TIPREC',"A",'DATREG',this.w_DATPRE,'IMPDAR',0,'IMPAVE',0,'SALPRO',this.w_SALPRO,'VALNAZ',this.w_VALINI,'SALREG',this.w_SALDO,'DESCON',this.w_DESCON,'NURIGA',this.w_ROWORD-0.1,'SERIAL',this.w_SERIAL,'SALINI',this.w_FLSALI)
              insert into (i_cTable) (CODCON,TIPREC,DATREG,IMPDAR,IMPAVE,SALPRO,VALNAZ,SALREG,DESCON,NURIGA,SERIAL,SALINI,SALFIN,CONSUP,FLCALC,BILSEZ,DESPRI,TIPCON,CONTRO,DESSUP &i_ccchkf. );
                 values (;
                   this.w_CODCON;
                   ,"A";
                   ,this.w_DATPRE;
                   ,0;
                   ,0;
                   ,this.w_SALPRO;
                   ,this.w_VALINI;
                   ,this.w_SALDO;
                   ,this.w_DESCON;
                   ,this.w_ROWORD-0.1;
                   ,this.w_SERIAL;
                   ,this.w_FLSALI;
                   ,this.w_FLSALF;
                   ,SPACE(15);
                   ," ";
                   ,this.w_BILSEZ;
                   ,this.w_DESPRI;
                   ,this.oParentObject.w_TIPCON;
                   ,SPACE(15);
                   ,SPACE(10);
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='ERRORE TIPREC A'
              return
            endif
            this.w_OLDES1 = this.w_CODES1
            this.w_OLDDAT = this.w_DATREG
          endif
          * --- Se 'N non concorre per i calcolo del saldo progressivo
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.oParentObject.w_CAMREG="S" AND NOT EMPTY(this.w_FLSALI)
            * --- Nel caso di fleg cambio di registrazione per distinguere prima apertura dalle 
            *     successive ( che non partecipano al tottale progressivo) eseguo query  filtrata
            *     per data registrazione -1 ovvero verifico la presenza di registrazioni precedenti.
            this.w_PREDATREG = this.w_DATREG-1
            vq_exec("query\GSCGGQSM.VQR",this,"REGPRE")
            Select REGPRE
            if Reccount("REGPRE")=0
              this.w_FLCALC = " "
            endif
            Select REGPRE 
 USE
          endif
          * --- Scrive le righe di Stampa (Per il Saldo Finale, Esclude anche le Righe non Confermate)
          * --- Se 'N' non stampa il valore del saldo (cambio data)
          SELECT RIGDET
          this.w_SALPRO = this.w_SALPRO+ IIF(this.w_FLCALC="N" OR this.w_FLPRO1="S", 0 , (this.w_IMPDAR-this.w_IMPAVE))
          * --- Utilizzo la variabile w_SALDOFIN per calcolare il progressivo correttamente e poterlo sbiancare a seconda della condizione.
          *     Se mantenevo la variabile w_SALPRO, azzeravo anche il saldo iniziale.
          do case
            case this.w_BILSEZ $ "CR" AND this.w_FLSALF<>" " AND this.w_CODES1= this.w_ULTESE AND Not Empty(this.oParentObject.w_CODESE) AND this.oParentObject.w_FLAPER <>"S"
              * --- Se � un costo o ricavo, se � una registrazione di chiusura e siamo nello stesso esercizio,  faccio ripartire il saldo finale
              this.w_SALDOFIN = -(this.w_IMPDAR-this.w_IMPAVE)
            otherwise
              * --- In tutti gli altri casi si comporta come w_SALPRO.
              this.w_SALDOFIN = this.w_SALDOFIN+ IIF(this.w_FLCALC="N" OR this.w_FLPRO1="S", 0 , (this.w_IMPDAR-this.w_IMPAVE))
          endcase
          if this.w_BILSEZ $ "CR" AND this.w_DATREG<= this.w_FINES1 
            this.w_OLDES1 = this.w_CODES1
          endif
          this.w_CONTRO = SPACE(15)
          this.w_IMPCON = 0
          this.w_DESCRI = ""
          this.w_DESCON = SPACE(40)
          this.w_DESPRI = NVL(DESCON, SPACE(40))
          this.w_RIGCON = -1
          this.w_CALC_CONTR = .F.
          * --- Se stampa , ricerca le Contropartite
          if this.oParentObject.w_FLCONT$ "S-T" AND USED("CALSAL")
            * --- Quando raggiungo la rottura devo sapere qual'� il numero di riga
            *     massimo raggiunto per quel conto/casuale/sezione dare/avere. 
            *     Questo lo utilizzer� per caricare le contropartite e per modificare quel recor
            *     con le informazioni della prima contropartita.
            this.w_MAXRIGA = IIF( this.w_MAXRIGA < this.w_NURIGA , this.w_NURIGA , this.w_MAXRIGA)
            * --- Utilizzato per valorizzare il numero riga delle contropartite...
            this.w_MAXROWORD = IIF( this.w_MAXROWORD< this.w_ROWORD , this.w_ROWORD , this.w_MAXROWORD )
            this.w_ROTTCONTR = RigDet.SERIAL + RigDet.CODCON + RigDet.CAURIG +this.w_FLZERO +IIF(this.w_IMPDAR<>0, "D", "A")
            * --- Le contropartite le calcolo solo quando giungo, data una registrazione, al cambio 
            *     di conto e di causale di riga, riga a zero, cambio segno (dare/avere)
            *     
            *     Devo sapere se sono al'ultimo record prima della rottura, quindi vado a 
            *     leggere queti dati nel record successivo (se Eof devo cmq calcolare le
            *     contropartite)
            if Not Eof("RigDet")
              Skip
              * --- Se il record successivo provoca la rottura allora aggiorno la variabile di
              *     rottura e determino le contropartite...
              this.w_CALC_CONTR = ( this.w_ROTTCONTR<>RigDet.SERIAL + RigDet.CODCON + RigDet.CAURIG + Nvl(RigDet.FLZERO," ") + IIF( RigDet.IMPDAR<>0, "D", "A" ) )
              * --- Riposiziono il cursore alla sua riga originaria...
              Skip -1
            else
              this.w_CALC_CONTR = .T.
            endif
            if this.w_CALC_CONTR
              * --- Cerco la prima contropartita 
               
 Select CalSal 
 Go Top 
 Locate For CalSal.CODCON=this.w_CODCON AND CalSal.SERIAL=this.w_SERIAL AND CalSal.NURIGA=this.w_NURIGA
              if FOUND()
                this.w_CONTRO = NVL(CONTRO, SPACE(15))
                this.w_IMPCON = NVL(IMPCON, 0)
                this.w_DESCRI = NVL(DESCRI, " ")
                this.w_DESCON = SUBSTR (NVL(DESCON, SPACE(40)),1,50)
                this.w_APPO1 = NVL(VALNAZ, g_PERVAL)
                if this.w_APPO1<>this.oParentObject.w_CODVAL
                  if this.w_APPO1<>g_PERVAL
                    * --- Se valuta esercizio diversa
                    this.w_IMPCON = cp_ROUND((this.w_IMPCON / IIF(this.w_APPO1=g_CODLIR, g_CAOEUR, 1)) * g_CAOVAL, g_PERPVL)
                  endif
                  if this.oParentObject.w_CODVAL<>g_PERVAL
                    * --- Converte alla Valuta di Stampa
                    if this.oParentObject.w_CAMREG="S"
                      if this.oParentObject.w_CODVAL=this.w_PNCODVAL
                        this.w_IMPCON = cp_ROUND((this.w_IMPCON / g_CAOVAL) * this.w_PNCAOVAL, this.oParentObject.w_DECTOT)
                      else
                        this.w_IMPCON = cp_ROUND((this.w_IMPCON / g_CAOVAL) * this.oParentObject.w_CAOVAL,this.oParentObject.w_DECTOT)
                      endif
                    else
                      this.w_IMPCON = cp_ROUND((this.w_IMPCON / g_CAOVAL) * this.oParentObject.w_CAOVAL, this.oParentObject.w_DECTOT)
                    endif
                  endif
                endif
                this.w_RIGCON = NVL(RIGCON, 0)
              endif
            endif
          endif
          this.w_TESTINS = .T.
          this.w_LNURIGA = 0
          if this.oParentObject.w_CAMREG="S"
            * --- Read from TMPSCO
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TMPSCO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPSCO_idx,2],.t.,this.TMPSCO_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "NURIGA"+;
                " from "+i_cTable+" TMPSCO where ";
                    +"SERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                    +" and NURIGA = "+cp_ToStrODBC(this.w_NURIGA);
                    +" and TIPREC = "+cp_ToStrODBC("B");
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                NURIGA;
                from (i_cTable) where;
                    SERIAL = this.w_SERIAL;
                    and NURIGA = this.w_NURIGA;
                    and TIPREC = "B";
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_LNURIGA = NVL(cp_ToDate(_read_.NURIGA),cp_NullValue(_read_.NURIGA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_TESTINS = IIF(i_ROWS<>0, .F., .T.)
            if NOT this.w_TESTINS
              * --- Write into TMPSCO
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPSCO_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPSCO_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPSCO_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"IMPAVE =IMPAVE+ "+cp_ToStrODBC(this.w_IMPAVE);
                +",IMPDAR =IMPDAR+ "+cp_ToStrODBC(this.w_IMPDAR);
                +",SALPRO ="+cp_NullLink(cp_ToStrODBC(this.w_SALPRO),'TMPSCO','SALPRO');
                +",SALDOFIN ="+cp_NullLink(cp_ToStrODBC(this.w_SALDOFIN),'TMPSCO','SALDOFIN');
                    +i_ccchkf ;
                +" where ";
                    +"SERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                    +" and NURIGA = "+cp_ToStrODBC(this.w_NURIGA);
                    +" and TIPREC = "+cp_ToStrODBC("B");
                       )
              else
                update (i_cTable) set;
                    IMPAVE = IMPAVE + this.w_IMPAVE;
                    ,IMPDAR = IMPDAR + this.w_IMPDAR;
                    ,SALPRO = this.w_SALPRO;
                    ,SALDOFIN = this.w_SALDOFIN;
                    &i_ccchkf. ;
                 where;
                    SERIAL = this.w_SERIAL;
                    and NURIGA = this.w_NURIGA;
                    and TIPREC = "B";

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
          if this.w_TESTINS
            * --- Insert into TMPSCO
            i_nConn=i_TableProp[this.TMPSCO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPSCO_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPSCO_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CODCON"+",TIPREC"+",DATREG"+",NUMRER"+",NUMDOC"+",ALFDOC"+",DATDOC"+",NUMPRO"+",ALFPRO"+",SERIAL"+",NURIGA"+",COMPET"+",IMPDAR"+",IMPAVE"+",SALPRO"+",FLCALC"+",FLPROV"+",VALNAZ"+",SALINI"+",SALFIN"+",DESSUP"+",DESRIG"+",ESFINESE"+",SALREG"+",INICOM"+",FINCOM"+",DESCAU"+",CONTRO"+",IMPCON"+",DESCRI"+",DESCON"+",ROWORD"+",CONSUP"+",SALDOFIN"+",BILSEZ"+",DESPRI"+",TIPCON"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_CODCON),'TMPSCO','CODCON');
              +","+cp_NullLink(cp_ToStrODBC("B"),'TMPSCO','TIPREC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'TMPSCO','DATREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRER),'TMPSCO','NUMRER');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC),'TMPSCO','NUMDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ALFDOC),'TMPSCO','ALFDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DATDOC),'TMPSCO','DATDOC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMPRO),'TMPSCO','NUMPRO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ALFPRO),'TMPSCO','ALFPRO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMPSCO','SERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NURIGA),'TMPSCO','NURIGA');
              +","+cp_NullLink(cp_ToStrODBC(this.w_CODES1),'TMPSCO','COMPET');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDAR),'TMPSCO','IMPDAR');
              +","+cp_NullLink(cp_ToStrODBC(this.w_IMPAVE),'TMPSCO','IMPAVE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SALPRO),'TMPSCO','SALPRO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLCALC),'TMPSCO','FLCALC');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLPRO1),'TMPSCO','FLPROV');
              +","+cp_NullLink(cp_ToStrODBC(this.w_VALINI),'TMPSCO','VALNAZ');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLSALI),'TMPSCO','SALINI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FLSALF),'TMPSCO','SALFIN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'TMPSCO','DESSUP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DESRIG),'TMPSCO','DESRIG');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FINESE),'TMPSCO','ESFINESE');
              +","+cp_NullLink(cp_ToStrODBC(0),'TMPSCO','SALREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_INICOM),'TMPSCO','INICOM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_FINCOM),'TMPSCO','FINCOM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DESCAU),'TMPSCO','DESCAU');
              +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'TMPSCO','CONTRO');
              +","+cp_NullLink(cp_ToStrODBC(0),'TMPSCO','IMPCON');
              +","+cp_NullLink(cp_ToStrODBC(SPACE(50)),'TMPSCO','DESCRI');
              +","+cp_NullLink(cp_ToStrODBC(SPACE(40)),'TMPSCO','DESCON');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'TMPSCO','ROWORD');
              +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'TMPSCO','CONSUP');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SALDOFIN),'TMPSCO','SALDOFIN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_BILSEZ),'TMPSCO','BILSEZ');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DESPRI),'TMPSCO','DESPRI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_TIPO),'TMPSCO','TIPCON');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CODCON',this.w_CODCON,'TIPREC',"B",'DATREG',this.w_DATREG,'NUMRER',this.w_NUMRER,'NUMDOC',this.w_NUMDOC,'ALFDOC',this.w_ALFDOC,'DATDOC',this.w_DATDOC,'NUMPRO',this.w_NUMPRO,'ALFPRO',this.w_ALFPRO,'SERIAL',this.w_SERIAL,'NURIGA',this.w_NURIGA,'COMPET',this.w_CODES1)
              insert into (i_cTable) (CODCON,TIPREC,DATREG,NUMRER,NUMDOC,ALFDOC,DATDOC,NUMPRO,ALFPRO,SERIAL,NURIGA,COMPET,IMPDAR,IMPAVE,SALPRO,FLCALC,FLPROV,VALNAZ,SALINI,SALFIN,DESSUP,DESRIG,ESFINESE,SALREG,INICOM,FINCOM,DESCAU,CONTRO,IMPCON,DESCRI,DESCON,ROWORD,CONSUP,SALDOFIN,BILSEZ,DESPRI,TIPCON &i_ccchkf. );
                 values (;
                   this.w_CODCON;
                   ,"B";
                   ,this.w_DATREG;
                   ,this.w_NUMRER;
                   ,this.w_NUMDOC;
                   ,this.w_ALFDOC;
                   ,this.w_DATDOC;
                   ,this.w_NUMPRO;
                   ,this.w_ALFPRO;
                   ,this.w_SERIAL;
                   ,this.w_NURIGA;
                   ,this.w_CODES1;
                   ,this.w_IMPDAR;
                   ,this.w_IMPAVE;
                   ,this.w_SALPRO;
                   ,this.w_FLCALC;
                   ,this.w_FLPRO1;
                   ,this.w_VALINI;
                   ,this.w_FLSALI;
                   ,this.w_FLSALF;
                   ,this.w_DESSUP;
                   ,this.w_DESRIG;
                   ,this.oParentObject.w_FINESE;
                   ,0;
                   ,this.w_INICOM;
                   ,this.w_FINCOM;
                   ,this.w_DESCAU;
                   ,SPACE(15);
                   ,0;
                   ,SPACE(50);
                   ,SPACE(40);
                   ,this.w_ROWORD;
                   ,SPACE(15);
                   ,this.w_SALDOFIN;
                   ,this.w_BILSEZ;
                   ,this.w_DESPRI;
                   ,this.w_TIPO;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='ERRORE TIPREC B'
              return
            endif
          endif
          if this.oParentObject.w_FLCONT$ "S-T" AND this.w_RIGCON<>-1 And this.w_CALC_CONTR
            * --- Aggiorno l'ultima riga, in ordine di stampa (quindi con w_NURIGA=w_MAXRIGA)
            *     con i dati relativi alla contropartita.
            if this.oParentObject.w_FLSTCONT="N" OR (CalSal.CCFLSALI # "S" AND CalSal.CCFLSALF # "S" )
              * --- Write into TMPSCO
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.TMPSCO_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPSCO_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPSCO_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CONTRO ="+cp_NullLink(cp_ToStrODBC(this.w_CONTRO),'TMPSCO','CONTRO');
                +",IMPCON ="+cp_NullLink(cp_ToStrODBC(this.w_IMPCON),'TMPSCO','IMPCON');
                +",DESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'TMPSCO','DESCRI');
                +",DESCON ="+cp_NullLink(cp_ToStrODBC(this.w_DESCON),'TMPSCO','DESCON');
                    +i_ccchkf ;
                +" where ";
                    +"SERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                    +" and CODCON = "+cp_ToStrODBC(this.w_CODCON);
                    +" and NURIGA = "+cp_ToStrODBC(this.w_MAXRIGA);
                       )
              else
                update (i_cTable) set;
                    CONTRO = this.w_CONTRO;
                    ,IMPCON = this.w_IMPCON;
                    ,DESCRI = this.w_DESCRI;
                    ,DESCON = this.w_DESCON;
                    &i_ccchkf. ;
                 where;
                    SERIAL = this.w_SERIAL;
                    and CODCON = this.w_CODCON;
                    and NURIGA = this.w_MAXRIGA;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            * --- ATTENZIONE non spostare il Puntatore a RIGCON
            Select CalSal
            SCAN WHILE CalSal.CODCON=this.w_CODCON AND CalSal.SERIAL=this.w_SERIAL AND CalSal.NURIGA=this.w_NURIGA
            * --- Per stampare le contropartite inserisco tante righe quante
            *     contropartite escludendo la contropartita utilizzata per valorizzare
            *     la riga del conto filtrato. questa � individuata in w_RIGCON
            if CalSal.RIGCON<>this.w_RIGCON
              this.w_CONTRO = NVL(CONTRO, SPACE(15))
              this.w_IMPCON = NVL(IMPCON, 0)
              this.w_APPO1 = NVL(VALNAZ, g_PERVAL)
              this.w_DESCRI = NVL(DESCRI, " ")
              this.w_DESCON = SUBSTR ( NVL(DESCON, " ") , 1, 50)
              * --- Ordino le contropartite in base al numero di riga.
              *     Divido per mille per gestire 99 contropartite
              this.w_CPROWORD = CALSAL.CPROWORD/1000
              if this.w_APPO1<>this.oParentObject.w_CODVAL
                if this.w_APPO1<>g_PERVAL
                  * --- Se valuta esercizio diversa
                  this.w_IMPCON = cp_ROUND((this.w_IMPCON / IIF(this.w_APPO1=g_CODLIR, g_CAOEUR, 1)) * g_CAOVAL, g_PERPVL)
                endif
                if this.oParentObject.w_CODVAL<>g_PERVAL
                  * --- Converte alla Valuta di Stampa
                  if this.oParentObject.w_CAMREG="S"
                    if this.oParentObject.w_CODVAL=this.w_PNCODVAL
                      this.w_IMPCON = cp_ROUND((this.w_IMPCON / g_CAOVAL) * this.w_PNCAOVAL, this.oParentObject.w_DECTOT)
                    else
                      this.w_IMPCON = cp_ROUND((this.w_IMPCON / g_CAOVAL) * this.oParentObject.w_CAOVAL,this.oParentObject.w_DECTOT)
                    endif
                  else
                    this.w_IMPCON = cp_ROUND((this.w_IMPCON / g_CAOVAL) * this.oParentObject.w_CAOVAL, this.oParentObject.w_DECTOT)
                  endif
                endif
              endif
              * --- Le contropartite seguono il conto perch� dato ordinato per TIPREC prima
              *     della stampa. per stampare le contropartite dopo il conto aggiungo w_CPROWORD al numero
              *     riga...
              if this.oParentObject.w_FLSTCONT="N" OR CalSal.CCFLSALI # "S" AND CalSal.CCFLSALF # "S"
                * --- Insert into TMPSCO
                i_nConn=i_TableProp[this.TMPSCO_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMPSCO_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPSCO_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"CODCON"+",TIPREC"+",DATREG"+",NUMRER"+",SERIAL"+",NURIGA"+",SALPRO"+",CONTRO"+",IMPCON"+",DESCRI"+",DESCON"+",ROWORD"+",SALINI"+",SALFIN"+",CONSUP"+",FLCALC"+",SALDOFIN"+",BILSEZ"+",DESPRI"+",TIPCON"+",DESSUP"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(this.w_CODCON),'TMPSCO','CODCON');
                  +","+cp_NullLink(cp_ToStrODBC("C"),'TMPSCO','TIPREC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'TMPSCO','DATREG');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRER),'TMPSCO','NUMRER');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMPSCO','SERIAL');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_MAXRIGA),'TMPSCO','NURIGA');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_SALPRO),'TMPSCO','SALPRO');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CONTRO),'TMPSCO','CONTRO');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_IMPCON),'TMPSCO','IMPCON');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'TMPSCO','DESCRI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DESCON),'TMPSCO','DESCON');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_MAXROWORD+this.w_CPROWORD),'TMPSCO','ROWORD');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_FLSALI),'TMPSCO','SALINI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_FLSALF),'TMPSCO','SALFIN');
                  +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'TMPSCO','CONSUP');
                  +","+cp_NullLink(cp_ToStrODBC(" "),'TMPSCO','FLCALC');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_SALDOFIN),'TMPSCO','SALDOFIN');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_BILSEZ),'TMPSCO','BILSEZ');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_DESPRI),'TMPSCO','DESPRI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_TIPO),'TMPSCO','TIPCON');
                  +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'TMPSCO','DESSUP');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'CODCON',this.w_CODCON,'TIPREC',"C",'DATREG',this.w_DATREG,'NUMRER',this.w_NUMRER,'SERIAL',this.w_SERIAL,'NURIGA',this.w_MAXRIGA,'SALPRO',this.w_SALPRO,'CONTRO',this.w_CONTRO,'IMPCON',this.w_IMPCON,'DESCRI',this.w_DESCRI,'DESCON',this.w_DESCON,'ROWORD',this.w_MAXROWORD+this.w_CPROWORD)
                  insert into (i_cTable) (CODCON,TIPREC,DATREG,NUMRER,SERIAL,NURIGA,SALPRO,CONTRO,IMPCON,DESCRI,DESCON,ROWORD,SALINI,SALFIN,CONSUP,FLCALC,SALDOFIN,BILSEZ,DESPRI,TIPCON,DESSUP &i_ccchkf. );
                     values (;
                       this.w_CODCON;
                       ,"C";
                       ,this.w_DATREG;
                       ,this.w_NUMRER;
                       ,this.w_SERIAL;
                       ,this.w_MAXRIGA;
                       ,this.w_SALPRO;
                       ,this.w_CONTRO;
                       ,this.w_IMPCON;
                       ,this.w_DESCRI;
                       ,this.w_DESCON;
                       ,this.w_MAXROWORD+this.w_CPROWORD;
                       ,this.w_FLSALI;
                       ,this.w_FLSALF;
                       ,SPACE(15);
                       ," ";
                       ,this.w_SALDOFIN;
                       ,this.w_BILSEZ;
                       ,this.w_DESPRI;
                       ,this.w_TIPO;
                       ,SPACE(10);
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error='ERRORE TIPREC C'
                  return
                endif
              endif
            endif
            SELECT CALSAL
            ENDSCAN 
            * --- Se ho aggiunto le contropartite azzero w_MAXRIGA per ricomnciare a cercare 
            *     il numero di riga pi� alto
            this.w_MAXRIGA = 0
            this.w_MAXROWORD = 0
          endif
        endif
        SELECT RIGDET
        ENDSCAN
        * --- Aggiungo sede
        * --- Write into TMPSCO
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPSCO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPSCO_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="TIPREC,SERIAL,NURIGA,CODCON,TIPCON"
          do vq_exec with 'gscgsqsm',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPSCO_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPSCO.TIPREC = _t2.TIPREC";
                  +" and "+"TMPSCO.SERIAL = _t2.SERIAL";
                  +" and "+"TMPSCO.NURIGA = _t2.NURIGA";
                  +" and "+"TMPSCO.CODCON = _t2.CODCON";
                  +" and "+"TMPSCO.TIPCON = _t2.TIPCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DESPRI = _t2.DESCON";
              +i_ccchkf;
              +" from "+i_cTable+" TMPSCO, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPSCO.TIPREC = _t2.TIPREC";
                  +" and "+"TMPSCO.SERIAL = _t2.SERIAL";
                  +" and "+"TMPSCO.NURIGA = _t2.NURIGA";
                  +" and "+"TMPSCO.CODCON = _t2.CODCON";
                  +" and "+"TMPSCO.TIPCON = _t2.TIPCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPSCO, "+i_cQueryTable+" _t2 set ";
              +"TMPSCO.DESPRI = _t2.DESCON";
              +Iif(Empty(i_ccchkf),"",",TMPSCO.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="TMPSCO.TIPREC = t2.TIPREC";
                  +" and "+"TMPSCO.SERIAL = t2.SERIAL";
                  +" and "+"TMPSCO.NURIGA = t2.NURIGA";
                  +" and "+"TMPSCO.CODCON = t2.CODCON";
                  +" and "+"TMPSCO.TIPCON = t2.TIPCON";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPSCO set (";
              +"DESPRI";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.DESCON";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="TMPSCO.TIPREC = _t2.TIPREC";
                  +" and "+"TMPSCO.SERIAL = _t2.SERIAL";
                  +" and "+"TMPSCO.NURIGA = _t2.NURIGA";
                  +" and "+"TMPSCO.CODCON = _t2.CODCON";
                  +" and "+"TMPSCO.TIPCON = _t2.TIPCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPSCO set ";
              +"DESPRI = _t2.DESCON";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".TIPREC = "+i_cQueryTable+".TIPREC";
                  +" and "+i_cTable+".SERIAL = "+i_cQueryTable+".SERIAL";
                  +" and "+i_cTable+".NURIGA = "+i_cQueryTable+".NURIGA";
                  +" and "+i_cTable+".CODCON = "+i_cQueryTable+".CODCON";
                  +" and "+i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DESPRI = (select DESCON from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if used("RIGDET")
          select RIGDET
          use
        endif
        if used("CALSAL")
          select CALSAL
          use
        endif
        * --- Ordino il cursore costruito per la stampa.:
        *     Codice Conto (CODCON)
        *     Data Registrazione (DATREG)
        *     Numero Registrazione (NUMRER)
        *     Seriale (SERIAL)
        *     Numero Riga ( ROWORD)
        if this.oParentObject.w_DETSCA="S"
          vq_exec("query\GSCG2SMA.VQR",this,"__TMP__")
        else
          vq_exec("query\GSCG1SMA.VQR",this,"__TMP__")
        endif
        * --- Esegue la Stampa
        L_PERCOM=this.oParentObject.w_PERCOM
        L_TIPCON=this.oParentObject.w_TIPCON
        L_FLPROV=this.oParentObject.w_FLPROV
        L_CODINI=this.oParentObject.w_CODINI
        L_CODFIN=this.oParentObject.w_CODFIN
        L_RAGSOCINI=this.oParentObject.w_RAGSOCINI
        L_RAGSOCFIN=this.oParentObject.w_RAGSOCFIN
        L_DATINI=this.oParentObject.w_DATINI
        L_DATFIN=this.oParentObject.w_DATFIN
        L_CODCAU=this.oParentObject.w_CODCAU
        L_CODESE=this.oParentObject.w_CODESE
        L_CODVAL=this.oParentObject.w_CODVAL
        L_SIMVAL=IIF(EMPTY(this.oParentObject.w_SIMVAL), this.oParentObject.w_CODVAL, this.oParentObject.w_SIMVAL)
        L_CAOVAL=this.oParentObject.w_CAOVAL
        L_DATPRE=this.w_DATPRE
        L_DECTOT=this.oParentObject.w_DECTOT
        L_FLDESC=this.oParentObject.w_FLDESC
        L_CONSU=this.oParentObject.w_CONSU
        L_CONSU1=this.oParentObject.w_CONSU1
        L_CONSU2=this.oParentObject.w_CONSU2
        L_CATCON=this.oParentObject.w_CATCON
        L_CAMREG=this.oParentObject.w_CAMREG
        L_FLAPER=this.oParentObject.w_FLAPER
        L_FLCONT=this.oParentObject.w_FLCONT
        if this.oParentObject.w_DATSAL="S"
          CP_CHPRN("QUERY\GSCG6SMA.FRX" ," ", this)
        else
          if this.oParentObject.w_FLCONT$ "S-T"
            * --- Stampa ControPartite 
            if this.oParentObject.w_FLSPAG="S"
              * --- GSCG1SMA.FRX se � abilitato Salto Pagina
              CP_CHPRN("QUERY\GSCG1SMA.FRX" ," ", this)
            else
              * --- GSCG_SMA.FRX se non � abilitato Salto Pagina
              CP_CHPRN("QUERY\GSCG_SMA.FRX" ," ", this)
            endif
          else
            * --- No Stampa ControPartite 
            * --- Stampa Dettaglio Scadenze
            if this.oParentObject.w_DETSCA="S"
              if this.oParentObject.w_FLSPAG="S"
                * --- GSCG5SMA.FRX con Salto Pagina
                CP_CHPRN("QUERY\GSCG5SMA.FRX" ," ", this)
              else
                * --- GSCG4SMA.FRX senza Salto Pagina
                CP_CHPRN("QUERY\GSCG4SMA.FRX" ," ", this)
              endif
            else
              if this.oParentObject.w_FLSPAG="S"
                * --- GSCG3SMA.FRX con Salto Pagina
                CP_CHPRN("QUERY\GSCG3SMA.FRX" ," ", this)
              else
                * --- GSCG2SMA.FRX senza Salto Pagina
                CP_CHPRN("QUERY\GSCG2SMA.FRX" ," ", this)
              endif
            endif
          endif
        endif
        if used("__TMP__")
          select __TMP__
          use
        endif
      endif
      if used("SALPRE")
        select SALPRE
        use
      endif
    endif
    if this.pEvento="Fine" AND this.pParam="V"
      VVP=20*G_PERPVL
    endif
    * --- Drop temporary table TMPSCO
    i_nIdx=cp_GetTableDefIdx('TMPSCO')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPSCO')
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ATTEZIONE: Questo Blocco e' sostanzialmente identico a quello in GSCG_BVM
    * --- Data Fine Esercizio Successivo (serve per ricercare eventuali registrazioni non comprese nell'intervallo di ricerca ma di competenza del periodo)
    this.w_PREINI = i_INIDAT
    this.w_PREFIN = i_INIDAT
    this.w_PREESE = "ZZXX"
    this.w_PRESUC = "XXZZ"
    this.w_ESEINI = CALCESER(this.oParentObject.w_DATINI," ")
    * --- Data Precedente la Selezione Iniziale
    this.w_DATPRE = this.oParentObject.w_DATINI - 1
    * --- Esercizio alla Data Iniziale - 1
    this.w_ESEPRE = CALCESER(this.w_DATPRE," ")
    * --- Esercizio alla Data di Fine Selezione (seve per filtrare nel saldo Finale movimenti di conti Economici di esercizi precedenti)
    this.w_ULTESE = this.oParentObject.w_CODESE
    if EMPTY(this.w_ULTESE)
      this.w_ULTESE = CALCESER(this.oParentObject.w_DATFIN, g_CODESE)
    endif
    * --- Select from ESERCIZI
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ESERCIZI ";
          +" where ESCODAZI="+cp_ToStrODBC(this.oParentObject.w_CODAZI)+"";
          +" order by ESINIESE";
           ,"_Curs_ESERCIZI")
    else
      select * from (i_cTable);
       where ESCODAZI=this.oParentObject.w_CODAZI;
       order by ESINIESE;
        into cursor _Curs_ESERCIZI
    endif
    if used('_Curs_ESERCIZI')
      select _Curs_ESERCIZI
      locate for 1=1
      do while not(eof())
      * --- Data Fine Esercizio Successivo (serve per ricercare eventuali registrazioni non comprese nell'intervallo di ricerca ma di competenza del periodo)
      if CP_TODATE(_Curs_ESERCIZI.ESFINESE)<=this.w_DATPRE
        this.w_PREINI = CP_TODATE(_Curs_ESERCIZI.ESINIESE)
        * --- Data di Fine dell'Ultimo Esercizio Interamente precedente la data di Inizio Selezione (Fin qui' leggo solo i Saldi)
        this.w_PREFIN = CP_TODATE(_Curs_ESERCIZI.ESFINESE)
        this.w_PREESE = NVL(_Curs_ESERCIZI.ESCODESE, "XXZZ")
      endif
        select _Curs_ESERCIZI
        continue
      enddo
      use
    endif
    * --- Per ricercare registrazioni dell'esercizio INTERAMENTE precedente la Selezione ma di competenza dell' esercizio selezionato
    * --- (devono essere considerate ai fini del Saldo iniziale)
    this.w_PRESUC = IIF(this.w_ESEINI=this.w_PREESE, "XXZZ", this.w_ESEINI)
    this.w_PRIESE = SPACE(4)
    this.w_BILSEZ = " "
    this.w_SALDO = 0
    this.w_OLDCON = "###@@@XXXZZZ"
    CREATE CURSOR SALPRE (CODCON C(15), PRIESE C(4), SEZBIL C(1), SALREG N(18,4))
    * --- Leggo Saldi fino all'Esercizio interamente la data Iniziale + le registrazioni dell'esercizio fino alla data iniziale
    ah_Msg("Lettura saldi e registrazioni del periodo precedente...",.T.)
    if this.oParentObject.w_CAMREG="S"
      vq_exec("query\GSCGCQSM.VQR",this,"CALSAL")
    else
      vq_exec("query\GSCG1QSM.VQR",this,"CALSAL")
    endif
    SELECT CALSAL
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODESE,"    ")) AND NOT EMPTY(NVL(CODCON, " ")) 
    this.w_NEWCON = CODCON
    if this.w_OLDCON<>this.w_NEWCON
      * --- Nuovo Conto
      if this.w_OLDCON<>"###@@@XXXZZZ"
        this.w_SALDO = cp_ROUND(this.w_SALDO, this.oParentObject.w_DECTOT) 
        INSERT INTO SALPRE (CODCON, PRIESE, SEZBIL, SALREG) VALUES (this.w_OLDCON, this.w_PRIESE, this.w_BILSEZ, this.w_SALDO)
        SELECT CALSAL
      endif
      this.w_OLDCON = this.w_NEWCON
      this.w_SALDO = 0
      * --- Primo esercizio utilizzato dal Conto
      this.w_PRIESE = NVL(CODESE,SPACE(4))
      * --- Se Conto Economico non considera i Saldi Iniziali (ma legge comunque il primo esercizio Movimentato)
      this.w_BILSEZ = " "
      this.w_CONSUP = NVL(CONSUP, SPACE(15))
      if NOT EMPTY(this.w_CONSUP) 
        this.w_BILSEZ = CALCSEZ(this.w_CONSUP)
      endif
    endif
    this.w_VALINI = NVL(VALNAZ, g_PERVAL)
    this.w_CAMBIO = GETCAM(this.w_VALINI, I_DATSYS)
    if NVL(TIPREC, " ")="A"
      * --- Legge Saldi dei periodi (interi) precedenti la data iniziale
      if CP_TODATE(FINESE)<=this.w_DATPRE AND NOT this.w_BILSEZ $ "CR"
        * --- I saldi Iniziali vanno calcolati solo se non e' Conto Economico
        * --- Legge solo i saldi del Mov. in linea tranne il primo al quale considera anche l'eventuale apertura
        this.w_APPO2 = NVL(TOTIMP,0) - IIF(CODESE=this.w_PRIESE, 0, NVL(SALINI, 0))
        if this.w_VALINI <> g_PERVAL
          this.w_APPO2 = VAL2VAL(this.w_APPO2, this.w_CAMBIO, I_DATSYS,g_FINESE, this.oParentObject.w_CAOVAL, 6)
        endif
        this.w_SALDO = this.w_SALDO + this.w_APPO2
      endif
    else
      * --- Leggo i Movimenti dell'eventuale esercizio precedente la data di inizio selezione
      * --- Esclude i Movimenti di Saldo Iniziale tranne il Primo
      if EMPTY(NVL(FLSALI," ")) OR this.w_ESEPRE=this.w_PRIESE
        if this.w_VALINI = g_PERVAL
          this.w_SALDO = this.w_SALDO + NVL(TOTIMP,0)
        else
          this.w_SALDO = this.w_SALDO + VAL2VAL(NVL(TOTIMP,0), this.w_CAMBIO, I_DATSYS,g_FINESE, this.oParentObject.w_CAOVAL, 6)
        endif
      endif
    endif
    ENDSCAN
    if this.w_OLDCON<>"###@@@XXXZZZ"
      * --- Ultimo Codice
      this.w_SALDO = cp_ROUND(this.w_SALDO, this.oParentObject.w_DECTOT) 
      INSERT INTO SALPRE (CODCON, PRIESE, SEZBIL, SALREG) VALUES (this.w_OLDCON, this.w_PRIESE, this.w_BILSEZ, this.w_SALDO)
    endif
    if used("CALSAL")
      select CALSAL
      use
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricerca Contropartite
    ah_Msg("Lettura contropartite...",.T.)
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSCG0QSM',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    SELECT RIGDET
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODCON, " ")) AND NOT EMPTY(NVL(SERIAL, " "))
    this.w_CODCON = CODCON
    this.w_SERIAL = SERIAL
    this.w_NURIGA = NVL(NURIGA, 0)
    this.w_IMPDAR = NVL(IMPDAR, 0)
    this.w_IMPAVE = NVL(IMPAVE, 0)
    this.w_APPO = IIF(this.w_IMPDAR<>0, "D", IIF(this.w_IMPAVE<>0, "A", "Z"))
    this.w_APPO1 = NVL(VALNAZ, g_PERVAL)
    this.w_CAURIG = RigDet.Caurig
    this.w_FLZERO = Nvl(RigDet.Flzero," ")
    * --- Insert into TMPVEND1
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPVEND1_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SERIAL"+",TIPCON"+",CODCON"+",NURIGA"+",FLDAVE"+",VALNAZ"+",CAURIG"+",FLZERO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMPVEND1','SERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPCON),'TMPVEND1','TIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'TMPVEND1','CODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NURIGA),'TMPVEND1','NURIGA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APPO),'TMPVEND1','FLDAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APPO1),'TMPVEND1','VALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAURIG),'TMPVEND1','CAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLZERO),'TMPVEND1','FLZERO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SERIAL',this.w_SERIAL,'TIPCON',this.oParentObject.w_TIPCON,'CODCON',this.w_CODCON,'NURIGA',this.w_NURIGA,'FLDAVE',this.w_APPO,'VALNAZ',this.w_APPO1,'CAURIG',this.w_CAURIG,'FLZERO',this.w_FLZERO)
      insert into (i_cTable) (SERIAL,TIPCON,CODCON,NURIGA,FLDAVE,VALNAZ,CAURIG,FLZERO &i_ccchkf. );
         values (;
           this.w_SERIAL;
           ,this.oParentObject.w_TIPCON;
           ,this.w_CODCON;
           ,this.w_NURIGA;
           ,this.w_APPO;
           ,this.w_APPO1;
           ,this.w_CAURIG;
           ,this.w_FLZERO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    SELECT RIGDET
    ENDSCAN 
    vq_exec("query\GSCG3QSM.VQR",this,"CALSAL")
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se N non concorre per i calcolo del saldo progressivo
    if this.oParentObject.w_FLAPER = "S"
      * --- Se attivo:  "Flag Considera solo movimenti del periodo" , tutti i movimenti concorrono al saldo finale
      this.w_FLCALC = " "
    else
      this.w_FLCALC = "N"
      if NVL(SALINI," ")=" " AND NVL(SALFIN," ")=" " AND CP_TODATE(ESFINESE)>this.w_DATPRE AND (this.oParentObject.w_DATFIN=this.oParentObject.w_FINESE OR NOT (this.w_DATREG>this.oParentObject.w_DATFIN))
        this.w_FLCALC = " "
      endif
      if NOT EMPTY(CP_TODATE(INICOM)) AND this.oParentObject.w_PERCOM="S" AND NOT EMPTY(CP_TODATE(FINCOM)) 
        * --- Se stampa per competenza e date competenza valorizzate deve concorrere 
        *     al saldo finale
        this.w_FLCALC = " "
      endif
      this.w_FLCALC = IIF(COMPET=this.w_PRIESE AND NOT EMPTY(NVL(SALINI," ")), " ", this.w_FLCALC)
    endif
  endproc


  proc Init(oParentObject,pEvento,pParam)
    this.pEvento=pEvento
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='SALDICON'
    this.cWorkTables[3]='*TMPSCO'
    this.cWorkTables[4]='*TMPVEND1'
    this.cWorkTables[5]='CONTROPA'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEvento,pParam"
endproc
