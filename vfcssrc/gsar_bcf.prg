* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bcf                                                        *
*              Calcola totali plafond                                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-20                                                      *
* Last revis.: 2006-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bcf",oParentObject)
return(i_retval)

define class tgsar_bcf as StdBatch
  * --- Local variables
  w_RECO = 0
  w_ANNFIN = space(4)
  w_MESFIN = space(2)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola totali Plafond Mobile (da GSAR_MDI)
    if this.oParentObject.w_PLAMOB<>"S"
      * --- Calcola solo Plafond Mobile
      i_retcode = 'stop'
      return
    endif
    * --- Cicla sui primi 12 mesi
    SELECT (this.oParentObject.cTrsName)
    this.w_RECO = RECNO()
    * --- Carica cursore di appoggio per calcoli (il tmp potrebbe non essere ordinato correttamente) 
    SELECT t_DI__ANNO AS ANNO, t_DIPERIOD AS MESE, t_DITOTESP AS ESPU, t_DIPLAUTI AS UTIU ;
    FROM (this.oParentObject.cTrsName) WHERE NOT EMPTY(t_DI__ANNO) AND t_DIPERIOD<>0 AND NOT DELETED() ;
    INTO CURSOR APPO ORDER BY t_DI__ANNO DESC, t_DIPERIOD DESC
    SELECT (this.oParentObject.cTrsName)
    if this.w_RECO>0 AND this.w_RECO<=RECCOUNT()
      GOTO this.w_RECO
    else
      GO TOP
    endif
    this.oParentObject.w_ESPU12 = 0
    this.oParentObject.w_UTIU12 = 0
    SELECT APPO
    GO TOP
    this.w_ANNFIN = VAL(ANNO)
    this.w_MESFIN = MESE
    SCAN FOR (VAL(ANNO)=this.w_ANNFIN OR (VAL(ANNO)=this.w_ANNFIN-1 AND MESE>this.w_MESFIN))
    this.oParentObject.w_ESPU12 = this.oParentObject.w_ESPU12 + ESPU
    this.oParentObject.w_UTIU12 = this.oParentObject.w_UTIU12 + UTIU
    ENDSCAN
    * --- Chiude cursore
    if USED("APPO")
      SELECT APPO
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
