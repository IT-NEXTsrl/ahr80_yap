* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bdd                                                        *
*              DUPLICA TOTALIZZATORE                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39][VRS_28]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-24                                                      *
* Last revis.: 2009-07-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bdd",oParentObject)
return(i_retval)

define class tgscg_bdd as StdBatch
  * --- Local variables
  * --- WorkFile variables
  TOT_MAST_idx=0
  TOT_DETT_idx=0
  DET_DIME_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue duplicazione Totlalizzatore eseguito dat GSCG_KDD
    if Not Empty(this.oParentObject.w_CODNEW)
      * --- Try
      local bErr_038C55E0
      bErr_038C55E0=bTrsErr
      this.Try_038C55E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_038C55E0
      * --- End
    else
      Ah_ErrorMsg("Attenzione: codice nuovo totalizzatore non specificato")
    endif
  endproc
  proc Try_038C55E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from TOT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TOT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TOT_MAST_idx,2],.t.,this.TOT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" TOT_MAST where ";
            +"TICODICE = "+cp_ToStrODBC(this.oParentObject.w_CODNEW);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            TICODICE = this.oParentObject.w_CODNEW;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_ROWS=0
      * --- Insert into TOT_MAST
      i_nConn=i_TableProp[this.TOT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TOT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TOT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"TICODICE"+",TI_FONTE"+",TIDESCRI"+",TITIPTOT"+",TIFLDECI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODNEW),'TOT_MAST','TICODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TI_FONTE),'TOT_MAST','TI_FONTE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWDES),'TOT_MAST','TIDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TITIPTOT),'TOT_MAST','TITIPTOT');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIFLDECI),'TOT_MAST','TIFLDECI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'TICODICE',this.oParentObject.w_CODNEW,'TI_FONTE',this.oParentObject.w_TI_FONTE,'TIDESCRI',this.oParentObject.w_NEWDES,'TITIPTOT',this.oParentObject.w_TITIPTOT,'TIFLDECI',this.oParentObject.w_TIFLDECI)
        insert into (i_cTable) (TICODICE,TI_FONTE,TIDESCRI,TITIPTOT,TIFLDECI &i_ccchkf. );
           values (;
             this.oParentObject.w_CODNEW;
             ,this.oParentObject.w_TI_FONTE;
             ,this.oParentObject.w_NEWDES;
             ,this.oParentObject.w_TITIPTOT;
             ,this.oParentObject.w_TIFLDECI;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Select from TOT_DETT
      i_nConn=i_TableProp[this.TOT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TOT_DETT_idx,2],.t.,this.TOT_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" TOT_DETT ";
            +" where TICODICE="+cp_ToStrODBC(this.oParentObject.w_TICODICE)+"";
             ,"_Curs_TOT_DETT")
      else
        select * from (i_cTable);
         where TICODICE=this.oParentObject.w_TICODICE;
          into cursor _Curs_TOT_DETT
      endif
      if used('_Curs_TOT_DETT')
        select _Curs_TOT_DETT
        locate for 1=1
        do while not(eof())
        * --- Insert into TOT_DETT
        i_nConn=i_TableProp[this.TOT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TOT_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TOT_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"TICODICE"+",CPROWNUM"+",TIFORMUL"+",TICODMIS"+",TIMFONTE"+",TIDESMIS"+",TICODFAZ"+",TICODCOM"+",CPROWORD"+",TITIPMIS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODNEW),'TOT_DETT','TICODICE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TOT_DETT.CPROWNUM),'TOT_DETT','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TOT_DETT.TIFORMUL),'TOT_DETT','TIFORMUL');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TOT_DETT.TICODMIS),'TOT_DETT','TICODMIS');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TOT_DETT.TIMFONTE),'TOT_DETT','TIMFONTE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TOT_DETT.TIDESMIS),'TOT_DETT','TIDESMIS');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TOT_DETT.TICODFAZ),'TOT_DETT','TICODFAZ');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TOT_DETT.TICODCOM),'TOT_DETT','TICODCOM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TOT_DETT.CPROWORD),'TOT_DETT','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_TOT_DETT.TITIPMIS),'TOT_DETT','TITIPMIS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'TICODICE',this.oParentObject.w_CODNEW,'CPROWNUM',_Curs_TOT_DETT.CPROWNUM,'TIFORMUL',_Curs_TOT_DETT.TIFORMUL,'TICODMIS',_Curs_TOT_DETT.TICODMIS,'TIMFONTE',_Curs_TOT_DETT.TIMFONTE,'TIDESMIS',_Curs_TOT_DETT.TIDESMIS,'TICODFAZ',_Curs_TOT_DETT.TICODFAZ,'TICODCOM',_Curs_TOT_DETT.TICODCOM,'CPROWORD',_Curs_TOT_DETT.CPROWORD,'TITIPMIS',_Curs_TOT_DETT.TITIPMIS)
          insert into (i_cTable) (TICODICE,CPROWNUM,TIFORMUL,TICODMIS,TIMFONTE,TIDESMIS,TICODFAZ,TICODCOM,CPROWORD,TITIPMIS &i_ccchkf. );
             values (;
               this.oParentObject.w_CODNEW;
               ,_Curs_TOT_DETT.CPROWNUM;
               ,_Curs_TOT_DETT.TIFORMUL;
               ,_Curs_TOT_DETT.TICODMIS;
               ,_Curs_TOT_DETT.TIMFONTE;
               ,_Curs_TOT_DETT.TIDESMIS;
               ,_Curs_TOT_DETT.TICODFAZ;
               ,_Curs_TOT_DETT.TICODCOM;
               ,_Curs_TOT_DETT.CPROWORD;
               ,_Curs_TOT_DETT.TITIPMIS;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
          select _Curs_TOT_DETT
          continue
        enddo
        use
      endif
      * --- Select from DET_DIME
      i_nConn=i_TableProp[this.DET_DIME_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DET_DIME_idx,2],.t.,this.DET_DIME_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" DET_DIME ";
            +" where DICODTOT="+cp_ToStrODBC(this.oParentObject.w_TICODICE)+"";
             ,"_Curs_DET_DIME")
      else
        select * from (i_cTable);
         where DICODTOT=this.oParentObject.w_TICODICE;
          into cursor _Curs_DET_DIME
      endif
      if used('_Curs_DET_DIME')
        select _Curs_DET_DIME
        locate for 1=1
        do while not(eof())
        * --- Insert into DET_DIME
        i_nConn=i_TableProp[this.DET_DIME_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DET_DIME_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DET_DIME_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DICODTOT"+",DI_FONTE"+",DICODICE"+",CPROWNUM"+",CPROWORD"+",DIDESCRI"+",DIVALCHR"+",DIVALNUM"+",DIVALDAT"+",DITIPDAT"+",DITIPDIM"+",DIARLINK"+",DIVLCOMC"+",DIVLCOMN"+",DIVALLIN"+",DIVALCAU"+",DIVALATT"+",DIVALCON"+",DIFLZERO"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODNEW),'DET_DIME','DICODTOT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DI_FONTE),'DET_DIME','DI_FONTE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DICODICE),'DET_DIME','DICODICE');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.CPROWNUM),'DET_DIME','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.CPROWORD),'DET_DIME','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DIDESCRI),'DET_DIME','DIDESCRI');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DIVALCHR),'DET_DIME','DIVALCHR');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DIVALNUM),'DET_DIME','DIVALNUM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DIVALDAT),'DET_DIME','DIVALDAT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DITIPDAT),'DET_DIME','DITIPDAT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DITIPDIM),'DET_DIME','DITIPDIM');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DIARLINK),'DET_DIME','DIARLINK');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DIVLCOMC),'DET_DIME','DIVLCOMC');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DIVLCOMN),'DET_DIME','DIVLCOMN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DIVALLIN),'DET_DIME','DIVALLIN');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DIVALCAU),'DET_DIME','DIVALCAU');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DIVALATT),'DET_DIME','DIVALATT');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DIVALCON),'DET_DIME','DIVALCON');
          +","+cp_NullLink(cp_ToStrODBC(_Curs_DET_DIME.DIFLZERO),'DET_DIME','DIFLZERO');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DICODTOT',this.oParentObject.w_CODNEW,'DI_FONTE',_Curs_DET_DIME.DI_FONTE,'DICODICE',_Curs_DET_DIME.DICODICE,'CPROWNUM',_Curs_DET_DIME.CPROWNUM,'CPROWORD',_Curs_DET_DIME.CPROWORD,'DIDESCRI',_Curs_DET_DIME.DIDESCRI,'DIVALCHR',_Curs_DET_DIME.DIVALCHR,'DIVALNUM',_Curs_DET_DIME.DIVALNUM,'DIVALDAT',_Curs_DET_DIME.DIVALDAT,'DITIPDAT',_Curs_DET_DIME.DITIPDAT,'DITIPDIM',_Curs_DET_DIME.DITIPDIM,'DIARLINK',_Curs_DET_DIME.DIARLINK)
          insert into (i_cTable) (DICODTOT,DI_FONTE,DICODICE,CPROWNUM,CPROWORD,DIDESCRI,DIVALCHR,DIVALNUM,DIVALDAT,DITIPDAT,DITIPDIM,DIARLINK,DIVLCOMC,DIVLCOMN,DIVALLIN,DIVALCAU,DIVALATT,DIVALCON,DIFLZERO &i_ccchkf. );
             values (;
               this.oParentObject.w_CODNEW;
               ,_Curs_DET_DIME.DI_FONTE;
               ,_Curs_DET_DIME.DICODICE;
               ,_Curs_DET_DIME.CPROWNUM;
               ,_Curs_DET_DIME.CPROWORD;
               ,_Curs_DET_DIME.DIDESCRI;
               ,_Curs_DET_DIME.DIVALCHR;
               ,_Curs_DET_DIME.DIVALNUM;
               ,_Curs_DET_DIME.DIVALDAT;
               ,_Curs_DET_DIME.DITIPDAT;
               ,_Curs_DET_DIME.DITIPDIM;
               ,_Curs_DET_DIME.DIARLINK;
               ,_Curs_DET_DIME.DIVLCOMC;
               ,_Curs_DET_DIME.DIVLCOMN;
               ,_Curs_DET_DIME.DIVALLIN;
               ,_Curs_DET_DIME.DIVALCAU;
               ,_Curs_DET_DIME.DIVALATT;
               ,_Curs_DET_DIME.DIVALCON;
               ,_Curs_DET_DIME.DIFLZERO;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
          select _Curs_DET_DIME
          continue
        enddo
        use
      endif
      Ah_Msg("Totalizzatore duplicato correttamente")
    else
      Ah_ErrorMsg("Attenzione: codice totalizzatore gi� presente")
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='TOT_MAST'
    this.cWorkTables[2]='TOT_DETT'
    this.cWorkTables[3]='DET_DIME'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_TOT_DETT')
      use in _Curs_TOT_DETT
    endif
    if used('_Curs_DET_DIME')
      use in _Curs_DET_DIME
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
