* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bki                                                        *
*              Controlli castelletto IVA                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-05-12                                                      *
* Last revis.: 2012-01-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPNSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bki",oParentObject,m.pPNSERIAL)
return(i_retval)

define class tgscg_bki as StdBatch
  * --- Local variables
  pPNSERIAL = space(10)
  w_oMess = .NULL.
  w_IMPIVA = 0
  w_IMPONI = 0
  w_TMP_IMPO = 0
  w_DECTOT = 0
  w_DECTOP = 0
  w_PNTIPDOC = space(2)
  w_PNVALNAZ = space(5)
  w_PNCODVAL = space(5)
  w_PNTIPREG = space(1)
  w_ESI_DET_IVA = .f.
  w_PNCODCLF = space(15)
  w_TOTIVA = 0
  w_CALDOC = space(1)
  w_oPart = .NULL.
  w_OK = .f.
  w_IVA = 0
  w_IMP = 0
  w_APPO = 0
  w_IVADET = 0
  w_IMPCLI = 0
  w_IMPIVA = 0
  w_OKREG = .f.
  w_DIFIVA = 0
  w_RESOCONT = space(0)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seriale primanota da verificare
    this.w_oMess=createobject("Ah_Message")
    * --- Oggetto log
    this.w_OK = .t.
    * --- Select from gscg_bki
    do vq_exec with 'gscg_bki',this,'_Curs_gscg_bki','',.f.,.t.
    if used('_Curs_gscg_bki')
      select _Curs_gscg_bki
      locate for 1=1
      do while not(eof())
      this.w_PNTIPDOC = _Curs_gscg_bki.PNTIPDOC
      this.w_PNVALNAZ = _Curs_gscg_bki.PNVALNAZ
      this.w_PNCODVAL = _Curs_gscg_bki.PNCODVAL
      this.w_PNTIPREG = _Curs_gscg_bki.PNTIPREG
      this.w_DECTOT = _Curs_gscg_bki.DECTOT
      this.w_DECTOP = _Curs_gscg_bki.DECTOP
      this.w_CALDOC = _Curs_gscg_bki.CALDOC
      this.w_PNCODCLF = Nvl(_Curs_gscg_bki.PNCODCLF," ")
      if (_Curs_gscg_bki.IVIMPONI<>0 OR _Curs_gscg_bki.IVIMPIVA<>0)
        this.w_ESI_DET_IVA = .T.
        if _Curs_gscg_bki.IVTIPREG="C"
          this.w_TMP_IMPO = (nvl(_Curs_gscg_bki.IVIMPONI,0)+nvl(_Curs_gscg_bki.IVIMPIVA,0))/(1+(_Curs_gscg_bki.PERIVA/100))
          if this.w_PNCODVAL=g_CODLIR OR this.w_PNTIPDOC $ "FE-NE" OR (this.w_PNTIPDOC $ "AU-NU" AND Nvl(_Curs_gscg_bki.REVCHA," "))
            * --- Se Lire Arrotonda l' IVA all'importo Superiore
            this.w_TMP_IMPO = IVAROUND(this.w_TMP_IMPO, this.w_DECTOT, IIF(this.w_TMP_IMPO<0, 0, 1),this.w_PNVALNAZ)
          else
            this.w_TMP_IMPO = cp_ROUND(this.w_TMP_IMPO, this.w_DECTOT)
          endif
          * --- Determina l'Iva per differenza
          this.w_IMPIVA = (nvl(_Curs_gscg_bki.IVIMPONI,0)+nvl(_Curs_gscg_bki.IVIMPIVA,0)) - this.w_TMP_IMPO
          this.w_IMPONI = this.w_TMP_IMPO
        else
          this.w_TMP_IMPO = (_Curs_gscg_bki.IVIMPONI*_Curs_gscg_bki.PERIVA)/100
          if this.w_PNCODVAL=g_CODLIR OR this.w_PNTIPDOC $ "FE-NE" OR (this.w_PNTIPDOC $ "AU-NU" AND Nvl(_Curs_gscg_bki.REVCHA," ")="S")
            * --- Se Lire Arrotonda l' IVA all'importo Superiore
            this.w_IMPIVA = IVAROUND(this.w_TMP_IMPO, this.w_DECTOT, IIF(this.w_TMP_IMPO<0, 0, 1),this.w_PNVALNAZ)
          else
            this.w_IMPIVA = cp_ROUND(this.w_TMP_IMPO,this.w_DECTOT)
          endif
        endif
        do case
          case Nvl(_Curs_gscg_bki.IVPERIND,0)<>0 and _Curs_gscg_bki.IVTIPREG="V"
            this.w_oPart = this.w_oMess.AddMsgPartNL("Riga castelletto: %1 codice IVA [%2] di tipo indetraibile")
            this.w_oPart.AddParam(Alltrim(STR(_Curs_gscg_bki.CPROWNUM,6,0)))     
            this.w_oPart.AddParam(_Curs_scg_bki.IVCODIVA)     
            this.w_OK = .F.
            Exit
          case Nvl(_Curs_gscg_bki.IVIMPONI,0)=0 and Nvl(_Curs_gscg_bki.IVIMPIVA,0)<>0
            this.w_oPart = this.w_oMess.AddMsgPartNL("Riga castelletto: %1 imponibile a zero con imposta diversa da zero")
            this.w_oPart.AddParam(Alltrim(STR(_Curs_gscg_bki.CPROWNUM,6,0)))     
            this.w_OK = .F.
            Exit
        endcase
        if Nvl(_Curs_gscg_bki.IVIMPIVA,0)<>this.w_IMPIVA AND this.w_OK AND Nvl(_Curs_gscg_bki.IVFLOMAG,0) <>"S" and this.w_PNVALNAZ=this.w_PNCODVAL
          if _Curs_gscg_bki.IVTIPREG="C"
            this.w_oPart = this.w_oMess.AddMsgPartNL("Riga castelletto: %1 imposta incoerente con imponibile scorporato legato al codice IVA utilizzato")
            this.w_oPart.AddParam(Alltrim(STR(_Curs_gscg_bki.CPROWNUM,6,0)))     
          else
            this.w_oPart = this.w_oMess.AddMsgPartNL("Riga castelletto: %1 imposta [%2] incoerente con importo [%3] legato al codice IVA utilizzato")
            this.w_oPart.AddParam(Alltrim(STR(_Curs_gscg_bki.CPROWNUM,6,0)))     
            this.w_oPart.AddParam(Alltrim(str(_Curs_gscg_bki.IVIMPIVA,20,4)))     
            this.w_oPart.AddParam(Alltrim(str(this.w_IMPIVA,20,4)))     
          endif
        endif
        this.w_IVA = cp_ROUND(NVL(_Curs_gscg_bki.IVIMPIVA, 0), this.w_DECTOP)
        this.w_IMP = cp_ROUND(NVL(_Curs_gscg_bki.IVIMPONI, 0), this.w_DECTOP)
        if _Curs_gscg_bki.IVTIPREG=this.w_PNTIPREG
          this.w_TOTIVA = this.w_TOTIVA + IIF(this.w_CALDOC $ "IZ" OR _Curs_gscg_bki.IVCFLOMA $ "IE", 0, this.w_IMP)
          * --- Se Codice Iva di Tipo Reverse Charge non considero l'iva nel controllo
          this.w_TOTIVA = this.w_TOTIVA + IIF(this.w_CALDOC $ "MZ" OR _Curs_gscg_bki.IVCFLOMA="E" or (this.w_PNTIPDOC $ "AU-NU" and Nvl(_Curs_gscg_bki.REVCHA," ")="S"), 0, this.w_IVA)
        endif
        * --- Eseguo controllo IVA indetraibile
        * --- Se no Riga Omaggio
        this.w_APPO = cp_ROUND((this.w_IVA*_Curs_gscg_bki.IVPERIND)/100, this.w_DECTOP)
        if _Curs_gscg_bki.IVTIPREG=this.w_PNTIPREG
          * --- Solo righe coerenti tipo regostro iva della causale
          this.w_IVADET = this.w_IVADET + ((this.w_IVA - this.w_APPO) * IIF(_Curs_gscg_bki.IVTIPREG="A", 1, -1))
        endif
      endif
      if _Curs_gscg_bki.IVTIPREG=this.w_PNTIPREG
        this.w_OKREG = .T.
      endif
      if _Curs_GSCG_BKI.TROVCF="N" and this.w_OK and NOT EMPTY(this.w_PNCODCLF) and g_AIFT="S" and _Curs_GSCG_BKI.CCTIPDOC<>"CO" and _Curs_GSCG_BKI.CCFLCOSE<>"S"
        this.w_oMess.AddMsgPartNL("Cliente/fornitore diverso dalla sezione IVA")     
        this.w_OK = .F.
      endif
      this.w_IMPCLI = Nvl(_Curs_gscg_bki.IMPCLI,0)
      this.w_IMPIVA = Nvl(_Curs_gscg_bki.IMPIVA,0)
        select _Curs_gscg_bki
        continue
      enddo
      use
    endif
    if this.w_OK and Not this.w_OKREG and Not(this.w_PNTIPREG $ "N-E") AND Not (this.w_PNTIPREG="C" AND Not (this.w_ESI_DET_IVA))
      this.w_oMess.AddMsgPartNL("Attenzione: deve esistere almeno una riga con il tipo registro IVA coerente alla causale contabile")     
      this.w_OK = .F.
    endif
    this.w_IVADET = ABS(this.w_IVADET)
    * --- Differenza IVA
    this.w_DIFIVA = ABS(Nvl(this.w_IMPCLI,0)) - ABS(this.w_TOTIVA)
    do case
      case this.w_OK AND this.w_DIFIVA<>0 and NOT EMPTY(this.w_PNCODCLF) and not this.w_PNTIPREG $ "CE"
        * --- Quadratura IVA
        this.w_oPart = this.w_oMess.AddMsgPartNL("Importo su cliente/fornitore incongruente con sezione IVA. (Differenza IVA = %1)")
        this.w_oPart.AddParam(ALLTRIM(TRAN(this.w_DIFIVA,v_PV[38+VVP])))     
        this.w_OK = .F.
      case this.w_OK and this.w_IVADET<> Abs(this.w_IMPIVA)
        * --- Importo IVA Detraibile non congruente con il Totale righe Conti IVA
        this.w_oPart = this.w_oMess.AddMsgPartNL("Importi righe IVA incongruenti con il tot. IVA deduc. (Tot. IVA  = %1)  (Tot. conti IVA= %2)")
        this.w_oPart.AddParam(ALLTRIM(TRAN(this.w_IVADET,v_PV[38+VVP])))     
        this.w_oPart.AddParam(ALLTRIM(TRAN(this.w_IMPIVA,v_PV[38+VVP])))     
        this.w_OK = .F.
    endcase
    this.w_RESOCONT = this.w_oMess.ComposeMessage()
    i_retcode = 'stop'
    i_retval = this.w_RESOCONT
    return
  endproc


  proc Init(oParentObject,pPNSERIAL)
    this.pPNSERIAL=pPNSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_gscg_bki')
      use in _Curs_gscg_bki
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPNSERIAL"
endproc
