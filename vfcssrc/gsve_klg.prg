* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_klg                                                        *
*              Log errori                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_35]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-01                                                      *
* Last revis.: 2016-03-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_klg",oParentObject))

* --- Class definition
define class tgsve_klg as StdForm
  Top    = 14
  Left   = 160

  * --- Standard Properties
  Width  = 531
  Height = 497
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-07"
  HelpContextID=216674921
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsve_klg"
  cComment = "Log errori"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DAIM = space(10)
  w_EDIT = .F.
  w_RESOCON1 = space(0)
  w_MESBLOK = space(0)
  w_ANNULLA = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_klgPag1","gsve_klg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRESOCON1_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsve_klg
    if EMPTY( NVL( iif(VarType(oparentobject.w_MESBLOK)='C',oparentobject.w_MESBLOK,oparentobject.w_MEMO), "" ) )
      this.parent.cComment= AH_MSGFORMAT("Avvisi")
    ENDIF
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DAIM=space(10)
      .w_EDIT=.f.
      .w_RESOCON1=space(0)
      .w_MESBLOK=space(0)
      .w_ANNULLA=.f.
      .w_ANNULLA=oParentObject.w_ANNULLA
        .w_DAIM = IIF(VarType(this.oParentObject .w_DAIM)='L',this.oParentObject .w_DAIM,.f.)
        .w_EDIT = Upper( this.oParentObject.Class )='TGSVE_BI2' Or Upper( this.oParentObject.Class )='TGSAR_BDA' Or Upper( this.oParentObject.Class )='TGSAR_BCO' Or Upper( this.oParentObject.Class )='TGSMA_BL4' OR Not .w_DAIM
      .oPgFrm.Page1.oPag.oObj_1_3.Calculate(ICASE(Upper(this.oParentObject.Class)='TGSMA_BL3',Ah_MsgFormat('Annulla inserimento'),Upper(this.oParentObject.Class)='TGSAR_BCO', Ah_MsgFormat('Annulla Calcolo Confezioni'),Upper(this.oParentObject.Class)='TGSAG_BCO', Ah_MsgFormat('Annulla salvataggio attivit�'),NOt this.oParentObject.Class='Tgsar_bda' , Ah_MsgFormat('Premere per annullare'),Ah_MsgFormat('Annulla salvataggio documento')))
        .w_RESOCON1 = iif(VarType(This.oparentobject .w_RESOCON1)='C',This.oparentobject .w_RESOCON1,This.oparentobject .w_NOBLOC)
        .w_MESBLOK = iif(VarType(This.oparentobject .w_MESBLOK)='C',This.oparentobject .w_MESBLOK,This.oparentobject .w_MEMO)
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_ANNULLA = .T.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_ANNULLA=.w_ANNULLA
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate(ICASE(Upper(this.oParentObject.Class)='TGSMA_BL3',Ah_MsgFormat('Annulla inserimento'),Upper(this.oParentObject.Class)='TGSAR_BCO', Ah_MsgFormat('Annulla Calcolo Confezioni'),Upper(this.oParentObject.Class)='TGSAG_BCO', Ah_MsgFormat('Annulla salvataggio attivit�'),NOt this.oParentObject.Class='Tgsar_bda' , Ah_MsgFormat('Premere per annullare'),Ah_MsgFormat('Annulla salvataggio documento')))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_3.Calculate(ICASE(Upper(this.oParentObject.Class)='TGSMA_BL3',Ah_MsgFormat('Annulla inserimento'),Upper(this.oParentObject.Class)='TGSAR_BCO', Ah_MsgFormat('Annulla Calcolo Confezioni'),Upper(this.oParentObject.Class)='TGSAG_BCO', Ah_MsgFormat('Annulla salvataggio attivit�'),NOt this.oParentObject.Class='Tgsar_bda' , Ah_MsgFormat('Premere per annullare'),Ah_MsgFormat('Annulla salvataggio documento')))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
  return

  proc Calculate_TKGKPESWZF()
    with this
          * --- Cambia caption form
          .cComment = ICASE(Upper(this.oParentObject.Class)='TGSAR_BEO' OR Upper(this.oParentObject.Class)='TGSIM_BSO',Ah_MsgFormat('Log elaborazione'),Upper(this.oParentObject.Class)='TGSAG_BC1',Ah_MsgFormat('Log modifiche'),.caption)
          .Caption = .cComment
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oRESOCON1_1_4.visible=!this.oPgFrm.Page1.oPag.oRESOCON1_1_4.mHide()
    this.oPgFrm.Page1.oPag.oMESBLOK_1_5.visible=!this.oPgFrm.Page1.oPag.oMESBLOK_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_3.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_TKGKPESWZF()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRESOCON1_1_4.value==this.w_RESOCON1)
      this.oPgFrm.Page1.oPag.oRESOCON1_1_4.value=this.w_RESOCON1
    endif
    if not(this.oPgFrm.Page1.oPag.oMESBLOK_1_5.value==this.w_MESBLOK)
      this.oPgFrm.Page1.oPag.oMESBLOK_1_5.value=this.w_MESBLOK
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsve_klg
      * --- Posso confermare la maschera solo se il bottone Ok � attivo
      If Not Empty( This.w_MESBLOK ) and  Not This.w_DAIM
       i_Bres=.f.
       i_bnoChk=.F.
       i_cErrorMsg=Ah_MsgFormat("Impossibile confermare")
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsve_klgPag1 as StdContainer
  Width  = 527
  height = 497
  stdWidth  = 527
  stdheight = 497
  resizeXpos=264
  resizeYpos=211
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_3 as cp_setobjprop with uid="PRPMODLBOW",left=119, top=500, width=148,height=25,;
    caption='Tooltip Annulla',;
   bGlobalFont=.t.,;
    cObj="\<Esci",cProp="ToolTipText",;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Modifica tooltip bottone";
    , HelpContextID = 150633370

  add object oRESOCON1_1_4 as StdMemo with uid="OISUCNFSEU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RESOCON1", cQueryName = "RESOCON1",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 110774343,;
   bGlobalFont=.t.,;
    Height=201, Width=518, Left=4, Top=242, readonly=.t.

  func oRESOCON1_1_4.mHide()
    with this.Parent.oContained
      return (Empty(.w_RESOCON1))
    endwith
  endfunc

  add object oMESBLOK_1_5 as StdMemo with uid="UYBSLYGQOF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MESBLOK", cQueryName = "MESBLOK",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 149076026,;
   bGlobalFont=.t.,;
    Height=201, Width=518, Left=4, Top=20, readonly=.t.

  func oMESBLOK_1_5.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_MESBLOK))
    endwith
  endfunc


  add object oBtn_1_8 as StdButton with uid="YVVNOWRCVM",left=414, top=445, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 216646170;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Empty( .w_MESBLOK ) or .w_DAIM)
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="PPMMLGISEW",left=8, top=445, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 193550118;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        do GSVE_BRE with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_10 as cp_runprogram with uid="UHZHAMSWBY",left=-1, top=500, width=116,height=25,;
    caption='GSVE_BLE',;
   bGlobalFont=.t.,;
    prg="GSVE_BLE",;
    cEvent = "Init",;
    nPag=1;
    , ToolTipText = "Riposizionamento e ridimensionamento della maschera e suoi oggetti";
    , HelpContextID = 78608981


  add object oBtn_1_13 as StdButton with uid="QNQUONUWQV",left=465, top=445, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 209357498;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_EDIT)
      endwith
    endif
  endfunc

  add object oStr_1_6 as StdString with uid="TLXAHGJZIA",Visible=.t., Left=4, Top=2,;
    Alignment=0, Width=518, Height=19,;
    Caption="Bloccanti"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)='AD HOC ENTERPRISE' OR EMPTY(.w_MESBLOK))
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="CUKYIMOASF",Visible=.t., Left=4, Top=225,;
    Alignment=0, Width=518, Height=19,;
    Caption="Conferma/avvisi"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)='AD HOC ENTERPRISE' OR (Empty(.w_RESOCON1) OR ( !EMPTY(.w_RESOCON1) AND EMPTY(.w_MESBLOK) )))
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="DYKCVBIUHY",Visible=.t., Left=-1, Top=528,;
    Alignment=0, Width=572, Height=18,;
    Caption="Questa maschera gestisce log errori import da gsve_bck, gsve_bi2 e gsve_bi3 (solo per lotti)."  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="RNCWBWRQPC",Visible=.t., Left=-1, Top=547,;
    Alignment=0, Width=593, Height=18,;
    Caption="La maschera si ridimensiona e vengono spostati gli oggetti a seconda della valorizzazione delle due variabili"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="QOOJCXVGNN",Visible=.t., Left=-1, Top=566,;
    Alignment=0, Width=671, Height=18,;
    Caption="La maschera � chiamata anche da gsar_bco per dare un resoconto sul ricalcolo numero confezioni"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="EIKBUYYZYJ",Visible=.t., Left=-1, Top=585,;
    Alignment=0, Width=557, Height=18,;
    Caption="Lanciata anche da gsma_bl4 per resoconto su inserimento listini"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="OQRIWBITAN",Visible=.t., Left=-1, Top=603,;
    Alignment=0, Width=557, Height=18,;
    Caption="Lanciata anche da gsag_bco per resoconto impegni partecipanti e disponibilit� calendario"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="MXOIPERCHD",Visible=.t., Left=-1, Top=621,;
    Alignment=0, Width=557, Height=18,;
    Caption="Lanciata da gsar_beo per resoconto import/export contatti"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="TVEIGIUIYN",Visible=.t., Left=4, Top=2,;
    Alignment=0, Width=518, Height=19,;
    Caption="Conferma/avvisi"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)='AD HOC ENTERPRISE' OR (EMPTY(.w_RESOCON1) OR !EMPTY(.w_MESBLOK)))
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="PORLWIOKSP",Visible=.t., Left=4, Top=2,;
    Alignment=0, Width=518, Height=19,;
    Caption="Elenco errori bloccanti"    , forecolor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE' OR EMPTY(.w_MESBLOK))
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="VMFQEMNDFO",Visible=.t., Left=4, Top=2,;
    Alignment=0, Width=518, Height=19,;
    Caption="Richieste di conferma/avvisi"    , forecolor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE' OR (EMPTY(.w_RESOCON1) OR !EMPTY(.w_MESBLOK)))
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="QJGNQSHTMI",Visible=.t., Left=4, Top=225,;
    Alignment=0, Width=518, Height=19,;
    Caption="Richieste di conferma/avvisi"    , forecolor=rgb(0,0,255);
  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (UPPER(g_APPLICATION)<>'AD HOC ENTERPRISE' OR (Empty(.w_RESOCON1) OR ( !EMPTY(.w_RESOCON1) AND EMPTY(.w_MESBLOK) )))
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="RSHHPBOKTP",Visible=.t., Left=-1, Top=670,;
    Alignment=0, Width=557, Height=18,;
    Caption="Lanciata da gsim_bso per resoconto import trascodifiche mancanti"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_klg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
