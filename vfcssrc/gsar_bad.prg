* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bad                                                        *
*              Controlli testata documento                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-29                                                      *
* Last revis.: 2014-11-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione,pArrKey,pParent,pTrsOk
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bad",oParentObject,m.pAzione,m.pArrKey,m.pParent,m.pTrsOk)
return(i_retval)

define class tgsar_bad as StdBatch
  * --- Local variables
  pAzione = space(1)
  pArrKey = space(10)
  pParent = .NULL.
  pTrsOk = .f.
  w_Azione = space(1)
  w_Serial = space(10)
  w_Padre = .NULL.
  w_MySelf = .NULL.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_Loop = 0
  w_Param = space(1)
  w_BNOSCHED = .f.
  w_OLDMSG = space(0)
  w_RESERRIF = space(10)
  w_REROWRIF = 0
  w_RENUMRIF = 0
  w_REKEYSAL = space(20)
  w_RECODMAG = space(5)
  w_RECODMAT = space(5)
  w_REFLIMPE = space(1)
  w_REF2IMPE = space(1)
  w_REFLORDI = space(1)
  w_REF2ORDI = space(1)
  w_REFLRISE = space(1)
  w_REF2RISE = space(1)
  w_REQTASAL = 0
  w_RE_FLEVAS = space(1)
  w_REQTAEVA = 0
  w_REIMPEVA = 0
  w_RETIPRIG = space(1)
  w_CODCOL = space(5)
  w_CODCONF = space(3)
  w_QTAEV1 = 0
  w_OK = .f.
  w_QTAMOV = 0
  w_FLGCOM = space(1)
  w_LASTDET = .f.
  w_OQTAUM1 = 0
  w_FLLOTT = space(1)
  w_RIFMAG = space(5)
  w_VALESE = space(3)
  w_MESS = space(200)
  w_OQTAEV1 = 0
  w_OCAORIF = 0
  w_RIFMAT = space(5)
  w_TIPRIG = space(1)
  w_SERIAL = space(10)
  w_LOTDIF = space(1)
  w_FLANAL = space(1)
  w_CODMAT = space(5)
  w_PREZZO = 0
  w_RQTA = 0
  w_FGMRIF = space(1)
  w_FLAGLO = space(1)
  w_FLAG2LO = space(1)
  w_QTAESI = 0
  w_OLDQTA = 0
  w_PARCLF = space(1)
  w_TESDIS = .f.
  w_FIDD = ctod("  /  /  ")
  w_CODUBI = space(20)
  w_CODUB2 = space(20)
  w_COMAG = space(5)
  w_CODLOT = space(20)
  w_DATGEN = ctod("  /  /  ")
  w_COART = space(20)
  w_RCPR = 0
  w_PERIVA = 0
  w_SCONT1 = 0
  w_IMPCOM = 0
  w_STIMPCOM = 0
  w_ORD_EVA = 0
  w_OLDEVAS1 = space(1)
  w_ORDFLCOCO = space(1)
  w_ORIMPCOM = 0
  w_ORD_PRZ = 0
  w_CO1CODCOM = space(15)
  w_CO1CODCOS = space(5)
  w_CODATTCOM = space(15)
  w_ORDFLORCO = space(1)
  w_COAPPSAL = 0
  w_IMPCOMCOM = 0
  w_SIMVAL = space(5)
  w_OIMPSCO = 0
  w_OIMPACC = 0
  w_RFLSCOR = space(1)
  w_OLDSER = space(10)
  w_TIPFAT = space(1)
  w_CONTA = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_OSCONT1 = 0
  w_OSCONT2 = 0
  w_OSCONT3 = 0
  w_OSCONT4 = 0
  w_RVALNAZ = space(3)
  w_RCODVAL = space(3)
  w_RCAOVAL = 0
  w_DOCEVA = space(10)
  w_FLVESC = .f.
  w_QTAI = 0
  w_QTAO = 0
  w_QTAIMP = 0
  w_QTAIM1 = 0
  w_FLDISC = space(1)
  w_RISPERIV = 0
  w_STORNA = .f.
  w_EVCASC = space(1)
  w_OKCAMBIO = .f.
  w_NCAOVAL = 0
  w_OQTAIM1 = 0
  w_IVATRA = 0
  w_CODIVAINC = space(5)
  w_CODIVAIMB = space(5)
  w_CODIVATRA = space(5)
  w_CODVALOR = space(3)
  w_CAMOR = 0
  w_FLOMAG = space(1)
  w_STATUS = space(1)
  w_IMPEVATOT = 0
  w_INDIVA = 0
  w_INDIVE = 0
  w_FLULCA = space(1)
  w_FLULPV = space(1)
  w_FLCASC = space(1)
  w_RVALULT = 0
  w_RPREZZO = 0
  w_RVALRIG = 0
  w_RVALMAG = 0
  w_OVALRIG = 0
  w_VALRIG = 0
  w_VALMAG = 0
  w_FLELGM = space(1)
  w_LFLRISE = space(1)
  w_CLADOR = space(2)
  w_IVA = 0
  w_CODIVA = space(5)
  w_SPEINC = 0
  w_SPEIMB = 0
  w_SPETRA = 0
  w_CODIVE = space(3)
  w_CLDOOR = space(2)
  w_FLGOK = space(1)
  w_IVAACC = 0
  w_STSERRIF = space(10)
  w_SPESACC = 0
  w_IVAINC = 0
  w_IVAIMB = 0
  w_EVARIF = space(1)
  w_COVAR = space(20)
  w_APPO2 = .f.
  w_DATG1 = ctod("  /  /  ")
  w_APPO = 0
  w_IMPUTI = 0
  w_DATG2 = ctod("  /  /  ")
  w_DATG = ctod("  /  /  ")
  w_APPO1 = space(1)
  w_APPO = space(1)
  w_DATB1 = ctod("  /  /  ")
  w_DATB = ctod("  /  /  ")
  w_DATB2 = ctod("  /  /  ")
  w_FLDEL = .f.
  w_QTAR = 0
  w_IMPNAZ = 0
  w_KEYSAL = space(20)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_OQTAMOV = 0
  w_QTAP = 0
  w_OIMPNAZ = 0
  w_PARCAU = space(1)
  w_OPREZZO = 0
  w_APPSAL = 0
  w_OQTAEVA = 0
  w_NQTAEVA = 0
  w_SRV = space(1)
  w_RMAG = space(5)
  w_OIMPEVA = 0
  w_OIMPDAE = 0
  w_PNDOC = space(1)
  w_RCAS = space(1)
  w_TOTDOC = 0
  w_TOTVALFIS = 0
  w_DR = ctod("  /  /  ")
  w_NR = space(6)
  w_OQTASAL = 0
  w_FLRISE = space(1)
  w_FID1 = 0
  w_QTAUM1 = 0
  w_ROWORD = 0
  w_OFLEVAS = space(1)
  w_OEFFEVA = ctod("  /  /  ")
  w_F2RISE = space(1)
  w_FID2 = 0
  w_FIDS4 = 0
  w_FIDS5 = 0
  w_FIDS6 = 0
  w_NUMRIF = 0
  w_FLORDI = space(1)
  w_FID3 = 0
  w_SERRIF = space(10)
  w_TIPDOC = space(2)
  w_RDATDOC = ctod("  /  /  ")
  w_NQTAEV1 = 0
  w_F2ORDI = space(1)
  w_FID4 = 0
  w_CAUMAG = space(5)
  w_ROWRIF = 0
  w_NQTASAL = 0
  w_FLIMPE = space(1)
  w_FID5 = 0
  w_CAUCOL = space(5)
  w_CFUNC = space(10)
  w_FLARIF = space(1)
  w_F2IMPE = space(1)
  w_FID6 = 0
  w_CODMAG = space(5)
  w_FLAPP = .f.
  w_FLERIF = space(1)
  w_SEGNALA = .f.
  w_FIDRES = 0
  w_FLGEFA = space(1)
  w_FLSILI = space(1)
  w_FLCASH = space(1)
  w_FLCRIS = space(1)
  w_FLRISC = space(1)
  w_FLFIDO = space(1)
  w_TOTFATTU = 0
  w_CAONAZ = 0
  w_OPEDIC = space(1)
  w_DECTOP = 0
  w_DECTOU = 0
  w_DECTOT = 0
  w_UTIDIC = 0
  w_MAXFID = 0
  w_OIMPUTI = 0
  w_TIPDIC = space(1)
  w_DATDIC = ctod("  /  /  ")
  w_FLBLVE = space(1)
  w_MAXORD = 0
  w_FLDTPR = space(1)
  w_FLPACK = space(1)
  w_ULTCAR = ctod("  /  /  ")
  w_ULTSCA = ctod("  /  /  ")
  w_PERIVE = 0
  w_IMPODL = .f.
  w_FLARCO = space(1)
  w_DDCODDES = space(5)
  w_DDNOMDES = space(40)
  w_DDINDIRI = space(35)
  w_DD__CAP = space(8)
  w_DDLOCALI = space(30)
  w_DDPROVIN = space(2)
  w_DDCODNAZ = space(3)
  w_TIPRIF = space(2)
  w_CARICAU = space(1)
  w_CODNAZ = space(3)
  w_Cur_Ev_Row = space(10)
  w_XCONORN = space(15)
  w_TESLOT = .f.
  w_FLUBIC = space(1)
  w_F2UBIC = space(1)
  w_OLDTES = space(35)
  w_DELESP = .f.
  w_OFLPROV = space(1)
  w_SPEBOL = 0
  w_SERPDA = space(10)
  w_ROWPDA = 0
  w_RCLADOC = space(2)
  w_SERFAT = space(10)
  w_RDAT = ctod("  /  /  ")
  w_RNUM = 0
  w_RALF = space(10)
  w_CODVAL = space(5)
  w_LDECCOM = 0
  w_TmpN = 0
  w_FLAGG = space(1)
  w_SERRIF_V = space(10)
  w_SERRIF_V_AGG = space(10)
  w_ROWRIF_V_AGG = 0
  w_IMPNAZ_V_AGG = 0
  w_ROWRIF_V = 0
  w_FGMRIF_V = space(1)
  w_IMPNAZ_V = 0
  w_LOOP = 0
  w_FLELGM_V = space(1)
  w_NUMRIF_V = 0
  w_NUMRIF_V = 0
  w_FLARIF_V = space(1)
  w_IMPCOM_V = 0
  w_VALCOM = space(3)
  w_COMDECTOT = 0
  w_OTIPDOC = space(5)
  w_OFLRISC = space(1)
  w_OIMPEVA_R = 0
  w_TmpFido = 0
  w_DECCOM = 0
  w_EVNUMRIF = 0
  w_EVAFLE = space(1)
  w_REFLEVAS = space(1)
  w_REF2EVAS = space(1)
  w_FAT_RAG = .f.
  w_RQTAIMP = 0
  w_RQTAIM1 = 0
  w_DMAGCAR = space(5)
  w_DMAGSCA = space(5)
  w_TESCOMP = .f.
  w_DOCRIG = .f.
  w_DOCROW = 0
  w_RESERIAL = space(10)
  w_RETIPDOC = space(5)
  w_RENUMDOC = 0
  w_REALFDOC = space(10)
  w_REDATDOC = ctod("  /  /  ")
  w_TIPPAG = space(2)
  w_FLINTR = space(1)
  w_PARA = space(1)
  w_NUMVEN = space(1)
  w_FLPDOC = space(1)
  w_FINEANNO = ctod("  /  /  ")
  w_INIANNO = ctod("  /  /  ")
  w_FLCHKNUMD = .f.
  w_FLCHKPRO = .f.
  w_DFLPP = space(1)
  w_TIPO = space(1)
  w_FLGCON = space(1)
  w_CAUOBS = ctod("  /  /  ")
  w_CAUPFI = space(5)
  w_CAUCOD = space(5)
  w_FLELGM = space(1)
  w_ROWNUM = 0
  w_AGGSAL = space(10)
  w_AGGSAL1 = space(10)
  w_IMPEVA = 0
  w_OFLDIF = space(36)
  w_CODART = space(20)
  w_MVSERIAL = space(10)
  w_MVNUMRIF = 0
  w_MVCAOVAL = 0
  w_MVCODCOM = space(15)
  w_MVACIVA1 = space(5)
  w_MVAIMPN1 = 0
  w_MVCODCEN = space(15)
  w_MVVOCCEN = space(15)
  w_MVCODATT = space(15)
  w_MVFLNOAN = space(1)
  w_MVDATREG = ctod("  /  /  ")
  w_MVDATCIV = ctod("  /  /  ")
  w_MVCODVAL = space(3)
  w_MVFLINTE = space(1)
  w_MVGENPRO = space(1)
  w_MVCODUBI = space(20)
  w_MVCODUB2 = space(20)
  w_MVACIVA2 = space(5)
  w_MVRIFACC = space(10)
  w_MVNUMCOR = space(25)
  w_MVCESSER = space(10)
  w_MVFLSCAF = space(1)
  w_MVRIFODL = space(15)
  w_MVAFLOM1 = space(1)
  w_MVAFLOM2 = space(1)
  w_MVAFLOM3 = space(1)
  w_MVAFLOM4 = space(1)
  w_MVAFLOM5 = space(1)
  w_MVAFLOM6 = space(1)
  w_MVAIMPN2 = 0
  w_MVFLVEAC = space(1)
  w_MVTCAMAG = space(5)
  w_MVTCOLIS = space(5)
  w_MVVALNAZ = space(3)
  w_MVACIVA3 = space(5)
  w_MVAIMPN3 = 0
  w_MVNUMDOC = 0
  w_MVCODORN = space(15)
  w_MVRIFCON = space(10)
  w_MVRIFDCO = space(10)
  w_MVRIFDIC = space(15)
  w_MVACIVA4 = space(5)
  w_MVAIMPN4 = 0
  w_MVTIPCON = space(1)
  w_MVCLADOC = space(2)
  w_MVCODIVE = space(5)
  w_MVACIVA5 = space(5)
  w_MVAIMPN5 = 0
  w_MVCODCON = space(15)
  w_MVACIVA6 = space(5)
  w_MVAIMPN6 = 0
  w_MVACCONT = 0
  w_MVFLGIOM = space(1)
  w_MVACCPRE = 0
  w_MVFLINTR = space(1)
  w_MVALFDOC = space(10)
  op_MVNUMDOC = 0
  op_MVALFDOC = space(10)
  op_MVNUMEST = 0
  op_MVALFEST = space(10)
  w_MVFLCONT = space(1)
  w_MVTIPDOC = space(5)
  w_MVRIFFAD = space(10)
  w_MVSERDDT = space(10)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVANNRET = space(4)
  w_MVPRD = space(2)
  w_MVCODESE = space(4)
  w_MVMOVCOM = space(10)
  w_MVFLOMAG = space(1)
  w_MVSPEBOL = 0
  w_MVFLPROV = space(1)
  w_MVRIFESP = space(10)
  w_MVFLBLOC = space(1)
  w_MVFLOFFE = space(1)
  w_MVCODVET = space(5)
  w_MVCODPOR = space(1)
  w_MVCODSPE = space(3)
  w_MVANNDOC = space(4)
  w_MVANNPRO = space(4)
  w_MVNUMEST = 0
  w_MVALFEST = space(10)
  w_MVPRP = space(2)
  w_MVCODBAN = space(10)
  w_MVCODPAG = space(5)
  w_MVTIPPER = space(1)
  w_MVPERRET = 0
  w_MVIVABOL = space(5)
  w_MVIMPARR = 0
  w_MVIVAARR = space(5)
  w_MVCAUCON = space(5)
  w_MVFLCASC = space(1)
  w_MVF2CASC = space(1)
  w_MVFLRISE = space(1)
  w_MVF2RISE = space(1)
  w_MVFLORDI = space(1)
  w_MVF2ORDI = space(1)
  w_MVFLIMPE = space(1)
  w_MVF2IMPE = space(1)
  w_MVCAUCOL = space(5)
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  w_MVFLELGM = space(1)
  w_MVDATGEN = ctod("  /  /  ")
  w_MVQTAEV1 = 0
  w_MVQTAUM1 = 0
  w_MVIMPEVA = 0
  w_MVFLEVAS = space(1)
  w_MVCODART = space(20)
  w_CPROWORD = 0
  w_MVTIPRIG = space(1)
  w_MVCODICE = space(20)
  w_DaGest = .f.
  w_TRIG = 0
  w_RIGA = space(4)
  w_ORIORD = space(4)
  w_ORISER = space(10)
  w_TMPMESS = space(10)
  w_LMESS = space(100)
  w_NUMMAG = 0
  w_RIGAPIENA = .f.
  w_FLINTE = space(1)
  w_CATDOC = space(1)
  w_FLARIF = space(1)
  w_FLEVAS = space(1)
  w_NR = space(6)
  w_MVUNIMIS = space(3)
  w_OLDUNIMIS = space(3)
  w_DESUM = space(35)
  w_FLDIF = .f.
  w_SERTEST = space(10)
  w_ROWTEST = 0
  w_CHECKEVA = .f.
  w_TESTFLARIF = space(1)
  w_RIFTEST = 0
  w_SERRDESC = space(10)
  * --- WorkFile variables
  ADDE_SPE_idx=0
  ATTIVITA_idx=0
  CAM_AGAZ_idx=0
  CAU_CONT_idx=0
  CONTI_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  ESERCIZI_idx=0
  INCDCORR_idx=0
  TIP_DOCU_idx=0
  RAG_FATT_idx=0
  DOC_DETT_idx=0
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo al salvataggio dei documenti e scritture sul database riguardanti il documento
    *     salvato. Il batch pu� essere lanciato non solo dalla gestione documentale ma anche da 
    *     batch di generazione documenti, import, ecc.
    *     
    *     ATTENZIONE: Nelle manual block sono gestite una serie di metodi per gestire il transitorio
    *     nel caso di lancio direttamente dalla gestione o del cursore nel caso il batch venga lanciato 
    *     da una generazione documenti
    * --- pAzione: 'I' Inserimento, 'D' Cancellazione, 'U' Modifica
    * --- Array delle chiavi del documento
    * --- oParentObject nel caso di lancio da gestione. Altrimenti .Null.
    * --- pTrsOk Passata per riferimento: indica se la transazione deve fallire
    *     .T. Tutto ok-> la transazione va a buon fine
    *     .F. Errore-> la transazione viene abbandonata
    * --- istanzio oggetto per mess. incrementali
    * --- Per gestione messaggistica di ritorno.
    *     Non posso visualizzare messaggi di avviso
    this.w_oMESS=createobject("ah_message")
    this.w_Azione = this.pAzione
    this.w_Loop = 1
    if Type("g_SCHEDULER")<>"C" Or cp_GetGlobalVar("g_SCHEDULER")="N"
      * --- Se schedulatore non � gia attivo, per impedire la visualizzazione di messaggi
      *     simulo una sua attivazione...
      * --- Lo schedulatore � attivo
      cp_SetGlobalVar("g_SCHEDULER","S")
      * --- Ripulisco la variabile contenente il log
      cp_SetGlobalVar("g_MSG","")
      this.w_BNOSCHED = .T.
    else
      * --- Se scheduler attivo allore memorizzo l'attuale situazione di log (in g_MSG 
      *     ci sono anche le note di elaborazione)
      this.w_OLDMSG = cp_GetGlobalVar("g_MSG")
    endif
    * --- Recupero il seriale
    *     Devo utilizzare la macro perch� l'array passato per riferimento da sincronizza
    *     visto che questo � un batch viene passato da pArrKey a this.pArrKey perdendo il giusto puntamento
    *     pArrKey della classe invece � ancora valorizzato
     
 Local AMacro 
 AMacro = "pArrKey"
    do while this.w_Loop <= Alen(&AMacro,1)
      if Upper(Alltrim(&AMacro(this.w_Loop, 1) ) ) = "MVSERIAL"
        this.w_Serial = Alltrim( &AMacro(this.w_Loop, 2) )
        Exit
      endif
      this.w_Loop = this.w_Loop + 1
    enddo
    this.w_Loop = 1
    if Type("this.pParent")="O"
      * --- Se il batch � lanciato dalla gestione pParent contiene l'oggetto gestione (Es.: GSVE_MDV)
      *     Se lanciato da batch di generazione pParent deve essere Null.
      if Upper(this.pParent.Class)="TGSVE_MDV" Or Upper(this.pParent.Class)="TGSAC_MDV" Or Upper(this.pParent.Class)="TGSOR_MDV"
        * --- Se lanciato da gestione assegno l'oggetto
        this.w_Padre = this.pParent
      else
        * --- Prevengo il caso in cui da batch venga passato un oggetto
        *     Comunque sia nel caso debba essere passato per qualche motivo 
        *     pParent resta sempre valorizzato correttamente
        this.w_Padre = .Null.
      endif
    endif
    * --- Oggetto MySelf per gestire funzioni nell'area manuale
    this.w_MySelf = This
    if Type("this.w_Padre")="O"
      * --- Variabili Inizializzate
      this.w_MVSERIAL = this.w_Padre.w_MVSERIAL
      this.w_MVTIPDOC = this.w_Padre.w_MVTIPDOC
      this.w_MVCAOVAL = this.w_Padre.w_MVCAOVAL
      this.w_FLGEFA = this.w_Padre.w_FLGEFA
      this.w_FLSILI = this.w_Padre.w_FLSILI
      this.w_FLCASH = this.w_Padre.w_FLSILI
      this.w_MVACIVA1 = this.w_Padre.w_MVACIVA1
      this.w_MVAIMPN1 = this.w_Padre.w_MVAIMPN1
      this.w_MVDATREG = this.w_Padre.w_MVDATREG
      this.w_MVDATCIV = this.w_Padre.w_MVDATCIV
      this.w_MVCODVAL = this.w_Padre.w_MVCODVAL
      this.w_MVGENPRO = this.w_Padre.w_MVGENPRO
      this.w_FLCRIS = this.w_Padre.w_FLCRIS
      this.w_FLRISC = this.w_Padre.w_FLRISC
      this.w_MVACIVA2 = this.w_Padre.w_MVACIVA2
      this.w_MVAIMPN2 = this.w_Padre.w_MVAIMPN2
      this.w_MVFLVEAC = this.w_Padre.w_MVFLVEAC
      this.w_MVVALNAZ = this.w_Padre.w_MVVALNAZ
      this.w_FLFIDO = this.w_Padre.w_FLFIDO
      this.w_MVACIVA3 = this.w_Padre.w_MVACIVA3
      this.w_MVAIMPN3 = this.w_Padre.w_MVAIMPN3
      this.w_MVCODORN = this.w_Padre.w_MVCODORN
      this.w_MVRIFCON = this.w_Padre.w_MVRIFCON
      this.w_MVRIFDCO = this.w_Padre.w_MVRIFDCO
      this.w_MVRIFDIC = this.w_Padre.w_MVRIFDCO
      this.w_MVACIVA4 = this.w_Padre.w_MVACIVA4
      this.w_MVAIMPN4 = this.w_Padre.w_MVAIMPN4
      this.w_MVTIPCON = this.w_Padre.w_MVTIPCON
      this.w_MVCLADOC = this.w_Padre.w_MVCLADOC
      this.w_MVCODIVE = this.w_Padre.w_MVCODIVE
      this.w_MVACIVA5 = this.w_Padre.w_MVACIVA5
      this.w_MVAIMPN5 = this.w_Padre.w_MVAIMPN5
      this.w_MVCODCON = this.w_Padre.w_MVCODCON
      this.w_TOTFATTU = this.w_Padre.w_TOTFATTU
      this.w_FLGCON = this.w_Padre.w_FLGCON
      this.w_TIPDIC = this.w_Padre.w_TIPDIC
      this.w_MVACIVA6 = this.w_Padre.w_MVACIVA6
      this.w_MVAIMPN6 = this.w_Padre.w_MVAIMPN6
      this.w_MVACCONT = this.w_Padre.w_MVACCONT
      this.w_CAONAZ = this.w_Padre.w_CAONAZ
      this.w_OPEDIC = this.w_Padre.w_OPEDIC
      this.w_MVFLGIOM = this.w_Padre.w_MVFLGIOM
      this.w_MVACCPRE = this.w_Padre.w_MVACCPRE
      this.w_MVNUMDOC = this.w_Padre.w_MVNUMDOC
      this.w_FLGCOM = this.w_Padre.w_FLGCOM
      this.w_DECTOP = this.w_Padre.w_DECTOP
      this.w_DECTOU = this.w_Padre.w_DECTOU
      this.w_DECTOT = this.w_Padre.w_DECTOT
      this.w_UTIDIC = this.w_Padre.w_UTIDIC
      this.w_MVFLINTR = this.w_Padre.w_MVFLINTR
      this.w_MVALFDOC = this.w_Padre.w_MVALFDOC
      this.w_MAXFID = this.w_Padre.w_MAXFID
      this.w_OIMPUTI = this.w_Padre.w_OIMPUTI
      this.w_MVFLCONT = this.w_Padre.w_MVFLCONT
      this.w_MVRIFFAD = this.w_Padre.w_MVRIFFAD
      this.w_MVSERDDT = this.w_Padre.w_MVSERDDT
      this.w_MVDATDOC = this.w_Padre.w_MVDATDOC
      this.w_MVPRD = this.w_Padre.w_MVPRD
      this.w_DATDIC = this.w_Padre.w_DATDIC
      this.w_MVCODESE = this.w_Padre.w_MVCODESE
      this.w_FLBLVE = this.w_Padre.w_FLBLVE
      this.w_MAXORD = this.w_Padre.w_MAXORD
      this.w_FLDTPR = this.w_Padre.w_FLDTPR
      this.w_MVMOVCOM = this.w_Padre.w_MVMOVCOM
      this.w_MVFLOMAG = this.w_Padre.w_MVFLOMAG
      this.w_MVSPEBOL = this.w_Padre.w_MVSPEBOL
      this.w_MVFLPROV = this.w_Padre.w_MVFLPROV
      this.w_FLPACK = this.w_Padre.w_FLPACK
      this.w_ULTCAR = this.w_Padre.w_ULTCAR
      this.w_ULTSCA = this.w_Padre.w_ULTSCA
      this.w_PERIVE = this.w_Padre.w_PERIVE
      this.w_IMPODL = this.w_Padre.w_IMPODL
      this.w_MVRIFESP = this.w_Padre.w_MVRIFESP
      this.w_MVFLBLOC = this.w_Padre.w_MVFLBLOC
      this.w_MVFLOFFE = this.w_Padre.w_MVFLOFFE
      this.w_MVTCAMAG = this.w_Padre.w_MVTCAMAG
      this.w_FLARCO = this.w_Padre.w_FLARCO
      this.w_DDCODDES = this.w_Padre.w_DDCODDES
      this.w_DDNOMDES = this.w_Padre.w_DDNOMDES
      this.w_DDINDIRI = this.w_Padre.w_DDINDIRI
      this.w_DD__CAP = this.w_Padre.w_DD__CAP
      this.w_DDLOCALI = this.w_Padre.w_DDLOCALI
      this.w_DDPROVIN = this.w_Padre.w_DDPROVIN
      this.w_DDCODNAZ = this.w_Padre.w_DDCODNAZ
      this.w_TIPRIF = this.w_Padre.w_TIPRIF
      this.w_CARICAU = this.w_Padre.w_CARICAU
      this.w_MVCODVET = this.w_Padre.w_MVCODVET
      this.w_MVCODPOR = this.w_Padre.w_MVCODPOR
      this.w_MVCODSPE = this.w_Padre.w_MVCODSPE
      this.w_MVCAUCON = this.w_Padre.w_MVCAUCON
      this.w_CODNAZ = this.w_Padre.w_CODNAZ
      this.w_Cur_Ev_Row = this.w_Padre.w_Cur_Ev_Row
      this.w_XCONORN = this.w_Padre.w_XCONORN
      this.w_TESLOT = this.w_Padre.w_TESLOT
      this.w_MVANNDOC = this.w_Padre.w_MVANNDOC
      this.w_MVANNPRO = this.w_Padre.w_MVANNPRO
      this.w_MVNUMEST = this.w_Padre.w_MVNUMEST
      this.w_MVALFEST = this.w_Padre.w_MVALFEST
      this.w_MVPRP = this.w_Padre.w_MVPRP
      this.w_OFLPROV = this.w_Padre.w_OFLPROV
      this.w_MVRIFACC = this.w_Padre.w_MVRIFACC
      this.w_OLDTES = this.w_Padre.w_OLDTES
      this.w_MVTCOLIS = this.w_Padre.w_MVTCOLIS
      this.w_MVAFLOM1 = this.w_Padre.w_MVAFLOM1
      this.w_MVAFLOM2 = this.w_Padre.w_MVAFLOM2
      this.w_MVAFLOM3 = this.w_Padre.w_MVAFLOM3
      this.w_MVAFLOM4 = this.w_Padre.w_MVAFLOM4
      this.w_MVAFLOM5 = this.w_Padre.w_MVAFLOM5
      this.w_MVAFLOM6 = this.w_Padre.w_MVAFLOM6
      this.w_MVRIFODL = this.w_Padre.w_MVRIFODL
      this.w_MVCODCOM = this.w_Padre.w_MVCODCOM
      this.w_DECCOM = this.w_Padre.w_DECCOM
      this.w_MVCODUBI = this.w_Padre.w_MVCODUBI
      this.w_MVCODUB2 = this.w_Padre.w_MVCODUB2
      this.w_MVNUMRIF = this.w_Padre.w_MVNUMRIF
      this.w_TESCOMP = this.w_Padre.w_TESCOMP
      this.w_FLUBIC = this.w_Padre.w_FLUBIC
      this.w_F2UBIC = this.w_Padre.w_F2UBIC
      this.w_PARCAU = this.w_Padre.w_PARCAU
      this.w_PARCLF = this.w_Padre.w_PARCLF
      this.w_MVCODBAN = this.w_Padre.w_MVCODBAN
      this.w_MVCODPAG = this.w_Padre.w_MVCODPAG
      this.w_MVNUMCOR = this.w_Padre.w_MVNUMCOR
      this.w_MVFLSCAF = this.w_Padre.w_MVFLSCAF
      this.w_TIPDOC = this.w_Padre.w_TIPDOC
      this.w_FLINTR = this.w_Padre.w_FLINTR
      this.w_MVANNRET = this.w_Padre.w_MVANNRET
      this.w_MVTIPPER = this.w_Padre.w_MVTIPPER
      this.w_MVPERRET = this.w_Padre.w_MVPERRET
      this.op_MVNUMDOC = this.w_Padre.op_MVNUMDOC
      this.op_MVALFDOC = this.w_Padre.op_MVALFDOC
      this.w_FLCHKNUMD = this.w_Padre.w_FLCHKNUMD
      this.w_FLCHKPRO = this.w_Padre.w_FLCHKPRO
      this.w_DFLPP = this.w_Padre.w_DFLPP
      this.w_MVIVABOL = this.w_Padre.w_MVIVABOL
      this.w_MVIMPARR = this.w_Padre.w_MVIMPARR
      this.w_MVIVAARR = this.w_Padre.w_MVIMPARR
      this.op_MVNUMEST = this.w_Padre.w_MVNUMEST
      this.op_MVALFEST = this.w_Padre.w_MVALFEST
      this.w_CAUPFI = this.w_Padre.w_CAUPFI
      this.w_CAUCOD = this.w_Padre.w_CAUCOD
      this.w_MVFLINTE = this.w_Padre.w_MVFLINTE
      * --- Negli ordini e nelle vendite non c'� w_FLPDOC
      if TYPE("this.w_Padre.w_FLPDOC")="C" 
 
        this.w_NUMVEN = this.w_Padre.w_FLPDOC
      else
        this.w_NUMVEN = " "
      endif
      * --- Lanciato da gestione
      this.w_CFUNC = this.w_PADRE.cFunction
      * --- Lanciato da gestione
      this.w_DaGest = .T.
    else
      * --- Lanciato da batch
      this.w_CFUNC = IIF(this.pAzione="I","Load",IIF(this.pAzione="D","Query","Edit"))
      * --- Lanciato da batch
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_Serial);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              MVSERIAL = this.w_Serial;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
        this.w_MVALFEST = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
        this.w_MVANNDOC = NVL(cp_ToDate(_read_.MVANNDOC),cp_NullValue(_read_.MVANNDOC))
        this.w_MVANNPRO = NVL(cp_ToDate(_read_.MVANNPRO),cp_NullValue(_read_.MVANNPRO))
        this.w_MVANNRET = NVL(cp_ToDate(_read_.MVANNRET),cp_NullValue(_read_.MVANNRET))
        this.w_MVCAUCON = NVL(cp_ToDate(_read_.MVCAUCON),cp_NullValue(_read_.MVCAUCON))
        this.w_MVCLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
        this.w_MVCODBAN = NVL(cp_ToDate(_read_.MVCODBAN),cp_NullValue(_read_.MVCODBAN))
        this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
        this.w_MVCODESE = NVL(cp_ToDate(_read_.MVCODESE),cp_NullValue(_read_.MVCODESE))
        this.w_MVCODORN = NVL(cp_ToDate(_read_.MVCODORN),cp_NullValue(_read_.MVCODORN))
        this.w_MVCODPAG = NVL(cp_ToDate(_read_.MVCODPAG),cp_NullValue(_read_.MVCODPAG))
        this.w_MVDATCIV = NVL(cp_ToDate(_read_.MVDATCIV),cp_NullValue(_read_.MVDATCIV))
        this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
        this.w_MVDATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
        this.w_MVFLBLOC = NVL(cp_ToDate(_read_.MVFLBLOC),cp_NullValue(_read_.MVFLBLOC))
        this.w_MVFLCONT = NVL(cp_ToDate(_read_.MVFLCONT),cp_NullValue(_read_.MVFLCONT))
        this.w_MVFLINTE = NVL(cp_ToDate(_read_.MVFLINTE),cp_NullValue(_read_.MVFLINTE))
        this.w_MVFLINTR = NVL(cp_ToDate(_read_.MVFLINTR),cp_NullValue(_read_.MVFLINTR))
        this.w_MVFLOFFE = NVL(cp_ToDate(_read_.MVFLOFFE),cp_NullValue(_read_.MVFLOFFE))
        this.w_MVFLPROV = NVL(cp_ToDate(_read_.MVFLPROV),cp_NullValue(_read_.MVFLPROV))
        this.w_MVFLSCAF = NVL(cp_ToDate(_read_.MVFLSCAF),cp_NullValue(_read_.MVFLSCAF))
        this.w_MVFLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
        this.w_MVGENPRO = NVL(cp_ToDate(_read_.MVGENPRO),cp_NullValue(_read_.MVGENPRO))
        this.w_MVIMPARR = NVL(cp_ToDate(_read_.MVIMPARR),cp_NullValue(_read_.MVIMPARR))
        this.w_MVIVAARR = NVL(cp_ToDate(_read_.MVIVAARR),cp_NullValue(_read_.MVIVAARR))
        this.w_MVIVABOL = NVL(cp_ToDate(_read_.MVIVABOL),cp_NullValue(_read_.MVIVABOL))
        this.w_MVMOVCOM = NVL(cp_ToDate(_read_.MVMOVCOM),cp_NullValue(_read_.MVMOVCOM))
        this.w_MVNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
        this.w_MVNUMEST = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
        this.w_MVPERRET = NVL(cp_ToDate(_read_.MVPERRET),cp_NullValue(_read_.MVPERRET))
        this.w_MVPRD = NVL(cp_ToDate(_read_.MVPRD),cp_NullValue(_read_.MVPRD))
        this.w_MVPRP = NVL(cp_ToDate(_read_.MVPRP),cp_NullValue(_read_.MVPRP))
        this.w_MVRIFACC = NVL(cp_ToDate(_read_.MVRIFACC),cp_NullValue(_read_.MVRIFACC))
        this.w_MVRIFDCO = NVL(cp_ToDate(_read_.MVRIFDCO),cp_NullValue(_read_.MVRIFDCO))
        this.w_MVRIFESP = NVL(cp_ToDate(_read_.MVRIFESP),cp_NullValue(_read_.MVRIFESP))
        this.w_MVRIFFAD = NVL(cp_ToDate(_read_.MVRIFFAD),cp_NullValue(_read_.MVRIFFAD))
        this.w_MVRIFODL = NVL(cp_ToDate(_read_.MVRIFODL),cp_NullValue(_read_.MVRIFODL))
        this.w_MVSERDDT = NVL(cp_ToDate(_read_.MVSERDDT),cp_NullValue(_read_.MVSERDDT))
        this.w_MVSPEBOL = NVL(cp_ToDate(_read_.MVSPEBOL),cp_NullValue(_read_.MVSPEBOL))
        this.w_MVTCAMAG = NVL(cp_ToDate(_read_.MVTCAMAG),cp_NullValue(_read_.MVTCAMAG))
        this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
        this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
        this.w_MVTIPPER = NVL(cp_ToDate(_read_.MVTIPPER),cp_NullValue(_read_.MVTIPPER))
        this.w_MVNUMCOR = NVL(cp_ToDate(_read_.MVNUMCOR),cp_NullValue(_read_.MVNUMCOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDCAUCOD,TDCAUPFI,TDFLARCO,TDFLCASH,TDFLPDOC,TDFLPPRO,TDFLANAL,TDLOTDIF,TDFLCOMM,TFFLGEFA"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDCAUCOD,TDCAUPFI,TDFLARCO,TDFLCASH,TDFLPDOC,TDFLPPRO,TDFLANAL,TDLOTDIF,TDFLCOMM,TFFLGEFA;
          from (i_cTable) where;
              TDTIPDOC = this.w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAUCOD = NVL(cp_ToDate(_read_.TDCAUCOD),cp_NullValue(_read_.TDCAUCOD))
        this.w_CAUPFI = NVL(cp_ToDate(_read_.TDCAUPFI),cp_NullValue(_read_.TDCAUPFI))
        this.w_FLARCO = NVL(cp_ToDate(_read_.TDFLARCO),cp_NullValue(_read_.TDFLARCO))
        this.w_FLCASH = NVL(cp_ToDate(_read_.TDFLCASH),cp_NullValue(_read_.TDFLCASH))
        this.w_FLPDOC = NVL(cp_ToDate(_read_.TDFLPDOC),cp_NullValue(_read_.TDFLPDOC))
        this.w_DFLPP = NVL(cp_ToDate(_read_.TDFLPPRO),cp_NullValue(_read_.TDFLPPRO))
        this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
        this.w_LOTDIF = NVL(cp_ToDate(_read_.TDLOTDIF),cp_NullValue(_read_.TDLOTDIF))
        this.w_FLGCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
        this.w_FLGEFA = NVL(cp_ToDate(_read_.TFFLGEFA),cp_NullValue(_read_.TFFLGEFA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCFLPART,CCTIPDOC"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_MVCAUCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCFLPART,CCTIPDOC;
          from (i_cTable) where;
              CCCODICE = this.w_MVCAUCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PARCAU = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
        this.w_TIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Calcolo l'intestatario tra Cliente/Fornitore e Per Conto Di..
      this.w_XCONORN = IIF(Not Empty(this.w_MVCODORN) And g_XCONDI = "S",this.w_MVCODORN,this.w_MVCODCON)
      * --- Variabile per controllo matricole
      this.w_TESLOT = IIF(this.w_LOTDIF="I",.F., .T.)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANPARTSN,AFFLINTR,ANFLGCON"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_XCONORN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANPARTSN,AFFLINTR,ANFLGCON;
          from (i_cTable) where;
              ANTIPCON = this.w_MVTIPCON;
              and ANCODICE = this.w_XCONORN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PARCLF = NVL(cp_ToDate(_read_.ANPARTSN),cp_NullValue(_read_.ANPARTSN))
        this.w_FLINTR = NVL(cp_ToDate(_read_.AFFLINTR),cp_NullValue(_read_.AFFLINTR))
        this.w_FLGCON = NVL(cp_ToDate(_read_.ANFLGCON),cp_NullValue(_read_.ANFLGCON))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Nel caso genero il documento da batch non considero variazioni di progressivo
      this.op_MVNUMDOC = this.w_MVNUMDOC
      this.op_MVALFDOC = this.w_MVALFDOC
      this.op_MVNUMEST = this.w_MVNUMEST
      this.op_MVALFEST = this.w_MVALFEST
      * --- Nel caso di generazione da batch, visto ch� � un documento nuovo, devo fare i 
      *     controlli di univocit�
      this.w_FLCHKNUMD = .T.
      this.w_FLCHKPRO = .T.
      this.w_MVSERIAL = this.w_Serial
      * --- Non lanciato da gestione
      this.w_DaGest = .F.
      * --- Utilizzato per il controllo matricole, indico fisso -20
      *     In pagina 2, nel ciclo sulle righe, viene poi riletto quello giusto
      this.w_MVNUMRIF = -20
    endif
    this.w_TRIG = 0
    this.w_OK = .T.
    * --- Date consolidamento
    *     CHKCONS con N all'utlimo parametro non ritorna il messaggio. 
    this.w_MESS = CHKCONS(this.w_MVFLVEAC,this.w_MVDATREG,"B","N")
    if Not Empty(this.w_MESS)
      this.w_OK = .F.
    endif
    this.w_Param = IIF(this.w_DaGest,"G","D")
    if this.w_OK And this.w_CFUNC<>"Query"
      * --- Se il N� Doc. � maggiore di 999999999999999
      if this.w_MVNUMDOC> 999999999999999
        this.w_MESS = Ah_MsgFormat("Il n� doc. non pu� essere maggiore di 999999999999999")
        this.w_OK = .F.
      endif
      * --- Controllo se data Competenza IVA Uguale o Minore data Documento
      if this.w_OK AND this.w_MVFLVEAC="V" AND this.w_MVCLADOC $ "FA-NC" AND NOT EMPTY(this.w_MVCAUCON) AND this.w_MVDATCIV>this.w_MVDATREG
        this.w_MESS = Ah_MsgFormat("Data competenza IVA superiore alla data documento")
        this.w_OK = .F.
      endif
      if this.w_OK AND EMPTY(this.w_MVCODPAG) AND g_PERPAR="S" AND NOT this.w_PARCAU $ " N" AND (this.w_PARCLF="S" OR (EMPTY(this.w_MVCODCON) AND this.w_MVCLADOC $ "DI-RF"))
        this.w_MESS = Ah_MsgFormat("Codice pagamento non definito")
        this.w_OK = .F.
      endif
      if NOT EMPTY(this.w_MVCODBAN)
        this.w_TIPPAG = LEFT(CHTIPPAG(this.w_MVCODPAG, "BO"), 2)
        if (this.w_TIPPAG="BO" AND EMPTY(this.w_MVNUMCOR)) AND ((this.w_MVTIPCON="C" AND this.w_MVCLADOC="NC") OR (this.w_MVTIPCON="F" AND this.w_MVCLADOC<>"NC")) AND this.w_MVFLSCAF<>"S"
          this.w_MESS = Ah_MsgFormat("Pagamento di tipo bonifico: inserire il conto corrente")
          this.w_OK = .F.
        endif
      endif
      if this.w_OK AND this.w_CFUNC="Load" AND this.w_FLINTR="S" AND this.w_TIPDOC="NE" AND EMPTY(this.w_MVANNRET)
        * --- Nota Credito INTRA senza specificati Periodi Rettifica, Portebbe trattarsi di Dimenticanza
        this.w_MESS = Ah_MsgFormat("Nel documento non sono stati specificati il periodo e/o l'anno di rettifica. (Bottone: dati testata)")
        this.w_OK = .F.
      endif
      if this.w_OK AND this.w_FLINTR="S" AND NOT EMPTY(this.w_MVANNRET)
        * --- Documento di rettifica INTRA verifica se completati i dati correlati
        if EMPTY(this.w_MVTIPPER)
          this.w_MESS = Ah_MsgFormat("Sul documento non � stata specificata la periodicit� relativa alla rettifica INTRA (bottone: dati testata)")
          this.w_OK = .F.
        else
          if (this.w_MVTIPPER $ "MT" AND this.w_MVPERRET=0) OR (this.w_MVTIPPER="M" AND this.w_MVPERRET>12) OR (this.w_MVTIPPER="T" AND this.w_MVPERRET>4)
            this.w_MESS = Ah_MsgFormat("Periodo di rettifica INTRA non inserito o incongruente con il tipo di periodicit� (bottone: dati testata)")
            this.w_OK = .F.
          endif
        endif
      endif
      * --- Controllo Univocit� Documento (Ciclo Vendite)
      *     Controllo Univocit� Protocollo (Ciclo Acquisti)
      *     Controllo sequenzialit� registri IVA (Vendite/Acquisti)
      *     Controllo univocit� documento (Tutti)
      if this.w_OK
        * --- FLAG NUMERAZIONE VENDITE NELLE CAUSALI D'ACQUISTO
        if (this.w_MVFLVEAC="V" OR (this.w_MVFLVEAC="A" AND this.w_NUMVEN="S")) AND this.w_MVCLADOC<>"RF" 
          * --- Parametri per determinare se il documento � in sequenza
          this.w_FINEANNO = cp_CharToDate("31-12-"+ALLTR(STR(YEAR(this.w_MVDATDOC),4,0)))
          this.w_INIANNO = cp_CharToDate("01-01-"+ALLTR(STR(YEAR(this.w_MVDATDOC),4,0)))
          * --- Se numerazione libera non effettuo il controllo
          if this.w_OK and (this.w_MVPRD<>"NN" OR this.w_MVCLADOC="DI" OR this.w_MVCLADOC="DT")
            if this.w_MVCLADOC="FA" Or this.w_MVCLADOC="NC"
              * --- Se fattura verifico l'univocit� tra prima nota e documenti
              this.w_PARA = "R"
            else
              * --- altrimenti verifico l'univocit� nei documenti
              this.w_PARA = "L"
            endif
            * --- Se � un documento interno con intestatario o DDT verifica l'uguaglianza di questo anche se � attivata la numerazione libera
            if (this.w_MVCLADOC="DI" OR (this.w_MVCLADOC="DT" AND this.w_MVPRD="NN")) AND NOT EMPTY(NVL(this.w_MVCODCON,SPACE(20)))
              this.w_OK = UNIVOC(SPACE(10),this.w_MVSERIAL,"V",this.w_MVNUMDOC,this.w_MVALFDOC,this.w_MVANNDOC, this.w_MVDATDOC,this.w_MVPRD,0,SPACE(2),SPACE(4), this.w_MVCODCON, this.w_MVTIPCON, this.w_PARA, this.w_MVCLADOC)
            else
              if this.w_MVPRD<>"NN"
                this.w_OK = UNIVOC(SPACE(10),this.w_MVSERIAL,"V",this.w_MVNUMDOC,this.w_MVALFDOC,this.w_MVANNDOC, cp_CharToDate("  -  -    "),this.w_MVPRD,0,SPACE(2),SPACE(4), SPACE(20), " ", this.w_PARA, "")
              endif
            endif
          endif
          * --- Tramite w_FLCHKNUMD inibisco i controlli in modifica se non ho modificato
          *     nessuno dei parametri della funzione
          if this.w_OK AND this.w_FLCHKNUMD
            * --- Controllo che non esistano documenti con numero maggiore e data minore
            *     o con numero minore e data maggiore del documento che si sta salvando
            *     Il filtro avviene sull'anno solare (Questi controlli sono x i registri IVA) e non sull'esercizio
            * --- Nel caso sia in caricamento non filtro sul seriale (la registrazione non � ancora sul
            *     database).
            *     Se MVPRD ='FV' ricerco anche considerando la prima nota..
            * --- Se � un documento interno con intestatario o DDT verifica l'uguaglianza di questo anche se � attivata la numerazione libera
            if (this.w_MVCLADOC="DI" OR (this.w_MVCLADOC="DT" AND this.w_MVPRD="NN"))
              this.w_MESS = CHKNUMD(this.w_MVDATDOC,this.w_MVNUMDOC,this.w_MVALFDOC,this.w_MVANNDOC,this.w_MVPRD,IIF(this.w_CFUNC="Edit",this.w_MVSERIAL,Space(10)),IIF(this.w_MVPRD="FV","D","T"), this.w_MVCLADOC, this.w_MVTIPCON, this.w_MVCODCON)
            else
              if this.w_MVPRD<>"NN"
                this.w_MESS = CHKNUMD(this.w_MVDATDOC,this.w_MVNUMDOC,this.w_MVALFDOC,this.w_MVANNDOC,this.w_MVPRD,IIF(this.w_CFUNC="Edit",this.w_MVSERIAL,Space(10)),IIF(this.w_MVPRD="FV","D","T"))
              endif
            endif
          endif
        endif
        if this.w_MVFLVEAC="A" 
          if this.w_MVCLADOC<>"OR"
            * --- Attenzione la var. w_DFLPP non � presente in gsor_mdv!
            * --- Se fattura o nota di credito (doc. contabilizzabili)   verifico
            *     l'univocit� del protocollo (anche per la prima nota),(PARAMETRO='P')
            *      altrimenti verifico solo sui documenti (PARAMETRO='N') se la causale
            *      documento gestisce la numerazione protocollo
            if this.w_MVCLADOC="FA" Or this.w_MVCLADOC="NC" OR this.w_DFLPP<>"N"
              if this.w_MVCLADOC="FA" Or this.w_MVCLADOC="NC"
                this.w_PARA = "P"
              else
                this.w_PARA = "N"
              endif
              this.w_OK = UNIVOC(SPACE(10),this.w_MVSERIAL,this.w_MVFLVEAC,0,SPACE(2),SPACE(4), cp_CharToDate("  -  -    "),this.w_MVPRP,this.w_MVNUMEST,this.w_MVALFEST,this.w_MVANNPRO,Space(15),Space(1),this.w_PARA, "")
            endif
            * --- Tramite w_FLCHKPRO inibisco i controlli in modifica se non ho modificato
            *     nessuno dei parametri della query
            if this.w_OK AND this.w_FLCHKPRO AND this.w_MVPRP<>"NN"
              * --- Controlla che NON vi siano registrazioni (P. Nota) con numero minore e data maggiore
              * --- Oppure che non vi siano registrazioni con numero maggiore e data minore
              * --- I controlli vanno fatti in base all'anno solare e non all'esercizio
              this.w_INIANNO = cp_CharToDate("01-01-"+STR(YEAR(this.w_MVDATDOC),4,0))
              this.w_FINEANNO = cp_CharToDate("31-12-"+STR(YEAR(this.w_MVDATDOC),4,0))
              this.w_MESS = CHKPROT(this.w_MVNUMEST,this.w_MVALFEST,this.w_MVDATREG,this.w_MVANNPRO,this.w_MVPRP)
              if NOT EMPTY(this.w_MESS)
                this.w_oPART = this.w_oMess.AddMsgPartNL("%1")
                this.w_oPART.AddParam(this.w_MESS)     
                this.w_MESS = this.w_oMess.ComposeMessage(.T.)
              endif
            endif
          endif
          do case
            case this.w_OK AND this.w_MVSPEBOL<>0 AND EMPTY(this.w_MVIVABOL)
              this.w_MESS = Ah_MsgFormat("Inserire codice IVA spese bolli")
              this.w_OK = .F.
            case this.w_OK AND this.w_MVIMPARR<>0 AND EMPTY(this.w_MVIVAARR)
              this.w_MESS = Ah_MsgFormat("Inserire codice IVA arrotondamenti")
              this.w_OK = .F.
          endcase
          if this.w_OK And Not Empty( this.w_MVCODCON )
            * --- Se documento contabilizzabile verifico l'univocit� considerando
            *     anche le registrazioni di prima nota con tipo reg. iva posto ad 'A'
            *     Altrimenti verifico solo i documenti con stesso w_MVPRD
            this.w_TIPO = IIF( Not Empty( this.w_MVCAUCON ) , "D" , "Z" )
            this.w_OK = UNIVOC(SPACE(10),this.w_MVSERIAL,this.w_MVFLVEAC,this.w_MVNUMDOC,this.w_MVALFDOC,SPACE(4), this.w_MVDATDOC,this.w_MVPRD,0,SPACE(2),SPACE(4), this.w_MVCODCON, this.w_MVTIPCON, this.w_TIPO, "")
          endif
        endif
      endif
      if this.w_OK And this.w_FLGCON="S" AND this.w_MVTIPCON="C" AND NOT EMPTY(this.w_MVCODCON) AND this.w_CFUNC="Load"
        * --- Blocco Documenti Da Contenzioso
        this.w_MESS = Ah_MsgFormat("Attivato blocco contenzioso per il cliente")
        this.w_OK = .F.
      endif
      if this.w_OK And g_ACQU<>"S" And this.w_MVFLVEAC="A" And Not this.w_MVCLADOC$"DI-DT-OR"
        * --- Documento di Acquisto dal ciclo vendite <> da DDT o DI
        this.w_MESS = Ah_MsgFormat("Categoria documento utilizzabile solo da ciclo acquisti")
        this.w_OK = .F.
      endif
      if this.w_OK
        * --- Controllo per verificare che il documento che sto sincronizzando, nel caso in cui evade un documento,
        *     non vada ad evadere un documento di origine gi� completamente evaso.
        *     In questo caso potrebe significare che che da un'altra sede ho gi� ricevuto i documenti di evasione di quel documento di origine
        *     oppure che la stessa sede ha gi� evaso il documento di origine
        *     Quindi annullo la sincronizzazione e segnalo
        this.w_oMESS=createobject("ah_message")
        * --- Select from gslr_qce
        do vq_exec with 'gslr_qce',this,'_Curs_gslr_qce','',.f.,.t.
        if used('_Curs_gslr_qce')
          select _Curs_gslr_qce
          locate for 1=1
          do while not(eof())
          this.w_RIGA = Alltrim( Str( _Curs_gslr_qce.ROWORD , 5, 0 ) )
          this.w_ORIORD = Alltrim( Str( _Curs_gslr_qce.ORIORD , 5, 0 ) )
          this.w_ORISER = _Curs_gslr_qce.ORISER
          * --- Se documento di origine mai evaso, significa che il documento di evasione
          *     � stato valutato prima, occorre o modicare l'ordine delle entit� o l'ordine
          *     di valutazione nelle entit�
          if _Curs_gslr_qce.EVASO=0
            this.w_oPART = this.w_oMess.AddMsgPartNL("Il documento evade il documento con chiave %3 che ha i dati sull'evasione a 0%0Verificare l'ordine di valutazione delle entit� sulla sede o l'espressione di ordinamento libero sull'entit� per processare prima il documento evaso rispetto a questo")
            this.w_oPART = this.w_oMess.AddMsgPartNL("Oppure, se il documento � stato evaso da altra sede, verificare che sia presente nel pacchetto")
          else
            this.w_oPART = this.w_oMess.AddMsgPartNL("Riga: %1 evade la riga %2 del documento con chiave %3%0La riga � gi� evasa totalmente da altri documenti")
          endif
          this.w_oPART.AddParam(this.w_RIGA)     
          this.w_oPART.AddParam(this.w_ORIORD)     
          this.w_oPART.AddParam(this.w_ORISER)     
          this.w_OK = .F.
          exit
            select _Curs_gslr_qce
            continue
          enddo
          use
        endif
        if Not this.w_OK
          this.w_MESS = this.w_oMess.ComposeMessage(.T.)
        endif
      endif
    endif
    if this.w_OK
      this.w_oMESS=createobject("ah_message")
      if this.w_OK And Not Empty(CHKCONS(IIF(g_PERCCR="S" AND this.w_FLANAL="S",this.w_MVFLVEAC+"C",this.w_MVFLVEAC),this.w_MVDATREG,"B","N"))
        * --- Eseguo controllo su data consolidamento impostata nei dati azienda
        this.w_MESS = CHKCONS(IIF(g_PERCCR="S" AND this.w_FLANAL="S",this.w_MVFLVEAC+"C",this.w_MVFLVEAC),this.w_MVDATREG,"B","N")
        this.w_oPART = this.w_oMess.AddMsgPart("%1")
        this.w_oPART.AddParam(this.w_MESS)     
      endif
      * --- Controlli bloccanti. (solo x cancellazione).
      *     Se sono in modifica, la modifica � preceduta da una cancellazione, sar� questa
      *     operazione eventualmente ad inibire la sincronizzazione.
      *     Se la sede attuale ha una fattura non contabilizzata e ne riceve una contabilizzata
      *     la fase di inserimento della fattura contabilizzata non deve dare msg di errore.
      if this.w_OK And this.w_CFUNC="Query"
        do case
          case this.w_MVFLCONT="S"
            this.w_oMESS.AddMsgPart("Documento contabilizzato")     
          case Not Empty( this.w_MVRIFACC )
            this.w_oMESS.AddMsgPart("Documento con acconto contabilizzato")     
          case g_COGE<>"S" AND NOT EMPTY(this.w_MVRIFDCO)
            this.w_oMESS.AddMsgPart("Documento confermato in gestione effetti")     
          case this.w_MVFLINTR="S"
            this.w_oMESS.AddMsgPart("Il documento ha generato gli elenchi INTRA")     
          case this.w_MVFLBLOC="S"
            this.w_oMESS.AddMsgPart("Documento generato da DDT di C/Lavoro rivalorizzato")     
          case g_PROD="S" AND (UPPER(ALLTRIM(this.w_MVRIFODL))= "CARICO" OR UPPER(ALLTRIM(this.w_MVRIFODL)) = "SCARICO")
            if UPPER(ALLTRIM(this.w_MVRIFODL))="CARICO"
              this.w_oMESS.AddMsgPart("Documento di carico generato da dichiarazione di produzione")     
            else
              this.w_oMESS.AddMsgPart("Documento di scarico generato da dichiarazione di produzione")     
            endif
          case this.w_MVFLVEAC="V" AND g_PERAGE="S" AND this.w_MVGENPRO="S"
            this.w_oMESS.AddMsgPart("Il documento ha generato dei movimenti di provvigione")     
          case g_GPOS="S" AND ((this.w_MVFLOFFE= "I" And this.w_CFUNC ="Query") Or ( this.w_MVFLOFFE$ "IDF" And this.w_CFUNC ="Edit"))
            this.w_oMESS.AddMsgPart("Documento generato da vendita negozio")     
          case Not Empty( this.w_MVMOVCOM ) And this.w_CFUNC ="Edit"
            this.w_oMESS.AddMsgPart("Documento generato da tempificazione")     
          case NOT EMPTY(this.w_MVSERDDT)
            this.w_oMESS.AddMsgPart("Documento di scarico componenti per C/Lavoro da magazzino terzista")     
          case this.w_MVCLADOC = "RF"
            * --- Se ricevuta fiscale controllo la presenza dell'Incasso.
            *     In tal caso blocco la modifica e la cancellazione
            * --- Select from INCDCORR
            i_nConn=i_TableProp[this.INCDCORR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.INCDCORR_idx,2],.t.,this.INCDCORR_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select INSERIAL  from "+i_cTable+" INCDCORR ";
                  +" where INORIDOC = "+cp_ToStrODBC(this.w_MVSERIAL)+"";
                   ,"_Curs_INCDCORR")
            else
              select INSERIAL from (i_cTable);
               where INORIDOC = this.w_MVSERIAL;
                into cursor _Curs_INCDCORR
            endif
            if used('_Curs_INCDCORR')
              select _Curs_INCDCORR
              locate for 1=1
              do while not(eof())
              this.w_oMESS.AddMsgPart("Documento associato ad incasso corrispettivo")     
              EXIT
                select _Curs_INCDCORR
                continue
              enddo
              use
            endif
        endcase
      endif
      this.w_TMPMESS = this.w_oMess.ComposeMessage(.F.)
      if Not Empty(this.w_TMPMESS)
        this.w_oMESS.AddMsgPartNL(", impossibile modificare/eliminare")     
        this.w_MESS = this.w_oMess.ComposeMessage(.T.)
        * --- Se un controllo salta deve valorizzare w_MESS
        this.w_OK = Empty( this.w_MESS )
      endif
      if this.w_OK And this.w_MVCLADOC $ "FA-NC"
        * --- Verifico se Fattura o Nota di credito se ha evaso un documento
        *     soggetto a ripartizione spese. Se si impedisco la modifica in quanto
        *     questa provocherebbe il ricalcolo di MVIMPNAZ...
        this.w_LMESS = CHKRIPSPE( this.w_MVSERIAL )
        this.w_OK = Empty( this.w_LMESS )
        this.w_MESS = IIF(Empty(this.w_LMESS),this.w_MESS,this.w_LMESS)
      endif
    endif
    if this.w_OK And this.w_CFUNC="Query"
      if g_COMM="S" And NOT EMPTY(this.w_MVMOVCOM)
        * --- Verifico che il documento non si� stato generato da una tempificazione
        *     Vado a leggere nel  Movimento di commessa attitiv� e Commessa (l'utente potrebbe modificare proprio quelle)
        *     Chiamo un Batch esterno, i movimenti NON SONO NELL'ANALISI PRINCIPALE
        GSPC_BDO(this,this.w_MVMOVCOM, "T")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- w_CODCOM e w_CODATT sono letti da GSPC_BDO
        if NOT EMPTY(this.w_CODCOM) AND NOT EMPTY(this.w_CODATT)
          this.w_STATUS = " "
          * --- Read from ATTIVITA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ATTIVITA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AT_STATO"+;
              " from "+i_cTable+" ATTIVITA where ";
                  +"ATCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
                  +" and ATTIPATT = "+cp_ToStrODBC("A");
                  +" and ATCODATT = "+cp_ToStrODBC(this.w_CODATT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AT_STATO;
              from (i_cTable) where;
                  ATCODCOM = this.w_CODCOM;
                  and ATTIPATT = "A";
                  and ATCODATT = this.w_CODATT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_STATUS = NVL(cp_ToDate(_read_.AT_STATO),cp_NullValue(_read_.AT_STATO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          do case
            case this.w_STATUS="L"
              this.w_OK = .F.
              this.w_MESS = ah_Msgformat("Attivit� lanciata. Impossibile variare")
            case this.w_STATUS="F"
              this.w_OK = .F.
              this.w_MESS = ah_Msgformat("Attivita completatata. Impossibile variare")
          endcase
        endif
      endif
    endif
    if this.w_OK
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Scorro il dettaglio documento...
    if this.w_OK
      if this.w_DaGest
        * --- Se lanciato da gestione lavoro sul TrsName...
        this.w_PADRE.MarkPos()     
      else
        * --- ...altrimenti costruisco un cursore con tutti i campi del dettaglio 
        *     e rinomino le colonne con t_+ nome campo
        vq_exec("query\CHKDOCUM.VQR",this,"GENEAPP")
      endif
      this.w_MySelf.FirstRow()     
      this.w_FLDEL = .F.
      this.w_TRIG = 0
      * --- Test se cancellata
      this.w_FLDEL = this.w_CFUNC="Query"
      this.w_CPROWORD = this.w_MySelf.Get("w_CPROWORD")
      this.w_MVTIPRIG = this.w_MySelf.Get("w_MVTIPRIG")
      this.w_MVCODICE = this.w_MySelf.Get("w_MVCODICE")
      * --- Array per controllo magazzino
      *     Non serve per righe cancellate
      this.w_RIGAPIENA = this.w_CPROWORD<>0 AND this.w_MVTIPRIG<>" " AND NOT EMPTY(this.w_MVCODICE)
       
 DECLARE ArrGiom(1) 
 ArrGiom(1) = Space(5)
      this.w_NUMMAG = 0
      this.w_OLDUNIMIS = "@@@"
      * --- Primo giro sulle righe non cancellate...
      do while Not this.w_MySelf.Eof_Trs()
        this.w_MySelf.SetRow()     
        if this.w_RIGAPIENA
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Se tutto ok passo alla prossima riga altrimenti esco...
        if this.w_OK
          this.w_MySelf.NextRow()     
        else
          Exit
        endif
      enddo
      if this.w_DaGest
        * --- Secondo giro, se tutto Ok sulle righe cancellate...
        *     Solo se lanciato da gestione
        if this.w_OK
          this.w_PADRE.FirstRowDel()     
          * --- Test Se riga Eliminata
          this.w_FLDEL = .T.
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Se tutto ok passo alla prossima riga altrimenti esco...
            if this.w_OK
              this.w_PADRE.NextRowDel()     
            else
              if not Empty( this.w_MESS )
                ah_ErrorMsg("%1",,"", this.w_MESS)
              endif
              Exit
            endif
          enddo
        endif
        if this.w_OK And this.w_MVFLPROV<>"S" And g_PERDIS="S" 
          * --- Lancio il check disponibilit� articoli
          this.w_OK = GSAR_BDA( This , "S", this.w_PADRE )
        endif
      endif
      if this.w_DaGest
        * --- Se lanciato da gestione mi riposiziono sul TrsName...
        this.w_PADRE.RePos(.F.)     
      else
        if Used("GENEAPP")
           
 Select GENEAPP 
 Use
        endif
      endif
    endif
    if this.w_OK AND this.w_TRIG=0 AND this.w_CFUNC<>"Query"
      this.w_MESS = Ah_MsgFormat("Registrazione senza righe di dettaglio")
      this.w_OK = .F.
    endif
    if this.w_OK AND g_EACD="S" AND this.w_MVFLPROV="N"
      if this.w_FLARCO="S" AND (NOT EMPTY(this.w_CAUPFI) OR NOT EMPTY(this.w_CAUCOD)) AND this.w_CFUNC <>"Query" 
        * --- Verifica Congruita' Causali
        if NOT EMPTY(this.w_CAUCOD)
          this.w_FLINTE = " "
          this.w_CATDOC = " "
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDFLINTE,TDCATDOC"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUCOD);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDFLINTE,TDCATDOC;
              from (i_cTable) where;
                  TDTIPDOC = this.w_CAUCOD;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            this.w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT (this.w_CATDOC="DI" AND (this.w_FLINTE="N" OR this.w_FLINTE=this.w_MVFLINTE))
            this.w_MESS = Ah_MsgFormat("Causale documento articoli composti incongruente")
            this.w_OK = .F.
          endif
        endif
        if this.w_OK AND NOT EMPTY(this.w_CAUPFI)
          this.w_FLINTE = " "
          this.w_CATDOC = " "
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDFLINTE,TDCATDOC"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_CAUPFI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDFLINTE,TDCATDOC;
              from (i_cTable) where;
                  TDTIPDOC = this.w_CAUPFI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            this.w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT (this.w_CATDOC="DI" AND (this.w_FLINTE="N" OR this.w_FLINTE=this.w_MVFLINTE))
            this.w_MESS = Ah_MsgFormat("Causale documento articoli composti incongruente")
            this.w_OK = .F.
          endif
        endif
      endif
    endif
    if Not this.w_OK and not isalt() and ! lower(this.oparentObject.class)=="tgsva_bim"
      * --- In alcuni casi, vedi UNIVOC, OK potrebbe non essere .T. ma il messaggio � vuoto
      *     In questo caso devo bloccare la transazione e per fare questo devo riempire w_MESS
      this.w_MESS = Ah_MsgFormat("Sincronizzazione abbandonata%0%1%0%2", Alltrim(this.w_MESS), Alltrim(cp_GetGlobalVar("g_MSG")))
    else
      if this.w_OK And (Len(cp_GetGlobalVar("g_MSG")) >Len(this.w_OLDMSG) Or Not Empty(this.w_MESS))
        * --- Messaggstica di errore non bloccante. Probabili YesNo confermati in automatico per lo SCHEDULER
        *     Stampo solo l ultima parte del messagio
        this.w_MESS = Ah_MsgFormat("Riscontrati errori non bloccanti%0%1%0%2", Alltrim(this.w_MESS), Alltrim(right(cp_GetGlobalVar("g_MSG"), len(cp_GetGlobalVar("g_MSG"))-len(this.w_OLDMSG))))
      endif
    endif
    if Not Empty(this.w_MESS)
      * --- Aggiungo gli estremi del documento
      if NOT EMPTY(this.w_MVCODCON) AND this.pAzione="I"
        if lower(this.oparentObject.class)=="tgsar_bgd"
          this.w_oPART = this.w_oMess.AddMsgPartNL("Documento %1 n.: %2%3 del %4 intestato a %5 gi� presente")
        else
          this.w_oPART = this.w_oMess.AddMsgPartNL("Documento %1 n.: %2%3 del %4 intestato a %5")
        endif
      else
        if ! lower(this.oparentObject.class)=="tgsva_bim"
          this.w_oPART = this.w_oMess.AddMsgPartNL("Documento %1 n.: %2%3 del %4 (chiave %5)")
        endif
      endif
      this.w_oPART.AddParam(Alltrim(this.w_MVTIPDOC))     
      this.w_oPART.AddParam(Alltrim(Str(this.w_MVNUMDOC,15,0)))     
      this.w_oPART.AddParam(IIF(Not Empty(this.w_MVALFDOC),"/"+Alltrim(this.w_MVALFDOC),""))     
      this.w_oPART.AddParam(DTOC(Cp_Todate(this.w_MVDATDOC)))     
      if NOT EMPTY(this.w_MVCODCON) AND this.pAzione="I"
        this.w_oPART.AddParam(Alltrim(this.w_MVCODCON))     
      else
        this.w_oPART.AddParam(this.w_Serial)     
      endif
      this.w_oPART = this.w_oMess.AddMsgPartNL("%1")
      this.w_oPART.AddParam(this.w_MESS)     
      this.w_MESS = this.w_oMess.ComposeMessage()
    endif
    * --- Ripasso per riferimento il buon fine della transazione
    *     devo usare la macro poich� essendo un batch il painter mette il this. davanti alla
    *     propriet� non andando a modificare la vera variabile passata per riferimento ma solo
    *     la propriet� this.pTrsOk di pag1
     
 L_Macro = "pTrsOk = this.w_OK" 
 &L_Macro
    if this.w_BNOSCHED
      * --- Disattivo lo schedulatore se non presente..
      cp_SetGlobalVar("g_SCHEDULER","N")
    endif
    i_retcode = 'stop'
    i_retval = this.w_MESS
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ciclo sui dettaglio documenti...
    * --- La riga � valida...
    if this.w_RIGAPIENA
      this.w_ROWNUM = this.w_MySelf.Get("w_CPROWNUM")
      this.w_ROWRIF = this.w_MySelf.Get("w_MVNUMRIF")
      this.w_TRIG = this.w_TRIG + IIF( this.w_FLDEL , 0, 1)
      this.w_MVTIPRIG = NVL(this.w_MySelf.Get("w_MVTIPRIG")," ")
      this.w_TIPRIG = this.w_MVTIPRIG
      this.w_NR = ALLTRIM(STR(this.w_MySelf.Get("w_CPROWORD")))
      this.w_MVCESSER = NVL(this.w_MySelf.Get("w_MVCESSER"),Space(10))
      this.w_MVCODCEN = NVL(this.w_MySelf.Get("w_MVCODCEN"),Space(15))
      this.w_MVVOCCEN = NVL(this.w_MySelf.Get("w_MVVOCCEN"),Space(15))
      this.w_MVCODCOM = NVL(this.w_MySelf.Get("w_MVCODCOM"),Space(15))
      this.w_MVCODATT = NVL(this.w_MySelf.Get("w_MVCODATT"),Space(15))
      this.w_MVFLNOAN = NVL(this.w_MySelf.Get("w_MVFLNOAN"),Space(15))
      this.w_KEYSAL = NVL(this.w_MySelf.Get("w_MVKEYSAL"), Space(20))
      this.w_CODMAG = NVL(this.w_MySelf.Get("w_MVCODMAG"), Space(5))
      this.w_MVFLCASC = NVL(this.w_MySelf.Get("w_MVFLCASC"), " ")
      this.w_MVF2CASC = NVL(this.w_MySelf.Get("w_MVF2CASC"), " ")
      this.w_MVFLRISE = NVL(this.w_MySelf.Get("w_MVFLRISE"), " ")
      this.w_MVF2RISE = NVL(this.w_MySelf.Get("w_MVF2RISE"), " ")
      this.w_MVFLORDI = NVL(this.w_MySelf.Get("w_MVFLORDI"), " ")
      this.w_MVF2ORDI = NVL(this.w_MySelf.Get("w_MVF2ORDI"), " ")
      this.w_MVFLIMPE = NVL(this.w_MySelf.Get("w_MVFLIMPE"), " ")
      this.w_MVF2IMPE = NVL(this.w_MySelf.Get("w_MVF2IMPE"), " ")
      this.w_AGGSAL = ALLTRIM(this.w_MVFLCASC+this.w_MVFLRISE+this.w_MVFLORDI+this.w_MVFLIMPE)
      this.w_AGGSAL1 = ALLTRIM(this.w_MVF2CASC+this.w_MVF2RISE+this.w_MVF2ORDI+this.w_MVF2IMPE)
      this.w_CAUCOL = NVL(this.w_MySelf.Get("w_MVCAUCOL"), Space(5))
      this.w_CODMAT = NVL(this.w_MySelf.Get("w_MVCODMAT"), Space(5))
      this.w_MVSERRIF = NVL(this.w_MySelf.Get("w_MVSERRIF"), Space(10))
      this.w_MVROWRIF = NVL(this.w_MySelf.Get("w_MVROWRIF"), 0 )
      this.w_MVFLELGM = NVL(this.w_MySelf.Get("w_MVFLELGM"), " ")
      this.w_MVDATGEN = NVL(this.w_MySelf.Get("w_MVDATGEN"), cp_CharToDate("  -  -  "))
      this.w_MVQTAEV1 = NVL(this.w_MySelf.Get("w_MVQTAEV1"), 0)
      this.w_MVQTAUM1 = NVL(this.w_MySelf.Get("w_MVQTAUM1"), 0)
      this.w_MVIMPEVA = NVL(this.w_MySelf.Get("w_MVIMPEVA"), 0)
      this.w_MVFLEVAS = NVL(this.w_MySelf.Get("w_MVFLEVAS"), " ")
      this.w_MVUNIMIS = NVL(this.w_MySelf.Get("w_MVUNIMIS"), " ")
      this.w_QTAUM1 = this.w_MVQTAUM1
      this.w_QTAEV1 = this.w_MVQTAEV1
      this.w_IMPEVA = this.w_MVIMPEVA
      this.w_FLELGM = this.w_MVFLELGM
      this.w_SERRIF = this.w_MVSERRIF
      this.w_STSERRIF = this.w_MVSERRIF
      this.w_FLEVAS = this.w_MVFLEVAS
      * --- Nel caso di generazione documenti considero sempre le righe in append
      this.w_SRV = iif( this.w_FLDEL , " ", IIF(this.w_DaGest,this.w_PADRE.RowStatus(), "A" ))
      * --- Se da gestione controllo se modificata riga.
      *     Da generazione documento non c'� la possibilit�
      if this.w_DaGest
        this.w_OFLDIF = NVL(this.w_MySelf.Get("OFLDIF"), Space(36))
        this.w_FLDIF = this.w_MVCODICE+this.w_MVUNIMIS+STR(w_MVQTAMOV,12,3)+this.w_MVFLOMAG<>this.w_OFLDIF
      else
        this.w_OFLDIF = ""
        this.w_FLDIF = .F.
      endif
      this.w_MVCODART = NVL(this.w_MySelf.Get("w_MVCODART"), Space(20))
      this.w_CODART = this.w_MVCODART
      * --- Controlli in cancellazione
      if this.w_OK And this.w_FLDEL
        if this.w_OK AND g_CESP="S" And Not Empty(this.w_MVCESSER)
          this.w_MESS = ah_Msgformat("Riga movimento: %1 associata a movimento cespite", ALLTRIM(STR(this.w_CPROWORD)) )
          this.w_OK = .F.
        endif
        if this.w_OK
          if this.w_MVCLADOC $ "FA-NC" And this.w_FLDEL And this.w_MVTIPRIG="R" And this.w_MVFLELGM="S" And this.w_MVFLPROV="N"
            * --- Read from ADDE_SPE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2],.t.,this.ADDE_SPE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "RLSERFAT"+;
                " from "+i_cTable+" ADDE_SPE where ";
                    +"RLSERDOC = "+cp_ToStrODBC(this.w_MVSERIAL);
                    +" and RLNUMDOC = "+cp_ToStrODBC(this.w_ROWNUM);
                    +" and RLROWDOC = "+cp_ToStrODBC(this.w_ROWRIF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                RLSERFAT;
                from (i_cTable) where;
                    RLSERDOC = this.w_MVSERIAL;
                    and RLNUMDOC = this.w_ROWNUM;
                    and RLROWDOC = this.w_ROWRIF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SERFAT = NVL(cp_ToDate(_read_.RLSERFAT),cp_NullValue(_read_.RLSERFAT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if Not Empty( this.w_SERFAT )
              this.w_MESS = ah_Msgformat("Impossibile eliminare: la riga %1 � oggetto di ripartizione spese%0Annullare la ripartizione spese da servizi / ripartizione fattura di spese", this.w_NR)
              this.w_OK = .F.
            endif
            this.w_SERFAT = Space(10)
          endif
        endif
      endif
      if this.w_OK and this.w_CFUNC<>"Query"
        * --- Vengono memorizzati in un Array i Codici magazzino gi� controllati
        *     in modo che non vengano controllati due volte.
        *     Questa operazione viene eseguita per problemi di lentezza 
        *     nel caso di documenti con molte righe
        if (Not Empty(this.w_CODMAG) Or not Empty(this.w_CODMAT)) And this.w_FLELGM ="S"
          if ASCAN(ArrGiom, Left(Alltrim(this.w_CODMAG)+Space(5),5) )=0 
            if NOT EMPTY(this.w_CODMAG) AND ! CHKGIOM(this.w_MVCODESE,this.w_CODMAG,this.w_MVDATREG,this.w_FLELGM)
              this.w_MESS = ah_Msgformat("Registrazione gi� stampata sul giornale magazzino o blocco stampa attivo; impossibile variare/cancellare")
              this.w_OK = .F.
            endif
            if Not Empty(this.w_CODMAG)
               
 DECLARE ArrGiom(ALEN(ArrGiom)+IIF(this.w_NUMMAG=0,0,1)) 
 ArrGiom(Alen(ArrGiom)) = Left(Alltrim(this.w_CODMAG)+Space(5),5)
              this.w_NUMMAG = 1
            endif
          endif
          if ASCAN(ArrGiom, Left(Alltrim(this.w_CODMAT)+Space(5),5) )=0 
            if ! EMPTY(this.w_CODMAT) AND ! CHKGIOM(this.w_MVCODESE,this.w_CODMAT,this.w_MVDATREG,this.w_FLELGM)
              this.w_MESS = ah_Msgformat("Registrazione gi� stampata sul giornale magazzino o blocco stampa attivo; impossibile variare/cancellare")
              this.w_OK = .F.
            endif
            if Not Empty(this.w_CODMAT)
               
 DECLARE ArrGiom(ALEN(ArrGiom)+IIF(this.w_NUMMAG=0,0,1)) 
 ArrGiom(Alen(ArrGiom)) = Left(Alltrim(this.w_CODMAT)+Space(5),5)
              this.w_NUMMAG = 1
            endif
          endif
        endif
      endif
      * --- I controlli sull'evasione sono svolti sempre
      if this.w_OK AND Not Empty(this.w_MVUNIMIS) and this.w_CFUNC="Load" and !this.w_DaGest AND this.w_OLDUNIMIS<>this.w_MVUNIMIS
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMDESCRI"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.w_MVUNIMIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMDESCRI;
            from (i_cTable) where;
                UMCODICE = this.w_MVUNIMIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESUM = NVL(cp_ToDate(_read_.UMDESCRI),cp_NullValue(_read_.UMDESCRI))
          use
          if i_Rows=0
            this.w_OK = .F.
            this.w_MESS = ah_Msgformat("Unit� di misura: %1 non presente", this.w_MVUNIMIS)
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if this.w_OK AND this.w_CFUNC<>"Load"
        * --- Se sono in Variazione o Cancellazione Verifico se documento e' Evaso
        *     Se si Inibisco la Registrazione (Unica Eccezione i Documenti Interni)
        this.w_CHECKEVA = .T.
        this.w_TESTFLARIF = ""
        * --- Verifico se esiste ancora un documento che evade questa riga
        *     Anche se questa riga � evasa, se il documento di evasione non esiste pi� perch�
        *     cancellato dalla sincronizzazione precedentemente, non devo dare nessun avviso e cancellare o modificare
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVSERIAL,MVFLARIF"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERRIF = "+cp_ToStrODBC(this.w_MVSERIAL);
                +" and MVROWRIF = "+cp_ToStrODBC(this.w_ROWNUM);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_ROWRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVSERIAL,MVFLARIF;
            from (i_cTable) where;
                MVSERRIF = this.w_MVSERIAL;
                and MVROWRIF = this.w_ROWNUM;
                and MVNUMRIF = this.w_ROWRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERTEST = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
          this.w_TESTFLARIF = NVL(cp_ToDate(_read_.MVFLARIF),cp_NullValue(_read_.MVFLARIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows=0
          * --- Controllo anche tra le fatture raggruppate
          * --- Read from RAG_FATT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.RAG_FATT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RAG_FATT_idx,2],.t.,this.RAG_FATT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "FRSERFAT,FRROWFAT,FRNUMFAT"+;
              " from "+i_cTable+" RAG_FATT where ";
                  +"FRSERDDT = "+cp_ToStrODBC(this.w_MVSERIAL);
                  +" and FRROWDDT = "+cp_ToStrODBC(this.w_ROWNUM);
                  +" and FRNUMDDT = "+cp_ToStrODBC(this.w_ROWRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              FRSERFAT,FRROWFAT,FRNUMFAT;
              from (i_cTable) where;
                  FRSERDDT = this.w_MVSERIAL;
                  and FRROWDDT = this.w_ROWNUM;
                  and FRNUMDDT = this.w_ROWRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERTEST = NVL(cp_ToDate(_read_.FRSERFAT),cp_NullValue(_read_.FRSERFAT))
            this.w_ROWTEST = NVL(cp_ToDate(_read_.FRROWFAT),cp_NullValue(_read_.FRROWFAT))
            this.w_RIFTEST = NVL(cp_ToDate(_read_.FRNUMFAT),cp_NullValue(_read_.FRNUMFAT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows<>0
            * --- Read from DOC_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVFLARIF"+;
                " from "+i_cTable+" DOC_DETT where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SERTEST);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWTEST);
                    +" and MVNUMRIF = "+cp_ToStrODBC(this.w_RIFTEST);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVFLARIF;
                from (i_cTable) where;
                    MVSERIAL = this.w_SERTEST;
                    and CPROWNUM = this.w_ROWTEST;
                    and MVNUMRIF = this.w_RIFTEST;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TESTFLARIF = NVL(cp_ToDate(_read_.MVFLARIF),cp_NullValue(_read_.MVFLARIF))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
        * --- Documento di evasione esistente se non vuoto w_TESTFLARIF
        this.w_CHECKEVA = Not Empty(this.w_TESTFLARIF)
        * --- Solo se esiste ancora il documento di evasione eseguo i controlli.
        *     Il documento di evasione potrebbe non esistere gi� pi� perch� � stato cancellato
        *     precedentemente nella sincronizzazione la quale non ha 'liberato' il documento evaso
        if this.w_CHECKEVA
          if this.w_FLGEFA="S" AND NOT EMPTY(NVL(this.w_MVDATGEN,cp_CharToDate("  -  -  "))) And this.w_MVTIPRIG<>"D"
            if this.w_CFUNC="Query"
              this.w_MESS = ah_Msgformat("Il documento ha generato una fattura differita%0Impossibile eliminare")
            else
              this.w_MESS = ah_Msgformat("Il documento ha generato una fattura differita%0Impossibile variare")
            endif
            this.w_OK = .F.
          else
            if this.w_MVCLADOC $ "OR-DI"
              * --- Se sono in Variazione o Cancellazione Verifico se documento e' Evaso. Se SI Inibisco la Registrazione
              do case
                case this.w_TIPRIG $ "R-M" AND ( (this.w_SRV="U" AND ABS(this.w_QTAEV1)>ABS(this.w_QTAUM1)) OR (this.w_FLDEL AND this.w_QTAEV1<>0) )
                  if this.w_CFUNC="Query" or (this.w_FLDEL and this.w_QTAEV1>0 ) 
                    this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile eliminare una riga parzialmente/totalmente evasa", this.w_NR)
                  else
                    this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile impostare una quantit� inferiore a quella gi� evasa", this.w_NR)
                  endif
                  this.w_OK = .F.
                case this.w_TIPRIG="F" AND (this.w_FLDEL AND this.w_IMPEVA<>0)
                  * --- Quantita' Evasa o Importo evaso se riga Forfettaria
                  if this.w_CFUNC="Query" or (this.w_FLDEL and this.w_IMPEVA>0)
                    this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile eliminare una riga parzialmente/totalmente evasa", this.w_NR)
                  else
                    this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile impostare un valore inferiore a quello gi� evaso", this.w_NR)
                  endif
                  this.w_OK = .F.
              endcase
            else
              do case
                case this.w_FLGEFA="B" AND this.w_CFUNC<>"Query"
                  * --- Controllo Variazione Riga Fattura Differita generata da P.Fatturazione in Variazione
                  do case
                    case this.w_FLDEL AND this.w_SRV<>"A" AND (this.w_TIPRIG="R" OR (this.w_TIPRIG $ "FM" AND NOT EMPTY(this.w_SERRIF)))
                      * --- Fattura Differita; Impossibile Eliminare Righe (tranne le Righe Descrittive o aggiunte)
                      this.w_MESS = ah_Msgformat("Riga movimento: %1%0Impossibile eliminare una riga (non descrittiva)%0da un documento di tipo fattura differita",this.w_NR)
                      this.w_OK = .F.
                    case this.w_SRV="A" AND Not this.w_FLDEL AND this.w_TIPRIG="R"
                      * --- Fattura Differita; Consentito inserire solo Righe Servizi
                      this.w_MESS = ah_Msgformat("Riga movimento: %1%0Impossibile aggiungere nuove righe articoli%0ad un documento di tipo fattura differita",this.w_NR)
                      this.w_OK = .F.
                    case this.w_SRV="U" AND Not this.w_FLDEL AND this.w_FLDIF
                      * --- Fattura Differita; Impossibile Variare Articolo, UM, Qta.
                      this.w_MESS = ah_Msgformat("Riga movimento: %1%0Impossibile modificare codice articolo o U.M. o quantit� o tipo riga%0ad un documento di tipo fattura differita",this.w_NR)
                      this.w_OK = .F.
                  endcase
                case (this.w_TIPRIG<>"F" AND this.w_QTAEV1<>0) OR (this.w_TIPRIG="F" AND this.w_IMPEVA<>0)
                  * --- Quantita' Evasa o Importo evaso se riga Forfettaria
                  if this.w_CFUNC="Query"
                    this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile eliminare", this.w_NR)
                  else
                    this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile variare", this.w_NR)
                  endif
                  this.w_OK = .F.
              endcase
            endif
          endif
          if this.w_OK AND this.w_TIPRIG="D" AND this.w_FLEVAS="S" 
            * --- Read from DOC_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVSERIAL"+;
                " from "+i_cTable+" DOC_DETT where ";
                    +"MVSERRIF = "+cp_ToStrODBC(this.w_MVSERIAL);
                    +" and MVROWRIF = "+cp_ToStrODBC(this.w_ROWNUM);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVSERIAL;
                from (i_cTable) where;
                    MVSERRIF = this.w_MVSERIAL;
                    and MVROWRIF = this.w_ROWNUM;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SERRDESC = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Controllo Righe Descrittive Evase da Altri Documenti
            if Not Empty(this.w_SERRDESC)
              if this.w_CFUNC="Query"
                this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile eliminare", this.w_NR)
              else
                this.w_MESS = ah_Msgformat("Riga movimento: %1 evasa da altri documenti%0%0Impossibile variare", this.w_NR)
              endif
              this.w_OK = .F.
            endif
          endif
        endif
      endif
      if this.w_OK AND (Not Empty(this.w_CODMAG) or Not Empty(this.w_CODMAT) )
        this.w_MESS = CHKCONS("M",this.w_MVDATREG,"B","N")
        this.w_OK = Empty( this.w_MESS )
      endif
      this.w_OLDUNIMIS = this.w_MVUNIMIS
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico la presenza delle matricole!
    if this.w_CFUNC<>"Query" And g_MATR="S" And this.w_MVDATREG>=nvl(g_DATMAT,cp_CharToDate("  /  /    "))
      if this.w_OK 
        * --- Verifico congruenza numero matricole con qt� di riga (SE NON IN FASE DI CORR. ERORRI LOTTI / UBICAZIONI)
        if NOT this.w_TESLOT
          * --- Select from GSVE_BMT
          do vq_exec with 'GSVE_BMT',this,'_Curs_GSVE_BMT','',.f.,.t.
          if used('_Curs_GSVE_BMT')
            select _Curs_GSVE_BMT
            locate for 1=1
            do while not(eof())
            this.w_OK = lower(this.oparentObject.class)=="tgsar_bgd"
            if NOT EMPTY(NVL(_Curs_GSVE_BMT.DOMAGSCA," "))
              * --- Se si tratta di un trasferimento determino il magazzino di carico\scarico corretto del documento
              this.w_DMAGCAR = Nvl(_Curs_GSVE_BMT.DOMAGCAR,Space(5) )
              this.w_DMAGSCA = Nvl(_Curs_GSVE_BMT.DOMAGSCA,Space(5) )
            else
              this.w_DMAGCAR = NVL(_Curs_GSVE_BMT.DOMAGCAR,Space(5))
              this.w_DMAGSCA = NVL(_Curs_GSVE_BMT.DOMAGCAR,Space(5))
            endif
            do case
              case Nvl(_Curs_GSVE_BMT.qtaum1,0)<>Nvl(_Curs_GSVE_BMT.qtamat,0)
                if cp_Round(Nvl(qtaum1,0),0)<>Nvl(qtaum1,0)
                  this.w_MESS = ah_Msgformat("Riga %1 impossibile movimentare articoli gestiti a matricole per quantit� frazionabili", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ) )
                else
                  if lower(this.oparentObject.class)<>"tgsar_bgd"
                    this.w_MESS = ah_Msgformat("Riga %1 con numero matricole incongruente. Qt� riga: %2, matricole movimentate: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ), Alltrim( Str( _Curs_GSVE_BMT.QTAUM1 ) ), Alltrim( Str( _Curs_GSVE_BMT.QTAMAT ) ) )
                  endif
                endif
              case ALLTRIM(Nvl(_Curs_GSVE_BMT.Mvcodice,Space(20)))<>ALLTRIM(Nvl(_Curs_GSVE_BMT.Mtkeysal,Space(20)))
                if lower(this.oparentObject.class)=="tgsar_bgd"
                  this.oParentObject.w_PADRE.AddLogMsg("W", ah_Msgformat("Riga %1 con articolo incongruente. Articolo documento: %2, articolo matricole: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ), Alltrim( _Curs_GSVE_BMT.MVCODICE ), Alltrim( _Curs_GSVE_BMT.MTKEYSAL ) ) )
                else
                  this.w_MESS = ah_Msgformat("Riga %1 con articolo incongruente. Articolo documento: %2, articolo matricole: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ), Alltrim( _Curs_GSVE_BMT.MVCODICE ), Alltrim( _Curs_GSVE_BMT.MTKEYSAL ) )
                endif
              case Nvl(_Curs_GSVE_BMT.Mvcodlot,Space(20)) <> Nvl(_Curs_GSVE_BMT.Mtcodlot,Space(20)) AND g_PERLOT="S"
                if lower(this.oparentObject.class)=="tgsar_bgd"
                  this.oParentObject.w_PADRE.AddLogMsg("W", ah_Msgformat("Riga %1 con lotto incongruente. Lotto documento: %2, lotto matricole: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ), Alltrim( NVL(_Curs_GSVE_BMT.MVCODLOT,Space(20)) ), Alltrim( NVL(_Curs_GSVE_BMT.MTCODLOT,Space(20)) ) ) )
                else
                  this.w_MESS = ah_Msgformat("Riga %1 con lotto incongruente. Lotto documento: %2, lotto matricole: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ), Alltrim( NVL(_Curs_GSVE_BMT.MVCODLOT,Space(20)) ), Alltrim( NVL(_Curs_GSVE_BMT.MTCODLOT,Space(20)) ) )
                endif
              case ALLTRIM(Nvl(_Curs_GSVE_BMT.Mtmagpri,Space(5)))<>Alltrim(this.w_DMAGCAR)
                if lower(this.oparentObject.class)=="tgsar_bgd"
                  this.oParentObject.w_PADRE.AddLogMsg("W", ah_Msgformat("Riga %1 con magazzino di carico incongruente. Magazzino documento: %2, magazzino matricole: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ), Alltrim(this.w_DMAGCAR), Alltrim( NVL(_Curs_GSVE_BMT.MTMAGPRI,Space(5))) ) )
                else
                  this.w_MESS = ah_Msgformat("Riga %1 con magazzino di carico incongruente. Magazzino documento: %2, magazzino matricole: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ), Alltrim(this.w_DMAGCAR), Alltrim( NVL(_Curs_GSVE_BMT.MTMAGPRI,Space(5))) )
                endif
              case Nvl(_Curs_GSVE_BMT.Mvcodubi,Space(20))<>Nvl(_Curs_GSVE_BMT.Mtcodubi,Space(20)) AND g_PERUBI="S"
                if lower(this.oparentObject.class)=="tgsar_bgd"
                  this.oParentObject.w_PADRE.AddLogMsg("W", ah_Msgformat("Riga %1 con ubicazione incongruente. Ubicazione documento: %2, ubicazione matricole: %3", Alltrim(Str(_Curs_GSVE_BMT.ROWORD ) ), Alltrim( NVL(_Curs_GSVE_BMT.MVCODUBI,Space(20)) ), Alltrim(NVL(_Curs_GSVE_BMT.MTCODUBI,Space(20)) )) )
                else
                  this.w_MESS = ah_Msgformat("Riga %1 con ubicazione incongruente. Ubicazione documento: %2, ubicazione matricole: %3", Alltrim(Str(_Curs_GSVE_BMT.ROWORD ) ), Alltrim( NVL(_Curs_GSVE_BMT.MVCODUBI,Space(20)) ), Alltrim(NVL(_Curs_GSVE_BMT.MTCODUBI,Space(20)) ))
                endif
              case ALLTRIM(Nvl(Mtmagsca,Space(5)))<>Alltrim(this.w_DMAGSCA) and Not Empty(this.w_DMAGSCA)
                if lower(this.oparentObject.class)=="tgsar_bgd"
                  this.oParentObject.w_PADRE.AddLogMsg("W", ah_Msgformat("Riga %1 con magazzino di scarico incongruente. Magazzino documento: %2, magazzino matricole: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ) , Alltrim(this.w_DMAGSCA), Alltrim( NVL(_Curs_GSVE_BMT.MTMAGSCA,Space(5))) ) )
                else
                  this.w_MESS = ah_Msgformat("Riga %1 con magazzino di scarico incongruente. Magazzino documento: %2, magazzino matricole: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ) , Alltrim(this.w_DMAGSCA), Alltrim( NVL(_Curs_GSVE_BMT.MTMAGSCA,Space(5))) )
                endif
            endcase
            Exit
              select _Curs_GSVE_BMT
              continue
            enddo
            use
          endif
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pAzione,pArrKey,pParent,pTrsOk)
    this.pAzione=pAzione
    this.pArrKey=pArrKey
    this.pParent=pParent
    this.pTrsOk=pTrsOk
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='ADDE_SPE'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='CAM_AGAZ'
    this.cWorkTables[4]='CAU_CONT'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='DOC_MAST'
    this.cWorkTables[7]='DOC_RATE'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='INCDCORR'
    this.cWorkTables[10]='TIP_DOCU'
    this.cWorkTables[11]='RAG_FATT'
    this.cWorkTables[12]='DOC_DETT'
    this.cWorkTables[13]='UNIMIS'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_gslr_qce')
      use in _Curs_gslr_qce
    endif
    if used('_Curs_INCDCORR')
      use in _Curs_INCDCORR
    endif
    if used('_Curs_GSVE_BMT')
      use in _Curs_GSVE_BMT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsar_bad
  * Il batch gestisce sia il transitorio dei documenti
  * che un transitorio costruito da batch per la generazione
  * documenti (Es. GSVE_BFD)
  * Definiti quindi metodi propri di questo batch che in base
  * al parametro lanciano metodi sul transitorio della gestione
  * o su GENEAPP (cursore creato dalle generazioni)
  
  Procedure Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
  LOCAL cName_1,cName_2,cCmdSql,cOlderr,cMsgErr
   If This.w_PARAM='D'
  	* Assegnamenti..
  	cOlderr=ON('error')
  	cMsgErr=''
  	On error cMsgErr=Message(1)+': '+MESSAGE()
  	
  	cTmp=   IIF( EMPTY(cTmp)   , '__Tmp__', cTmp )
  	cFields=IIF( EMPTY(cFields), '*'      , cFields )
  	cWhere= IIF( EMPTY(cWhere) , '1=1'    , cWhere )	
  	cOrder= IIF( EMPTY(cOrder) , '1'      , cOrder )	
  	
  	cOrder=IIF( EMPTY(cGroupBy),'',' GROUP BY '+(cGroupBy)+IIF( EMPTY(cHaving),'',' HAVING '+(cHaving) ) )+' Order By '+cOrder
  	
  	cName_1='GENEAPP'
  	
  	cCmdSql='Select '+cFields+' From '+cName_1			
  	cCmdSql=cCmdSql+' Where '
  	
  	cCmdSql=cCmdSql+cWhere+' &cOrder Into Cursor '+cTmp+' Nofilter '
  	* Eseguo la frase..
  	&cCmdSql
  
  	* Ripristino la vecchia gestione errori
  	ON ERROR &cOldErr
  
  	* Se Qualcosa va storto lo segnalo ed esco...
  	  IF NOT EMPTY(cMsgErr)
        ah_ErrorMsg(cMsgErr,'stop','')
        * memorizzo nella clipboard la frase generata in caso di errore
        _ClipText=cCmdSql
      ELSE
  		  * mi posiziono sul temporaneo al primo record..
  		  SELECT (cTmp)
  		  GO Top
      ENDIF  
   Else
    This.w_PADRE.Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
   Endif
  Endproc
  
  PROCEDURE SetRow(id_row) 
  LOCAL cOldArea
   if This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se paramento valorizzato mi posiziono sulla riga passata 
  	* altrimenti sulla riga attuale...
  	IF TYPE( 'id_row' )='N'
  		SELECT( 'GENEAPP' )
  		goto (id_row) 	
  	EndIf
  		
  	* ripristino la vecchia area
  	SELECT(coldArea)	  
   else
  	 this.w_PADRE.SetRow( id_Row )
   endif
  
  Endproc
  
  Procedure FirstRow()
   LOCAL cOldArea
   If This.w_PARAM='D'	
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
    Select GENEAPP
    Go Top
  	* ripristino la vecchia area
  	SELECT(coldArea)  
   else
    this.w_PADRE.FirstRow()
   endif
  EndProc
  
  Function Eof_Trs() 
   if This.w_PARAM='D'
    RETURN (EOF('GENEAPP'))
   else
  	 RETURN (this.w_PADRE.Eof_Trs())
   endif
  ENDFUNC
  
  
  FUNCTION GET(Item)
  LOCAL cOldArea,cFldName
   if This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Nel caso di GENEAPP i campi del cursore non hanno la t_ davanti
  	* quindi prendo il nome della colonne che ha lo stesso nome della variabile senza w_ davanti
  	IF lower(LEFT(Item,2))='w_' 
  		cFldName=SUBSTR(Item,3,LEN(Item))
  	ELSE
  		cFldName=Item
  	ENDIF
  
  	SELECT( 'GENEAPP' )
  	* Leggo il campo...
  	Result=eval(cFldName)
  	
  	* ripristino la vecchia area
  	SELECT(coldArea)	
  	
  	* Restituisco il valore recuperato..
  	Return(Result)
     
   else
    Return( This.w_PADRE.Get(Item) )
   Endif
  EndFunc
  
  Procedure NextRow()
  LOCAL cOldArea
   If This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
   
    Select 'GENEAPP'
    Skip
    
  	* ripristino la vecchia area
  	SELECT(coldArea)  
    
   Else
    This.w_PADRE.NextRow()
   Endif
  	
  EndProc
  
  Function Search(Criterio,StartFrom)
  LOCAL cOldArea,cName_1, cCursName,nResult,cCond
   If This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	cName_1='GENEAPP'
  	cCursName=SYS(2015)	
  	* Se ricerca a partire da un certo record aggiungo la condizione..
  	IF TYPE('StartFrom')='N' AND StartFrom<>0
  	cCond=' RECNO(cName_1)>'+ALLTRIM(STR(StartFrom))
  	ELSE
  	cCond=' 1=1 '
  	Endif
  		SELECT MIN(RECNO()) As Riga FROM (cName_1) WHERE  &cCond AND  		&Criterio INTO CURSOR &cCursName
  	* leggo il risultato
  	SELECT(cCursName)
  	GO Top
  	nResult=NVL(&cCursName..Riga,-1) 
  	nResult=IIF(nResult=0,-1, nResult)
  	* rimuovo il cursore di appoggio
      if used(cCursName)
        select (cCursName)
        use
      ENDIF    
  	* ripristino la vecchia area
  	SELECT(coldArea)
  	* restituisco il risultato
  	RETURN nResult
    
   Else
    Return(This.w_PADRE.Search(Criterio,StartFrom))
   Endif
  
  EndFunc
  
  
  Procedure SET(cItem,vValue,bNoUpd)
  LOCAL cOldArea,cFldName,bResult
   If This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Nel caso di GENEAPP i campi non hanno t_ davanti 
  	* quindi cerco semplicemente il campo con il nome della variabile senza w_
  	IF lower(LEFT(cItem,2))='w_' 
  		cFldName=SUBSTR(cItem,3,LEN(cItem))
  	ELSE
  		cFldName=cItem
  	ENDIF
  
  	* Aggiorno il transitorio normale...
  	SELECT( 'GENEAPP' )
  		
  	* Se cambio allora svolgo la Replace..
  	IF &cFldName<>vValue
  		Replace &cFldName WITH vValue
  	endif			
  	* ripristino la vecchia area
  	SELECT(coldArea) 
   Else
    This.w_PADRE.Set(cItem,vValue,bNoUpd)
   Endif
  EndProc
  
  FUNCTION GetType(cFieldsName)
   If This.w_PARAM='D'
   	LOCAL cOldArea,cFldName
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se le prima due lettere sono w_ le sostituisco con t_
  	* altrimenti lascio quanto trovo...
  	IF lower(LEFT(cFieldsName,2))='w_' 
  		cFldName='t_'+SUBSTR(cFieldsName,3,LEN(cFieldsName))
  	ELSE
  		cFldName=cFieldsName
  	ENDIF	
  	
  		SELECT( 'GENEAPP'  )
  			
  	* Leggo il campo...
  	Result=Type(cFldName)
  				
  	* ripristino la vecchia area
  	SELECT(coldArea)		
  	Return( Result )
   ELSE
   Return(This.w_PADRE.GetType( cFieldsName ))
   endif
  EndFunc
  
  Procedure Done()
   *Azzerro la variabile di comodo, se non lo facessi rimarebbe la classe
   *del batch appesa in memoria, impedendo tra l'altro la compilazione
   *all'interno di ad hoc
   this.w_MYSELF=.Null.
   DoDefault()
  EndpRoc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione,pArrKey,pParent,pTrsOk"
endproc
