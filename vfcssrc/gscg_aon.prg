* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_aon                                                        *
*              Modello pagamento F24/2006 accise                               *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-14                                                      *
* Last revis.: 2009-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_aon"))

* --- Class definition
define class tgscg_aon as StdForm
  Top    = 3
  Left   = 10

  * --- Standard Properties
  Width  = 808
  Height = 413+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-02"
  HelpContextID=91609751
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=150

  * --- Constant Properties
  MOD_PAG_IDX = 0
  TRI_BUTI_IDX = 0
  AZIENDA_IDX = 0
  TITOLARI_IDX = 0
  COD_TRIB_IDX = 0
  CODI_UFF_IDX = 0
  SED_INPS_IDX = 0
  CAU_INPS_IDX = 0
  REG_PROV_IDX = 0
  ENTI_LOC_IDX = 0
  SE_INAIL_IDX = 0
  COD_PREV_IDX = 0
  SED_AEN_IDX = 0
  CAU_AEN_IDX = 0
  MODVPAG_IDX = 0
  MODCPAG_IDX = 0
  MODEPAG_IDX = 0
  VALUTE_IDX = 0
  CPUSERS_IDX = 0
  MODIPAG_IDX = 0
  SEDIAZIE_IDX = 0
  cFile = "MOD_PAG"
  cKeySelect = "MFSERIAL"
  cKeyWhere  = "MFSERIAL=this.w_MFSERIAL"
  cKeyWhereODBC = '"MFSERIAL="+cp_ToStrODBC(this.w_MFSERIAL)';

  cKeyWhereODBCqualified = '"MOD_PAG.MFSERIAL="+cp_ToStrODBC(this.w_MFSERIAL)';

  cPrg = "gscg_aon"
  cComment = "Modello pagamento F24/2006 accise"
  icon = "anag.ico"
  cAutoZoom = 'GSCG_AFN'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MFSERIAL = space(10)
  w_MFMESRIF = space(2)
  w_MFANNRIF = space(4)
  w_MFVALUTA = space(3)
  w_APPCOIN = space(1)
  o_APPCOIN = space(1)
  w_MF_COINC = space(1)
  w_MFSERIAL = space(10)
  w_APPOIMP = 0
  w_MFCODUFF = space(3)
  w_MFCODATT = space(11)
  w_MESE = space(2)
  w_ANNO = space(4)
  w_GENERA = space(10)
  w_IMPORTA = .F.
  w_MFSERIAL = space(10)
  w_MFCDSED1 = space(5)
  o_MFCDSED1 = space(5)
  w_MFCCONT1 = space(5)
  o_MFCCONT1 = space(5)
  w_MFMINPS1 = space(22)
  w_APP1A = .F.
  o_APP1A = .F.
  w_APP1 = .F.
  o_APP1 = .F.
  w_MFDAMES1 = space(2)
  w_MFDAANN1 = space(4)
  w_MF_AMES1 = space(2)
  w_MFAN1 = space(4)
  w_MFIMPSD1 = 0
  w_MFIMPSC1 = 0
  w_MFCDSED2 = space(5)
  o_MFCDSED2 = space(5)
  w_MFCCONT2 = space(5)
  o_MFCCONT2 = space(5)
  w_MFMINPS2 = space(22)
  w_APP2A = .F.
  o_APP2A = .F.
  w_MFDAMES2 = space(2)
  w_MFDAANN2 = space(4)
  w_APP2 = .F.
  o_APP2 = .F.
  w_MF_AMES2 = space(2)
  w_MFAN2 = space(4)
  w_MFIMPSD2 = 0
  w_MFIMPSC2 = 0
  w_MFCDSED3 = space(5)
  o_MFCDSED3 = space(5)
  w_MFCCONT3 = space(5)
  o_MFCCONT3 = space(5)
  w_MFMINPS3 = space(22)
  w_APP3A = .F.
  o_APP3A = .F.
  w_MFDAMES3 = space(2)
  w_APP3 = .F.
  o_APP3 = .F.
  w_MFDAANN3 = space(4)
  w_MF_AMES3 = space(2)
  w_MFAN3 = space(4)
  w_MFIMPSD3 = 0
  w_MFIMPSC3 = 0
  w_MFCDSED4 = space(5)
  o_MFCDSED4 = space(5)
  w_MFCCONT4 = space(5)
  o_MFCCONT4 = space(5)
  w_MFMINPS4 = space(22)
  w_APP4A = .F.
  o_APP4A = .F.
  w_MFDAMES4 = space(2)
  w_APP4 = .F.
  o_APP4 = .F.
  w_MFDAANN4 = space(4)
  w_MF_AMES4 = space(2)
  w_MFAN4 = space(4)
  w_MFIMPSD4 = 0
  w_MFIMPSC4 = 0
  w_MFTOTDPS = 0
  w_MFTOTCPS = 0
  w_MFSALDPS = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_GENINPS = space(10)
  w_IMPINPS = .F.
  w_MFSERIAL = space(10)
  w_MFCODRE1 = space(2)
  o_MFCODRE1 = space(2)
  w_MFTRIRE1 = space(5)
  o_MFTRIRE1 = space(5)
  w_MFRATRE1 = space(4)
  w_MFANNRE1 = space(4)
  w_MFIMDRE1 = 0
  w_MFIMCRE1 = 0
  w_MFCODRE2 = space(2)
  o_MFCODRE2 = space(2)
  w_MFTRIRE2 = space(5)
  o_MFTRIRE2 = space(5)
  w_MFRATRE2 = space(4)
  w_MFANNRE2 = space(4)
  w_MFIMDRE2 = 0
  w_MFIMCRE2 = 0
  w_MFCODRE3 = space(2)
  o_MFCODRE3 = space(2)
  w_MFTRIRE3 = space(5)
  o_MFTRIRE3 = space(5)
  w_MFRATRE3 = space(4)
  w_MFANNRE3 = space(4)
  w_MFIMDRE3 = 0
  w_MFIMCRE3 = 0
  w_MFCODRE4 = space(2)
  o_MFCODRE4 = space(2)
  w_MFTRIRE4 = space(5)
  o_MFTRIRE4 = space(5)
  w_MFRATRE4 = space(4)
  w_MFANNRE4 = space(4)
  w_MFIMDRE4 = 0
  w_MFIMCRE4 = 0
  w_MFTOTDRE = 0
  w_MFTOTCRE = 0
  w_MFSALDRE = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_MFSERIAL = space(10)
  w_MFSINAI1 = space(5)
  o_MFSINAI1 = space(5)
  w_MF_NPOS1 = space(8)
  w_MF_PACC1 = space(2)
  w_MF_NRIF1 = space(7)
  w_MFCAUSA1 = space(2)
  w_MFIMDIL1 = 0
  w_MFIMCIL1 = 0
  w_MFSINAI2 = space(5)
  o_MFSINAI2 = space(5)
  w_MF_NPOS2 = space(8)
  w_MF_PACC2 = space(2)
  w_MF_NRIF2 = space(7)
  w_MFCAUSA2 = space(2)
  w_MFIMDIL2 = 0
  w_MFIMCIL2 = 0
  w_MFSINAI3 = space(5)
  o_MFSINAI3 = space(5)
  w_MF_NPOS3 = space(8)
  w_MF_PACC3 = space(2)
  w_MF_NRIF3 = space(7)
  w_MFCAUSA3 = space(2)
  w_MFIMDIL3 = 0
  w_MFIMCIL3 = 0
  w_MFTDINAI = 0
  w_MFTCINAI = 0
  w_MFSALINA = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_MFSERIAL = space(10)
  w_MESE = space(2)
  w_ANNO = space(4)
  w_GENPREV = space(10)
  w_RESCHK = 0
  w_IMPPREV = .F.
  w_MFSERIAL = space(10)
  w_MFSALFIN = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_TIPOMOD = space(3)
  w_APPOIMP2 = 0
  w_ANNORIF = space(4)
  w_TIPTRI = space(10)
  w_MFDESSTA = space(5)
  o_MFDESSTA = space(5)
  w_CAP1 = space(9)
  w_LOCALI1 = space(30)
  w_INDIRI1 = space(35)
  w_PROVIN1 = space(2)
  w_CAP = space(9)
  w_LOCALI = space(30)
  w_PROVIN = space(2)
  w_INDIRI = space(35)
  w_DESSTA = space(1)
  w_CODAZIE = space(5)
  w_MFTIPMOD = space(10)
  w_MFSALAEN = 0
  o_MFSALAEN = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_MFSERIAL = this.W_MFSERIAL

  * --- Children pointers
  GSCG_ACF = .NULL.
  GSCG_AFQ = .NULL.
  GSCG_AVF = .NULL.
  GSCG_AIF = .NULL.
  GSCG_MAT = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=8, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOD_PAG','gscg_aon')
    stdPageFrame::Init()
    *set procedure to GSCG_ACF additive
    with this
      .Pages(1).addobject("oPag","tgscg_aonPag1","gscg_aon",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Contribuente")
      .Pages(1).HelpContextID = 159522069
      .Pages(2).addobject("oPag","tgscg_aonPag2","gscg_aon",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Erario")
      .Pages(2).HelpContextID = 75603642
      .Pages(3).addobject("oPag","tgscg_aonPag3","gscg_aon",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("INPS")
      .Pages(3).HelpContextID = 97398150
      .Pages(4).addobject("oPag","tgscg_aonPag4","gscg_aon",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Regioni enti locali")
      .Pages(4).HelpContextID = 112366599
      .Pages(5).addobject("oPag","tgscg_aonPag5","gscg_aon",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("INAIL")
      .Pages(5).HelpContextID = 176373126
      .Pages(6).addobject("oPag","tgscg_aonPag6","gscg_aon",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Accise")
      .Pages(6).HelpContextID = 233475578
      .Pages(7).addobject("oPag","tgscg_aonPag7","gscg_aon",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Estr.versamento")
      .Pages(7).HelpContextID = 98841880
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMFSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCG_ACF
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[21]
    this.cWorkTables[1]='TRI_BUTI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='TITOLARI'
    this.cWorkTables[4]='COD_TRIB'
    this.cWorkTables[5]='CODI_UFF'
    this.cWorkTables[6]='SED_INPS'
    this.cWorkTables[7]='CAU_INPS'
    this.cWorkTables[8]='REG_PROV'
    this.cWorkTables[9]='ENTI_LOC'
    this.cWorkTables[10]='SE_INAIL'
    this.cWorkTables[11]='COD_PREV'
    this.cWorkTables[12]='SED_AEN'
    this.cWorkTables[13]='CAU_AEN'
    this.cWorkTables[14]='MODVPAG'
    this.cWorkTables[15]='MODCPAG'
    this.cWorkTables[16]='MODEPAG'
    this.cWorkTables[17]='VALUTE'
    this.cWorkTables[18]='CPUSERS'
    this.cWorkTables[19]='MODIPAG'
    this.cWorkTables[20]='SEDIAZIE'
    this.cWorkTables[21]='MOD_PAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(21))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_PAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_PAG_IDX,3]
  return

  function CreateChildren()
    this.GSCG_ACF = CREATEOBJECT('stdDynamicChild',this,'GSCG_ACF',this.oPgFrm.Page1.oPag.oLinkPC_1_8)
    this.GSCG_ACF.createrealchild()
    this.GSCG_AFQ = CREATEOBJECT('stdDynamicChild',this,'GSCG_AFQ',this.oPgFrm.Page2.oPag.oLinkPC_2_2)
    this.GSCG_AVF = CREATEOBJECT('stdDynamicChild',this,'GSCG_AVF',this.oPgFrm.Page7.oPag.oLinkPC_7_3)
    this.GSCG_AIF = CREATEOBJECT('stdDynamicChild',this,'GSCG_AIF',this.oPgFrm.Page4.oPag.oLinkPC_4_47)
    this.GSCG_MAT = CREATEOBJECT('stdDynamicChild',this,'GSCG_MAT',this.oPgFrm.Page6.oPag.oLinkPC_6_12)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_ACF)
      this.GSCG_ACF.DestroyChildrenChain()
      this.GSCG_ACF=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_8')
    if !ISNULL(this.GSCG_AFQ)
      this.GSCG_AFQ.DestroyChildrenChain()
      this.GSCG_AFQ=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_2')
    if !ISNULL(this.GSCG_AVF)
      this.GSCG_AVF.DestroyChildrenChain()
      this.GSCG_AVF=.NULL.
    endif
    this.oPgFrm.Page7.oPag.RemoveObject('oLinkPC_7_3')
    if !ISNULL(this.GSCG_AIF)
      this.GSCG_AIF.DestroyChildrenChain()
      this.GSCG_AIF=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_47')
    if !ISNULL(this.GSCG_MAT)
      this.GSCG_MAT.DestroyChildrenChain()
      this.GSCG_MAT=.NULL.
    endif
    this.oPgFrm.Page6.oPag.RemoveObject('oLinkPC_6_12')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_ACF.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_AFQ.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_AVF.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_AIF.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MAT.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_ACF.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_AFQ.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_AVF.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_AIF.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MAT.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_ACF.NewDocument()
    this.GSCG_AFQ.NewDocument()
    this.GSCG_AVF.NewDocument()
    this.GSCG_AIF.NewDocument()
    this.GSCG_MAT.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_ACF.SetKey(;
            .w_MFSERIAL,"CFSERIAL";
            )
      this.GSCG_AFQ.SetKey(;
            .w_MFSERIAL,"EFSERIAL";
            )
      this.GSCG_AVF.SetKey(;
            .w_MFSERIAL,"VFSERIAL";
            )
      this.GSCG_AIF.SetKey(;
            .w_MFSERIAL,"IFSERIAL";
            )
      this.GSCG_MAT.SetKey(;
            .w_MFSERIAL,"AFSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_ACF.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"CFSERIAL";
             )
      .GSCG_AFQ.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"EFSERIAL";
             )
      .GSCG_AVF.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"VFSERIAL";
             )
      .GSCG_AIF.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"IFSERIAL";
             )
      .GSCG_MAT.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"AFSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_ACF)
        i_f=.GSCG_ACF.BuildFilter()
        if !(i_f==.GSCG_ACF.cQueryFilter)
          i_fnidx=.GSCG_ACF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_ACF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_ACF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_ACF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_ACF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_AFQ)
        i_f=.GSCG_AFQ.BuildFilter()
        if !(i_f==.GSCG_AFQ.cQueryFilter)
          i_fnidx=.GSCG_AFQ.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AFQ.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AFQ.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AFQ.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AFQ.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_AVF)
        i_f=.GSCG_AVF.BuildFilter()
        if !(i_f==.GSCG_AVF.cQueryFilter)
          i_fnidx=.GSCG_AVF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AVF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AVF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AVF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AVF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_AIF)
        i_f=.GSCG_AIF.BuildFilter()
        if !(i_f==.GSCG_AIF.cQueryFilter)
          i_fnidx=.GSCG_AIF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AIF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AIF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AIF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AIF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_MAT)
        i_f=.GSCG_MAT.BuildFilter()
        if !(i_f==.GSCG_MAT.cQueryFilter)
          i_fnidx=.GSCG_MAT.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MAT.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MAT.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MAT.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MAT.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MFSERIAL = NVL(MFSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_4_5_joined
    link_4_5_joined=.f.
    local link_4_18_joined
    link_4_18_joined=.f.
    local link_4_24_joined
    link_4_24_joined=.f.
    local link_4_30_joined
    link_4_30_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOD_PAG where MFSERIAL=KeySet.MFSERIAL
    *
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_PAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_PAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_PAG '
      link_4_5_joined=this.AddJoinedLink_4_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_18_joined=this.AddJoinedLink_4_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_24_joined=this.AddJoinedLink_4_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_30_joined=this.AddJoinedLink_4_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_APPCOIN = space(1)
        .w_APPOIMP = 0
        .w_GENERA = Sys(2015)
        .w_IMPORTA = .F.
        .w_GENINPS = Sys(2015)
        .w_IMPINPS = .F.
        .w_GENPREV = Sys(2015)
        .w_RESCHK = 0
        .w_IMPPREV = .F.
        .w_TIPOMOD = 'NEW'
        .w_APPOIMP2 = 0
        .w_ANNORIF = '2003'
        .w_TIPTRI = space(10)
        .w_CAP1 = space(9)
        .w_LOCALI1 = space(30)
        .w_INDIRI1 = space(35)
        .w_PROVIN1 = space(2)
        .w_DESSTA = space(1)
        .w_CODAZIE = i_CODAZI
        .w_MFSALAEN = 0
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFMESRIF = NVL(MFMESRIF,space(2))
        .w_MFANNRIF = NVL(MFANNRIF,space(4))
        .w_MFVALUTA = NVL(MFVALUTA,space(3))
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .w_MF_COINC = NVL(MF_COINC,space(1))
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCODUFF = NVL(MFCODUFF,space(3))
          * evitabile
          *.link_2_4('Load')
        .w_MFCODATT = NVL(MFCODATT,space(11))
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_16.Calculate()
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCDSED1 = NVL(MFCDSED1,space(5))
          * evitabile
          *.link_3_2('Load')
        .w_MFCCONT1 = NVL(MFCCONT1,space(5))
          * evitabile
          *.link_3_3('Load')
        .w_MFMINPS1 = NVL(MFMINPS1,space(22))
        .w_APP1A = !empty(.w_MFCCONT1) and alltrim(.w_MFCCONT1)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_APP1 = !empty(.w_MFCCONT1) and (alltrim(.w_MFCCONT1)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP1A)
        .w_MFDAMES1 = NVL(MFDAMES1,space(2))
        .w_MFDAANN1 = NVL(MFDAANN1,space(4))
        .w_MF_AMES1 = NVL(MF_AMES1,space(2))
        .w_MFAN1 = NVL(MFAN1,space(4))
        .w_MFIMPSD1 = NVL(MFIMPSD1,0)
        .w_MFIMPSC1 = NVL(MFIMPSC1,0)
        .w_MFCDSED2 = NVL(MFCDSED2,space(5))
          * evitabile
          *.link_3_13('Load')
        .w_MFCCONT2 = NVL(MFCCONT2,space(5))
          * evitabile
          *.link_3_14('Load')
        .w_MFMINPS2 = NVL(MFMINPS2,space(22))
        .w_APP2A = !empty(.w_MFCCONT2) and alltrim(.w_MFCCONT2)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES2 = NVL(MFDAMES2,space(2))
        .w_MFDAANN2 = NVL(MFDAANN2,space(4))
        .w_APP2 = !empty(.w_MFCCONT2) and (alltrim(.w_MFCCONT2)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBTU-EBIT-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP2A)
        .w_MF_AMES2 = NVL(MF_AMES2,space(2))
        .w_MFAN2 = NVL(MFAN2,space(4))
        .w_MFIMPSD2 = NVL(MFIMPSD2,0)
        .w_MFIMPSC2 = NVL(MFIMPSC2,0)
        .w_MFCDSED3 = NVL(MFCDSED3,space(5))
          * evitabile
          *.link_3_24('Load')
        .w_MFCCONT3 = NVL(MFCCONT3,space(5))
          * evitabile
          *.link_3_25('Load')
        .w_MFMINPS3 = NVL(MFMINPS3,space(22))
        .w_APP3A = !empty(.w_MFCCONT3) and alltrim(.w_MFCCONT3)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES3 = NVL(MFDAMES3,space(2))
        .w_APP3 = !empty(.w_MFCCONT3) and (alltrim(.w_MFCCONT3)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP3A)
        .w_MFDAANN3 = NVL(MFDAANN3,space(4))
        .w_MF_AMES3 = NVL(MF_AMES3,space(2))
        .w_MFAN3 = NVL(MFAN3,space(4))
        .w_MFIMPSD3 = NVL(MFIMPSD3,0)
        .w_MFIMPSC3 = NVL(MFIMPSC3,0)
        .w_MFCDSED4 = NVL(MFCDSED4,space(5))
          * evitabile
          *.link_3_35('Load')
        .w_MFCCONT4 = NVL(MFCCONT4,space(5))
          * evitabile
          *.link_3_36('Load')
        .w_MFMINPS4 = NVL(MFMINPS4,space(22))
        .w_APP4A = !empty(.w_MFCCONT4) and alltrim(.w_MFCCONT4)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES4 = NVL(MFDAMES4,space(2))
        .w_APP4 = !empty(.w_MFCCONT4) and (alltrim(.w_MFCCONT4)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP4A)
        .w_MFDAANN4 = NVL(MFDAANN4,space(4))
        .w_MF_AMES4 = NVL(MF_AMES4,space(2))
        .w_MFAN4 = NVL(MFAN4,space(4))
        .w_MFIMPSD4 = NVL(MFIMPSD4,0)
        .w_MFIMPSC4 = NVL(MFIMPSC4,0)
        .w_MFTOTDPS = NVL(MFTOTDPS,0)
        .w_MFTOTCPS = NVL(MFTOTCPS,0)
        .w_MFSALDPS = NVL(MFSALDPS,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCODRE1 = NVL(MFCODRE1,space(2))
          * evitabile
          *.link_4_2('Load')
        .w_MFTRIRE1 = NVL(MFTRIRE1,space(5))
          if link_4_5_joined
            this.w_MFTRIRE1 = NVL(TRCODICE405,NVL(this.w_MFTRIRE1,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI405,space(10))
          else
          .link_4_5('Load')
          endif
        .w_MFRATRE1 = NVL(MFRATRE1,space(4))
        .w_MFANNRE1 = NVL(MFANNRE1,space(4))
        .w_MFIMDRE1 = NVL(MFIMDRE1,0)
        .w_MFIMCRE1 = NVL(MFIMCRE1,0)
        .w_MFCODRE2 = NVL(MFCODRE2,space(2))
          * evitabile
          *.link_4_17('Load')
        .w_MFTRIRE2 = NVL(MFTRIRE2,space(5))
          if link_4_18_joined
            this.w_MFTRIRE2 = NVL(TRCODICE418,NVL(this.w_MFTRIRE2,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI418,space(10))
          else
          .link_4_18('Load')
          endif
        .w_MFRATRE2 = NVL(MFRATRE2,space(4))
        .w_MFANNRE2 = NVL(MFANNRE2,space(4))
        .w_MFIMDRE2 = NVL(MFIMDRE2,0)
        .w_MFIMCRE2 = NVL(MFIMCRE2,0)
        .w_MFCODRE3 = NVL(MFCODRE3,space(2))
          * evitabile
          *.link_4_23('Load')
        .w_MFTRIRE3 = NVL(MFTRIRE3,space(5))
          if link_4_24_joined
            this.w_MFTRIRE3 = NVL(TRCODICE424,NVL(this.w_MFTRIRE3,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI424,space(10))
          else
          .link_4_24('Load')
          endif
        .w_MFRATRE3 = NVL(MFRATRE3,space(4))
        .w_MFANNRE3 = NVL(MFANNRE3,space(4))
        .w_MFIMDRE3 = NVL(MFIMDRE3,0)
        .w_MFIMCRE3 = NVL(MFIMCRE3,0)
        .w_MFCODRE4 = NVL(MFCODRE4,space(2))
          * evitabile
          *.link_4_29('Load')
        .w_MFTRIRE4 = NVL(MFTRIRE4,space(5))
          if link_4_30_joined
            this.w_MFTRIRE4 = NVL(TRCODICE430,NVL(this.w_MFTRIRE4,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI430,space(10))
          else
          .link_4_30('Load')
          endif
        .w_MFRATRE4 = NVL(MFRATRE4,space(4))
        .w_MFANNRE4 = NVL(MFANNRE4,space(4))
        .w_MFIMDRE4 = NVL(MFIMDRE4,0)
        .w_MFIMCRE4 = NVL(MFIMCRE4,0)
        .w_MFTOTDRE = NVL(MFTOTDRE,0)
        .w_MFTOTCRE = NVL(MFTOTCRE,0)
        .w_MFSALDRE = NVL(MFSALDRE,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFSINAI1 = NVL(MFSINAI1,space(5))
          * evitabile
          *.link_5_2('Load')
        .w_MF_NPOS1 = NVL(MF_NPOS1,space(8))
        .w_MF_PACC1 = NVL(MF_PACC1,space(2))
        .w_MF_NRIF1 = NVL(MF_NRIF1,space(7))
        .w_MFCAUSA1 = NVL(MFCAUSA1,space(2))
        .w_MFIMDIL1 = NVL(MFIMDIL1,0)
        .w_MFIMCIL1 = NVL(MFIMCIL1,0)
        .w_MFSINAI2 = NVL(MFSINAI2,space(5))
          * evitabile
          *.link_5_9('Load')
        .w_MF_NPOS2 = NVL(MF_NPOS2,space(8))
        .w_MF_PACC2 = NVL(MF_PACC2,space(2))
        .w_MF_NRIF2 = NVL(MF_NRIF2,space(7))
        .w_MFCAUSA2 = NVL(MFCAUSA2,space(2))
        .w_MFIMDIL2 = NVL(MFIMDIL2,0)
        .w_MFIMCIL2 = NVL(MFIMCIL2,0)
        .w_MFSINAI3 = NVL(MFSINAI3,space(5))
          * evitabile
          *.link_5_16('Load')
        .w_MF_NPOS3 = NVL(MF_NPOS3,space(8))
        .w_MF_PACC3 = NVL(MF_PACC3,space(2))
        .w_MF_NRIF3 = NVL(MF_NRIF3,space(7))
        .w_MFCAUSA3 = NVL(MFCAUSA3,space(2))
        .w_MFIMDIL3 = NVL(MFIMDIL3,0)
        .w_MFIMCIL3 = NVL(MFIMCIL3,0)
        .w_MFTDINAI = NVL(MFTDINAI,0)
        .w_MFTCINAI = NVL(MFTCINAI,0)
        .w_MFSALINA = NVL(MFSALINA,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFSALFIN = NVL(MFSALFIN,0)
        .oPgFrm.Page7.oPag.oObj_7_9.Calculate()
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page6.oPag.oObj_6_11.Calculate()
        .w_MFDESSTA = NVL(MFDESSTA,space(5))
          .link_7_13('Load')
        .w_CAP = iif(empty(.w_MFDESSTA),g_capazi,.w_CAP1)
        .w_LOCALI = iif(empty(.w_MFDESSTA),g_locazi,.w_LOCALI1)
        .w_PROVIN = iif(empty(.w_MFDESSTA),g_proazi,.w_PROVIN1)
        .w_INDIRI = iif(empty(.w_MFDESSTA),g_indazi,.w_INDIRI1)
        .w_MFTIPMOD = NVL(MFTIPMOD,space(10))
        .oPgFrm.Page6.oPag.oObj_6_15.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'MOD_PAG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page7.oPag.oBtn_7_8.enabled = this.oPgFrm.Page7.oPag.oBtn_7_8.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_aon
    if this.GSCG_AVF.w_VFDTPRES <= cp_CharToDate('31-12-2001')
      this.BlankRec()
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_aon
    this.w_MFSALAEN=0
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MFSERIAL = space(10)
      .w_MFMESRIF = space(2)
      .w_MFANNRIF = space(4)
      .w_MFVALUTA = space(3)
      .w_APPCOIN = space(1)
      .w_MF_COINC = space(1)
      .w_MFSERIAL = space(10)
      .w_APPOIMP = 0
      .w_MFCODUFF = space(3)
      .w_MFCODATT = space(11)
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_GENERA = space(10)
      .w_IMPORTA = .f.
      .w_MFSERIAL = space(10)
      .w_MFCDSED1 = space(5)
      .w_MFCCONT1 = space(5)
      .w_MFMINPS1 = space(22)
      .w_APP1A = .f.
      .w_APP1 = .f.
      .w_MFDAMES1 = space(2)
      .w_MFDAANN1 = space(4)
      .w_MF_AMES1 = space(2)
      .w_MFAN1 = space(4)
      .w_MFIMPSD1 = 0
      .w_MFIMPSC1 = 0
      .w_MFCDSED2 = space(5)
      .w_MFCCONT2 = space(5)
      .w_MFMINPS2 = space(22)
      .w_APP2A = .f.
      .w_MFDAMES2 = space(2)
      .w_MFDAANN2 = space(4)
      .w_APP2 = .f.
      .w_MF_AMES2 = space(2)
      .w_MFAN2 = space(4)
      .w_MFIMPSD2 = 0
      .w_MFIMPSC2 = 0
      .w_MFCDSED3 = space(5)
      .w_MFCCONT3 = space(5)
      .w_MFMINPS3 = space(22)
      .w_APP3A = .f.
      .w_MFDAMES3 = space(2)
      .w_APP3 = .f.
      .w_MFDAANN3 = space(4)
      .w_MF_AMES3 = space(2)
      .w_MFAN3 = space(4)
      .w_MFIMPSD3 = 0
      .w_MFIMPSC3 = 0
      .w_MFCDSED4 = space(5)
      .w_MFCCONT4 = space(5)
      .w_MFMINPS4 = space(22)
      .w_APP4A = .f.
      .w_MFDAMES4 = space(2)
      .w_APP4 = .f.
      .w_MFDAANN4 = space(4)
      .w_MF_AMES4 = space(2)
      .w_MFAN4 = space(4)
      .w_MFIMPSD4 = 0
      .w_MFIMPSC4 = 0
      .w_MFTOTDPS = 0
      .w_MFTOTCPS = 0
      .w_MFSALDPS = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_GENINPS = space(10)
      .w_IMPINPS = .f.
      .w_MFSERIAL = space(10)
      .w_MFCODRE1 = space(2)
      .w_MFTRIRE1 = space(5)
      .w_MFRATRE1 = space(4)
      .w_MFANNRE1 = space(4)
      .w_MFIMDRE1 = 0
      .w_MFIMCRE1 = 0
      .w_MFCODRE2 = space(2)
      .w_MFTRIRE2 = space(5)
      .w_MFRATRE2 = space(4)
      .w_MFANNRE2 = space(4)
      .w_MFIMDRE2 = 0
      .w_MFIMCRE2 = 0
      .w_MFCODRE3 = space(2)
      .w_MFTRIRE3 = space(5)
      .w_MFRATRE3 = space(4)
      .w_MFANNRE3 = space(4)
      .w_MFIMDRE3 = 0
      .w_MFIMCRE3 = 0
      .w_MFCODRE4 = space(2)
      .w_MFTRIRE4 = space(5)
      .w_MFRATRE4 = space(4)
      .w_MFANNRE4 = space(4)
      .w_MFIMDRE4 = 0
      .w_MFIMCRE4 = 0
      .w_MFTOTDRE = 0
      .w_MFTOTCRE = 0
      .w_MFSALDRE = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_MFSERIAL = space(10)
      .w_MFSINAI1 = space(5)
      .w_MF_NPOS1 = space(8)
      .w_MF_PACC1 = space(2)
      .w_MF_NRIF1 = space(7)
      .w_MFCAUSA1 = space(2)
      .w_MFIMDIL1 = 0
      .w_MFIMCIL1 = 0
      .w_MFSINAI2 = space(5)
      .w_MF_NPOS2 = space(8)
      .w_MF_PACC2 = space(2)
      .w_MF_NRIF2 = space(7)
      .w_MFCAUSA2 = space(2)
      .w_MFIMDIL2 = 0
      .w_MFIMCIL2 = 0
      .w_MFSINAI3 = space(5)
      .w_MF_NPOS3 = space(8)
      .w_MF_PACC3 = space(2)
      .w_MF_NRIF3 = space(7)
      .w_MFCAUSA3 = space(2)
      .w_MFIMDIL3 = 0
      .w_MFIMCIL3 = 0
      .w_MFTDINAI = 0
      .w_MFTCINAI = 0
      .w_MFSALINA = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_MFSERIAL = space(10)
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_GENPREV = space(10)
      .w_RESCHK = 0
      .w_IMPPREV = .f.
      .w_MFSERIAL = space(10)
      .w_MFSALFIN = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_TIPOMOD = space(3)
      .w_APPOIMP2 = 0
      .w_ANNORIF = space(4)
      .w_TIPTRI = space(10)
      .w_MFDESSTA = space(5)
      .w_CAP1 = space(9)
      .w_LOCALI1 = space(30)
      .w_INDIRI1 = space(35)
      .w_PROVIN1 = space(2)
      .w_CAP = space(9)
      .w_LOCALI = space(30)
      .w_PROVIN = space(2)
      .w_INDIRI = space(35)
      .w_DESSTA = space(1)
      .w_CODAZIE = space(5)
      .w_MFTIPMOD = space(10)
      .w_MFSALAEN = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_MFMESRIF = ALLTRIM(STR(MONTH(i_datsys)))
        .w_MFANNRIF = ALLTRIM(STR(YEAR(i_datsys)))
        .w_MFVALUTA = g_CODEUR
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
          .DoRTCalc(5,5,.f.)
        .w_MF_COINC = .w_APPCOIN
        .DoRTCalc(7,9,.f.)
          if not(empty(.w_MFCODUFF))
          .link_2_4('Full')
          endif
          .DoRTCalc(10,10,.f.)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_GENERA = Sys(2015)
        .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
        .w_IMPORTA = .F.
        .oPgFrm.Page2.oPag.oObj_2_16.Calculate()
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_MFCDSED1))
          .link_3_2('Full')
          endif
        .w_MFCCONT1 = iif(empty(.w_MFCDSED1),' ',.w_MFCCONT1)
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_MFCCONT1))
          .link_3_3('Full')
          endif
        .w_MFMINPS1 = iif(empty(.w_MFCDSED1),' ',.w_MFMINPS1)
        .w_APP1A = !empty(.w_MFCCONT1) and alltrim(.w_MFCCONT1)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_APP1 = !empty(.w_MFCCONT1) and (alltrim(.w_MFCCONT1)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP1A)
        .w_MFDAMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1A,' ',.w_MFDAMES1))
        .w_MFDAANN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1A,' ',.w_MFDAANN1))
        .w_MF_AMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,' ',.w_MF_AMES1))
        .w_MFAN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,' ',.w_MFAN1))
        .w_MFIMPSD1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSD1)
        .w_MFIMPSC1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSC1)
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_MFCDSED2))
          .link_3_13('Full')
          endif
        .w_MFCCONT2 = iif(empty(.w_MFCDSED2),' ',.w_MFCCONT2)
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_MFCCONT2))
          .link_3_14('Full')
          endif
        .w_MFMINPS2 = iif(empty(.w_MFCDSED2),' ',.w_MFMINPS2)
        .w_APP2A = !empty(.w_MFCCONT2) and alltrim(.w_MFCCONT2)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2A,' ',.w_MFDAMES2))
        .w_MFDAANN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2A,' ',.w_MFDAANN2))
        .w_APP2 = !empty(.w_MFCCONT2) and (alltrim(.w_MFCCONT2)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBTU-EBIT-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP2A)
        .w_MF_AMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,' ',.w_MF_AMES2))
        .w_MFAN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,' ',.w_MFAN2))
        .w_MFIMPSD2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSD2)
        .w_MFIMPSC2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSC2)
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_MFCDSED3))
          .link_3_24('Full')
          endif
        .w_MFCCONT3 = iif(empty(.w_MFCDSED3),' ',.w_MFCCONT3)
        .DoRTCalc(39,39,.f.)
          if not(empty(.w_MFCCONT3))
          .link_3_25('Full')
          endif
        .w_MFMINPS3 = iif(empty(.w_MFCDSED3),' ',.w_MFMINPS3)
        .w_APP3A = !empty(.w_MFCCONT3) and alltrim(.w_MFCCONT3)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3A,' ',.w_MFDAMES3))
        .w_APP3 = !empty(.w_MFCCONT3) and (alltrim(.w_MFCCONT3)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP3A)
        .w_MFDAANN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3A,' ',.w_MFDAANN3))
        .w_MF_AMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,' ',.w_MF_AMES3))
        .w_MFAN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,' ',.w_MFAN3))
        .w_MFIMPSD3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSD3)
        .w_MFIMPSC3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSC3)
        .DoRTCalc(49,49,.f.)
          if not(empty(.w_MFCDSED4))
          .link_3_35('Full')
          endif
        .w_MFCCONT4 = iif(empty(.w_MFCDSED4),' ',.w_MFCCONT4)
        .DoRTCalc(50,50,.f.)
          if not(empty(.w_MFCCONT4))
          .link_3_36('Full')
          endif
        .w_MFMINPS4 = iif(empty(.w_MFCDSED4),' ',.w_MFMINPS4)
        .w_APP4A = !empty(.w_MFCCONT4) and alltrim(.w_MFCCONT4)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4A,' ',.w_MFDAMES4))
        .w_APP4 = !empty(.w_MFCCONT4) and (alltrim(.w_MFCCONT4)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP4A)
        .w_MFDAANN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4A,' ',.w_MFDAANN4))
        .w_MF_AMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,' ',.w_MF_AMES4))
        .w_MFAN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,' ',.w_MFAN4))
        .w_MFIMPSD4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSD4)
        .w_MFIMPSC4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSC4)
        .w_MFTOTDPS = .w_MFIMPSD1+.w_MFIMPSD2+.w_MFIMPSD3+.w_MFIMPSD4
        .w_MFTOTCPS = .w_MFIMPSC1+.w_MFIMPSC2+.w_MFIMPSC3+.w_MFIMPSC4
        .w_MFSALDPS = .w_MFTOTDPS-.w_MFTOTCPS
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_GENINPS = Sys(2015)
        .w_IMPINPS = .F.
        .DoRTCalc(67,68,.f.)
          if not(empty(.w_MFCODRE1))
          .link_4_2('Full')
          endif
        .w_MFTRIRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFTRIRE1)
        .DoRTCalc(69,69,.f.)
          if not(empty(.w_MFTRIRE1))
          .link_4_5('Full')
          endif
        .w_MFRATRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFRATRE1)
        .w_MFANNRE1 = iif(empty(.w_MFCODRE1),' ',iif(.w_MFTRIRE1= '3805','0',.w_MFANNRE1))
        .w_MFIMDRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMDRE1)
        .w_MFIMCRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMCRE1)
        .DoRTCalc(74,74,.f.)
          if not(empty(.w_MFCODRE2))
          .link_4_17('Full')
          endif
        .w_MFTRIRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFTRIRE2)
        .DoRTCalc(75,75,.f.)
          if not(empty(.w_MFTRIRE2))
          .link_4_18('Full')
          endif
        .w_MFRATRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFRATRE2)
        .w_MFANNRE2 = iif(empty(.w_MFCODRE2),' ',iif(.w_MFTRIRE2 ='3805','0',.w_MFANNRE2))
        .w_MFIMDRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMDRE2)
        .w_MFIMCRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMCRE2)
        .DoRTCalc(80,80,.f.)
          if not(empty(.w_MFCODRE3))
          .link_4_23('Full')
          endif
        .w_MFTRIRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFTRIRE3)
        .DoRTCalc(81,81,.f.)
          if not(empty(.w_MFTRIRE3))
          .link_4_24('Full')
          endif
        .w_MFRATRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFRATRE3)
        .w_MFANNRE3 = iif(empty(.w_MFCODRE3),' ',iif(.w_MFTRIRE3 = '3805','0',.w_MFANNRE3))
        .w_MFIMDRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMDRE3)
        .w_MFIMCRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMCRE3)
        .DoRTCalc(86,86,.f.)
          if not(empty(.w_MFCODRE4))
          .link_4_29('Full')
          endif
        .w_MFTRIRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFTRIRE4)
        .DoRTCalc(87,87,.f.)
          if not(empty(.w_MFTRIRE4))
          .link_4_30('Full')
          endif
        .w_MFRATRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFRATRE4)
        .w_MFANNRE4 = iif(empty(.w_MFCODRE4),' ',iif( .w_MFTRIRE4 = '3805','0',.w_MFANNRE4))
        .w_MFIMDRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMDRE4)
        .w_MFIMCRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMCRE4)
        .w_MFTOTDRE = .w_MFIMDRE1+.w_MFIMDRE2+.w_MFIMDRE3+.w_MFIMDRE4
        .w_MFTOTCRE = .w_MFIMCRE1+.w_MFIMCRE2+.w_MFIMCRE3+.w_MFIMCRE4
        .w_MFSALDRE = .w_MFTOTDRE-.w_MFTOTCRE
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .DoRTCalc(97,98,.f.)
          if not(empty(.w_MFSINAI1))
          .link_5_2('Full')
          endif
        .w_MF_NPOS1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NPOS1)
        .w_MF_PACC1 = iif(empty(.w_MFSINAI1),' ',.w_MF_PACC1)
        .w_MF_NRIF1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NRIF1)
        .w_MFCAUSA1 = iif(empty(.w_MFSINAI1),' ',.w_MFCAUSA1)
        .w_MFIMDIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMDIL1)
        .w_MFIMCIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMCIL1)
        .DoRTCalc(105,105,.f.)
          if not(empty(.w_MFSINAI2))
          .link_5_9('Full')
          endif
        .w_MF_NPOS2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NPOS2)
        .w_MF_PACC2 = iif(empty(.w_MFSINAI2),' ',.w_MF_PACC2)
        .w_MF_NRIF2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NRIF2)
        .w_MFCAUSA2 = iif(empty(.w_MFSINAI2),' ',.w_MFCAUSA2)
        .w_MFIMDIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMDIL2)
        .w_MFIMCIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMCIL2)
        .DoRTCalc(112,112,.f.)
          if not(empty(.w_MFSINAI3))
          .link_5_16('Full')
          endif
        .w_MF_NPOS3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NPOS3)
        .w_MF_PACC3 = iif(empty(.w_MFSINAI3),' ',.w_MF_PACC3)
        .w_MF_NRIF3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NRIF3)
        .w_MFCAUSA3 = iif(empty(.w_MFSINAI3),' ',.w_MFCAUSA3)
        .w_MFIMDIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMDIL3)
        .w_MFIMCIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMCIL3)
        .w_MFTDINAI = .w_MFIMDIL1+.w_MFIMDIL2+.w_MFIMDIL3
        .w_MFTCINAI = .w_MFIMCIL1+.w_MFIMCIL2+.w_MFIMCIL3
        .w_MFSALINA = .w_MFTDINAI-.w_MFTCINAI
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
          .DoRTCalc(124,124,.f.)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_GENPREV = Sys(2015)
          .DoRTCalc(128,128,.f.)
        .w_IMPPREV = .F.
          .DoRTCalc(130,130,.f.)
        .w_MFSALFIN = .w_APPOIMP + .w_APPOIMP2 +  .w_MFSALDPS + .w_MFSALDRE + .w_MFSALINA +  .w_MFSALAEN
        .oPgFrm.Page7.oPag.oObj_7_9.Calculate()
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_TIPOMOD = 'NEW'
        .oPgFrm.Page6.oPag.oObj_6_11.Calculate()
          .DoRTCalc(135,135,.f.)
        .w_ANNORIF = '2003'
        .DoRTCalc(137,138,.f.)
          if not(empty(.w_MFDESSTA))
          .link_7_13('Full')
          endif
          .DoRTCalc(139,142,.f.)
        .w_CAP = iif(empty(.w_MFDESSTA),g_capazi,.w_CAP1)
        .w_LOCALI = iif(empty(.w_MFDESSTA),g_locazi,.w_LOCALI1)
        .w_PROVIN = iif(empty(.w_MFDESSTA),g_proazi,.w_PROVIN1)
        .w_INDIRI = iif(empty(.w_MFDESSTA),g_indazi,.w_INDIRI1)
          .DoRTCalc(147,147,.f.)
        .w_CODAZIE = i_CODAZI
        .w_MFTIPMOD = 'ASSISE06'
        .w_MFSALAEN = 0
        .oPgFrm.Page6.oPag.oObj_6_15.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_PAG')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page7.oPag.oBtn_7_8.enabled = this.oPgFrm.Page7.oPag.oBtn_7_8.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_aon
    * instanzio il figlio della seconda,quarta e settima pagina immediatamente
       if Upper(this.GSCG_AVF.class)='STDDYNAMICCHILD'
         This.oPgFrm.Pages[7].opag.uienable(.T.)
         This.oPgFrm.ActivePage=1
       Endif
       if Upper(this.GSCG_AIF.class)='STDDYNAMICCHILD'
         This.oPgFrm.Pages[4].opag.uienable(.T.)
         This.oPgFrm.ActivePage=1
       Endif
       if Upper(this.GSCG_AFQ.class)='STDDYNAMICCHILD'
         This.oPgFrm.Pages[2].opag.uienable(.T.)
         This.oPgFrm.ActivePage=1
       Endif
          if Upper(this.GSCG_MAT.class)='STDDYNAMICCHILD'
         This.oPgFrm.Pages[6].opag.uienable(.T.)
         This.oPgFrm.ActivePage=1
       Endif
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"MODEL","i_CODAZI,w_MFSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_MFSERIAL = .w_MFSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMFSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oMFMESRIF_1_2.enabled = i_bVal
      .Page1.oPag.oMFANNRIF_1_3.enabled = i_bVal
      .Page1.oPag.oMF_COINC_1_7.enabled = i_bVal
      .Page2.oPag.oMFCODUFF_2_4.enabled = i_bVal
      .Page2.oPag.oMFCODATT_2_5.enabled = i_bVal
      .Page3.oPag.oMFCDSED1_3_2.enabled = i_bVal
      .Page3.oPag.oMFCCONT1_3_3.enabled = i_bVal
      .Page3.oPag.oMFMINPS1_3_4.enabled = i_bVal
      .Page3.oPag.oMFDAMES1_3_7.enabled = i_bVal
      .Page3.oPag.oMFDAANN1_3_8.enabled = i_bVal
      .Page3.oPag.oMF_AMES1_3_9.enabled = i_bVal
      .Page3.oPag.oMFAN1_3_10.enabled = i_bVal
      .Page3.oPag.oMFIMPSD1_3_11.enabled = i_bVal
      .Page3.oPag.oMFIMPSC1_3_12.enabled = i_bVal
      .Page3.oPag.oMFCDSED2_3_13.enabled = i_bVal
      .Page3.oPag.oMFCCONT2_3_14.enabled = i_bVal
      .Page3.oPag.oMFMINPS2_3_15.enabled = i_bVal
      .Page3.oPag.oMFDAMES2_3_17.enabled = i_bVal
      .Page3.oPag.oMFDAANN2_3_18.enabled = i_bVal
      .Page3.oPag.oMF_AMES2_3_20.enabled = i_bVal
      .Page3.oPag.oMFAN2_3_21.enabled = i_bVal
      .Page3.oPag.oMFIMPSD2_3_22.enabled = i_bVal
      .Page3.oPag.oMFIMPSC2_3_23.enabled = i_bVal
      .Page3.oPag.oMFCDSED3_3_24.enabled = i_bVal
      .Page3.oPag.oMFCCONT3_3_25.enabled = i_bVal
      .Page3.oPag.oMFMINPS3_3_26.enabled = i_bVal
      .Page3.oPag.oMFDAMES3_3_28.enabled = i_bVal
      .Page3.oPag.oMFDAANN3_3_30.enabled = i_bVal
      .Page3.oPag.oMF_AMES3_3_31.enabled = i_bVal
      .Page3.oPag.oMFAN3_3_32.enabled = i_bVal
      .Page3.oPag.oMFIMPSD3_3_33.enabled = i_bVal
      .Page3.oPag.oMFIMPSC3_3_34.enabled = i_bVal
      .Page3.oPag.oMFCDSED4_3_35.enabled = i_bVal
      .Page3.oPag.oMFCCONT4_3_36.enabled = i_bVal
      .Page3.oPag.oMFMINPS4_3_37.enabled = i_bVal
      .Page3.oPag.oMFDAMES4_3_39.enabled = i_bVal
      .Page3.oPag.oMFDAANN4_3_41.enabled = i_bVal
      .Page3.oPag.oMF_AMES4_3_42.enabled = i_bVal
      .Page3.oPag.oMFAN4_3_43.enabled = i_bVal
      .Page3.oPag.oMFIMPSD4_3_44.enabled = i_bVal
      .Page3.oPag.oMFIMPSC4_3_45.enabled = i_bVal
      .Page4.oPag.oMFCODRE1_4_2.enabled = i_bVal
      .Page4.oPag.oMFTRIRE1_4_5.enabled = i_bVal
      .Page4.oPag.oMFRATRE1_4_6.enabled = i_bVal
      .Page4.oPag.oMFANNRE1_4_7.enabled = i_bVal
      .Page4.oPag.oMFIMDRE1_4_8.enabled = i_bVal
      .Page4.oPag.oMFIMCRE1_4_9.enabled = i_bVal
      .Page4.oPag.oMFCODRE2_4_17.enabled = i_bVal
      .Page4.oPag.oMFTRIRE2_4_18.enabled = i_bVal
      .Page4.oPag.oMFRATRE2_4_19.enabled = i_bVal
      .Page4.oPag.oMFANNRE2_4_20.enabled = i_bVal
      .Page4.oPag.oMFIMDRE2_4_21.enabled = i_bVal
      .Page4.oPag.oMFIMCRE2_4_22.enabled = i_bVal
      .Page4.oPag.oMFCODRE3_4_23.enabled = i_bVal
      .Page4.oPag.oMFTRIRE3_4_24.enabled = i_bVal
      .Page4.oPag.oMFRATRE3_4_25.enabled = i_bVal
      .Page4.oPag.oMFANNRE3_4_26.enabled = i_bVal
      .Page4.oPag.oMFIMDRE3_4_27.enabled = i_bVal
      .Page4.oPag.oMFIMCRE3_4_28.enabled = i_bVal
      .Page4.oPag.oMFCODRE4_4_29.enabled = i_bVal
      .Page4.oPag.oMFTRIRE4_4_30.enabled = i_bVal
      .Page4.oPag.oMFRATRE4_4_31.enabled = i_bVal
      .Page4.oPag.oMFANNRE4_4_32.enabled = i_bVal
      .Page4.oPag.oMFIMDRE4_4_33.enabled = i_bVal
      .Page4.oPag.oMFIMCRE4_4_34.enabled = i_bVal
      .Page5.oPag.oMFSINAI1_5_2.enabled = i_bVal
      .Page5.oPag.oMF_NPOS1_5_3.enabled = i_bVal
      .Page5.oPag.oMF_PACC1_5_4.enabled = i_bVal
      .Page5.oPag.oMF_NRIF1_5_5.enabled = i_bVal
      .Page5.oPag.oMFCAUSA1_5_6.enabled = i_bVal
      .Page5.oPag.oMFIMDIL1_5_7.enabled = i_bVal
      .Page5.oPag.oMFIMCIL1_5_8.enabled = i_bVal
      .Page5.oPag.oMFSINAI2_5_9.enabled = i_bVal
      .Page5.oPag.oMF_NPOS2_5_10.enabled = i_bVal
      .Page5.oPag.oMF_PACC2_5_11.enabled = i_bVal
      .Page5.oPag.oMF_NRIF2_5_12.enabled = i_bVal
      .Page5.oPag.oMFCAUSA2_5_13.enabled = i_bVal
      .Page5.oPag.oMFIMDIL2_5_14.enabled = i_bVal
      .Page5.oPag.oMFIMCIL2_5_15.enabled = i_bVal
      .Page5.oPag.oMFSINAI3_5_16.enabled = i_bVal
      .Page5.oPag.oMF_NPOS3_5_17.enabled = i_bVal
      .Page5.oPag.oMF_PACC3_5_18.enabled = i_bVal
      .Page5.oPag.oMF_NRIF3_5_19.enabled = i_bVal
      .Page5.oPag.oMFCAUSA3_5_20.enabled = i_bVal
      .Page5.oPag.oMFIMDIL3_5_21.enabled = i_bVal
      .Page5.oPag.oMFIMCIL3_5_22.enabled = i_bVal
      .Page7.oPag.oMFDESSTA_7_13.enabled = i_bVal
      .Page2.oPag.oBtn_2_13.enabled = i_bVal
      .Page3.oPag.oBtn_3_69.enabled = i_bVal
      .Page7.oPag.oBtn_7_7.enabled = i_bVal
      .Page7.oPag.oBtn_7_8.enabled = .Page7.oPag.oBtn_7_8.mCond()
      .Page1.oPag.oObj_1_5.enabled = i_bVal
      .Page2.oPag.oObj_2_14.enabled = i_bVal
      .Page2.oPag.oObj_2_16.enabled = i_bVal
      .Page7.oPag.oObj_7_9.enabled = i_bVal
      .Page6.oPag.oObj_6_11.enabled = i_bVal
      .Page6.oPag.oObj_6_15.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMFSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMFSERIAL_1_1.enabled = .t.
        .Page1.oPag.oMFMESRIF_1_2.enabled = .t.
        .Page1.oPag.oMFANNRIF_1_3.enabled = .t.
      endif
    endwith
    this.GSCG_ACF.SetStatus(i_cOp)
    this.GSCG_AFQ.SetStatus(i_cOp)
    this.GSCG_AVF.SetStatus(i_cOp)
    this.GSCG_AIF.SetStatus(i_cOp)
    this.GSCG_MAT.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'MOD_PAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_ACF.SetChildrenStatus(i_cOp)
  *  this.GSCG_AFQ.SetChildrenStatus(i_cOp)
  *  this.GSCG_AVF.SetChildrenStatus(i_cOp)
  *  this.GSCG_AIF.SetChildrenStatus(i_cOp)
  *  this.GSCG_MAT.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMESRIF,"MFMESRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRIF,"MFANNRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFVALUTA,"MFVALUTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_COINC,"MF_COINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODUFF,"MFCODUFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODATT,"MFCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED1,"MFCDSED1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT1,"MFCCONT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS1,"MFMINPS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES1,"MFDAMES1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN1,"MFDAANN1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES1,"MF_AMES1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN1,"MFAN1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD1,"MFIMPSD1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC1,"MFIMPSC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED2,"MFCDSED2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT2,"MFCCONT2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS2,"MFMINPS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES2,"MFDAMES2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN2,"MFDAANN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES2,"MF_AMES2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN2,"MFAN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD2,"MFIMPSD2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC2,"MFIMPSC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED3,"MFCDSED3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT3,"MFCCONT3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS3,"MFMINPS3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES3,"MFDAMES3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN3,"MFDAANN3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES3,"MF_AMES3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN3,"MFAN3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD3,"MFIMPSD3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC3,"MFIMPSC3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED4,"MFCDSED4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT4,"MFCCONT4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS4,"MFMINPS4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES4,"MFDAMES4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN4,"MFDAANN4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES4,"MF_AMES4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN4,"MFAN4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD4,"MFIMPSD4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC4,"MFIMPSC4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTDPS,"MFTOTDPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTCPS,"MFTOTCPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALDPS,"MFSALDPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE1,"MFCODRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE1,"MFTRIRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE1,"MFRATRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE1,"MFANNRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE1,"MFIMDRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE1,"MFIMCRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE2,"MFCODRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE2,"MFTRIRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE2,"MFRATRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE2,"MFANNRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE2,"MFIMDRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE2,"MFIMCRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE3,"MFCODRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE3,"MFTRIRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE3,"MFRATRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE3,"MFANNRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE3,"MFIMDRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE3,"MFIMCRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE4,"MFCODRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE4,"MFTRIRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE4,"MFRATRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE4,"MFANNRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE4,"MFIMDRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE4,"MFIMCRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTDRE,"MFTOTDRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTCRE,"MFTOTCRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALDRE,"MFSALDRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSINAI1,"MFSINAI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NPOS1,"MF_NPOS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PACC1,"MF_PACC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NRIF1,"MF_NRIF1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUSA1,"MFCAUSA1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDIL1,"MFIMDIL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCIL1,"MFIMCIL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSINAI2,"MFSINAI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NPOS2,"MF_NPOS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PACC2,"MF_PACC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NRIF2,"MF_NRIF2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUSA2,"MFCAUSA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDIL2,"MFIMDIL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCIL2,"MFIMCIL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSINAI3,"MFSINAI3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NPOS3,"MF_NPOS3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PACC3,"MF_PACC3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NRIF3,"MF_NRIF3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUSA3,"MFCAUSA3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDIL3,"MFIMDIL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCIL3,"MFIMCIL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTDINAI,"MFTDINAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTCINAI,"MFTCINAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALINA,"MFSALINA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALFIN,"MFSALFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDESSTA,"MFDESSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTIPMOD,"MFTIPMOD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    i_lTable = "MOD_PAG"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOD_PAG_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        do GSCG_BMF with this
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOD_PAG_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"MODEL","i_CODAZI,w_MFSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOD_PAG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_PAG')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_PAG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MFSERIAL,MFMESRIF,MFANNRIF,MFVALUTA,MF_COINC"+;
                  ",MFCODUFF,MFCODATT,MFCDSED1,MFCCONT1,MFMINPS1"+;
                  ",MFDAMES1,MFDAANN1,MF_AMES1,MFAN1,MFIMPSD1"+;
                  ",MFIMPSC1,MFCDSED2,MFCCONT2,MFMINPS2,MFDAMES2"+;
                  ",MFDAANN2,MF_AMES2,MFAN2,MFIMPSD2,MFIMPSC2"+;
                  ",MFCDSED3,MFCCONT3,MFMINPS3,MFDAMES3,MFDAANN3"+;
                  ",MF_AMES3,MFAN3,MFIMPSD3,MFIMPSC3,MFCDSED4"+;
                  ",MFCCONT4,MFMINPS4,MFDAMES4,MFDAANN4,MF_AMES4"+;
                  ",MFAN4,MFIMPSD4,MFIMPSC4,MFTOTDPS,MFTOTCPS"+;
                  ",MFSALDPS,MFCODRE1,MFTRIRE1,MFRATRE1,MFANNRE1"+;
                  ",MFIMDRE1,MFIMCRE1,MFCODRE2,MFTRIRE2,MFRATRE2"+;
                  ",MFANNRE2,MFIMDRE2,MFIMCRE2,MFCODRE3,MFTRIRE3"+;
                  ",MFRATRE3,MFANNRE3,MFIMDRE3,MFIMCRE3,MFCODRE4"+;
                  ",MFTRIRE4,MFRATRE4,MFANNRE4,MFIMDRE4,MFIMCRE4"+;
                  ",MFTOTDRE,MFTOTCRE,MFSALDRE,MFSINAI1,MF_NPOS1"+;
                  ",MF_PACC1,MF_NRIF1,MFCAUSA1,MFIMDIL1,MFIMCIL1"+;
                  ",MFSINAI2,MF_NPOS2,MF_PACC2,MF_NRIF2,MFCAUSA2"+;
                  ",MFIMDIL2,MFIMCIL2,MFSINAI3,MF_NPOS3,MF_PACC3"+;
                  ",MF_NRIF3,MFCAUSA3,MFIMDIL3,MFIMCIL3,MFTDINAI"+;
                  ",MFTCINAI,MFSALINA,MFSALFIN,MFDESSTA,MFTIPMOD "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MFSERIAL)+;
                  ","+cp_ToStrODBC(this.w_MFMESRIF)+;
                  ","+cp_ToStrODBC(this.w_MFANNRIF)+;
                  ","+cp_ToStrODBC(this.w_MFVALUTA)+;
                  ","+cp_ToStrODBC(this.w_MF_COINC)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODUFF)+;
                  ","+cp_ToStrODBC(this.w_MFCODATT)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT1)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS1)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES1)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN1)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES1)+;
                  ","+cp_ToStrODBC(this.w_MFAN1)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD1)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT2)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS2)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES2)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN2)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES2)+;
                  ","+cp_ToStrODBC(this.w_MFAN2)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD2)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT3)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS3)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES3)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN3)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES3)+;
                  ","+cp_ToStrODBC(this.w_MFAN3)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD3)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED4)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT4)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS4)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES4)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN4)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES4)+;
                  ","+cp_ToStrODBC(this.w_MFAN4)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD4)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC4)+;
                  ","+cp_ToStrODBC(this.w_MFTOTDPS)+;
                  ","+cp_ToStrODBC(this.w_MFTOTCPS)+;
                  ","+cp_ToStrODBC(this.w_MFSALDPS)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE1)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE1)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE1)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE1)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE1)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE2)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE2)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE2)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE2)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE2)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE3)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE3)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE3)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE3)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE3)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE4)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE4)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE4)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE4)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE4)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE4)+;
                  ","+cp_ToStrODBC(this.w_MFTOTDRE)+;
                  ","+cp_ToStrODBC(this.w_MFTOTCRE)+;
                  ","+cp_ToStrODBC(this.w_MFSALDRE)+;
                  ","+cp_ToStrODBCNull(this.w_MFSINAI1)+;
                  ","+cp_ToStrODBC(this.w_MF_NPOS1)+;
                  ","+cp_ToStrODBC(this.w_MF_PACC1)+;
                  ","+cp_ToStrODBC(this.w_MF_NRIF1)+;
                  ","+cp_ToStrODBC(this.w_MFCAUSA1)+;
                  ","+cp_ToStrODBC(this.w_MFIMDIL1)+;
                  ","+cp_ToStrODBC(this.w_MFIMCIL1)+;
                  ","+cp_ToStrODBCNull(this.w_MFSINAI2)+;
                  ","+cp_ToStrODBC(this.w_MF_NPOS2)+;
                  ","+cp_ToStrODBC(this.w_MF_PACC2)+;
                  ","+cp_ToStrODBC(this.w_MF_NRIF2)+;
                  ","+cp_ToStrODBC(this.w_MFCAUSA2)+;
                  ","+cp_ToStrODBC(this.w_MFIMDIL2)+;
                  ","+cp_ToStrODBC(this.w_MFIMCIL2)+;
                  ","+cp_ToStrODBCNull(this.w_MFSINAI3)+;
                  ","+cp_ToStrODBC(this.w_MF_NPOS3)+;
                  ","+cp_ToStrODBC(this.w_MF_PACC3)+;
                  ","+cp_ToStrODBC(this.w_MF_NRIF3)+;
                  ","+cp_ToStrODBC(this.w_MFCAUSA3)+;
                  ","+cp_ToStrODBC(this.w_MFIMDIL3)+;
                  ","+cp_ToStrODBC(this.w_MFIMCIL3)+;
                  ","+cp_ToStrODBC(this.w_MFTDINAI)+;
                  ","+cp_ToStrODBC(this.w_MFTCINAI)+;
                  ","+cp_ToStrODBC(this.w_MFSALINA)+;
                  ","+cp_ToStrODBC(this.w_MFSALFIN)+;
                  ","+cp_ToStrODBCNull(this.w_MFDESSTA)+;
                  ","+cp_ToStrODBC(this.w_MFTIPMOD)+;
             ""
             i_nnn=i_nnn+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_PAG')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_PAG')
        cp_CheckDeletedKey(i_cTable,0,'MFSERIAL',this.w_MFSERIAL)
        INSERT INTO (i_cTable);
              (MFSERIAL,MFMESRIF,MFANNRIF,MFVALUTA,MF_COINC,MFCODUFF,MFCODATT,MFCDSED1,MFCCONT1,MFMINPS1,MFDAMES1,MFDAANN1,MF_AMES1,MFAN1,MFIMPSD1,MFIMPSC1,MFCDSED2,MFCCONT2,MFMINPS2,MFDAMES2,MFDAANN2,MF_AMES2,MFAN2,MFIMPSD2,MFIMPSC2,MFCDSED3,MFCCONT3,MFMINPS3,MFDAMES3,MFDAANN3,MF_AMES3,MFAN3,MFIMPSD3,MFIMPSC3,MFCDSED4,MFCCONT4,MFMINPS4,MFDAMES4,MFDAANN4,MF_AMES4,MFAN4,MFIMPSD4,MFIMPSC4,MFTOTDPS,MFTOTCPS,MFSALDPS,MFCODRE1,MFTRIRE1,MFRATRE1,MFANNRE1,MFIMDRE1,MFIMCRE1,MFCODRE2,MFTRIRE2,MFRATRE2,MFANNRE2,MFIMDRE2,MFIMCRE2,MFCODRE3,MFTRIRE3,MFRATRE3,MFANNRE3,MFIMDRE3,MFIMCRE3,MFCODRE4,MFTRIRE4,MFRATRE4,MFANNRE4,MFIMDRE4,MFIMCRE4,MFTOTDRE,MFTOTCRE,MFSALDRE,MFSINAI1,MF_NPOS1,MF_PACC1,MF_NRIF1,MFCAUSA1,MFIMDIL1,MFIMCIL1,MFSINAI2,MF_NPOS2,MF_PACC2,MF_NRIF2,MFCAUSA2,MFIMDIL2,MFIMCIL2,MFSINAI3,MF_NPOS3,MF_PACC3,MF_NRIF3,MFCAUSA3,MFIMDIL3,MFIMCIL3,MFTDINAI,MFTCINAI,MFSALINA,MFSALFIN,MFDESSTA,MFTIPMOD  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MFSERIAL;
                  ,this.w_MFMESRIF;
                  ,this.w_MFANNRIF;
                  ,this.w_MFVALUTA;
                  ,this.w_MF_COINC;
                  ,this.w_MFCODUFF;
                  ,this.w_MFCODATT;
                  ,this.w_MFCDSED1;
                  ,this.w_MFCCONT1;
                  ,this.w_MFMINPS1;
                  ,this.w_MFDAMES1;
                  ,this.w_MFDAANN1;
                  ,this.w_MF_AMES1;
                  ,this.w_MFAN1;
                  ,this.w_MFIMPSD1;
                  ,this.w_MFIMPSC1;
                  ,this.w_MFCDSED2;
                  ,this.w_MFCCONT2;
                  ,this.w_MFMINPS2;
                  ,this.w_MFDAMES2;
                  ,this.w_MFDAANN2;
                  ,this.w_MF_AMES2;
                  ,this.w_MFAN2;
                  ,this.w_MFIMPSD2;
                  ,this.w_MFIMPSC2;
                  ,this.w_MFCDSED3;
                  ,this.w_MFCCONT3;
                  ,this.w_MFMINPS3;
                  ,this.w_MFDAMES3;
                  ,this.w_MFDAANN3;
                  ,this.w_MF_AMES3;
                  ,this.w_MFAN3;
                  ,this.w_MFIMPSD3;
                  ,this.w_MFIMPSC3;
                  ,this.w_MFCDSED4;
                  ,this.w_MFCCONT4;
                  ,this.w_MFMINPS4;
                  ,this.w_MFDAMES4;
                  ,this.w_MFDAANN4;
                  ,this.w_MF_AMES4;
                  ,this.w_MFAN4;
                  ,this.w_MFIMPSD4;
                  ,this.w_MFIMPSC4;
                  ,this.w_MFTOTDPS;
                  ,this.w_MFTOTCPS;
                  ,this.w_MFSALDPS;
                  ,this.w_MFCODRE1;
                  ,this.w_MFTRIRE1;
                  ,this.w_MFRATRE1;
                  ,this.w_MFANNRE1;
                  ,this.w_MFIMDRE1;
                  ,this.w_MFIMCRE1;
                  ,this.w_MFCODRE2;
                  ,this.w_MFTRIRE2;
                  ,this.w_MFRATRE2;
                  ,this.w_MFANNRE2;
                  ,this.w_MFIMDRE2;
                  ,this.w_MFIMCRE2;
                  ,this.w_MFCODRE3;
                  ,this.w_MFTRIRE3;
                  ,this.w_MFRATRE3;
                  ,this.w_MFANNRE3;
                  ,this.w_MFIMDRE3;
                  ,this.w_MFIMCRE3;
                  ,this.w_MFCODRE4;
                  ,this.w_MFTRIRE4;
                  ,this.w_MFRATRE4;
                  ,this.w_MFANNRE4;
                  ,this.w_MFIMDRE4;
                  ,this.w_MFIMCRE4;
                  ,this.w_MFTOTDRE;
                  ,this.w_MFTOTCRE;
                  ,this.w_MFSALDRE;
                  ,this.w_MFSINAI1;
                  ,this.w_MF_NPOS1;
                  ,this.w_MF_PACC1;
                  ,this.w_MF_NRIF1;
                  ,this.w_MFCAUSA1;
                  ,this.w_MFIMDIL1;
                  ,this.w_MFIMCIL1;
                  ,this.w_MFSINAI2;
                  ,this.w_MF_NPOS2;
                  ,this.w_MF_PACC2;
                  ,this.w_MF_NRIF2;
                  ,this.w_MFCAUSA2;
                  ,this.w_MFIMDIL2;
                  ,this.w_MFIMCIL2;
                  ,this.w_MFSINAI3;
                  ,this.w_MF_NPOS3;
                  ,this.w_MF_PACC3;
                  ,this.w_MF_NRIF3;
                  ,this.w_MFCAUSA3;
                  ,this.w_MFIMDIL3;
                  ,this.w_MFIMCIL3;
                  ,this.w_MFTDINAI;
                  ,this.w_MFTCINAI;
                  ,this.w_MFSALINA;
                  ,this.w_MFSALFIN;
                  ,this.w_MFDESSTA;
                  ,this.w_MFTIPMOD;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- gscg_aon
    * Forza aggiornamento del database
    this.bupdated=.t.
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOD_PAG_IDX,i_nConn)
      *
      * update MOD_PAG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_PAG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MFMESRIF="+cp_ToStrODBC(this.w_MFMESRIF)+;
             ",MFANNRIF="+cp_ToStrODBC(this.w_MFANNRIF)+;
             ",MFVALUTA="+cp_ToStrODBC(this.w_MFVALUTA)+;
             ",MF_COINC="+cp_ToStrODBC(this.w_MF_COINC)+;
             ",MFCODUFF="+cp_ToStrODBCNull(this.w_MFCODUFF)+;
             ",MFCODATT="+cp_ToStrODBC(this.w_MFCODATT)+;
             ",MFCDSED1="+cp_ToStrODBCNull(this.w_MFCDSED1)+;
             ",MFCCONT1="+cp_ToStrODBCNull(this.w_MFCCONT1)+;
             ",MFMINPS1="+cp_ToStrODBC(this.w_MFMINPS1)+;
             ",MFDAMES1="+cp_ToStrODBC(this.w_MFDAMES1)+;
             ",MFDAANN1="+cp_ToStrODBC(this.w_MFDAANN1)+;
             ",MF_AMES1="+cp_ToStrODBC(this.w_MF_AMES1)+;
             ",MFAN1="+cp_ToStrODBC(this.w_MFAN1)+;
             ",MFIMPSD1="+cp_ToStrODBC(this.w_MFIMPSD1)+;
             ",MFIMPSC1="+cp_ToStrODBC(this.w_MFIMPSC1)+;
             ",MFCDSED2="+cp_ToStrODBCNull(this.w_MFCDSED2)+;
             ",MFCCONT2="+cp_ToStrODBCNull(this.w_MFCCONT2)+;
             ",MFMINPS2="+cp_ToStrODBC(this.w_MFMINPS2)+;
             ",MFDAMES2="+cp_ToStrODBC(this.w_MFDAMES2)+;
             ",MFDAANN2="+cp_ToStrODBC(this.w_MFDAANN2)+;
             ",MF_AMES2="+cp_ToStrODBC(this.w_MF_AMES2)+;
             ",MFAN2="+cp_ToStrODBC(this.w_MFAN2)+;
             ",MFIMPSD2="+cp_ToStrODBC(this.w_MFIMPSD2)+;
             ",MFIMPSC2="+cp_ToStrODBC(this.w_MFIMPSC2)+;
             ",MFCDSED3="+cp_ToStrODBCNull(this.w_MFCDSED3)+;
             ",MFCCONT3="+cp_ToStrODBCNull(this.w_MFCCONT3)+;
             ",MFMINPS3="+cp_ToStrODBC(this.w_MFMINPS3)+;
             ",MFDAMES3="+cp_ToStrODBC(this.w_MFDAMES3)+;
             ",MFDAANN3="+cp_ToStrODBC(this.w_MFDAANN3)+;
             ",MF_AMES3="+cp_ToStrODBC(this.w_MF_AMES3)+;
             ",MFAN3="+cp_ToStrODBC(this.w_MFAN3)+;
             ",MFIMPSD3="+cp_ToStrODBC(this.w_MFIMPSD3)+;
             ",MFIMPSC3="+cp_ToStrODBC(this.w_MFIMPSC3)+;
             ",MFCDSED4="+cp_ToStrODBCNull(this.w_MFCDSED4)+;
             ",MFCCONT4="+cp_ToStrODBCNull(this.w_MFCCONT4)+;
             ",MFMINPS4="+cp_ToStrODBC(this.w_MFMINPS4)+;
             ",MFDAMES4="+cp_ToStrODBC(this.w_MFDAMES4)+;
             ",MFDAANN4="+cp_ToStrODBC(this.w_MFDAANN4)+;
             ",MF_AMES4="+cp_ToStrODBC(this.w_MF_AMES4)+;
             ",MFAN4="+cp_ToStrODBC(this.w_MFAN4)+;
             ",MFIMPSD4="+cp_ToStrODBC(this.w_MFIMPSD4)+;
             ",MFIMPSC4="+cp_ToStrODBC(this.w_MFIMPSC4)+;
             ",MFTOTDPS="+cp_ToStrODBC(this.w_MFTOTDPS)+;
             ",MFTOTCPS="+cp_ToStrODBC(this.w_MFTOTCPS)+;
             ",MFSALDPS="+cp_ToStrODBC(this.w_MFSALDPS)+;
             ",MFCODRE1="+cp_ToStrODBCNull(this.w_MFCODRE1)+;
             ",MFTRIRE1="+cp_ToStrODBCNull(this.w_MFTRIRE1)+;
             ",MFRATRE1="+cp_ToStrODBC(this.w_MFRATRE1)+;
             ",MFANNRE1="+cp_ToStrODBC(this.w_MFANNRE1)+;
             ",MFIMDRE1="+cp_ToStrODBC(this.w_MFIMDRE1)+;
             ",MFIMCRE1="+cp_ToStrODBC(this.w_MFIMCRE1)+;
             ",MFCODRE2="+cp_ToStrODBCNull(this.w_MFCODRE2)+;
             ",MFTRIRE2="+cp_ToStrODBCNull(this.w_MFTRIRE2)+;
             ",MFRATRE2="+cp_ToStrODBC(this.w_MFRATRE2)+;
             ",MFANNRE2="+cp_ToStrODBC(this.w_MFANNRE2)+;
             ",MFIMDRE2="+cp_ToStrODBC(this.w_MFIMDRE2)+;
             ",MFIMCRE2="+cp_ToStrODBC(this.w_MFIMCRE2)+;
             ",MFCODRE3="+cp_ToStrODBCNull(this.w_MFCODRE3)+;
             ",MFTRIRE3="+cp_ToStrODBCNull(this.w_MFTRIRE3)+;
             ",MFRATRE3="+cp_ToStrODBC(this.w_MFRATRE3)+;
             ",MFANNRE3="+cp_ToStrODBC(this.w_MFANNRE3)+;
             ",MFIMDRE3="+cp_ToStrODBC(this.w_MFIMDRE3)+;
             ",MFIMCRE3="+cp_ToStrODBC(this.w_MFIMCRE3)+;
             ",MFCODRE4="+cp_ToStrODBCNull(this.w_MFCODRE4)+;
             ",MFTRIRE4="+cp_ToStrODBCNull(this.w_MFTRIRE4)+;
             ",MFRATRE4="+cp_ToStrODBC(this.w_MFRATRE4)+;
             ",MFANNRE4="+cp_ToStrODBC(this.w_MFANNRE4)+;
             ",MFIMDRE4="+cp_ToStrODBC(this.w_MFIMDRE4)+;
             ",MFIMCRE4="+cp_ToStrODBC(this.w_MFIMCRE4)+;
             ",MFTOTDRE="+cp_ToStrODBC(this.w_MFTOTDRE)+;
             ",MFTOTCRE="+cp_ToStrODBC(this.w_MFTOTCRE)+;
             ",MFSALDRE="+cp_ToStrODBC(this.w_MFSALDRE)+;
             ",MFSINAI1="+cp_ToStrODBCNull(this.w_MFSINAI1)+;
             ",MF_NPOS1="+cp_ToStrODBC(this.w_MF_NPOS1)+;
             ",MF_PACC1="+cp_ToStrODBC(this.w_MF_PACC1)+;
             ",MF_NRIF1="+cp_ToStrODBC(this.w_MF_NRIF1)+;
             ",MFCAUSA1="+cp_ToStrODBC(this.w_MFCAUSA1)+;
             ",MFIMDIL1="+cp_ToStrODBC(this.w_MFIMDIL1)+;
             ",MFIMCIL1="+cp_ToStrODBC(this.w_MFIMCIL1)+;
             ",MFSINAI2="+cp_ToStrODBCNull(this.w_MFSINAI2)+;
             ",MF_NPOS2="+cp_ToStrODBC(this.w_MF_NPOS2)+;
             ",MF_PACC2="+cp_ToStrODBC(this.w_MF_PACC2)+;
             ",MF_NRIF2="+cp_ToStrODBC(this.w_MF_NRIF2)+;
             ",MFCAUSA2="+cp_ToStrODBC(this.w_MFCAUSA2)+;
             ",MFIMDIL2="+cp_ToStrODBC(this.w_MFIMDIL2)+;
             ",MFIMCIL2="+cp_ToStrODBC(this.w_MFIMCIL2)+;
             ",MFSINAI3="+cp_ToStrODBCNull(this.w_MFSINAI3)+;
             ",MF_NPOS3="+cp_ToStrODBC(this.w_MF_NPOS3)+;
             ",MF_PACC3="+cp_ToStrODBC(this.w_MF_PACC3)+;
             ",MF_NRIF3="+cp_ToStrODBC(this.w_MF_NRIF3)+;
             ",MFCAUSA3="+cp_ToStrODBC(this.w_MFCAUSA3)+;
             ",MFIMDIL3="+cp_ToStrODBC(this.w_MFIMDIL3)+;
             ",MFIMCIL3="+cp_ToStrODBC(this.w_MFIMCIL3)+;
             ",MFTDINAI="+cp_ToStrODBC(this.w_MFTDINAI)+;
             ",MFTCINAI="+cp_ToStrODBC(this.w_MFTCINAI)+;
             ",MFSALINA="+cp_ToStrODBC(this.w_MFSALINA)+;
             ",MFSALFIN="+cp_ToStrODBC(this.w_MFSALFIN)+;
             ",MFDESSTA="+cp_ToStrODBCNull(this.w_MFDESSTA)+;
             ",MFTIPMOD="+cp_ToStrODBC(this.w_MFTIPMOD)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_PAG')
        i_cWhere = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
        UPDATE (i_cTable) SET;
              MFMESRIF=this.w_MFMESRIF;
             ,MFANNRIF=this.w_MFANNRIF;
             ,MFVALUTA=this.w_MFVALUTA;
             ,MF_COINC=this.w_MF_COINC;
             ,MFCODUFF=this.w_MFCODUFF;
             ,MFCODATT=this.w_MFCODATT;
             ,MFCDSED1=this.w_MFCDSED1;
             ,MFCCONT1=this.w_MFCCONT1;
             ,MFMINPS1=this.w_MFMINPS1;
             ,MFDAMES1=this.w_MFDAMES1;
             ,MFDAANN1=this.w_MFDAANN1;
             ,MF_AMES1=this.w_MF_AMES1;
             ,MFAN1=this.w_MFAN1;
             ,MFIMPSD1=this.w_MFIMPSD1;
             ,MFIMPSC1=this.w_MFIMPSC1;
             ,MFCDSED2=this.w_MFCDSED2;
             ,MFCCONT2=this.w_MFCCONT2;
             ,MFMINPS2=this.w_MFMINPS2;
             ,MFDAMES2=this.w_MFDAMES2;
             ,MFDAANN2=this.w_MFDAANN2;
             ,MF_AMES2=this.w_MF_AMES2;
             ,MFAN2=this.w_MFAN2;
             ,MFIMPSD2=this.w_MFIMPSD2;
             ,MFIMPSC2=this.w_MFIMPSC2;
             ,MFCDSED3=this.w_MFCDSED3;
             ,MFCCONT3=this.w_MFCCONT3;
             ,MFMINPS3=this.w_MFMINPS3;
             ,MFDAMES3=this.w_MFDAMES3;
             ,MFDAANN3=this.w_MFDAANN3;
             ,MF_AMES3=this.w_MF_AMES3;
             ,MFAN3=this.w_MFAN3;
             ,MFIMPSD3=this.w_MFIMPSD3;
             ,MFIMPSC3=this.w_MFIMPSC3;
             ,MFCDSED4=this.w_MFCDSED4;
             ,MFCCONT4=this.w_MFCCONT4;
             ,MFMINPS4=this.w_MFMINPS4;
             ,MFDAMES4=this.w_MFDAMES4;
             ,MFDAANN4=this.w_MFDAANN4;
             ,MF_AMES4=this.w_MF_AMES4;
             ,MFAN4=this.w_MFAN4;
             ,MFIMPSD4=this.w_MFIMPSD4;
             ,MFIMPSC4=this.w_MFIMPSC4;
             ,MFTOTDPS=this.w_MFTOTDPS;
             ,MFTOTCPS=this.w_MFTOTCPS;
             ,MFSALDPS=this.w_MFSALDPS;
             ,MFCODRE1=this.w_MFCODRE1;
             ,MFTRIRE1=this.w_MFTRIRE1;
             ,MFRATRE1=this.w_MFRATRE1;
             ,MFANNRE1=this.w_MFANNRE1;
             ,MFIMDRE1=this.w_MFIMDRE1;
             ,MFIMCRE1=this.w_MFIMCRE1;
             ,MFCODRE2=this.w_MFCODRE2;
             ,MFTRIRE2=this.w_MFTRIRE2;
             ,MFRATRE2=this.w_MFRATRE2;
             ,MFANNRE2=this.w_MFANNRE2;
             ,MFIMDRE2=this.w_MFIMDRE2;
             ,MFIMCRE2=this.w_MFIMCRE2;
             ,MFCODRE3=this.w_MFCODRE3;
             ,MFTRIRE3=this.w_MFTRIRE3;
             ,MFRATRE3=this.w_MFRATRE3;
             ,MFANNRE3=this.w_MFANNRE3;
             ,MFIMDRE3=this.w_MFIMDRE3;
             ,MFIMCRE3=this.w_MFIMCRE3;
             ,MFCODRE4=this.w_MFCODRE4;
             ,MFTRIRE4=this.w_MFTRIRE4;
             ,MFRATRE4=this.w_MFRATRE4;
             ,MFANNRE4=this.w_MFANNRE4;
             ,MFIMDRE4=this.w_MFIMDRE4;
             ,MFIMCRE4=this.w_MFIMCRE4;
             ,MFTOTDRE=this.w_MFTOTDRE;
             ,MFTOTCRE=this.w_MFTOTCRE;
             ,MFSALDRE=this.w_MFSALDRE;
             ,MFSINAI1=this.w_MFSINAI1;
             ,MF_NPOS1=this.w_MF_NPOS1;
             ,MF_PACC1=this.w_MF_PACC1;
             ,MF_NRIF1=this.w_MF_NRIF1;
             ,MFCAUSA1=this.w_MFCAUSA1;
             ,MFIMDIL1=this.w_MFIMDIL1;
             ,MFIMCIL1=this.w_MFIMCIL1;
             ,MFSINAI2=this.w_MFSINAI2;
             ,MF_NPOS2=this.w_MF_NPOS2;
             ,MF_PACC2=this.w_MF_PACC2;
             ,MF_NRIF2=this.w_MF_NRIF2;
             ,MFCAUSA2=this.w_MFCAUSA2;
             ,MFIMDIL2=this.w_MFIMDIL2;
             ,MFIMCIL2=this.w_MFIMCIL2;
             ,MFSINAI3=this.w_MFSINAI3;
             ,MF_NPOS3=this.w_MF_NPOS3;
             ,MF_PACC3=this.w_MF_PACC3;
             ,MF_NRIF3=this.w_MF_NRIF3;
             ,MFCAUSA3=this.w_MFCAUSA3;
             ,MFIMDIL3=this.w_MFIMDIL3;
             ,MFIMCIL3=this.w_MFIMCIL3;
             ,MFTDINAI=this.w_MFTDINAI;
             ,MFTCINAI=this.w_MFTCINAI;
             ,MFSALINA=this.w_MFSALINA;
             ,MFSALFIN=this.w_MFSALFIN;
             ,MFDESSTA=this.w_MFDESSTA;
             ,MFTIPMOD=this.w_MFTIPMOD;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCG_ACF : Saving
      this.GSCG_ACF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"CFSERIAL";
             )
      this.GSCG_ACF.mReplace()
      * --- GSCG_AFQ : Saving
      this.GSCG_AFQ.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"EFSERIAL";
             )
      this.GSCG_AFQ.mReplace()
      * --- GSCG_AVF : Saving
      this.GSCG_AVF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"VFSERIAL";
             )
      this.GSCG_AVF.mReplace()
      * --- GSCG_AIF : Saving
      this.GSCG_AIF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"IFSERIAL";
             )
      this.GSCG_AIF.mReplace()
      * --- GSCG_MAT : Saving
      this.GSCG_MAT.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"AFSERIAL";
             )
      this.GSCG_MAT.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCG_ACF : Deleting
    this.GSCG_ACF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"CFSERIAL";
           )
    this.GSCG_ACF.mDelete()
    * --- GSCG_AFQ : Deleting
    this.GSCG_AFQ.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"EFSERIAL";
           )
    this.GSCG_AFQ.mDelete()
    * --- GSCG_AVF : Deleting
    this.GSCG_AVF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"VFSERIAL";
           )
    this.GSCG_AVF.mDelete()
    * --- GSCG_AIF : Deleting
    this.GSCG_AIF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"IFSERIAL";
           )
    this.GSCG_AIF.mDelete()
    * --- GSCG_MAT : Deleting
    this.GSCG_MAT.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"AFSERIAL";
           )
    this.GSCG_MAT.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOD_PAG_IDX,i_nConn)
      *
      * delete MOD_PAG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(1,5,.t.)
        if .o_APPCOIN<>.w_APPCOIN
            .w_MF_COINC = .w_APPCOIN
        endif
        .DoRTCalc(7,10,.t.)
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_16.Calculate()
        .DoRTCalc(13,16,.t.)
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFCCONT1 = iif(empty(.w_MFCDSED1),' ',.w_MFCCONT1)
          .link_3_3('Full')
        endif
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFMINPS1 = iif(empty(.w_MFCDSED1),' ',.w_MFMINPS1)
        endif
        if .o_MFCCONT1<>.w_MFCCONT1
            .w_APP1A = !empty(.w_MFCCONT1) and alltrim(.w_MFCCONT1)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        endif
        if .o_MFCCONT1<>.w_MFCCONT1.or. .o_APP1A<>.w_APP1A
            .w_APP1 = !empty(.w_MFCCONT1) and (alltrim(.w_MFCCONT1)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP1A)
        endif
        if .o_MFCDSED1<>.w_MFCDSED1.or. .o_APP1A<>.w_APP1A
            .w_MFDAMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1A,' ',.w_MFDAMES1))
        endif
        if .o_APP1A<>.w_APP1A.or. .o_MFCDSED1<>.w_MFCDSED1
            .w_MFDAANN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1A,' ',.w_MFDAANN1))
        endif
        if .o_MFCDSED1<>.w_MFCDSED1.or. .o_APP1<>.w_APP1
            .w_MF_AMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,' ',.w_MF_AMES1))
        endif
        if .o_MFCDSED1<>.w_MFCDSED1.or. .o_APP1<>.w_APP1
            .w_MFAN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,' ',.w_MFAN1))
        endif
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFIMPSD1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSD1)
        endif
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFIMPSC1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSC1)
        endif
        .DoRTCalc(27,27,.t.)
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFCCONT2 = iif(empty(.w_MFCDSED2),' ',.w_MFCCONT2)
          .link_3_14('Full')
        endif
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFMINPS2 = iif(empty(.w_MFCDSED2),' ',.w_MFMINPS2)
        endif
        if .o_MFCCONT2<>.w_MFCCONT2
            .w_APP2A = !empty(.w_MFCCONT2) and alltrim(.w_MFCCONT2)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        endif
        if .o_APP2A<>.w_APP2A.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MFDAMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2A,' ',.w_MFDAMES2))
        endif
        if .o_APP2A<>.w_APP2A.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MFDAANN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2A,' ',.w_MFDAANN2))
        endif
        if .o_MFCCONT2<>.w_MFCCONT2.or. .o_APP2A<>.w_APP2A
            .w_APP2 = !empty(.w_MFCCONT2) and (alltrim(.w_MFCCONT2)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBTU-EBIT-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP2A)
        endif
        if .o_APP2<>.w_APP2.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MF_AMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,' ',.w_MF_AMES2))
        endif
        if .o_APP2<>.w_APP2.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MFAN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,' ',.w_MFAN2))
        endif
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFIMPSD2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSD2)
        endif
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFIMPSC2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSC2)
        endif
        .DoRTCalc(38,38,.t.)
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFCCONT3 = iif(empty(.w_MFCDSED3),' ',.w_MFCCONT3)
          .link_3_25('Full')
        endif
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFMINPS3 = iif(empty(.w_MFCDSED3),' ',.w_MFMINPS3)
        endif
        if .o_MFCCONT3<>.w_MFCCONT3
            .w_APP3A = !empty(.w_MFCCONT3) and alltrim(.w_MFCCONT3)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        endif
        if .o_APP3A<>.w_APP3A.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MFDAMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3A,' ',.w_MFDAMES3))
        endif
        if .o_MFCCONT3<>.w_MFCCONT3.or. .o_APP3A<>.w_APP3A
            .w_APP3 = !empty(.w_MFCCONT3) and (alltrim(.w_MFCCONT3)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP3A)
        endif
        if .o_APP3A<>.w_APP3A.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MFDAANN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3A,' ',.w_MFDAANN3))
        endif
        if .o_APP3<>.w_APP3.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MF_AMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,' ',.w_MF_AMES3))
        endif
        if .o_APP3<>.w_APP3.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MFAN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,' ',.w_MFAN3))
        endif
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFIMPSD3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSD3)
        endif
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFIMPSC3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSC3)
        endif
        .DoRTCalc(49,49,.t.)
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFCCONT4 = iif(empty(.w_MFCDSED4),' ',.w_MFCCONT4)
          .link_3_36('Full')
        endif
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFMINPS4 = iif(empty(.w_MFCDSED4),' ',.w_MFMINPS4)
        endif
        if .o_MFCCONT4<>.w_MFCCONT4
            .w_APP4A = !empty(.w_MFCCONT4) and alltrim(.w_MFCCONT4)$ 'ABR-ABS-AC-ACON-DOM2-DSOS-EMI-EBCE-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-TUEB-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        endif
        if .o_APP4A<>.w_APP4A.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MFDAMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4A,' ',.w_MFDAMES4))
        endif
        if .o_MFCCONT4<>.w_MFCCONT4.or. .o_APP4A<>.w_APP4A
            .w_APP4 = !empty(.w_MFCCONT4) and (alltrim(.w_MFCCONT4)$ 'AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBIT-EBTU-EMCO-EMDM-EMLA-FIPP-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-TCEB' or .w_APP4A)
        endif
        if .o_APP4A<>.w_APP4A.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MFDAANN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4A,' ',.w_MFDAANN4))
        endif
        if .o_APP4<>.w_APP4.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MF_AMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,' ',.w_MF_AMES4))
        endif
        if .o_APP4<>.w_APP4.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MFAN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,' ',.w_MFAN4))
        endif
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFIMPSD4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSD4)
        endif
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFIMPSC4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSC4)
        endif
            .w_MFTOTDPS = .w_MFIMPSD1+.w_MFIMPSD2+.w_MFIMPSD3+.w_MFIMPSD4
            .w_MFTOTCPS = .w_MFIMPSC1+.w_MFIMPSC2+.w_MFIMPSC3+.w_MFIMPSC4
            .w_MFSALDPS = .w_MFTOTDPS-.w_MFTOTCPS
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(65,68,.t.)
        if .o_MFCODRE1<>.w_MFCODRE1.or. .o_MFTRIRE1<>.w_MFTRIRE1
            .w_MFTRIRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFTRIRE1)
          .link_4_5('Full')
        endif
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFRATRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFRATRE1)
        endif
        if .o_MFCODRE1<>.w_MFCODRE1.or. .o_MFTRIRE1<>.w_MFTRIRE1
            .w_MFANNRE1 = iif(empty(.w_MFCODRE1),' ',iif(.w_MFTRIRE1= '3805','0',.w_MFANNRE1))
        endif
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFIMDRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMDRE1)
        endif
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFIMCRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMCRE1)
        endif
        .DoRTCalc(74,74,.t.)
        if .o_MFCODRE2<>.w_MFCODRE2.or. .o_MFTRIRE2<>.w_MFTRIRE2
            .w_MFTRIRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFTRIRE2)
          .link_4_18('Full')
        endif
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFRATRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFRATRE2)
        endif
        if .o_MFCODRE2<>.w_MFCODRE2.or. .o_MFTRIRE2<>.w_MFTRIRE2
            .w_MFANNRE2 = iif(empty(.w_MFCODRE2),' ',iif(.w_MFTRIRE2 ='3805','0',.w_MFANNRE2))
        endif
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFIMDRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMDRE2)
        endif
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFIMCRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMCRE2)
        endif
        .DoRTCalc(80,80,.t.)
        if .o_MFCODRE3<>.w_MFCODRE3.or. .o_MFTRIRE3<>.w_MFTRIRE3
            .w_MFTRIRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFTRIRE3)
          .link_4_24('Full')
        endif
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFRATRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFRATRE3)
        endif
        if .o_MFCODRE3<>.w_MFCODRE3.or. .o_MFTRIRE3<>.w_MFTRIRE3
            .w_MFANNRE3 = iif(empty(.w_MFCODRE3),' ',iif(.w_MFTRIRE3 = '3805','0',.w_MFANNRE3))
        endif
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFIMDRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMDRE3)
        endif
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFIMCRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMCRE3)
        endif
        .DoRTCalc(86,86,.t.)
        if .o_MFCODRE4<>.w_MFCODRE4.or. .o_MFTRIRE4<>.w_MFTRIRE4
            .w_MFTRIRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFTRIRE4)
          .link_4_30('Full')
        endif
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFRATRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFRATRE4)
        endif
        if .o_MFCODRE4<>.w_MFCODRE4.or. .o_MFTRIRE4<>.w_MFTRIRE4
            .w_MFANNRE4 = iif(empty(.w_MFCODRE4),' ',iif( .w_MFTRIRE4 = '3805','0',.w_MFANNRE4))
        endif
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFIMDRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMDRE4)
        endif
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFIMCRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMCRE4)
        endif
            .w_MFTOTDRE = .w_MFIMDRE1+.w_MFIMDRE2+.w_MFIMDRE3+.w_MFIMDRE4
            .w_MFTOTCRE = .w_MFIMCRE1+.w_MFIMCRE2+.w_MFIMCRE3+.w_MFIMCRE4
            .w_MFSALDRE = .w_MFTOTDRE-.w_MFTOTCRE
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(97,98,.t.)
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MF_NPOS1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NPOS1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MF_PACC1 = iif(empty(.w_MFSINAI1),' ',.w_MF_PACC1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MF_NRIF1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NRIF1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MFCAUSA1 = iif(empty(.w_MFSINAI1),' ',.w_MFCAUSA1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MFIMDIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMDIL1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MFIMCIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMCIL1)
        endif
        .DoRTCalc(105,105,.t.)
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MF_NPOS2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NPOS2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MF_PACC2 = iif(empty(.w_MFSINAI2),' ',.w_MF_PACC2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MF_NRIF2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NRIF2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MFCAUSA2 = iif(empty(.w_MFSINAI2),' ',.w_MFCAUSA2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MFIMDIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMDIL2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MFIMCIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMCIL2)
        endif
        .DoRTCalc(112,112,.t.)
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MF_NPOS3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NPOS3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MF_PACC3 = iif(empty(.w_MFSINAI3),' ',.w_MF_PACC3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MF_NRIF3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NRIF3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MFCAUSA3 = iif(empty(.w_MFSINAI3),' ',.w_MFCAUSA3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MFIMDIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMDIL3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MFIMCIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMCIL3)
        endif
            .w_MFTDINAI = .w_MFIMDIL1+.w_MFIMDIL2+.w_MFIMDIL3
            .w_MFTCINAI = .w_MFIMCIL1+.w_MFIMCIL2+.w_MFIMCIL3
            .w_MFSALINA = .w_MFTDINAI-.w_MFTCINAI
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(124,124,.t.)
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(127,130,.t.)
        if .o_MFSALAEN<>.w_MFSALAEN
            .w_MFSALFIN = .w_APPOIMP + .w_APPOIMP2 +  .w_MFSALDPS + .w_MFSALDRE + .w_MFSALINA +  .w_MFSALAEN
        endif
        .oPgFrm.Page7.oPag.oObj_7_9.Calculate()
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page6.oPag.oObj_6_11.Calculate()
        .DoRTCalc(134,142,.t.)
        if .o_MFDESSTA<>.w_MFDESSTA
            .w_CAP = iif(empty(.w_MFDESSTA),g_capazi,.w_CAP1)
        endif
        if .o_MFDESSTA<>.w_MFDESSTA
            .w_LOCALI = iif(empty(.w_MFDESSTA),g_locazi,.w_LOCALI1)
        endif
        if .o_MFDESSTA<>.w_MFDESSTA
            .w_PROVIN = iif(empty(.w_MFDESSTA),g_proazi,.w_PROVIN1)
        endif
        if .o_MFDESSTA<>.w_MFDESSTA
            .w_INDIRI = iif(empty(.w_MFDESSTA),g_indazi,.w_INDIRI1)
        endif
        .oPgFrm.Page6.oPag.oObj_6_15.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"MODEL","i_CODAZI,w_MFSERIAL")
          .op_MFSERIAL = .w_MFSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(147,150,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_16.Calculate()
        .oPgFrm.Page7.oPag.oObj_7_9.Calculate()
        .oPgFrm.Page6.oPag.oObj_6_11.Calculate()
        .oPgFrm.Page6.oPag.oObj_6_15.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.enabled = this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.enabled = this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES1_3_7.enabled = this.oPgFrm.Page3.oPag.oMFDAMES1_3_7.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN1_3_8.enabled = this.oPgFrm.Page3.oPag.oMFDAANN1_3_8.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES1_3_9.enabled = this.oPgFrm.Page3.oPag.oMF_AMES1_3_9.mCond()
    this.oPgFrm.Page3.oPag.oMFAN1_3_10.enabled = this.oPgFrm.Page3.oPag.oMFAN1_3_10.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD1_3_11.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD1_3_11.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC1_3_12.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC1_3_12.mCond()
    this.oPgFrm.Page3.oPag.oMFCCONT2_3_14.enabled = this.oPgFrm.Page3.oPag.oMFCCONT2_3_14.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS2_3_15.enabled = this.oPgFrm.Page3.oPag.oMFMINPS2_3_15.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES2_3_17.enabled = this.oPgFrm.Page3.oPag.oMFDAMES2_3_17.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN2_3_18.enabled = this.oPgFrm.Page3.oPag.oMFDAANN2_3_18.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES2_3_20.enabled = this.oPgFrm.Page3.oPag.oMF_AMES2_3_20.mCond()
    this.oPgFrm.Page3.oPag.oMFAN2_3_21.enabled = this.oPgFrm.Page3.oPag.oMFAN2_3_21.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD2_3_22.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD2_3_22.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC2_3_23.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC2_3_23.mCond()
    this.oPgFrm.Page3.oPag.oMFCCONT3_3_25.enabled = this.oPgFrm.Page3.oPag.oMFCCONT3_3_25.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS3_3_26.enabled = this.oPgFrm.Page3.oPag.oMFMINPS3_3_26.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES3_3_28.enabled = this.oPgFrm.Page3.oPag.oMFDAMES3_3_28.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN3_3_30.enabled = this.oPgFrm.Page3.oPag.oMFDAANN3_3_30.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES3_3_31.enabled = this.oPgFrm.Page3.oPag.oMF_AMES3_3_31.mCond()
    this.oPgFrm.Page3.oPag.oMFAN3_3_32.enabled = this.oPgFrm.Page3.oPag.oMFAN3_3_32.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD3_3_33.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD3_3_33.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC3_3_34.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC3_3_34.mCond()
    this.oPgFrm.Page3.oPag.oMFCCONT4_3_36.enabled = this.oPgFrm.Page3.oPag.oMFCCONT4_3_36.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS4_3_37.enabled = this.oPgFrm.Page3.oPag.oMFMINPS4_3_37.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES4_3_39.enabled = this.oPgFrm.Page3.oPag.oMFDAMES4_3_39.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN4_3_41.enabled = this.oPgFrm.Page3.oPag.oMFDAANN4_3_41.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES4_3_42.enabled = this.oPgFrm.Page3.oPag.oMF_AMES4_3_42.mCond()
    this.oPgFrm.Page3.oPag.oMFAN4_3_43.enabled = this.oPgFrm.Page3.oPag.oMFAN4_3_43.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD4_3_44.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD4_3_44.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC4_3_45.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC4_3_45.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE1_4_5.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE1_4_5.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE1_4_6.enabled = this.oPgFrm.Page4.oPag.oMFRATRE1_4_6.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE1_4_7.enabled = this.oPgFrm.Page4.oPag.oMFANNRE1_4_7.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE1_4_8.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE1_4_8.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE1_4_9.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE1_4_9.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE2_4_18.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE2_4_18.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE2_4_19.enabled = this.oPgFrm.Page4.oPag.oMFRATRE2_4_19.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE2_4_20.enabled = this.oPgFrm.Page4.oPag.oMFANNRE2_4_20.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE2_4_21.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE2_4_21.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE2_4_22.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE2_4_22.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE3_4_24.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE3_4_24.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE3_4_25.enabled = this.oPgFrm.Page4.oPag.oMFRATRE3_4_25.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE3_4_26.enabled = this.oPgFrm.Page4.oPag.oMFANNRE3_4_26.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE3_4_27.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE3_4_27.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE3_4_28.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE3_4_28.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE4_4_30.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE4_4_30.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE4_4_31.enabled = this.oPgFrm.Page4.oPag.oMFRATRE4_4_31.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE4_4_32.enabled = this.oPgFrm.Page4.oPag.oMFANNRE4_4_32.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE4_4_33.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE4_4_33.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE4_4_34.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE4_4_34.mCond()
    this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.enabled = this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.mCond()
    this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.enabled = this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.mCond()
    this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.enabled = this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.mCond()
    this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.enabled = this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.mCond()
    this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.enabled = this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.mCond()
    this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.enabled = this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.mCond()
    this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.enabled = this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.mCond()
    this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.enabled = this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.mCond()
    this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.enabled = this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.mCond()
    this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.enabled = this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.mCond()
    this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.enabled = this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.mCond()
    this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.enabled = this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.mCond()
    this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.enabled = this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.mCond()
    this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.enabled = this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.mCond()
    this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.enabled = this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.mCond()
    this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.enabled = this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.mCond()
    this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.enabled = this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.mCond()
    this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.enabled = this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oBtn_2_13.visible=!this.oPgFrm.Page2.oPag.oBtn_2_13.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_69.visible=!this.oPgFrm.Page3.oPag.oBtn_3_69.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_14.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_16.Event(cEvent)
      .oPgFrm.Page7.oPag.oObj_7_9.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_11.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_15.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MFCODUFF
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CODI_UFF_IDX,3]
    i_lTable = "CODI_UFF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CODI_UFF_IDX,2], .t., this.CODI_UFF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CODI_UFF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODUFF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AUF',True,'CODI_UFF')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UFCODICE like "+cp_ToStrODBC(trim(this.w_MFCODUFF)+"%");

          i_ret=cp_SQL(i_nConn,"select UFCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UFCODICE',trim(this.w_MFCODUFF))
          select UFCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODUFF)==trim(_Link_.UFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODUFF) and !this.bDontReportError
            deferred_cp_zoom('CODI_UFF','*','UFCODICE',cp_AbsName(oSource.parent,'oMFCODUFF_2_4'),i_cWhere,'GSCG_AUF',"Codici ufficio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',oSource.xKey(1))
            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODUFF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(this.w_MFCODUFF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',this.w_MFCODUFF)
            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODUFF = NVL(_Link_.UFCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODUFF = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CODI_UFF_IDX,2])+'\'+cp_ToStr(_Link_.UFCODICE,1)
      cp_ShowWarn(i_cKey,this.CODI_UFF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODUFF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED1
  func Link_3_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED1)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED1))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED1)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED1) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED1_3_2'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED1)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED1 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT1
  func Link_3_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT1))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT1)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT1) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT1_3_3'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT1)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT1 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED2
  func Link_3_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED2)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED2))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED2)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED2) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED2_3_13'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED2)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED2 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT2
  func Link_3_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT2)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT2))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT2)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT2) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT2_3_14'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT2)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT2 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED3
  func Link_3_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED3)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED3))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED3)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED3) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED3_3_24'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED3)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED3 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT3
  func Link_3_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT3)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT3))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT3)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT3) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT3_3_25'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT3)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT3 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED4
  func Link_3_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED4)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED4))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED4)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED4) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED4_3_35'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED4)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED4 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED4 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT4
  func Link_3_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT4)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT4))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT4)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT4) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT4_3_36'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT4)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT4 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT4 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCODRE1
  func Link_4_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE1)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE1))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE1)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE1) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE1_4_2'),i_cWhere,'GSCG_ARP',"Codici regione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE1)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE1 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE1 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE1
  func Link_4_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE1))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE1)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE1) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE1_4_5'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE1)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE1 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE1 = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Regione' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
        endif
        this.w_MFTRIRE1 = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_5.TRCODICE as TRCODICE405"+ ",link_4_5.TRTIPTRI as TRTIPTRI405"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_5 on MOD_PAG.MFTRIRE1=link_4_5.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_5"
          i_cKey=i_cKey+'+" and MOD_PAG.MFTRIRE1=link_4_5.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFCODRE2
  func Link_4_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE2)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE2))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE2)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE2) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE2_4_17'),i_cWhere,'GSCG_ARP',"Codici regione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE2)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE2 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE2 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE2
  func Link_4_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE2))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE2)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE2) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE2_4_18'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE2)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE2 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE2 = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Regione'  or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
        endif
        this.w_MFTRIRE2 = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_18.TRCODICE as TRCODICE418"+ ",link_4_18.TRTIPTRI as TRTIPTRI418"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_18 on MOD_PAG.MFTRIRE2=link_4_18.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_18"
          i_cKey=i_cKey+'+" and MOD_PAG.MFTRIRE2=link_4_18.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFCODRE3
  func Link_4_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE3)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE3))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE3)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE3) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE3_4_23'),i_cWhere,'GSCG_ARP',"Codici regione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE3)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE3 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE3 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE3
  func Link_4_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE3)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE3))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE3)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE3) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE3_4_24'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE3)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE3 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE3 = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Regione'   or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
        endif
        this.w_MFTRIRE3 = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_24.TRCODICE as TRCODICE424"+ ",link_4_24.TRTIPTRI as TRTIPTRI424"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_24 on MOD_PAG.MFTRIRE3=link_4_24.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_24"
          i_cKey=i_cKey+'+" and MOD_PAG.MFTRIRE3=link_4_24.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFCODRE4
  func Link_4_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE4)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE4))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE4)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE4) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE4_4_29'),i_cWhere,'GSCG_ARP',"Codici regione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE4)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE4 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE4 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE4
  func Link_4_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE4)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE4))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE4)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE4) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE4_4_30'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE4)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE4 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE4 = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Regione'  or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
        endif
        this.w_MFTRIRE4 = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_30.TRCODICE as TRCODICE430"+ ",link_4_30.TRTIPTRI as TRTIPTRI430"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_30 on MOD_PAG.MFTRIRE4=link_4_30.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_30"
          i_cKey=i_cKey+'+" and MOD_PAG.MFTRIRE4=link_4_30.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFSINAI1
  func Link_5_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_lTable = "SE_INAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2], .t., this.SE_INAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSINAI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ASI',True,'SE_INAIL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SICODICE like "+cp_ToStrODBC(trim(this.w_MFSINAI1)+"%");

          i_ret=cp_SQL(i_nConn,"select SICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SICODICE',trim(this.w_MFSINAI1))
          select SICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSINAI1)==trim(_Link_.SICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSINAI1) and !this.bDontReportError
            deferred_cp_zoom('SE_INAIL','*','SICODICE',cp_AbsName(oSource.parent,'oMFSINAI1_5_2'),i_cWhere,'GSCG_ASI',"Codici sede INAIL",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',oSource.xKey(1))
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSINAI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(this.w_MFSINAI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',this.w_MFSINAI1)
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSINAI1 = NVL(_Link_.SICODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSINAI1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])+'\'+cp_ToStr(_Link_.SICODICE,1)
      cp_ShowWarn(i_cKey,this.SE_INAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSINAI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFSINAI2
  func Link_5_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_lTable = "SE_INAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2], .t., this.SE_INAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSINAI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ASI',True,'SE_INAIL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SICODICE like "+cp_ToStrODBC(trim(this.w_MFSINAI2)+"%");

          i_ret=cp_SQL(i_nConn,"select SICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SICODICE',trim(this.w_MFSINAI2))
          select SICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSINAI2)==trim(_Link_.SICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSINAI2) and !this.bDontReportError
            deferred_cp_zoom('SE_INAIL','*','SICODICE',cp_AbsName(oSource.parent,'oMFSINAI2_5_9'),i_cWhere,'GSCG_ASI',"Codici sede INAIL",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',oSource.xKey(1))
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSINAI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(this.w_MFSINAI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',this.w_MFSINAI2)
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSINAI2 = NVL(_Link_.SICODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSINAI2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])+'\'+cp_ToStr(_Link_.SICODICE,1)
      cp_ShowWarn(i_cKey,this.SE_INAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSINAI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFSINAI3
  func Link_5_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_lTable = "SE_INAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2], .t., this.SE_INAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSINAI3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ASI',True,'SE_INAIL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SICODICE like "+cp_ToStrODBC(trim(this.w_MFSINAI3)+"%");

          i_ret=cp_SQL(i_nConn,"select SICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SICODICE',trim(this.w_MFSINAI3))
          select SICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSINAI3)==trim(_Link_.SICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSINAI3) and !this.bDontReportError
            deferred_cp_zoom('SE_INAIL','*','SICODICE',cp_AbsName(oSource.parent,'oMFSINAI3_5_16'),i_cWhere,'GSCG_ASI',"Codici sede INAIL",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',oSource.xKey(1))
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSINAI3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(this.w_MFSINAI3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',this.w_MFSINAI3)
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSINAI3 = NVL(_Link_.SICODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSINAI3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])+'\'+cp_ToStr(_Link_.SICODICE,1)
      cp_ShowWarn(i_cKey,this.SE_INAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSINAI3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFDESSTA
  func Link_7_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFDESSTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASE',True,'SEDIAZIE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODDES like "+cp_ToStrODBC(trim(this.w_MFDESSTA)+"%");
                   +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZIE);

          i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SECODAZI,SECODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SECODAZI',this.w_CODAZIE;
                     ,'SECODDES',trim(this.w_MFDESSTA))
          select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SECODAZI,SECODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFDESSTA)==trim(_Link_.SECODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFDESSTA) and !this.bDontReportError
            deferred_cp_zoom('SEDIAZIE','*','SECODAZI,SECODDES',cp_AbsName(oSource.parent,'oMFDESSTA_7_13'),i_cWhere,'GSAR_ASE',"Sedi aziende",'GSAR_ASI.SEDIAZIE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZIE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA";
                     +" from "+i_cTable+" "+i_lTable+" where SECODDES="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZIE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',oSource.xKey(1);
                       ,'SECODDES',oSource.xKey(2))
            select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFDESSTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA";
                   +" from "+i_cTable+" "+i_lTable+" where SECODDES="+cp_ToStrODBC(this.w_MFDESSTA);
                   +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZIE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',this.w_CODAZIE;
                       ,'SECODDES',this.w_MFDESSTA)
            select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFDESSTA = NVL(_Link_.SECODDES,space(5))
      this.w_CAP1 = NVL(_Link_.SE___CAP,space(9))
      this.w_LOCALI1 = NVL(_Link_.SELOCALI,space(30))
      this.w_PROVIN1 = NVL(_Link_.SEPROVIN,space(2))
      this.w_INDIRI1 = NVL(_Link_.SEINDIRI,space(35))
      this.w_DESSTA = NVL(_Link_.SEDESSTA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MFDESSTA = space(5)
      endif
      this.w_CAP1 = space(9)
      this.w_LOCALI1 = space(30)
      this.w_PROVIN1 = space(2)
      this.w_INDIRI1 = space(35)
      this.w_DESSTA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SECODAZI,1)+'\'+cp_ToStr(_Link_.SECODDES,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFDESSTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMFSERIAL_1_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page1.oPag.oMFSERIAL_1_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMFMESRIF_1_2.value==this.w_MFMESRIF)
      this.oPgFrm.Page1.oPag.oMFMESRIF_1_2.value=this.w_MFMESRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oMFANNRIF_1_3.value==this.w_MFANNRIF)
      this.oPgFrm.Page1.oPag.oMFANNRIF_1_3.value=this.w_MFANNRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oMF_COINC_1_7.RadioValue()==this.w_MF_COINC)
      this.oPgFrm.Page1.oPag.oMF_COINC_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMFSERIAL_2_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page2.oPag.oMFSERIAL_2_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page2.oPag.oMFCODUFF_2_4.value==this.w_MFCODUFF)
      this.oPgFrm.Page2.oPag.oMFCODUFF_2_4.value=this.w_MFCODUFF
    endif
    if not(this.oPgFrm.Page2.oPag.oMFCODATT_2_5.value==this.w_MFCODATT)
      this.oPgFrm.Page2.oPag.oMFCODATT_2_5.value=this.w_MFCODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oMESE_2_8.value==this.w_MESE)
      this.oPgFrm.Page2.oPag.oMESE_2_8.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page2.oPag.oANNO_2_9.value==this.w_ANNO)
      this.oPgFrm.Page2.oPag.oANNO_2_9.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page3.oPag.oMFSERIAL_3_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page3.oPag.oMFSERIAL_3_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED1_3_2.value==this.w_MFCDSED1)
      this.oPgFrm.Page3.oPag.oMFCDSED1_3_2.value=this.w_MFCDSED1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.value==this.w_MFCCONT1)
      this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.value=this.w_MFCCONT1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.value==this.w_MFMINPS1)
      this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.value=this.w_MFMINPS1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES1_3_7.value==this.w_MFDAMES1)
      this.oPgFrm.Page3.oPag.oMFDAMES1_3_7.value=this.w_MFDAMES1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN1_3_8.value==this.w_MFDAANN1)
      this.oPgFrm.Page3.oPag.oMFDAANN1_3_8.value=this.w_MFDAANN1
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES1_3_9.value==this.w_MF_AMES1)
      this.oPgFrm.Page3.oPag.oMF_AMES1_3_9.value=this.w_MF_AMES1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN1_3_10.value==this.w_MFAN1)
      this.oPgFrm.Page3.oPag.oMFAN1_3_10.value=this.w_MFAN1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD1_3_11.value==this.w_MFIMPSD1)
      this.oPgFrm.Page3.oPag.oMFIMPSD1_3_11.value=this.w_MFIMPSD1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC1_3_12.value==this.w_MFIMPSC1)
      this.oPgFrm.Page3.oPag.oMFIMPSC1_3_12.value=this.w_MFIMPSC1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED2_3_13.value==this.w_MFCDSED2)
      this.oPgFrm.Page3.oPag.oMFCDSED2_3_13.value=this.w_MFCDSED2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT2_3_14.value==this.w_MFCCONT2)
      this.oPgFrm.Page3.oPag.oMFCCONT2_3_14.value=this.w_MFCCONT2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS2_3_15.value==this.w_MFMINPS2)
      this.oPgFrm.Page3.oPag.oMFMINPS2_3_15.value=this.w_MFMINPS2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES2_3_17.value==this.w_MFDAMES2)
      this.oPgFrm.Page3.oPag.oMFDAMES2_3_17.value=this.w_MFDAMES2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN2_3_18.value==this.w_MFDAANN2)
      this.oPgFrm.Page3.oPag.oMFDAANN2_3_18.value=this.w_MFDAANN2
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES2_3_20.value==this.w_MF_AMES2)
      this.oPgFrm.Page3.oPag.oMF_AMES2_3_20.value=this.w_MF_AMES2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN2_3_21.value==this.w_MFAN2)
      this.oPgFrm.Page3.oPag.oMFAN2_3_21.value=this.w_MFAN2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD2_3_22.value==this.w_MFIMPSD2)
      this.oPgFrm.Page3.oPag.oMFIMPSD2_3_22.value=this.w_MFIMPSD2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC2_3_23.value==this.w_MFIMPSC2)
      this.oPgFrm.Page3.oPag.oMFIMPSC2_3_23.value=this.w_MFIMPSC2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED3_3_24.value==this.w_MFCDSED3)
      this.oPgFrm.Page3.oPag.oMFCDSED3_3_24.value=this.w_MFCDSED3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT3_3_25.value==this.w_MFCCONT3)
      this.oPgFrm.Page3.oPag.oMFCCONT3_3_25.value=this.w_MFCCONT3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS3_3_26.value==this.w_MFMINPS3)
      this.oPgFrm.Page3.oPag.oMFMINPS3_3_26.value=this.w_MFMINPS3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES3_3_28.value==this.w_MFDAMES3)
      this.oPgFrm.Page3.oPag.oMFDAMES3_3_28.value=this.w_MFDAMES3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN3_3_30.value==this.w_MFDAANN3)
      this.oPgFrm.Page3.oPag.oMFDAANN3_3_30.value=this.w_MFDAANN3
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES3_3_31.value==this.w_MF_AMES3)
      this.oPgFrm.Page3.oPag.oMF_AMES3_3_31.value=this.w_MF_AMES3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN3_3_32.value==this.w_MFAN3)
      this.oPgFrm.Page3.oPag.oMFAN3_3_32.value=this.w_MFAN3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD3_3_33.value==this.w_MFIMPSD3)
      this.oPgFrm.Page3.oPag.oMFIMPSD3_3_33.value=this.w_MFIMPSD3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC3_3_34.value==this.w_MFIMPSC3)
      this.oPgFrm.Page3.oPag.oMFIMPSC3_3_34.value=this.w_MFIMPSC3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED4_3_35.value==this.w_MFCDSED4)
      this.oPgFrm.Page3.oPag.oMFCDSED4_3_35.value=this.w_MFCDSED4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT4_3_36.value==this.w_MFCCONT4)
      this.oPgFrm.Page3.oPag.oMFCCONT4_3_36.value=this.w_MFCCONT4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS4_3_37.value==this.w_MFMINPS4)
      this.oPgFrm.Page3.oPag.oMFMINPS4_3_37.value=this.w_MFMINPS4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES4_3_39.value==this.w_MFDAMES4)
      this.oPgFrm.Page3.oPag.oMFDAMES4_3_39.value=this.w_MFDAMES4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN4_3_41.value==this.w_MFDAANN4)
      this.oPgFrm.Page3.oPag.oMFDAANN4_3_41.value=this.w_MFDAANN4
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES4_3_42.value==this.w_MF_AMES4)
      this.oPgFrm.Page3.oPag.oMF_AMES4_3_42.value=this.w_MF_AMES4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN4_3_43.value==this.w_MFAN4)
      this.oPgFrm.Page3.oPag.oMFAN4_3_43.value=this.w_MFAN4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD4_3_44.value==this.w_MFIMPSD4)
      this.oPgFrm.Page3.oPag.oMFIMPSD4_3_44.value=this.w_MFIMPSD4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC4_3_45.value==this.w_MFIMPSC4)
      this.oPgFrm.Page3.oPag.oMFIMPSC4_3_45.value=this.w_MFIMPSC4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFTOTDPS_3_59.value==this.w_MFTOTDPS)
      this.oPgFrm.Page3.oPag.oMFTOTDPS_3_59.value=this.w_MFTOTDPS
    endif
    if not(this.oPgFrm.Page3.oPag.oMFTOTCPS_3_60.value==this.w_MFTOTCPS)
      this.oPgFrm.Page3.oPag.oMFTOTCPS_3_60.value=this.w_MFTOTCPS
    endif
    if not(this.oPgFrm.Page3.oPag.oMFSALDPS_3_61.value==this.w_MFSALDPS)
      this.oPgFrm.Page3.oPag.oMFSALDPS_3_61.value=this.w_MFSALDPS
    endif
    if not(this.oPgFrm.Page3.oPag.oMESE_3_67.value==this.w_MESE)
      this.oPgFrm.Page3.oPag.oMESE_3_67.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page3.oPag.oANNO_3_68.value==this.w_ANNO)
      this.oPgFrm.Page3.oPag.oANNO_3_68.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page4.oPag.oMFSERIAL_4_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page4.oPag.oMFSERIAL_4_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE1_4_2.value==this.w_MFCODRE1)
      this.oPgFrm.Page4.oPag.oMFCODRE1_4_2.value=this.w_MFCODRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE1_4_5.value==this.w_MFTRIRE1)
      this.oPgFrm.Page4.oPag.oMFTRIRE1_4_5.value=this.w_MFTRIRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE1_4_6.value==this.w_MFRATRE1)
      this.oPgFrm.Page4.oPag.oMFRATRE1_4_6.value=this.w_MFRATRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE1_4_7.value==this.w_MFANNRE1)
      this.oPgFrm.Page4.oPag.oMFANNRE1_4_7.value=this.w_MFANNRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE1_4_8.value==this.w_MFIMDRE1)
      this.oPgFrm.Page4.oPag.oMFIMDRE1_4_8.value=this.w_MFIMDRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE1_4_9.value==this.w_MFIMCRE1)
      this.oPgFrm.Page4.oPag.oMFIMCRE1_4_9.value=this.w_MFIMCRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE2_4_17.value==this.w_MFCODRE2)
      this.oPgFrm.Page4.oPag.oMFCODRE2_4_17.value=this.w_MFCODRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE2_4_18.value==this.w_MFTRIRE2)
      this.oPgFrm.Page4.oPag.oMFTRIRE2_4_18.value=this.w_MFTRIRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE2_4_19.value==this.w_MFRATRE2)
      this.oPgFrm.Page4.oPag.oMFRATRE2_4_19.value=this.w_MFRATRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE2_4_20.value==this.w_MFANNRE2)
      this.oPgFrm.Page4.oPag.oMFANNRE2_4_20.value=this.w_MFANNRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE2_4_21.value==this.w_MFIMDRE2)
      this.oPgFrm.Page4.oPag.oMFIMDRE2_4_21.value=this.w_MFIMDRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE2_4_22.value==this.w_MFIMCRE2)
      this.oPgFrm.Page4.oPag.oMFIMCRE2_4_22.value=this.w_MFIMCRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE3_4_23.value==this.w_MFCODRE3)
      this.oPgFrm.Page4.oPag.oMFCODRE3_4_23.value=this.w_MFCODRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE3_4_24.value==this.w_MFTRIRE3)
      this.oPgFrm.Page4.oPag.oMFTRIRE3_4_24.value=this.w_MFTRIRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE3_4_25.value==this.w_MFRATRE3)
      this.oPgFrm.Page4.oPag.oMFRATRE3_4_25.value=this.w_MFRATRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE3_4_26.value==this.w_MFANNRE3)
      this.oPgFrm.Page4.oPag.oMFANNRE3_4_26.value=this.w_MFANNRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE3_4_27.value==this.w_MFIMDRE3)
      this.oPgFrm.Page4.oPag.oMFIMDRE3_4_27.value=this.w_MFIMDRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE3_4_28.value==this.w_MFIMCRE3)
      this.oPgFrm.Page4.oPag.oMFIMCRE3_4_28.value=this.w_MFIMCRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE4_4_29.value==this.w_MFCODRE4)
      this.oPgFrm.Page4.oPag.oMFCODRE4_4_29.value=this.w_MFCODRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE4_4_30.value==this.w_MFTRIRE4)
      this.oPgFrm.Page4.oPag.oMFTRIRE4_4_30.value=this.w_MFTRIRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE4_4_31.value==this.w_MFRATRE4)
      this.oPgFrm.Page4.oPag.oMFRATRE4_4_31.value=this.w_MFRATRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE4_4_32.value==this.w_MFANNRE4)
      this.oPgFrm.Page4.oPag.oMFANNRE4_4_32.value=this.w_MFANNRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE4_4_33.value==this.w_MFIMDRE4)
      this.oPgFrm.Page4.oPag.oMFIMDRE4_4_33.value=this.w_MFIMDRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE4_4_34.value==this.w_MFIMCRE4)
      this.oPgFrm.Page4.oPag.oMFIMCRE4_4_34.value=this.w_MFIMCRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTOTDRE_4_38.value==this.w_MFTOTDRE)
      this.oPgFrm.Page4.oPag.oMFTOTDRE_4_38.value=this.w_MFTOTDRE
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTOTCRE_4_39.value==this.w_MFTOTCRE)
      this.oPgFrm.Page4.oPag.oMFTOTCRE_4_39.value=this.w_MFTOTCRE
    endif
    if not(this.oPgFrm.Page4.oPag.oMFSALDRE_4_40.value==this.w_MFSALDRE)
      this.oPgFrm.Page4.oPag.oMFSALDRE_4_40.value=this.w_MFSALDRE
    endif
    if not(this.oPgFrm.Page4.oPag.oMESE_4_45.value==this.w_MESE)
      this.oPgFrm.Page4.oPag.oMESE_4_45.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page4.oPag.oANNO_4_46.value==this.w_ANNO)
      this.oPgFrm.Page4.oPag.oANNO_4_46.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSERIAL_5_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page5.oPag.oMFSERIAL_5_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSINAI1_5_2.value==this.w_MFSINAI1)
      this.oPgFrm.Page5.oPag.oMFSINAI1_5_2.value=this.w_MFSINAI1
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.value==this.w_MF_NPOS1)
      this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.value=this.w_MF_NPOS1
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.value==this.w_MF_PACC1)
      this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.value=this.w_MF_PACC1
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.value==this.w_MF_NRIF1)
      this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.value=this.w_MF_NRIF1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.value==this.w_MFCAUSA1)
      this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.value=this.w_MFCAUSA1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.value==this.w_MFIMDIL1)
      this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.value=this.w_MFIMDIL1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.value==this.w_MFIMCIL1)
      this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.value=this.w_MFIMCIL1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSINAI2_5_9.value==this.w_MFSINAI2)
      this.oPgFrm.Page5.oPag.oMFSINAI2_5_9.value=this.w_MFSINAI2
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.value==this.w_MF_NPOS2)
      this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.value=this.w_MF_NPOS2
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.value==this.w_MF_PACC2)
      this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.value=this.w_MF_PACC2
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.value==this.w_MF_NRIF2)
      this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.value=this.w_MF_NRIF2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.value==this.w_MFCAUSA2)
      this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.value=this.w_MFCAUSA2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.value==this.w_MFIMDIL2)
      this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.value=this.w_MFIMDIL2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.value==this.w_MFIMCIL2)
      this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.value=this.w_MFIMCIL2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSINAI3_5_16.value==this.w_MFSINAI3)
      this.oPgFrm.Page5.oPag.oMFSINAI3_5_16.value=this.w_MFSINAI3
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.value==this.w_MF_NPOS3)
      this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.value=this.w_MF_NPOS3
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.value==this.w_MF_PACC3)
      this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.value=this.w_MF_PACC3
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.value==this.w_MF_NRIF3)
      this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.value=this.w_MF_NRIF3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.value==this.w_MFCAUSA3)
      this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.value=this.w_MFCAUSA3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.value==this.w_MFIMDIL3)
      this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.value=this.w_MFIMDIL3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.value==this.w_MFIMCIL3)
      this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.value=this.w_MFIMCIL3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFTDINAI_5_32.value==this.w_MFTDINAI)
      this.oPgFrm.Page5.oPag.oMFTDINAI_5_32.value=this.w_MFTDINAI
    endif
    if not(this.oPgFrm.Page5.oPag.oMFTCINAI_5_33.value==this.w_MFTCINAI)
      this.oPgFrm.Page5.oPag.oMFTCINAI_5_33.value=this.w_MFTCINAI
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSALINA_5_34.value==this.w_MFSALINA)
      this.oPgFrm.Page5.oPag.oMFSALINA_5_34.value=this.w_MFSALINA
    endif
    if not(this.oPgFrm.Page5.oPag.oMESE_5_42.value==this.w_MESE)
      this.oPgFrm.Page5.oPag.oMESE_5_42.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page5.oPag.oANNO_5_43.value==this.w_ANNO)
      this.oPgFrm.Page5.oPag.oANNO_5_43.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page6.oPag.oMFSERIAL_6_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page6.oPag.oMFSERIAL_6_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page6.oPag.oMESE_6_6.value==this.w_MESE)
      this.oPgFrm.Page6.oPag.oMESE_6_6.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page6.oPag.oANNO_6_7.value==this.w_ANNO)
      this.oPgFrm.Page6.oPag.oANNO_6_7.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page7.oPag.oMFSERIAL_7_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page7.oPag.oMFSERIAL_7_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page7.oPag.oMFSALFIN_7_2.value==this.w_MFSALFIN)
      this.oPgFrm.Page7.oPag.oMFSALFIN_7_2.value=this.w_MFSALFIN
    endif
    if not(this.oPgFrm.Page7.oPag.oMESE_7_10.value==this.w_MESE)
      this.oPgFrm.Page7.oPag.oMESE_7_10.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page7.oPag.oANNO_7_11.value==this.w_ANNO)
      this.oPgFrm.Page7.oPag.oANNO_7_11.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page7.oPag.oMFDESSTA_7_13.value==this.w_MFDESSTA)
      this.oPgFrm.Page7.oPag.oMFDESSTA_7_13.value=this.w_MFDESSTA
    endif
    if not(this.oPgFrm.Page7.oPag.oCAP_7_18.value==this.w_CAP)
      this.oPgFrm.Page7.oPag.oCAP_7_18.value=this.w_CAP
    endif
    if not(this.oPgFrm.Page7.oPag.oLOCALI_7_19.value==this.w_LOCALI)
      this.oPgFrm.Page7.oPag.oLOCALI_7_19.value=this.w_LOCALI
    endif
    if not(this.oPgFrm.Page7.oPag.oPROVIN_7_20.value==this.w_PROVIN)
      this.oPgFrm.Page7.oPag.oPROVIN_7_20.value=this.w_PROVIN
    endif
    if not(this.oPgFrm.Page7.oPag.oINDIRI_7_21.value==this.w_INDIRI)
      this.oPgFrm.Page7.oPag.oINDIRI_7_21.value=this.w_INDIRI
    endif
    if not(this.oPgFrm.Page6.oPag.oMFSALAEN_6_13.value==this.w_MFSALAEN)
      this.oPgFrm.Page6.oPag.oMFSALAEN_6_13.value=this.w_MFSALAEN
    endif
    cp_SetControlsValueExtFlds(this,'MOD_PAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(not empty(.w_MFMESRIF) and val(.w_MFMESRIF)>= 1 and val(.w_MFMESRIF)<13)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMFMESRIF_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mese di riferimento non valido")
          case   not(not empty(.w_MFANNRIF) and val(.w_MFANNRIF)>1950 and val(.w_MFANNRIF)<2050)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMFANNRIF_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un anno compreso fra il 1950 e 2050")
          case   (empty(.w_MFCCONT1))  and (not empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT1_3_3.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_APP1A  Or  (!empty(.w_MFDAMES1) and 0<val(.w_MFDAMES1) and val(.w_MFDAMES1)<13))  and (not empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES1_3_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP1A  Or  (!empty(.w_MFDAANN1) and (val(.w_MFDAANN1)>=1996  and val(.w_MFDAANN1)<=2050)))  and (!empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN1_3_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(.w_APP1 Or (!empty(.w_MF_AMES1)  and 0<val(.w_MF_AMES1) and val(.w_MF_AMES1)<13))  and (!empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES1_3_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP1 Or (val(.w_MFAN1)<=2050 and(val(.w_MFDAANN1)<val(.w_MFAN1)or .w_MFDAANN1=.w_MFAN1 and val(.w_MFDAMES1)<=val(.w_MF_AMES1))))  and (!empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN1_3_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCONT2))  and (not empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT2_3_14.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_APP2A  Or  (!empty(.w_MFDAMES2) and 0<val(.w_MFDAMES2) and val(.w_MFDAMES2)<13))  and (not empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES2_3_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP2A  Or  (!empty(.w_MFDAANN2) and (val(.w_MFDAANN2)>=1996  and val(.w_MFDAANN2)<=2050)))  and (!empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN2_3_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(.w_APP2 Or (!empty(.w_MF_AMES2)  and 0<val(.w_MF_AMES2) and val(.w_MF_AMES2)<13))  and (!empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES2_3_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP2 Or (val(.w_MFAN2)<=2050 and(val(.w_MFDAANN2)<val(.w_MFAN2)or .w_MFDAANN2=.w_MFAN2 and val(.w_MFDAMES2)<=val(.w_MF_AMES2))))  and (!empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN2_3_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCONT3))  and (not empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT3_3_25.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_APP3A  Or  (!empty(.w_MFDAMES3) and 0<val(.w_MFDAMES3) and val(.w_MFDAMES3)<13))  and (not empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES3_3_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP3A  Or  (!empty(.w_MFDAANN3) and (val(.w_MFDAANN3)>=1996  and val(.w_MFDAANN3)<=2050)))  and (!empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN3_3_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(.w_APP3 Or (!empty(.w_MF_AMES3)  and 0<val(.w_MF_AMES3) and val(.w_MF_AMES3)<13))  and (!empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES3_3_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP3 Or (val(.w_MFAN3)<=2050 and(val(.w_MFDAANN3)<val(.w_MFAN3)or .w_MFDAANN3=.w_MFAN3 and val(.w_MFDAMES3)<=val(.w_MF_AMES3))))  and (!empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN3_3_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCONT4))  and (not empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT4_3_36.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT4)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_APP4A  Or  (!empty(.w_MFDAMES4) and 0<val(.w_MFDAMES4) and val(.w_MFDAMES4)<13))  and (not empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES4_3_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP4A  Or  (!empty(.w_MFDAANN4) and (val(.w_MFDAANN4)>=1996  and val(.w_MFDAANN4)<=2050)))  and (!empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN4_3_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(.w_APP4 Or (!empty(.w_MF_AMES4)  and 0<val(.w_MF_AMES4) and val(.w_MF_AMES4)<13))  and (!empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES4_3_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP4 Or (val(.w_MFAN4)<=2050 and(val(.w_MFDAANN4)<val(.w_MFAN4)or .w_MFDAANN4=.w_MFAN4 and val(.w_MFDAMES4)<=val(.w_MF_AMES4))))  and (!empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN4_3_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   ((empty(.w_MFTRIRE1)) or not(.w_TIPTRI='Regione' or empty(.w_TIPTRI)))  and (not empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE1_4_5.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
          case   not(empty(.w_MFRATRE1) or left(.w_MFRATRE1,2)<=right(.w_MFRATRE1,2) and len(alltrim(.w_MFRATRE1))=4)  and (not empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE1_4_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNRE1) and (val(.w_MFANNRE1)>=1996  and val(.w_MFANNRE1)<=2050  or val(.w_MFANNRE1)=0))  and (!empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE1_4_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050")
          case   ((empty(.w_MFTRIRE2)) or not(.w_TIPTRI='Regione'  or empty(.w_TIPTRI)))  and (not empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE2_4_18.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
          case   not(empty(.w_MFRATRE2) or left(.w_MFRATRE2,2)<=right(.w_MFRATRE2,2) and len(alltrim(.w_MFRATRE2))=4)  and (not empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE2_4_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNRE2) and (val(.w_MFANNRE2)>=1996  and val(.w_MFANNRE2)<=2050   or val(.w_MFANNRE2)=0))  and (!empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE2_4_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050")
          case   ((empty(.w_MFTRIRE3)) or not(.w_TIPTRI='Regione'   or empty(.w_TIPTRI)))  and (not empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE3_4_24.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE3)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
          case   not(empty(.w_MFRATRE3) or left(.w_MFRATRE3,2)<=right(.w_MFRATRE3,2) and len(alltrim(.w_MFRATRE3))=4)  and (not empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE3_4_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNRE3) and (val(.w_MFANNRE3)>=1996  and val(.w_MFANNRE3)<=2050  or val(.w_MFANNRE3)=0))  and (!empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE3_4_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050")
          case   ((empty(.w_MFTRIRE4)) or not(.w_TIPTRI='Regione'  or empty(.w_TIPTRI)))  and (not empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE4_4_30.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE4)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
          case   not(empty(.w_MFRATRE4) or left(.w_MFRATRE4,2)<=right(.w_MFRATRE4,2) and len(alltrim(.w_MFRATRE4))=4)  and (not empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE4_4_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(!empty(.w_MFANNRE4) and (val(.w_MFANNRE4)>=1996  and val(.w_MFANNRE4)<=2050   or val(.w_MFANNRE4)=0))  and (!empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE4_4_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCG_ACF.CheckForm()
      if i_bres
        i_bres=  .GSCG_ACF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_AFQ.CheckForm()
      if i_bres
        i_bres=  .GSCG_AFQ.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_AVF.CheckForm()
      if i_bres
        i_bres=  .GSCG_AVF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=7
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_AIF.CheckForm()
      if i_bres
        i_bres=  .GSCG_AIF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_MAT.CheckForm()
      if i_bres
        i_bres=  .GSCG_MAT.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=6
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gscg_aon
      if i_bRes
        .w_RESCHK=0
        .NotifyEvent('ControllaDati')
        if .w_RESCHK<>0
          i_bRes=.f.
        endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_APPCOIN = this.w_APPCOIN
    this.o_MFCDSED1 = this.w_MFCDSED1
    this.o_MFCCONT1 = this.w_MFCCONT1
    this.o_APP1A = this.w_APP1A
    this.o_APP1 = this.w_APP1
    this.o_MFCDSED2 = this.w_MFCDSED2
    this.o_MFCCONT2 = this.w_MFCCONT2
    this.o_APP2A = this.w_APP2A
    this.o_APP2 = this.w_APP2
    this.o_MFCDSED3 = this.w_MFCDSED3
    this.o_MFCCONT3 = this.w_MFCCONT3
    this.o_APP3A = this.w_APP3A
    this.o_APP3 = this.w_APP3
    this.o_MFCDSED4 = this.w_MFCDSED4
    this.o_MFCCONT4 = this.w_MFCCONT4
    this.o_APP4A = this.w_APP4A
    this.o_APP4 = this.w_APP4
    this.o_MFCODRE1 = this.w_MFCODRE1
    this.o_MFTRIRE1 = this.w_MFTRIRE1
    this.o_MFCODRE2 = this.w_MFCODRE2
    this.o_MFTRIRE2 = this.w_MFTRIRE2
    this.o_MFCODRE3 = this.w_MFCODRE3
    this.o_MFTRIRE3 = this.w_MFTRIRE3
    this.o_MFCODRE4 = this.w_MFCODRE4
    this.o_MFTRIRE4 = this.w_MFTRIRE4
    this.o_MFSINAI1 = this.w_MFSINAI1
    this.o_MFSINAI2 = this.w_MFSINAI2
    this.o_MFSINAI3 = this.w_MFSINAI3
    this.o_MFDESSTA = this.w_MFDESSTA
    this.o_MFSALAEN = this.w_MFSALAEN
    * --- GSCG_ACF : Depends On
    this.GSCG_ACF.SaveDependsOn()
    * --- GSCG_AFQ : Depends On
    this.GSCG_AFQ.SaveDependsOn()
    * --- GSCG_AVF : Depends On
    this.GSCG_AVF.SaveDependsOn()
    * --- GSCG_AIF : Depends On
    this.GSCG_AIF.SaveDependsOn()
    * --- GSCG_MAT : Depends On
    this.GSCG_MAT.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_aonPag1 as StdContainer
  Width  = 804
  height = 413
  stdWidth  = 804
  stdheight = 413
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_1_1 as StdField with uid="INGQIDSUEM",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero modello",;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=107, Top=19, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFMESRIF_1_2 as StdField with uid="HFOOEBHDWN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MFMESRIF", cQueryName = "MFMESRIF",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Mese di riferimento non valido",;
    HelpContextID = 217052684,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=363, Top=19, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMESRIF_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_MFMESRIF) and val(.w_MFMESRIF)>= 1 and val(.w_MFMESRIF)<13)
    endwith
    return bRes
  endfunc

  add object oMFANNRIF_1_3 as StdField with uid="SPMPJCSSZI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MFANNRIF", cQueryName = "MFANNRIF",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un anno compreso fra il 1950 e 2050",;
    HelpContextID = 212350476,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=388, Top=19, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  proc oMFANNRIF_1_3.mAfter
    with this.Parent.oContained
      .w_MFANNRIF=yy2yyyy(.w_MFANNRIF)
    endwith
  endproc

  func oMFANNRIF_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_MFANNRIF) and val(.w_MFANNRIF)>1950 and val(.w_MFANNRIF)<2050)
    endwith
    return bRes
  endfunc


  add object oObj_1_5 as cp_runprogram with uid="UGGQSTDUFI",left=10, top=438, width=134,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BAK('N','ANN')",;
    cEvent = "New record",;
    nPag=1;
    , HelpContextID = 267263514

  add object oMF_COINC_1_7 as StdCheck with uid="RKATVGKMBB",rtseq=6,rtrep=.f.,left=664, top=55, caption="",;
    HelpContextID = 206629367,;
    cFormVar="w_MF_COINC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMF_COINC_1_7.RadioValue()
    return(iif(this.value =1,'N',;
    'S'))
  endfunc
  func oMF_COINC_1_7.GetRadio()
    this.Parent.oContained.w_MF_COINC = this.RadioValue()
    return .t.
  endfunc

  func oMF_COINC_1_7.SetRadio()
    this.Parent.oContained.w_MF_COINC=trim(this.Parent.oContained.w_MF_COINC)
    this.value = ;
      iif(this.Parent.oContained.w_MF_COINC=='N',1,;
      0)
  endfunc


  add object oLinkPC_1_8 as stdDynamicChildContainer with uid="VFEXZQDZFH",left=5, top=69, width=755, height=303, bOnScreen=.t.;


  add object oStr_1_9 as StdString with uid="RIMPGFGUUC",Visible=.t., Left=345, Top=51,;
    Alignment=1, Width=308, Height=15,;
    Caption="Anno di imposta non coincidente con anno solare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="EFRBFXDZBK",Visible=.t., Left=5, Top=19,;
    Alignment=1, Width=98, Height=15,;
    Caption="Numero modello:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="DNRJNKBPHF",Visible=.t., Left=204, Top=19,;
    Alignment=1, Width=154, Height=15,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="IWKPFCUQCR",Visible=.t., Left=465, Top=20,;
    Alignment=1, Width=124, Height=15,;
    Caption="Valuta di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="IZPIVVEYIL",Visible=.t., Left=594, Top=20,;
    Alignment=0, Width=34, Height=18,;
    Caption="Euro"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgscg_aonPag2 as StdContainer
  Width  = 804
  height = 413
  stdWidth  = 804
  stdheight = 413
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_2_1 as StdField with uid="ODPEQKOEGB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero modello",;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=111, Top=18, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)


  add object oLinkPC_2_2 as stdDynamicChildContainer with uid="LPQACJIYQL",left=5, top=44, width=761, height=294, bOnScreen=.t.;


  add object oMFCODUFF_2_4 as StdField with uid="RYJFAXKYGF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MFCODUFF", cQueryName = "MFCODUFF",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice ufficio",;
    HelpContextID = 252270092,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=100, Top=353, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CODI_UFF", cZoomOnZoom="GSCG_AUF", oKey_1_1="UFCODICE", oKey_1_2="this.w_MFCODUFF"

  func oMFCODUFF_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODUFF_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODUFF_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CODI_UFF','*','UFCODICE',cp_AbsName(this.parent,'oMFCODUFF_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AUF',"Codici ufficio",'',this.parent.oContained
  endproc
  proc oMFCODUFF_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AUF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UFCODICE=this.parent.oContained.w_MFCODUFF
     i_obj.ecpSave()
  endproc

  add object oMFCODATT_2_5 as StdField with uid="TNODRPVFFT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MFCODATT", cQueryName = "MFCODATT",;
    bObbl = .f. , nPag = 2, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice atto",;
    HelpContextID = 83274214,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=240, Top=353, cSayPict='"99999999999"', cGetPict='"99999999999"', InputMask=replicate('X',11)

  add object oMESE_2_8 as StdField with uid="KPWCJAHHDA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=330, Top=19, InputMask=replicate('X',2)

  add object oANNO_2_9 as StdField with uid="QEFDSTIJJN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=355, Top=19, InputMask=replicate('X',4)


  add object oBtn_2_13 as StdButton with uid="ZFJOVZKJND",left=733, top=351, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per visualizzare le distinte versamento di tipo I.R.PE.F.";
    , HelpContextID = 182357498;
    , tabstop=.f., caption='\<Distinte';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      with this.Parent.oContained
        GSRI_BVI(this.Parent.oContained,"IR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_Rite<>'S')
     endwith
    endif
  endfunc


  add object oObj_2_14 as cp_runprogram with uid="EVNNJKOTZA",left=-1, top=442, width=189,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="Gscg_Bvi('I')",;
    cEvent = "Insert end,Update end",;
    nPag=2;
    , HelpContextID = 267263514


  add object oObj_2_16 as cp_runprogram with uid="EWTIJKBMUU",left=-1, top=464, width=128,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="Gscg_Bvi('C')",;
    cEvent = "Delete end",;
    nPag=2;
    , HelpContextID = 267263514

  add object oStr_2_6 as StdString with uid="DUVZRBXUKS",Visible=.t., Left=13, Top=19,;
    Alignment=1, Width=95, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="EWLCKETTJZ",Visible=.t., Left=200, Top=19,;
    Alignment=1, Width=126, Height=15,;
    Caption="Periodo di riferimento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_11 as StdString with uid="NFEBJOXPXM",Visible=.t., Left=5, Top=356,;
    Alignment=1, Width=91, Height=15,;
    Caption="Codice ufficio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="LRITFRLPUR",Visible=.t., Left=165, Top=356,;
    Alignment=1, Width=71, Height=15,;
    Caption="Codice atto:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_afq",lower(this.oContained.GSCG_AFQ.class))=0
        this.oContained.GSCG_AFQ.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgscg_aonPag3 as StdContainer
  Width  = 804
  height = 413
  stdWidth  = 804
  stdheight = 413
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_3_1 as StdField with uid="UMTAXXYHVZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=118, Top=26, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFCDSED1_3_2 as StdField with uid="CKABJGIONW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MFCDSED1", cQueryName = "MFCDSED1",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 1157641,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=91, Top=131, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED1"

  func oMFCDSED1_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED1_3_2.ecpDrop(oSource)
    this.Parent.oContained.link_3_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED1_3_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED1_3_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED1_3_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED1
     i_obj.ecpSave()
  endproc

  add object oMFCCONT1_3_3 as StdField with uid="SPGALMZAEZ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MFCCONT1", cQueryName = "MFCCONT1",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 122857993,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=164, Top=131, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT1"

  func oMFCCONT1_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFCCONT1_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT1_3_3.ecpDrop(oSource)
    this.Parent.oContained.link_3_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT1_3_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT1_3_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT1_3_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT1
     i_obj.ecpSave()
  endproc

  add object oMFMINPS1_3_4 as StdField with uid="UZPQGAZZEX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MFMINPS1", cQueryName = "MFMINPS1",;
    bObbl = .f. , nPag = 3, value=space(22), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 89917961,;
   bGlobalFont=.t.,;
    Height=21, Width=200, Left=232, Top=131, InputMask=replicate('X',22)

  func oMFMINPS1_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  add object oMFDAMES1_3_7 as StdField with uid="LNKWVLBIEA",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MFDAMES1", cQueryName = "MFDAMES1",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7641609,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=496, Top=131, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES1_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFDAMES1_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP1A  Or  (!empty(.w_MFDAMES1) and 0<val(.w_MFDAMES1) and val(.w_MFDAMES1)<13))
    endwith
    return bRes
  endfunc

  add object oMFDAANN1_3_8 as StdField with uid="AKJYRQPYXF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MFDAANN1", cQueryName = "MFDAANN1",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 137665033,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=523, Top=131, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN1_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFDAANN1_3_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP1A  Or  (!empty(.w_MFDAANN1) and (val(.w_MFDAANN1)>=1996  and val(.w_MFDAANN1)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMF_AMES1_3_9 as StdField with uid="UGNEEGCWGA",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MF_AMES1", cQueryName = "MF_AMES1",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7531017,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=572, Top=131, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES1_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMF_AMES1_3_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP1 Or (!empty(.w_MF_AMES1)  and 0<val(.w_MF_AMES1) and val(.w_MF_AMES1)<13))
    endwith
    return bRes
  endfunc

  add object oMFAN1_3_10 as StdField with uid="HMOIVJLGQJ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MFAN1", cQueryName = "MFAN1",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 148387270,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=601, Top=131, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN1_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFAN1_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP1 Or (val(.w_MFAN1)<=2050 and(val(.w_MFDAANN1)<val(.w_MFAN1)or .w_MFDAANN1=.w_MFAN1 and val(.w_MFDAMES1)<=val(.w_MF_AMES1))))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD1_3_11 as StdField with uid="TCTJSLGIBA",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MFIMPSD1", cQueryName = "MFIMPSD1",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243401,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=91, Top=254, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMPSD1_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  add object oMFIMPSC1_3_12 as StdField with uid="FPDWLHKRZP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MFIMPSC1", cQueryName = "MFIMPSC1",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243401,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=255, Top=254, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMPSC1_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  add object oMFCDSED2_3_13 as StdField with uid="SXZYRVNBKS",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MFCDSED2", cQueryName = "MFCDSED2",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 1157640,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=91, Top=155, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED2"

  func oMFCDSED2_3_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED2_3_13.ecpDrop(oSource)
    this.Parent.oContained.link_3_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED2_3_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED2_3_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED2_3_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED2
     i_obj.ecpSave()
  endproc

  add object oMFCCONT2_3_14 as StdField with uid="AVTFDROKIG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MFCCONT2", cQueryName = "MFCCONT2",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 122857992,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=164, Top=155, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT2"

  func oMFCCONT2_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFCCONT2_3_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT2_3_14.ecpDrop(oSource)
    this.Parent.oContained.link_3_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT2_3_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT2_3_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT2_3_14.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT2
     i_obj.ecpSave()
  endproc

  add object oMFMINPS2_3_15 as StdField with uid="TLRSSESNPI",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MFMINPS2", cQueryName = "MFMINPS2",;
    bObbl = .f. , nPag = 3, value=space(22), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 89917960,;
   bGlobalFont=.t.,;
    Height=21, Width=200, Left=232, Top=155, InputMask=replicate('X',22)

  func oMFMINPS2_3_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  add object oMFDAMES2_3_17 as StdField with uid="HZGQCKOMWJ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_MFDAMES2", cQueryName = "MFDAMES2",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7641608,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=496, Top=155, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES2_3_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFDAMES2_3_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP2A  Or  (!empty(.w_MFDAMES2) and 0<val(.w_MFDAMES2) and val(.w_MFDAMES2)<13))
    endwith
    return bRes
  endfunc

  add object oMFDAANN2_3_18 as StdField with uid="WBAUDZABUC",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MFDAANN2", cQueryName = "MFDAANN2",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 137665032,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=523, Top=155, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN2_3_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFDAANN2_3_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP2A  Or  (!empty(.w_MFDAANN2) and (val(.w_MFDAANN2)>=1996  and val(.w_MFDAANN2)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMF_AMES2_3_20 as StdField with uid="XRSTGKETUN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_MF_AMES2", cQueryName = "MF_AMES2",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7531016,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=572, Top=155, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES2_3_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMF_AMES2_3_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP2 Or (!empty(.w_MF_AMES2)  and 0<val(.w_MF_AMES2) and val(.w_MF_AMES2)<13))
    endwith
    return bRes
  endfunc

  add object oMFAN2_3_21 as StdField with uid="LQXSVVFIUC",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MFAN2", cQueryName = "MFAN2",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 149435846,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=601, Top=155, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN2_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFAN2_3_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP2 Or (val(.w_MFAN2)<=2050 and(val(.w_MFDAANN2)<val(.w_MFAN2)or .w_MFDAANN2=.w_MFAN2 and val(.w_MFDAMES2)<=val(.w_MF_AMES2))))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD2_3_22 as StdField with uid="DPPNTFIXZF",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MFIMPSD2", cQueryName = "MFIMPSD2",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243400,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=91, Top=278, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMPSD2_3_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  add object oMFIMPSC2_3_23 as StdField with uid="DKBHHCVJQC",rtseq=37,rtrep=.f.,;
    cFormVar = "w_MFIMPSC2", cQueryName = "MFIMPSC2",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243400,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=255, Top=278, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMPSC2_3_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  add object oMFCDSED3_3_24 as StdField with uid="PMUMNAHIUI",rtseq=38,rtrep=.f.,;
    cFormVar = "w_MFCDSED3", cQueryName = "MFCDSED3",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 1157639,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=91, Top=179, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED3"

  func oMFCDSED3_3_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED3_3_24.ecpDrop(oSource)
    this.Parent.oContained.link_3_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED3_3_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED3_3_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED3_3_24.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED3
     i_obj.ecpSave()
  endproc

  add object oMFCCONT3_3_25 as StdField with uid="MUKHFMMIGC",rtseq=39,rtrep=.f.,;
    cFormVar = "w_MFCCONT3", cQueryName = "MFCCONT3",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 122857991,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=164, Top=179, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT3"

  func oMFCCONT3_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFCCONT3_3_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT3_3_25.ecpDrop(oSource)
    this.Parent.oContained.link_3_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT3_3_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT3_3_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT3_3_25.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT3
     i_obj.ecpSave()
  endproc

  add object oMFMINPS3_3_26 as StdField with uid="CPVWKREMYW",rtseq=40,rtrep=.f.,;
    cFormVar = "w_MFMINPS3", cQueryName = "MFMINPS3",;
    bObbl = .f. , nPag = 3, value=space(22), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 89917959,;
   bGlobalFont=.t.,;
    Height=21, Width=200, Left=232, Top=179, InputMask=replicate('X',22)

  func oMFMINPS3_3_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  add object oMFDAMES3_3_28 as StdField with uid="UCGDBCLFJK",rtseq=42,rtrep=.f.,;
    cFormVar = "w_MFDAMES3", cQueryName = "MFDAMES3",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7641607,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=496, Top=179, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES3_3_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFDAMES3_3_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP3A  Or  (!empty(.w_MFDAMES3) and 0<val(.w_MFDAMES3) and val(.w_MFDAMES3)<13))
    endwith
    return bRes
  endfunc

  add object oMFDAANN3_3_30 as StdField with uid="PYVGITCREO",rtseq=44,rtrep=.f.,;
    cFormVar = "w_MFDAANN3", cQueryName = "MFDAANN3",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 137665031,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=523, Top=179, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN3_3_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFDAANN3_3_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP3A  Or  (!empty(.w_MFDAANN3) and (val(.w_MFDAANN3)>=1996  and val(.w_MFDAANN3)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMF_AMES3_3_31 as StdField with uid="QTOOIBGIKI",rtseq=45,rtrep=.f.,;
    cFormVar = "w_MF_AMES3", cQueryName = "MF_AMES3",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7531015,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=572, Top=179, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES3_3_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMF_AMES3_3_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP3 Or (!empty(.w_MF_AMES3)  and 0<val(.w_MF_AMES3) and val(.w_MF_AMES3)<13))
    endwith
    return bRes
  endfunc

  add object oMFAN3_3_32 as StdField with uid="FWZATATEDM",rtseq=46,rtrep=.f.,;
    cFormVar = "w_MFAN3", cQueryName = "MFAN3",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 150484422,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=601, Top=179, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN3_3_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFAN3_3_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP3 Or (val(.w_MFAN3)<=2050 and(val(.w_MFDAANN3)<val(.w_MFAN3)or .w_MFDAANN3=.w_MFAN3 and val(.w_MFDAMES3)<=val(.w_MF_AMES3))))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD3_3_33 as StdField with uid="NRTFEMYREN",rtseq=47,rtrep=.f.,;
    cFormVar = "w_MFIMPSD3", cQueryName = "MFIMPSD3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243399,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=91, Top=302, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMPSD3_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  add object oMFIMPSC3_3_34 as StdField with uid="RMAERXBXYN",rtseq=48,rtrep=.f.,;
    cFormVar = "w_MFIMPSC3", cQueryName = "MFIMPSC3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243399,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=255, Top=302, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMPSC3_3_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  add object oMFCDSED4_3_35 as StdField with uid="RENIINYJRG",rtseq=49,rtrep=.f.,;
    cFormVar = "w_MFCDSED4", cQueryName = "MFCDSED4",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 1157638,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=91, Top=203, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED4"

  func oMFCDSED4_3_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED4_3_35.ecpDrop(oSource)
    this.Parent.oContained.link_3_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED4_3_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED4_3_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED4_3_35.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED4
     i_obj.ecpSave()
  endproc

  add object oMFCCONT4_3_36 as StdField with uid="ZYWRGKWSCK",rtseq=50,rtrep=.f.,;
    cFormVar = "w_MFCCONT4", cQueryName = "MFCCONT4",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 122857990,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=164, Top=203, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT4"

  func oMFCCONT4_3_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFCCONT4_3_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT4_3_36.ecpDrop(oSource)
    this.Parent.oContained.link_3_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT4_3_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT4_3_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT4_3_36.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT4
     i_obj.ecpSave()
  endproc

  add object oMFMINPS4_3_37 as StdField with uid="YUDKXKBRDK",rtseq=51,rtrep=.f.,;
    cFormVar = "w_MFMINPS4", cQueryName = "MFMINPS4",;
    bObbl = .f. , nPag = 3, value=space(22), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 89917958,;
   bGlobalFont=.t.,;
    Height=21, Width=200, Left=232, Top=203, InputMask=replicate('X',22)

  func oMFMINPS4_3_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  add object oMFDAMES4_3_39 as StdField with uid="IUDRVUKOUP",rtseq=53,rtrep=.f.,;
    cFormVar = "w_MFDAMES4", cQueryName = "MFDAMES4",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7641606,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=496, Top=203, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES4_3_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFDAMES4_3_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP4A  Or  (!empty(.w_MFDAMES4) and 0<val(.w_MFDAMES4) and val(.w_MFDAMES4)<13))
    endwith
    return bRes
  endfunc

  add object oMFDAANN4_3_41 as StdField with uid="HJXJWHWGKV",rtseq=55,rtrep=.f.,;
    cFormVar = "w_MFDAANN4", cQueryName = "MFDAANN4",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 137665030,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=523, Top=203, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN4_3_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFDAANN4_3_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP4A  Or  (!empty(.w_MFDAANN4) and (val(.w_MFDAANN4)>=1996  and val(.w_MFDAANN4)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMF_AMES4_3_42 as StdField with uid="RGSVDGJKVT",rtseq=56,rtrep=.f.,;
    cFormVar = "w_MF_AMES4", cQueryName = "MF_AMES4",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7531014,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=572, Top=203, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES4_3_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMF_AMES4_3_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP4 Or (!empty(.w_MF_AMES4)  and 0<val(.w_MF_AMES4) and val(.w_MF_AMES4)<13))
    endwith
    return bRes
  endfunc

  add object oMFAN4_3_43 as StdField with uid="CFHZNDFFLJ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_MFAN4", cQueryName = "MFAN4",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 151532998,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=601, Top=203, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN4_3_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFAN4_3_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP4 Or (val(.w_MFAN4)<=2050 and(val(.w_MFDAANN4)<val(.w_MFAN4)or .w_MFDAANN4=.w_MFAN4 and val(.w_MFDAMES4)<=val(.w_MF_AMES4))))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD4_3_44 as StdField with uid="MNPGVFYFDQ",rtseq=58,rtrep=.f.,;
    cFormVar = "w_MFIMPSD4", cQueryName = "MFIMPSD4",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243398,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=91, Top=326, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMPSD4_3_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  add object oMFIMPSC4_3_45 as StdField with uid="UYJTGTJJGS",rtseq=59,rtrep=.f.,;
    cFormVar = "w_MFIMPSC4", cQueryName = "MFIMPSC4",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243398,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=255, Top=326, cSayPict="'@Z '+ v_GV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMPSC4_3_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  add object oMFTOTDPS_3_59 as StdField with uid="XAUJQDXFLK",rtseq=60,rtrep=.f.,;
    cFormVar = "w_MFTOTDPS", cQueryName = "MFTOTDPS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 16095719,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=91, Top=357, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(72)"

  add object oMFTOTCPS_3_60 as StdField with uid="MGBYMLHXPA",rtseq=61,rtrep=.f.,;
    cFormVar = "w_MFTOTCPS", cQueryName = "MFTOTCPS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 32872935,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=255, Top=357, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  add object oMFSALDPS_3_61 as StdField with uid="JXAPRSKVXL",rtseq=62,rtrep=.f.,;
    cFormVar = "w_MFSALDPS", cQueryName = "MFSALDPS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25405927,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=399, Top=357, cSayPict="v_PV(76)", cGetPict="v_GV(76)"

  add object oMESE_3_67 as StdField with uid="BKZRGNZFUB",rtseq=63,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=353, Top=26, InputMask=replicate('X',2)

  add object oANNO_3_68 as StdField with uid="MJMYMEQJVT",rtseq=64,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=378, Top=26, InputMask=replicate('X',4)


  add object oBtn_3_69 as StdButton with uid="LBKQCEWXSG",left=734, top=350, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per visualizzare le distinte versamento di tipo I.N.P.S.";
    , HelpContextID = 182357498;
    , tabstop=.f., caption='\<Distinte';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_69.Click()
      with this.Parent.oContained
        GSRI_BVI(this.Parent.oContained,"IN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_69.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_Rite<>'S')
     endwith
    endif
  endfunc

  add object oStr_3_46 as StdString with uid="AYUEVOPXGG",Visible=.t., Left=92, Top=88,;
    Alignment=0, Width=42, Height=15,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_3_47 as StdString with uid="NBVWNRXNAQ",Visible=.t., Left=97, Top=105,;
    Alignment=0, Width=41, Height=15,;
    Caption="Sede"  ;
  , bGlobalFont=.t.

  add object oStr_3_48 as StdString with uid="RLQSQDFBLN",Visible=.t., Left=160, Top=88,;
    Alignment=0, Width=73, Height=15,;
    Caption="Causale"  ;
  , bGlobalFont=.t.

  add object oStr_3_49 as StdString with uid="VIPSVEHZFB",Visible=.t., Left=154, Top=105,;
    Alignment=0, Width=60, Height=15,;
    Caption="Contributo"  ;
  , bGlobalFont=.t.

  add object oStr_3_50 as StdString with uid="IYEELTZMSP",Visible=.t., Left=250, Top=89,;
    Alignment=0, Width=156, Height=15,;
    Caption="Matricola INPS/codice INPS/"  ;
  , bGlobalFont=.t.

  add object oStr_3_51 as StdString with uid="YEXOAXJKLC",Visible=.t., Left=292, Top=105,;
    Alignment=0, Width=104, Height=15,;
    Caption="Filiale azienda"  ;
  , bGlobalFont=.t.

  add object oStr_3_52 as StdString with uid="MFUFBDQZKN",Visible=.t., Left=502, Top=89,;
    Alignment=0, Width=123, Height=15,;
    Caption="Periodo di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_3_53 as StdString with uid="BOQOXBLNLK",Visible=.t., Left=480, Top=107,;
    Alignment=0, Width=85, Height=15,;
    Caption="da mm/aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_3_54 as StdString with uid="YZBSWXWFTX",Visible=.t., Left=569, Top=107,;
    Alignment=0, Width=68, Height=15,;
    Caption="a mm/aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_3_55 as StdString with uid="SYTXHRSHZV",Visible=.t., Left=91, Top=231,;
    Alignment=0, Width=154, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_3_56 as StdString with uid="HOACILSNNJ",Visible=.t., Left=253, Top=231,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_3_57 as StdString with uid="LGLKGDJLHM",Visible=.t., Left=36, Top=357,;
    Alignment=0, Width=49, Height=15,;
    Caption="Totale C"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_58 as StdString with uid="TZCTEEYMBI",Visible=.t., Left=234, Top=358,;
    Alignment=0, Width=15, Height=14,;
    Caption="D"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_62 as StdString with uid="XBSXKBBESY",Visible=.t., Left=412, Top=333,;
    Alignment=0, Width=79, Height=15,;
    Caption="Saldo (C-D)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_63 as StdString with uid="ASFKUCRSCQ",Visible=.t., Left=20, Top=26,;
    Alignment=1, Width=95, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_65 as StdString with uid="KPNHKKCGVH",Visible=.t., Left=2, Top=55,;
    Alignment=0, Width=82, Height=15,;
    Caption="Sezione INPS"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_66 as StdString with uid="IZXDXVWWJG",Visible=.t., Left=222, Top=26,;
    Alignment=1, Width=126, Height=15,;
    Caption="Periodo di riferimento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_3_64 as StdBox with uid="DBOPZLDODS",left=13, top=73, width=766,height=1
enddefine
define class tgscg_aonPag4 as StdContainer
  Width  = 804
  height = 413
  stdWidth  = 804
  stdheight = 413
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_4_1 as StdField with uid="WUJQAZLJAD",rtseq=67,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=121, Top=8, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFCODRE1_4_2 as StdField with uid="AIZVRMHYSY",rtseq=68,rtrep=.f.,;
    cFormVar = "w_MFCODRE1", cQueryName = "MFCODRE1",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 66497033,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=41, Left=26, Top=88, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE1"

  func oMFCODRE1_4_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE1_4_2.ecpDrop(oSource)
    this.Parent.oContained.link_4_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE1_4_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE1_4_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regione",'',this.parent.oContained
  endproc
  proc oMFCODRE1_4_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE1
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE1_4_5 as StdField with uid="EOPPRNCXED",rtseq=69,rtrep=.f.,;
    cFormVar = "w_MFTRIRE1", cQueryName = "MFTRIRE1",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'regione'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 60987913,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=88, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE1"

  func oMFTRIRE1_4_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFTRIRE1_4_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE1_4_5.ecpDrop(oSource)
    this.Parent.oContained.link_4_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE1_4_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE1_4_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE1_4_5.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE1
     i_obj.ecpSave()
  endproc

  add object oMFRATRE1_4_6 as StdField with uid="WJSZNHKTPL",rtseq=70,rtrep=.f.,;
    cFormVar = "w_MFRATRE1", cQueryName = "MFRATRE1",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 50575881,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=161, Top=88, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE1_4_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFRATRE1_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE1) or left(.w_MFRATRE1,2)<=right(.w_MFRATRE1,2) and len(alltrim(.w_MFRATRE1))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNRE1_4_7 as StdField with uid="MUARMFNAEO",rtseq=71,rtrep=.f.,;
    cFormVar = "w_MFANNRE1", cQueryName = "MFANNRE1",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 56085001,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=245, Top=88, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE1_4_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFANNRE1_4_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE1) and (val(.w_MFANNRE1)>=1996  and val(.w_MFANNRE1)<=2050  or val(.w_MFANNRE1)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE1_4_8 as StdField with uid="DHWEMFLHFR",rtseq=72,rtrep=.f.,;
    cFormVar = "w_MFIMDRE1", cQueryName = "MFIMDRE1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 66603529,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=308, Top=88, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMDRE1_4_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  add object oMFIMCRE1_4_9 as StdField with uid="UKNUQBICLX",rtseq=73,rtrep=.f.,;
    cFormVar = "w_MFIMCRE1", cQueryName = "MFIMCRE1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 67652105,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=470, Top=88, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMCRE1_4_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  add object oMFCODRE2_4_17 as StdField with uid="ZYXFOZVGTH",rtseq=74,rtrep=.f.,;
    cFormVar = "w_MFCODRE2", cQueryName = "MFCODRE2",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 66497032,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=26, Top=111, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE2"

  func oMFCODRE2_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE2_4_17.ecpDrop(oSource)
    this.Parent.oContained.link_4_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE2_4_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE2_4_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regione",'',this.parent.oContained
  endproc
  proc oMFCODRE2_4_17.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE2
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE2_4_18 as StdField with uid="FKBLLTEVWV",rtseq=75,rtrep=.f.,;
    cFormVar = "w_MFTRIRE2", cQueryName = "MFTRIRE2",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'regione'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 60987912,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=111, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE2"

  func oMFTRIRE2_4_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFTRIRE2_4_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE2_4_18.ecpDrop(oSource)
    this.Parent.oContained.link_4_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE2_4_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE2_4_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE2_4_18.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE2
     i_obj.ecpSave()
  endproc

  add object oMFRATRE2_4_19 as StdField with uid="UQVLTAPSHS",rtseq=76,rtrep=.f.,;
    cFormVar = "w_MFRATRE2", cQueryName = "MFRATRE2",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 50575880,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=161, Top=111, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE2_4_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFRATRE2_4_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE2) or left(.w_MFRATRE2,2)<=right(.w_MFRATRE2,2) and len(alltrim(.w_MFRATRE2))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNRE2_4_20 as StdField with uid="HEIMJNJWRR",rtseq=77,rtrep=.f.,;
    cFormVar = "w_MFANNRE2", cQueryName = "MFANNRE2",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 56085000,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=245, Top=111, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE2_4_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFANNRE2_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE2) and (val(.w_MFANNRE2)>=1996  and val(.w_MFANNRE2)<=2050   or val(.w_MFANNRE2)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE2_4_21 as StdField with uid="XOVRSESBNM",rtseq=78,rtrep=.f.,;
    cFormVar = "w_MFIMDRE2", cQueryName = "MFIMDRE2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 66603528,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=308, Top=111, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMDRE2_4_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  add object oMFIMCRE2_4_22 as StdField with uid="RCLUETTAKO",rtseq=79,rtrep=.f.,;
    cFormVar = "w_MFIMCRE2", cQueryName = "MFIMCRE2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 67652104,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=470, Top=111, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMCRE2_4_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  add object oMFCODRE3_4_23 as StdField with uid="ZVBRNRSCQE",rtseq=80,rtrep=.f.,;
    cFormVar = "w_MFCODRE3", cQueryName = "MFCODRE3",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 66497031,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=26, Top=134, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE3"

  func oMFCODRE3_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE3_4_23.ecpDrop(oSource)
    this.Parent.oContained.link_4_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE3_4_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE3_4_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regione",'',this.parent.oContained
  endproc
  proc oMFCODRE3_4_23.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE3
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE3_4_24 as StdField with uid="YCEJPTYMRK",rtseq=81,rtrep=.f.,;
    cFormVar = "w_MFTRIRE3", cQueryName = "MFTRIRE3",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'regione'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 60987911,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=134, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE3"

  func oMFTRIRE3_4_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFTRIRE3_4_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE3_4_24.ecpDrop(oSource)
    this.Parent.oContained.link_4_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE3_4_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE3_4_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE3_4_24.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE3
     i_obj.ecpSave()
  endproc

  add object oMFRATRE3_4_25 as StdField with uid="TWMNLHYLWV",rtseq=82,rtrep=.f.,;
    cFormVar = "w_MFRATRE3", cQueryName = "MFRATRE3",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 50575879,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=161, Top=134, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE3_4_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFRATRE3_4_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE3) or left(.w_MFRATRE3,2)<=right(.w_MFRATRE3,2) and len(alltrim(.w_MFRATRE3))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNRE3_4_26 as StdField with uid="SUHDLNHBSQ",rtseq=83,rtrep=.f.,;
    cFormVar = "w_MFANNRE3", cQueryName = "MFANNRE3",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 56084999,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=245, Top=134, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE3_4_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFANNRE3_4_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE3) and (val(.w_MFANNRE3)>=1996  and val(.w_MFANNRE3)<=2050  or val(.w_MFANNRE3)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE3_4_27 as StdField with uid="CYOULTQZVW",rtseq=84,rtrep=.f.,;
    cFormVar = "w_MFIMDRE3", cQueryName = "MFIMDRE3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 66603527,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=308, Top=134, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMDRE3_4_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  add object oMFIMCRE3_4_28 as StdField with uid="QQOFTRRZLZ",rtseq=85,rtrep=.f.,;
    cFormVar = "w_MFIMCRE3", cQueryName = "MFIMCRE3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 67652103,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=470, Top=134, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMCRE3_4_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  add object oMFCODRE4_4_29 as StdField with uid="MBZNNKESOD",rtseq=86,rtrep=.f.,;
    cFormVar = "w_MFCODRE4", cQueryName = "MFCODRE4",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 66497030,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=26, Top=157, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE4"

  func oMFCODRE4_4_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE4_4_29.ecpDrop(oSource)
    this.Parent.oContained.link_4_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE4_4_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE4_4_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regione",'',this.parent.oContained
  endproc
  proc oMFCODRE4_4_29.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE4
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE4_4_30 as StdField with uid="MJAPDUZSJV",rtseq=87,rtrep=.f.,;
    cFormVar = "w_MFTRIRE4", cQueryName = "MFTRIRE4",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'regione'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 60987910,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE4"

  func oMFTRIRE4_4_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFTRIRE4_4_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE4_4_30.ecpDrop(oSource)
    this.Parent.oContained.link_4_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE4_4_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE4_4_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE4_4_30.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE4
     i_obj.ecpSave()
  endproc

  add object oMFRATRE4_4_31 as StdField with uid="NDAJMIEPRZ",rtseq=88,rtrep=.f.,;
    cFormVar = "w_MFRATRE4", cQueryName = "MFRATRE4",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 50575878,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=161, Top=157, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE4_4_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFRATRE4_4_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE4) or left(.w_MFRATRE4,2)<=right(.w_MFRATRE4,2) and len(alltrim(.w_MFRATRE4))=4)
    endwith
    return bRes
  endfunc

  add object oMFANNRE4_4_32 as StdField with uid="BIEFIFIRQX",rtseq=89,rtrep=.f.,;
    cFormVar = "w_MFANNRE4", cQueryName = "MFANNRE4",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 56084998,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=245, Top=157, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE4_4_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFANNRE4_4_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE4) and (val(.w_MFANNRE4)>=1996  and val(.w_MFANNRE4)<=2050   or val(.w_MFANNRE4)=0))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE4_4_33 as StdField with uid="BCINHDFTDU",rtseq=90,rtrep=.f.,;
    cFormVar = "w_MFIMDRE4", cQueryName = "MFIMDRE4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 66603526,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=308, Top=157, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMDRE4_4_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  add object oMFIMCRE4_4_34 as StdField with uid="GJHJZPGKWU",rtseq=91,rtrep=.f.,;
    cFormVar = "w_MFIMCRE4", cQueryName = "MFIMCRE4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 67652102,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=470, Top=157, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMCRE4_4_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  add object oMFTOTDRE_4_38 as StdField with uid="RWVYQDEOZY",rtseq=92,rtrep=.f.,;
    cFormVar = "w_MFTOTDRE", cQueryName = "MFTOTDRE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 16095733,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=308, Top=187, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  add object oMFTOTCRE_4_39 as StdField with uid="NEFFOASBPH",rtseq=93,rtrep=.f.,;
    cFormVar = "w_MFTOTCRE", cQueryName = "MFTOTCRE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 32872949,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=470, Top=187, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  add object oMFSALDRE_4_40 as StdField with uid="ZPSTXWEYRU",rtseq=94,rtrep=.f.,;
    cFormVar = "w_MFSALDRE", cQueryName = "MFSALDRE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25405941,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=612, Top=187, cSayPict="v_PV(76)", cGetPict="v_GV(76)"

  add object oMESE_4_45 as StdField with uid="QDHEDGHHIW",rtseq=95,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=358, Top=9, InputMask=replicate('X',2)

  add object oANNO_4_46 as StdField with uid="EBJMOQWTXY",rtseq=96,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=383, Top=9, InputMask=replicate('X',4)


  add object oLinkPC_4_47 as stdDynamicChildContainer with uid="IJXOOZHGBN",left=1, top=213, width=802, height=198, bOnScreen=.t.;


  add object oStr_4_3 as StdString with uid="JMYELXYYLS",Visible=.t., Left=22, Top=56,;
    Alignment=0, Width=42, Height=15,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_4_4 as StdString with uid="QOAKWPNUWO",Visible=.t., Left=20, Top=72,;
    Alignment=0, Width=47, Height=15,;
    Caption="Regione"  ;
  , bGlobalFont=.t.

  add object oStr_4_10 as StdString with uid="AZYSIRFDIY",Visible=.t., Left=89, Top=55,;
    Alignment=0, Width=52, Height=15,;
    Caption="Cod."  ;
  , bGlobalFont=.t.

  add object oStr_4_11 as StdString with uid="UJCCQXXPKY",Visible=.t., Left=89, Top=72,;
    Alignment=0, Width=45, Height=15,;
    Caption="Tributo"  ;
  , bGlobalFont=.t.

  add object oStr_4_12 as StdString with uid="YQGQUJLBET",Visible=.t., Left=142, Top=72,;
    Alignment=2, Width=89, Height=15,;
    Caption="Rateazione"  ;
  , bGlobalFont=.t.

  add object oStr_4_13 as StdString with uid="TVCKYQGKBA",Visible=.t., Left=246, Top=55,;
    Alignment=0, Width=54, Height=15,;
    Caption="Anno di"  ;
  , bGlobalFont=.t.

  add object oStr_4_14 as StdString with uid="ETADKKPXVC",Visible=.t., Left=235, Top=71,;
    Alignment=0, Width=64, Height=15,;
    Caption="Riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_4_15 as StdString with uid="MVBKBDZKBM",Visible=.t., Left=307, Top=70,;
    Alignment=0, Width=154, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="NHHSLKGDGU",Visible=.t., Left=468, Top=70,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_4_35 as StdString with uid="KEBVMVBZNJ",Visible=.t., Left=255, Top=189,;
    Alignment=0, Width=49, Height=15,;
    Caption="Totale E"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_36 as StdString with uid="FIWKHDGIJY",Visible=.t., Left=627, Top=162,;
    Alignment=0, Width=76, Height=15,;
    Caption="Saldo (E-F)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_37 as StdString with uid="WDIGBVHLVI",Visible=.t., Left=449, Top=188,;
    Alignment=0, Width=18, Height=16,;
    Caption="F"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_42 as StdString with uid="WRQXUQEJZJ",Visible=.t., Left=23, Top=9,;
    Alignment=1, Width=95, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_43 as StdString with uid="XBBOUQJYEG",Visible=.t., Left=227, Top=10,;
    Alignment=1, Width=126, Height=15,;
    Caption="Periodo di riferimento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_44 as StdString with uid="DORQAGMARM",Visible=.t., Left=6, Top=31,;
    Alignment=0, Width=109, Height=15,;
    Caption="Sezione regioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_4_41 as StdBox with uid="KHJPYHBFQV",left=3, top=52, width=778,height=1
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_aif",lower(this.oContained.GSCG_AIF.class))=0
        this.oContained.GSCG_AIF.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgscg_aonPag5 as StdContainer
  Width  = 804
  height = 413
  stdWidth  = 804
  stdheight = 413
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_5_1 as StdField with uid="UHIVYKYYLY",rtseq=97,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 5, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=113, Top=24, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFSINAI1_5_2 as StdField with uid="VJCJNRLRZM",rtseq=98,rtrep=.f.,;
    cFormVar = "w_MFSINAI1", cQueryName = "MFSINAI1",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INAIL",;
    HelpContextID = 195319287,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=9, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SE_INAIL", cZoomOnZoom="GSCG_ASI", oKey_1_1="SICODICE", oKey_1_2="this.w_MFSINAI1"

  func oMFSINAI1_5_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSINAI1_5_2.ecpDrop(oSource)
    this.Parent.oContained.link_5_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSINAI1_5_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SE_INAIL','*','SICODICE',cp_AbsName(this.parent,'oMFSINAI1_5_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ASI',"Codici sede INAIL",'',this.parent.oContained
  endproc
  proc oMFSINAI1_5_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ASI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SICODICE=this.parent.oContained.w_MFSINAI1
     i_obj.ecpSave()
  endproc

  add object oMF_NPOS1_5_3 as StdField with uid="IOZFIFMCFH",rtseq=99,rtrep=.f.,;
    cFormVar = "w_MF_NPOS1", cQueryName = "MF_NPOS1",;
    bObbl = .f. , nPag = 5, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Numero posizione assicurativa",;
    HelpContextID = 104196617,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=82, Top=173, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8)

  func oMF_NPOS1_5_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMF_PACC1_5_4 as StdField with uid="ZRBNXSIDVP",rtseq=100,rtrep=.f.,;
    cFormVar = "w_MF_PACC1", cQueryName = "MF_PACC1",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "C.C. posizione assicurativa",;
    HelpContextID = 52685321,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=154, Top=173, InputMask=replicate('X',2)

  func oMF_PACC1_5_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMF_NRIF1_5_5 as StdField with uid="MNCHMZYNTX",rtseq=101,rtrep=.f.,;
    cFormVar = "w_MF_NRIF1", cQueryName = "MF_NRIF1",;
    bObbl = .f. , nPag = 5, value=space(7), bMultilanguage =  .f.,;
    ToolTipText = "Numero di riferimento",;
    HelpContextID = 65672695,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=192, Top=173, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',7)

  func oMF_NRIF1_5_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFCAUSA1_5_6 as StdField with uid="UYLFQZKBVP",rtseq=102,rtrep=.f.,;
    cFormVar = "w_MFCAUSA1", cQueryName = "MFCAUSA1",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Causale",;
    HelpContextID = 32811529,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=262, Top=173, cSayPict='repl("!",1)', cGetPict='repl("!",1)', InputMask=replicate('X',2)

  func oMFCAUSA1_5_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFIMDIL1_5_7 as StdField with uid="KHMXTBZGND",rtseq=103,rtrep=.f.,;
    cFormVar = "w_MFIMDIL1", cQueryName = "MFIMDIL1",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 50836983,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=309, Top=173, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMDIL1_5_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFIMCIL1_5_8 as StdField with uid="LCWCGXKFZS",rtseq=104,rtrep=.f.,;
    cFormVar = "w_MFIMCIL1", cQueryName = "MFIMCIL1",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 49788407,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=470, Top=173, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMCIL1_5_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFSINAI2_5_9 as StdField with uid="CRJUFFVUOV",rtseq=105,rtrep=.f.,;
    cFormVar = "w_MFSINAI2", cQueryName = "MFSINAI2",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INAIL",;
    HelpContextID = 195319288,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=9, Top=198, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SE_INAIL", cZoomOnZoom="GSCG_ASI", oKey_1_1="SICODICE", oKey_1_2="this.w_MFSINAI2"

  func oMFSINAI2_5_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSINAI2_5_9.ecpDrop(oSource)
    this.Parent.oContained.link_5_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSINAI2_5_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SE_INAIL','*','SICODICE',cp_AbsName(this.parent,'oMFSINAI2_5_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ASI',"Codici sede INAIL",'',this.parent.oContained
  endproc
  proc oMFSINAI2_5_9.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ASI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SICODICE=this.parent.oContained.w_MFSINAI2
     i_obj.ecpSave()
  endproc

  add object oMF_NPOS2_5_10 as StdField with uid="FUEQFYRIPH",rtseq=106,rtrep=.f.,;
    cFormVar = "w_MF_NPOS2", cQueryName = "MF_NPOS2",;
    bObbl = .f. , nPag = 5, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Numero posizione assicurativa",;
    HelpContextID = 104196616,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=82, Top=198, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8)

  func oMF_NPOS2_5_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMF_PACC2_5_11 as StdField with uid="TUSBHCGAVC",rtseq=107,rtrep=.f.,;
    cFormVar = "w_MF_PACC2", cQueryName = "MF_PACC2",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "C.C. posizione assicurativa",;
    HelpContextID = 52685320,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=154, Top=198, InputMask=replicate('X',2)

  func oMF_PACC2_5_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMF_NRIF2_5_12 as StdField with uid="DJFDGNQMDU",rtseq=108,rtrep=.f.,;
    cFormVar = "w_MF_NRIF2", cQueryName = "MF_NRIF2",;
    bObbl = .f. , nPag = 5, value=space(7), bMultilanguage =  .f.,;
    ToolTipText = "Numero di riferimento",;
    HelpContextID = 65672696,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=192, Top=198, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',7)

  func oMF_NRIF2_5_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFCAUSA2_5_13 as StdField with uid="HNWHAKLLNT",rtseq=109,rtrep=.f.,;
    cFormVar = "w_MFCAUSA2", cQueryName = "MFCAUSA2",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Causale",;
    HelpContextID = 32811528,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=262, Top=198, cSayPict='repl("!",1)', cGetPict='repl("!",1)', InputMask=replicate('X',2)

  func oMFCAUSA2_5_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFIMDIL2_5_14 as StdField with uid="KEXJJEVGQG",rtseq=110,rtrep=.f.,;
    cFormVar = "w_MFIMDIL2", cQueryName = "MFIMDIL2",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 50836984,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=309, Top=198, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMDIL2_5_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFIMCIL2_5_15 as StdField with uid="HAECOJYPAK",rtseq=111,rtrep=.f.,;
    cFormVar = "w_MFIMCIL2", cQueryName = "MFIMCIL2",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 49788408,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=470, Top=198, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMCIL2_5_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFSINAI3_5_16 as StdField with uid="XBUILCQVSF",rtseq=112,rtrep=.f.,;
    cFormVar = "w_MFSINAI3", cQueryName = "MFSINAI3",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INAIL",;
    HelpContextID = 195319289,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=9, Top=223, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SE_INAIL", cZoomOnZoom="GSCG_ASI", oKey_1_1="SICODICE", oKey_1_2="this.w_MFSINAI3"

  func oMFSINAI3_5_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSINAI3_5_16.ecpDrop(oSource)
    this.Parent.oContained.link_5_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSINAI3_5_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SE_INAIL','*','SICODICE',cp_AbsName(this.parent,'oMFSINAI3_5_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ASI',"Codici sede INAIL",'',this.parent.oContained
  endproc
  proc oMFSINAI3_5_16.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ASI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SICODICE=this.parent.oContained.w_MFSINAI3
     i_obj.ecpSave()
  endproc

  add object oMF_NPOS3_5_17 as StdField with uid="KYNIYEUWFN",rtseq=113,rtrep=.f.,;
    cFormVar = "w_MF_NPOS3", cQueryName = "MF_NPOS3",;
    bObbl = .f. , nPag = 5, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Numero posizione assicurativa",;
    HelpContextID = 104196615,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=82, Top=223, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8)

  func oMF_NPOS3_5_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMF_PACC3_5_18 as StdField with uid="RMJFRMESCX",rtseq=114,rtrep=.f.,;
    cFormVar = "w_MF_PACC3", cQueryName = "MF_PACC3",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "C.C. posizione assicurativa",;
    HelpContextID = 52685319,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=154, Top=223, InputMask=replicate('X',2)

  func oMF_PACC3_5_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMF_NRIF3_5_19 as StdField with uid="PAMPHMQGFW",rtseq=115,rtrep=.f.,;
    cFormVar = "w_MF_NRIF3", cQueryName = "MF_NRIF3",;
    bObbl = .f. , nPag = 5, value=space(7), bMultilanguage =  .f.,;
    ToolTipText = "Numero di riferimento",;
    HelpContextID = 65672697,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=192, Top=223, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',7)

  func oMF_NRIF3_5_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFCAUSA3_5_20 as StdField with uid="BWTVBLXRGC",rtseq=116,rtrep=.f.,;
    cFormVar = "w_MFCAUSA3", cQueryName = "MFCAUSA3",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Causale",;
    HelpContextID = 32811527,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=262, Top=223, cSayPict='repl("!",1)', cGetPict='repl("!",1)', InputMask=replicate('X',2)

  func oMFCAUSA3_5_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFIMDIL3_5_21 as StdField with uid="ZSLZCWMSTL",rtseq=117,rtrep=.f.,;
    cFormVar = "w_MFIMDIL3", cQueryName = "MFIMDIL3",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 50836985,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=309, Top=223, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMDIL3_5_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFIMCIL3_5_22 as StdField with uid="RWHHSNNRDK",rtseq=118,rtrep=.f.,;
    cFormVar = "w_MFIMCIL3", cQueryName = "MFIMCIL3",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 49788409,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=470, Top=223, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  func oMFIMCIL3_5_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFTDINAI_5_32 as StdField with uid="PSBGPKLIAA",rtseq=119,rtrep=.f.,;
    cFormVar = "w_MFTDINAI", cQueryName = "MFTDINAI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 129014257,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=309, Top=252, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  add object oMFTCINAI_5_33 as StdField with uid="XTGTLEVZXZ",rtseq=120,rtrep=.f.,;
    cFormVar = "w_MFTCINAI", cQueryName = "MFTCINAI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 129079793,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=470, Top=252, cSayPict="'@Z '+ v_PV(76)", cGetPict="'@Z '+ v_GV(76)"

  add object oMFSALINA_5_34 as StdField with uid="DZWWWGTXFX",rtseq=121,rtrep=.f.,;
    cFormVar = "w_MFSALINA", cQueryName = "MFSALINA",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 209955321,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=609, Top=252, cSayPict="v_PV(76)", cGetPict="v_GV(76)"

  add object oMESE_5_42 as StdField with uid="MLVBGKPOHZ",rtseq=122,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=351, Top=25, InputMask=replicate('X',2)

  add object oANNO_5_43 as StdField with uid="JTXVHPAOPB",rtseq=123,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=376, Top=25, InputMask=replicate('X',4)

  add object oStr_5_23 as StdString with uid="TZTHCVPSZI",Visible=.t., Left=82, Top=151,;
    Alignment=0, Width=51, Height=15,;
    Caption="Numero"  ;
  , bGlobalFont=.t.

  add object oStr_5_24 as StdString with uid="FGFSRZKDQK",Visible=.t., Left=154, Top=153,;
    Alignment=0, Width=24, Height=15,;
    Caption="C.C."  ;
  , bGlobalFont=.t.

  add object oStr_5_25 as StdString with uid="XMJKQNQCQQ",Visible=.t., Left=309, Top=153,;
    Alignment=0, Width=154, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_5_26 as StdString with uid="QPZQHUEURM",Visible=.t., Left=470, Top=153,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_5_27 as StdString with uid="YCITJFFMQZ",Visible=.t., Left=7, Top=151,;
    Alignment=0, Width=71, Height=15,;
    Caption="Cod.sede"  ;
  , bGlobalFont=.t.

  add object oStr_5_28 as StdString with uid="DUQPKICCWN",Visible=.t., Left=82, Top=135,;
    Alignment=0, Width=109, Height=15,;
    Caption="Posiz.assicurativa"  ;
  , bGlobalFont=.t.

  add object oStr_5_29 as StdString with uid="AZKLFKKYGI",Visible=.t., Left=192, Top=135,;
    Alignment=0, Width=73, Height=15,;
    Caption="Numero di"  ;
  , bGlobalFont=.t.

  add object oStr_5_30 as StdString with uid="OPXQMQDPMJ",Visible=.t., Left=192, Top=153,;
    Alignment=0, Width=59, Height=15,;
    Caption="riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_5_31 as StdString with uid="GWKFEEEWGD",Visible=.t., Left=260, Top=153,;
    Alignment=0, Width=45, Height=15,;
    Caption="Caus."  ;
  , bGlobalFont=.t.

  add object oStr_5_35 as StdString with uid="CAXNCWDVNP",Visible=.t., Left=251, Top=254,;
    Alignment=0, Width=49, Height=15,;
    Caption="Totale I"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_36 as StdString with uid="YQYWHXKWQX",Visible=.t., Left=623, Top=230,;
    Alignment=0, Width=73, Height=15,;
    Caption="Saldo (I-L)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_37 as StdString with uid="HTBHHGCOXH",Visible=.t., Left=448, Top=254,;
    Alignment=0, Width=20, Height=15,;
    Caption="L"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_38 as StdString with uid="UICRRZTZSD",Visible=.t., Left=12, Top=63,;
    Alignment=0, Width=311, Height=18,;
    Caption="Altri enti previdenziali ed assicurativi - INAIL"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_40 as StdString with uid="VXPRYYUHZU",Visible=.t., Left=15, Top=25,;
    Alignment=1, Width=95, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_41 as StdString with uid="HJKKMLLUEV",Visible=.t., Left=219, Top=25,;
    Alignment=1, Width=126, Height=15,;
    Caption="Periodo di riferimento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_5_39 as StdBox with uid="DLSNHYHKXJ",left=12, top=86, width=769,height=1
enddefine
define class tgscg_aonPag6 as StdContainer
  Width  = 804
  height = 413
  stdWidth  = 804
  stdheight = 413
  resizeXpos=509
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_6_1 as StdField with uid="BOVCFDHJYO",rtseq=124,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 6, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=117, Top=26, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMESE_6_6 as StdField with uid="BPSADYMFGH",rtseq=125,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=346, Top=27, InputMask=replicate('X',2)

  add object oANNO_6_7 as StdField with uid="XFUVSGDJBX",rtseq=126,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=371, Top=27, InputMask=replicate('X',4)


  add object oObj_6_11 as cp_runprogram with uid="IMVHWAQFRK",left=3, top=433, width=143,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BBF ('MP')",;
    cEvent = "ControllaDati",;
    nPag=6;
    , HelpContextID = 267263514


  add object oLinkPC_6_12 as stdDynamicChildContainer with uid="RVHNDFUOXF",left=0, top=131, width=783, height=171, bOnScreen=.t.;


  add object oMFSALAEN_6_13 as StdField with uid="AOXLAZGPXE",rtseq=150,rtrep=.f.,;
    cFormVar = "w_MFSALAEN", cQueryName = "MFSALAEN",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 75737580,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=654, Top=327, cSayPict="v_PV(76)", cGetPict="v_GV(76)"


  add object oObj_6_15 as cp_runprogram with uid="ACGPEEDSNQ",left=3, top=452, width=143,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BTO('S')",;
    cEvent = "Update start,Insert start",;
    nPag=6;
    , HelpContextID = 267263514

  add object oStr_6_2 as StdString with uid="DDIXCLCYBG",Visible=.t., Left=20, Top=27,;
    Alignment=1, Width=95, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_3 as StdString with uid="CTXZAWNSVI",Visible=.t., Left=3, Top=70,;
    Alignment=0, Width=263, Height=15,;
    Caption="Altri enti previdenziali ed assicurativi"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_5 as StdString with uid="JWKSCNPXLY",Visible=.t., Left=215, Top=27,;
    Alignment=1, Width=126, Height=15,;
    Caption="Periodo di riferimento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_14 as StdString with uid="LTURFGEZTI",Visible=.t., Left=598, Top=331,;
    Alignment=0, Width=55, Height=18,;
    Caption="Saldo (O)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_6_4 as StdBox with uid="OSEQIEOXBC",left=13, top=89, width=761,height=1
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_mat",lower(this.oContained.GSCG_MAT.class))=0
        this.oContained.GSCG_MAT.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgscg_aonPag7 as StdContainer
  Width  = 804
  height = 413
  stdWidth  = 804
  stdheight = 413
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_7_1 as StdField with uid="FWNKVSYQIO",rtseq=130,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 7, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=79, Left=116, Top=26, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFSALFIN_7_2 as StdField with uid="XPRIIKCUAV",rtseq=131,rtrep=.f.,;
    cFormVar = "w_MFSALFIN", cQueryName = "MFSALFIN",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 8148500,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=114, Left=555, Top=28, cSayPict="v_PV(76)", cGetPict="v_GV(76)"


  add object oLinkPC_7_3 as stdDynamicChildContainer with uid="CDRKTLRCVN",left=5, top=78, width=791, height=243, bOnScreen=.t.;



  add object oBtn_7_7 as StdButton with uid="CJUEHBMWXH",left=696, top=366, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=7;
    , ToolTipText = "Premere per salvare i risultati e stampare il modello";
    , HelpContextID = 91630310;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_7.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_7_8 as StdButton with uid="VSFOQMDZOF",left=748, top=366, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=7;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 98927174;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_7_9 as cp_runprogram with uid="GFQIHJVWOZ",left=3, top=432, width=244,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BMF",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=7;
    , HelpContextID = 267263514

  add object oMESE_7_10 as StdField with uid="VBABMPRSRE",rtseq=132,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=342, Top=27, InputMask=replicate('X',2)

  add object oANNO_7_11 as StdField with uid="DRPQPJTNKA",rtseq=133,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=367, Top=27, InputMask=replicate('X',4)

  add object oMFDESSTA_7_13 as StdField with uid="ZXJMGMFFSC",rtseq=138,rtrep=.f.,;
    cFormVar = "w_MFDESSTA", cQueryName = "MFDESSTA",;
    bObbl = .f. , nPag = 7, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede azienda impostata come destinataria stampa",;
    HelpContextID = 34642425,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=169, Top=348, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SEDIAZIE", cZoomOnZoom="GSAR_ASE", oKey_1_1="SECODAZI", oKey_1_2="this.w_CODAZIE", oKey_2_1="SECODDES", oKey_2_2="this.w_MFDESSTA"

  func oMFDESSTA_7_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFDESSTA_7_13.ecpDrop(oSource)
    this.Parent.oContained.link_7_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFDESSTA_7_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SEDIAZIE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZIE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZIE)
    endif
    do cp_zoom with 'SEDIAZIE','*','SECODAZI,SECODDES',cp_AbsName(this.parent,'oMFDESSTA_7_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASE',"Sedi aziende",'GSAR_ASI.SEDIAZIE_VZM',this.parent.oContained
  endproc
  proc oMFDESSTA_7_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SECODAZI=w_CODAZIE
     i_obj.w_SECODDES=this.parent.oContained.w_MFDESSTA
     i_obj.ecpSave()
  endproc

  add object oCAP_7_18 as StdField with uid="EHGCBYPICB",rtseq=143,rtrep=.f.,;
    cFormVar = "w_CAP", cQueryName = "CAP",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "CAP della sede destinataria stampa",;
    HelpContextID = 91955238,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=306, Top=348, InputMask=replicate('X',9)

  add object oLOCALI_7_19 as StdField with uid="YJQQJNWEDS",rtseq=144,rtrep=.f.,;
    cFormVar = "w_LOCALI", cQueryName = "LOCALI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� della sede destinataria stampa",;
    HelpContextID = 210018634,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=456, Top=348, InputMask=replicate('X',30)

  add object oPROVIN_7_20 as StdField with uid="KKBGTKSRJE",rtseq=145,rtrep=.f.,;
    cFormVar = "w_PROVIN", cQueryName = "PROVIN",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia della sede destinataria stampa",;
    HelpContextID = 127852042,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=169, Top=377, InputMask=replicate('X',2)

  add object oINDIRI_7_21 as StdField with uid="LLDGSFXWVI",rtseq=146,rtrep=.f.,;
    cFormVar = "w_INDIRI", cQueryName = "INDIRI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede destinataria stampa",;
    HelpContextID = 203199098,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=319, Top=377, InputMask=replicate('X',35)

  add object oStr_7_4 as StdString with uid="VKEGRLLTGP",Visible=.t., Left=435, Top=31,;
    Alignment=1, Width=113, Height=15,;
    Caption="SALDO FINALE:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_5 as StdString with uid="BYCBSRREFL",Visible=.t., Left=17, Top=28,;
    Alignment=1, Width=95, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_6 as StdString with uid="SCVOUDRHJZ",Visible=.t., Left=212, Top=28,;
    Alignment=1, Width=126, Height=15,;
    Caption="Periodo di riferimento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_22 as StdString with uid="YNXKDJTXGM",Visible=.t., Left=239, Top=352,;
    Alignment=1, Width=66, Height=18,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_7_23 as StdString with uid="TSJBINZCHU",Visible=.t., Left=393, Top=352,;
    Alignment=1, Width=60, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_7_24 as StdString with uid="GGEVHAPSLD",Visible=.t., Left=113, Top=381,;
    Alignment=1, Width=53, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_7_25 as StdString with uid="JLVIFDUYJQ",Visible=.t., Left=8, Top=352,;
    Alignment=1, Width=158, Height=18,;
    Caption="Sede destinatario di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_7_26 as StdString with uid="GNVKOPQKBS",Visible=.t., Left=255, Top=381,;
    Alignment=1, Width=57, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oBox_7_12 as StdBox with uid="LIMWEMRJGT",left=5, top=68, width=774,height=2
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_avf",lower(this.oContained.GSCG_AVF.class))=0
        this.oContained.GSCG_AVF.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_aon','MOD_PAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MFSERIAL=MOD_PAG.MFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
