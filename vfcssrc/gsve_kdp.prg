* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kdp                                                        *
*              Dettaglio provvigione                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_21]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kdp",oParentObject))

* --- Class definition
define class tgsve_kdp as StdForm
  Top    = 151
  Left   = 29

  * --- Standard Properties
  Width  = 614
  Height = 266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=185978263
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gsve_kdp"
  cComment = "Dettaglio provvigione"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SERIAL = space(10)
  w_ROWNUM = 0
  w_MPNUMDOC = 0
  w_MPALFDOC = space(10)
  w_MPTIPCON = space(1)
  w_MPDATDOC = ctod('  /  /  ')
  w_MPCODCON = space(15)
  w_DESCRI = space(40)
  w_MPDESSUP = space(50)
  w_MPTIPMAT = space(2)
  w_MPDATSCA = ctod('  /  /  ')
  w_MPDATMAT = ctod('  /  /  ')
  w_MPDATLIQ = ctod('  /  /  ')
  w_MPCODVAL = space(3)
  o_MPCODVAL = space(3)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_MPTOTIMP = 0
  o_MPTOTIMP = 0
  w_MPPERPRA = 0
  o_MPPERPRA = 0
  w_MPTOTAGE = 0
  w_MPPERPRC = 0
  o_MPPERPRC = 0
  w_MPTOTZON = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kdpPag1","gsve_kdp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMPDESSUP_1_9
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SERIAL=space(10)
      .w_ROWNUM=0
      .w_MPNUMDOC=0
      .w_MPALFDOC=space(10)
      .w_MPTIPCON=space(1)
      .w_MPDATDOC=ctod("  /  /  ")
      .w_MPCODCON=space(15)
      .w_DESCRI=space(40)
      .w_MPDESSUP=space(50)
      .w_MPTIPMAT=space(2)
      .w_MPDATSCA=ctod("  /  /  ")
      .w_MPDATMAT=ctod("  /  /  ")
      .w_MPDATLIQ=ctod("  /  /  ")
      .w_MPCODVAL=space(3)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_MPTOTIMP=0
      .w_MPPERPRA=0
      .w_MPTOTAGE=0
      .w_MPPERPRC=0
      .w_MPTOTZON=0
      .w_SERIAL=oParentObject.w_SERIAL
      .w_ROWNUM=oParentObject.w_ROWNUM
      .w_MPNUMDOC=oParentObject.w_MPNUMDOC
      .w_MPALFDOC=oParentObject.w_MPALFDOC
      .w_MPTIPCON=oParentObject.w_MPTIPCON
      .w_MPDATDOC=oParentObject.w_MPDATDOC
      .w_MPCODCON=oParentObject.w_MPCODCON
      .w_MPDESSUP=oParentObject.w_MPDESSUP
      .w_MPTIPMAT=oParentObject.w_MPTIPMAT
      .w_MPDATSCA=oParentObject.w_MPDATSCA
      .w_MPDATMAT=oParentObject.w_MPDATMAT
      .w_MPDATLIQ=oParentObject.w_MPDATLIQ
      .w_MPCODVAL=oParentObject.w_MPCODVAL
      .w_MPTOTIMP=oParentObject.w_MPTOTIMP
      .w_MPPERPRA=oParentObject.w_MPPERPRA
      .w_MPTOTAGE=oParentObject.w_MPTOTAGE
      .w_MPPERPRC=oParentObject.w_MPPERPRC
      .w_MPTOTZON=oParentObject.w_MPTOTZON
          .DoRTCalc(1,4,.f.)
        .w_MPTIPCON = 'C'
          .DoRTCalc(6,6,.f.)
        .w_MPCODCON = .w_MPCODCON
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_MPCODCON))
          .link_1_7('Full')
        endif
          .DoRTCalc(8,13,.f.)
        .w_MPCODVAL = .w_MPCODVAL
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_MPCODVAL))
          .link_1_14('Full')
        endif
          .DoRTCalc(15,15,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
      .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
    endwith
    this.DoRTCalc(17,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_SERIAL=.w_SERIAL
      .oParentObject.w_ROWNUM=.w_ROWNUM
      .oParentObject.w_MPNUMDOC=.w_MPNUMDOC
      .oParentObject.w_MPALFDOC=.w_MPALFDOC
      .oParentObject.w_MPTIPCON=.w_MPTIPCON
      .oParentObject.w_MPDATDOC=.w_MPDATDOC
      .oParentObject.w_MPCODCON=.w_MPCODCON
      .oParentObject.w_MPDESSUP=.w_MPDESSUP
      .oParentObject.w_MPTIPMAT=.w_MPTIPMAT
      .oParentObject.w_MPDATSCA=.w_MPDATSCA
      .oParentObject.w_MPDATMAT=.w_MPDATMAT
      .oParentObject.w_MPDATLIQ=.w_MPDATLIQ
      .oParentObject.w_MPCODVAL=.w_MPCODVAL
      .oParentObject.w_MPTOTIMP=.w_MPTOTIMP
      .oParentObject.w_MPPERPRA=.w_MPPERPRA
      .oParentObject.w_MPTOTAGE=.w_MPTOTAGE
      .oParentObject.w_MPPERPRC=.w_MPPERPRC
      .oParentObject.w_MPTOTZON=.w_MPTOTZON
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
          .link_1_7('Full')
        .DoRTCalc(8,13,.t.)
          .link_1_14('Full')
        .DoRTCalc(15,15,.t.)
        if .o_MPCODVAL<>.w_MPCODVAL
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(17,18,.t.)
        if .o_MPTOTIMP<>.w_MPTOTIMP.or. .o_MPPERPRA<>.w_MPPERPRA
            .w_MPTOTAGE = cp_ROUND (.w_MPTOTIMP*.w_MPPERPRA/100,.w_DECTOT)
        endif
        .DoRTCalc(20,20,.t.)
        if .o_MPTOTIMP<>.w_MPTOTIMP.or. .o_MPPERPRC<>.w_MPPERPRC
            .w_MPTOTZON = cp_ROUND (.w_MPTOTIMP*.w_MPPERPRC/100,.w_DECTOT)
        endif
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MPCODCON
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MPCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MPTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MPTIPCON;
                       ,'ANCODICE',this.w_MPCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MPCODCON = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MPCODVAL
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MPCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MPCODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_MPCODVAL = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMPNUMDOC_1_3.value==this.w_MPNUMDOC)
      this.oPgFrm.Page1.oPag.oMPNUMDOC_1_3.value=this.w_MPNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMPALFDOC_1_4.value==this.w_MPALFDOC)
      this.oPgFrm.Page1.oPag.oMPALFDOC_1_4.value=this.w_MPALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMPDATDOC_1_6.value==this.w_MPDATDOC)
      this.oPgFrm.Page1.oPag.oMPDATDOC_1_6.value=this.w_MPDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMPCODCON_1_7.value==this.w_MPCODCON)
      this.oPgFrm.Page1.oPag.oMPCODCON_1_7.value=this.w_MPCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_8.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_8.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMPDESSUP_1_9.value==this.w_MPDESSUP)
      this.oPgFrm.Page1.oPag.oMPDESSUP_1_9.value=this.w_MPDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oMPTIPMAT_1_10.RadioValue()==this.w_MPTIPMAT)
      this.oPgFrm.Page1.oPag.oMPTIPMAT_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMPDATSCA_1_11.value==this.w_MPDATSCA)
      this.oPgFrm.Page1.oPag.oMPDATSCA_1_11.value=this.w_MPDATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oMPDATMAT_1_12.value==this.w_MPDATMAT)
      this.oPgFrm.Page1.oPag.oMPDATMAT_1_12.value=this.w_MPDATMAT
    endif
    if not(this.oPgFrm.Page1.oPag.oMPDATLIQ_1_13.value==this.w_MPDATLIQ)
      this.oPgFrm.Page1.oPag.oMPDATLIQ_1_13.value=this.w_MPDATLIQ
    endif
    if not(this.oPgFrm.Page1.oPag.oMPCODVAL_1_14.value==this.w_MPCODVAL)
      this.oPgFrm.Page1.oPag.oMPCODVAL_1_14.value=this.w_MPCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMPTOTIMP_1_17.value==this.w_MPTOTIMP)
      this.oPgFrm.Page1.oPag.oMPTOTIMP_1_17.value=this.w_MPTOTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oMPPERPRA_1_18.value==this.w_MPPERPRA)
      this.oPgFrm.Page1.oPag.oMPPERPRA_1_18.value=this.w_MPPERPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oMPTOTAGE_1_19.value==this.w_MPTOTAGE)
      this.oPgFrm.Page1.oPag.oMPTOTAGE_1_19.value=this.w_MPTOTAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oMPPERPRC_1_30.value==this.w_MPPERPRC)
      this.oPgFrm.Page1.oPag.oMPPERPRC_1_30.value=this.w_MPPERPRC
    endif
    if not(this.oPgFrm.Page1.oPag.oMPTOTZON_1_31.value==this.w_MPTOTZON)
      this.oPgFrm.Page1.oPag.oMPTOTZON_1_31.value=this.w_MPTOTZON
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MPDATSCA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMPDATSCA_1_11.SetFocus()
            i_bnoObbl = !empty(.w_MPDATSCA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MPPERPRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMPPERPRA_1_18.SetFocus()
            i_bnoObbl = !empty(.w_MPPERPRA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MPCODVAL = this.w_MPCODVAL
    this.o_MPTOTIMP = this.w_MPTOTIMP
    this.o_MPPERPRA = this.w_MPPERPRA
    this.o_MPPERPRC = this.w_MPPERPRC
    return

enddefine

* --- Define pages as container
define class tgsve_kdpPag1 as StdContainer
  Width  = 610
  height = 266
  stdWidth  = 610
  stdheight = 266
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMPNUMDOC_1_3 as StdField with uid="PSTSISWKBP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MPNUMDOC", cQueryName = "MPNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 197131511,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=117, Top=13

  add object oMPALFDOC_1_4 as StdField with uid="GMOKXSIIAR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MPALFDOC", cQueryName = "MPALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 205114615,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=247, Top=13, InputMask=replicate('X',10)

  add object oMPDATDOC_1_6 as StdField with uid="SVDVRQXDSL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MPDATDOC", cQueryName = "MPDATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 191143159,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=397, Top=13

  add object oMPCODCON_1_7 as StdField with uid="EBJOQOWTVY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MPCODCON", cQueryName = "MPCODCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 223784172,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=117, Top=42, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MPTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_MPCODCON"

  func oMPCODCON_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCRI_1_8 as StdField with uid="FWBYYSERAX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 109164746,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=247, Top=42, InputMask=replicate('X',40)

  add object oMPDESSUP_1_9 as StdField with uid="BPSXDBPVVD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MPDESSUP", cQueryName = "MPDESSUP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione supplementare",;
    HelpContextID = 59728662,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=117, Top=71, InputMask=replicate('X',50)


  add object oMPTIPMAT_1_10 as StdCombo with uid="DAWMQZQLTY",rtseq=10,rtrep=.f.,left=117,top=100,width=97,height=21;
    , ToolTipText = "Tipo maturazione";
    , HelpContextID = 43752678;
    , cFormVar="w_MPTIPMAT",RowSource=""+"Dt.fattura,"+"Dt.scadenza,"+"Dt.incasso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMPTIPMAT_1_10.RadioValue()
    return(iif(this.value =1,'FA',;
    iif(this.value =2,'SC',;
    iif(this.value =3,'IN',;
    space(2)))))
  endfunc
  func oMPTIPMAT_1_10.GetRadio()
    this.Parent.oContained.w_MPTIPMAT = this.RadioValue()
    return .t.
  endfunc

  func oMPTIPMAT_1_10.SetRadio()
    this.Parent.oContained.w_MPTIPMAT=trim(this.Parent.oContained.w_MPTIPMAT)
    this.value = ;
      iif(this.Parent.oContained.w_MPTIPMAT=='FA',1,;
      iif(this.Parent.oContained.w_MPTIPMAT=='SC',2,;
      iif(this.Parent.oContained.w_MPTIPMAT=='IN',3,;
      0)))
  endfunc

  add object oMPDATSCA_1_11 as StdField with uid="EHKZQMOMRV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MPDATSCA", cQueryName = "MPDATSCA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scadenza selezionata",;
    HelpContextID = 207920377,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=117, Top=132

  add object oMPDATMAT_1_12 as StdField with uid="ZPMKXDAILY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_MPDATMAT", cQueryName = "MPDATMAT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di maturazione",;
    HelpContextID = 40148198,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=324, Top=132

  add object oMPDATLIQ_1_13 as StdField with uid="XWORRQYOKC",rtseq=13,rtrep=.f.,;
    cFormVar = "w_MPDATLIQ", cQueryName = "MPDATLIQ",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di liquidazione",;
    HelpContextID = 211510039,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=520, Top=132

  add object oMPCODVAL_1_14 as StdField with uid="UNMLEEJSFK",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MPCODVAL", cQueryName = "MPCODVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 173452526,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=117, Top=161, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_MPCODVAL"

  func oMPCODVAL_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oMPTOTIMP_1_17 as StdField with uid="RCMYXXWABL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MPTOTIMP", cQueryName = "MPTOTIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 106274026,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=275, Top=161, cSayPict="v_GV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oMPPERPRA_1_18 as StdField with uid="XYNWCHAKUR",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MPPERPRA", cQueryName = "MPPERPRA",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale provvigione dell'agente selezionato",;
    HelpContextID = 260037881,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=117, Top=190, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oMPTOTAGE_1_19 as StdField with uid="ZKAJSTBPRA",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MPTOTAGE", cQueryName = "MPTOTAGE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo della provvigione dell'agente selezionato",;
    HelpContextID = 27943691,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=275, Top=190, cSayPict="v_GV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oMPPERPRC_1_30 as StdField with uid="ZSCZFNZARZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MPPERPRC", cQueryName = "MPPERPRC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale provvigione del capo area selezionato",;
    HelpContextID = 260037879,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=117, Top=219, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oMPTOTZON_1_31 as StdField with uid="AXGEAPAVHA",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MPTOTZON", cQueryName = "MPTOTZON",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo della provvigione del capo area selezionato",;
    HelpContextID = 89496812,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=275, Top=219, cSayPict="v_GV(40+VVL)", cGetPict="v_GV(40+VVL)"


  add object oBtn_1_34 as StdButton with uid="SJUJUMZNTZ",left=551, top=213, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193295686;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_35 as StdButton with uid="YKWNVFZXPQ",left=500, top=213, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare le provvigioni";
    , HelpContextID = 186007014;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_39 as cp_runprogram with uid="YDXLSEVZZP",left=4, top=271, width=162,height=21,;
    caption='GSVE_BD2',;
   bGlobalFont=.t.,;
    prg="GSVE_BD2",;
    cEvent = "Update end",;
    nPag=1;
    , HelpContextID = 212826728

  add object oStr_1_20 as StdString with uid="SFDRLJNRNI",Visible=.t., Left=360, Top=13,;
    Alignment=1, Width=34, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="YHFZIHDUEW",Visible=.t., Left=11, Top=42,;
    Alignment=1, Width=104, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="YXBXIBFSBI",Visible=.t., Left=11, Top=71,;
    Alignment=1, Width=104, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="DBHLENAYKL",Visible=.t., Left=6, Top=100,;
    Alignment=1, Width=109, Height=15,;
    Caption="Tipo maturazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="GICGYQYAPP",Visible=.t., Left=191, Top=132,;
    Alignment=1, Width=131, Height=15,;
    Caption="Data maturazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="OSUFKSTMCK",Visible=.t., Left=404, Top=132,;
    Alignment=1, Width=115, Height=15,;
    Caption="Data liquidazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="DUCSDLCTHP",Visible=.t., Left=192, Top=161,;
    Alignment=1, Width=80, Height=15,;
    Caption="Imponibile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="GSMEHTELCK",Visible=.t., Left=192, Top=190,;
    Alignment=1, Width=80, Height=15,;
    Caption="Provvigione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="HYKFDRQKYQ",Visible=.t., Left=192, Top=219,;
    Alignment=1, Width=80, Height=15,;
    Caption="Provvigione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="AVTIATENNZ",Visible=.t., Left=11, Top=161,;
    Alignment=1, Width=104, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="EPWBKDYBNI",Visible=.t., Left=11, Top=13,;
    Alignment=1, Width=104, Height=15,;
    Caption="Documento n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="RTPREAOOMZ",Visible=.t., Left=238, Top=15,;
    Alignment=2, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="ZBEWBNVNFO",Visible=.t., Left=11, Top=132,;
    Alignment=1, Width=104, Height=15,;
    Caption="Data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="WRXGOJAWUN",Visible=.t., Left=11, Top=190,;
    Alignment=1, Width=104, Height=15,;
    Caption="% Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="BTTSRAIRMW",Visible=.t., Left=11, Top=219,;
    Alignment=1, Width=104, Height=15,;
    Caption="% Capoarea:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kdp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
