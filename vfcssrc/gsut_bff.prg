* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bff                                                        *
*              Firma documento                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-12-02                                                      *
* Last revis.: 2013-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPathDoc
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bff",oParentObject,m.pPathDoc)
return(i_retval)

define class tgsut_bff as StdBatch
  * --- Local variables
  pPathDoc = space(254)
  w_RUNCMD = space(254)
  w_LogErr = space(100)
  w_PathDest = space(100)
  w_CODPRES = space(5)
  w_DP__DIKE = space(254)
  w_Parametri_Dike = space(250)
  w_DELAY = 0
  w_PathDocFirmato = space(254)
  w_NumGiri = 0
  * --- WorkFile variables
  DIPENDEN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FIRMA IL DOCUMENTO
    * --- Path assoluto del documento da firmare
    this.w_LogErr = "C:\"+SYS(2015)+".TXT"
    this.w_LogErr = ADDBS(TempAdHoc())+SYS(2015)+".TXT"
    * --- Controllo esistenza applicativo DIKE.exe
    if vartype(g_DIKE)="C" and cp_fileexist(g_DIKE)
      this.w_RUNCMD = Alltrim(g_DIKE)
    else
      this.w_CODPRES = Readdipend(i_CodUte, "C")
      * --- Read from DIPENDEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIPENDEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DP__DIKE"+;
          " from "+i_cTable+" DIPENDEN where ";
              +"DPCODICE = "+cp_ToStrODBC(this.w_CODPRES);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DP__DIKE;
          from (i_cTable) where;
              DPCODICE = this.w_CODPRES;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DP__DIKE = NVL(cp_ToDate(_read_.DP__DIKE),cp_NullValue(_read_.DP__DIKE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not Empty(this.w_DP__DIKE) and cp_fileexist(this.w_DP__DIKE) 
        this.w_RUNCMD = Alltrim(this.w_DP__DIKE)
      else
        this.w_RUNCMD = CHECKOPENTO("p7m")
      endif
    endif
    if Not Empty( this.w_RUNCMD )
      * --- Rimuovo il parametro finale (se cambia qualcosa nell'interfaccia di DIKE potrebbe essere necessario modificare questa logica)
      this.w_RUNCMD = ALLTRIM(STRTRAN(this.w_RUNCMD,'"%1"', ""))
      * --- Viene lanciato Dike.exe
      if .T. OR OCCURS(" ",Alltrim(this.pPathDoc))>0
        * --- Non utilizziamo la shell execute
        * --- Otteniamo il path di destinazione del documento firmato
        this.w_PathDest = JUSTPATH(this.pPathDoc)
        w_ERRORE = .F.
        w_ErrorHandler = on("ERROR")
        on error w_ERRORE = .T.
         
 cexe=this.w_RUNCMD
        if "DIKE"$UPPER(JUSTSTEM(this.w_RUNCMD))
          * --- Dike - utilizziamo i parametri definiti
           
 cexec="!/N " +(cexe)+" '" + (this.pPathDoc)+"'"+" '" + (this.w_PathDest)+"'"+" '" + (this.w_LogErr)+"'"
          this.w_Parametri_Dike = '"'+this.pPathDoc+'" "'+this.w_PathDest+'" "'+this.w_LogErr+'"'
          * --- w_Parametri_Dike='"'+NOMDOC+'" "'+SUBSTR(w_PathDest,1, LEN(w_PathDest)-1)+'" "'+w_LogErr+'"'
          if shellex( this.w_RUNCMD, "open" , this.w_Parametri_Dike )>32
            * --- In assenza di errori
          else
            * --- Impossibile lanciare DIKE, qualche problema a livello di sistema operativo
            *     Memorizzo nella Clipboard il comando verosimilmente lanciato in modo da poterlo provare dalla Shell
            Ah_ErrorMsg( "Impossibile lanciare il software per la firma digitale" )
            _Cliptext=this.w_RUNCMD+' "'+this.pPathDoc+'"'
            i_retcode = 'stop'
            return
          endif
        else
          * --- Unico parametro il documento
           
 cexec="!/N" +(cexe)+" '" + (this.pPathDoc)+"'"
           
 &cexec
        endif
        on error &w_ErrorHandler
        if w_errore
          Ah_ErrorMsg( "Impossibile lanciare il software per la firma digitale" )
        endif
      else
        if shellex( this.w_RUNCMD, "open" , this.pPathDoc )>32
          * --- In assenza di errori
        else
          * --- Impossibile lanciare DIKE, qualche problema a livello di sistema operativo
          *     Memorizzo nella Clipboard il comando verosimilmente lanciato in modo da poterlo provare dalla Shell
          Ah_ErrorMsg( "Impossibile lanciare il software per la firma digitale" )
          _Cliptext=this.w_RUNCMD+' "'+this.pPathDoc+'"'
          i_retcode = 'stop'
          return
        endif
      endif
    else
      * --- Impossibile recuperare dal registro di windows un'applicazione collegata all'estensione P7M
      Ah_ErrorMsg( "Non esiste il software per la firma digitale" )
      i_retcode = 'stop'
      return
    endif
    this.w_NumGiri = 0
    * --- Il nome del file firmato � composto dal file originale (inclusa l'estensione) + l'aggiunta di  '.P7M'
    this.w_PathDocFirmato = Alltrim(this.pPathDoc) + ".P7M"
    * --- Massimo 30 giri, se necessario � possibile definire nel CNF (tramite la g_FIRMADELAY) un numero differente di giri
    this.w_DELAY = IIF( vartype( g_FIRMADELAY)="N" And g_FIRMADELAY>0 , g_FIRMADELAY , 50)
    do while NOT cp_fileexist(this.w_PathDocFirmato) AND this.w_NumGiri < this.w_DELAY
      wait wind "" timeout 1
      this.w_NumGiri = this.w_NumGiri + 1
    enddo
    * --- Ho il documento firmato o no ?
    *     Se non trovo il documento significa che l'utente si � attardato su DIKE o lo ha chiuso senza firmare il documento.
    if NOT cp_fileexist(this.w_PathDocFirmato)
      Ah_ErrorMsg( "Attenzione il documento firmato %1 � inesistente. Procedere con la firma prima di premere OK " , "!", "", this.w_PathDocFirmato)
    endif
    if cp_fileexist(this.w_LogErr)
      w_ERRORE = .F.
      w_ErrorHandler = on("ERROR")
      on error w_ERRORE = .T.
      ERASE (this.w_LogErr)
      on error &w_ErrorHandler
    endif
  endproc


  proc Init(oParentObject,pPathDoc)
    this.pPathDoc=pPathDoc
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DIPENDEN'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPathDoc"
endproc
