* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mit                                                        *
*              Manutenzione elenchi INTRA                                      *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_397]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-13                                                      *
* Last revis.: 2018-03-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_mit"))

* --- Class definition
define class tgsar_mit as StdTrsForm
  Top    = 7
  Left   = 5

  * --- Standard Properties
  Width  = 952
  Height = 536+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-03-06"
  HelpContextID=264861545
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=92

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  ELEIMAST_IDX = 0
  ELEIDETT_IDX = 0
  CONTI_IDX = 0
  TIPITRAN_IDX = 0
  NOMENCLA_IDX = 0
  NAZIONI_IDX = 0
  VALUTE_IDX = 0
  AZIENDA_IDX = 0
  MODASPED_IDX = 0
  CON_CONS_IDX = 0
  AZDATINT_IDX = 0
  cFile = "ELEIMAST"
  cFileDetail = "ELEIDETT"
  cKeySelect = "PISERIAL"
  cKeyWhere  = "PISERIAL=this.w_PISERIAL"
  cKeyDetail  = "PISERIAL=this.w_PISERIAL"
  cKeyWhereODBC = '"PISERIAL="+cp_ToStrODBC(this.w_PISERIAL)';

  cKeyDetailWhereODBC = '"PISERIAL="+cp_ToStrODBC(this.w_PISERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"ELEIDETT.PISERIAL="+cp_ToStrODBC(this.w_PISERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ELEIDETT.CPROWORD '
  cPrg = "gsar_mit"
  cComment = "Manutenzione elenchi INTRA"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_READAZI = space(5)
  w_AITIPE = space(1)
  w_AITIPS = space(1)
  w_AITIPV = space(1)
  w_AITIAS = space(1)
  w_PISERIAL = space(10)
  o_PISERIAL = space(10)
  w_PINUMREG = 0
  w_PI__ANNO = space(4)
  o_PI__ANNO = space(4)
  w_PIDATREG = ctod('  /  /  ')
  w_PIALFDOC = space(10)
  w_PINUMDOC = 0
  w_PIDATDOC = ctod('  /  /  ')
  o_PIDATDOC = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_PIVALNAZ = space(3)
  w_PITIPMOV = space(2)
  o_PITIPMOV = space(2)
  w_PITIPCON = space(1)
  w_PICODCON = space(15)
  o_PICODCON = space(15)
  w_DESCLF = space(40)
  w_PIVA = space(12)
  w_TIPPER = space(1)
  o_TIPPER = space(1)
  w_FLINTR = space(1)
  w_PIANNRET = space(4)
  o_PIANNRET = space(4)
  w_PITIPPER = space(1)
  o_PITIPPER = space(1)
  w_PIPERRET = 0
  o_PIPERRET = 0
  w_DESPER = space(25)
  w_PIDATCOM = ctod('  /  /  ')
  w_PIVALORI = space(3)
  o_PIVALORI = space(3)
  w_DESVAL = space(10)
  w_CAMVAL = 0
  w_DECVAL = 0
  w_DECPIC = 0
  w_DESSPE = space(10)
  w_DATOBSO = ctod('  /  /  ')
  w_DECVAP = 0
  w_DECPIP = 0
  w_PIRIFDOC = space(10)
  w_PIIMPNAZ = 0
  o_PIIMPNAZ = 0
  w_CPROWORD = 0
  w_PIIMPVAL = 0
  w_PINATTRA = space(3)
  w_PINOMENC = space(8)
  w_PIROWORD = 0
  w_UNSUP = space(3)
  w_PIMASNET = 0
  w_PIQTASUP = 0
  w_ANCODISO = space(3)
  w_PIVALSTA = 0
  w_PICONDCO = space(1)
  w_PIMODTRA = space(3)
  w_PINAZORI = space(3)
  w_PINAZPRO = space(3)
  w_PINAZDES = space(3)
  w_PIPRODES = space(2)
  w_PIPROORI = space(2)
  w_PITIPRET = space(1)
  w_DTOBSO = ctod('  /  /  ')
  w_DTOBBL = space(1)
  w_RESCHK = 0
  w_SIMVAL = space(5)
  w_SIMVAL1 = space(5)
  w_PIPORORI = space(10)
  w_PIPORDES = space(10)
  w_NAZION = space(3)
  w_ISONAZ = space(3)
  w_ISONA2 = space(3)
  w_ISONA3 = space(3)
  w_PITIPMOV1 = space(2)
  o_PITIPMOV1 = space(2)
  w_ARUTISER = space(1)
  o_ARUTISER = space(1)
  w_PIPAEPAG = space(3)
  w_PISEZDOG = space(6)
  w_PIANNREG = space(4)
  w_PIPRORET = space(6)
  w_PIPROGSE = space(5)
  w_PINUMFAT = 0
  w_PIALFFAT = space(10)
  w_PIDATFAT = ctod('  /  /  ')
  w_PIPERSER = space(1)
  w_PIMODINC = space(1)
  w_PIPAEPAG = space(3)
  w_TIPOMOV = space(2)
  w_PIPERSER = space(1)
  w_PIMODINC = space(1)
  w_PIANNREG = space(4)
  w_PIPROGSE = space(5)
  w_MODIFICA = .F.
  w_ANCONCON = space(1)
  w_HASEVCOP = space(50)
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PISERIAL = this.W_PISERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PI__ANNO = this.W_PI__ANNO
  op_PINUMREG = this.W_PINUMREG
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar_mit
  w_TIPO='CE'
  
  Func ah_HasCPEvents(i_cOp)
   if  Not Empty(This.w_PIDATCOM) And Upper(i_cop)='ECPDELETE'
    			* esegue controllo se movimento bloccato se da problemi si pu� chiamare direttamente il Batch
          This.w_HASEVCOP=i_cop
  		  	This.NotifyEvent('HasEvent')
  	  		Return(.t.)
   Else
         * --- Se l'evento non � cancellazione prosegue
  	  	Dodefault(i_cOp)
   endif
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ELEIMAST','gsar_mit')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mitPag1","gsar_mit",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Manutenzione")
      .Pages(1).HelpContextID = 44096304
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='TIPITRAN'
    this.cWorkTables[3]='NOMENCLA'
    this.cWorkTables[4]='NAZIONI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='AZIENDA'
    this.cWorkTables[7]='MODASPED'
    this.cWorkTables[8]='CON_CONS'
    this.cWorkTables[9]='AZDATINT'
    this.cWorkTables[10]='ELEIMAST'
    this.cWorkTables[11]='ELEIDETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(11))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ELEIMAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ELEIMAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_PISERIAL = NVL(PISERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_30_joined
    link_1_30_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_13_joined
    link_2_13_joined=.f.
    local link_2_14_joined
    link_2_14_joined=.f.
    local link_2_15_joined
    link_2_15_joined=.f.
    local link_2_16_joined
    link_2_16_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from ELEIMAST where PISERIAL=KeySet.PISERIAL
    *
    i_nConn = i_TableProp[this.ELEIMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELEIMAST_IDX,2],this.bLoadRecFilter,this.ELEIMAST_IDX,"gsar_mit")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ELEIMAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ELEIMAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"ELEIDETT.","ELEIMAST.")
      i_cTable = i_cTable+' ELEIMAST '
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_30_joined=this.AddJoinedLink_1_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PISERIAL',this.w_PISERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_READAZI = i_CODAZI
        .w_AITIPE = space(1)
        .w_AITIPS = space(1)
        .w_AITIPV = space(1)
        .w_AITIAS = space(1)
        .w_DESCLF = space(40)
        .w_PIVA = space(12)
        .w_FLINTR = space(1)
        .w_DESVAL = space(10)
        .w_DECVAL = 0
        .w_DESSPE = space(10)
        .w_DATOBSO = ctod("  /  /  ")
        .w_DECVAP = 0
        .w_ANCODISO = g_ISONAZ
        .w_DTOBSO = ctod("  /  /  ")
        .w_DTOBBL = space(1)
        .w_RESCHK = 0
        .w_SIMVAL = space(5)
        .w_SIMVAL1 = space(5)
        .w_NAZION = space(3)
        .w_MODIFICA = .T.
        .w_ANCONCON = space(1)
        .w_HASEVCOP = space(50)
        .w_CODAZI = i_CODAZI
          .link_1_1('Load')
          .link_1_2('Load')
        .w_PISERIAL = NVL(PISERIAL,space(10))
        .op_PISERIAL = .w_PISERIAL
        .w_PINUMREG = NVL(PINUMREG,0)
        .op_PINUMREG = .w_PINUMREG
        .w_PI__ANNO = NVL(PI__ANNO,space(4))
        .op_PI__ANNO = .w_PI__ANNO
        .w_PIDATREG = NVL(cp_ToDate(PIDATREG),ctod("  /  /  "))
        .w_PIALFDOC = NVL(PIALFDOC,space(10))
        .w_PINUMDOC = NVL(PINUMDOC,0)
        .w_PIDATDOC = NVL(cp_ToDate(PIDATDOC),ctod("  /  /  "))
        .w_OBTEST = IIF(EMPTY(.w_PIDATDOC) OR .w_PIDATDOC < i_INIDAT, i_INIDAT,.w_PIDATDOC)
        .w_PIVALNAZ = NVL(PIVALNAZ,space(3))
          if link_1_16_joined
            this.w_PIVALNAZ = NVL(VACODVAL116,NVL(this.w_PIVALNAZ,space(3)))
            this.w_DESVAL = NVL(VADESVAL116,space(10))
            this.w_DECVAP = NVL(VADECTOT116,0)
            this.w_SIMVAL = NVL(VASIMVAL116,space(5))
          else
          .link_1_16('Load')
          endif
        .w_PITIPMOV = NVL(PITIPMOV,space(2))
        .w_PITIPCON = NVL(PITIPCON,space(1))
        .w_PICODCON = NVL(PICODCON,space(15))
          if link_1_19_joined
            this.w_PICODCON = NVL(ANCODICE119,NVL(this.w_PICODCON,space(15)))
            this.w_DESCLF = NVL(ANDESCRI119,space(40))
            this.w_PIVA = NVL(ANPARIVA119,space(12))
            this.w_PIVALORI = NVL(ANCODVAL119,space(3))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO119),ctod("  /  /  "))
            this.w_FLINTR = NVL(AFFLINTR119,space(1))
            this.w_NAZION = NVL(ANNAZION119,space(3))
            this.w_ANCONCON = NVL(ANCONCON119,space(1))
          else
          .link_1_19('Load')
          endif
        .w_TIPPER = IIF(.w_PITIPMOV $ 'CE-RC',.w_AITIPE,IIF(.w_PITIPMOV $ 'CS-RS',.w_AITIPS,IIF(.w_PITIPMOV $ 'AS-RX',.w_AITIAS,.w_AITIPV)))
        .w_PIANNRET = NVL(PIANNRET,space(4))
        .w_PITIPPER = NVL(PITIPPER,space(1))
        .w_PIPERRET = NVL(PIPERRET,0)
        .w_DESPER = IIF(.w_PIPERRET=0, SPACE(25),.w_DESPER)
        .w_PIDATCOM = NVL(cp_ToDate(PIDATCOM),ctod("  /  /  "))
        .w_PIVALORI = NVL(PIVALORI,space(3))
          if link_1_30_joined
            this.w_PIVALORI = NVL(VACODVAL130,NVL(this.w_PIVALORI,space(3)))
            this.w_DESVAL = NVL(VADESVAL130,space(10))
            this.w_DECVAL = NVL(VADECTOT130,0)
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO130),ctod("  /  /  "))
            this.w_SIMVAL1 = NVL(VASIMVAL130,space(5))
          else
          .link_1_30('Load')
          endif
        .w_CAMVAL = GETCAM(.w_PIVALORI,.w_PIDATREG)
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .w_DECPIC = DEFPIC(.w_DECVAL)
        .w_DECPIP = DEFPIP(.w_DECVAP)
        .w_PIRIFDOC = NVL(PIRIFDOC,space(10))
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate(IIF(.w_PITIPCON="C", AH_MsgFormat("Cliente:"),IIF(.w_PITIPCON="F",AH_MsgFormat("Fornitore:"),"")),'','')
        .w_PITIPMOV1 = .w_PITIPMOV
        .w_ARUTISER = IIF(.w_PITIPMOV $ "CE-RC-AC-RA",'N','S')
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ELEIMAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from ELEIDETT where PISERIAL=KeySet.PISERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.ELEIDETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELEIDETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('ELEIDETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "ELEIDETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" ELEIDETT"
        link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_14_joined=this.AddJoinedLink_2_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_15_joined=this.AddJoinedLink_2_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_16_joined=this.AddJoinedLink_2_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'PISERIAL',this.w_PISERIAL  )
        select * from (i_cTable) ELEIDETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_UNSUP = space(3)
          .w_ISONAZ = space(3)
          .w_ISONA2 = space(3)
          .w_ISONA3 = space(3)
        .w_TIPOMOV = .w_PITIPMOV
          .w_CPROWNUM = CPROWNUM
          .w_PIIMPNAZ = NVL(PIIMPNAZ,0)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_PIIMPVAL = NVL(PIIMPVAL,0)
          .w_PINATTRA = NVL(PINATTRA,space(3))
          * evitabile
          *.link_2_4('Load')
          .w_PINOMENC = NVL(PINOMENC,space(8))
          if link_2_5_joined
            this.w_PINOMENC = NVL(NMCODICE205,NVL(this.w_PINOMENC,space(8)))
            this.w_UNSUP = NVL(NMUNISUP205,space(3))
          else
          .link_2_5('Load')
          endif
          .w_PIROWORD = NVL(PIROWORD,0)
          .w_PIMASNET = NVL(PIMASNET,0)
          .w_PIQTASUP = NVL(PIQTASUP,0)
          .w_PIVALSTA = NVL(PIVALSTA,0)
          .w_PICONDCO = NVL(PICONDCO,space(1))
          * evitabile
          *.link_2_12('Load')
          .w_PIMODTRA = NVL(PIMODTRA,space(3))
          if link_2_13_joined
            this.w_PIMODTRA = NVL(SPCODSPE213,NVL(this.w_PIMODTRA,space(3)))
            this.w_DESSPE = NVL(SPDESSPE213,space(10))
          else
          .link_2_13('Load')
          endif
          .w_PINAZORI = NVL(PINAZORI,space(3))
          if link_2_14_joined
            this.w_PINAZORI = NVL(NACODNAZ214,NVL(this.w_PINAZORI,space(3)))
            this.w_ISONAZ = NVL(NACODISO214,space(3))
          else
          .link_2_14('Load')
          endif
          .w_PINAZPRO = NVL(PINAZPRO,space(3))
          if link_2_15_joined
            this.w_PINAZPRO = NVL(NACODNAZ215,NVL(this.w_PINAZPRO,space(3)))
            this.w_ISONA3 = NVL(NACODISO215,space(3))
          else
          .link_2_15('Load')
          endif
          .w_PINAZDES = NVL(PINAZDES,space(3))
          if link_2_16_joined
            this.w_PINAZDES = NVL(NACODNAZ216,NVL(this.w_PINAZDES,space(3)))
            this.w_ISONA2 = NVL(NACODISO216,space(3))
          else
          .link_2_16('Load')
          endif
          .w_PIPRODES = NVL(PIPRODES,space(2))
          .w_PIPROORI = NVL(PIPROORI,space(2))
          .w_PITIPRET = NVL(PITIPRET,space(1))
        .oPgFrm.Page1.oPag.oObj_2_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_28.Calculate()
          .w_PIPORORI = NVL(PIPORORI,space(10))
          .w_PIPORDES = NVL(PIPORDES,space(10))
          .w_PIPAEPAG = NVL(PIPAEPAG,space(3))
          * evitabile
          *.link_2_35('Load')
          .w_PISEZDOG = NVL(PISEZDOG,space(6))
          .w_PIANNREG = NVL(PIANNREG,space(4))
          .w_PIPRORET = NVL(PIPRORET,space(6))
          .w_PIPROGSE = NVL(PIPROGSE,space(5))
          .w_PINUMFAT = NVL(PINUMFAT,0)
          .w_PIALFFAT = NVL(PIALFFAT,space(10))
          .w_PIDATFAT = NVL(cp_ToDate(PIDATFAT),ctod("  /  /  "))
          .w_PIPERSER = NVL(PIPERSER,space(1))
          .w_PIMODINC = NVL(PIMODINC,space(1))
          .w_PIPAEPAG = NVL(PIPAEPAG,space(3))
          * evitabile
          *.link_2_45('Load')
          .w_PIPERSER = NVL(PIPERSER,space(1))
          .w_PIMODINC = NVL(PIMODINC,space(1))
        .oPgFrm.Page1.oPag.oObj_2_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_54.Calculate()
          .w_PIANNREG = NVL(PIANNREG,space(4))
          .w_PIPROGSE = NVL(PIPROGSE,space(5))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CODAZI = i_CODAZI
        .w_OBTEST = IIF(EMPTY(.w_PIDATDOC) OR .w_PIDATDOC < i_INIDAT, i_INIDAT,.w_PIDATDOC)
        .w_TIPPER = IIF(.w_PITIPMOV $ 'CE-RC',.w_AITIPE,IIF(.w_PITIPMOV $ 'CS-RS',.w_AITIPS,IIF(.w_PITIPMOV $ 'AS-RX',.w_AITIAS,.w_AITIPV)))
        .w_DESPER = IIF(.w_PIPERRET=0, SPACE(25),.w_DESPER)
        .w_CAMVAL = GETCAM(.w_PIVALORI,.w_PIDATREG)
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .w_DECPIC = DEFPIC(.w_DECVAL)
        .w_DECPIP = DEFPIP(.w_DECVAP)
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate(IIF(.w_PITIPCON="C", AH_MsgFormat("Cliente:"),IIF(.w_PITIPCON="F",AH_MsgFormat("Fornitore:"),"")),'','')
        .w_PITIPMOV1 = .w_PITIPMOV
        .w_ARUTISER = IIF(.w_PITIPMOV $ "CE-RC-AC-RA",'N','S')
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_69.enabled = .oPgFrm.Page1.oPag.oBtn_1_69.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_29.enabled = .oPgFrm.Page1.oPag.oBtn_2_29.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_57.enabled = .oPgFrm.Page1.oPag.oBtn_2_57.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_mit
        * --- Controlli Frequenza INTRA
    IF this.w_PITIPMOV $ "RC-RA"
        this.NotifyEvent('Frequenza')
    Endif
    
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_PICONDCO")
    CTRL_CONCON.Popola()
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CODAZI=space(5)
      .w_READAZI=space(5)
      .w_AITIPE=space(1)
      .w_AITIPS=space(1)
      .w_AITIPV=space(1)
      .w_AITIAS=space(1)
      .w_PISERIAL=space(10)
      .w_PINUMREG=0
      .w_PI__ANNO=space(4)
      .w_PIDATREG=ctod("  /  /  ")
      .w_PIALFDOC=space(10)
      .w_PINUMDOC=0
      .w_PIDATDOC=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_PIVALNAZ=space(3)
      .w_PITIPMOV=space(2)
      .w_PITIPCON=space(1)
      .w_PICODCON=space(15)
      .w_DESCLF=space(40)
      .w_PIVA=space(12)
      .w_TIPPER=space(1)
      .w_FLINTR=space(1)
      .w_PIANNRET=space(4)
      .w_PITIPPER=space(1)
      .w_PIPERRET=0
      .w_DESPER=space(25)
      .w_PIDATCOM=ctod("  /  /  ")
      .w_PIVALORI=space(3)
      .w_DESVAL=space(10)
      .w_CAMVAL=0
      .w_DECVAL=0
      .w_DECPIC=0
      .w_DESSPE=space(10)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DECVAP=0
      .w_DECPIP=0
      .w_PIRIFDOC=space(10)
      .w_PIIMPNAZ=0
      .w_CPROWORD=10
      .w_PIIMPVAL=0
      .w_PINATTRA=space(3)
      .w_PINOMENC=space(8)
      .w_PIROWORD=0
      .w_UNSUP=space(3)
      .w_PIMASNET=0
      .w_PIQTASUP=0
      .w_ANCODISO=space(3)
      .w_PIVALSTA=0
      .w_PICONDCO=space(1)
      .w_PIMODTRA=space(3)
      .w_PINAZORI=space(3)
      .w_PINAZPRO=space(3)
      .w_PINAZDES=space(3)
      .w_PIPRODES=space(2)
      .w_PIPROORI=space(2)
      .w_PITIPRET=space(1)
      .w_DTOBSO=ctod("  /  /  ")
      .w_DTOBBL=space(1)
      .w_RESCHK=0
      .w_SIMVAL=space(5)
      .w_SIMVAL1=space(5)
      .w_PIPORORI=space(10)
      .w_PIPORDES=space(10)
      .w_NAZION=space(3)
      .w_ISONAZ=space(3)
      .w_ISONA2=space(3)
      .w_ISONA3=space(3)
      .w_PITIPMOV1=space(2)
      .w_ARUTISER=space(1)
      .w_PIPAEPAG=space(3)
      .w_PISEZDOG=space(6)
      .w_PIANNREG=space(4)
      .w_PIPRORET=space(6)
      .w_PIPROGSE=space(5)
      .w_PINUMFAT=0
      .w_PIALFFAT=space(10)
      .w_PIDATFAT=ctod("  /  /  ")
      .w_PIPERSER=space(1)
      .w_PIMODINC=space(1)
      .w_PIPAEPAG=space(3)
      .w_TIPOMOV=space(2)
      .w_PIPERSER=space(1)
      .w_PIMODINC=space(1)
      .w_PIANNREG=space(4)
      .w_PIPROGSE=space(5)
      .w_MODIFICA=.f.
      .w_ANCONCON=space(1)
      .w_HASEVCOP=space(50)
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
         .link_1_1('Full')
        endif
        .w_READAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_READAZI))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,8,.f.)
        .w_PI__ANNO = STR(YEAR(i_DATSYS),4,0)
        .w_PIDATREG = i_datsys
        .DoRTCalc(11,13,.f.)
        .w_OBTEST = IIF(EMPTY(.w_PIDATDOC) OR .w_PIDATDOC < i_INIDAT, i_INIDAT,.w_PIDATDOC)
        .w_PIVALNAZ = g_PERVAL
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_PIVALNAZ))
         .link_1_16('Full')
        endif
        .w_PITIPMOV = 'CE'
        .w_PITIPCON = IIF ( this.cfunction='Edit' , .w_PITIPCON ,  IIF(.w_PITIPMOV $ 'CE-RC-RS-CS', 'C', 'F') )
        .w_PICODCON = IIF (this.cfunction='Edit', .w_PICODCON , space(15))
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_PICODCON))
         .link_1_19('Full')
        endif
        .DoRTCalc(19,20,.f.)
        .w_TIPPER = IIF(.w_PITIPMOV $ 'CE-RC',.w_AITIPE,IIF(.w_PITIPMOV $ 'CS-RS',.w_AITIPS,IIF(.w_PITIPMOV $ 'AS-RX',.w_AITIAS,.w_AITIPV)))
        .DoRTCalc(22,22,.f.)
        .w_PIANNRET = IIF(.w_PITIPMOV$'RC-RA', .w_PI__ANNO, ' ')
        .w_PITIPPER = IIF(EMPTY(NVL(.w_PIANNRET,'')), ' ',.w_TIPPER)
        .w_PIPERRET = IIF(.w_PITIPMOV $ "RC-RA" AND .w_PITIPPER='A',1,0)
        .w_DESPER = IIF(.w_PIPERRET=0, SPACE(25),.w_DESPER)
        .w_PIDATCOM = .w_PIDATDOC
        .w_PIVALORI = .w_PIVALORI
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_PIVALORI))
         .link_1_30('Full')
        endif
        .DoRTCalc(29,29,.f.)
        .w_CAMVAL = GETCAM(.w_PIVALORI,.w_PIDATREG)
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .DoRTCalc(31,31,.f.)
        .w_DECPIC = DEFPIC(.w_DECVAL)
        .DoRTCalc(33,35,.f.)
        .w_DECPIP = DEFPIP(.w_DECVAP)
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .DoRTCalc(37,39,.f.)
        .w_PIIMPVAL = IIF(.w_PIIMPNAZ=0 And .w_PITIPMOV='RX',0,.w_PIIMPVAL)
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_PINATTRA))
         .link_2_4('Full')
        endif
        .w_PINOMENC = IIF(.w_PIIMPNAZ=0 And .w_PITIPMOV $ 'RS-RX' ,space(8),.w_PINOMENC)
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_PINOMENC))
         .link_2_5('Full')
        endif
        .DoRTCalc(43,46,.f.)
        .w_ANCODISO = g_ISONAZ
        .DoRTCalc(48,48,.f.)
        .w_PICONDCO = IIF(g_ISONAZ<>'ESP',IIF(.w_PITIPMOV $ 'RC-RA-CS-RS-AS-RX' OR .w_TIPPER='T','N',IIF(Not Empty(.w_ANCONCON),.w_ANCONCON,'E')),'1')
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_PICONDCO))
         .link_2_12('Full')
        endif
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_PIMODTRA))
         .link_2_13('Full')
        endif
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_PINAZORI))
         .link_2_14('Full')
        endif
        .DoRTCalc(52,52,.f.)
        if not(empty(.w_PINAZPRO))
         .link_2_15('Full')
        endif
        .DoRTCalc(53,53,.f.)
        if not(empty(.w_PINAZDES))
         .link_2_16('Full')
        endif
        .DoRTCalc(54,55,.f.)
        .w_PITIPRET = IIF(.w_PITIPMOV $ 'RC-RA',.w_PITIPRET,' ')
        .oPgFrm.Page1.oPag.oObj_2_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_27.Calculate()
        .DoRTCalc(57,58,.f.)
        .w_RESCHK = 0
        .oPgFrm.Page1.oPag.oObj_2_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate(IIF(.w_PITIPCON="C", AH_MsgFormat("Cliente:"),IIF(.w_PITIPCON="F",AH_MsgFormat("Fornitore:"),"")),'','')
        .DoRTCalc(60,67,.f.)
        .w_PITIPMOV1 = .w_PITIPMOV
        .w_ARUTISER = IIF(.w_PITIPMOV $ "CE-RC-AC-RA",'N','S')
        .DoRTCalc(70,70,.f.)
        if not(empty(.w_PIPAEPAG))
         .link_2_35('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .DoRTCalc(71,77,.f.)
        .w_PIPERSER = 'I'
        .w_PIMODINC = 'X'
        .w_PIPAEPAG = IIF(.w_PIIMPNAZ=0,space(3),.w_PIPAEPAG)
        .DoRTCalc(80,80,.f.)
        if not(empty(.w_PIPAEPAG))
         .link_2_45('Full')
        endif
        .w_TIPOMOV = .w_PITIPMOV
        .w_PIPERSER = 'I'
        .w_PIMODINC = 'X'
        .oPgFrm.Page1.oPag.oObj_2_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_54.Calculate()
        .DoRTCalc(84,85,.f.)
        .w_MODIFICA = .T.
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ELEIMAST')
    this.DoRTCalc(87,92,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_69.enabled = this.oPgFrm.Page1.oPag.oBtn_1_69.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_29.enabled = this.oPgFrm.Page1.oPag.oBtn_2_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_57.enabled = this.oPgFrm.Page1.oPag.oBtn_2_57.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_mit
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_PICONDCO")
    CTRL_CONCON.Popola()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPINUMREG_1_9.enabled = i_bVal
      .Page1.oPag.oPI__ANNO_1_10.enabled = i_bVal
      .Page1.oPag.oPIDATREG_1_11.enabled = i_bVal
      .Page1.oPag.oPIALFDOC_1_12.enabled = i_bVal
      .Page1.oPag.oPINUMDOC_1_13.enabled = i_bVal
      .Page1.oPag.oPIDATDOC_1_14.enabled = i_bVal
      .Page1.oPag.oPITIPMOV_1_17.enabled_(i_bVal)
      .Page1.oPag.oPICODCON_1_19.enabled = i_bVal
      .Page1.oPag.oPIANNRET_1_24.enabled = i_bVal
      .Page1.oPag.oPITIPPER_1_26.enabled = i_bVal
      .Page1.oPag.oPIPERRET_1_27.enabled = i_bVal
      .Page1.oPag.oPIDATCOM_1_29.enabled = i_bVal
      .Page1.oPag.oPIVALORI_1_30.enabled = i_bVal
      .Page1.oPag.oPIMASNET_2_8.enabled = i_bVal
      .Page1.oPag.oPIQTASUP_2_9.enabled = i_bVal
      .Page1.oPag.oPIVALSTA_2_11.enabled = i_bVal
      .Page1.oPag.oPICONDCO_2_12.enabled = i_bVal
      .Page1.oPag.oPIMODTRA_2_13.enabled = i_bVal
      .Page1.oPag.oPINAZORI_2_14.enabled = i_bVal
      .Page1.oPag.oPINAZPRO_2_15.enabled = i_bVal
      .Page1.oPag.oPINAZDES_2_16.enabled = i_bVal
      .Page1.oPag.oPIPRODES_2_17.enabled = i_bVal
      .Page1.oPag.oPIPROORI_2_18.enabled = i_bVal
      .Page1.oPag.oPITIPRET_2_19.enabled = i_bVal
      .Page1.oPag.oPIPAEPAG_2_35.enabled = i_bVal
      .Page1.oPag.oPISEZDOG_2_36.enabled = i_bVal
      .Page1.oPag.oPIANNREG_2_37.enabled = i_bVal
      .Page1.oPag.oPIPRORET_2_38.enabled = i_bVal
      .Page1.oPag.oPIPROGSE_2_39.enabled = i_bVal
      .Page1.oPag.oPINUMFAT_2_40.enabled = i_bVal
      .Page1.oPag.oPIALFFAT_2_41.enabled = i_bVal
      .Page1.oPag.oPIDATFAT_2_42.enabled = i_bVal
      .Page1.oPag.oPIPERSER_2_43.enabled = i_bVal
      .Page1.oPag.oPIMODINC_2_44.enabled = i_bVal
      .Page1.oPag.oPIPAEPAG_2_45.enabled = i_bVal
      .Page1.oPag.oPIPERSER_2_48.enabled = i_bVal
      .Page1.oPag.oPIMODINC_2_49.enabled = i_bVal
      .Page1.oPag.oBtn_1_69.enabled = .Page1.oPag.oBtn_1_69.mCond()
      .Page1.oPag.oBtn_2_29.enabled = .Page1.oPag.oBtn_2_29.mCond()
      .Page1.oPag.oBtn_2_46.enabled = i_bVal
      .Page1.oPag.oBtn_2_57.enabled = .Page1.oPag.oBtn_2_57.mCond()
      .Page1.oPag.oObj_1_45.enabled = i_bVal
      .Page1.oPag.oObj_1_46.enabled = i_bVal
      .Page1.oPag.oObj_1_68.enabled = i_bVal
      .Page1.oPag.oObj_2_20.enabled = i_bVal
      .Page1.oPag.oObj_2_21.enabled = i_bVal
      .Page1.oPag.oObj_2_22.enabled = i_bVal
      .Page1.oPag.oObj_2_23.enabled = i_bVal
      .Page1.oPag.oObj_2_24.enabled = i_bVal
      .Page1.oPag.oObj_2_25.enabled = i_bVal
      .Page1.oPag.oObj_2_26.enabled = i_bVal
      .Page1.oPag.oObj_2_27.enabled = i_bVal
      .Page1.oPag.oObj_2_28.enabled = i_bVal
      .Page1.oPag.oObj_1_81.enabled = i_bVal
      .Page1.oPag.oObj_2_50.enabled = i_bVal
      .Page1.oPag.oObj_2_51.enabled = i_bVal
      .Page1.oPag.oObj_2_52.enabled = i_bVal
      .Page1.oPag.oObj_2_53.enabled = i_bVal
      .Page1.oPag.oObj_2_54.enabled = i_bVal
      .Page1.oPag.oObj_1_98.enabled = i_bVal
      .Page1.oPag.oObj_1_103.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oPINUMREG_1_9.enabled = .t.
        .Page1.oPag.oPIDATREG_1_11.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ELEIMAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ELEIMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELEIMAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEINT","i_codazi,w_PISERIAL")
      cp_AskTableProg(this,i_nConn,"PRINT","i_codazi,w_PI__ANNO,w_PINUMREG")
      .op_codazi = .w_codazi
      .op_PISERIAL = .w_PISERIAL
      .op_codazi = .w_codazi
      .op_PI__ANNO = .w_PI__ANNO
      .op_PINUMREG = .w_PINUMREG
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ELEIMAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PISERIAL,"PISERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PINUMREG,"PINUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PI__ANNO,"PI__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PIDATREG,"PIDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PIALFDOC,"PIALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PINUMDOC,"PINUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PIDATDOC,"PIDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PIVALNAZ,"PIVALNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PITIPMOV,"PITIPMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PITIPCON,"PITIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PICODCON,"PICODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PIANNRET,"PIANNRET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PITIPPER,"PITIPPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PIPERRET,"PIPERRET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PIDATCOM,"PIDATCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PIVALORI,"PIVALORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PIRIFDOC,"PIRIFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ELEIMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELEIMAST_IDX,2])
    i_lTable = "ELEIMAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ELEIMAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SIB with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PIIMPNAZ N(18,4);
      ,t_CPROWORD N(5);
      ,t_PIIMPVAL N(18,4);
      ,t_PINATTRA C(3);
      ,t_PINOMENC C(8);
      ,t_PIROWORD N(5);
      ,t_PIMASNET N(12,3);
      ,t_PIQTASUP N(12,3);
      ,t_PIVALSTA N(18,4);
      ,t_PICONDCO N(3);
      ,t_PIMODTRA C(3);
      ,t_PINAZORI C(3);
      ,t_PINAZPRO C(3);
      ,t_PINAZDES C(3);
      ,t_PIPRODES C(2);
      ,t_PIPROORI C(2);
      ,t_PITIPRET C(1);
      ,t_ISONAZ C(3);
      ,t_ISONA2 C(3);
      ,t_ISONA3 C(3);
      ,t_PIPAEPAG C(3);
      ,t_PISEZDOG C(6);
      ,t_PIANNREG C(4);
      ,t_PIPRORET C(6);
      ,t_PIPROGSE C(5);
      ,t_PINUMFAT N(15);
      ,t_PIALFFAT C(10);
      ,t_PIDATFAT D(8);
      ,t_PIPERSER N(3);
      ,t_PIMODINC N(3);
      ,CPROWNUM N(10);
      ,t_UNSUP C(3);
      ,t_PIPORORI C(10);
      ,t_PIPORDES C(10);
      ,t_TIPOMOV C(2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mitbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPIIMPNAZ_2_1.controlsource=this.cTrsName+'.t_PIIMPNAZ'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPIIMPVAL_2_3.controlsource=this.cTrsName+'.t_PIIMPVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPINATTRA_2_4.controlsource=this.cTrsName+'.t_PINATTRA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPINOMENC_2_5.controlsource=this.cTrsName+'.t_PINOMENC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPIROWORD_2_6.controlsource=this.cTrsName+'.t_PIROWORD'
    this.oPgFRm.Page1.oPag.oPIMASNET_2_8.controlsource=this.cTrsName+'.t_PIMASNET'
    this.oPgFRm.Page1.oPag.oPIQTASUP_2_9.controlsource=this.cTrsName+'.t_PIQTASUP'
    this.oPgFRm.Page1.oPag.oPIVALSTA_2_11.controlsource=this.cTrsName+'.t_PIVALSTA'
    this.oPgFRm.Page1.oPag.oPICONDCO_2_12.controlsource=this.cTrsName+'.t_PICONDCO'
    this.oPgFRm.Page1.oPag.oPIMODTRA_2_13.controlsource=this.cTrsName+'.t_PIMODTRA'
    this.oPgFRm.Page1.oPag.oPINAZORI_2_14.controlsource=this.cTrsName+'.t_PINAZORI'
    this.oPgFRm.Page1.oPag.oPINAZPRO_2_15.controlsource=this.cTrsName+'.t_PINAZPRO'
    this.oPgFRm.Page1.oPag.oPINAZDES_2_16.controlsource=this.cTrsName+'.t_PINAZDES'
    this.oPgFRm.Page1.oPag.oPIPRODES_2_17.controlsource=this.cTrsName+'.t_PIPRODES'
    this.oPgFRm.Page1.oPag.oPIPROORI_2_18.controlsource=this.cTrsName+'.t_PIPROORI'
    this.oPgFRm.Page1.oPag.oPITIPRET_2_19.controlsource=this.cTrsName+'.t_PITIPRET'
    this.oPgFRm.Page1.oPag.oISONAZ_2_32.controlsource=this.cTrsName+'.t_ISONAZ'
    this.oPgFRm.Page1.oPag.oISONA2_2_33.controlsource=this.cTrsName+'.t_ISONA2'
    this.oPgFRm.Page1.oPag.oISONA3_2_34.controlsource=this.cTrsName+'.t_ISONA3'
    this.oPgFRm.Page1.oPag.oPIPAEPAG_2_35.controlsource=this.cTrsName+'.t_PIPAEPAG'
    this.oPgFRm.Page1.oPag.oPISEZDOG_2_36.controlsource=this.cTrsName+'.t_PISEZDOG'
    this.oPgFRm.Page1.oPag.oPIANNREG_2_37.controlsource=this.cTrsName+'.t_PIANNREG'
    this.oPgFRm.Page1.oPag.oPIPRORET_2_38.controlsource=this.cTrsName+'.t_PIPRORET'
    this.oPgFRm.Page1.oPag.oPIPROGSE_2_39.controlsource=this.cTrsName+'.t_PIPROGSE'
    this.oPgFRm.Page1.oPag.oPINUMFAT_2_40.controlsource=this.cTrsName+'.t_PINUMFAT'
    this.oPgFRm.Page1.oPag.oPIALFFAT_2_41.controlsource=this.cTrsName+'.t_PIALFFAT'
    this.oPgFRm.Page1.oPag.oPIDATFAT_2_42.controlsource=this.cTrsName+'.t_PIDATFAT'
    this.oPgFRm.Page1.oPag.oPIPERSER_2_43.controlsource=this.cTrsName+'.t_PIPERSER'
    this.oPgFRm.Page1.oPag.oPIMODINC_2_44.controlsource=this.cTrsName+'.t_PIMODINC'
    this.oPgFRm.Page1.oPag.oPIPAEPAG_2_45.controlsource=this.cTrsName+'.t_PIPAEPAG'
    this.oPgFRm.Page1.oPag.oPIPERSER_2_48.controlsource=this.cTrsName+'.t_PIPERSER'
    this.oPgFRm.Page1.oPag.oPIMODINC_2_49.controlsource=this.cTrsName+'.t_PIMODINC'
    this.oPgFRm.Page1.oPag.oPIANNREG_2_55.controlsource=this.cTrsName+'.t_PIANNREG'
    this.oPgFRm.Page1.oPag.oPIPROGSE_2_56.controlsource=this.cTrsName+'.t_PIPROGSE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(76)
    this.AddVLine(240)
    this.AddVLine(402)
    this.AddVLine(443)
    this.AddVLine(533)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIIMPNAZ_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ELEIMAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELEIMAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEINT","i_codazi,w_PISERIAL")
          cp_NextTableProg(this,i_nConn,"PRINT","i_codazi,w_PI__ANNO,w_PINUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ELEIMAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ELEIMAST')
        i_extval=cp_InsertValODBCExtFlds(this,'ELEIMAST')
        local i_cFld
        i_cFld=" "+;
                  "(PISERIAL,PINUMREG,PI__ANNO,PIDATREG,PIALFDOC"+;
                  ",PINUMDOC,PIDATDOC,PIVALNAZ,PITIPMOV,PITIPCON"+;
                  ",PICODCON,PIANNRET,PITIPPER,PIPERRET,PIDATCOM"+;
                  ",PIVALORI,PIRIFDOC,UTCC,UTDC,UTDV"+;
                  ",UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_PISERIAL)+;
                    ","+cp_ToStrODBC(this.w_PINUMREG)+;
                    ","+cp_ToStrODBC(this.w_PI__ANNO)+;
                    ","+cp_ToStrODBC(this.w_PIDATREG)+;
                    ","+cp_ToStrODBC(this.w_PIALFDOC)+;
                    ","+cp_ToStrODBC(this.w_PINUMDOC)+;
                    ","+cp_ToStrODBC(this.w_PIDATDOC)+;
                    ","+cp_ToStrODBCNull(this.w_PIVALNAZ)+;
                    ","+cp_ToStrODBC(this.w_PITIPMOV)+;
                    ","+cp_ToStrODBC(this.w_PITIPCON)+;
                    ","+cp_ToStrODBCNull(this.w_PICODCON)+;
                    ","+cp_ToStrODBC(this.w_PIANNRET)+;
                    ","+cp_ToStrODBC(this.w_PITIPPER)+;
                    ","+cp_ToStrODBC(this.w_PIPERRET)+;
                    ","+cp_ToStrODBC(this.w_PIDATCOM)+;
                    ","+cp_ToStrODBCNull(this.w_PIVALORI)+;
                    ","+cp_ToStrODBC(this.w_PIRIFDOC)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ELEIMAST')
        i_extval=cp_InsertValVFPExtFlds(this,'ELEIMAST')
        cp_CheckDeletedKey(i_cTable,0,'PISERIAL',this.w_PISERIAL)
        INSERT INTO (i_cTable);
              (PISERIAL,PINUMREG,PI__ANNO,PIDATREG,PIALFDOC,PINUMDOC,PIDATDOC,PIVALNAZ,PITIPMOV,PITIPCON,PICODCON,PIANNRET,PITIPPER,PIPERRET,PIDATCOM,PIVALORI,PIRIFDOC,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_PISERIAL;
                  ,this.w_PINUMREG;
                  ,this.w_PI__ANNO;
                  ,this.w_PIDATREG;
                  ,this.w_PIALFDOC;
                  ,this.w_PINUMDOC;
                  ,this.w_PIDATDOC;
                  ,this.w_PIVALNAZ;
                  ,this.w_PITIPMOV;
                  ,this.w_PITIPCON;
                  ,this.w_PICODCON;
                  ,this.w_PIANNRET;
                  ,this.w_PITIPPER;
                  ,this.w_PIPERRET;
                  ,this.w_PIDATCOM;
                  ,this.w_PIVALORI;
                  ,this.w_PIRIFDOC;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ELEIDETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELEIDETT_IDX,2])
      *
      * insert into ELEIDETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(PISERIAL,PIIMPNAZ,CPROWORD,PIIMPVAL,PINATTRA"+;
                  ",PINOMENC,PIROWORD,PIMASNET,PIQTASUP,PIVALSTA"+;
                  ",PICONDCO,PIMODTRA,PINAZORI,PINAZPRO,PINAZDES"+;
                  ",PIPRODES,PIPROORI,PITIPRET,PIPORORI,PIPORDES"+;
                  ",PIPAEPAG,PISEZDOG,PIANNREG,PIPRORET,PIPROGSE"+;
                  ",PINUMFAT,PIALFFAT,PIDATFAT,PIPERSER,PIMODINC,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PISERIAL)+","+cp_ToStrODBC(this.w_PIIMPNAZ)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_PIIMPVAL)+","+cp_ToStrODBCNull(this.w_PINATTRA)+;
             ","+cp_ToStrODBCNull(this.w_PINOMENC)+","+cp_ToStrODBC(this.w_PIROWORD)+","+cp_ToStrODBC(this.w_PIMASNET)+","+cp_ToStrODBC(this.w_PIQTASUP)+","+cp_ToStrODBC(this.w_PIVALSTA)+;
             ","+cp_ToStrODBCNull(this.w_PICONDCO)+","+cp_ToStrODBCNull(this.w_PIMODTRA)+","+cp_ToStrODBCNull(this.w_PINAZORI)+","+cp_ToStrODBCNull(this.w_PINAZPRO)+","+cp_ToStrODBCNull(this.w_PINAZDES)+;
             ","+cp_ToStrODBC(this.w_PIPRODES)+","+cp_ToStrODBC(this.w_PIPROORI)+","+cp_ToStrODBC(this.w_PITIPRET)+","+cp_ToStrODBC(this.w_PIPORORI)+","+cp_ToStrODBC(this.w_PIPORDES)+;
             ","+cp_ToStrODBCNull(this.w_PIPAEPAG)+","+cp_ToStrODBC(this.w_PISEZDOG)+","+cp_ToStrODBC(this.w_PIANNREG)+","+cp_ToStrODBC(this.w_PIPRORET)+","+cp_ToStrODBC(this.w_PIPROGSE)+;
             ","+cp_ToStrODBC(this.w_PINUMFAT)+","+cp_ToStrODBC(this.w_PIALFFAT)+","+cp_ToStrODBC(this.w_PIDATFAT)+","+cp_ToStrODBC(this.w_PIPERSER)+","+cp_ToStrODBC(this.w_PIMODINC)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PISERIAL',this.w_PISERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_PISERIAL,this.w_PIIMPNAZ,this.w_CPROWORD,this.w_PIIMPVAL,this.w_PINATTRA"+;
                ",this.w_PINOMENC,this.w_PIROWORD,this.w_PIMASNET,this.w_PIQTASUP,this.w_PIVALSTA"+;
                ",this.w_PICONDCO,this.w_PIMODTRA,this.w_PINAZORI,this.w_PINAZPRO,this.w_PINAZDES"+;
                ",this.w_PIPRODES,this.w_PIPROORI,this.w_PITIPRET,this.w_PIPORORI,this.w_PIPORDES"+;
                ",this.w_PIPAEPAG,this.w_PISEZDOG,this.w_PIANNREG,this.w_PIPRORET,this.w_PIPROGSE"+;
                ",this.w_PINUMFAT,this.w_PIALFFAT,this.w_PIDATFAT,this.w_PIPERSER,this.w_PIMODINC,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.ELEIMAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ELEIMAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update ELEIMAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'ELEIMAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " PINUMREG="+cp_ToStrODBC(this.w_PINUMREG)+;
             ",PI__ANNO="+cp_ToStrODBC(this.w_PI__ANNO)+;
             ",PIDATREG="+cp_ToStrODBC(this.w_PIDATREG)+;
             ",PIALFDOC="+cp_ToStrODBC(this.w_PIALFDOC)+;
             ",PINUMDOC="+cp_ToStrODBC(this.w_PINUMDOC)+;
             ",PIDATDOC="+cp_ToStrODBC(this.w_PIDATDOC)+;
             ",PIVALNAZ="+cp_ToStrODBCNull(this.w_PIVALNAZ)+;
             ",PITIPMOV="+cp_ToStrODBC(this.w_PITIPMOV)+;
             ",PITIPCON="+cp_ToStrODBC(this.w_PITIPCON)+;
             ",PICODCON="+cp_ToStrODBCNull(this.w_PICODCON)+;
             ",PIANNRET="+cp_ToStrODBC(this.w_PIANNRET)+;
             ",PITIPPER="+cp_ToStrODBC(this.w_PITIPPER)+;
             ",PIPERRET="+cp_ToStrODBC(this.w_PIPERRET)+;
             ",PIDATCOM="+cp_ToStrODBC(this.w_PIDATCOM)+;
             ",PIVALORI="+cp_ToStrODBCNull(this.w_PIVALORI)+;
             ",PIRIFDOC="+cp_ToStrODBC(this.w_PIRIFDOC)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'ELEIMAST')
          i_cWhere = cp_PKFox(i_cTable  ,'PISERIAL',this.w_PISERIAL  )
          UPDATE (i_cTable) SET;
              PINUMREG=this.w_PINUMREG;
             ,PI__ANNO=this.w_PI__ANNO;
             ,PIDATREG=this.w_PIDATREG;
             ,PIALFDOC=this.w_PIALFDOC;
             ,PINUMDOC=this.w_PINUMDOC;
             ,PIDATDOC=this.w_PIDATDOC;
             ,PIVALNAZ=this.w_PIVALNAZ;
             ,PITIPMOV=this.w_PITIPMOV;
             ,PITIPCON=this.w_PITIPCON;
             ,PICODCON=this.w_PICODCON;
             ,PIANNRET=this.w_PIANNRET;
             ,PITIPPER=this.w_PITIPPER;
             ,PIPERRET=this.w_PIPERRET;
             ,PIDATCOM=this.w_PIDATCOM;
             ,PIVALORI=this.w_PIVALORI;
             ,PIRIFDOC=this.w_PIRIFDOC;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWORD<>0  and ( (t_TIPOMOV $ 'CS-CE-RC-AC-AS-RA' AND (t_PIIMPNAZ <>0 or t_PIVALSTA<>0 OR t_PIMASNET<>0 ) ) OR ( t_TIPOMOV $ 'RS-RX' AND (NOT EMPTY(t_PIPROGSE) OR t_PIIMPNAZ<>0) ) )) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.ELEIDETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.ELEIDETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from ELEIDETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ELEIDETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PIIMPNAZ="+cp_ToStrODBC(this.w_PIIMPNAZ)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",PIIMPVAL="+cp_ToStrODBC(this.w_PIIMPVAL)+;
                     ",PINATTRA="+cp_ToStrODBCNull(this.w_PINATTRA)+;
                     ",PINOMENC="+cp_ToStrODBCNull(this.w_PINOMENC)+;
                     ",PIROWORD="+cp_ToStrODBC(this.w_PIROWORD)+;
                     ",PIMASNET="+cp_ToStrODBC(this.w_PIMASNET)+;
                     ",PIQTASUP="+cp_ToStrODBC(this.w_PIQTASUP)+;
                     ",PIVALSTA="+cp_ToStrODBC(this.w_PIVALSTA)+;
                     ",PICONDCO="+cp_ToStrODBCNull(this.w_PICONDCO)+;
                     ",PIMODTRA="+cp_ToStrODBCNull(this.w_PIMODTRA)+;
                     ",PINAZORI="+cp_ToStrODBCNull(this.w_PINAZORI)+;
                     ",PINAZPRO="+cp_ToStrODBCNull(this.w_PINAZPRO)+;
                     ",PINAZDES="+cp_ToStrODBCNull(this.w_PINAZDES)+;
                     ",PIPRODES="+cp_ToStrODBC(this.w_PIPRODES)+;
                     ",PIPROORI="+cp_ToStrODBC(this.w_PIPROORI)+;
                     ",PITIPRET="+cp_ToStrODBC(this.w_PITIPRET)+;
                     ",PIPORORI="+cp_ToStrODBC(this.w_PIPORORI)+;
                     ",PIPORDES="+cp_ToStrODBC(this.w_PIPORDES)+;
                     ",PIPAEPAG="+cp_ToStrODBCNull(this.w_PIPAEPAG)+;
                     ",PISEZDOG="+cp_ToStrODBC(this.w_PISEZDOG)+;
                     ",PIANNREG="+cp_ToStrODBC(this.w_PIANNREG)+;
                     ",PIPRORET="+cp_ToStrODBC(this.w_PIPRORET)+;
                     ",PIPROGSE="+cp_ToStrODBC(this.w_PIPROGSE)+;
                     ",PINUMFAT="+cp_ToStrODBC(this.w_PINUMFAT)+;
                     ",PIALFFAT="+cp_ToStrODBC(this.w_PIALFFAT)+;
                     ",PIDATFAT="+cp_ToStrODBC(this.w_PIDATFAT)+;
                     ",PIPERSER="+cp_ToStrODBC(this.w_PIPERSER)+;
                     ",PIMODINC="+cp_ToStrODBC(this.w_PIMODINC)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PIIMPNAZ=this.w_PIIMPNAZ;
                     ,CPROWORD=this.w_CPROWORD;
                     ,PIIMPVAL=this.w_PIIMPVAL;
                     ,PINATTRA=this.w_PINATTRA;
                     ,PINOMENC=this.w_PINOMENC;
                     ,PIROWORD=this.w_PIROWORD;
                     ,PIMASNET=this.w_PIMASNET;
                     ,PIQTASUP=this.w_PIQTASUP;
                     ,PIVALSTA=this.w_PIVALSTA;
                     ,PICONDCO=this.w_PICONDCO;
                     ,PIMODTRA=this.w_PIMODTRA;
                     ,PINAZORI=this.w_PINAZORI;
                     ,PINAZPRO=this.w_PINAZPRO;
                     ,PINAZDES=this.w_PINAZDES;
                     ,PIPRODES=this.w_PIPRODES;
                     ,PIPROORI=this.w_PIPROORI;
                     ,PITIPRET=this.w_PITIPRET;
                     ,PIPORORI=this.w_PIPORORI;
                     ,PIPORDES=this.w_PIPORDES;
                     ,PIPAEPAG=this.w_PIPAEPAG;
                     ,PISEZDOG=this.w_PISEZDOG;
                     ,PIANNREG=this.w_PIANNREG;
                     ,PIPRORET=this.w_PIPRORET;
                     ,PIPROGSE=this.w_PIPROGSE;
                     ,PINUMFAT=this.w_PINUMFAT;
                     ,PIALFFAT=this.w_PIALFFAT;
                     ,PIDATFAT=this.w_PIDATFAT;
                     ,PIPERSER=this.w_PIPERSER;
                     ,PIMODINC=this.w_PIMODINC;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0  and ( (t_TIPOMOV $ 'CS-CE-RC-AC-AS-RA' AND (t_PIIMPNAZ <>0 or t_PIVALSTA<>0 OR t_PIMASNET<>0 ) ) OR ( t_TIPOMOV $ 'RS-RX' AND (NOT EMPTY(t_PIPROGSE) OR t_PIIMPNAZ<>0) ) )) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.ELEIDETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.ELEIDETT_IDX,2])
        *
        * delete ELEIDETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.ELEIMAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.ELEIMAST_IDX,2])
        *
        * delete ELEIMAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0  and ( (t_TIPOMOV $ 'CS-CE-RC-AC-AS-RA' AND (t_PIIMPNAZ <>0 or t_PIVALSTA<>0 OR t_PIMASNET<>0 ) ) OR ( t_TIPOMOV $ 'RS-RX' AND (NOT EMPTY(t_PIPROGSE) OR t_PIIMPNAZ<>0) ) )) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ELEIMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ELEIMAST_IDX,2])
    if i_bUpd
      with this
        if .o_PISERIAL<>.w_PISERIAL
          .w_CODAZI = i_CODAZI
          .link_1_1('Full')
        endif
          .link_1_2('Full')
        .DoRTCalc(3,13,.t.)
          .w_OBTEST = IIF(EMPTY(.w_PIDATDOC) OR .w_PIDATDOC < i_INIDAT, i_INIDAT,.w_PIDATDOC)
          .link_1_16('Full')
        .DoRTCalc(16,16,.t.)
          .w_PITIPCON = IIF ( this.cfunction='Edit' , .w_PITIPCON ,  IIF(.w_PITIPMOV $ 'CE-RC-RS-CS', 'C', 'F') )
        if .o_PITIPMOV1<>.w_PITIPMOV1
          .w_PICODCON = IIF (this.cfunction='Edit', .w_PICODCON , space(15))
          .link_1_19('Full')
        endif
        .DoRTCalc(19,20,.t.)
          .w_TIPPER = IIF(.w_PITIPMOV $ 'CE-RC',.w_AITIPE,IIF(.w_PITIPMOV $ 'CS-RS',.w_AITIPS,IIF(.w_PITIPMOV $ 'AS-RX',.w_AITIAS,.w_AITIPV)))
        .DoRTCalc(22,22,.t.)
        if .o_PI__ANNO<>.w_PI__ANNO.or. .o_PITIPMOV<>.w_PITIPMOV
          .w_PIANNRET = IIF(.w_PITIPMOV$'RC-RA', .w_PI__ANNO, ' ')
        endif
        if .o_PIANNRET<>.w_PIANNRET.or. .o_TIPPER<>.w_TIPPER
          .w_PITIPPER = IIF(EMPTY(NVL(.w_PIANNRET,'')), ' ',.w_TIPPER)
        endif
        if .o_PIANNRET<>.w_PIANNRET.or. .o_PITIPPER<>.w_PITIPPER.or. .o_PITIPMOV<>.w_PITIPMOV
          .w_PIPERRET = IIF(.w_PITIPMOV $ "RC-RA" AND .w_PITIPPER='A',1,0)
        endif
        if .o_PIPERRET<>.w_PIPERRET
          .w_DESPER = IIF(.w_PIPERRET=0, SPACE(25),.w_DESPER)
        endif
        if .o_PIDATDOC<>.w_PIDATDOC
          .w_PIDATCOM = .w_PIDATDOC
        endif
        if .o_PIVALORI<>.w_PIVALORI
          .w_PIVALORI = .w_PIVALORI
          .link_1_30('Full')
        endif
        .DoRTCalc(29,29,.t.)
          .w_CAMVAL = GETCAM(.w_PIVALORI,.w_PIDATREG)
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .DoRTCalc(31,31,.t.)
          .w_DECPIC = DEFPIC(.w_DECVAL)
        .DoRTCalc(33,35,.t.)
          .w_DECPIP = DEFPIP(.w_DECVAP)
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .DoRTCalc(37,39,.t.)
        if .o_PIIMPNAZ<>.w_PIIMPNAZ
          .w_PIIMPVAL = IIF(.w_PIIMPNAZ=0 And .w_PITIPMOV='RX',0,.w_PIIMPVAL)
        endif
        .DoRTCalc(41,41,.t.)
        if .o_PIIMPNAZ<>.w_PIIMPNAZ
          .w_PINOMENC = IIF(.w_PIIMPNAZ=0 And .w_PITIPMOV $ 'RS-RX' ,space(8),.w_PINOMENC)
          .link_2_5('Full')
        endif
        .DoRTCalc(43,48,.t.)
        if .o_PICODCON<>.w_PICODCON
          .link_2_12('Full')
        endif
        .DoRTCalc(50,55,.t.)
        if .o_PITIPMOV<>.w_PITIPMOV
          .w_PITIPRET = IIF(.w_PITIPMOV $ 'RC-RA',.w_PITIPRET,' ')
        endif
        .oPgFrm.Page1.oPag.oObj_2_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate(IIF(.w_PITIPCON="C", AH_MsgFormat("Cliente:"),IIF(.w_PITIPCON="F",AH_MsgFormat("Fornitore:"),"")),'','')
        .DoRTCalc(57,67,.t.)
          .w_PITIPMOV1 = .w_PITIPMOV
        if .o_PITIPMOV1<>.w_PITIPMOV1
          .w_ARUTISER = IIF(.w_PITIPMOV $ "CE-RC-AC-RA",'N','S')
        endif
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        if .o_ARUTISER<>.w_ARUTISER
          .Calculate_WFAMROENJC()
        endif
        .DoRTCalc(70,77,.t.)
        if .o_PIIMPNAZ<>.w_PIIMPNAZ
          .w_PIPERSER = 'I'
        endif
        if .o_PIIMPNAZ<>.w_PIIMPNAZ
          .w_PIMODINC = 'X'
        endif
        if .o_PIIMPNAZ<>.w_PIIMPNAZ
          .w_PIPAEPAG = IIF(.w_PIIMPNAZ=0,space(3),.w_PIPAEPAG)
          .link_2_45('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_2_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEINT","i_codazi,w_PISERIAL")
          .op_PISERIAL = .w_PISERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_PI__ANNO<>.w_PI__ANNO
           cp_AskTableProg(this,i_nConn,"PRINT","i_codazi,w_PI__ANNO,w_PINUMREG")
          .op_PINUMREG = .w_PINUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_PI__ANNO = .w_PI__ANNO
      endwith
      this.DoRTCalc(81,92,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_UNSUP with this.w_UNSUP
      replace t_PIPORORI with this.w_PIPORORI
      replace t_PIPORDES with this.w_PIPORDES
      replace t_TIPOMOV with this.w_TIPOMOV
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_45.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate(IIF(.w_PITIPCON="C", AH_MsgFormat("Cliente:"),IIF(.w_PITIPCON="F",AH_MsgFormat("Fornitore:"),"")),'','')
        .oPgFrm.Page1.oPag.oObj_1_81.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_98.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_103.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_54.Calculate()
    endwith
  return
  proc Calculate_WFAMROENJC()
    with this
          * --- w_ARUTISER Changed
          GSAR_BIR(this;
              ,'NO';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPITIPMOV_1_17.enabled_(this.oPgFrm.Page1.oPag.oPITIPMOV_1_17.mCond())
    this.oPgFrm.Page1.oPag.oPIANNRET_1_24.enabled = this.oPgFrm.Page1.oPag.oPIANNRET_1_24.mCond()
    this.oPgFrm.Page1.oPag.oPITIPPER_1_26.enabled = this.oPgFrm.Page1.oPag.oPITIPPER_1_26.mCond()
    this.oPgFrm.Page1.oPag.oPIPERRET_1_27.enabled = this.oPgFrm.Page1.oPag.oPIPERRET_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_69.enabled = this.oPgFrm.Page1.oPag.oBtn_1_69.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPIIMPVAL_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPIIMPVAL_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPINATTRA_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPINATTRA_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPINOMENC_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPINOMENC_2_5.mCond()
    this.oPgFrm.Page1.oPag.oPIMASNET_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPIMASNET_2_8.mCond()
    this.oPgFrm.Page1.oPag.oPIQTASUP_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPIQTASUP_2_9.mCond()
    this.oPgFrm.Page1.oPag.oPIVALSTA_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPIVALSTA_2_11.mCond()
    this.oPgFrm.Page1.oPag.oPICONDCO_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPICONDCO_2_12.mCond()
    this.oPgFrm.Page1.oPag.oPIMODTRA_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPIMODTRA_2_13.mCond()
    this.oPgFrm.Page1.oPag.oPINAZORI_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPINAZORI_2_14.mCond()
    this.oPgFrm.Page1.oPag.oPINAZPRO_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPINAZPRO_2_15.mCond()
    this.oPgFrm.Page1.oPag.oPINAZDES_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPINAZDES_2_16.mCond()
    this.oPgFrm.Page1.oPag.oPIPRODES_2_17.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPIPRODES_2_17.mCond()
    this.oPgFrm.Page1.oPag.oPIPROORI_2_18.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPIPROORI_2_18.mCond()
    this.oPgFrm.Page1.oPag.oPITIPRET_2_19.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPITIPRET_2_19.mCond()
    this.oPgFrm.Page1.oPag.oPINUMFAT_2_40.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPINUMFAT_2_40.mCond()
    this.oPgFrm.Page1.oPag.oPIALFFAT_2_41.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPIALFFAT_2_41.mCond()
    this.oPgFrm.Page1.oPag.oPIDATFAT_2_42.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPIDATFAT_2_42.mCond()
    this.oPgFrm.Page1.oPag.oPIPERSER_2_43.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPIPERSER_2_43.mCond()
    this.oPgFrm.Page1.oPag.oPIMODINC_2_44.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPIMODINC_2_44.mCond()
    this.oPgFrm.Page1.oPag.oPIPAEPAG_2_45.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPIPAEPAG_2_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_29.enabled =this.oPgFrm.Page1.oPag.oBtn_2_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_46.enabled =this.oPgFrm.Page1.oPag.oBtn_2_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_57.enabled =this.oPgFrm.Page1.oPag.oBtn_2_57.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPIPERRET_1_27.visible=!this.oPgFrm.Page1.oPag.oPIPERRET_1_27.mHide()
    this.oPgFrm.Page1.oPag.oDESPER_1_28.visible=!this.oPgFrm.Page1.oPag.oDESPER_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_69.visible=!this.oPgFrm.Page1.oPag.oBtn_1_69.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_77.visible=!this.oPgFrm.Page1.oPag.oStr_1_77.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_78.visible=!this.oPgFrm.Page1.oPag.oStr_1_78.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_83.visible=!this.oPgFrm.Page1.oPag.oStr_1_83.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_84.visible=!this.oPgFrm.Page1.oPag.oStr_1_84.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_85.visible=!this.oPgFrm.Page1.oPag.oStr_1_85.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_86.visible=!this.oPgFrm.Page1.oPag.oStr_1_86.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_87.visible=!this.oPgFrm.Page1.oPag.oStr_1_87.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_88.visible=!this.oPgFrm.Page1.oPag.oStr_1_88.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_89.visible=!this.oPgFrm.Page1.oPag.oStr_1_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_90.visible=!this.oPgFrm.Page1.oPag.oStr_1_90.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_91.visible=!this.oPgFrm.Page1.oPag.oStr_1_91.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_93.visible=!this.oPgFrm.Page1.oPag.oStr_1_93.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_94.visible=!this.oPgFrm.Page1.oPag.oStr_1_94.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_95.visible=!this.oPgFrm.Page1.oPag.oStr_1_95.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_99.visible=!this.oPgFrm.Page1.oPag.oStr_1_99.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_100.visible=!this.oPgFrm.Page1.oPag.oStr_1_100.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_101.visible=!this.oPgFrm.Page1.oPag.oStr_1_101.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oPIMASNET_2_8.visible=!this.oPgFrm.Page1.oPag.oPIMASNET_2_8.mHide()
    this.oPgFrm.Page1.oPag.oPIQTASUP_2_9.visible=!this.oPgFrm.Page1.oPag.oPIQTASUP_2_9.mHide()
    this.oPgFrm.Page1.oPag.oPIVALSTA_2_11.visible=!this.oPgFrm.Page1.oPag.oPIVALSTA_2_11.mHide()
    this.oPgFrm.Page1.oPag.oPICONDCO_2_12.visible=!this.oPgFrm.Page1.oPag.oPICONDCO_2_12.mHide()
    this.oPgFrm.Page1.oPag.oPIMODTRA_2_13.visible=!this.oPgFrm.Page1.oPag.oPIMODTRA_2_13.mHide()
    this.oPgFrm.Page1.oPag.oPINAZORI_2_14.visible=!this.oPgFrm.Page1.oPag.oPINAZORI_2_14.mHide()
    this.oPgFrm.Page1.oPag.oPINAZPRO_2_15.visible=!this.oPgFrm.Page1.oPag.oPINAZPRO_2_15.mHide()
    this.oPgFrm.Page1.oPag.oPINAZDES_2_16.visible=!this.oPgFrm.Page1.oPag.oPINAZDES_2_16.mHide()
    this.oPgFrm.Page1.oPag.oPIPRODES_2_17.visible=!this.oPgFrm.Page1.oPag.oPIPRODES_2_17.mHide()
    this.oPgFrm.Page1.oPag.oPIPROORI_2_18.visible=!this.oPgFrm.Page1.oPag.oPIPROORI_2_18.mHide()
    this.oPgFrm.Page1.oPag.oPITIPRET_2_19.visible=!this.oPgFrm.Page1.oPag.oPITIPRET_2_19.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_29.visible=!this.oPgFrm.Page1.oPag.oBtn_2_29.mHide()
    this.oPgFrm.Page1.oPag.oISONAZ_2_32.visible=!this.oPgFrm.Page1.oPag.oISONAZ_2_32.mHide()
    this.oPgFrm.Page1.oPag.oISONA2_2_33.visible=!this.oPgFrm.Page1.oPag.oISONA2_2_33.mHide()
    this.oPgFrm.Page1.oPag.oISONA3_2_34.visible=!this.oPgFrm.Page1.oPag.oISONA3_2_34.mHide()
    this.oPgFrm.Page1.oPag.oPIPAEPAG_2_35.visible=!this.oPgFrm.Page1.oPag.oPIPAEPAG_2_35.mHide()
    this.oPgFrm.Page1.oPag.oPISEZDOG_2_36.visible=!this.oPgFrm.Page1.oPag.oPISEZDOG_2_36.mHide()
    this.oPgFrm.Page1.oPag.oPIANNREG_2_37.visible=!this.oPgFrm.Page1.oPag.oPIANNREG_2_37.mHide()
    this.oPgFrm.Page1.oPag.oPIPRORET_2_38.visible=!this.oPgFrm.Page1.oPag.oPIPRORET_2_38.mHide()
    this.oPgFrm.Page1.oPag.oPIPROGSE_2_39.visible=!this.oPgFrm.Page1.oPag.oPIPROGSE_2_39.mHide()
    this.oPgFrm.Page1.oPag.oPINUMFAT_2_40.visible=!this.oPgFrm.Page1.oPag.oPINUMFAT_2_40.mHide()
    this.oPgFrm.Page1.oPag.oPIALFFAT_2_41.visible=!this.oPgFrm.Page1.oPag.oPIALFFAT_2_41.mHide()
    this.oPgFrm.Page1.oPag.oPIDATFAT_2_42.visible=!this.oPgFrm.Page1.oPag.oPIDATFAT_2_42.mHide()
    this.oPgFrm.Page1.oPag.oPIPERSER_2_43.visible=!this.oPgFrm.Page1.oPag.oPIPERSER_2_43.mHide()
    this.oPgFrm.Page1.oPag.oPIMODINC_2_44.visible=!this.oPgFrm.Page1.oPag.oPIMODINC_2_44.mHide()
    this.oPgFrm.Page1.oPag.oPIPAEPAG_2_45.visible=!this.oPgFrm.Page1.oPag.oPIPAEPAG_2_45.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_46.visible=!this.oPgFrm.Page1.oPag.oBtn_2_46.mHide()
    this.oPgFrm.Page1.oPag.oPIPERSER_2_48.visible=!this.oPgFrm.Page1.oPag.oPIPERSER_2_48.mHide()
    this.oPgFrm.Page1.oPag.oPIMODINC_2_49.visible=!this.oPgFrm.Page1.oPag.oPIMODINC_2_49.mHide()
    this.oPgFrm.Page1.oPag.oPIANNREG_2_55.visible=!this.oPgFrm.Page1.oPag.oPIANNREG_2_55.mHide()
    this.oPgFrm.Page1.oPag.oPIPROGSE_2_56.visible=!this.oPgFrm.Page1.oPag.oPIPROGSE_2_56.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_57.visible=!this.oPgFrm.Page1.oPag.oBtn_2_57.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_45.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_68.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_26.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_76.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_81.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_51.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_52.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_53.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_54.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_98.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_103.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZDTOBBL";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZDTOBBL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_DTOBBL = NVL(_Link_.AZDTOBBL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_DTOBBL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZDATINT_IDX,3]
    i_lTable = "AZDATINT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZDATINT_IDX,2], .t., this.AZDATINT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZDATINT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODAZI,ITAITIPE,ITAITIPV,ITAITIPS,ITAITIAS";
                   +" from "+i_cTable+" "+i_lTable+" where ITCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODAZI',this.w_READAZI)
            select ITCODAZI,ITAITIPE,ITAITIPV,ITAITIPS,ITAITIAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.ITCODAZI,space(5))
      this.w_AITIPE = NVL(_Link_.ITAITIPE,space(1))
      this.w_AITIPV = NVL(_Link_.ITAITIPV,space(1))
      this.w_AITIPS = NVL(_Link_.ITAITIPS,space(1))
      this.w_AITIAS = NVL(_Link_.ITAITIAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_AITIPE = space(1)
      this.w_AITIPV = space(1)
      this.w_AITIPS = space(1)
      this.w_AITIAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZDATINT_IDX,2])+'\'+cp_ToStr(_Link_.ITCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZDATINT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PIVALNAZ
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PIVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PIVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PIVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PIVALNAZ)
            select VACODVAL,VADESVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PIVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(10))
      this.w_DECVAP = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PIVALNAZ = space(3)
      endif
      this.w_DESVAL = space(10)
      this.w_DECVAP = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PIVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.VACODVAL as VACODVAL116"+ ",link_1_16.VADESVAL as VADESVAL116"+ ",link_1_16.VADECTOT as VADECTOT116"+ ",link_1_16.VASIMVAL as VASIMVAL116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on ELEIMAST.PIVALNAZ=link_1_16.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and ELEIMAST.PIVALNAZ=link_1_16.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PICODCON
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PICODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PICODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PITIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANDTOBSO,AFFLINTR,ANNAZION,ANCONCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PITIPCON;
                     ,'ANCODICE',trim(this.w_PICODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANDTOBSO,AFFLINTR,ANNAZION,ANCONCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PICODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PICODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PITIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANDTOBSO,AFFLINTR,ANNAZION,ANCONCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PICODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PITIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANDTOBSO,AFFLINTR,ANNAZION,ANCONCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANPARIVA like "+cp_ToStrODBC(trim(this.w_PICODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PITIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANDTOBSO,AFFLINTR,ANNAZION,ANCONCON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANPARIVA like "+cp_ToStr(trim(this.w_PICODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PITIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANDTOBSO,AFFLINTR,ANNAZION,ANCONCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PICODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPICODCON_1_19'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'GSAR_MIT.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PITIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANDTOBSO,AFFLINTR,ANNAZION,ANCONCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANDTOBSO,AFFLINTR,ANNAZION,ANCONCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANDTOBSO,AFFLINTR,ANNAZION,ANCONCON";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PITIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANDTOBSO,AFFLINTR,ANNAZION,ANCONCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PICODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANDTOBSO,AFFLINTR,ANNAZION,ANCONCON";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PICODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PITIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PITIPCON;
                       ,'ANCODICE',this.w_PICODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANCODVAL,ANDTOBSO,AFFLINTR,ANNAZION,ANCONCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PICODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
      this.w_PIVA = NVL(_Link_.ANPARIVA,space(12))
      this.w_PIVALORI = NVL(_Link_.ANCODVAL,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_FLINTR = NVL(_Link_.AFFLINTR,space(1))
      this.w_NAZION = NVL(_Link_.ANNAZION,space(3))
      this.w_ANCONCON = NVL(_Link_.ANCONCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PICODCON = space(15)
      endif
      this.w_DESCLF = space(40)
      this.w_PIVA = space(12)
      this.w_PIVALORI = space(3)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLINTR = space(1)
      this.w_NAZION = space(3)
      this.w_ANCONCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND .w_FLINTR='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_PICODCON = space(15)
        this.w_DESCLF = space(40)
        this.w_PIVA = space(12)
        this.w_PIVALORI = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLINTR = space(1)
        this.w_NAZION = space(3)
        this.w_ANCONCON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PICODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.ANCODICE as ANCODICE119"+ ",link_1_19.ANDESCRI as ANDESCRI119"+ ",link_1_19.ANPARIVA as ANPARIVA119"+ ",link_1_19.ANCODVAL as ANCODVAL119"+ ",link_1_19.ANDTOBSO as ANDTOBSO119"+ ",link_1_19.AFFLINTR as AFFLINTR119"+ ",link_1_19.ANNAZION as ANNAZION119"+ ",link_1_19.ANCONCON as ANCONCON119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on ELEIMAST.PICODCON=link_1_19.ANCODICE"+" and ELEIMAST.PITIPCON=link_1_19.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and ELEIMAST.PICODCON=link_1_19.ANCODICE(+)"'+'+" and ELEIMAST.PITIPCON=link_1_19.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PIVALORI
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PIVALORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_PIVALORI)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_PIVALORI))
          select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PIVALORI)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_PIVALORI)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_PIVALORI)+"%");

            select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PIVALORI) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oPIVALORI_1_30'),i_cWhere,'GSAR_AVL',"Elenco valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PIVALORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PIVALORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PIVALORI)
            select VACODVAL,VADESVAL,VADECTOT,VADTOBSO,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PIVALORI = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(10))
      this.w_DECVAL = NVL(_Link_.VADECTOT,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_SIMVAL1 = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PIVALORI = space(3)
      endif
      this.w_DESVAL = space(10)
      this.w_DECVAL = 0
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_SIMVAL1 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta originaria incongruente o obsoleta")
        endif
        this.w_PIVALORI = space(3)
        this.w_DESVAL = space(10)
        this.w_DECVAL = 0
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_SIMVAL1 = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PIVALORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_30.VACODVAL as VACODVAL130"+ ",link_1_30.VADESVAL as VADESVAL130"+ ",link_1_30.VADECTOT as VADECTOT130"+ ",link_1_30.VADTOBSO as VADTOBSO130"+ ",link_1_30.VASIMVAL as VASIMVAL130"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_30 on ELEIMAST.PIVALORI=link_1_30.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_30"
          i_cKey=i_cKey+'+" and ELEIMAST.PIVALORI=link_1_30.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PINATTRA
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPITRAN_IDX,3]
    i_lTable = "TIPITRAN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2], .t., this.TIPITRAN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PINATTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATT',True,'TIPITRAN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TTCODICE like "+cp_ToStrODBC(trim(this.w_PINATTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select TTCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TTCODICE',trim(this.w_PINATTRA))
          select TTCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PINATTRA)==trim(_Link_.TTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PINATTRA) and !this.bDontReportError
            deferred_cp_zoom('TIPITRAN','*','TTCODICE',cp_AbsName(oSource.parent,'oPINATTRA_2_4'),i_cWhere,'GSAR_ATT',"Tipo transazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where TTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODICE',oSource.xKey(1))
            select TTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PINATTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODICE="+cp_ToStrODBC(this.w_PINATTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODICE',this.w_PINATTRA)
            select TTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PINATTRA = NVL(_Link_.TTCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PINATTRA = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPITRAN_IDX,2])+'\'+cp_ToStr(_Link_.TTCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPITRAN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PINATTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PINOMENC
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
    i_lTable = "NOMENCLA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2], .t., this.NOMENCLA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PINOMENC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANM',True,'NOMENCLA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NMCODICE like "+cp_ToStrODBC(trim(this.w_PINOMENC)+"%");

          i_ret=cp_SQL(i_nConn,"select NMCODICE,NMUNISUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NMCODICE',trim(this.w_PINOMENC))
          select NMCODICE,NMUNISUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PINOMENC)==trim(_Link_.NMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PINOMENC) and !this.bDontReportError
            deferred_cp_zoom('NOMENCLA','*','NMCODICE',cp_AbsName(oSource.parent,'oPINOMENC_2_5'),i_cWhere,'GSAR_ANM',"Nomenclatura",'GSMA_AZN.NOMENCLA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMUNISUP";
                     +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',oSource.xKey(1))
            select NMCODICE,NMUNISUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PINOMENC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NMCODICE,NMUNISUP";
                   +" from "+i_cTable+" "+i_lTable+" where NMCODICE="+cp_ToStrODBC(this.w_PINOMENC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NMCODICE',this.w_PINOMENC)
            select NMCODICE,NMUNISUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PINOMENC = NVL(_Link_.NMCODICE,space(8))
      this.w_UNSUP = NVL(_Link_.NMUNISUP,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PINOMENC = space(8)
      endif
      this.w_UNSUP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_PITIPMOV $ "CE-RC-AC-RA") OR (.w_PITIPMOV $ "CS-RS-AS-RX" AND ( LEN(ALLTRIM(.w_PINOMENC))=6 OR LEN(ALLTRIM(.w_PINOMENC))=5))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Nomenclatura non valida o non definita")
        endif
        this.w_PINOMENC = space(8)
        this.w_UNSUP = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])+'\'+cp_ToStr(_Link_.NMCODICE,1)
      cp_ShowWarn(i_cKey,this.NOMENCLA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PINOMENC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NOMENCLA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.NMCODICE as NMCODICE205"+ ",link_2_5.NMUNISUP as NMUNISUP205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on ELEIDETT.PINOMENC=link_2_5.NMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and ELEIDETT.PINOMENC=link_2_5.NMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PICONDCO
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_CONS_IDX,3]
    i_lTable = "CON_CONS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_CONS_IDX,2], .t., this.CON_CONS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_CONS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PICONDCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CON_CONS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COCODICE like "+cp_ToStrODBC(trim(this.w_PICONDCO)+"%");
                   +" and CCCODISO="+cp_ToStrODBC(this.w_ANCODISO);

          i_ret=cp_SQL(i_nConn,"select CCCODISO,COCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODISO,COCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODISO',this.w_ANCODISO;
                     ,'COCODICE',trim(this.w_PICONDCO))
          select CCCODISO,COCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODISO,COCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PICONDCO)==trim(_Link_.COCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PICONDCO) and !this.bDontReportError
            deferred_cp_zoom('CON_CONS','*','CCCODISO,COCODICE',cp_AbsName(oSource.parent,'oPICONDCO_2_12'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANCODISO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODISO,COCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCCODISO,COCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODISO,COCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where COCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CCCODISO="+cp_ToStrODBC(this.w_ANCODISO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODISO',oSource.xKey(1);
                       ,'COCODICE',oSource.xKey(2))
            select CCCODISO,COCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PICONDCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODISO,COCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where COCODICE="+cp_ToStrODBC(this.w_PICONDCO);
                   +" and CCCODISO="+cp_ToStrODBC(this.w_ANCODISO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODISO',this.w_ANCODISO;
                       ,'COCODICE',this.w_PICONDCO)
            select CCCODISO,COCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PICONDCO = NVL(_Link_.COCODICE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PICONDCO = space(1)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_CONS_IDX,2])+'\'+cp_ToStr(_Link_.CCCODISO,1)+'\'+cp_ToStr(_Link_.COCODICE,1)
      cp_ShowWarn(i_cKey,this.CON_CONS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PICONDCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PIMODTRA
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_lTable = "MODASPED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2], .t., this.MODASPED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PIMODTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASP',True,'MODASPED')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SPCODSPE like "+cp_ToStrODBC(trim(this.w_PIMODTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SPCODSPE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SPCODSPE',trim(this.w_PIMODTRA))
          select SPCODSPE,SPDESSPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SPCODSPE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PIMODTRA)==trim(_Link_.SPCODSPE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PIMODTRA) and !this.bDontReportError
            deferred_cp_zoom('MODASPED','*','SPCODSPE',cp_AbsName(oSource.parent,'oPIMODTRA_2_13'),i_cWhere,'GSAR_ASP',"Spedizioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                     +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',oSource.xKey(1))
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PIMODTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                   +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(this.w_PIMODTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',this.w_PIMODTRA)
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PIMODTRA = NVL(_Link_.SPCODSPE,space(3))
      this.w_DESSPE = NVL(cp_TransLoadField('_Link_.SPDESSPE'),space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PIMODTRA = space(3)
      endif
      this.w_DESSPE = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])+'\'+cp_ToStr(_Link_.SPCODSPE,1)
      cp_ShowWarn(i_cKey,this.MODASPED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PIMODTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODASPED_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.SPCODSPE as SPCODSPE213"+ ","+cp_TransLinkFldName('link_2_13.SPDESSPE')+" as SPDESSPE213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on ELEIDETT.PIMODTRA=link_2_13.SPCODSPE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and ELEIDETT.PIMODTRA=link_2_13.SPCODSPE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PINAZORI
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PINAZORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_PINAZORI)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_PINAZORI))
          select NACODNAZ,NACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PINAZORI)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PINAZORI) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oPINAZORI_2_14'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PINAZORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_PINAZORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_PINAZORI)
            select NACODNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PINAZORI = NVL(_Link_.NACODNAZ,space(3))
      this.w_ISONAZ = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PINAZORI = space(3)
      endif
      this.w_ISONAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PINAZORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_14.NACODNAZ as NACODNAZ214"+ ",link_2_14.NACODISO as NACODISO214"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_14 on ELEIDETT.PINAZORI=link_2_14.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_14"
          i_cKey=i_cKey+'+" and ELEIDETT.PINAZORI=link_2_14.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PINAZPRO
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PINAZPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_PINAZPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_PINAZPRO))
          select NACODNAZ,NACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PINAZPRO)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PINAZPRO) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oPINAZPRO_2_15'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PINAZPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_PINAZPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_PINAZPRO)
            select NACODNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PINAZPRO = NVL(_Link_.NACODNAZ,space(3))
      this.w_ISONA3 = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PINAZPRO = space(3)
      endif
      this.w_ISONA3 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PINAZPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_15.NACODNAZ as NACODNAZ215"+ ",link_2_15.NACODISO as NACODISO215"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_15 on ELEIDETT.PINAZPRO=link_2_15.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_15"
          i_cKey=i_cKey+'+" and ELEIDETT.PINAZPRO=link_2_15.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PINAZDES
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PINAZDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_PINAZDES)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_PINAZDES))
          select NACODNAZ,NACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PINAZDES)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PINAZDES) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oPINAZDES_2_16'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PINAZDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_PINAZDES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_PINAZDES)
            select NACODNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PINAZDES = NVL(_Link_.NACODNAZ,space(3))
      this.w_ISONA2 = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PINAZDES = space(3)
      endif
      this.w_ISONA2 = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PINAZDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_16.NACODNAZ as NACODNAZ216"+ ",link_2_16.NACODISO as NACODISO216"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_16 on ELEIDETT.PINAZDES=link_2_16.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_16"
          i_cKey=i_cKey+'+" and ELEIDETT.PINAZDES=link_2_16.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PIPAEPAG
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PIPAEPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_PIPAEPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_PIPAEPAG))
          select NACODNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PIPAEPAG)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PIPAEPAG) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oPIPAEPAG_2_35'),i_cWhere,'GSAR_ANZ',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PIPAEPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_PIPAEPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_PIPAEPAG)
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PIPAEPAG = NVL(_Link_.NACODNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PIPAEPAG = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PIPAEPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PIPAEPAG
  func Link_2_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PIPAEPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_PIPAEPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_PIPAEPAG))
          select NACODNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PIPAEPAG)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PIPAEPAG) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oPIPAEPAG_2_45'),i_cWhere,'GSAR_ANZ',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PIPAEPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_PIPAEPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_PIPAEPAG)
            select NACODNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PIPAEPAG = NVL(_Link_.NACODNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PIPAEPAG = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PIPAEPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPINUMREG_1_9.value==this.w_PINUMREG)
      this.oPgFrm.Page1.oPag.oPINUMREG_1_9.value=this.w_PINUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPI__ANNO_1_10.value==this.w_PI__ANNO)
      this.oPgFrm.Page1.oPag.oPI__ANNO_1_10.value=this.w_PI__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oPIDATREG_1_11.value==this.w_PIDATREG)
      this.oPgFrm.Page1.oPag.oPIDATREG_1_11.value=this.w_PIDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPIALFDOC_1_12.value==this.w_PIALFDOC)
      this.oPgFrm.Page1.oPag.oPIALFDOC_1_12.value=this.w_PIALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPINUMDOC_1_13.value==this.w_PINUMDOC)
      this.oPgFrm.Page1.oPag.oPINUMDOC_1_13.value=this.w_PINUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPIDATDOC_1_14.value==this.w_PIDATDOC)
      this.oPgFrm.Page1.oPag.oPIDATDOC_1_14.value=this.w_PIDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPIVALNAZ_1_16.value==this.w_PIVALNAZ)
      this.oPgFrm.Page1.oPag.oPIVALNAZ_1_16.value=this.w_PIVALNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oPITIPMOV_1_17.RadioValue()==this.w_PITIPMOV)
      this.oPgFrm.Page1.oPag.oPITIPMOV_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPICODCON_1_19.value==this.w_PICODCON)
      this.oPgFrm.Page1.oPag.oPICODCON_1_19.value=this.w_PICODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_20.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_20.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oPIVA_1_21.value==this.w_PIVA)
      this.oPgFrm.Page1.oPag.oPIVA_1_21.value=this.w_PIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oPIANNRET_1_24.value==this.w_PIANNRET)
      this.oPgFrm.Page1.oPag.oPIANNRET_1_24.value=this.w_PIANNRET
    endif
    if not(this.oPgFrm.Page1.oPag.oPITIPPER_1_26.RadioValue()==this.w_PITIPPER)
      this.oPgFrm.Page1.oPag.oPITIPPER_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPIPERRET_1_27.value==this.w_PIPERRET)
      this.oPgFrm.Page1.oPag.oPIPERRET_1_27.value=this.w_PIPERRET
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPER_1_28.value==this.w_DESPER)
      this.oPgFrm.Page1.oPag.oDESPER_1_28.value=this.w_DESPER
    endif
    if not(this.oPgFrm.Page1.oPag.oPIDATCOM_1_29.value==this.w_PIDATCOM)
      this.oPgFrm.Page1.oPag.oPIDATCOM_1_29.value=this.w_PIDATCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oPIVALORI_1_30.value==this.w_PIVALORI)
      this.oPgFrm.Page1.oPag.oPIVALORI_1_30.value=this.w_PIVALORI
    endif
    if not(this.oPgFrm.Page1.oPag.oPIMASNET_2_8.value==this.w_PIMASNET)
      this.oPgFrm.Page1.oPag.oPIMASNET_2_8.value=this.w_PIMASNET
      replace t_PIMASNET with this.oPgFrm.Page1.oPag.oPIMASNET_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIQTASUP_2_9.value==this.w_PIQTASUP)
      this.oPgFrm.Page1.oPag.oPIQTASUP_2_9.value=this.w_PIQTASUP
      replace t_PIQTASUP with this.oPgFrm.Page1.oPag.oPIQTASUP_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIVALSTA_2_11.value==this.w_PIVALSTA)
      this.oPgFrm.Page1.oPag.oPIVALSTA_2_11.value=this.w_PIVALSTA
      replace t_PIVALSTA with this.oPgFrm.Page1.oPag.oPIVALSTA_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPICONDCO_2_12.RadioValue()==this.w_PICONDCO)
      this.oPgFrm.Page1.oPag.oPICONDCO_2_12.SetRadio()
      replace t_PICONDCO with this.oPgFrm.Page1.oPag.oPICONDCO_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIMODTRA_2_13.value==this.w_PIMODTRA)
      this.oPgFrm.Page1.oPag.oPIMODTRA_2_13.value=this.w_PIMODTRA
      replace t_PIMODTRA with this.oPgFrm.Page1.oPag.oPIMODTRA_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPINAZORI_2_14.value==this.w_PINAZORI)
      this.oPgFrm.Page1.oPag.oPINAZORI_2_14.value=this.w_PINAZORI
      replace t_PINAZORI with this.oPgFrm.Page1.oPag.oPINAZORI_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPINAZPRO_2_15.value==this.w_PINAZPRO)
      this.oPgFrm.Page1.oPag.oPINAZPRO_2_15.value=this.w_PINAZPRO
      replace t_PINAZPRO with this.oPgFrm.Page1.oPag.oPINAZPRO_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPINAZDES_2_16.value==this.w_PINAZDES)
      this.oPgFrm.Page1.oPag.oPINAZDES_2_16.value=this.w_PINAZDES
      replace t_PINAZDES with this.oPgFrm.Page1.oPag.oPINAZDES_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIPRODES_2_17.value==this.w_PIPRODES)
      this.oPgFrm.Page1.oPag.oPIPRODES_2_17.value=this.w_PIPRODES
      replace t_PIPRODES with this.oPgFrm.Page1.oPag.oPIPRODES_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIPROORI_2_18.value==this.w_PIPROORI)
      this.oPgFrm.Page1.oPag.oPIPROORI_2_18.value=this.w_PIPROORI
      replace t_PIPROORI with this.oPgFrm.Page1.oPag.oPIPROORI_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPITIPRET_2_19.value==this.w_PITIPRET)
      this.oPgFrm.Page1.oPag.oPITIPRET_2_19.value=this.w_PITIPRET
      replace t_PITIPRET with this.oPgFrm.Page1.oPag.oPITIPRET_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_73.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_73.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL1_1_74.value==this.w_SIMVAL1)
      this.oPgFrm.Page1.oPag.oSIMVAL1_1_74.value=this.w_SIMVAL1
    endif
    if not(this.oPgFrm.Page1.oPag.oISONAZ_2_32.value==this.w_ISONAZ)
      this.oPgFrm.Page1.oPag.oISONAZ_2_32.value=this.w_ISONAZ
      replace t_ISONAZ with this.oPgFrm.Page1.oPag.oISONAZ_2_32.value
    endif
    if not(this.oPgFrm.Page1.oPag.oISONA2_2_33.value==this.w_ISONA2)
      this.oPgFrm.Page1.oPag.oISONA2_2_33.value=this.w_ISONA2
      replace t_ISONA2 with this.oPgFrm.Page1.oPag.oISONA2_2_33.value
    endif
    if not(this.oPgFrm.Page1.oPag.oISONA3_2_34.value==this.w_ISONA3)
      this.oPgFrm.Page1.oPag.oISONA3_2_34.value=this.w_ISONA3
      replace t_ISONA3 with this.oPgFrm.Page1.oPag.oISONA3_2_34.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIPAEPAG_2_35.value==this.w_PIPAEPAG)
      this.oPgFrm.Page1.oPag.oPIPAEPAG_2_35.value=this.w_PIPAEPAG
      replace t_PIPAEPAG with this.oPgFrm.Page1.oPag.oPIPAEPAG_2_35.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPISEZDOG_2_36.value==this.w_PISEZDOG)
      this.oPgFrm.Page1.oPag.oPISEZDOG_2_36.value=this.w_PISEZDOG
      replace t_PISEZDOG with this.oPgFrm.Page1.oPag.oPISEZDOG_2_36.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIANNREG_2_37.value==this.w_PIANNREG)
      this.oPgFrm.Page1.oPag.oPIANNREG_2_37.value=this.w_PIANNREG
      replace t_PIANNREG with this.oPgFrm.Page1.oPag.oPIANNREG_2_37.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIPRORET_2_38.value==this.w_PIPRORET)
      this.oPgFrm.Page1.oPag.oPIPRORET_2_38.value=this.w_PIPRORET
      replace t_PIPRORET with this.oPgFrm.Page1.oPag.oPIPRORET_2_38.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIPROGSE_2_39.value==this.w_PIPROGSE)
      this.oPgFrm.Page1.oPag.oPIPROGSE_2_39.value=this.w_PIPROGSE
      replace t_PIPROGSE with this.oPgFrm.Page1.oPag.oPIPROGSE_2_39.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPINUMFAT_2_40.value==this.w_PINUMFAT)
      this.oPgFrm.Page1.oPag.oPINUMFAT_2_40.value=this.w_PINUMFAT
      replace t_PINUMFAT with this.oPgFrm.Page1.oPag.oPINUMFAT_2_40.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIALFFAT_2_41.value==this.w_PIALFFAT)
      this.oPgFrm.Page1.oPag.oPIALFFAT_2_41.value=this.w_PIALFFAT
      replace t_PIALFFAT with this.oPgFrm.Page1.oPag.oPIALFFAT_2_41.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIDATFAT_2_42.value==this.w_PIDATFAT)
      this.oPgFrm.Page1.oPag.oPIDATFAT_2_42.value=this.w_PIDATFAT
      replace t_PIDATFAT with this.oPgFrm.Page1.oPag.oPIDATFAT_2_42.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIPERSER_2_43.RadioValue()==this.w_PIPERSER)
      this.oPgFrm.Page1.oPag.oPIPERSER_2_43.SetRadio()
      replace t_PIPERSER with this.oPgFrm.Page1.oPag.oPIPERSER_2_43.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIMODINC_2_44.RadioValue()==this.w_PIMODINC)
      this.oPgFrm.Page1.oPag.oPIMODINC_2_44.SetRadio()
      replace t_PIMODINC with this.oPgFrm.Page1.oPag.oPIMODINC_2_44.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIPAEPAG_2_45.value==this.w_PIPAEPAG)
      this.oPgFrm.Page1.oPag.oPIPAEPAG_2_45.value=this.w_PIPAEPAG
      replace t_PIPAEPAG with this.oPgFrm.Page1.oPag.oPIPAEPAG_2_45.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIPERSER_2_48.RadioValue()==this.w_PIPERSER)
      this.oPgFrm.Page1.oPag.oPIPERSER_2_48.SetRadio()
      replace t_PIPERSER with this.oPgFrm.Page1.oPag.oPIPERSER_2_48.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIMODINC_2_49.RadioValue()==this.w_PIMODINC)
      this.oPgFrm.Page1.oPag.oPIMODINC_2_49.SetRadio()
      replace t_PIMODINC with this.oPgFrm.Page1.oPag.oPIMODINC_2_49.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIANNREG_2_55.value==this.w_PIANNREG)
      this.oPgFrm.Page1.oPag.oPIANNREG_2_55.value=this.w_PIANNREG
      replace t_PIANNREG with this.oPgFrm.Page1.oPag.oPIANNREG_2_55.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPIPROGSE_2_56.value==this.w_PIPROGSE)
      this.oPgFrm.Page1.oPag.oPIPROGSE_2_56.value=this.w_PIPROGSE
      replace t_PIPROGSE with this.oPgFrm.Page1.oPag.oPIPROGSE_2_56.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIIMPNAZ_2_1.value==this.w_PIIMPNAZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIIMPNAZ_2_1.value=this.w_PIIMPNAZ
      replace t_PIIMPNAZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIIMPNAZ_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIIMPVAL_2_3.value==this.w_PIIMPVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIIMPVAL_2_3.value=this.w_PIIMPVAL
      replace t_PIIMPVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIIMPVAL_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPINATTRA_2_4.value==this.w_PINATTRA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPINATTRA_2_4.value=this.w_PINATTRA
      replace t_PINATTRA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPINATTRA_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPINOMENC_2_5.value==this.w_PINOMENC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPINOMENC_2_5.value=this.w_PINOMENC
      replace t_PINOMENC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPINOMENC_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIROWORD_2_6.value==this.w_PIROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIROWORD_2_6.value=this.w_PIROWORD
      replace t_PIROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIROWORD_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'ELEIMAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PI__ANNO) or not(VAL(.w_PI__ANNO)>1900))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPI__ANNO_1_10.SetFocus()
            i_bnoObbl = !empty(.w_PI__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_PIDATDOC>i_INIDAT and not .w_PITIPMOV $ 'RS-RX') or .w_PITIPMOV $ 'RS-RX' )
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPIDATDOC_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data non corretta")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) AND .w_FLINTR='S')  and not(empty(.w_PICODCON))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPICODCON_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   (empty(.w_PIANNRET) or not(VAL(.w_PIANNRET)>1900))  and (.w_PITIPMOV $ "RC-RA" AND UPPER(.cFunction) <> "QUERY")
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPIANNRET_1_24.SetFocus()
            i_bnoObbl = !empty(.w_PIANNRET)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PITIPPER))  and (.w_PITIPMOV $ "RC-RA" AND .w_PI__ANNO<>.w_PIANNRET)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPITIPPER_1_26.SetFocus()
            i_bnoObbl = !empty(.w_PITIPPER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PIPERRET) or not(.w_PIPERRET>0 AND ((.w_PITIPPER='M' AND .w_PIPERRET<13) OR (.w_PITIPPER='T' AND .w_PIPERRET<5))))  and not(.w_PITIPPER='A')  and (.w_PITIPMOV $ "RC-RA" AND NOT EMPTY(.w_PIANNRET) AND .w_PITIPPER<>'A' AND UPPER(.cFunction) <> "QUERY")
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPIPERRET_1_27.SetFocus()
            i_bnoObbl = !empty(.w_PIPERRET)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PIDATCOM>i_INIDAT)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPIDATCOM_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data non corretta")
          case   not(CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.))  and not(empty(.w_PIVALORI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPIVALORI_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta originaria incongruente o obsoleta")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_mit
      if i_bRes=.t. and .cFunction<>'Query'
         .w_TIPO=this.w_PITIPMOV
         .w_RESCHK=0
         ah_Msg('Controlli finali...' ,.T.)
         .NotifyEvent('ControlliFinali')
         WAIT CLEAR
         if .w_RESCHK<>0
            i_bRes=.f.
            return(i_bRes)
         endif
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_PIIMPNAZ >= 0) and (.w_CPROWORD<>0  and ( (.w_TIPOMOV $ 'CS-CE-RC-AC-AS-RA' AND (.w_PIIMPNAZ <>0 or .w_PIVALSTA<>0 OR .w_PIMASNET<>0 ) ) OR ( .w_TIPOMOV $ 'RS-RX' AND (NOT EMPTY(.w_PIPROGSE) OR .w_PIIMPNAZ<>0) ) ))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIIMPNAZ_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("L'importo non pu� essere negativo")
        case   not(.w_PIIMPNAZ=0 or .w_CAMVAL<> 0 or (.w_PIIMPNAZ > 0 and .w_CAMVAL = 0 and .w_PIIMPVAL > 0)) and ((.w_PIIMPNAZ<>0 And .w_PITIPMOV='RX') OR .w_PITIPMOV $ "AC-RA-AS") and (.w_CPROWORD<>0  and ( (.w_TIPOMOV $ 'CS-CE-RC-AC-AS-RA' AND (.w_PIIMPNAZ <>0 or .w_PIVALSTA<>0 OR .w_PIMASNET<>0 ) ) OR ( .w_TIPOMOV $ 'RS-RX' AND (NOT EMPTY(.w_PIPROGSE) OR .w_PIIMPNAZ<>0) ) ))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPIIMPVAL_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valuta fornitore non aderente all'EMU: l'importo deve essere maggiore di zero")
        case   not((.w_PITIPMOV $ "CE-RC-AC-RA") OR (.w_PITIPMOV $ "CS-RS-AS-RX" AND ( LEN(ALLTRIM(.w_PINOMENC))=6 OR LEN(ALLTRIM(.w_PINOMENC))=5))) and (.w_PIIMPNAZ<>0 OR .w_PITIPMOV $ ' CE-RC-AC-RA-CS-AS') and not(empty(.w_PINOMENC)) and (.w_CPROWORD<>0  and ( (.w_TIPOMOV $ 'CS-CE-RC-AC-AS-RA' AND (.w_PIIMPNAZ <>0 or .w_PIVALSTA<>0 OR .w_PIMASNET<>0 ) ) OR ( .w_TIPOMOV $ 'RS-RX' AND (NOT EMPTY(.w_PIPROGSE) OR .w_PIIMPNAZ<>0) ) ))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPINOMENC_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Nomenclatura non valida o non definita")
        case   not(.w_PIMASNET >= 0) and (.w_TIPPER='M') and (.w_CPROWORD<>0  and ( (.w_TIPOMOV $ 'CS-CE-RC-AC-AS-RA' AND (.w_PIIMPNAZ <>0 or .w_PIVALSTA<>0 OR .w_PIMASNET<>0 ) ) OR ( .w_TIPOMOV $ 'RS-RX' AND (NOT EMPTY(.w_PIPROGSE) OR .w_PIIMPNAZ<>0) ) ))
          .oNewFocus=.oPgFrm.Page1.oPag.oPIMASNET_2_8
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_PIQTASUP>0 OR .w_UNSUP='ZZZ' OR EMPTY(.w_UNSUP) OR .w_PITIPMOV $ 'RC-RA' OR .w_NAZION='SM') and (.w_TIPPER='M') and (.w_CPROWORD<>0  and ( (.w_TIPOMOV $ 'CS-CE-RC-AC-AS-RA' AND (.w_PIIMPNAZ <>0 or .w_PIVALSTA<>0 OR .w_PIMASNET<>0 ) ) OR ( .w_TIPOMOV $ 'RS-RX' AND (NOT EMPTY(.w_PIPROGSE) OR .w_PIIMPNAZ<>0) ) ))
          .oNewFocus=.oPgFrm.Page1.oPag.oPIQTASUP_2_9
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� nella unit� di misura supplementare non definita")
        case   not(.w_PIVALSTA >= 0) and (.w_TIPPER='M' and .w_PITIPMOV $ 'AC-CE-RC-RA') and (.w_CPROWORD<>0  and ( (.w_TIPOMOV $ 'CS-CE-RC-AC-AS-RA' AND (.w_PIIMPNAZ <>0 or .w_PIVALSTA<>0 OR .w_PIMASNET<>0 ) ) OR ( .w_TIPOMOV $ 'RS-RX' AND (NOT EMPTY(.w_PIPROGSE) OR .w_PIIMPNAZ<>0) ) ))
          .oNewFocus=.oPgFrm.Page1.oPag.oPIVALSTA_2_11
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("L'importo non pu� essere negativo")
        case   not(.w_PITIPRET $ '+- ') and (.w_PITIPMOV $ 'RC-RA') and (.w_CPROWORD<>0  and ( (.w_TIPOMOV $ 'CS-CE-RC-AC-AS-RA' AND (.w_PIIMPNAZ <>0 or .w_PIVALSTA<>0 OR .w_PIMASNET<>0 ) ) OR ( .w_TIPOMOV $ 'RS-RX' AND (NOT EMPTY(.w_PIPROGSE) OR .w_PIIMPNAZ<>0) ) ))
          .oNewFocus=.oPgFrm.Page1.oPag.oPITIPRET_2_19
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if .w_CPROWORD<>0  and ( (.w_TIPOMOV $ 'CS-CE-RC-AC-AS-RA' AND (.w_PIIMPNAZ <>0 or .w_PIVALSTA<>0 OR .w_PIMASNET<>0 ) ) OR ( .w_TIPOMOV $ 'RS-RX' AND (NOT EMPTY(.w_PIPROGSE) OR .w_PIIMPNAZ<>0) ) )
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PISERIAL = this.w_PISERIAL
    this.o_PI__ANNO = this.w_PI__ANNO
    this.o_PIDATDOC = this.w_PIDATDOC
    this.o_PITIPMOV = this.w_PITIPMOV
    this.o_PICODCON = this.w_PICODCON
    this.o_TIPPER = this.w_TIPPER
    this.o_PIANNRET = this.w_PIANNRET
    this.o_PITIPPER = this.w_PITIPPER
    this.o_PIPERRET = this.w_PIPERRET
    this.o_PIVALORI = this.w_PIVALORI
    this.o_PIIMPNAZ = this.w_PIIMPNAZ
    this.o_PITIPMOV1 = this.w_PITIPMOV1
    this.o_ARUTISER = this.w_ARUTISER
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0  and ( (t_TIPOMOV $ 'CS-CE-RC-AC-AS-RA' AND (t_PIIMPNAZ <>0 or t_PIVALSTA<>0 OR t_PIMASNET<>0 ) ) OR ( t_TIPOMOV $ 'RS-RX' AND (NOT EMPTY(t_PIPROGSE) OR t_PIIMPNAZ<>0) ) ))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PIIMPNAZ=0
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_PIIMPVAL=0
      .w_PINATTRA=space(3)
      .w_PINOMENC=space(8)
      .w_PIROWORD=0
      .w_UNSUP=space(3)
      .w_PIMASNET=0
      .w_PIQTASUP=0
      .w_PIVALSTA=0
      .w_PICONDCO=space(1)
      .w_PIMODTRA=space(3)
      .w_PINAZORI=space(3)
      .w_PINAZPRO=space(3)
      .w_PINAZDES=space(3)
      .w_PIPRODES=space(2)
      .w_PIPROORI=space(2)
      .w_PITIPRET=space(1)
      .w_PIPORORI=space(10)
      .w_PIPORDES=space(10)
      .w_ISONAZ=space(3)
      .w_ISONA2=space(3)
      .w_ISONA3=space(3)
      .w_PIPAEPAG=space(3)
      .w_PISEZDOG=space(6)
      .w_PIANNREG=space(4)
      .w_PIPRORET=space(6)
      .w_PIPROGSE=space(5)
      .w_PINUMFAT=0
      .w_PIALFFAT=space(10)
      .w_PIDATFAT=ctod("  /  /  ")
      .w_PIPERSER=space(1)
      .w_PIMODINC=space(1)
      .w_PIPAEPAG=space(3)
      .w_TIPOMOV=space(2)
      .w_PIPERSER=space(1)
      .w_PIMODINC=space(1)
      .w_PIANNREG=space(4)
      .w_PIPROGSE=space(5)
      .DoRTCalc(1,39,.f.)
        .w_PIIMPVAL = IIF(.w_PIIMPNAZ=0 And .w_PITIPMOV='RX',0,.w_PIIMPVAL)
      .DoRTCalc(41,41,.f.)
      if not(empty(.w_PINATTRA))
        .link_2_4('Full')
      endif
        .w_PINOMENC = IIF(.w_PIIMPNAZ=0 And .w_PITIPMOV $ 'RS-RX' ,space(8),.w_PINOMENC)
      .DoRTCalc(42,42,.f.)
      if not(empty(.w_PINOMENC))
        .link_2_5('Full')
      endif
      .DoRTCalc(43,48,.f.)
        .w_PICONDCO = IIF(g_ISONAZ<>'ESP',IIF(.w_PITIPMOV $ 'RC-RA-CS-RS-AS-RX' OR .w_TIPPER='T','N',IIF(Not Empty(.w_ANCONCON),.w_ANCONCON,'E')),'1')
      .DoRTCalc(49,49,.f.)
      if not(empty(.w_PICONDCO))
        .link_2_12('Full')
      endif
      .DoRTCalc(50,50,.f.)
      if not(empty(.w_PIMODTRA))
        .link_2_13('Full')
      endif
      .DoRTCalc(51,51,.f.)
      if not(empty(.w_PINAZORI))
        .link_2_14('Full')
      endif
      .DoRTCalc(52,52,.f.)
      if not(empty(.w_PINAZPRO))
        .link_2_15('Full')
      endif
      .DoRTCalc(53,53,.f.)
      if not(empty(.w_PINAZDES))
        .link_2_16('Full')
      endif
      .DoRTCalc(54,55,.f.)
        .w_PITIPRET = IIF(.w_PITIPMOV $ 'RC-RA',.w_PITIPRET,' ')
        .oPgFrm.Page1.oPag.oObj_2_20.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_21.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_28.Calculate()
      .DoRTCalc(57,70,.f.)
      if not(empty(.w_PIPAEPAG))
        .link_2_35('Full')
      endif
      .DoRTCalc(71,77,.f.)
        .w_PIPERSER = 'I'
        .w_PIMODINC = 'X'
        .w_PIPAEPAG = IIF(.w_PIIMPNAZ=0,space(3),.w_PIPAEPAG)
      .DoRTCalc(80,80,.f.)
      if not(empty(.w_PIPAEPAG))
        .link_2_45('Full')
      endif
        .w_TIPOMOV = .w_PITIPMOV
        .w_PIPERSER = 'I'
        .w_PIMODINC = 'X'
        .oPgFrm.Page1.oPag.oObj_2_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_51.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_54.Calculate()
    endwith
    this.DoRTCalc(84,92,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PIIMPNAZ = t_PIIMPNAZ
    this.w_CPROWORD = t_CPROWORD
    this.w_PIIMPVAL = t_PIIMPVAL
    this.w_PINATTRA = t_PINATTRA
    this.w_PINOMENC = t_PINOMENC
    this.w_PIROWORD = t_PIROWORD
    this.w_UNSUP = t_UNSUP
    this.w_PIMASNET = t_PIMASNET
    this.w_PIQTASUP = t_PIQTASUP
    this.w_PIVALSTA = t_PIVALSTA
    this.w_PICONDCO = this.oPgFrm.Page1.oPag.oPICONDCO_2_12.RadioValue(.t.)
    this.w_PIMODTRA = t_PIMODTRA
    this.w_PINAZORI = t_PINAZORI
    this.w_PINAZPRO = t_PINAZPRO
    this.w_PINAZDES = t_PINAZDES
    this.w_PIPRODES = t_PIPRODES
    this.w_PIPROORI = t_PIPROORI
    this.w_PITIPRET = t_PITIPRET
    this.w_PIPORORI = t_PIPORORI
    this.w_PIPORDES = t_PIPORDES
    this.w_ISONAZ = t_ISONAZ
    this.w_ISONA2 = t_ISONA2
    this.w_ISONA3 = t_ISONA3
    this.w_PIPAEPAG = t_PIPAEPAG
    this.w_PISEZDOG = t_PISEZDOG
    this.w_PIANNREG = t_PIANNREG
    this.w_PIPRORET = t_PIPRORET
    this.w_PIPROGSE = t_PIPROGSE
    this.w_PINUMFAT = t_PINUMFAT
    this.w_PIALFFAT = t_PIALFFAT
    this.w_PIDATFAT = t_PIDATFAT
    this.w_PIPERSER = this.oPgFrm.Page1.oPag.oPIPERSER_2_43.RadioValue(.t.)
    this.w_PIMODINC = this.oPgFrm.Page1.oPag.oPIMODINC_2_44.RadioValue(.t.)
    this.w_PIPAEPAG = t_PIPAEPAG
    this.w_TIPOMOV = t_TIPOMOV
    this.w_PIPERSER = this.oPgFrm.Page1.oPag.oPIPERSER_2_48.RadioValue(.t.)
    this.w_PIMODINC = this.oPgFrm.Page1.oPag.oPIMODINC_2_49.RadioValue(.t.)
    this.w_PIANNREG = t_PIANNREG
    this.w_PIPROGSE = t_PIPROGSE
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PIIMPNAZ with this.w_PIIMPNAZ
    replace t_CPROWORD with this.w_CPROWORD
    replace t_PIIMPVAL with this.w_PIIMPVAL
    replace t_PINATTRA with this.w_PINATTRA
    replace t_PINOMENC with this.w_PINOMENC
    replace t_PIROWORD with this.w_PIROWORD
    replace t_UNSUP with this.w_UNSUP
    replace t_PIMASNET with this.w_PIMASNET
    replace t_PIQTASUP with this.w_PIQTASUP
    replace t_PIVALSTA with this.w_PIVALSTA
    replace t_PICONDCO with this.oPgFrm.Page1.oPag.oPICONDCO_2_12.ToRadio()
    replace t_PIMODTRA with this.w_PIMODTRA
    replace t_PINAZORI with this.w_PINAZORI
    replace t_PINAZPRO with this.w_PINAZPRO
    replace t_PINAZDES with this.w_PINAZDES
    replace t_PIPRODES with this.w_PIPRODES
    replace t_PIPROORI with this.w_PIPROORI
    replace t_PITIPRET with this.w_PITIPRET
    replace t_PIPORORI with this.w_PIPORORI
    replace t_PIPORDES with this.w_PIPORDES
    replace t_ISONAZ with this.w_ISONAZ
    replace t_ISONA2 with this.w_ISONA2
    replace t_ISONA3 with this.w_ISONA3
    replace t_PIPAEPAG with this.w_PIPAEPAG
    replace t_PISEZDOG with this.w_PISEZDOG
    replace t_PIANNREG with this.w_PIANNREG
    replace t_PIPRORET with this.w_PIPRORET
    replace t_PIPROGSE with this.w_PIPROGSE
    replace t_PINUMFAT with this.w_PINUMFAT
    replace t_PIALFFAT with this.w_PIALFFAT
    replace t_PIDATFAT with this.w_PIDATFAT
    replace t_PIPERSER with this.oPgFrm.Page1.oPag.oPIPERSER_2_43.ToRadio()
    replace t_PIMODINC with this.oPgFrm.Page1.oPag.oPIMODINC_2_44.ToRadio()
    replace t_PIPAEPAG with this.w_PIPAEPAG
    replace t_TIPOMOV with this.w_TIPOMOV
    replace t_PIPERSER with this.oPgFrm.Page1.oPag.oPIPERSER_2_48.ToRadio()
    replace t_PIMODINC with this.oPgFrm.Page1.oPag.oPIMODINC_2_49.ToRadio()
    replace t_PIANNREG with this.w_PIANNREG
    replace t_PIPROGSE with this.w_PIPROGSE
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mitPag1 as StdContainer
  Width  = 948
  height = 536
  stdWidth  = 948
  stdheight = 536
  resizeXpos=443
  resizeYpos=288
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPINUMREG_1_9 as StdField with uid="ZELZDZQXPV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PINUMREG", cQueryName = "PINUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero movimento INTRA",;
    HelpContextID = 123778877,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=84, Top=7, cSayPict='"999999"', cGetPict='"999999"'

  add object oPI__ANNO_1_10 as StdField with uid="WPFQGEFAOY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PI__ANNO", cQueryName = "PI__ANNO",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di competenza",;
    HelpContextID = 223623355,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=157, Top=7, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  func oPI__ANNO_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_PI__ANNO)>1900)
    endwith
    return bRes
  endfunc

  add object oPIDATREG_1_11 as StdField with uid="UOCZOPHQVV",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PIDATREG", cQueryName = "PIDATREG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data movimento",;
    HelpContextID = 129767229,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=277, Top=7

  add object oPIALFDOC_1_12 as StdField with uid="YUHOROGPBJ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PIALFDOC", cQueryName = "PIALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Alfa documento",;
    HelpContextID = 119085255,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=654, Top=7, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oPINUMDOC_1_13 as StdField with uid="TUNNSJVEWR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PINUMDOC", cQueryName = "PINUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 111102151,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=524, Top=7, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oPIDATDOC_1_14 as StdField with uid="AHAXKCTYXV",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PIDATDOC", cQueryName = "PIDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data non corretta",;
    ToolTipText = "Data documento",;
    HelpContextID = 105113799,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=797, Top=7

  func oPIDATDOC_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PIDATDOC>i_INIDAT and not .w_PITIPMOV $ 'RS-RX') or .w_PITIPMOV $ 'RS-RX' )
    endwith
    return bRes
  endfunc

  add object oPIVALNAZ_1_16 as StdField with uid="SPMUQPFJTA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PIVALNAZ", cQueryName = "PIVALNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta nazionale",;
    HelpContextID = 54343504,;
   bGlobalFont=.t.,;
    Height=21, Width=45, Left=490, Top=178, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_PIVALNAZ"

  func oPIVALNAZ_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oPITIPMOV_1_17 as StdRadio with uid="AKJOZAQDEM",rtseq=16,rtrep=.f.,left=4, top=64, width=166,height=143;
    , ToolTipText = "Tipo movimento";
    , cFormVar="w_PITIPMOV", ButtonCount=8, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oPITIPMOV_1_17.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Cessione beni"
      this.Buttons(1).HelpContextID = 226158772
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Rettifica cessione beni"
      this.Buttons(2).HelpContextID = 226158772
      this.Buttons(2).Top=17
      this.Buttons(3).Caption="Acquisto beni"
      this.Buttons(3).HelpContextID = 226158772
      this.Buttons(3).Top=34
      this.Buttons(4).Caption="Rettifica acquisto beni"
      this.Buttons(4).HelpContextID = 226158772
      this.Buttons(4).Top=51
      this.Buttons(5).Caption="Cessione servizi"
      this.Buttons(5).HelpContextID = 226158772
      this.Buttons(5).Top=68
      this.Buttons(6).Caption="Rettifica cessione servizi"
      this.Buttons(6).HelpContextID = 226158772
      this.Buttons(6).Top=85
      this.Buttons(7).Caption="Acquisto servizi"
      this.Buttons(7).HelpContextID = 226158772
      this.Buttons(7).Top=102
      this.Buttons(8).Caption="Rettifica acquisto servizi"
      this.Buttons(8).HelpContextID = 226158772
      this.Buttons(8).Top=119
      this.SetAll("Width",164)
      this.SetAll("Height",19)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Tipo movimento")
      StdRadio::init()
    endproc

  func oPITIPMOV_1_17.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PITIPMOV,&i_cF..t_PITIPMOV),this.value)
    return(iif(xVal =1,"CE",;
    iif(xVal =2,"RC",;
    iif(xVal =3,"AC",;
    iif(xVal =4,"RA",;
    iif(xVal =5,"CS",;
    iif(xVal =6,"RS",;
    iif(xVal =7,"AS",;
    iif(xVal =8,"RX",;
    space(2))))))))))
  endfunc
  func oPITIPMOV_1_17.GetRadio()
    this.Parent.oContained.w_PITIPMOV = this.RadioValue()
    return .t.
  endfunc

  func oPITIPMOV_1_17.ToRadio()
    this.Parent.oContained.w_PITIPMOV=trim(this.Parent.oContained.w_PITIPMOV)
    return(;
      iif(this.Parent.oContained.w_PITIPMOV=="CE",1,;
      iif(this.Parent.oContained.w_PITIPMOV=="RC",2,;
      iif(this.Parent.oContained.w_PITIPMOV=="AC",3,;
      iif(this.Parent.oContained.w_PITIPMOV=="RA",4,;
      iif(this.Parent.oContained.w_PITIPMOV=="CS",5,;
      iif(this.Parent.oContained.w_PITIPMOV=="RS",6,;
      iif(this.Parent.oContained.w_PITIPMOV=="AS",7,;
      iif(this.Parent.oContained.w_PITIPMOV=="RX",8,;
      0)))))))))
  endfunc

  func oPITIPMOV_1_17.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPITIPMOV_1_17.mCond()
    with this.Parent.oContained
      return (.cFunction='Load' And .w_Modifica)
    endwith
  endfunc

  add object oPICODCON_1_19 as StdField with uid="EEUXIOFPKE",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PICODCON", cQueryName = "PICODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Codice cliente o fornitore",;
    HelpContextID = 137754812,;
   bGlobalFont=.t.,;
    Height=21, Width=138, Left=261, Top=66, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PITIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PICODCON"

  proc oPICODCON_1_19.mAfter
    with this.Parent.oContained
      .w_PICODCON=CALCZER(.w_PICODCON,'CONTI')
    endwith
  endproc

  func oPICODCON_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oPICODCON_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPICODCON_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PITIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PITIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPICODCON_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'GSAR_MIT.CONTI_VZM',this.parent.oContained
  endproc
  proc oPICODCON_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PITIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PICODCON
    i_obj.ecpSave()
  endproc

  add object oDESCLF_1_20 as StdField with uid="ECBGMODMAV",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 79756746,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=404, Top=66, InputMask=replicate('X',40)

  add object oPIVA_1_21 as StdField with uid="CBMRHCHORZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PIVA", cQueryName = "PIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 260229386,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=261, Top=93, cSayPict='repl("X",12)', cGetPict='repl("X",12)', InputMask=replicate('X',12)

  add object oPIANNRET_1_24 as StdField with uid="LLDRAJTZSF",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PIANNRET", cQueryName = "PIANNRET",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno rettifica",;
    HelpContextID = 124315466,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=294, Top=132, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',4)

  func oPIANNRET_1_24.mCond()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "RC-RA" AND UPPER(.cFunction) <> "QUERY")
    endwith
  endfunc

  func oPIANNRET_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_PIANNRET)>1900)
    endwith
    return bRes
  endfunc


  add object oPITIPPER_1_26 as StdCombo with uid="EIBZTRFBLF",rtseq=24,rtrep=.f.,left=357,top=133,width=96,height=21;
    , ToolTipText = "Periodicit� rettifica";
    , HelpContextID = 92608328;
    , cFormVar="w_PITIPPER",RowSource=""+"Mensile,"+"Trimestrale", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oPITIPPER_1_26.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PITIPPER,&i_cF..t_PITIPPER),this.value)
    return(iif(xVal =1,'M',;
    iif(xVal =2,'T',;
    space(1))))
  endfunc
  func oPITIPPER_1_26.GetRadio()
    this.Parent.oContained.w_PITIPPER = this.RadioValue()
    return .t.
  endfunc

  func oPITIPPER_1_26.ToRadio()
    this.Parent.oContained.w_PITIPPER=trim(this.Parent.oContained.w_PITIPPER)
    return(;
      iif(this.Parent.oContained.w_PITIPPER=='M',1,;
      iif(this.Parent.oContained.w_PITIPPER=='T',2,;
      0)))
  endfunc

  func oPITIPPER_1_26.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPITIPPER_1_26.mCond()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "RC-RA" AND .w_PI__ANNO<>.w_PIANNRET)
    endwith
  endfunc

  add object oPIPERRET_1_27 as StdField with uid="NFSLGOWNRK",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PIPERRET", cQueryName = "PIPERRET",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Periodo di riferimento della rettifica",;
    HelpContextID = 127981386,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=466, Top=132, cSayPict='v_ZR+"99"', cGetPict='v_ZR+"99"'

  func oPIPERRET_1_27.mCond()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "RC-RA" AND NOT EMPTY(.w_PIANNRET) AND .w_PITIPPER<>'A' AND UPPER(.cFunction) <> "QUERY")
    endwith
  endfunc

  func oPIPERRET_1_27.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPPER='A')
    endwith
    endif
  endfunc

  func oPIPERRET_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PIPERRET>0 AND ((.w_PITIPPER='M' AND .w_PIPERRET<13) OR (.w_PITIPPER='T' AND .w_PIPERRET<5)))
    endwith
    return bRes
  endfunc

  add object oDESPER_1_28 as StdField with uid="TLQTHIEZYT",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESPER", cQueryName = "DESPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    HelpContextID = 115081782,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=500, Top=132, InputMask=replicate('X',25)

  func oDESPER_1_28.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPPER='A')
    endwith
    endif
  endfunc

  add object oPIDATCOM_1_29 as StdField with uid="NAJFGKAUTW",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PIDATCOM", cQueryName = "PIDATCOM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data non corretta",;
    ToolTipText = "Data di competenza",;
    HelpContextID = 121891005,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=294, Top=178

  func oPIDATCOM_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PIDATCOM>i_INIDAT)
    endwith
    return bRes
  endfunc

  add object oPIVALORI_1_30 as StdField with uid="BYIPNTUPCJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PIVALORI", cQueryName = "PIVALORI",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta originaria incongruente o obsoleta",;
    ToolTipText = "Valuta originaria",;
    HelpContextID = 71120703,;
   bGlobalFont=.t.,;
    Height=21, Width=45, Left=710, Top=178, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_PIVALORI"

  func oPIVALORI_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oPIVALORI_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPIVALORI_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oPIVALORI_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Elenco valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oPIVALORI_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_PIVALORI
    i_obj.ecpSave()
  endproc


  add object oObj_1_45 as cp_runprogram with uid="YKCVTCSEGD",left=292, top=589, width=180,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIN("I")',;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 181571558


  add object oObj_1_46 as cp_runprogram with uid="VYUKBFEESR",left=292, top=565, width=271,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BIT",;
    cEvent = "w_PIPERRET Changed, Frequenza",;
    nPag=1;
    , HelpContextID = 181571558


  add object oObj_1_68 as cp_runprogram with uid="GQVJRYAIVP",left=292, top=613, width=180,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIN("D")',;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 181571558


  add object oBtn_1_69 as StdButton with uid="RVSUCXPEDH",left=883, top=7, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per per accedere al documento di provenienza";
    , HelpContextID = 58575974;
    , TabStop=.f., Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_69.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_PIRIFDOC, -20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_69.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PIRIFDOC))
    endwith
  endfunc

  func oBtn_1_69.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_PIRIFDOC))
    endwith
   endif
  endfunc

  add object oSIMVAL_1_73 as StdField with uid="WUZOPEOGVJ",rtseq=60,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 10594086,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=535, Top=178, InputMask=replicate('X',5)

  add object oSIMVAL1_1_74 as StdField with uid="RXRVGTIHCP",rtseq=61,rtrep=.f.,;
    cFormVar = "w_SIMVAL1", cQueryName = "SIMVAL1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 10594086,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=755, Top=178, InputMask=replicate('X',5)


  add object oObj_1_76 as cp_calclbl with uid="JPVKLBKEBE",left=180, top=69, width=79,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=1;
    , HelpContextID = 181571558


  add object oObj_1_81 as cp_runprogram with uid="OEMDMHVWVO",left=19, top=549, width=221,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BIR('PI')",;
    cEvent = "w_PITIPMOV Changed",;
    nPag=1;
    , HelpContextID = 181571558


  add object oObj_1_98 as cp_runprogram with uid="PXMCEYQFWV",left=19, top=598, width=221,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BIR('CC')",;
    cEvent = "w_PICODCON Changed",;
    nPag=1;
    , HelpContextID = 181571558


  add object oObj_1_103 as cp_runprogram with uid="TLETYERABD",left=19, top=619, width=221,height=19,;
    caption='GSAR_BIN',;
   bGlobalFont=.t.,;
    prg="GSAR_BIN('C')",;
    cEvent = "HasEvent",;
    nPag=1;
    , ToolTipText = "Richiamato dall'area manuale declare (hascpevent)";
    , HelpContextID = 126029644


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=216, width=598,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Riga",Field2="PIIMPNAZ",Label2="Importo in valuta nazionale",Field3="PIIMPVAL",Label3="Importo in valuta originaria",Field4="PINATTRA",Label4="Natura",Field5="PINOMENC",Label5="iif(!w_PITIPMOV $ 'CS-RS-AS-RX', ah_msgformat('Nomenclatura'), ah_msgformat('Codice servizio'))",Field6="PIROWORD",Label6="Riga doc.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 51229306

  add object oStr_1_33 as StdString with uid="TPUXALPBNX",Visible=.t., Left=747, Top=7,;
    Alignment=1, Width=39, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="TKQXDRTCIN",Visible=.t., Left=644, Top=7,;
    Alignment=2, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="HVAOYVDALI",Visible=.t., Left=180, Top=93,;
    Alignment=1, Width=79, Height=15,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="AIWTXAIZVU",Visible=.t., Left=634, Top=339,;
    Alignment=1, Width=120, Height=15,;
    Caption="Modalit� trasporto:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="CXJBVKMDON",Visible=.t., Left=693, Top=314,;
    Alignment=1, Width=61, Height=15,;
    Caption="Consegna:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="SMFDAEFQPH",Visible=.t., Left=675, Top=389,;
    Alignment=1, Width=79, Height=15,;
    Caption="Paese prov.:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (NOT( .w_PITIPMOV $ 'AC-RA'  AND .w_AITIPV='M' ) or .w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="DOONNDZQBZ",Visible=.t., Left=666, Top=364,;
    Alignment=1, Width=88, Height=15,;
    Caption="Paese orig.:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (NOT( .w_PITIPMOV $ 'CE-RC' AND .w_AITIPE='M' ) or .w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="EYRBQEKXNK",Visible=.t., Left=681, Top=389,;
    Alignment=1, Width=74, Height=15,;
    Caption="Paese dest.:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (NOT( .w_PITIPMOV $ 'CE-RC' AND .w_AITIPE='M' ) or .w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="ONWBNLPEKH",Visible=.t., Left=689, Top=423,;
    Alignment=1, Width=66, Height=15,;
    Caption="Prov orig.:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (NOT( .w_PITIPMOV $ 'CE-RC' AND .w_AITIPE='M' ) or .w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="LBOVEONRBQ",Visible=.t., Left=695, Top=423,;
    Alignment=1, Width=63, Height=15,;
    Caption="Prov dest.:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (NOT( .w_PITIPMOV $ 'AC-RA'  AND .w_AITIPV='M' ) or .w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="CQXRDXBNOV",Visible=.t., Left=700, Top=448,;
    Alignment=1, Width=54, Height=15,;
    Caption="Segno:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="RDVRPXHRDT",Visible=.t., Left=629, Top=217,;
    Alignment=2, Width=268, Height=15,;
    Caption="Altri dati elenchi mensili"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX" )
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="HIDPHOBWXP",Visible=.t., Left=10, Top=7,;
    Alignment=1, Width=71, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="NNKMSSSTTK",Visible=.t., Left=236, Top=7,;
    Alignment=1, Width=39, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="BZQYRFPRFI",Visible=.t., Left=422, Top=7,;
    Alignment=1, Width=99, Height=15,;
    Caption="Documento n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="DUTTJOHDNU",Visible=.t., Left=147, Top=7,;
    Alignment=2, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="TVLVIOGZMO",Visible=.t., Left=199, Top=178,;
    Alignment=1, Width=94, Height=15,;
    Caption="Competenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="OFXKLTPMSD",Visible=.t., Left=645, Top=264,;
    Alignment=1, Width=109, Height=15,;
    Caption="UM supplementare:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="QMEKEVJCUE",Visible=.t., Left=664, Top=289,;
    Alignment=1, Width=90, Height=15,;
    Caption="Val. statistico:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="TLYNEGWTCT",Visible=.t., Left=400, Top=178,;
    Alignment=1, Width=90, Height=15,;
    Caption="Valuta naz.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="FHPPCPRVHY",Visible=.t., Left=622, Top=178,;
    Alignment=1, Width=88, Height=15,;
    Caption="Valuta orig.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="NBIFJNJKYP",Visible=.t., Left=4, Top=44,;
    Alignment=0, Width=90, Height=16,;
    Caption="Tipo operazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="YBGGFKJPFK",Visible=.t., Left=180, Top=135,;
    Alignment=1, Width=113, Height=18,;
    Caption="Periodo rettifica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="GLHEJVWYJM",Visible=.t., Left=682, Top=239,;
    Alignment=1, Width=72, Height=15,;
    Caption="Massa netta:"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
  endfunc

  add object oStr_1_77 as StdString with uid="TDEREKANJN",Visible=.t., Left=828, Top=364,;
    Alignment=1, Width=58, Height=18,;
    Caption="Cod. ISO:"  ;
  , bGlobalFont=.t.

  func oStr_1_77.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
  endfunc

  add object oStr_1_78 as StdString with uid="HPLEAOJIRO",Visible=.t., Left=828, Top=389,;
    Alignment=1, Width=58, Height=18,;
    Caption="Cod. ISO:"  ;
  , bGlobalFont=.t.

  func oStr_1_78.mHide()
    with this.Parent.oContained
      return (NOT( .w_PITIPMOV $ 'AC-RA'  AND .w_AITIPV='M' ) AND NOT( .w_PITIPMOV $ 'CE-RC' AND .w_AITIPE='M' ))
    endwith
  endfunc

  add object oStr_1_83 as StdString with uid="FFCRVVFBDT",Visible=.t., Left=639, Top=288,;
    Alignment=1, Width=132, Height=15,;
    Caption="Modalit� di erogazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_83.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-RS-RX")
    endwith
  endfunc

  add object oStr_1_84 as StdString with uid="XXTXHENNUJ",Visible=.t., Left=639, Top=319,;
    Alignment=1, Width=132, Height=15,;
    Caption="Modalit� di incasso:"  ;
  , bGlobalFont=.t.

  func oStr_1_84.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-RS-RX")
    endwith
  endfunc

  add object oStr_1_85 as StdString with uid="MVALBQVCQF",Visible=.t., Left=699, Top=350,;
    Alignment=1, Width=72, Height=15,;
    Caption="Paese pag.:"  ;
  , bGlobalFont=.t.

  func oStr_1_85.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-RS-RX")
    endwith
  endfunc

  add object oStr_1_86 as StdString with uid="UFZSLQCINP",Visible=.t., Left=647, Top=323,;
    Alignment=1, Width=160, Height=15,;
    Caption="Progr. sezione da rettificare:"  ;
  , bGlobalFont=.t.

  func oStr_1_86.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
  endfunc

  add object oStr_1_87 as StdString with uid="XUHOEAPRLR",Visible=.t., Left=658, Top=238,;
    Alignment=1, Width=149, Height=15,;
    Caption="Sezione doganale:"  ;
  , bGlobalFont=.t.

  func oStr_1_87.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
  endfunc

  add object oStr_1_88 as StdString with uid="FYLKBLPCCX",Visible=.t., Left=726, Top=268,;
    Alignment=1, Width=81, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  func oStr_1_88.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
  endfunc

  add object oStr_1_89 as StdString with uid="PUANHYOZSC",Visible=.t., Left=647, Top=296,;
    Alignment=1, Width=160, Height=15,;
    Caption="Protocollo dichiarazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_89.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
  endfunc

  add object oStr_1_90 as StdString with uid="LCCCRUSJYH",Visible=.t., Left=727, Top=359,;
    Alignment=1, Width=82, Height=15,;
    Caption="Fattura n.:"  ;
  , bGlobalFont=.t.

  func oStr_1_90.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
  endfunc

  add object oStr_1_91 as StdString with uid="RHCEIRVBMW",Visible=.t., Left=786, Top=379,;
    Alignment=1, Width=23, Height=18,;
    Caption="Alfa:"  ;
  , bGlobalFont=.t.

  func oStr_1_91.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
  endfunc

  add object oStr_1_92 as StdString with uid="NOUCGLAOVV",Visible=.t., Left=779, Top=404,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
  endfunc

  add object oStr_1_93 as StdString with uid="SLWXYQVVAT",Visible=.t., Left=677, Top=430,;
    Alignment=1, Width=132, Height=15,;
    Caption="Modalit� di erogazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_93.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
  endfunc

  add object oStr_1_94 as StdString with uid="XJTENFBYQN",Visible=.t., Left=677, Top=464,;
    Alignment=1, Width=132, Height=15,;
    Caption="Modalit� di incasso:"  ;
  , bGlobalFont=.t.

  func oStr_1_94.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
  endfunc

  add object oStr_1_95 as StdString with uid="YLUXREQZLI",Visible=.t., Left=737, Top=500,;
    Alignment=1, Width=72, Height=15,;
    Caption="Paese pag.:"  ;
  , bGlobalFont=.t.

  func oStr_1_95.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
  endfunc

  add object oStr_1_99 as StdString with uid="JIBMHSCPAI",Visible=.t., Left=690, Top=385,;
    Alignment=1, Width=81, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  func oStr_1_99.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-RX-RS")
    endwith
  endfunc

  add object oStr_1_100 as StdString with uid="HGIPNDSCOS",Visible=.t., Left=611, Top=415,;
    Alignment=1, Width=160, Height=18,;
    Caption="Progressivo sezione:"  ;
  , bGlobalFont=.t.

  func oStr_1_100.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-RX-RS")
    endwith
  endfunc

  add object oStr_1_101 as StdString with uid="DZJBWQWQTV",Visible=.t., Left=651, Top=217,;
    Alignment=2, Width=229, Height=15,;
    Caption="Altri dati elenchi"  ;
  , bGlobalFont=.t.

  func oStr_1_101.mHide()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "AC-RA-RC-CE")
    endwith
  endfunc

  add object oBox_1_7 as StdBox with uid="XLLATFXVGM",left=175, top=59, width=760,height=61

  add object oBox_1_25 as StdBox with uid="EXIMKUUTNN",left=175, top=127, width=760,height=33

  add object oBox_1_56 as StdBox with uid="RYLXIOIPIN",left=2, top=59, width=170,height=150

  add object oBox_1_59 as StdBox with uid="MTDWIKFASY",left=175, top=170, width=759,height=38

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=237,;
    width=594+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=238,width=593+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TIPITRAN|NOMENCLA|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPIMASNET_2_8.Refresh()
      this.Parent.oPIQTASUP_2_9.Refresh()
      this.Parent.oPIVALSTA_2_11.Refresh()
      this.Parent.oPICONDCO_2_12.Refresh()
      this.Parent.oPIMODTRA_2_13.Refresh()
      this.Parent.oPINAZORI_2_14.Refresh()
      this.Parent.oPINAZPRO_2_15.Refresh()
      this.Parent.oPINAZDES_2_16.Refresh()
      this.Parent.oPIPRODES_2_17.Refresh()
      this.Parent.oPIPROORI_2_18.Refresh()
      this.Parent.oPITIPRET_2_19.Refresh()
      this.Parent.oISONAZ_2_32.Refresh()
      this.Parent.oISONA2_2_33.Refresh()
      this.Parent.oISONA3_2_34.Refresh()
      this.Parent.oPIPAEPAG_2_35.Refresh()
      this.Parent.oPISEZDOG_2_36.Refresh()
      this.Parent.oPIANNREG_2_37.Refresh()
      this.Parent.oPIPRORET_2_38.Refresh()
      this.Parent.oPIPROGSE_2_39.Refresh()
      this.Parent.oPINUMFAT_2_40.Refresh()
      this.Parent.oPIALFFAT_2_41.Refresh()
      this.Parent.oPIDATFAT_2_42.Refresh()
      this.Parent.oPIPERSER_2_43.Refresh()
      this.Parent.oPIMODINC_2_44.Refresh()
      this.Parent.oPIPAEPAG_2_45.Refresh()
      this.Parent.oPIPERSER_2_48.Refresh()
      this.Parent.oPIMODINC_2_49.Refresh()
      this.Parent.oPIANNREG_2_55.Refresh()
      this.Parent.oPIPROGSE_2_56.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TIPITRAN'
        oDropInto=this.oBodyCol.oRow.oPINATTRA_2_4
      case cFile='NOMENCLA'
        oDropInto=this.oBodyCol.oRow.oPINOMENC_2_5
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oPIMASNET_2_8 as StdTrsField with uid="KIBXYCJUAC",rtseq=45,rtrep=.t.,;
    cFormVar="w_PIMASNET",value=0,;
    ToolTipText = "Massa netta",;
    HelpContextID = 61646666,;
    cTotal="", bFixedPos=.t., cQueryName = "PIMASNET",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=761, Top=239, cSayPict=[v_PQ(14)], cGetPict=[v_GQ(14)]

  func oPIMASNET_2_8.mCond()
    with this.Parent.oContained
      return (.w_TIPPER='M')
    endwith
  endfunc

  func oPIMASNET_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
    endif
  endfunc

  func oPIMASNET_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PIMASNET >= 0)
    endwith
    return bRes
  endfunc

  add object oPIQTASUP_2_9 as StdTrsField with uid="QJUJEGVEQG",rtseq=46,rtrep=.t.,;
    cFormVar="w_PIQTASUP",value=0,;
    ToolTipText = "Quantit� espressa nell'unit� di misura supplementare",;
    HelpContextID = 127919942,;
    cTotal="", bFixedPos=.t., cQueryName = "PIQTASUP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� nella unit� di misura supplementare non definita",;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=761, Top=264, cSayPict=[v_PQ(14)], cGetPict=[v_GQ(14)]

  func oPIQTASUP_2_9.mCond()
    with this.Parent.oContained
      return (.w_TIPPER='M')
    endwith
  endfunc

  func oPIQTASUP_2_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
    endif
  endfunc

  func oPIQTASUP_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PIQTASUP>0 OR .w_UNSUP='ZZZ' OR EMPTY(.w_UNSUP) OR .w_PITIPMOV $ 'RC-RA' OR .w_NAZION='SM')
    endwith
    return bRes
  endfunc

  add object oPIVALSTA_2_11 as StdTrsField with uid="DRMQDKIWFW",rtseq=48,rtrep=.t.,;
    cFormVar="w_PIVALSTA",value=0,;
    ToolTipText = "Valore statistico delle merci espresso in valuta nazionale",;
    HelpContextID = 138229559,;
    cTotal="", bFixedPos=.t., cQueryName = "PIVALSTA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "L'importo non pu� essere negativo",;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=761, Top=289, cSayPict=[v_PV(20)], cGetPict=[v_GV(20)]

  func oPIVALSTA_2_11.mCond()
    with this.Parent.oContained
      return (.w_TIPPER='M' and .w_PITIPMOV $ 'AC-CE-RC-RA')
    endwith
  endfunc

  func oPIVALSTA_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
    endif
  endfunc

  func oPIVALSTA_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PIVALSTA >= 0)
    endwith
    return bRes
  endfunc

  add object oPICONDCO_2_12 as StdZTamTableCombo with uid="PBKPTFKGIG",rtrep=.t.,;
    cFormVar="w_PICONDCO", tablefilter="" , ;
    ToolTipText = "Condizioni di consegna",;
    HelpContextID = 157943621,;
    Height=25, Width=149, Left=761, Top=313,;
    cTotal="", cQueryName = "PICONDCO",;
    bObbl = .f. , nPag=2, cLinkFile="CON_CONS", bIsInHeader=.f.;
    , cTable='QUERY\GSARCACL.VQR',cKey='COCODICE',cValue='CODESCRI',cOrderBy='CODESCRI',xDefault=space(1);
  , bGlobalFont=.t.



  func oPICONDCO_2_12.mCond()
    with this.Parent.oContained
      return (.w_TIPPER='M' and .w_PITIPMOV $ 'AC-CE-RC-RA')
    endwith
  endfunc

  func oPICONDCO_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
    endif
  endfunc

  func oPICONDCO_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oPICONDCO_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  add object oPIMODTRA_2_13 as StdTrsField with uid="IVOMJCSMPA",rtseq=50,rtrep=.t.,;
    cFormVar="w_PIMODTRA",value=space(3),;
    ToolTipText = "Codice della modalit� di trasporto",;
    HelpContextID = 147498807,;
    cTotal="", bFixedPos=.t., cQueryName = "PIMODTRA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=761, Top=339, cSayPict=["XXX"], cGetPict=["XXX"], InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODASPED", cZoomOnZoom="GSAR_ASP", oKey_1_1="SPCODSPE", oKey_1_2="this.w_PIMODTRA"

  func oPIMODTRA_2_13.mCond()
    with this.Parent.oContained
      return (.w_TIPPER='M')
    endwith
  endfunc

  func oPIMODTRA_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
    endif
  endfunc

  func oPIMODTRA_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPIMODTRA_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPIMODTRA_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODASPED','*','SPCODSPE',cp_AbsName(this.parent,'oPIMODTRA_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASP',"Spedizioni",'',this.parent.oContained
  endproc
  proc oPIMODTRA_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SPCODSPE=this.parent.oContained.w_PIMODTRA
    i_obj.ecpSave()
  endproc

  add object oPINAZORI_2_14 as StdTrsField with uid="HZWWLUBYTE",rtseq=51,rtrep=.t.,;
    cFormVar="w_PINAZORI",value=space(3),;
    ToolTipText = "Codice del paese di origine del fornitore della merce",;
    HelpContextID = 85767999,;
    cTotal="", bFixedPos=.t., cQueryName = "PINAZORI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=761, Top=364, cSayPict=["!!!"], cGetPict=["!!!"], InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_PINAZORI"

  func oPINAZORI_2_14.mCond()
    with this.Parent.oContained
      return (.w_PITIPMOV $ 'AC-RA' AND .w_AITIPV='M')
    endwith
  endfunc

  func oPINAZORI_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
    endif
  endfunc

  func oPINAZORI_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oPINAZORI_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPINAZORI_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oPINAZORI_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oPINAZORI_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_PINAZORI
    i_obj.ecpSave()
  endproc

  add object oPINAZPRO_2_15 as StdTrsField with uid="JYXSMHCXIG",rtseq=52,rtrep=.t.,;
    cFormVar="w_PINAZPRO",value=space(3),;
    ToolTipText = "Codice del paese dal quale provengono le merci",;
    HelpContextID = 102545221,;
    cTotal="", bFixedPos=.t., cQueryName = "PINAZPRO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=761, Top=389, cSayPict=["!!!"], cGetPict=["!!!"], InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_PINAZPRO"

  func oPINAZPRO_2_15.mCond()
    with this.Parent.oContained
      return (.w_PITIPMOV $ 'AC-RA'  AND .w_AITIPV='M')
    endwith
  endfunc

  func oPINAZPRO_2_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT( .w_PITIPMOV $ 'AC-RA'  AND .w_AITIPV='M' ) or .w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
    endif
  endfunc

  func oPINAZPRO_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oPINAZPRO_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPINAZPRO_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oPINAZPRO_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oPINAZPRO_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_PINAZPRO
    i_obj.ecpSave()
  endproc

  add object oPINAZDES_2_16 as StdTrsField with uid="MBXRAIJUJI",rtseq=53,rtrep=.t.,;
    cFormVar="w_PINAZDES",value=space(3),;
    ToolTipText = "Codice del paese di destinazione delle merci",;
    HelpContextID = 169654089,;
    cTotal="", bFixedPos=.t., cQueryName = "PINAZDES",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=761, Top=389, cSayPict=["!!!"], cGetPict=["!!!"], InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_PINAZDES"

  func oPINAZDES_2_16.mCond()
    with this.Parent.oContained
      return (.w_PITIPMOV = 'CE' AND .w_AITIPE='M')
    endwith
  endfunc

  func oPINAZDES_2_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT( .w_PITIPMOV $ 'CE-RC' AND .w_AITIPE='M' ) or .w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
    endif
  endfunc

  func oPINAZDES_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPINAZDES_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPINAZDES_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oPINAZDES_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oPINAZDES_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_PINAZDES
    i_obj.ecpSave()
  endproc

  add object oPIPRODES_2_17 as StdTrsField with uid="WCSKZBCHOO",rtseq=54,rtrep=.t.,;
    cFormVar="w_PIPRODES",value=space(2),;
    ToolTipText = "Sigla automobilistica della provincia di destinazione della merce",;
    HelpContextID = 159242057,;
    cTotal="", bFixedPos=.t., cQueryName = "PIPRODES",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=761, Top=423, cSayPict=["!!"], cGetPict=["!!"], InputMask=replicate('X',2)

  func oPIPRODES_2_17.mCond()
    with this.Parent.oContained
      return (.w_PITIPMOV $ 'AC-RA'  AND .w_AITIPV='M')
    endwith
  endfunc

  func oPIPRODES_2_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT( .w_PITIPMOV $ 'AC-RA'  AND g_PEINAC='M' ) or .w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
    endif
  endfunc

  add object oPIPROORI_2_18 as StdTrsField with uid="ZMCDKFVIKT",rtseq=55,rtrep=.t.,;
    cFormVar="w_PIPROORI",value=space(2),;
    ToolTipText = "Sigla automobilistica della provincia di origine/produzione della merce",;
    HelpContextID = 75355967,;
    cTotal="", bFixedPos=.t., cQueryName = "PIPROORI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=761, Top=423, cSayPict=["!!"], cGetPict=["!!"], InputMask=replicate('X',2)

  func oPIPROORI_2_18.mCond()
    with this.Parent.oContained
      return (.w_PITIPMOV $ 'CE-RC' AND .w_AITIPE='M')
    endwith
  endfunc

  func oPIPROORI_2_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT( .w_PITIPMOV $ 'CE-RC' AND .w_AITIPE='M' ) or .w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
    endif
  endfunc

  add object oPITIPRET_2_19 as StdTrsField with uid="HYOJVWGPPV",rtseq=56,rtrep=.t.,;
    cFormVar="w_PITIPRET",value=space(1),;
    ToolTipText = "Segno della rettifica (+ per aumento e - per diminuzione)",;
    HelpContextID = 126162762,;
    cTotal="", bFixedPos=.t., cQueryName = "PITIPRET",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=761, Top=448, InputMask=replicate('X',1)

  func oPITIPRET_2_19.mCond()
    with this.Parent.oContained
      return (.w_PITIPMOV $ 'RC-RA')
    endwith
  endfunc

  func oPITIPRET_2_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
    endif
  endfunc

  func oPITIPRET_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PITIPRET $ '+- ')
    endwith
    return bRes
  endfunc

  add object oObj_2_20 as cp_runprogram with uid="TPWKAKZSXD",width=160,height=23,;
   left=579, top=563,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("NT")',;
    cEvent = "w_PINATTRA Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oObj_2_21 as cp_runprogram with uid="OOALSEARGQ",width=160,height=23,;
   left=579, top=585,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("MT")',;
    cEvent = "w_PIMODTRA Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oObj_2_22 as cp_runprogram with uid="SWUHEKZWLK",width=160,height=23,;
   left=579, top=607,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("MC")',;
    cEvent = "w_PICONDCO Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oObj_2_23 as cp_runprogram with uid="IWFXCSZKEG",width=160,height=23,;
   left=579, top=629,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("PO")',;
    cEvent = "w_PINAZORI Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oObj_2_24 as cp_runprogram with uid="YVVAAMCOSR",width=160,height=23,;
   left=579, top=651,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("PP")',;
    cEvent = "w_PINAZPRO Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oObj_2_25 as cp_runprogram with uid="HRXURXEACV",width=160,height=23,;
   left=579, top=673,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("VD")',;
    cEvent = "w_PIPRODES Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oObj_2_26 as cp_runprogram with uid="WDYQZAKGVT",width=160,height=23,;
   left=579, top=695,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("ND")',;
    cEvent = "w_PINAZDES Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oObj_2_27 as cp_runprogram with uid="MBNLDCVTZX",width=160,height=23,;
   left=579, top=717,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("VO")',;
    cEvent = "w_PIPROORI Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oObj_2_28 as cp_runprogram with uid="TSXNWTOVUK",width=160,height=23,;
   left=578, top=740,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BIR('SE')",;
    cEvent = "w_PITIPRET Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oBtn_2_29 as StdButton with uid="NVRNZCRUXR",width=48,height=45,;
   left=883, top=488,;
    CpPicture="BMP\CONCLUSI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai dati di riga";
    , HelpContextID = 257479114;
    , TabStop=.f.,Caption='\<Dati di Riga';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_29.Click()
      do GSAR_KDA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_29.mCond()
    with this.Parent.oContained
      return ((.w_PIIMPNAZ<>0 OR .w_PIMASNET<>0 OR .w_PIVALSTA<>0))
    endwith
  endfunc

  func oBtn_2_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
   endif
  endfunc

  add object oISONAZ_2_32 as StdTrsField with uid="CFZCDKDLXB",rtseq=65,rtrep=.t.,;
    cFormVar="w_ISONAZ",value=space(3),enabled=.f.,;
    HelpContextID = 244961414,;
    cTotal="", bFixedPos=.t., cQueryName = "ISONAZ",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=891, Top=364, InputMask=replicate('X',3)

  func oISONAZ_2_32.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CS-RS-AS-RX")
    endwith
    endif
  endfunc

  add object oISONA2_2_33 as StdTrsField with uid="BWBCUHMXXF",rtseq=66,rtrep=.t.,;
    cFormVar="w_ISONA2",value=space(3),enabled=.f.,;
    HelpContextID = 157691770,;
    cTotal="", bFixedPos=.t., cQueryName = "ISONA2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=891, Top=389, InputMask=replicate('X',3)

  func oISONA2_2_33.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT( .w_PITIPMOV $ 'CE-RC' AND .w_AITIPE='M' ))
    endwith
    endif
  endfunc

  add object oISONA3_2_34 as StdTrsField with uid="LQIHPRHIZI",rtseq=67,rtrep=.t.,;
    cFormVar="w_ISONA3",value=space(3),enabled=.f.,;
    HelpContextID = 140914554,;
    cTotal="", bFixedPos=.t., cQueryName = "ISONA3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=891, Top=389, InputMask=replicate('X',3)

  func oISONA3_2_34.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT( .w_PITIPMOV $ 'AC-RA'  AND .w_AITIPV='M' ))
    endwith
    endif
  endfunc

  add object oPIPAEPAG_2_35 as StdTrsField with uid="APYXASZSPL",rtseq=70,rtrep=.t.,;
    cFormVar="w_PIPAEPAG",value=space(3),;
    ToolTipText = "Codice del paese di pagamento",;
    HelpContextID = 80533309,;
    cTotal="", bFixedPos=.t., cQueryName = "PIPAEPAG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=775, Top=349, cSayPict=["!!!"], cGetPict=["!!!"], InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_PIPAEPAG"

  func oPIPAEPAG_2_35.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-RX-RS")
    endwith
    endif
  endfunc

  func oPIPAEPAG_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oPIPAEPAG_2_35.ecpDrop(oSource)
    this.Parent.oContained.link_2_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPIPAEPAG_2_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oPIPAEPAG_2_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"",'',this.parent.oContained
  endproc
  proc oPIPAEPAG_2_35.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_PIPAEPAG
    i_obj.ecpSave()
  endproc

  add object oPISEZDOG_2_36 as StdTrsField with uid="YZSIJJDIKL",rtseq=71,rtrep=.t.,;
    cFormVar="w_PISEZDOG",value=space(6),nZero=6,;
    ToolTipText = "Sezione doganale in cui � stata registrata la dichiarazione da rettificare",;
    HelpContextID = 98498755,;
    cTotal="", bFixedPos=.t., cQueryName = "PISEZDOG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=811, Top=239, cSayPict=[repl("9",6)], cGetPict=[repl("9",6)], InputMask=replicate('X',6)

  func oPISEZDOG_2_36.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
    endif
  endfunc

  add object oPIANNREG_2_37 as StdTrsField with uid="KDBSQSLBMP",rtseq=72,rtrep=.t.,;
    cFormVar="w_PIANNREG",value=space(4),nZero=4,;
    ToolTipText = "Anno di registrazione della dichiarazione da rettificare",;
    HelpContextID = 124315453,;
    cTotal="", bFixedPos=.t., cQueryName = "PIANNREG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=812, Top=266, cSayPict=['9999'], cGetPict=['9999'], InputMask=replicate('X',4)

  func oPIANNREG_2_37.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
    endif
  endfunc

  proc oPIANNREG_2_37.mAfter
    with this.Parent.oContained
      .w_PIANNREG = yy2yyyy(.w_PIANNREG)
    endwith
  endproc

  add object oPIPRORET_2_38 as StdTrsField with uid="WFXXXHFSRY",rtseq=73,rtrep=.t.,;
    cFormVar="w_PIPRORET",value=space(6),nZero=6,;
    ToolTipText = "Protocollo della dichiarazione da rettificare",;
    HelpContextID = 125687626,;
    cTotal="", bFixedPos=.t., cQueryName = "PIPRORET",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=812, Top=293, cSayPict=[repl("9",6)], cGetPict=[repl("9",6)], InputMask=replicate('X',6)

  func oPIPRORET_2_38.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
    endif
  endfunc

  add object oPIPROGSE_2_39 as StdTrsField with uid="QELKRUKHED",rtseq=74,rtrep=.t.,;
    cFormVar="w_PIPROGSE",value=space(5),nZero=5,;
    ToolTipText = "Progressivo sezione da rettificare",;
    HelpContextID = 209573691,;
    cTotal="", bFixedPos=.t., cQueryName = "PIPROGSE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=812, Top=320, cSayPict=[repl("9",5)], cGetPict=[repl("9",5)], InputMask=replicate('X',5)

  func oPIPROGSE_2_39.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
    endif
  endfunc

  add object oPINUMFAT_2_40 as StdTrsField with uid="SGXYTXLZRF",rtseq=75,rtrep=.t.,;
    cFormVar="w_PINUMFAT",value=0,;
    ToolTipText = "Numero fattura da rettificare",;
    HelpContextID = 190887754,;
    cTotal="", bFixedPos=.t., cQueryName = "PINUMFAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=813, Top=355, cSayPict=["999999999999999"], cGetPict=["999999999999999"], nMaxValue = 999999999999999

  func oPINUMFAT_2_40.mCond()
    with this.Parent.oContained
      return (.w_PIIMPNAZ<>0)
    endwith
  endfunc

  func oPINUMFAT_2_40.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
    endif
  endfunc

  add object oPIALFFAT_2_41 as StdTrsField with uid="ISICTVWWNY",rtseq=76,rtrep=.t.,;
    cFormVar="w_PIALFFAT",value=space(10),;
    ToolTipText = "Serie fattura da rettificare",;
    HelpContextID = 182904650,;
    cTotal="", bFixedPos=.t., cQueryName = "PIALFFAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=813, Top=378, InputMask=replicate('X',10)

  func oPIALFFAT_2_41.mCond()
    with this.Parent.oContained
      return (.w_PIIMPNAZ<>0)
    endwith
  endfunc

  func oPIALFFAT_2_41.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
    endif
  endfunc

  add object oPIDATFAT_2_42 as StdTrsField with uid="KQKMALOGZY",rtseq=77,rtrep=.t.,;
    cFormVar="w_PIDATFAT",value=ctod("  /  /  "),;
    ToolTipText = "Data fattura da rettificare",;
    HelpContextID = 196876106,;
    cTotal="", bFixedPos=.t., cQueryName = "PIDATFAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=813, Top=401

  func oPIDATFAT_2_42.mCond()
    with this.Parent.oContained
      return (.w_PIIMPNAZ<>0)
    endwith
  endfunc

  func oPIDATFAT_2_42.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
    endif
  endfunc

  add object oPIPERSER_2_43 as StdTrsCombo with uid="FQQKBLLQLS",rtrep=.t.,;
    cFormVar="w_PIPERSER", RowSource=""+"Istantanea,"+"A pi� riprese" , ;
    ToolTipText = "Modalit� di erogazione",;
    HelpContextID = 144758600,;
    Height=25, Width=118, Left=813, Top=426,;
    cTotal="", cQueryName = "PIPERSER",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPIPERSER_2_43.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PIPERSER,&i_cF..t_PIPERSER),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'R',;
    space(1))))
  endfunc
  func oPIPERSER_2_43.GetRadio()
    this.Parent.oContained.w_PIPERSER = this.RadioValue()
    return .t.
  endfunc

  func oPIPERSER_2_43.ToRadio()
    this.Parent.oContained.w_PIPERSER=trim(this.Parent.oContained.w_PIPERSER)
    return(;
      iif(this.Parent.oContained.w_PIPERSER=='I',1,;
      iif(this.Parent.oContained.w_PIPERSER=='R',2,;
      0)))
  endfunc

  func oPIPERSER_2_43.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPIPERSER_2_43.mCond()
    with this.Parent.oContained
      return (.w_PIIMPNAZ<>0)
    endwith
  endfunc

  func oPIPERSER_2_43.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
    endif
  endfunc

  add object oPIMODINC_2_44 as StdTrsCombo with uid="NCJJAELCAI",rtrep=.t.,;
    cFormVar="w_PIMODINC", RowSource=""+"Accredito,"+"Bonifico,"+"Altro" , ;
    HelpContextID = 37050567,;
    Height=25, Width=118, Left=813, Top=461,;
    cTotal="", cQueryName = "PIMODINC",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPIMODINC_2_44.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PIMODINC,&i_cF..t_PIMODINC),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'B',;
    iif(xVal =3,'X',;
    space(1)))))
  endfunc
  func oPIMODINC_2_44.GetRadio()
    this.Parent.oContained.w_PIMODINC = this.RadioValue()
    return .t.
  endfunc

  func oPIMODINC_2_44.ToRadio()
    this.Parent.oContained.w_PIMODINC=trim(this.Parent.oContained.w_PIMODINC)
    return(;
      iif(this.Parent.oContained.w_PIMODINC=='A',1,;
      iif(this.Parent.oContained.w_PIMODINC=='B',2,;
      iif(this.Parent.oContained.w_PIMODINC=='X',3,;
      0))))
  endfunc

  func oPIMODINC_2_44.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPIMODINC_2_44.mCond()
    with this.Parent.oContained
      return (.w_PIIMPNAZ<>0)
    endwith
  endfunc

  func oPIMODINC_2_44.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
    endif
  endfunc

  add object oPIPAEPAG_2_45 as StdTrsField with uid="SXQBTQJLFA",rtseq=80,rtrep=.t.,;
    cFormVar="w_PIPAEPAG",value=space(3),;
    ToolTipText = "Codice del paese di pagamento",;
    HelpContextID = 80533309,;
    cTotal="", bFixedPos=.t., cQueryName = "PIPAEPAG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=813, Top=497, cSayPict=["!!!"], cGetPict=["!!!"], InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_PIPAEPAG"

  func oPIPAEPAG_2_45.mCond()
    with this.Parent.oContained
      return (.w_PIIMPNAZ<>0)
    endwith
  endfunc

  func oPIPAEPAG_2_45.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
    endif
  endfunc

  func oPIPAEPAG_2_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oPIPAEPAG_2_45.ecpDrop(oSource)
    this.Parent.oContained.link_2_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPIPAEPAG_2_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oPIPAEPAG_2_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"",'',this.parent.oContained
  endproc
  proc oPIPAEPAG_2_45.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_PIPAEPAG
    i_obj.ecpSave()
  endproc

  add object oBtn_2_46 as StdButton with uid="GJQKCBDMMT",width=48,height=45,;
   left=883, top=239,;
    CpPicture="BMP\CONCLUSI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai servizi da rettificare";
    , HelpContextID = 257479114;
    , TabStop=.f., Caption='\<Rettifiche';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_46.Click()
      do GSAR_KTI with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_46.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_PICODCON) And Not Empty(.w_PIDATCOM))
    endwith
  endfunc

  func oBtn_2_46.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS")
    endwith
   endif
  endfunc

  add object oPIPERSER_2_48 as StdTrsCombo with uid="XDAHBDMASF",rtrep=.t.,;
    cFormVar="w_PIPERSER", RowSource=""+"Istantanea,"+"A pi� riprese" , ;
    ToolTipText = "Modalit� di erogazione",;
    HelpContextID = 144758600,;
    Height=25, Width=118, Left=775, Top=284,;
    cTotal="", cQueryName = "PIPERSER",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPIPERSER_2_48.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PIPERSER,&i_cF..t_PIPERSER),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'R',;
    space(1))))
  endfunc
  func oPIPERSER_2_48.GetRadio()
    this.Parent.oContained.w_PIPERSER = this.RadioValue()
    return .t.
  endfunc

  func oPIPERSER_2_48.ToRadio()
    this.Parent.oContained.w_PIPERSER=trim(this.Parent.oContained.w_PIPERSER)
    return(;
      iif(this.Parent.oContained.w_PIPERSER=='I',1,;
      iif(this.Parent.oContained.w_PIPERSER=='R',2,;
      0)))
  endfunc

  func oPIPERSER_2_48.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPIPERSER_2_48.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-RX-RS")
    endwith
    endif
  endfunc

  add object oPIMODINC_2_49 as StdTrsCombo with uid="IJMBIMWTDI",rtrep=.t.,;
    cFormVar="w_PIMODINC", RowSource=""+"Accredito,"+"Bonifico,"+"Altro" , ;
    HelpContextID = 37050567,;
    Height=25, Width=118, Left=775, Top=317,;
    cTotal="", cQueryName = "PIMODINC",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPIMODINC_2_49.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PIMODINC,&i_cF..t_PIMODINC),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'B',;
    iif(xVal =3,'X',;
    space(1)))))
  endfunc
  func oPIMODINC_2_49.GetRadio()
    this.Parent.oContained.w_PIMODINC = this.RadioValue()
    return .t.
  endfunc

  func oPIMODINC_2_49.ToRadio()
    this.Parent.oContained.w_PIMODINC=trim(this.Parent.oContained.w_PIMODINC)
    return(;
      iif(this.Parent.oContained.w_PIMODINC=='A',1,;
      iif(this.Parent.oContained.w_PIMODINC=='B',2,;
      iif(this.Parent.oContained.w_PIMODINC=='X',3,;
      0))))
  endfunc

  func oPIMODINC_2_49.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPIMODINC_2_49.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-RX-RS")
    endwith
    endif
  endfunc

  add object oObj_2_50 as cp_runprogram with uid="CIFDRCWHMK",width=160,height=23,;
   left=775, top=563,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("SZ")',;
    cEvent = "w_PISEZDOG Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oObj_2_51 as cp_runprogram with uid="WDIPDUUCAD",width=160,height=23,;
   left=775, top=583,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("PR")',;
    cEvent = "w_PIPRORET Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oObj_2_52 as cp_runprogram with uid="BSFAWYFMQH",width=160,height=23,;
   left=775, top=603,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("ME")',;
    cEvent = "w_PIPERSER Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oObj_2_53 as cp_runprogram with uid="GLNINMQBNP",width=160,height=23,;
   left=775, top=623,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("MI")',;
    cEvent = "w_PIMODINC Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oObj_2_54 as cp_runprogram with uid="LVTECJUXGG",width=160,height=23,;
   left=775, top=643,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BIR("PA")',;
    cEvent = "w_PIPAEPAG Changed",;
    nPag=2;
    , HelpContextID = 181571558

  add object oPIANNREG_2_55 as StdTrsField with uid="IEYEZXQYEL",rtseq=84,rtrep=.t.,;
    cFormVar="w_PIANNREG",value=space(4),enabled=.f.,;
    ToolTipText = "Anno di registrazione della dichiarazione da rettificare",;
    HelpContextID = 124315453,;
    cTotal="", bFixedPos=.t., cQueryName = "PIANNREG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=775, Top=382, cSayPict=['9999'], cGetPict=['9999'], InputMask=replicate('X',4)

  func oPIANNREG_2_55.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-RX-RS")
    endwith
    endif
  endfunc

  proc oPIANNREG_2_55.mAfter
    with this.Parent.oContained
      .w_PIANNREG = yy2yyyy(.w_PIANNREG)
    endwith
  endproc

  add object oPIPROGSE_2_56 as StdTrsField with uid="ENTUNDBLRA",rtseq=85,rtrep=.t.,;
    cFormVar="w_PIPROGSE",value=space(5),enabled=.f.,;
    ToolTipText = "Progressivo sezione da rettificare",;
    HelpContextID = 209573691,;
    cTotal="", bFixedPos=.t., cQueryName = "PIPROGSE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=775, Top=415, cSayPict=[repl("9",5)], cGetPict=[repl("9",5)], InputMask=replicate('X',5)

  func oPIPROGSE_2_56.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-RX-RS")
    endwith
    endif
  endfunc

  add object oBtn_2_57 as StdButton with uid="WTDNRMYDBZ",width=48,height=45,;
   left=883, top=292,;
    CpPicture="bmp\doc.bmp", caption="", nPag=2;
    , ToolTipText = "Premere per accedere al movimento INTRA rettificato";
    , HelpContextID = 257479114;
    , TabStop=.f., Caption='\<Mov. Rett.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_57.Click()
      with this.Parent.oContained
        GSAR_BIN(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_57.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  func oBtn_2_57.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PITIPMOV $ "CE-RC-AC-RA-CS-AS" or Empty(.w_PIPROGSE) or Empty (.w_PIANNREG) or .w_PINUMFAT=0 or empty(.w_PIDATFAT))
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mitBodyRow as CPBodyRowCnt
  Width=584
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPIIMPNAZ_2_1 as StdTrsField with uid="ADKKOMGGND",rtseq=38,rtrep=.t.,;
    cFormVar="w_PIIMPNAZ",value=0,;
    ToolTipText = "Importo in valuta nazionale",;
    HelpContextID = 59270992,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "L'importo non pu� essere negativo",;
   bGlobalFont=.t.,;
    Height=17, Width=154, Left=71, Top=0, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]

  func oPIIMPNAZ_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PIIMPNAZ >= 0)
    endwith
    return bRes
  endfunc

  add object oCPROWORD_2_2 as StdTrsField with uid="IKEJGEBGRD",rtseq=39,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,enabled=.f.,;
    ToolTipText = "Progressivo di riga",;
    HelpContextID = 83557738,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oPIIMPVAL_2_3 as StdTrsField with uid="XWUWBKCFTX",rtseq=40,rtrep=.t.,;
    cFormVar="w_PIIMPVAL",value=0,;
    ToolTipText = "Importo in valuta originaria",;
    HelpContextID = 193488706,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valuta fornitore non aderente all'EMU: l'importo deve essere maggiore di zero",;
   bGlobalFont=.t.,;
    Height=17, Width=154, Left=234, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oPIIMPVAL_2_3.mCond()
    with this.Parent.oContained
      return ((.w_PIIMPNAZ<>0 And .w_PITIPMOV='RX') OR .w_PITIPMOV $ "AC-RA-AS")
    endwith
  endfunc

  func oPIIMPVAL_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PIIMPNAZ=0 or .w_CAMVAL<> 0 or (.w_PIIMPNAZ > 0 and .w_CAMVAL = 0 and .w_PIIMPVAL > 0))
    endwith
    return bRes
  endfunc

  add object oPINATTRA_2_4 as StdTrsField with uid="KFSFWUFRED",rtseq=41,rtrep=.t.,;
    cFormVar="w_PINATTRA",value=space(3),;
    ToolTipText = "Natura della transazione",;
    HelpContextID = 163362615,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=395, Top=0, cSayPict=["!!!"], cGetPict=["!!!"], InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="TIPITRAN", cZoomOnZoom="GSAR_ATT", oKey_1_1="TTCODICE", oKey_1_2="this.w_PINATTRA"

  func oPINATTRA_2_4.mCond()
    with this.Parent.oContained
      return (.w_PITIPMOV $ "AC-RA-RC-CE")
    endwith
  endfunc

  func oPINATTRA_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPINATTRA_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPINATTRA_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPITRAN','*','TTCODICE',cp_AbsName(this.parent,'oPINATTRA_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATT',"Tipo transazione",'',this.parent.oContained
  endproc
  proc oPINATTRA_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TTCODICE=this.parent.oContained.w_PINATTRA
    i_obj.ecpSave()
  endproc

  add object oPINOMENC_2_5 as StdTrsField with uid="KWEMPIEHOM",rtseq=42,rtrep=.t.,;
    cFormVar="w_PINOMENC",value=space(8),;
    ToolTipText = "Nomenclatura combinata della tariffa doganale",;
    HelpContextID = 94718151,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Nomenclatura non valida o non definita",;
   bGlobalFont=.t.,;
    Height=17, Width=80, Left=433, Top=0, cSayPict=[repl("X",8)], cGetPict=[repl("X",8)], InputMask=replicate('X',8), bHasZoom = .t. , cLinkFile="NOMENCLA", cZoomOnZoom="GSAR_ANM", oKey_1_1="NMCODICE", oKey_1_2="this.w_PINOMENC"

  func oPINOMENC_2_5.mCond()
    with this.Parent.oContained
      return (.w_PIIMPNAZ<>0 OR .w_PITIPMOV $ ' CE-RC-AC-RA-CS-AS')
    endwith
  endfunc

  func oPINOMENC_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPINOMENC_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPINOMENC_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NOMENCLA','*','NMCODICE',cp_AbsName(this.parent,'oPINOMENC_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANM',"Nomenclatura",'GSMA_AZN.NOMENCLA_VZM',this.parent.oContained
  endproc
  proc oPINOMENC_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NMCODICE=this.parent.oContained.w_PINOMENC
    i_obj.ecpSave()
  endproc

  add object oPIROWORD_2_6 as StdTrsField with uid="YEFJPZDFLF",rtseq=43,rtrep=.t.,;
    cFormVar="w_PIROWORD",value=0,enabled=.f.,;
    ToolTipText = "Progressivo riga documento",;
    HelpContextID = 83556154,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=531, Top=0, cSayPict=["99999"], cGetPict=["99999"]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPIIMPNAZ_2_1.When()
    return(.t.)
  proc oPIIMPNAZ_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPIIMPNAZ_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mit','ELEIMAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PISERIAL=ELEIMAST.PISERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_mit
* --- Classe per gestire la combo PICONDCON
* --- derivata dalla classe combo da tabella

define class StdZTamTableCombo as StdTrsTableCombo

proc Init()
  IF VARTYPE(this.bNoBackColor)='U'
		This.backcolor=i_EBackColor
	ENDIF

endproc

  proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear
      i_bCharKey=type(i_fk)='C'
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()]=trim(&i_fk)
        else
          this.combovalues[recno()]=&i_fk
        endif
        skip
      enddo
      use
    endif
  endproc


enddefine
* --- Fine Area Manuale
