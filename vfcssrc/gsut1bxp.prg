* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1bxp                                                        *
*              Zcp- esegue zoom codice ruolo                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-04-02                                                      *
* Last revis.: 2004-04-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pINDICE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut1bxp",oParentObject,m.pINDICE)
return(i_retval)

define class tgsut1bxp as StdBatch
  * --- Local variables
  pINDICE = space(2)
  w_DECODGRU = 0
  w_DECODROL = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    local LIndice
    * --- Check Parametro
    if VarType(this.pIndice)<>"C" or Empty(this.pIndice)
      Ah_ErrorMsg("Indice non corretto!",48,"")
      i_retcode = 'stop'
      return
    endif
    LIndice = this.pIndice
    * --- Esegue Zoom e recupera risultato ...
    this.w_DECODGRU = this.oParentObject.w_CODGRU&LINDICE
    vx_exec("query\GSAR1AGT.VZM",this)
    this.oParentObject.w_CODROL&LINDICE = this.w_DECODROL
  endproc


  proc Init(oParentObject,pINDICE)
    this.pINDICE=pINDICE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pINDICE"
endproc
