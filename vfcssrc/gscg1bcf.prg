* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg1bcf                                                        *
*              Contabilizzazione fittizia                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_272]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-10                                                      *
* Last revis.: 2012-02-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg1bcf",oParentObject)
return(i_retval)

define class tgscg1bcf as StdBatch
  * --- Local variables
  w_MAXDAT = ctod("  /  /  ")
  w_STALIG = ctod("  /  /  ")
  w_DATBLO = ctod("  /  /  ")
  w_MESS1 = space(50)
  w_MVSERIAL = space(10)
  w_FLVEAC = space(1)
  w_OSERIAL = space(10)
  w_INIDATA = ctod("  /  /  ")
  w_FINDATA = ctod("  /  /  ")
  w_TIPCLF = space(1)
  w_CODCLF = space(15)
  w_CODVAL = space(3)
  w_CODUTE = 0
  w_NUMINI = 0
  w_NUMFIN = 0
  w_ALFINI = space(10)
  w_ALFFIN = space(10)
  w_MAXCOMIVA = ctod("  /  /  ")
  w_PNNUMREG = 0
  w_PNTIPREG = space(1)
  w_PNDATREG = ctod("  /  /  ")
  w_CONTA = 0
  w_ATTIVI = space(5)
  w_ASSEG = .f.
  w_VABENE = .f.
  w_conta = 0
  * --- WorkFile variables
  CAU_CONT_idx=0
  PNT_MAST_idx=0
  DOC_MAST_idx=0
  VALUTE_idx=0
  PNT_DETT_idx=0
  SALDICON_idx=0
  PAR_TITE_idx=0
  AZIENDA_idx=0
  CONTI_idx=0
  BAN_CONTI_idx=0
  CON_PAGA_idx=0
  ATTIDETT_idx=0
  COC_MAST_idx=0
  VOCIIVA_idx=0
  CONTVEAC_idx=0
  DATIRITE_idx=0
  CAUIVA_idx=0
  CAUIVA1_idx=0
  MOD_PAGA_idx=0
  CCM_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contabilizzazione fittizia di Anticipazioni, Acconti e Fatture (da GSCG1KCF)
    if this.oParentObject.w_TIPOMSK="C"
      if this.oParentObject.w_ANTICIP="S"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.oParentObject.w_ACCONTI="S"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.oParentObject.w_FATTURE="S"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      * --- Try
      local bErr_047B92F0
      bErr_047B92F0=bTrsErr
      this.Try_047B92F0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        Ah_errormsg("Errore in rimozione contabilizzazione fittizia %0%1", " "," ",Message())
      endif
      bTrsErr=bTrsErr or bErr_047B92F0
      * --- End
    endif
  endproc
  proc Try_047B92F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL"
      do vq_exec with 'GSCGDBCF',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLCONT ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
      +",MVRIFCON ="+cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFCON');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
      +"DOC_MAST.MVFLCONT ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
      +",DOC_MAST.MVRIFCON ="+cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFCON');
          +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
          +"MVFLCONT,";
          +"MVRIFCON";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT')+",";
          +cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFCON')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
      +"MVFLCONT ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
      +",MVRIFCON ="+cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFCON');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLCONT ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
      +",MVRIFCON ="+cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFCON');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_conta = i_ROWS
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL"
      do vq_exec with 'GSCGABCF',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVRIFACC ="+cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFACC');
          +i_ccchkf;
          +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
      +"DOC_MAST.MVRIFACC ="+cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFACC');
          +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DOC_MAST.MVSERIAL = t2.MVSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set (";
          +"MVRIFACC";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFACC')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
      +"MVRIFACC ="+cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFACC');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVRIFACC ="+cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFACC');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_conta = this.w_conta + i_ROWS
    Ah_errormsg("Elaborazione terminata, eliminate %1 contabilizzazioni fittizie"," !","",Alltrim(str(this.w_conta)))
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contabilizzazione fittizia anticipazioni
    this.w_INIDATA = IIF(EMPTY(this.oParentObject.w_DATINI),i_INIDAT,this.oParentObject.w_DATINI)
    this.w_FINDATA = this.oParentObject.w_DATFIN
    vq_exec("..\ALTE\EXE\QUERY\GSAL_BGA.VQR",this,"ANTICIPA")
    if Reccount("ANTICIPA")>0
      SELECT ("ANTICIPA")
      GO TOP
      ah_Msg("Inizio fase di contabilizzazione fittizia anticipazioni...")
      * --- Fase di Blocco
      *     Blocco la Prima Nota con data = Max (data reg.)
      *     Calcolo il Max Data reg.
      this.w_MAXDAT = ANTICIPA.MVDATREG
      * --- Try
      local bErr_047C2020
      bErr_047C2020=bTrsErr
      this.Try_047C2020()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg(this.w_MESS1,"!","")
      endif
      bTrsErr=bTrsErr or bErr_047C2020
      * --- End
      this.w_OSERIAL = REPL("#", 10)
      * --- Try
      local bErr_047B5360
      bErr_047B5360=bTrsErr
      this.Try_047B5360()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.w_MESS1 = "Errore nella scrittura"
      endif
      bTrsErr=bTrsErr or bErr_047B5360
      * --- End
      * --- Alla Fine Rimuovo il Blocco Primanota
      * --- Try
      local bErr_0498F1B0
      bErr_0498F1B0=bTrsErr
      this.Try_0498F1B0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        ah_ErrorMsg("Impossibile rimuovere blocco primanota. Rimuoverlo dai dati azienda","!","")
      endif
      bTrsErr=bTrsErr or bErr_0498F1B0
      * --- End
    else
      this.w_MESS1 = "Per l'intervallo selezionato non esistono anticipazioni da marcare come contabilizzate, verificare Parametri anticipazioni"
      ah_ErrorMsg(this.w_MESS1,"!","")
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_047C2020()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    this.w_STALIG = cp_CharToDate("  -  -  ")
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO,AZSTALIG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO,AZSTALIG;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = 'AZIENDA'
      return
    endif
    select (i_nOldArea)
    if empty(NVL(this.w_datblo,cp_CharToDate("  -  -  ")))
      * --- Controllo Integrit� Prima Nota
      if this.w_MAXDAT<=MAX(this.w_STALIG,g_CONCON)
        this.w_MESS1 = "Data registrazione inferiore o uguale alla data di consolidamento o alla data di stampa del libro giornale"
        * --- Raise
        i_Error=this.w_MESS1
        return
      else
        * --- Inserisce <Blocco> per Primanota
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_MAXDAT),'AZIENDA','AZDATBLO');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZDATBLO = this.w_MAXDAT;
              &i_ccchkf. ;
           where;
              AZCODAZI = this.oParentObject.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    else
      * --- UN altro utente ha impostato il blocco - controllo concorrenza
      this.w_MESS1 = "Prima nota bloccata - verificare semaforo bollati in dati azienda. Impossibile contabilizzare"
      * --- Raise
      i_Error=this.w_MESS1
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_047B5360()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if Reccount("ANTICIPA")>0
      Select ANTICIPA
      Go Top
      Scan 
      this.w_MVSERIAL = ANTICIPA.MVSERIAL
      if this.w_OSERIAL<>this.w_MVSERIAL
        * --- Marco documenti generati
        * --- Write into DOC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVRIFCON ="+cp_NullLink(cp_ToStrODBC("ABCDEFGHIL"),'DOC_MAST','MVRIFCON');
          +",MVFLCONT ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLCONT');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                 )
        else
          update (i_cTable) set;
              MVRIFCON = "ABCDEFGHIL";
              ,MVFLCONT = "S";
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      Endscan
      * --- commit
      cp_EndTrs(.t.)
      this.w_MESS1 = "Contabilizzazione fittizia anticipazioni terminata con successo"
      ah_ErrorMsg(this.w_MESS1,"!","")
    endif
    return
  proc Try_0498F1B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contabilizzazione fittizia acconti
    this.w_FLVEAC = "V"
    this.w_TIPCLF = "C"
    this.w_CODCLF = SPACE(15)
    this.w_CODVAL = SPACE(3)
    this.w_CODUTE = 0
    vq_exec("QUERY\GSCGAKCN.VQR",this,"CONTADOC")
    if RECCOUNT("CONTADOC") > 0
      SELECT CONTADOC
      GO TOP
      ah_Msg("Inizio fase di contabilizzazione fittizia acconti...")
      * --- Fase di Blocco
      * --- 1 - Blocco la Prima Nota con data = Max (data reg.)
      * --- Calcolo il Max Data reg.
      CALCULATE MAX(MVDATREG) FOR NOT EMPTY(NVL(MVSERIAL," ")) AND ;
      NOT EMPTY(NVL(MVCODCON," ")) TO this.w_MAXDAT
      * --- Try
      local bErr_04772FF0
      bErr_04772FF0=bTrsErr
      this.Try_04772FF0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg(this.w_MESS1,"!","")
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_04772FF0
      * --- End
      * --- Try
      local bErr_04772DB0
      bErr_04772DB0=bTrsErr
      this.Try_04772DB0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.w_MESS1 = "Errore nella scrittura"
      endif
      bTrsErr=bTrsErr or bErr_04772DB0
      * --- End
      * --- Rimuovo il blocco Stampa Libro giornale
      * --- Try
      local bErr_04772ED0
      bErr_04772ED0=bTrsErr
      this.Try_04772ED0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        ah_ErrorMsg("Impossibile rimuovere i blocchi. Rimuoverli dai dati azienda",,"")
      endif
      bTrsErr=bTrsErr or bErr_04772ED0
      * --- End
    else
      this.w_MESS1 = "Per l'intervallo selezionato non esistono acconti da marcare come contabilizzati"
      ah_ErrorMsg(this.w_MESS1,"!","")
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_04772FF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO,AZSTALIG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO,AZSTALIG;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(NVL(this.w_datblo,cp_CharToDate("  -  -  ")))
      * --- Inserisce <Blocco> per Primanota
      * --- Write into AZIENDA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_MAXDAT),'AZIENDA','AZDATBLO');
            +i_ccchkf ;
        +" where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
               )
      else
        update (i_cTable) set;
            AZDATBLO = this.w_MAXDAT;
            &i_ccchkf. ;
         where;
            AZCODAZI = this.oParentObject.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- UN altro utente ha impostato il blocco - controllo concorrenza
      this.w_MESS1 = "Prima nota bloccata - verificare semaforo bollati in dati azienda. Impossibile contabilizzare"
      * --- Raise
      i_Error=this.w_MESS1
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04772DB0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT CONTADOC
    GO TOP
    SCAN FOR NOT EMPTY(NVL(MVSERIAL," ")) AND NVL(MVACCONT,0)<>0
    this.w_MVSERIAL = CONTADOC.MVSERIAL
    * --- Scrive Flag Contabilizzato sul Documento Origine
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVRIFACC ="+cp_NullLink(cp_ToStrODBC("ABCDEFGHIL"),'DOC_MAST','MVRIFACC');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVRIFACC = "ABCDEFGHIL";
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    SELECT CONTADOC
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    this.w_MESS1 = "Contabilizzazione fittizia acconti terminata con successo"
    ah_ErrorMsg(this.w_MESS1,"!","")
    return
  proc Try_04772ED0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contabilizzazione fittizia fatture
    this.w_FLVEAC = "V"
    this.w_NUMINI = 1
    this.w_NUMFIN = 999999999999999
    this.w_ALFINI = ""
    this.w_ALFFIN = ""
    vq_exec("query\GSVEAQCD.VQR",this,"CONTADOC")
    if RECCOUNT("CONTADOC") > 0
      SELECT CONTADOC
      GO TOP
      ah_Msg("Inizio fase di contabilizzazione fittizia fatture...",.T.)
      * --- Fase di Blocco
      * --- 1 - Blocco la Prima Nota con data = Max (data reg.)
      * --- 2 - Blocco tutti i Registri Iva con data = Max( Data reg)
      * --- Calcolo il Max Data reg.
      CALCULATE MAX(MVDATREG) FOR NOT EMPTY(NVL(MVSERIAL," ")) AND NOT EMPTY(NVL(MVCAUCON," ")) TO this.w_MAXDAT
      CALCULATE MAX(NVL(MVDATCIV, this.w_PNDATREG)) FOR NOT EMPTY(NVL(MVSERIAL," ")) AND NOT EMPTY(NVL(MVCAUCON," ")) TO this.w_MAXCOMIVA
      this.w_VABENE = .T.
      this.w_ASSEG = .F.
      * --- Leggo la data dei blocco e la data stampa del libro giornale
      this.w_MESS1 = BLOC_AZI( .T. , this.w_MAXDAT, .F. ) 
      if not Empty( this.w_MESS1 )
        ah_ErrorMsg("%1","!","", this.w_MESS1)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      * --- Try
      local bErr_0476C9C0
      bErr_0476C9C0=bTrsErr
      this.Try_0476C9C0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- Rimuovo i blocchi
        this.w_MESS1 = BLOC_AZI( .F. ) 
        if not Empty( this.w_MESS1 )
          ah_ErrorMsg("%1","!","",this.w_MESS1)
        endif
        ah_ErrorMsg(i_errmsg,,"")
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_0476C9C0
      * --- End
      * --- begin transaction
      cp_BeginTrs()
      * --- Try
      local bErr_0476C630
      bErr_0476C630=bTrsErr
      this.Try_0476C630()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        this.w_MESS1 = "Errore nella scrittura"
      endif
      bTrsErr=bTrsErr or bErr_0476C630
      * --- End
      * --- Rimuovo i blocchi
      this.w_MESS1 = BLOC_AZI( .F. ) 
      if not Empty( this.w_MESS1 )
        ah_ErrorMsg("%1","!","",this.w_MESS1)
      endif
      * --- Stampa Libro giornale e Registri Iva
      * --- Try
      local bErr_0476BFD0
      bErr_0476BFD0=bTrsErr
      this.Try_0476BFD0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        ah_ErrorMsg("Impossibile rimuovere i blocchi. Rimuoverli dai dati azienda",,"")
      endif
      bTrsErr=bTrsErr or bErr_0476BFD0
      * --- End
    else
      this.w_MESS1 = "Per l'intervallo selezionato non esistono fatture da marcare come contabilizzate"
      ah_ErrorMsg(this.w_MESS1,"!","")
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_0476C9C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Tento il blocco dei Registri IVA
    * --- se g_catazi vuoto esci e segnala nessuna attivit�
    if empty(nvl(g_catazi,"")) AND this.w_VABENE
      this.w_VABENE = .F.
      * --- Raise
      i_Error="Nessuna attivit� presente. Inserirne una per proseguire"
      return
    endif
    * --- Se il numero e il tipo del registro non ha attivit� significa che non � stato ancora stampato
    * --- infatti la stampa IVA crea in automatico una voce nell'attivit�, questo non esclude che qualche utentedecida di stampare i  regi
    * --- durante la contabilizzaione => associo il registro all'attivit� di default
    if this.w_VABENE
      select max(NVL(MVDATCIV, MVDATREG)) as MVDATREG,CCTIPREG,CCNUMREG FROM ;
      CONTADOC GROUP BY CCTIPREG,CCNUMREG ;
      WHERE NOT EMPTY(NVL(MVSERIAL," ")) AND NOT EMPTY(NVL(MVCAUCON," ")) ;
      into cursor reg_iva1
      vq_exec("query\GSVE3QCD.VQR",this,"reg_iva2")
      if USED("reg_iva2")
        SELECT * FROM reg_iva1 INTO CURSOR reg_iva4 UNION ;
        SELECT * FROM reg_iva2
        SELECT reg_iva2
        USE
      else
        SELECT * FROM reg_iva1 INTO CURSOR reg_iva4
      endif
      SELECT reg_iva1
      USE
      vq_exec("query\GSVE4QCD.VQR",this,"reg_iva3")
      if USED("reg_iva3")
        SELECT * FROM reg_iva4 INTO CURSOR reg_iva5 UNION ;
        SELECT * FROM reg_iva3
        SELECT reg_iva3
        USE
      else
        SELECT * FROM reg_iva4 INTO CURSOR reg_iva5
      endif
      SELECT reg_iva4
      USE
      select max(NVL(MVDATREG, this.w_PNDATREG)) as MVDATREG,CCTIPREG,CCNUMREG FROM ;
      reg_iva5 GROUP BY CCTIPREG,CCNUMREG ;
      into cursor reg_iva
      SELECT reg_iva5
      USE
      SELECT reg_iva
      scan
      this.w_MAXDAT = MVDATREG
      this.w_PNNUMREG = NVL(CCNUMREG, 1)
      this.w_PNTIPREG = NVL(CCTIPREG, "N")
      * --- Se ha un tipo IVA buono
      if this.w_PNTIPREG<>"N"
        this.w_CONTA = 0
        * --- Select from ATTIDETT
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ATDATBLO,ATCODATT  from "+i_cTable+" ATTIDETT ";
              +" where ATNUMREG="+cp_ToStrODBC(this.w_PNNUMREG)+" AND ATTIPREG="+cp_ToStrODBC(this.w_PNTIPREG)+"";
               ,"_Curs_ATTIDETT")
        else
          select ATDATBLO,ATCODATT from (i_cTable);
           where ATNUMREG=this.w_PNNUMREG AND ATTIPREG=this.w_PNTIPREG;
            into cursor _Curs_ATTIDETT
        endif
        if used('_Curs_ATTIDETT')
          select _Curs_ATTIDETT
          locate for 1=1
          do while not(eof())
          this.w_DATBLO = NVL(_Curs_ATTIDETT.ATDATBLO, cp_CharToDate("  -  -  "))
          this.w_ATTIVI = NVL(_Curs_ATTIDETT.ATCODATT, cp_CharToDate("  -  -  "))
          this.w_CONTA = this.w_CONTA+1
            select _Curs_ATTIDETT
            continue
          enddo
          use
        endif
        if this.w_conta>1
          * --- Raise
          i_Error="Esistono registri dello stesso numero e tipo associati ad una/pi� attivit�"
          return
          this.w_VABENE = .F.
        endif
        this.w_CONTA = 0
        if empty(NVL(this.w_datblo,cp_CharToDate("  -  -  "))) and this.w_VABENE
          if EMPTY(NVL(this.w_ATTIVI,""))
            * --- Se l'attivit� non la trovo per default � quella principale
            this.w_ATTIVI = g_CATAZI
            * --- Insert into ATTIDETT
            i_nConn=i_TableProp[this.ATTIDETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATTIDETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"ATCODATT"+",ATNUMREG"+",ATTIPREG"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_ATTIVI),'ATTIDETT','ATCODATT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'ATTIDETT','ATNUMREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'ATTIDETT','ATTIPREG');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'ATCODATT',this.w_ATTIVI,'ATNUMREG',this.w_PNNUMREG,'ATTIPREG',this.w_PNTIPREG)
              insert into (i_cTable) (ATCODATT,ATNUMREG,ATTIPREG &i_ccchkf. );
                 values (;
                   this.w_ATTIVI;
                   ,this.w_PNNUMREG;
                   ,this.w_PNTIPREG;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            * --- sotto tranzazione � meglio non mettere messaggi per avvisare l'utente di questa impostazione
            this.w_ASSEG = .T.
          endif
          * --- Inserisce <Blocco> per Primanota
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_MAXDAT),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATCODATT = "+cp_ToStrODBC(this.w_ATTIVI);
                +" and ATNUMREG = "+cp_ToStrODBC(this.w_PNNUMREG);
                +" and ATTIPREG = "+cp_ToStrODBC(this.w_PNTIPREG);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_MAXDAT;
                &i_ccchkf. ;
             where;
                ATCODATT = this.w_ATTIVI;
                and ATNUMREG = this.w_PNNUMREG;
                and ATTIPREG = this.w_PNTIPREG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- UN ALTRO STA STAMPANDO REGISTRI IVA - controllo concorrenza
          this.w_VABENE = .F.
          * --- Raise
          i_Error="Contabilizzazione annullata. Blocco stampa registri IVA"
          return
        endif
      endif
      endscan
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0476C630()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_OSERIAL = REPL("#", 10)
    SELECT CONTADOC
    GO TOP
    SCAN FOR NOT EMPTY(NVL(MVSERIAL," ")) AND NOT EMPTY(NVL(MVCAUCON," "))
    * --- Testa Cambio Documento
    if this.w_OSERIAL<>ContaDoc.MvSerial
      this.w_OSERIAL = ContaDoc.MvSerial
      * --- Marco documenti generati
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLCONT ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLCONT');
        +",MVRIFCON ="+cp_NullLink(cp_ToStrODBC("ABCDEFGHIL"),'DOC_MAST','MVRIFCON');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_OSERIAL);
               )
      else
        update (i_cTable) set;
            MVFLCONT = "S";
            ,MVRIFCON = "ABCDEFGHIL";
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_OSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    SELECT CONTADOC
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    this.w_MESS1 = "Contabilizzazione fittizia fatture terminata con successo"
    ah_ErrorMsg(this.w_MESS1,"!","")
    return
  proc Try_0476BFD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    select reg_iva
    scan for NVL(cctipreg,"")<>"N"
    this.w_PNNUMREG = NVL(CCNUMREG, 1)
    this.w_PNTIPREG = NVL(CCTIPREG, "N")
    * --- Write into ATTIDETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ATTIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
          +i_ccchkf ;
      +" where ";
          +"ATNUMREG = "+cp_ToStrODBC(this.w_PNNUMREG);
          +" and ATTIPREG = "+cp_ToStrODBC(this.w_PNTIPREG);
             )
    else
      update (i_cTable) set;
          ATDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          ATNUMREG = this.w_PNNUMREG;
          and ATTIPREG = this.w_PNTIPREG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ENDSCAN
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Cursori di Appoggio
    if USED("ANTICIPA")
      SELECT ANTICIPA
      USE
    endif
    if USED("CONTADOC")
      SELECT CONTADOC
      USE
    endif
    if USED("reg_iva")
      SELECT reg_iva
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,20)]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='DOC_MAST'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='PNT_DETT'
    this.cWorkTables[6]='SALDICON'
    this.cWorkTables[7]='PAR_TITE'
    this.cWorkTables[8]='AZIENDA'
    this.cWorkTables[9]='CONTI'
    this.cWorkTables[10]='BAN_CONTI'
    this.cWorkTables[11]='CON_PAGA'
    this.cWorkTables[12]='ATTIDETT'
    this.cWorkTables[13]='COC_MAST'
    this.cWorkTables[14]='VOCIIVA'
    this.cWorkTables[15]='CONTVEAC'
    this.cWorkTables[16]='DATIRITE'
    this.cWorkTables[17]='CAUIVA'
    this.cWorkTables[18]='CAUIVA1'
    this.cWorkTables[19]='MOD_PAGA'
    this.cWorkTables[20]='CCM_DETT'
    return(this.OpenAllTables(20))

  proc CloseCursors()
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
