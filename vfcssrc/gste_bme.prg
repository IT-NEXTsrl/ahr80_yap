* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bme                                                        *
*              Mandati SDD                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-08-06                                                      *
* Last revis.: 2013-08-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bme",oParentObject,m.pParame)
return(i_retval)

define class tgste_bme as StdBatch
  * --- Local variables
  pParame = space(4)
  w_Cprownum = 0
  w_Mess = space(10)
  w_Macodice = space(15)
  w_Mdserial = space(10)
  w_Mdroword = 0
  w_Mdrownum = 0
  w_Mdtipseq = space(4)
  w_Zoom = .NULL.
  w_Zoom1 = space(10)
  w_Ok = .f.
  w_Numrec = 0
  * --- WorkFile variables
  MANDDATI_idx=0
  PAR_TITE_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch utilizzato per gestire il caricamento degli zoom dell'anagrafica
    *     "Mandati SDD"
    do case
      case this.pParame="IBAN"
        this.oparentObject.ecpsave()
        i_retcode = 'stop'
        return
      case this.pParame="AGGIO"
        this.w_Ok = .f.
        this.w_Cprownum = 0
        this.w_Zoom = This.oParentObject.w_Scadenze
        this.w_Zoom1 = this.oParentObject.oParentObject.w_Scad_Mandati
        this.w_Macodice = This.oParentObject.oParentObject.w_Macodice
        this.w_Mess = "Attenzione:%0Le scadenze selezionate saranno abbinate al mandato%0Confermi?"
        if AH_YESNO(this.w_Mess)
          * --- Select from MANDDATI
          i_nConn=i_TableProp[this.MANDDATI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MANDDATI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select Max(CPROWNUM) As NumeroRiga  from "+i_cTable+" MANDDATI ";
                +" where MDCODICE="+cp_ToStrODBC(this.w_MACODICE)+"";
                +" group by MDCODICE";
                 ,"_Curs_MANDDATI")
          else
            select Max(CPROWNUM) As NumeroRiga from (i_cTable);
             where MDCODICE=this.w_MACODICE;
             group by MDCODICE;
              into cursor _Curs_MANDDATI
          endif
          if used('_Curs_MANDDATI')
            select _Curs_MANDDATI
            locate for 1=1
            do while not(eof())
            this.w_Cprownum = NumeroRiga
              select _Curs_MANDDATI
              continue
            enddo
            use
          endif
          * --- Devo verificare se esiste almeno un record associato alla tabella
          *     del dettaglio scadenze per mandato, se esiste tutte le righe che 
          *     vengono inserite assumono nel valore tipo sequenza "Recurrent" 
          NC = this.w_ZOOM.cCursor
          SELECT (NC)
          select Dataord,Ptserial,Ptroword,Cprownum; 
 from (NC) where Xchk=1 into cursor Scadenze order by Dataord
          Select Scadenze 
 Go Top
          * --- Try
          local bErr_039A0708
          bErr_039A0708=bTrsErr
          this.Try_039A0708()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_errormsg("Errore durante aggiornamento, operazione abbandonata")
          endif
          bTrsErr=bTrsErr or bErr_039A0708
          * --- End
        endif
        if this.w_Ok
          This.oParentObject.oParentObject.NotifyEvent("Interroga")
          Select (this.w_Zoom1.cCursor) 
 Go Top
          this.w_Zoom1.refresh()
          this.oparentobject.oParentObject.oPgFrm.ActivePage=2
          this.oparentobject.oParentObject.oPgFrm.Pages(2).oPag.setfocus()
          * --- Chiudo la Maschera
          This.oParentObject.ecpQuit()
          Ah_ErrorMsg("Aggiornamento completato")
        endif
        Use in select("Scadenze")
      case LEFT(this.pParame, 3) = "SEL" AND not EMPTY (this.oParentObject.w_Tipo)
        if this.oParentObject.w_Tipo="A"
          this.w_ZOOM = This.oParentObject.w_Scad_Mandati
        else
          this.w_ZOOM = This.oParentObject.w_Scadenze
        endif
        Select ( this.w_Zoom.cCursor )
        if Used(this.w_Zoom.cCursor) And Reccount (this.w_Zoom.cCursor)>0
          Select ( this.w_Zoom.cCursor )
          this.w_Numrec = Recno()
          Update (this.w_Zoom.cCursor) Set Xchk=Icase(Right(this.pParame,1)="S",1,Right(this.pParame,1)="D", 0,Right (this.pParame,1)="I",Iif(Xchk=1,0,1),Xchk)
          Select ( this.w_Zoom.cCursor )
          Count For Xchk=1 To w_Count
          if w_Count>0
            this.oParentObject.w_Testsel = "S"
          else
            this.oParentObject.w_Testsel = "D"
          endif
          Go this.w_Numrec
        endif
      case this.pParame="CANC" or this.pParame="TIPS"
        this.w_ZOOM = This.oParentObject.w_Scad_Mandati
        if this.pParame="CANC"
          this.w_Mess = "Attenzione:%0Le scadenze selezionate saranno cancellate dal mandato in modo definitivo%0Confermi?"
        else
          this.w_Mess = "Attenzione:%0Alle scadenze selezionate verr� modificato il tipo sequenza%0Confermi?"
        endif
        this.w_Ok = .f.
        if Ah_Yesno(this.w_Mess)
          * --- Try
          local bErr_0399B9C8
          bErr_0399B9C8=bTrsErr
          this.Try_0399B9C8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_errormsg("Errore durante aggiornamento, operazione abbandonata")
          endif
          bTrsErr=bTrsErr or bErr_0399B9C8
          * --- End
          if this.w_Ok
            This.oParentObject.NotifyEvent("Interroga")
            Select (this.w_Zoom.cCursor) 
 Go Top
            this.w_Zoom.refresh()
            Ah_ErrorMsg("Aggiornamento completato")
          endif
        endif
      case this.pParame="RICR"
        this.w_ZOOM = This.oParentObject.w_Scadenze
        * --- Cancello il contenuto dello zoom
        ZAP IN ( this.w_ZOOM.cCursor )
        This.oParentObject.NotifyEvent("Aggiorna")
        Select (this.w_Zoom.cCursor) 
 Go Top
        this.w_Zoom.refresh()
        Select ( this.w_Zoom.cCursor )
        if Used(this.w_Zoom.cCursor) And Reccount (this.w_Zoom.cCursor)>0
          Select ( this.w_Zoom.cCursor )
          Count For Xchk=1 To w_Count
          if w_Count>0
            this.oParentObject.w_Testsel = "S"
          else
            this.oParentObject.w_Testsel = "D"
          endif
          Select ( this.w_Zoom.cCursor ) 
 Go Top
        endif
    endcase
  endproc
  proc Try_039A0708()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    Scan
    this.w_Mdserial = Scadenze.Ptserial
    this.w_Mdroword = Scadenze.Ptroword
    this.w_Mdrownum = Scadenze.Cprownum
    this.w_Mdtipseq = iif(this.w_Cprownum=0,"FRST","RCUR")
    this.w_Cprownum = this.w_Cprownum+1
    * --- Insert into MANDDATI
    i_nConn=i_TableProp[this.MANDDATI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MANDDATI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MANDDATI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MDCODICE"+",CPROWNUM"+",MDSERIAL"+",MDROWORD"+",MDROWNUM"+",MDTIPSEQ"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MACODICE),'MANDDATI','MDCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MANDDATI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDSERIAL),'MANDDATI','MDSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDROWORD),'MANDDATI','MDROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDROWNUM),'MANDDATI','MDROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MDTIPSEQ),'MANDDATI','MDTIPSEQ');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MDCODICE',this.w_MACODICE,'CPROWNUM',this.w_CPROWNUM,'MDSERIAL',this.w_MDSERIAL,'MDROWORD',this.w_MDROWORD,'MDROWNUM',this.w_MDROWNUM,'MDTIPSEQ',this.w_MDTIPSEQ)
      insert into (i_cTable) (MDCODICE,CPROWNUM,MDSERIAL,MDROWORD,MDROWNUM,MDTIPSEQ &i_ccchkf. );
         values (;
           this.w_MACODICE;
           ,this.w_CPROWNUM;
           ,this.w_MDSERIAL;
           ,this.w_MDROWORD;
           ,this.w_MDROWNUM;
           ,this.w_MDTIPSEQ;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    Endscan
    this.w_Ok = .T.
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0399B9C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    NC = this.w_ZOOM.cCursor
    Select &NC
    Go Top
    * --- Cicla sui record Selezionati
    Scan For Xchk<>0
    this.w_Macodice = Mdcodice
    this.w_Cprownum = Cprownum
    if this.pParame="CANC"
      * --- Delete from MANDDATI
      i_nConn=i_TableProp[this.MANDDATI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MANDDATI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MDCODICE = "+cp_ToStrODBC(this.w_MACODICE);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        delete from (i_cTable) where;
              MDCODICE = this.w_MACODICE;
              and CPROWNUM = this.w_CPROWNUM;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Write into MANDDATI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MANDDATI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MANDDATI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MANDDATI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MDTIPSEQ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TipSeq),'MANDDATI','MDTIPSEQ');
            +i_ccchkf ;
        +" where ";
            +"MDCODICE = "+cp_ToStrODBC(this.w_MACODICE);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            MDTIPSEQ = this.oParentObject.w_TipSeq;
            &i_ccchkf. ;
         where;
            MDCODICE = this.w_MACODICE;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    Endscan
    this.w_Ok = .T.
    this.oParentObject.w_Testsel = "D"
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='MANDDATI'
    this.cWorkTables[2]='PAR_TITE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_MANDDATI')
      use in _Curs_MANDDATI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
