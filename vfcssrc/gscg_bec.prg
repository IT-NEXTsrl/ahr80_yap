* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bec                                                        *
*              Check codice fiscale/P.IVA                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-17                                                      *
* Last revis.: 2008-01-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CODI,w_PICF,w_TIPCLF,w_CODICE,w_CODNAZ,w_ANCOGNOM,w_AN__NOME,w_ANLOCNAS,w_ANPRONAS,w_ANDATNAS,w_AN_SESSO,w_ANPERFIS,w_DUPLICATI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bec",oParentObject,m.w_CODI,m.w_PICF,m.w_TIPCLF,m.w_CODICE,m.w_CODNAZ,m.w_ANCOGNOM,m.w_AN__NOME,m.w_ANLOCNAS,m.w_ANPRONAS,m.w_ANDATNAS,m.w_AN_SESSO,m.w_ANPERFIS,m.w_DUPLICATI)
return(i_retval)

define class tgscg_bec as StdBatch
  * --- Local variables
  w_CODI = space(16)
  w_PICF = space(3)
  w_TIPCLF = space(1)
  w_CODICE = space(15)
  w_CODNAZ = space(3)
  w_ANCOGNOM = space(40)
  w_AN__NOME = space(20)
  w_ANLOCNAS = space(30)
  w_ANPRONAS = space(2)
  w_ANDATNAS = ctod("  /  /  ")
  w_AN_SESSO = space(1)
  w_ANPERFIS = space(1)
  w_DUPLICATI = space(1)
  w_SUBCOCO = space(40)
  w_SUBCOVO = space(40)
  w_CODFISC = space(16)
  w_MESS = space(10)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_ERR = 0
  w_CODISO = space(2)
  w_ERRF = .f.
  w_valdisp = space(10)
  w_prcar = 0
  w_lencf = 0
  w_icf = 0
  w_ascca = 0
  w_riscf = 0
  w_totcf = 0
  w_CA = space(10)
  * --- WorkFile variables
  CONTI_idx=0
  NAZIONI_idx=0
  ANAG_CAP_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Partita IVA / Codice Fiscale
    * --- w_CODI: Codice da Verificare
    * --- w_PICF: PI=Partita IVA; CF=Codice Fiscale; SPI=Partita IVA per Stampe; SCF=Codice Fiscale per Stampe
    if EMPTY(NVL(this.w_ANCOGNOM,""))
      this.w_ANCOGNOM = ""
    endif
    if EMPTY(NVL(this.w_AN__NOME,""))
      this.w_AN__NOME = ""
    endif
    if EMPTY(NVL(this.w_ANLOCNAS,""))
      this.w_ANLOCNAS = ""
    endif
    if EMPTY(NVL(this.w_ANPRONAS,""))
      this.w_ANPRONAS = ""
    endif
    if EMPTY(NVL(this.w_ANDATNAS,ctod("  -  -  ")))
      this.w_ANDATNAS = ctod("  -  -  ")
    endif
    if EMPTY(NVL(this.w_AN_SESSO,""))
      this.w_AN_SESSO = ""
    endif
    if EMPTY(NVL(this.w_ANPERFIS,""))
      this.w_ANPERFIS = "N"
    endif
    * --- se il codice (fornitore) non viene passato lo metto vuoto
    if EMPTY(NVL(this.w_TIPCLF,""))
      this.w_TIPCLF = ""
    endif
    if EMPTY(NVL(this.w_CODICE,""))
      this.w_CODICE = ""
    endif
    if EMPTY(NVL(this.w_CODNAZ,""))
      this.w_CODNAZ = g_CODNAZ
    endif
    if this.w_PICF="CF" OR this.w_PICF = "SCF"
      this.w_CODI = Upper(this.w_CODI)
    endif
    this.w_CODISO = "  "
    this.w_ERR = 0
    this.w_ERRF = .T.
    * --- Per le Societa' di Capitali (Test C.Fiscale = P.IVA)
    if (this.w_PICF="CF" OR this.w_PICF="SCF") AND LEN(ALLTRIM(this.w_CODI))=11
      this.w_PICF = IIF(this.w_PICF="SCF", "SPI", "PI")
    endif
    this.w_prcar = ASC(LEFT(this.w_CODI,1))
    this.w_lencf = LEN(ALLTRIM(this.w_CODI))
    this.w_icf = 1
    this.w_totcf = 0
    this.w_ascca = 0
    this.w_riscf = 0
    do case
      case this.w_PICF="PI" OR this.w_PICF = "SPI"
        * --- Controllo Partita IVA
        * --- Leggo il codice ISO
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NACODISO"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_CODNAZ);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NACODISO;
            from (i_cTable) where;
                NACODNAZ = this.w_CODNAZ;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODISO = NVL(cp_ToDate(_read_.NACODISO),cp_NullValue(_read_.NACODISO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_CODISO = UPPER(this.w_CODISO)
        if this.w_PICF="PI" AND (NOT EMPTY(this.w_TIPCLF) ) AND ( NOT EMPTY(this.w_CODI)) 
          * --- Select from CONTI
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ANCODICE,ANPARIVA  from "+i_cTable+" CONTI ";
                +" where ANPARIVA="+cp_ToStrODBC(this.w_CODI)+" AND ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF)+" AND ANCODICE<>"+cp_ToStrODBC(this.w_CODICE)+"";
                 ,"_Curs_CONTI")
          else
            select ANCODICE,ANPARIVA from (i_cTable);
             where ANPARIVA=this.w_CODI AND ANTIPCON=this.w_TIPCLF AND ANCODICE<>this.w_CODICE;
              into cursor _Curs_CONTI
          endif
          if used('_Curs_CONTI')
            select _Curs_CONTI
            locate for 1=1
            do while not(eof())
            this.w_ERRF = .F.
            this.w_ERR = 2
              select _Curs_CONTI
              continue
            enddo
            use
          endif
        endif
        * --- Testo se la partita IVA � gi� stata utilizzata - SE VUOTA VA BENE
        if this.w_ERR=0
          if this.w_lencf=0
            this.w_ERR = 1
          endif
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.w_PICF="CF" OR this.w_PICF = "SCF"
        if this.w_PICF="CF" AND (NOT EMPTY(this.w_TIPCLF) ) AND (NOT EMPTY(this.w_CODI)) 
          * --- Select from CONTI
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ANCODICE,ANCODFIS  from "+i_cTable+" CONTI ";
                +" where ANCODFIS="+cp_ToStrODBC(this.w_CODI)+" AND ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF)+" AND ANCODICE<>"+cp_ToStrODBC(this.w_CODICE)+"";
                 ,"_Curs_CONTI")
          else
            select ANCODICE,ANCODFIS from (i_cTable);
             where ANCODFIS=this.w_CODI AND ANTIPCON=this.w_TIPCLF AND ANCODICE<>this.w_CODICE;
              into cursor _Curs_CONTI
          endif
          if used('_Curs_CONTI')
            select _Curs_CONTI
            locate for 1=1
            do while not(eof())
            this.w_ERRF = .F.
            this.w_ERR = 3
              select _Curs_CONTI
              continue
            enddo
            use
          endif
        endif
        * --- Controllo Codice Fiscale
        do case
          case this.w_lencf=0 AND this.w_ERR=0
            this.w_ERR = 1
          case this.w_lencf <> 16 AND this.w_ERR=0
            this.w_ERR = 1
          case (this.w_prcar < 65 OR this.w_prcar > 90) AND this.w_ERR=0
            this.w_ERR = 1
        endcase
        * --- Se test lunghezza codice OK
        if this.w_ERR=0 AND this.w_ANPERFIS="S"
          if !EMPTY(NVL(this.w_ANCOGNOM,""))
            * --- Controllo la parte del Codice Fiscale relativa al Cognome
            this.w_SUBCOVO = ""
            this.w_SUBCOCO = ""
            do while this.w_icf <= LEN(ALLTRIM(this.w_ANCOGNOM)) and LEN(ALLTRIM(this.w_SUBCOCO))<=3 
              * --- Conto le consonanti e le vocali del cognome e del nome
              this.w_ca = SUBSTR(UPPER(this.w_ANCOGNOM), this.w_icf, 1)
              do case
                case this.w_ca $ "BCDFGHJKLMNPQRSTVWXYZ"
                  this.w_SUBCOCO = this.w_SUBCOCO+this.w_ca
                case this.w_ca $ "AEIOU"
                  this.w_SUBCOVO = this.w_SUBCOVO+this.w_ca
              endcase
              this.w_icf = this.w_icf + 1
            enddo
            this.w_SUBCOCO = alltrim(this.w_SUBCOCO)
            this.w_SUBCOVO = alltrim(this.w_SUBCOVO)
            do case
              case LEN(this.w_SUBCOCO)>=3
                this.w_CODFISC = LEFT(this.w_SUBCOCO,3)
              case LEN(this.w_SUBCOCO)=2 AND LEN(this.w_SUBCOVO)>=1
                this.w_CODFISC = LEFT(this.w_SUBCOCO,2)+LEFT(this.w_SUBCOVO,1)
              case LEN(this.w_SUBCOCO)=1 AND LEN(this.w_SUBCOVO)>=1
                this.w_CODFISC = this.w_SUBCOCO+IIF(LEN(this.w_SUBCOVO)>=2,LEFT(this.w_SUBCOVO,2),this.w_SUBCOVO+"X")
              case LEN(this.w_SUBCOCO)=0 AND LEN(this.w_SUBCOVO)>=2 or LEN(this.w_SUBCOCO)>=2 AND LEN(this.w_SUBCOVO)=0
                this.w_CODFISC = LEFT(this.w_SUBCOCO,2)+LEFT(this.w_SUBCOVO,2)+"X"
            endcase
            if this.w_CODFISC <> SUBSTR(this.w_codi,1,3)
              this.w_ERR = 1
            endif
          endif
          this.w_CODFISC = ""
          this.w_icf = 1
          if !EMPTY(NVL(this.w_AN__NOME,""))
            * --- Controllo la parte del Codice Fiscale relativa al Nome
            this.w_SUBCOCO = ""
            this.w_SUBCOVO = ""
            do while this.w_icf <= LEN(ALLTRIM(this.w_AN__NOME)) and LEN(ALLTRIM(this.w_SUBCOCO))<=4
              * --- Conto le consonanti e le vocali del cognome e del nome
              this.w_ca = SUBSTR(UPPER(this.w_AN__NOME), this.w_icf, 1)
              do case
                case this.w_ca $ "BCDFGHJKLMNPQRSTVWXYZ"
                  this.w_SUBCOCO = this.w_SUBCOCO+this.w_ca
                case this.w_ca $ "AEIOU"
                  this.w_SUBCOVO = this.w_SUBCOVO+this.w_ca
              endcase
              this.w_icf = this.w_icf + 1
            enddo
            this.w_SUBCOCO = alltrim(this.w_SUBCOCO)
            this.w_SUBCOVO = alltrim(this.w_SUBCOVO)
            do case
              case LEN(this.w_SUBCOCO)>=4
                this.w_CODFISC = this.w_CODFISC+LEFT(this.w_SUBCOCO,1)+SUBSTR(this.w_SUBCOCO,3, 2)
              case LEN(this.w_SUBCOCO)=3
                this.w_CODFISC = this.w_CODFISC+LEFT(this.w_SUBCOCO,3)
              case LEN(this.w_SUBCOCO)=2 AND LEN(this.w_SUBCOVO)>=1
                this.w_CODFISC = this.w_CODFISC+LEFT(this.w_SUBCOCO,2)+LEFT(this.w_SUBCOVO,1)
              case LEN(this.w_SUBCOCO)=1 AND LEN(this.w_SUBCOVO)>=1
                this.w_CODFISC = this.w_CODFISC+this.w_SUBCOCO
                this.w_CODFISC = this.w_CODFISC+ IIF(LEN(this.w_SUBCOVO)>=2,LEFT(this.w_SUBCOVO,2),this.w_SUBCOVO+"X")
              case LEN(this.w_SUBCOCO)=0 AND LEN(this.w_SUBCOVO)>=2 or LEN(this.w_SUBCOCO)>=2 AND LEN(this.w_SUBCOVO)=0
                this.w_CODFISC = this.w_CODFISC+LEFT(this.w_SUBCOCO,2)+LEFT(this.w_SUBCOVO,2)+"X"
            endcase
            if this.w_CODFISC <> SUBSTR(this.w_codi,4,3)
              if this.w_ERR=0
                this.w_ERR = 1
              endif
            endif
          endif
          this.w_CODFISC = ""
          this.w_icf = 1
          if !empty(nvl(this.w_ANDATNAS,ctod("  -  -  ")))
            * --- Controllo la parte del Codice Fiscale relativa alla Data di Nascita
            this.w_CODFISC = SUBSTR(DTOC(this.w_ANDATNAS),9, 2)
            this.w_valdisp = "65 |66 |67 |68 |69 |72 |76 |77 |80 |82 |83 |84 |"
            this.w_icf = VAL(SUBSTR(DTOC(this.w_ANDATNAS),4, 2))
            this.w_CODFISC = this.w_CODFISC+CHR(VAL(SUBSTR(this.w_VALDISP,this.w_icf*4-3, 2)))
            if this.w_AN_SESSO="F"
              this.w_CODFISC = this.w_CODFISC+ALLTRIM(STR(VAL(SUBSTR(DTOC(this.w_ANDATNAS),1, 2))+40))
            else
              this.w_CODFISC = this.w_CODFISC+ALLTRIM(SUBSTR(DTOC(this.w_ANDATNAS),1, 2))
            endif
            if this.w_CODFISC <> SUBSTR(this.w_codi,7,5)
              if this.w_ERR=0
                this.w_ERR = 1
              endif
            endif
          endif
          this.w_CODFISC = ""
          this.w_icf = 1
          if !empty(nvl(this.w_ANLOCNAS,"")) AND !empty(nvl(this.w_ANPRONAS,""))
            * --- Read from ANAG_CAP
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ANAG_CAP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ANAG_CAP_idx,2],.t.,this.ANAG_CAP_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CPCODFIS"+;
                " from "+i_cTable+" ANAG_CAP where ";
                    +"CPDESLOC = "+cp_ToStrODBC(this.w_ANLOCNAS);
                    +" and CPCODPRO = "+cp_ToStrODBC(this.w_ANPRONAS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CPCODFIS;
                from (i_cTable) where;
                    CPDESLOC = this.w_ANLOCNAS;
                    and CPCODPRO = this.w_ANPRONAS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODFISC = NVL(cp_ToDate(_read_.CPCODFIS),cp_NullValue(_read_.CPCODFIS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Controllo la parte del Codice Fiscale relativa al Luogo di Nascita
            if this.w_CODFISC <> SUBSTR(this.w_codi,12,4) and !empty(nvl(this.w_CODFISC," "))
              if this.w_ERR=0
                this.w_ERR = 1
              endif
            endif
          endif
        endif
        this.w_icf = 1
        if this.w_ERR=0
          * --- Controllo l'ultima lettera del Codice Fiscale
          * --- Dati per la decodifica del codice Fiscale
          this.w_valdisp = "1 |0 |5 |7 |9 |13|15|17|19|21|2 |4 |18|20|11|3 |6 |8 |12|14|16|10|22|25|24|23|"
          * --- Decodifica dei Caratteri
          do while this.w_icf < this.w_lencf
            this.w_ca = SUBSTR(this.w_CODI, this.w_icf, 1)
            this.w_ascca = ASC(this.w_ca) - 65
            if this.w_ascca < 0
              this.w_ascca = this.w_ascca + 17
            endif
            * --- Nella stringa e' presente un carattere non ammesso
            if this.w_ascca < 0 OR this.w_ascca > 25
              if this.w_ERR=0
                this.w_ERR = 1
                EXIT
              else
                EXIT
              endif
            endif
            * --- Assegna la posizione nella stringa
            if (this.w_icf - INT(this.w_icf/2)*2) <> 0
              * --- assegna posizioni Dispari
              this.w_totcf = this.w_totcf + VAL(SUBSTR(this.w_valdisp, this.w_ascca*3+1,2))
            else
              * --- assegna posizioni Pari
              this.w_totcf = this.w_totcf + this.w_ascca
            endif
            this.w_icf = this.w_icf + 1
          enddo
          this.w_riscf = CHR(this.w_totcf - INT(this.w_totcf/26)*26 + 65)
          if this.w_riscf <> RIGHT(this.w_codi, 1)
            if this.w_ERR=0
              this.w_ERR = 1
            endif
          endif
        endif
    endcase
    i_retcode = 'stop'
    i_retval = this.w_ERR
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.w_CODISO="IT" or empty(nvl(this.w_CODISO,""))
        * --- ITALIA
        if this.w_ERR=0 AND (this.w_lencf<>11 OR this.w_lencf<11)
          this.w_ERR = 1
        endif
        if this.w_ERR=0 AND NOT(RIGHT(REPL("0",11)+alltrim(STR(VAL(this.w_CODI),11)),11)=LEFT(this.w_CODI,11))
          this.w_ERR = 1
        endif
        if this.w_ERR=0 AND REPL("0",11)=LEFT(this.w_CODI,11)
          this.w_ERR = 1
        endif
        * --- Se test lunghezza codice OK
        if this.w_ERR=0
          do while this.w_icf<11
            this.w_ca = SUBSTR(this.w_CODI, this.w_icf, 1)
            this.w_ascca = VAL(this.w_ca)
            * --- Assegna la posizione nella stringa
            if (this.w_icf - INT(this.w_icf/2)*2) <> 0
              * --- assegna posizioni Dispari
              this.w_totcf = this.w_totcf + this.w_ascca
            else
              * --- assegna posizioni Pari
              this.w_ascca = this.w_ascca * 2
              if this.w_ascca > 9
                this.w_ascca = this.w_ascca - 9
              endif
              this.w_totcf = this.w_totcf + this.w_ascca
            endif
            this.w_icf = this.w_icf + 1
          enddo
          * --- CONTROLLO FINALE
          * --- Le Partite IVA che iniziano per 8 appartengono ai comuni
          * --- Trattasi di Codici Fiscali che possono essere usati anche come Partita IVA
          * --- Le Partite IVA che iniziano per 9 appartengono ai Condomini
          * --- in tutti i casi vale lo stesso tipo di controllo.
          this.w_riscf = STR(100-this.w_totcf, 3)
          if RIGHT(this.w_riscf,1) <> SUBSTR(this.w_CODI, 11, 1)
            this.w_ERR = 1
          endif
        endif
      case this.w_CODISO="AT"
        * --- AUSTRIA
        if this.w_ERR=0 AND NOT (this.w_lencf=9 AND SUBSTR(this.w_CODI,1,1)="U")
          this.w_ERR = 1
        endif
      case this.w_CODISO="BE" OR this.w_CODISO="DE" OR this.w_CODISO="PT" OR this.w_CODISO="ES" OR this.w_CODISO="EL"
        * --- BELGIO
        * --- GERMANIA
        * --- PORTOGALLO
        * --- GRECIA
        * --- SPAGNA
        if this.w_ERR=0 AND NOT (this.w_lencf=9)
          this.w_ERR = 1
        endif
      case this.w_CODISO="DK" OR this.w_CODISO="FI" OR this.w_CODISO="IE" OR this.w_CODISO="LU"
        * --- DANIMARCA
        * --- FINLANDIA
        * --- IRLANDA
        * --- LUSSEMBURGO
        if this.w_ERR=0 AND NOT (this.w_lencf=8)
          this.w_ERR = 1
        endif
      case this.w_CODISO="FR"
        * --- FRANCIA
        if this.w_ERR=0 AND NOT (this.w_lencf=11)
          this.w_ERR = 1
        endif
      case this.w_CODISO="NL"
        * --- OLANDA
        if this.w_ERR=0 AND NOT (this.w_lencf=12 AND SUBSTR(this.w_CODI,10,1)="B")
          this.w_ERR = 1
        endif
      case this.w_CODISO="SE"
        * --- SVEZIA
        if this.w_ERR=0 AND NOT (this.w_lencf=12)
          this.w_ERR = 1
        endif
      case this.w_CODISO="GB"
        * --- GRAN BRETAGNA
        if this.w_ERR=0 AND NOT (this.w_lencf=12 OR this.w_lencf=9 OR this.w_lencf=5)
          this.w_ERR = 1
        endif
      case this.w_CODISO="SM"
        * --- SAN MARINO
        if this.w_ERR=0 AND NOT (this.w_lencf=5)
          this.w_ERR = 1
        endif
    endcase
  endproc


  proc Init(oParentObject,w_CODI,w_PICF,w_TIPCLF,w_CODICE,w_CODNAZ,w_ANCOGNOM,w_AN__NOME,w_ANLOCNAS,w_ANPRONAS,w_ANDATNAS,w_AN_SESSO,w_ANPERFIS,w_DUPLICATI)
    this.w_CODI=w_CODI
    this.w_PICF=w_PICF
    this.w_TIPCLF=w_TIPCLF
    this.w_CODICE=w_CODICE
    this.w_CODNAZ=w_CODNAZ
    this.w_ANCOGNOM=w_ANCOGNOM
    this.w_AN__NOME=w_AN__NOME
    this.w_ANLOCNAS=w_ANLOCNAS
    this.w_ANPRONAS=w_ANPRONAS
    this.w_ANDATNAS=w_ANDATNAS
    this.w_AN_SESSO=w_AN_SESSO
    this.w_ANPERFIS=w_ANPERFIS
    this.w_DUPLICATI=w_DUPLICATI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='NAZIONI'
    this.cWorkTables[3]='ANAG_CAP'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CODI,w_PICF,w_TIPCLF,w_CODICE,w_CODNAZ,w_ANCOGNOM,w_AN__NOME,w_ANLOCNAS,w_ANPRONAS,w_ANDATNAS,w_AN_SESSO,w_ANPERFIS,w_DUPLICATI"
endproc
