* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bsa                                                        *
*              Salvataggio allegato                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_39]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-11                                                      *
* Last revis.: 2005-05-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bsa",oParentObject)
return(i_retval)

define class tgsut_bsa as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_Ok = .f.
  w_curdir = space(50)
  w_FOLDER = space(254)
  w_TmpPat = space(10)
  w_esiste = .f.
  w_Chiave = space(20)
  * --- WorkFile variables
  GESTFILE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato da GSUT_AGF per salvataggio file Allegato
    this.w_Ok = .T.
    this.w_PADRE = this.oParentObject
    * --- Controllo Check Allegato Principale
    if this.oParentObject.w_GFFLPRIN = "S"
      * --- Select from QUERY\GSUT_BSA
      do vq_exec with 'QUERY\GSUT_BSA',this,'_Curs_QUERY_GSUT_BSA','',.f.,.t.
      if used('_Curs_QUERY_GSUT_BSA')
        select _Curs_QUERY_GSUT_BSA
        locate for 1=1
        do while not(eof())
        ah_ErrorMsg("Impossibile attivare il check allegato principale poich� ne esiste gi� uno","!","")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=MSG_TRANSACTION_ERROR
        i_retcode = 'stop'
        return
          select _Curs_QUERY_GSUT_BSA
          continue
        enddo
        use
      endif
    endif
    if this.oParentObject.w_FLUNIC="S"
      * --- Select from QUERY\GSUT1BSA
      do vq_exec with 'QUERY\GSUT1BSA',this,'_Curs_QUERY_GSUT1BSA','',.f.,.t.
      if used('_Curs_QUERY_GSUT1BSA')
        select _Curs_QUERY_GSUT1BSA
        locate for 1=1
        do while not(eof())
        ah_ErrorMsg("Impossibile creare un allegato con la classe univoca %1%0Esiste gi� un allegato per questo record con la stessa classe","!","", Alltrim(this.oParentObject.w_GFCLAALL) )
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=MSG_TRANSACTION_ERROR
        i_retcode = 'stop'
        return
          select _Curs_QUERY_GSUT1BSA
          continue
        enddo
        use
      endif
    endif
    this.oParentObject.w_COPIA = alltrim(left(this.oParentObject.w_GF__PATH,Rat("\",this.oParentObject.w_GF__PATH)))
    * --- Solo se non si � lanciata la modifica dalla maschera GSUT_KGF
    if this.w_PADRE.cFunction<>"Edit"
      * --- L'utente ha cliccato sul tasto "OK" della anagrafica
      if (.not.directory(alltrim(this.oParentObject.w_COPIA)))
        * --- Creazione directory immagini (LA GENERALE)
        this.w_curdir = sys(5) + sys(2003)
        this.w_FOLDER = left(this.oParentObject.w_COPIA,len(this.oParentObject.w_COPIA)-1)
        w_ERRORE = .F.
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if w_ERRORE
          ah_ErrorMsg("Impossibile creare la cartella %1","!","", this.w_FOLDER)
          this.w_PADRE.ecpQuit()     
          i_retcode = 'stop'
          return
        else
          ah_Msg("Creata nuova cartella %1",.T.,.F.,.F., this.w_FOLDER)
        endif
        * --- Acquisizione della directory corrente
        cd (this.w_curdir)
      endif
      * --- Copia il file catturato nel nuovo file appena costruito
      do case
        case this.oParentObject.w_GFCODPAD = "A"
          * --- Articoli
          this.w_Chiave = Ah_Msgformat("articolo: %1", Alltrim(this.oParentObject.w_GFCODART) )
        case this.oParentObject.w_GFCODPAD = "D"
          * --- Documenti
          this.w_Chiave = ah_Msgformat("documento %1 n. %2 del %3",Alltrim(this.oParentObject.w_TIPDOC), Alltrim(Str(this.oParentObject.w_NUMDOC,15))+IIF(Not Empty(this.oParentObject.w_ALFDOC),"\"+Alltrim(this.oParentObject.w_ALFDOC),""), DTOC(this.oParentObject.w_DATDOC) )
        case this.oParentObject.w_GFCODPAD = "P"
          * --- Primanota
          this.w_Chiave = ah_Msgformat("registrazione %1 n.%2 del %3", Alltrim(this.oParentObject.w_CODCAU), Alltrim(Str(this.oParentObject.w_NUMRER))+IIF(Not Empty(this.oParentObject.w_CODUTE),"\"+Str(this.oParentObject.w_CODUTE,3,0),""), DTOC(this.oParentObject.w_DATREG) )
        case this.oParentObject.w_GFCODPAD = "C"
          * --- Clienti e Fornitori
          if this.oParentObject.w_GFTIPCON = "C"
            this.w_Chiave = ah_Msgformat("cliente: %1", Alltrim(this.oParentObject.w_GFCODCON) )
          else
            this.w_Chiave = ah_Msgformat("fornitore: %1", Alltrim(this.oParentObject.w_GFCODCON) )
          endif
        case this.oParentObject.w_GFCODPAD = "O"
          * --- Categorie Omogenee
          this.w_Chiave = ah_Msgformat("categoria omogenea: %1", Alltrim(this.oParentObject.w_GFCODCAT) )
        case this.oParentObject.w_GFCODPAD = "F"
          * --- Famiglie Articoli
          this.w_Chiave = ah_Msgformat("famiglia: %1", Alltrim(this.oParentObject.w_GFCODFAM) )
        case this.oParentObject.w_GFCODPAD = "M"
          * --- Marchi
          this.w_Chiave = ah_Msgformat("marchio: %1", Alltrim(this.oParentObject.w_GFCODMAR) )
        case this.oParentObject.w_GFCODPAD = "G"
          * --- Gruppi merceologici
          this.w_Chiave = ah_Msgformat("gruppo merceologico: %1", Alltrim(this.oParentObject.w_GFCODGRU) )
      endcase
      if file(this.oParentObject.w_COLLEG) And this.oParentObject.w_GFMODALL = "F"
        * --- Costruzione Path di default che punta alla cartella in cui sono definiti i file associati alle varie chiavi
        this.w_curdir = sys(5) + sys(2003)
        this.w_esiste = .F.
        this.w_esiste = File(Upper(this.oParentObject.w_GF__PATH))
        if Not(this.w_esiste)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_Ok = ah_YesNo("Nella cartella %1 � gi� presente un file con lo stesso nome%0Si desidera sovrascriverlo?","", alltrim(this.oParentObject.w_COPIA) )
          if this.w_Ok
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      else
        if File(this.oParentObject.w_COLLEG)
          ah_ErrorMsg("� stato collegato il file %1 a %2",,"", Alltrim(this.oParentObject.w_COLLEG), alltrim(this.w_Chiave) )
        endif
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Copia direttamente
    copy file (this.oParentObject.w_COLLEG) to (this.oParentObject.w_GF__PATH)
    ah_ErrorMsg("� stato associato il file %1 a %2",,"", Alltrim(this.oParentObject.w_GF__FILE), alltrim(this.w_Chiave))
    if this.oParentObject.w_SCANNER
      Erase(this.oParentObject.w_COLLEG)
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea la cartella specificata nella variabile w_FOLDER
    *     Imposta la variabile w_ERRORE in caso di errore.
    w_ERRORE = .F.
    w_ErrorHandler = on("ERROR")
    on error w_ERRORE = .T.
    md (this.w_FOLDER)
    on error &w_ErrorHandler
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='GESTFILE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_QUERY_GSUT_BSA')
      use in _Curs_QUERY_GSUT_BSA
    endif
    if used('_Curs_QUERY_GSUT1BSA')
      use in _Curs_QUERY_GSUT1BSA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
