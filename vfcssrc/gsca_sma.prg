* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_sma                                                        *
*              Stampa movimenti di analitica                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_76]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-11                                                      *
* Last revis.: 2014-09-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsca_sma",oParentObject))

* --- Class definition
define class tgsca_sma as StdForm
  Top    = 18
  Left   = 43

  * --- Standard Properties
  Width  = 575
  Height = 411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-29"
  HelpContextID=191530345
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=26

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  CENCOST_IDX = 0
  VOC_COST_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsca_sma"
  cComment = "Stampa movimenti di analitica"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPCON = space(1)
  w_azienda = space(10)
  w_TIPO = space(1)
  w_PROVE = space(1)
  w_ORIGINE = space(1)
  o_ORIGINE = space(1)
  w_PROVCONF = space(1)
  w_MRCODCOM = space(15)
  o_MRCODCOM = space(15)
  w_CNDESCAN = space(40)
  w_MRCODICE = space(15)
  o_MRCODICE = space(15)
  w_CCDESPIA = space(40)
  w_MRCODVOC = space(15)
  o_MRCODVOC = space(15)
  w_VCDESCRI = space(40)
  w_PNCODCON = space(15)
  o_PNCODCON = space(15)
  w_ANDESCRI = space(40)
  w_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_NUMER1 = 0
  w_NUMER2 = 0
  w_LIVE1 = 0
  w_LIVE2 = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPOCF = space(1)
  o_TIPOCF = space(1)
  w_CODCON = space(15)
  w_DESCRI = space(40)
  w_TIPCON = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsca_smaPag1","gsca_sma",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPO_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='CENCOST'
    this.cWorkTables[3]='VOC_COST'
    this.cWorkTables[4]='CONTI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON=space(1)
      .w_azienda=space(10)
      .w_TIPO=space(1)
      .w_PROVE=space(1)
      .w_ORIGINE=space(1)
      .w_PROVCONF=space(1)
      .w_MRCODCOM=space(15)
      .w_CNDESCAN=space(40)
      .w_MRCODICE=space(15)
      .w_CCDESPIA=space(40)
      .w_MRCODVOC=space(15)
      .w_VCDESCRI=space(40)
      .w_PNCODCON=space(15)
      .w_ANDESCRI=space(40)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_NUMER1=0
      .w_NUMER2=0
      .w_LIVE1=0
      .w_LIVE2=0
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPOCF=space(1)
      .w_CODCON=space(15)
      .w_DESCRI=space(40)
      .w_TIPCON=space(10)
        .w_TIPCON = 'G'
        .w_azienda = i_CODAZI
        .w_TIPO = 'E'
        .w_PROVE = 'T'
        .w_ORIGINE = 'T'
        .w_PROVCONF = 'N'
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_MRCODCOM))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_MRCODICE))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,11,.f.)
        if not(empty(.w_MRCODVOC))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,12,.f.)
        .w_PNCODCON = IIF(.w_ORIGINE='A', '', .w_PNCODCON)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_PNCODCON))
          .link_1_13('Full')
        endif
          .DoRTCalc(14,14,.f.)
        .w_DATA1 = g_iniese
        .w_DATA2 = g_finese
        .w_NUMER1 = 1
        .w_NUMER2 = 999999999999999
        .w_LIVE1 = 1
        .w_LIVE2 = 99
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .w_OBTEST = i_DATSYS
          .DoRTCalc(22,22,.f.)
        .w_TIPOCF = ' '
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_CODCON))
          .link_1_42('Full')
        endif
          .DoRTCalc(25,25,.f.)
        .w_TIPCON = 'G'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,12,.t.)
        if .o_ORIGINE<>.w_ORIGINE
            .w_PNCODCON = IIF(.w_ORIGINE='A', '', .w_PNCODCON)
          .link_1_13('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        .DoRTCalc(14,20,.t.)
            .w_OBTEST = i_DATSYS
        if .o_TIPOCF<>.w_TIPOCF
          .Calculate_QWKRQHGBHI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(22,26,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
    endwith
  return

  proc Calculate_QWKRQHGBHI()
    with this
          * --- Sbianco intestatario
          .w_CODCON = iif(.w_TIPOCF=' ','',.w_CODCON)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPNCODCON_1_13.enabled = this.oPgFrm.Page1.oPag.oPNCODCON_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCODCON_1_42.enabled = this.oPgFrm.Page1.oPag.oCODCON_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MRCODCOM
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_MRCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_MRCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oMRCODCOM_1_7'),i_cWhere,'GSAR_ACN',"Elenco commesse",'GSCA_MMC.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_MRCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_MRCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_CNDESCAN = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODCOM = space(15)
      endif
      this.w_CNDESCAN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice commessa inesistente oppure obsoleto")
        endif
        this.w_MRCODCOM = space(15)
        this.w_CNDESCAN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MRCODICE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_MRCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_MRCODICE))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODICE)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODICE) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oMRCODICE_1_9'),i_cWhere,'GSCA_ACC',"Centri di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_MRCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_MRCODICE)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODICE = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODICE = space(15)
      endif
      this.w_CCDESPIA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice centro di costo e ricavo inesistente oppure obsoleto")
        endif
        this.w_MRCODICE = space(15)
        this.w_CCDESPIA = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MRCODVOC
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MRCODVOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_MRCODVOC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_MRCODVOC))
          select VCCODICE,VCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MRCODVOC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MRCODVOC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oMRCODVOC_1_11'),i_cWhere,'GSCA_AVC',"Voci di costo/ricavo",'GSCA_MMC.VOC_COST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MRCODVOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_MRCODVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_MRCODVOC)
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MRCODVOC = NVL(_Link_.VCCODICE,space(15))
      this.w_VCDESCRI = NVL(_Link_.VCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MRCODVOC = space(15)
      endif
      this.w_VCDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice voce di costo o ricavo inesistente oppure obsoleto")
        endif
        this.w_MRCODVOC = space(15)
        this.w_VCDESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MRCODVOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNCODCON
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PNCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PNCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PNCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PNCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PNCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PNCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPNCODCON_1_13'),i_cWhere,'GSAR_BZC',"Elenco conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PNCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PNCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODCON = space(15)
      endif
      this.w_ANDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_PNCODCON = space(15)
        this.w_ANDESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPOCF;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPOCF);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_42'),i_cWhere,'GSAR_BZC',"Elenco intestatari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOCF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPOCF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPOCF;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_DESCRI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_3.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVE_1_4.RadioValue()==this.w_PROVE)
      this.oPgFrm.Page1.oPag.oPROVE_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oORIGINE_1_5.RadioValue()==this.w_ORIGINE)
      this.oPgFrm.Page1.oPag.oORIGINE_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVCONF_1_6.RadioValue()==this.w_PROVCONF)
      this.oPgFrm.Page1.oPag.oPROVCONF_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODCOM_1_7.value==this.w_MRCODCOM)
      this.oPgFrm.Page1.oPag.oMRCODCOM_1_7.value=this.w_MRCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCNDESCAN_1_8.value==this.w_CNDESCAN)
      this.oPgFrm.Page1.oPag.oCNDESCAN_1_8.value=this.w_CNDESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODICE_1_9.value==this.w_MRCODICE)
      this.oPgFrm.Page1.oPag.oMRCODICE_1_9.value=this.w_MRCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESPIA_1_10.value==this.w_CCDESPIA)
      this.oPgFrm.Page1.oPag.oCCDESPIA_1_10.value=this.w_CCDESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oMRCODVOC_1_11.value==this.w_MRCODVOC)
      this.oPgFrm.Page1.oPag.oMRCODVOC_1_11.value=this.w_MRCODVOC
    endif
    if not(this.oPgFrm.Page1.oPag.oVCDESCRI_1_12.value==this.w_VCDESCRI)
      this.oPgFrm.Page1.oPag.oVCDESCRI_1_12.value=this.w_VCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCODCON_1_13.value==this.w_PNCODCON)
      this.oPgFrm.Page1.oPag.oPNCODCON_1_13.value=this.w_PNCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_14.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_14.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_15.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_15.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_16.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_16.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMER1_1_17.value==this.w_NUMER1)
      this.oPgFrm.Page1.oPag.oNUMER1_1_17.value=this.w_NUMER1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMER2_1_18.value==this.w_NUMER2)
      this.oPgFrm.Page1.oPag.oNUMER2_1_18.value=this.w_NUMER2
    endif
    if not(this.oPgFrm.Page1.oPag.oLIVE1_1_19.value==this.w_LIVE1)
      this.oPgFrm.Page1.oPag.oLIVE1_1_19.value=this.w_LIVE1
    endif
    if not(this.oPgFrm.Page1.oPag.oLIVE2_1_20.value==this.w_LIVE2)
      this.oPgFrm.Page1.oPag.oLIVE2_1_20.value=this.w_LIVE2
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOCF_1_41.RadioValue()==this.w_TIPOCF)
      this.oPgFrm.Page1.oPag.oTIPOCF_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_42.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_42.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_43.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_43.value=this.w_DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_MRCODCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRCODCOM_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice commessa inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_MRCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRCODICE_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice centro di costo e ricavo inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_MRCODVOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMRCODVOC_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice voce di costo o ricavo inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_ORIGINE<>'A')  and not(empty(.w_PNCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPNCODCON_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(empty(.w_data2) or .w_data1<=.w_data2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_data1) or .w_data1<=.w_data2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(.w_numer1<=.w_numer2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMER1_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il primo numero � maggiore del secondo")
          case   not(.w_numer1<=.w_numer2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMER2_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il primo numero � maggiore del secondo")
          case   not(.w_LIVE1<=.w_LIVE2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLIVE1_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il primo livello � maggiore del secondo")
          case   not(.w_LIVE1<=.w_LIVE2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLIVE2_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il primo livello � maggiore del secondo")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_TIPOCF<>' ')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ORIGINE = this.w_ORIGINE
    this.o_MRCODCOM = this.w_MRCODCOM
    this.o_MRCODICE = this.w_MRCODICE
    this.o_MRCODVOC = this.w_MRCODVOC
    this.o_PNCODCON = this.w_PNCODCON
    this.o_TIPOCF = this.w_TIPOCF
    return

enddefine

* --- Define pages as container
define class tgsca_smaPag1 as StdContainer
  Width  = 571
  height = 411
  stdWidth  = 571
  stdheight = 411
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPO_1_3 as StdCombo with uid="MWWDNPAJRA",rtseq=3,rtrep=.f.,left=106,top=16,width=142,height=21;
    , ToolTipText = "Tipo movimento selezionato";
    , HelpContextID = 186005194;
    , cFormVar="w_TIPO",RowSource=""+"Effettivo,"+"Previsionale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_1_3.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oTIPO_1_3.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_3.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='E',1,;
      iif(this.Parent.oContained.w_TIPO=='P',2,;
      0))
  endfunc


  add object oPROVE_1_4 as StdCombo with uid="SMPWSCFUOH",rtseq=4,rtrep=.f.,left=397,top=15,width=167,height=21;
    , ToolTipText = "Status dei movimenti selezionati";
    , HelpContextID = 113196554;
    , cFormVar="w_PROVE",RowSource=""+"Tutti,"+"Originari,"+"Escluso ripartiti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVE_1_4.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'O',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oPROVE_1_4.GetRadio()
    this.Parent.oContained.w_PROVE = this.RadioValue()
    return .t.
  endfunc

  func oPROVE_1_4.SetRadio()
    this.Parent.oContained.w_PROVE=trim(this.Parent.oContained.w_PROVE)
    this.value = ;
      iif(this.Parent.oContained.w_PROVE=='T',1,;
      iif(this.Parent.oContained.w_PROVE=='O',2,;
      iif(this.Parent.oContained.w_PROVE=='E',3,;
      0)))
  endfunc


  add object oORIGINE_1_5 as StdCombo with uid="GQPTCBFRWN",rtseq=5,rtrep=.f.,left=106,top=47,width=142,height=21;
    , ToolTipText = "Tipo provenienza selezionata";
    , HelpContextID = 124871142;
    , cFormVar="w_ORIGINE",RowSource=""+"Manuali,"+"Prima nota,"+"Documenti,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oORIGINE_1_5.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'P',;
    iif(this.value =3,'C',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oORIGINE_1_5.GetRadio()
    this.Parent.oContained.w_ORIGINE = this.RadioValue()
    return .t.
  endfunc

  func oORIGINE_1_5.SetRadio()
    this.Parent.oContained.w_ORIGINE=trim(this.Parent.oContained.w_ORIGINE)
    this.value = ;
      iif(this.Parent.oContained.w_ORIGINE=='A',1,;
      iif(this.Parent.oContained.w_ORIGINE=='P',2,;
      iif(this.Parent.oContained.w_ORIGINE=='C',3,;
      iif(this.Parent.oContained.w_ORIGINE=='T',4,;
      0))))
  endfunc


  add object oPROVCONF_1_6 as StdCombo with uid="DDYSJFDVCA",rtseq=6,rtrep=.f.,left=397,top=46,width=167,height=21;
    , ToolTipText = "Stato della registrazione (confermato / provvisorio)";
    , HelpContextID = 132070852;
    , cFormVar="w_PROVCONF",RowSource=""+"Confermato,"+"Provvisorio", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVCONF_1_6.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oPROVCONF_1_6.GetRadio()
    this.Parent.oContained.w_PROVCONF = this.RadioValue()
    return .t.
  endfunc

  func oPROVCONF_1_6.SetRadio()
    this.Parent.oContained.w_PROVCONF=trim(this.Parent.oContained.w_PROVCONF)
    this.value = ;
      iif(this.Parent.oContained.w_PROVCONF=='N',1,;
      iif(this.Parent.oContained.w_PROVCONF=='S',2,;
      0))
  endfunc

  add object oMRCODCOM_1_7 as StdField with uid="ZVDQQRFVZI",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MRCODCOM", cQueryName = "MRCODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa inesistente oppure obsoleto",;
    ToolTipText = "Codice commessa",;
    HelpContextID = 64421357,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=106, Top=78, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_MRCODCOM"

  func oMRCODCOM_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODCOM_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODCOM_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oMRCODCOM_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Elenco commesse",'GSCA_MMC.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oMRCODCOM_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_MRCODCOM
     i_obj.ecpSave()
  endproc

  add object oCNDESCAN_1_8 as StdField with uid="XNFZGZZPOA",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CNDESCAN", cQueryName = "CNDESCAN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione commessa",;
    HelpContextID = 49345164,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=247, Top=80, InputMask=replicate('X',40)

  add object oMRCODICE_1_9 as StdField with uid="BRMLSORVOQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MRCODICE", cQueryName = "MRCODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice centro di costo e ricavo inesistente oppure obsoleto",;
    ToolTipText = "Codice centro di costo e ricavo",;
    HelpContextID = 36241931,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=106, Top=109, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_MRCODICE"

  func oMRCODICE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODICE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODICE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oMRCODICE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri di costo/ricavo",'',this.parent.oContained
  endproc
  proc oMRCODICE_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_MRCODICE
     i_obj.ecpSave()
  endproc

  add object oCCDESPIA_1_10 as StdField with uid="VQNWRNINBK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CCDESPIA", cQueryName = "CCDESPIA",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione centro di costo e ricavo",;
    HelpContextID = 168755815,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=247, Top=109, InputMask=replicate('X',40)

  add object oMRCODVOC_1_11 as StdField with uid="BESREYAVYD",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MRCODVOC", cQueryName = "MRCODVOC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice voce di costo o ricavo inesistente oppure obsoleto",;
    ToolTipText = "Codice voce di costo o ricavo",;
    HelpContextID = 14089719,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=106, Top=139, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_MRCODVOC"

  func oMRCODVOC_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oMRCODVOC_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMRCODVOC_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oMRCODVOC_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo/ricavo",'GSCA_MMC.VOC_COST_VZM',this.parent.oContained
  endproc
  proc oMRCODVOC_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_MRCODVOC
     i_obj.ecpSave()
  endproc

  add object oVCDESCRI_1_12 as StdField with uid="TGYAOYEORE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_VCDESCRI", cQueryName = "VCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione voce di costo o ricavo",;
    HelpContextID = 49347681,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=246, Top=140, InputMask=replicate('X',40)

  add object oPNCODCON_1_13 as StdField with uid="YWWDKJFDHV",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PNCODCON", cQueryName = "PNCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Codice conto contabile (editabile solo se provenienza <> manuali (se valorizzato i mov. manuali vengono esclusi))",;
    HelpContextID = 64422332,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=105, Top=171, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PNCODCON"

  func oPNCODCON_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ORIGINE<>'A')
    endwith
   endif
  endfunc

  func oPNCODCON_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPNCODCON_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPNCODCON_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPNCODCON_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco conti",'',this.parent.oContained
  endproc
  proc oPNCODCON_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PNCODCON
     i_obj.ecpSave()
  endproc

  add object oANDESCRI_1_14 as StdField with uid="FYMKGKRTSB",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione conto",;
    HelpContextID = 49345201,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=246, Top=171, InputMask=replicate('X',40)

  add object oDATA1_1_15 as StdField with uid="FLQRTXUTOU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data di registrazione di inizio stampa",;
    HelpContextID = 135528394,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=106, Top=266

  func oDATA1_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data2) or .w_data1<=.w_data2)
    endwith
    return bRes
  endfunc

  add object oDATA2_1_16 as StdField with uid="EQEKVSIWGJ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data di registrazione di fine stampa",;
    HelpContextID = 134479818,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=106, Top=298

  func oDATA2_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data1) or .w_data1<=.w_data2)
    endwith
    return bRes
  endfunc

  add object oNUMER1_1_17 as StdField with uid="OPTLQUFZFI",rtseq=17,rtrep=.f.,;
    cFormVar = "w_NUMER1", cQueryName = "NUMER1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il primo numero � maggiore del secondo",;
    ToolTipText = "Numero di registrazione/documento di inizio stampa",;
    HelpContextID = 83909418,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=278, Top=266, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMER1_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numer1<=.w_numer2)
    endwith
    return bRes
  endfunc

  add object oNUMER2_1_18 as StdField with uid="VVMUSWVKVT",rtseq=18,rtrep=.f.,;
    cFormVar = "w_NUMER2", cQueryName = "NUMER2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il primo numero � maggiore del secondo",;
    ToolTipText = "Numero di registrazione/documento di fine stampa",;
    HelpContextID = 67132202,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=278, Top=298, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMER2_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numer1<=.w_numer2)
    endwith
    return bRes
  endfunc

  add object oLIVE1_1_19 as StdField with uid="OQXEIABASU",rtseq=19,rtrep=.f.,;
    cFormVar = "w_LIVE1", cQueryName = "LIVE1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il primo livello � maggiore del secondo",;
    ToolTipText = "Numero di registrazione di inizio stampa",;
    HelpContextID = 135255882,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=529, Top=266, cSayPict='"99"', cGetPict='"99"'

  func oLIVE1_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LIVE1<=.w_LIVE2)
    endwith
    return bRes
  endfunc

  add object oLIVE2_1_20 as StdField with uid="VNHBAKCPJK",rtseq=20,rtrep=.f.,;
    cFormVar = "w_LIVE2", cQueryName = "LIVE2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il primo livello � maggiore del secondo",;
    ToolTipText = "Numero di registrazione di fine stampa",;
    HelpContextID = 134207306,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=529, Top=298, cSayPict='"99"', cGetPict='"99"'

  func oLIVE2_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_LIVE1<=.w_LIVE2)
    endwith
    return bRes
  endfunc


  add object oObj_1_21 as cp_outputCombo with uid="ZLERXIVPWT",left=106, top=330, width=381,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 13532698


  add object oBtn_1_22 as StdButton with uid="WUGPPMHMYU",left=460, top=358, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 49740762;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        do GSCA_BMA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_data1) and not empty(.w_data2))
      endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="FJBHLKNCRO",left=515, top=358, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 184212922;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oTIPOCF_1_41 as StdCombo with uid="VBIDEFVZDV",value=3,rtseq=23,rtrep=.f.,left=106,top=203,width=142,height=21;
    , ToolTipText = "Tipo cliente/fornitore";
    , HelpContextID = 15087306;
    , cFormVar="w_TIPOCF",RowSource=""+"Cliente,"+"Fornitore,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCF_1_41.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oTIPOCF_1_41.GetRadio()
    this.Parent.oContained.w_TIPOCF = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCF_1_41.SetRadio()
    this.Parent.oContained.w_TIPOCF=trim(this.Parent.oContained.w_TIPOCF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCF=='C',1,;
      iif(this.Parent.oContained.w_TIPOCF=='F',2,;
      iif(this.Parent.oContained.w_TIPOCF=='',3,;
      0)))
  endfunc

  func oTIPOCF_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_1_42('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCON_1_42 as StdField with uid="AVLNXWJTAL",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente oppure obsoleto",;
    ToolTipText = "Codice intestatario",;
    HelpContextID = 137556442,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=106, Top=234, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPOCF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOCF<>' ')
    endwith
   endif
  endfunc

  func oCODCON_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_42.ecpDrop(oSource)
    this.Parent.oContained.link_1_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_42.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPOCF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPOCF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco intestatari",'',this.parent.oContained
  endproc
  proc oCODCON_1_42.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPOCF
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_43 as StdField with uid="HBJNFXNXZX",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione conto",;
    HelpContextID = 218237898,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=246, Top=234, InputMask=replicate('X',40)

  add object oStr_1_24 as StdString with uid="VOXGBJFEJL",Visible=.t., Left=8, Top=270,;
    Alignment=1, Width=97, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="BRSOYYEROT",Visible=.t., Left=8, Top=302,;
    Alignment=1, Width=97, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="CYZLPTYBOV",Visible=.t., Left=190, Top=270,;
    Alignment=1, Width=86, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="CRPMILLJDX",Visible=.t., Left=190, Top=302,;
    Alignment=1, Width=86, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="FUOVNIOIRU",Visible=.t., Left=8, Top=16,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="QSRSLXHABC",Visible=.t., Left=8, Top=47,;
    Alignment=1, Width=97, Height=15,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="TAHTANHAMA",Visible=.t., Left=8, Top=333,;
    Alignment=1, Width=97, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="TYKAWXRWQT",Visible=.t., Left=8, Top=78,;
    Alignment=1, Width=97, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="YDYSBDVAKB",Visible=.t., Left=8, Top=109,;
    Alignment=1, Width=97, Height=15,;
    Caption="C/Costo R.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="TDBQKBCAZT",Visible=.t., Left=3, Top=140,;
    Alignment=1, Width=102, Height=15,;
    Caption="Voce costo/ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="SAOAPDLIFN",Visible=.t., Left=8, Top=171,;
    Alignment=1, Width=97, Height=15,;
    Caption="Conto contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="SXKDUOADIN",Visible=.t., Left=253, Top=16,;
    Alignment=1, Width=141, Height=15,;
    Caption="Natura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="KEBDJHOPVI",Visible=.t., Left=407, Top=268,;
    Alignment=1, Width=120, Height=18,;
    Caption="Da livello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="KTTPIFUKQX",Visible=.t., Left=407, Top=300,;
    Alignment=1, Width=120, Height=18,;
    Caption="A livello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="LZQLOXVHQD",Visible=.t., Left=253, Top=47,;
    Alignment=1, Width=141, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="AYGWAHWKAF",Visible=.t., Left=8, Top=203,;
    Alignment=1, Width=97, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="PUDZCPZYAD",Visible=.t., Left=7, Top=235,;
    Alignment=1, Width=98, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsca_sma','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
