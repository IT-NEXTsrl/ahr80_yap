* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bme                                                        *
*              Gestione accessi                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_258]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-29                                                      *
* Last revis.: 2011-06-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SCELTOP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bme",oParentObject,m.w_SCELTOP)
return(i_retval)

define class tgsut_bme as StdBatch
  * --- Local variables
  w_SCELTOP = space(3)
  w_COUNTER = 0
  w_ERRORE = .f.
  w_PROGRAMMA = space(50)
  MM = space(10)
  w_VIRGOLA = space(1)
  w_GRUPPO = 0
  w_UTENTE = 0
  w_TMPC = space(100)
  w_OK = space(1)
  w_CODREL = space(10)
  w_PRNUMERO = 0
  w_PROGDES = space(50)
  w_PROGRAM = space(20)
  w_PRPARAME = space(15)
  w_ZOOMPAR = .NULL.
  * --- WorkFile variables
  CPPRGSEC_idx=0
  TSMENUVO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invocato dalla maschera GSUT_KSU
    do case
      case this.w_SCELTOP="CAN"
        * --- Eliminazione dei records selezionati dalla tabella 'CPPRGSEC'.
        this.w_COUNTER = 0
        this.w_ERRORE = .F.
        if this.oParentObject.w_PARAM = "U"
          this.MM = this.oParentObject.w_ZOOMENU.cCursor
        else
          this.MM = this.oParentObject.w_ZOOMENG.cCursor
        endif
        Select (this.MM)
        go top
        SCAN FOR XCHK=1
        this.w_COUNTER = this.w_COUNTER+1
        this.w_PROGRAMMA = LOWER(ALLTRIM(PROGRAM1))
        this.w_GRUPPO = iif(this.oParentObject.w_PARAM4="M", -10, 1) * IIF(this.oParentObject.w_UTEGRP="G",NVL(CODE,0),0)
        this.w_UTENTE = iif(this.oParentObject.w_PARAM4="M", -10, 1) * IIF(this.oParentObject.w_UTEGRP="U",NVL(CODE,0),0)
        * --- Try
        local bErr_03B6A678
        bErr_03B6A678=bTrsErr
        this.Try_03B6A678()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_ERRORE = .T.
        endif
        bTrsErr=bTrsErr or bErr_03B6A678
        * --- End
        ENDSCAN
        if this.w_ERRORE
          if this.oParentObject.w_UTEGRP="G"
            ah_ErrorMsg("Errore in aggiornamento dati (%1 gruppo %2)","!","",RTRIM(this.w_PROGRAMMA),ALLTRIM(STR(this.w_GRUPPO,4,0)))
          else
            ah_ErrorMsg("Errore in aggiornamento dati (%1 utente %2)","!","",RTRIM(this.w_PROGRAMMA),ALLTRIM(STR(this.w_UTENTE,4,0)))
          endif
        else
          if this.w_COUNTER=0
            this.w_TMPC = "Non sono state selezionate righe da elaborare"
          else
            this.w_TMPC = "Operazione terminata%0Premere il bottone <ricerca> per aggiornare la lista"
          endif
          ah_ErrorMsg(this.w_TMPC,"!","")
        endif
      case this.w_SCELTOP="MOD"
        * --- Modifica/Inserimento dei permessi di accesso per i programmi selezionati nella maschera.
        *     Ma non appartenenti ad un gruppo specificato nella maschera.
        this.w_COUNTER = 0
        this.w_ERRORE = .F.
        if this.oParentObject.w_PARAM = "U"
          this.MM = this.oParentObject.w_ZOOMENU.cCursor
        else
          this.MM = this.oParentObject.w_ZOOMENG.cCursor
        endif
        Select (this.MM)
        go top
        SCAN FOR XCHK=1 and NOT this.w_ERRORE
        this.w_COUNTER = this.w_COUNTER+1
        this.w_PROGRAMMA = LOWER(ALLTRIM(PROGRAM1) )
        this.w_GRUPPO = iif(this.oParentObject.w_PARAM4="M", -10, 1) * IIF(this.oParentObject.w_UTEGRP="G",NVL(CODE,0),0)
        this.w_UTENTE = iif(this.oParentObject.w_PARAM4="M", -10, 1) * IIF(this.oParentObject.w_UTEGRP="U",NVL(CODE,0),0)
        * --- Prova ad inserire 
        * --- Try
        local bErr_03C1AE40
        bErr_03C1AE40=bTrsErr
        this.Try_03C1AE40()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03C1AE40
        * --- End
        * --- Try
        local bErr_03A50640
        bErr_03A50640=bTrsErr
        this.Try_03A50640()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_ERRORE = .T.
        endif
        bTrsErr=bTrsErr or bErr_03A50640
        * --- End
        ENDSCAN
        if this.w_ERRORE
          if this.oParentObject.w_UTEGRP="G"
            ah_ErrorMsg("Errore in aggiornamento dati (%1 gruppo %2)","!","",RTRIM(this.w_PROGRAMMA),ALLTRIM(STR(this.w_GRUPPO,4,0)))
          else
            ah_ErrorMsg("Errore in aggiornamento dati (%1 utente %2)","!","",RTRIM(this.w_PROGRAMMA),ALLTRIM(STR(this.w_UTENTE,4,0)))
          endif
        else
          if this.w_COUNTER=0
            this.w_TMPC = "Non sono state selezionate righe da elaborare"
          else
            this.w_TMPC = "Operazione terminata%0Premere il bottone <ricerca> per aggiornare la lista"
          endif
          ah_ErrorMsg(this.w_TMPC,"!","")
        endif
      case this.w_SCELTOP = "SEL"
        * --- Selezione o deselezione di tutti i programmi visualizzati nella maschera.
        if this.oParentObject.w_PARAM = "U"
          this.MM = this.oParentObject.w_ZOOMENU.cCursor
        else
          this.MM = this.oParentObject.w_ZOOMENG.cCursor
        endif
        if this.oParentObject.w_SELEZ = "S"
          UPDATE (this.MM) SET XCHK = 1
        else
          UPDATE (this.MM) SET XCHK = 0
        endif
        SELECT( this.MM )
        GO TOP
        if this.oParentObject.w_PARAM = "U"
          this.oParentObject.w_ZOOMENU.refresh()
        else
          this.oParentObject.w_ZOOMENG.refresh()
        endif
      case this.w_SCELTOP="INT"
        this.oParentObject.w_VISUAL = IIF(this.oParentObject.w_UTEGRP="U",-1000,0)
        this.oParentObject.w_CARICA = IIF(this.oParentObject.w_UTEGRP="U",-1000,0)
        this.oParentObject.w_ERASE = IIF(this.oParentObject.w_UTEGRP="U",-1000,0)
        this.oParentObject.w_MODIF = IIF(this.oParentObject.w_UTEGRP="U",-1000,0)
        this.oParentObject.w_SELEZ = "D"
        if this.oParentObject.w_PARAM4="M"
          if this.oParentObject.w_PARAM2="R"
            this.oParentObject.w_ZOOMENG.cCpQueryName = "QUERY\GSUT4KVOM"
          else
            if this.oParentObject.w_GESTIONI="C"
              * --- Riempie il cursore con le gestioni consigliate
              * --- Creazione cursore gestioni consigliate (da passare alla query)
              CREATE CURSOR CONSIGLI (PROGRA C(20))
              * --- Rubrica
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSAG_KRU")
              * --- Ricerca documenti archiviati
              * --- utilizzo
              *     GSUT_BCV,.NULL.
              *     al posto di
              *     GSUT_BCV,.NULL.,GBGBGB,GBGBGB,GBGBGB,V
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSUT_BCV,.NULL.")
              * --- Esplora documenti pratiche
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSUT_KVT")
              * --- Ricerca documenti pratica
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSUT_KFG,AET")
              * --- Nuovo nominativo
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSMO_KWC,.NULL.")
              * --- Elenco pratiche
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSPR_KRP")
              * --- Anteprima pratiche
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSPR_KSC")
              * --- Fascicolo pratica
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSPR_ACN")
              * --- Visione fascicolo
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSMO_KVF")
              * --- Wizard caricamento attivit�
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSAG_KWA")
              * --- La mia agenda
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSAG_KAG,P")
              * --- Agenda di studio
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSAG_KAG,G")
              * --- Confronto agende giornaliere
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSAG_KCP")
              * --- Elenco sintetico
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSAG_KRA,L")
              * --- Timer
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSAG_KTM")
              * --- Elenco prestazioni
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSAG_KRP")
              * --- Inserimento provvisorio delle prestazioni
              INSERT INTO CONSIGLI (PROGRA) VALUES ("GSAG_KPR")
            endif
            this.oParentObject.w_ZOOMENG.cCpQueryName = "QUERY\GSUT_KVOM"
          endif
        else
          if this.oParentObject.w_PARAM2="R"
            this.oParentObject.w_ZOOMENG.cCpQueryName = "QUERY\GSUT4KVO"
          else
            this.oParentObject.w_ZOOMENG.cCpQueryName = "QUERY\GSUT_KVO"
          endif
        endif
        this.oParentObject.NotifyEvent("Carica")
        if this.oParentObject.w_GESTIONI="C"
          * --- Chiusura cursore dellle gestioni consigliate
          if used("CONSIGLI")
            select CONSIGLI
            use
          endif
        endif
      case this.w_SCELTOP="UTINT"
        this.oParentObject.w_USRCODE = this.oParentObject.w_UTESEL
        this.oParentObject.w_PROGNAME = this.oParentObject.w_PRGSEL
        this.oParentObject.w_PROGDES = this.oParentObject.w_DPRGSEL
        this.oParentObject.NotifyEvent("ListaGRP")
      case this.w_SCELTOP="VAR" and this.oParentObject.w_PARAM3<>"GR"
        * --- Vario i campi dello zoom (funzione/descrizione/parametro)
        if this.oParentObject.w_PARAM = "G"
          this.w_ZOOMPAR = this.oParentObject.w_ZOOMENG
        else
          this.w_ZOOMPAR = this.oParentObject.w_ZOOMENU
        endif
        this.w_OK = "N"
        this.w_CODREL = this.oParentObject.w_CODREL
        this.w_PRNUMERO = this.w_zoomPar.getvar("PRNUMERO")
        this.w_PROGDES = this.w_zoomPar.getvar("PROGDES")
        this.w_PROGRAM = this.w_zoomPar.getvar("PROGRAM1")
        this.w_PRPARAME = this.w_zoomPar.getvar("PARAMETRO")
        this.w_ZOOMPAR = .Null.
        * --- Richiamo la maschera per modificare i valori funzione/descrizione/parametro
        do GSUT3KMO with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_OK ="S"
          * --- Rendo definitive le modifiche impostate nella maschera
          * --- Write into TSMENUVO
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TSMENUVO_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TSMENUVO_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TSMENUVO_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PROGDES ="+cp_NullLink(cp_ToStrODBC(this.w_PROGDES),'TSMENUVO','PROGDES');
            +",PROGRAM ="+cp_NullLink(cp_ToStrODBC(this.w_PROGRAM),'TSMENUVO','PROGRAM');
            +",PRPARAME ="+cp_NullLink(cp_ToStrODBC(this.w_PRPARAME),'TSMENUVO','PRPARAME');
                +i_ccchkf ;
            +" where ";
                +"PRRELEAS = "+cp_ToStrODBC(this.w_CODREL);
                +" and PRNUMERO = "+cp_ToStrODBC(this.w_PRNUMERO);
                   )
          else
            update (i_cTable) set;
                PROGDES = this.w_PROGDES;
                ,PROGRAM = this.w_PROGRAM;
                ,PRPARAME = this.w_PRPARAME;
                &i_ccchkf. ;
             where;
                PRRELEAS = this.w_CODREL;
                and PRNUMERO = this.w_PRNUMERO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          ah_ErrorMsg("Operazione terminata%0Premere il bottone <ricerca> per aggiornare la lista","!","")
        endif
    endcase
  endproc
  proc Try_03B6A678()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from CPPRGSEC
    i_nConn=i_TableProp[this.CPPRGSEC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPPRGSEC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PROGNAME = "+cp_ToStrODBC(this.w_PROGRAMMA);
            +" and GRPCODE = "+cp_ToStrODBC(this.w_GRUPPO);
            +" and USRCODE = "+cp_ToStrODBC(this.w_UTENTE);
             )
    else
      delete from (i_cTable) where;
            PROGNAME = this.w_PROGRAMMA;
            and GRPCODE = this.w_GRUPPO;
            and USRCODE = this.w_UTENTE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_03C1AE40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CPPRGSEC
    i_nConn=i_TableProp[this.CPPRGSEC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPPRGSEC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CPPRGSEC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PROGNAME"+",GRPCODE"+",USRCODE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PROGRAMMA),'CPPRGSEC','PROGNAME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GRUPPO),'CPPRGSEC','GRPCODE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTENTE),'CPPRGSEC','USRCODE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PROGNAME',this.w_PROGRAMMA,'GRPCODE',this.w_GRUPPO,'USRCODE',this.w_UTENTE)
      insert into (i_cTable) (PROGNAME,GRPCODE,USRCODE &i_ccchkf. );
         values (;
           this.w_PROGRAMMA;
           ,this.w_GRUPPO;
           ,this.w_UTENTE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03A50640()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CPPRGSEC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CPPRGSEC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPPRGSEC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CPPRGSEC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SEC1 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_VISUAL),'CPPRGSEC','SEC1');
      +",SEC2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CARICA),'CPPRGSEC','SEC2');
      +",SEC3 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MODIF),'CPPRGSEC','SEC3');
      +",SEC4 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ERASE),'CPPRGSEC','SEC4');
          +i_ccchkf ;
      +" where ";
          +"PROGNAME = "+cp_ToStrODBC(this.w_PROGRAMMA);
          +" and GRPCODE = "+cp_ToStrODBC(this.w_GRUPPO);
          +" and USRCODE = "+cp_ToStrODBC(this.w_UTENTE);
             )
    else
      update (i_cTable) set;
          SEC1 = this.oParentObject.w_VISUAL;
          ,SEC2 = this.oParentObject.w_CARICA;
          ,SEC3 = this.oParentObject.w_MODIF;
          ,SEC4 = this.oParentObject.w_ERASE;
          &i_ccchkf. ;
       where;
          PROGNAME = this.w_PROGRAMMA;
          and GRPCODE = this.w_GRUPPO;
          and USRCODE = this.w_UTENTE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_SCELTOP)
    this.w_SCELTOP=w_SCELTOP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CPPRGSEC'
    this.cWorkTables[2]='TSMENUVO'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SCELTOP"
endproc
