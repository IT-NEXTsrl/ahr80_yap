* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bde                                                        *
*              Check descrizione aggiuntiva da primanota                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_35]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-20                                                      *
* Last revis.: 2008-06-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bde",oParentObject)
return(i_retval)

define class tgscg_bde as StdBatch
  * --- Local variables
  w_RECO = 0
  w_OK = .f.
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messaggio Descrizione Aggiuntiva variata in Testata della Primanota (da GSCG_MPN)
    this.w_PADRE = this.oParentObject
    this.w_PADRE.MarkPos()     
    this.w_RECO = RECCOUNT(this.w_PADRE.cTrsName)
    if this.w_RECO>1 OR (this.w_RECO=1 AND NOT EMPTY(this.oParentObject.w_PNCODCON))
      this.w_OK = ah_YesNo("Il campo note � stato modificato. Si desidera aggiornare le descrizioni di riga?")
      if this.w_OK=.T.
        SELECT (this.w_PADRE.cTrsName)
        GO TOP
        * --- Condizione di riga piena
        SCAN FOR ((t_PNIMPDAR<>0 OR t_PNIMPAVE<>0 OR t_PNFLZERO="S") OR i_SRV="A") AND NOT EMPTY(t_PNCODCON) 
        * --- Legge i Dati del Temporaneo
        this.w_PADRE.WorkFromTrs()     
        this.w_PADRE.SaveDependsOn()     
        this.oParentObject.w_PNDESRIG = this.oParentObject.w_PNDESSUP
        * --- Carico il Temporaneo dei Dati
        this.w_PADRE.TrsFromWork()     
        * --- Flag Notifica Riga Variata
        if i_SRV<>"A"
          replace i_SRV with "U"
        endif
        * --- Riposizionamento sul Transitorio effettuando la SaveDependsOn()
        ENDSCAN
      endif
    endif
    this.w_PADRE.RePos(.F.)     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
