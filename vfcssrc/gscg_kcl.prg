* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kcl                                                        *
*              Comunicazione lettere di intento ricevute                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_856]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-11                                                      *
* Last revis.: 2012-03-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gscg_kcl
L_comb1='Persona fisica'
L_comb2='Altri Soggetti'

L_descri1=Ah_MsgFormat( "1) Rappr. legale o negoziale")
L_descri2=Ah_MsgFormat( "2) Rappr. di minore - curatore eredit�")
L_descri3=Ah_MsgFormat( "3) Curatore fallimentare")
L_descri4=Ah_MsgFormat( "4) Commissario liquidatore")
L_descri5=Ah_MsgFormat( "5) Commissario giudiziale")
L_descri6=Ah_MsgFormat( "6) Rappr. fiscale di soggetto non residente")
L_descri7=Ah_MsgFormat( "7) Erede del dichiarante")
L_descri8=Ah_MsgFormat( "8) Liquidatore volontario")
L_descri9=Ah_MsgFormat( "9) Sogg. obbl. per operaz. straord.")
* --- Fine Area Manuale
return(createobject("tgscg_kcl",oParentObject))

* --- Class definition
define class tgscg_kcl as StdForm
  Top    = 2
  Left   = 10

  * --- Standard Properties
  Width  = 704
  Height = 538+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-03-20"
  HelpContextID=169204375
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=98

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  SEDIAZIE_IDX = 0
  TITOLARI_IDX = 0
  DAT_RAPP_IDX = 0
  cPrg = "gscg_kcl"
  cComment = "Comunicazione lettere di intento ricevute"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_RAPFIRM = space(5)
  w_PERAZI = space(1)
  o_PERAZI = space(1)
  w_PERFIS = space(5)
  w_TIPSL = space(2)
  w_SEDILEG = space(5)
  w_TIPFS = space(2)
  w_SEDIFIS = space(5)
  w_CodFisAzi = space(16)
  w_PERAZI2 = space(1)
  w_FOCOGNOME = space(24)
  o_FOCOGNOME = space(24)
  w_FONOME = space(20)
  o_FONOME = space(20)
  w_FODENOMINA = space(60)
  o_FODENOMINA = space(60)
  w_FOCODFIS = space(16)
  o_FOCODFIS = space(16)
  w_FOANNO = space(4)
  o_FOANNO = space(4)
  w_FOMESE = 0
  o_FOMESE = 0
  w_FLAGEURO = space(1)
  w_FLAGCOR = space(1)
  w_FLAGEVE = space(1)
  w_FOPARIVA = space(16)
  w_DATANASC = ctod('  /  /  ')
  w_COMUNE = space(40)
  w_SIGLA = space(2)
  w_SESSO = space(1)
  w_RECOMUNE = space(40)
  w_RESIGLA = space(2)
  w_INDIRIZ = space(35)
  w_CAP = space(5)
  w_SECOMUNE = space(40)
  w_SESIGLA = space(2)
  w_NATGIU = space(2)
  w_SEINDIRI2 = space(35)
  w_SECAP = space(5)
  w_SERCOMUN = space(40)
  w_SERSIGLA = space(2)
  w_SERINDIR = space(35)
  w_ANNO = space(4)
  w_ESERCIZIO = space(4)
  w_VALUTAESE = space(3)
  w_VALUTA = space(1)
  w_CODVAL = space(3)
  w_decimi = 0
  w_TIPCON = space(1)
  w_RITE = space(1)
  w_TIPCLF = space(1)
  w_COGNOME = space(24)
  w_NOME = space(20)
  o_NOME = space(20)
  w_DENOMINA = space(60)
  w_ONLUS = space(1)
  w_SETATT = space(2)
  w_NUMCERTIF2 = 0
  w_SERCAP = space(5)
  w_STAEST = space(24)
  w_CODEST = 0
  w_IDIVAEST = space(20)
  w_SESTAEST = space(24)
  w_SECODEST = 0
  w_SEIDIVES = space(20)
  w_AZLOCAZI = space(50)
  w_AZPROAZI = space(2)
  w_AZINDAZI = space(50)
  w_AZCAPAZI = space(10)
  w_FLEDITOK = .F.
  o_FLEDITOK = .F.
  w_RFCODFIS = space(16)
  w_RFPARIVA = space(11)
  w_RFCODCAR = space(2)
  w_RFCOGNOME = space(24)
  w_RFNOME = space(20)
  w_RFSESSO = space(1)
  w_RFDATANASC = ctod('  /  /  ')
  w_RFCOMNAS = space(40)
  w_RFSIGNAS = space(2)
  w_RFCOMUNE = space(40)
  w_RFSIGLA = space(2)
  w_RFCAP = space(5)
  w_RFINDIRIZ = space(35)
  w_RFTELEFONO = space(12)
  w_FLNOCOR = space(1)
  w_FIRMDICH = space(1)
  w_RFCFINS2 = space(13)
  w_NUMCAF = 0
  w_RFDATIMP = ctod('  /  /  ')
  w_IMPTRTEL = space(1)
  o_IMPTRTEL = space(1)
  w_IMPTRSOG = space(1)
  o_IMPTRSOG = space(1)
  w_FIRMINT = space(1)
  w_TIPFORN = space(2)
  o_TIPFORN = space(2)
  w_CODFISIN = space(16)
  o_CODFISIN = space(16)
  w_DATAPPLI = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_DirName = space(200)
  w_FileName = space(31)
  w_CODAZI = space(5)
  w_PERCIN = space(15)
  w_DESCINI = space(40)
  w_PERCFIN = space(15)
  w_DESCFIN = space(40)
  w_OBTEST = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kclPag1","gscg_kcl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Persone Fisiche / Altri Soggetti")
      .Pages(2).addobject("oPag","tgscg_kclPag2","gscg_kcl",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Rappresentante Firmatario / Firma")
      .Pages(3).addobject("oPag","tgscg_kclPag3","gscg_kcl",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("File telematico")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFOCOGNOME_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='SEDIAZIE'
    this.cWorkTables[6]='TITOLARI'
    this.cWorkTables[7]='DAT_RAPP'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_RAPFIRM=space(5)
      .w_PERAZI=space(1)
      .w_PERFIS=space(5)
      .w_TIPSL=space(2)
      .w_SEDILEG=space(5)
      .w_TIPFS=space(2)
      .w_SEDIFIS=space(5)
      .w_CodFisAzi=space(16)
      .w_PERAZI2=space(1)
      .w_FOCOGNOME=space(24)
      .w_FONOME=space(20)
      .w_FODENOMINA=space(60)
      .w_FOCODFIS=space(16)
      .w_FOANNO=space(4)
      .w_FOMESE=0
      .w_FLAGEURO=space(1)
      .w_FLAGCOR=space(1)
      .w_FLAGEVE=space(1)
      .w_FOPARIVA=space(16)
      .w_DATANASC=ctod("  /  /  ")
      .w_COMUNE=space(40)
      .w_SIGLA=space(2)
      .w_SESSO=space(1)
      .w_RECOMUNE=space(40)
      .w_RESIGLA=space(2)
      .w_INDIRIZ=space(35)
      .w_CAP=space(5)
      .w_SECOMUNE=space(40)
      .w_SESIGLA=space(2)
      .w_NATGIU=space(2)
      .w_SEINDIRI2=space(35)
      .w_SECAP=space(5)
      .w_SERCOMUN=space(40)
      .w_SERSIGLA=space(2)
      .w_SERINDIR=space(35)
      .w_ANNO=space(4)
      .w_ESERCIZIO=space(4)
      .w_VALUTAESE=space(3)
      .w_VALUTA=space(1)
      .w_CODVAL=space(3)
      .w_decimi=0
      .w_TIPCON=space(1)
      .w_RITE=space(1)
      .w_TIPCLF=space(1)
      .w_COGNOME=space(24)
      .w_NOME=space(20)
      .w_DENOMINA=space(60)
      .w_ONLUS=space(1)
      .w_SETATT=space(2)
      .w_NUMCERTIF2=0
      .w_SERCAP=space(5)
      .w_STAEST=space(24)
      .w_CODEST=0
      .w_IDIVAEST=space(20)
      .w_SESTAEST=space(24)
      .w_SECODEST=0
      .w_SEIDIVES=space(20)
      .w_AZLOCAZI=space(50)
      .w_AZPROAZI=space(2)
      .w_AZINDAZI=space(50)
      .w_AZCAPAZI=space(10)
      .w_FLEDITOK=.f.
      .w_RFCODFIS=space(16)
      .w_RFPARIVA=space(11)
      .w_RFCODCAR=space(2)
      .w_RFCOGNOME=space(24)
      .w_RFNOME=space(20)
      .w_RFSESSO=space(1)
      .w_RFDATANASC=ctod("  /  /  ")
      .w_RFCOMNAS=space(40)
      .w_RFSIGNAS=space(2)
      .w_RFCOMUNE=space(40)
      .w_RFSIGLA=space(2)
      .w_RFCAP=space(5)
      .w_RFINDIRIZ=space(35)
      .w_RFTELEFONO=space(12)
      .w_FLNOCOR=space(1)
      .w_FIRMDICH=space(1)
      .w_RFCFINS2=space(13)
      .w_NUMCAF=0
      .w_RFDATIMP=ctod("  /  /  ")
      .w_IMPTRTEL=space(1)
      .w_IMPTRSOG=space(1)
      .w_FIRMINT=space(1)
      .w_TIPFORN=space(2)
      .w_CODFISIN=space(16)
      .w_DATAPPLI=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DirName=space(200)
      .w_FileName=space(31)
      .w_CODAZI=space(5)
      .w_PERCIN=space(15)
      .w_DESCINI=space(40)
      .w_PERCFIN=space(15)
      .w_DESCFIN=space(40)
      .w_OBTEST=ctod("  /  /  ")
        .w_CODAZI = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_RAPFIRM = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_RAPFIRM))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,3,.f.)
        .w_PERFIS = iif(.w_PERAZI='S',i_CODAZI,' ')
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PERFIS))
          .link_1_4('Full')
        endif
        .w_TIPSL = iif(.w_Perazi<>'S','SL','  ')
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_TIPSL))
          .link_1_5('Full')
        endif
        .w_SEDILEG = iif(.w_PERAZI<>'S',i_CODAZI,' ')
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_SEDILEG))
          .link_1_6('Full')
        endif
        .w_TIPFS = iif(.w_Perazi<>'S','DF','  ')
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_TIPFS))
          .link_1_7('Full')
        endif
        .w_SEDIFIS = iif(.w_PERAZI<>'S',i_CODAZI,' ')
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_SEDIFIS))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_PERAZI2 = IsPerFis(.w_FOCOGNOME,.w_FONOME,.w_FODENOMINA)
        .w_FOCOGNOME = iif(.w_Perazi='S',left(.w_FOCOGNOME+SPACE(24),24),space(24))
        .w_FONOME = iif(.w_Perazi='S',left(.w_FONOME+SPACE(20),20),space(20))
        .w_FODENOMINA = iif(.w_Perazi<>'S',left(.w_FODENOMINA+space(60),60),space(60))
        .w_FOCODFIS = iif(empty(.w_FOCODFIS),.w_CodFisAzi,.w_FOCODFIS)
        .w_FOANNO = ALLTRIM(STR(YEAR(i_datsys)))
        .w_FOMESE = IIF(MONTH(i_datsys)>1,MONTH(i_datsys)-1,12)
        .w_FLAGEURO = '1'
        .w_FLAGCOR = '0'
        .w_FLAGEVE = '0'
          .DoRTCalc(20,20,.f.)
        .w_DATANASC = iif(.w_Perazi='S',.w_DATANASC,cp_CharToDate('  -  -  '))
        .w_COMUNE = iif(.w_Perazi='S',left(.w_COMUNE+space(40),40),space(40))
        .w_SIGLA = iif(.w_Perazi='S' ,.w_SIGLA,'  ')
        .w_SESSO = iif(.w_Perazi='S',.w_SESSO,' ')
        .w_RECOMUNE = iif(.w_Perazi='S',left(.w_RECOMUNE+space(40),40),space(40))
        .w_RESIGLA = iif(.w_Perazi='S',.w_RESIGLA,'  ')
        .w_INDIRIZ = iif(.w_Perazi='S',left(.w_INDIRIZ+space(35),35),space(35))
        .w_CAP = iif(.w_Perazi='S',.w_CAP,space(5))
        .w_SECOMUNE = iif(.w_Perazi<>'S',IIF(EMPTY(.w_SECOMUNE),LEFT(.w_AZLOCAZI+SPACE(40),40),left(.w_SECOMUNE+space(40),40)),space(40))
        .w_SESIGLA = iif(.w_Perazi<>'S',IIF(EMPTY(.w_SESIGLA),.w_AZPROAZI,.w_SESIGLA),'  ')
        .w_NATGIU = iif(.w_Perazi<>'S',NATGIU(),'  ')
        .w_SEINDIRI2 = iif(.w_Perazi<>'S',IIF(EMPTY(.w_SEINDIRI2),LEFT(.w_AZINDAZI+SPACE(30),30),left(.w_SEINDIRI2+space(35),35)),space(35))
        .w_SECAP = iif(.w_Perazi<>'S',IIF(EMPTY(.w_SECAP),.w_AZCAPAZI,.w_SECAP),space(5))
        .w_SERCOMUN = iif(.w_Perazi<>'S',left(.w_SERCOMUN+space(40),40),space(40))
        .w_SERSIGLA = iif(.w_Perazi<>'S',.w_SERSIGLA,'  ')
        .w_SERINDIR = iif(.w_Perazi<>'S',left(.w_SERINDIR+space(35),35),space(35))
        .w_ANNO = STR(YEAR(i_DATSYS),4,0)
        .w_ESERCIZIO = calceser(.w_DATFIN)
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_ESERCIZIO))
          .link_1_38('Full')
        endif
          .DoRTCalc(39,39,.f.)
        .w_VALUTA = IIF(.w_VALUTAESE=g_CODLIR,'L','E')
        .w_CODVAL = iif(.w_flageuro='0',g_codlir,g_codeur)
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_CODVAL))
          .link_1_41('Full')
        endif
          .DoRTCalc(42,42,.f.)
        .w_TIPCON = 'F'
          .DoRTCalc(44,48,.f.)
        .w_ONLUS = iif(.w_Perazi<>'S',.w_ONLUS,' ')
        .w_SETATT = iif(.w_Perazi<>'S',.w_SETATT,'  ')
        .w_NUMCERTIF2 = 0
        .w_SERCAP = iif(.w_Perazi<>'S',.w_SERCAP,space(5))
          .DoRTCalc(53,62,.f.)
        .w_FLEDITOK = iif(.w_Perazi<>'S',.w_FLEDITOK,.f.)
        .w_RFCODFIS = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFCODFIS,space(16))
        .w_RFPARIVA = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFPARIVA,space(11))
        .w_RFCODCAR = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFCODCAR,'  ')
        .w_RFCOGNOME = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFCOGNOME,space(24))
        .w_RFNOME = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFNOME,space(20))
        .w_RFSESSO = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFSESSO,' ')
        .w_RFDATANASC = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFDATANASC,cp_CharToDate('  -  -  '))
        .w_RFCOMNAS = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFCOMNAS,space(40))
        .w_RFSIGNAS = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFSIGNAS,'  ')
        .w_RFCOMUNE = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFCOMUNE,space(40))
        .w_RFSIGLA = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFSIGLA,'  ')
        .w_RFCAP = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFCAP,space(5))
        .w_RFINDIRIZ = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFINDIRIZ,space(35))
        .w_RFTELEFONO = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFTELEFONO,space(12))
        .w_FLNOCOR = '0'
        .w_FIRMDICH = '0'
        .w_RFCFINS2 = space(13)
        .w_NUMCAF = 0
        .w_RFDATIMP = cp_CharToDate('  -  -  ')
        .w_IMPTRTEL = iif(.w_IMPTRSOG='1','0',.w_IMPTRTEL)
        .w_IMPTRSOG = iif(.w_IMPTRTEL='1','0',.w_IMPTRSOG)
        .w_FIRMINT = '0'
        .w_TIPFORN = '01'
        .w_CODFISIN = space(16)
        .w_DATAPPLI = 'N'
        .w_DATINI = cp_CharToDate('01-'+STR(.w_FOMESE)+'-'+.w_FOANNO)
        .w_DATFIN = DATE(INT(VAL(ALLTRIM(.w_FOANNO)))+IIF(.w_FOMESE=12,1,0),IIF(.w_FOMESE=12,1,.w_FOMESE+1),1)-1
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_FileName = 'IVD05'+ALLTRIM(.w_FOANNO)+RIGHT('00'+ALLTRIM(STR(.w_FOMESE)),2)+alltrim(.w_FOCODFIS)+'.IVD'
        .w_CODAZI = i_codazi
        .DoRTCalc(94,94,.f.)
        if not(empty(.w_PERCIN))
          .link_3_10('Full')
        endif
        .DoRTCalc(95,96,.f.)
        if not(empty(.w_PERCFIN))
          .link_3_12('Full')
        endif
          .DoRTCalc(97,97,.f.)
        .w_OBTEST = .w_DATINI
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page3.oPag.oBtn_3_7.enabled = this.oPgFrm.Page3.oPag.oBtn_3_7.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_25.enabled = this.oPgFrm.Page3.oPag.oBtn_3_25.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_26.enabled = this.oPgFrm.Page3.oPag.oBtn_3_26.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_27.enabled = this.oPgFrm.Page3.oPag.oBtn_3_27.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_28.enabled = this.oPgFrm.Page3.oPag.oBtn_3_28.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_CODAZI<>.w_CODAZI
          .link_1_1('Full')
        endif
        if .o_CODAZI<>.w_CODAZI
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.t.)
        if .o_PERAZI<>.w_PERAZI
            .w_PERFIS = iif(.w_PERAZI='S',i_CODAZI,' ')
          .link_1_4('Full')
        endif
        if .o_CODAZI<>.w_CODAZI
          .link_1_5('Full')
        endif
        if .o_PERAZI<>.w_PERAZI
            .w_SEDILEG = iif(.w_PERAZI<>'S',i_CODAZI,' ')
          .link_1_6('Full')
        endif
        if .o_CODAZI<>.w_CODAZI
          .link_1_7('Full')
        endif
        if .o_PERAZI<>.w_PERAZI
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.t.)
            .w_PERAZI2 = IsPerFis(.w_FOCOGNOME,.w_FONOME,.w_FODENOMINA)
        if .o_PERAZI<>.w_PERAZI
            .w_FOCOGNOME = iif(.w_Perazi='S',left(.w_FOCOGNOME+SPACE(24),24),space(24))
        endif
        if .o_PERAZI<>.w_PERAZI
            .w_FONOME = iif(.w_Perazi='S',left(.w_FONOME+SPACE(20),20),space(20))
        endif
        if .o_PERAZI<>.w_PERAZI
            .w_FODENOMINA = iif(.w_Perazi<>'S',left(.w_FODENOMINA+space(60),60),space(60))
        endif
        if .o_FOCODFIS<>.w_FOCODFIS
            .w_FOCODFIS = iif(empty(.w_FOCODFIS),.w_CodFisAzi,.w_FOCODFIS)
        endif
        .DoRTCalc(15,20,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_DATANASC = iif(.w_Perazi='S',.w_DATANASC,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_COMUNE = iif(.w_Perazi='S',left(.w_COMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SIGLA = iif(.w_Perazi='S' ,.w_SIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_NOME<>.w_NOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SESSO = iif(.w_Perazi='S',.w_SESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_RECOMUNE = iif(.w_Perazi='S',left(.w_RECOMUNE+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_RESIGLA = iif(.w_Perazi='S',.w_RESIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_INDIRIZ = iif(.w_Perazi='S',left(.w_INDIRIZ+space(35),35),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_CAP = iif(.w_Perazi='S',.w_CAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SECOMUNE = iif(.w_Perazi<>'S',IIF(EMPTY(.w_SECOMUNE),LEFT(.w_AZLOCAZI+SPACE(40),40),left(.w_SECOMUNE+space(40),40)),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SESIGLA = iif(.w_Perazi<>'S',IIF(EMPTY(.w_SESIGLA),.w_AZPROAZI,.w_SESIGLA),'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_NATGIU = iif(.w_Perazi<>'S',NATGIU(),'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SEINDIRI2 = iif(.w_Perazi<>'S',IIF(EMPTY(.w_SEINDIRI2),LEFT(.w_AZINDAZI+SPACE(30),30),left(.w_SEINDIRI2+space(35),35)),space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SECAP = iif(.w_Perazi<>'S',IIF(EMPTY(.w_SECAP),.w_AZCAPAZI,.w_SECAP),space(5))
        endif
        if .o_FONOME<>.w_FONOME.or. .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SERCOMUN = iif(.w_Perazi<>'S',left(.w_SERCOMUN+space(40),40),space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SERSIGLA = iif(.w_Perazi<>'S',.w_SERSIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SERINDIR = iif(.w_Perazi<>'S',left(.w_SERINDIR+space(35),35),space(35))
        endif
        .DoRTCalc(37,37,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_ESERCIZIO = calceser(.w_DATFIN)
          .link_1_38('Full')
        endif
        .DoRTCalc(39,39,.t.)
            .w_VALUTA = IIF(.w_VALUTAESE=g_CODLIR,'L','E')
            .w_CODVAL = iif(.w_flageuro='0',g_codlir,g_codeur)
          .link_1_41('Full')
        .DoRTCalc(42,48,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_ONLUS = iif(.w_Perazi<>'S',.w_ONLUS,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SETATT = iif(.w_Perazi<>'S',.w_SETATT,'  ')
        endif
        .DoRTCalc(51,51,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_SERCAP = iif(.w_Perazi<>'S',.w_SERCAP,space(5))
        endif
        .DoRTCalc(53,62,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA
            .w_FLEDITOK = iif(.w_Perazi<>'S',.w_FLEDITOK,.f.)
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFCODFIS = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFCODFIS,space(16))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFPARIVA = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFPARIVA,space(11))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFCODCAR = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFCODCAR,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFCOGNOME = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFCOGNOME,space(24))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFNOME = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFNOME,space(20))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFSESSO = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFSESSO,' ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFDATANASC = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFDATANASC,cp_CharToDate('  -  -  '))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFCOMNAS = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFCOMNAS,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFSIGNAS = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFSIGNAS,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFCOMUNE = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFCOMUNE,space(40))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFSIGLA = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFSIGLA,'  ')
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFCAP = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFCAP,space(5))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFINDIRIZ = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFINDIRIZ,space(35))
        endif
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_FLEDITOK<>.w_FLEDITOK
            .w_RFTELEFONO = iif(.w_Perazi<>'S' or .w_FLEDITOK,.w_RFTELEFONO,space(12))
        endif
        .DoRTCalc(78,82,.t.)
        if .o_IMPTRSOG<>.w_IMPTRSOG
            .w_IMPTRTEL = iif(.w_IMPTRSOG='1','0',.w_IMPTRTEL)
        endif
        if .o_IMPTRTEL<>.w_IMPTRTEL
            .w_IMPTRSOG = iif(.w_IMPTRTEL='1','0',.w_IMPTRSOG)
        endif
        .DoRTCalc(85,86,.t.)
        if .o_FOCOGNOME<>.w_FOCOGNOME.or. .o_FONOME<>.w_FONOME.or. .o_FODENOMINA<>.w_FODENOMINA.or. .o_TIPFORN<>.w_TIPFORN.or. .o_FOCODFIS<>.w_FOCODFIS
            .w_CODFISIN = space(16)
        endif
        .DoRTCalc(88,88,.t.)
        if .o_FOANNO<>.w_FOANNO.or. .o_FOMESE<>.w_FOMESE
            .w_DATINI = cp_CharToDate('01-'+STR(.w_FOMESE)+'-'+.w_FOANNO)
        endif
        if .o_FOANNO<>.w_FOANNO.or. .o_FOMESE<>.w_FOMESE
            .w_DATFIN = DATE(INT(VAL(ALLTRIM(.w_FOANNO)))+IIF(.w_FOMESE=12,1,0),IIF(.w_FOMESE=12,1,.w_FOMESE+1),1)-1
        endif
        .DoRTCalc(91,91,.t.)
        if .o_FOMESE<>.w_FOMESE.or. .o_FOANNO<>.w_FOANNO
            .w_FileName = 'IVD05'+ALLTRIM(.w_FOANNO)+RIGHT('00'+ALLTRIM(STR(.w_FOMESE)),2)+alltrim(.w_FOCODFIS)+'.IVD'
        endif
            .w_CODAZI = i_codazi
          .link_3_10('Full')
        .DoRTCalc(95,95,.t.)
          .link_3_12('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(97,98,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.enabled = this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.mCond()
    this.oPgFrm.Page1.oPag.oFONOME_1_12.enabled = this.oPgFrm.Page1.oPag.oFONOME_1_12.mCond()
    this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.enabled = this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.mCond()
    this.oPgFrm.Page1.oPag.oDATANASC_1_21.enabled = this.oPgFrm.Page1.oPag.oDATANASC_1_21.mCond()
    this.oPgFrm.Page1.oPag.oCOMUNE_1_22.enabled = this.oPgFrm.Page1.oPag.oCOMUNE_1_22.mCond()
    this.oPgFrm.Page1.oPag.oSIGLA_1_23.enabled = this.oPgFrm.Page1.oPag.oSIGLA_1_23.mCond()
    this.oPgFrm.Page1.oPag.oSESSO_1_24.enabled = this.oPgFrm.Page1.oPag.oSESSO_1_24.mCond()
    this.oPgFrm.Page1.oPag.oRECOMUNE_1_25.enabled = this.oPgFrm.Page1.oPag.oRECOMUNE_1_25.mCond()
    this.oPgFrm.Page1.oPag.oRESIGLA_1_26.enabled = this.oPgFrm.Page1.oPag.oRESIGLA_1_26.mCond()
    this.oPgFrm.Page1.oPag.oINDIRIZ_1_27.enabled = this.oPgFrm.Page1.oPag.oINDIRIZ_1_27.mCond()
    this.oPgFrm.Page1.oPag.oCAP_1_28.enabled = this.oPgFrm.Page1.oPag.oCAP_1_28.mCond()
    this.oPgFrm.Page1.oPag.oSECOMUNE_1_29.enabled = this.oPgFrm.Page1.oPag.oSECOMUNE_1_29.mCond()
    this.oPgFrm.Page1.oPag.oSESIGLA_1_30.enabled = this.oPgFrm.Page1.oPag.oSESIGLA_1_30.mCond()
    this.oPgFrm.Page1.oPag.oNATGIU_1_31.enabled = this.oPgFrm.Page1.oPag.oNATGIU_1_31.mCond()
    this.oPgFrm.Page1.oPag.oSEINDIRI2_1_32.enabled = this.oPgFrm.Page1.oPag.oSEINDIRI2_1_32.mCond()
    this.oPgFrm.Page1.oPag.oSECAP_1_33.enabled = this.oPgFrm.Page1.oPag.oSECAP_1_33.mCond()
    this.oPgFrm.Page1.oPag.oSERCOMUN_1_34.enabled = this.oPgFrm.Page1.oPag.oSERCOMUN_1_34.mCond()
    this.oPgFrm.Page1.oPag.oSERSIGLA_1_35.enabled = this.oPgFrm.Page1.oPag.oSERSIGLA_1_35.mCond()
    this.oPgFrm.Page1.oPag.oSERINDIR_1_36.enabled = this.oPgFrm.Page1.oPag.oSERINDIR_1_36.mCond()
    this.oPgFrm.Page1.oPag.oSERCAP_1_83.enabled = this.oPgFrm.Page1.oPag.oSERCAP_1_83.mCond()
    this.oPgFrm.Page1.oPag.oSTAEST_1_84.enabled = this.oPgFrm.Page1.oPag.oSTAEST_1_84.mCond()
    this.oPgFrm.Page1.oPag.oCODEST_1_87.enabled = this.oPgFrm.Page1.oPag.oCODEST_1_87.mCond()
    this.oPgFrm.Page1.oPag.oIDIVAEST_1_89.enabled = this.oPgFrm.Page1.oPag.oIDIVAEST_1_89.mCond()
    this.oPgFrm.Page1.oPag.oSESTAEST_1_91.enabled = this.oPgFrm.Page1.oPag.oSESTAEST_1_91.mCond()
    this.oPgFrm.Page1.oPag.oSECODEST_1_93.enabled = this.oPgFrm.Page1.oPag.oSECODEST_1_93.mCond()
    this.oPgFrm.Page1.oPag.oSEIDIVES_1_95.enabled = this.oPgFrm.Page1.oPag.oSEIDIVES_1_95.mCond()
    this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.enabled = this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.mCond()
    this.oPgFrm.Page2.oPag.oRFPARIVA_2_3.enabled = this.oPgFrm.Page2.oPag.oRFPARIVA_2_3.mCond()
    this.oPgFrm.Page2.oPag.oRFCODCAR_2_4.enabled = this.oPgFrm.Page2.oPag.oRFCODCAR_2_4.mCond()
    this.oPgFrm.Page2.oPag.oRFCOGNOME_2_5.enabled = this.oPgFrm.Page2.oPag.oRFCOGNOME_2_5.mCond()
    this.oPgFrm.Page2.oPag.oRFNOME_2_6.enabled = this.oPgFrm.Page2.oPag.oRFNOME_2_6.mCond()
    this.oPgFrm.Page2.oPag.oRFSESSO_2_7.enabled = this.oPgFrm.Page2.oPag.oRFSESSO_2_7.mCond()
    this.oPgFrm.Page2.oPag.oRFDATANASC_2_8.enabled = this.oPgFrm.Page2.oPag.oRFDATANASC_2_8.mCond()
    this.oPgFrm.Page2.oPag.oRFCOMNAS_2_9.enabled = this.oPgFrm.Page2.oPag.oRFCOMNAS_2_9.mCond()
    this.oPgFrm.Page2.oPag.oRFSIGNAS_2_10.enabled = this.oPgFrm.Page2.oPag.oRFSIGNAS_2_10.mCond()
    this.oPgFrm.Page2.oPag.oRFCOMUNE_2_11.enabled = this.oPgFrm.Page2.oPag.oRFCOMUNE_2_11.mCond()
    this.oPgFrm.Page2.oPag.oRFSIGLA_2_12.enabled = this.oPgFrm.Page2.oPag.oRFSIGLA_2_12.mCond()
    this.oPgFrm.Page2.oPag.oRFCAP_2_13.enabled = this.oPgFrm.Page2.oPag.oRFCAP_2_13.mCond()
    this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_14.enabled = this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_14.mCond()
    this.oPgFrm.Page2.oPag.oRFTELEFONO_2_15.enabled = this.oPgFrm.Page2.oPag.oRFTELEFONO_2_15.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCOFAZI,AZRAGAZI,AZPERAZI,AZIVACAR,AZIVACOF,AZPIVAZI,AZLOCAZI,AZPROAZI,AZINDAZI,AZCAPAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZCOFAZI,AZRAGAZI,AZPERAZI,AZIVACAR,AZIVACOF,AZPIVAZI,AZLOCAZI,AZPROAZI,AZINDAZI,AZCAPAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_CodFisAzi = NVL(_Link_.AZCOFAZI,space(16))
      this.w_FODENOMINA = NVL(_Link_.AZRAGAZI,space(60))
      this.w_PERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_RFCODCAR = NVL(_Link_.AZIVACAR,space(2))
      this.w_RFCODFIS = NVL(_Link_.AZIVACOF,space(16))
      this.w_FOPARIVA = NVL(_Link_.AZPIVAZI,space(16))
      this.w_AZLOCAZI = NVL(_Link_.AZLOCAZI,space(50))
      this.w_AZPROAZI = NVL(_Link_.AZPROAZI,space(2))
      this.w_AZINDAZI = NVL(_Link_.AZINDAZI,space(50))
      this.w_AZCAPAZI = NVL(_Link_.AZCAPAZI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CodFisAzi = space(16)
      this.w_FODENOMINA = space(60)
      this.w_PERAZI = space(1)
      this.w_RFCODCAR = space(2)
      this.w_RFCODFIS = space(16)
      this.w_FOPARIVA = space(16)
      this.w_AZLOCAZI = space(50)
      this.w_AZPROAZI = space(2)
      this.w_AZINDAZI = space(50)
      this.w_AZCAPAZI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAPFIRM
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DAT_RAPP_IDX,3]
    i_lTable = "DAT_RAPP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2], .t., this.DAT_RAPP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAPFIRM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAPFIRM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RFCODAZI,RFCODFIS,RFCODCAR,RF__NOME,RFCOGNOM,RF_SESSO,RFDATNAS,RFCOMNAS,RFPRONAS,RFCOMRES,RFCAPRES,RFPRORES,RFINDRES,RFTELEFO";
                   +" from "+i_cTable+" "+i_lTable+" where RFCODAZI="+cp_ToStrODBC(this.w_RAPFIRM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RFCODAZI',this.w_RAPFIRM)
            select RFCODAZI,RFCODFIS,RFCODCAR,RF__NOME,RFCOGNOM,RF_SESSO,RFDATNAS,RFCOMNAS,RFPRONAS,RFCOMRES,RFCAPRES,RFPRORES,RFINDRES,RFTELEFO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAPFIRM = NVL(_Link_.RFCODAZI,space(5))
      this.w_RFCODFIS = NVL(_Link_.RFCODFIS,space(16))
      this.w_RFCODCAR = NVL(_Link_.RFCODCAR,space(2))
      this.w_RFNOME = NVL(_Link_.RF__NOME,space(20))
      this.w_RFCOGNOME = NVL(_Link_.RFCOGNOM,space(24))
      this.w_RFSESSO = NVL(_Link_.RF_SESSO,space(1))
      this.w_RFDATANASC = NVL(cp_ToDate(_Link_.RFDATNAS),ctod("  /  /  "))
      this.w_RFCOMNAS = NVL(_Link_.RFCOMNAS,space(40))
      this.w_RFSIGNAS = NVL(_Link_.RFPRONAS,space(2))
      this.w_RFCOMUNE = NVL(_Link_.RFCOMRES,space(40))
      this.w_RFCAP = NVL(_Link_.RFCAPRES,space(5))
      this.w_RFSIGLA = NVL(_Link_.RFPRORES,space(2))
      this.w_RFINDIRIZ = NVL(_Link_.RFINDRES,space(35))
      this.w_RFTELEFONO = NVL(_Link_.RFTELEFO,space(12))
    else
      if i_cCtrl<>'Load'
        this.w_RAPFIRM = space(5)
      endif
      this.w_RFCODFIS = space(16)
      this.w_RFCODCAR = space(2)
      this.w_RFNOME = space(20)
      this.w_RFCOGNOME = space(24)
      this.w_RFSESSO = space(1)
      this.w_RFDATANASC = ctod("  /  /  ")
      this.w_RFCOMNAS = space(40)
      this.w_RFSIGNAS = space(2)
      this.w_RFCOMUNE = space(40)
      this.w_RFCAP = space(5)
      this.w_RFSIGLA = space(2)
      this.w_RFINDIRIZ = space(35)
      this.w_RFTELEFONO = space(12)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])+'\'+cp_ToStr(_Link_.RFCODAZI,1)
      cp_ShowWarn(i_cKey,this.DAT_RAPP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAPFIRM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERFIS
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERFIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERFIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTDATNAS,TT_SESSO,TTLUONAS,TTPRONAS,TTLOCTIT,TTPROTIT,TTINDIRI,TTCAPTIT,TTCOGTIT,TTNOMTIT";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_PERFIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_PERFIS)
            select TTCODAZI,TTDATNAS,TT_SESSO,TTLUONAS,TTPRONAS,TTLOCTIT,TTPROTIT,TTINDIRI,TTCAPTIT,TTCOGTIT,TTNOMTIT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERFIS = NVL(_Link_.TTCODAZI,space(5))
      this.w_DATANASC = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_SESSO = NVL(_Link_.TT_SESSO,space(1))
      this.w_COMUNE = NVL(_Link_.TTLUONAS,space(40))
      this.w_SIGLA = NVL(_Link_.TTPRONAS,space(2))
      this.w_RECOMUNE = NVL(_Link_.TTLOCTIT,space(40))
      this.w_RESIGLA = NVL(_Link_.TTPROTIT,space(2))
      this.w_INDIRIZ = NVL(_Link_.TTINDIRI,space(35))
      this.w_CAP = NVL(_Link_.TTCAPTIT,space(5))
      this.w_FOCOGNOME = NVL(_Link_.TTCOGTIT,space(24))
      this.w_FONOME = NVL(_Link_.TTNOMTIT,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_PERFIS = space(5)
      endif
      this.w_DATANASC = ctod("  /  /  ")
      this.w_SESSO = space(1)
      this.w_COMUNE = space(40)
      this.w_SIGLA = space(2)
      this.w_RECOMUNE = space(40)
      this.w_RESIGLA = space(2)
      this.w_INDIRIZ = space(35)
      this.w_CAP = space(5)
      this.w_FOCOGNOME = space(24)
      this.w_FONOME = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERFIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPSL
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPSL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPSL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SETIPRIF,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where SETIPRIF="+cp_ToStrODBC(this.w_TIPSL);
                   +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',this.w_CODAZI;
                       ,'SETIPRIF',this.w_TIPSL)
            select SECODAZI,SETIPRIF,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPSL = NVL(_Link_.SETIPRIF,space(2))
      this.w_SERCOMUN = NVL(_Link_.SELOCALI,space(40))
      this.w_SERSIGLA = NVL(_Link_.SEPROVIN,space(2))
      this.w_SERINDIR = NVL(_Link_.SEINDIRI,space(35))
      this.w_SERCAP = NVL(_Link_.SE___CAP,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TIPSL = space(2)
      endif
      this.w_SERCOMUN = space(40)
      this.w_SERSIGLA = space(2)
      this.w_SERINDIR = space(35)
      this.w_SERCAP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SECODAZI,1)+'\'+cp_ToStr(_Link_.SETIPRIF,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPSL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDILEG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDILEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDILEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDILEG);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPSL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPSL;
                       ,'SECODAZI',this.w_SEDILEG)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDILEG = NVL(_Link_.SECODAZI,space(5))
      this.w_SECOMUNE = NVL(_Link_.SELOCALI,space(40))
      this.w_SESIGLA = NVL(_Link_.SEPROVIN,space(2))
      this.w_SEINDIRI2 = NVL(_Link_.SEINDIRI,space(35))
      this.w_SECAP = NVL(_Link_.SE___CAP,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SEDILEG = space(5)
      endif
      this.w_SECOMUNE = space(40)
      this.w_SESIGLA = space(2)
      this.w_SEINDIRI2 = space(35)
      this.w_SECAP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDILEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPFS
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPFS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPFS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SETIPRIF,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where SETIPRIF="+cp_ToStrODBC(this.w_TIPFS);
                   +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',this.w_CODAZI;
                       ,'SETIPRIF',this.w_TIPFS)
            select SECODAZI,SETIPRIF,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPFS = NVL(_Link_.SETIPRIF,space(2))
      this.w_SERCOMUN = NVL(_Link_.SELOCALI,space(40))
      this.w_SERSIGLA = NVL(_Link_.SEPROVIN,space(2))
      this.w_SERINDIR = NVL(_Link_.SEINDIRI,space(35))
      this.w_SERCAP = NVL(_Link_.SE___CAP,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_TIPFS = space(2)
      endif
      this.w_SERCOMUN = space(40)
      this.w_SERSIGLA = space(2)
      this.w_SERINDIR = space(35)
      this.w_SERCAP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SECODAZI,1)+'\'+cp_ToStr(_Link_.SETIPRIF,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPFS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDIFIS
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDIFIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDIFIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDIFIS);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPFS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPFS;
                       ,'SECODAZI',this.w_SEDIFIS)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN,SEINDIRI,SE___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDIFIS = NVL(_Link_.SECODAZI,space(5))
      this.w_SERCOMUN = NVL(_Link_.SELOCALI,space(40))
      this.w_SERSIGLA = NVL(_Link_.SEPROVIN,space(2))
      this.w_SERINDIR = NVL(_Link_.SEINDIRI,space(35))
      this.w_SERCAP = NVL(_Link_.SE___CAP,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_SEDIFIS = space(5)
      endif
      this.w_SERCOMUN = space(40)
      this.w_SERSIGLA = space(2)
      this.w_SERINDIR = space(35)
      this.w_SERCAP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDIFIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCIZIO
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCIZIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCIZIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCIZIO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERCIZIO)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCIZIO = NVL(_Link_.ESCODESE,space(4))
      this.w_VALUTAESE = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCIZIO = space(4)
      endif
      this.w_VALUTAESE = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCIZIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_decimi = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCIN
  func Link_3_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCINI = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITE = NVL(_Link_.ANRITENU,space(1))
      this.w_TIPCLF = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCIN = space(15)
      endif
      this.w_DESCINI = space(40)
      this.w_RITE = space(1)
      this.w_TIPCLF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endif
        this.w_PERCIN = space(15)
        this.w_DESCINI = space(40)
        this.w_RITE = space(1)
        this.w_TIPCLF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERCFIN
  func Link_3_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PERCFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PERCFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANRITENU,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERCFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_RITE = NVL(_Link_.ANRITENU,space(1))
      this.w_TIPCLF = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PERCFIN = space(15)
      endif
      this.w_DESCFIN = space(40)
      this.w_RITE = space(1)
      this.w_TIPCLF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endif
        this.w_PERCFIN = space(15)
        this.w_DESCFIN = space(40)
        this.w_RITE = space(1)
        this.w_TIPCLF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.value==this.w_FOCOGNOME)
      this.oPgFrm.Page1.oPag.oFOCOGNOME_1_11.value=this.w_FOCOGNOME
    endif
    if not(this.oPgFrm.Page1.oPag.oFONOME_1_12.value==this.w_FONOME)
      this.oPgFrm.Page1.oPag.oFONOME_1_12.value=this.w_FONOME
    endif
    if not(this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.value==this.w_FODENOMINA)
      this.oPgFrm.Page1.oPag.oFODENOMINA_1_13.value=this.w_FODENOMINA
    endif
    if not(this.oPgFrm.Page1.oPag.oFOCODFIS_1_14.value==this.w_FOCODFIS)
      this.oPgFrm.Page1.oPag.oFOCODFIS_1_14.value=this.w_FOCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oFOANNO_1_15.value==this.w_FOANNO)
      this.oPgFrm.Page1.oPag.oFOANNO_1_15.value=this.w_FOANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oFOMESE_1_16.RadioValue()==this.w_FOMESE)
      this.oPgFrm.Page1.oPag.oFOMESE_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGCOR_1_18.RadioValue()==this.w_FLAGCOR)
      this.oPgFrm.Page1.oPag.oFLAGCOR_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGEVE_1_19.RadioValue()==this.w_FLAGEVE)
      this.oPgFrm.Page1.oPag.oFLAGEVE_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFOPARIVA_1_20.value==this.w_FOPARIVA)
      this.oPgFrm.Page1.oPag.oFOPARIVA_1_20.value=this.w_FOPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDATANASC_1_21.value==this.w_DATANASC)
      this.oPgFrm.Page1.oPag.oDATANASC_1_21.value=this.w_DATANASC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMUNE_1_22.value==this.w_COMUNE)
      this.oPgFrm.Page1.oPag.oCOMUNE_1_22.value=this.w_COMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oSIGLA_1_23.value==this.w_SIGLA)
      this.oPgFrm.Page1.oPag.oSIGLA_1_23.value=this.w_SIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSESSO_1_24.RadioValue()==this.w_SESSO)
      this.oPgFrm.Page1.oPag.oSESSO_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRECOMUNE_1_25.value==this.w_RECOMUNE)
      this.oPgFrm.Page1.oPag.oRECOMUNE_1_25.value=this.w_RECOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oRESIGLA_1_26.value==this.w_RESIGLA)
      this.oPgFrm.Page1.oPag.oRESIGLA_1_26.value=this.w_RESIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oINDIRIZ_1_27.value==this.w_INDIRIZ)
      this.oPgFrm.Page1.oPag.oINDIRIZ_1_27.value=this.w_INDIRIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCAP_1_28.value==this.w_CAP)
      this.oPgFrm.Page1.oPag.oCAP_1_28.value=this.w_CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oSECOMUNE_1_29.value==this.w_SECOMUNE)
      this.oPgFrm.Page1.oPag.oSECOMUNE_1_29.value=this.w_SECOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oSESIGLA_1_30.value==this.w_SESIGLA)
      this.oPgFrm.Page1.oPag.oSESIGLA_1_30.value=this.w_SESIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oNATGIU_1_31.value==this.w_NATGIU)
      this.oPgFrm.Page1.oPag.oNATGIU_1_31.value=this.w_NATGIU
    endif
    if not(this.oPgFrm.Page1.oPag.oSEINDIRI2_1_32.value==this.w_SEINDIRI2)
      this.oPgFrm.Page1.oPag.oSEINDIRI2_1_32.value=this.w_SEINDIRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oSECAP_1_33.value==this.w_SECAP)
      this.oPgFrm.Page1.oPag.oSECAP_1_33.value=this.w_SECAP
    endif
    if not(this.oPgFrm.Page1.oPag.oSERCOMUN_1_34.value==this.w_SERCOMUN)
      this.oPgFrm.Page1.oPag.oSERCOMUN_1_34.value=this.w_SERCOMUN
    endif
    if not(this.oPgFrm.Page1.oPag.oSERSIGLA_1_35.value==this.w_SERSIGLA)
      this.oPgFrm.Page1.oPag.oSERSIGLA_1_35.value=this.w_SERSIGLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSERINDIR_1_36.value==this.w_SERINDIR)
      this.oPgFrm.Page1.oPag.oSERINDIR_1_36.value=this.w_SERINDIR
    endif
    if not(this.oPgFrm.Page1.oPag.oSERCAP_1_83.value==this.w_SERCAP)
      this.oPgFrm.Page1.oPag.oSERCAP_1_83.value=this.w_SERCAP
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAEST_1_84.value==this.w_STAEST)
      this.oPgFrm.Page1.oPag.oSTAEST_1_84.value=this.w_STAEST
    endif
    if not(this.oPgFrm.Page1.oPag.oCODEST_1_87.value==this.w_CODEST)
      this.oPgFrm.Page1.oPag.oCODEST_1_87.value=this.w_CODEST
    endif
    if not(this.oPgFrm.Page1.oPag.oIDIVAEST_1_89.value==this.w_IDIVAEST)
      this.oPgFrm.Page1.oPag.oIDIVAEST_1_89.value=this.w_IDIVAEST
    endif
    if not(this.oPgFrm.Page1.oPag.oSESTAEST_1_91.value==this.w_SESTAEST)
      this.oPgFrm.Page1.oPag.oSESTAEST_1_91.value=this.w_SESTAEST
    endif
    if not(this.oPgFrm.Page1.oPag.oSECODEST_1_93.value==this.w_SECODEST)
      this.oPgFrm.Page1.oPag.oSECODEST_1_93.value=this.w_SECODEST
    endif
    if not(this.oPgFrm.Page1.oPag.oSEIDIVES_1_95.value==this.w_SEIDIVES)
      this.oPgFrm.Page1.oPag.oSEIDIVES_1_95.value=this.w_SEIDIVES
    endif
    if not(this.oPgFrm.Page2.oPag.oFLEDITOK_2_1.RadioValue()==this.w_FLEDITOK)
      this.oPgFrm.Page2.oPag.oFLEDITOK_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.value==this.w_RFCODFIS)
      this.oPgFrm.Page2.oPag.oRFCODFIS_2_2.value=this.w_RFCODFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFPARIVA_2_3.value==this.w_RFPARIVA)
      this.oPgFrm.Page2.oPag.oRFPARIVA_2_3.value=this.w_RFPARIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCODCAR_2_4.RadioValue()==this.w_RFCODCAR)
      this.oPgFrm.Page2.oPag.oRFCODCAR_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOGNOME_2_5.value==this.w_RFCOGNOME)
      this.oPgFrm.Page2.oPag.oRFCOGNOME_2_5.value=this.w_RFCOGNOME
    endif
    if not(this.oPgFrm.Page2.oPag.oRFNOME_2_6.value==this.w_RFNOME)
      this.oPgFrm.Page2.oPag.oRFNOME_2_6.value=this.w_RFNOME
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSESSO_2_7.RadioValue()==this.w_RFSESSO)
      this.oPgFrm.Page2.oPag.oRFSESSO_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDATANASC_2_8.value==this.w_RFDATANASC)
      this.oPgFrm.Page2.oPag.oRFDATANASC_2_8.value=this.w_RFDATANASC
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOMNAS_2_9.value==this.w_RFCOMNAS)
      this.oPgFrm.Page2.oPag.oRFCOMNAS_2_9.value=this.w_RFCOMNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSIGNAS_2_10.value==this.w_RFSIGNAS)
      this.oPgFrm.Page2.oPag.oRFSIGNAS_2_10.value=this.w_RFSIGNAS
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCOMUNE_2_11.value==this.w_RFCOMUNE)
      this.oPgFrm.Page2.oPag.oRFCOMUNE_2_11.value=this.w_RFCOMUNE
    endif
    if not(this.oPgFrm.Page2.oPag.oRFSIGLA_2_12.value==this.w_RFSIGLA)
      this.oPgFrm.Page2.oPag.oRFSIGLA_2_12.value=this.w_RFSIGLA
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCAP_2_13.value==this.w_RFCAP)
      this.oPgFrm.Page2.oPag.oRFCAP_2_13.value=this.w_RFCAP
    endif
    if not(this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_14.value==this.w_RFINDIRIZ)
      this.oPgFrm.Page2.oPag.oRFINDIRIZ_2_14.value=this.w_RFINDIRIZ
    endif
    if not(this.oPgFrm.Page2.oPag.oRFTELEFONO_2_15.value==this.w_RFTELEFONO)
      this.oPgFrm.Page2.oPag.oRFTELEFONO_2_15.value=this.w_RFTELEFONO
    endif
    if not(this.oPgFrm.Page2.oPag.oFLNOCOR_2_16.RadioValue()==this.w_FLNOCOR)
      this.oPgFrm.Page2.oPag.oFLNOCOR_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIRMDICH_2_17.RadioValue()==this.w_FIRMDICH)
      this.oPgFrm.Page2.oPag.oFIRMDICH_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRFCFINS2_2_18.value==this.w_RFCFINS2)
      this.oPgFrm.Page2.oPag.oRFCFINS2_2_18.value=this.w_RFCFINS2
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMCAF_2_19.value==this.w_NUMCAF)
      this.oPgFrm.Page2.oPag.oNUMCAF_2_19.value=this.w_NUMCAF
    endif
    if not(this.oPgFrm.Page2.oPag.oRFDATIMP_2_20.value==this.w_RFDATIMP)
      this.oPgFrm.Page2.oPag.oRFDATIMP_2_20.value=this.w_RFDATIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPTRTEL_2_21.RadioValue()==this.w_IMPTRTEL)
      this.oPgFrm.Page2.oPag.oIMPTRTEL_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPTRSOG_2_22.RadioValue()==this.w_IMPTRSOG)
      this.oPgFrm.Page2.oPag.oIMPTRSOG_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFIRMINT_2_23.RadioValue()==this.w_FIRMINT)
      this.oPgFrm.Page2.oPag.oFIRMINT_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPFORN_3_1.RadioValue()==this.w_TIPFORN)
      this.oPgFrm.Page3.oPag.oTIPFORN_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oCODFISIN_3_2.value==this.w_CODFISIN)
      this.oPgFrm.Page3.oPag.oCODFISIN_3_2.value=this.w_CODFISIN
    endif
    if not(this.oPgFrm.Page3.oPag.oDATAPPLI_3_3.RadioValue()==this.w_DATAPPLI)
      this.oPgFrm.Page3.oPag.oDATAPPLI_3_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDATINI_3_4.value==this.w_DATINI)
      this.oPgFrm.Page3.oPag.oDATINI_3_4.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page3.oPag.oDATFIN_3_5.value==this.w_DATFIN)
      this.oPgFrm.Page3.oPag.oDATFIN_3_5.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oDirName_3_6.value==this.w_DirName)
      this.oPgFrm.Page3.oPag.oDirName_3_6.value=this.w_DirName
    endif
    if not(this.oPgFrm.Page3.oPag.oFileName_3_8.value==this.w_FileName)
      this.oPgFrm.Page3.oPag.oFileName_3_8.value=this.w_FileName
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FOCOGNOME))  and (empty(.w_FODENOMINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCOGNOME_1_11.SetFocus()
            i_bnoObbl = !empty(.w_FOCOGNOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_FONOME))  and (empty(.w_FODENOMINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFONOME_1_12.SetFocus()
            i_bnoObbl = !empty(.w_FONOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_FODENOMINA)) or not(not empty(.w_FODENOMINA)))  and (empty(.w_FOCOGNOME) and empty(.w_FONOME))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFODENOMINA_1_13.SetFocus()
            i_bnoObbl = !empty(.w_FODENOMINA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_FOCODFIS)) or not(iif(.w_PERAZI = 'S',chkcfp(alltrim(.w_FOCODFIS),'CF'),chkcfp(alltrim(.w_FOCODFIS),'PI'))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCODFIS_1_14.SetFocus()
            i_bnoObbl = !empty(.w_FOCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Lunghezza codice fiscale o partita IVA non corretta")
          case   (empty(.w_FOANNO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOANNO_1_15.SetFocus()
            i_bnoObbl = !empty(.w_FOANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_FOMESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOMESE_1_16.SetFocus()
            i_bnoObbl = !empty(.w_FOMESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_FOPARIVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOPARIVA_1_20.SetFocus()
            i_bnoObbl = !empty(.w_FOPARIVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATANASC))  and (.w_Perazi='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATANASC_1_21.SetFocus()
            i_bnoObbl = !empty(.w_DATANASC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_COMUNE))  and (.w_Perazi='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMUNE_1_22.SetFocus()
            i_bnoObbl = !empty(.w_COMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SIGLA))  and (.w_Perazi='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSIGLA_1_23.SetFocus()
            i_bnoObbl = !empty(.w_SIGLA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SESSO))  and (.w_Perazi='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSESSO_1_24.SetFocus()
            i_bnoObbl = !empty(.w_SESSO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(! empty(.w_RESIGLA))  and (.w_Perazi='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRESIGLA_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire la sigla della provincia di residenza anagrafica o di domicilio fiscale")
          case   not(! empty(.w_CAP))  and (.w_Perazi='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAP_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore")
          case   (empty(.w_SECOMUNE))  and (.w_Perazi<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSECOMUNE_1_29.SetFocus()
            i_bnoObbl = !empty(.w_SECOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_SESIGLA))  and (.w_Perazi<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSESIGLA_1_30.SetFocus()
            i_bnoObbl = !empty(.w_SESIGLA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NATGIU)) or not((val(.w_NATGIU)>=1 and val(.w_NATGIU)<=43) OR (val(.w_NATGIU)>=50 and val(.w_NATGIU)<=53)))  and (.w_Perazi<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNATGIU_1_31.SetFocus()
            i_bnoObbl = !empty(.w_NATGIU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso per natura giuridica")
          case   (empty(.w_SEINDIRI2))  and (.w_Perazi<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSEINDIRI2_1_32.SetFocus()
            i_bnoObbl = !empty(.w_SEINDIRI2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(! empty(.w_SECAP))  and (.w_Perazi<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSECAP_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il C.A.P. della sede legale del fornitore")
          case   not(iif(not empty(.w_RFCODFIS),chkcfp(alltrim(.w_RFCODFIS),'CF'),.T.))  and (.w_PERAZI <> 'S' or .w_FLEDITOK)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFCODFIS_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_RFPARIVA),chkcfp(alltrim(.w_RFPARIVA),'PI'),.T.))  and (.w_PERAZI <> 'S' or .w_FLEDITOK)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFPARIVA_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_RFTELEFONO))=0 and at('/',alltrim(.w_RFTELEFONO))=0 and at('-',alltrim(.w_RFTELEFONO))=0)  and (.w_PERAZI <> 'S' or .w_FLEDITOK)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFTELEFONO_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   not(iif(not empty(.w_RFCFINS2),chkcfp(alltrim(.w_RFCFINS2),'CF'),.T. ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRFCFINS2_2_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il C.F. dell'intermediario della sezione II")
          case   (empty(.w_TIPFORN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oTIPFORN_3_1.SetFocus()
            i_bnoObbl = !empty(.w_TIPFORN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CODFISIN)) or not(iif(not empty(.w_CODFISIN),chkcfp(alltrim(.w_CODFISIN),'CF'),.T.)))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oCODFISIN_3_2.SetFocus()
            i_bnoObbl = !empty(.w_CODFISIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATINI)) or not(.w_DATFIN>=.w_DATINI))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oDATINI_3_4.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   ((empty(.w_DATFIN)) or not(.w_DATFIN>=.w_DATINI))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oDATFIN_3_5.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale")
          case   (empty(.w_FileName))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oFileName_3_8.SetFocus()
            i_bnoObbl = !empty(.w_FileName)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((EMPTY(.w_PERCFIN)) OR (.w_PERCIN<=.w_PERCFIN)) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A'))  and not(empty(.w_PERCIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPERCIN_3_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
          case   not(((.w_PERCFIN>=.w_PERCIN) OR (EMPTY(.w_PERCIN))) AND (.w_RITE $ 'CS' AND .w_TIPCLF<>'A'))  and not(empty(.w_PERCFIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oPERCFIN_3_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente o obsoleto o non soggetto a ritenute o agente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_PERAZI = this.w_PERAZI
    this.o_FOCOGNOME = this.w_FOCOGNOME
    this.o_FONOME = this.w_FONOME
    this.o_FODENOMINA = this.w_FODENOMINA
    this.o_FOCODFIS = this.w_FOCODFIS
    this.o_FOANNO = this.w_FOANNO
    this.o_FOMESE = this.w_FOMESE
    this.o_NOME = this.w_NOME
    this.o_FLEDITOK = this.w_FLEDITOK
    this.o_IMPTRTEL = this.w_IMPTRTEL
    this.o_IMPTRSOG = this.w_IMPTRSOG
    this.o_TIPFORN = this.w_TIPFORN
    this.o_CODFISIN = this.w_CODFISIN
    this.o_DATFIN = this.w_DATFIN
    return

enddefine

* --- Define pages as container
define class tgscg_kclPag1 as StdContainer
  Width  = 700
  height = 538
  stdWidth  = 700
  stdheight = 538
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFOCOGNOME_1_11 as StdField with uid="SZIDZXIFTU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FOCOGNOME", cQueryName = "FOCOGNOME",;
    bObbl = .t. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 215573235,;
   bGlobalFont=.t.,;
    Height=21, Width=195, Left=101, Top=10, cSayPict="repl('!',24)", cGetPict="repl('!',24)", InputMask=replicate('X',24)

  func oFOCOGNOME_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FODENOMINA))
    endwith
   endif
  endfunc

  add object oFONOME_1_12 as StdField with uid="NPXPQLRYZB",rtseq=12,rtrep=.f.,;
    cFormVar = "w_FONOME", cQueryName = "FONOME",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 70913622,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=353, Top=10, cSayPict="repl('!',20)", cGetPict="repl('!',20)", InputMask=replicate('X',20)

  func oFONOME_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FODENOMINA))
    endwith
   endif
  endfunc

  add object oFODENOMINA_1_13 as StdField with uid="WNCFJWGGXA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_FODENOMINA", cQueryName = "FODENOMINA",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione",;
    HelpContextID = 239055999,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=101, Top=35, cSayPict="repl('!',60)", cGetPict="repl('!',60)", InputMask=replicate('X',60)

  func oFODENOMINA_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (empty(.w_FOCOGNOME) and empty(.w_FONOME))
    endwith
   endif
  endfunc

  func oFODENOMINA_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_FODENOMINA))
    endwith
    return bRes
  endfunc

  add object oFOCODFIS_1_14 as StdField with uid="NQSHPZVXNW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_FOCODFIS", cQueryName = "FOCODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Lunghezza codice fiscale o partita IVA non corretta",;
    HelpContextID = 78208681,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=540, Top=35, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oFOCODFIS_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(.w_PERAZI = 'S',chkcfp(alltrim(.w_FOCODFIS),'CF'),chkcfp(alltrim(.w_FOCODFIS),'PI')))
    endwith
    return bRes
  endfunc

  add object oFOANNO_1_15 as StdField with uid="CNLNNOICVT",rtseq=15,rtrep=.f.,;
    cFormVar = "w_FOANNO", cQueryName = "FOANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno del periodo di riferimento",;
    HelpContextID = 239615574,;
   bGlobalFont=.t.,;
    Height=21, Width=45, Left=133, Top=83, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)


  add object oFOMESE_1_16 as StdCombo with uid="HAGRESAGMX",rtseq=16,rtrep=.f.,left=220,top=83,width=126,height=21;
    , HelpContextID = 76545622;
    , cFormVar="w_FOMESE",RowSource=""+"Gennaio,"+"Febbraio,"+"Marzo,"+"Aprile,"+"Maggio,"+"Giugno,"+"Luglio,"+"Agosto,"+"Settembre,"+"Ottobre,"+"Novembre,"+"Dicembre", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oFOMESE_1_16.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    iif(this.value =6,6,;
    iif(this.value =7,7,;
    iif(this.value =8,8,;
    iif(this.value =9,9,;
    iif(this.value =10,10,;
    iif(this.value =11,11,;
    iif(this.value =12,12,;
    0)))))))))))))
  endfunc
  func oFOMESE_1_16.GetRadio()
    this.Parent.oContained.w_FOMESE = this.RadioValue()
    return .t.
  endfunc

  func oFOMESE_1_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FOMESE==1,1,;
      iif(this.Parent.oContained.w_FOMESE==2,2,;
      iif(this.Parent.oContained.w_FOMESE==3,3,;
      iif(this.Parent.oContained.w_FOMESE==4,4,;
      iif(this.Parent.oContained.w_FOMESE==5,5,;
      iif(this.Parent.oContained.w_FOMESE==6,6,;
      iif(this.Parent.oContained.w_FOMESE==7,7,;
      iif(this.Parent.oContained.w_FOMESE==8,8,;
      iif(this.Parent.oContained.w_FOMESE==9,9,;
      iif(this.Parent.oContained.w_FOMESE==10,10,;
      iif(this.Parent.oContained.w_FOMESE==11,11,;
      iif(this.Parent.oContained.w_FOMESE==12,12,;
      0))))))))))))
  endfunc

  add object oFLAGCOR_1_18 as StdCheck with uid="KAIBPTXLER",rtseq=18,rtrep=.f.,left=363, top=85, caption="Correttiva nei termini",;
    HelpContextID = 40813738,;
    cFormVar="w_FLAGCOR", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGCOR_1_18.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGCOR_1_18.GetRadio()
    this.Parent.oContained.w_FLAGCOR = this.RadioValue()
    return .t.
  endfunc

  func oFLAGCOR_1_18.SetRadio()
    this.Parent.oContained.w_FLAGCOR=trim(this.Parent.oContained.w_FLAGCOR)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGCOR=='1',1,;
      0)
  endfunc

  add object oFLAGEVE_1_19 as StdCheck with uid="WYTEXMRWTY",rtseq=19,rtrep=.f.,left=363, top=109, caption="Eventi eccezionali",;
    HelpContextID = 189711530,;
    cFormVar="w_FLAGEVE", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oFLAGEVE_1_19.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLAGEVE_1_19.GetRadio()
    this.Parent.oContained.w_FLAGEVE = this.RadioValue()
    return .t.
  endfunc

  func oFLAGEVE_1_19.SetRadio()
    this.Parent.oContained.w_FLAGEVE=trim(this.Parent.oContained.w_FLAGEVE)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGEVE=='1',1,;
      0)
  endfunc

  add object oFOPARIVA_1_20 as StdField with uid="PMHHIZZXAL",rtseq=20,rtrep=.f.,;
    cFormVar = "w_FOPARIVA", cQueryName = "FOPARIVA",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 126079337,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=133, Top=109, InputMask=replicate('X',16)

  add object oDATANASC_1_21 as StdField with uid="WGYOTWHLZZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DATANASC", cQueryName = "DATANASC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del fornitore",;
    HelpContextID = 264478599,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=6, Top=173

  func oDATANASC_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc

  add object oCOMUNE_1_22 as StdField with uid="QDXVFBYYJG",rtseq=22,rtrep=.f.,;
    cFormVar = "w_COMUNE", cQueryName = "COMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del fornitore",;
    HelpContextID = 72351270,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=109, Top=173, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oCOMUNE_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc

  add object oSIGLA_1_23 as StdField with uid="AQSAXFLTQI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SIGLA", cQueryName = "SIGLA",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del fornitore",;
    HelpContextID = 242653478,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=430, Top=173, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSIGLA_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc


  add object oSESSO_1_24 as StdCombo with uid="NCCDEVHYQI",rtseq=24,rtrep=.f.,left=518,top=174,width=92,height=21;
    , height = 21;
    , ToolTipText = "Sesso del fornitore";
    , HelpContextID = 257840422;
    , cFormVar="w_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oSESSO_1_24.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oSESSO_1_24.GetRadio()
    this.Parent.oContained.w_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oSESSO_1_24.SetRadio()
    this.Parent.oContained.w_SESSO=trim(this.Parent.oContained.w_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_SESSO=='M',1,;
      iif(this.Parent.oContained.w_SESSO=='F',2,;
      0))
  endfunc

  func oSESSO_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc

  add object oRECOMUNE_1_25 as StdField with uid="OZIDXVLWTW",rtseq=25,rtrep=.f.,;
    cFormVar = "w_RECOMUNE", cQueryName = "RECOMUNE",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del fornitore",;
    HelpContextID = 70866267,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=109, Top=213, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRECOMUNE_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc

  add object oRESIGLA_1_26 as StdField with uid="SUWJKWJLTK",rtseq=26,rtrep=.f.,;
    cFormVar = "w_RESIGLA", cQueryName = "RESIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire la sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 86747882,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=430, Top=213, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRESIGLA_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc

  func oRESIGLA_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_RESIGLA))
    endwith
    return bRes
  endfunc

  add object oINDIRIZ_1_27 as StdField with uid="ZFPHCRDCWK",rtseq=27,rtrep=.f.,;
    cFormVar = "w_INDIRIZ", cQueryName = "INDIRIZ",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 142830982,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=109, Top=254, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oINDIRIZ_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc

  add object oCAP_1_28 as StdField with uid="MDJULVDKRQ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CAP", cQueryName = "CAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del fornitore",;
    HelpContextID = 169549862,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=430, Top=254, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oCAP_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc

  func oCAP_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_CAP))
    endwith
    return bRes
  endfunc

  add object oSECOMUNE_1_29 as StdField with uid="NFZSOTAWKE",rtseq=29,rtrep=.f.,;
    cFormVar = "w_SECOMUNE", cQueryName = "SECOMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune della sede legale del fornitore",;
    HelpContextID = 70866283,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=109, Top=352, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oSECOMUNE_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oSESIGLA_1_30 as StdField with uid="OOQMIUMTFG",rtseq=30,rtrep=.f.,;
    cFormVar = "w_SESIGLA", cQueryName = "SESIGLA",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia della sede legale del fornitore",;
    HelpContextID = 86747866,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=430, Top=352, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSESIGLA_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oNATGIU_1_31 as StdField with uid="GGFCANZHTS",rtseq=31,rtrep=.f.,;
    cFormVar = "w_NATGIU", cQueryName = "NATGIU",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso per natura giuridica",;
    ToolTipText = "Tipologia natura giuridica",;
    HelpContextID = 202219306,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=41, Left=506, Top=354, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oNATGIU_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  func oNATGIU_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((val(.w_NATGIU)>=1 and val(.w_NATGIU)<=43) OR (val(.w_NATGIU)>=50 and val(.w_NATGIU)<=53))
    endwith
    return bRes
  endfunc

  add object oSEINDIRI2_1_32 as StdField with uid="OXQZLOKZQT",rtseq=32,rtrep=.f.,;
    cFormVar = "w_SEINDIRI2", cQueryName = "SEINDIRI2",;
    bObbl = .t. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede legale del fornitore",;
    HelpContextID = 139937649,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=109, Top=393, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oSEINDIRI2_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oSECAP_1_33 as StdField with uid="AVSZVWBCSZ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_SECAP", cQueryName = "SECAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il C.A.P. della sede legale del fornitore",;
    ToolTipText = "C.A.P. della sede legale del fornitore",;
    HelpContextID = 257643814,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=430, Top=393, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oSECAP_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  func oSECAP_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_SECAP))
    endwith
    return bRes
  endfunc

  add object oSERCOMUN_1_34 as StdField with uid="KMAFMNRNPP",rtseq=34,rtrep=.f.,;
    cFormVar = "w_SERCOMUN", cQueryName = "SERCOMUN",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di domicilio fiscale del fornitore",;
    HelpContextID = 61979276,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=109, Top=431, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oSERCOMUN_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oSERSIGLA_1_35 as StdField with uid="TGOHNSUWWG",rtseq=35,rtrep=.f.,;
    cFormVar = "w_SERSIGLA", cQueryName = "SERSIGLA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di domicilio fiscale del fornitore",;
    HelpContextID = 100549991,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=430, Top=431, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oSERSIGLA_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oSERINDIR_1_36 as StdField with uid="ZEWEROQCEA",rtseq=36,rtrep=.f.,;
    cFormVar = "w_SERINDIR", cQueryName = "SERINDIR",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di domicilio fiscale del fornitore",;
    HelpContextID = 54805880,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=109, Top=469, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oSERINDIR_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oSERCAP_1_83 as StdField with uid="KHVCIKTTRJ",rtseq=52,rtrep=.f.,;
    cFormVar = "w_SERCAP", cQueryName = "SERCAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. del domicilio fiscale del fornitore",;
    HelpContextID = 242107686,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=430, Top=470, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oSERCAP_1_83.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oSTAEST_1_84 as StdField with uid="ITETMLBRKD",rtseq=53,rtrep=.f.,;
    cFormVar = "w_STAEST", cQueryName = "STAEST",;
    bObbl = .f. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Stato estero di residenza anagrafica o domicilio",;
    HelpContextID = 208714714,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=109, Top=294, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oSTAEST_1_84.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc

  add object oCODEST_1_87 as StdField with uid="FQRXEKYJJD",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CODEST", cQueryName = "CODEST",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero di residenza o domicilio",;
    HelpContextID = 208703962,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=430, Top=294

  func oCODEST_1_87.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc

  add object oIDIVAEST_1_89 as StdField with uid="EGERYCZIDG",rtseq=55,rtrep=.f.,;
    cFormVar = "w_IDIVAEST", cQueryName = "IDIVAEST",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Numero identificativo IVA stato estero",;
    HelpContextID = 209669158,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=518, Top=294, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',20)

  func oIDIVAEST_1_89.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi='S')
    endwith
   endif
  endfunc

  add object oSESTAEST_1_91 as StdField with uid="XYREBBCELC",rtseq=56,rtrep=.f.,;
    cFormVar = "w_SESTAEST", cQueryName = "SESTAEST",;
    bObbl = .f. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Stato estero di residenza anagrafica o domicilio",;
    HelpContextID = 209758854,;
   bGlobalFont=.t.,;
    Height=21, Width=319, Left=110, Top=510, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oSESTAEST_1_91.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oSECODEST_1_93 as StdField with uid="QSEFUETRRT",rtseq=57,rtrep=.f.,;
    cFormVar = "w_SECODEST", cQueryName = "SECODEST",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice stato estero di residenza o domicilio",;
    HelpContextID = 207006342,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=431, Top=510

  func oSECODEST_1_93.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oSEIDIVES_1_95 as StdField with uid="WRJXBYJKNU",rtseq=58,rtrep=.f.,;
    cFormVar = "w_SEIDIVES", cQueryName = "SEIDIVES",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Numero identificativo IVA stato estero",;
    HelpContextID = 185682567,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=515, Top=511, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',20)

  func oSEIDIVES_1_95.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Perazi<>'S')
    endwith
   endif
  endfunc

  add object oStr_1_46 as StdString with uid="PZCKHMGKWT",Visible=.t., Left=32, Top=11,;
    Alignment=1, Width=66, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="MIJLOSOMKI",Visible=.t., Left=312, Top=12,;
    Alignment=1, Width=37, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="TZKQRFHOUY",Visible=.t., Left=9, Top=62,;
    Alignment=0, Width=204, Height=18,;
    Caption="Comunicazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_50 as StdString with uid="UCIEAMMITE",Visible=.t., Left=8, Top=36,;
    Alignment=1, Width=90, Height=15,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="HLIVBEVOMR",Visible=.t., Left=545, Top=18,;
    Alignment=0, Width=90, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="EFRWRPPCTM",Visible=.t., Left=11, Top=141,;
    Alignment=0, Width=204, Height=15,;
    Caption="Persona fisica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_57 as StdString with uid="HRDBHHFBYE",Visible=.t., Left=109, Top=197,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di residenza o domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_58 as StdString with uid="ZDKIULOHFL",Visible=.t., Left=5, Top=158,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_59 as StdString with uid="CTCAOZXFVH",Visible=.t., Left=109, Top=158,;
    Alignment=0, Width=223, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_60 as StdString with uid="VETKYUGDVO",Visible=.t., Left=430, Top=158,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_61 as StdString with uid="HVQAZPYBNO",Visible=.t., Left=430, Top=197,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_62 as StdString with uid="ATTHQLRPRN",Visible=.t., Left=109, Top=239,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_63 as StdString with uid="KSQHILFMHS",Visible=.t., Left=518, Top=158,;
    Alignment=0, Width=48, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_64 as StdString with uid="IVLKBJFSJW",Visible=.t., Left=430, Top=239,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_68 as StdString with uid="DEKDZLFCML",Visible=.t., Left=11, Top=317,;
    Alignment=0, Width=228, Height=15,;
    Caption="Altri soggetti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_69 as StdString with uid="EBKJKEHJKK",Visible=.t., Left=20, Top=337,;
    Alignment=0, Width=80, Height=13,;
    Caption="Sede legale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_70 as StdString with uid="RZVEDQJFMM",Visible=.t., Left=109, Top=337,;
    Alignment=0, Width=76, Height=13,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_71 as StdString with uid="UNXIIROKSZ",Visible=.t., Left=506, Top=339,;
    Alignment=0, Width=84, Height=13,;
    Caption="Natura giuridica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_72 as StdString with uid="UQKLITXGSW",Visible=.t., Left=430, Top=337,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_73 as StdString with uid="EXNBDAOCWO",Visible=.t., Left=109, Top=378,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_74 as StdString with uid="WLXGABIKNW",Visible=.t., Left=430, Top=378,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_75 as StdString with uid="RMDNMKOJQJ",Visible=.t., Left=20, Top=416,;
    Alignment=0, Width=76, Height=13,;
    Caption="Dom. fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_76 as StdString with uid="ZVHEBJFODD",Visible=.t., Left=109, Top=416,;
    Alignment=0, Width=76, Height=13,;
    Caption="Comune"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_77 as StdString with uid="VJNNNJGRSB",Visible=.t., Left=430, Top=416,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_78 as StdString with uid="XMGFDNXRGY",Visible=.t., Left=109, Top=454,;
    Alignment=0, Width=216, Height=13,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_80 as StdString with uid="DOZJEKFWEL",Visible=.t., Left=7, Top=85,;
    Alignment=1, Width=120, Height=18,;
    Caption="Periodo di rif.: anno"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="JZVJTRCBMB",Visible=.t., Left=182, Top=85,;
    Alignment=0, Width=33, Height=18,;
    Caption="Mese"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="CNBFEAFDFD",Visible=.t., Left=430, Top=455,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_85 as StdString with uid="GIPZFJHZAE",Visible=.t., Left=52, Top=111,;
    Alignment=1, Width=77, Height=18,;
    Caption="Partita I.V.A.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="VGVSUSDFSZ",Visible=.t., Left=109, Top=278,;
    Alignment=0, Width=216, Height=14,;
    Caption="Stato estero di residenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_88 as StdString with uid="IROYWJJXZT",Visible=.t., Left=430, Top=279,;
    Alignment=0, Width=81, Height=14,;
    Caption="Cod. stato est."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_90 as StdString with uid="KUSGBUTWMP",Visible=.t., Left=520, Top=275,;
    Alignment=0, Width=178, Height=17,;
    Caption="Num. identificazione IVA stato est."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_92 as StdString with uid="SSLSKEUTDP",Visible=.t., Left=110, Top=494,;
    Alignment=0, Width=216, Height=14,;
    Caption="Stato estero di residenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_94 as StdString with uid="IIBHYYQHXH",Visible=.t., Left=431, Top=494,;
    Alignment=0, Width=81, Height=14,;
    Caption="Cod. stato est."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_96 as StdString with uid="WHVIUFMTHG",Visible=.t., Left=516, Top=494,;
    Alignment=0, Width=178, Height=14,;
    Caption="Num. identificazione IVA stato est."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_48 as StdBox with uid="YPBURJVQFL",left=7, top=78, width=681,height=2

  add object oBox_1_55 as StdBox with uid="TFBVYLMEDX",left=7, top=155, width=681,height=2

  add object oBox_1_67 as StdBox with uid="HJLYLCJQUK",left=7, top=331, width=681,height=2
enddefine
define class tgscg_kclPag2 as StdContainer
  Width  = 700
  height = 538
  stdWidth  = 700
  stdheight = 538
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFLEDITOK_2_1 as StdCheck with uid="ANPGYPQFYE",rtseq=63,rtrep=.f.,left=561, top=4, caption="Forza editing",;
    HelpContextID = 49183649,;
    cFormVar="w_FLEDITOK", bObbl = .f. , nPag = 2;
    , TABSTOP=.F.;
   , bGlobalFont=.t.


  func oFLEDITOK_2_1.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oFLEDITOK_2_1.GetRadio()
    this.Parent.oContained.w_FLEDITOK = this.RadioValue()
    return .t.
  endfunc

  func oFLEDITOK_2_1.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLEDITOK==.t.,1,;
      0)
  endfunc

  add object oRFCODFIS_2_2 as StdField with uid="MTXMRCWFQH",rtseq=64,rtrep=.f.,;
    cFormVar = "w_RFCODFIS", cQueryName = "RFCODFIS",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    HelpContextID = 78206569,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=48, Top=46, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)

  func oRFCODFIS_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  func oRFCODFIS_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_RFCODFIS),chkcfp(alltrim(.w_RFCODFIS),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oRFPARIVA_2_3 as StdField with uid="LOCJXXTAOO",rtseq=65,rtrep=.f.,;
    cFormVar = "w_RFPARIVA", cQueryName = "RFPARIVA",;
    bObbl = .f. , nPag = 2, value=space(11), bMultilanguage =  .f.,;
    HelpContextID = 126081449,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=208, Top=46, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',11)

  func oRFPARIVA_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  func oRFPARIVA_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_RFPARIVA),chkcfp(alltrim(.w_RFPARIVA),'PI'),.T.))
    endwith
    return bRes
  endfunc


  add object oRFCODCAR_2_4 as StdCombo with uid="YYMESQRZRO",rtseq=66,rtrep=.f.,left=350,top=46,width=286,height=21;
    , ToolTipText = "Codice carica";
    , HelpContextID = 240560536;
    , cFormVar="w_RFCODCAR",RowSource=""+""+l_descri1+","+""+l_descri2+","+""+l_descri3+","+""+l_descri4+","+""+l_descri5+","+""+l_descri6+","+""+l_descri7+","+""+l_descri8+","+""+l_descri9+"", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oRFCODCAR_2_4.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    '  '))))))))))
  endfunc
  func oRFCODCAR_2_4.GetRadio()
    this.Parent.oContained.w_RFCODCAR = this.RadioValue()
    return .t.
  endfunc

  func oRFCODCAR_2_4.SetRadio()
    this.Parent.oContained.w_RFCODCAR=trim(this.Parent.oContained.w_RFCODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_RFCODCAR=='1',1,;
      iif(this.Parent.oContained.w_RFCODCAR=='2',2,;
      iif(this.Parent.oContained.w_RFCODCAR=='3',3,;
      iif(this.Parent.oContained.w_RFCODCAR=='4',4,;
      iif(this.Parent.oContained.w_RFCODCAR=='5',5,;
      iif(this.Parent.oContained.w_RFCODCAR=='6',6,;
      iif(this.Parent.oContained.w_RFCODCAR=='7',7,;
      iif(this.Parent.oContained.w_RFCODCAR=='8',8,;
      iif(this.Parent.oContained.w_RFCODCAR=='9',9,;
      0)))))))))
  endfunc

  func oRFCODCAR_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  add object oRFCOGNOME_2_5 as StdField with uid="TUTTYIGKSZ",rtseq=67,rtrep=.f.,;
    cFormVar = "w_RFCOGNOME", cQueryName = "RFCOGNOME",;
    bObbl = .f. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del rappresentante",;
    HelpContextID = 215571123,;
   bGlobalFont=.t.,;
    Height=21, Width=195, Left=48, Top=84, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oRFCOGNOME_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  add object oRFNOME_2_6 as StdField with uid="RKLEAKPRVC",rtseq=68,rtrep=.f.,;
    cFormVar = "w_RFNOME", cQueryName = "RFNOME",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del rappresentante",;
    HelpContextID = 70911510,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=248, Top=84, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',20)

  func oRFNOME_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc


  add object oRFSESSO_2_7 as StdCombo with uid="WGXEPZGXVZ",value=1,rtseq=69,rtrep=.f.,left=429,top=84,width=114,height=21;
    , height = 21;
    , ToolTipText = "Sesso del rappresentante";
    , HelpContextID = 43013654;
    , cFormVar="w_RFSESSO",RowSource=""+"Non selezionato,"+"Maschio,"+"Femmina", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oRFSESSO_2_7.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'M',;
    iif(this.value =3,'F',;
    ' '))))
  endfunc
  func oRFSESSO_2_7.GetRadio()
    this.Parent.oContained.w_RFSESSO = this.RadioValue()
    return .t.
  endfunc

  func oRFSESSO_2_7.SetRadio()
    this.Parent.oContained.w_RFSESSO=trim(this.Parent.oContained.w_RFSESSO)
    this.value = ;
      iif(this.Parent.oContained.w_RFSESSO=='',1,;
      iif(this.Parent.oContained.w_RFSESSO=='M',2,;
      iif(this.Parent.oContained.w_RFSESSO=='F',3,;
      0)))
  endfunc

  func oRFSESSO_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  add object oRFDATANASC_2_8 as StdField with uid="AUOENXXQZR",rtseq=70,rtrep=.f.,;
    cFormVar = "w_RFDATANASC", cQueryName = "RFDATANASC",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del rappresentante",;
    HelpContextID = 10202759,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=48, Top=125

  func oRFDATANASC_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  add object oRFCOMNAS_2_9 as StdField with uid="RPGGCLVJHX",rtseq=71,rtrep=.f.,;
    cFormVar = "w_RFCOMNAS", cQueryName = "RFCOMNAS",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del rappresentante",;
    HelpContextID = 46573975,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=156, Top=125, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRFCOMNAS_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  add object oRFSIGNAS_2_10 as StdField with uid="DAVHPWZQVP",rtseq=72,rtrep=.f.,;
    cFormVar = "w_RFSIGNAS", cQueryName = "RFSIGNAS",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del rappresentante",;
    HelpContextID = 53193111,;
   bGlobalFont=.t.,;
    Height=21, Width=32, Left=470, Top=125, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRFSIGNAS_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  add object oRFCOMUNE_2_11 as StdField with uid="DDNELJFMGC",rtseq=73,rtrep=.f.,;
    cFormVar = "w_RFCOMUNE", cQueryName = "RFCOMUNE",;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del rappresentante",;
    HelpContextID = 70866523,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=48, Top=165, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oRFCOMUNE_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  add object oRFSIGLA_2_12 as StdField with uid="MPTADLFURD",rtseq=74,rtrep=.f.,;
    cFormVar = "w_RFSIGLA", cQueryName = "RFSIGLA",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 86747626,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=363, Top=165, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  func oRFSIGLA_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  add object oRFCAP_2_13 as StdField with uid="VMMUJSTIQO",rtseq=75,rtrep=.f.,;
    cFormVar = "w_RFCAP", cQueryName = "RFCAP",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del rappresentante",;
    HelpContextID = 257644054,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=416, Top=165, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  func oRFCAP_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  add object oRFINDIRIZ_2_14 as StdField with uid="HCOWWPHROD",rtseq=76,rtrep=.f.,;
    cFormVar = "w_RFINDIRIZ", cQueryName = "RFINDIRIZ",;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 139936769,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=48, Top=205, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oRFINDIRIZ_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  add object oRFTELEFONO_2_15 as StdField with uid="MMGWOVEOZI",rtseq=77,rtrep=.f.,;
    cFormVar = "w_RFTELEFONO", cQueryName = "RFTELEFONO",;
    bObbl = .f. , nPag = 2, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    HelpContextID = 199181755,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=363, Top=205, cSayPict="repl('9',12)", cGetPict="repl('9',12)", InputMask=replicate('X',12)

  func oRFTELEFONO_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERAZI <> 'S' or .w_FLEDITOK)
    endwith
   endif
  endfunc

  func oRFTELEFONO_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_RFTELEFONO))=0 and at('/',alltrim(.w_RFTELEFONO))=0 and at('-',alltrim(.w_RFTELEFONO))=0)
    endwith
    return bRes
  endfunc

  add object oFLNOCOR_2_16 as StdCheck with uid="IATICRAGRO",rtseq=78,rtrep=.f.,left=16, top=263, caption="Flag conferma",;
    HelpContextID = 40236202,;
    cFormVar="w_FLNOCOR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLNOCOR_2_16.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLNOCOR_2_16.GetRadio()
    this.Parent.oContained.w_FLNOCOR = this.RadioValue()
    return .t.
  endfunc

  func oFLNOCOR_2_16.SetRadio()
    this.Parent.oContained.w_FLNOCOR=trim(this.Parent.oContained.w_FLNOCOR)
    this.value = ;
      iif(this.Parent.oContained.w_FLNOCOR=='1',1,;
      0)
  endfunc

  add object oFIRMDICH_2_17 as StdCheck with uid="ZFJVFMHAUQ",rtseq=79,rtrep=.f.,left=15, top=322, caption="Flag conferma",;
    ToolTipText = "Firma del dichiarante",;
    HelpContextID = 139966306,;
    cFormVar="w_FIRMDICH", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFIRMDICH_2_17.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMDICH_2_17.GetRadio()
    this.Parent.oContained.w_FIRMDICH = this.RadioValue()
    return .t.
  endfunc

  func oFIRMDICH_2_17.SetRadio()
    this.Parent.oContained.w_FIRMDICH=trim(this.Parent.oContained.w_FIRMDICH)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMDICH=='1',1,;
      0)
  endfunc

  add object oRFCFINS2_2_18 as StdField with uid="ZFMCVGTPTY",rtseq=80,rtrep=.f.,;
    cFormVar = "w_RFCFINS2", cQueryName = "RFCFINS2",;
    bObbl = .f. , nPag = 2, value=space(13), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il C.F. dell'intermediario della sezione II",;
    ToolTipText = "Codice fiscale dell'intermediario della sezione II",;
    HelpContextID = 51358136,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=17, Top=400, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',13)

  func oRFCFINS2_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_RFCFINS2),chkcfp(alltrim(.w_RFCFINS2),'CF'),.T. ))
    endwith
    return bRes
  endfunc

  add object oNUMCAF_2_19 as StdField with uid="KVCYFTJAQL",rtseq=81,rtrep=.f.,;
    cFormVar = "w_NUMCAF", cQueryName = "NUMCAF",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero iscrizione all'albo del C.A.F.",;
    HelpContextID = 74319062,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=175, Top=400, cSayPict='"99999"', cGetPict='"99999"'

  add object oRFDATIMP_2_20 as StdField with uid="UVYTNIYLIS",rtseq=82,rtrep=.f.,;
    cFormVar = "w_RFDATIMP", cQueryName = "RFDATIMP",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data impegno trasmissione fornitura",;
    HelpContextID = 144402022,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=382, Top=400

  add object oIMPTRTEL_2_21 as StdCheck with uid="UWMCFDUCPC",rtseq=83,rtrep=.f.,left=20, top=447, caption="Impegno a trasmettere in via telematica la dich. predisposta dal contribuente",;
    ToolTipText = "Impegno trasmissione telematica dichiarazione predisposta dal contribuente",;
    HelpContextID = 208720686,;
    cFormVar="w_IMPTRTEL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oIMPTRTEL_2_21.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oIMPTRTEL_2_21.GetRadio()
    this.Parent.oContained.w_IMPTRTEL = this.RadioValue()
    return .t.
  endfunc

  func oIMPTRTEL_2_21.SetRadio()
    this.Parent.oContained.w_IMPTRTEL=trim(this.Parent.oContained.w_IMPTRTEL)
    this.value = ;
      iif(this.Parent.oContained.w_IMPTRTEL=='1',1,;
      0)
  endfunc

  add object oIMPTRSOG_2_22 as StdCheck with uid="XXHZPERTNM",rtseq=84,rtrep=.f.,left=20, top=478, caption="Impegno a trasmettere in via telematica la dich. del contribuente",;
    ToolTipText = "Impegno a trasmettere in via telematica la dichiarazione del contribuente",;
    HelpContextID = 42937549,;
    cFormVar="w_IMPTRSOG", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oIMPTRSOG_2_22.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oIMPTRSOG_2_22.GetRadio()
    this.Parent.oContained.w_IMPTRSOG = this.RadioValue()
    return .t.
  endfunc

  func oIMPTRSOG_2_22.SetRadio()
    this.Parent.oContained.w_IMPTRSOG=trim(this.Parent.oContained.w_IMPTRSOG)
    this.value = ;
      iif(this.Parent.oContained.w_IMPTRSOG=='1',1,;
      0)
  endfunc

  add object oFIRMINT_2_23 as StdCheck with uid="PJOQBGGUFU",rtseq=85,rtrep=.f.,left=489, top=402, caption="Firma intermediario",;
    ToolTipText = "Firma intermediario",;
    HelpContextID = 50837418,;
    cFormVar="w_FIRMINT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFIRMINT_2_23.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFIRMINT_2_23.GetRadio()
    this.Parent.oContained.w_FIRMINT = this.RadioValue()
    return .t.
  endfunc

  func oFIRMINT_2_23.SetRadio()
    this.Parent.oContained.w_FIRMINT=trim(this.Parent.oContained.w_FIRMINT)
    this.value = ;
      iif(this.Parent.oContained.w_FIRMINT=='1',1,;
      0)
  endfunc

  add object oStr_2_25 as StdString with uid="FTEQCYTUWC",Visible=.t., Left=11, Top=244,;
    Alignment=0, Width=458, Height=18,;
    Caption="Mancata corrispondenza dei dati da trasmettere con quelli della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_27 as StdString with uid="FHMRICXWEG",Visible=.t., Left=11, Top=11,;
    Alignment=0, Width=367, Height=15,;
    Caption="Dati relativi al rappresentante firmatario della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_28 as StdString with uid="RULZHJZXRW",Visible=.t., Left=48, Top=29,;
    Alignment=0, Width=112, Height=13,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_29 as StdString with uid="USKIUPDQMV",Visible=.t., Left=348, Top=29,;
    Alignment=0, Width=84, Height=13,;
    Caption="Codice carica"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_30 as StdString with uid="GPZTYMBFPO",Visible=.t., Left=156, Top=110,;
    Alignment=0, Width=104, Height=13,;
    Caption="Comune di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_31 as StdString with uid="RCZFFIGQZJ",Visible=.t., Left=48, Top=70,;
    Alignment=0, Width=76, Height=13,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_32 as StdString with uid="BXGOIXINBX",Visible=.t., Left=249, Top=70,;
    Alignment=0, Width=76, Height=13,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_33 as StdString with uid="XMDUKEDFTN",Visible=.t., Left=429, Top=69,;
    Alignment=0, Width=76, Height=13,;
    Caption="Sesso"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_34 as StdString with uid="VNNLUPWWEO",Visible=.t., Left=48, Top=110,;
    Alignment=0, Width=103, Height=13,;
    Caption="Data di nascita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_35 as StdString with uid="LCBMVTEUCO",Visible=.t., Left=470, Top=110,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_36 as StdString with uid="ECENBGSUXY",Visible=.t., Left=48, Top=150,;
    Alignment=0, Width=120, Height=13,;
    Caption="Comune di residenza"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_37 as StdString with uid="WXIGENELJC",Visible=.t., Left=363, Top=150,;
    Alignment=0, Width=48, Height=13,;
    Caption="Provincia"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_38 as StdString with uid="FSUMAEAACK",Visible=.t., Left=416, Top=150,;
    Alignment=0, Width=56, Height=13,;
    Caption="CAP"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_39 as StdString with uid="XDOCDGJPIJ",Visible=.t., Left=48, Top=190,;
    Alignment=0, Width=48, Height=13,;
    Caption="Indirizzo"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_40 as StdString with uid="XJBBNGRGLQ",Visible=.t., Left=363, Top=190,;
    Alignment=0, Width=48, Height=13,;
    Caption="Telefono"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_41 as StdString with uid="LQRAAIJAJN",Visible=.t., Left=382, Top=379,;
    Alignment=0, Width=183, Height=13,;
    Caption="Data dell'impegno a trasmettere"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_42 as StdString with uid="ELNVKYREGV",Visible=.t., Left=11, Top=355,;
    Alignment=0, Width=253, Height=15,;
    Caption="Impegno alla presentazione telematica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_44 as StdString with uid="EMANNTQNBZ",Visible=.t., Left=175, Top=379,;
    Alignment=0, Width=203, Height=13,;
    Caption="Numero di iscrizione all'albo del C.A.F."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_46 as StdString with uid="UONHLZGTRJ",Visible=.t., Left=11, Top=298,;
    Alignment=0, Width=154, Height=18,;
    Caption="Firma della dichiarazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_47 as StdString with uid="EZGKVWNGDV",Visible=.t., Left=18, Top=379,;
    Alignment=0, Width=139, Height=13,;
    Caption="Codice fiscale intermediario"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_48 as StdString with uid="OETTYLRHBC",Visible=.t., Left=208, Top=29,;
    Alignment=0, Width=137, Height=13,;
    Caption="Codice fiscale soc. rapp."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_24 as StdBox with uid="FZZKXTTQTF",left=11, top=258, width=672,height=2

  add object oBox_2_26 as StdBox with uid="LEBGSAUSDO",left=7, top=26, width=677,height=2

  add object oBox_2_43 as StdBox with uid="DTCNDAKETU",left=11, top=369, width=672,height=2

  add object oBox_2_45 as StdBox with uid="LDVQJNYPWN",left=11, top=314, width=672,height=2
enddefine
define class tgscg_kclPag3 as StdContainer
  Width  = 700
  height = 538
  stdWidth  = 700
  stdheight = 538
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPFORN_3_1 as StdCombo with uid="TMIIFPZQPN",rtseq=86,rtrep=.f.,left=101,top=31,width=262,height=21;
    , height = 21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 22096182;
    , cFormVar="w_TIPFORN",RowSource=""+"01: Soggetti che inviano le dichiarazioni,"+"10: Altri soggetti", bObbl = .t. , nPag = 3;
  , bGlobalFont=.t.


  func oTIPFORN_3_1.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'10',;
    space(2))))
  endfunc
  func oTIPFORN_3_1.GetRadio()
    this.Parent.oContained.w_TIPFORN = this.RadioValue()
    return .t.
  endfunc

  func oTIPFORN_3_1.SetRadio()
    this.Parent.oContained.w_TIPFORN=trim(this.Parent.oContained.w_TIPFORN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPFORN=='01',1,;
      iif(this.Parent.oContained.w_TIPFORN=='10',2,;
      0))
  endfunc

  add object oCODFISIN_3_2 as StdField with uid="OIWLMRKWII",rtseq=87,rtrep=.f.,;
    cFormVar = "w_CODFISIN", cQueryName = "CODFISIN",;
    bObbl = .t. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'intermediario che effettua la trasmissione",;
    HelpContextID = 32534132,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=532, Top=31, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oCODFISIN_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_CODFISIN),chkcfp(alltrim(.w_CODFISIN),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oDATAPPLI_3_3 as StdCheck with uid="UDCUDWSPAA",rtseq=88,rtrep=.f.,left=101, top=109, caption="Considera data prima applicazione",;
    ToolTipText = "Se attivo, verranno considerate le dichiarazioni con data prima applicazione nell'intervallo",;
    HelpContextID = 257712255,;
    cFormVar="w_DATAPPLI", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oDATAPPLI_3_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDATAPPLI_3_3.GetRadio()
    this.Parent.oContained.w_DATAPPLI = this.RadioValue()
    return .t.
  endfunc

  func oDATAPPLI_3_3.SetRadio()
    this.Parent.oContained.w_DATAPPLI=trim(this.Parent.oContained.w_DATAPPLI)
    this.value = ;
      iif(this.Parent.oContained.w_DATAPPLI=='S',1,;
      0)
  endfunc

  add object oDATINI_3_4 as StdField with uid="JPXZKEKVIP",rtseq=89,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data iniziale di stampa",;
    HelpContextID = 138698806,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=101, Top=157

  func oDATINI_3_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFIN>=.w_DATINI)
    endwith
    return bRes
  endfunc

  add object oDATFIN_3_5 as StdField with uid="FNNEPIJJDP",rtseq=90,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale",;
    ToolTipText = "Data finale di stampa",;
    HelpContextID = 217145398,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=250, Top=157

  func oDATFIN_3_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFIN>=.w_DATINI)
    endwith
    return bRes
  endfunc

  add object oDirName_3_6 as StdField with uid="EJPBDNOIEF",rtseq=91,rtrep=.f.,;
    cFormVar = "w_DirName", cQueryName = "DirName",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 42244042,;
   bGlobalFont=.t.,;
    Height=21, Width=446, Left=109, Top=232, InputMask=replicate('X',200)


  add object oBtn_3_7 as StdButton with uid="QIOPRPPDCF",left=557, top=232, width=18,height=21,;
    caption="...", nPag=3;
    , HelpContextID = 169405398;
    , tabstop = .f.;
  , bGlobalFont=.t.

    proc oBtn_3_7.Click()
      with this.Parent.oContained
        do SfogliaDir with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFileName_3_8 as StdField with uid="OOIIRZMLFQ",rtseq=92,rtrep=.f.,;
    cFormVar = "w_FileName", cQueryName = "FileName",;
    bObbl = .t. , nPag = 3, value=space(31), bMultilanguage =  .f.,;
    HelpContextID = 6424763,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=109, Top=267, InputMask=replicate('X',31)


  add object oBtn_3_25 as StdButton with uid="AXYVHWBWMA",left=641, top=134, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=3;
    , ToolTipText = "Stampa di controllo dati dichiarazioni intento";
    , HelpContextID = 8285162;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_25.Click()
      with this.Parent.oContained
        GSCG_BDG(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_26 as StdButton with uid="ARLJUNADDL",left=597, top=492, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=3;
    , HelpContextID = 8285162;
    , tabstop = .f., Caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_26.Click()
      with this.Parent.oContained
        GSCG_BDG(this.Parent.oContained,"G")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_27 as StdButton with uid="SIRVPTMAGL",left=649, top=492, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=3;
    , HelpContextID = 8285162;
    , tabstop = .f., Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_3_28 as StdButton with uid="GCUKLMFKUC",left=545, top=492, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=3;
    , ToolTipText = "Crea la stampa del frontespizio dell'inzio dichiarazione d'intento ricevute";
    , HelpContextID = 8285162;
    , Caption='\<PDF';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_28.Click()
      with this.Parent.oContained
        GSCG_BDG(this.Parent.oContained,"F")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_3_14 as StdString with uid="LCHEPLEODA",Visible=.t., Left=32, Top=157,;
    Alignment=1, Width=64, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_3_15 as StdString with uid="GUHHFLLZER",Visible=.t., Left=185, Top=157,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="QDPZXPECZB",Visible=.t., Left=46, Top=232,;
    Alignment=1, Width=58, Height=15,;
    Caption="Directory:"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="FKJYZDRMHF",Visible=.t., Left=36, Top=267,;
    Alignment=1, Width=69, Height=15,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_3_18 as StdString with uid="TKFFKWNXKB",Visible=.t., Left=12, Top=77,;
    Alignment=0, Width=163, Height=15,;
    Caption="Parametri di selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_21 as StdString with uid="KLZWVWGXPN",Visible=.t., Left=12, Top=199,;
    Alignment=0, Width=204, Height=15,;
    Caption="File telematico"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_23 as StdString with uid="MDEDFCKUHY",Visible=.t., Left=9, Top=32,;
    Alignment=1, Width=87, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_3_24 as StdString with uid="AJIXNHKAAD",Visible=.t., Left=379, Top=33,;
    Alignment=1, Width=146, Height=15,;
    Caption="Cod. fis. intermediario:"  ;
  , bGlobalFont=.t.

  add object oBox_3_19 as StdBox with uid="EWLUHYXJCA",left=8, top=93, width=676,height=2

  add object oBox_3_20 as StdBox with uid="ALLGPWEGAO",left=8, top=215, width=676,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kcl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_kcl
proc SfogliaDir (parent)
local PathName, oField
  PathName = Cp_GetDir()
  if !empty(PathName)
    parent.w_DirName = PathName
  endif
endproc

func IsPerFis(Cognome,Nome,Denominazione)
do case
case empty(Cognome) and empty(Nome) and empty(Denominazione)
  return 'N'
case !empty(Cognome) or !empty(Nome)
  return 'S'
case !empty(Denominazione)
  return 'N'
endcase
* --- Fine Area Manuale
