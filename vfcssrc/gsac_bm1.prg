* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bm1                                                        *
*              Eventi da detail dichiarazioni di produzione                    *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-07                                                      *
* Last revis.: 2017-03-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,TipOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bm1",oParentObject,m.TipOpe)
return(i_retval)

define class tgsac_bm1 as StdBatch
  * --- Local variables
  TipOpe = space(15)
  w_GSAC_MMP = .NULL.
  GSDB_MCO = .NULL.
  GSAC_MLO = .NULL.
  GSDB_MCM = .NULL.
  GSAC_MDV = .NULL.
  w_QTAUM1 = 0
  w_RDCOMMAT = space(1)
  w_EDCOMMAT = .f.
  w_MATENABL = .f.
  w_RIGCUR = 0
  w_MATGES = space(1)
  w_FLLOTTI = space(1)
  w_FLUBIC = space(1)
  w_DESARTI = space(40)
  NUMREC = 0
  w_RDCODODL = space(15)
  w_RDFASODL = 0
  w_MODEVA = .f.
  w_WIPNETT = space(1)
  w_CODMAG = space(5)
  w_QTAPPO = 0
  w_ODLRIG = 0
  w_QTAMOV = 0
  w_UM1 = space(3)
  w_UM2 = space(3)
  w_UM3 = space(3)
  w_OP = space(1)
  w_MOLT = 0
  w_OP3 = space(1)
  w_MOLT3 = 0
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  w_MVCODCON = space(15)
  w_MVQTAUM1 = 0
  w_MVQTAEV1 = 0
  w_MVCODCOM = space(15)
  w_STATPAD = space(10)
  w_MVSERIAL = space(15)
  w_AVASERIA = space(15)
  w_CARICA = .f.
  w_CPROW = 0
  w_NUMRIF = 0
  w_TMPc = space(10)
  w_EVAS = space(1)
  w_STATRIG = space(1)
  radice = space(240)
  w_ODLRIG = 0
  riga = 0
  w_QTARES = 0
  w_QTSALUM = 0
  w_MVSEROCL = space(15)
  w_SEROCL = space(15)
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  w_CLARIF = space(2)
  w_ODLRIF = space(10)
  w_KO = .f.
  w_Msg = space(100)
  w_QTASAL = 0
  w_TMPSEROCL = space(15)
  w_SEDOC = space(10)
  w_CODMAT = space(40)
  w_KEYSAL = space(20)
  * --- WorkFile variables
  ART_ICOL_idx=0
  MAT_PROD_idx=0
  CONTI_idx=0
  MAGAZZIN_idx=0
  SALDIART_idx=0
  DOC_DETT_idx=0
  MATRICOL_idx=0
  MOVIMATR_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Gestione GSAC_MMP
    * --- Locals
    * --- Callers
    * --- Riferimenti agli oggetti correlati
    *     ------------------------------------------------
    if this.TipOpe="Ricalcola"
      * --- Documenti
      this.GSAC_MDV = this.oParentObject.oparentobject
      * --- Dettaglio Componenti Dichiarazioni
      this.w_GSAC_MMP = this.GSAC_MDV.GSAC_MMP.cnt
      * --- Consuntivazione Lotti/Matricole
      this.GSAC_MLO = this.w_GSAC_MMP.GSAC_MLO
    else
      * --- Padre-Dettaglio Componenti Dichiarazioni
      this.w_GSAC_MMP = this.oParentObject
      * --- Nonno - Documenti
      this.GSAC_MDV = this.w_GSAC_MMP.oparentobject
      * --- Figlio del Padre - Consuntivazione Lotti/Matricole
      this.GSAC_MLO = this.w_GSAC_MMP.GSAC_MLO
    endif
    * --- ------------------------------------------------
    this.w_STATPAD = this.GSAC_MDV.cfunction
    this.w_MVSERIAL = this.GSAC_MDV.w_MVSERIAL
    this.w_MVSERIAL = alltrim(this.w_MVSERIAL)
    this.w_MVSERRIF = this.GSAC_MDV.w_MVSERRIF
    this.w_MVROWRIF = this.GSAC_MDV.w_MVROWRIF
    this.w_MVCODCON = this.GSAC_MDV.w_MVCODCON
    this.w_MVCODCOM = this.GSAC_MDV.w_MVCODCOM
    this.w_MVQTAUM1 = this.GSAC_MDV.w_MVQTAUM1
    this.w_MVQTAEV1 = this.GSAC_MDV.w_MVQTAEV1
    this.w_QTAUM1 = NVL(this.w_MVQTAUM1-this.w_MVQTAEV1, 0)
    this.w_CPROW = this.GSAC_MDV.w_CPROWNUM
    this.w_NUMRIF = -30
    do case
      case this.TipOpe = "QTAMOV"
        * --- Ricalcolo il coeff. di impiego a seguito della modifica della q.t� da scaricare
        if this.w_QTAUM1<>0
          this.oParentObject.w_MPCOEIMP = cp_round(this.oParentObject.w_MPQTAMOV/this.w_QTAUM1,5)
        endif
        * --- Notifica al padre la avvenuta modifica di w_MPCOEIMP
        this.w_GSAC_MMP.notifyevent("w_MPCOEIMP Changed")     
      case this.TipOpe="AGGLOT" and (g_PERLOT="S" AND this.oParentObject.w_FLLOTT<>"N" and nvl(this.oParentObject.w_MATCOM,"N")<>"S")
        this.GSAC_MLO.LinkPCClick()     
        * --- Nuovo Oggetto
        this.GSAC_MLO = this.w_GSAC_MMP.GSAC_MLO
        this.GSAC_MLO.Hide()     
        this.GSAC_MLO = this.w_GSAC_MMP.GSAC_MLO.cnt
        NC = this.GSAC_MLO.cTrsName
        SELECT (NC)
        GO TOP
        count for not deleted() to this.numrec
        if this.numrec>0
          ah_msg("Ricalcolo dettaglio lotti in corso...")
          SELECT (NC)
          GO TOP
          DELETE ALL
          this.GSAC_MLO.w_TOTS = 0
          this.GSAC_MLO.w_TOTC = 0
          this.GSAC_MLO.w_TOTQTA = 0
          this.GSAC_MLO.initrow()     
          this.GSAC_MLO.notifyevent("Ricalcola")     
          this.GSAC_MLO.ecpsave()     
        endif
      case this.TipOpe="OPEN" or this.TipOpe="Ricalcola"
        * --- Read from MAT_PROD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAT_PROD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2],.t.,this.MAT_PROD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" MAT_PROD where ";
                +"MPSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
                +" and MPROWDOC = "+cp_ToStrODBC(this.w_CPROW);
                +" and MPNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                MPSERIAL = this.w_MVSERIAL;
                and MPROWDOC = this.w_CPROW;
                and MPNUMRIF = this.w_NUMRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows=0
          if this.w_STATPAD<>"Query" 
            * --- In alcuni casi (cambio riga in modifica senza chiudere il GSAC_MMP) devo chiudere la maschera, ripeto il controllo su DOC_DETT.
            this.w_MVSERRIF = NVL(this.GSAC_MDV.GET("t_MVSERRIF"), space(10))
            this.w_MVROWRIF = NVL(this.GSAC_MDV.GET("t_MVROWRIF"), 0)
            this.w_CLARIF = NVL(this.GSAC_MDV.GET("t_CLARIF"), space(2))
            this.w_ODLRIF = NVL(this.GSAC_MDV.GET("t_ODLRIF"), space(10))
            * --- Seriale OCL di origine
            this.w_MVSEROCL = SPACE(15)
            this.w_TMPSEROCL = SPACE(15)
            * --- Read from DOC_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVCODODL"+;
                " from "+i_cTable+" DOC_DETT where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVCODODL;
                from (i_cTable) where;
                    MVSERIAL = this.w_MVSERRIF;
                    and CPROWNUM = this.w_MVROWRIF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TMPSEROCL = NVL(cp_ToDate(_read_.MVCODODL),cp_NullValue(_read_.MVCODODL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if g_PROD="S"
              * --- Select from gsac2bm1
              do vq_exec with 'gsac2bm1',this,'_Curs_gsac2bm1','',.f.,.t.
              if used('_Curs_gsac2bm1')
                select _Curs_gsac2bm1
                locate for 1=1
                do while not(eof())
                this.w_MVSEROCL = NVL(_Curs_gsac2bm1.MVSEROCL , SPACE(15))
                  select _Curs_gsac2bm1
                  continue
                enddo
                use
              endif
            endif
            if not empty(this.w_MVSEROCL)
              this.w_CARICA = True
            endif
          endif
        else
          if this.TipOpe="Ricalcola"
            this.w_CARICA = True
          endif
        endif
        if this.w_CARICA
          this.w_GSAC_MMP.w_MPSERIAL = this.w_MVSERIAL
          this.w_GSAC_MMP.w_MPROWDOC = this.w_CPROW
          this.w_GSAC_MMP.w_MPNUMRIF = this.w_NUMRIF
          this.w_GSAC_MMP.w_CPROWNUM = 0
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.TipOpe="AGGIO"
        * --- Solo se il padre � in modifica setto i_srv su DOC_DETT ad 'U' per forzare la partenza dei controlli del Conto Lavoro (GSCO_BRL) da GSAC_MDV
        if this.w_STATPAD="Edit"
          this.GSAC_MDV.MARKPOS()     
          this.w_STATRIG = this.GSAC_MDV.GET("i_srv")
          if upper(this.w_STATRIG)<>"A"
            this.GSAC_MDV.SETUPDATEROW()     
          endif
          this.GSAC_MDV.REPOS()     
        endif
      case this.TipOpe="CHKFINAL"
        * --- Controllo che le quantit� di riga siano congruenti con quelle dichiarate nei figli lotti e matricole
        if g_MATR="S" AND nvl(this.oParentObject.w_MATCOM,"N")="S" and g_DATMAT<=this.oParentObject.w_DATREG
          * --- Select from GSAC_MMP
          do vq_exec with 'GSAC_MMP',this,'_Curs_GSAC_MMP','',.f.,.t.
          if used('_Curs_GSAC_MMP')
            select _Curs_GSAC_MMP
            locate for 1=1
            do while not(eof())
            if this.oParentObject.w_MPQTAMOV <> _Curs_GSAC_MMP.NUMMAT and _Curs_GSAC_MMP.NUMMAT<>0
              this.w_KO = .t.
              this.w_Msg = "Per il componente %1 sono state dichiarate %2 matricole anzich� %3"
              this.w_Msg = Ah_msgformat(this.w_Msg,ALLTRIM(_Curs_GSAC_MMP.MPCODART),ALLTRIM(STR(_Curs_GSAC_MMP.NUMMAT)),alltrim(trans(this.oParentObject.w_MPQTAMOV,v_pv(32))))
            endif
              select _Curs_GSAC_MMP
              continue
            enddo
            use
          endif
        endif
        if (g_PERLOT="S" AND NVL(this.oParentObject.w_FLLOTT,"N")<>"N" or g_PERUBI="S" AND this.oParentObject.w_FLUBI="S") and nvl(this.oParentObject.w_MATCOM,"N")<>"S"
          * --- Select from GSAC1MMP
          do vq_exec with 'GSAC1MMP',this,'_Curs_GSAC1MMP','',.f.,.t.
          if used('_Curs_GSAC1MMP')
            select _Curs_GSAC1MMP
            locate for 1=1
            do while not(eof())
            if this.oParentObject.w_MPQTAMOV <> _Curs_GSAC1MMP.QTALOT and _Curs_GSAC1MMP.QTALOT<>0
              this.w_KO = .t.
              this.w_Msg = "Per il componente %1 sono state dichiarate quantit� incongruenti: %2 anzich� %3"
              this.w_Msg = Ah_msgformat(this.w_Msg,ALLTRIM(_Curs_GSAC1MMP.MPCODART),ALLTRIM(STR(_Curs_GSAC1MMP.QTALOT)),alltrim(trans(this.oParentObject.w_MPQTAMOV,v_pv(32))))
            endif
              select _Curs_GSAC1MMP
              continue
            enddo
            use
          endif
        endif
        if this.w_KO
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_Msg
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica/Ricarica Lista Materiali
    if g_PROD="S"
      this.w_MVSERRIF = NVL(this.GSAC_MDV.GET("t_MVSERRIF"), space(10))
      this.w_MVROWRIF = NVL(this.GSAC_MDV.GET("t_MVROWRIF"), 0)
      this.w_CLARIF = NVL(this.GSAC_MDV.GET("t_CLARIF"), space(2))
      this.w_ODLRIF = NVL(this.GSAC_MDV.GET("t_ODLRIF"), space(10))
      * --- Seriale OCL di origine
      this.w_RDCODODL = SPACE(15)
      this.w_TMPSEROCL = SPACE(15)
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVCODODL"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVCODODL;
          from (i_cTable) where;
              MVSERIAL = this.w_MVSERRIF;
              and CPROWNUM = this.w_MVROWRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TMPSEROCL = NVL(cp_ToDate(_read_.MVCODODL),cp_NullValue(_read_.MVCODODL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Select from gsac2bm1
      do vq_exec with 'gsac2bm1',this,'_Curs_gsac2bm1','',.f.,.t.
      if used('_Curs_gsac2bm1')
        select _Curs_gsac2bm1
        locate for 1=1
        do while not(eof())
        this.w_RDCODODL = _Curs_gsac2bm1.MVSEROCL
          select _Curs_gsac2bm1
          continue
        enddo
        use
      endif
      this.w_RDFASODL = 0
      this.w_SEROCL = this.w_RDCODODL
      * --- Legge codice magazzino terzista (w_CODMAG)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANMAGTER"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC("F");
              +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANMAGTER;
          from (i_cTable) where;
              ANTIPCON = "F";
              and ANCODICE = this.w_MVCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODMAG = NVL(cp_ToDate(_read_.ANMAGTER),cp_NullValue(_read_.ANMAGTER))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Legge flag wip nettificabile
      * --- Read from MAGAZZIN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MGDISMAG,MGFLUBIC"+;
          " from "+i_cTable+" MAGAZZIN where ";
              +"MGCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MGDISMAG,MGFLUBIC;
          from (i_cTable) where;
              MGCODMAG = this.w_CODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_WIPNETT = NVL(cp_ToDate(_read_.MGDISMAG),cp_NullValue(_read_.MGDISMAG))
        this.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Lettura della lista Materiali da ODL_DETT
      vq_exec ("..\COLA\EXE\QUERY\GSCO2BSM.VQR", this, "MATPRO")
      * --- Azzera Cursore Movimentazione (ATTENZIONE NON USARE ZAP -- Fallisce "transazione" saldi)
      NC = this.w_GSAC_MMP.cTrsName
      SELECT (NC)
      GO TOP
      DELETE ALL
      if vartype(this.w_GSAC_MMP.GSAC_MLO.cnt) = "O"
        NC = this.w_GSAC_MMP.GSAC_MLO.cnt.cTrsName
        SELECT (NC)
        GO TOP
        DELETE ALL
      endif
      * --- Tolgo il flag prenotato dalle matricole utilizzate in precedenza
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVSERIAL"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERDDT = "+cp_ToStrODBC(this.w_MVSERIAL);
              +" and MVROWDDT = "+cp_ToStrODBC(this.w_CPROW);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVSERIAL;
          from (i_cTable) where;
              MVSERDDT = this.w_MVSERIAL;
              and MVROWDDT = this.w_CPROW;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SEDOC = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Write into MOVIMATR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MOVIMATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MTSERIAL,MTROWNUM,MTKEYSAL,MTCODMAT"
        do vq_exec with 'gsac1bm1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MOVIMATR_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
            +i_ccchkf;
            +" from "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR, "+i_cQueryTable+" _t2 set ";
        +"MOVIMATR.MT_SALDO ="+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
            +Iif(Empty(i_ccchkf),"",",MOVIMATR.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="MOVIMATR.MTSERIAL = t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = t2.MTROWNUM";
                +" and "+"MOVIMATR.MTKEYSAL = t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = t2.MTCODMAT";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set (";
            +"MT_SALDO";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="MOVIMATR.MTSERIAL = _t2.MTSERIAL";
                +" and "+"MOVIMATR.MTROWNUM = _t2.MTROWNUM";
                +" and "+"MOVIMATR.MTKEYSAL = _t2.MTKEYSAL";
                +" and "+"MOVIMATR.MTCODMAT = _t2.MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" MOVIMATR set ";
        +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MTSERIAL = "+i_cQueryTable+".MTSERIAL";
                +" and "+i_cTable+".MTROWNUM = "+i_cQueryTable+".MTROWNUM";
                +" and "+i_cTable+".MTKEYSAL = "+i_cQueryTable+".MTKEYSAL";
                +" and "+i_cTable+".MTCODMAT = "+i_cQueryTable+".MTCODMAT";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MT_SALDO ="+cp_NullLink(cp_ToStrODBC(0),'MOVIMATR','MT_SALDO');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Delete from MOVIMATR
      i_nConn=i_TableProp[this.MOVIMATR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MTSERIAL = "+cp_ToStrODBC(this.w_SEDOC);
              +" and MTNUMRIF = "+cp_ToStrODBC(-20);
               )
      else
        delete from (i_cTable) where;
              MTSERIAL = this.w_SEDOC;
              and MTNUMRIF = -20;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      if vartype(this.w_GSAC_MMP.GSAC_MCM.cnt) = "O" and .f.
        NC = this.w_GSAC_MMP.GSAC_MCM.cnt.cTrsName
        SELECT (NC)
        GO TOP
        SCAN
        this.w_CODMAT = NVL(MTCODMAT,"")
        this.w_KEYSAL = NVL(MTKEYSAL,"")
        if !EMPTY(this.w_CODMAT) and !EMPTY(this.w_KEYSAL)
          * --- Write into MATRICOL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MATRICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MATRICOL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MATRICOL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AM_PRODU ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRODU');
            +",AM_PRENO ="+cp_NullLink(cp_ToStrODBC(0),'MATRICOL','AM_PRENO');
                +i_ccchkf ;
            +" where ";
                +"AMCODICE = "+cp_ToStrODBC(this.w_CODMAT);
                +" and AMKEYSAL = "+cp_ToStrODBC(this.w_KEYSAL);
                   )
          else
            update (i_cTable) set;
                AM_PRODU = 0;
                ,AM_PRENO = 0;
                &i_ccchkf. ;
             where;
                AMCODICE = this.w_CODMAT;
                and AMKEYSAL = this.w_KEYSAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Delete from MOVIMATR
          i_nConn=i_TableProp[this.MOVIMATR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"MTSERIAL = "+cp_ToStrODBC(w_SERSCA);
                  +" and MTROWNUM = "+cp_ToStrODBC(w_ROWSCA);
                  +" and MTNUMRIF = "+cp_ToStrODBC(-20);
                   )
          else
            delete from (i_cTable) where;
                  MTSERIAL = w_SERSCA;
                  and MTROWNUM = w_ROWSCA;
                  and MTNUMRIF = -20;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        ENDSCAN
        GO TOP
        DELETE ALL
      endif
      if USED("MATPRO")
        SELECT MATPRO
        GO TOP
        SCAN FOR NOT EMPTY(NVL(PDCODICE,""))
        SELECT MATPRO
        * --- E' un fantasma?
        if not empty(nvl(OLPROFAN," ")) and ARTIPART="PH"
          * --- Mi memorizzo la radice della catena del fantasma
          this.radice = OLPROFAN
          * --- Se l'articolo in esame � un fantasma (o figlio di un fantasma) deve essere consumato finch� ce n'� disponibile a magazzino, poi si passa 
          *     l'impegno ai suoi figli.
          this.w_ODLRIG = MATPRO.PDROWNUM
          * --- Select from MAT_PROD
          i_nConn=i_TableProp[this.MAT_PROD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAT_PROD_idx,2],.t.,this.MAT_PROD_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select sum(MPQTAMOV) as QTAMOV  from "+i_cTable+" MAT_PROD ";
                +" where MPSERODL="+cp_ToStrODBC(this.w_RDCODODL)+" and MPROWODL="+cp_ToStrODBC(this.w_ODLRIG)+" and MPNUMRIF=-10 AND MPSERIAL<>PUNMAT.w_MPSERIAL";
                 ,"_Curs_MAT_PROD")
          else
            select sum(MPQTAMOV) as QTAMOV from (i_cTable);
             where MPSERODL=this.w_RDCODODL and MPROWODL=this.w_ODLRIG and MPNUMRIF=-10 AND MPSERIAL<>PUNMAT.w_MPSERIAL;
              into cursor _Curs_MAT_PROD
          endif
          if used('_Curs_MAT_PROD')
            select _Curs_MAT_PROD
            locate for 1=1
            do while not(eof())
            * --- Totale scarichi (esclusa la presente dichiarazione)
            this.w_QTAMOV = nvl(_Curs_MAT_PROD.QTAMOV,0)
              select _Curs_MAT_PROD
              continue
            enddo
            use
          endif
          SELECT MATPRO
          this.riga = recno()
          * --- Verifico se la quantit� disponibile del fantasma � sufficiente a coprire l'impegno residuo
          if (NVL(OLCOEIMP, 0)* iif(qtares<>0,qtares,this.w_QTAUM1))>(PDQTAORD-this.w_QTAMOV) and OLFLELAB $ "S-F"
            * --- La quantit� disponibile non � sufficiente, devo considerare anche i/il figlio
            * --- Quantit� che resta da evadere
            this.w_QTARES = max((OLCOEIMP * iif(qtares<>0,qtares,this.w_QTAUM1))-(PDQTAORD-this.w_QTAMOV),0)/OLCOEIMP
            replace qtares with this.w_QTARES
            * --- Setto la corretta quantit� anche per tutti i figli
            update MATPRO set OLFLELAB="F" ,PDQTAORD= iif(artipart="PH",min(this.w_qtares*OLCOEIMP,PDQTAORD),this.w_qtares*OLCOEIMP),QTARES=this.w_QTARES where OLPROFAN=alltrim(this.radice)+"."
            * --- Mi riposiziono sulla riga corretta
            SELECT MATPRO 
 go this.riga
            if min(OLCOEIMP * this.w_QTAUM1,(PDQTAORD-this.w_QTAMOV))>0
              update MATPRO set OLFLELAB="F" ,PDQTAORD=min(OLCOEIMP * this.w_QTAUM1,(PDQTAORD-this.w_QTAMOV)) where OLPROFAN==alltrim(this.radice)
            else
              update MATPRO set OLFLELAB="N" ,PDQTAORD=min(OLCOEIMP * this.w_QTAUM1,(PDQTAORD-this.w_QTAMOV)) where OLPROFAN==alltrim(this.radice)
            endif
          else
            * --- La quantit� disponibile � sufficiente, non devo considerare anche i/il figlio
            update MATPRO set OLFLELAB="N" where OLPROFAN=alltrim(this.radice)+"."
          endif
          SELECT MATPRO 
 go this.riga
        endif
        if OLFLELAB $ "S-F"
          * --- Nuova Riga del Temporaneo
          this.w_GSAC_MMP.InitRow()     
          SELECT MATPRO
          * --- Valorizza Variabili di Riga ...
          this.w_GSAC_MMP.w_MPSERIAL = this.w_MVSERIAL
          this.w_GSAC_MMP.w_CPROWORD = NVL(CPROWORD, 0)
          this.w_GSAC_MMP.w_MPCODICE = PDCODICE
          this.w_GSAC_MMP.w_MPUNIMIS = NVL(PDUMORDI, "   ")
          this.w_GSAC_MMP.w_MPCODMAG = this.w_CODMAG
          this.w_GSAC_MMP.w_DESART = NVL(CADESART, SPACE(40))
          this.w_GSAC_MMP.w_MPCODART = NVL(PDCODART, SPACE(20))
          this.w_GSAC_MMP.w_MPKEYSAL = NVL(PDCODART, SPACE(20))
          this.w_GSAC_MMP.w_FLLOTT = NVL(ARFLLOTT, SPACE(1))
          this.w_GSAC_MMP.w_FLUBI = nvl(this.w_FLUBIC," ")
          this.w_GSAC_MMP.w_MATCOM = NVL(ARGESMAT, SPACE(1))
          this.w_UM1 = NVL(ARUNMIS1, "   ")
          this.w_UM2 = NVL(ARUNMIS2, "   ")
          this.w_UM3 = NVL(CAUNIMIS, "   ")
          this.w_OP = NVL(AROPERAT, " ")
          this.w_MOLT = NVL(ARMOLTIP, 0)
          this.w_OP3 = NVL(CAOPERAT, " ")
          this.w_MOLT3 = NVL(CAMOLTIP, 0)
          * --- Calcolo quantit�
          this.w_QTASAL = NVL(OLQTASAL,0)
          if NVL(PDUMORDI, "   ")<>this.w_UM1
            * --- Ricalcolo quantit� nell'Unit� di misura principale
            this.w_QTAPPO = CALQTA( NVL(this.w_QTASAL, 0)* this.w_QTAUM1, NVL(PDUMORDI, "   ") ,iif(PDUMORDI=this.w_UM2,this.w_UM2,space(3)), IIF(this.w_OP="/","*","/"), this.w_MOLT, "", "", "", , this.w_UM3, IIF(this.w_OP3="/","*","/"), this.w_MOLT3)
          else
            this.w_QTAPPO = this.w_QTASAL
          endif
          this.w_GSAC_MMP.w_MPCOEIMP = NVL(OLCOEIMP, 0)
          this.w_GSAC_MMP.w_MPQTAMOV = NVL(OLCOEIMP, 0)* this.w_QTAUM1
          this.w_GSAC_MMP.w_MPQTAUM1 = NVL(OLCOEIMP, 0)* this.w_QTAUM1
          this.w_GSAC_MMP.w_MPQTAEVA = NVL(OLCOEIMP, 0)* this.w_QTAUM1
          this.w_GSAC_MMP.w_MPQTAEV1 = NVL(OLCOEIMP, 0)* this.w_QTAUM1
          this.w_GSAC_MMP.w_UNMIS1 = this.w_UM1
          this.w_GSAC_MMP.w_UNMIS2 = this.w_UM2
          this.w_GSAC_MMP.w_UNMIS3 = this.w_UM3
          this.w_GSAC_MMP.w_OPERAT = this.w_OP
          this.w_GSAC_MMP.w_OPERA3 = this.w_OP3
          this.w_GSAC_MMP.w_MOLTIP = this.w_MOLT
          this.w_GSAC_MMP.w_MOLTI3 = this.w_MOLT3
          this.w_GSAC_MMP.w_MPSERODL = NVL(PDORDINE, space(15))
          this.w_GSAC_MMP.w_MPROWODL = NVL(PDROWNUM, 0)
          if NVL(PDUMORDI, "   ")<>this.w_UM1
            * --- Ricalcolo quantit� nell'Unit� di misura principale
            this.w_QTAPPO = CALQTA( NVL(OLCOEIMP, 0)* this.w_QTAUM1, NVL(PDUMORDI, "   ") ,this.w_UM2, this.w_OP, this.w_MOLT, "", "", "", , this.w_UM3, this.w_OP3, this.w_MOLT3)
            this.w_GSAC_MMP.w_MPQTAEV1 = NVL(this.w_QTAPPO, 0)
            this.w_GSAC_MMP.w_MPQTAUM1 = NVL(this.w_QTAPPO, 0)
          endif
          this.w_GSAC_MMP.w_MPFLEVAS = iif(this.GSAC_MDV.w_MVFLEVAS="S" or this.GSAC_MDV.w_MVFLERIF="S","S"," ")
          this.w_GSAC_MMP.TrsFromWork()     
        endif
        * --- Carica il Temporaneo dei Dati e skippa al record successivo
        SELECT MATPRO
        ENDSCAN
        * --- Rinfrescamenti vari
        SELECT (NC) 
 GO TOP 
 With this.w_GSAC_MMP 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .WorkFromTrs() 
 .SetControlsValue() 
 .mHideControls() 
 .ChildrenChangeRow() 
 .SaveDependsOn() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow=1 
 .oPgFrm.Page1.oPag.oBody.nRelRow=1 
 .bUpDated = TRUE 
 EndWith
        SELECT MATPRO
        USE
      endif
      if this.TipOpe="Ricalcola"
        ah_ErrorMsg("Aggiornata lista materiali ",48)
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,TipOpe)
    this.TipOpe=TipOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='MAT_PROD'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='SALDIART'
    this.cWorkTables[6]='DOC_DETT'
    this.cWorkTables[7]='MATRICOL'
    this.cWorkTables[8]='MOVIMATR'
    this.cWorkTables[9]='DOC_MAST'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_gsac2bm1')
      use in _Curs_gsac2bm1
    endif
    if used('_Curs_GSAC_MMP')
      use in _Curs_GSAC_MMP
    endif
    if used('_Curs_GSAC1MMP')
      use in _Curs_GSAC1MMP
    endif
    if used('_Curs_gsac2bm1')
      use in _Curs_gsac2bm1
    endif
    if used('_Curs_MAT_PROD')
      use in _Curs_MAT_PROD
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="TipOpe"
endproc
