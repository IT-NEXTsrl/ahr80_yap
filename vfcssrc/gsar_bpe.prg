* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bpe                                                        *
*              Controlli su gstione pagamenti esclusi                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-02                                                      *
* Last revis.: 2008-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bpe",oParentObject)
return(i_retval)

define class tgsar_bpe as StdBatch
  * --- Local variables
  w_OK = .f.
  w_SERIAL = space(10)
  w_PADRE = .NULL.
  * --- WorkFile variables
  PAGMESCL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chk Finali gestione PAgamenti esclusi  GSCG_MPE
    this.w_OK = .t.
    * --- Select from PAGMESCL
    i_nConn=i_TableProp[this.PAGMESCL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAGMESCL_idx,2],.t.,this.PAGMESCL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select PACATCON,PACODCON,PAMASTRO,PATIPCON,PASERIAL  from "+i_cTable+" PAGMESCL ";
           ,"_Curs_PAGMESCL")
    else
      select PACATCON,PACODCON,PAMASTRO,PATIPCON,PASERIAL from (i_cTable);
        into cursor _Curs_PAGMESCL
    endif
    if used('_Curs_PAGMESCL')
      select _Curs_PAGMESCL
      locate for 1=1
      do while not(eof())
      if Nvl(_Curs_PAGMESCL.PACATCON,Space(5))=this.oParentObject.w_PACATCON AND Nvl(_Curs_PAGMESCL.PATIPCON,Space(1))=this.oParentObject.w_PATIPCON AND; 
 Nvl(_Curs_PAGMESCL.PACODCON,Space(15))=this.oParentObject.w_PACODCON AND Nvl(_Curs_PAGMESCL.PAMASTRO,Space(15))=this.oParentObject.w_PAMASTRO; 
 AND _Curs_PAGMESCL.PASERIAL<>this.oParentObject.w_PASERIAL
        this.w_OK = .f.
        this.oParentObject.w_MESS = ah_Msgformat("Attenzione: esiste gi� una configurazione con le stesse caratterisitiche, impossibile salvare")
        exit
      endif
        select _Curs_PAGMESCL
        continue
      enddo
      use
    endif
    if this.w_OK
      * --- Controllo numero di righe del dettaglio
      this.w_PADRE = This.Oparentobject
      this.w_PADRE.Markpos()     
      if this.w_PADRE.NumRow()=0
        this.oParentObject.w_MESS = ah_Msgformat("Attenzione: inserire almeno una riga di dettaglio pagamento")
        this.w_OK = .f.
      endif
      this.w_PADRE.Repos()     
    endif
    * --- Se .F. operazione non possibile...
    i_retcode = 'stop'
    i_retval = this.w_OK
    return
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAGMESCL'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_PAGMESCL')
      use in _Curs_PAGMESCL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
