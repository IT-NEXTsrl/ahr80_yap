* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bif                                                        *
*              IMPORT MASSIVO DI FILE EDI                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-19                                                      *
* Last revis.: 2015-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bif",oParentObject)
return(i_retval)

define class tgsva_bif as StdBatch
  * --- Local variables
  w_FLFREEZE = space(1)
  w_IMSSTEPA = space(1)
  w_IMSSTEPB = space(1)
  w_IMSSTEPC = space(1)
  w_SER_ELAB = space(10)
  w_STCODICE = space(10)
  w_LOOP = 0
  w_LMsg = space(0)
  w_FILEINGR = space(200)
  w_FLVERBOS = .f.
  w_CUR_STEP = 0
  w_IMSERIAL = space(10)
  w_PADRE = .NULL.
  w_OBJ__XML = .NULL.
  w_Msg = space(0)
  w_FLT_ELAB = space(1)
  w_TOT_STEP = 0
  w_FILE_XML = space(254)
  w_STDESCRI = space(30)
  w_STMODMSK = space(30)
  w_STTIPGES = space(1)
  w_STBACIMP = space(30)
  w_STDRIVER = space(30)
  w_STBACXML = space(30)
  w_STFUNRIC = space(30)
  w_ONLY_VERIF = .f.
  w_OKSTEP = .f.
  w_OLDSTR = space(10)
  w_LMSG = space(0)
  w_CURSORE = space(30)
  w_PFPATTER = space(50)
  w_PFSUBDIR = space(1)
  w_PFDIRFIL = space(254)
  w_ORDSTR = 0
  w_RECATT = 0
  w_ONLYCONF = .f.
  w_CHKELAB = .f.
  w_ERRORE_ASS = .f.
  w_OKELAB = .f.
  * --- WorkFile variables
  VASTRUTT_idx=0
  VAANAIMP_idx=0
  PAT_FILE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- esegue import  massivo da file EDI da GSVA_KIF
    this.w_PADRE = This.oparentobject
    this.w_PADRE.w_MSG = ""
    this.oParentObject.w_FILEELAB = ""
    this.w_FLFREEZE = this.w_PADRE.w_FLFREEZE
    this.w_ONLYCONF = .F.
    if Empty(Nvl(this.oParentObject.w_DIRSPO," ")) or Empty(Nvl(this.oParentObject.w_DIRXML," ")) OR Empty(Nvl(this.oParentObject.w_DIRFIL," ")) or ! Directory(Alltrim(this.oParentObject.w_DIRFIL)) or ! Directory(Alltrim(this.oParentObject.w_DIRXML)) or ! Directory(Alltrim(this.oParentObject.w_DIRSPO))
      AddMsgNL("%0Attenzione! uno o pi� percorsi nei parametri non sono corretti oppure sono inesistenti!",this.w_PADRE)
      i_retcode = 'stop'
      return
    endif
    AddMsgNL("Elaborazione generale iniziata alle %1",This, Time() , , , ,, )
    * --- Se ho granularit�  nessuna posso eseguire ultimo step senza sola verifica
     
 Create Cursor ELE_STR (CODSTR C(10))
    this.w_ONLY_VERIF = !(this.oParentObject.w_GENERR$ "N-C")
    this.w_OLDSTR = "@@@@@@@@@@@"
    this.w_FLVERBOS = this.oParentObject.w_FLAGLOG
    * --- Preparo il cursore dei file da analizzare
    this.w_CURSORE = Sys(2015)
     
 CREATE CURSOR (this.w_CURSORE) (FILE C(254),CODSTR C(10), ORDSTR N(3,0) ,DATA D(8))
    * --- Select from VASTRUTT
    i_nConn=i_TableProp[this.VASTRUTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select STCODICE,STORDIMP  from "+i_cTable+" VASTRUTT ";
          +" where STFLIMPO='S'";
          +" order by STORDIMP";
           ,"_Curs_VASTRUTT")
    else
      select STCODICE,STORDIMP from (i_cTable);
       where STFLIMPO="S";
       order by STORDIMP;
        into cursor _Curs_VASTRUTT
    endif
    if used('_Curs_VASTRUTT')
      select _Curs_VASTRUTT
      locate for 1=1
      do while not(eof())
      this.w_STCODICE = ALLTRIM(_Curs_VASTRUTT.STCODICE)
      this.w_ORDSTR = NVL(_Curs_VASTRUTT.STORDIMP, 0)
      * --- Select from PAT_FILE
      i_nConn=i_TableProp[this.PAT_FILE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAT_FILE_idx,2],.t.,this.PAT_FILE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PFPATTER,PFSUBDIR,PFDIRFIL  from "+i_cTable+" PAT_FILE ";
            +" where PFCODSTR="+cp_ToStrODBC(this.w_STCODICE)+"";
             ,"_Curs_PAT_FILE")
      else
        select PFPATTER,PFSUBDIR,PFDIRFIL from (i_cTable);
         where PFCODSTR=this.w_STCODICE;
          into cursor _Curs_PAT_FILE
      endif
      if used('_Curs_PAT_FILE')
        select _Curs_PAT_FILE
        locate for 1=1
        do while not(eof())
        this.w_PFPATTER = ALLTRIM(_Curs_PAT_FILE.PFPATTER)
        this.w_PFSUBDIR = ALLTRIM(NVL(_Curs_PAT_FILE.PFSUBDIR,"N"))
        this.w_PFSUBDIR = iif(!Empty(this.w_PFSUBDIR),this.w_PFSUBDIR,"N")
        this.w_PFDIRFIL = ADDBS(ALLTRIM(_Curs_PAT_FILE.PFDIRFIL))
        if !EMPTY(NVL(this.w_PFPATTER," ")) and ((Cp_Fileexist(Substr(ADDBS(Alltrim(this.w_PFDIRFIL)),1,Len(ADDBS(Alltrim(this.w_PFDIRFIL)))-1)) and !EMPTY(NVL(this.w_PFDIRFIL," "))) or EMPTY(NVL(this.w_PFDIRFIL," ")))
          NC=CreaCurFile(IIF(EMPTY(NVL(this.w_PFDIRFIL," ")), this.oParentObject.w_DIRFIL , this.w_PFDIRFIL ) , this.w_PFPATTER, this.w_STCODICE, this.w_ORDSTR)
          if Used((NC)) and Reccount((NC)) <>0
            INSERT INTO (this.w_CURSORE) SELECT * FROM (NC)
            USE IN (NC)
          endif
        endif
          select _Curs_PAT_FILE
          continue
        enddo
        use
      endif
      this.w_STCODICE = ""
        select _Curs_VASTRUTT
        continue
      enddo
      use
    endif
    this.w_OKELAB = .t.
    if Used((this.w_CURSORE)) and Reccount((this.w_CURSORE)) >0
      Select *, "N" AS FL_ELAB, SPACE(10) AS IMSERIAL from (this.w_CURSORE) into cursor (this.w_CURSORE) order by ORDSTR, CODSTR,DATA READWRITE
      Select (this.w_CURSORE) 
      Go top
      SCAN
      this.w_FILEINGR = ALLTRIM(FILE)
      this.oParentObject.w_FILEELAB = this.oParentObject.w_FILEELAB+ah_MsgFormat("Elaborazione file %1...", this.w_FILEINGR)
       oField = this.w_PADRE.getctrl("w_FILEELAB")
       this.w_PADRE.SetControlsValue()
       oField.SelStart = len(alltrim(this.oParentObject.w_FILEELAB))
      this.w_IMSERIAL = SPACE(10)
      this.w_CHKELAB = .T.
      if this.oParentObject.w_FLCHKFIL="S"
        * --- Read from VAANAIMP
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VAANAIMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2],.t.,this.VAANAIMP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" VAANAIMP where ";
                +"IMPATORI = "+cp_ToStrODBC(this.w_FILEINGR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                IMPATORI = this.w_FILEINGR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_rows>0
          this.w_CHKELAB = .F.
        endif
      endif
      if FL_ELAB="N" AND cp_fileexist(this.w_FILEINGR) AND this.w_CHKELAB
        this.w_FILEINGR = FILE
        this.w_STCODICE = CODSTR
        if this.w_STCODICE <> this.w_OLDSTR and ! this.w_ERRORE_ASS
          * --- Read from VASTRUTT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VASTRUTT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "STDESCRI,STMODMSK,STTIPGES,STBACIMP,STDRIVER,STBACXML,STFUNRIC"+;
              " from "+i_cTable+" VASTRUTT where ";
                  +"STCODICE = "+cp_ToStrODBC(this.w_STCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              STDESCRI,STMODMSK,STTIPGES,STBACIMP,STDRIVER,STBACXML,STFUNRIC;
              from (i_cTable) where;
                  STCODICE = this.w_STCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_STDESCRI = NVL(cp_ToDate(_read_.STDESCRI),cp_NullValue(_read_.STDESCRI))
            this.w_STMODMSK = NVL(cp_ToDate(_read_.STMODMSK),cp_NullValue(_read_.STMODMSK))
            this.w_STTIPGES = NVL(cp_ToDate(_read_.STTIPGES),cp_NullValue(_read_.STTIPGES))
            this.w_STBACIMP = NVL(cp_ToDate(_read_.STBACIMP),cp_NullValue(_read_.STBACIMP))
            this.w_STDRIVER = NVL(cp_ToDate(_read_.STDRIVER),cp_NullValue(_read_.STDRIVER))
            this.w_STBACXML = NVL(cp_ToDate(_read_.STBACXML),cp_NullValue(_read_.STBACXML))
            this.w_STFUNRIC = NVL(cp_ToDate(_read_.STFUNRIC),cp_NullValue(_read_.STFUNRIC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Verifico esito import ,  per applicare granularit� impostata 
          if this.w_OKSTEP
            this.oParentObject.w_FILEELAB = this.oParentObject.w_FILEELAB+ah_MsgFormat("Verifica OK!%0")
          else
            this.oParentObject.w_FILEELAB = this.oParentObject.w_FILEELAB+ah_MsgFormat("Errore!%0")
            if this.oParentObject.w_GENERR = "E" 
              this.w_ERRORE_ASS = .t.
            endif
            do case
              case this.oParentObject.w_GENERR="S"
                * --- Se granularit� per struttura devo passare al primo file della successiva struttura
                this.w_OLDSTR = this.w_STCODICE
                Insert Into ELE_STR (CODSTR) values (this.w_STCODICE)
              case this.oParentObject.w_GENERR="E"
                * --- Se granularit� per elaborazione  devo fermarmi al primo errore
                * --- Ho un errore non devo fare la conferma
                this.w_OKELAB = .f.
            endcase
          endif
          * --- Cumulo i Log
          this.w_PADRE.w_MSG = this.w_LMSG + this.w_PADRE.w_MSG
           oField = this.w_PADRE.getctrl("w_FILEELAB")
           this.w_PADRE.SetControlsValue()
           oField.SelStart = len(alltrim(this.oParentObject.w_FILEELAB))
        else
          this.w_PADRE.w_MSG = this.w_PADRE.w_MSG+this.w_MSG
          this.oParentObject.w_FILEELAB = this.oParentObject.w_FILEELAB+ah_MsgFormat("Non processato per errori su altro file!%0")
        endif
        Select (this.w_CURSORE)
        * --- Segno gi� processati eventuali file identici
        this.w_RECATT = RECNO()
        GO TOP
        UPDATE (this.w_CURSORE) SET FL_ELAB="S", IMSERIAL=this.w_IMSERIAL WHERE UPPER(ALLTRIM(FILE))=UPPER(ALLTRIM(this.w_FILEINGR))
        GO TOP
        GO this.w_RECATT
      else
        this.oParentObject.w_FILEELAB = this.oParentObject.w_FILEELAB+ah_MsgFormat("gi� processato!%0")
         oField = this.w_PADRE.getctrl("w_FILEELAB")
         this.w_PADRE.SetControlsValue()
         oField.SelStart = len(alltrim(this.oParentObject.w_FILEELAB))
        this.w_FILEINGR = ""
      endif
      ENDSCAN
      Delete from (this.w_CURSORE) where CODSTR in (Select CODSTR from ELE_STR)
      * --- Rieseguo ciclo per confermare strutture andate a buon fine 
      *     prima eseguita solo verifica nel terzo step
      *     Se avvenuto errore  tutti i file che fanno capo alla stessa struttura sono gi� esclusi
      if this.oParentObject.w_GENERR $ "S-E" and this.w_OKELAB
        this.w_ONLY_VERIF = .f.
        this.w_ONLYCONF = .T.
        Select * from (this.w_CURSORE) into cursor (this.w_CURSORE) order by ORDSTR, CODSTR,DATA
        Select (this.w_CURSORE) 
        GO TOP
        SCAN FOR FL_ELAB="S" AND !EMPTY(NVL(IMSERIAL, " "))
        this.w_STCODICE = CODSTR
        this.w_FILEINGR = FILE
        this.w_IMSERIAL = ALLTRIM(NVL(IMSERIAL," "))
        this.w_SER_ELAB = ALLTRIM(NVL(IMSERIAL," "))
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Cumulo i Log
        this.w_PADRE.w_MSG = this.w_LMSG + this.w_PADRE.w_MSG
         oField = this.w_PADRE.getctrl("w_FILEELAB")
         this.w_PADRE.SetControlsValue()
         oField.SelStart = len(alltrim(this.oParentObject.w_FILEELAB))
        Select (this.w_CURSORE)
        ENDSCAN
      endif
       oField = this.w_PADRE.getctrl("w_MSG")
       oField.SelStart = 1
       oField.REFRESH()
    else
      AddMsgNL("%0Attenzione nessun file rilevato nella directory selezionata ",This)
    endif
    if Used("ELE_STR")
      USE IN ELE_STR
    endif
    if Used((this.w_CURSORE))
      USE IN (this.w_CURSORE)
    endif
    AddMsgNL("%0Elaborazione generale finita alle %1",This, Time() , , , ,, )
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_LOOP = IIF(this.w_ONLYCONF, 2, 0)
    this.w_CUR_STEP = IIF(this.w_ONLYCONF, 3, 1)
    this.w_LMSG = this.w_PADRE.w_MSG
    this.w_OKSTEP = .t.
    this.w_IMSSTEPA = "N"
    this.w_IMSSTEPB = IIF(this.w_ONLYCONF, "O", "N")
    this.w_IMSSTEPC = "N"
    * --- Eseguo Step importazione
    do while this.w_LOOP < 4 and this.w_OKSTEP
      this.w_LOOP = this.w_LOOP + 1
      do case
        case this.w_LOOP=1 and this.w_IMSSTEPA="N"
          this.w_OKSTEP = .t.
        case this.w_LOOP=2
          this.w_OKSTEP = this.w_IMSSTEPA="O"
        case this.w_LOOP=3
          this.w_OKSTEP = this.w_IMSSTEPB="O"
        case this.w_LOOP=4 
          this.w_OKSTEP = this.w_IMSSTEPC="O" 
          this.w_PADRE.w_MSG = this.w_PADRE.w_MSG+this.w_MSG
      endcase
      if this.w_OKSTEP AND this.w_LOOP<4
        this.w_PADRE.w_MSG = ""
        gsva_bwi(this,"NEXT"," ",This.oparentobject)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    enddo
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='VAANAIMP'
    this.cWorkTables[3]='PAT_FILE'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_VASTRUTT')
      use in _Curs_VASTRUTT
    endif
    if used('_Curs_PAT_FILE')
      use in _Curs_PAT_FILE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
