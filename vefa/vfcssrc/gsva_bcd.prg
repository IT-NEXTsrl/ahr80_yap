* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bcd                                                        *
*              CONFERMA\VERIFICA DA WIZARD                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-08-31                                                      *
* Last revis.: 2015-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODSTR,pSERIAL,pLog,pOnlyVerif,pVerbose,pGranu
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bcd",oParentObject,m.pCODSTR,m.pSERIAL,m.pLog,m.pOnlyVerif,m.pVerbose,m.pGranu)
return(i_retval)

define class tgsva_bcd as StdBatch
  * --- Local variables
  pCODSTR = space(10)
  pSERIAL = space(10)
  pLog = .NULL.
  pOnlyVerif = .f.
  pVerbose = .f.
  pGranu = space(1)
  w_Msg = space(0)
  w_SER_ELAB = space(10)
  w_PARAM = space(50)
  w_FLTLOG = space(1)
  w_RISUL = space(1)
  w_FLRPZ = space(1)
  w_FLRAT = space(1)
  w_FLCAST = space(1)
  w_LGSERORI = space(10)
  w_LGROWORI = 0
  w_LGTIPRIG = space(0)
  w_TIPDOC = space(5)
  w_SERGDOC = space(10)
  w_FASE2 = .f.
  w_FLAGELA = space(1)
  w_DATELA = ctod("  /  /  ")
  w_ORAELA = space(8)
  w_CODAZI = space(5)
  w_IMPATORI = space(254)
  w_FILEORI = space(254)
  w_FILEDEST = space(254)
  w_IMFLELAB = space(1)
  w_OKLOG = .f.
  w_STEPC = space(1)
  * --- WorkFile variables
  LOG_GEND_idx=0
  VAANAIMP_idx=0
  PAR_VEFA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- codice struttura
    * --- Seriale importazione
    * --- Oggetto su cui scrivere il log
    * --- Se passato a true esegue solo la verifica
    *     utilizzato nel caso di import massivo
    * --- richiesta log dettaglito
    * --- GranularitÓ import massivo
    this.w_Msg = ""
    this.w_FLRPZ = "N"
    this.w_FLRAT = "S"
    this.w_FLCAST = "S"
    this.w_FLAGELA = "N"
    this.w_DATELA = .Null.
    this.w_ORAELA = ""
    this.w_SER_ELAB = this.pSERIAL
    GSVA_BI3(this,this.pSERIAL,this.pCODSTR,.t.)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    GSVA_BIM(this,"",this.pCODSTR,"VERIF"," ")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Verifico la presenza di errori 
    this.w_FLTLOG = IIF(this.pVERBOSE,"T","E")
    this.w_DATELA = cp_chartodate("  -  -    ")
    this.w_ORAELA = " "
    * --- Read from LOG_GEND
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.LOG_GEND_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOG_GEND_idx,2],.t.,this.LOG_GEND_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LGMESSAG"+;
        " from "+i_cTable+" LOG_GEND where ";
            +"LGSERIAL = "+cp_ToStrODBC(this.w_SERGDOC);
            +" and LGTIPRIG = "+cp_ToStrODBC("E");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LGMESSAG;
        from (i_cTable) where;
            LGSERIAL = this.w_SERGDOC;
            and LGTIPRIG = "E";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MSG = NVL(cp_ToDate(_read_.LGMESSAG),cp_NullValue(_read_.LGMESSAG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_RISUL = "E"
    if (i_ROWS=0 OR this.pGranu="C") and Not this.pOnlyVerif
      GSVA_BIM(this,"",this.pCODSTR,"CONFE",this.pGRANU)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_RISUL = "O"
      this.w_DATELA = i_DATSYS
      this.w_ORAELA = Alltrim(Time())
      * --- Sposto il file elaborato nella directory di destinazione
      this.w_CODAZI = i_CODAZI
      * --- Read from VAANAIMP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VAANAIMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2],.t.,this.VAANAIMP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IMPATORI"+;
          " from "+i_cTable+" VAANAIMP where ";
              +"IMSERIAL = "+cp_ToStrODBC(this.pSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IMPATORI;
          from (i_cTable) where;
              IMSERIAL = this.pSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_IMPATORI = NVL(cp_ToDate(_read_.IMPATORI),cp_NullValue(_read_.IMPATORI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from PAR_VEFA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_VEFA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_VEFA_idx,2],.t.,this.PAR_VEFA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PADIRSPO"+;
          " from "+i_cTable+" PAR_VEFA where ";
              +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PADIRSPO;
          from (i_cTable) where;
              PACODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_PADIRSPO = NVL(cp_ToDate(_read_.PADIRSPO),cp_NullValue(_read_.PADIRSPO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_FILEORI = JUSTFNAME(this.w_IMPATORI)
      this.w_FILEDEST = ALLTRIM(ADDBS(ALLTRIM(w_PADIRSPO))+this.w_FILEORI)
      if this.w_IMPATORI <> this.w_FILEDEST
        if cp_fileexist(this.w_FILEDEST)
          DELETE FILE (this.w_FILEDEST)
        endif
        RENAME (this.w_IMPATORI) TO (this.w_FILEDEST)
      endif
      * --- Write into VAANAIMP
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.VAANAIMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.VAANAIMP_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IMPATORI ="+cp_NullLink(cp_ToStrODBC(this.w_FILEDEST),'VAANAIMP','IMPATORI');
            +i_ccchkf ;
        +" where ";
            +"IMSERIAL = "+cp_ToStrODBC(this.pSERIAL);
               )
      else
        update (i_cTable) set;
            IMPATORI = this.w_FILEDEST;
            &i_ccchkf. ;
         where;
            IMSERIAL = this.pSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      GSVA_BIM(this,"",this.pCODSTR,"DONE"," ")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_RISUL = iif(i_ROWS=0 OR this.pGranu="C"," ","E")
    endif
    * --- Scrivo log step 3
    this.w_OKLOG = .t.
    * --- Select from gsva_kle
    do vq_exec with 'gsva_kle',this,'_Curs_gsva_kle','',.f.,.t.
    if used('_Curs_gsva_kle')
      select _Curs_gsva_kle
      locate for 1=1
      do while not(eof())
      if this.w_OKLOG
        this.w_Msg = ""
        this.w_OKLOG = .f.
      endif
      this.w_LGSERORI = _Curs_gsva_kle.LGSERORI
      this.w_LGROWORI = _Curs_gsva_kle.LGROWORI
      this.w_LGTIPRIG = _Curs_gsva_kle.LGTIPRIG
      if this.w_LGTIPRIG="E" 
        this.w_RISUL = "E"
      endif
      this.w_Msg = this.w_Msg + IIF(!EMPTY(NVL(this.w_LGROWORI, " ")), ah_msgformat("Riga %1: ", ALLTRIM(STR(this.w_LGROWORI))) , "") + ALLTRIM(_Curs_gsva_kle.LGMESSAG)+CHR(13)
        select _Curs_gsva_kle
        continue
      enddo
      use
    endif
    this.w_Msg = Left(this.w_Msg ,len(this.w_MSG)-1)
    * --- Try
    local bErr_028048A8
    bErr_028048A8=bTrsErr
    this.Try_028048A8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Errore aggiornamento anagrafica elaborazioni (Step C). Impossibile proseguire")
    endif
    bTrsErr=bTrsErr or bErr_028048A8
    * --- End
    i_retcode = 'stop'
    i_retval = iif(this.w_RISUL="E",-1,0)
    return
  endproc
  proc Try_028048A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.pGRANU="C"
      * --- Leggo lo step per vedere se ci sono estati errori
      * --- Read from VAANAIMP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VAANAIMP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2],.t.,this.VAANAIMP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IMSSTEPC,IMFLELAB"+;
          " from "+i_cTable+" VAANAIMP where ";
              +"IMSERIAL = "+cp_ToStrODBC(this.pSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IMSSTEPC,IMFLELAB;
          from (i_cTable) where;
              IMSERIAL = this.pSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_STEPC = NVL(cp_ToDate(_read_.IMSSTEPC),cp_NullValue(_read_.IMSSTEPC))
        this.w_IMFLELAB = NVL(cp_ToDate(_read_.IMFLELAB),cp_NullValue(_read_.IMFLELAB))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if !(this.pGRANU="C" and this.w_STEPC="E")
      this.w_STEPC = this.w_RISUL
    endif
    this.w_FLAGELA = IIF((this.w_IMFLELAB="N" AND this.pGranu="C") or this.w_STEPC <> "O","N","S")
    * --- Write into VAANAIMP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VAANAIMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VAANAIMP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.VAANAIMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IMCODSTR ="+cp_NullLink(cp_ToStrODBC(this.pCODSTR),'VAANAIMP','IMCODSTR');
      +",IMLSTEPC ="+cp_NullLink(cp_ToStrODBC(this.w_msg),'VAANAIMP','IMLSTEPC');
      +",IMSSTEPC ="+cp_NullLink(cp_ToStrODBC(this.w_STEPC),'VAANAIMP','IMSSTEPC');
      +",IMFLELAB ="+cp_NullLink(cp_ToStrODBC(this.w_FLAGELA),'VAANAIMP','IMFLELAB');
      +",IMORAELA ="+cp_NullLink(cp_ToStrODBC(this.w_ORAELA),'VAANAIMP','IMORAELA');
      +",IMDATELA ="+cp_NullLink(cp_ToStrODBC(this.w_DATELA),'VAANAIMP','IMDATELA');
      +",IMSERGEN ="+cp_NullLink(cp_ToStrODBC(this.w_SERGDOC),'VAANAIMP','IMSERGEN');
      +",IMFLREEZ ="+cp_NullLink(cp_ToStrODBC("N"),'VAANAIMP','IMFLREEZ');
          +i_ccchkf ;
      +" where ";
          +"IMSERIAL = "+cp_ToStrODBC(this.pSERIAL);
             )
    else
      update (i_cTable) set;
          IMCODSTR = this.pCODSTR;
          ,IMLSTEPC = this.w_msg;
          ,IMSSTEPC = this.w_STEPC;
          ,IMFLELAB = this.w_FLAGELA;
          ,IMORAELA = this.w_ORAELA;
          ,IMDATELA = this.w_DATELA;
          ,IMSERGEN = this.w_SERGDOC;
          ,IMFLREEZ = "N";
          &i_ccchkf. ;
       where;
          IMSERIAL = this.pSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.pLog.w_MSG = this.w_MSG
    return


  proc Init(oParentObject,pCODSTR,pSERIAL,pLog,pOnlyVerif,pVerbose,pGranu)
    this.pCODSTR=pCODSTR
    this.pSERIAL=pSERIAL
    this.pLog=pLog
    this.pOnlyVerif=pOnlyVerif
    this.pVerbose=pVerbose
    this.pGranu=pGranu
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='LOG_GEND'
    this.cWorkTables[2]='VAANAIMP'
    this.cWorkTables[3]='PAR_VEFA'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_gsva_kle')
      use in _Curs_gsva_kle
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODSTR,pSERIAL,pLog,pOnlyVerif,pVerbose,pGranu"
endproc
