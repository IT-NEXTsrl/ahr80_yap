* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_mdf                                                        *
*              Dettaglio formati                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_32]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-30                                                      *
* Last revis.: 2012-01-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsva_mdf")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsva_mdf")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsva_mdf")
  return

* --- Class definition
define class tgsva_mdf as StdPCForm
  Width  = 692
  Height = 274
  Top    = 10
  Left   = 10
  cComment = "Dettaglio formati"
  cPrg = "gsva_mdf"
  HelpContextID=80376425
  add object cnt as tcgsva_mdf
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsva_mdf as PCContext
  w_DFCODSTR = space(10)
  w_DFCODFOR = space(20)
  w_CPROWORD = 0
  w_DF__TIPO = space(1)
  w_DFVALORE = space(50)
  w_TIPFIL = space(1)
  w_DFSEZINI = 0
  w_DFSEZFIN = 0
  w_DFFLNORM = space(1)
  w_DFFLSEGM = space(1)
  w_DFFLESAL = space(1)
  proc Save(i_oFrom)
    this.w_DFCODSTR = i_oFrom.w_DFCODSTR
    this.w_DFCODFOR = i_oFrom.w_DFCODFOR
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_DF__TIPO = i_oFrom.w_DF__TIPO
    this.w_DFVALORE = i_oFrom.w_DFVALORE
    this.w_TIPFIL = i_oFrom.w_TIPFIL
    this.w_DFSEZINI = i_oFrom.w_DFSEZINI
    this.w_DFSEZFIN = i_oFrom.w_DFSEZFIN
    this.w_DFFLNORM = i_oFrom.w_DFFLNORM
    this.w_DFFLSEGM = i_oFrom.w_DFFLSEGM
    this.w_DFFLESAL = i_oFrom.w_DFFLESAL
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DFCODSTR = this.w_DFCODSTR
    i_oTo.w_DFCODFOR = this.w_DFCODFOR
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_DF__TIPO = this.w_DF__TIPO
    i_oTo.w_DFVALORE = this.w_DFVALORE
    i_oTo.w_TIPFIL = this.w_TIPFIL
    i_oTo.w_DFSEZINI = this.w_DFSEZINI
    i_oTo.w_DFSEZFIN = this.w_DFSEZFIN
    i_oTo.w_DFFLNORM = this.w_DFFLNORM
    i_oTo.w_DFFLSEGM = this.w_DFFLSEGM
    i_oTo.w_DFFLESAL = this.w_DFFLESAL
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsva_mdf as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 692
  Height = 274
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-01-30"
  HelpContextID=80376425
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  VADETTFO_IDX = 0
  VASTRUTT_IDX = 0
  cFile = "VADETTFO"
  cKeySelect = "DFCODSTR,DFCODFOR"
  cKeyWhere  = "DFCODSTR=this.w_DFCODSTR and DFCODFOR=this.w_DFCODFOR"
  cKeyDetail  = "DFCODSTR=this.w_DFCODSTR and DFCODFOR=this.w_DFCODFOR"
  cKeyWhereODBC = '"DFCODSTR="+cp_ToStrODBC(this.w_DFCODSTR)';
      +'+" and DFCODFOR="+cp_ToStrODBC(this.w_DFCODFOR)';

  cKeyDetailWhereODBC = '"DFCODSTR="+cp_ToStrODBC(this.w_DFCODSTR)';
      +'+" and DFCODFOR="+cp_ToStrODBC(this.w_DFCODFOR)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"VADETTFO.DFCODSTR="+cp_ToStrODBC(this.w_DFCODSTR)';
      +'+" and VADETTFO.DFCODFOR="+cp_ToStrODBC(this.w_DFCODFOR)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'VADETTFO.CPROWORD'
  cPrg = "gsva_mdf"
  cComment = "Dettaglio formati"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DFCODSTR = space(10)
  w_DFCODFOR = space(20)
  w_CPROWORD = 0
  w_DF__TIPO = space(1)
  o_DF__TIPO = space(1)
  w_DFVALORE = space(50)
  w_TIPFIL = space(1)
  w_DFSEZINI = 0
  o_DFSEZINI = 0
  w_DFSEZFIN = 0
  w_DFFLNORM = space(1)
  w_DFFLSEGM = space(1)
  w_DFFLESAL = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_mdfPag1","gsva_mdf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='VADETTFO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VADETTFO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VADETTFO_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsva_mdf'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from VADETTFO where DFCODSTR=KeySet.DFCODSTR
    *                            and DFCODFOR=KeySet.DFCODFOR
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.VADETTFO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VADETTFO_IDX,2],this.bLoadRecFilter,this.VADETTFO_IDX,"gsva_mdf")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VADETTFO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VADETTFO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VADETTFO '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DFCODSTR',this.w_DFCODSTR  ,'DFCODFOR',this.w_DFCODFOR  )
      select * from (i_cTable) VADETTFO where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPFIL = space(1)
        .w_DFCODSTR = NVL(DFCODSTR,space(10))
          if link_1_1_joined
            this.w_DFCODSTR = NVL(STCODICE101,NVL(this.w_DFCODSTR,space(10)))
            this.w_TIPFIL = NVL(STTIPFIL101,space(1))
          else
          .link_1_1('Load')
          endif
        .w_DFCODFOR = NVL(DFCODFOR,space(20))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'VADETTFO')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DF__TIPO = NVL(DF__TIPO,space(1))
          .w_DFVALORE = NVL(DFVALORE,space(50))
          .w_DFSEZINI = NVL(DFSEZINI,0)
          .w_DFSEZFIN = NVL(DFSEZFIN,0)
          .w_DFFLNORM = NVL(DFFLNORM,space(1))
          .w_DFFLSEGM = NVL(DFFLSEGM,space(1))
          .w_DFFLESAL = NVL(DFFLESAL,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DFCODSTR=space(10)
      .w_DFCODFOR=space(20)
      .w_CPROWORD=10
      .w_DF__TIPO=space(1)
      .w_DFVALORE=space(50)
      .w_TIPFIL=space(1)
      .w_DFSEZINI=0
      .w_DFSEZFIN=0
      .w_DFFLNORM=space(1)
      .w_DFFLSEGM=space(1)
      .w_DFFLESAL=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_DFCODSTR))
         .link_1_1('Full')
        endif
        .DoRTCalc(2,4,.f.)
        .w_DFVALORE = ''
        .DoRTCalc(6,6,.f.)
        .w_DFSEZINI = 0
        .w_DFSEZFIN = 0
        .w_DFFLNORM = 'N'
        .DoRTCalc(10,10,.f.)
        .w_DFFLESAL = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'VADETTFO')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'VADETTFO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VADETTFO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCODSTR,"DFCODSTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DFCODFOR,"DFCODFOR",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_DF__TIPO N(3);
      ,t_DFVALORE C(50);
      ,t_DFSEZINI N(3);
      ,t_DFSEZFIN N(3);
      ,t_DFFLNORM N(3);
      ,t_DFFLSEGM N(3);
      ,t_DFFLESAL N(3);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsva_mdfbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDF__TIPO_2_2.controlsource=this.cTrsName+'.t_DF__TIPO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFVALORE_2_3.controlsource=this.cTrsName+'.t_DFVALORE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFSEZINI_2_4.controlsource=this.cTrsName+'.t_DFSEZINI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFSEZFIN_2_5.controlsource=this.cTrsName+'.t_DFSEZFIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLNORM_2_6.controlsource=this.cTrsName+'.t_DFFLNORM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLSEGM_2_7.controlsource=this.cTrsName+'.t_DFFLSEGM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLESAL_2_8.controlsource=this.cTrsName+'.t_DFFLESAL'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(60)
    this.AddVLine(245)
    this.AddVLine(454)
    this.AddVLine(490)
    this.AddVLine(529)
    this.AddVLine(577)
    this.AddVLine(626)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VADETTFO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VADETTFO_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VADETTFO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VADETTFO_IDX,2])
      *
      * insert into VADETTFO
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VADETTFO')
        i_extval=cp_InsertValODBCExtFlds(this,'VADETTFO')
        i_cFldBody=" "+;
                  "(DFCODSTR,DFCODFOR,CPROWORD,DF__TIPO,DFVALORE"+;
                  ",DFSEZINI,DFSEZFIN,DFFLNORM,DFFLSEGM,DFFLESAL,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_DFCODSTR)+","+cp_ToStrODBC(this.w_DFCODFOR)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_DF__TIPO)+","+cp_ToStrODBC(this.w_DFVALORE)+;
             ","+cp_ToStrODBC(this.w_DFSEZINI)+","+cp_ToStrODBC(this.w_DFSEZFIN)+","+cp_ToStrODBC(this.w_DFFLNORM)+","+cp_ToStrODBC(this.w_DFFLSEGM)+","+cp_ToStrODBC(this.w_DFFLESAL)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VADETTFO')
        i_extval=cp_InsertValVFPExtFlds(this,'VADETTFO')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DFCODSTR',this.w_DFCODSTR,'DFCODFOR',this.w_DFCODFOR)
        INSERT INTO (i_cTable) (;
                   DFCODSTR;
                  ,DFCODFOR;
                  ,CPROWORD;
                  ,DF__TIPO;
                  ,DFVALORE;
                  ,DFSEZINI;
                  ,DFSEZFIN;
                  ,DFFLNORM;
                  ,DFFLSEGM;
                  ,DFFLESAL;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DFCODSTR;
                  ,this.w_DFCODFOR;
                  ,this.w_CPROWORD;
                  ,this.w_DF__TIPO;
                  ,this.w_DFVALORE;
                  ,this.w_DFSEZINI;
                  ,this.w_DFSEZFIN;
                  ,this.w_DFFLNORM;
                  ,this.w_DFFLSEGM;
                  ,this.w_DFFLESAL;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.VADETTFO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VADETTFO_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_DF__TIPO))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'VADETTFO')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'VADETTFO')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_DF__TIPO))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update VADETTFO
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'VADETTFO')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DF__TIPO="+cp_ToStrODBC(this.w_DF__TIPO)+;
                     ",DFVALORE="+cp_ToStrODBC(this.w_DFVALORE)+;
                     ",DFSEZINI="+cp_ToStrODBC(this.w_DFSEZINI)+;
                     ",DFSEZFIN="+cp_ToStrODBC(this.w_DFSEZFIN)+;
                     ",DFFLNORM="+cp_ToStrODBC(this.w_DFFLNORM)+;
                     ",DFFLSEGM="+cp_ToStrODBC(this.w_DFFLSEGM)+;
                     ",DFFLESAL="+cp_ToStrODBC(this.w_DFFLESAL)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'VADETTFO')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,DF__TIPO=this.w_DF__TIPO;
                     ,DFVALORE=this.w_DFVALORE;
                     ,DFSEZINI=this.w_DFSEZINI;
                     ,DFSEZFIN=this.w_DFSEZFIN;
                     ,DFFLNORM=this.w_DFFLNORM;
                     ,DFFLSEGM=this.w_DFFLSEGM;
                     ,DFFLESAL=this.w_DFFLESAL;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VADETTFO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VADETTFO_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_DF__TIPO))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete VADETTFO
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_DF__TIPO))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VADETTFO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VADETTFO_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,4,.t.)
        if .o_DF__TIPO<>.w_DF__TIPO
          .w_DFVALORE = ''
        endif
        .DoRTCalc(6,6,.t.)
        if .o_DF__TIPO<>.w_DF__TIPO
          .w_DFSEZINI = 0
        endif
        if .o_DFSEZINI<>.w_DFSEZINI.or. .o_DF__TIPO<>.w_DF__TIPO
          .w_DFSEZFIN = 0
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFVALORE_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFVALORE_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFFLNORM_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFFLNORM_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFFLESAL_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDFFLESAL_2_8.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DFCODSTR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DFCODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DFCODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STTIPFIL";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_DFCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_DFCODSTR)
            select STCODICE,STTIPFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DFCODSTR = NVL(_Link_.STCODICE,space(10))
      this.w_TIPFIL = NVL(_Link_.STTIPFIL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DFCODSTR = space(10)
      endif
      this.w_TIPFIL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DFCODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VASTRUTT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.STCODICE as STCODICE101"+ ",link_1_1.STTIPFIL as STTIPFIL101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on VADETTFO.DFCODSTR=link_1_1.STCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and VADETTFO.DFCODSTR=link_1_1.STCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDF__TIPO_2_2.RadioValue()==this.w_DF__TIPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDF__TIPO_2_2.SetRadio()
      replace t_DF__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDF__TIPO_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFVALORE_2_3.value==this.w_DFVALORE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFVALORE_2_3.value=this.w_DFVALORE
      replace t_DFVALORE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFVALORE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFSEZINI_2_4.value==this.w_DFSEZINI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFSEZINI_2_4.value=this.w_DFSEZINI
      replace t_DFSEZINI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFSEZINI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFSEZFIN_2_5.value==this.w_DFSEZFIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFSEZFIN_2_5.value=this.w_DFSEZFIN
      replace t_DFSEZFIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFSEZFIN_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLNORM_2_6.RadioValue()==this.w_DFFLNORM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLNORM_2_6.SetRadio()
      replace t_DFFLNORM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLNORM_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLSEGM_2_7.RadioValue()==this.w_DFFLSEGM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLSEGM_2_7.SetRadio()
      replace t_DFFLSEGM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLSEGM_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLESAL_2_8.RadioValue()==this.w_DFFLESAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLESAL_2_8.SetRadio()
      replace t_DFFLESAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLESAL_2_8.value
    endif
    cp_SetControlsValueExtFlds(this,'VADETTFO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_DFSEZFIN=0 or (.w_DFSEZINI>0 and .w_DFSEZINI<=.w_DFSEZFIN)) and (not(Empty(.w_DF__TIPO)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFSEZINI_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Attenzione valore iniziale non positivo o maggiore del valore finale")
        case   not(.w_DFSEZINI=0 or .w_DFSEZFIN>=.w_DFSEZINI) and (not(Empty(.w_DF__TIPO)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFSEZFIN_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valore finale inferiore al valore iniziale")
      endcase
      if not(Empty(.w_DF__TIPO))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DF__TIPO = this.w_DF__TIPO
    this.o_DFSEZINI = this.w_DFSEZINI
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_DF__TIPO)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_DF__TIPO=space(1)
      .w_DFVALORE=space(50)
      .w_DFSEZINI=0
      .w_DFSEZFIN=0
      .w_DFFLNORM=space(1)
      .w_DFFLSEGM=space(1)
      .w_DFFLESAL=space(1)
      .DoRTCalc(1,4,.f.)
        .w_DFVALORE = ''
      .DoRTCalc(6,6,.f.)
        .w_DFSEZINI = 0
        .w_DFSEZFIN = 0
        .w_DFFLNORM = 'N'
      .DoRTCalc(10,10,.f.)
        .w_DFFLESAL = 'N'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_DF__TIPO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDF__TIPO_2_2.RadioValue(.t.)
    this.w_DFVALORE = t_DFVALORE
    this.w_DFSEZINI = t_DFSEZINI
    this.w_DFSEZFIN = t_DFSEZFIN
    this.w_DFFLNORM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLNORM_2_6.RadioValue(.t.)
    this.w_DFFLSEGM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLSEGM_2_7.RadioValue(.t.)
    this.w_DFFLESAL = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLESAL_2_8.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DF__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDF__TIPO_2_2.ToRadio()
    replace t_DFVALORE with this.w_DFVALORE
    replace t_DFSEZINI with this.w_DFSEZINI
    replace t_DFSEZFIN with this.w_DFSEZFIN
    replace t_DFFLNORM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLNORM_2_6.ToRadio()
    replace t_DFFLSEGM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLSEGM_2_7.ToRadio()
    replace t_DFFLESAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDFFLESAL_2_8.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsva_mdfPag1 as StdContainer
  Width  = 688
  height = 274
  stdWidth  = 688
  stdheight = 274
  resizeXpos=256
  resizeYpos=122
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=0, width=671,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Riga",Field2="DF__TIPO",Label2="Tipo",Field3="DFVALORE",Label3="Valore nel caso di tipo stringa fissa",Field4="DFSEZINI",Label4="Da",Field5="DFSEZFIN",Label5="A",Field6="DFFLNORM",Label6="Norm.",Field7="DFFLSEGM",Label7="Seg.",Field8="DFFLESAL",Label8="Elim.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235714426

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=19,;
    width=671+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3999999999999999)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=20,width=670+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*12*1.3999999999999999)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsva_mdfBodyRow as CPBodyRowCnt
  Width=661
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3999999999999999)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="EDSHBSUUKR",rtseq=3,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 268042858,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oDF__TIPO_2_2 as StdTrsCombo with uid="KCNAPMMVPI",rtrep=.t.,;
    cFormVar="w_DF__TIPO", RowSource=""+"Stringa fissa,"+"Id primario,"+"Id secondario,"+"Cod. txt descr.,"+"Valore sul DB,"+"Espressione,"+"Esploso figli elementi,"+"Esploso figli attributi,"+"Esploso figli,"+"Fine riga CR+LF,"+"Fine riga LF,"+"Indenta,"+"Deindenta,"+"Allegato" , ;
    HelpContextID = 165333125,;
    Height=21, Width=180, Left=51, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDF__TIPO_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DF__TIPO,&i_cF..t_DF__TIPO),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'C',;
    iif(xVal =3,'T',;
    iif(xVal =4,'D',;
    iif(xVal =5,'V',;
    iif(xVal =6,'X',;
    iif(xVal =7,'F',;
    iif(xVal =8,'A',;
    iif(xVal =9,'G',;
    iif(xVal =10,'E',;
    iif(xVal =11,'R',;
    iif(xVal =12,'I',;
    iif(xVal =13,'N',;
    iif(xVal =14,'L',;
    space(1))))))))))))))))
  endfunc
  func oDF__TIPO_2_2.GetRadio()
    this.Parent.oContained.w_DF__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oDF__TIPO_2_2.ToRadio()
    this.Parent.oContained.w_DF__TIPO=trim(this.Parent.oContained.w_DF__TIPO)
    return(;
      iif(this.Parent.oContained.w_DF__TIPO=='S',1,;
      iif(this.Parent.oContained.w_DF__TIPO=='C',2,;
      iif(this.Parent.oContained.w_DF__TIPO=='T',3,;
      iif(this.Parent.oContained.w_DF__TIPO=='D',4,;
      iif(this.Parent.oContained.w_DF__TIPO=='V',5,;
      iif(this.Parent.oContained.w_DF__TIPO=='X',6,;
      iif(this.Parent.oContained.w_DF__TIPO=='F',7,;
      iif(this.Parent.oContained.w_DF__TIPO=='A',8,;
      iif(this.Parent.oContained.w_DF__TIPO=='G',9,;
      iif(this.Parent.oContained.w_DF__TIPO=='E',10,;
      iif(this.Parent.oContained.w_DF__TIPO=='R',11,;
      iif(this.Parent.oContained.w_DF__TIPO=='I',12,;
      iif(this.Parent.oContained.w_DF__TIPO=='N',13,;
      iif(this.Parent.oContained.w_DF__TIPO=='L',14,;
      0)))))))))))))))
  endfunc

  func oDF__TIPO_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDFVALORE_2_3 as StdTrsField with uid="SNUXGLFAZU",rtseq=5,rtrep=.t.,;
    cFormVar="w_DFVALORE",value=space(50),;
    HelpContextID = 255604859,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=206, Left=235, Top=0, InputMask=replicate('X',50)

  func oDFVALORE_2_3.mCond()
    with this.Parent.oContained
      return (.w_DF__TIPO='S')
    endwith
  endfunc

  add object oDFSEZINI_2_4 as StdTrsField with uid="HPUUDMYRPU",rtseq=7,rtrep=.t.,;
    cFormVar="w_DFSEZINI",value=0,;
    ToolTipText = "Inizio frammento di informazione",;
    HelpContextID = 98563969,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione valore iniziale non positivo o maggiore del valore finale",;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=443, Top=0

  func oDFSEZINI_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DFSEZFIN=0 or (.w_DFSEZINI>0 and .w_DFSEZINI<=.w_DFSEZFIN))
    endwith
    return bRes
  endfunc

  add object oDFSEZFIN_2_5 as StdTrsField with uid="PJWTLHDPAN",rtseq=8,rtrep=.t.,;
    cFormVar="w_DFSEZFIN",value=0,;
    ToolTipText = "Fine frammento di informazione",;
    HelpContextID = 148895612,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valore finale inferiore al valore iniziale",;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=480, Top=0

  func oDFSEZFIN_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DFSEZINI=0 or .w_DFSEZFIN>=.w_DFSEZINI)
    endwith
    return bRes
  endfunc

  add object oDFFLNORM_2_6 as StdTrsCheck with uid="LUTDGTUYRB",rtrep=.t.,;
    cFormVar="w_DFFLNORM",  caption="",;
    ToolTipText = "Se attivo non normalizza in funzione del tipo file",;
    HelpContextID = 258357379,;
    Left=520, Top=0, Width=42,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , autosize=.f.;
   , bGlobalFont=.t.


  func oDFFLNORM_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DFFLNORM,&i_cF..t_DFFLNORM),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDFFLNORM_2_6.GetRadio()
    this.Parent.oContained.w_DFFLNORM = this.RadioValue()
    return .t.
  endfunc

  func oDFFLNORM_2_6.ToRadio()
    this.Parent.oContained.w_DFFLNORM=trim(this.Parent.oContained.w_DFFLNORM)
    return(;
      iif(this.Parent.oContained.w_DFFLNORM=='S',1,;
      0))
  endfunc

  func oDFFLNORM_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDFFLNORM_2_6.mCond()
    with this.Parent.oContained
      return (.w_TIPFIL<>'G' AND .w_DF__TIPO $ 'X-V')
    endwith
  endfunc

  add object oDFFLSEGM_2_7 as StdTrsCheck with uid="QAQWSNZULD",rtrep=.t.,;
    cFormVar="w_DFFLSEGM",  caption="",;
    ToolTipText = "Se attivo, nei file xml identifica le righe che partecipano al calcolo del relativo totale",;
    HelpContextID = 172607357,;
    Left=569, Top=0, Width=42,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , autosize=.f.;
   , bGlobalFont=.t.


  func oDFFLSEGM_2_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DFFLSEGM,&i_cF..t_DFFLSEGM),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDFFLSEGM_2_7.GetRadio()
    this.Parent.oContained.w_DFFLSEGM = this.RadioValue()
    return .t.
  endfunc

  func oDFFLSEGM_2_7.ToRadio()
    this.Parent.oContained.w_DFFLSEGM=trim(this.Parent.oContained.w_DFFLSEGM)
    return(;
      iif(this.Parent.oContained.w_DFFLSEGM=='S',1,;
      0))
  endfunc

  func oDFFLSEGM_2_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDFFLESAL_2_8 as StdTrsCheck with uid="CYTNDDGCNY",rtrep=.t.,;
    cFormVar="w_DFFLESAL",  caption="",;
    ToolTipText = "Se attivo annulla eliminazioni spazi sulla riga di formato se previsto nella struttura",;
    HelpContextID = 47593602,;
    Left=618, Top=0, Width=38,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , autosize=.f.;
   , bGlobalFont=.t.


  func oDFFLESAL_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DFFLESAL,&i_cF..t_DFFLESAL),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oDFFLESAL_2_8.GetRadio()
    this.Parent.oContained.w_DFFLESAL = this.RadioValue()
    return .t.
  endfunc

  func oDFFLESAL_2_8.ToRadio()
    this.Parent.oContained.w_DFFLESAL=trim(this.Parent.oContained.w_DFFLESAL)
    return(;
      iif(this.Parent.oContained.w_DFFLESAL=='S',1,;
      0))
  endfunc

  func oDFFLESAL_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDFFLESAL_2_8.mCond()
    with this.Parent.oContained
      return (.w_TIPFIL<>'L' )
    endwith
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_mdf','VADETTFO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DFCODSTR=VADETTFO.DFCODSTR";
  +" and "+i_cAliasName2+".DFCODFOR=VADETTFO.DFCODFOR";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
