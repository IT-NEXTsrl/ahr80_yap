* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_kbc                                                        *
*              Manutenzione basi di calcolo                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_239]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-20                                                      *
* Last revis.: 2013-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsva_kbc",oParentObject))

* --- Class definition
define class tgsva_kbc as StdForm
  Top    = 4
  Left   = 14

  * --- Standard Properties
  Width  = 863
  Height = 480
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-07-16"
  HelpContextID=116028009
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Constant Properties
  _IDX = 0
  CLLSMAST_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  cPrg = "gsva_kbc"
  cComment = "Manutenzione basi di calcolo"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_ORIGINE = space(1)
  w_CODART = space(20)
  o_CODART = space(20)
  w_DESART = space(40)
  w_DATRIF = ctod('  /  /  ')
  o_DATRIF = ctod('  /  /  ')
  w_QTAMOV = 0
  o_QTAMOV = 0
  w_CODLIS = space(5)
  w_GRUMER = space(5)
  w_DECTOT = 0
  o_DECTOT = 0
  w_SERIAL = space(10)
  w_CHIAVE = space(33)
  o_CHIAVE = space(33)
  w_SELEZI = space(1)
  w_CALCOLA = space(1)
  w_FLAGGIO = space(1)
  w_FLSELE = 0
  w_CALCPICT = 0
  w_VARIATI = .F.
  o_VARIATI = .F.
  w_OBTEST = ctod('  /  /  ')
  w_OLD = space(1)
  w_LISAGG = space(5)
  w_PREZZOOM = 0
  w_UNISEL = space(3)
  o_UNISEL = space(3)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_FLSERG = space(1)
  w_UMBAS = space(3)
  w_UNMIS3 = space(3)
  w_BASUMLIS = 0
  w_UMLISAGG = space(3)
  w_TIPUMI = space(1)
  w_FLUSEP = space(1)
  w_FLFRAZ1 = space(1)
  w_OPERAT = space(1)
  w_MOLTIP = 0
  w_QTAUM1 = 0
  w_QTAUM2 = 0
  w_UNIMOV = space(3)
  w_BASCALC = 0
  o_BASCALC = 0
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsva_kbc
    Proc Activate()
      DoDefault()
        =DEFPCUNI(THIS.w_DECTOT)
    EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsva_kbcPag1","gsva_kbc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODART_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CLLSMAST'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='UNIMIS'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ORIGINE=space(1)
      .w_CODART=space(20)
      .w_DESART=space(40)
      .w_DATRIF=ctod("  /  /  ")
      .w_QTAMOV=0
      .w_CODLIS=space(5)
      .w_GRUMER=space(5)
      .w_DECTOT=0
      .w_SERIAL=space(10)
      .w_CHIAVE=space(33)
      .w_SELEZI=space(1)
      .w_CALCOLA=space(1)
      .w_FLAGGIO=space(1)
      .w_FLSELE=0
      .w_CALCPICT=0
      .w_VARIATI=.f.
      .w_OBTEST=ctod("  /  /  ")
      .w_OLD=space(1)
      .w_LISAGG=space(5)
      .w_PREZZOOM=0
      .w_UNISEL=space(3)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_FLSERG=space(1)
      .w_UMBAS=space(3)
      .w_UNMIS3=space(3)
      .w_BASUMLIS=0
      .w_UMLISAGG=space(3)
      .w_TIPUMI=space(1)
      .w_FLUSEP=space(1)
      .w_FLFRAZ1=space(1)
      .w_OPERAT=space(1)
      .w_MOLTIP=0
      .w_QTAUM1=0
      .w_QTAUM2=0
      .w_UNIMOV=space(3)
      .w_BASCALC=0
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_CODART))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_QTAMOV = IIF(.w_UNISEL=.w_UNMIS2, .w_QTAUM2, .w_QTAUM1)
          .DoRTCalc(6,8,.f.)
        .w_SERIAL = .w_Zoom.getVar('CLSERIAL')
      .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
      .oPgFrm.Page1.oPag.Zoom.Calculate()
          .DoRTCalc(10,10,.f.)
        .w_SELEZI = 'D'
        .w_CALCOLA = IIF(left(.w_CHIAVE,1) ='L', .w_CALCOLA, 'P')
        .w_FLAGGIO = IIF(left(.w_CHIAVE,1) $'LC', .w_FLAGGIO,' ')
        .w_FLSELE = 0
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .w_CALCPICT = DEFPCUNI(.w_DECTOT)
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate('',RGB(255,255,0),RGB(255,255,0))
        .w_VARIATI = (.w_Zoom.getVar('OLDPREZZO') <> .w_Zoom.getVar('PREZZOFI')) OR (.w_Zoom.getVar('OLDVARPERC3') <> .w_Zoom.getVar('VARPERC3')) OR (.w_Zoom.getVar('OLDVARPERC2') <> .w_Zoom.getVar('VARPERC2'))
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate('',RGB(255,0,0),RGB(255,0,0))
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_40.Calculate('',RGB(135,242,248),RGB(135,242,248))
      .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
          .DoRTCalc(17,18,.f.)
        .w_LISAGG = .w_Zoom.getVar('CLAGGLIS')
        .w_PREZZOOM = .w_Zoom.getVar('PREZZOUM')
      .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .w_UNISEL = .w_UNMIS1
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_UNISEL))
          .link_1_45('Full')
        endif
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_UNMIS1))
          .link_1_47('Full')
        endif
          .DoRTCalc(23,25,.f.)
        .w_UNMIS3 = IIF(.w_UNISEL<>.w_UNMIS1  AND .w_UNISEL<>.w_UNMIS2, .w_UNISEL, SPACE(3))
        .w_BASUMLIS = .w_Zoom.getVar('BASUMLIS')
        .w_UMLISAGG = .w_Zoom.getVar('LIUNIMIS')
        .w_TIPUMI = IIF(.w_UNISEL=.w_UNMIS1, 'P', 'S')
          .DoRTCalc(30,33,.f.)
        .w_QTAUM1 = iif(.w_UNISEL=.w_UNMIS1, .w_QTAMOV, CALQTA(.w_QTAMOV,.w_UNMIS2,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, '', , '', '', 0,,'I'))
        .w_QTAUM2 = IIF(.w_UNISEL=.w_UNMIS2, .w_QTAMOV, Caunmis2('R', .w_QTAMOV, .w_UNMIS1, .w_UNMIS2, .w_UNMIS1,  .w_OPERAT,  .w_MOLTIP))
    endwith
    this.DoRTCalc(36,37,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsva_kbc
    
    if type("this.oparentobject")='O'
     DO CASE
      CASE Upper (this.oparentobject.Class )= "TGSVE_MDV" OR Upper (this.oparentobject.Class )= "TGSAC_MDV" OR Upper (this.oparentobject.Class )= "TGSOR_MDV"
       THIS.w_ORIGINE='D'
       This.w_CODART= This.oparentobject .w_MVCODART
       this.w_DATRIF= This.oparentobject .w_MVDATDOC
       this.w_UNISEL= This.oparentobject .w_UNMIS1
       this.w_UNIMOV= This.oparentobject .w_MVUNIMIS
       this.w_QTAMOV= This.oparentobject .w_MVQTAMOV
       this.w_DECTOT= This.oparentobject .w_DECUNI
       this.w_UNMIS3= This.oparentobject .w_UNMIS3
       this.w_QTAUM1= This.oparentobject .w_MVQTAUM1
      CASE Upper (this.oparentobject.Class )= "TGSAR_KCA"
       THIS.w_ORIGINE='P'
       THIS.w_CODLIS=This.oparentobject.w_CODLIS
       This.w_CODART= This.oparentobject .w_CODART
       this.w_DATRIF= This.oparentobject .w_DATDOC
       this.w_QTAMOV= This.oparentobject .w_MQTAMOV
       this.w_UNIMOV= This.oparentobject .w_MUNIMIS
       *this.w_UNISEL= This.oparentobject .w_MUNIMIS
       this.w_DECTOT= g_PERPUL
      CASE Upper (this.oparentobject.Class )= "TGSMA_AAR" OR Upper (this.oparentobject.Class )= "TGSMA_AAS"
       THIS.w_ORIGINE='A'
       This.w_CODART= This.oparentobject .w_ARCODART
       this.w_DATRIF= i_DATSYS
       this.w_QTAMOV= 0
       this.w_UNIMOV= This.oparentobject .w_ARUNMIS1
       this.w_UNISEL= This.oparentobject .w_ARUNMIS1
       this.w_DECTOT= g_PERPUL
     ENDCASE
    ELSE
       THIS.w_ORIGINE='M'
       This.w_CODART= SPACE(20)
       this.w_DATRIF= i_DATSYS
       this.w_QTAMOV= 0
       this.w_UNISEL= SPACE(3)
       this.w_UNIMOV= SPACE(3)
       this.w_DECTOT= g_PERPUL
    ENDIF
     *faccio scattare link di w_codart
    IF not empty(this.w_CODART)
      CTRL_CODART= This.GetCtrl("w_CODART")
      CTRL_CODART.Check()
      this.NotifyEvent("w_CODART Changed")
      this.setcontrolsvalue()
       *Popolo la combo
       *Local CTRL_CHIAVE
       *CTRL_CHIAVE= This.GetCtrl("w_CHIAVE")
       *CTRL_CHIAVE.Popola()
    ENDIF
    this.w_CALCOLA='P'
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_UNISEL<>.w_UNISEL.or. .o_CODART<>.w_CODART
            .w_QTAMOV = IIF(.w_UNISEL=.w_UNMIS2, .w_QTAUM2, .w_QTAUM1)
        endif
        .DoRTCalc(6,8,.t.)
            .w_SERIAL = .w_Zoom.getVar('CLSERIAL')
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .DoRTCalc(10,11,.t.)
        if .o_CHIAVE<>.w_CHIAVE
            .w_CALCOLA = IIF(left(.w_CHIAVE,1) ='L', .w_CALCOLA, 'P')
        endif
        if .o_CHIAVE<>.w_CHIAVE
            .w_FLAGGIO = IIF(left(.w_CHIAVE,1) $'LC', .w_FLAGGIO,' ')
        endif
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .DoRTCalc(14,14,.t.)
        if .o_DECTOT<>.w_DECTOT.or. .o_CODART<>.w_CODART
            .w_CALCPICT = DEFPCUNI(.w_DECTOT)
        endif
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate('',RGB(255,255,0),RGB(255,255,0))
            .w_VARIATI = (.w_Zoom.getVar('OLDPREZZO') <> .w_Zoom.getVar('PREZZOFI')) OR (.w_Zoom.getVar('OLDVARPERC3') <> .w_Zoom.getVar('VARPERC3')) OR (.w_Zoom.getVar('OLDVARPERC2') <> .w_Zoom.getVar('VARPERC2'))
        if .o_VARIATI<>.w_VARIATI
          .Calculate_VLCVEFSPNI()
        endif
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate('',RGB(255,0,0),RGB(255,0,0))
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate('',RGB(135,242,248),RGB(135,242,248))
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .DoRTCalc(17,18,.t.)
            .w_LISAGG = .w_Zoom.getVar('CLAGGLIS')
            .w_PREZZOOM = .w_Zoom.getVar('PREZZOUM')
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        if .o_CODART<>.w_CODART
            .w_UNISEL = .w_UNMIS1
          .link_1_45('Full')
        endif
          .link_1_47('Full')
        .DoRTCalc(23,25,.t.)
            .w_UNMIS3 = IIF(.w_UNISEL<>.w_UNMIS1  AND .w_UNISEL<>.w_UNMIS2, .w_UNISEL, SPACE(3))
            .w_BASUMLIS = .w_Zoom.getVar('BASUMLIS')
            .w_UMLISAGG = .w_Zoom.getVar('LIUNIMIS')
        if .o_UNISEL<>.w_UNISEL.or. .o_CODART<>.w_CODART
            .w_TIPUMI = IIF(.w_UNISEL=.w_UNMIS1, 'P', 'S')
        endif
        .DoRTCalc(30,33,.t.)
        if .o_UNISEL<>.w_UNISEL.or. .o_CODART<>.w_CODART.or. .o_QTAMOV<>.w_QTAMOV
            .w_QTAUM1 = iif(.w_UNISEL=.w_UNMIS1, .w_QTAMOV, CALQTA(.w_QTAMOV,.w_UNMIS2,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_FLFRAZ1, '', , '', '', 0,,'I'))
        endif
        if .o_UNISEL<>.w_UNISEL.or. .o_CODART<>.w_CODART.or. .o_QTAMOV<>.w_QTAMOV
            .w_QTAUM2 = IIF(.w_UNISEL=.w_UNMIS2, .w_QTAMOV, Caunmis2('R', .w_QTAMOV, .w_UNMIS1, .w_UNMIS2, .w_UNMIS1,  .w_OPERAT,  .w_MOLTIP))
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(36,37,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_15.Calculate()
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate('',RGB(255,255,0),RGB(255,255,0))
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate('',RGB(255,0,0),RGB(255,0,0))
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate('',RGB(135,242,248),RGB(135,242,248))
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
    endwith
  return

  proc Calculate_VLCVEFSPNI()
    with this
          * --- Gsva_bbc (R)
          GSVA_BBC(this;
              ,'R';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODART_1_2.enabled = this.oPgFrm.Page1.oPag.oCODART_1_2.mCond()
    this.oPgFrm.Page1.oPag.oSELEZI_1_18.enabled_(this.oPgFrm.Page1.oPag.oSELEZI_1_18.mCond())
    this.oPgFrm.Page1.oPag.oCALCOLA_1_19.enabled = this.oPgFrm.Page1.oPag.oCALCOLA_1_19.mCond()
    this.oPgFrm.Page1.oPag.oFLAGGIO_1_21.enabled = this.oPgFrm.Page1.oPag.oFLAGGIO_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_20.visible=!this.oPgFrm.Page1.oPag.oBtn_1_20.mHide()
    this.oPgFrm.Page1.oPag.oFLAGGIO_1_21.visible=!this.oPgFrm.Page1.oPag.oFLAGGIO_1_21.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_15.Event(cEvent)
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
        if lower(cEvent)==lower("w_BASCALC Changed")
          .Calculate_VLCVEFSPNI()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsva_kbc
    * --- Blocco colonne Zoom per problemi c0005
    If lower(cEvent)='w_zoom after query'
     BloccaZoom( this.w_ZOOM )
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODART
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARGRUMER,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG,ARFLUSEP,AROPERAT,ARMOLTIP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARGRUMER,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG,ARFLUSEP,AROPERAT,ARMOLTIP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARGRUMER,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG,ARFLUSEP,AROPERAT,ARMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARGRUMER,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG,ARFLUSEP,AROPERAT,ARMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_2'),i_cWhere,'GSMA_BZA',"Articoli/servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARGRUMER,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG,ARFLUSEP,AROPERAT,ARMOLTIP";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARGRUMER,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG,ARFLUSEP,AROPERAT,ARMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARGRUMER,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG,ARFLUSEP,AROPERAT,ARMOLTIP";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARGRUMER,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG,ARFLUSEP,AROPERAT,ARMOLTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_GRUMER = NVL(_Link_.ARGRUMER,space(5))
      this.w_OBTEST = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_GRUMER = space(5)
      this.w_OBTEST = ctod("  /  /  ")
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_FLSERG = space(1)
      this.w_FLUSEP = space(1)
      this.w_OPERAT = space(1)
      this.w_MOLTIP = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNISEL
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNISEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_UNISEL)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_UNISEL))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UNISEL)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_UNISEL) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oUNISEL_1_45'),i_cWhere,'',"",'GSVA1QUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNISEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNISEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNISEL)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNISEL = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_UNISEL = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_UNISEL=.w_UNMIS1 OR .w_UNISEL=.w_UNMIS2
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_UNISEL = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNISEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ1 = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_FLFRAZ1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODART_1_2.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_2.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_3.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_3.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDATRIF_1_5.value==this.w_DATRIF)
      this.oPgFrm.Page1.oPag.oDATRIF_1_5.value=this.w_DATRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oQTAMOV_1_7.value==this.w_QTAMOV)
      this.oPgFrm.Page1.oPag.oQTAMOV_1_7.value=this.w_QTAMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oCHIAVE_1_14.RadioValue()==this.w_CHIAVE)
      this.oPgFrm.Page1.oPag.oCHIAVE_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_18.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCALCOLA_1_19.RadioValue()==this.w_CALCOLA)
      this.oPgFrm.Page1.oPag.oCALCOLA_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGGIO_1_21.RadioValue()==this.w_FLAGGIO)
      this.oPgFrm.Page1.oPag.oFLAGGIO_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUNISEL_1_45.value==this.w_UNISEL)
      this.oPgFrm.Page1.oPag.oUNISEL_1_45.value=this.w_UNISEL
    endif
    if not(this.oPgFrm.Page1.oPag.oBASCALC_1_62.value==this.w_BASCALC)
      this.oPgFrm.Page1.oPag.oBASCALC_1_62.value=this.w_BASCALC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODART))  and (.w_ORIGINE='M')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CODART)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_UNISEL=.w_UNMIS1 OR .w_UNISEL=.w_UNMIS2)  and not(empty(.w_UNISEL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUNISEL_1_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
          case   not(.w_BASCALC>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBASCALC_1_62.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODART = this.w_CODART
    this.o_DATRIF = this.w_DATRIF
    this.o_QTAMOV = this.w_QTAMOV
    this.o_DECTOT = this.w_DECTOT
    this.o_CHIAVE = this.w_CHIAVE
    this.o_VARIATI = this.w_VARIATI
    this.o_UNISEL = this.w_UNISEL
    this.o_BASCALC = this.w_BASCALC
    return

enddefine

* --- Define pages as container
define class tgsva_kbcPag1 as StdContainer
  Width  = 859
  height = 480
  stdWidth  = 859
  stdheight = 480
  resizeXpos=583
  resizeYpos=345
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODART_1_2 as StdField with uid="AEBOTHMFKP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articolo di riferimento",;
    HelpContextID = 226811610,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=109, Top=15, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ORIGINE='M')
    endwith
   endif
  endfunc

  func oCODART_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli/servizi",'',this.parent.oContained
  endproc
  proc oCODART_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oDESART_1_3 as StdField with uid="MGXTECMWEB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 226752714,;
   bGlobalFont=.t.,;
    Height=21, Width=335, Left=279, Top=15, InputMask=replicate('X',40)

  add object oDATRIF_1_5 as StdField with uid="WFZKVFRNAF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATRIF", cQueryName = "DATRIF",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento per la validit� delle schede di calcolo",;
    HelpContextID = 201518282,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=109, Top=42

  add object oQTAMOV_1_7 as StdField with uid="LFHJGRYLZE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_QTAMOV", cQueryName = "QTAMOV",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantit� di riferimento per la validit� dello scaglione",;
    HelpContextID = 195627258,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=343, Top=42, cSayPict="'@Z '+v_PQ(11)", cGetPict="'@Z '+v_GQ(11)"


  add object oCHIAVE_1_14 as StdZTamTableCombo with uid="CZWUEZBREH",rtseq=10,rtrep=.f.,left=109,top=66,width=505,height=21;
    , ToolTipText = "Basi di calcolo selezionabili";
    , HelpContextID = 205821402;
    , cFormVar="w_CHIAVE",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='GSVA3MSL.VQR',cKey='CLBASSEL',cValue='CLDESCRI',cOrderBy='CLBASSEL',xDefault=space(33);
  , bGlobalFont=.t.



  add object oObj_1_15 as cp_runprogram with uid="HYQJVTFCPJ",left=9, top=488, width=262,height=21,;
    caption='GSVA_BBC(E)',;
   bGlobalFont=.t.,;
    prg="GSVA_BBC('E')",;
    cEvent = "w_CHIAVE Changed",;
    nPag=1;
    , HelpContextID = 21962025


  add object Zoom as cp_szoombox with uid="HVNHBNCDGU",left=4, top=125, width=849,height=300,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSVA_KBC",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,cTable="CLLSMAST",cMenuFile="GSVA_KBC",bQueryOnLoad=.f.,cZoomOnZoom="GSVA1BBC",;
    cEvent = "Lanciazoom",;
    nPag=1;
    , HelpContextID = 61969638

  add object oSELEZI_1_18 as StdRadio with uid="ZXLDALFOBQ",rtseq=11,rtrep=.f.,left=6, top=433, width=140,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_18.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 134244314
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 134244314
      this.Buttons(2).Top=15
      this.SetAll("Width",138)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_18.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_18.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_SERIAL))
    endwith
   endif
  endfunc


  add object oCALCOLA_1_19 as StdCombo with uid="OKQJSPXGPO",rtseq=12,rtrep=.f.,left=329,top=437,width=255,height=21;
    , ToolTipText = "Metodo di calcolo per gli altri scaglioni";
    , HelpContextID = 95579354;
    , cFormVar="w_CALCOLA",RowSource=""+"Calcola gli altri scaglioni in proporzione,"+"Applica le basi di calcolo su tutti gli scaglioni", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCALCOLA_1_19.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oCALCOLA_1_19.GetRadio()
    this.Parent.oContained.w_CALCOLA = this.RadioValue()
    return .t.
  endfunc

  func oCALCOLA_1_19.SetRadio()
    this.Parent.oContained.w_CALCOLA=trim(this.Parent.oContained.w_CALCOLA)
    this.value = ;
      iif(this.Parent.oContained.w_CALCOLA=='P',1,;
      iif(this.Parent.oContained.w_CALCOLA=='C',2,;
      0))
  endfunc

  func oCALCOLA_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (left(.w_CHIAVE,1) ='L')
    endwith
   endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="XEGWXEWFNR",left=619, top=430, width=48,height=45,;
    CpPicture="BMP\ULTVEN.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare il prezzo sulla riga del documento";
    , HelpContextID = 189913867;
    , tabStop=.f., Caption='\<Agg. prezzo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSVA_BBC(this.Parent.oContained,"U")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_SERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_ORIGINE<>'D')
     endwith
    endif
  endfunc

  add object oFLAGGIO_1_21 as StdCheck with uid="NVSQBRFIPR",rtseq=13,rtrep=.f.,left=621, top=15, caption="Aggiorna basi di calcolo",;
    ToolTipText = "Se attivo, alla pressione del bottone aggiorna vengono aggiornate le basi di calcolo secondo le variazioni effettuate ai prezzi di listino",;
    HelpContextID = 154079658,;
    cFormVar="w_FLAGGIO", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oFLAGGIO_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLAGGIO_1_21.GetRadio()
    this.Parent.oContained.w_FLAGGIO = this.RadioValue()
    return .t.
  endfunc

  func oFLAGGIO_1_21.SetRadio()
    this.Parent.oContained.w_FLAGGIO=trim(this.Parent.oContained.w_FLAGGIO)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGGIO=='S',1,;
      0)
  endfunc

  func oFLAGGIO_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (left(.w_CHIAVE,1) $'LC')
    endwith
   endif
  endfunc

  func oFLAGGIO_1_21.mHide()
    with this.Parent.oContained
      return (.w_ORIGINE='A')
    endwith
  endfunc


  add object oBtn_1_22 as StdButton with uid="IIIHCGWDQG",left=669, top=430, width=48,height=45,;
    CpPicture="BMP\INVENTAR.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare listini e basi di calcolo in seguito alle modifiche effettuate";
    , HelpContextID = 138364487;
    , tabStop=.f., Caption='\<Agg. list.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        GSVA_BBC(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_SERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="VAYMUUHAYT",left=799, top=430, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 108710586;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_25 as cp_runprogram with uid="DNCXRYGZBW",left=9, top=510, width=239,height=25,;
    caption='GSVA_BBC(S)',;
   bGlobalFont=.t.,;
    prg='GSVA_BBC("S")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 21965609


  add object oObj_1_26 as cp_runprogram with uid="QKHRGIARBC",left=9, top=537, width=239,height=25,;
    caption='GSVA_BBC(D)',;
   bGlobalFont=.t.,;
    prg='GSVA_BBC("D")',;
    cEvent = "Done",;
    nPag=1;
    , HelpContextID = 21961769


  add object oObj_1_29 as cp_calclbl with uid="PNKQPMDGQW",left=621, top=47, width=33,height=17,;
    caption='',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 116027914


  add object oObj_1_33 as cp_calclbl with uid="MYQBAOIESV",left=621, top=70, width=33,height=17,;
    caption='',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 116027914


  add object oObj_1_34 as cp_runprogram with uid="QRFIODYTGA",left=281, top=486, width=629,height=21,;
    caption='GSVA_BBC(P)',;
   bGlobalFont=.t.,;
    prg="GSVA_BBC('P')",;
    cEvent = "w_DATRIF Changed, w_QTAMOV Changed, w_CODART Changed, w_UNISEL Changed",;
    nPag=1;
    , HelpContextID = 21964841


  add object oObj_1_38 as cp_runprogram with uid="QKDIDEVOHV",left=281, top=508, width=357,height=23,;
    caption='GSVA_BBC(Z)',;
   bGlobalFont=.t.,;
    prg="GSVA_BBC('Z')",;
    cEvent = "w_Zoom row checked",;
    nPag=1;
    , HelpContextID = 21967401


  add object oObj_1_40 as cp_calclbl with uid="LLLUNFOIPR",left=621, top=93, width=33,height=17,;
    caption='',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 116027914


  add object oObj_1_41 as cp_runprogram with uid="QICDBHJACA",left=281, top=532, width=357,height=23,;
    caption='GSVA_BBC(C)',;
   bGlobalFont=.t.,;
    prg="GSVA_BBC('C')",;
    cEvent = "w_CALCOLA Changed",;
    nPag=1;
    , HelpContextID = 21961513


  add object oObj_1_44 as cp_runprogram with uid="NHJZCYDPIA",left=281, top=556, width=262,height=21,;
    caption='GSVA_BBC(U)',;
   bGlobalFont=.t.,;
    prg="GSVA_BBC('U')",;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 21966121

  add object oUNISEL_1_45 as StdField with uid="WSEZOVHKYH",rtseq=21,rtrep=.f.,;
    cFormVar = "w_UNISEL", cQueryName = "UNISEL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
    ToolTipText = "Unit� di misura di selezione",;
    HelpContextID = 105025210,;
   bGlobalFont=.t.,;
    Height=18, Width=39, Left=233, Top=42, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_UNISEL"

  func oUNISEL_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oUNISEL_1_45.ecpDrop(oSource)
    this.Parent.oContained.link_1_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUNISEL_1_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oUNISEL_1_45'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'GSVA1QUM.UNIMIS_VZM',this.parent.oContained
  endproc

  add object oBASCALC_1_62 as StdField with uid="XHSKWTDVKR",rtseq=37,rtrep=.f.,;
    cFormVar = "w_BASCALC", cQueryName = "BASCALC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore della base di calcolo selezionata",;
    HelpContextID = 158204694,;
   bGlobalFont=.t.,;
    Height=21, Width=130, Left=109, Top=97, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"

  func oBASCALC_1_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_BASCALC>0)
    endwith
    return bRes
  endfunc

  add object oStr_1_4 as StdString with uid="CFATLDLWOC",Visible=.t., Left=5, Top=15,;
    Alignment=1, Width=103, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="CXJQUBYAPJ",Visible=.t., Left=5, Top=42,;
    Alignment=1, Width=103, Height=18,;
    Caption="Data riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="RAJDKDWOZW",Visible=.t., Left=282, Top=42,;
    Alignment=1, Width=60, Height=18,;
    Caption="Quantit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="HAQSDIXQTT",Visible=.t., Left=5, Top=69,;
    Alignment=1, Width=103, Height=18,;
    Caption="Base di calcolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="BHWNOJFFMN",Visible=.t., Left=5, Top=98,;
    Alignment=1, Width=103, Height=18,;
    Caption="Valore base:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="WASNBKROBS",Visible=.t., Left=661, Top=47,;
    Alignment=0, Width=180, Height=17,;
    Caption="Listino da creare"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="HWSBNZHWFU",Visible=.t., Left=661, Top=70,;
    Alignment=0, Width=180, Height=17,;
    Caption="Riga modificata manualmente"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_37 as StdString with uid="DFFDKFJFCQ",Visible=.t., Left=156, Top=437,;
    Alignment=1, Width=170, Height=18,;
    Caption="Tipo di aggiornamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="NXFIJQQBKK",Visible=.t., Left=661, Top=94,;
    Alignment=0, Width=180, Height=17,;
    Caption="Base di calcolo incongruente"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="LNHTRQITFA",Visible=.t., Left=190, Top=42,;
    Alignment=1, Width=42, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsva_kbc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsva_kbc
* --- Classe per gestire la combo SERIAL
* --- derivata dalla classe combo da tabella

define class StdZTamTableCombo as StdTrsTableCombo

proc Init()
  IF VARTYPE(this.bNoBackColor)='U'
		This.backcolor=i_EBackColor
	ENDIF

endproc

proc SetModified()
    return

  proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear
      i_bCharKey=type(i_fk)='C'
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()]=trim(&i_fk)
        else
          this.combovalues[recno()]=&i_fk
        endif
        skip
      enddo
      use
        if this.nValues=1
         this.value=this.combovalues[1]
         this.parent.ocontained.w_CHIAVE=this.Value
         **ripristino il valore iniziale
         this.value=-1
         this.parent.ocontained.NotifyEvent("w_CHIAVE Changed")
      endif
    endif
  endproc
enddefine

* --- Blocca colonne Zoom
* --- per problema di c0005
proc Bloccazoom(oZoom)
   * --- Disabilito la possibilit� di muovere le colonne
   * --- x problema c0005...
   local ni
   For ni=1 to oZoom.Grd.ColumnCount
    oZoom.Grd.Columns[ni].movable=.f.
   endfor
endproc
* --- Fine Area Manuale
