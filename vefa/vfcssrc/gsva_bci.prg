* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsva_bci                                                        *
*              Calcola cauzione imballi                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-21                                                      *
* Last revis.: 2011-03-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsva_bci",oParentObject)
return(i_retval)

define class tgsva_bci as StdBatch
  * --- Local variables
  w_CFUNC = space(10)
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato alla notifyevent "CalcolaImballi" nel GSAR_BFA
    *     utilizzato per calcolare il costo totale degli imballi per valorizzare la cauzione sul documento
    *     
    *     Inoltre questo batch viene lanciato anche in modifica di un documento che genera imballi poich�
    *     valorizza i transitori dei due figli integrati in modo che il GSAR_BEA poi li possa utilizzare per generare i documenti
    this.w_PADRE = This.oParentObject
    this.w_CFUNC = this.w_PADRE.cFunction
    if this.oParentObject.w_MVFLFOCA<>"S"
      this.oParentObject.w_MVCAUIMB = 0
    endif
    if g_VEFA = "S"
      if this.oParentObject.w_MVTIPIMB$"RC" 
        * --- Prima di eliminare il documento apro il figlio integrato perch� in modifica devo avere i
        *     figli instanziati altrimenti non mi rigenera il documento di esplosione
        this.w_PADRE.GSAR_MIR.LinkPCClick(.T.)     
        this.w_PADRE.GSAR_MIR.HideChildrenChain()     
        * --- Metto bUpdated a F perch� non deve salvare niente e perch� altrimenti la
        *     ecpquit mi farebbe la domanda se voglio abbandonare le modifiche
        this.w_PADRE.GSAR_MIR.cnt.bUpdated = .F.
        * --- Non ricalcolo le cauzioni ma il batch lo devo lanciare comunque per ricreare 
        *     i temporanei dei figli in modo da utilizzarli nel GSAR_BEA
        *     Solo se non cauzioni forzate e se il cliente e la causale gestiscono le cauzioni
        if this.oParentObject.w_MVFLFOCA<>"S" And this.oParentObject.w_FLGCAU="S" And this.oParentObject.w_MVTIPIMB="C"
          this.oParentObject.w_MVCAUIMB = - this.w_PADRE.GSAR_MIR.cnt.w_TOTCAU
        endif
      endif
      * --- Ciclo sulle righe per gli imballi a perdere
      this.w_PADRE.MarkPos()     
      this.w_PADRE.FirstRow()     
      do while Not this.w_PADRE.Eof_Trs()
        this.w_PADRE.SetRow()     
        if this.w_PADRE.FullRow()
          if Not Empty(this.oParentObject.w_CODDIS) And this.oParentObject.w_MVTIPIMB <>"N" And CHKKITIMB(this.oParentObject.w_CODDIS) = "I"
            * --- Settando la propriet� ShowMMT (nativa della matricole) a N sui documenti
            *     il figlio integrato GSAR_MIP non viene visualizzato alla LinkPCClick
            this.w_PADRE.w_SHOWMMT = "N"
            this.w_PADRE.ChildrenChangeRow()     
            * --- Prima di eliminare il documento apro il figlio integrato perch� in modifica devo avere i
            *     figli instanziati altrimenti non mi rigenera il documento di esplosione
            this.w_PADRE.GSAR_MIP.LinkPCClick(.T.)     
            this.w_PADRE.GSAR_MIP.HideChildrenChain()     
            * --- Non ricalcolo le cauzioni ma il batch lo devo lanciare comunque per ricreare 
            *     i temporanei dei figli in modo da utilizzarli nel GSAR_BEA
            *     Solo se non cauzioni forzate e se il cliente e la causale gestiscono le cauzioni
            if this.oParentObject.w_MVFLFOCA<>"S" And this.oParentObject.w_FLGCAU="S" And this.oParentObject.w_MVTIPIMB="C"
              this.oParentObject.w_MVCAUIMB = this.oParentObject.w_MVCAUIMB + this.w_PADRE.GSAR_MIP.cnt.w_TOTCAU
            endif
            * --- Metto bUpdated a F perch� non deve salvare niente e perch� altrimenti la
            *     ecpquit mi farebbe la domanda se voglio abbandonare le modifiche
            this.w_PADRE.GSAR_MIP.cnt.bUpdated = .F.
            this.w_PADRE.GSAR_MIP.cnt.w_CONTROLLO = this.w_PADRE.w_KEYCHILD
          endif
        endif
        this.w_PADRE.NextRow()     
      enddo
      this.w_PADRE.RePos(.t.)     
      * --- Arrotondo ai decimali globali
      this.oParentObject.w_MVCAUIMB = cp_ROUND(this.oParentObject.w_MVCAUIMB, g_PERPVL)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
